#ifndef HW_AUDIO_OUT_H_
#define HW_AUDIO_OUT_H_

#include "adodec.h"
#include "ador.h"
#include "adoout.h"
#include "dac.h"
#include "i2s.h"

void hw_audio_out_sw_reset(void);
void hw_audio_out_csr_reset(void);
void hw_audio_out_reset(void);
void hw_audio_out_init(AudioDst dst);
void hw_audio_out_init_module(void);
void hw_audio_out_set_buffer(uint32_t addr, uint32_t length);
void hw_audio_out_dac_stream_start(void);
void hw_audio_out_dac_stream_stop(void);
void hw_audio_out_i2s_stream_start(void);
void hw_audio_out_i2s_stream_stop(void);
void hw_audio_out_unmask_irq(void);

void hw_audio_out_mask_irq_all(void);
void hw_audio_out_clear_irq_all(void);

#endif /* HW_AUDIO_OUT_H_ */
