#ifndef ADOENC_H_
#define ADOENC_H_

#include "csr_bank_adoenc.h"

typedef enum adoenc_ch_mode {
	ADOENC_CH_MODE_PASS_ALL = 0,
	ADOENC_CH_MODE_PASS_NONE,
	ADOENC_CH_MODE_PASS_EVEN,
	ADOENC_CH_MODE_PASS_ODD,
	ADOENC_CH_MODE_DUPLICATE,
} AdoencChMode;

typedef enum adoenc_compress_mode {
	ADOENC_COMPRESS_MODE_S16LE = 0,
	ADOENC_COMPRESS_MODE_S32LE,
	ADOENC_COMPRESS_MODE_A_LAW,
	ADOENC_COMPRESS_MODE_MU_LAW,
	ADOENC_COMPRESS_MODE_G726_4_LE,
	ADOENC_COMPRESS_MODE_G726_4_BE,
	ADOENC_COMPRESS_MODE_G726_2_LE,
	ADOENC_COMPRESS_MODE_G726_2_BE,
} AdoencCompressMode;

void adoenc_clear(void);
void adoenc_set_ch_mode(AdoencChMode mode);
int adoenc_set_compress_mode(AdoencCompressMode mode);
void adoenc_set_pre_shift(uint32_t shift);
void adoenc_set_post_shift(uint32_t shift);
void adoenc_set_gain(uint16_t gain);
uint16_t adoenc_get_gain(void);

#endif /* ADOENC_H_ */
