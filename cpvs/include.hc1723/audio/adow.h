#ifndef ADOW_H_
#define ADOW_H_

#include "csr_bank_audiow.h"

void adow_set_dram_config(uint32_t col_addr_type, uint32_t bank_addr_type);
void adow_start(void);
void adow_buffer_switch(void);
void adow_req_write_end(void);

void adow_set_burst_length(uint32_t length);
void adow_set_target_fifo_level(uint32_t level);
void adow_set_fifo_full_level(uint32_t level);
void adow_set_access_length(uint32_t length);
void adow_set_violation(uint32_t start, uint32_t end);
void adow_set_ini_addr_linear(uint32_t addr);

void adow_set_ts_scaler(uint32_t factor);
void adow_clear_ts(void);
void adow_set_sample_target(uint32_t target);
void adow_enable_ts(uint32_t en);
void adow_set_ts_clear_value(uint32_t value);
void adow_irq_clear_buffer_full_pause_avail(void);
uint8_t adow_status_buffer_full_pause_avail(void);
void adow_irq_mask_buffer_full_pause_avail(uint8_t mask);

uint32_t adow_get_ts(void);
uint32_t adow_get_ts_last(void);
uint32_t adow_get_capture_sample_cnt(void);
uint32_t adow_get_sample_cnt(void);

#endif /* ADOW_H_ */
