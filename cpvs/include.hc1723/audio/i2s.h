#ifndef I2S_H_
#define I2S_H_

#include "csr_bank_i2s.h"

typedef enum i2s_dir {
	I2S_DIR_IN = 0,
	I2S_DIR_OUT,
} I2sDir;

typedef enum i2s_mode {
	I2S_MODE_SLAVE = 0,
	I2S_MODE_MASTER,
} I2sMode;

typedef enum i2s_src {
	I2S_SRC_INTRA = 0,
	I2S_SRC_INTER,
} I2sSrc;

typedef enum i2s_dst {
	I2S_DST_INTRA = 0,
	I2S_DST_INTER,
} I2sDst;

void i2s_start(I2sDir dir);
void i2s_stop(I2sDir dir);

/* unit action */
void i2s_set_mode(I2sDir dir, I2sMode mode);
void i2s_set_bclk_src(I2sDir dir, I2sSrc src);
void i2s_set_ws_src(I2sDir dir, I2sSrc src);
void i2s_set_master_clk_params(I2sDir dir, uint32_t cyc_l, uint32_t cyc_h, uint32_t len);
void i2s_set_deglitch(I2sDir dir, uint32_t en, uint32_t bypass, uint32_t thres);
void i2s_set_mute(I2sDir dir, uint32_t mute);
void i2s_set_bclk_dst(I2sDir dir, I2sDst dst);
void i2s_set_ws_dst(I2sDir dir, I2sDst dst);

#endif /* I2S_H_ */
