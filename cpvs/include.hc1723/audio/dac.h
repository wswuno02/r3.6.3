#ifndef DAC_H_
#define DAC_H_

#include "csr_bank_adac_ctr.h"
#include "csr_bank_adaccfg.h"

void daccfg_endac(uint32_t en);
void daccfg_envbg(uint32_t en);

void dacctr_clk_start(void);
void dacctr_clk_stop(void);
void dacctr_endac(uint32_t en);
void dacctr_set_mute(uint32_t mute);
void dacctr_set_factor(uint32_t factor_l, uint32_t factor_h);

#endif /* DAC_H_ */
