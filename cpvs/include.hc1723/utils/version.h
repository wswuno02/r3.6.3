#ifndef VERSION_H_
#define VERSION_H_

void show_hardware_version();
void show_software_version();

#endif /* VERSION_H_ */
