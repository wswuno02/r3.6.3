#ifndef __DELAY_H_
#define __DELAY_H_

enum pll_status {
	PLL_OFF = 0,
	PLL_ON = 1,
};

unsigned int get_cpu_counter(void);

/** delay_ns_icache_on: Delay function when ICache is enabled.
 *
 * Maximum delay time limit for this function would be 2^31 / 451 = 4761604ns ~ 4.7 msec
 */
void delay_ns_icache_on(int ns, int is_pll_enabled);

#endif /* __DELAY_H_ */
