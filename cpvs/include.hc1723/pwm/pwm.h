#ifndef __PWM__H__
#define __PWM__H__

#include <stdint.h>
#include <time.h>

#define PWM0_BASE_ADDR 0x80060000
#define PWM1_BASE_ADDR 0x80060010
#define PWM2_BASE_ADDR 0x80060020
#define PWM3_BASE_ADDR 0x80060030
#define PWM4_BASE_ADDR 0x80060040
#define PWM5_BASE_ADDR 0x80060050

#define PWM_IRQ__MASK__DISABLE 0x0
#define PWM_IRQ__MASK__ENABLE  0x1

#define PWM_MODE__CONTINUOUS   0x0
#define PWM_MODE__FIXED__COUNT 0x1

#define PWM_REF_CLK_24MHZ   24000000
#define PWM_REF_CLK_125MHZ 125000000
#define MIN_PRESCALER 0x00
#define MAX_PRESCALER 0x18              // max decimal val is 24
#define MIN_COUNT__PERIOD 0x01
#define MAX_COUNT__PERIOD 0xFF

typedef struct pwm_dev {
    uint32_t ref_clk_rate;
    uint32_t mode;
    uint32_t counting_rate;
    struct timespec pulse_time;
    uint32_t duty_cycle_ratio;      // percentage (0-100)
    uint32_t num_period;
} PwmDev;

typedef struct pwm_dev_usr {
    uint32_t clk;
    uint32_t mode;
    struct timespec pulse_time;
    uint32_t duty_cycle_ratio;
    uint32_t tolerance;
    int32_t rounding_mode;          // -1: round down, 1: round up, 0: find closest
    uint32_t num_period;
} PwmDevUsr;

typedef struct pwm_dev_usr_adv {
    uint32_t mode;
    uint32_t num_period;
    uint32_t prescaler;
    uint32_t count_period;
    uint32_t count_high;
} PwmDevUsrAdv;

int raise(int signum);
void bsearch_prescaler(const uint32_t clk, uint32_t n, uint32_t *target, uint32_t *prescaler);
void pwm_prevent_overflow(uint32_t *para, uint32_t *rnd, uint32_t *div);
void pwm_init(struct pwm_dev *dev);
void pwm_trigger();
void pwm_mask(int32_t en);
void pwm_clr__match();
uint32_t pwm_busy();
uint32_t pwm_status();
uint32_t pwm_num__prd();
void pwm_extremum_init();
int32_t bsearch_cp(uint32_t start, int32_t rounding_mode, uint64_t target, const uint64_t baseline[], uint64_t tol, int64_t *dis);
int32_t bsearch_cp_equal(uint32_t start, uint64_t target, const uint64_t baseline[]);
void pwm_init_user(struct pwm_dev_usr *dev);
void pwm_init_user_adv(struct pwm_dev_usr_adv *dev);

#endif  /* __PWM__H__ */