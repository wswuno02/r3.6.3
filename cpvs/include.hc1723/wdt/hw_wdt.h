#ifndef HW_WDT_H_
#define HW_WDT_H_

#include "csr_bank_wdt.h"

void hw_wdt_unlock(volatile struct csr_bank_wdt *base);
void hw_wdt_lock(volatile struct csr_bank_wdt *base);
void hw_wdt_disable(volatile struct csr_bank_wdt *base);
void hw_wdt_enable(volatile struct csr_bank_wdt *base);
void hw_wdt_reboot(volatile struct csr_bank_wdt *base);

#endif /* HW_ADC_H_ */
