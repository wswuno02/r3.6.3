#ifndef KYOTO_CCQ_H_
#define KYOTO_CCQ_H_

#include "csr_bank_accq.h"
#include "printf.h"

#define u64 uint64_t
#define u32 uint32_t
#define s32 int
#define s16 short
#define u16 unsigned short
#define u8 uint8_t

#define AON_CCQ_BASE (0x80420000)
#define AON_CCQ_CMD_LENTH (60)
#define AON_CCQ_LENGTH_TEMP_ADDR (u32 *)(0x8050000C)

#define aon_ccq_print(format, args...) printf("[%s:%d] " format, __func__, __LINE__, ##args)
#define aon_ccq_dump_print(format, args...) printf("[%s:%d] " format, __func__, __LINE__, ##args, ##args)

typedef enum ccq_moudle {
	CCQ_MODULE_IS = 0,
	CCQ_MODULE_ISP,
	CCQ_MODULE_DISP,
	MAX_CCQ_MODULE,
} CcqModule;

typedef enum ccq_irq_flag {
	CCQ_IRQ_FLAG_ISSUE_IRQ,
	CCQ_IRQ_FLAG_CCQR,
	CCQ_IRQ_FLAG_CCQW,
} CcqIrqFlag;

typedef enum ccq_op_code {
	CCQ_OP_CODE_CSR_WRITE = 0,
	CCQ_OP_CODE_CSR_BURST_WRITE_INCR,
	CCQ_OP_CODE_CSR_BURST_WRITE_FIXED,
	CCQ_OP_CODE_CSR_READ,
	CCQ_OP_CODE_CSR_BURST_READ_INCR,
	CCQ_OP_CODE_CSR_BURST_READ_FIXED,
	CCQ_OP_CODE_CSR_POLLING,
	CCQ_OP_CODE_CSR_CHECK,
	CCQ_OP_CODE_WAIT,
	CCQ_OP_CODE_WAIT_IRQ,
	CCQ_OP_CODE_ISSUE_IRQ,
	MAX_CCQ_OP_CODE,
} CcqOpCode;

typedef enum aon_ccq_irq {
	AON_CCQ_PWM5_IRQ = 0,
	AON_CCQ_PWM4_IRQ,
	AON_CCQ_PWM3_IRQ,
	AON_CCQ_PWM2_IRQ,
	AON_CCQ_PWM1_IRQ,
	AON_CCQ_PWM0_IRQ,
	AON_CCQ_TIMER3_IRQ,
	AON_CCQ_TIMER2_IRQ,
	AON_CCQ_TIMER1_IRQ,
	AON_CCQ_TIMER0_IRQ,
	AON_CCQ_UART5_IRQ,
	AON_CCQ_UART4_IRQ,
	AON_CCQ_UART3_IRQ,
	AON_CCQ_UART2_IRQ,
	AON_CCQ_UART1_IRQ,
	AON_CCQ_UART0_IRQ,
	AON_CCQ_SPI1_IRQ,
	AON_CCQ_SPI0_IRQ,
	AON_CCQ_I2C1_IRQ,
	AON_CCQ_I2C0_IRQ,
} AonCcqIrq;

void aon_ccq_delay(u32 n);
u32 hw_aon_ccq_get_cmd_len();
u32 hw_aon_ccq_check_irq();
void hw_aon_ccq_dump_command(u32 *ptr, u32 instruction_length);
void hw_aon_ccq_add_csr_write_cmd(u32 *ptr, u32 csr_addr, u32 data);
void hw_aon_ccq_add_csr_polling_cmd(u32 *ptr, u32 csr_addr, u32 data);
void hw_aon_ccq_add_csr_check_cmd(u32 *ptr, u32 csr_addr, u32 data);
void hw_aon_ccq_add_csr_wait_cmd(u32 *ptr, u32 cycle);
void hw_aon_ccq_add_csr_wait_irq_cmd(u32 *ptr, u64 irq);
void hw_aon_ccq_add_csr_issue_irq_cmd(u32 *ptr);
void hw_aon_ccq_sram_buffer_setting(u32 *data, u32 instruction_length);
void hw_aon_ccq_start(u32 instruction_length);
void hw_aon_ccq_init();

#endif //KYOTO_CCQ_H_
