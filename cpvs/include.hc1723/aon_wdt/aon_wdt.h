#ifndef AON_WDT_H_
#define AON_WDT_H_

#include <stdint.h>

#define EN_BASE_ADDR 0x80530020
#define WDT_AON_ADDR 0x80550000
/* 1 offset in uint32 means 0x4 */
#define PRESCALER_OFFSET 1
#define CNT_TH0_OFFSET 2
#define CNT_TH2_OFFSET 4
#define STAGE_OFFSET 6
#define PRESCALER_VALUE 0
#define CNT_VALUE 1

struct aon_wdt_dev {
	volatile uint32_t *en_base_addr;
	volatile uint32_t *wdt_aon_addr;
};

void aon_wdt_init(struct aon_wdt_dev *dev);
void aon_wdt_trigger(struct aon_wdt_dev *dev);

#endif /* AON_WDT_H_ */
