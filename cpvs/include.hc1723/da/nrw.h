#ifndef HW_NRW_H
#define HW_NRW_H

#include "da_utils.h"
#include "csr_bank_nrw.h"

#ifndef __KERNEL__
#include <stdint.h>
void hw_nrw_frame_start();
void hw_nrw_irq_clear();
uint32_t hw_nrw_status_frame_end();
uint32_t hw_nrw_status_bw_insufficient();
uint32_t hw_nrw_status_access_violation();
void hw_nrw_irq_mask(uint32_t frame_end, uint32_t bw_insufficient, uint32_t access_violation);
void hw_nrw_dram_info(uint32_t col_addr_type, uint32_t bank_interleave_type, uint32_t bank_group_type);
void hw_nrw_access_illegal(uint32_t hang, uint32_t mask);
void hw_nrw_set_target_burst_len(uint32_t burst_len);

void hw_nrw_init();
void hw_nrw_256x1080_8b();
void hw_nrw_set_addr(uint32_t addr);
void hw_nrw_set_height(uint32_t height);
void hw_nrw_set_addr_new(uint32_t base, uint32_t add);
void hw_nrw_cal_tile_info(BlockAgentConfig *cfg, TileInfo *tile, BlockTileInfo *blk_tile);
void hw_nrw_set_init_csr(BlockAgentConfig *cfg, BlockTileInfo *blk_tile);
void hw_nrw_set_tile_csr(BlockAgentConfig *cfg, BlockTileInfo *blk_tile);
void nrw_set_init_csr(volatile CsrBankNrw *nrw, BlockAgentConfig *cfg, BlockTileInfo *blk_tile);
void nrw_calc_tile_info(DramAgentConfig *cfg, FrameInfo *frame, TileInfo *tile, DaBlockTile *da, uint16_t offset_x,
                        uint16_t offset_y);
void nrw_set_frame_csr(volatile CsrBankNrw *csr, DramAgentConfig *cfg, FrameInfo *frame);

typedef struct vp_nrw_coring_cfg {
	uint8_t enable;
	uint16_t th0;
	uint16_t slope;
} VpNrwCoringCfg;

void vp_nrw_set_coring_cfg(volatile CsrBankNrw *csr, const struct vp_nrw_coring_cfg *cfg);
void vp_nrw_get_coring_cfg(volatile CsrBankNrw *csr, struct vp_nrw_coring_cfg *cfg);
#else
#include <linux/types.h>
#endif

void nrw_set_tile_csr(volatile CsrBankNrw *csr, DaBlockTile *da, uint32_t init_addr);
uint32_t calc_nrw_buffer_size(const uint32_t width, const uint32_t height, const struct dram_agent_config *dram_agent);

#endif