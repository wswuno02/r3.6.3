#ifndef VPK_ME_H_
#define VPK_ME_H_

#include "common.h"
#include "da_define.h"
#include "isp_utils.h"
#include "csr_bank_me.h"

#define VP_ME_STRIP_MOTION_IDX 2
#define VP_ME_STRIP_MOTION_NUM 8
#define VP_ME_TONE_CURVE_IDX 2
#define VP_ME_TONE_CURVE_NUM 33

typedef struct vp_me_setting {
	uint32_t me_mode;
	uint32_t me_scene_change;

	uint32_t me_gmv_x;
	uint32_t me_gmv_y;
	uint32_t me_cand_valid;

	uint32_t me_bottom_left_dist_x;
	uint32_t me_bottom_left_dist_y;
	uint32_t me_bottom_right_dist_x;
	uint32_t me_bottom_right_dist_y;

	uint32_t me_mode_left_mv_diff_th;
	uint32_t me_mode_top_left_mv_diff_th;
	uint32_t me_mode_top_right_mv_diff_th;

	uint32_t me_search_texture_th;

	uint32_t me_sw_ix;
	uint32_t me_sw_iy;
	uint32_t me_sw_ix_size;
	uint32_t me_sw_iy_size;

	uint32_t me_sw_center_mode;
	uint32_t me_sw_center_weight;
	uint32_t me_sw_center_x_sw;
	uint32_t me_sw_center_y_sw;

	uint32_t me_ime_penalty_gain_x_sel;
	uint32_t me_ime_penalty_gain_y_sel;
	uint32_t me_ime_penalty_bound_x_ini;
	uint32_t me_ime_penalty_bound_y_ini;

	uint32_t me_matching_weight;
	uint32_t me_range_weight;
	uint32_t me_singularity_weight;

	uint32_t me_m_cost_quarter_gain;
	uint32_t me_m_cost_half_gain;
	uint32_t me_m_cost_frac_offset;

	uint32_t me_texture_gain_low_th;
	uint32_t me_texture_gain_high_th;
	uint32_t me_texture_gain_low;
	uint32_t me_texture_gain_high;
	uint32_t me_texture_gain_slope;
	uint32_t me_singularity_min_th;
	uint32_t me_singularity_max_th;

	uint32_t me_range_min_x_th;
	uint32_t me_range_max_x_th;
	uint32_t me_range_min_y_th;
	uint32_t me_range_max_y_th;

	uint32_t me_non_zero_mv_cost_offset;
	uint32_t me_spatial_cost_offset;
	uint32_t me_temporal_cost_offset;
	uint32_t me_search_weak_cost_offset;
	uint32_t me_search_cost_offset;
	uint32_t me_refine_cost_offset;
	uint32_t me_gmv_cost_offset;

	uint32_t me_valid_true_mv_th;

} Vp_Me_Setting;

typedef struct vp_me_satistic {
	uint32_t me_mv_roi_0_en;
	uint32_t me_mv_roi_0_clear;
	uint32_t me_mv_roi_0_sx;
	uint32_t me_mv_roi_0_sy;
	uint32_t me_mv_roi_0_ex;
	uint32_t me_mv_roi_0_ey;
	uint32_t me_mv_roi_0_x_accuracy;
	uint32_t me_mv_roi_0_y_accuracy;
	uint32_t me_mv_roi_0_step;
	//uint32_t me_mv_roi_0_hist[32];  //* read only *//
	//uint32_t me_mv_roi_0_valid_cnt; //* read only *//

} Vp_Me_Satistic;

typedef struct vp_me_strip_motion {
	uint32_t me_strip_motion_en[VP_ME_STRIP_MOTION_NUM];
	uint32_t me_strip_motion_clear[VP_ME_STRIP_MOTION_NUM];
	uint32_t me_strip_motion_mode[VP_ME_STRIP_MOTION_NUM];
	uint32_t me_strip_motion_th[VP_ME_STRIP_MOTION_NUM];
	uint32_t me_strip_motion_qs[VP_ME_STRIP_MOTION_NUM];
	uint32_t me_strip_motion_sx[VP_ME_STRIP_MOTION_NUM];
	uint32_t me_strip_motion_sy[VP_ME_STRIP_MOTION_NUM];
	uint32_t me_strip_motion_ex[VP_ME_STRIP_MOTION_NUM];
	uint32_t me_strip_motion_ey[VP_ME_STRIP_MOTION_NUM];
	//uint32_t me_strip_motion_index[VP_ME_STRIP_MOTION_NUM]; //* read only *//

} Vp_Me_Strip_Motion;

typedef struct vp_me_tone_curve {
	uint32_t tone_curve[33];

} Vp_Me_Tone_Cure;

typedef struct vp_me_y_avg {
	uint32_t me_y_avg_roi_0_en;
	uint32_t me_y_avg_roi_0_clear;
	uint32_t me_y_avg_roi_0_sx;
	uint32_t me_y_avg_roi_0_sy;
	uint32_t me_y_avg_roi_0_ex;
	uint32_t me_y_avg_roi_0_ey;
	uint32_t me_y_avg_roi_0_total_pix_cnt;
	//uint32_t me_y_avg_roi_0_before_cte; //* read only *//
	//uint32_t me_y_avg_roi_0_after_cte; //* read only *//

} Vp_Me_Y_Avg;

typedef struct vp_me_texture {
	uint32_t me_texture_roi_0_en;
	uint32_t me_texture_roi_0_clear;
	uint32_t me_texture_roi_0_sx;
	uint32_t me_texture_roi_0_sy;
	uint32_t me_texture_roi_0_ex;
	uint32_t me_texture_roi_0_ey;
	//uint32_t me_texture_roi_0_hor; //* read only *//
	//uint32_t me_texture_roi_0_ver; //* read only *//
	//uint32_t me_texture_roi_0_hor_ver; //* read only *//

} Vp_Me_Texture;

typedef struct vp_me_fg_y_avg {
	uint32_t me_fg_y_roi_0_en;
	uint32_t me_fg_y_roi_0_clear;
	uint32_t me_fg_y_roi_0_sx;
	uint32_t me_fg_y_roi_0_sy;
	uint32_t me_fg_y_roi_0_ex;
	uint32_t me_fg_y_roi_0_ey;
	uint32_t me_fg_y_roi_0_soft_clip_slope;
	uint32_t me_fg_y_roi_0_soft_clip_th;
	uint32_t me_fg_y_roi_0_gmv_x;
	uint32_t me_fg_y_roi_0_gmv_y;
	//uint32_t me_fg_y_roi_0_conf; //* read only *//
	//uint32_t me_fg_y_roi_0_pix_sum; //* read only *//
	// uint32_t me_fg_y_roi_0_hist[33]; //* read only *//

} Vp_Me_Fg_Y_Avg;

typedef struct vp_me_fg_texture {
	uint32_t me_fg_texture_roi_0_en;
	uint32_t me_fg_texture_roi_0_clear;
	uint32_t me_fg_texture_roi_0_sx;
	uint32_t me_fg_texture_roi_0_sy;
	uint32_t me_fg_texture_roi_0_ex;
	uint32_t me_fg_texture_roi_0_ey;
	uint32_t me_fg_texture_roi_0_soft_clip_slope;
	uint32_t me_fg_texture_roi_0_soft_clip_th;
	uint32_t me_fg_texture_roi_0_gmv_x;
	uint32_t me_fg_texture_roi_0_gmv_y;
	//uint32_t me_fg_texture_roi_0_conf; //* read only *//
	//uint32_t me_fg_texture_roi_0_hor; //* read only *//
	//uint32_t me_fg_texture_roi_0_ver; //* read only *//
	//uint32_t me_fg_texture_roi_0_hor_ver; //* read only *//

} Vp_Me_Fg_Texture;

typedef struct vp_me_probe {
	uint32_t me_probe_en;
	uint32_t me_probe_debug_mode;
	uint32_t me_probe_x;
	uint32_t me_probe_y;

	//* the following ctx iare all read only *//
	//uint32_t me_probe_cur_zero_texture;
	//uint32_t me_probe_searched_mv_x;
	//uint32_t me_probe_searched_mv_y;
	//uint32_t me_probe_searched_mv_valid;
	//uint32_t me_probe_searched_mv_type;
	//uint32_t me_probe_searched_ref_texture_clip;
	//uint32_t me_probe_searched_m_cost_raw;
	//uint32_t me_probe_spatial_0_mv_x;
	//uint32_t me_probe_spatial_0_mv_y;
	//uint32_t me_probe_spatial_0_mv_valid;
	//uint32_t me_probe_spatial_0_mv_type;
	//uint32_t me_probe_spatial_0_ref_texture_clip;
	//uint32_t me_probe_spatial_0_m_cost_raw;
	//uint32_t me_probe_spatial_1_mv_x;
	//uint32_t me_probe_spatial_1_mv_y;
	//uint32_t me_probe_spatial_1_mv_valid;
	//uint32_t me_probe_spatial_1_mv_type;
	//uint32_t me_probe_spatial_1_ref_texture_clip;
	//uint32_t me_probe_spatial_1_m_cost_raw;
	//uint32_t me_probe_spatial_2_mv_x;
	//uint32_t me_probe_spatial_2_mv_y;
	//uint32_t me_probe_spatial_2_mv_valid;
	//uint32_t me_probe_spatial_2_mv_type;
	//uint32_t me_probe_spatial_2_ref_texture_clip;
	//uint32_t me_probe_spatial_2_m_cost_raw;
	//uint32_t me_probe_temporal_0_mv_x;
	//uint32_t me_probe_temporal_0_mv_y;
	//uint32_t me_probe_temporal_0_mv_valid;
	//uint32_t me_probe_temporal_0_mv_type;
	//uint32_t me_probe_temporal_0_ref_texture_clip;
	//uint32_t me_probe_temporal_0_m_cost_raw;
	//uint32_t me_probe_temporal_1_mv_x;
	//uint32_t me_probe_temporal_1_mv_y;
	//uint32_t me_probe_temporal_1_mv_valid;
	//uint32_t me_probe_temporal_1_mv_type;
	//uint32_t me_probe_temporal_1_ref_texture_clip;
	//uint32_t me_probe_temporal_1_m_cost_raw;
	//uint32_t me_probe_refined_mv_x;
	//uint32_t me_probe_refined_mv_y;
	//uint32_t me_probe_refined_mv_valid;
	//uint32_t me_probe_refined_ref_texture_clip;
	//uint32_t me_probe_refined_m_cost_raw;
	//uint32_t me_probe_gmv_ref_texture_clip;
	//uint32_t me_probe_gmv_m_cost_raw;

} Vp_Me_Probe;

typedef struct vp_me_ctx {
	Vp_Me_Setting me_setting;
	Vp_Me_Satistic me_satistic;
	Vp_Me_Strip_Motion me_strip_motion;
	Vp_Me_Tone_Cure me_tone_curve;
	Vp_Me_Y_Avg me_y_avg;
	Vp_Me_Texture me_texture;
	Vp_Me_Fg_Y_Avg me_fg_y_avg;
	Vp_Me_Fg_Texture me_fg_texture;
	Vp_Me_Probe me_probe;
	uint32_t me_mv_ink_range_sel;

} VpMeCtx;

typedef struct vp_me_attr {
	uint32_t me_width;
	uint32_t me_height;

	uint32_t me_mode;
	uint32_t me_scene_change;

	uint32_t me_cand_valid;
	uint32_t me_sw_center_mode;

	uint32_t me_mv_roi_0_en;
	//uint32_t me_mv_roi_0_sx;
	//uint32_t me_mv_roi_0_sy;
	//uint32_t me_mv_roi_0_ex;
	//uint32_t me_mv_roi_0_ey;
	//uint32_t me_mv_roi_0_x_accuracy;
	//uint32_t me_mv_roi_0_y_accuracy;
	//uint32_t me_mv_roi_0_step;

	uint32_t me_strip_motion_en[VP_ME_STRIP_MOTION_NUM];
	uint32_t me_strip_motion_mode[VP_ME_STRIP_MOTION_NUM];
	//uint32_t me_strip_motion_th[VP_ME_STRIP_MOTION_NUM];
	//uint32_t me_strip_motion_qs[VP_ME_STRIP_MOTION_NUM];
	//uint32_t me_strip_motion_sx[VP_ME_STRIP_MOTION_NUM];
	//uint32_t me_strip_motion_sy[VP_ME_STRIP_MOTION_NUM];
	//uint32_t me_strip_motion_ex[VP_ME_STRIP_MOTION_NUM];
	//uint32_t me_strip_motion_ey[VP_ME_STRIP_MOTION_NUM];

	uint32_t me_tone_curve_idx;

	uint32_t me_y_avg_roi_0_en;
	//uint32_t me_y_avg_roi_0_sx;
	//uint32_t me_y_avg_roi_0_sy;
	//uint32_t me_y_avg_roi_0_ex;
	//uint32_t me_y_avg_roi_0_ey;
	//uint32_t me_y_avg_roi_0_total_pix_cnt;

	uint32_t me_texture_roi_0_en;
	//uint32_t me_texture_roi_0_sx;
	//uint32_t me_texture_roi_0_sy;
	//uint32_t me_texture_roi_0_ex;
	//uint32_t me_texture_roi_0_ey;

	uint32_t me_fg_y_roi_0_en;
	//uint32_t me_fg_y_roi_0_sx;
	//uint32_t me_fg_y_roi_0_sy;
	//uint32_t me_fg_y_roi_0_ex;
	//uint32_t me_fg_y_roi_0_ey;
	//uint32_t me_fg_y_roi_0_soft_clip_slope;
	//uint32_t me_fg_y_roi_0_soft_clip_th;
	//uint32_t me_fg_y_roi_0_gmv_x;
	//uint32_t me_fg_y_roi_0_gmv_y;

	uint32_t me_fg_texture_roi_0_en;
	//uint32_t me_fg_texture_roi_0_sx;
	//uint32_t me_fg_texture_roi_0_sy;
	//uint32_t me_fg_texture_roi_0_ex;
	//uint32_t me_fg_texture_roi_0_ey;
	//uint32_t me_fg_texture_roi_0_soft_clip_slope;
	//uint32_t me_fg_texture_roi_0_soft_clip_th;
	//uint32_t me_fg_texture_roi_0_gmv_x;
	//uint32_t me_fg_texture_roi_0_gmv_y;

	uint32_t me_mv_ink_range_sel;
	uint32_t probe_en;
	uint32_t probe_debug_mode;

} VpMeAttr;

void hw_vp_me_bypass(void);
void hw_vp_me_run(VpMeAttr *meattr);
void hw_vp_me_set_me(VpMeCtx *mectx, VpMeAttr *meattr);
void hw_vp_me_set_me_statistic(VpMeCtx *mectx, VpMeAttr *meattr);
void hw_vp_me_set_strip_motion(VpMeCtx *mectx, VpMeAttr *meattr);
void hw_vp_me_set_tone_curve(VpMeCtx *mectx, VpMeAttr *meattr);
void hw_vp_me_set_y_evg(VpMeCtx *mectx, VpMeAttr *meattr);
void hw_vp_me_set_texture(VpMeCtx *mectx, VpMeAttr *meattr);
void hw_vp_me_set_fg_y_evg(VpMeCtx *mectx, VpMeAttr *meattr);
void hw_vp_me_set_fg_texture(VpMeCtx *mectx, VpMeAttr *meattr);
void hw_vp_me_set_probe(VpMeCtx *mectx, VpMeAttr *meattr);
void hw_vp_me_set_reg(VpMeCtx *mectx);

#define ME_MV_HIST_ENTRY_NUM (32)
#define ME_MV_ROI_STAT_MVX_PADDING_BIT (22)
#define ME_MV_ROI_STAT_MVY_PADDING_BIT (12)
#define ME_MV_ROI_STAT_MVY_BIT_MASK (0b00000000001111111111)
#define ME_MV_ROI_STAT_COUNT_BIT_MASK (0b00000000000000000000111111111111)
#define ME_MAX_STRIP_MOTION_ROI_NUM (8)
#define ME_FG_Y_ENTRY_NUM (33)
#define PTE_CURVE_ENTRY_NUM (33)

typedef struct vp_pte_cfg {
	struct vp_pte_curve {
		uint16_t val[PTE_CURVE_ENTRY_NUM];
	} curve; /**< Post tone enhancement curve */
} VpPteCfg;

typedef enum vp_me_mode {
	VP_ME_MODE_NORMAL = 0,
	VP_ME_MODE_DISABLE = 1,
	VP_ME_MODE_MUM = 2,
} VpMeMode;

typedef struct vp_me_cfg {
	enum vp_me_mode mode;
// candidates
	uint8_t scene_change;
	uint16_t gmv_x;
	uint16_t gmv_y;
	uint8_t gmv_cand_valid;
	uint32_t mode_left_mv_diff_th;
	uint32_t mode_top_left_mv_diff_th;
	uint32_t mode_top_right_mv_diff_th;
	uint8_t bottom_left_dist_x;
	uint8_t bottom_left_dist_y;
	uint8_t bottom_right_dist_x;
	uint8_t bottom_right_dist_y;
// search range
	uint8_t sw_center_mode;
	uint16_t sw_center_x_sw;
	uint16_t sw_center_y_sw;
	uint8_t sw_center_weight;
	uint8_t sw_ix;
	uint8_t sw_iy;
	uint8_t sw_ix_size;
	uint8_t sw_iy_size;
	uint8_t mvr_left_extend_dist;
	uint8_t mvr_right_extend_dist;
// motion search
	uint8_t ime_penalty_gain_x_sel;
	uint8_t ime_penalty_gain_y_sel;
	uint8_t ime_penalty_bound_x_ini;
	uint8_t ime_penalty_bound_y_ini;
	uint16_t search_texture_th;
// cost
	uint16_t valid_true_mv_th;
	uint8_t matching_weight;
	uint8_t singularity_weight;
	uint8_t range_weight;
// block matching
	uint8_t m_cost_quarter_gain;
	uint8_t m_cost_half_gain;
	uint16_t m_cost_frac_offset;
// singularity
	uint16_t singularity_min_th;
	uint16_t singularity_max_th;
	uint16_t texture_gain_low_th;
	uint16_t texture_gain_high_th;
	uint8_t texture_gain_low;
	uint8_t texture_gain_high;
	uint8_t texture_gain_slope;
// mv range
	uint16_t range_min_x_th;
	uint16_t range_max_x_th;
	uint16_t range_min_y_th;
	uint16_t range_max_y_th;
// type
	uint16_t spatial_cost_offset;
	uint16_t temporal_cost_offset;
	uint16_t search_weak_cost_offset;
	uint16_t search_cost_offset;
	uint16_t refine_cost_offset;
	uint16_t gmv_cost_offset;
// non zero
	uint16_t non_zero_mv_cost_offset;
} VpMeCfg;

typedef struct vp_me_mv_roi_stat_frame_ctrl {
	uint16_t sy;
	uint16_t ey;
	uint8_t x_accuracy;
	uint8_t y_accuracy;
	uint8_t step;
} VpMeMvRoiStatFrameCtrl;

typedef IspvpTileCtrl VpMeMvRoiStatTileCtrl;

typedef struct vp_me_mv_hist {
	uint16_t x;
	uint16_t y;
	uint16_t count;
} VpMeMvHist;

typedef struct vp_me_mv_roi_stat {
	struct vp_me_mv_hist hist[ME_MV_HIST_ENTRY_NUM];
	uint32_t valid_cnt;
} VpMeMvRoiStat;

typedef struct vp_me_mv_roi_raw_stat {
	uint32_t hist[ME_MV_HIST_ENTRY_NUM];
	uint32_t valid_cnt;
} VpMeMvRoiRawStat;

typedef enum vp_me_strip_motion_mode {
	VP_ME_STRIP_MOTION_MODE_UNI_DIR = 0,
	VP_ME_STRIP_MOTION_MODE_BI_DIR = 1,
	VP_ME_STRIP_MOTION_MODE_NUM = 2,
} VpMeStripMotionMode;

typedef struct vp_me_strip_motion_roi_stat_frame_ctrl {
	enum vp_me_strip_motion_mode mode;
	uint8_t th_2s;
	uint8_t qs;
	uint16_t sy;
	uint16_t ey;
} VpMeStripMotionRoiStatFrameCtrl;

typedef struct vp_me_strip_motion_roi_stat_tile_ctrl {
	uint16_t sx;
	uint16_t ex;
} VpMeStripMotionRoiStatTileCtrl;

typedef struct vp_me_strip_motion_roi_stat {
	uint16_t index;
} VpMeStripMotionRoiStat;

typedef IspvpFrameCtrl VpMeYAvgRoiStatFrameCtrl;

typedef IspvpTileCtrl VpMeYAvgRoiStatTileCtrl;

typedef struct vp_me_y_avg_roi_stat {
	uint16_t luma_before_pte;
	uint16_t luma_after_pte;
} VpMeYAvgRoiStat;

typedef struct vp_me_texture_roi_stat_frame_ctrl {
	uint16_t sy;
	uint16_t ey;
} VpMeTextureRoiStatFrameCtrl;

typedef IspvpTileCtrl VpMeTextureRoiStatTileCtrl;

typedef struct vp_me_texture_roi_stat {
	uint32_t hor;
	uint32_t ver;
	uint32_t hor_ver;
} VpMeTextureRoiStat;

typedef struct vp_me_fg_y_roi_stat_frame_ctrl {
	uint16_t sy;
	uint16_t ey;
	uint8_t soft_clip_slope;
	uint8_t soft_clip_th;
	uint16_t gmv_x;
	uint16_t gmv_y;
} VpMeFgYRoiStatFrameCtrl;

typedef struct vp_me_fg_y_roi_stat_tile_ctrl {
	uint16_t sx;
	uint16_t ex;
} VpMeFgYRoiStatTileCtrl;

typedef struct vp_me_fg_y_roi_stat {
	uint32_t hist[ME_FG_Y_ENTRY_NUM];
	uint32_t confidence;
	uint32_t pix_sum;
} VpMeFgYRoiStat;

typedef struct vp_me_fg_texture_roi_stat_frame_ctrl {
	uint16_t sy;
	uint16_t ey;
	uint8_t soft_clip_slope;
	uint8_t soft_clip_th;
	uint16_t gmv_x;
	uint16_t gmv_y;
} VpMeFgTextureRoiStatFrameCtrl;

typedef struct vp_me_fg_texture_roi_stat_tile_ctrl {
	uint16_t sx;
	uint16_t ex;
} VpMeFgTextureRoiStatTileCtrl;

typedef struct vp_me_fg_texture_roi_stat {
	uint32_t confidence;
	uint32_t hor;
	uint32_t ver;
	uint32_t hor_ver;
} VpMeFgTextureRoiStat;

/* PTE*/
void hw_vp_pte_set_cfg(const struct vp_pte_cfg *cfg);
void hw_vp_pte_get_cfg(struct vp_pte_cfg *cfg);

/* ME*/
void hw_vp_me_set_cfg(const struct vp_me_cfg *cfg);
void hw_vp_me_get_cfg(struct vp_me_cfg *cfg);

/* Statistics */
// MV
void hw_vp_me_set_mv_roi_stat_frame_ctrl(const struct vp_me_mv_roi_stat_frame_ctrl *cfg);
void hw_vp_me_get_mv_roi_stat_frame_ctrl(struct vp_me_mv_roi_stat_frame_ctrl *cfg);
void hw_vp_me_set_mv_roi_stat_tile_ctrl(const VpMeMvRoiStatTileCtrl *cfg);
void hw_vp_me_get_mv_roi_stat_tile_ctrl(VpMeMvRoiStatTileCtrl *cfg);
void hw_vp_me_set_mv_roi_stat_clear(uint8_t clear);
uint8_t hw_vp_me_get_mv_roi_stat_clear(void);
void hw_vp_me_set_mv_roi_stat_enable(uint8_t enable);
uint8_t hw_vp_me_get_mv_roi_stat_enable(void);
void hw_vp_me_get_mv_roi_stat(struct vp_me_mv_roi_stat *stat);
void hw_vp_me_get_mv_roi_raw_stat(struct vp_me_mv_roi_raw_stat *stat);
// Strip Motion
void hw_vp_me_set_strip_motion_roi_stat_frame_ctrl(uint8_t idx, const struct vp_me_strip_motion_roi_stat_frame_ctrl *cfg);
void hw_vp_me_get_strip_motion_roi_stat_frame_ctrl(uint8_t idx, struct vp_me_strip_motion_roi_stat_frame_ctrl *cfg);
void hw_vp_me_set_strip_motion_roi_stat_tile_ctrl(uint8_t idx, const struct vp_me_strip_motion_roi_stat_tile_ctrl *cfg);
void hw_vp_me_get_strip_motion_roi_stat_tile_ctrl(uint8_t idx, struct vp_me_strip_motion_roi_stat_tile_ctrl *cfg);
void hw_vp_me_set_strip_motion_roi_stat_clear(uint8_t idx, uint8_t clear);
uint8_t hw_vp_me_get_strip_motion_roi_stat_clear(uint8_t idx);
void hw_vp_me_set_strip_motion_roi_stat_enable(uint8_t idx, uint8_t enable);
uint8_t hw_vp_me_get_strip_motion_roi_stat_enable(uint8_t idx);
void hw_vp_me_get_strip_motion_roi_stat(uint8_t idx, struct vp_me_strip_motion_roi_stat *stat);
// Y Avg
void hw_vp_me_set_y_avg_roi_stat_frame_ctrl(const VpMeYAvgRoiStatFrameCtrl *cfg);
void hw_vp_me_get_y_avg_roi_stat_frame_ctrl(VpMeYAvgRoiStatFrameCtrl *cfg);
void hw_vp_me_set_y_avg_roi_stat_tile_ctrl(const VpMeYAvgRoiStatTileCtrl *cfg);
void hw_vp_me_get_y_avg_roi_stat_tile_ctrl(VpMeYAvgRoiStatTileCtrl *cfg);
void hw_vp_me_set_y_avg_roi_stat_clear(uint8_t clear);
uint8_t hw_vp_me_get_y_avg_roi_stat_clear(void);
void hw_vp_me_set_y_avg_roi_stat_enable(uint8_t enable);
uint8_t hw_vp_me_get_y_avg_roi_stat_enable(void);
void hw_vp_me_get_y_avg_roi_stat(struct vp_me_y_avg_roi_stat *stat);
// Texture
void hw_vp_me_set_texture_roi_stat_frame_ctrl(const struct vp_me_texture_roi_stat_frame_ctrl *cfg);
void hw_vp_me_get_texture_roi_stat_frame_ctrl(struct vp_me_texture_roi_stat_frame_ctrl *cfg);
void hw_vp_me_set_texture_roi_stat_tile_ctrl(const VpMeTextureRoiStatTileCtrl *cfg);
void hw_vp_me_get_texture_roi_stat_tile_ctrl(VpMeTextureRoiStatTileCtrl *cfg);
void hw_vp_me_set_texture_roi_stat_clear(uint8_t clear);
uint8_t hw_vp_me_get_texture_roi_stat_clear(void);
void hw_vp_me_set_texture_roi_stat_enable(uint8_t enable);
uint8_t hw_vp_me_get_texture_roi_stat_enable(void);
void hw_vp_me_get_texture_roi_stat(struct vp_me_texture_roi_stat *stat);
// Fg Y
void hw_vp_me_set_fg_y_roi_stat_frame_ctrl(const struct vp_me_fg_y_roi_stat_frame_ctrl *cfg);
void hw_vp_me_get_fg_y_roi_stat_frame_ctrl(struct vp_me_fg_y_roi_stat_frame_ctrl *cfg);
void hw_vp_me_set_fg_y_roi_stat_tile_ctrl(const struct vp_me_fg_y_roi_stat_tile_ctrl *cfg);
void hw_vp_me_get_fg_y_roi_stat_tile_ctrl(struct vp_me_fg_y_roi_stat_tile_ctrl *cfg);
void hw_vp_me_set_fg_y_roi_stat_clear(uint8_t clear);
uint8_t hw_vp_me_get_fg_y_roi_stat_clear(void);
void hw_vp_me_set_fg_y_roi_stat_enable(uint8_t enable);
uint8_t hw_vp_me_get_fg_y_roi_stat_enable(void);
void hw_vp_me_get_fg_y_roi_stat(struct vp_me_fg_y_roi_stat *stat);
// Fg Texture
void hw_vp_me_set_fg_texture_roi_stat_frame_ctrl(const struct vp_me_fg_texture_roi_stat_frame_ctrl *cfg);
void hw_vp_me_get_fg_texture_roi_stat_frame_ctrl(struct vp_me_fg_texture_roi_stat_frame_ctrl *cfg);
void hw_vp_me_set_fg_texture_roi_stat_tile_ctrl(const struct vp_me_fg_texture_roi_stat_tile_ctrl *cfg);
void hw_vp_me_get_fg_texture_roi_stat_tile_ctrl(struct vp_me_fg_texture_roi_stat_tile_ctrl *cfg);
void hw_vp_me_set_fg_texture_roi_stat_clear(uint8_t clear);
uint8_t hw_vp_me_get_fg_texture_roi_stat_clear(void);
void hw_vp_me_set_fg_texture_roi_stat_enable(uint8_t enable);
uint8_t hw_vp_me_get_fg_texture_roi_stat_enable(void);
void hw_vp_me_get_fg_texture_roi_stat(struct vp_me_fg_texture_roi_stat *stat);

//
/* PTE*/
void vp_pte_set_cfg(volatile CsrBankMe *csr, const struct vp_pte_cfg *cfg);
void vp_pte_get_cfg(volatile CsrBankMe *csr, struct vp_pte_cfg *cfg);

/* ME*/
void vp_me_set_cfg(volatile CsrBankMe *csr, const struct vp_me_cfg *cfg);
void vp_me_get_cfg(volatile CsrBankMe *csr, struct vp_me_cfg *cfg);

/* Statistics */
// MV
void vp_me_set_mv_roi_stat_frame_ctrl(volatile CsrBankMe *csr, const struct vp_me_mv_roi_stat_frame_ctrl *cfg);
void vp_me_get_mv_roi_stat_frame_ctrl(volatile CsrBankMe *csr, struct vp_me_mv_roi_stat_frame_ctrl *cfg);
void vp_me_set_mv_roi_stat_tile_ctrl(volatile CsrBankMe *csr, const VpMeMvRoiStatTileCtrl *cfg);
void vp_me_get_mv_roi_stat_tile_ctrl(volatile CsrBankMe *csr, VpMeMvRoiStatTileCtrl *cfg);
void vp_me_set_mv_roi_stat_clear(volatile CsrBankMe *csr, uint8_t clear);
uint8_t vp_me_get_mv_roi_stat_clear(volatile CsrBankMe *csr);
void vp_me_set_mv_roi_stat_enable(volatile CsrBankMe *csr, uint8_t enable);
uint8_t vp_me_get_mv_roi_stat_enable(volatile CsrBankMe *csr);
void vp_me_get_mv_roi_stat(volatile CsrBankMe *csr, struct vp_me_mv_roi_stat *stat);
void vp_me_get_mv_roi_raw_stat(volatile CsrBankMe *csr, struct vp_me_mv_roi_raw_stat *stat);
// Strip Motion
void vp_me_set_strip_motion_roi_stat_frame_ctrl(volatile CsrBankMe *csr, uint8_t idx,
                                                const struct vp_me_strip_motion_roi_stat_frame_ctrl *cfg);
void vp_me_get_strip_motion_roi_stat_frame_ctrl(volatile CsrBankMe *csr, uint8_t idx,
                                                struct vp_me_strip_motion_roi_stat_frame_ctrl *cfg);
void vp_me_set_strip_motion_roi_stat_tile_ctrl(volatile CsrBankMe *csr, uint8_t idx,
                                               const struct vp_me_strip_motion_roi_stat_tile_ctrl *cfg);
void vp_me_get_strip_motion_roi_stat_tile_ctrl(volatile CsrBankMe *csr, uint8_t idx,
                                               struct vp_me_strip_motion_roi_stat_tile_ctrl *cfg);
void vp_me_set_strip_motion_roi_stat_clear(volatile CsrBankMe *csr, uint8_t idx, uint8_t clear);
uint8_t vp_me_get_strip_motion_roi_stat_clear(volatile CsrBankMe *csr, uint8_t idx);
void vp_me_set_strip_motion_roi_stat_enable(volatile CsrBankMe *csr, uint8_t idx, uint8_t enable);
uint8_t vp_me_get_strip_motion_roi_stat_enable(volatile CsrBankMe *csr, uint8_t idx);
void vp_me_get_strip_motion_roi_stat(volatile CsrBankMe *csr, uint8_t idx, struct vp_me_strip_motion_roi_stat *stat);
// Y Avg
void vp_me_set_y_avg_roi_stat_frame_ctrl(volatile CsrBankMe *csr, const VpMeYAvgRoiStatFrameCtrl *cfg);
void vp_me_get_y_avg_roi_stat_frame_ctrl(volatile CsrBankMe *csr, VpMeYAvgRoiStatFrameCtrl *cfg);
void vp_me_set_y_avg_roi_stat_tile_ctrl(volatile CsrBankMe *csr, const VpMeYAvgRoiStatTileCtrl *cfg);
void vp_me_get_y_avg_roi_stat_tile_ctrl(volatile CsrBankMe *csr, VpMeYAvgRoiStatTileCtrl *cfg);
void vp_me_set_y_avg_roi_stat_clear(volatile CsrBankMe *csr, uint8_t clear);
uint8_t vp_me_get_y_avg_roi_stat_clear(volatile CsrBankMe *csr);
void vp_me_set_y_avg_roi_stat_enable(volatile CsrBankMe *csr, uint8_t enable);
uint8_t vp_me_get_y_avg_roi_stat_enable(volatile CsrBankMe *csr);
void vp_me_get_y_avg_roi_stat(volatile CsrBankMe *csr, struct vp_me_y_avg_roi_stat *stat);
// Texture
void vp_me_set_texture_roi_stat_frame_ctrl(volatile CsrBankMe *csr,
                                           const struct vp_me_texture_roi_stat_frame_ctrl *cfg);
void vp_me_get_texture_roi_stat_frame_ctrl(volatile CsrBankMe *csr, struct vp_me_texture_roi_stat_frame_ctrl *cfg);
void vp_me_set_texture_roi_stat_tile_ctrl(volatile CsrBankMe *csr, const VpMeTextureRoiStatTileCtrl *cfg);
void vp_me_get_texture_roi_stat_tile_ctrl(volatile CsrBankMe *csr, VpMeTextureRoiStatTileCtrl *cfg);
void vp_me_set_texture_roi_stat_clear(volatile CsrBankMe *csr, uint8_t clear);
uint8_t vp_me_get_texture_roi_stat_clear(volatile CsrBankMe *csr);
void vp_me_set_texture_roi_stat_enable(volatile CsrBankMe *csr, uint8_t enable);
uint8_t vp_me_get_texture_roi_stat_enable(volatile CsrBankMe *csr);
void vp_me_get_texture_roi_stat(volatile CsrBankMe *csr, struct vp_me_texture_roi_stat *stat);
// Fg Y
void vp_me_set_fg_y_roi_stat_frame_ctrl(volatile CsrBankMe *csr, const struct vp_me_fg_y_roi_stat_frame_ctrl *cfg);
void vp_me_get_fg_y_roi_stat_frame_ctrl(volatile CsrBankMe *csr, struct vp_me_fg_y_roi_stat_frame_ctrl *cfg);
void vp_me_set_fg_y_roi_stat_tile_ctrl(volatile CsrBankMe *csr, const struct vp_me_fg_y_roi_stat_tile_ctrl *cfg);
void vp_me_get_fg_y_roi_stat_tile_ctrl(volatile CsrBankMe *csr, struct vp_me_fg_y_roi_stat_tile_ctrl *cfg);
void vp_me_set_fg_y_roi_stat_clear(volatile CsrBankMe *csr, uint8_t clear);
uint8_t vp_me_get_fg_y_roi_stat_clear(volatile CsrBankMe *csr);
void vp_me_set_fg_y_roi_stat_enable(volatile CsrBankMe *csr, uint8_t enable);
uint8_t vp_me_get_fg_y_roi_stat_enable(volatile CsrBankMe *csr);
void vp_me_get_fg_y_roi_stat(volatile CsrBankMe *csr, struct vp_me_fg_y_roi_stat *stat);
// Fg Texture
void vp_me_set_fg_texture_roi_stat_frame_ctrl(volatile CsrBankMe *csr,
                                              const struct vp_me_fg_texture_roi_stat_frame_ctrl *cfg);
void vp_me_get_fg_texture_roi_stat_frame_ctrl(volatile CsrBankMe *csr,
                                              struct vp_me_fg_texture_roi_stat_frame_ctrl *cfg);
void vp_me_set_fg_texture_roi_stat_tile_ctrl(volatile CsrBankMe *csr,
                                             const struct vp_me_fg_texture_roi_stat_tile_ctrl *cfg);
void vp_me_get_fg_texture_roi_stat_tile_ctrl(volatile CsrBankMe *csr, struct vp_me_fg_texture_roi_stat_tile_ctrl *cfg);
void vp_me_set_fg_texture_roi_stat_clear(volatile CsrBankMe *csr, uint8_t clear);
uint8_t vp_me_get_fg_texture_roi_stat_clear(volatile CsrBankMe *csr);
void vp_me_set_fg_texture_roi_stat_enable(volatile CsrBankMe *csr, uint8_t enable);
uint8_t vp_me_get_fg_texture_roi_stat_enable(volatile CsrBankMe *csr);
void vp_me_get_fg_texture_roi_stat(volatile CsrBankMe *csr, struct vp_me_fg_texture_roi_stat *stat);

#endif