#ifndef VPK_NR_H_
#define VPK_NR_H_

#include "common.h"
#include "csr_bank_nr.h"

typedef struct vp_nr_setting {
	uint32_t nr_mode;

	//* 2D Y *//
	uint32_t nr_fir_kernel_sel_y;
	uint32_t nr_fir_weight_y;
	uint32_t nr_iir_y_conf_sel;
	uint32_t nr_iir_alpha_y_gain;

	//* 2D C *//
	uint32_t nr_fir_weight_c;
	uint32_t nr_iir_alpha_c_gain;

	//* 3D MA Y *//
	uint32_t nr_ma_y_lut_step;
	uint32_t nr_ma_y_lut_i_0;
	uint32_t nr_ma_y_lut_i_1;
	uint32_t nr_ma_y_lut_i_2;
	uint32_t nr_ma_y_lut_i_3;
	uint32_t nr_ma_y_lut_i_4;
	uint32_t nr_ma_y_lut_i_5;
	uint32_t nr_ma_y_lut_i_6;
	uint32_t nr_ma_y_lut_i_7;
	uint32_t nr_ma_y_lut_i_8;
	uint32_t nr_ma_y_lut_o_0;
	uint32_t nr_ma_y_lut_o_1;
	uint32_t nr_ma_y_lut_o_2;
	uint32_t nr_ma_y_lut_o_3;
	uint32_t nr_ma_y_lut_o_4;
	uint32_t nr_ma_y_lut_o_5;
	uint32_t nr_ma_y_lut_o_6;
	uint32_t nr_ma_y_lut_o_7;
	uint32_t nr_ma_y_lut_o_8;
	uint32_t nr_ma_y_lut_slope_0;
	uint32_t nr_ma_y_lut_slope_1;
	uint32_t nr_ma_y_lut_slope_2;
	uint32_t nr_ma_y_lut_slope_3;
	uint32_t nr_ma_y_lut_slope_4;
	uint32_t nr_ma_y_lut_slope_5;
	uint32_t nr_ma_y_lut_slope_6;
	uint32_t nr_ma_y_lut_slope_7;

	uint32_t nr_ma_y_lp_dyn_gain_th_min;
	uint32_t nr_ma_y_lp_dyn_gain_th_max;
	uint32_t nr_ma_y_lp_dyn_gain_min;
	uint32_t nr_ma_y_lp_dyn_gain_max;
	uint32_t nr_ma_y_lp_dyn_gain_slope;

	uint32_t nr_ma_y_lp_diff_gain_th_min;
	uint32_t nr_ma_y_lp_diff_gain_th_range;
	uint32_t nr_ma_y_lp_diff_gain_min;
	uint32_t nr_ma_y_lp_diff_gain_max;

	//* 3D MC Y *//
	uint32_t nr_mc_y_lut_step;
	uint32_t nr_mc_y_lut_i_0;
	uint32_t nr_mc_y_lut_i_1;
	uint32_t nr_mc_y_lut_i_2;
	uint32_t nr_mc_y_lut_i_3;
	uint32_t nr_mc_y_lut_i_4;
	uint32_t nr_mc_y_lut_i_5;
	uint32_t nr_mc_y_lut_i_6;
	uint32_t nr_mc_y_lut_i_7;
	uint32_t nr_mc_y_lut_i_8;

	uint32_t nr_mc_y_lut_o_0;
	uint32_t nr_mc_y_lut_o_1;
	uint32_t nr_mc_y_lut_o_2;
	uint32_t nr_mc_y_lut_o_3;
	uint32_t nr_mc_y_lut_o_4;
	uint32_t nr_mc_y_lut_o_5;
	uint32_t nr_mc_y_lut_o_6;
	uint32_t nr_mc_y_lut_o_7;
	uint32_t nr_mc_y_lut_o_8;

	uint32_t nr_mc_y_lut_slope_0;
	uint32_t nr_mc_y_lut_slope_1;
	uint32_t nr_mc_y_lut_slope_2;
	uint32_t nr_mc_y_lut_slope_3;
	uint32_t nr_mc_y_lut_slope_4;
	uint32_t nr_mc_y_lut_slope_5;
	uint32_t nr_mc_y_lut_slope_6;
	uint32_t nr_mc_y_lut_slope_7;

	uint32_t nr_mc_y_lp_dyn_gain_th_min;
	uint32_t nr_mc_y_lp_dyn_gain_th_max;
	uint32_t nr_mc_y_lp_dyn_gain_min;
	uint32_t nr_mc_y_lp_dyn_gain_max;
	uint32_t nr_mc_y_lp_dyn_gain_slope;

	uint32_t nr_mc_y_lp_diff_gain_th_min;
	uint32_t nr_mc_y_lp_diff_gain_th_range;
	uint32_t nr_mc_y_lp_diff_gain_min;
	uint32_t nr_mc_y_lp_diff_gain_max;

	//* 3D Y *//
	uint32_t nr_mc_y_base_ratio;
	uint32_t nr_mc_y_base_dyn;

	//* 3D MA C *//
	uint32_t nr_ma_c_lut_step;
	uint32_t nr_ma_c_lut_i_0;
	uint32_t nr_ma_c_lut_i_1;
	uint32_t nr_ma_c_lut_i_2;
	uint32_t nr_ma_c_lut_i_3;
	uint32_t nr_ma_c_lut_i_4;
	uint32_t nr_ma_c_lut_i_5;
	uint32_t nr_ma_c_lut_i_6;
	uint32_t nr_ma_c_lut_i_7;
	uint32_t nr_ma_c_lut_i_8;

	uint32_t nr_ma_c_lut_o_0;
	uint32_t nr_ma_c_lut_o_1;
	uint32_t nr_ma_c_lut_o_2;
	uint32_t nr_ma_c_lut_o_3;
	uint32_t nr_ma_c_lut_o_4;
	uint32_t nr_ma_c_lut_o_5;
	uint32_t nr_ma_c_lut_o_6;
	uint32_t nr_ma_c_lut_o_7;
	uint32_t nr_ma_c_lut_o_8;

	uint32_t nr_ma_c_lut_slope_0;
	uint32_t nr_ma_c_lut_slope_1;
	uint32_t nr_ma_c_lut_slope_2;
	uint32_t nr_ma_c_lut_slope_3;
	uint32_t nr_ma_c_lut_slope_4;
	uint32_t nr_ma_c_lut_slope_5;
	uint32_t nr_ma_c_lut_slope_6;
	uint32_t nr_ma_c_lut_slope_7;

	uint32_t nr_ma_c_weight_y_ref_sel;

	uint32_t nr_ma_c_weight_y_gain_th_min;
	uint32_t nr_ma_c_weight_y_gain_th_max;
	uint32_t nr_ma_c_weight_y_gain_min;
	uint32_t nr_ma_c_weight_y_gain_max;
	uint32_t nr_ma_c_weight_y_gain_slope;

} Vp_Nr_Setting;

typedef struct vp_nr_abs_diff {
	uint32_t nr_abs_diff_hist_en;
	uint32_t nr_abs_diff_hist_clear;

	uint32_t nr_abs_diff_hist_sel;
	uint32_t nr_abs_diff_hist_mode;
	uint32_t nr_abs_diff_hist_step;

	uint32_t nr_abs_diff_hist_sx;
	uint32_t nr_abs_diff_hist_sy;
	uint32_t nr_abs_diff_hist_ex;
	uint32_t nr_abs_diff_hist_ey;

	//* read only *//
	//uint32_t nr_abs_diff_hist_overflow;
	//uint32_t nr_abs_diff_hist_0;
	//uint32_t nr_abs_diff_hist_1;
	//uint32_t nr_abs_diff_hist_2;
	//uint32_t nr_abs_diff_hist_3;
	//uint32_t nr_abs_diff_hist_4;
	//uint32_t nr_abs_diff_hist_5;
	//uint32_t nr_abs_diff_hist_6;
	//uint32_t nr_abs_diff_hist_7;
	//uint32_t nr_abs_diff_hist_8;
	//uint32_t nr_abs_diff_hist_9;
	//uint32_t nr_abs_diff_hist_10;
	//uint32_t nr_abs_diff_hist_11;
	//uint32_t nr_abs_diff_hist_12;
	//uint32_t nr_abs_diff_hist_13;
	//uint32_t nr_abs_diff_hist_14;
	//uint32_t nr_abs_diff_hist_15;
	//uint32_t nr_abs_diff_hist_16;
	//uint32_t nr_abs_diff_hist_17;
	//uint32_t nr_abs_diff_hist_18;

} Vp_Nr_Abs_Diff;

typedef struct vp_nr_tdiff {
	uint32_t nr_tdiff_roi_0_en;
	uint32_t nr_tdiff_roi_0_clear;
	uint32_t nr_tdiff_roi_0_sx;
	uint32_t nr_tdiff_roi_0_sy;
	uint32_t nr_tdiff_roi_0_ex;
	uint32_t nr_tdiff_roi_0_ey;
	uint32_t nr_tdiff_roi_0_acc_th_y;
	uint32_t nr_tdiff_roi_0_acc_th_c;
	//* read only *//
	//uint32_t nr_tdiff_roi_0_avg_y;
	//uint32_t nr_tdiff_roi_0_avg_c;

	uint32_t nr_tdiff_roi_1_en;
	uint32_t nr_tdiff_roi_1_clear;
	uint32_t nr_tdiff_roi_1_sx;
	uint32_t nr_tdiff_roi_1_sy;
	uint32_t nr_tdiff_roi_1_ex;
	uint32_t nr_tdiff_roi_1_ey;
	uint32_t nr_tdiff_roi_1_acc_th_y;
	uint32_t nr_tdiff_roi_1_acc_th_c;
	//* read only *//
	//uint32_t nr_tdiff_roi_1_avg_y;
	//uint32_t nr_tdiff_roi_1_avg_c;

	uint32_t nr_tdiff_roi_2_en;
	uint32_t nr_tdiff_roi_2_clear;
	uint32_t nr_tdiff_roi_2_sx;
	uint32_t nr_tdiff_roi_2_sy;
	uint32_t nr_tdiff_roi_2_ex;
	uint32_t nr_tdiff_roi_2_ey;
	uint32_t nr_tdiff_roi_2_acc_th_y;
	uint32_t nr_tdiff_roi_2_acc_th_c;
	//* read only *//
	//uint32_t nr_tdiff_roi_2_avg_y;
	//uint32_t nr_tdiff_roi_2_avg_c;

	uint32_t nr_tdiff_roi_3_en;
	uint32_t nr_tdiff_roi_3_clear;
	uint32_t nr_tdiff_roi_3_sx;
	uint32_t nr_tdiff_roi_3_sy;
	uint32_t nr_tdiff_roi_3_ex;
	uint32_t nr_tdiff_roi_3_ey;
	uint32_t nr_tdiff_roi_3_acc_th_y;
	uint32_t nr_tdiff_roi_3_acc_th_c;
	//* read only *//
	//uint32_t nr_tdiff_roi_3_avg_y;
	//uint32_t nr_tdiff_roi_3_avg_c;

} Vp_Nr_Tdiff;
/*
typedef struct vp_nr_demo {
	uint32_t nr_demo_en;
	uint32_t nr_demo_mode;
	uint32_t nr_demo_sx;
	uint32_t nr_demo_sy;
	uint32_t nr_demo_ex;
	uint32_t nr_demo_ey;

} Vp_Nr_Demo;
*/

typedef struct vp_nr_ctx {
	Vp_Nr_Setting nr_setting;
	Vp_Nr_Abs_Diff nr_abs_diff;
	Vp_Nr_Tdiff nr_tdiff;
	//Vp_Nr_Demo nr_demo;
	uint32_t nr_ink_en;
	uint32_t nr_ink_mode;

} VpNrCtx;

typedef struct vp_nr_attr {
	uint32_t nr_width;
	uint32_t nr_height;
	uint32_t nr_mode;
	uint32_t nr_abs_diff_hist_en;
	uint32_t nr_tdiff_roi_0_en;
	uint32_t nr_tdiff_roi_1_en;
	uint32_t nr_tdiff_roi_2_en;
	uint32_t nr_tdiff_roi_3_en;
	uint32_t nr_demo_en;
	uint32_t nr_demo_mode;
	uint32_t nr_ink_en;
	uint32_t nr_ink_mode;

} VpNrAttr;

void hw_vp_nr_bypass(void);
void hw_vp_nr_run(VpNrAttr *nrattr);
void hw_vp_nr_set_nr(VpNrCtx *vpnrctx, VpNrAttr *nrattr);
void hw_vp_nr_set_abs_diff(VpNrCtx *vpnrctx, VpNrAttr *nrattr);
void hw_vp_nr_set_tdiff(VpNrCtx *vpnrctx, VpNrAttr *nrattr);
//void hw_vp_nr_set_demo(VpNrCtx *vpnrctx, VpNrAttr *nrattr);
void hw_vp_nr_set_reg(VpNrCtx *vpnrctx);

#define NR_MA_Y_LUT_CTRL_POINT_NUM (9)
#define NR_MC_Y_LUT_CTRL_POINT_NUM (9)
#define NR_MA_C_LUT_CTRL_POINT_NUM (9)
#define NR_ABS_DIFF_ENTRY_NUM (19)
#define NR_MAX_TDIFF_ROI_NUM (4)

typedef enum vp_nr_mode {
	VP_NR_MODE_NORMAL = 0,
	VP_NR_MODE_2D_ONLY = 1,
	VP_NR_MODE_3D_ONLY = 2,
	VP_NR_MODE_MONO = 3,
	VP_NR_MODE_DISABLE = 4,
	VP_NR_MODE_BYPASS = 5,
	VP_NR_MODE_NUM = 6,
} VpNrMode;

typedef enum vp_nr_fir_kernel_sel_y {
	VP_NR_FIR_KERNEL_SEL_Y_3X3 = 0,
	VP_NR_FIR_KERNEL_SEL_Y_5X5 = 1,
	VP_NR_FIR_KERNEL_SEL_Y_NUM = 2,
} VpNrFirKernelSelY;

typedef enum vp_nr_iir_y_conf_sel {
	VP_NR_IIR_Y_CONF_SEL_MAX_HP = 0,
	VP_NR_IIR_Y_CONF_SEL_MAX_LP_DYN = 1,
	VP_NR_IIR_Y_CONF_SEL_MAX_LP = 2,
	VP_NR_IIR_Y_CONF_SEL_NUM = 3,
} VpNrIirYConfSel;

typedef enum vp_nr_weight_y_ref_sel {
	VP_NR_WEIGHT_Y_REF_SEL_HP = 0,
	VP_NR_WEIGHT_Y_REF_SEL_LP_DYN = 1,
	VP_NR_WEIGHT_Y_REF_SEL_LP = 2,
	VP_NR_WEIGHT_Y_REF_SEL_NUM = 3,
} VpNrWeightYRefSel;

typedef struct vp_nr_cfg {
	enum vp_nr_mode mode;
	struct nr_3d_ma_y {
		uint8_t step;
		uint8_t lut_i[NR_MA_Y_LUT_CTRL_POINT_NUM];
		uint8_t lut_o[NR_MA_Y_LUT_CTRL_POINT_NUM];
		uint16_t lut_slope[NR_MA_Y_LUT_CTRL_POINT_NUM - 1];
		uint8_t lp_dyn_gain_th_min;
		uint8_t lp_dyn_gain_th_max;
		uint8_t lp_dyn_gain_min;
		uint8_t lp_dyn_gain_max;
		uint16_t lp_dyn_gain_slope;
		uint8_t lp_diff_gain_th_min;
		uint8_t lp_diff_gain_th_range;
		uint8_t lp_diff_gain_min;
		uint8_t lp_diff_gain_max;
	} ma_y;
	struct nr_3d_mc_y {
		uint8_t step;
		uint8_t lut_i[NR_MA_Y_LUT_CTRL_POINT_NUM];
		uint8_t lut_o[NR_MA_Y_LUT_CTRL_POINT_NUM];
		uint16_t lut_slope[NR_MA_Y_LUT_CTRL_POINT_NUM - 1];
		uint8_t lp_dyn_gain_th_min;
		uint8_t lp_dyn_gain_th_max;
		uint8_t lp_dyn_gain_min;
		uint8_t lp_dyn_gain_max;
		uint16_t lp_dyn_gain_slope;
		uint8_t lp_diff_gain_th_min;
		uint8_t lp_diff_gain_th_range;
		uint8_t lp_diff_gain_min;
		uint8_t lp_diff_gain_max;
		uint8_t base_ratio;
		uint8_t base_dyn;
	} mc_y;
	struct nr_3d_ma_c {
		uint8_t step;
		uint8_t lut_i[NR_MA_Y_LUT_CTRL_POINT_NUM];
		uint8_t lut_o[NR_MA_Y_LUT_CTRL_POINT_NUM];
		uint16_t lut_slope[NR_MA_Y_LUT_CTRL_POINT_NUM - 1];
		enum vp_nr_weight_y_ref_sel ref;
		uint8_t weight_y_gain_th_min;
		uint8_t weight_y_gain_th_max;
		uint8_t weight_y_gain_min;
		uint8_t weight_y_gain_max;
		uint16_t weight_y_gain_slope;
	} ma_c;
	struct nr_2d_fir {
		enum vp_nr_fir_kernel_sel_y kernel;
		uint8_t weight_y;
		uint8_t weight_c;
	} fir;
	struct nr_blend_2d_and_3d {
		enum vp_nr_iir_y_conf_sel confidence;
		uint8_t alpha_y_gain;
		uint8_t alpha_c_gain;
	} iir;
} VpNrCfg;

typedef enum vp_nr_abs_diff_hist_sel {
	VP_NR_ABS_DIFF_HIST_SEL_BLEND_Y_VS_MC_Y = 0,
	VP_NR_ABS_DIFF_HIST_SEL_INPUT_Y_VS_MC_Y = 1,
	VP_NR_ABS_DIFF_HIST_SEL_INPUT_Y_VS_MA_Y = 2,
	VP_NR_ABS_DIFF_HIST_SEL_INPUT_Y_VS_MA_C = 3,
	VP_NR_ABS_DIFF_HIST_SEL_NUM = 4,
} VpNrAbsDiffHistSel;

typedef enum vp_nr_abs_diff_hist_mode {
	VP_NR_ABS_DIFF_HIST_MODE_OVERFLOW_STOP = 0,
	VP_NR_ABS_DIFF_HIST_MODE_OVERFLOW_SAT = 1,
	VP_NR_ABS_DIFF_HIST_MODE_NUM = 2,
} VpNrAbsDiffHistMode;

typedef struct vp_nr_abs_diff_roi_stat_frame_ctrl {
	enum vp_nr_abs_diff_hist_sel sel;
	enum vp_nr_abs_diff_hist_mode mode;
	uint8_t step;
	uint16_t sy;
	uint16_t ey;
} VpNrAbsDiffRoiStatFrameCtrl;

typedef struct vp_nr_abs_diff_roi_stat_tile_ctrl {
	uint16_t sx;
	uint16_t ex;
} VpNrAbsDiffRoiStatTileCtrl;

typedef struct vp_nr_abs_diff_roi_stat {
	uint8_t overflow;
	uint32_t hist[NR_ABS_DIFF_ENTRY_NUM];
} VpNrAbsDiffRoiStat;

typedef enum vp_nr_tdiff_mode {
	VP_NR_TDIFF_MODE_NORMAL = 0,
	VP_NR_TDIFF_MODE_DISABLE = 1,
	VP_NR_TDIFF_MODE_MUM = 2,
} VpNrTdiffMode;

typedef struct vp_nr_tdiff_roi_stat_frame_ctrl {
	enum vp_nr_tdiff_mode mode;
	uint16_t sy;
	uint16_t ey;
	uint32_t acc_th_y;
	uint32_t acc_th_c;
} VpNrTdiffRoiStatFrameCtrl;

typedef struct vp_nr_tdiff_roi_stat_tile_ctrl {
	uint16_t sx;
	uint16_t ex;
} VpNrTdiffRoiStatTileCtrl;

typedef struct vp_nr_tdiff_roi_stat {
	uint16_t avg_y;
	uint16_t avg_c;
} VpNrTdiffRoiStat;

typedef enum vp_nr_demo_mode {
	VP_NR_DEMO_MODE_NORMAL = 0,
	VP_NR_DEMO_MODE_2D_ONLY = 1,
	VP_NR_DEMO_MODE_3D_ONLY = 2,
	VP_NR_DEMO_MODE_MONO = 3,
	VP_NR_DEMO_MODE_DISABLE = 4,
	VP_NR_DEMO_MODE_BYPASS = 5,
	VP_NR_DEMO_MODE_NUM = 6,
} VpNrDemoMode;

typedef struct vp_nr_demo {
	uint8_t enable;
	enum vp_nr_demo_mode mode;
	uint16_t sx;
	uint16_t sy;
	uint16_t ex;
	uint16_t ey;
} VpNrDemo;

typedef struct vp_nr_ink {
	uint8_t enable;
	uint8_t mode;
} VpNrInk;

/* NR */
void hw_vp_nr_set_cfg(const struct vp_nr_cfg *cfg);
void hw_vp_nr_get_cfg(struct vp_nr_cfg *cfg);

/* Statistics */
// Abs Diff
void hw_vp_nr_set_abs_diff_roi_stat_frame_ctrl(const struct vp_nr_abs_diff_roi_stat_frame_ctrl *cfg);
void hw_vp_nr_get_abs_diff_roi_stat_frame_ctrl(struct vp_nr_abs_diff_roi_stat_frame_ctrl *cfg);
void hw_vp_nr_set_abs_diff_roi_stat_tile_ctrl(const struct vp_nr_abs_diff_roi_stat_tile_ctrl *cfg);
void hw_vp_nr_get_abs_diff_roi_stat_tile_ctrl(struct vp_nr_abs_diff_roi_stat_tile_ctrl *cfg);
void hw_vp_nr_set_abs_diff_roi_stat_clear(uint8_t clear);
uint8_t hw_vp_nr_get_abs_diff_roi_stat_clear(void);
void hw_vp_nr_set_abs_diff_roi_stat_enable(uint8_t enable);
uint8_t hw_vp_nr_get_abs_diff_roi_stat_enable(void);
void hw_vp_nr_get_abs_diff_roi_stat(struct vp_nr_abs_diff_roi_stat *stat);
// Tdiff
void hw_vp_nr_set_tdiff_roi_stat_frame_ctrl(uint8_t idx, const struct vp_nr_tdiff_roi_stat_frame_ctrl *cfg);
void hw_vp_nr_get_tdiff_roi_stat_frame_ctrl(uint8_t idx, struct vp_nr_tdiff_roi_stat_frame_ctrl *cfg);
void hw_vp_nr_set_tdiff_roi_stat_tile_ctrl(uint8_t idx, const struct vp_nr_tdiff_roi_stat_tile_ctrl *cfg);
void hw_vp_nr_get_tdiff_roi_stat_tile_ctrl(uint8_t idx, struct vp_nr_tdiff_roi_stat_tile_ctrl *cfg);
void hw_vp_nr_set_tdiff_roi_stat_clear(uint8_t idx, uint8_t clear);
uint8_t hw_vp_nr_get_tdiff_roi_stat_clear(uint8_t idx);
void hw_vp_nr_set_tdiff_roi_stat_enable(uint8_t idx, uint8_t enable);
uint8_t hw_vp_nr_get_tdiff_roi_stat_enable(uint8_t idx);
void hw_vp_nr_get_tdiff_roi_stat(uint8_t idx, struct vp_nr_tdiff_roi_stat *stat);

/* Demo */
void hw_vp_nr_set_demo(const struct vp_nr_demo *demo);
void hw_vp_nr_get_demo(struct vp_nr_demo *demo);

/* Ink */
void hw_vp_nr_set_ink(const struct vp_nr_ink *demo);
void hw_vp_nr_get_ink(struct vp_nr_ink *demo);

/* Reserved */
void hw_vp_nr_set_reserved_0(uint32_t reserved_0);
uint32_t hw_vp_nr_get_reserved_0(void);
void hw_vp_nr_set_reserved_1(uint32_t reserved_1);
uint32_t hw_vp_nr_get_reserved_1(void);

//
/* NR */
void vp_nr_set_cfg(volatile CsrBankNr *csr, const struct vp_nr_cfg *cfg);
void vp_nr_get_cfg(volatile CsrBankNr *csr, struct vp_nr_cfg *cfg);

/* Statistics */
// Abs Diff
void vp_nr_set_abs_diff_roi_stat_frame_ctrl(volatile CsrBankNr *csr,
                                            const struct vp_nr_abs_diff_roi_stat_frame_ctrl *cfg);
void vp_nr_get_abs_diff_roi_stat_frame_ctrl(volatile CsrBankNr *csr, struct vp_nr_abs_diff_roi_stat_frame_ctrl *cfg);
void vp_nr_set_abs_diff_roi_stat_tile_ctrl(volatile CsrBankNr *csr,
                                           const struct vp_nr_abs_diff_roi_stat_tile_ctrl *cfg);
void vp_nr_get_abs_diff_roi_stat_tile_ctrl(volatile CsrBankNr *csr, struct vp_nr_abs_diff_roi_stat_tile_ctrl *cfg);
void vp_nr_set_abs_diff_roi_stat_clear(volatile CsrBankNr *csr, uint8_t clear);
uint8_t vp_nr_get_abs_diff_roi_stat_clear(volatile CsrBankNr *csr);
void vp_nr_set_abs_diff_roi_stat_enable(volatile CsrBankNr *csr, uint8_t enable);
uint8_t vp_nr_get_abs_diff_roi_stat_enable(volatile CsrBankNr *csr);
void vp_nr_get_abs_diff_roi_stat(volatile CsrBankNr *csr, struct vp_nr_abs_diff_roi_stat *stat);
// Tdiff
void vp_nr_set_tdiff_roi_stat_frame_ctrl(volatile CsrBankNr *csr, uint8_t idx,
                                         const struct vp_nr_tdiff_roi_stat_frame_ctrl *cfg);
void vp_nr_get_tdiff_roi_stat_frame_ctrl(volatile CsrBankNr *csr, uint8_t idx,
                                         struct vp_nr_tdiff_roi_stat_frame_ctrl *cfg);
void vp_nr_set_tdiff_roi_stat_tile_ctrl(volatile CsrBankNr *csr, uint8_t idx,
                                        const struct vp_nr_tdiff_roi_stat_tile_ctrl *cfg);
void vp_nr_get_tdiff_roi_stat_tile_ctrl(volatile CsrBankNr *csr, uint8_t idx,
                                        struct vp_nr_tdiff_roi_stat_tile_ctrl *cfg);
void vp_nr_set_tdiff_roi_stat_clear(volatile CsrBankNr *csr, uint8_t idx, uint8_t clear);
uint8_t vp_nr_get_tdiff_roi_stat_clear(volatile CsrBankNr *csr, uint8_t idx);
void vp_nr_set_tdiff_roi_stat_enable(volatile CsrBankNr *csr, uint8_t idx, uint8_t enable);
uint8_t vp_nr_get_tdiff_roi_stat_enable(volatile CsrBankNr *csr, uint8_t idx);
void vp_nr_get_tdiff_roi_stat(volatile CsrBankNr *csr, uint8_t idx, struct vp_nr_tdiff_roi_stat *stat);

/* Demo */
void vp_nr_set_demo(volatile CsrBankNr *csr, const struct vp_nr_demo *demo);
void vp_nr_get_demo(volatile CsrBankNr *csr, struct vp_nr_demo *demo);

/* Ink */
void vp_nr_set_ink(volatile CsrBankNr *csr, const struct vp_nr_ink *demo);
void vp_nr_get_ink(volatile CsrBankNr *csr, struct vp_nr_ink *demo);

/* Reserved */
void vp_nr_set_reserved_0(volatile CsrBankNr *csr, uint32_t reserved_0);
uint32_t vp_nr_get_reserved_0(volatile CsrBankNr *csr);
void vp_nr_set_reserved_1(volatile CsrBankNr *csr, uint32_t reserved_1);
uint32_t vp_nr_get_reserved_1(volatile CsrBankNr *csr);

#endif