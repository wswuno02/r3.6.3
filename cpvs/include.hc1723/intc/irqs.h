/**
 * @file irqs.h
 * @brief define IRQ numbers
 */

#ifndef IRQS_H
#define IRQS_H

#define IRQ_ID_CA7_TIMER 29

#endif /* IRQS_H */
