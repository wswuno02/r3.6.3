#ifndef KYOTO_AGMA_H_
#define KYOTO_AGMA_H_

#include "is_utils.h"
#include "csr_bank_gma.h"

typedef enum is_agma_mode {
	IS_AGMA_MODE_NORMAL = 0,
	IS_AGMA_MODE_DISABLE = 1,
	IS_AGMA_MODE_NUM = 2,
} IsAgmaMode;

typedef struct is_agma_cfg {
	enum is_agma_mode mode;
	struct agma_curve {
		uint16_t val[AGMA_CURVE_ENTRY_NUM];
	} curve; /**< Anti-Gamma curve */
} IsAgmaCfg;

/* AGMA */
void hw_is_agma_set_cfg(const enum is_kel_no no, const struct is_agma_cfg *cfg);
void hw_is_agma_get_cfg(const enum is_kel_no no, struct is_agma_cfg *cfg);

/* Debug */
void hw_is_agma_set_dbg_mon_sel(const enum is_kel_no no, uint8_t debug_mon_sel);
uint8_t hw_is_agma_get_dbg_mon_sel(const enum is_kel_no no);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//* for mpp is *//

/* AGMA */
void is_agma_set_cfg(volatile CsrBankGma *csr, const struct is_agma_cfg *cfg);
void is_agma_get_cfg(volatile CsrBankGma *csr, struct is_agma_cfg *cfg);

/* Debug */
void is_agma_set_dbg_mon_sel(volatile CsrBankGma *csr, uint8_t debug_mon_sel);
uint8_t is_agma_get_dbg_mon_sel(volatile CsrBankGma *csr);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//* need to remove *//
// Move to test

#define IS_AGMA_CURVE_NUM 4
#define IS_AGMA_CURVE_ENTRY_NUM 58

typedef struct is_agma_curve {
	uint32_t agma_curve_table[IS_AGMA_CURVE_ENTRY_NUM];

} IsAgmaCurve;

typedef struct is_agma_ctx {
	uint32_t agma_mode;
	IsAgmaCurve agma_curve;

} IsAgmaCtx;

typedef struct is_agma_attr {
	uint32_t agma_mode;
	uint32_t agma_curve_idx;

} IsAgmaAttr;

void hw_is_agma_bypass(void);
void hw_is_agma_run(IsAgmaAttr *agmaattr);
void hw_is_agma_set_ctx(IsAgmaCtx *agmactx, IsAgmaAttr *agmaattr);
void hw_is_agma_set_reg(IsAgmaCtx *agmactx);
void hw_is_agama_debug_mon_sel(uint32_t debug_mon_sel);

#endif /* KYOTO_AGMA_H_ */