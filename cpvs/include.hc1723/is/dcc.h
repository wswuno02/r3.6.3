#ifndef KYOTO_DCC_H_
#define KYOTO_DCC_H_

#include "is_utils.h"
#include "csr_bank_dcc.h"

typedef enum is_dcc_mode {
	IS_DCC_MODE_NORMAL = 0,
	IS_DCC_MODE_DISABLE = 1,
	IS_DCC_MODE_NUM = 2,
} IsDccMode;

typedef struct is_dcc_cfg {
	enum is_dcc_mode mode;
	uint32_t gain[CFA_PHASE_NUM];
	uint32_t offset_2s[CFA_PHASE_NUM];
} IsDccCfg;

typedef struct is_dcc_gain_curve_cfg {
	uint16_t x[DCC_GAIN_CURVE_CTRL_POINT_NUM]; /**< Curve bin */
	uint8_t m[DCC_GAIN_CURVE_CTRL_POINT_NUM - 1]; /**< Curve slpoe */
	uint16_t y[DCC_GAIN_CURVE_CTRL_POINT_NUM]; /**< Curve value */
} IsDccGainCurveCfg;

/* Frame start */
void hw_is_dcc_set_frame_start(const enum is_kel_no no);

/* IRQ clear */
void hw_is_dcc_set_irq_clear_frame_end(const enum is_kel_no no);

/* Frame end*/
uint8_t hw_is_dcc_get_status_frame_end(const enum is_kel_no no);

/* IRQ mask */
void hw_is_dcc_set_irq_mask_frame_end(const enum is_kel_no no, uint8_t irq_mask_frame_end);
uint8_t hw_is_dcc_get_irq_mask_frame_end(const enum is_kel_no no);

/* Resolution */
void hw_is_dcc_set_res(const enum is_kel_no no, const struct res *res);
void hw_is_dcc_get_res(const enum is_kel_no no, struct res *res);

/* Input format */
void hw_is_dcc_set_input_format(const enum is_kel_no no, const struct is_input_format *cfg);
void hw_is_dcc_get_input_format(const enum is_kel_no no, struct is_input_format *cfg);

/* DCC */
void hw_is_dcc_set_cfg(const enum is_kel_no no, const struct is_dcc_cfg *cfg);
void hw_is_dcc_get_cfg(const enum is_kel_no no, struct is_dcc_cfg *cfg);
void hw_is_dcc_set_gain_curve_cfg(const enum is_kel_no no, const enum cfa_phase phase, const struct is_dcc_gain_curve_cfg *cfg);
void hw_is_dcc_get_gain_curve_cfg(const enum is_kel_no no, const enum cfa_phase phase, struct is_dcc_gain_curve_cfg *cfg);

/* Debug */
void hw_is_dcc_set_dbg_mon_sel(const enum is_kel_no no, uint8_t debug_mon_sel);
uint8_t hw_is_dcc_get_dbg_mon_sel(const enum is_kel_no no);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//* for mpp is *//

/* IRQ mask */
void is_dcc_set_irq_mask_frame_end(volatile CsrBankDcc *csr, uint8_t irq_mask_frame_end);
uint8_t is_dcc_get_irq_mask_frame_end(volatile CsrBankDcc *csr);

/* Resolution */
void is_dcc_set_res(volatile CsrBankDcc *csr, const struct res *res);
void is_dcc_get_res(volatile CsrBankDcc *csr, struct res *res);

/* Input format */
void is_dcc_set_input_format(volatile CsrBankDcc *csr, const struct is_input_format *cfg);
void is_dcc_get_input_format(volatile CsrBankDcc *csr, struct is_input_format *cfg);

/* DCC */
void is_dcc_set_cfg(volatile CsrBankDcc *csr, const struct is_dcc_cfg *cfg);
void is_dcc_get_cfg(volatile CsrBankDcc *csr, struct is_dcc_cfg *cfg);
void is_dcc_set_gain_curve_cfg(volatile CsrBankDcc *csr, const enum cfa_phase phase, const struct is_dcc_gain_curve_cfg *cfg);
void is_dcc_get_gain_curve_cfg(volatile CsrBankDcc *csr, const enum cfa_phase phase, struct is_dcc_gain_curve_cfg *cfg);

/* Debug */
void is_dcc_set_dbg_mon_sel(volatile CsrBankDcc *csr, uint8_t debug_mon_sel);
uint8_t is_dcc_get_dbg_mon_sel(volatile CsrBankDcc *csr);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//* need to remove *//
// Move to test

#define IS_DCC_CFA_PHASE_IDX 3
#define IS_DCC_CFA_PHASE_NUM 16
#define IS_DCC_GAIN_NUM 5
#define IS_DCC_offset_NUM 5
#define IS_DCC_MAPPING_CURVE_INI_NUM 5

#define IS_DCC_MAPPING_CURVE_idx 3
#define IS_DCC_MAPPING_CURVE_NUM 8

typedef struct is_dcc_resolution {
	uint32_t width;
	uint32_t height;
} IsDccResolution;

typedef struct is_dcc_cfa_set {
	uint32_t cfa_mode;
	uint32_t bayer_ini_phase;
	uint32_t cfa_phase[IS_DCC_CFA_PHASE_NUM];
} IsDccCfaSet;

typedef struct is_dcc_ctx {
	uint32_t dcc_mode;
	IsDccResolution dcc_res;
	IsDccCfaSet dcc_cfa_set;
	uint32_t dcc_gain[IS_DCC_GAIN_NUM];
	uint32_t dcc_offset[IS_DCC_offset_NUM];
	uint32_t dcc_maping_curve_ini[IS_DCC_MAPPING_CURVE_INI_NUM];
	uint32_t dcc_mapping_curve_g0_idx;
	uint32_t dcc_mapping_curve_r_idx;
	uint32_t dcc_mapping_curve_b_idx;
	uint32_t dcc_mapping_curve_g1_idx;
	uint32_t dcc_mapping_curve_s_idx;
} IsDccCtx;

typedef struct is_dcc_attr {
	uint32_t dcc_mode;
	uint32_t dcc_width;
	uint32_t dcc_height;
	uint32_t dcc_cfa_mode_idx;
	uint32_t dcc_bayer_ini_phase;
	uint32_t dcc_mapping_curve_g0_idx;
	uint32_t dcc_mapping_curve_r_idx;
	uint32_t dcc_mapping_curve_b_idx;
	uint32_t dcc_mapping_curve_g1_idx;
	uint32_t dcc_mapping_curve_s_idx;
} IsDccAttr;

void hw_is_dcc_frame_start(void);
void hw_is_dcc_irq_clear_frame_end(void);
uint32_t hw_is_dcc_status_frame_end(void);
void hw_is_dcc_irq_mask_frame_end(uint32_t irq_mask_frame_end);
void hw_is_dcc_resolution(uint32_t width, uint32_t height);
void hw_is_dcc_bypass(uint32_t cfa_mode, uint32_t bayer_ini_phase);
void hw_is_dcc_run(IsDccAttr *dccattr);
void hw_is_dcc_set_ctx(IsDccCtx *dccctx, IsDccAttr *dccattr);
void hw_is_dcc_set_reg(IsDccCtx *dccctx);
void hw_is_dcc_debug_mon_sel(uint32_t debug_mon_sel);

#endif /* KYOTO_DCC_H_ */