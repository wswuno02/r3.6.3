#ifndef IO_H_
#define IO_H_

#include <stdint.h>

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

/* base address */
#define PIN_BASE_ADDR 0x80001000
#define GPIO_BASE_ADDR 0x80001800
/* GPIO group offset */
#define PIOC0_PIN_OFFSET 0
#define PIOC1_PIN_OFFSET 32
#define PIOC2_PIN_OFFSET 64
#define AIOC_PIN_OFFSET 68
#define MIPI_TX_PIN_OFFSET 81
#define MIPI_RX0_PIN_OFFSET 91
#define MIPI_RX1_PIN_OFFSET 101
#define AIOC_O_OFFSET 0x0
#define AIOC_OE_OFFSET 0x4
#define AIOC_I_OFFSET 0x8
#define PIOC0_O_OFFSET 0xC
#define PIOC0_OE_OFFSET 0x10
#define PIOC0_I_OFFSET 0x14
#define PIOC1_O_OFFSET 0x18
#define PIOC1_OE_OFFSET 0x1C
#define PIOC1_I_OFFSET 0x20
#define PIOC2_O_OFFSET 0x24
#define PIOC2_OE_OFFSET 0x28
#define PIOC2_I_OFFSET 0x30
#define MIPI_TX_O_OFFSET 0x34
#define MIPI_RX0_I_OFFSET 0x38
#define MIPI_RX1_I_OFFSET 0x3C

// clang-format off
enum pin_type {
    AIOC,
    PIOC
};

struct pinctrl_addr {
	const char *name;
	enum pin_type type;
	int pin;
	uint32_t mux_bias;
	uint32_t conf_bias;
	int ds_bit;
};

static struct pinctrl_addr kyoto_ioc_addr[] = {
	{ "PAD_SPI0_SCK", PIOC, 0, 0x514, 0x400, 3 },
	{ "PAD_SPI0_SDI", PIOC, 1, 0x518, 0x404, 3 },
	{ "PAD_SPI0_SDO", PIOC, 2, 0x51C, 0x408, 3 },
	{ "PAD_EIRQ0", PIOC, 3, 0x520, 0x40C, 3 },
	{ "PAD_SD_CD", PIOC, 4, 0x524, 0x410, 2 },
	{ "PAD_SD_D2", PIOC, 5, 0x528, 0x414, 3 },
	{ "PAD_SD_D3", PIOC, 6, 0x52C, 0x418, 3 },
	{ "PAD_SD_CMD", PIOC, 7, 0x530, 0x41C, 3 },
	{ "PAD_SD_CK", PIOC, 8, 0x534, 0x420, 3 },
	{ "PAD_SD_D0", PIOC, 9, 0x538, 0x424, 3 },
	{ "PAD_SD_D1", PIOC, 10, 0x53C, 0x428, 3 },
	{ "PAD_GPIO3", PIOC, 11, 0x540, 0x430, 3 },
	{ "PAD_PWM1", PIOC, 12, 0x544, 0x434, 3 },
	{ "PAD_PWM4", PIOC, 13, 0x548, 0x438, 3 },
	{ "PAD_I2C1_SCL", PIOC, 14, 0x54C, 0x43C, 3 },
	{ "PAD_I2C1_SDA", PIOC, 15, 0x550, 0x440, 3 },
	{ "PAD_UART1_TXD", PIOC, 16, 0x554, 0x444, 3 },
	{ "PAD_UART1_RXD", PIOC, 17, 0x558, 0x448, 3 },
	{ "PAD_UART2_TXD", PIOC, 18, 0x55C, 0x44C, 3 },
	{ "PAD_UART2_RXD", PIOC, 19, 0x560, 0x450, 3 },
	{ "PAD_QSPI_CE_N", PIOC, 20, 0x598, 0x454, 3 },
	{ "PAD_QSPI_D1", PIOC, 21, 0x59C, 0x458, 3 },
	{ "PAD_QSPI_D2", PIOC, 22, 0x5A0, 0x45C, 3 },
	{ "PAD_QSPI_D3", PIOC, 23, 0x5A4, 0x460, 3 },
	{ "PAD_QSPI_CK", PIOC, 24, 0x5A8, 0x464, 3 },
	{ "PAD_QSPI_D0", PIOC, 25, 0x5AC, 0x468, 3 },
	{ "PAD_EPHY_RST", PIOC, 26, 0x5B0, 0x46C, 3 },
	{ "PAD_EMAC_TX_CK", PIOC, 27, 0x5B4, 0x470, 3 },
	{ "PAD_EMAC_TX_CTL", PIOC, 28, 0x5B8, 0x474, 3 },
	{ "PAD_EMAC_TX_D3", PIOC, 29, 0x5BC, 0x478, 3 },
	{ "PAD_EMAC_TX_D2", PIOC, 30, 0x5C0, 0x47C, 3 },
	{ "PAD_EMAC_TX_D1", PIOC, 31, 0x5C4, 0x480, 3 },
	{ "PAD_EMAC_TX_D0", PIOC, 32, 0x5C8, 0x484, 3 },
	{ "PAD_EMAC_RX_CK", PIOC, 33, 0x5CC, 0x488, 3 },
	{ "PAD_EMAC_RX_D0", PIOC, 34, 0x5D0, 0x48C, 3 },
	{ "PAD_EMAC_RX_D1", PIOC, 35, 0x5D4, 0x490, 3 },
	{ "PAD_EMAC_RX_D2", PIOC, 36, 0x5D8, 0x494, 3 },
	{ "PAD_EMAC_RX_D3", PIOC, 37, 0x5DC, 0x498, 3 },
	{ "PAD_EMAC_RX_CTL", PIOC, 38, 0x5E0, 0x49C, 3 },
	{ "PAD_EMAC_MDC", PIOC, 39, 0x5E4, 0x4A0, 3 },
	{ "PAD_EMAC_MDIO", PIOC, 40, 0x5E8, 0x4A4, 3 },
	{ "PAD_SPI1_SDI", PIOC, 41, 0x5EC, 0x4A8, 3 },
	{ "PAD_SPI1_SDO", PIOC, 42, 0x5F0, 0x4AC, 3 },
	{ "PAD_SPI1_SCK", PIOC, 43, 0x5F4, 0x4B0, 3 },
	{ "PAD_GPIO7", PIOC, 44, 0x5F8, 0x4B4, 3 },
	{ "PAD_I2C0_SDA", PIOC, 45, 0x5FC, 0x4B8, 2 },
	{ "PAD_I2C0_SCL", PIOC, 46, 0x600, 0x4BC, 2 },
	{ "PAD_SENSOR_CLK", PIOC, 47, 0x604, 0x4C0, 2 },
	{ "PAD_PWM2", PIOC, 48, 0x608, 0x4C4, 2 },
	{ "PAD_SENSOR_RSTB", PIOC, 49, 0x60C, 0x4C8, 2 },
	{ "PAD_SENSOR_PWDN", PIOC, 50, 0x610, 0x4CC, 2 },
	{ "PAD_GPIO4", PIOC, 51, 0x614, 0x4D0, 3 },
	{ "PAD_GPIO5", PIOC, 52, 0x618, 0x4D4, 3 },
	{ "PAD_GPIO6", PIOC, 53, 0x61C, 0x4D8, 3 },
	{ "PAD_I2S_TX_CK", PIOC, 54, 0x620, 0x4DC, 3 },
	{ "PAD_I2S_RX_CK", PIOC, 55, 0x624, 0x4E0, 3 },
	{ "PAD_I2S_RX_SD", PIOC, 56, 0x628, 0x4E4, 3 },
	{ "PAD_I2S_TX_SD", PIOC, 57, 0x62C, 0x4E8, 3 },
	{ "PAD_I2S_RX_WS", PIOC, 58, 0x630, 0x4EC, 3 },
	{ "PAD_I2S_TX_WS", PIOC, 59, 0x634, 0x4F0, 3 },
	{ "PAD_GPIO2", PIOC, 60, 0x638, 0x4F4, 3 },
	{ "PAD_UART0_TXD", PIOC, 61, 0x63C, 0x4F8, 3 },
	{ "PAD_UART0_RXD", PIOC, 62, 0x640, 0x4FC, 3 },
	{ "PAD_EIRQ1", PIOC, 63, 0x644, 0x500, 3 },
	{ "PAD_PWM3", PIOC, 64, 0x648, 0x504, 3 },
	{ "PAD_PWM0", PIOC, 65, 0x64C, 0x508, 3 },
	{ "PAD_GPIO0", PIOC, 66, 0x650, 0x50C, 3 },
	{ "PAD_GPIO1", PIOC, 67, 0x654, 0x510, 3 },
	{ "PAD_DVP_0", AIOC, 68, 0x44, 0x10, 2 },
	{ "PAD_DVP_1", AIOC, 69, 0x48, 0x14, 2 },
	{ "PAD_DVP_2", AIOC, 70, 0x4C, 0x18, 2 },
	{ "PAD_DVP_3", AIOC, 71, 0x50, 0x1C, 2 },
	{ "PAD_DVP_4", AIOC, 72, 0x54, 0x20, 2 },
	{ "PAD_DVP_5", AIOC, 73, 0x58, 0x24, 2 },
	{ "PAD_DVP_6", AIOC, 74, 0x5C, 0x28, 2 },
	{ "PAD_DVP_7", AIOC, 75, 0x60, 0x2C, 2 },
	{ "PAD_DVP_8", AIOC, 76, 0x64, 0x30, 2 },
	{ "PAD_POWER_CTRL_0", AIOC, 77, 0x3C, 0x8, 2 },
	{ "PAD_POWER_CTRL_1", AIOC, 78, 0x40, 0xC, 2 },
	{ "PAD_AO_BUTTON", AIOC, 79, 0x68, 0x34, 2 },
	{ "PAD_AO_WAKEUP", AIOC, 80, 0x6C, 0x38, 2 },
	{ "PAD_XIN_PCFG", AIOC, 81, -1, 0x0, 2 },
	{ "PAD_RESETB_PCFG", AIOC, 82, -1, 0x4, 2 },
	{ "PAD_SD_POC_PCFG", PIOC, 83, -1, 0x42C, 2 },
};

enum gpio_type {
	GPIO_AIOC,
	GPIO_PIOC0,
	GPIO_PIOC1,
	GPIO_PIOC2,
	GPIO_MIPI_TX,
	GPIO_MIPI_RX0,
	GPIO_MIPI_RX1,
};

struct gpio_desc {
	const char *name;
	enum gpio_type type;
	int pin;
};

static struct gpio_desc kyoto_gpio_addr[] = {
	{ "PAD_SPI0_SCK", GPIO_PIOC0, 0 },
	{ "PAD_SPI0_SDI", GPIO_PIOC0, 1 },
	{ "PAD_SPI0_SDO", GPIO_PIOC0, 2 },
	{ "PAD_EIRQ0", GPIO_PIOC0, 3 },
	{ "PAD_SD_CD", GPIO_PIOC0, 4 },
	{ "PAD_SD_D2", GPIO_PIOC0, 5 },
	{ "PAD_SD_D3", GPIO_PIOC0, 6 },
	{ "PAD_SD_CMD", GPIO_PIOC0, 7 },
	{ "PAD_SD_CK", GPIO_PIOC0, 8 },
	{ "PAD_SD_D0", GPIO_PIOC0, 9 },
	{ "PAD_SD_D1", GPIO_PIOC0, 10 },
	{ "PAD_GPIO3", GPIO_PIOC0, 11 },
	{ "PAD_PWM1", GPIO_PIOC0, 12 },
	{ "PAD_PWM4", GPIO_PIOC0, 13 },
	{ "PAD_I2C1_SCL", GPIO_PIOC0, 14 },
	{ "PAD_I2C1_SDA", GPIO_PIOC0, 15 },
	{ "PAD_UART1_TXD", GPIO_PIOC0, 16 },
	{ "PAD_UART1_RXD", GPIO_PIOC0, 17 },
	{ "PAD_UART2_TXD", GPIO_PIOC0, 18 },
	{ "PAD_UART2_RXD", GPIO_PIOC0, 19 },
	{ "PAD_QSPI_CE_N", GPIO_PIOC0, 20 },
	{ "PAD_QSPI_D1", GPIO_PIOC0, 21 },
	{ "PAD_QSPI_D2", GPIO_PIOC0, 22 },
	{ "PAD_QSPI_D3", GPIO_PIOC0, 23 },
	{ "PAD_QSPI_CK", GPIO_PIOC0, 24 },
	{ "PAD_QSPI_D0", GPIO_PIOC0, 25 },
	{ "PAD_EPHY_RST", GPIO_PIOC0, 26 },
	{ "PAD_EMAC_TX_CK", GPIO_PIOC0, 27 },
	{ "PAD_EMAC_TX_CTL", GPIO_PIOC0, 28 },
	{ "PAD_EMAC_TX_D3", GPIO_PIOC0, 29 },
	{ "PAD_EMAC_TX_D2", GPIO_PIOC0, 30 },
	{ "PAD_EMAC_TX_D1", GPIO_PIOC0, 31 },
	{ "PAD_EMAC_TX_D0", GPIO_PIOC1, 32 },
	{ "PAD_EMAC_RX_CK", GPIO_PIOC1, 33 },
	{ "PAD_EMAC_RX_D0", GPIO_PIOC1, 34 },
	{ "PAD_EMAC_RX_D1", GPIO_PIOC1, 35 },
	{ "PAD_EMAC_RX_D2", GPIO_PIOC1, 36 },
	{ "PAD_EMAC_RX_D3", GPIO_PIOC1, 37 },
	{ "PAD_EMAC_RX_CTL", GPIO_PIOC1, 38 },
	{ "PAD_EMAC_MDC", GPIO_PIOC1, 39 },
	{ "PAD_EMAC_MDIO", GPIO_PIOC1, 40 },
	{ "PAD_SPI1_SDI", GPIO_PIOC1, 41 },
	{ "PAD_SPI1_SDO", GPIO_PIOC1, 42 },
	{ "PAD_SPI1_SCK", GPIO_PIOC1, 43 },
	{ "PAD_GPIO7", GPIO_PIOC1, 44 },
	{ "PAD_I2C0_SDA", GPIO_PIOC1, 45 },
	{ "PAD_I2C0_SCL", GPIO_PIOC1, 46 },
	{ "PAD_SENSOR_CLK", GPIO_PIOC1, 47 },
	{ "PAD_PWM2", GPIO_PIOC1, 48 },
	{ "PAD_SENSOR_RSTB", GPIO_PIOC1, 49 },
	{ "PAD_SENSOR_PWDN", GPIO_PIOC1, 50 },
	{ "PAD_GPIO4", GPIO_PIOC1, 51 },
	{ "PAD_GPIO5", GPIO_PIOC1, 52 },
	{ "PAD_GPIO6", GPIO_PIOC1, 53 },
	{ "PAD_I2S_TX_CK", GPIO_PIOC1, 54 },
	{ "PAD_I2S_RX_CK", GPIO_PIOC1, 55 },
	{ "PAD_I2S_RX_SD", GPIO_PIOC1, 56 },
	{ "PAD_I2S_TX_SD", GPIO_PIOC1, 57 },
	{ "PAD_I2S_RX_WS", GPIO_PIOC1, 58 },
	{ "PAD_I2S_TX_WS", GPIO_PIOC1, 59 },
	{ "PAD_GPIO2", GPIO_PIOC1, 60 },
	{ "PAD_UART0_TXD", GPIO_PIOC1, 61 },
	{ "PAD_UART0_RXD", GPIO_PIOC1, 62 },
	{ "PAD_EIRQ1", GPIO_PIOC1, 63 },
	{ "PAD_PWM3", GPIO_PIOC2, 64 },
	{ "PAD_PWM0", GPIO_PIOC2, 65 },
	{ "PAD_GPIO0", GPIO_PIOC2, 66 },
	{ "PAD_GPIO1", GPIO_PIOC2, 67 },
	{ "PAD_DVP_0", GPIO_AIOC, 68 },
	{ "PAD_DVP_1", GPIO_AIOC, 69 },
	{ "PAD_DVP_2", GPIO_AIOC, 70 },
	{ "PAD_DVP_3", GPIO_AIOC, 71 },
	{ "PAD_DVP_4", GPIO_AIOC, 72 },
	{ "PAD_DVP_5", GPIO_AIOC, 73 },
	{ "PAD_DVP_6", GPIO_AIOC, 74 },
	{ "PAD_DVP_7", GPIO_AIOC, 75 },
	{ "PAD_DVP_8", GPIO_AIOC, 76 },
	{ "PAD_POWER_CTRL_0", GPIO_AIOC, 77 },
	{ "PAD_POWER_CTRL_1", GPIO_AIOC, 78 },
	{ "PAD_AO_BUTTON", GPIO_AIOC, 79 },
	{ "PAD_AO_WAKEUP", GPIO_AIOC, 80 },
	{ "PAD_MIPI_TX_LN0_N", GPIO_MIPI_TX, 81 },
	{ "PAD_MIPI_TX_LN0_P", GPIO_MIPI_TX, 82 },
	{ "PAD_MIPI_TX_LN1_N", GPIO_MIPI_TX, 83 },
	{ "PAD_MIPI_TX_LN1_P", GPIO_MIPI_TX, 84 },
	{ "PAD_MIPI_TX_LN2_N", GPIO_MIPI_TX, 85 },
	{ "PAD_MIPI_TX_LN2_P", GPIO_MIPI_TX, 86 },
	{ "PAD_MIPI_TX_LN3_N", GPIO_MIPI_TX, 87 },
	{ "PAD_MIPI_TX_LN3_P", GPIO_MIPI_TX, 88 },
	{ "PAD_MIPI_TX_LN4_N", GPIO_MIPI_TX, 89 },
	{ "PAD_MIPI_TX_LN4_P", GPIO_MIPI_TX, 90 },
	{ "PAD_MIPI_RX0_LN0_N", GPIO_MIPI_RX0, 91 },
	{ "PAD_MIPI_RX0_LN0_P", GPIO_MIPI_RX0, 92 },
	{ "PAD_MIPI_RX0_LN1_N", GPIO_MIPI_RX0, 93 },
	{ "PAD_MIPI_RX0_LN1_P", GPIO_MIPI_RX0, 94 },
	{ "PAD_MIPI_RX0_LN2_N", GPIO_MIPI_RX0, 95 },
	{ "PAD_MIPI_RX0_LN2_P", GPIO_MIPI_RX0, 96 },
	{ "PAD_MIPI_RX0_LN3_N", GPIO_MIPI_RX0, 97 },
	{ "PAD_MIPI_RX0_LN3_P", GPIO_MIPI_RX0, 98 },
	{ "PAD_MIPI_RX0_LN4_N", GPIO_MIPI_RX0, 99 },
	{ "PAD_MIPI_RX0_LN4_P", GPIO_MIPI_RX0, 100 },
	{ "PAD_MIPI_RX1_LN0_N", GPIO_MIPI_RX1, 101 },
	{ "PAD_MIPI_RX1_LN0_P", GPIO_MIPI_RX1, 102 },
	{ "PAD_MIPI_RX1_LN1_N", GPIO_MIPI_RX1, 103 },
	{ "PAD_MIPI_RX1_LN1_P", GPIO_MIPI_RX1, 104 },
	{ "PAD_MIPI_RX1_LN2_N", GPIO_MIPI_RX1, 105 },
	{ "PAD_MIPI_RX1_LN2_P", GPIO_MIPI_RX1, 106 },
	{ "PAD_MIPI_RX1_LN3_N", GPIO_MIPI_RX1, 107 },
	{ "PAD_MIPI_RX1_LN3_P", GPIO_MIPI_RX1, 108 },
	{ "PAD_MIPI_RX1_LN4_N", GPIO_MIPI_RX1, 109 },
	{ "PAD_MIPI_RX1_LN4_P", GPIO_MIPI_RX1, 110 },
};
// clang-format on

void io_set_mux(uint32_t base_addr, int pin, uint32_t muxsel);
void io_read_mux(uint32_t base_addr, int pin, uint32_t *value);
void io_set_conf(uint32_t base_addr, int pin, uint32_t pud, uint32_t pcfg);
int io_set_gpio_output_value(uint32_t base_addr, int gpio_pin, int value);
int io_get_gpio_value(uint32_t base_addr, int gpio_pin);
int io_set_direction_input(uint32_t base_addr, int gpio_pin);
int io_set_direction_output(uint32_t base_addr, int gpio_pin, int value);

#endif /* IO_H */
