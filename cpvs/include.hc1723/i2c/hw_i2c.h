/**
 * @file hw_i2c.h
 * @brief i2c sensor
 */
#ifndef HW_I2C_H
#define HW_I2C_H

#ifndef __KERNEL__
#include <stdint.h>
#else
#include <linux/types.h>
#endif

#define I2CM0 0 /**< Master 0. */
/**
 * @brief idxxxx.
 * @details m1.
 */
#define I2CM1 1 /*!< @brief some comment */

int32_t hw_i2c_init(uint8_t idx);
int32_t hw_i2c_slave_init(uint8_t addr);
void hw_i2c_set_tar_slave_addr(uint8_t addr);
int32_t hw_i2c_read(uint8_t len);
int32_t hw_i2c_read_after_write(const uint8_t *data, uint8_t wlen, uint8_t rlen);
uint8_t hw_i2c_read_data(uint8_t slave_addr, uint8_t reg);
void hw_i2c_write_data(uint8_t slave_addr, uint8_t reg, uint8_t data);
int32_t hw_i2c_write(const uint8_t *data, uint8_t len);
uint32_t hw_get_i2c_status();
int32_t hw_i2c_set_sda_deglitch_th(uint8_t sda_deglitch_th);
int32_t hw_i2c_set_sdo_delay_cycle(uint8_t sdo_delay_cycle);
void hw_i2c_set_scl_rate_cycle(uint8_t scl_rate_cycle);
#endif
