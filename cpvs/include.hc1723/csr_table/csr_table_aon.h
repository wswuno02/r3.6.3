#ifndef CSR_TABLE_AON_H_
#define CSR_TABLE_AON_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_aon[] =
{
  // WORD psd_in_ctrl
  { "dvp_in_reverse", 0x00000000,  0,  0,   CSR_RW, 0x00000000 },
  { "dvp_in_big_endian", 0x00000000,  8,  8,   CSR_RW, 0x00000000 },
  { "PSD_IN_CTRL", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD aon_debug
  { "debug_mon_sel", 0x00000004,  2,  0,   CSR_RW, 0x00000000 },
  { "AON_DEBUG", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mem_lp_ctrl
  { "sd", 0x00000008,  0,  0,   CSR_RW, 0x00000000 },
  { "slp", 0x00000008,  8,  8,   CSR_RW, 0x00000000 },
  { "MEM_LP_CTRL", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_reserved_0
  { "reserved_0", 0x0000000C, 15,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED_0", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_reserved_1
  { "reserved_1", 0x00000010, 15,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED_1", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_AON_H_
