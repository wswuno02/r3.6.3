#ifndef CSR_TABLE_CCM_H_
#define CSR_TABLE_CCM_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_ccm[] =
{
  // WORD r_para_0
  { "coeff_00_2s", 0x00000000, 15,  0,   CSR_RW, 0x00000177 },
  { "coeff_01_2s", 0x00000000, 31, 16,   CSR_RW, 0x000004E9 },
  { "R_PARA_0", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD r_para_1
  { "coeff_02_2s", 0x00000004, 15,  0,   CSR_RW, 0x0000007F },
  { "offset_o_0_2s", 0x00000004, 30, 16,   CSR_RW, 0x00000040 },
  { "R_PARA_1", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD g_para_0
  { "coeff_10_2s", 0x00000008, 15,  0,   CSR_RW, 0x00003F31 },
  { "coeff_11_2s", 0x00000008, 31, 16,   CSR_RW, 0x00003D4C },
  { "G_PARA_0", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD g_para_1
  { "coeff_12_2s", 0x0000000C, 15,  0,   CSR_RW, 0x00000383 },
  { "offset_o_1_2s", 0x0000000C, 30, 16,   CSR_RW, 0x00000200 },
  { "G_PARA_1", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD b_para_0
  { "coeff_20_2s", 0x00000010, 15,  0,   CSR_RW, 0x00000383 },
  { "coeff_21_2s", 0x00000010, 31, 16,   CSR_RW, 0x00003CCF },
  { "B_PARA_0", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD b_para_1
  { "coeff_22_2s", 0x00000014, 15,  0,   CSR_RW, 0x00003FAE },
  { "offset_o_2_2s", 0x00000014, 30, 16,   CSR_RW, 0x00000200 },
  { "B_PARA_1", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_debug_mon_sel
  { "debug_mon_sel", 0x00000018,  0,  0,   CSR_RW, 0x00000000 },
  { "WORD_DEBUG_MON_SEL", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_CCM_H_
