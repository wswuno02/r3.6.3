#ifndef CSR_TABLE_I2S_H_
#define CSR_TABLE_I2S_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_i2s[] =
{
  // WORD i2s00
  { "start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "stop", 0x00000000,  8,  8,  CSR_W1P, 0x00000000 },
  { "I2S00", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD i2s04
  { "sd_format", 0x00000010,  0,  0,   CSR_RW, 0x00000000 },
  { "master_mode", 0x00000010,  8,  8,   CSR_RW, 0x00000001 },
  { "use_sck_inter", 0x00000010, 16, 16,   CSR_RW, 0x00000000 },
  { "use_ws_inter", 0x00000010, 24, 24,   CSR_RW, 0x00000000 },
  { "I2S04", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2s05
  { "master_sck_0_cycle", 0x00000014,  7,  0,   CSR_RW, 0x00000005 },
  { "master_sck_1_cycle", 0x00000014, 23, 16,   CSR_RW, 0x00000005 },
  { "I2S05", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2s06
  { "master_ws_len", 0x00000018,  8,  0,   CSR_RW, 0x00000096 },
  { "master_sd_len", 0x00000018, 24, 16,   CSR_RW, 0x00000096 },
  { "I2S06", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2s07
  { "sck_deglitch_en", 0x0000001C,  0,  0,   CSR_RW, 0x00000000 },
  { "ws_deglitch_en", 0x0000001C,  8,  8,   CSR_RW, 0x00000000 },
  { "sd_deglitch_en", 0x0000001C, 16, 16,   CSR_RW, 0x00000000 },
  { "I2S07", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2s08
  { "sck_deglitch_bypass", 0x00000020,  0,  0,   CSR_RW, 0x00000001 },
  { "ws_deglitch_bypass", 0x00000020,  8,  8,   CSR_RW, 0x00000001 },
  { "sd_deglitch_bypass", 0x00000020, 16, 16,   CSR_RW, 0x00000001 },
  { "I2S08", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2s09
  { "sck_deglitch_th", 0x00000024,  5,  0,   CSR_RW, 0x00000000 },
  { "ws_deglitch_th", 0x00000024, 13,  8,   CSR_RW, 0x00000000 },
  { "sd_deglitch_th", 0x00000024, 21, 16,   CSR_RW, 0x00000000 },
  { "I2S09", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2s10
  { "debug_mon_sel", 0x00000028,  0,  0,   CSR_RW, 0x00000000 },
  { "mute", 0x00000028,  8,  8,   CSR_RW, 0x00000000 },
  { "I2S10", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2s11
  { "reserved", 0x0000002C, 31,  0,   CSR_RW, 0x00000000 },
  { "I2S11", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2s12
  { "output_sck_inter", 0x00000030,  0,  0,   CSR_RW, 0x00000000 },
  { "output_ws_inter", 0x00000030,  8,  8,   CSR_RW, 0x00000000 },
  { "I2S12", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_I2S_H_
