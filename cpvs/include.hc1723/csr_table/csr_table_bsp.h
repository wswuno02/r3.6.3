#ifndef CSR_TABLE_BSP_H_
#define CSR_TABLE_BSP_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_bsp[] =
{
  // WORD word_frame_start
  { "frame_start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "WORD_FRAME_START", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD irq_clear
  { "irq_clear_frame_end", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "IRQ_CLEAR", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD status
  { "status_frame_end", 0x00000008,  0,  0,   CSR_RO, 0x00000000 },
  { "STATUS", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // WORD irq_mask
  { "irq_mask_frame_end", 0x0000000C,  0,  0,   CSR_RW, 0x00000001 },
  { "IRQ_MASK", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io_format
  { "cfa_mode", 0x00000010,  1,  0,   CSR_RW, 0x00000000 },
  { "bayer_ini_phase_i", 0x00000010,  9,  8,   CSR_RW, 0x00000000 },
  { "bayer_ini_phase_o", 0x00000010, 17, 16,   CSR_RW, 0x00000000 },
  { "mono_o", 0x00000010, 24, 24,   CSR_RW, 0x00000000 },
  { "IO_FORMAT", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_cfa_phase_0
  { "cfa_phase_0", 0x00000014,  2,  0,   CSR_RW, 0x00000000 },
  { "cfa_phase_1", 0x00000014, 10,  8,   CSR_RW, 0x00000000 },
  { "cfa_phase_2", 0x00000014, 18, 16,   CSR_RW, 0x00000000 },
  { "cfa_phase_3", 0x00000014, 26, 24,   CSR_RW, 0x00000000 },
  { "WORD_CFA_PHASE_0", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_cfa_phase_1
  { "cfa_phase_4", 0x00000018,  2,  0,   CSR_RW, 0x00000000 },
  { "cfa_phase_5", 0x00000018, 10,  8,   CSR_RW, 0x00000000 },
  { "cfa_phase_6", 0x00000018, 18, 16,   CSR_RW, 0x00000000 },
  { "cfa_phase_7", 0x00000018, 26, 24,   CSR_RW, 0x00000000 },
  { "CFA_PHASE_1", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_cfa_phase_2
  { "cfa_phase_8", 0x0000001C,  2,  0,   CSR_RW, 0x00000000 },
  { "cfa_phase_9", 0x0000001C, 10,  8,   CSR_RW, 0x00000000 },
  { "cfa_phase_10", 0x0000001C, 18, 16,   CSR_RW, 0x00000000 },
  { "cfa_phase_11", 0x0000001C, 26, 24,   CSR_RW, 0x00000000 },
  { "CFA_PHASE_2", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_cfa_phase_3
  { "cfa_phase_12", 0x00000020,  2,  0,   CSR_RW, 0x00000000 },
  { "cfa_phase_13", 0x00000020, 10,  8,   CSR_RW, 0x00000000 },
  { "cfa_phase_14", 0x00000020, 18, 16,   CSR_RW, 0x00000000 },
  { "cfa_phase_15", 0x00000020, 26, 24,   CSR_RW, 0x00000000 },
  { "CFA_PHASE_3", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD resolution
  { "width", 0x00000024, 15,  0,   CSR_RW, 0x00000780 },
  { "height", 0x00000024, 31, 16,   CSR_RW, 0x00000438 },
  { "RESOLUTION", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sdpd_en
  { "sdpd_enable", 0x00000028,  0,  0,   CSR_RW, 0x00000000 },
  { "SDPD_EN", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sdpd_offset
  { "sdpd_offset_x", 0x0000002C, 15,  0,   CSR_RW, 0x00000000 },
  { "sdpd_offset_y", 0x0000002C, 31, 16,   CSR_RW, 0x00000000 },
  { "SDPD_OFFSET", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ddpd_mode
  { "ddpd_enable", 0x00000030,  0,  0,   CSR_RW, 0x00000000 },
  { "ddpd_g_pattern_mode", 0x00000030,  9,  8,   CSR_RW, 0x00000000 },
  { "ddpd_pix_num_mode", 0x00000030, 16, 16,   CSR_RW, 0x00000001 },
  { "DDPD_MODE", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ddpd_ratio
  { "ddpd_diff_ratio", 0x00000034,  5,  0,   CSR_RW, 0x00000000 },
  { "ddpd_avg_ratio", 0x00000034, 13,  8,   CSR_RW, 0x00000000 },
  { "ddpd_edge_diff_ratio", 0x00000034, 21, 16,   CSR_RW, 0x00000000 },
  { "ddpd_edge_avg_ratio", 0x00000034, 29, 24,   CSR_RW, 0x00000000 },
  { "DDPD_RATIO", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ddpd_edge
  { "ddpd_edge_m", 0x00000038,  6,  0,   CSR_RW, 0x00000002 },
  { "DDPD_EDGE", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_ddpd_dp_num
  { "ddpd_dp_num", 0x0000003C, 23,  0,   CSR_RO, 0x00000000 },
  { "WORD_DDPD_DP_NUM", 0x0000003C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD dpc_mode
  { "dpc_enable", 0x00000040,  0,  0,   CSR_RW, 0x00000000 },
  { "dpc_dir_enable", 0x00000040,  8,  8,   CSR_RW, 0x00000001 },
  { "DPC_MODE", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dpc_dir
  { "dpc_dir_th_ratio", 0x00000044,  5,  0,   CSR_RW, 0x00000000 },
  { "dpc_dir_str_m", 0x00000044, 14,  8,   CSR_RW, 0x00000078 },
  { "DPC_DIR", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_ct_enable
  { "ct_enable", 0x00000048,  0,  0,   CSR_RW, 0x00000000 },
  { "WORD_CT_ENABLE", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ct_avg
  { "ct_avg_lb", 0x0000004C, 15,  0,   CSR_RW, 0x00000100 },
  { "ct_avg_ratio", 0x0000004C, 24, 16,   CSR_RW, 0x00000010 },
  { "CT_AVG", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ct_str
  { "ct_str_lb", 0x00000050, 15,  0,   CSR_RW, 0x00000100 },
  { "ct_str_ratio", 0x00000050, 24, 16,   CSR_RW, 0x00000010 },
  { "CT_STR", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_ct_str_m
  { "ct_str_m", 0x00000054,  5,  0,   CSR_RW, 0x00000000 },
  { "WORD_CT_STR_M", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ch_weight_0
  { "channel_weight_00", 0x00000058,  4,  0,   CSR_RW, 0x00000001 },
  { "channel_weight_01", 0x00000058, 12,  8,   CSR_RW, 0x00000004 },
  { "channel_weight_02", 0x00000058, 20, 16,   CSR_RW, 0x00000006 },
  { "CH_WEIGHT_0", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ch_weight_1
  { "channel_weight_11", 0x0000005C,  6,  0,   CSR_RW, 0x00000010 },
  { "channel_weight_12", 0x0000005C, 14,  8,   CSR_RW, 0x00000018 },
  { "channel_weight_22", 0x0000005C, 22, 16,   CSR_RW, 0x00000024 },
  { "CH_WEIGHT_1", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_remos_enable
  { "remos_enable", 0x00000060,  0,  0,   CSR_RW, 0x00000000 },
  { "WORD_REMOS_ENABLE", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD remos_coeff_r_0
  { "remos_r_r_coeff_2s", 0x00000064, 13,  0,   CSR_RW, 0x00000000 },
  { "remos_r_g_coeff_2s", 0x00000064, 29, 16,   CSR_RW, 0x00000000 },
  { "REMOS_COEFF_R_0", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD remos_coeff_r_1
  { "remos_r_b_coeff_2s", 0x00000068, 13,  0,   CSR_RW, 0x00000000 },
  { "remos_r_s_coeff_2s", 0x00000068, 29, 16,   CSR_RW, 0x00000000 },
  { "REMOS_COEFF_R_1", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD remos_coeff_g_0
  { "remos_g_r_coeff_2s", 0x0000006C, 13,  0,   CSR_RW, 0x00000000 },
  { "remos_g_g_coeff_2s", 0x0000006C, 29, 16,   CSR_RW, 0x00000000 },
  { "REMOS_COEFF_G_0", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD remos_coeff_g_1
  { "remos_g_b_coeff_2s", 0x00000070, 13,  0,   CSR_RW, 0x00000000 },
  { "remos_g_s_coeff_2s", 0x00000070, 29, 16,   CSR_RW, 0x00000000 },
  { "REMOS_COEFF_G_1", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // WORD remos_coeff_b_0
  { "remos_b_r_coeff_2s", 0x00000074, 13,  0,   CSR_RW, 0x00000000 },
  { "remos_b_g_coeff_2s", 0x00000074, 29, 16,   CSR_RW, 0x00000000 },
  { "REMOS_COEFF_B_0", 0x00000074, 31, 0,   CSR_RW, 0x00000000 },
  // WORD remos_coeff_b_1
  { "remos_b_b_coeff_2s", 0x00000078, 13,  0,   CSR_RW, 0x00000000 },
  { "remos_b_s_coeff_2s", 0x00000078, 29, 16,   CSR_RW, 0x00000000 },
  { "REMOS_COEFF_B_1", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_y_est_mode
  { "y_est_mode", 0x0000007C,  1,  0,   CSR_RW, 0x00000000 },
  { "WORD_Y_EST_MODE", 0x0000007C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD y_est_weight
  { "y_r_weight", 0x00000080,  5,  0,   CSR_RW, 0x00000008 },
  { "y_g_weight", 0x00000080, 13,  8,   CSR_RW, 0x00000010 },
  { "y_b_weight", 0x00000080, 21, 16,   CSR_RW, 0x00000008 },
  { "y_s_weight", 0x00000080, 29, 24,   CSR_RW, 0x00000000 },
  { "Y_EST_WEIGHT", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_contrast_y_mode
  { "contrast_y_mode", 0x00000084,  1,  0,   CSR_RW, 0x00000000 },
  { "WORD_CONTRAST_Y_MODE", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD contrast_y_gain_0
  { "contrast_y_gain_th_max", 0x00000088,  7,  0,   CSR_RW, 0x000000C0 },
  { "contrast_y_gain_th_min", 0x00000088, 23, 16,   CSR_RW, 0x00000040 },
  { "CONTRAST_Y_GAIN_0", 0x00000088, 31, 0,   CSR_RW, 0x00000000 },
  // WORD contrast_y_gain_1
  { "contrast_y_gain_m", 0x0000008C,  4,  0,   CSR_RW, 0x00000010 },
  { "CONTRAST_Y_GAIN_1", 0x0000008C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD contrast_cfa_enable_0
  { "contrast_cfa_phase_en_g0", 0x00000090,  0,  0,   CSR_RW, 0x00000001 },
  { "contrast_cfa_phase_en_r", 0x00000090,  8,  8,   CSR_RW, 0x00000001 },
  { "contrast_cfa_phase_en_b", 0x00000090, 16, 16,   CSR_RW, 0x00000001 },
  { "CONTRAST_CFA_ENABLE_0", 0x00000090, 31, 0,   CSR_RW, 0x00000000 },
  // WORD contrast_cfa_enable_1
  { "contrast_cfa_phase_en_g1", 0x00000094,  0,  0,   CSR_RW, 0x00000001 },
  { "contrast_cfa_phase_en_s", 0x00000094,  8,  8,   CSR_RW, 0x00000000 },
  { "CONTRAST_CFA_ENABLE_1", 0x00000094, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_contrast_h1_weight_0
  { "contrast_h1_weight_0", 0x00000098,  7,  0,   CSR_RW, 0x0000003C },
  { "contrast_h1_weight_1", 0x00000098, 15,  8,   CSR_RW, 0x000000E4 },
  { "contrast_h1_weight_2", 0x00000098, 23, 16,   CSR_RW, 0x00000015 },
  { "contrast_h1_weight_3", 0x00000098, 31, 24,   CSR_RW, 0x000000D6 },
  { "WORD_CONTRAST_H1_WEIGHT_0", 0x00000098, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_contrast_h1_weight_1
  { "contrast_h1_weight_4", 0x0000009C,  7,  0,   CSR_RW, 0x00000015 },
  { "contrast_h1_weight_5", 0x0000009C, 15,  8,   CSR_RW, 0x00000037 },
  { "contrast_h1_weight_6", 0x0000009C, 23, 16,   CSR_RW, 0x000000E8 },
  { "contrast_h1_weight_7", 0x0000009C, 31, 24,   CSR_RW, 0x00000027 },
  { "CONTRAST_H1_WEIGHT_1", 0x0000009C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_contrast_h1_weight_2
  { "contrast_h1_weight_8", 0x000000A0,  7,  0,   CSR_RW, 0x000000B1 },
  { "contrast_h1_weight_9", 0x000000A0, 15,  8,   CSR_RW, 0x00000027 },
  { "CONTRAST_H1_WEIGHT_2", 0x000000A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_contrast_h2_weight_0
  { "contrast_h2_weight_0", 0x000000A4,  7,  0,   CSR_RW, 0x0000003C },
  { "contrast_h2_weight_1", 0x000000A4, 15,  8,   CSR_RW, 0x000000E4 },
  { "contrast_h2_weight_2", 0x000000A4, 23, 16,   CSR_RW, 0x00000015 },
  { "contrast_h2_weight_3", 0x000000A4, 31, 24,   CSR_RW, 0x000000D6 },
  { "WORD_CONTRAST_H2_WEIGHT_0", 0x000000A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_contrast_h2_weight_1
  { "contrast_h2_weight_4", 0x000000A8,  7,  0,   CSR_RW, 0x00000015 },
  { "contrast_h2_weight_5", 0x000000A8, 15,  8,   CSR_RW, 0x00000037 },
  { "contrast_h2_weight_6", 0x000000A8, 23, 16,   CSR_RW, 0x000000E8 },
  { "contrast_h2_weight_7", 0x000000A8, 31, 24,   CSR_RW, 0x00000027 },
  { "CONTRAST_H2_WEIGHT_1", 0x000000A8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_contrast_h2_weight_2
  { "contrast_h2_weight_8", 0x000000AC,  7,  0,   CSR_RW, 0x000000B1 },
  { "contrast_h2_weight_9", 0x000000AC, 15,  8,   CSR_RW, 0x00000027 },
  { "CONTRAST_H2_WEIGHT_2", 0x000000AC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_contrast_v1_weight_0
  { "contrast_v1_weight_0", 0x000000B0,  8,  0,   CSR_RW, 0x000000E0 },
  { "contrast_v1_weight_1", 0x000000B0, 24, 16,   CSR_RW, 0x00000000 },
  { "WORD_CONTRAST_V1_WEIGHT_0", 0x000000B0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_contrast_v1_weight_1
  { "contrast_v1_weight_2", 0x000000B4,  8,  0,   CSR_RW, 0x00000020 },
  { "CONTRAST_V1_WEIGHT_1", 0x000000B4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_contrast_v2_weight_0
  { "contrast_v2_weight_0", 0x000000B8,  8,  0,   CSR_RW, 0x000000E0 },
  { "contrast_v2_weight_1", 0x000000B8, 24, 16,   CSR_RW, 0x00000000 },
  { "WORD_CONTRAST_V2_WEIGHT_0", 0x000000B8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_contrast_v2_weight_1
  { "contrast_v2_weight_2", 0x000000BC,  8,  0,   CSR_RW, 0x00000020 },
  { "CONTRAST_V2_WEIGHT_1", 0x000000BC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_contrast_coring_th
  { "contrast_coring_th", 0x000000C0, 13,  0,   CSR_RW, 0x00000020 },
  { "WORD_CONTRAST_CORING_TH", 0x000000C0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_awb_gwd_y_mode
  { "awb_gwd_y_mode", 0x000000C4,  1,  0,   CSR_RW, 0x00000000 },
  { "WORD_AWB_GWD_Y_MODE", 0x000000C4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_gwd_y_gain_0
  { "awb_gwd_y_th_max", 0x000000C8,  7,  0,   CSR_RW, 0x000000C0 },
  { "awb_gwd_y_th_min", 0x000000C8, 23, 16,   CSR_RW, 0x00000040 },
  { "AWB_GWD_Y_GAIN_0", 0x000000C8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_gwd_y_gain_1
  { "awb_gwd_y_m", 0x000000CC,  4,  0,   CSR_RW, 0x00000010 },
  { "AWB_GWD_Y_GAIN_1", 0x000000CC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_gwd_rto_rg_0
  { "awb_gwd_rg_0", 0x000000D0,  5,  0,   CSR_RW, 0x00000000 },
  { "awb_gwd_rg_1", 0x000000D0, 13,  8,   CSR_RW, 0x00000005 },
  { "awb_gwd_rg_2", 0x000000D0, 21, 16,   CSR_RW, 0x00000007 },
  { "awb_gwd_rg_3", 0x000000D0, 29, 24,   CSR_RW, 0x00000008 },
  { "AWB_GWD_RTO_RG_0", 0x000000D0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_gwd_rto_rg_1
  { "awb_gwd_rg_4", 0x000000D4,  5,  0,   CSR_RW, 0x00000008 },
  { "AWB_GWD_RTO_RG_1", 0x000000D4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_gwd_rto_bg_0
  { "awb_gwd_bg_0", 0x000000D8,  5,  0,   CSR_RW, 0x0000000C },
  { "awb_gwd_bg_1", 0x000000D8, 13,  8,   CSR_RW, 0x0000000C },
  { "awb_gwd_bg_2", 0x000000D8, 21, 16,   CSR_RW, 0x0000000A },
  { "awb_gwd_bg_3", 0x000000D8, 29, 24,   CSR_RW, 0x00000008 },
  { "AWB_GWD_RTO_BG_0", 0x000000D8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_gwd_rto_bg_1
  { "awb_gwd_bg_4", 0x000000DC,  5,  0,   CSR_RW, 0x00000008 },
  { "AWB_GWD_RTO_BG_1", 0x000000DC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_gwd_rto_a_0
  { "awb_gwd_a_2s_0", 0x000000E0,  9,  0,   CSR_RW, 0x00000000 },
  { "awb_gwd_a_2s_1", 0x000000E0, 25, 16,   CSR_RW, 0x0000000B },
  { "AWB_GWD_RTO_A_0", 0x000000E0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_gwd_rto_a_1
  { "awb_gwd_a_2s_2", 0x000000E4,  9,  0,   CSR_RW, 0x0000000E },
  { "awb_gwd_a_2s_3", 0x000000E4, 25, 16,   CSR_RW, 0x00000010 },
  { "AWB_GWD_RTO_A_1", 0x000000E4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_gwd_rto_b_0
  { "awb_gwd_b_2s_0", 0x000000E8,  9,  0,   CSR_RW, 0x00000010 },
  { "awb_gwd_b_2s_1", 0x000000E8, 25, 16,   CSR_RW, 0x0000000B },
  { "AWB_GWD_RTO_B_0", 0x000000E8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_gwd_rto_b_1
  { "awb_gwd_b_2s_2", 0x000000EC,  9,  0,   CSR_RW, 0x00000007 },
  { "awb_gwd_b_2s_3", 0x000000EC, 25, 16,   CSR_RW, 0x00000002 },
  { "AWB_GWD_RTO_B_1", 0x000000EC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_gwd_rto_c_0
  { "awb_gwd_c_2s_0", 0x000000F0, 15,  0,   CSR_RW, 0x00000040 },
  { "awb_gwd_c_2s_1", 0x000000F0, 31, 16,   CSR_RW, 0x00000040 },
  { "AWB_GWD_RTO_C_0", 0x000000F0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_gwd_rto_c_1
  { "awb_gwd_c_2s_2", 0x000000F4, 15,  0,   CSR_RW, 0x00000054 },
  { "awb_gwd_c_2s_3", 0x000000F4, 31, 16,   CSR_RW, 0x00000071 },
  { "AWB_GWD_RTO_C_1", 0x000000F4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_gwd_rto_th
  { "awb_gwd_rto_bg_th_0", 0x000000F8,  6,  0,   CSR_RW, 0x00000001 },
  { "awb_gwd_rto_bg_th_1", 0x000000F8, 14,  8,   CSR_RW, 0x00000001 },
  { "awb_gwd_rto_bg_th_2", 0x000000F8, 22, 16,   CSR_RW, 0x00000001 },
  { "awb_gwd_rto_bg_th_3", 0x000000F8, 30, 24,   CSR_RW, 0x00000001 },
  { "AWB_GWD_RTO_TH", 0x000000F8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_gwd_rto_th_m
  { "awb_gwd_rto_bg_m_0", 0x000000FC,  6,  0,   CSR_RW, 0x0000001C },
  { "awb_gwd_rto_bg_m_1", 0x000000FC, 14,  8,   CSR_RW, 0x0000001C },
  { "awb_gwd_rto_bg_m_2", 0x000000FC, 22, 16,   CSR_RW, 0x0000001C },
  { "awb_gwd_rto_bg_m_3", 0x000000FC, 30, 24,   CSR_RW, 0x0000001C },
  { "AWB_GWD_RTO_TH_M", 0x000000FC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_awb_rtx_y_mode
  { "awb_rtx_y_mode", 0x00000100,  1,  0,   CSR_RW, 0x00000000 },
  { "WORD_AWB_RTX_Y_MODE", 0x00000100, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_rtx_y_gain_0
  { "awb_rtx_y_th_min", 0x00000104,  7,  0,   CSR_RW, 0x000000C0 },
  { "awb_rtx_y_th_max", 0x00000104, 23, 16,   CSR_RW, 0x00000040 },
  { "AWB_RTX_Y_GAIN_0", 0x00000104, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_rtx_y_gain_1
  { "awb_rtx_y_m", 0x00000108,  4,  0,   CSR_RW, 0x00000010 },
  { "AWB_RTX_Y_GAIN_1", 0x00000108, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_awb_rtx_region_mode
  { "awb_rtx_region_mode", 0x0000010C,  2,  0,   CSR_RW, 0x00000003 },
  { "WORD_AWB_RTX_REGION_MODE", 0x0000010C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_tm_enable
  { "tm_enable", 0x00000110,  0,  0,   CSR_RW, 0x00000000 },
  { "WORD_TM_ENABLE", 0x00000110, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_0
  { "tone_curve_1", 0x00000114, 13,  0,   CSR_RW, 0x00000040 },
  { "tone_curve_2", 0x00000114, 29, 16,   CSR_RW, 0x00000080 },
  { "TM_STRL_0", 0x00000114, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_1
  { "tone_curve_3", 0x00000118, 13,  0,   CSR_RW, 0x000000C0 },
  { "tone_curve_4", 0x00000118, 29, 16,   CSR_RW, 0x00000100 },
  { "TM_STRL_1", 0x00000118, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_2
  { "tone_curve_5", 0x0000011C, 13,  0,   CSR_RW, 0x00000140 },
  { "tone_curve_6", 0x0000011C, 29, 16,   CSR_RW, 0x00000180 },
  { "TM_STRL_2", 0x0000011C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_3
  { "tone_curve_7", 0x00000120, 13,  0,   CSR_RW, 0x000001C0 },
  { "tone_curve_8", 0x00000120, 29, 16,   CSR_RW, 0x00000200 },
  { "TM_STRL_3", 0x00000120, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_4
  { "tone_curve_9", 0x00000124, 13,  0,   CSR_RW, 0x00000280 },
  { "tone_curve_10", 0x00000124, 29, 16,   CSR_RW, 0x00000300 },
  { "TM_STRL_4", 0x00000124, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_5
  { "tone_curve_11", 0x00000128, 13,  0,   CSR_RW, 0x00000380 },
  { "tone_curve_12", 0x00000128, 29, 16,   CSR_RW, 0x00000400 },
  { "TM_STRL_5", 0x00000128, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_6
  { "tone_curve_13", 0x0000012C, 13,  0,   CSR_RW, 0x00000480 },
  { "tone_curve_14", 0x0000012C, 29, 16,   CSR_RW, 0x00000500 },
  { "TM_STRL_6", 0x0000012C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_7
  { "tone_curve_15", 0x00000130, 13,  0,   CSR_RW, 0x00000580 },
  { "tone_curve_16", 0x00000130, 29, 16,   CSR_RW, 0x00000600 },
  { "TM_STRL_7", 0x00000130, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_8
  { "tone_curve_17", 0x00000134, 13,  0,   CSR_RW, 0x00000680 },
  { "tone_curve_18", 0x00000134, 29, 16,   CSR_RW, 0x00000700 },
  { "TM_STRL_8", 0x00000134, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_9
  { "tone_curve_19", 0x00000138, 13,  0,   CSR_RW, 0x00000780 },
  { "tone_curve_20", 0x00000138, 29, 16,   CSR_RW, 0x00000800 },
  { "TM_STRL_9", 0x00000138, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_10
  { "tone_curve_21", 0x0000013C, 13,  0,   CSR_RW, 0x00000880 },
  { "tone_curve_22", 0x0000013C, 29, 16,   CSR_RW, 0x00000900 },
  { "TM_STRL_10", 0x0000013C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_11
  { "tone_curve_23", 0x00000140, 13,  0,   CSR_RW, 0x00000980 },
  { "tone_curve_24", 0x00000140, 29, 16,   CSR_RW, 0x00000A00 },
  { "TM_STRL_11", 0x00000140, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_12
  { "tone_curve_25", 0x00000144, 13,  0,   CSR_RW, 0x00000B00 },
  { "tone_curve_26", 0x00000144, 29, 16,   CSR_RW, 0x00000C00 },
  { "TM_STRL_12", 0x00000144, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_13
  { "tone_curve_27", 0x00000148, 13,  0,   CSR_RW, 0x00000D00 },
  { "tone_curve_28", 0x00000148, 29, 16,   CSR_RW, 0x00000E00 },
  { "TM_STRL_13", 0x00000148, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_14
  { "tone_curve_29", 0x0000014C, 13,  0,   CSR_RW, 0x00000F00 },
  { "tone_curve_30", 0x0000014C, 29, 16,   CSR_RW, 0x00001000 },
  { "TM_STRL_14", 0x0000014C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_15
  { "tone_curve_31", 0x00000150, 13,  0,   CSR_RW, 0x00001100 },
  { "tone_curve_32", 0x00000150, 29, 16,   CSR_RW, 0x00001200 },
  { "TM_STRL_15", 0x00000150, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_16
  { "tone_curve_33", 0x00000154, 13,  0,   CSR_RW, 0x00001300 },
  { "tone_curve_34", 0x00000154, 29, 16,   CSR_RW, 0x00001400 },
  { "TM_STRL_16", 0x00000154, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_17
  { "tone_curve_35", 0x00000158, 13,  0,   CSR_RW, 0x00001500 },
  { "tone_curve_36", 0x00000158, 29, 16,   CSR_RW, 0x00001600 },
  { "TM_STRL_17", 0x00000158, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_18
  { "tone_curve_37", 0x0000015C, 13,  0,   CSR_RW, 0x00001700 },
  { "tone_curve_38", 0x0000015C, 29, 16,   CSR_RW, 0x00001800 },
  { "TM_STRL_18", 0x0000015C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_19
  { "tone_curve_39", 0x00000160, 13,  0,   CSR_RW, 0x00001900 },
  { "tone_curve_40", 0x00000160, 29, 16,   CSR_RW, 0x00001A00 },
  { "TM_STRL_19", 0x00000160, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_20
  { "tone_curve_41", 0x00000164, 13,  0,   CSR_RW, 0x00001C00 },
  { "tone_curve_42", 0x00000164, 29, 16,   CSR_RW, 0x00001E00 },
  { "TM_STRL_20", 0x00000164, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_21
  { "tone_curve_43", 0x00000168, 13,  0,   CSR_RW, 0x00002000 },
  { "tone_curve_44", 0x00000168, 29, 16,   CSR_RW, 0x00002200 },
  { "TM_STRL_21", 0x00000168, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_22
  { "tone_curve_45", 0x0000016C, 13,  0,   CSR_RW, 0x00002400 },
  { "tone_curve_46", 0x0000016C, 29, 16,   CSR_RW, 0x00002600 },
  { "TM_STRL_22", 0x0000016C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_23
  { "tone_curve_47", 0x00000170, 13,  0,   CSR_RW, 0x00002800 },
  { "tone_curve_48", 0x00000170, 29, 16,   CSR_RW, 0x00002A00 },
  { "TM_STRL_23", 0x00000170, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_24
  { "tone_curve_49", 0x00000174, 13,  0,   CSR_RW, 0x00002C00 },
  { "tone_curve_50", 0x00000174, 29, 16,   CSR_RW, 0x00002E00 },
  { "TM_STRL_24", 0x00000174, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_25
  { "tone_curve_51", 0x00000178, 13,  0,   CSR_RW, 0x00003000 },
  { "tone_curve_52", 0x00000178, 29, 16,   CSR_RW, 0x00003200 },
  { "TM_STRL_25", 0x00000178, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_26
  { "tone_curve_53", 0x0000017C, 13,  0,   CSR_RW, 0x00003400 },
  { "tone_curve_54", 0x0000017C, 29, 16,   CSR_RW, 0x00003600 },
  { "TM_STRL_26", 0x0000017C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_27
  { "tone_curve_55", 0x00000180, 13,  0,   CSR_RW, 0x00003800 },
  { "tone_curve_56", 0x00000180, 29, 16,   CSR_RW, 0x00003A00 },
  { "TM_STRL_27", 0x00000180, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tm_strl_28
  { "tone_curve_57", 0x00000184, 13,  0,   CSR_RW, 0x00003C00 },
  { "tone_curve_58", 0x00000184, 29, 16,   CSR_RW, 0x00003E00 },
  { "TM_STRL_28", 0x00000184, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_wb_enable
  { "wb_enable", 0x00000188,  0,  0,   CSR_RW, 0x00000000 },
  { "WORD_WB_ENABLE", 0x00000188, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wb_gain_0
  { "wb_gain_g0", 0x0000018C, 11,  0,   CSR_RW, 0x00000100 },
  { "wb_gain_r", 0x0000018C, 27, 16,   CSR_RW, 0x00000100 },
  { "WB_GAIN_0", 0x0000018C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wb_gain_1
  { "wb_gain_b", 0x00000190, 11,  0,   CSR_RW, 0x00000100 },
  { "wb_gain_g1", 0x00000190, 27, 16,   CSR_RW, 0x00000100 },
  { "WB_GAIN_1", 0x00000190, 31, 0,   CSR_RW, 0x00000000 },
  // WORD y_hist_mode
  { "y_hist_in_mode", 0x00000194,  1,  0,   CSR_RW, 0x00000000 },
  { "y_hist_roi_en", 0x00000194, 16, 16,   CSR_RW, 0x00000000 },
  { "Y_HIST_MODE", 0x00000194, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_y_hist_overflow
  { "y_hist_overflow", 0x00000198,  0,  0,   CSR_RO, 0x00000000 },
  { "WORD_Y_HIST_OVERFLOW", 0x00000198, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_roi_x
  { "y_hist_roi_sx", 0x0000019C, 15,  0,   CSR_RW, 0x00000000 },
  { "y_hist_roi_ex", 0x0000019C, 31, 16,   CSR_RW, 0x00000780 },
  { "Y_HIST_ROI_X", 0x0000019C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD y_hist_roi_y
  { "y_hist_roi_sy", 0x000001A0, 15,  0,   CSR_RW, 0x00000000 },
  { "y_hist_roi_ey", 0x000001A0, 31, 16,   CSR_RW, 0x00000438 },
  { "Y_HIST_ROI_Y", 0x000001A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_y_hist_offset
  { "y_hist_offset", 0x000001A4, 13,  0,   CSR_RW, 0x00000000 },
  { "WORD_Y_HIST_OFFSET", 0x000001A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD y_hist_0
  { "y_hist_hist_0", 0x000001A8, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_0", 0x000001A8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_1
  { "y_hist_hist_1", 0x000001AC, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_1", 0x000001AC, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_2
  { "y_hist_hist_2", 0x000001B0, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_2", 0x000001B0, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_3
  { "y_hist_hist_3", 0x000001B4, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_3", 0x000001B4, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_4
  { "y_hist_hist_4", 0x000001B8, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_4", 0x000001B8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_5
  { "y_hist_hist_5", 0x000001BC, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_5", 0x000001BC, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_6
  { "y_hist_hist_6", 0x000001C0, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_6", 0x000001C0, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_7
  { "y_hist_hist_7", 0x000001C4, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_7", 0x000001C4, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_8
  { "y_hist_hist_8", 0x000001C8, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_8", 0x000001C8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_9
  { "y_hist_hist_9", 0x000001CC, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_9", 0x000001CC, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_10
  { "y_hist_hist_10", 0x000001D0, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_10", 0x000001D0, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_11
  { "y_hist_hist_11", 0x000001D4, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_11", 0x000001D4, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_12
  { "y_hist_hist_12", 0x000001D8, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_12", 0x000001D8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_13
  { "y_hist_hist_13", 0x000001DC, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_13", 0x000001DC, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_14
  { "y_hist_hist_14", 0x000001E0, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_14", 0x000001E0, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_15
  { "y_hist_hist_15", 0x000001E4, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_15", 0x000001E4, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_16
  { "y_hist_hist_16", 0x000001E8, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_16", 0x000001E8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_17
  { "y_hist_hist_17", 0x000001EC, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_17", 0x000001EC, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_18
  { "y_hist_hist_18", 0x000001F0, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_18", 0x000001F0, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_19
  { "y_hist_hist_19", 0x000001F4, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_19", 0x000001F4, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_20
  { "y_hist_hist_20", 0x000001F8, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_20", 0x000001F8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_21
  { "y_hist_hist_21", 0x000001FC, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_21", 0x000001FC, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_22
  { "y_hist_hist_22", 0x00000200, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_22", 0x00000200, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_23
  { "y_hist_hist_23", 0x00000204, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_23", 0x00000204, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_24
  { "y_hist_hist_24", 0x00000208, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_24", 0x00000208, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_25
  { "y_hist_hist_25", 0x0000020C, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_25", 0x0000020C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_26
  { "y_hist_hist_26", 0x00000210, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_26", 0x00000210, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_27
  { "y_hist_hist_27", 0x00000214, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_27", 0x00000214, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_28
  { "y_hist_hist_28", 0x00000218, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_28", 0x00000218, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_29
  { "y_hist_hist_29", 0x0000021C, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_29", 0x0000021C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_30
  { "y_hist_hist_30", 0x00000220, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_30", 0x00000220, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_31
  { "y_hist_hist_31", 0x00000224, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_31", 0x00000224, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_32
  { "y_hist_hist_32", 0x00000228, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_32", 0x00000228, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_33
  { "y_hist_hist_33", 0x0000022C, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_33", 0x0000022C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_34
  { "y_hist_hist_34", 0x00000230, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_34", 0x00000230, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_35
  { "y_hist_hist_35", 0x00000234, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_35", 0x00000234, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_36
  { "y_hist_hist_36", 0x00000238, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_36", 0x00000238, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_37
  { "y_hist_hist_37", 0x0000023C, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_37", 0x0000023C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_38
  { "y_hist_hist_38", 0x00000240, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_38", 0x00000240, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_39
  { "y_hist_hist_39", 0x00000244, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_39", 0x00000244, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_40
  { "y_hist_hist_40", 0x00000248, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_40", 0x00000248, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_41
  { "y_hist_hist_41", 0x0000024C, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_41", 0x0000024C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_42
  { "y_hist_hist_42", 0x00000250, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_42", 0x00000250, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_43
  { "y_hist_hist_43", 0x00000254, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_43", 0x00000254, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_44
  { "y_hist_hist_44", 0x00000258, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_44", 0x00000258, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_45
  { "y_hist_hist_45", 0x0000025C, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_45", 0x0000025C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_46
  { "y_hist_hist_46", 0x00000260, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_46", 0x00000260, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_47
  { "y_hist_hist_47", 0x00000264, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_47", 0x00000264, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_48
  { "y_hist_hist_48", 0x00000268, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_48", 0x00000268, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_49
  { "y_hist_hist_49", 0x0000026C, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_49", 0x0000026C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_50
  { "y_hist_hist_50", 0x00000270, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_50", 0x00000270, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_51
  { "y_hist_hist_51", 0x00000274, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_51", 0x00000274, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_52
  { "y_hist_hist_52", 0x00000278, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_52", 0x00000278, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_53
  { "y_hist_hist_53", 0x0000027C, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_53", 0x0000027C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_54
  { "y_hist_hist_54", 0x00000280, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_54", 0x00000280, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_55
  { "y_hist_hist_55", 0x00000284, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_55", 0x00000284, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_56
  { "y_hist_hist_56", 0x00000288, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_56", 0x00000288, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_57
  { "y_hist_hist_57", 0x0000028C, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_57", 0x0000028C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_58
  { "y_hist_hist_58", 0x00000290, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_58", 0x00000290, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_hist_59
  { "y_hist_hist_59", 0x00000294, 17,  0,   CSR_RO, 0x00000000 },
  { "Y_HIST_59", 0x00000294, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_rtx_mode
  { "awb_rtx_roi_en", 0x00000298,  0,  0,   CSR_RW, 0x00000000 },
  { "AWB_RTX_MODE", 0x00000298, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_rtx_roi_x
  { "awb_rtx_roi_sx", 0x0000029C, 15,  0,   CSR_RW, 0x00000000 },
  { "awb_rtx_roi_ex", 0x0000029C, 31, 16,   CSR_RW, 0x00000780 },
  { "AWB_RTX_ROI_X", 0x0000029C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_rtx_roi_y
  { "awb_rtx_roi_sy", 0x000002A0, 15,  0,   CSR_RW, 0x00000000 },
  { "awb_rtx_roi_ey", 0x000002A0, 31, 16,   CSR_RW, 0x00000438 },
  { "AWB_RTX_ROI_Y", 0x000002A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_awb_rtx_sum_g0
  { "awb_rtx_sum_g0", 0x000002A4, 20,  0,   CSR_RO, 0x00000000 },
  { "WORD_AWB_RTX_SUM_G0", 0x000002A4, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_awb_rtx_sum_r
  { "awb_rtx_sum_r", 0x000002A8, 20,  0,   CSR_RO, 0x00000000 },
  { "WORD_AWB_RTX_SUM_R", 0x000002A8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_awb_rtx_sum_b
  { "awb_rtx_sum_b", 0x000002AC, 20,  0,   CSR_RO, 0x00000000 },
  { "WORD_AWB_RTX_SUM_B", 0x000002AC, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_awb_rtx_sum_g1
  { "awb_rtx_sum_g1", 0x000002B0, 20,  0,   CSR_RO, 0x00000000 },
  { "WORD_AWB_RTX_SUM_G1", 0x000002B0, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_rtx_weight_sum
  { "awb_rtx_norm_g0", 0x000002B4,  6,  0,   CSR_RO, 0x00000000 },
  { "awb_rtx_norm_r", 0x000002B4, 14,  8,   CSR_RO, 0x00000000 },
  { "awb_rtx_norm_b", 0x000002B4, 22, 16,   CSR_RO, 0x00000000 },
  { "awb_rtx_norm_g1", 0x000002B4, 30, 24,   CSR_RO, 0x00000000 },
  { "AWB_RTX_WEIGHT_SUM", 0x000002B4, 31, 0,   CSR_RO, 0x00000000 },
  // WORD roi_y_avg_mode
  { "roi_y_avg_in_mode", 0x000002B8,  1,  0,   CSR_RW, 0x00000000 },
  { "ROI_Y_AVG_MODE", 0x000002B8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD roi_y_avg_enable
  { "roi_0_y_avg_en", 0x000002BC,  0,  0,   CSR_RW, 0x00000000 },
  { "roi_1_y_avg_en", 0x000002BC,  8,  8,   CSR_RW, 0x00000000 },
  { "roi_2_y_avg_en", 0x000002BC, 16, 16,   CSR_RW, 0x00000000 },
  { "roi_3_y_avg_en", 0x000002BC, 24, 24,   CSR_RW, 0x00000000 },
  { "ROI_Y_AVG_ENABLE", 0x000002BC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD y_avg_roi_0_x
  { "roi_0_y_avg_sx", 0x000002C0, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_0_y_avg_ex", 0x000002C0, 31, 16,   CSR_RW, 0x00000780 },
  { "Y_AVG_ROI_0_X", 0x000002C0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD y_avg_roi_0_y
  { "roi_0_y_avg_sy", 0x000002C4, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_0_y_avg_ey", 0x000002C4, 31, 16,   CSR_RW, 0x00000438 },
  { "Y_AVG_ROI_0_Y", 0x000002C4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD y_avg_roi_1_x
  { "roi_1_y_avg_sx", 0x000002C8, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_1_y_avg_ex", 0x000002C8, 31, 16,   CSR_RW, 0x00000780 },
  { "Y_AVG_ROI_1_X", 0x000002C8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD y_avg_roi_1_y
  { "roi_1_y_avg_sy", 0x000002CC, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_1_y_avg_ey", 0x000002CC, 31, 16,   CSR_RW, 0x00000438 },
  { "Y_AVG_ROI_1_Y", 0x000002CC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD y_avg_roi_2_x
  { "roi_2_y_avg_sx", 0x000002D0, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_2_y_avg_ex", 0x000002D0, 31, 16,   CSR_RW, 0x00000780 },
  { "Y_AVG_ROI_2_X", 0x000002D0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD y_avg_roi_2_y
  { "roi_2_y_avg_sy", 0x000002D4, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_2_y_avg_ey", 0x000002D4, 31, 16,   CSR_RW, 0x00000438 },
  { "Y_AVG_ROI_2_Y", 0x000002D4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD y_avg_roi_3_x
  { "roi_3_y_avg_sx", 0x000002D8, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_3_y_avg_ex", 0x000002D8, 31, 16,   CSR_RW, 0x00000780 },
  { "Y_AVG_ROI_3_X", 0x000002D8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD y_avg_roi_3_y
  { "roi_3_y_avg_sy", 0x000002DC, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_3_y_avg_ey", 0x000002DC, 31, 16,   CSR_RW, 0x00000438 },
  { "Y_AVG_ROI_3_Y", 0x000002DC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD y_avg_roi_pix_num_0
  { "roi_0_y_avg_pix_num", 0x000002E0, 23,  0,   CSR_RW, 0x00000000 },
  { "Y_AVG_ROI_PIX_NUM_0", 0x000002E0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD y_avg_roi_pix_num_1
  { "roi_1_y_avg_pix_num", 0x000002E4, 23,  0,   CSR_RW, 0x00000000 },
  { "Y_AVG_ROI_PIX_NUM_1", 0x000002E4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD y_avg_roi_pix_num_2
  { "roi_2_y_avg_pix_num", 0x000002E8, 23,  0,   CSR_RW, 0x00000000 },
  { "Y_AVG_ROI_PIX_NUM_2", 0x000002E8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD y_avg_roi_pix_num_3
  { "roi_3_y_avg_pix_num", 0x000002EC, 23,  0,   CSR_RW, 0x00000000 },
  { "Y_AVG_ROI_PIX_NUM_3", 0x000002EC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD y_avg_roi_remainder_0
  { "roi_0_y_avg_remainder", 0x000002F0, 23,  0,   CSR_RO, 0x00000000 },
  { "Y_AVG_ROI_REMAINDER_0", 0x000002F0, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_avg_roi_remainder_1
  { "roi_1_y_avg_remainder", 0x000002F4, 23,  0,   CSR_RO, 0x00000000 },
  { "Y_AVG_ROI_REMAINDER_1", 0x000002F4, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_avg_roi_remainder_2
  { "roi_2_y_avg_remainder", 0x000002F8, 23,  0,   CSR_RO, 0x00000000 },
  { "Y_AVG_ROI_REMAINDER_2", 0x000002F8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_avg_roi_remainder_3
  { "roi_3_y_avg_remainder", 0x000002FC, 23,  0,   CSR_RO, 0x00000000 },
  { "Y_AVG_ROI_REMAINDER_3", 0x000002FC, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_avg_roi_pix_avg_0
  { "roi_0_y_avg_avg", 0x00000300, 13,  0,   CSR_RO, 0x00000000 },
  { "roi_1_y_avg_avg", 0x00000300, 29, 16,   CSR_RO, 0x00000000 },
  { "Y_AVG_ROI_PIX_AVG_0", 0x00000300, 31, 0,   CSR_RO, 0x00000000 },
  // WORD y_avg_roi_pix_avg_1
  { "roi_2_y_avg_avg", 0x00000304, 13,  0,   CSR_RO, 0x00000000 },
  { "roi_3_y_avg_avg", 0x00000304, 29, 16,   CSR_RO, 0x00000000 },
  { "Y_AVG_ROI_PIX_AVG_1", 0x00000304, 31, 0,   CSR_RO, 0x00000000 },
  // WORD roi_contrast_enable
  { "roi_0_contrast_en", 0x00000308,  0,  0,   CSR_RW, 0x00000000 },
  { "roi_1_contrast_en", 0x00000308,  8,  8,   CSR_RW, 0x00000000 },
  { "roi_2_contrast_en", 0x00000308, 16, 16,   CSR_RW, 0x00000000 },
  { "roi_3_contrast_en", 0x00000308, 24, 24,   CSR_RW, 0x00000000 },
  { "ROI_CONTRAST_ENABLE", 0x00000308, 31, 0,   CSR_RW, 0x00000000 },
  // WORD contrast_roi_0_x
  { "roi_0_contrast_sx", 0x0000030C, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_0_contrast_ex", 0x0000030C, 31, 16,   CSR_RW, 0x00000780 },
  { "CONTRAST_ROI_0_X", 0x0000030C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD contrast_roi_0_y
  { "roi_0_contrast_sy", 0x00000310, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_0_contrast_ey", 0x00000310, 31, 16,   CSR_RW, 0x00000438 },
  { "CONTRAST_ROI_0_Y", 0x00000310, 31, 0,   CSR_RW, 0x00000000 },
  // WORD contrast_roi_1_x
  { "roi_1_contrast_sx", 0x00000314, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_1_contrast_ex", 0x00000314, 31, 16,   CSR_RW, 0x00000780 },
  { "CONTRAST_ROI_1_X", 0x00000314, 31, 0,   CSR_RW, 0x00000000 },
  // WORD contrast_roi_1_y
  { "roi_1_contrast_sy", 0x00000318, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_1_contrast_ey", 0x00000318, 31, 16,   CSR_RW, 0x00000438 },
  { "CONTRAST_ROI_1_Y", 0x00000318, 31, 0,   CSR_RW, 0x00000000 },
  // WORD contrast_roi_2_x
  { "roi_2_contrast_sx", 0x0000031C, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_2_contrast_ex", 0x0000031C, 31, 16,   CSR_RW, 0x00000780 },
  { "CONTRAST_ROI_2_X", 0x0000031C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD contrast_roi_2_y
  { "roi_2_contrast_sy", 0x00000320, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_2_contrast_ey", 0x00000320, 31, 16,   CSR_RW, 0x00000438 },
  { "CONTRAST_ROI_2_Y", 0x00000320, 31, 0,   CSR_RW, 0x00000000 },
  // WORD contrast_roi_3_x
  { "roi_3_contrast_sx", 0x00000324, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_3_contrast_ex", 0x00000324, 31, 16,   CSR_RW, 0x00000780 },
  { "CONTRAST_ROI_3_X", 0x00000324, 31, 0,   CSR_RW, 0x00000000 },
  // WORD contrast_roi_3_y
  { "roi_3_contrast_sy", 0x00000328, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_3_contrast_ey", 0x00000328, 31, 16,   CSR_RW, 0x00000438 },
  { "CONTRAST_ROI_3_Y", 0x00000328, 31, 0,   CSR_RW, 0x00000000 },
  // WORD contrast_pix_num_0
  { "roi_0_contrast_pix_num", 0x0000032C, 23,  0,   CSR_RW, 0x00000000 },
  { "CONTRAST_PIX_NUM_0", 0x0000032C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD contrast_pix_num_1
  { "roi_1_contrast_pix_num", 0x00000330, 23,  0,   CSR_RW, 0x00000000 },
  { "CONTRAST_PIX_NUM_1", 0x00000330, 31, 0,   CSR_RW, 0x00000000 },
  // WORD contrast_pix_num_2
  { "roi_2_contrast_pix_num", 0x00000334, 23,  0,   CSR_RW, 0x00000000 },
  { "CONTRAST_PIX_NUM_2", 0x00000334, 31, 0,   CSR_RW, 0x00000000 },
  // WORD contrast_pix_num_3
  { "roi_3_contrast_pix_num", 0x00000338, 23,  0,   CSR_RW, 0x00000000 },
  { "CONTRAST_PIX_NUM_3", 0x00000338, 31, 0,   CSR_RW, 0x00000000 },
  // WORD contrast_h1_avg_0
  { "roi_0_contrast_h1_avg", 0x0000033C, 13,  0,   CSR_RO, 0x00000000 },
  { "roi_1_contrast_h1_avg", 0x0000033C, 29, 16,   CSR_RO, 0x00000000 },
  { "CONTRAST_H1_AVG_0", 0x0000033C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD contrast_h1_avg_1
  { "roi_2_contrast_h1_avg", 0x00000340, 13,  0,   CSR_RO, 0x00000000 },
  { "roi_3_contrast_h1_avg", 0x00000340, 29, 16,   CSR_RO, 0x00000000 },
  { "CONTRAST_H1_AVG_1", 0x00000340, 31, 0,   CSR_RO, 0x00000000 },
  // WORD contrast_h2_avg_0
  { "roi_0_contrast_h2_avg", 0x00000344, 13,  0,   CSR_RO, 0x00000000 },
  { "roi_1_contrast_h2_avg", 0x00000344, 29, 16,   CSR_RO, 0x00000000 },
  { "CONTRAST_H2_AVG_0", 0x00000344, 31, 0,   CSR_RO, 0x00000000 },
  // WORD contrast_h2_avg_1
  { "roi_2_contrast_h2_avg", 0x00000348, 13,  0,   CSR_RO, 0x00000000 },
  { "roi_3_contrast_h2_avg", 0x00000348, 29, 16,   CSR_RO, 0x00000000 },
  { "CONTRAST_H2_AVG_1", 0x00000348, 31, 0,   CSR_RO, 0x00000000 },
  // WORD contrast_v1_avg_0
  { "roi_0_contrast_v1_avg", 0x0000034C, 13,  0,   CSR_RO, 0x00000000 },
  { "roi_1_contrast_v1_avg", 0x0000034C, 29, 16,   CSR_RO, 0x00000000 },
  { "CONTRAST_V1_AVG_0", 0x0000034C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD contrast_v1_avg_1
  { "roi_2_contrast_v1_avg", 0x00000350, 13,  0,   CSR_RO, 0x00000000 },
  { "roi_3_contrast_v1_avg", 0x00000350, 29, 16,   CSR_RO, 0x00000000 },
  { "CONTRAST_V1_AVG_1", 0x00000350, 31, 0,   CSR_RO, 0x00000000 },
  // WORD contrast_v2_avg_0
  { "roi_0_contrast_v2_avg", 0x00000354, 13,  0,   CSR_RO, 0x00000000 },
  { "roi_1_contrast_v2_avg", 0x00000354, 29, 16,   CSR_RO, 0x00000000 },
  { "CONTRAST_V2_AVG_0", 0x00000354, 31, 0,   CSR_RO, 0x00000000 },
  // WORD contrast_v2_avg_1
  { "roi_2_contrast_v2_avg", 0x00000358, 13,  0,   CSR_RO, 0x00000000 },
  { "roi_3_contrast_v2_avg", 0x00000358, 29, 16,   CSR_RO, 0x00000000 },
  { "CONTRAST_V2_AVG_1", 0x00000358, 31, 0,   CSR_RO, 0x00000000 },
  // WORD roi_awb_gwd_enable
  { "roi_0_awb_gwd_en", 0x0000035C,  0,  0,   CSR_RW, 0x00000000 },
  { "roi_1_awb_gwd_en", 0x0000035C,  8,  8,   CSR_RW, 0x00000000 },
  { "roi_2_awb_gwd_en", 0x0000035C, 16, 16,   CSR_RW, 0x00000000 },
  { "roi_3_awb_gwd_en", 0x0000035C, 24, 24,   CSR_RW, 0x00000000 },
  { "ROI_AWB_GWD_ENABLE", 0x0000035C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_gwd_roi_0_x
  { "roi_0_awb_gwd_sx", 0x00000360, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_0_awb_gwd_ex", 0x00000360, 31, 16,   CSR_RW, 0x00000780 },
  { "AWB_GWD_ROI_0_X", 0x00000360, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_gwd_roi_0_y
  { "roi_0_awb_gwd_sy", 0x00000364, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_0_awb_gwd_ey", 0x00000364, 31, 16,   CSR_RW, 0x00000438 },
  { "AWB_GWD_ROI_0_Y", 0x00000364, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_gwd_roi_1_x
  { "roi_1_awb_gwd_sx", 0x00000368, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_1_awb_gwd_ex", 0x00000368, 31, 16,   CSR_RW, 0x00000780 },
  { "AWB_GWD_ROI_1_X", 0x00000368, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_gwd_roi_1_y
  { "roi_1_awb_gwd_sy", 0x0000036C, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_1_awb_gwd_ey", 0x0000036C, 31, 16,   CSR_RW, 0x00000438 },
  { "AWB_GWD_ROI_1_Y", 0x0000036C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_gwd_roi_2_x
  { "roi_2_awb_gwd_sx", 0x00000370, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_2_awb_gwd_ex", 0x00000370, 31, 16,   CSR_RW, 0x00000780 },
  { "AWB_GWD_ROI_2_X", 0x00000370, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_gwd_roi_2_y
  { "roi_2_awb_gwd_sy", 0x00000374, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_2_awb_gwd_ey", 0x00000374, 31, 16,   CSR_RW, 0x00000438 },
  { "AWB_GWD_ROI_2_Y", 0x00000374, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_gwd_roi_3_x
  { "roi_3_awb_gwd_sx", 0x00000378, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_3_awb_gwd_ex", 0x00000378, 31, 16,   CSR_RW, 0x00000780 },
  { "AWB_GWD_ROI_3_X", 0x00000378, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_gwd_roi_3_y
  { "roi_3_awb_gwd_sy", 0x0000037C, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_3_awb_gwd_ey", 0x0000037C, 31, 16,   CSR_RW, 0x00000438 },
  { "AWB_GWD_ROI_3_Y", 0x0000037C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_gwd_sum_g0_0
  { "roi_0_awb_gwd_sum_g0", 0x00000380, 31,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_SUM_G0_0", 0x00000380, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_sum_g0_1
  { "roi_1_awb_gwd_sum_g0", 0x00000384, 31,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_SUM_G0_1", 0x00000384, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_sum_g0_2
  { "roi_2_awb_gwd_sum_g0", 0x00000388, 31,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_SUM_G0_2", 0x00000388, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_sum_g0_3
  { "roi_3_awb_gwd_sum_g0", 0x0000038C, 31,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_SUM_G0_3", 0x0000038C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_sum_r_0
  { "roi_0_awb_gwd_sum_r", 0x00000390, 31,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_SUM_R_0", 0x00000390, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_sum_r_1
  { "roi_1_awb_gwd_sum_r", 0x00000394, 31,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_SUM_R_1", 0x00000394, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_sum_r_2
  { "roi_2_awb_gwd_sum_r", 0x00000398, 31,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_SUM_R_2", 0x00000398, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_sum_r_3
  { "roi_3_awb_gwd_sum_r", 0x0000039C, 31,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_SUM_R_3", 0x0000039C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_sum_b_0
  { "roi_0_awb_gwd_sum_b", 0x000003A0, 31,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_SUM_B_0", 0x000003A0, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_sum_b_1
  { "roi_1_awb_gwd_sum_b", 0x000003A4, 31,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_SUM_B_1", 0x000003A4, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_sum_b_2
  { "roi_2_awb_gwd_sum_b", 0x000003A8, 31,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_SUM_B_2", 0x000003A8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_sum_b_3
  { "roi_3_awb_gwd_sum_b", 0x000003AC, 31,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_SUM_B_3", 0x000003AC, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_sum_g1_0
  { "roi_0_awb_gwd_sum_g1", 0x000003B0, 31,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_SUM_G1_0", 0x000003B0, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_sum_g1_1
  { "roi_1_awb_gwd_sum_g1", 0x000003B4, 31,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_SUM_G1_1", 0x000003B4, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_sum_g1_2
  { "roi_2_awb_gwd_sum_g1", 0x000003B8, 31,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_SUM_G1_2", 0x000003B8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_sum_g1_3
  { "roi_3_awb_gwd_sum_g1", 0x000003BC, 31,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_SUM_G1_3", 0x000003BC, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_norm_g0_0
  { "roi_0_awb_gwd_norm_g0", 0x000003C0, 26,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_NORM_G0_0", 0x000003C0, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_norm_g0_1
  { "roi_1_awb_gwd_norm_g0", 0x000003C4, 26,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_NORM_G0_1", 0x000003C4, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_norm_g0_2
  { "roi_2_awb_gwd_norm_g0", 0x000003C8, 26,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_NORM_G0_2", 0x000003C8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_norm_g0_3
  { "roi_3_awb_gwd_norm_g0", 0x000003CC, 26,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_NORM_G0_3", 0x000003CC, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_norm_r_0
  { "roi_0_awb_gwd_norm_r", 0x000003D0, 26,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_NORM_R_0", 0x000003D0, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_norm_r_1
  { "roi_1_awb_gwd_norm_r", 0x000003D4, 26,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_NORM_R_1", 0x000003D4, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_norm_r_2
  { "roi_2_awb_gwd_norm_r", 0x000003D8, 26,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_NORM_R_2", 0x000003D8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_norm_r_3
  { "roi_3_awb_gwd_norm_r", 0x000003DC, 26,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_NORM_R_3", 0x000003DC, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_norm_b_0
  { "roi_0_awb_gwd_norm_b", 0x000003E0, 26,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_NORM_B_0", 0x000003E0, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_norm_b_1
  { "roi_1_awb_gwd_norm_b", 0x000003E4, 26,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_NORM_B_1", 0x000003E4, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_norm_b_2
  { "roi_2_awb_gwd_norm_b", 0x000003E8, 26,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_NORM_B_2", 0x000003E8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_norm_b_3
  { "roi_3_awb_gwd_norm_b", 0x000003EC, 26,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_NORM_B_3", 0x000003EC, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_norm_g1_0
  { "roi_0_awb_gwd_norm_g1", 0x000003F0, 26,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_NORM_G1_0", 0x000003F0, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_norm_g1_1
  { "roi_1_awb_gwd_norm_g1", 0x000003F4, 26,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_NORM_G1_1", 0x000003F4, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_norm_g1_2
  { "roi_2_awb_gwd_norm_g1", 0x000003F8, 26,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_NORM_G1_2", 0x000003F8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_gwd_norm_g1_3
  { "roi_3_awb_gwd_norm_g1", 0x000003FC, 26,  0,   CSR_RO, 0x00000000 },
  { "AWB_GWD_NORM_G1_3", 0x000003FC, 31, 0,   CSR_RO, 0x00000000 },
  // WORD rgl_y_avg_mode
  { "rgl_y_avg_en", 0x00000400,  0,  0,   CSR_RW, 0x00000000 },
  { "rgl_y_avg_in_mode", 0x00000400, 17, 16,   CSR_RW, 0x00000000 },
  { "RGL_Y_AVG_MODE", 0x00000400, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rgl_y_avg_blk
  { "rgl_y_avg_blk_ht", 0x00000404, 13,  0,   CSR_RW, 0x0000005A },
  { "rgl_y_avg_blk_wd", 0x00000404, 29, 16,   CSR_RW, 0x00000078 },
  { "RGL_Y_AVG_BLK", 0x00000404, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_rgl_y_avg_pix_num
  { "rgl_y_avg_pix_num", 0x00000408, 17,  0,   CSR_RW, 0x00000000 },
  { "WORD_RGL_Y_AVG_PIX_NUM", 0x00000408, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rgl_y_avg_x
  { "rgl_y_avg_sx", 0x0000040C, 15,  0,   CSR_RW, 0x00000000 },
  { "rgl_y_avg_ex", 0x0000040C, 31, 16,   CSR_RW, 0x00000780 },
  { "RGL_Y_AVG_X", 0x0000040C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rgl_y_avg_y
  { "rgl_y_avg_sy", 0x00000410, 15,  0,   CSR_RW, 0x00000000 },
  { "rgl_y_avg_ey", 0x00000410, 31, 16,   CSR_RW, 0x00000438 },
  { "RGL_Y_AVG_Y", 0x00000410, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rgl_contrast_mode
  { "rgl_contrast_en", 0x00000414,  0,  0,   CSR_RW, 0x00000000 },
  { "RGL_CONTRAST_MODE", 0x00000414, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rgl_contrast_blk
  { "rgl_contrast_blk_ht", 0x00000418, 13,  0,   CSR_RW, 0x0000005A },
  { "rgl_contrast_blk_wd", 0x00000418, 29, 16,   CSR_RW, 0x00000078 },
  { "RGL_CONTRAST_BLK", 0x00000418, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_rgl_contrast_pix_num
  { "rgl_contrast_pix_num", 0x0000041C, 17,  0,   CSR_RW, 0x00000000 },
  { "WORD_RGL_CONTRAST_PIX_NUM", 0x0000041C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rgl_contrast_x
  { "rgl_contrast_sx", 0x00000420, 15,  0,   CSR_RW, 0x00000000 },
  { "rgl_contrast_ex", 0x00000420, 31, 16,   CSR_RW, 0x00000780 },
  { "RGL_CONTRAST_X", 0x00000420, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rgl_contrast_y
  { "rgl_contrast_sy", 0x00000424, 15,  0,   CSR_RW, 0x00000000 },
  { "rgl_contrast_ey", 0x00000424, 31, 16,   CSR_RW, 0x00000438 },
  { "RGL_CONTRAST_Y", 0x00000424, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rgl_awb_rtx_mode
  { "rgl_awb_rtx_en", 0x00000428,  0,  0,   CSR_RW, 0x00000000 },
  { "RGL_AWB_RTX_MODE", 0x00000428, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rgl_awb_rtx_blk
  { "rgl_awb_rtx_blk_ht", 0x0000042C, 13,  0,   CSR_RW, 0x0000005A },
  { "rgl_awb_rtx_blk_wd", 0x0000042C, 29, 16,   CSR_RW, 0x00000078 },
  { "RGL_AWB_RTX_BLK", 0x0000042C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rgl_awb_rtx_x
  { "rgl_awb_rtx_sx", 0x00000430, 15,  0,   CSR_RW, 0x00000000 },
  { "rgl_awb_rtx_ex", 0x00000430, 31, 16,   CSR_RW, 0x00000780 },
  { "RGL_AWB_RTX_X", 0x00000430, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rgl_awb_rtx_y
  { "rgl_awb_rtx_sy", 0x00000434, 15,  0,   CSR_RW, 0x00000000 },
  { "rgl_awb_rtx_ey", 0x00000434, 31, 16,   CSR_RW, 0x00000438 },
  { "RGL_AWB_RTX_Y", 0x00000434, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rgl_awb_gwd_mode
  { "rgl_awb_gwd_en", 0x00000438,  0,  0,   CSR_RW, 0x00000000 },
  { "RGL_AWB_GWD_MODE", 0x00000438, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rgl_awb_gwd_blk
  { "rgl_awb_gwd_blk_ht", 0x0000043C, 13,  0,   CSR_RW, 0x0000005A },
  { "rgl_awb_gwd_blk_wd", 0x0000043C, 29, 16,   CSR_RW, 0x00000078 },
  { "RGL_AWB_GWD_BLK", 0x0000043C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rgl_awb_gwd_x
  { "rgl_awb_gwd_sx", 0x00000440, 15,  0,   CSR_RW, 0x00000000 },
  { "rgl_awb_gwd_ex", 0x00000440, 31, 16,   CSR_RW, 0x00000780 },
  { "RGL_AWB_GWD_X", 0x00000440, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rgl_awb_gwd_y
  { "rgl_awb_gwd_sy", 0x00000444, 15,  0,   CSR_RW, 0x00000000 },
  { "rgl_awb_gwd_ey", 0x00000444, 31, 16,   CSR_RW, 0x00000438 },
  { "RGL_AWB_GWD_Y", 0x00000444, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_csr2sram_sel
  { "csr2sram_sel", 0x00000448,  0,  0,   CSR_RW, 0x00000000 },
  { "WORD_CSR2SRAM_SEL", 0x00000448, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ae_rgl_addr
  { "ae_rgl_y_avg_r_addr", 0x0000044C,  5,  0,   CSR_RW, 0x00000000 },
  { "AE_RGL_ADDR", 0x0000044C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD af_rgl_addr
  { "af_rgl_h1h2v1v2_avg_r_addr", 0x00000450,  5,  0,   CSR_RW, 0x00000000 },
  { "AF_RGL_ADDR", 0x00000450, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_rgl_gwd_addr
  { "awb_rgl_gwd_pix_avg_r_addr", 0x00000454,  5,  0,   CSR_RW, 0x00000000 },
  { "AWB_RGL_GWD_ADDR", 0x00000454, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_rgl_rtx_addr
  { "awb_rgl_rtx_max_weight_r_addr", 0x00000458,  6,  0,   CSR_RW, 0x00000000 },
  { "AWB_RGL_RTX_ADDR", 0x00000458, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_rtx_sorting_addr
  { "awb_rtx_sorting_r_addr", 0x0000045C,  5,  0,   CSR_RW, 0x00000000 },
  { "AWB_RTX_SORTING_ADDR", 0x0000045C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ae_rgl_r_data
  { "ae_rgl_y_avg_r_data", 0x00000460,  7,  0,   CSR_RO, 0x00000000 },
  { "AE_RGL_R_DATA", 0x00000460, 31, 0,   CSR_RO, 0x00000000 },
  // WORD af_rgl_r_data
  { "af_rgl_h1h2v1v2_avg_r_data", 0x00000464, 13,  0,   CSR_RO, 0x00000000 },
  { "AF_RGL_R_DATA", 0x00000464, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_rgl_gwd_r_data
  { "awb_rgl_gwd_pix_avg_r_data", 0x00000468,  7,  0,   CSR_RO, 0x00000000 },
  { "AWB_RGL_GWD_R_DATA", 0x00000468, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_rgl_rtx_r_data
  { "awb_rgl_rtx_max_value_weight_r_data", 0x0000046C, 14,  0,   CSR_RO, 0x00000000 },
  { "AWB_RGL_RTX_R_DATA", 0x0000046C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD awb_rtx_sorting_data
  { "awb_rtx_sorting_r_data", 0x00000470, 16,  0,   CSR_RO, 0x00000000 },
  { "AWB_RTX_SORTING_DATA", 0x00000470, 31, 0,   CSR_RO, 0x00000000 },
  // WORD cfa_phase_en_set_0
  { "cfa_phase_en_0", 0x00000474,  0,  0,   CSR_RW, 0x00000001 },
  { "cfa_phase_en_1", 0x00000474,  8,  8,   CSR_RW, 0x00000001 },
  { "cfa_phase_en_2", 0x00000474, 16, 16,   CSR_RW, 0x00000001 },
  { "cfa_phase_en_3", 0x00000474, 24, 24,   CSR_RW, 0x00000001 },
  { "CFA_PHASE_EN_SET_0", 0x00000474, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cfa_phase_en_set_1
  { "cfa_phase_en_4", 0x00000478,  0,  0,   CSR_RW, 0x00000001 },
  { "cfa_phase_en_5", 0x00000478,  8,  8,   CSR_RW, 0x00000001 },
  { "cfa_phase_en_6", 0x00000478, 16, 16,   CSR_RW, 0x00000001 },
  { "cfa_phase_en_7", 0x00000478, 24, 24,   CSR_RW, 0x00000001 },
  { "CFA_PHASE_EN_SET_1", 0x00000478, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cfa_phase_en_set_2
  { "cfa_phase_en_8", 0x0000047C,  0,  0,   CSR_RW, 0x00000001 },
  { "cfa_phase_en_9", 0x0000047C,  8,  8,   CSR_RW, 0x00000001 },
  { "cfa_phase_en_10", 0x0000047C, 16, 16,   CSR_RW, 0x00000001 },
  { "cfa_phase_en_11", 0x0000047C, 24, 24,   CSR_RW, 0x00000001 },
  { "CFA_PHASE_EN_SET_2", 0x0000047C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cfa_phase_en_set_3
  { "cfa_phase_en_12", 0x00000480,  0,  0,   CSR_RW, 0x00000001 },
  { "cfa_phase_en_13", 0x00000480,  8,  8,   CSR_RW, 0x00000001 },
  { "cfa_phase_en_14", 0x00000480, 16, 16,   CSR_RW, 0x00000001 },
  { "cfa_phase_en_15", 0x00000480, 24, 24,   CSR_RW, 0x00000001 },
  { "CFA_PHASE_EN_SET_3", 0x00000480, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dbg_sel
  { "debug_mon_sel", 0x00000484,  1,  0,   CSR_RW, 0x00000000 },
  { "DBG_SEL", 0x00000484, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_atpg_ctrl
  { "atpg_ctrl", 0x00000488,  3,  0,   CSR_RW, 0x00000000 },
  { "WORD_ATPG_CTRL", 0x00000488, 31, 0,   CSR_RW, 0x00000000 },
  // WORD efuse_status
  { "efuse_sensor_cfa_violation", 0x0000048C,  0,  0,   CSR_RO, 0x00000000 },
  { "EFUSE_STATUS", 0x0000048C, 31, 0,   CSR_RO, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_BSP_H_
