#ifndef CSR_TABLE_WDT_AON_H_
#define CSR_TABLE_WDT_AON_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_wdt_aon[] =
{
  // WORD wdt_aon00
  { "dis_wdt", 0x00000000,  0,  0,   CSR_RW, 0x00000000 },
  { "WDT_AON00", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wdt_aon01
  { "prescaler_th", 0x00000004, 15,  0,   CSR_RW, 0x00000018 },
  { "WDT_AON01", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wdt_aon02
  { "cnt_th_0", 0x00000008, 15,  0,   CSR_RW, 0x00000E10 },
  { "WDT_AON02", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wdt_aon03
  { "cnt_th_1", 0x0000000C, 15,  0,   CSR_RW, 0x000011F8 },
  { "WDT_AON03", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wdt_aon04
  { "cnt_th_2", 0x00000010, 31, 16,   CSR_RW, 0x00000E10 },
  { "WDT_AON04", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wdt_aon05
  { "cnt", 0x00000014, 15,  0,   CSR_RO, 0x00000000 },
  { "WDT_AON05", 0x00000014, 31, 0,   CSR_RO, 0x00000000 },
  // WORD wdt_aon06
  { "stage", 0x00000018,  1,  0,   CSR_RO, 0x00000000 },
  { "WDT_AON06", 0x00000018, 31, 0,   CSR_RO, 0x00000000 },
  // WORD wdt_aon07
  { "polarity", 0x0000001C,  0,  0,   CSR_RW, 0x00000001 },
  { "WDT_AON07", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wdt_aon08
  { "reserved", 0x00000020, 31,  0,   CSR_RW, 0x00000000 },
  { "WDT_AON08", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_WDT_AON_H_
