#ifndef CSR_TABLE_REFR_H_
#define CSR_TABLE_REFR_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_refr[] =
{
  // WORD rr_00
  { "frame_start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "RR_00", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rr_01
  { "access_illegal_hang", 0x00000004,  0,  0,   CSR_RW, 0x00000001 },
  { "access_illegal_mask", 0x00000004,  8,  8,   CSR_RW, 0x00000001 },
  { "RR_01", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rr_04
  { "col_addr_type", 0x00000010,  9,  8,   CSR_RW, 0x00000000 },
  { "RR_04", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rr_07
  { "target_burst_len", 0x0000001C,  4,  0,   CSR_RW, 0x00000010 },
  { "access_end_sel", 0x0000001C,  8,  8,   CSR_RW, 0x00000001 },
  { "debug_mon_sel", 0x0000001C, 24, 24,   CSR_RW, 0x00000000 },
  { "RR_07", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rr_08
  { "target_fifo_level", 0x00000020,  8,  0,   CSR_RW, 0x00000020 },
  { "fifo_full_level", 0x00000020, 24, 16,   CSR_RW, 0x00000100 },
  { "RR_08", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rr_09
  { "height", 0x00000024, 15,  0,   CSR_RW, 0x00000000 },
  { "width", 0x00000024, 31, 16,   CSR_RW, 0x00000000 },
  { "RR_09", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rr_12
  { "start_addr", 0x00000030, 27,  0,   CSR_RW, 0x00000000 },
  { "RR_12", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rr_13
  { "end_addr", 0x00000034, 27,  0,   CSR_RW, 0x0FFFFFFF },
  { "RR_13", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rr_15
  { "reserved", 0x0000003C, 31,  0,   CSR_RW, 0x00000000 },
  { "RR_15", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rr_16
  { "bank_interleave_type", 0x00000040,  1,  0,   CSR_RW, 0x00000002 },
  { "bank_group_type", 0x00000040,  9,  8,   CSR_RW, 0x00000001 },
  { "RR_16", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rr_17
  { "ini_addr_bank_offset", 0x00000044,  2,  0,   CSR_RW, 0x00000000 },
  { "RR_17", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rr_18
  { "ini_addr_linear_y", 0x00000048, 27,  0,   CSR_RW, 0x00000000 },
  { "RR_18", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rr_19
  { "ini_addr_linear_c", 0x0000004C, 27,  0,   CSR_RW, 0x00000000 },
  { "RR_19", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_REFR_H_
