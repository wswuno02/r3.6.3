#ifndef CSR_TABLE_PDI_H_
#define CSR_TABLE_PDI_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_pdi[] =
{
  // WORD pdi00
  { "start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "stop", 0x00000000,  8,  8,  CSR_W1P, 0x00000000 },
  { "PDI00", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD pdi01
  { "irq_clear_frame_end", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "irq_clear_insufficient", 0x00000004,  8,  8,  CSR_W1P, 0x00000000 },
  { "irq_clear_drop_frame", 0x00000004, 16, 16,  CSR_W1P, 0x00000000 },
  { "irq_clear_seq_end", 0x00000004, 24, 24,  CSR_W1P, 0x00000000 },
  { "PDI01", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD pdi02
  { "status_frame_end", 0x00000008,  0,  0,   CSR_RO, 0x00000000 },
  { "status_insufficient", 0x00000008,  8,  8,   CSR_RO, 0x00000000 },
  { "status_drop_frame", 0x00000008, 16, 16,   CSR_RO, 0x00000000 },
  { "status_seq_end", 0x00000008, 24, 24,   CSR_RO, 0x00000000 },
  { "PDI02", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // WORD pdi03
  { "irq_mask_frame_end", 0x0000000C,  0,  0,   CSR_RW, 0x00000001 },
  { "irq_mask_insufficient", 0x0000000C,  8,  8,   CSR_RW, 0x00000001 },
  { "irq_mask_drop_frame", 0x0000000C, 16, 16,   CSR_RW, 0x00000001 },
  { "irq_mask_seq_end", 0x0000000C, 24, 24,   CSR_RW, 0x00000001 },
  { "PDI03", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi04
  { "frame_start_mode", 0x00000010,  0,  0,   CSR_RW, 0x00000000 },
  { "PDI04", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi05
  { "eav", 0x00000014, 15,  0,   CSR_RW, 0x00000004 },
  { "PDI05", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi06
  { "ln", 0x00000018, 15,  0,   CSR_RW, 0x00000000 },
  { "PDI06", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi07
  { "crc", 0x0000001C, 15,  0,   CSR_RW, 0x00000000 },
  { "PDI07", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi08
  { "blank_data", 0x00000020, 15,  0,   CSR_RW, 0x00000118 },
  { "PDI08", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi09
  { "sav", 0x00000024, 15,  0,   CSR_RW, 0x00000004 },
  { "PDI09", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi10
  { "h_back_porch", 0x00000028, 15,  0,   CSR_RW, 0x00000000 },
  { "PDI10", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi11
  { "active_data", 0x0000002C, 15,  0,   CSR_RW, 0x000005A0 },
  { "PDI11", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi12
  { "h_front_porch", 0x00000030, 15,  0,   CSR_RW, 0x00000000 },
  { "PDI12", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi13
  { "field0_vsync", 0x00000034, 11,  0,   CSR_RW, 0x00000000 },
  { "PDI13", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi14
  { "field0_blanking_top", 0x00000038, 11,  0,   CSR_RW, 0x00000016 },
  { "PDI14", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi15
  { "field0_active_data", 0x0000003C, 11,  0,   CSR_RW, 0x00000120 },
  { "PDI15", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi16
  { "field0_blanking_bottom", 0x00000040, 11,  0,   CSR_RW, 0x00000002 },
  { "PDI16", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi17
  { "field1_vsync", 0x00000044, 11,  0,   CSR_RW, 0x00000000 },
  { "PDI17", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi18
  { "field1_blanking_top", 0x00000048, 11,  0,   CSR_RW, 0x00000017 },
  { "PDI18", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi19
  { "field1_active_data", 0x0000004C, 11,  0,   CSR_RW, 0x00000120 },
  { "PDI19", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi20
  { "field1_blanking_bottom", 0x00000050, 11,  0,   CSR_RW, 0x00000002 },
  { "PDI20", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi21
  { "eav_user_mode", 0x00000054,  0,  0,   CSR_RW, 0x00000000 },
  { "PDI21", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi22
  { "eav_user_setting_0", 0x00000058,  9,  0,   CSR_RW, 0x00000000 },
  { "PDI22", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi23
  { "eav_user_setting_1", 0x0000005C,  9,  0,   CSR_RW, 0x00000000 },
  { "PDI23", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi24
  { "eav_user_setting_2", 0x00000060,  9,  0,   CSR_RW, 0x00000000 },
  { "PDI24", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi25
  { "eav_user_setting_3", 0x00000064,  9,  0,   CSR_RW, 0x00000000 },
  { "PDI25", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi26
  { "eav_user_setting_4", 0x00000068,  9,  0,   CSR_RW, 0x00000000 },
  { "PDI26", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi27
  { "eav_user_setting_5", 0x0000006C,  9,  0,   CSR_RW, 0x00000000 },
  { "PDI27", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi28
  { "eav_user_setting_6", 0x00000070,  9,  0,   CSR_RW, 0x00000000 },
  { "PDI28", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi29
  { "eav_user_setting_7", 0x00000074,  9,  0,   CSR_RW, 0x00000000 },
  { "PDI29", 0x00000074, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi30
  { "blank_data_mode", 0x00000078,  1,  0,   CSR_RW, 0x00000000 },
  { "PDI30", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi31
  { "ln_height", 0x0000007C, 11,  0,   CSR_RW, 0x00000438 },
  { "PDI31", 0x0000007C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi32
  { "crc_empty_mode", 0x00000080,  1,  0,   CSR_RW, 0x00000000 },
  { "PDI32", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi33
  { "sav_user_mode", 0x00000084,  0,  0,   CSR_RW, 0x00000000 },
  { "PDI33", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi34
  { "sav_user_setting_0", 0x00000088,  9,  0,   CSR_RW, 0x00000000 },
  { "PDI34", 0x00000088, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi35
  { "sav_user_setting_1", 0x0000008C,  9,  0,   CSR_RW, 0x00000000 },
  { "PDI35", 0x0000008C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi36
  { "sav_user_setting_2", 0x00000090,  9,  0,   CSR_RW, 0x00000000 },
  { "PDI36", 0x00000090, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi37
  { "sav_user_setting_3", 0x00000094,  9,  0,   CSR_RW, 0x00000000 },
  { "PDI37", 0x00000094, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi38
  { "sav_user_setting_4", 0x00000098,  9,  0,   CSR_RW, 0x00000000 },
  { "PDI38", 0x00000098, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi39
  { "sav_user_setting_5", 0x0000009C,  9,  0,   CSR_RW, 0x00000000 },
  { "PDI39", 0x0000009C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi40
  { "sav_user_setting_6", 0x000000A0,  9,  0,   CSR_RW, 0x00000000 },
  { "PDI40", 0x000000A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi41
  { "sav_user_setting_7", 0x000000A4,  9,  0,   CSR_RW, 0x00000000 },
  { "PDI41", 0x000000A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi42
  { "hbp_mode", 0x000000A8,  1,  0,   CSR_RW, 0x00000000 },
  { "PDI42", 0x000000A8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi43
  { "empty_data_mode", 0x000000AC,  1,  0,   CSR_RW, 0x00000000 },
  { "PDI43", 0x000000AC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi44
  { "hfp_mode", 0x000000B0,  1,  0,   CSR_RW, 0x00000000 },
  { "PDI44", 0x000000B0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi45
  { "active_data_rab_uppacker", 0x000000B4,  0,  0,   CSR_RW, 0x00000001 },
  { "PDI45", 0x000000B4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi46
  { "active_data_rab_bypass", 0x000000B8,  0,  0,   CSR_RW, 0x00000000 },
  { "PDI46", 0x000000B8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi47
  { "active_data_ram_active", 0x000000BC,  0,  0,   CSR_RW, 0x00000001 },
  { "PDI47", 0x000000BC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi48
  { "data_format_sel", 0x000000C0,  2,  0,   CSR_RW, 0x00000000 },
  { "rgb_bitwidth_sel", 0x000000C0,  9,  8,   CSR_RW, 0x00000000 },
  { "mcu_format_en", 0x000000C0, 16, 16,   CSR_RW, 0x00000000 },
  { "PDI48", 0x000000C0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi49
  { "vsync_set", 0x000000C4,  8,  0,   CSR_RW, 0x00000011 },
  { "PDI49", 0x000000C4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi50
  { "hsync_set", 0x000000C8,  8,  0,   CSR_RW, 0x00000051 },
  { "PDI50", 0x000000C8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi51
  { "de_set", 0x000000CC, 10,  0,   CSR_RW, 0x00000040 },
  { "wrx_set", 0x000000CC, 16, 16,   CSR_RW, 0x00000000 },
  { "PDI51", 0x000000CC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi52
  { "mode", 0x000000D0,  1,  0,   CSR_RW, 0x00000000 },
  { "PDI52", 0x000000D0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi53
  { "dummy_th", 0x000000D4, 27,  0,   CSR_RW, 0x00004B00 },
  { "PDI53", 0x000000D4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi54
  { "wxh", 0x000000D8, 27,  0,   CSR_RW, 0x001FA400 },
  { "PDI54", 0x000000D8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi55
  { "width", 0x000000DC, 15,  0,   CSR_RW, 0x00000780 },
  { "PDI55", 0x000000DC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi56
  { "height", 0x000000E0, 11,  0,   CSR_RW, 0x00000438 },
  { "PDI56", 0x000000E0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi57
  { "value_0_i", 0x000000E4,  9,  0,   CSR_RW, 0x00000000 },
  { "PDI57", 0x000000E4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi58
  { "value_1_i", 0x000000E8,  9,  0,   CSR_RW, 0x00000000 },
  { "PDI58", 0x000000E8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi59
  { "value_2_i", 0x000000EC,  9,  0,   CSR_RW, 0x00000000 },
  { "PDI59", 0x000000EC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi60
  { "reg_pd_reset", 0x000000F0,  0,  0,   CSR_RW, 0x00000000 },
  { "reg_sd", 0x000000F0,  8,  8,   CSR_RW, 0x00000000 },
  { "reg_cm", 0x000000F0, 16, 16,   CSR_RW, 0x00000000 },
  { "PDI60", 0x000000F0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi61
  { "is_one_frame", 0x000000F4,  0,  0,   CSR_RW, 0x00000000 },
  { "PDI61", 0x000000F4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi62
  { "frame_cnt", 0x000000F8, 15,  0,   CSR_RO, 0x00000000 },
  { "PDI62", 0x000000F8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD pdi63
  { "frame_cnt_clear", 0x000000FC,  0,  0,  CSR_W1P, 0x00000000 },
  { "PDI63", 0x000000FC, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD pdi64
  { "csr_wrx_i", 0x00000100,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rdx_i", 0x00000100,  8,  8,  CSR_W1P, 0x00000000 },
  { "PDI64", 0x00000100, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD pdi65
  { "csr_csx_i", 0x00000104,  0,  0,   CSR_RW, 0x00000000 },
  { "csr_dcx_i", 0x00000104,  8,  8,   CSR_RW, 0x00000000 },
  { "PDI65", 0x00000104, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi66
  { "csr_data_i", 0x00000108, 17,  0,   CSR_RW, 0x00000000 },
  { "PDI66", 0x00000108, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi67
  { "mapping_sel", 0x0000010C,  3,  0,   CSR_RW, 0x00000000 },
  { "up_sampling", 0x0000010C,  9,  8,   CSR_RW, 0x00000000 },
  { "double_sampling", 0x0000010C, 16, 16,   CSR_RW, 0x00000000 },
  { "PDI67", 0x0000010C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi68
  { "csr_data_i_en", 0x00000110,  8,  8,   CSR_RW, 0x00000000 },
  { "PDI68", 0x00000110, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pdi69
  { "reserved", 0x00000114, 31,  0,   CSR_RW, 0x00000000 },
  { "PDI69", 0x00000114, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_PDI_H_
