#ifndef CSR_TABLE_GIC_H_
#define CSR_TABLE_GIC_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_gic[] =
{
  // WORD ip00
  { "ip", 0x00000000,  0,  0,   CSR_RW, 0x00000000 },
  { "IP00", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_GIC_H_
