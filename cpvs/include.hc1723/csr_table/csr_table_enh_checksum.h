#ifndef CSR_TABLE_ENH_CHECKSUM_H_
#define CSR_TABLE_ENH_CHECKSUM_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_enh_checksum[] =
{
  // WORD clr
  { "clear", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "CLR", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD enh_y
  { "checksum_enh_y", 0x00000004, 31,  0,   CSR_RO, 0x00000000 },
  { "ENH_Y", 0x00000004, 31, 0,   CSR_RO, 0x00000000 },
  // WORD shp_y
  { "checksum_shp_y", 0x00000008, 31,  0,   CSR_RO, 0x00000000 },
  { "SHP_Y", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // WORD subsample
  { "checksum_subsample", 0x0000000C, 31,  0,   CSR_RO, 0x00000000 },
  { "SUBSAMPLE", 0x0000000C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD enh_c
  { "checksum_enh_c", 0x00000010, 31,  0,   CSR_RO, 0x00000000 },
  { "ENH_C", 0x00000010, 31, 0,   CSR_RO, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_ENH_CHECKSUM_H_
