#ifndef CSR_TABLE_TX_OUT_H_
#define CSR_TABLE_TX_OUT_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_tx_out[] =
{
  // WORD main
  { "enable", 0x00000000,  0,  0,   CSR_RW, 0x00000000 },
  { "MAIN", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD irqsta
  { "status_fifo_err", 0x00000004,  0,  0,   CSR_RO, 0x00000000 },
  { "status_lpcd_err", 0x00000004,  1,  1,   CSR_RO, 0x00000000 },
  { "IRQSTA", 0x00000004, 31, 0,   CSR_RO, 0x00000000 },
  // WORD irqack
  { "irq_clear_fifo_err", 0x00000008,  0,  0,  CSR_W1P, 0x00000000 },
  { "irq_clear_lpcd_err", 0x00000008,  1,  1,  CSR_W1P, 0x00000000 },
  { "IRQACK", 0x00000008, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD irqmsk
  { "irq_mask_fifo_err", 0x0000000C,  0,  0,   CSR_RW, 0x00000000 },
  { "irq_mask_lpcd_err", 0x0000000C,  1,  1,   CSR_RW, 0x00000000 },
  { "IRQMSK", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hsck
  { "ck_pat", 0x00000010,  7,  0,   CSR_RW, 0x00000055 },
  { "HSCK", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ln0_ctrl
  { "ln0_en", 0x00000014,  0,  0,   CSR_RW, 0x00000000 },
  { "ln0_pn_sel", 0x00000014, 16, 16,   CSR_RW, 0x00000000 },
  { "LN0_CTRL", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ln1_ctrl
  { "ln1_en", 0x00000018,  0,  0,   CSR_RW, 0x00000000 },
  { "ln1_rx_sel", 0x00000018,  9,  8,   CSR_RW, 0x00000001 },
  { "ln1_pn_sel", 0x00000018, 16, 16,   CSR_RW, 0x00000000 },
  { "LN1_CTRL", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ln2_ctrl
  { "ln2_en", 0x0000001C,  0,  0,   CSR_RW, 0x00000000 },
  { "ln2_rx_sel", 0x0000001C,  9,  8,   CSR_RW, 0x00000000 },
  { "ln2_pn_sel", 0x0000001C, 16, 16,   CSR_RW, 0x00000000 },
  { "LN2_CTRL", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ln3_ctrl
  { "ln3_en", 0x00000020,  0,  0,   CSR_RW, 0x00000000 },
  { "ln3_rx_sel", 0x00000020,  9,  8,   CSR_RW, 0x00000002 },
  { "ln3_pn_sel", 0x00000020, 16, 16,   CSR_RW, 0x00000000 },
  { "LN3_CTRL", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ln4_ctrl
  { "ln4_en", 0x00000024,  0,  0,   CSR_RW, 0x00000000 },
  { "ln4_rx_sel", 0x00000024,  9,  8,   CSR_RW, 0x00000003 },
  { "ln4_pn_sel", 0x00000024, 16, 16,   CSR_RW, 0x00000000 },
  { "LN4_CTRL", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ln0_test
  { "ln0_test_en", 0x00000028,  1,  0,   CSR_RW, 0x00000000 },
  { "ln0_test_lp", 0x00000028, 11,  8,   CSR_RW, 0x00000000 },
  { "ln0_test_hs", 0x00000028, 23, 16,   CSR_RW, 0x00000000 },
  { "LN0_TEST", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ln1_test
  { "ln1_test_en", 0x0000002C,  1,  0,   CSR_RW, 0x00000000 },
  { "ln1_test_lp", 0x0000002C, 11,  8,   CSR_RW, 0x00000000 },
  { "ln1_test_hs", 0x0000002C, 23, 16,   CSR_RW, 0x00000000 },
  { "LN1_TEST", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ln2_test
  { "ln2_test_en", 0x00000030,  1,  0,   CSR_RW, 0x00000000 },
  { "ln2_test_lp", 0x00000030, 11,  8,   CSR_RW, 0x00000000 },
  { "ln2_test_hs", 0x00000030, 23, 16,   CSR_RW, 0x00000000 },
  { "LN2_TEST", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ln3_test
  { "ln3_test_en", 0x00000034,  1,  0,   CSR_RW, 0x00000000 },
  { "ln3_test_lp", 0x00000034, 11,  8,   CSR_RW, 0x00000000 },
  { "ln3_test_hs", 0x00000034, 23, 16,   CSR_RW, 0x00000000 },
  { "LN3_TEST", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ln4_test
  { "ln4_test_en", 0x00000038,  1,  0,   CSR_RW, 0x00000000 },
  { "ln4_test_lp", 0x00000038, 11,  8,   CSR_RW, 0x00000000 },
  { "ln4_test_hs", 0x00000038, 23, 16,   CSR_RW, 0x00000000 },
  { "LN4_TEST", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ln0_sta
  { "ln0_rx_sta", 0x00000040,  3,  0,   CSR_RO, 0x00000000 },
  { "LN0_STA", 0x00000040, 31, 0,   CSR_RO, 0x00000000 },
  // WORD dbgsta
  { "debug_sta", 0x00000044, 31,  0,   CSR_RO, 0x00000000 },
  { "DBGSTA", 0x00000044, 31, 0,   CSR_RO, 0x00000000 },
  // WORD dbgsel
  { "debug_sel", 0x00000048,  3,  0,   CSR_RW, 0x00000000 },
  { "DBGSEL", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_TX_OUT_H_
