#ifndef CSR_TABLE_TX_CTRL_H_
#define CSR_TABLE_TX_CTRL_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_tx_ctrl[] =
{
  // WORD ctrl_0
  { "tx_lev_en", 0x00000000,  0,  0,   CSR_RW, 0x00000000 },
  { "tx_ref_en", 0x00000000,  8,  8,   CSR_RW, 0x00000000 },
  { "tx_bias_en", 0x00000000, 16, 16,   CSR_RW, 0x00000000 },
  { "tx_out_en", 0x00000000, 24, 24,   CSR_RW, 0x00000000 },
  { "CTRL_0", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ctrl_1
  { "tx_clk_en", 0x00000004,  0,  0,   CSR_RW, 0x00000000 },
  { "pwr_ctrl_sel", 0x00000004,  9,  4,   CSR_RW, 0x0000003F },
  { "CTRL_1", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ctrl_2
  { "base_pwron_start", 0x00000008,  0,  0,  CSR_W1P, 0x00000000 },
  { "ln0_pwron_start", 0x00000008,  1,  1,  CSR_W1P, 0x00000000 },
  { "ln1_pwron_start", 0x00000008,  2,  2,  CSR_W1P, 0x00000000 },
  { "ln2_pwron_start", 0x00000008,  3,  3,  CSR_W1P, 0x00000000 },
  { "ln3_pwron_start", 0x00000008,  4,  4,  CSR_W1P, 0x00000000 },
  { "ln4_pwron_start", 0x00000008,  5,  5,  CSR_W1P, 0x00000000 },
  { "all_pwron_start", 0x00000008,  6,  6,  CSR_W1P, 0x00000000 },
  { "gpo_base_pwron_start", 0x00000008,  8,  8,  CSR_W1P, 0x00000000 },
  { "gpo_ln0_pwron_start", 0x00000008,  9,  9,  CSR_W1P, 0x00000000 },
  { "gpo_ln1_pwron_start", 0x00000008, 10, 10,  CSR_W1P, 0x00000000 },
  { "gpo_ln2_pwron_start", 0x00000008, 11, 11,  CSR_W1P, 0x00000000 },
  { "gpo_ln3_pwron_start", 0x00000008, 12, 12,  CSR_W1P, 0x00000000 },
  { "gpo_ln4_pwron_start", 0x00000008, 13, 13,  CSR_W1P, 0x00000000 },
  { "CTRL_2", 0x00000008, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD ctrl_ln0_a
  { "tx_ln0_lev_en", 0x0000000C,  0,  0,   CSR_RW, 0x00000000 },
  { "tx_ln0_ref_en", 0x0000000C,  8,  8,   CSR_RW, 0x00000000 },
  { "tx_ln0_bias04_en", 0x0000000C, 16, 16,   CSR_RW, 0x00000000 },
  { "tx_ln0_bias12_en", 0x0000000C, 24, 24,   CSR_RW, 0x00000000 },
  { "CTRL_LN0_A", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ctrl_ln0_b
  { "tx_ln0_out12_en", 0x00000010,  0,  0,   CSR_RW, 0x00000000 },
  { "tx_ln0_lp_en", 0x00000010,  8,  8,   CSR_RW, 0x00000000 },
  { "tx_ln0_hs_en", 0x00000010, 16, 16,   CSR_RW, 0x00000000 },
  { "tx_ln0_oen_en", 0x00000010, 24, 24,   CSR_RW, 0x00000000 },
  { "CTRL_LN0_B", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ctrl_ln1_a
  { "tx_ln1_lev_en", 0x00000014,  0,  0,   CSR_RW, 0x00000000 },
  { "tx_ln1_ref_en", 0x00000014,  8,  8,   CSR_RW, 0x00000000 },
  { "tx_ln1_bias04_en", 0x00000014, 16, 16,   CSR_RW, 0x00000000 },
  { "tx_ln1_bias12_en", 0x00000014, 24, 24,   CSR_RW, 0x00000000 },
  { "CTRL_LN1_A", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ctrl_ln1_b
  { "tx_ln1_out12_en", 0x00000018,  0,  0,   CSR_RW, 0x00000000 },
  { "tx_ln1_lp_en", 0x00000018,  8,  8,   CSR_RW, 0x00000000 },
  { "tx_ln1_hs_en", 0x00000018, 16, 16,   CSR_RW, 0x00000000 },
  { "tx_ln1_oen_en", 0x00000018, 24, 24,   CSR_RW, 0x00000000 },
  { "CTRL_LN1_B", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ctrl_ln2_a
  { "tx_ln2_lev_en", 0x0000001C,  0,  0,   CSR_RW, 0x00000000 },
  { "tx_ln2_ref_en", 0x0000001C,  8,  8,   CSR_RW, 0x00000000 },
  { "tx_ln2_bias04_en", 0x0000001C, 16, 16,   CSR_RW, 0x00000000 },
  { "tx_ln2_bias12_en", 0x0000001C, 24, 24,   CSR_RW, 0x00000000 },
  { "CTRL_LN2_A", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ctrl_ln2_b
  { "tx_ln2_out12_en", 0x00000020,  0,  0,   CSR_RW, 0x00000000 },
  { "tx_ln2_lp_en", 0x00000020,  8,  8,   CSR_RW, 0x00000000 },
  { "tx_ln2_hs_en", 0x00000020, 16, 16,   CSR_RW, 0x00000000 },
  { "tx_ln2_oen_en", 0x00000020, 24, 24,   CSR_RW, 0x00000000 },
  { "CTRL_LN2_B", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ctrl_ln3_a
  { "tx_ln3_lev_en", 0x00000024,  0,  0,   CSR_RW, 0x00000000 },
  { "tx_ln3_ref_en", 0x00000024,  8,  8,   CSR_RW, 0x00000000 },
  { "tx_ln3_bias04_en", 0x00000024, 16, 16,   CSR_RW, 0x00000000 },
  { "tx_ln3_bias12_en", 0x00000024, 24, 24,   CSR_RW, 0x00000000 },
  { "CTRL_LN3_A", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ctrl_ln3_b
  { "tx_ln3_out12_en", 0x00000028,  0,  0,   CSR_RW, 0x00000000 },
  { "tx_ln3_lp_en", 0x00000028,  8,  8,   CSR_RW, 0x00000000 },
  { "tx_ln3_hs_en", 0x00000028, 16, 16,   CSR_RW, 0x00000000 },
  { "tx_ln3_oen_en", 0x00000028, 24, 24,   CSR_RW, 0x00000000 },
  { "CTRL_LN3_B", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ctrl_ln4_a
  { "tx_ln4_lev_en", 0x0000002C,  0,  0,   CSR_RW, 0x00000000 },
  { "tx_ln4_ref_en", 0x0000002C,  8,  8,   CSR_RW, 0x00000000 },
  { "tx_ln4_bias04_en", 0x0000002C, 16, 16,   CSR_RW, 0x00000000 },
  { "tx_ln4_bias12_en", 0x0000002C, 24, 24,   CSR_RW, 0x00000000 },
  { "CTRL_LN4_A", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ctrl_ln4_b
  { "tx_ln4_out12_en", 0x00000030,  0,  0,   CSR_RW, 0x00000000 },
  { "tx_ln4_lp_en", 0x00000030,  8,  8,   CSR_RW, 0x00000000 },
  { "tx_ln4_hs_en", 0x00000030, 16, 16,   CSR_RW, 0x00000000 },
  { "tx_ln4_oen_en", 0x00000030, 24, 24,   CSR_RW, 0x00000000 },
  { "CTRL_LN4_B", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_bist_start
  { "bist_start", 0x00000034,  0,  0,  CSR_W1P, 0x00000000 },
  { "WORD_BIST_START", 0x00000034, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD bist_ctrl_0
  { "bist_en", 0x00000038,  0,  0,   CSR_RW, 0x00000000 },
  { "bist_mode", 0x00000038, 17, 16,   CSR_RW, 0x00000000 },
  { "bist_crc_en", 0x00000038, 24, 24,   CSR_RW, 0x00000000 },
  { "BIST_CTRL_0", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD bist_ctrl_1
  { "bist_pac_di", 0x0000003C,  7,  0,   CSR_RW, 0x00000029 },
  { "bist_pac_wc", 0x0000003C, 23,  8,   CSR_RW, 0x00000400 },
  { "BIST_CTRL_1", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD bist_time_0
  { "t_lpx", 0x00000040,  7,  0,   CSR_RW, 0x00000010 },
  { "t_prep", 0x00000040, 15,  8,   CSR_RW, 0x00000010 },
  { "t_zero", 0x00000040, 23, 16,   CSR_RW, 0x00000020 },
  { "t_trial", 0x00000040, 31, 24,   CSR_RW, 0x00000020 },
  { "BIST_TIME_0", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD bist_time_1
  { "t_clk_pre", 0x00000044,  7,  0,   CSR_RW, 0x00000020 },
  { "t_clk_post", 0x00000044, 15,  8,   CSR_RW, 0x00000020 },
  { "BIST_TIME_1", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD bist_ln0
  { "bist_ln0_en", 0x00000048,  0,  0,   CSR_RW, 0x00000000 },
  { "bist_ln0_clk_sel", 0x00000048,  8,  8,   CSR_RW, 0x00000000 },
  { "bist_ln0_pat", 0x00000048, 23, 16,   CSR_RW, 0x00000000 },
  { "BIST_LN0", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD bist_ln1
  { "bist_ln1_en", 0x0000004C,  0,  0,   CSR_RW, 0x00000000 },
  { "bist_ln1_clk_sel", 0x0000004C,  8,  8,   CSR_RW, 0x00000000 },
  { "bist_ln1_pat", 0x0000004C, 23, 16,   CSR_RW, 0x00000000 },
  { "BIST_LN1", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD bist_ln2
  { "bist_ln2_en", 0x00000050,  0,  0,   CSR_RW, 0x00000000 },
  { "bist_ln2_clk_sel", 0x00000050,  8,  8,   CSR_RW, 0x00000001 },
  { "bist_ln2_pat", 0x00000050, 23, 16,   CSR_RW, 0x00000055 },
  { "BIST_LN2", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD bist_ln3
  { "bist_ln3_en", 0x00000054,  0,  0,   CSR_RW, 0x00000000 },
  { "bist_ln3_clk_sel", 0x00000054,  8,  8,   CSR_RW, 0x00000000 },
  { "bist_ln3_pat", 0x00000054, 23, 16,   CSR_RW, 0x00000000 },
  { "BIST_LN3", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD bist_ln4
  { "bist_ln4_en", 0x00000058,  0,  0,   CSR_RW, 0x00000000 },
  { "bist_ln4_clk_sel", 0x00000058,  8,  8,   CSR_RW, 0x00000000 },
  { "bist_ln4_pat", 0x00000058, 23, 16,   CSR_RW, 0x00000000 },
  { "BIST_LN4", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD bist_sta_0
  { "bist_busy", 0x0000005C,  0,  0,   CSR_RO, 0x00000000 },
  { "bist_done", 0x0000005C,  8,  8,   CSR_RO, 0x00000000 },
  { "BIST_STA_0", 0x0000005C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD bist_sta_1
  { "bist_status", 0x00000060, 23,  0,   CSR_RO, 0x00000000 },
  { "BIST_STA_1", 0x00000060, 31, 0,   CSR_RO, 0x00000000 },
  // WORD bist_sta_ln0
  { "bist_ln0_crc", 0x00000064, 15,  0,   CSR_RO, 0x00000000 },
  { "BIST_STA_LN0", 0x00000064, 31, 0,   CSR_RO, 0x00000000 },
  // WORD bist_sta_ln1
  { "bist_ln1_crc", 0x00000068, 15,  0,   CSR_RO, 0x00000000 },
  { "BIST_STA_LN1", 0x00000068, 31, 0,   CSR_RO, 0x00000000 },
  // WORD bist_sta_ln2
  { "bist_ln2_crc", 0x0000006C, 15,  0,   CSR_RO, 0x00000000 },
  { "BIST_STA_LN2", 0x0000006C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD bist_sta_ln3
  { "bist_ln3_crc", 0x00000070, 15,  0,   CSR_RO, 0x00000000 },
  { "BIST_STA_LN3", 0x00000070, 31, 0,   CSR_RO, 0x00000000 },
  // WORD bist_sta_ln4
  { "bist_ln4_crc", 0x00000074, 15,  0,   CSR_RO, 0x00000000 },
  { "BIST_STA_LN4", 0x00000074, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_pwr_st0
  { "pwr_st0", 0x00000078, 23,  0,   CSR_RO, 0x00000000 },
  { "WORD_PWR_ST0", 0x00000078, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_pwr_st1
  { "pwr_st1", 0x0000007C, 23,  0,   CSR_RO, 0x00000000 },
  { "WORD_PWR_ST1", 0x0000007C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD resv
  { "reserved", 0x00000080, 31,  0,   CSR_RW, 0x00000000 },
  { "RESV", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_TX_CTRL_H_
