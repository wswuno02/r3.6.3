#ifndef CSR_TABLE_MCVP_H_
#define CSR_TABLE_MCVP_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_mcvp[] =
{
  // WORD mcvp_start
  { "frame_start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "MCVP000", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD mcvp_irq_clear
  { "irq_clear_frame_end", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "mer_irq_clear_frame_end", 0x00000004,  4,  4,  CSR_W1P, 0x00000000 },
  { "mer_irq_clear_bw_insufficient", 0x00000004,  5,  5,  CSR_W1P, 0x00000000 },
  { "mer_irq_clear_access_violation", 0x00000004,  6,  6,  CSR_W1P, 0x00000000 },
  { "nrw_irq_clear_frame_end", 0x00000004,  8,  8,  CSR_W1P, 0x00000000 },
  { "nrw_irq_clear_bw_insufficient", 0x00000004,  9,  9,  CSR_W1P, 0x00000000 },
  { "nrw_irq_clear_access_violation", 0x00000004, 10, 10,  CSR_W1P, 0x00000000 },
  { "mvr_irq_clear_frame_end", 0x00000004, 12, 12,  CSR_W1P, 0x00000000 },
  { "mvr_irq_clear_bw_insufficient", 0x00000004, 13, 13,  CSR_W1P, 0x00000000 },
  { "mvr_irq_clear_access_violation", 0x00000004, 14, 14,  CSR_W1P, 0x00000000 },
  { "mvw_irq_clear_frame_end", 0x00000004, 16, 16,  CSR_W1P, 0x00000000 },
  { "mvw_irq_clear_bw_insufficient", 0x00000004, 17, 17,  CSR_W1P, 0x00000000 },
  { "mvw_irq_clear_access_violation", 0x00000004, 18, 18,  CSR_W1P, 0x00000000 },
  { "venc_mvw_irq_clear_frame_end", 0x00000004, 20, 20,  CSR_W1P, 0x00000000 },
  { "venc_mvw_irq_clear_bw_insufficient", 0x00000004, 21, 21,  CSR_W1P, 0x00000000 },
  { "venc_mvw_irq_clear_access_violation", 0x00000004, 22, 22,  CSR_W1P, 0x00000000 },
  { "MCVP001", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD mcvp_irq
  { "status_frame_end", 0x00000008,  0,  0,   CSR_RO, 0x00000000 },
  { "mer_status_frame_end", 0x00000008,  4,  4,   CSR_RO, 0x00000000 },
  { "mer_status_bw_insufficient", 0x00000008,  5,  5,   CSR_RO, 0x00000000 },
  { "mer_status_access_violation", 0x00000008,  6,  6,   CSR_RO, 0x00000000 },
  { "nrw_status_frame_end", 0x00000008,  8,  8,   CSR_RO, 0x00000000 },
  { "nrw_status_bw_insufficient", 0x00000008,  9,  9,   CSR_RO, 0x00000000 },
  { "nrw_status_access_violation", 0x00000008, 10, 10,   CSR_RO, 0x00000000 },
  { "mvr_status_frame_end", 0x00000008, 12, 12,   CSR_RO, 0x00000000 },
  { "mvr_status_bw_insufficient", 0x00000008, 13, 13,   CSR_RO, 0x00000000 },
  { "mvr_status_access_violation", 0x00000008, 14, 14,   CSR_RO, 0x00000000 },
  { "mvw_status_frame_end", 0x00000008, 16, 16,   CSR_RO, 0x00000000 },
  { "mvw_status_bw_insufficient", 0x00000008, 17, 17,   CSR_RO, 0x00000000 },
  { "mvw_status_access_violation", 0x00000008, 18, 18,   CSR_RO, 0x00000000 },
  { "venc_mvw_status_frame_end", 0x00000008, 20, 20,   CSR_RO, 0x00000000 },
  { "venc_mvw_status_bw_insufficient", 0x00000008, 21, 21,   CSR_RO, 0x00000000 },
  { "venc_mvw_status_access_violation", 0x00000008, 22, 22,   CSR_RO, 0x00000000 },
  { "MCVP002", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // WORD mcvp_irq_mask
  { "irq_mask_frame_end", 0x0000000C,  0,  0,   CSR_RW, 0x00000001 },
  { "mer_irq_mask_frame_end", 0x0000000C,  4,  4,   CSR_RW, 0x00000001 },
  { "mer_irq_mask_bw_insufficient", 0x0000000C,  5,  5,   CSR_RW, 0x00000001 },
  { "mer_irq_mask_access_violation", 0x0000000C,  6,  6,   CSR_RW, 0x00000001 },
  { "nrw_irq_mask_frame_end", 0x0000000C,  8,  8,   CSR_RW, 0x00000001 },
  { "nrw_irq_mask_bw_insufficient", 0x0000000C,  9,  9,   CSR_RW, 0x00000001 },
  { "nrw_irq_mask_access_violation", 0x0000000C, 10, 10,   CSR_RW, 0x00000001 },
  { "mvr_irq_mask_frame_end", 0x0000000C, 12, 12,   CSR_RW, 0x00000001 },
  { "mvr_irq_mask_bw_insufficient", 0x0000000C, 13, 13,   CSR_RW, 0x00000001 },
  { "mvr_irq_mask_access_violation", 0x0000000C, 14, 14,   CSR_RW, 0x00000001 },
  { "mvw_irq_mask_frame_end", 0x0000000C, 16, 16,   CSR_RW, 0x00000001 },
  { "mvw_irq_mask_bw_insufficient", 0x0000000C, 17, 17,   CSR_RW, 0x00000001 },
  { "mvw_irq_mask_access_violation", 0x0000000C, 18, 18,   CSR_RW, 0x00000001 },
  { "venc_mvw_irq_mask_frame_end", 0x0000000C, 20, 20,   CSR_RW, 0x00000001 },
  { "venc_mvw_irq_mask_bw_insufficient", 0x0000000C, 21, 21,   CSR_RW, 0x00000001 },
  { "venc_mvw_irq_mask_access_violation", 0x0000000C, 22, 22,   CSR_RW, 0x00000001 },
  { "MCVP003", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mcvp_debug_mon_sel
  { "debug_mon_sel", 0x00000010,  2,  0,   CSR_RW, 0x00000000 },
  { "MCVP004", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mcvp_disable
  { "disable_mode", 0x00000014,  0,  0,   CSR_RW, 0x00000000 },
  { "MCVP005", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mcvp006
  { "frame_width_i", 0x00000018,  8,  0,   CSR_RW, 0x00000000 },
  { "frame_height", 0x00000018, 31, 16,   CSR_RW, 0x00000000 },
  { "MCVP006", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mcvp007
  { "most_left_tile", 0x0000001C,  0,  0,   CSR_RW, 0x00000000 },
  { "most_right_tile", 0x0000001C,  8,  8,   CSR_RW, 0x00000000 },
  { "padding_right_venc_mv", 0x0000001C, 16, 16,   CSR_RW, 0x00000000 },
  { "MCVP007", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mcvp008
  { "r2b_left_skip_pel", 0x00000020,  8,  0,   CSR_RW, 0x00000000 },
  { "r2b_out_blk_width", 0x00000020, 21, 16,   CSR_RW, 0x00000000 },
  { "MCVP008", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mcvp009
  { "me_out_blk_width", 0x00000024,  5,  0,   CSR_RW, 0x00000000 },
  { "me_out_blk_width_m1", 0x00000024, 21, 16,   CSR_RW, 0x00000000 },
  { "MCVP009", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mcvp010
  { "blk_height", 0x00000028, 12,  0,   CSR_RW, 0x00000000 },
  { "blk_height_m1", 0x00000028, 28, 16,   CSR_RW, 0x00000000 },
  { "MCVP010", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mcvp011
  { "sr_ix", 0x0000002C,  6,  0,   CSR_RW, 0x0000007C },
  { "sr_iy", 0x0000002C, 14,  8,   CSR_RW, 0x00000044 },
  { "sr_blk8_ix", 0x0000002C, 20, 16,   CSR_RW, 0x00000010 },
  { "sr_blk8_iy", 0x0000002C, 28, 24,   CSR_RW, 0x00000009 },
  { "MCVP011", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_MCVP_H_
