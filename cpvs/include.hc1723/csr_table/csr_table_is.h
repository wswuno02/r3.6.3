#ifndef CSR_TABLE_IS_H_
#define CSR_TABLE_IS_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_is[] =
{
  // WORD irq_clear
  { "irq_clear_lp_not_ack", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "IRQ_CLEAR", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD status
  { "status_lp_not_ack", 0x00000004,  0,  0,   CSR_RO, 0x00000000 },
  { "STATUS", 0x00000004, 31, 0,   CSR_RO, 0x00000000 },
  // WORD irq_mask
  { "irq_mask_lp_not_ack", 0x00000008,  0,  0,   CSR_RW, 0x00000001 },
  { "IRQ_MASK", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD efuse_vio
  { "fe0_efuse_resolution_violation", 0x0000000C,  0,  0,   CSR_RO, 0x00000000 },
  { "fe0_efuse_data_rate_violation", 0x0000000C,  8,  8,   CSR_RO, 0x00000000 },
  { "fe1_efuse_resolution_violation", 0x0000000C, 16, 16,   CSR_RO, 0x00000000 },
  { "fe1_efuse_data_rate_violation", 0x0000000C, 24, 24,   CSR_RO, 0x00000000 },
  { "EFUSE_VIO", 0x0000000C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_fe0_prd_mode
  { "fe0_prd_mode", 0x00000010,  0,  0,   CSR_RW, 0x00000001 },
  { "WORD_FE0_PRD_MODE", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD fe0_prd_broadcst
  { "fe0_prd_broadcast_to_edp_enable", 0x00000014,  0,  0,   CSR_RW, 0x00000000 },
  { "fe0_prd_broadcast_to_isr_enable", 0x00000014,  8,  8,   CSR_RW, 0x00000000 },
  { "FE0_PRD_BROADCST", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_fe0_edp_mux_sel
  { "fe0_edp_mux_sel", 0x00000018,  0,  0,   CSR_RW, 0x00000001 },
  { "WORD_FE0_EDP_MUX_SEL", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD fe0_ack_not_sel
  { "fe0_prd_broadcast_ack_not_sel", 0x0000001C,  0,  0,   CSR_RW, 0x00000000 },
  { "fe0_edp_mux_ack_not_sel", 0x0000001C,  8,  8,   CSR_RW, 0x00000000 },
  { "FE0_ACK_NOT_SEL", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_fe1_prd_mode
  { "fe1_prd_mode", 0x00000020,  0,  0,   CSR_RW, 0x00000000 },
  { "WORD_FE1_PRD_MODE", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD fe1_prd_broadcst
  { "fe1_prd_broadcast_to_edp_enable", 0x00000024,  0,  0,   CSR_RW, 0x00000000 },
  { "fe1_prd_broadcast_to_isr_enable", 0x00000024,  8,  8,   CSR_RW, 0x00000000 },
  { "FE1_PRD_BROADCST", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_fe1_edp_mux_sel
  { "fe1_edp_mux_sel", 0x00000028,  0,  0,   CSR_RW, 0x00000001 },
  { "WORD_FE1_EDP_MUX_SEL", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD fe1_ack_not_sel
  { "fe1_prd_broadcast_ack_not_sel", 0x0000002C,  0,  0,   CSR_RW, 0x00000000 },
  { "fe1_edp_mux_ack_not_sel", 0x0000002C,  8,  8,   CSR_RW, 0x00000000 },
  { "FE1_ACK_NOT_SEL", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD iroute_enable_0
  { "iroute_src_lp_broadcast_enable", 0x00000030,  5,  0,   CSR_RW, 0x00000000 },
  { "iroute_src_fe0_edp0_broadcast_enable", 0x00000030, 13,  8,   CSR_RW, 0x00000002 },
  { "iroute_src_fe0_edp1_broadcast_enable", 0x00000030, 21, 16,   CSR_RW, 0x00000004 },
  { "iroute_src_fe0_isr_broadcast_enable", 0x00000030, 29, 24,   CSR_RW, 0x00000000 },
  { "IROUTE_ENABLE_0", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD iroute_enable_1
  { "iroute_src_fe1_edp0_broadcast_enable", 0x00000034,  5,  0,   CSR_RW, 0x00000008 },
  { "iroute_src_fe1_edp1_broadcast_enable", 0x00000034, 13,  8,   CSR_RW, 0x00000010 },
  { "iroute_src_fe1_isr_broadcast_enable", 0x00000034, 21, 16,   CSR_RW, 0x00000000 },
  { "IROUTE_ENABLE_1", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD iroute_sel_0
  { "iroute_dst_bypass_isk0_sel", 0x00000038,  2,  0,   CSR_RW, 0x00000000 },
  { "iroute_dst_isk0_in0_sel", 0x00000038, 10,  8,   CSR_RW, 0x00000001 },
  { "iroute_dst_isk0_in1_sel", 0x00000038, 18, 16,   CSR_RW, 0x00000002 },
  { "iroute_dst_bypass_isk1_sel", 0x00000038, 26, 24,   CSR_RW, 0x00000000 },
  { "IROUTE_SEL_0", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD iroute_sel_1
  { "iroute_dst_isk1_in0_sel", 0x0000003C,  2,  0,   CSR_RW, 0x00000004 },
  { "iroute_dst_isk1_in1_sel", 0x0000003C, 10,  8,   CSR_RW, 0x00000005 },
  { "IROUTE_SEL_1", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD iroute_ack_not_sel_0
  { "iroute_src_broadcast_ack_not_sel", 0x00000040,  6,  0,   CSR_RW, 0x00000000 },
  { "IROUTE_ACK_NOT_SEL_0", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD iroute_ack_not_sel_1
  { "iroute_dst_mux_ack_not_sel", 0x00000044,  5,  0,   CSR_RW, 0x00000000 },
  { "IROUTE_ACK_NOT_SEL_1", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD iroute_cat0
  { "iroute_dst_bypass_isk0_msb_cat", 0x00000048,  0,  0,   CSR_RW, 0x00000000 },
  { "iroute_dst_isk0_in0_msb_cat", 0x00000048,  8,  8,   CSR_RW, 0x00000000 },
  { "iroute_dst_isk0_in1_msb_cat", 0x00000048, 16, 16,   CSR_RW, 0x00000000 },
  { "iroute_dst_bypass_isk1_msb_cat", 0x00000048, 24, 24,   CSR_RW, 0x00000000 },
  { "IROUTE_CAT0", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD iroute_cat1
  { "iroute_dst_isk1_in0_msb_cat", 0x0000004C,  0,  0,   CSR_RW, 0x00000000 },
  { "iroute_dst_isk1_in1_msb_cat", 0x0000004C,  8,  8,   CSR_RW, 0x00000000 },
  { "IROUTE_CAT1", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD oroute_enable_0
  { "oroute_src_bypass_isk0_broadcast_enable", 0x00000050,  3,  0,   CSR_RW, 0x00000000 },
  { "oroute_src_isk0_agma_broadcast_enable", 0x00000050, 11,  8,   CSR_RW, 0x00000000 },
  { "oroute_src_isk0_crop_broadcast_enable", 0x00000050, 19, 16,   CSR_RW, 0x00000000 },
  { "oroute_src_isk0_bsp_broadcast_enable", 0x00000050, 27, 24,   CSR_RW, 0x00000000 },
  { "OROUTE_ENABLE_0", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD oroute_enable_1
  { "oroute_src_isk0_fgma_broadcast_enable", 0x00000054,  3,  0,   CSR_RW, 0x00000000 },
  { "oroute_src_isk0_fsc_broadcast_enable", 0x00000054, 11,  8,   CSR_RW, 0x00000001 },
  { "oroute_src_isk0_cvs_broadcast_enable", 0x00000054, 19, 16,   CSR_RW, 0x00000000 },
  { "oroute_src_isk0_cs_broadcast_enable", 0x00000054, 27, 24,   CSR_RW, 0x00000000 },
  { "OROUTE_ENABLE_1", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD oroute_enable_2
  { "oroute_src_isk0_ink_broadcast_enable", 0x00000058,  3,  0,   CSR_RW, 0x00000000 },
  { "OROUTE_ENABLE_2", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD oroute_enable_3
  { "oroute_src_bypass_isk1_broadcast_enable", 0x0000005C,  3,  0,   CSR_RW, 0x00000000 },
  { "oroute_src_isk1_agma_broadcast_enable", 0x0000005C, 11,  8,   CSR_RW, 0x00000000 },
  { "oroute_src_isk1_crop_broadcast_enable", 0x0000005C, 19, 16,   CSR_RW, 0x00000000 },
  { "oroute_src_isk1_bsp_broadcast_enable", 0x0000005C, 27, 24,   CSR_RW, 0x00000000 },
  { "OROUTE_ENABLE_3", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD oroute_enable_4
  { "oroute_src_isk1_fgma_broadcast_enable", 0x00000060,  3,  0,   CSR_RW, 0x00000000 },
  { "oroute_src_isk1_fsc_broadcast_enable", 0x00000060, 11,  8,   CSR_RW, 0x00000002 },
  { "oroute_src_isk1_cvs_broadcast_enable", 0x00000060, 19, 16,   CSR_RW, 0x00000000 },
  { "oroute_src_isk1_cs_broadcast_enable", 0x00000060, 27, 24,   CSR_RW, 0x00000000 },
  { "OROUTE_ENABLE_4", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD oroute_enable_5
  { "oroute_src_isk1_ink_broadcast_enable", 0x00000064,  3,  0,   CSR_RW, 0x00000000 },
  { "OROUTE_ENABLE_5", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD oroute_sel
  { "oroute_dst_iswroi0_sel", 0x00000068,  4,  0,   CSR_RW, 0x00000005 },
  { "oroute_dst_iswroi1_sel", 0x00000068, 12,  8,   CSR_RW, 0x0000000E },
  { "oroute_dst_isw0_sel", 0x00000068, 20, 16,   CSR_RW, 0x00000000 },
  { "oroute_dst_isw1_sel", 0x00000068, 28, 24,   CSR_RW, 0x00000000 },
  { "OROUTE_SEL", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD oroute_cat
  { "oroute_dst_iswroi0_msb_cat", 0x0000006C,  0,  0,   CSR_RW, 0x00000000 },
  { "oroute_dst_iswroi1_msb_cat", 0x0000006C,  8,  8,   CSR_RW, 0x00000000 },
  { "oroute_dst_isw0_msb_cat", 0x0000006C, 16, 16,   CSR_RW, 0x00000000 },
  { "oroute_dst_isw1_msb_cat", 0x0000006C, 24, 24,   CSR_RW, 0x00000000 },
  { "OROUTE_CAT", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD oroute_ack_not_sel_0
  { "oroute_src0_broadcast_ack_not_sel", 0x00000070,  8,  0,   CSR_RW, 0x00000000 },
  { "oroute_src1_broadcast_ack_not_sel", 0x00000070, 24, 16,   CSR_RW, 0x00000000 },
  { "OROUTE_ACK_NOT_SEL_0", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // WORD oroute_ack_not_sel_1
  { "oroute_dst_mux_ack_not_sel", 0x00000074,  3,  0,   CSR_RW, 0x00000000 },
  { "OROUTE_ACK_NOT_SEL_1", 0x00000074, 31, 0,   CSR_RW, 0x00000000 },
  // WORD shared_sram_fsc
  { "fsc_buf_mode", 0x00000078,  0,  0,   CSR_RW, 0x00000001 },
  { "SHARED_SRAM_FSC", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD shared_sram_cfg
  { "bsp_cvs_cs_sram_mode", 0x0000007C,  0,  0,   CSR_RW, 0x00000001 },
  { "bsp_cvs_cs_sram_master", 0x0000007C,  8,  8,   CSR_RW, 0x00000000 },
  { "SHARED_SRAM_CFG", 0x0000007C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwe_mode
  { "pwe0_mode", 0x00000080,  0,  0,   CSR_RW, 0x00000001 },
  { "pwe1_mode", 0x00000080,  8,  8,   CSR_RW, 0x00000001 },
  { "pwe2_mode", 0x00000080, 16, 16,   CSR_RW, 0x00000001 },
  { "pwe3_mode", 0x00000080, 24, 24,   CSR_RW, 0x00000001 },
  { "PWE_MODE", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dbg_mon_sel
  { "debug_mon_sel", 0x00000084,  5,  0,   CSR_RW, 0x00000000 },
  { "DBG_MON_SEL", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD buf_update
  { "double_buf_update", 0x00000088,  0,  0,  CSR_W1P, 0x00000000 },
  { "BUF_UPDATE", 0x00000088, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD mem_lp_ctrl
  { "sd", 0x00000098,  0,  0,   CSR_RW, 0x00000000 },
  { "slp", 0x00000098,  8,  8,   CSR_RW, 0x00000000 },
  { "MEM_LP_CTRL", 0x00000098, 31, 0,   CSR_RW, 0x00000000 },
  // WORD fe_dbg_mon_sel
  { "fe0_debug_mon_sel", 0x0000009C,  1,  0,   CSR_RW, 0x00000000 },
  { "fe1_debug_mon_sel", 0x0000009C,  9,  8,   CSR_RW, 0x00000000 },
  { "FE_DBG_MON_SEL", 0x0000009C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD isk_dbg_mon_sel
  { "isk0_debug_mon_sel", 0x000000A0,  2,  0,   CSR_RW, 0x00000000 },
  { "isk1_debug_mon_sel", 0x000000A0, 10,  8,   CSR_RW, 0x00000000 },
  { "ISK_DBG_MON_SEL", 0x000000A0, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_IS_H_
