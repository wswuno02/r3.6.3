#ifndef CSR_TABLE_SENIF_SYSCFG_H_
#define CSR_TABLE_SENIF_SYSCFG_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_senif_syscfg[] =
{
  // WORD ckg_rx0
  { "cken_rx0", 0x00000000,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_RX0", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_rx1
  { "cken_rx1", 0x00000004,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_RX1", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_dec0
  { "cken_dec0", 0x00000008,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_DEC0", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_dec1
  { "cken_dec1", 0x0000000C,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_DEC1", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_ps
  { "cken_ps", 0x00000010,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_PS", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_slb0
  { "cken_slb0", 0x00000014,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_SLB0", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_slb1
  { "cken_slb1", 0x00000018,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_SLB1", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_ctrl
  { "cken_ctrl", 0x0000001C,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_CTRL", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD resv
  { "reserved", 0x0000003C, 15,  0,   CSR_RW, 0x00000000 },
  { "RESV", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rst_rx0
  { "sw_rst_rx0_senif_clk", 0x00000040,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_rx0", 0x00000040,  8,  8,  CSR_W1P, 0x00000000 },
  { "RST_RX0", 0x00000040, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_rx1
  { "sw_rst_rx1_senif_clk", 0x00000044,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_rx1", 0x00000044,  8,  8,  CSR_W1P, 0x00000000 },
  { "RST_RX1", 0x00000044, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_dec0
  { "sw_rst_dec0_senif_clk", 0x00000048,  0,  0,  CSR_W1P, 0x00000000 },
  { "sw_rst_dec0_out_clk", 0x00000048,  8,  8,  CSR_W1P, 0x00000000 },
  { "csr_rst_dec0", 0x00000048, 16, 16,  CSR_W1P, 0x00000000 },
  { "RST_DEC0", 0x00000048, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_dec1
  { "sw_rst_dec1_senif_clk", 0x0000004C,  0,  0,  CSR_W1P, 0x00000000 },
  { "sw_rst_dec1_out_clk", 0x0000004C,  8,  8,  CSR_W1P, 0x00000000 },
  { "csr_rst_dec1", 0x0000004C, 16, 16,  CSR_W1P, 0x00000000 },
  { "RST_DEC1", 0x0000004C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_ps
  { "sw_rst_ps_senif_clk", 0x00000060,  0,  0,  CSR_W1P, 0x00000000 },
  { "sw_rst_ps_out_clk", 0x00000060,  8,  8,  CSR_W1P, 0x00000000 },
  { "csr_rst_ps", 0x00000060, 16, 16,  CSR_W1P, 0x00000000 },
  { "RST_PS", 0x00000060, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_slb0
  { "sw_rst_slb0_out_clk", 0x00000064,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_slb0", 0x00000064,  8,  8,  CSR_W1P, 0x00000000 },
  { "RST_SLB0", 0x00000064, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_slb1
  { "sw_rst_slb1_out_clk", 0x00000068,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_slb1", 0x00000068,  8,  8,  CSR_W1P, 0x00000000 },
  { "RST_SLB1", 0x00000068, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_ctrl
  { "sw_rst_ctrl_sensor_clk", 0x0000006C,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_ctrl", 0x0000006C,  8,  8,  CSR_W1P, 0x00000000 },
  { "RST_CTRL", 0x0000006C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD lvrst_rx0
  { "lv_rst_rx0_senif_clk", 0x00000080,  0,  0,   CSR_RW, 0x00000000 },
  { "LVRST_RX0", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_rx1
  { "lv_rst_rx1_senif_clk", 0x00000084,  0,  0,   CSR_RW, 0x00000000 },
  { "LVRST_RX1", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_dec0
  { "lv_rst_dec0_senif_clk", 0x00000088,  0,  0,   CSR_RW, 0x00000000 },
  { "lv_rst_dec0_out_clk", 0x00000088,  8,  8,   CSR_RW, 0x00000000 },
  { "LVRST_DEC0", 0x00000088, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_dec1
  { "lv_rst_dec1_senif_clk", 0x0000008C,  0,  0,   CSR_RW, 0x00000000 },
  { "lv_rst_dec1_out_clk", 0x0000008C,  8,  8,   CSR_RW, 0x00000000 },
  { "LVRST_DEC1", 0x0000008C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_ps
  { "lv_rst_ps_senif_clk", 0x00000090,  0,  0,   CSR_RW, 0x00000000 },
  { "lv_rst_ps_out_clk", 0x00000090,  8,  8,   CSR_RW, 0x00000000 },
  { "LVRST_PS", 0x00000090, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_slb0
  { "lv_rst_slb0_out_clk", 0x00000094,  0,  0,   CSR_RW, 0x00000000 },
  { "LVRST_SLB0", 0x00000094, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_slb1
  { "lv_rst_slb1_out_clk", 0x00000098,  0,  0,   CSR_RW, 0x00000000 },
  { "LVRST_SLB1", 0x00000098, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_ctrl
  { "lv_rst_ctrl_sensor_clk", 0x0000009C,  0,  0,   CSR_RW, 0x00000000 },
  { "LVRST_CTRL", 0x0000009C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mem_wrapper
  { "sd", 0x00000100,  8,  8,   CSR_RW, 0x00000000 },
  { "slp", 0x00000100, 16, 16,   CSR_RW, 0x00000000 },
  { "MEM_WRAPPER", 0x00000100, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_SENIF_SYSCFG_H_
