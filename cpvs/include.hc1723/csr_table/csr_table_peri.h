#ifndef CSR_TABLE_PERI_H_
#define CSR_TABLE_PERI_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_peri[] =
{
  // WORD peri00
  { "uart0_mode_sel", 0x00000000,  1,  0,   CSR_RW, 0x00000000 },
  { "uart1_mode_sel", 0x00000000,  9,  8,   CSR_RW, 0x00000000 },
  { "uart2_mode_sel", 0x00000000, 17, 16,   CSR_RW, 0x00000000 },
  { "PERI00", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD peri01
  { "uart3_mode_sel", 0x00000004,  1,  0,   CSR_RW, 0x00000000 },
  { "uart4_mode_sel", 0x00000004,  9,  8,   CSR_RW, 0x00000000 },
  { "uart5_mode_sel", 0x00000004, 17, 16,   CSR_RW, 0x00000000 },
  { "PERI01", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD peri02
  { "uart0_debug_sel", 0x00000008,  0,  0,   CSR_RW, 0x00000000 },
  { "uart1_debug_sel", 0x00000008,  8,  8,   CSR_RW, 0x00000000 },
  { "uart2_debug_sel", 0x00000008, 16, 16,   CSR_RW, 0x00000000 },
  { "PERI02", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD peri03
  { "uart3_debug_sel", 0x0000000C,  0,  0,   CSR_RW, 0x00000000 },
  { "uart4_debug_sel", 0x0000000C,  8,  8,   CSR_RW, 0x00000000 },
  { "uart5_debug_sel", 0x0000000C, 16, 16,   CSR_RW, 0x00000000 },
  { "PERI03", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD peri04
  { "debug_sel", 0x00000010,  3,  0,   CSR_RW, 0x00000000 },
  { "PERI04", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_PERI_H_
