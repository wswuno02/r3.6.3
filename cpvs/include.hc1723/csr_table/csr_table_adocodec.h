#ifndef CSR_TABLE_ADOCODEC_H_
#define CSR_TABLE_ADOCODEC_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_adocodec[] =
{
  // WORD word_codec_mode
  { "codec_mode", 0x00000000,  1,  0,   CSR_RW, 0x00000000 },
  { "codec_mux_ack_not_sel", 0x00000000,  8,  8,   CSR_RW, 0x00000000 },
  { "WORD_CODEC_MODE", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_g711_mode
  { "g711_mode", 0x00000004,  0,  0,   CSR_RW, 0x00000000 },
  { "WORD_G711_MODE", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_g726_mode
  { "g726_mode", 0x00000008,  0,  0,   CSR_RW, 0x00000000 },
  { "WORD_G726_MODE", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD double_buf
  { "double_buf_update", 0x0000000C,  0,  0,  CSR_W1P, 0x00000000 },
  { "DOUBLE_BUF", 0x0000000C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD clear
  { "codec_clear", 0x00000010,  0,  0,  CSR_W1P, 0x00000000 },
  { "CLEAR", 0x00000010, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD pack_i_ctrl
  { "pki_little_endian", 0x00000014,  0,  0,   CSR_RW, 0x00000000 },
  { "PACK_I_CTRL", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pack_o_ctrl
  { "pko_little_endian", 0x00000018,  0,  0,   CSR_RW, 0x00000000 },
  { "PACK_O_CTRL", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_ADOCODEC_H_
