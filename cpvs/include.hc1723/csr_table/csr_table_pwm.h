#ifndef CSR_TABLE_PWM_H_
#define CSR_TABLE_PWM_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_pwm[] =
{
  // WORD pwm00
  { "trigger_0", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "irq_clear_0", 0x00000000, 16, 16,  CSR_W1P, 0x00000000 },
  { "PWM00", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD pwm01
  { "busy_0", 0x00000004,  0,  0,   CSR_RO, 0x00000000 },
  { "status_0", 0x00000004, 16, 16,   CSR_RO, 0x00000000 },
  { "PWM01", 0x00000004, 31, 0,   CSR_RO, 0x00000000 },
  // WORD pwm02
  { "irq_mask_0", 0x00000008,  0,  0,   CSR_RW, 0x00000001 },
  { "mode_0", 0x00000008, 16, 16,   CSR_RW, 0x00000000 },
  { "PWM02", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm03
  { "prescaler_0", 0x0000000C,  4,  0,   CSR_RW, 0x00000000 },
  { "count_period_0", 0x0000000C, 15,  8,   CSR_RW, 0x00000002 },
  { "count_high_0", 0x0000000C, 23, 16,   CSR_RW, 0x00000001 },
  { "num_period_0", 0x0000000C, 31, 24,   CSR_RW, 0x00000000 },
  { "PWM03", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm04
  { "trigger_1", 0x00000010,  0,  0,  CSR_W1P, 0x00000000 },
  { "irq_clear_1", 0x00000010, 16, 16,  CSR_W1P, 0x00000000 },
  { "PWM04", 0x00000010, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD pwm05
  { "busy_1", 0x00000014,  0,  0,   CSR_RO, 0x00000000 },
  { "status_1", 0x00000014, 16, 16,   CSR_RO, 0x00000000 },
  { "PWM05", 0x00000014, 31, 0,   CSR_RO, 0x00000000 },
  // WORD pwm06
  { "irq_mask_1", 0x00000018,  0,  0,   CSR_RW, 0x00000001 },
  { "mode_1", 0x00000018, 16, 16,   CSR_RW, 0x00000000 },
  { "PWM06", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm07
  { "prescaler_1", 0x0000001C,  4,  0,   CSR_RW, 0x00000000 },
  { "count_period_1", 0x0000001C, 15,  8,   CSR_RW, 0x00000002 },
  { "count_high_1", 0x0000001C, 23, 16,   CSR_RW, 0x00000001 },
  { "num_period_1", 0x0000001C, 31, 24,   CSR_RW, 0x00000000 },
  { "PWM07", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm08
  { "trigger_2", 0x00000020,  0,  0,  CSR_W1P, 0x00000000 },
  { "irq_clear_2", 0x00000020, 16, 16,  CSR_W1P, 0x00000000 },
  { "PWM08", 0x00000020, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD pwm09
  { "busy_2", 0x00000024,  0,  0,   CSR_RO, 0x00000000 },
  { "status_2", 0x00000024, 16, 16,   CSR_RO, 0x00000000 },
  { "PWM09", 0x00000024, 31, 0,   CSR_RO, 0x00000000 },
  // WORD pwm10
  { "irq_mask_2", 0x00000028,  0,  0,   CSR_RW, 0x00000001 },
  { "mode_2", 0x00000028, 16, 16,   CSR_RW, 0x00000000 },
  { "PWM10", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm11
  { "prescaler_2", 0x0000002C,  4,  0,   CSR_RW, 0x00000000 },
  { "count_period_2", 0x0000002C, 15,  8,   CSR_RW, 0x00000002 },
  { "count_high_2", 0x0000002C, 23, 16,   CSR_RW, 0x00000001 },
  { "num_period_2", 0x0000002C, 31, 24,   CSR_RW, 0x00000000 },
  { "PWM11", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm12
  { "trigger_3", 0x00000030,  0,  0,  CSR_W1P, 0x00000000 },
  { "irq_clear_3", 0x00000030, 16, 16,  CSR_W1P, 0x00000000 },
  { "PWM12", 0x00000030, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD pwm13
  { "busy_3", 0x00000034,  0,  0,   CSR_RO, 0x00000000 },
  { "status_3", 0x00000034, 16, 16,   CSR_RO, 0x00000000 },
  { "PWM13", 0x00000034, 31, 0,   CSR_RO, 0x00000000 },
  // WORD pwm14
  { "irq_mask_3", 0x00000038,  0,  0,   CSR_RW, 0x00000001 },
  { "mode_3", 0x00000038, 16, 16,   CSR_RW, 0x00000000 },
  { "PWM14", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm15
  { "prescaler_3", 0x0000003C,  4,  0,   CSR_RW, 0x00000000 },
  { "count_period_3", 0x0000003C, 15,  8,   CSR_RW, 0x00000002 },
  { "count_high_3", 0x0000003C, 23, 16,   CSR_RW, 0x00000001 },
  { "num_period_3", 0x0000003C, 31, 24,   CSR_RW, 0x00000000 },
  { "PWM15", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm16
  { "trigger_4", 0x00000040,  0,  0,  CSR_W1P, 0x00000000 },
  { "irq_clear_4", 0x00000040, 16, 16,  CSR_W1P, 0x00000000 },
  { "PWM16", 0x00000040, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD pwm17
  { "busy_4", 0x00000044,  0,  0,   CSR_RO, 0x00000000 },
  { "status_4", 0x00000044, 16, 16,   CSR_RO, 0x00000000 },
  { "PWM17", 0x00000044, 31, 0,   CSR_RO, 0x00000000 },
  // WORD pwm18
  { "irq_mask_4", 0x00000048,  0,  0,   CSR_RW, 0x00000001 },
  { "mode_4", 0x00000048, 16, 16,   CSR_RW, 0x00000000 },
  { "PWM18", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm19
  { "prescaler_4", 0x0000004C,  4,  0,   CSR_RW, 0x00000000 },
  { "count_period_4", 0x0000004C, 15,  8,   CSR_RW, 0x00000002 },
  { "count_high_4", 0x0000004C, 23, 16,   CSR_RW, 0x00000001 },
  { "num_period_4", 0x0000004C, 31, 24,   CSR_RW, 0x00000000 },
  { "PWM19", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm20
  { "trigger_5", 0x00000050,  0,  0,  CSR_W1P, 0x00000000 },
  { "irq_clear_5", 0x00000050, 16, 16,  CSR_W1P, 0x00000000 },
  { "PWM20", 0x00000050, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD pwm21
  { "busy_5", 0x00000054,  0,  0,   CSR_RO, 0x00000000 },
  { "status_5", 0x00000054, 16, 16,   CSR_RO, 0x00000000 },
  { "PWM21", 0x00000054, 31, 0,   CSR_RO, 0x00000000 },
  // WORD pwm22
  { "irq_mask_5", 0x00000058,  0,  0,   CSR_RW, 0x00000001 },
  { "mode_5", 0x00000058, 16, 16,   CSR_RW, 0x00000000 },
  { "PWM22", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm23
  { "prescaler_5", 0x0000005C,  4,  0,   CSR_RW, 0x00000000 },
  { "count_period_5", 0x0000005C, 15,  8,   CSR_RW, 0x00000002 },
  { "count_high_5", 0x0000005C, 23, 16,   CSR_RW, 0x00000001 },
  { "num_period_5", 0x0000005C, 31, 24,   CSR_RW, 0x00000000 },
  { "PWM23", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm24
  { "out_sel_0", 0x00000060,  2,  0,   CSR_RW, 0x00000000 },
  { "out_sel_1", 0x00000060, 10,  8,   CSR_RW, 0x00000000 },
  { "out_sel_2", 0x00000060, 18, 16,   CSR_RW, 0x00000000 },
  { "out_sel_3", 0x00000060, 26, 24,   CSR_RW, 0x00000000 },
  { "PWM24", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm25
  { "out_sel_4", 0x00000064,  2,  0,   CSR_RW, 0x00000000 },
  { "out_sel_5", 0x00000064, 10,  8,   CSR_RW, 0x00000000 },
  { "PWM25", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm27
  { "debug_mon_sel", 0x0000006C,  0,  0,   CSR_RW, 0x00000000 },
  { "PWM27", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_PWM_H_
