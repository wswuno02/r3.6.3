#ifndef CSR_TABLE_VENC_MVW_H_
#define CSR_TABLE_VENC_MVW_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_venc_mvw[] =
{
  // WORD sw132_00
  { "frame_start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "SW132_00", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD sw132_01
  { "access_illegal_hang", 0x00000004,  0,  0,   CSR_RW, 0x00000001 },
  { "access_illegal_mask", 0x00000004,  8,  8,   CSR_RW, 0x00000001 },
  { "SW132_01", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sw132_04
  { "col_addr_type", 0x00000010,  9,  8,   CSR_RW, 0x00000000 },
  { "debug_mon_sel", 0x00000010, 24, 24,   CSR_RW, 0x00000000 },
  { "SW132_04", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sw132_07
  { "target_burst_len", 0x0000001C,  4,  0,   CSR_RW, 0x00000010 },
  { "access_end_sel", 0x0000001C,  8,  8,   CSR_RW, 0x00000001 },
  { "SW132_07", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sw132_08
  { "target_fifo_level", 0x00000020,  6,  0,   CSR_RW, 0x00000020 },
  { "fifo_full_level", 0x00000020, 22, 16,   CSR_RW, 0x00000040 },
  { "SW132_08", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sw132_09
  { "height", 0x00000024, 11,  0,   CSR_RW, 0x00000000 },
  { "width", 0x00000024, 29, 16,   CSR_RW, 0x00000000 },
  { "SW132_09", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sw132_12
  { "fifo_flush_len", 0x00000030, 12,  0,   CSR_RW, 0x00000000 },
  { "fifo_start_phase", 0x00000030, 16, 16,   CSR_RW, 0x00000000 },
  { "fifo_end_phase", 0x00000030, 24, 24,   CSR_RW, 0x00000001 },
  { "SW132_12", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sw132_13
  { "start_addr", 0x00000034, 27,  0,   CSR_RW, 0x00000000 },
  { "SW132_13", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sw132_14
  { "end_addr", 0x00000038, 27,  0,   CSR_RW, 0x0FFFFFFF },
  { "SW132_14", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sw132_16
  { "pixel_flush_len", 0x00000040, 13,  0,   CSR_RW, 0x00000000 },
  { "SW132_16", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sw132_17
  { "flush_addr_skip", 0x00000044, 12,  0,   CSR_RW, 0x00000000 },
  { "SW132_17", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sw132_18
  { "reserved", 0x00000048, 31,  0,   CSR_RW, 0x00000000 },
  { "SW132_18", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sw132_25
  { "bank_interleave_type", 0x00000064,  1,  0,   CSR_RW, 0x00000002 },
  { "bank_group_type", 0x00000064,  9,  8,   CSR_RW, 0x00000001 },
  { "SW132_25", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sw132_26
  { "v_start", 0x00000068, 11,  0,   CSR_RW, 0x00000000 },
  { "v_end", 0x00000068, 27, 16,   CSR_RW, 0x00000000 },
  { "SW132_26", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sw132_27
  { "h_start", 0x0000006C, 13,  0,   CSR_RW, 0x00000000 },
  { "h_end", 0x0000006C, 29, 16,   CSR_RW, 0x00000000 },
  { "SW132_27", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sw132_40
  { "ini_addr_linear_0", 0x000000A0, 27,  0,   CSR_RW, 0x00000000 },
  { "SW132_40", 0x000000A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sw132_41
  { "ini_addr_linear_1", 0x000000A4, 27,  0,   CSR_RW, 0x00000000 },
  { "SW132_41", 0x000000A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sw132_42
  { "ini_addr_linear_2", 0x000000A8, 27,  0,   CSR_RW, 0x00000000 },
  { "SW132_42", 0x000000A8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sw132_43
  { "ini_addr_linear_3", 0x000000AC, 27,  0,   CSR_RW, 0x00000000 },
  { "SW132_43", 0x000000AC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sw132_44
  { "ini_addr_linear_4", 0x000000B0, 27,  0,   CSR_RW, 0x00000000 },
  { "SW132_44", 0x000000B0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sw132_45
  { "ini_addr_linear_5", 0x000000B4, 27,  0,   CSR_RW, 0x00000000 },
  { "SW132_45", 0x000000B4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sw132_46
  { "ini_addr_linear_6", 0x000000B8, 27,  0,   CSR_RW, 0x00000000 },
  { "SW132_46", 0x000000B8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sw132_47
  { "ini_addr_linear_7", 0x000000BC, 27,  0,   CSR_RW, 0x00000000 },
  { "SW132_47", 0x000000BC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sw132_48
  { "ini_addr_bank_offset", 0x000000C0,  2,  0,   CSR_RW, 0x00000000 },
  { "SW132_48", 0x000000C0, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_VENC_MVW_H_
