#ifndef CSR_TABLE_DMA_8_C1_D64_SYSCFG_H_
#define CSR_TABLE_DMA_8_C1_D64_SYSCFG_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_dma_8_c1_d64_syscfg[] =
{
  // WORD ckg_dma_k
  { "cken_dma_k", 0x00000000,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_DMA_K", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_dma_d
  { "cken_dma_d", 0x00000004,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_DMA_D", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_dma_a
  { "cken_dma_a", 0x00000008,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_DMA_A", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD resv
  { "reserved", 0x0000000C, 15,  0,   CSR_RW, 0x00000000 },
  { "RESV", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_DMA_8_C1_D64_SYSCFG_H_
