#ifndef CSR_TABLE_RST_AON_H_
#define CSR_TABLE_RST_AON_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_rst_aon[] =
{
  // WORD rst_aon_0
  { "sw_rst_psdecoder", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "sw_rst_lpmd", 0x00000000,  8,  8,  CSR_W1P, 0x00000000 },
  { "sw_rst_wdt", 0x00000000, 16, 16,  CSR_W1P, 0x00000000 },
  { "sw_rst_rtc", 0x00000000, 24, 24,  CSR_W1P, 0x00000000 },
  { "RST_AON_0", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_aon_1
  { "sw_rst_amc", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "sw_rst_irq", 0x00000004,  8,  8,  CSR_W1P, 0x00000000 },
  { "sw_rst_dvp", 0x00000004, 16, 16,  CSR_W1P, 0x00000000 },
  { "RST_AON_1", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD lvrst_aon_0
  { "lv_rst_psdecoder", 0x00000008,  0,  0,   CSR_RW, 0x00000000 },
  { "lv_rst_lpmd", 0x00000008,  8,  8,   CSR_RW, 0x00000000 },
  { "lv_rst_wdt", 0x00000008, 16, 16,   CSR_RW, 0x00000000 },
  { "lv_rst_rtc", 0x00000008, 24, 24,   CSR_RW, 0x00000000 },
  { "LVRST_AON_0", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_aon_1
  { "lv_rst_amc", 0x0000000C,  0,  0,   CSR_RW, 0x00000000 },
  { "lv_rst_irq", 0x0000000C,  8,  8,   CSR_RW, 0x00000000 },
  { "lv_rst_dvp", 0x0000000C, 16, 16,   CSR_RW, 0x00000000 },
  { "LVRST_AON_1", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_reserved_0
  { "reserved_0", 0x00000018, 15,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED_0", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_reserved_1
  { "reserved_1", 0x0000001C, 15,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED_1", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_RST_AON_H_
