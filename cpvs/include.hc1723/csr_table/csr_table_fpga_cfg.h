#ifndef CSR_TABLE_FPGA_CFG_H_
#define CSR_TABLE_FPGA_CFG_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_fpga_cfg[] =
{
  // WORD fpga_ver
  { "fpga_version", 0x00000000, 31,  0,   CSR_RO, 0x00000000 },
  { "FPGA_VER", 0x00000000, 31, 0,   CSR_RO, 0x00000000 },
  // WORD fpga_module
  { "fpga_module_en", 0x00000004, 31,  0,   CSR_RO, 0x00000000 },
  { "FPGA_MODULE", 0x00000004, 31, 0,   CSR_RO, 0x00000000 },
  // WORD fpga_gpio_o
  { "fpga_gpio_o_0", 0x00000008, 17,  0,   CSR_RW, 0x00000000 },
  { "FPGA_GPIO_O", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD fpga_gpio_oe
  { "fpga_gpio_oe_0", 0x0000000C, 17,  0,   CSR_RW, 0x00000000 },
  { "FPGA_GPIO_OE", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD fpga_gpio_i
  { "fpga_gpio_i_0", 0x00000010, 17,  0,   CSR_RO, 0x00000000 },
  { "FPGA_GPIO_I", 0x00000010, 31, 0,   CSR_RO, 0x00000000 },
  // WORD emac_mdc_iosel
  { "pad_emac_mdc_iosel", 0x00000014,  1,  0,   CSR_RW, 0x00000000 },
  { "EMAC_MDC_IOSEL", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_tx_ctl_iosel
  { "pad_emac_tx_ctl_iosel", 0x00000018,  1,  0,   CSR_RW, 0x00000000 },
  { "EMAC_TX_CTL_IOSEL", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_tx_o_clk_iosel
  { "pad_emac_tx_o_clk_iosel", 0x0000001C,  1,  0,   CSR_RW, 0x00000000 },
  { "EMAC_TX_O_CLK_IOSEL", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_tx_d0_iosel
  { "pad_emac_tx_d0_iosel", 0x00000020,  1,  0,   CSR_RW, 0x00000000 },
  { "EMAC_TX_D0_IOSEL", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_tx_d1_iosel
  { "pad_emac_tx_d1_iosel", 0x00000024,  1,  0,   CSR_RW, 0x00000000 },
  { "EMAC_TX_D1_IOSEL", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io82_iosel
  { "pad_io82_iosel", 0x00000028,  1,  0,   CSR_RW, 0x00000000 },
  { "IO82_IOSEL", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io81_iosel
  { "pad_io81_iosel", 0x0000002C,  1,  0,   CSR_RW, 0x00000000 },
  { "IO81_IOSEL", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io80_iosel
  { "pad_io80_iosel", 0x00000030,  1,  0,   CSR_RW, 0x00000000 },
  { "IO80_IOSEL", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io79_iosel
  { "pad_io79_iosel", 0x00000034,  1,  0,   CSR_RW, 0x00000000 },
  { "IO79_IOSEL", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io78_iosel
  { "pad_io78_iosel", 0x00000038,  1,  0,   CSR_RW, 0x00000000 },
  { "IO78_IOSEL", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io77_iosel
  { "pad_io77_iosel", 0x0000003C,  1,  0,   CSR_RW, 0x00000000 },
  { "IO77_IOSEL", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io48_iosel
  { "pad_io48_iosel", 0x00000040,  1,  0,   CSR_RW, 0x00000000 },
  { "IO48_IOSEL", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io47_iosel
  { "pad_io47_iosel", 0x00000044,  1,  0,   CSR_RW, 0x00000000 },
  { "IO47_IOSEL", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io46_iosel
  { "pad_io46_iosel", 0x00000048,  1,  0,   CSR_RW, 0x00000000 },
  { "IO46_IOSEL", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io45_iosel
  { "pad_io45_iosel", 0x0000004C,  1,  0,   CSR_RW, 0x00000000 },
  { "IO45_IOSEL", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io76_iosel
  { "pad_io76_iosel", 0x00000050,  1,  0,   CSR_RW, 0x00000000 },
  { "IO76_IOSEL", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io75_iosel
  { "pad_io75_iosel", 0x00000054,  1,  0,   CSR_RW, 0x00000000 },
  { "IO75_IOSEL", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io74_iosel
  { "pad_io74_iosel", 0x00000058,  1,  0,   CSR_RW, 0x00000000 },
  { "IO74_IOSEL", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io73_iosel
  { "pad_io73_iosel", 0x0000005C,  1,  0,   CSR_RW, 0x00000000 },
  { "IO73_IOSEL", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io72_iosel
  { "pad_io72_iosel", 0x00000060,  1,  0,   CSR_RW, 0x00000000 },
  { "IO72_IOSEL", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io71_iosel
  { "pad_io71_iosel", 0x00000064,  1,  0,   CSR_RW, 0x00000000 },
  { "IO71_IOSEL", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io70_iosel
  { "pad_io70_iosel", 0x00000068,  1,  0,   CSR_RW, 0x00000000 },
  { "IO70_IOSEL", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io69_iosel
  { "pad_io69_iosel", 0x0000006C,  1,  0,   CSR_RW, 0x00000000 },
  { "IO69_IOSEL", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io44_iosel
  { "pad_io44_iosel", 0x00000070,  1,  0,   CSR_RW, 0x00000000 },
  { "IO44_IOSEL", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io43_iosel
  { "pad_io43_iosel", 0x00000074,  1,  0,   CSR_RW, 0x00000000 },
  { "IO43_IOSEL", 0x00000074, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io42_iosel
  { "pad_io42_iosel", 0x00000078,  1,  0,   CSR_RW, 0x00000000 },
  { "IO42_IOSEL", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io41_iosel
  { "pad_io41_iosel", 0x0000007C,  1,  0,   CSR_RW, 0x00000000 },
  { "IO41_IOSEL", 0x0000007C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io40_iosel
  { "pad_io40_iosel", 0x00000080,  1,  0,   CSR_RW, 0x00000000 },
  { "IO40_IOSEL", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io39_iosel
  { "pad_io39_iosel", 0x00000084,  1,  0,   CSR_RW, 0x00000000 },
  { "IO39_IOSEL", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tc_sensor_clk_iosel
  { "pad_tc_sensor_clk_iosel", 0x00000088,  1,  0,   CSR_RW, 0x00000000 },
  { "TC_SENSOR_CLK_IOSEL", 0x00000088, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io83_iosel
  { "pad_io83_iosel", 0x0000008C,  1,  0,   CSR_RW, 0x00000000 },
  { "IO83_IOSEL", 0x0000008C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io84_iosel
  { "pad_io84_iosel", 0x00000090,  1,  0,   CSR_RW, 0x00000000 },
  { "IO84_IOSEL", 0x00000090, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tc_disp_reset_iosel
  { "pad_tc_disp_reset_iosel", 0x00000094,  1,  0,   CSR_RW, 0x00000000 },
  { "TC_DISP_RESET_IOSEL", 0x00000094, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io7_iosel
  { "pad_io7_iosel", 0x00000098,  1,  0,   CSR_RW, 0x00000000 },
  { "IO7_IOSEL", 0x00000098, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io14_iosel
  { "pad_io14_iosel", 0x0000009C,  1,  0,   CSR_RW, 0x00000000 },
  { "IO14_IOSEL", 0x0000009C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io13_iosel
  { "pad_io13_iosel", 0x000000A0,  1,  0,   CSR_RW, 0x00000000 },
  { "IO13_IOSEL", 0x000000A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io12_iosel
  { "pad_io12_iosel", 0x000000A4,  1,  0,   CSR_RW, 0x00000000 },
  { "IO12_IOSEL", 0x000000A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io11_iosel
  { "pad_io11_iosel", 0x000000A8,  1,  0,   CSR_RW, 0x00000000 },
  { "IO11_IOSEL", 0x000000A8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io10_iosel
  { "pad_io10_iosel", 0x000000AC,  1,  0,   CSR_RW, 0x00000000 },
  { "IO10_IOSEL", 0x000000AC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io9_iosel
  { "pad_io9_iosel", 0x000000B0,  1,  0,   CSR_RW, 0x00000000 },
  { "IO9_IOSEL", 0x000000B0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io8_iosel
  { "pad_io8_iosel", 0x000000B4,  1,  0,   CSR_RW, 0x00000000 },
  { "IO8_IOSEL", 0x000000B4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io68_iosel
  { "pad_io68_iosel", 0x000000B8,  1,  0,   CSR_RW, 0x00000000 },
  { "IO68_IOSEL", 0x000000B8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io67_iosel
  { "pad_io67_iosel", 0x000000BC,  1,  0,   CSR_RW, 0x00000000 },
  { "IO67_IOSEL", 0x000000BC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io66_iosel
  { "pad_io66_iosel", 0x000000C0,  1,  0,   CSR_RW, 0x00000000 },
  { "IO66_IOSEL", 0x000000C0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io65_iosel
  { "pad_io65_iosel", 0x000000C4,  1,  0,   CSR_RW, 0x00000000 },
  { "IO65_IOSEL", 0x000000C4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io64_iosel
  { "pad_io64_iosel", 0x000000C8,  1,  0,   CSR_RW, 0x00000000 },
  { "IO64_IOSEL", 0x000000C8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io63_iosel
  { "pad_io63_iosel", 0x000000CC,  1,  0,   CSR_RW, 0x00000000 },
  { "IO63_IOSEL", 0x000000CC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io62_iosel
  { "pad_io62_iosel", 0x000000D0,  1,  0,   CSR_RW, 0x00000000 },
  { "IO62_IOSEL", 0x000000D0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io61_iosel
  { "pad_io61_iosel", 0x000000D4,  1,  0,   CSR_RW, 0x00000000 },
  { "IO61_IOSEL", 0x000000D4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io60_iosel
  { "pad_io60_iosel", 0x000000D8,  1,  0,   CSR_RW, 0x00000000 },
  { "IO60_IOSEL", 0x000000D8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io59_iosel
  { "pad_io59_iosel", 0x000000DC,  1,  0,   CSR_RW, 0x00000000 },
  { "IO59_IOSEL", 0x000000DC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io38_iosel
  { "pad_io38_iosel", 0x000000E0,  1,  0,   CSR_RW, 0x00000000 },
  { "IO38_IOSEL", 0x000000E0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io37_iosel
  { "pad_io37_iosel", 0x000000E4,  1,  0,   CSR_RW, 0x00000000 },
  { "IO37_IOSEL", 0x000000E4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io36_iosel
  { "pad_io36_iosel", 0x000000E8,  1,  0,   CSR_RW, 0x00000000 },
  { "IO36_IOSEL", 0x000000E8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io35_iosel
  { "pad_io35_iosel", 0x000000EC,  1,  0,   CSR_RW, 0x00000000 },
  { "IO35_IOSEL", 0x000000EC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io34_iosel
  { "pad_io34_iosel", 0x000000F0,  1,  0,   CSR_RW, 0x00000000 },
  { "IO34_IOSEL", 0x000000F0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io33_iosel
  { "pad_io33_iosel", 0x000000F4,  1,  0,   CSR_RW, 0x00000000 },
  { "IO33_IOSEL", 0x000000F4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io32_iosel
  { "pad_io32_iosel", 0x000000F8,  1,  0,   CSR_RW, 0x00000000 },
  { "IO32_IOSEL", 0x000000F8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io31_iosel
  { "pad_io31_iosel", 0x000000FC,  1,  0,   CSR_RW, 0x00000000 },
  { "IO31_IOSEL", 0x000000FC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io30_iosel
  { "pad_io30_iosel", 0x00000100,  1,  0,   CSR_RW, 0x00000000 },
  { "IO30_IOSEL", 0x00000100, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io29_iosel
  { "pad_io29_iosel", 0x00000104,  1,  0,   CSR_RW, 0x00000000 },
  { "IO29_IOSEL", 0x00000104, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io28_iosel
  { "pad_io28_iosel", 0x00000108,  1,  0,   CSR_RW, 0x00000000 },
  { "IO28_IOSEL", 0x00000108, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io27_iosel
  { "pad_io27_iosel", 0x0000010C,  1,  0,   CSR_RW, 0x00000000 },
  { "IO27_IOSEL", 0x0000010C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io58_iosel
  { "pad_io58_iosel", 0x00000110,  1,  0,   CSR_RW, 0x00000000 },
  { "IO58_IOSEL", 0x00000110, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io57_iosel
  { "pad_io57_iosel", 0x00000114,  1,  0,   CSR_RW, 0x00000000 },
  { "IO57_IOSEL", 0x00000114, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io56_iosel
  { "pad_io56_iosel", 0x00000118,  1,  0,   CSR_RW, 0x00000000 },
  { "IO56_IOSEL", 0x00000118, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io55_iosel
  { "pad_io55_iosel", 0x0000011C,  1,  0,   CSR_RW, 0x00000000 },
  { "IO55_IOSEL", 0x0000011C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io54_iosel
  { "pad_io54_iosel", 0x00000120,  1,  0,   CSR_RW, 0x00000000 },
  { "IO54_IOSEL", 0x00000120, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io53_iosel
  { "pad_io53_iosel", 0x00000124,  1,  0,   CSR_RW, 0x00000000 },
  { "IO53_IOSEL", 0x00000124, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io52_iosel
  { "pad_io52_iosel", 0x00000128,  1,  0,   CSR_RW, 0x00000000 },
  { "IO52_IOSEL", 0x00000128, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io51_iosel
  { "pad_io51_iosel", 0x0000012C,  1,  0,   CSR_RW, 0x00000000 },
  { "IO51_IOSEL", 0x0000012C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io50_iosel
  { "pad_io50_iosel", 0x00000130,  1,  0,   CSR_RW, 0x00000000 },
  { "IO50_IOSEL", 0x00000130, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io49_iosel
  { "pad_io49_iosel", 0x00000134,  1,  0,   CSR_RW, 0x00000000 },
  { "IO49_IOSEL", 0x00000134, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io26_iosel
  { "pad_io26_iosel", 0x00000138,  1,  0,   CSR_RW, 0x00000000 },
  { "IO26_IOSEL", 0x00000138, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io25_iosel
  { "pad_io25_iosel", 0x0000013C,  1,  0,   CSR_RW, 0x00000000 },
  { "IO25_IOSEL", 0x0000013C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io24_iosel
  { "pad_io24_iosel", 0x00000140,  1,  0,   CSR_RW, 0x00000000 },
  { "IO24_IOSEL", 0x00000140, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io23_iosel
  { "pad_io23_iosel", 0x00000144,  1,  0,   CSR_RW, 0x00000000 },
  { "IO23_IOSEL", 0x00000144, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io22_iosel
  { "pad_io22_iosel", 0x00000148,  1,  0,   CSR_RW, 0x00000000 },
  { "IO22_IOSEL", 0x00000148, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io21_iosel
  { "pad_io21_iosel", 0x0000014C,  1,  0,   CSR_RW, 0x00000000 },
  { "IO21_IOSEL", 0x0000014C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io20_iosel
  { "pad_io20_iosel", 0x00000150,  1,  0,   CSR_RW, 0x00000000 },
  { "IO20_IOSEL", 0x00000150, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io19_iosel
  { "pad_io19_iosel", 0x00000154,  1,  0,   CSR_RW, 0x00000000 },
  { "IO19_IOSEL", 0x00000154, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io18_iosel
  { "pad_io18_iosel", 0x00000158,  1,  0,   CSR_RW, 0x00000000 },
  { "IO18_IOSEL", 0x00000158, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io17_iosel
  { "pad_io17_iosel", 0x0000015C,  1,  0,   CSR_RW, 0x00000000 },
  { "IO17_IOSEL", 0x0000015C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io16_iosel
  { "pad_io16_iosel", 0x00000160,  1,  0,   CSR_RW, 0x00000000 },
  { "IO16_IOSEL", 0x00000160, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io15_iosel
  { "pad_io15_iosel", 0x00000164,  1,  0,   CSR_RW, 0x00000000 },
  { "IO15_IOSEL", 0x00000164, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io6_iosel
  { "pad_io6_iosel", 0x00000168,  1,  0,   CSR_RW, 0x00000000 },
  { "IO6_IOSEL", 0x00000168, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io5_iosel
  { "pad_io5_iosel", 0x0000016C,  1,  0,   CSR_RW, 0x00000000 },
  { "IO5_IOSEL", 0x0000016C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io4_iosel
  { "pad_io4_iosel", 0x00000170,  1,  0,   CSR_RW, 0x00000000 },
  { "IO4_IOSEL", 0x00000170, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io3_iosel
  { "pad_io3_iosel", 0x00000174,  1,  0,   CSR_RW, 0x00000000 },
  { "IO3_IOSEL", 0x00000174, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io2_iosel
  { "pad_io2_iosel", 0x00000178,  1,  0,   CSR_RW, 0x00000000 },
  { "IO2_IOSEL", 0x00000178, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io1_iosel
  { "pad_io1_iosel", 0x0000017C,  1,  0,   CSR_RW, 0x00000000 },
  { "IO1_IOSEL", 0x0000017C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD io0_iosel
  { "pad_io0_iosel", 0x00000180,  1,  0,   CSR_RW, 0x00000000 },
  { "IO0_IOSEL", 0x00000180, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio0_iosel
  { "pad_fpga_gpio0_iosel", 0x00000184,  1,  0,   CSR_RW, 0x00000000 },
  { "GPIO0_IOSEL", 0x00000184, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio1_iosel
  { "pad_fpga_gpio1_iosel", 0x00000188,  1,  0,   CSR_RW, 0x00000000 },
  { "GPIO1_IOSEL", 0x00000188, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio2_iosel
  { "pad_fpga_gpio2_iosel", 0x0000018C,  1,  0,   CSR_RW, 0x00000000 },
  { "GPIO2_IOSEL", 0x0000018C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio3_iosel
  { "pad_fpga_gpio3_iosel", 0x00000190,  1,  0,   CSR_RW, 0x00000000 },
  { "GPIO3_IOSEL", 0x00000190, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio4_iosel
  { "pad_fpga_gpio4_iosel", 0x00000194,  1,  0,   CSR_RW, 0x00000000 },
  { "GPIO4_IOSEL", 0x00000194, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio5_iosel
  { "pad_fpga_gpio5_iosel", 0x00000198,  1,  0,   CSR_RW, 0x00000000 },
  { "GPIO5_IOSEL", 0x00000198, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio10_iosel
  { "pad_fpga_gpio10_iosel", 0x0000019C,  1,  0,   CSR_RW, 0x00000000 },
  { "GPIO10_IOSEL", 0x0000019C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio11_iosel
  { "pad_fpga_gpio11_iosel", 0x000001A0,  1,  0,   CSR_RW, 0x00000000 },
  { "GPIO11_IOSEL", 0x000001A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio12_iosel
  { "pad_fpga_gpio12_iosel", 0x000001A4,  1,  0,   CSR_RW, 0x00000000 },
  { "GPIO12_IOSEL", 0x000001A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio13_iosel
  { "pad_fpga_gpio13_iosel", 0x000001A8,  1,  0,   CSR_RW, 0x00000000 },
  { "GPIO13_IOSEL", 0x000001A8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio14_iosel
  { "pad_fpga_gpio14_iosel", 0x000001AC,  1,  0,   CSR_RW, 0x00000000 },
  { "GPIO14_IOSEL", 0x000001AC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio15_iosel
  { "pad_fpga_gpio15_iosel", 0x000001B0,  1,  0,   CSR_RW, 0x00000000 },
  { "GPIO15_IOSEL", 0x000001B0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio16_iosel
  { "pad_fpga_gpio16_iosel", 0x000001B4,  1,  0,   CSR_RW, 0x00000000 },
  { "GPIO16_IOSEL", 0x000001B4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio17_iosel
  { "pad_fpga_gpio17_iosel", 0x000001B8,  1,  0,   CSR_RW, 0x00000000 },
  { "GPIO17_IOSEL", 0x000001B8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi_d3_iosel
  { "pad_qspi_d3_iosel", 0x000001BC,  1,  0,   CSR_RW, 0x00000000 },
  { "QSPI_D3_IOSEL", 0x000001BC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi_d2_iosel
  { "pad_qspi_d2_iosel", 0x000001C0,  1,  0,   CSR_RW, 0x00000000 },
  { "QSPI_D2_IOSEL", 0x000001C0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi_d1_iosel
  { "pad_qspi_d1_iosel", 0x000001C4,  1,  0,   CSR_RW, 0x00000000 },
  { "QSPI_D1_IOSEL", 0x000001C4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi_d0_iosel
  { "pad_qspi_d0_iosel", 0x000001C8,  1,  0,   CSR_RW, 0x00000000 },
  { "QSPI_D0_IOSEL", 0x000001C8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi_ck_iosel
  { "pad_qspi_ck_iosel", 0x000001CC,  1,  0,   CSR_RW, 0x00000000 },
  { "QSPI_CK_IOSEL", 0x000001CC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sd0_d3_iosel
  { "pad_sd0_d3_iosel", 0x000001D0,  1,  0,   CSR_RW, 0x00000000 },
  { "SD0_D3_IOSEL", 0x000001D0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sd0_d2_iosel
  { "pad_sd0_d2_iosel", 0x000001D4,  1,  0,   CSR_RW, 0x00000000 },
  { "SD0_D2_IOSEL", 0x000001D4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD uart0_txd_iosel
  { "pad_uart0_txd_iosel", 0x000001D8,  1,  0,   CSR_RW, 0x00000000 },
  { "UART0_TXD_IOSEL", 0x000001D8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD uart0_rxd_iosel
  { "pad_uart0_rxd_iosel", 0x000001DC,  1,  0,   CSR_RW, 0x00000000 },
  { "UART0_RXD_IOSEL", 0x000001DC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD uart0_rts_iosel
  { "pad_uart0_rts_iosel", 0x000001E0,  1,  0,   CSR_RW, 0x00000000 },
  { "UART0_RTS_IOSEL", 0x000001E0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD uart0_cts_iosel
  { "pad_uart0_cts_iosel", 0x000001E4,  1,  0,   CSR_RW, 0x00000000 },
  { "UART0_CTS_IOSEL", 0x000001E4, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_FPGA_CFG_H_
