#ifndef CSR_TABLE_ADCCFG_H_
#define CSR_TABLE_ADCCFG_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_adccfg[] =
{
  // WORD adccfg00
  { "adc_enadc", 0x00000000,  0,  0,   CSR_RW, 0x00000000 },
  { "ADCCFG00", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adccfg01
  { "adc_selres", 0x00000004,  1,  0,   CSR_RW, 0x00000000 },
  { "ADCCFG01", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adccfg02
  { "adc_enctr", 0x00000008, 18, 16,   CSR_RW, 0x00000000 },
  { "ADCCFG02", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adccfg03
  { "adc_out_mon", 0x0000000C, 11,  0,   CSR_RO, 0x00000000 },
  { "adc_ctrl_mon", 0x0000000C, 22, 16,   CSR_RO, 0x00000000 },
  { "ADCCFG03", 0x0000000C, 31, 0,   CSR_RO, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_ADCCFG_H_
