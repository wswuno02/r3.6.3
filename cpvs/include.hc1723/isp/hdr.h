#ifndef KYOTO_HDR_H_
#define KYOTO_HDR_H_

#include "isp_utils.h"
#include "csr_bank_hdr.h"

typedef enum isp_hdr_mode {
	ISP_HDR_MODE_NORMAL = 0,
	ISP_HDR_MODE_MONO_NORMAL = 1,
	ISP_HDR_MODE_BYPASS_SE = 2,
	ISP_HDR_MODE_BYPASS_LE = 3,
	ISP_HDR_MODE_NUM,
} IspHdrMode;

typedef enum isp_hdr_anti_gma_mode {
	ISP_HDR_ANTI_GMA_MODE_NORMAL = 0,
	ISP_HDR_ANTI_GMA_MODE_DISABLE = 1,
	ISP_HDR_ANTI_GMA_MODE_NUM = 2,
} IspHdrAntiGmaMode;

typedef struct isp_hdr_anti_gma_cfg {
	enum isp_hdr_anti_gma_mode long_exp_mode; /* Long Exposure Anti-Gamma Mode */
	enum isp_hdr_anti_gma_mode short_exp_mode; /* Short Exposure Anti-Gamma Mode */
} IspHdrAntiGmaCfg;

typedef struct isp_hdr_exp_cfg {
	uint8_t exp_long_early;
	uint16_t exp_ratio;
	uint16_t exp_ratio_inv;
	uint16_t se_exp_ratio_min_int;
} IspHdrExpCfg;

typedef struct isp_hdr_weight_cfg {
	uint16_t se_weight_th_min;
	uint8_t se_weight_slope;
	uint8_t se_weight_min;
	uint8_t se_weight_max;
	uint16_t le_weight_th_max;
	uint8_t le_weight_slope;
	uint8_t le_weight_min;
	uint8_t le_weight_max;
} IspHdrWeightCfg;

typedef struct isp_hdr_fb_cfg {
	uint16_t local_fb_th;
	uint8_t local_fb_slope;
	uint8_t local_fb_min;
	uint8_t local_fb_max;
	uint8_t frame_fb_strength;
	uint8_t fb_target;
	uint8_t fb_alpha;
	uint8_t mismatch_var_gain_2s;
} IspHdrFbCfg;

typedef struct isp_hdr_tm_cfg {
	uint8_t enable; /* HDR Tone Mapping Enable */
	struct isp_hte_curve {
		uint16_t val[HDR_TM_CURVE_ENTRY_NUM];
	} curve; /**< HDR Tone Mapping curve */
} IspHdrTmCfg;

typedef struct isp_hdr_awb_cfg {
	uint8_t enable;
	uint16_t gain[HDR_AWB_CHN_NUM]; /* HDR White Balance Gain */
} IspHdrAwbCfg;

typedef enum isp_hdr_gma_mode {
	ISP_HDR_GMA_MODE_NORMAL = 0,
	ISP_HDR_GMA_MODE_DISABLE = 1,
	ISP_HDR_GMA_MODE_NUM = 2,
} IspHdrGmaMode;

typedef struct isp_hdr_gma_cfg {
	enum isp_hdr_gma_mode mode; /* HDR Gamma Mode	*/
} IspHdrGmaCfg;

/* Frame start */
void hw_isp_hdr_set_frame_start(void);

/* IRQ clear */
void hw_isp_hdr_set_irq_clear_frame_end(void);

/* Frame end*/
uint8_t hw_isp_hdr_get_status_frame_end(void);

/* IRQ mask */
void hw_isp_hdr_set_irq_mask_frame_end(uint8_t irq_mask_frame_end);
uint8_t hw_isp_hdr_get_irq_mask_frame_end(void);

/* Resolution */
void hw_isp_hdr_set_res(const struct res *res);
void hw_isp_hdr_get_res(struct res *res);

/* Bayer */
void hw_isp_hdr_set_ini_bayer_phase(enum ini_bayer_phase bayer);
enum ini_bayer_phase hw_isp_hdr_get_ini_bayer_phase(void);

/* HDR */
void hw_isp_hdr_set_mode(enum isp_hdr_mode mode);
enum isp_hdr_mode hw_isp_hdr_get_mode(void);
void hw_isp_hdr_set_en(uint8_t hdr_en);
uint8_t hw_isp_hdr_get_en(void);
void hw_isp_hdr_set_anti_gma_cfg(const struct isp_hdr_anti_gma_cfg *cfg);
void hw_isp_hdr_get_anti_gma_cfg(struct isp_hdr_anti_gma_cfg *cfg);
void hw_isp_hdr_set_exp_cfg(const struct isp_hdr_exp_cfg *cfg);
void hw_isp_hdr_get_exp_cfg(struct isp_hdr_exp_cfg *cfg);
void hw_isp_hdr_set_weight_cfg(const struct isp_hdr_weight_cfg *cfg);
void hw_isp_hdr_get_weight_cfg(struct isp_hdr_weight_cfg *cfg);
void hw_isp_hdr_set_fb_cfg(const struct isp_hdr_fb_cfg *cfg);
void hw_isp_hdr_get_fb_cfg(struct isp_hdr_fb_cfg *cfg);
void hw_isp_hdr_set_tm_cfg(const struct isp_hdr_tm_cfg *cfg);
void hw_isp_hdr_get_tm_cfg(struct isp_hdr_tm_cfg *cfg);
void hw_isp_hdr_set_awb_cfg(const struct isp_hdr_awb_cfg *cfg);
void hw_isp_hdr_get_awb_cfg(struct isp_hdr_awb_cfg *cfg);
void hw_isp_hdr_set_gma_cfg(const struct isp_hdr_gma_cfg *cfg);
void hw_isp_hdr_get_gma_cfg(struct isp_hdr_gma_cfg *cfg);

/* Debug */
void hw_isp_hdr_set_dbg_mon_sel(uint8_t debug_mon_sel);
uint8_t hw_isp_hdr_get_dbg_mon_sel(void);

/* Efuse */
uint8_t hw_isp_hdr_get_efuse_dis_hdr_violation(void);

/* Reserved */
void hw_isp_hdr_set_reserved_0(uint32_t reserved_0);
uint32_t hw_isp_hdr_get_reserved_0(void);
void hw_isp_hdr_set_reserved_1(uint32_t reserved_1);
uint32_t hw_isp_hdr_get_reserved_1(void);

//
/* IRQ mask */
void isp_hdr_set_irq_mask_frame_end(volatile CsrBankHdr *csr, uint8_t irq_mask_frame_end);
uint8_t isp_hdr_get_irq_mask_frame_end(volatile CsrBankHdr *csr);

/* Resolution */
void isp_hdr_set_res(volatile CsrBankHdr *csr, const struct res *res);
void isp_hdr_get_res(volatile CsrBankHdr *csr, struct res *res);

/* Bayer */
void isp_hdr_set_ini_bayer_phase(volatile CsrBankHdr *csr, enum ini_bayer_phase bayer);
enum ini_bayer_phase isp_hdr_get_ini_bayer_phase(volatile CsrBankHdr *csr);

/* HDR */
void isp_hdr_set_mode(volatile CsrBankHdr *csr, enum isp_hdr_mode mode);
enum isp_hdr_mode isp_hdr_get_mode(volatile CsrBankHdr *csr);
void isp_hdr_set_en(volatile CsrBankHdr *csr, uint8_t hdr_en);
uint8_t isp_hdr_get_en(volatile CsrBankHdr *csr);
void isp_hdr_set_anti_gma_cfg(volatile CsrBankHdr *csr, const struct isp_hdr_anti_gma_cfg *cfg);
void isp_hdr_get_anti_gma_cfg(volatile CsrBankHdr *csr, struct isp_hdr_anti_gma_cfg *cfg);
void isp_hdr_set_exp_cfg(volatile CsrBankHdr *csr, const struct isp_hdr_exp_cfg *cfg);
void isp_hdr_get_exp_cfg(volatile CsrBankHdr *csr, struct isp_hdr_exp_cfg *cfg);
void isp_hdr_set_weight_cfg(volatile CsrBankHdr *csr, const struct isp_hdr_weight_cfg *cfg);
void isp_hdr_get_weight_cfg(volatile CsrBankHdr *csr, struct isp_hdr_weight_cfg *cfg);
void isp_hdr_set_fb_cfg(volatile CsrBankHdr *csr, const struct isp_hdr_fb_cfg *cfg);
void isp_hdr_get_fb_cfg(volatile CsrBankHdr *csr, struct isp_hdr_fb_cfg *cfg);
void isp_hdr_set_tm_cfg(volatile CsrBankHdr *csr, const struct isp_hdr_tm_cfg *cfg);
void isp_hdr_get_tm_cfg(volatile CsrBankHdr *csr, struct isp_hdr_tm_cfg *cfg);
void isp_hdr_set_awb_cfg(volatile CsrBankHdr *csr, const struct isp_hdr_awb_cfg *cfg);
void isp_hdr_get_awb_cfg(volatile CsrBankHdr *csr, struct isp_hdr_awb_cfg *cfg);
void isp_hdr_set_gma_cfg(volatile CsrBankHdr *csr, const struct isp_hdr_gma_cfg *cfg);
void isp_hdr_get_gma_cfg(volatile CsrBankHdr *csr, struct isp_hdr_gma_cfg *cfg);

/* Debug */
void isp_hdr_set_dbg_mon_sel(volatile CsrBankHdr *csr, uint8_t debug_mon_sel);
uint8_t isp_hdr_get_dbg_mon_sel(volatile CsrBankHdr *csr);

/* Efuse */
uint8_t isp_hdr_get_efuse_dis_hdr_violation(volatile CsrBankHdr *csr);

/* Reserved */
void isp_hdr_set_reserved_0(volatile CsrBankHdr *csr, uint32_t reserved_0);
uint32_t isp_hdr_get_reserved_0(volatile CsrBankHdr *csr);
void isp_hdr_set_reserved_1(volatile CsrBankHdr *csr, uint32_t reserved_1);
uint32_t isp_hdr_get_reserved_1(volatile CsrBankHdr *csr);

/* TODO - Move to test */
void hw_isp_hdr_res(uint32_t width, uint32_t height);
void hw_isp_hdr_bypass(void);

#endif /* KYOTO_HDR_H_ */