#ifndef KYOTO_FGMA_H_
#define KYOTO_FGMA_H_

#include "is_utils.h"
#include "csr_bank_isk.h"

typedef enum is_fgma_mode {
	IS_FGMA_MODE_NORMAL = 0,
	IS_FGMA_MODE_DISABLE = 1,
	IS_FGMA_MODE_NUM = 2,
} IsFgmaMode;

typedef struct is_fgma_cfg {
	enum is_fgma_mode mode;
} IsFgmaCfg;

/* FGMA */
void hw_is_fgma_set_cfg(const enum is_kel_no no, const struct is_fgma_cfg *cfg);
void hw_is_fgma_get_cfg(const enum is_kel_no no, struct is_fgma_cfg *cfg);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//* for mpp is *//

/* FGMA */
void is_fgma_set_cfg(volatile CsrBankIsk *csr, const struct is_fgma_cfg *cfg);
void is_fgma_get_cfg(volatile CsrBankIsk *csr, struct is_fgma_cfg *cfg);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//* need to remove *//
// Move to test

typedef struct is_fgma_ctx {
	uint32_t fgma_mode;

} IsFgmaCtx;

typedef struct is_fgma_attr {
	uint32_t fgma_mode;

} IsFgmaAttr;

void hw_is_fgma_bypass(void);
void hw_is_fgma_run(IsFgmaAttr *fgmaattr);
void hw_is_fgma_set_ctx(IsFgmaCtx *fgmactx, IsFgmaAttr *fgmaattr);
void hw_is_fgma_set_reg(IsFgmaCtx *fgmactx);

#endif /* KYOTO_FGMA_H_ */