#ifndef KYOTO_DBC_H_
#define KYOTO_DBC_H_

#include "is_utils.h"
#include "csr_bank_dbc.h"

typedef enum is_dbc_mode {
	IS_DBC_MODE_NORMAL = 0,
	IS_DBC_MODE_DISABLE = 1,
	IS_DBC_MODE_NUM = 2,
} IsDbcMode;

typedef struct is_dbc_cfg {
	enum is_dbc_mode mode;
	uint16_t level[CFA_PHASE_NUM];
} IsDbcCfg;

/* Frame start */
void hw_is_dbc_set_frame_start(const enum is_kel_no no);

/* IRQ clear */
void hw_is_dbc_set_irq_clear_frame_end(const enum is_kel_no no);

/* Frame end*/
uint8_t hw_is_dbc_get_status_frame_end(const enum is_kel_no no);

/* IRQ mask */
void hw_is_dbc_set_irq_mask_frame_end(const enum is_kel_no no, uint8_t irq_mask_frame_end);
uint8_t hw_is_dbc_get_irq_mask_frame_end(const enum is_kel_no no);

/* Resolution */
void hw_is_dbc_set_res(const enum is_kel_no no, const struct res *res);
void hw_is_dbc_get_res(const enum is_kel_no no, struct res *res);

/* Input format */
void hw_is_dbc_set_input_format(const enum is_kel_no no, const struct is_input_format *cfg);
void hw_is_dbc_get_input_format(const enum is_kel_no no, struct is_input_format *cfg);

/* DBC */
void hw_is_dbc_set_cfg(const enum is_kel_no no, const struct is_dbc_cfg *cfg);
void hw_is_dbc_get_cfg(const enum is_kel_no no, struct is_dbc_cfg *cfg);

/* Debug */
void hw_is_dbc_set_dbg_mon_sel(const enum is_kel_no no, uint8_t debug_mon_sel);
uint8_t hw_is_dbc_get_dbg_mon_sel(const enum is_kel_no no);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//* for mpp is *//

/* IRQ mask */
void is_dbc_set_irq_mask_frame_end(volatile CsrBankDbc *csr, uint8_t irq_mask_frame_end);
uint8_t is_dbc_get_irq_mask_frame_end(volatile CsrBankDbc *csr);

/* Resolution */
void is_dbc_set_res(volatile CsrBankDbc *csr, const struct res *res);
void is_dbc_get_res(volatile CsrBankDbc *csr, struct res *res);

/* Input format */
void is_dbc_set_input_format(volatile CsrBankDbc *csr, const struct is_input_format *cfg);
void is_dbc_get_input_format(volatile CsrBankDbc *csr, struct is_input_format *cfg);

/* DBC */
void is_dbc_set_cfg(volatile CsrBankDbc *csr, const struct is_dbc_cfg *cfg);
void is_dbc_get_cfg(volatile CsrBankDbc *csr, struct is_dbc_cfg *cfg);

/* Debug */
void is_dbc_set_dbg_mon_sel(volatile CsrBankDbc *csr, uint8_t debug_mon_sel);
uint8_t is_dbc_get_dbg_mon_sel(volatile CsrBankDbc *csr);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//* need to remove *//
// Move to test

#define IS_DBC_CFA_PHASE_IDX 3
#define IS_DBC_CFA_PHASE_NUM 16

#define IS_DBC_LEVEL_IDX 4
#define IS_DBC_LEVEL_NUM 5

typedef struct is_dbc_resolution {
	uint32_t width;
	uint32_t height;
} IsDbcResolution;

typedef struct is_dbc_cfa_set {
	uint32_t cfa_mode;
	uint32_t bayer_ini_phase;
	uint32_t cfa_phase[IS_DBC_CFA_PHASE_NUM];
} IsDbcCfaSet;

typedef struct is_dbc_ctx {
	uint32_t dbc_mode;
	uint32_t dbc_level[IS_DBC_LEVEL_NUM];
	IsDbcResolution dbc_res;
	IsDbcCfaSet dbc_cfa_set;

} IsDbcCtx;

typedef struct is_dbc_attr {
	uint32_t dbc_width;
	uint32_t dbc_height;
	uint32_t dbc_mode;
	uint32_t dbc_cfa_mode;
	uint32_t dbc_bayer_ini_phase;
	uint32_t dbc_level_case_idx;
} IsDbcAttr;

void hw_is_dbc_frame_start(void);
void hw_is_dbc_irq_clear_frame_end(void);
uint32_t hw_is_dbc_status_frame_end(void);
void hw_is_dbc_irq_mask_frame_end(uint32_t irq_mask_frame_end);
void hw_is_dbc_resolution(uint32_t width, uint32_t height);
void hw_is_dbc_bypass(uint32_t cfa_mode, uint32_t bayer_ini_phase);
void hw_is_dbc_run(IsDbcAttr *dbcattr);
void hw_is_dbc_set_ctx(IsDbcCtx *dbcctx, IsDbcAttr *dbcattr);
void hw_is_dbc_set_reg(IsDbcCtx *dbcctx);
void hw_is_dbc_debug_mon_sel(uint32_t debug_mon_sel);

#endif /* KYOTO_DBC_H_ */
