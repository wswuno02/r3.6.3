#ifndef __HW_USB__H__
#define __HW_USB__H__

#include "address_map.h"
#include "csr_bank_usbcfg.h"
#include "csr_bank_rst.h"
#include "delay.h"

void hw_usb_phy_init(void);

#endif