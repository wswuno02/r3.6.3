/**
 * @file hw_fm.h
 * @brief frequency meter 
 */

#ifndef HW_FM_H
#define HW_FM_H

#include "csr_bank_fm.h"

#define XTAL_FREQ 24000000

void fm_start(volatile CsrBankFm *csr);
uint32_t fm_pass(volatile CsrBankFm *csr);
uint32_t fm_calc_real_freq(volatile CsrBankFm *csr);
void pr_fm(volatile CsrBankFm *fm, char *str);

/**
 *  target_freq : Hz,
 * 	tolerance : per mille (‰)
 */
int fm_set_parm(volatile CsrBankFm *csr, uint32_t target_freq, uint32_t tol);

#endif