#ifndef KYOTO_SHP_H_
#define KYOTO_SHP_H_

#include "isp_utils.h"
#include "csr_bank_shp.h"

typedef enum isp_shp_sample_mode {
	ISP_SHP_SAMPLE_MODE_YUV444 = 0,
	ISP_SHP_SAMPLE_MODE_YUV420 = 1,
	ISP_SHP_SAMPLE_MODE_NUM = 2,
} IspShpSampleMode;

typedef struct isp_shp_roi_level_gain_frame_cfg {
	uint8_t enable;
	uint16_t level_gain_2s;
	uint16_t sy;
	uint16_t ey;
} IspShpRoiLevelGainFrameCfg;

typedef struct isp_shp_roi_level_gain_tile_cfg {
	uint16_t sx;
	uint16_t ex;
} IspShpRoiLevelGainTileCfg;

typedef enum isp_shp_mode {
	ISP_SHP_MODE_NORMAL = 0,
	ISP_SHP_MODE_DISABLE = 1,
	ISP_SHP_MODE_BYPASS = 2,
	ISP_SHP_MODE_NUM = 3,
} IspShpMode;

typedef struct isp_shp_cfg {
	IspShpMode mode;
	uint8_t bpf_gain_2s;
	uint8_t hpf_gain_2s;
	uint16_t level_gain_2s;
	uint8_t soft_clip_slope;
	struct shp_gain_curve {
		uint16_t x[SHP_GAIN_CURVE_CTRL_POINT_NUM]; /**< Curve bin */
		uint8_t m_2s[SHP_GAIN_CURVE_CTRL_POINT_NUM - 1]; /**< Curve slpoe */
		uint16_t y[SHP_GAIN_CURVE_CTRL_POINT_NUM]; /**< Curve value */
	} gain_curve; /**< Gain curve */
	struct isp_shp_luma_ctrl_gain {
		uint16_t x[SHP_LUMA_CTRL_GAIN_CTRL_POINT_NUM]; /**< Curve bin */
		uint8_t m_2s[SHP_LUMA_CTRL_GAIN_CTRL_POINT_NUM - 1]; /**< Curve slpoe */
		uint16_t y[SHP_LUMA_CTRL_GAIN_CTRL_POINT_NUM]; /**< Curve value */
	} luma_ctrl_gain; /**< Luma control gain curve */
} IspShpCfg;

typedef struct isp_shp_roi_stat_frame_ctrl {
	uint16_t sy;
	uint16_t ey;
	uint32_t pix_num;
} IspShpRoiStatFrameCtrl;

typedef struct isp_shp_roi_stat_tile_ctrl {
	uint16_t sx;
	uint16_t ex;
} IspShpRoiStatTileCtrl;

typedef struct isp_shp_roi_stat {
	uint16_t lpf_luma_avg;
} IspShpRoiStat;

typedef struct isp_shp_rgl_stat_frame_ctrl {
	uint16_t sy;
	uint16_t ey;
	uint16_t rgl_y_lpf_rgl_wd;
	uint16_t rgl_y_lpf_rgl_ht;
	uint32_t pix_num;
} IspShpRglStatFrameCtrl;

typedef struct isp_shp_rgl_stat_tile_ctrl {
	uint16_t sx;
	uint16_t ex;
	uint16_t rgl_y_lpf_x_cnt_ini;
	uint16_t rgl_y_lpf_y_cnt_ini;
	uint8_t rgl_y_lpf_r_cnt_ini;
} IspShpRglStatTileCtrl;

typedef struct isp_shp_rgl_stat {
	uint16_t lpf_luma_min[SHP_MAX_RGL_NUM];
	uint16_t lpf_luma_avg[SHP_MAX_RGL_NUM];
} IspShpRglStat;

typedef struct isp_shp_demo {
	uint8_t enable;
	uint16_t x_min;
	uint16_t x_max;
	uint16_t y_min;
	uint16_t y_max;
} IspShpDemo;

/* Frame start */
void hw_isp_shp_set_frame_start(void);

/* IRQ clear */
void hw_isp_shp_set_irq_clear_frame_end(void);

/* Frame end*/
uint8_t hw_isp_shp_get_status_frame_end(void);

/* IRQ mask */
void hw_isp_shp_set_irq_mask_frame_end(uint8_t irq_mask_frame_end);
uint8_t hw_isp_shp_get_irq_mask_frame_end(void);

/* Input format */
void hw_isp_shp_set_sample_mode(enum isp_shp_sample_mode in);
enum isp_shp_sample_mode hw_isp_shp_get_sample_mode(void);

/* Resolution */
void hw_isp_shp_set_res(const struct res *res);
void hw_isp_shp_get_res(struct res *res);

/* SHP */
void hw_isp_shp_set_roi_level_gain_frame_cfg(uint8_t roi_idx, const struct isp_shp_roi_level_gain_frame_cfg *cfg);
void hw_isp_shp_get_roi_level_gain_frame_cfg(uint8_t roi_idx, struct isp_shp_roi_level_gain_frame_cfg *cfg);
void hw_isp_shp_set_roi_level_gain_tile_cfg(uint8_t roi_idx, const struct isp_shp_roi_level_gain_tile_cfg *cfg);
void hw_isp_shp_get_roi_level_gain_tile_cfg(uint8_t roi_idx, struct isp_shp_roi_level_gain_tile_cfg *cfg);
void hw_isp_shp_set_cfg(const struct isp_shp_cfg *cfg);
void hw_isp_shp_get_cfg(struct isp_shp_cfg *cfg);

/* Statistics */
// ROI
/*
 * The following sequence is used to obtain ROI statistics of SHP by frame
 *
 * for frame N
 *		hw_isp_shp_set_rgl_stat_enable(1);
 *		hw_isp_shp_set_rgl_stat_clear(1);
 *		hw_isp_shp_set_rgl_stat_clear(0);
 *		hw_isp_shp_set_roi_stat_frame_ctrl(&frame_cfg);
 *		for tile 0
 *				hw_isp_shp_set_roi_stat_tile_ctrl(&tile_cfg);
 *			tile 1
 *				hw_isp_shp_set_roi_stat_tile_ctrl(&tile_cfg);
 *			...
 *			tile M
 *				hw_isp_shp_set_roi_stat_tile_ctrl(&tile_cfg);
 *		hw_isp_shp_get_roi_stat(&stat);
 */
void hw_isp_shp_set_roi_stat_frame_ctrl(const struct isp_shp_roi_stat_frame_ctrl *cfg);
void hw_isp_shp_get_roi_stat_frame_ctrl(struct isp_shp_roi_stat_frame_ctrl *cfg);
void hw_isp_shp_set_roi_stat_tile_ctrl(const struct isp_shp_roi_stat_tile_ctrl *cfg);
void hw_isp_shp_get_roi_stat_tile_ctrl(struct isp_shp_roi_stat_tile_ctrl *cfg);
void hw_isp_shp_set_roi_stat_clear(uint8_t clear);
uint8_t hw_isp_shp_get_roi_stat_clear(void);
void hw_isp_shp_set_roi_stat_enable(uint8_t enable);
uint8_t hw_isp_shp_get_roi_stat_enable(void);
void hw_isp_shp_get_roi_stat(struct isp_shp_roi_stat *stat);
// RGL
/*
 * TO-DO: hw_isp_shp_get_rgl_stat?
 */
void hw_isp_shp_set_rgl_stat_frame_ctrl(const struct isp_shp_rgl_stat_frame_ctrl *cfg);
void hw_isp_shp_get_rgl_stat_frame_ctrl(struct isp_shp_rgl_stat_frame_ctrl *cfg);
void hw_isp_shp_set_rgl_stat_tile_ctrl(const struct isp_shp_rgl_stat_tile_ctrl *cfg);
void hw_isp_shp_get_rgl_stat_tile_ctrl(struct isp_shp_rgl_stat_tile_ctrl *cfg);
void hw_isp_shp_set_rgl_stat_clear(uint8_t clear);
uint8_t hw_isp_shp_get_rgl_stat_clear(void);
void hw_isp_shp_set_rgl_stat_enable(uint8_t enable);
uint8_t hw_isp_shp_get_rgl_stat_enable(void);
uint8_t hw_isp_shp_get_rgl_stat_num_of_rgl(void);
void hw_isp_shp_get_rgl_stat(uint8_t num_of_rgl, struct isp_shp_rgl_stat *stat);

/* Demo */
void hw_isp_shp_set_demo(const struct isp_shp_demo *demo);
void hw_isp_shp_get_demo(struct isp_shp_demo *demo);

/* Debug */
void hw_isp_shp_set_dbg_mon_sel(uint8_t debug_mon_sel);
uint8_t hw_isp_shp_get_dbg_mon_sel(void);

/* Reserved */
void hw_isp_shp_set_reserved(uint32_t reserved);
uint32_t hw_isp_shp_get_reserved(void);

/* ATPG */
void hw_isp_shp_set_atpg_ctrl_0(uint8_t atpg_ctrl_0);
uint8_t hw_isp_shp_get_atpg_ctrl_0(void);
void hw_isp_shp_set_atpg_ctrl_1(uint8_t atpg_ctrl_1);
uint8_t hw_isp_shp_get_atpg_ctrl_1(void);

//
/* IRQ mask */
void isp_shp_set_irq_mask_frame_end(volatile CsrBankShp *csr, uint8_t irq_mask_frame_end);
uint8_t isp_shp_get_irq_mask_frame_end(volatile CsrBankShp *csr);

/* Input format */
void isp_shp_set_sample_mode(volatile CsrBankShp *csr, enum isp_shp_sample_mode in);
enum isp_shp_sample_mode isp_shp_get_sample_mode(volatile CsrBankShp *csr);

/* Resolution */
void isp_shp_set_res(volatile CsrBankShp *csr, const struct res *res);
void isp_shp_get_res(volatile CsrBankShp *csr, struct res *res);

/* SHP */
void isp_shp_set_roi_level_gain_frame_cfg(volatile CsrBankShp *csr, uint8_t roi_idx,
                                          const struct isp_shp_roi_level_gain_frame_cfg *cfg);
void isp_shp_get_roi_level_gain_frame_cfg(volatile CsrBankShp *csr, uint8_t roi_idx,
                                          struct isp_shp_roi_level_gain_frame_cfg *cfg);
void isp_shp_set_roi_level_gain_tile_cfg(volatile CsrBankShp *csr, uint8_t roi_idx,
                                         const struct isp_shp_roi_level_gain_tile_cfg *cfg);
void isp_shp_get_roi_level_gain_tile_cfg(volatile CsrBankShp *csr, uint8_t roi_idx,
                                         struct isp_shp_roi_level_gain_tile_cfg *cfg);
void isp_shp_set_cfg(volatile CsrBankShp *csr, const struct isp_shp_cfg *cfg);
void isp_shp_get_cfg(volatile CsrBankShp *csr, struct isp_shp_cfg *cfg);

void isp_shp_set_roi_stat_frame_ctrl(volatile CsrBankShp *csr, const struct isp_shp_roi_stat_frame_ctrl *cfg);
void isp_shp_get_roi_stat_frame_ctrl(volatile CsrBankShp *csr, struct isp_shp_roi_stat_frame_ctrl *cfg);
void isp_shp_set_roi_stat_tile_ctrl(volatile CsrBankShp *csr, const struct isp_shp_roi_stat_tile_ctrl *cfg);
void isp_shp_get_roi_stat_tile_ctrl(volatile CsrBankShp *csr, struct isp_shp_roi_stat_tile_ctrl *cfg);
void isp_shp_set_roi_stat_clear(volatile CsrBankShp *csr, uint8_t clear);
uint8_t isp_shp_get_roi_stat_clear(volatile CsrBankShp *csr);
void isp_shp_set_roi_stat_enable(volatile CsrBankShp *csr, uint8_t enable);
uint8_t isp_shp_get_roi_stat_enable(volatile CsrBankShp *csr);
void isp_shp_get_roi_stat(volatile CsrBankShp *csr, struct isp_shp_roi_stat *stat);
// RGL
/*
 * TO-DO: isp_shp_get_rgl_stat?
 */
void isp_shp_set_rgl_stat_frame_ctrl(volatile CsrBankShp *csr, const struct isp_shp_rgl_stat_frame_ctrl *cfg);
void isp_shp_get_rgl_stat_frame_ctrl(volatile CsrBankShp *csr, struct isp_shp_rgl_stat_frame_ctrl *cfg);
void isp_shp_set_rgl_stat_tile_ctrl(volatile CsrBankShp *csr, const struct isp_shp_rgl_stat_tile_ctrl *cfg);
void isp_shp_get_rgl_stat_tile_ctrl(volatile CsrBankShp *csr, struct isp_shp_rgl_stat_tile_ctrl *cfg);
void isp_shp_set_rgl_stat_clear(volatile CsrBankShp *csr, uint8_t clear);
uint8_t isp_shp_get_rgl_stat_clear(volatile CsrBankShp *csr);
void isp_shp_set_rgl_stat_enable(volatile CsrBankShp *csr, uint8_t enable);
uint8_t isp_shp_get_rgl_stat_enable(volatile CsrBankShp *csr);
uint8_t isp_shp_get_rgl_stat_num_of_rgl(volatile CsrBankShp *csr);
void isp_shp_get_rgl_stat(volatile CsrBankShp *csr, uint8_t num_of_rgl, struct isp_shp_rgl_stat *stat);

/* Demo */
void isp_shp_set_demo(volatile CsrBankShp *csr, const struct isp_shp_demo *demo);
void isp_shp_get_demo(volatile CsrBankShp *csr, struct isp_shp_demo *demo);

/* Debug */
void isp_shp_set_dbg_mon_sel(volatile CsrBankShp *csr, uint8_t debug_mon_sel);
uint8_t isp_shp_get_dbg_mon_sel(volatile CsrBankShp *csr);

/* Reserved */
void isp_shp_set_reserved(volatile CsrBankShp *csr, uint32_t reserved);
uint32_t isp_shp_get_reserved(volatile CsrBankShp *csr);

/* ATPG */
void isp_shp_set_atpg_ctrl_0(volatile CsrBankShp *csr, uint8_t atpg_ctrl_0);
uint8_t isp_shp_get_atpg_ctrl_0(volatile CsrBankShp *csr);
void isp_shp_set_atpg_ctrl_1(volatile CsrBankShp *csr, uint8_t atpg_ctrl_1);
uint8_t isp_shp_get_atpg_ctrl_1(volatile CsrBankShp *csr);

/* TODO - Move to test */
void hw_isp_shp_res(uint32_t width, uint32_t height);
void hw_isp_shp_bypass(void);

#endif /* KYOTO_SHP_H_ */