#ifndef KYOTO_FAGMA_H_
#define KYOTO_FAGMA_H_

#include "isp_utils.h"
#include "csr_bank_isp.h"

typedef enum isp_fagma_mode {
	ISP_FAGMA_MODE_NORMAL = 0,
	ISP_FAGMA_MODE_DISABLE = 1,
	ISP_FAGMA_MODE_NUM = 2,
} IspFagmaMode;

typedef struct isp_fagma_cfg {
	enum isp_fagma_mode mode;
} IspFagmaCfg;

/* FAGMA */
void hw_isp_fagma_set_cfg(const struct isp_fagma_cfg *cfg);
void hw_isp_fagma_get_cfg(struct isp_fagma_cfg *cfg);

//
/* FAGMA */
void isp_fagma_set_cfg(volatile CsrBankIsp *csr, const struct isp_fagma_cfg *cfg);
void isp_fagma_get_cfg(volatile CsrBankIsp *csr, struct isp_fagma_cfg *cfg);

/* TODO - Move to test */
void hw_isp_fagma_bypass(void);

#endif /* KYOTO_FAGMA_H_ */