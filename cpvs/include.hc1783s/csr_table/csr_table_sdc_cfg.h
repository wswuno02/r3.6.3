#ifndef CSR_TABLE_SDC_CFG_H_
#define CSR_TABLE_SDC_CFG_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_sdc_cfg[] =
{
  // WORD cfg_00
  { "cken_sdc", 0x00000000,  0,  0,   CSR_RW, 0x00000000 },
  { "CFG_00", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cfg_01
  { "sdc_spl_phase_sel", 0x00000004,  1,  0,   CSR_RW, 0x00000000 },
  { "sdc_drv_phase_sel", 0x00000004,  9,  8,   CSR_RW, 0x00000000 },
  { "CFG_01", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cfg_02
  { "sd2_wp_from_csr", 0x00000008,  0,  0,   CSR_RW, 0x00000000 },
  { "sd_wp_from_csr", 0x00000008,  8,  8,   CSR_RW, 0x00000000 },
  { "sd2_cd_from_csr", 0x00000008, 16, 16,   CSR_RW, 0x00000000 },
  { "sd_cd_from_csr", 0x00000008, 24, 24,   CSR_RW, 0x00000000 },
  { "CFG_02", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cfg_03
  { "en_sd2_wp_from_csr", 0x0000000C,  0,  0,   CSR_RW, 0x00000000 },
  { "en_sd_wp_from_csr", 0x0000000C,  8,  8,   CSR_RW, 0x00000000 },
  { "en_sd2_cd_from_csr", 0x0000000C, 16, 16,   CSR_RW, 0x00000000 },
  { "en_sd_cd_from_csr", 0x0000000C, 24, 24,   CSR_RW, 0x00000000 },
  { "CFG_03", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mem_wrapper
  { "sd", 0x00000038,  8,  8,   CSR_RW, 0x00000000 },
  { "slp", 0x00000038, 16, 16,   CSR_RW, 0x00000000 },
  { "MEM_WRAPPER", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_SDC_CFG_H_
