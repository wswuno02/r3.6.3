#ifndef CSR_TABLE_DCST_H_
#define CSR_TABLE_DCST_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_dcst[] =
{
  // WORD y_para_0
  { "coeff_00_2s", 0x00000000, 16,  0,   CSR_RW, 0x00004A85 },
  { "Y_PARA_0", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD y_para_1
  { "coeff_01_2s", 0x00000004, 16,  0,   CSR_RW, 0x00000001 },
  { "Y_PARA_1", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD y_para_2
  { "coeff_02_2s", 0x00000008, 16,  0,   CSR_RW, 0x00007300 },
  { "Y_PARA_2", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD y_para_3
  { "offset_i_0_2s", 0x0000000C, 13,  0,   CSR_RW, 0x00003E00 },
  { "Y_PARA_3", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD u_para_0
  { "coeff_10_2s", 0x00000010, 16,  0,   CSR_RW, 0x00004A85 },
  { "U_PARA_0", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD u_para_1
  { "coeff_11_2s", 0x00000014, 16,  0,   CSR_RW, 0x0001F25A },
  { "U_PARA_1", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD u_para_2
  { "coeff_12_2s", 0x00000018, 16,  0,   CSR_RW, 0x0001DDD0 },
  { "U_PARA_2", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD u_para_3
  { "offset_i_1_2s", 0x0000001C, 13,  0,   CSR_RW, 0x00003000 },
  { "U_PARA_3", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD v_para_0
  { "coeff_20_2s", 0x00000020, 16,  0,   CSR_RW, 0x00004A85 },
  { "V_PARA_0", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD v_para_1
  { "coeff_21_2s", 0x00000024, 16,  0,   CSR_RW, 0x00008733 },
  { "V_PARA_1", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD v_para_2
  { "coeff_22_2s", 0x00000028, 16,  0,   CSR_RW, 0x0001FFFD },
  { "V_PARA_2", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD v_para_3
  { "offset_i_2_2s", 0x0000002C, 13,  0,   CSR_RW, 0x00003000 },
  { "V_PARA_3", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD atpg_test
  { "atpg_test_enable", 0x00000030,  1,  0,   CSR_RW, 0x00000000 },
  { "ATPG_TEST", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_debug_mon_sel
  { "debug_mon_sel", 0x00000034,  0,  0,   CSR_RW, 0x00000000 },
  { "WORD_DEBUG_MON_SEL", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_reserved
  { "reserved", 0x00000038, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_DCST_H_
