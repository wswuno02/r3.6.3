#ifndef CSR_TABLE_ADAC_CTR_H_
#define CSR_TABLE_ADAC_CTR_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_adac_ctr[] =
{
  // WORD dac00
  { "dac_clk_start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "dac_clk_stop", 0x00000000,  8,  8,  CSR_W1P, 0x00000000 },
  { "DAC00", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD dac01
  { "endac", 0x00000004,  0,  0,   CSR_RW, 0x00000000 },
  { "mute", 0x00000004,  8,  8,   CSR_RW, 0x00000000 },
  { "DAC01", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dac02
  { "format", 0x00000008,  0,  0,   CSR_RW, 0x00000001 },
  { "DAC02", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dac03
  { "factor_l", 0x0000000C, 31,  0,   CSR_RW, 0x00002000 },
  { "DAC03", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dac04
  { "factor_h", 0x00000010,  7,  0,   CSR_RW, 0x00000000 },
  { "DAC04", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dac07
  { "debug_mon_sel", 0x0000001C, 25, 24,   CSR_RW, 0x00000000 },
  { "DAC07", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dac13
  { "reserved", 0x00000034, 31,  0,   CSR_RW, 0x00000000 },
  { "DAC13", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_ADAC_CTR_H_
