#ifndef CSR_TABLE_TIMER_H_
#define CSR_TABLE_TIMER_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_timer[] =
{
  // WORD timer00
  { "trigger_0", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "TIMER00", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD timer01
  { "irq_clear_match_0", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "TIMER01", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD timer02
  { "status_match_0", 0x00000008,  0,  0,   CSR_RO, 0x00000000 },
  { "TIMER02", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // WORD timer03
  { "irq_mask_match_0", 0x0000000C,  0,  0,   CSR_RW, 0x00000001 },
  { "TIMER03", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD timer04
  { "status_0", 0x00000010,  0,  0,   CSR_RO, 0x00000000 },
  { "TIMER04", 0x00000010, 31, 0,   CSR_RO, 0x00000000 },
  // WORD timer05
  { "count_value_0", 0x00000014, 31,  0,   CSR_RO, 0x00000000 },
  { "TIMER05", 0x00000014, 31, 0,   CSR_RO, 0x00000000 },
  // WORD timer06
  { "mode_0", 0x00000018,  0,  0,   CSR_RW, 0x00000000 },
  { "prescaler_0", 0x00000018, 23, 16,   CSR_RW, 0x00000000 },
  { "TIMER06", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD timer07
  { "count_load_0", 0x0000001C, 31,  0,   CSR_RW, 0x00000000 },
  { "TIMER07", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD timer08
  { "trigger_1", 0x00000020,  0,  0,  CSR_W1P, 0x00000000 },
  { "TIMER08", 0x00000020, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD timer09
  { "irq_clear_match_1", 0x00000024,  0,  0,  CSR_W1P, 0x00000000 },
  { "TIMER09", 0x00000024, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD timer10
  { "status_match_1", 0x00000028,  0,  0,   CSR_RO, 0x00000000 },
  { "TIMER10", 0x00000028, 31, 0,   CSR_RO, 0x00000000 },
  // WORD timer11
  { "irq_mask_match_1", 0x0000002C,  0,  0,   CSR_RW, 0x00000001 },
  { "TIMER11", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD timer12
  { "status_1", 0x00000030,  0,  0,   CSR_RO, 0x00000000 },
  { "TIMER12", 0x00000030, 31, 0,   CSR_RO, 0x00000000 },
  // WORD timer13
  { "count_value_1", 0x00000034, 31,  0,   CSR_RO, 0x00000000 },
  { "TIMER13", 0x00000034, 31, 0,   CSR_RO, 0x00000000 },
  // WORD timer14
  { "mode_1", 0x00000038,  0,  0,   CSR_RW, 0x00000000 },
  { "prescaler_1", 0x00000038, 23, 16,   CSR_RW, 0x00000000 },
  { "TIMER14", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD timer15
  { "count_load_1", 0x0000003C, 31,  0,   CSR_RW, 0x00000000 },
  { "TIMER15", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD timer16
  { "trigger_2", 0x00000040,  0,  0,  CSR_W1P, 0x00000000 },
  { "TIMER16", 0x00000040, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD timer17
  { "irq_clear_match_2", 0x00000044,  0,  0,  CSR_W1P, 0x00000000 },
  { "TIMER17", 0x00000044, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD timer18
  { "status_match_2", 0x00000048,  0,  0,   CSR_RO, 0x00000000 },
  { "TIMER18", 0x00000048, 31, 0,   CSR_RO, 0x00000000 },
  // WORD timer19
  { "irq_mask_match_2", 0x0000004C,  0,  0,   CSR_RW, 0x00000001 },
  { "TIMER19", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD timer20
  { "status_2", 0x00000050,  0,  0,   CSR_RO, 0x00000000 },
  { "TIMER20", 0x00000050, 31, 0,   CSR_RO, 0x00000000 },
  // WORD timer21
  { "count_value_2", 0x00000054, 31,  0,   CSR_RO, 0x00000000 },
  { "TIMER21", 0x00000054, 31, 0,   CSR_RO, 0x00000000 },
  // WORD timer22
  { "mode_2", 0x00000058,  0,  0,   CSR_RW, 0x00000000 },
  { "prescaler_2", 0x00000058, 23, 16,   CSR_RW, 0x00000000 },
  { "TIMER22", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD timer23
  { "count_load_2", 0x0000005C, 31,  0,   CSR_RW, 0x00000000 },
  { "TIMER23", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD timer24
  { "trigger_3", 0x00000060,  0,  0,  CSR_W1P, 0x00000000 },
  { "TIMER24", 0x00000060, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD timer25
  { "irq_clear_match_3", 0x00000064,  0,  0,  CSR_W1P, 0x00000000 },
  { "TIMER25", 0x00000064, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD timer26
  { "status_match_3", 0x00000068,  0,  0,   CSR_RO, 0x00000000 },
  { "TIMER26", 0x00000068, 31, 0,   CSR_RO, 0x00000000 },
  // WORD timer27
  { "irq_mask_match_3", 0x0000006C,  0,  0,   CSR_RW, 0x00000001 },
  { "TIMER27", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD timer28
  { "status_3", 0x00000070,  0,  0,   CSR_RO, 0x00000000 },
  { "TIMER28", 0x00000070, 31, 0,   CSR_RO, 0x00000000 },
  // WORD timer29
  { "count_value_3", 0x00000074, 31,  0,   CSR_RO, 0x00000000 },
  { "TIMER29", 0x00000074, 31, 0,   CSR_RO, 0x00000000 },
  // WORD timer30
  { "mode_3", 0x00000078,  0,  0,   CSR_RW, 0x00000000 },
  { "prescaler_3", 0x00000078, 23, 16,   CSR_RW, 0x00000000 },
  { "TIMER30", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD timer31
  { "count_load_3", 0x0000007C, 31,  0,   CSR_RW, 0x00000000 },
  { "TIMER31", 0x0000007C, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_TIMER_H_
