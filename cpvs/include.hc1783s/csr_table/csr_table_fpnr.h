#ifndef CSR_TABLE_FPNR_H_
#define CSR_TABLE_FPNR_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_fpnr[] =
{
  // WORD word_frame_start
  { "frame_start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "WORD_FRAME_START", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD irq_clear
  { "irq_clear_frame_end", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "IRQ_CLEAR", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD status
  { "status_frame_end", 0x00000008,  0,  0,   CSR_RO, 0x00000000 },
  { "STATUS", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // WORD irq_mask
  { "irq_mask_frame_end", 0x0000000C,  0,  0,   CSR_RW, 0x00000001 },
  { "IRQ_MASK", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD resolution
  { "width", 0x00000010, 15,  0,   CSR_RW, 0x00000780 },
  { "height", 0x00000010, 31, 16,   CSR_RW, 0x00000438 },
  { "RESOLUTION", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_mode
  { "mode", 0x00000014,  0,  0,   CSR_RW, 0x00000000 },
  { "WORD_MODE", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD roi_0
  { "roi_sx", 0x00000018, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_ex", 0x00000018, 31, 16,   CSR_RW, 0x00000780 },
  { "ROI_0", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD roi_1
  { "roi_sy", 0x0000001C, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_ey", 0x0000001C, 31, 16,   CSR_RW, 0x00000438 },
  { "ROI_1", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dram_dec
  { "offset_bw", 0x00000020,  4,  0,   CSR_RW, 0x00000000 },
  { "offset_shift_bit", 0x00000020, 11,  8,   CSR_RW, 0x00000000 },
  { "gain_bw", 0x00000020, 20, 16,   CSR_RW, 0x00000010 },
  { "gain_prec", 0x00000020, 28, 24,   CSR_RW, 0x00000000 },
  { "DRAM_DEC", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD atpg_test
  { "atpg_test_enable", 0x00000024,  1,  0,   CSR_RW, 0x00000000 },
  { "ATPG_TEST", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_debug_mon_sel
  { "debug_mon_sel", 0x00000028,  1,  0,   CSR_RW, 0x00000000 },
  { "WORD_DEBUG_MON_SEL", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_FPNR_H_
