#ifndef CSR_TABLE_IS_CFG_H_
#define CSR_TABLE_IS_CFG_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_is_cfg[] =
{
  // WORD conf0_isr0
  { "cken_isr0", 0x00000000,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_isr0", 0x00000000,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_ISR0", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_isr0
  { "sw_rst_isr0", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_isr0", 0x00000004,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_ISR0", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_isr1
  { "cken_isr1", 0x00000008,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_isr1", 0x00000008,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_ISR1", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_isr1
  { "sw_rst_isr1", 0x0000000C,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_isr1", 0x0000000C,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_ISR1", 0x0000000C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_fe0
  { "cken_fe0", 0x00000010,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_fe0", 0x00000010,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_FE0", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_fe0
  { "sw_rst_fe0", 0x00000014,  0,  0,  CSR_W1P, 0x00000000 },
  { "CONF1_FE0", 0x00000014, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_fe1
  { "cken_fe1", 0x00000018,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_fe1", 0x00000018,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_FE1", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_fe1
  { "sw_rst_fe1", 0x0000001C,  0,  0,  CSR_W1P, 0x00000000 },
  { "CONF1_FE1", 0x0000001C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_isk0
  { "cken_isk0", 0x00000020,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_isk0", 0x00000020,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_ISK0", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_isk0
  { "sw_rst_isk0", 0x00000024,  0,  0,  CSR_W1P, 0x00000000 },
  { "CONF1_ISK0", 0x00000024, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_isk1
  { "cken_isk1", 0x00000028,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_isk1", 0x00000028,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_ISK1", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_isk1
  { "sw_rst_isk1", 0x0000002C,  0,  0,  CSR_W1P, 0x00000000 },
  { "CONF1_ISK1", 0x0000002C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_iswroi0
  { "cken_iswroi0", 0x00000030,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_iswroi0", 0x00000030,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_ISWROI0", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_iswroi0
  { "sw_rst_iswroi0", 0x00000034,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_iswroi0", 0x00000034,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_ISWROI0", 0x00000034, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_iswroi1
  { "cken_iswroi1", 0x00000038,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_iswroi1", 0x00000038,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_ISWROI1", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_iswroi1
  { "sw_rst_iswroi1", 0x0000003C,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_iswroi1", 0x0000003C,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_ISWROI1", 0x0000003C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_isw0
  { "cken_isw0", 0x00000040,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_isw0", 0x00000040,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_ISW0", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_isw0
  { "sw_rst_isw0", 0x00000044,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_isw0", 0x00000044,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_ISW0", 0x00000044, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_isw1
  { "cken_isw1", 0x00000048,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_isw1", 0x00000048,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_ISW1", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_isw1
  { "sw_rst_isw1", 0x0000004C,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_isw1", 0x0000004C,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_ISW1", 0x0000004C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_ctrl
  { "cken_ctrl", 0x00000050,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_ctrl", 0x00000050,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_CTRL", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_ctrl
  { "sw_rst_ctrl", 0x00000054,  0,  0,  CSR_W1P, 0x00000000 },
  { "CONF1_CTRL", 0x00000054, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_lp
  { "cken_lp", 0x00000058,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_lp", 0x00000058,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_LP", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_lp
  { "sw_rst_lp", 0x0000005C,  0,  0,  CSR_W1P, 0x00000000 },
  { "CONF1_LP", 0x0000005C, 31, 0,  CSR_W1P, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_IS_CFG_H_
