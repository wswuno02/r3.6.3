#ifndef CSR_TABLE_ENH_H_
#define CSR_TABLE_ENH_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_enh[] =
{
  // WORD word_frame_start
  { "frame_start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "WORD_FRAME_START", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD irq_clear
  { "irq_clear_frame_end", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "IRQ_CLEAR", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD status
  { "status_frame_end", 0x00000008,  0,  0,   CSR_RO, 0x00000000 },
  { "STATUS", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // WORD irq_mask
  { "irq_mask_frame_end", 0x0000000C,  0,  0,   CSR_RW, 0x00000001 },
  { "IRQ_MASK", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mode
  { "y_mode", 0x00000010,  1,  0,   CSR_RW, 0x00000000 },
  { "c_mode", 0x00000010,  9,  8,   CSR_RW, 0x00000000 },
  { "sample_mode", 0x00000010, 16, 16,   CSR_RW, 0x00000000 },
  { "cac_enable", 0x00000010, 24, 24,   CSR_RW, 0x00000000 },
  { "MODE", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD frame_size
  { "width", 0x00000014, 15,  0,   CSR_RW, 0x00000100 },
  { "height", 0x00000014, 31, 16,   CSR_RW, 0x00000438 },
  { "FRAME_SIZE", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD radius
  { "y_l_r", 0x00000018,  2,  0,   CSR_RW, 0x00000002 },
  { "c_r", 0x00000018, 10,  8,   CSR_RW, 0x00000001 },
  { "RADIUS", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD edge_strength
  { "y_l_edge_strength", 0x0000001C,  5,  0,   CSR_RW, 0x00000020 },
  { "y_s_edge_strength", 0x0000001C, 13,  8,   CSR_RW, 0x00000000 },
  { "c_edge_y_strength", 0x0000001C, 21, 16,   CSR_RW, 0x00000020 },
  { "c_edge_c_strength", 0x0000001C, 29, 24,   CSR_RW, 0x00000020 },
  { "EDGE_STRENGTH", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD eps_y_l
  { "y_l_eps", 0x00000020, 24,  0,   CSR_RW, 0x00000100 },
  { "EPS_Y_L", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mean_norm_y_l
  { "y_l_mean_norm", 0x00000024, 11,  0,   CSR_RW, 0x00000000 },
  { "MEAN_NORM_Y_L", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dt_norm_y_l
  { "y_l_dt_mean_norm", 0x00000028, 15,  0,   CSR_RW, 0x00000000 },
  { "y_l_dt_corr_norm", 0x00000028, 31, 16,   CSR_RW, 0x00000000 },
  { "DT_NORM_Y_L", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD eps_y_s
  { "y_s_eps", 0x0000002C, 24,  0,   CSR_RW, 0x00000100 },
  { "EPS_Y_S", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mean_norm_y_s
  { "y_s_mean_norm", 0x00000030,  8,  0,   CSR_RW, 0x00000000 },
  { "MEAN_NORM_Y_S", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dt_norm_y_s
  { "y_s_dt_mean_norm", 0x00000034,  8,  0,   CSR_RW, 0x00000000 },
  { "y_s_dt_corr_norm", 0x00000034, 24, 16,   CSR_RW, 0x00000000 },
  { "DT_NORM_Y_S", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD blending
  { "y_blend_lut_x_0", 0x00000038,  7,  0,   CSR_RW, 0x00000020 },
  { "y_blend_lut_x_1", 0x00000038, 15,  8,   CSR_RW, 0x00000060 },
  { "y_blend_lut_y_0", 0x00000038, 23, 16,   CSR_RW, 0x00000000 },
  { "y_blend_lut_y_1", 0x00000038, 31, 24,   CSR_RW, 0x00000080 },
  { "BLENDING", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD blending_m
  { "y_blend_lut_m_0", 0x0000003C,  5,  0,   CSR_RW, 0x00000010 },
  { "BLENDING_M", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD eps_c_y
  { "c_eps_y", 0x00000040, 16,  0,   CSR_RW, 0x00000000 },
  { "EPS_C_Y", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD eps_c_c
  { "c_eps_c", 0x00000044, 16,  0,   CSR_RW, 0x00000000 },
  { "EPS_C_C", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mean_norm_c
  { "c_mean_norm", 0x00000048, 12,  0,   CSR_RW, 0x00000000 },
  { "MEAN_NORM_C", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dt_norm_c
  { "c_dt_mean_norm", 0x0000004C, 14,  0,   CSR_RW, 0x00000000 },
  { "c_dt_corr_norm", 0x0000004C, 28, 16,   CSR_RW, 0x00000000 },
  { "DT_NORM_C", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cac_center
  { "cac_center_x", 0x00000050, 15,  0,   CSR_RW, 0x00000001 },
  { "cac_center_y", 0x00000050, 31, 16,   CSR_RW, 0x00000001 },
  { "CAC_CENTER", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tile_x
  { "tile_sx", 0x00000054, 15,  0,   CSR_RW, 0x00000000 },
  { "TILE_X", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD center_norm
  { "cac_center_diff_norm_x", 0x00000058, 10,  0,   CSR_RW, 0x00000000 },
  { "cac_center_diff_norm_y", 0x00000058, 26, 16,   CSR_RW, 0x00000000 },
  { "CENTER_NORM", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cac_color
  { "cac_color_u", 0x0000005C,  9,  0,   CSR_RW, 0x00000001 },
  { "cac_color_v", 0x0000005C, 25, 16,   CSR_RW, 0x00000001 },
  { "CAC_COLOR", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD color_norm
  { "cac_color_diff_norm_u", 0x00000060, 10,  0,   CSR_RW, 0x00000000 },
  { "cac_color_diff_norm_v", 0x00000060, 26, 16,   CSR_RW, 0x00000000 },
  { "COLOR_NORM", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD table_luma_lut_y_0
  { "luma_lut_y_ini", 0x00000064,  5,  0,   CSR_RW, 0x00000020 },
  { "luma_lut_y_0", 0x00000064, 13,  8,   CSR_RW, 0x00000020 },
  { "luma_lut_y_1", 0x00000064, 21, 16,   CSR_RW, 0x0000001C },
  { "luma_lut_y_2", 0x00000064, 29, 24,   CSR_RW, 0x00000018 },
  { "TABLE_LUMA_LUT_Y_0", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD table_luma_lut_y_1
  { "luma_lut_y_3", 0x00000068,  5,  0,   CSR_RW, 0x00000014 },
  { "luma_lut_y_4", 0x00000068, 13,  8,   CSR_RW, 0x00000014 },
  { "TABLE_LUMA_LUT_Y_1", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD table_luma_lut_x_0
  { "luma_lut_x_0", 0x0000006C,  9,  0,   CSR_RW, 0x00000080 },
  { "luma_lut_x_1", 0x0000006C, 25, 16,   CSR_RW, 0x00000100 },
  { "TABLE_LUMA_LUT_X_0", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD table_luma_lut_x_1
  { "luma_lut_x_2", 0x00000070,  9,  0,   CSR_RW, 0x00000180 },
  { "luma_lut_x_3", 0x00000070, 25, 16,   CSR_RW, 0x00000200 },
  { "TABLE_LUMA_LUT_X_1", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // WORD table_luma_lut_x_2
  { "luma_lut_x_4", 0x00000074,  9,  0,   CSR_RW, 0x000003FF },
  { "TABLE_LUMA_LUT_X_2", 0x00000074, 31, 0,   CSR_RW, 0x00000000 },
  // WORD table_luma_lut_m_0
  { "luma_lut_m_0_2s", 0x00000078,  9,  0,   CSR_RW, 0x00000000 },
  { "luma_lut_m_1_2s", 0x00000078, 25, 16,   CSR_RW, 0x00000000 },
  { "TABLE_LUMA_LUT_M_0", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD table_luma_lut_m_1
  { "luma_lut_m_2_2s", 0x0000007C,  9,  0,   CSR_RW, 0x00000000 },
  { "luma_lut_m_3_2s", 0x0000007C, 25, 16,   CSR_RW, 0x00000000 },
  { "TABLE_LUMA_LUT_M_1", 0x0000007C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD table_luma_lut_m_2
  { "luma_lut_m_4_2s", 0x00000080,  9,  0,   CSR_RW, 0x00000000 },
  { "luma_lut_m_5_2s", 0x00000080, 25, 16,   CSR_RW, 0x00000000 },
  { "TABLE_LUMA_LUT_M_2", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // WORD edge_0
  { "cac_edge_lut_x_0", 0x00000084, 11,  0,   CSR_RW, 0x00000000 },
  { "cac_edge_lut_x_1", 0x00000084, 27, 16,   CSR_RW, 0x00000800 },
  { "EDGE_0", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD edge_1
  { "cac_edge_lut_y_0", 0x00000088, 10,  0,   CSR_RW, 0x00000000 },
  { "cac_edge_lut_y_1", 0x00000088, 26, 16,   CSR_RW, 0x00000400 },
  { "EDGE_1", 0x00000088, 31, 0,   CSR_RW, 0x00000000 },
  // WORD edge_2
  { "cac_edge_lut_m_0", 0x0000008C,  5,  0,   CSR_RW, 0x00000004 },
  { "EDGE_2", 0x0000008C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_debug_mon_sel
  { "debug_mon_sel", 0x00000090,  1,  0,   CSR_RW, 0x00000000 },
  { "WORD_DEBUG_MON_SEL", 0x00000090, 31, 0,   CSR_RW, 0x00000000 },
  // WORD detail_gain
  { "y_l_detail_gain", 0x00000094,  6,  0,   CSR_RW, 0x00000000 },
  { "y_s_detail_gain", 0x00000094, 14,  8,   CSR_RW, 0x00000000 },
  { "DETAIL_GAIN", 0x00000094, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_atpg_ctrl
  { "atpg_ctrl", 0x00000098,  7,  0,   CSR_RW, 0x00000000 },
  { "WORD_ATPG_CTRL", 0x00000098, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_ENH_H_
