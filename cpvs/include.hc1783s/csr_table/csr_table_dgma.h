#ifndef CSR_TABLE_DGMA_H_
#define CSR_TABLE_DGMA_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_dgma[] =
{
  // WORD word_mode
  { "mode", 0x00000000,  0,  0,   CSR_RW, 0x00000000 },
  { "WORD_MODE", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_0
  { "gma_curve_0_1", 0x00000004, 13,  0,   CSR_RW, 0x00000120 },
  { "gma_curve_0_2", 0x00000004, 29, 16,   CSR_RW, 0x00000240 },
  { "GAMMA_CURVE_0_0", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_1
  { "gma_curve_0_3", 0x00000008, 13,  0,   CSR_RW, 0x00000360 },
  { "gma_curve_0_4", 0x00000008, 29, 16,   CSR_RW, 0x00000480 },
  { "GAMMA_CURVE_0_1", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_2
  { "gma_curve_0_5", 0x0000000C, 13,  0,   CSR_RW, 0x000005A0 },
  { "gma_curve_0_6", 0x0000000C, 29, 16,   CSR_RW, 0x000006A8 },
  { "GAMMA_CURVE_0_2", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_3
  { "gma_curve_0_7", 0x00000010, 13,  0,   CSR_RW, 0x00000798 },
  { "gma_curve_0_8", 0x00000010, 29, 16,   CSR_RW, 0x00000874 },
  { "GAMMA_CURVE_0_3", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_4
  { "gma_curve_0_9", 0x00000014, 13,  0,   CSR_RW, 0x00000A04 },
  { "gma_curve_0_10", 0x00000014, 29, 16,   CSR_RW, 0x00000B68 },
  { "GAMMA_CURVE_0_4", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_5
  { "gma_curve_0_11", 0x00000018, 13,  0,   CSR_RW, 0x00000CB0 },
  { "gma_curve_0_12", 0x00000018, 29, 16,   CSR_RW, 0x00000DDC },
  { "GAMMA_CURVE_0_5", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_6
  { "gma_curve_0_13", 0x0000001C, 13,  0,   CSR_RW, 0x00000EF8 },
  { "gma_curve_0_14", 0x0000001C, 29, 16,   CSR_RW, 0x00001000 },
  { "GAMMA_CURVE_0_6", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_7
  { "gma_curve_0_15", 0x00000020, 13,  0,   CSR_RW, 0x000010F8 },
  { "gma_curve_0_16", 0x00000020, 29, 16,   CSR_RW, 0x000011E8 },
  { "GAMMA_CURVE_0_7", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_8
  { "gma_curve_0_17", 0x00000024, 13,  0,   CSR_RW, 0x000012CC },
  { "gma_curve_0_18", 0x00000024, 29, 16,   CSR_RW, 0x000013A8 },
  { "GAMMA_CURVE_0_8", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_9
  { "gma_curve_0_19", 0x00000028, 13,  0,   CSR_RW, 0x00001478 },
  { "gma_curve_0_20", 0x00000028, 29, 16,   CSR_RW, 0x00001544 },
  { "GAMMA_CURVE_0_9", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_10
  { "gma_curve_0_21", 0x0000002C, 13,  0,   CSR_RW, 0x00001600 },
  { "gma_curve_0_22", 0x0000002C, 29, 16,   CSR_RW, 0x000016C4 },
  { "GAMMA_CURVE_0_10", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_11
  { "gma_curve_0_23", 0x00000030, 13,  0,   CSR_RW, 0x00001770 },
  { "gma_curve_0_24", 0x00000030, 29, 16,   CSR_RW, 0x0000182C },
  { "GAMMA_CURVE_0_11", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_12
  { "gma_curve_0_25", 0x00000034, 13,  0,   CSR_RW, 0x00001984 },
  { "gma_curve_0_26", 0x00000034, 29, 16,   CSR_RW, 0x00001AC8 },
  { "GAMMA_CURVE_0_12", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_13
  { "gma_curve_0_27", 0x00000038, 13,  0,   CSR_RW, 0x00001C00 },
  { "gma_curve_0_28", 0x00000038, 29, 16,   CSR_RW, 0x00001D28 },
  { "GAMMA_CURVE_0_13", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_14
  { "gma_curve_0_29", 0x0000003C, 13,  0,   CSR_RW, 0x00001E48 },
  { "gma_curve_0_30", 0x0000003C, 29, 16,   CSR_RW, 0x00001F5C },
  { "GAMMA_CURVE_0_14", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_15
  { "gma_curve_0_31", 0x00000040, 13,  0,   CSR_RW, 0x00002060 },
  { "gma_curve_0_32", 0x00000040, 29, 16,   CSR_RW, 0x00002168 },
  { "GAMMA_CURVE_0_15", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_16
  { "gma_curve_0_33", 0x00000044, 13,  0,   CSR_RW, 0x00002260 },
  { "gma_curve_0_34", 0x00000044, 29, 16,   CSR_RW, 0x00002358 },
  { "GAMMA_CURVE_0_16", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_17
  { "gma_curve_0_35", 0x00000048, 13,  0,   CSR_RW, 0x00002440 },
  { "gma_curve_0_36", 0x00000048, 29, 16,   CSR_RW, 0x0000252C },
  { "GAMMA_CURVE_0_17", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_18
  { "gma_curve_0_37", 0x0000004C, 13,  0,   CSR_RW, 0x0000260C },
  { "gma_curve_0_38", 0x0000004C, 29, 16,   CSR_RW, 0x000026E8 },
  { "GAMMA_CURVE_0_18", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_19
  { "gma_curve_0_39", 0x00000050, 13,  0,   CSR_RW, 0x000027C0 },
  { "gma_curve_0_40", 0x00000050, 29, 16,   CSR_RW, 0x00002890 },
  { "GAMMA_CURVE_0_19", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_20
  { "gma_curve_0_41", 0x00000054, 13,  0,   CSR_RW, 0x00002A28 },
  { "gma_curve_0_42", 0x00000054, 29, 16,   CSR_RW, 0x00002BB0 },
  { "GAMMA_CURVE_0_20", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_21
  { "gma_curve_0_43", 0x00000058, 13,  0,   CSR_RW, 0x00002D28 },
  { "gma_curve_0_44", 0x00000058, 29, 16,   CSR_RW, 0x00002E90 },
  { "GAMMA_CURVE_0_21", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_22
  { "gma_curve_0_45", 0x0000005C, 13,  0,   CSR_RW, 0x00002FF4 },
  { "gma_curve_0_46", 0x0000005C, 29, 16,   CSR_RW, 0x00003140 },
  { "GAMMA_CURVE_0_22", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_23
  { "gma_curve_0_47", 0x00000060, 13,  0,   CSR_RW, 0x00003298 },
  { "gma_curve_0_48", 0x00000060, 29, 16,   CSR_RW, 0x000033D4 },
  { "GAMMA_CURVE_0_23", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_24
  { "gma_curve_0_49", 0x00000064, 13,  0,   CSR_RW, 0x00003514 },
  { "gma_curve_0_50", 0x00000064, 29, 16,   CSR_RW, 0x00003640 },
  { "GAMMA_CURVE_0_24", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_25
  { "gma_curve_0_51", 0x00000068, 13,  0,   CSR_RW, 0x00003774 },
  { "gma_curve_0_52", 0x00000068, 29, 16,   CSR_RW, 0x00003894 },
  { "GAMMA_CURVE_0_25", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_26
  { "gma_curve_0_53", 0x0000006C, 13,  0,   CSR_RW, 0x000039B8 },
  { "gma_curve_0_54", 0x0000006C, 29, 16,   CSR_RW, 0x00003AD0 },
  { "GAMMA_CURVE_0_26", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_27
  { "gma_curve_0_55", 0x00000070, 13,  0,   CSR_RW, 0x00003BE4 },
  { "gma_curve_0_56", 0x00000070, 29, 16,   CSR_RW, 0x00003CF0 },
  { "GAMMA_CURVE_0_27", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_0_28
  { "gma_curve_0_57", 0x00000074, 13,  0,   CSR_RW, 0x00003DFC },
  { "gma_curve_0_58", 0x00000074, 29, 16,   CSR_RW, 0x00003F00 },
  { "GAMMA_CURVE_0_28", 0x00000074, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_0
  { "gma_curve_1_1", 0x00000078, 13,  0,   CSR_RW, 0x00000120 },
  { "gma_curve_1_2", 0x00000078, 29, 16,   CSR_RW, 0x00000240 },
  { "GAMMA_CURVE_1_0", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_1
  { "gma_curve_1_3", 0x0000007C, 13,  0,   CSR_RW, 0x00000360 },
  { "gma_curve_1_4", 0x0000007C, 29, 16,   CSR_RW, 0x00000480 },
  { "GAMMA_CURVE_1_1", 0x0000007C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_2
  { "gma_curve_1_5", 0x00000080, 13,  0,   CSR_RW, 0x000005A0 },
  { "gma_curve_1_6", 0x00000080, 29, 16,   CSR_RW, 0x000006A8 },
  { "GAMMA_CURVE_1_2", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_3
  { "gma_curve_1_7", 0x00000084, 13,  0,   CSR_RW, 0x00000798 },
  { "gma_curve_1_8", 0x00000084, 29, 16,   CSR_RW, 0x00000874 },
  { "GAMMA_CURVE_1_3", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_4
  { "gma_curve_1_9", 0x00000088, 13,  0,   CSR_RW, 0x00000A04 },
  { "gma_curve_1_10", 0x00000088, 29, 16,   CSR_RW, 0x00000B68 },
  { "GAMMA_CURVE_1_4", 0x00000088, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_5
  { "gma_curve_1_11", 0x0000008C, 13,  0,   CSR_RW, 0x00000CB0 },
  { "gma_curve_1_12", 0x0000008C, 29, 16,   CSR_RW, 0x00000DDC },
  { "GAMMA_CURVE_1_5", 0x0000008C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_6
  { "gma_curve_1_13", 0x00000090, 13,  0,   CSR_RW, 0x00000EF8 },
  { "gma_curve_1_14", 0x00000090, 29, 16,   CSR_RW, 0x00001000 },
  { "GAMMA_CURVE_1_6", 0x00000090, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_7
  { "gma_curve_1_15", 0x00000094, 13,  0,   CSR_RW, 0x000010F8 },
  { "gma_curve_1_16", 0x00000094, 29, 16,   CSR_RW, 0x000011E8 },
  { "GAMMA_CURVE_1_7", 0x00000094, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_8
  { "gma_curve_1_17", 0x00000098, 13,  0,   CSR_RW, 0x000012CC },
  { "gma_curve_1_18", 0x00000098, 29, 16,   CSR_RW, 0x000013A8 },
  { "GAMMA_CURVE_1_8", 0x00000098, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_9
  { "gma_curve_1_19", 0x0000009C, 13,  0,   CSR_RW, 0x00001478 },
  { "gma_curve_1_20", 0x0000009C, 29, 16,   CSR_RW, 0x00001544 },
  { "GAMMA_CURVE_1_9", 0x0000009C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_10
  { "gma_curve_1_21", 0x000000A0, 13,  0,   CSR_RW, 0x00001600 },
  { "gma_curve_1_22", 0x000000A0, 29, 16,   CSR_RW, 0x000016C4 },
  { "GAMMA_CURVE_1_10", 0x000000A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_11
  { "gma_curve_1_23", 0x000000A4, 13,  0,   CSR_RW, 0x00001770 },
  { "gma_curve_1_24", 0x000000A4, 29, 16,   CSR_RW, 0x0000182C },
  { "GAMMA_CURVE_1_11", 0x000000A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_12
  { "gma_curve_1_25", 0x000000A8, 13,  0,   CSR_RW, 0x00001984 },
  { "gma_curve_1_26", 0x000000A8, 29, 16,   CSR_RW, 0x00001AC8 },
  { "GAMMA_CURVE_1_12", 0x000000A8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_13
  { "gma_curve_1_27", 0x000000AC, 13,  0,   CSR_RW, 0x00001C00 },
  { "gma_curve_1_28", 0x000000AC, 29, 16,   CSR_RW, 0x00001D28 },
  { "GAMMA_CURVE_1_13", 0x000000AC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_14
  { "gma_curve_1_29", 0x000000B0, 13,  0,   CSR_RW, 0x00001E48 },
  { "gma_curve_1_30", 0x000000B0, 29, 16,   CSR_RW, 0x00001F5C },
  { "GAMMA_CURVE_1_14", 0x000000B0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_15
  { "gma_curve_1_31", 0x000000B4, 13,  0,   CSR_RW, 0x00002060 },
  { "gma_curve_1_32", 0x000000B4, 29, 16,   CSR_RW, 0x00002168 },
  { "GAMMA_CURVE_1_15", 0x000000B4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_16
  { "gma_curve_1_33", 0x000000B8, 13,  0,   CSR_RW, 0x00002260 },
  { "gma_curve_1_34", 0x000000B8, 29, 16,   CSR_RW, 0x00002358 },
  { "GAMMA_CURVE_1_16", 0x000000B8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_17
  { "gma_curve_1_35", 0x000000BC, 13,  0,   CSR_RW, 0x00002440 },
  { "gma_curve_1_36", 0x000000BC, 29, 16,   CSR_RW, 0x0000252C },
  { "GAMMA_CURVE_1_17", 0x000000BC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_18
  { "gma_curve_1_37", 0x000000C0, 13,  0,   CSR_RW, 0x0000260C },
  { "gma_curve_1_38", 0x000000C0, 29, 16,   CSR_RW, 0x000026E8 },
  { "GAMMA_CURVE_1_18", 0x000000C0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_19
  { "gma_curve_1_39", 0x000000C4, 13,  0,   CSR_RW, 0x000027C0 },
  { "gma_curve_1_40", 0x000000C4, 29, 16,   CSR_RW, 0x00002890 },
  { "GAMMA_CURVE_1_19", 0x000000C4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_20
  { "gma_curve_1_41", 0x000000C8, 13,  0,   CSR_RW, 0x00002A28 },
  { "gma_curve_1_42", 0x000000C8, 29, 16,   CSR_RW, 0x00002BB0 },
  { "GAMMA_CURVE_1_20", 0x000000C8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_21
  { "gma_curve_1_43", 0x000000CC, 13,  0,   CSR_RW, 0x00002D28 },
  { "gma_curve_1_44", 0x000000CC, 29, 16,   CSR_RW, 0x00002E90 },
  { "GAMMA_CURVE_1_21", 0x000000CC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_22
  { "gma_curve_1_45", 0x000000D0, 13,  0,   CSR_RW, 0x00002FF4 },
  { "gma_curve_1_46", 0x000000D0, 29, 16,   CSR_RW, 0x00003140 },
  { "GAMMA_CURVE_1_22", 0x000000D0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_23
  { "gma_curve_1_47", 0x000000D4, 13,  0,   CSR_RW, 0x00003298 },
  { "gma_curve_1_48", 0x000000D4, 29, 16,   CSR_RW, 0x000033D4 },
  { "GAMMA_CURVE_1_23", 0x000000D4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_24
  { "gma_curve_1_49", 0x000000D8, 13,  0,   CSR_RW, 0x00003514 },
  { "gma_curve_1_50", 0x000000D8, 29, 16,   CSR_RW, 0x00003640 },
  { "GAMMA_CURVE_1_24", 0x000000D8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_25
  { "gma_curve_1_51", 0x000000DC, 13,  0,   CSR_RW, 0x00003774 },
  { "gma_curve_1_52", 0x000000DC, 29, 16,   CSR_RW, 0x00003894 },
  { "GAMMA_CURVE_1_25", 0x000000DC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_26
  { "gma_curve_1_53", 0x000000E0, 13,  0,   CSR_RW, 0x000039B8 },
  { "gma_curve_1_54", 0x000000E0, 29, 16,   CSR_RW, 0x00003AD0 },
  { "GAMMA_CURVE_1_26", 0x000000E0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_27
  { "gma_curve_1_55", 0x000000E4, 13,  0,   CSR_RW, 0x00003BE4 },
  { "gma_curve_1_56", 0x000000E4, 29, 16,   CSR_RW, 0x00003CF0 },
  { "GAMMA_CURVE_1_27", 0x000000E4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_1_28
  { "gma_curve_1_57", 0x000000E8, 13,  0,   CSR_RW, 0x00003DFC },
  { "gma_curve_1_58", 0x000000E8, 29, 16,   CSR_RW, 0x00003F00 },
  { "GAMMA_CURVE_1_28", 0x000000E8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_0
  { "gma_curve_2_1", 0x000000EC, 13,  0,   CSR_RW, 0x00000120 },
  { "gma_curve_2_2", 0x000000EC, 29, 16,   CSR_RW, 0x00000240 },
  { "GAMMA_CURVE_2_0", 0x000000EC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_1
  { "gma_curve_2_3", 0x000000F0, 13,  0,   CSR_RW, 0x00000360 },
  { "gma_curve_2_4", 0x000000F0, 29, 16,   CSR_RW, 0x00000480 },
  { "GAMMA_CURVE_2_1", 0x000000F0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_2
  { "gma_curve_2_5", 0x000000F4, 13,  0,   CSR_RW, 0x000005A0 },
  { "gma_curve_2_6", 0x000000F4, 29, 16,   CSR_RW, 0x000006A8 },
  { "GAMMA_CURVE_2_2", 0x000000F4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_3
  { "gma_curve_2_7", 0x000000F8, 13,  0,   CSR_RW, 0x00000798 },
  { "gma_curve_2_8", 0x000000F8, 29, 16,   CSR_RW, 0x00000874 },
  { "GAMMA_CURVE_2_3", 0x000000F8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_4
  { "gma_curve_2_9", 0x000000FC, 13,  0,   CSR_RW, 0x00000A04 },
  { "gma_curve_2_10", 0x000000FC, 29, 16,   CSR_RW, 0x00000B68 },
  { "GAMMA_CURVE_2_4", 0x000000FC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_5
  { "gma_curve_2_11", 0x00000100, 13,  0,   CSR_RW, 0x00000CB0 },
  { "gma_curve_2_12", 0x00000100, 29, 16,   CSR_RW, 0x00000DDC },
  { "GAMMA_CURVE_2_5", 0x00000100, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_6
  { "gma_curve_2_13", 0x00000104, 13,  0,   CSR_RW, 0x00000EF8 },
  { "gma_curve_2_14", 0x00000104, 29, 16,   CSR_RW, 0x00001000 },
  { "GAMMA_CURVE_2_6", 0x00000104, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_7
  { "gma_curve_2_15", 0x00000108, 13,  0,   CSR_RW, 0x000010F8 },
  { "gma_curve_2_16", 0x00000108, 29, 16,   CSR_RW, 0x000011E8 },
  { "GAMMA_CURVE_2_7", 0x00000108, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_8
  { "gma_curve_2_17", 0x0000010C, 13,  0,   CSR_RW, 0x000012CC },
  { "gma_curve_2_18", 0x0000010C, 29, 16,   CSR_RW, 0x000013A8 },
  { "GAMMA_CURVE_2_8", 0x0000010C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_9
  { "gma_curve_2_19", 0x00000110, 13,  0,   CSR_RW, 0x00001478 },
  { "gma_curve_2_20", 0x00000110, 29, 16,   CSR_RW, 0x00001544 },
  { "GAMMA_CURVE_2_9", 0x00000110, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_10
  { "gma_curve_2_21", 0x00000114, 13,  0,   CSR_RW, 0x00001600 },
  { "gma_curve_2_22", 0x00000114, 29, 16,   CSR_RW, 0x000016C4 },
  { "GAMMA_CURVE_2_10", 0x00000114, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_11
  { "gma_curve_2_23", 0x00000118, 13,  0,   CSR_RW, 0x00001770 },
  { "gma_curve_2_24", 0x00000118, 29, 16,   CSR_RW, 0x0000182C },
  { "GAMMA_CURVE_2_11", 0x00000118, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_12
  { "gma_curve_2_25", 0x0000011C, 13,  0,   CSR_RW, 0x00001984 },
  { "gma_curve_2_26", 0x0000011C, 29, 16,   CSR_RW, 0x00001AC8 },
  { "GAMMA_CURVE_2_12", 0x0000011C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_13
  { "gma_curve_2_27", 0x00000120, 13,  0,   CSR_RW, 0x00001C00 },
  { "gma_curve_2_28", 0x00000120, 29, 16,   CSR_RW, 0x00001D28 },
  { "GAMMA_CURVE_2_13", 0x00000120, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_14
  { "gma_curve_2_29", 0x00000124, 13,  0,   CSR_RW, 0x00001E48 },
  { "gma_curve_2_30", 0x00000124, 29, 16,   CSR_RW, 0x00001F5C },
  { "GAMMA_CURVE_2_14", 0x00000124, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_15
  { "gma_curve_2_31", 0x00000128, 13,  0,   CSR_RW, 0x00002060 },
  { "gma_curve_2_32", 0x00000128, 29, 16,   CSR_RW, 0x00002168 },
  { "GAMMA_CURVE_2_15", 0x00000128, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_16
  { "gma_curve_2_33", 0x0000012C, 13,  0,   CSR_RW, 0x00002260 },
  { "gma_curve_2_34", 0x0000012C, 29, 16,   CSR_RW, 0x00002358 },
  { "GAMMA_CURVE_2_16", 0x0000012C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_17
  { "gma_curve_2_35", 0x00000130, 13,  0,   CSR_RW, 0x00002440 },
  { "gma_curve_2_36", 0x00000130, 29, 16,   CSR_RW, 0x0000252C },
  { "GAMMA_CURVE_2_17", 0x00000130, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_18
  { "gma_curve_2_37", 0x00000134, 13,  0,   CSR_RW, 0x0000260C },
  { "gma_curve_2_38", 0x00000134, 29, 16,   CSR_RW, 0x000026E8 },
  { "GAMMA_CURVE_2_18", 0x00000134, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_19
  { "gma_curve_2_39", 0x00000138, 13,  0,   CSR_RW, 0x000027C0 },
  { "gma_curve_2_40", 0x00000138, 29, 16,   CSR_RW, 0x00002890 },
  { "GAMMA_CURVE_2_19", 0x00000138, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_20
  { "gma_curve_2_41", 0x0000013C, 13,  0,   CSR_RW, 0x00002A28 },
  { "gma_curve_2_42", 0x0000013C, 29, 16,   CSR_RW, 0x00002BB0 },
  { "GAMMA_CURVE_2_20", 0x0000013C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_21
  { "gma_curve_2_43", 0x00000140, 13,  0,   CSR_RW, 0x00002D28 },
  { "gma_curve_2_44", 0x00000140, 29, 16,   CSR_RW, 0x00002E90 },
  { "GAMMA_CURVE_2_21", 0x00000140, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_22
  { "gma_curve_2_45", 0x00000144, 13,  0,   CSR_RW, 0x00002FF4 },
  { "gma_curve_2_46", 0x00000144, 29, 16,   CSR_RW, 0x00003140 },
  { "GAMMA_CURVE_2_22", 0x00000144, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_23
  { "gma_curve_2_47", 0x00000148, 13,  0,   CSR_RW, 0x00003298 },
  { "gma_curve_2_48", 0x00000148, 29, 16,   CSR_RW, 0x000033D4 },
  { "GAMMA_CURVE_2_23", 0x00000148, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_24
  { "gma_curve_2_49", 0x0000014C, 13,  0,   CSR_RW, 0x00003514 },
  { "gma_curve_2_50", 0x0000014C, 29, 16,   CSR_RW, 0x00003640 },
  { "GAMMA_CURVE_2_24", 0x0000014C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_25
  { "gma_curve_2_51", 0x00000150, 13,  0,   CSR_RW, 0x00003774 },
  { "gma_curve_2_52", 0x00000150, 29, 16,   CSR_RW, 0x00003894 },
  { "GAMMA_CURVE_2_25", 0x00000150, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_26
  { "gma_curve_2_53", 0x00000154, 13,  0,   CSR_RW, 0x000039B8 },
  { "gma_curve_2_54", 0x00000154, 29, 16,   CSR_RW, 0x00003AD0 },
  { "GAMMA_CURVE_2_26", 0x00000154, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_27
  { "gma_curve_2_55", 0x00000158, 13,  0,   CSR_RW, 0x00003BE4 },
  { "gma_curve_2_56", 0x00000158, 29, 16,   CSR_RW, 0x00003CF0 },
  { "GAMMA_CURVE_2_27", 0x00000158, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gamma_curve_2_28
  { "gma_curve_2_57", 0x0000015C, 13,  0,   CSR_RW, 0x00003DFC },
  { "gma_curve_2_58", 0x0000015C, 29, 16,   CSR_RW, 0x00003F00 },
  { "GAMMA_CURVE_2_28", 0x0000015C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_debug_mon_sel
  { "debug_mon_sel", 0x00000160,  0,  0,   CSR_RW, 0x00000000 },
  { "WORD_DEBUG_MON_SEL", 0x00000160, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_DGMA_H_
