#ifndef CSR_TABLE_TX_PHYCFG_H_
#define CSR_TABLE_TX_PHYCFG_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_tx_phycfg[] =
{
  // WORD mipi_tx_phy00
  { "rg_mipi_tx_ck2p5g_sel", 0x00000000,  0,  0,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY00", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy01
  { "rg_mipi_tx_lev09_sel", 0x00000004,  1,  0,   CSR_RW, 0x00000001 },
  { "rg_mipi_tx_ref09_iminus", 0x00000004,  8,  8,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_ref09_iplus", 0x00000004, 16, 16,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY01", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy02
  { "rg_mipi_tx_lprx_lpf_en", 0x00000008,  0,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_lprx_cd_vth_sel", 0x00000008,  9,  8,   CSR_RW, 0x00000001 },
  { "rg_mipi_tx_lprx_disp_vth_sel", 0x00000008, 17, 16,   CSR_RW, 0x00000001 },
  { "MIPI_TX_PHY02", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy03
  { "rg_mipi_tx_ln0_clk_sel", 0x0000000C,  0,  0,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY03", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy04
  { "rg_mipi_tx_ln0_gpo_en", 0x00000010,  0,  0,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY04", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy05
  { "rg_mipi_tx_ln0_ref_iminus", 0x00000014,  0,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_ln0_ref_iplus", 0x00000014, 16, 16,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY05", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy06
  { "rg_mipi_tx_ln0_hs_rminus", 0x00000018,  2,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_ln0_hs_rplus", 0x00000018, 16, 16,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY06", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy07
  { "rg_mipi_tx_ln0_lp_rminus", 0x0000001C,  1,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_ln0_lp_iminus", 0x0000001C,  8,  8,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_ln0_lp_iplus", 0x0000001C, 16, 16,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY07", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy08
  { "rg_mipi_tx_ln0_lev04_sel", 0x00000020,  1,  0,   CSR_RW, 0x00000001 },
  { "rg_mipi_tx_ln0_lev12_sel", 0x00000020, 17, 16,   CSR_RW, 0x00000001 },
  { "MIPI_TX_PHY08", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy09
  { "rg_mipi_tx_ln1_clk_sel", 0x00000024,  0,  0,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY09", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy10
  { "rg_mipi_tx_ln1_gpo_en", 0x00000028,  0,  0,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY10", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy11
  { "rg_mipi_tx_ln1_ref_iminus", 0x0000002C,  0,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_ln1_ref_iplus", 0x0000002C, 16, 16,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY11", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy12
  { "rg_mipi_tx_ln1_hs_rminus", 0x00000030,  2,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_ln1_hs_rplus", 0x00000030, 16, 16,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY12", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy13
  { "rg_mipi_tx_ln1_lp_rminus", 0x00000034,  1,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_ln1_lp_iminus", 0x00000034,  8,  8,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_ln1_lp_iplus", 0x00000034, 16, 16,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY13", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy14
  { "rg_mipi_tx_ln1_lev04_sel", 0x00000038,  1,  0,   CSR_RW, 0x00000001 },
  { "rg_mipi_tx_ln1_lev12_sel", 0x00000038, 17, 16,   CSR_RW, 0x00000001 },
  { "MIPI_TX_PHY14", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy15
  { "rg_mipi_tx_ln2_clk_sel", 0x0000003C,  0,  0,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY15", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy16
  { "rg_mipi_tx_ln2_gpo_en", 0x00000040,  0,  0,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY16", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy17
  { "rg_mipi_tx_ln2_ref_iminus", 0x00000044,  0,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_ln2_ref_iplus", 0x00000044, 16, 16,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY17", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy18
  { "rg_mipi_tx_ln2_hs_rminus", 0x00000048,  2,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_ln2_hs_rplus", 0x00000048, 16, 16,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY18", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy19
  { "rg_mipi_tx_ln2_lp_rminus", 0x0000004C,  1,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_ln2_lp_iminus", 0x0000004C,  8,  8,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_ln2_lp_iplus", 0x0000004C, 16, 16,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY19", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy20
  { "rg_mipi_tx_ln2_lev04_sel", 0x00000050,  1,  0,   CSR_RW, 0x00000001 },
  { "rg_mipi_tx_ln2_lev12_sel", 0x00000050, 17, 16,   CSR_RW, 0x00000001 },
  { "MIPI_TX_PHY20", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy21
  { "rg_mipi_tx_ln3_clk_sel", 0x00000054,  0,  0,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY21", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy22
  { "rg_mipi_tx_ln3_gpo_en", 0x00000058,  0,  0,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY22", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy23
  { "rg_mipi_tx_ln3_ref_iminus", 0x0000005C,  0,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_ln3_ref_iplus", 0x0000005C, 16, 16,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY23", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy24
  { "rg_mipi_tx_ln3_hs_rminus", 0x00000060,  2,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_ln3_hs_rplus", 0x00000060, 16, 16,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY24", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy25
  { "rg_mipi_tx_ln3_lp_rminus", 0x00000064,  1,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_ln3_lp_iminus", 0x00000064,  8,  8,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_ln3_lp_iplus", 0x00000064, 16, 16,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY25", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy26
  { "rg_mipi_tx_ln3_lev04_sel", 0x00000068,  1,  0,   CSR_RW, 0x00000001 },
  { "rg_mipi_tx_ln3_lev12_sel", 0x00000068, 17, 16,   CSR_RW, 0x00000001 },
  { "MIPI_TX_PHY26", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy27
  { "rg_mipi_tx_ln4_clk_sel", 0x0000006C,  0,  0,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY27", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy28
  { "rg_mipi_tx_ln4_gpo_en", 0x00000070,  0,  0,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY28", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy29
  { "rg_mipi_tx_ln4_ref_iminus", 0x00000074,  0,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_ln4_ref_iplus", 0x00000074, 16, 16,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY29", 0x00000074, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy30
  { "rg_mipi_tx_ln4_hs_rminus", 0x00000078,  2,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_ln4_hs_rplus", 0x00000078, 16, 16,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY30", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy31
  { "rg_mipi_tx_ln4_lp_rminus", 0x0000007C,  1,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_ln4_lp_iminus", 0x0000007C,  8,  8,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_ln4_lp_iplus", 0x0000007C, 16, 16,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY31", 0x0000007C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy32
  { "rg_mipi_tx_ln4_lev04_sel", 0x00000080,  1,  0,   CSR_RW, 0x00000001 },
  { "rg_mipi_tx_ln4_lev12_sel", 0x00000080, 17, 16,   CSR_RW, 0x00000001 },
  { "MIPI_TX_PHY32", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy33
  { "rg_mipi_tx_reserved", 0x00000084, 16,  0,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY33", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_phy34
  { "rg_mipi_tx_lane_sel", 0x00000088,  4,  0,   CSR_RW, 0x00000000 },
  { "MIPI_TX_PHY34", 0x00000088, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_fpll_enable0
  { "rg_mipi_tx_fpll_pll_rstb", 0x0000008C,  0,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_pll_en", 0x0000008C,  8,  8,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_sscg_en", 0x0000008C, 16, 16,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_frac_en", 0x0000008C, 24, 24,   CSR_RW, 0x00000000 },
  { "MIPI_TX_FPLL_ENABLE0", 0x0000008C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_fpll_enable1
  { "rg_mipi_tx_fpll_ldo_bias_en", 0x00000090,  0,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_ldo_out_en", 0x00000090,  8,  8,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_ic_ico_en", 0x00000090, 16, 16,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_kband_en", 0x00000090, 24, 24,   CSR_RW, 0x00000000 },
  { "MIPI_TX_FPLL_ENABLE1", 0x00000090, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_fpll_overwrite_enable
  { "rg_mipi_tx_fpll_kband_complete", 0x00000094,  0,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_zerostart", 0x00000094,  8,  8,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_pll_out", 0x00000094, 16, 16,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_ic_ico", 0x00000094, 24, 24,   CSR_RW, 0x00000000 },
  { "MIPI_TX_FPLL_OVERWRITE_ENABLE", 0x00000094, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_fpll_dig_en
  { "rg_mipi_tx_fpll_post_div1_en", 0x00000098,  0,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_post_div2_en", 0x00000098,  8,  8,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_post_div3_en", 0x00000098, 16, 16,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_post_div4_halfdiv_en", 0x00000098, 24, 24,   CSR_RW, 0x00000000 },
  { "MIPI_TX_FPLL_DIG_EN", 0x00000098, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_fpll_ref_gen
  { "rg_mipi_tx_fpll_v09_sel", 0x0000009C,  1,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_ref_iplus", 0x0000009C,  8,  8,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_ref_iminus", 0x0000009C, 16, 16,   CSR_RW, 0x00000000 },
  { "MIPI_TX_FPLL_REF_GEN", 0x0000009C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_fpll_pfd_sel
  { "rg_mipi_tx_fpll_pfd_ckico_sel", 0x000000A0,  0,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_pfd_clk_retimed_sel", 0x000000A0,  8,  8,   CSR_RW, 0x00000000 },
  { "MIPI_TX_FPLL_PFD_SEL", 0x000000A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_fpll_cp_sel
  { "rg_mipi_tx_fpll_ip_sel", 0x000000A4,  3,  0,   CSR_RW, 0x00000002 },
  { "rg_mipi_tx_fpll_ii_sel", 0x000000A4, 11,  8,   CSR_RW, 0x00000002 },
  { "MIPI_TX_FPLL_CP_SEL", 0x000000A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_fpll_lf_sel
  { "rg_mipi_tx_fpll_rp_sel", 0x000000A8,  3,  0,   CSR_RW, 0x00000004 },
  { "rg_mipi_tx_fpll_ci_sel", 0x000000A8,  9,  8,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_cp_sel", 0x000000A8, 17, 16,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_pll_op_sel", 0x000000A8, 27, 24,   CSR_RW, 0x00000007 },
  { "MIPI_TX_FPLL_LF_SEL", 0x000000A8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_fpll_lpf_sel
  { "rg_mipi_tx_fpll_rlp_sel", 0x000000AC,  1,  0,   CSR_RW, 0x00000001 },
  { "rg_mipi_tx_fpll_clp_sel", 0x000000AC,  9,  8,   CSR_RW, 0x00000001 },
  { "rg_mipi_tx_fpll_rlp_sel_2", 0x000000AC, 17, 16,   CSR_RW, 0x00000001 },
  { "rg_mipi_tx_fpll_clp_sel_2", 0x000000AC, 25, 24,   CSR_RW, 0x00000001 },
  { "MIPI_TX_FPLL_LPF_SEL", 0x000000AC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_fpll_kband_sel
  { "rg_mipi_tx_fpll_sq_bi_sel", 0x000000B0,  4,  0,   CSR_RW, 0x00000007 },
  { "rg_mipi_tx_fpll_enable_out_del_sel", 0x000000B0,  9,  8,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_freqm_count_ext_sel", 0x000000B0, 18, 16,   CSR_RW, 0x00000001 },
  { "rg_mipi_tx_fpll_pll_out_del_sel", 0x000000B0, 25, 24,   CSR_RW, 0x00000001 },
  { "MIPI_TX_FPLL_KBAND_SEL", 0x000000B0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_fpll_div_sel
  { "rg_mipi_tx_fpll_ref_div_sel", 0x000000B4,  1,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_ffb_prediv_sel", 0x000000B4,  6,  4,   CSR_RW, 0x00000001 },
  { "rg_mipi_tx_fpll_ffb_halfdiv_sel", 0x000000B4,  9,  8,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_ffb_intdiv_sel", 0x000000B4, 23, 16,   CSR_RW, 0x00000001 },
  { "MIPI_TX_FPLL_DIV_SEL", 0x000000B4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_fpll_post_div_sel
  { "rg_mipi_tx_fpll_post_div1_sel", 0x000000B8,  7,  0,   CSR_RW, 0x00000001 },
  { "rg_mipi_tx_fpll_post_div2_sel", 0x000000B8, 15,  8,   CSR_RW, 0x00000001 },
  { "rg_mipi_tx_fpll_post_div3_sel", 0x000000B8, 23, 16,   CSR_RW, 0x00000001 },
  { "rg_mipi_tx_fpll_post_div4_halfdiv_sel", 0x000000B8, 25, 24,   CSR_RW, 0x00000000 },
  { "MIPI_TX_FPLL_POST_DIV_SEL", 0x000000B8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_fpll_postdiv_clkin_sel
  { "rg_mipi_tx_fpll_postdiv_clkin_sel_1", 0x000000BC,  0,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_postdiv_clkin_sel_2", 0x000000BC,  8,  8,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_postdiv_clkin_sel_3", 0x000000BC, 16, 16,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_postdiv_clkin_sel_4", 0x000000BC, 24, 24,   CSR_RW, 0x00000000 },
  { "MIPI_TX_FPLL_POSTDIV_CLKIN_SEL", 0x000000BC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_fpll_overwrite
  { "rg_mipi_tx_fpll_band", 0x000000C0,  5,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_control_sel", 0x000000C0,  8,  8,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_digital_control_sel", 0x000000C0, 16, 16,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_ico_sel", 0x000000C0, 24, 24,   CSR_RW, 0x00000000 },
  { "MIPI_TX_FPLL_OVERWRITE", 0x000000C0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_fpll_mon_clk_sel
  { "rg_mipi_tx_fpll_mon_en", 0x000000C4,  0,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_mon_clk_sel", 0x000000C4, 10,  8,   CSR_RW, 0x00000000 },
  { "MIPI_TX_FPLL_MON_CLK_SEL", 0x000000C4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_fpll_reserved
  { "rg_mipi_tx_fpll_reserved", 0x000000C8, 15,  0,   CSR_RW, 0x00000000 },
  { "MIPI_TX_FPLL_RESERVED", 0x000000C8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_fpll_sscg_f
  { "rg_mipi_tx_fpll_sscg_f", 0x000000CC, 23,  0,   CSR_RW, 0x00000000 },
  { "MIPI_TX_FPLL_SSCG_F", 0x000000CC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_fpll_sscg_sel
  { "rg_mipi_tx_fpll_sscg_deviation_unit", 0x000000D0, 10,  0,   CSR_RW, 0x00000000 },
  { "rg_mipi_tx_fpll_sscg_freq_count", 0x000000D0, 27, 16,   CSR_RW, 0x00000000 },
  { "MIPI_TX_FPLL_SSCG_SEL", 0x000000D0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi_tx_fpll_rgs_enable0
  { "rgs_mipi_tx_fpll_ldo_bias_en_d", 0x000000D4,  0,  0,   CSR_RO, 0x00000000 },
  { "rgs_mipi_tx_fpll_ldo_out_en_d", 0x000000D4,  8,  8,   CSR_RO, 0x00000000 },
  { "rgs_mipi_tx_fpll_ic_ico_en_d", 0x000000D4, 16, 16,   CSR_RO, 0x00000000 },
  { "rgs_mipi_tx_fpll_kband_en_mux_d", 0x000000D4, 24, 24,   CSR_RO, 0x00000000 },
  { "MIPI_TX_FPLL_RGS_ENABLE0", 0x000000D4, 31, 0,   CSR_RO, 0x00000000 },
  // WORD mipi_tx_fpll_rgs_enable1
  { "rgs_mipi_tx_fpll_kband_complete_mux_d", 0x000000D8,  0,  0,   CSR_RO, 0x00000000 },
  { "rgs_mipi_tx_fpll_zerostart_mux_d", 0x000000D8,  8,  8,   CSR_RO, 0x00000000 },
  { "rgs_mipi_tx_fpll_pll_out_mux_d", 0x000000D8, 16, 16,   CSR_RO, 0x00000000 },
  { "rgs_mipi_tx_fpll_band_mux_d", 0x000000D8, 29, 24,   CSR_RO, 0x00000000 },
  { "MIPI_TX_FPLL_RGS_ENABLE1", 0x000000D8, 31, 0,   CSR_RO, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_TX_PHYCFG_H_
