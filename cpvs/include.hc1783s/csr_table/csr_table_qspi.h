#ifndef CSR_TABLE_QSPI_H_
#define CSR_TABLE_QSPI_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_qspi[] =
{
  // WORD qspi00
  { "start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "QSPI00", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD qspi01
  { "enable", 0x00000004,  0,  0,   CSR_RW, 0x00000000 },
  { "sd", 0x00000004,  8,  8,   CSR_RW, 0x00000000 },
  { "slp", 0x00000004, 16, 16,   CSR_RW, 0x00000000 },
  { "QSPI01", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi02
  { "irq_clear_spi_done", 0x00000008,  0,  0,  CSR_W1P, 0x00000000 },
  { "irq_clear_auto_done", 0x00000008,  2,  2,  CSR_W1P, 0x00000000 },
  { "irq_clear_done", 0x00000008,  4,  4,  CSR_W1P, 0x00000000 },
  { "irq_clear_error", 0x00000008,  6,  6,  CSR_W1P, 0x00000000 },
  { "irq_clear_tx_fifo_full", 0x00000008,  8,  8,  CSR_W1P, 0x00000000 },
  { "irq_clear_tx_fifo_empty", 0x00000008, 10, 10,  CSR_W1P, 0x00000000 },
  { "irq_clear_rx_fifo_full", 0x00000008, 12, 12,  CSR_W1P, 0x00000000 },
  { "irq_clear_rx_fifo_empty", 0x00000008, 14, 14,  CSR_W1P, 0x00000000 },
  { "QSPI02", 0x00000008, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD qspi03
  { "status_spi_done", 0x0000000C,  0,  0,   CSR_RO, 0x00000000 },
  { "status_auto_done", 0x0000000C,  2,  2,   CSR_RO, 0x00000000 },
  { "status_done", 0x0000000C,  4,  4,   CSR_RO, 0x00000000 },
  { "status_error", 0x0000000C,  6,  6,   CSR_RO, 0x00000000 },
  { "status_tx_fifo_full", 0x0000000C,  8,  8,   CSR_RO, 0x00000000 },
  { "status_tx_fifo_empty", 0x0000000C, 10, 10,   CSR_RO, 0x00000000 },
  { "status_rx_fifo_full", 0x0000000C, 12, 12,   CSR_RO, 0x00000000 },
  { "status_rx_fifo_empty", 0x0000000C, 14, 14,   CSR_RO, 0x00000000 },
  { "QSPI03", 0x0000000C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD qspi04
  { "irq_mask_spi_done", 0x00000010,  0,  0,   CSR_RW, 0x00000001 },
  { "irq_mask_auto_done", 0x00000010,  2,  2,   CSR_RW, 0x00000001 },
  { "irq_mask_done", 0x00000010,  4,  4,   CSR_RW, 0x00000001 },
  { "irq_mask_error", 0x00000010,  6,  6,   CSR_RW, 0x00000001 },
  { "irq_mask_tx_fifo_full", 0x00000010,  8,  8,   CSR_RW, 0x00000001 },
  { "irq_mask_tx_fifo_empty", 0x00000010, 10, 10,   CSR_RW, 0x00000001 },
  { "irq_mask_rx_fifo_full", 0x00000010, 12, 12,   CSR_RW, 0x00000001 },
  { "irq_mask_rx_fifo_empty", 0x00000010, 14, 14,   CSR_RW, 0x00000001 },
  { "QSPI04", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi05
  { "spi_busy", 0x00000014,  0,  0,   CSR_RO, 0x00000000 },
  { "spi_done", 0x00000014,  2,  2,   CSR_RO, 0x00000000 },
  { "auto_busy", 0x00000014,  4,  4,   CSR_RO, 0x00000000 },
  { "auto_done", 0x00000014,  6,  6,   CSR_RO, 0x00000000 },
  { "done", 0x00000014,  8,  8,   CSR_RO, 0x00000000 },
  { "error", 0x00000014, 10, 10,   CSR_RO, 0x00000000 },
  { "tx_fifo_full", 0x00000014, 16, 16,   CSR_RO, 0x00000000 },
  { "tx_fifo_empty", 0x00000014, 18, 18,   CSR_RO, 0x00000000 },
  { "rx_fifo_full", 0x00000014, 20, 20,   CSR_RO, 0x00000000 },
  { "rx_fifo_empty", 0x00000014, 22, 22,   CSR_RO, 0x00000000 },
  { "QSPI05", 0x00000014, 31, 0,   CSR_RO, 0x00000000 },
  // WORD qspi06
  { "mst_state", 0x00000018,  2,  0,   CSR_RO, 0x00000000 },
  { "auto_state", 0x00000018,  7,  4,   CSR_RO, 0x00000000 },
  { "cnt_frame", 0x00000018, 28, 16,   CSR_RO, 0x00000000 },
  { "QSPI06", 0x00000018, 31, 0,   CSR_RO, 0x00000000 },
  // WORD qspi07
  { "df_cnt", 0x0000001C, 15,  0,   CSR_RO, 0x00000000 },
  { "tdf_cnt", 0x0000001C, 31, 16,   CSR_RO, 0x00000000 },
  { "QSPI07", 0x0000001C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD qspi08
  { "transfer_mode", 0x00000020,  0,  0,   CSR_RW, 0x00000000 },
  { "frame_format", 0x00000020,  3,  2,   CSR_RW, 0x00000000 },
  { "data_frame_size", 0x00000020,  8,  4,   CSR_RW, 0x0000001F },
  { "msb_first", 0x00000020, 10, 10,   CSR_RW, 0x00000001 },
  { "endian_sel", 0x00000020, 12, 12,   CSR_RW, 0x00000000 },
  { "transfer_type", 0x00000020, 15, 14,   CSR_RW, 0x00000000 },
  { "inst_l", 0x00000020, 17, 16,   CSR_RW, 0x00000002 },
  { "addr_l", 0x00000020, 21, 18,   CSR_RW, 0x00000004 },
  { "wait_l", 0x00000020, 29, 24,   CSR_RW, 0x00000007 },
  { "QSPI08", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi09
  { "ndf", 0x00000024, 15,  0,   CSR_RW, 0x00000000 },
  { "ndfw", 0x00000024, 31, 16,   CSR_RW, 0x00000000 },
  { "QSPI09", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi10
  { "sck_dv", 0x00000028,  7,  0,   CSR_RW, 0x00000002 },
  { "sck_pol", 0x00000028,  8,  8,   CSR_RW, 0x00000000 },
  { "sck_pha", 0x00000028, 10, 10,   CSR_RW, 0x00000000 },
  { "sck_dly", 0x00000028, 23, 16,   CSR_RW, 0x00000000 },
  { "rx_delay", 0x00000028, 27, 24,   CSR_RW, 0x00000001 },
  { "QSPI10", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi11
  { "wr_data", 0x0000002C, 31,  0,   CSR_RW, 0x00000000 },
  { "QSPI11", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi12
  { "rd_data", 0x00000030, 31,  0,   CSR_RO, 0x00000000 },
  { "QSPI12", 0x00000030, 31, 0,   CSR_RO, 0x00000000 },
  // WORD qspi13
  { "rd_data_r", 0x00000034, 31,  0,   CSR_RO, 0x00000000 },
  { "QSPI13", 0x00000034, 31, 0,   CSR_RO, 0x00000000 },
  // WORD qspi14
  { "dma_mode_en", 0x00000038,  0,  0,   CSR_RW, 0x00000000 },
  { "auto_mode_en", 0x00000038,  8,  8,   CSR_RW, 0x00000000 },
  { "status_bo_sel", 0x00000038, 10, 10,   CSR_RW, 0x00000000 },
  { "timeout_free", 0x00000038, 12, 12,   CSR_RW, 0x00000000 },
  { "pre_scaler_th", 0x00000038, 23, 16,   CSR_RW, 0x000000C8 },
  { "QSPI14", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi15
  { "flash_start_addr", 0x0000003C, 15,  0,   CSR_RW, 0x00000000 },
  { "flash_page_size", 0x0000003C, 31, 16,   CSR_RW, 0x00000000 },
  { "QSPI15", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi16
  { "status_flash", 0x00000040, 31,  0,   CSR_RO, 0x00000000 },
  { "QSPI16", 0x00000040, 31, 0,   CSR_RO, 0x00000000 },
  // WORD qspi17
  { "flash_addr", 0x00000044, 15,  0,   CSR_RO, 0x00000000 },
  { "QSPI17", 0x00000044, 31, 0,   CSR_RO, 0x00000000 },
  // WORD qspi18
  { "op_code_page_rd", 0x00000048,  7,  0,   CSR_RW, 0x00000000 },
  { "QSPI18", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi19
  { "transfer_mode_page_rd", 0x0000004C,  0,  0,   CSR_RW, 0x00000000 },
  { "frame_format_page_rd", 0x0000004C,  9,  8,   CSR_RW, 0x00000000 },
  { "transfer_type_page_rd", 0x0000004C, 17, 16,   CSR_RW, 0x00000000 },
  { "data_frame_size_page_rd", 0x0000004C, 28, 24,   CSR_RW, 0x00000007 },
  { "QSPI19", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi20
  { "inst_l_page_rd", 0x00000050,  1,  0,   CSR_RW, 0x00000002 },
  { "addr_l_page_rd", 0x00000050, 11,  8,   CSR_RW, 0x00000004 },
  { "wait_l_page_rd", 0x00000050, 21, 16,   CSR_RW, 0x00000007 },
  { "QSPI20", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi21
  { "ndf_page_rd", 0x00000054, 12,  0,   CSR_RW, 0x00000000 },
  { "ndfw_page_rd", 0x00000054, 28, 16,   CSR_RW, 0x00000000 },
  { "QSPI21", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi22
  { "op_code_page_rd_poll", 0x00000058,  7,  0,   CSR_RW, 0x00000000 },
  { "QSPI22", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi23
  { "reg_page_rd_poll", 0x0000005C, 31,  0,   CSR_RW, 0x00000000 },
  { "QSPI23", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi24
  { "transfer_mode_page_rd_poll", 0x00000060,  0,  0,   CSR_RW, 0x00000000 },
  { "frame_format_page_rd_poll", 0x00000060,  9,  8,   CSR_RW, 0x00000000 },
  { "transfer_type_page_rd_poll", 0x00000060, 17, 16,   CSR_RW, 0x00000000 },
  { "data_frame_size_page_rd_poll", 0x00000060, 28, 24,   CSR_RW, 0x00000007 },
  { "QSPI24", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi25
  { "inst_l_page_rd_poll", 0x00000064,  1,  0,   CSR_RW, 0x00000002 },
  { "addr_l_page_rd_poll", 0x00000064, 11,  8,   CSR_RW, 0x00000004 },
  { "wait_l_page_rd_poll", 0x00000064, 21, 16,   CSR_RW, 0x00000007 },
  { "QSPI25", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi26
  { "ndf_page_rd_poll", 0x00000068, 12,  0,   CSR_RW, 0x00000000 },
  { "ndfw_page_rd_poll", 0x00000068, 28, 16,   CSR_RW, 0x00000000 },
  { "QSPI26", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi27
  { "op_code_read", 0x0000006C,  7,  0,   CSR_RW, 0x00000000 },
  { "QSPI27", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi28
  { "reg_read", 0x00000070, 31,  0,   CSR_RW, 0x00000000 },
  { "QSPI28", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi29
  { "transfer_mode_read", 0x00000074,  0,  0,   CSR_RW, 0x00000000 },
  { "frame_format_read", 0x00000074,  9,  8,   CSR_RW, 0x00000000 },
  { "transfer_type_read", 0x00000074, 17, 16,   CSR_RW, 0x00000000 },
  { "data_frame_size_read", 0x00000074, 28, 24,   CSR_RW, 0x00000007 },
  { "QSPI29", 0x00000074, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi30
  { "inst_l_read", 0x00000078,  1,  0,   CSR_RW, 0x00000002 },
  { "addr_l_read", 0x00000078, 11,  8,   CSR_RW, 0x00000004 },
  { "wait_l_read", 0x00000078, 21, 16,   CSR_RW, 0x00000007 },
  { "QSPI30", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi31
  { "ndf_read", 0x0000007C, 12,  0,   CSR_RW, 0x00000000 },
  { "ndfw_read", 0x0000007C, 28, 16,   CSR_RW, 0x00000000 },
  { "QSPI31", 0x0000007C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi32
  { "op_code_we_pg", 0x00000080,  7,  0,   CSR_RW, 0x00000000 },
  { "QSPI32", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi33
  { "reg_we_pg", 0x00000084, 31,  0,   CSR_RW, 0x00000000 },
  { "QSPI33", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi34
  { "transfer_mode_we_pg", 0x00000088,  0,  0,   CSR_RW, 0x00000000 },
  { "frame_format_we_pg", 0x00000088,  9,  8,   CSR_RW, 0x00000000 },
  { "transfer_type_we_pg", 0x00000088, 17, 16,   CSR_RW, 0x00000000 },
  { "data_frame_size_we_pg", 0x00000088, 28, 24,   CSR_RW, 0x00000007 },
  { "QSPI34", 0x00000088, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi35
  { "inst_l_we_pg", 0x0000008C,  1,  0,   CSR_RW, 0x00000002 },
  { "addr_l_we_pg", 0x0000008C, 11,  8,   CSR_RW, 0x00000004 },
  { "wait_l_we_pg", 0x0000008C, 21, 16,   CSR_RW, 0x00000007 },
  { "QSPI35", 0x0000008C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi36
  { "ndf_we_pg", 0x00000090, 12,  0,   CSR_RW, 0x00000000 },
  { "ndfw_we_pg", 0x00000090, 28, 16,   CSR_RW, 0x00000000 },
  { "QSPI36", 0x00000090, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi37
  { "op_code_we_pg_poll", 0x00000094,  7,  0,   CSR_RW, 0x00000000 },
  { "QSPI37", 0x00000094, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi38
  { "reg_we_pg_poll", 0x00000098, 31,  0,   CSR_RW, 0x00000000 },
  { "QSPI38", 0x00000098, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi39
  { "transfer_mode_we_pg_poll", 0x0000009C,  0,  0,   CSR_RW, 0x00000000 },
  { "frame_format_we_pg_poll", 0x0000009C,  9,  8,   CSR_RW, 0x00000000 },
  { "transfer_type_we_pg_poll", 0x0000009C, 17, 16,   CSR_RW, 0x00000000 },
  { "data_frame_size_we_pg_poll", 0x0000009C, 28, 24,   CSR_RW, 0x00000007 },
  { "QSPI39", 0x0000009C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi40
  { "inst_l_we_pg_poll", 0x000000A0,  1,  0,   CSR_RW, 0x00000002 },
  { "addr_l_we_pg_poll", 0x000000A0, 11,  8,   CSR_RW, 0x00000004 },
  { "wait_l_we_pg_poll", 0x000000A0, 21, 16,   CSR_RW, 0x00000007 },
  { "QSPI40", 0x000000A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi41
  { "ndf_we_pg_poll", 0x000000A4, 12,  0,   CSR_RW, 0x00000000 },
  { "ndfw_we_pg_poll", 0x000000A4, 28, 16,   CSR_RW, 0x00000000 },
  { "QSPI41", 0x000000A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi42
  { "op_code_prog", 0x000000A8,  7,  0,   CSR_RW, 0x00000000 },
  { "QSPI42", 0x000000A8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi43
  { "reg_prog", 0x000000AC, 31,  0,   CSR_RW, 0x00000000 },
  { "QSPI43", 0x000000AC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi44
  { "transfer_mode_prog", 0x000000B0,  0,  0,   CSR_RW, 0x00000000 },
  { "frame_format_prog", 0x000000B0,  9,  8,   CSR_RW, 0x00000000 },
  { "transfer_type_prog", 0x000000B0, 17, 16,   CSR_RW, 0x00000000 },
  { "data_frame_size_prog", 0x000000B0, 28, 24,   CSR_RW, 0x00000007 },
  { "QSPI44", 0x000000B0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi45
  { "inst_l_prog", 0x000000B4,  1,  0,   CSR_RW, 0x00000002 },
  { "addr_l_prog", 0x000000B4, 11,  8,   CSR_RW, 0x00000004 },
  { "wait_l_prog", 0x000000B4, 21, 16,   CSR_RW, 0x00000007 },
  { "QSPI45", 0x000000B4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi46
  { "ndf_prog", 0x000000B8, 12,  0,   CSR_RW, 0x00000000 },
  { "ndfw_prog", 0x000000B8, 28, 16,   CSR_RW, 0x00000000 },
  { "QSPI46", 0x000000B8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi47
  { "op_code_we_exe", 0x000000BC,  7,  0,   CSR_RW, 0x00000000 },
  { "QSPI47", 0x000000BC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi48
  { "reg_we_exe", 0x000000C0, 31,  0,   CSR_RW, 0x00000000 },
  { "QSPI48", 0x000000C0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi49
  { "transfer_mode_we_exe", 0x000000C4,  0,  0,   CSR_RW, 0x00000000 },
  { "frame_format_we_exe", 0x000000C4,  9,  8,   CSR_RW, 0x00000000 },
  { "transfer_type_we_exe", 0x000000C4, 17, 16,   CSR_RW, 0x00000000 },
  { "data_frame_size_we_exe", 0x000000C4, 28, 24,   CSR_RW, 0x00000007 },
  { "QSPI49", 0x000000C4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi50
  { "inst_l_we_exe", 0x000000C8,  1,  0,   CSR_RW, 0x00000002 },
  { "addr_l_we_exe", 0x000000C8, 11,  8,   CSR_RW, 0x00000004 },
  { "wait_l_we_exe", 0x000000C8, 21, 16,   CSR_RW, 0x00000007 },
  { "QSPI50", 0x000000C8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi51
  { "ndf_we_exe", 0x000000CC, 12,  0,   CSR_RW, 0x00000000 },
  { "ndfw_we_exe", 0x000000CC, 28, 16,   CSR_RW, 0x00000000 },
  { "QSPI51", 0x000000CC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi52
  { "op_code_we_exe_poll", 0x000000D0,  7,  0,   CSR_RW, 0x00000000 },
  { "QSPI52", 0x000000D0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi53
  { "reg_we_exe_poll", 0x000000D4, 31,  0,   CSR_RW, 0x00000000 },
  { "QSPI53", 0x000000D4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi54
  { "transfer_mode_we_exe_poll", 0x000000D8,  0,  0,   CSR_RW, 0x00000000 },
  { "frame_format_we_exe_poll", 0x000000D8,  9,  8,   CSR_RW, 0x00000000 },
  { "transfer_type_we_exe_poll", 0x000000D8, 17, 16,   CSR_RW, 0x00000000 },
  { "data_frame_size_we_exe_poll", 0x000000D8, 28, 24,   CSR_RW, 0x00000007 },
  { "QSPI54", 0x000000D8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi55
  { "inst_l_we_exe_poll", 0x000000DC,  1,  0,   CSR_RW, 0x00000002 },
  { "addr_l_we_exe_poll", 0x000000DC, 11,  8,   CSR_RW, 0x00000004 },
  { "wait_l_we_exe_poll", 0x000000DC, 21, 16,   CSR_RW, 0x00000007 },
  { "QSPI55", 0x000000DC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi56
  { "ndf_we_exe_poll", 0x000000E0, 12,  0,   CSR_RW, 0x00000000 },
  { "ndfw_we_exe_poll", 0x000000E0, 28, 16,   CSR_RW, 0x00000000 },
  { "QSPI56", 0x000000E0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi57
  { "op_code_exec", 0x000000E4,  7,  0,   CSR_RW, 0x00000000 },
  { "QSPI57", 0x000000E4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi58
  { "transfer_mode_exec", 0x000000E8,  0,  0,   CSR_RW, 0x00000000 },
  { "frame_format_exec", 0x000000E8,  9,  8,   CSR_RW, 0x00000000 },
  { "transfer_type_exec", 0x000000E8, 17, 16,   CSR_RW, 0x00000000 },
  { "data_frame_size_exec", 0x000000E8, 28, 24,   CSR_RW, 0x00000007 },
  { "QSPI58", 0x000000E8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi59
  { "inst_l_exec", 0x000000EC,  1,  0,   CSR_RW, 0x00000002 },
  { "addr_l_exec", 0x000000EC, 11,  8,   CSR_RW, 0x00000004 },
  { "wait_l_exec", 0x000000EC, 21, 16,   CSR_RW, 0x00000007 },
  { "QSPI59", 0x000000EC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi60
  { "ndf_exec", 0x000000F0, 12,  0,   CSR_RW, 0x00000000 },
  { "ndfw_exec", 0x000000F0, 28, 16,   CSR_RW, 0x00000000 },
  { "QSPI60", 0x000000F0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi61
  { "op_code_exec_poll", 0x000000F4,  7,  0,   CSR_RW, 0x00000000 },
  { "QSPI61", 0x000000F4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi62
  { "reg_exec_poll", 0x000000F8, 31,  0,   CSR_RW, 0x00000000 },
  { "QSPI62", 0x000000F8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi63
  { "transfer_mode_exec_poll", 0x000000FC,  0,  0,   CSR_RW, 0x00000000 },
  { "frame_format_exec_poll", 0x000000FC,  9,  8,   CSR_RW, 0x00000000 },
  { "transfer_type_exec_poll", 0x000000FC, 17, 16,   CSR_RW, 0x00000000 },
  { "data_frame_size_exec_poll", 0x000000FC, 28, 24,   CSR_RW, 0x00000007 },
  { "QSPI63", 0x000000FC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi64
  { "inst_l_exec_poll", 0x00000100,  1,  0,   CSR_RW, 0x00000002 },
  { "addr_l_exec_poll", 0x00000100, 11,  8,   CSR_RW, 0x00000004 },
  { "wait_l_exec_poll", 0x00000100, 21, 16,   CSR_RW, 0x00000007 },
  { "QSPI64", 0x00000100, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi65
  { "ndf_exec_poll", 0x00000104, 12,  0,   CSR_RW, 0x00000000 },
  { "ndfw_exec_poll", 0x00000104, 28, 16,   CSR_RW, 0x00000000 },
  { "QSPI65", 0x00000104, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi66
  { "run_type_page_rd", 0x00000108,  0,  0,   CSR_RW, 0x00000000 },
  { "QSPI66", 0x00000108, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi67
  { "timeout_time_page_rd", 0x0000010C, 15,  0,   CSR_RW, 0x00000000 },
  { "wait_busy_time_page_rd", 0x0000010C, 31, 16,   CSR_RW, 0x00000000 },
  { "QSPI67", 0x0000010C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi68
  { "valid_status_page_rd_poll", 0x00000110, 31,  0,   CSR_RW, 0x00000000 },
  { "QSPI68", 0x00000110, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi69
  { "timeout_time_page_rd_poll", 0x00000114, 15,  0,   CSR_RW, 0x00000000 },
  { "wait_busy_time_page_rd_poll", 0x00000114, 31, 16,   CSR_RW, 0x00000000 },
  { "QSPI69", 0x00000114, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi70
  { "run_type_read", 0x00000118,  0,  0,   CSR_RW, 0x00000000 },
  { "QSPI70", 0x00000118, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi71
  { "timeout_time_read", 0x0000011C, 15,  0,   CSR_RW, 0x00000000 },
  { "wait_busy_time_read", 0x0000011C, 31, 16,   CSR_RW, 0x00000000 },
  { "QSPI71", 0x0000011C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi72
  { "run_type_we_pg", 0x00000120,  0,  0,   CSR_RW, 0x00000000 },
  { "QSPI72", 0x00000120, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi73
  { "timeout_time_we_pg", 0x00000124, 15,  0,   CSR_RW, 0x00000000 },
  { "wait_busy_time_we_pg", 0x00000124, 31, 16,   CSR_RW, 0x00000000 },
  { "QSPI73", 0x00000124, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi74
  { "valid_status_we_pg_poll", 0x00000128, 31,  0,   CSR_RW, 0x00000000 },
  { "QSPI74", 0x00000128, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi75
  { "timeout_time_we_pg_poll", 0x0000012C, 15,  0,   CSR_RW, 0x00000000 },
  { "wait_busy_time_we_pg_poll", 0x0000012C, 31, 16,   CSR_RW, 0x00000000 },
  { "QSPI75", 0x0000012C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi76
  { "run_type_prog", 0x00000130,  0,  0,   CSR_RW, 0x00000000 },
  { "QSPI76", 0x00000130, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi77
  { "timeout_time_prog", 0x00000134, 15,  0,   CSR_RW, 0x00000000 },
  { "wait_busy_time_prog", 0x00000134, 31, 16,   CSR_RW, 0x00000000 },
  { "QSPI77", 0x00000134, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi80
  { "run_type_we_exe", 0x00000140,  0,  0,   CSR_RW, 0x00000000 },
  { "QSPI80", 0x00000140, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi81
  { "timeout_time_we_exe", 0x00000144, 15,  0,   CSR_RW, 0x00000000 },
  { "wait_busy_time_we_exe", 0x00000144, 31, 16,   CSR_RW, 0x00000000 },
  { "QSPI81", 0x00000144, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi82
  { "valid_status_we_exe_poll", 0x00000148, 31,  0,   CSR_RW, 0x00000000 },
  { "QSPI82", 0x00000148, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi83
  { "timeout_time_we_exe_poll", 0x0000014C, 15,  0,   CSR_RW, 0x00000000 },
  { "wait_busy_time_we_exe_poll", 0x0000014C, 31, 16,   CSR_RW, 0x00000000 },
  { "QSPI83", 0x0000014C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi84
  { "run_type_exec", 0x00000150,  0,  0,   CSR_RW, 0x00000000 },
  { "QSPI84", 0x00000150, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi85
  { "timeout_time_exec", 0x00000154, 15,  0,   CSR_RW, 0x00000000 },
  { "wait_busy_time_exec", 0x00000154, 31, 16,   CSR_RW, 0x00000000 },
  { "QSPI85", 0x00000154, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi86
  { "valid_status_exec_poll", 0x00000158, 31,  0,   CSR_RW, 0x00000000 },
  { "QSPI86", 0x00000158, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi87
  { "timeout_time_exec_poll", 0x0000015C, 15,  0,   CSR_RW, 0x00000000 },
  { "wait_busy_time_exec_poll", 0x0000015C, 31, 16,   CSR_RW, 0x00000000 },
  { "QSPI87", 0x0000015C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi90
  { "debug_mon_sel", 0x00000168,  1,  0,   CSR_RW, 0x00000000 },
  { "QSPI90", 0x00000168, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi91
  { "reserved", 0x0000016C, 31,  0,   CSR_RW, 0x00000000 },
  { "QSPI91", 0x0000016C, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_QSPI_H_
