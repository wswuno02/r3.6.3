#ifndef CSR_TABLE_REFW_H_
#define CSR_TABLE_REFW_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_refw[] =
{
  // WORD rw_00
  { "frame_start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "RW_00", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rw_01
  { "access_illegal_hang", 0x00000004,  0,  0,   CSR_RW, 0x00000001 },
  { "access_illegal_mask", 0x00000004,  8,  8,   CSR_RW, 0x00000001 },
  { "RW_01", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rw_04
  { "col_addr_type", 0x00000010,  9,  8,   CSR_RW, 0x00000000 },
  { "RW_04", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rw_07
  { "target_burst_len", 0x0000001C,  4,  0,   CSR_RW, 0x00000010 },
  { "access_end_sel", 0x0000001C,  8,  8,   CSR_RW, 0x00000001 },
  { "debug_mon_sel", 0x0000001C, 24, 24,   CSR_RW, 0x00000000 },
  { "RW_07", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rw_08
  { "target_fifo_level", 0x00000020,  8,  0,   CSR_RW, 0x00000020 },
  { "fifo_full_level", 0x00000020, 24, 16,   CSR_RW, 0x00000100 },
  { "RW_08", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rw_09
  { "mb_cnt_ver", 0x00000024, 11,  0,   CSR_RW, 0x00000000 },
  { "mb_cnt_hor", 0x00000024, 27, 16,   CSR_RW, 0x00000000 },
  { "RW_09", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rw_10
  { "y_only", 0x00000028,  0,  0,   CSR_RW, 0x00000000 },
  { "RW_10", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rw_11
  { "total_fifo_cnt", 0x0000002C, 29,  0,   CSR_RW, 0x00000000 },
  { "RW_11", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rw_12
  { "start_addr", 0x00000030, 27,  0,   CSR_RW, 0x00000000 },
  { "RW_12", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rw_13
  { "end_addr", 0x00000034, 27,  0,   CSR_RW, 0x0FFFFFFF },
  { "RW_13", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rw_15
  { "reserved", 0x0000003C, 31,  0,   CSR_RW, 0x00000000 },
  { "RW_15", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rw_16
  { "bank_interleave_type", 0x00000040,  1,  0,   CSR_RW, 0x00000002 },
  { "bank_group_type", 0x00000040,  9,  8,   CSR_RW, 0x00000001 },
  { "RW_16", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rw_17
  { "ini_addr_bank_offset", 0x00000044,  2,  0,   CSR_RW, 0x00000000 },
  { "RW_17", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rw_18
  { "ini_addr_linear_y", 0x00000048, 27,  0,   CSR_RW, 0x00000000 },
  { "RW_18", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rw_19
  { "ini_addr_linear_c", 0x0000004C, 27,  0,   CSR_RW, 0x00000000 },
  { "RW_19", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_REFW_H_
