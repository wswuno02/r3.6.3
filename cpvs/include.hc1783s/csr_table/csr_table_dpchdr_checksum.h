#ifndef CSR_TABLE_DPCHDR_CHECKSUM_H_
#define CSR_TABLE_DPCHDR_CHECKSUM_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_dpchdr_checksum[] =
{
  // WORD clr
  { "clear", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "CLR", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD dpc
  { "checksum_dpc", 0x00000004, 31,  0,   CSR_RO, 0x00000000 },
  { "DPC", 0x00000004, 31, 0,   CSR_RO, 0x00000000 },
  // WORD hdr
  { "checksum_hdr", 0x00000008, 31,  0,   CSR_RO, 0x00000000 },
  { "HDR", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_DPCHDR_CHECKSUM_H_
