#ifndef CSR_TABLE_APREAMPCFG_H_
#define CSR_TABLE_APREAMPCFG_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_apreampcfg[] =
{
  // WORD audio_preamp_enable
  { "rg_audio_preamp_en", 0x00000000,  0,  0,   CSR_RW, 0x00000000 },
  { "rg_audio_preamp_clken", 0x00000000, 16, 16,   CSR_RW, 0x00000000 },
  { "AUDIO_PREAMP_ENABLE", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_preamp_ref
  { "rg_audio_preamp_ref_en", 0x00000004,  0,  0,   CSR_RW, 0x00000000 },
  { "rg_audio_preamp_plus_en", 0x00000004,  8,  8,   CSR_RW, 0x00000000 },
  { "rg_audio_preamp_minus_en", 0x00000004, 16, 16,   CSR_RW, 0x00000000 },
  { "AUDIO_PREAMP_REF", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_preamp_sw
  { "rg_audio_preamp_sw1_en", 0x00000008,  0,  0,   CSR_RW, 0x00000000 },
  { "rg_audio_preamp_sw2_en", 0x00000008,  8,  8,   CSR_RW, 0x00000000 },
  { "rg_audio_preamp_sw3_en", 0x00000008, 16, 16,   CSR_RW, 0x00000000 },
  { "AUDIO_PREAMP_SW", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_preamp_mon
  { "preamp_ctrl_mon", 0x0000000C,  7,  0,   CSR_RO, 0x00000000 },
  { "AUDIO_PREAMP_MON", 0x0000000C, 31, 0,   CSR_RO, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_APREAMPCFG_H_
