#ifndef CSR_TABLE_AUDIO_ANR_H_
#define CSR_TABLE_AUDIO_ANR_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_audio_anr[] =
{
  // WORD audio_anr_0
  { "weight_0_0", 0x00000000, 18,  0,   CSR_RW, 0x0001F704 },
  { "AUDIO_ANR_0", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_anr_1
  { "weight_0_1", 0x00000004, 18,  0,   CSR_RW, 0x00070467 },
  { "AUDIO_ANR_1", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_anr_2
  { "weight_0_2", 0x00000008, 18,  0,   CSR_RW, 0x0000FB4D },
  { "AUDIO_ANR_2", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_anr_3
  { "weight_0_3", 0x0000000C, 18,  0,   CSR_RW, 0x00070E8F },
  { "AUDIO_ANR_3", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_anr_4
  { "weight_0_4", 0x00000010, 18,  0,   CSR_RW, 0x0000FB4D },
  { "AUDIO_ANR_4", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_anr_5
  { "weight_0_5", 0x00000014, 18,  0,   CSR_RW, 0x0001F514 },
  { "AUDIO_ANR_5", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_anr_6
  { "weight_0_6", 0x00000018, 18,  0,   CSR_RW, 0x000704FF },
  { "AUDIO_ANR_6", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_anr_7
  { "weight_0_7", 0x0000001C, 18,  0,   CSR_RW, 0x0000FFFF },
  { "AUDIO_ANR_7", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_anr_8
  { "weight_0_8", 0x00000020, 18,  0,   CSR_RW, 0x00060543 },
  { "AUDIO_ANR_8", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_anr_9
  { "weight_0_9", 0x00000024, 18,  0,   CSR_RW, 0x0000FFFF },
  { "AUDIO_ANR_9", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_anr_10
  { "weight_1_0", 0x00000028, 18,  0,   CSR_RW, 0x0001F704 },
  { "AUDIO_ANR_10", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_anr_11
  { "weight_1_1", 0x0000002C, 18,  0,   CSR_RW, 0x00070467 },
  { "AUDIO_ANR_11", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_anr_12
  { "weight_1_2", 0x00000030, 18,  0,   CSR_RW, 0x0000FB4D },
  { "AUDIO_ANR_12", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_anr_13
  { "weight_1_3", 0x00000034, 18,  0,   CSR_RW, 0x00070E8F },
  { "AUDIO_ANR_13", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_anr_14
  { "weight_1_4", 0x00000038, 18,  0,   CSR_RW, 0x0000FB4D },
  { "AUDIO_ANR_14", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_anr_15
  { "weight_1_5", 0x0000003C, 18,  0,   CSR_RW, 0x0001F514 },
  { "AUDIO_ANR_15", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_anr_16
  { "weight_1_6", 0x00000040, 18,  0,   CSR_RW, 0x000704FF },
  { "AUDIO_ANR_16", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_anr_17
  { "weight_1_7", 0x00000044, 18,  0,   CSR_RW, 0x0000FFFF },
  { "AUDIO_ANR_17", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_anr_18
  { "weight_1_8", 0x00000048, 18,  0,   CSR_RW, 0x00060543 },
  { "AUDIO_ANR_18", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_anr_19
  { "weight_1_9", 0x0000004C, 18,  0,   CSR_RW, 0x0000FFFF },
  { "AUDIO_ANR_19", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD buf_update
  { "double_buf_update", 0x00000050,  0,  0,  CSR_W1P, 0x00000000 },
  { "BUF_UPDATE", 0x00000050, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD word_anr0_mode
  { "anr0_mode", 0x00000054,  0,  0,   CSR_RW, 0x00000000 },
  { "WORD_ANR0_MODE", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_anr1_mode
  { "anr1_mode", 0x00000058,  0,  0,   CSR_RW, 0x00000000 },
  { "WORD_ANR1_MODE", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_AUDIO_ANR_H_
