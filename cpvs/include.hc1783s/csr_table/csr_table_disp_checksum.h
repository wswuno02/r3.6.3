#ifndef CSR_TABLE_DISP_CHECKSUM_H_
#define CSR_TABLE_DISP_CHECKSUM_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_disp_checksum[] =
{
  // WORD word_clear
  { "clear", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "WORD_CLEAR", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD dispr
  { "checksum_dispr", 0x00000004, 31,  0,   CSR_RO, 0x00000000 },
  { "DISPR", 0x00000004, 31, 0,   CSR_RO, 0x00000000 },
  // WORD pat_gen
  { "checksum_pg", 0x00000008, 31,  0,   CSR_RO, 0x00000000 },
  { "PAT_GEN", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // WORD disp_cs
  { "checksum_disp_cs", 0x0000000C, 31,  0,   CSR_RO, 0x00000000 },
  { "DISP_CS", 0x0000000C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD prd
  { "checksum_prd", 0x00000010, 31,  0,   CSR_RO, 0x00000000 },
  { "PRD", 0x00000010, 31, 0,   CSR_RO, 0x00000000 },
  // WORD yuv422
  { "checksum_yuv420to422", 0x00000014, 31,  0,   CSR_RO, 0x00000000 },
  { "YUV422", 0x00000014, 31, 0,   CSR_RO, 0x00000000 },
  // WORD yuv444_0
  { "checksum_yuv422to444_0", 0x00000018, 31,  0,   CSR_RO, 0x00000000 },
  { "YUV444_0", 0x00000018, 31, 0,   CSR_RO, 0x00000000 },
  // WORD yuv444_1
  { "checksum_yuv422to444_1", 0x0000001C, 31,  0,   CSR_RO, 0x00000000 },
  { "YUV444_1", 0x0000001C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD dosd0
  { "checksum_dosd_0", 0x00000020, 31,  0,   CSR_RO, 0x00000000 },
  { "DOSD0", 0x00000020, 31, 0,   CSR_RO, 0x00000000 },
  // WORD dosd1
  { "checksum_dosd_1", 0x00000024, 31,  0,   CSR_RO, 0x00000000 },
  { "DOSD1", 0x00000024, 31, 0,   CSR_RO, 0x00000000 },
  // WORD dosdr0
  { "checksum_dosdr_0", 0x00000028, 31,  0,   CSR_RO, 0x00000000 },
  { "DOSDR0", 0x00000028, 31, 0,   CSR_RO, 0x00000000 },
  // WORD dosdr1
  { "checksum_dosdr_1", 0x0000002C, 31,  0,   CSR_RO, 0x00000000 },
  { "DOSDR1", 0x0000002C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD dcs
  { "checksum_dcs", 0x00000030, 31,  0,   CSR_RO, 0x00000000 },
  { "DCS", 0x00000030, 31, 0,   CSR_RO, 0x00000000 },
  // WORD dcst0
  { "checksum_dcst_0", 0x00000034, 31,  0,   CSR_RO, 0x00000000 },
  { "DCST0", 0x00000034, 31, 0,   CSR_RO, 0x00000000 },
  // WORD dcst1
  { "checksum_dcst_1", 0x00000038, 31,  0,   CSR_RO, 0x00000000 },
  { "DCST1", 0x00000038, 31, 0,   CSR_RO, 0x00000000 },
  // WORD dgma0
  { "checksum_dgma_0", 0x0000003C, 31,  0,   CSR_RO, 0x00000000 },
  { "DGMA0", 0x0000003C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD dgma1
  { "checksum_dgma_1", 0x00000040, 31,  0,   CSR_RO, 0x00000000 },
  { "DGMA1", 0x00000040, 31, 0,   CSR_RO, 0x00000000 },
  // WORD dcfa
  { "checksum_dcfa", 0x00000044, 31,  0,   CSR_RO, 0x00000000 },
  { "DCFA", 0x00000044, 31, 0,   CSR_RO, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_DISP_CHECKSUM_H_
