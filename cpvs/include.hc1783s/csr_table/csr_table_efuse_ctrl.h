#ifndef CSR_TABLE_EFUSE_CTRL_H_
#define CSR_TABLE_EFUSE_CTRL_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_efuse_ctrl[] =
{
  // WORD word_mode
  { "mode", 0x00000000,  2,  0,   CSR_RW, 0x00000000 },
  { "WORD_MODE", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD time_set0
  { "tsu_pd_ps", 0x00000004,  6,  0,   CSR_RW, 0x00000000 },
  { "tsu_ps_cs", 0x00000004, 14,  8,   CSR_RW, 0x00000014 },
  { "th_cs", 0x00000004, 17, 16,   CSR_RW, 0x00000001 },
  { "th_ps_cs", 0x00000004, 26, 24,   CSR_RW, 0x00000002 },
  { "TIME_SET0", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD time_set1
  { "tsu_a", 0x00000008,  1,  0,   CSR_RW, 0x00000001 },
  { "th_a", 0x00000008,  9,  8,   CSR_RW, 0x00000001 },
  { "t_strobe", 0x00000008, 25, 16,   CSR_RW, 0x00000003 },
  { "TIME_SET1", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ctrl
  { "start", 0x0000000C,  0,  0,  CSR_W1P, 0x00000000 },
  { "stop", 0x0000000C,  8,  8,  CSR_W1P, 0x00000000 },
  { "CTRL", 0x0000000C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD addr
  { "rw_addr", 0x00000010,  8,  0,   CSR_RW, 0x00000000 },
  { "ADDR", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wdata
  { "w_data", 0x00000014,  0,  0,   CSR_RW, 0x00000000 },
  { "WDATA", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rdata
  { "r_data", 0x00000018, 31,  0,   CSR_RO, 0x00000000 },
  { "RDATA", 0x00000018, 31, 0,   CSR_RO, 0x00000000 },
  // WORD hw_rdata
  { "hardware_r_data", 0x0000001C, 31,  0,   CSR_RO, 0x00000000 },
  { "HW_RDATA", 0x0000001C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD status
  { "rw_ready", 0x00000020,  0,  0,   CSR_RO, 0x00000000 },
  { "rd_done", 0x00000020,  8,  8,   CSR_RO, 0x00000000 },
  { "esc_done", 0x00000020, 16, 16,   CSR_RO, 0x00000000 },
  { "power_dn", 0x00000020, 24, 24,   CSR_RO, 0x00000000 },
  { "STATUS", 0x00000020, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_time_cnt
  { "time_cnt", 0x00000024, 15,  0,   CSR_RO, 0x00000000 },
  { "WORD_TIME_CNT", 0x00000024, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_lock
  { "lock", 0x00000028,  0,  0,   CSR_RW, 0x00000000 },
  { "WORD_LOCK", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_EFUSE_CTRL_H_
