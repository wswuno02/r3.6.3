#ifndef CSR_TABLE_DISPIF_CTR_H_
#define CSR_TABLE_DISPIF_CTR_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_dispif_ctr[] =
{
  // WORD mux0
  { "src_broadcast_enable_pd", 0x00000000,  0,  0,   CSR_RW, 0x00000000 },
  { "src_broadcast_enable_mipi", 0x00000000,  8,  8,   CSR_RW, 0x00000001 },
  { "src_broadcast_ack_not_sel", 0x00000000, 16, 16,   CSR_RW, 0x00000000 },
  { "MUX0", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dbg
  { "debug_sel", 0x00000040,  1,  0,   CSR_RW, 0x00000000 },
  { "DBG", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_DISPIF_CTR_H_
