#ifndef CSR_TABLE_FCS_H_
#define CSR_TABLE_FCS_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_fcs[] =
{
  // WORD word_frame_start
  { "frame_start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "WORD_FRAME_START", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD irq_clear
  { "irq_clear_frame_end", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "IRQ_CLEAR", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD irq_status
  { "status_frame_end", 0x00000008,  0,  0,   CSR_RO, 0x00000000 },
  { "IRQ_STATUS", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // WORD irq_mask
  { "irq_mask_frame_end", 0x0000000C,  0,  0,   CSR_RW, 0x00000001 },
  { "IRQ_MASK", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_mode
  { "mode", 0x00000010,  0,  0,   CSR_RW, 0x00000000 },
  { "WORD_MODE", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD resolution
  { "width", 0x00000014, 15,  0,   CSR_RW, 0x00000100 },
  { "height", 0x00000014, 31, 16,   CSR_RW, 0x00000438 },
  { "RESOLUTION", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD trans
  { "trans_diff_level_max", 0x00000018,  5,  0,   CSR_RW, 0x00000008 },
  { "trans_gain", 0x00000018, 12,  8,   CSR_RW, 0x00000004 },
  { "TRANS", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD grid_cnt
  { "grid_cnt_th_high", 0x0000001C,  2,  0,   CSR_RW, 0x00000005 },
  { "grid_cnt_th_low", 0x0000001C, 10,  8,   CSR_RW, 0x00000001 },
  { "GRID_CNT", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD grid_cnt_th
  { "grid_cnt_raw_th", 0x00000020,  2,  0,   CSR_RW, 0x00000004 },
  { "GRID_CNT_TH", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD grid_trans
  { "grid_trans_level_th", 0x00000024,  8,  0,   CSR_RW, 0x00000018 },
  { "grid_trans_round_bit_high", 0x00000024, 17, 16,   CSR_RW, 0x00000002 },
  { "grid_trans_round_bit_low", 0x00000024, 25, 24,   CSR_RW, 0x00000001 },
  { "GRID_TRANS", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD color
  { "color_diff_self_th", 0x00000028,  5,  0,   CSR_RW, 0x00000006 },
  { "color_diff_neighbor_th", 0x00000028, 13,  8,   CSR_RW, 0x00000000 },
  { "color_coring_th", 0x00000028, 21, 16,   CSR_RW, 0x00000002 },
  { "COLOR", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD correction
  { "correction_gain", 0x0000002C,  4,  0,   CSR_RW, 0x00000008 },
  { "CORRECTION", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD target_ratio
  { "target_bg_ratio", 0x00000030,  8,  0,   CSR_RW, 0x00000080 },
  { "target_rg_ratio", 0x00000030, 24, 16,   CSR_RW, 0x00000080 },
  { "TARGET_RATIO", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD atpg_test
  { "atpg_test_enable_0", 0x00000034,  1,  0,   CSR_RW, 0x00000000 },
  { "atpg_test_enable_1", 0x00000034,  9,  8,   CSR_RW, 0x00000000 },
  { "ATPG_TEST", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD debug_mon
  { "debug_mon_sel", 0x00000038,  1,  0,   CSR_RW, 0x00000000 },
  { "DEBUG_MON", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_reserved
  { "reserved", 0x0000003C, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_FCS_H_
