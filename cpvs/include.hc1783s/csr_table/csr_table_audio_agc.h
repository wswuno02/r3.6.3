#ifndef CSR_TABLE_AUDIO_AGC_H_
#define CSR_TABLE_AUDIO_AGC_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_audio_agc[] =
{
  // WORD audio_hpf_b0
  { "hpf_b0", 0x00000000, 15,  0,   CSR_RW, 0x00001FD2 },
  { "AUDIO_HPF_B0", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_hpf_b1
  { "hpf_b1", 0x00000004, 15,  0,   CSR_RW, 0x0000E02E },
  { "AUDIO_HPF_B1", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_hpf_a
  { "hpf_a", 0x00000008, 15,  0,   CSR_RW, 0x00003F48 },
  { "AUDIO_HPF_A", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_pd
  { "pd_th", 0x0000000C,  6,  0,   CSR_RW, 0x00000002 },
  { "pd_filtered_alpha", 0x0000000C, 13,  8,   CSR_RW, 0x00000002 },
  { "pd_env_alpha", 0x0000000C, 24, 16,   CSR_RW, 0x00000064 },
  { "AUDIO_PD", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_agc_mode
  { "agc_gain_mode", 0x00000010,  1,  0,   CSR_RW, 0x00000000 },
  { "pre_amp_mod", 0x00000010, 10,  8,   CSR_RW, 0x00000002 },
  { "AUDIO_AGC_MODE", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_agc_bond0
  { "agc_upper_bond", 0x00000014, 10,  0,   CSR_RW, 0x0000015E },
  { "AUDIO_AGC_BOND0", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_agc_bond1
  { "agc_lower_bond", 0x00000018, 10,  0,   CSR_RW, 0x0000000F },
  { "AUDIO_AGC_BOND1", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_agc_bond2
  { "agc_noise_gate", 0x0000001C, 10,  0,   CSR_RW, 0x00000002 },
  { "AUDIO_AGC_BOND2", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD agc_gain_h
  { "agc_gain_h_v", 0x00000020, 10,  0,   CSR_RW, 0x000003F7 },
  { "AGC_GAIN_H", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD agc_gain_l
  { "agc_gain_l_v", 0x00000024, 10,  0,   CSR_RW, 0x000003F7 },
  { "AGC_GAIN_L", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD agc_gain_ll
  { "agc_gain_ll_v", 0x00000028, 10,  0,   CSR_RW, 0x000003F7 },
  { "AGC_GAIN_LL", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ana_agc_gain_h
  { "agc_analog_gain_delta_h", 0x0000002C, 10,  0,   CSR_RW, 0x000007FB },
  { "ANA_AGC_GAIN_H", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ana_agc_gain_l
  { "agc_analog_gain_delta_l", 0x00000030, 10,  0,   CSR_RW, 0x000007FB },
  { "ANA_AGC_GAIN_L", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ana_agc_gain_ll
  { "agc_analog_gain_delta_ll", 0x00000034, 10,  0,   CSR_RW, 0x0000000E },
  { "ANA_AGC_GAIN_LL", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_double_buf_update
  { "double_buf_update", 0x00000038,  0,  0,  CSR_W1P, 0x00000000 },
  { "WORD_DOUBLE_BUF_UPDATE", 0x00000038, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD gain_curve_0
  { "gain_0", 0x0000003C,  7,  0,   CSR_RW, 0x00000000 },
  { "gain_1", 0x0000003C, 15,  8,   CSR_RW, 0x00000000 },
  { "gain_2", 0x0000003C, 23, 16,   CSR_RW, 0x00000000 },
  { "gain_3", 0x0000003C, 31, 24,   CSR_RW, 0x00000000 },
  { "GAIN_CURVE_0", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_curve_1
  { "gain_4", 0x00000040,  7,  0,   CSR_RW, 0x00000000 },
  { "gain_5", 0x00000040, 15,  8,   CSR_RW, 0x00000000 },
  { "gain_6", 0x00000040, 23, 16,   CSR_RW, 0x00000000 },
  { "gain_7", 0x00000040, 31, 24,   CSR_RW, 0x00000000 },
  { "GAIN_CURVE_1", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_curve_2
  { "gain_8", 0x00000044,  7,  0,   CSR_RW, 0x00000000 },
  { "gain_9", 0x00000044, 15,  8,   CSR_RW, 0x00000000 },
  { "gain_10", 0x00000044, 23, 16,   CSR_RW, 0x00000000 },
  { "gain_11", 0x00000044, 31, 24,   CSR_RW, 0x00000000 },
  { "GAIN_CURVE_2", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_curve_3
  { "gain_12", 0x00000048,  7,  0,   CSR_RW, 0x00000000 },
  { "gain_13", 0x00000048, 15,  8,   CSR_RW, 0x00000000 },
  { "gain_14", 0x00000048, 23, 16,   CSR_RW, 0x00000000 },
  { "gain_15", 0x00000048, 31, 24,   CSR_RW, 0x00000000 },
  { "GAIN_CURVE_3", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_curve_4
  { "gain_16", 0x0000004C,  7,  0,   CSR_RW, 0x00000000 },
  { "gain_17", 0x0000004C, 15,  8,   CSR_RW, 0x00000000 },
  { "gain_18", 0x0000004C, 23, 16,   CSR_RW, 0x00000000 },
  { "gain_19", 0x0000004C, 31, 24,   CSR_RW, 0x00000000 },
  { "GAIN_CURVE_4", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_curve_5
  { "gain_20", 0x00000050,  7,  0,   CSR_RW, 0x00000000 },
  { "gain_21", 0x00000050, 15,  8,   CSR_RW, 0x00000000 },
  { "gain_22", 0x00000050, 23, 16,   CSR_RW, 0x00000000 },
  { "gain_23", 0x00000050, 31, 24,   CSR_RW, 0x00000000 },
  { "GAIN_CURVE_5", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_curve_6
  { "gain_24", 0x00000054,  7,  0,   CSR_RW, 0x00000000 },
  { "gain_25", 0x00000054, 15,  8,   CSR_RW, 0x00000000 },
  { "gain_26", 0x00000054, 23, 16,   CSR_RW, 0x00000000 },
  { "gain_27", 0x00000054, 31, 24,   CSR_RW, 0x00000000 },
  { "GAIN_CURVE_6", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_curve_7
  { "gain_28", 0x00000058,  7,  0,   CSR_RW, 0x00000000 },
  { "gain_29", 0x00000058, 15,  8,   CSR_RW, 0x00000000 },
  { "gain_30", 0x00000058, 23, 16,   CSR_RW, 0x00000000 },
  { "gain_31", 0x00000058, 31, 24,   CSR_RW, 0x00000000 },
  { "GAIN_CURVE_7", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_curve_8
  { "gain_32", 0x0000005C,  7,  0,   CSR_RW, 0x00000000 },
  { "gain_33", 0x0000005C, 15,  8,   CSR_RW, 0x00000000 },
  { "gain_34", 0x0000005C, 23, 16,   CSR_RW, 0x00000000 },
  { "gain_35", 0x0000005C, 31, 24,   CSR_RW, 0x00000000 },
  { "GAIN_CURVE_8", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_curve_9
  { "gain_36", 0x00000060,  7,  0,   CSR_RW, 0x00000000 },
  { "gain_37", 0x00000060, 15,  8,   CSR_RW, 0x00000000 },
  { "gain_38", 0x00000060, 23, 16,   CSR_RW, 0x00000000 },
  { "gain_39", 0x00000060, 31, 24,   CSR_RW, 0x00000000 },
  { "GAIN_CURVE_9", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_curve_10
  { "gain_40", 0x00000064,  7,  0,   CSR_RW, 0x00000000 },
  { "gain_41", 0x00000064, 15,  8,   CSR_RW, 0x00000000 },
  { "gain_42", 0x00000064, 23, 16,   CSR_RW, 0x00000000 },
  { "gain_43", 0x00000064, 31, 24,   CSR_RW, 0x00000000 },
  { "GAIN_CURVE_10", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_curve_11
  { "gain_44", 0x00000068,  7,  0,   CSR_RW, 0x00000000 },
  { "gain_45", 0x00000068, 15,  8,   CSR_RW, 0x00000000 },
  { "gain_46", 0x00000068, 23, 16,   CSR_RW, 0x00000000 },
  { "gain_47", 0x00000068, 31, 24,   CSR_RW, 0x00000000 },
  { "GAIN_CURVE_11", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_curve_12
  { "gain_48", 0x0000006C,  7,  0,   CSR_RW, 0x00000000 },
  { "gain_49", 0x0000006C, 15,  8,   CSR_RW, 0x00000000 },
  { "gain_50", 0x0000006C, 23, 16,   CSR_RW, 0x00000000 },
  { "gain_51", 0x0000006C, 31, 24,   CSR_RW, 0x00000000 },
  { "GAIN_CURVE_12", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_curve_13
  { "gain_52", 0x00000070,  7,  0,   CSR_RW, 0x00000000 },
  { "gain_53", 0x00000070, 15,  8,   CSR_RW, 0x00000000 },
  { "gain_54", 0x00000070, 23, 16,   CSR_RW, 0x00000000 },
  { "gain_55", 0x00000070, 31, 24,   CSR_RW, 0x00000000 },
  { "GAIN_CURVE_13", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_curve_14
  { "gain_56", 0x00000074,  7,  0,   CSR_RW, 0x00000000 },
  { "gain_57", 0x00000074, 15,  8,   CSR_RW, 0x00000000 },
  { "gain_58", 0x00000074, 23, 16,   CSR_RW, 0x00000000 },
  { "gain_59", 0x00000074, 31, 24,   CSR_RW, 0x00000000 },
  { "GAIN_CURVE_14", 0x00000074, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_curve_15
  { "gain_60", 0x00000078,  7,  0,   CSR_RW, 0x00000000 },
  { "gain_61", 0x00000078, 15,  8,   CSR_RW, 0x00000000 },
  { "gain_62", 0x00000078, 23, 16,   CSR_RW, 0x00000000 },
  { "gain_63", 0x00000078, 31, 24,   CSR_RW, 0x00000000 },
  { "GAIN_CURVE_15", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_AUDIO_AGC_H_
