#ifndef CSR_TABLE_ADCCTR_H_
#define CSR_TABLE_ADCCTR_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_adcctr[] =
{
  // WORD adc00
  { "adc_clk_start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "adc_clk_stop", 0x00000000,  8,  8,  CSR_W1P, 0x00000000 },
  { "adc_start", 0x00000000, 16, 16,  CSR_W1P, 0x00000000 },
  { "adc_stop", 0x00000000, 24, 24,  CSR_W1P, 0x00000000 },
  { "ADC00", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD adc01
  { "env_query_0", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "env_query_1", 0x00000004,  8,  8,  CSR_W1P, 0x00000000 },
  { "env_query_2", 0x00000004, 16, 16,  CSR_W1P, 0x00000000 },
  { "env_query_3", 0x00000004, 24, 24,  CSR_W1P, 0x00000000 },
  { "ADC01", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD adc02
  { "env_query_4", 0x00000008,  0,  0,  CSR_W1P, 0x00000000 },
  { "env_query_5", 0x00000008,  8,  8,  CSR_W1P, 0x00000000 },
  { "env_init", 0x00000008, 16, 16,  CSR_W1P, 0x00000000 },
  { "ADC02", 0x00000008, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD adc03
  { "init_phy_reset_en", 0x0000000C, 15,  0,   CSR_RW, 0x00000001 },
  { "init_phy_soc_en", 0x0000000C, 23, 16,   CSR_RW, 0x00000001 },
  { "phy_serial_sel", 0x0000000C, 24, 24,   CSR_RW, 0x00000000 },
  { "ADC03", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc04
  { "clk_0_cycle", 0x00000010,  7,  0,   CSR_RW, 0x0000000F },
  { "clk_1_cycle", 0x00000010, 23, 16,   CSR_RW, 0x0000000F },
  { "ADC04", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc05
  { "length", 0x00000014,  7,  0,   CSR_RW, 0x00000019 },
  { "latency", 0x00000014, 23, 16,   CSR_RW, 0x0000000E },
  { "ADC05", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc06
  { "ch_0_type", 0x00000018,  0,  0,   CSR_RW, 0x00000000 },
  { "ch_1_type", 0x00000018,  8,  8,   CSR_RW, 0x00000000 },
  { "ch_2_type", 0x00000018, 16, 16,   CSR_RW, 0x00000000 },
  { "ch_3_type", 0x00000018, 24, 24,   CSR_RW, 0x00000000 },
  { "ADC06", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc07
  { "ch_4_type", 0x0000001C,  0,  0,   CSR_RW, 0x00000000 },
  { "ch_5_type", 0x0000001C,  8,  8,   CSR_RW, 0x00000000 },
  { "ADC07", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc09
  { "ch_num", 0x00000024,  4,  0,   CSR_RW, 0x00000004 },
  { "adc_format", 0x00000024,  8,  8,   CSR_RW, 0x00000001 },
  { "audio_lsb_repeat", 0x00000024, 16, 16,   CSR_RW, 0x00000000 },
  { "debug_mon_sel", 0x00000024, 25, 24,   CSR_RW, 0x00000000 },
  { "ADC09", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc10
  { "adc_selres_reg", 0x00000028,  1,  0,   CSR_RW, 0x00000003 },
  { "ADC10", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc11
  { "mute", 0x0000002C,  0,  0,   CSR_RW, 0x00000000 },
  { "ADC11", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc12
  { "env_data_0", 0x00000030, 11,  0,   CSR_RO, 0x00000000 },
  { "ADC12", 0x00000030, 31, 0,   CSR_RO, 0x00000000 },
  // WORD adc13
  { "env_data_1", 0x00000034, 11,  0,   CSR_RO, 0x00000000 },
  { "ADC13", 0x00000034, 31, 0,   CSR_RO, 0x00000000 },
  // WORD adc14
  { "env_data_2", 0x00000038, 11,  0,   CSR_RO, 0x00000000 },
  { "ADC14", 0x00000038, 31, 0,   CSR_RO, 0x00000000 },
  // WORD adc15
  { "env_data_3", 0x0000003C, 11,  0,   CSR_RO, 0x00000000 },
  { "ADC15", 0x0000003C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD adc16
  { "env_data_4", 0x00000040, 11,  0,   CSR_RO, 0x00000000 },
  { "ADC16", 0x00000040, 31, 0,   CSR_RO, 0x00000000 },
  // WORD adc17
  { "env_data_5", 0x00000044, 11,  0,   CSR_RO, 0x00000000 },
  { "ADC17", 0x00000044, 31, 0,   CSR_RO, 0x00000000 },
  // WORD adc18
  { "adc_b_reg", 0x00000048, 11,  0,   CSR_RO, 0x00000000 },
  { "adc_eoc_reg", 0x00000048, 16, 16,   CSR_RO, 0x00000000 },
  { "ADC18", 0x00000048, 31, 0,   CSR_RO, 0x00000000 },
  // WORD adc19
  { "virtual_ch_cnt", 0x0000004C,  4,  0,   CSR_RW, 0x00000006 },
  { "ADC19", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc20
  { "virtual_ch_0_num", 0x00000050,  2,  0,   CSR_RW, 0x00000000 },
  { "virtual_ch_0_seldiff", 0x00000050,  8,  8,   CSR_RW, 0x00000001 },
  { "virtual_ch_0_selref", 0x00000050, 16, 16,   CSR_RW, 0x00000001 },
  { "virtual_ch_0_selbg", 0x00000050, 24, 24,   CSR_RW, 0x00000001 },
  { "ADC20", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc21
  { "virtual_ch_1_num", 0x00000054,  2,  0,   CSR_RW, 0x00000001 },
  { "virtual_ch_1_seldiff", 0x00000054,  8,  8,   CSR_RW, 0x00000000 },
  { "virtual_ch_1_selref", 0x00000054, 16, 16,   CSR_RW, 0x00000001 },
  { "virtual_ch_1_selbg", 0x00000054, 24, 24,   CSR_RW, 0x00000001 },
  { "ADC21", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc22
  { "virtual_ch_2_num", 0x00000058,  2,  0,   CSR_RW, 0x00000002 },
  { "virtual_ch_2_seldiff", 0x00000058,  8,  8,   CSR_RW, 0x00000000 },
  { "virtual_ch_2_selref", 0x00000058, 16, 16,   CSR_RW, 0x00000001 },
  { "virtual_ch_2_selbg", 0x00000058, 24, 24,   CSR_RW, 0x00000001 },
  { "ADC22", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc23
  { "virtual_ch_3_num", 0x0000005C,  2,  0,   CSR_RW, 0x00000003 },
  { "virtual_ch_3_seldiff", 0x0000005C,  8,  8,   CSR_RW, 0x00000000 },
  { "virtual_ch_3_selref", 0x0000005C, 16, 16,   CSR_RW, 0x00000001 },
  { "virtual_ch_3_selbg", 0x0000005C, 24, 24,   CSR_RW, 0x00000001 },
  { "ADC23", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc24
  { "virtual_ch_4_num", 0x00000060,  2,  0,   CSR_RW, 0x00000004 },
  { "virtual_ch_4_seldiff", 0x00000060,  8,  8,   CSR_RW, 0x00000000 },
  { "virtual_ch_4_selref", 0x00000060, 16, 16,   CSR_RW, 0x00000001 },
  { "virtual_ch_4_selbg", 0x00000060, 24, 24,   CSR_RW, 0x00000001 },
  { "ADC24", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc25
  { "virtual_ch_5_num", 0x00000064,  2,  0,   CSR_RW, 0x00000005 },
  { "virtual_ch_5_seldiff", 0x00000064,  8,  8,   CSR_RW, 0x00000000 },
  { "virtual_ch_5_selref", 0x00000064, 16, 16,   CSR_RW, 0x00000001 },
  { "virtual_ch_5_selbg", 0x00000064, 24, 24,   CSR_RW, 0x00000001 },
  { "ADC25", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc26
  { "virtual_ch_6_num", 0x00000068,  2,  0,   CSR_RW, 0x00000006 },
  { "virtual_ch_6_seldiff", 0x00000068,  8,  8,   CSR_RW, 0x00000000 },
  { "virtual_ch_6_selref", 0x00000068, 16, 16,   CSR_RW, 0x00000001 },
  { "virtual_ch_6_selbg", 0x00000068, 24, 24,   CSR_RW, 0x00000001 },
  { "ADC26", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc27
  { "virtual_ch_7_num", 0x0000006C,  2,  0,   CSR_RW, 0x00000007 },
  { "virtual_ch_7_seldiff", 0x0000006C,  8,  8,   CSR_RW, 0x00000000 },
  { "virtual_ch_7_selref", 0x0000006C, 16, 16,   CSR_RW, 0x00000001 },
  { "virtual_ch_7_selbg", 0x0000006C, 24, 24,   CSR_RW, 0x00000001 },
  { "ADC27", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc28
  { "virtual_ch_8_num", 0x00000070,  2,  0,   CSR_RW, 0x00000000 },
  { "virtual_ch_8_seldiff", 0x00000070,  8,  8,   CSR_RW, 0x00000001 },
  { "virtual_ch_8_selref", 0x00000070, 16, 16,   CSR_RW, 0x00000001 },
  { "virtual_ch_8_selbg", 0x00000070, 24, 24,   CSR_RW, 0x00000001 },
  { "ADC28", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc29
  { "virtual_ch_9_num", 0x00000074,  2,  0,   CSR_RW, 0x00000001 },
  { "virtual_ch_9_seldiff", 0x00000074,  8,  8,   CSR_RW, 0x00000000 },
  { "virtual_ch_9_selref", 0x00000074, 16, 16,   CSR_RW, 0x00000001 },
  { "virtual_ch_9_selbg", 0x00000074, 24, 24,   CSR_RW, 0x00000001 },
  { "ADC29", 0x00000074, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc30
  { "virtual_ch_a_num", 0x00000078,  2,  0,   CSR_RW, 0x00000002 },
  { "virtual_ch_a_seldiff", 0x00000078,  8,  8,   CSR_RW, 0x00000000 },
  { "virtual_ch_a_selref", 0x00000078, 16, 16,   CSR_RW, 0x00000001 },
  { "virtual_ch_a_selbg", 0x00000078, 24, 24,   CSR_RW, 0x00000001 },
  { "ADC30", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc31
  { "virtual_ch_b_num", 0x0000007C,  2,  0,   CSR_RW, 0x00000003 },
  { "virtual_ch_b_seldiff", 0x0000007C,  8,  8,   CSR_RW, 0x00000000 },
  { "virtual_ch_b_selref", 0x0000007C, 16, 16,   CSR_RW, 0x00000001 },
  { "virtual_ch_b_selbg", 0x0000007C, 24, 24,   CSR_RW, 0x00000001 },
  { "ADC31", 0x0000007C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc32
  { "virtual_ch_c_num", 0x00000080,  2,  0,   CSR_RW, 0x00000004 },
  { "virtual_ch_c_seldiff", 0x00000080,  8,  8,   CSR_RW, 0x00000000 },
  { "virtual_ch_c_selref", 0x00000080, 16, 16,   CSR_RW, 0x00000001 },
  { "virtual_ch_c_selbg", 0x00000080, 24, 24,   CSR_RW, 0x00000001 },
  { "ADC32", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc33
  { "virtual_ch_d_num", 0x00000084,  2,  0,   CSR_RW, 0x00000005 },
  { "virtual_ch_d_seldiff", 0x00000084,  8,  8,   CSR_RW, 0x00000000 },
  { "virtual_ch_d_selref", 0x00000084, 16, 16,   CSR_RW, 0x00000001 },
  { "virtual_ch_d_selbg", 0x00000084, 24, 24,   CSR_RW, 0x00000001 },
  { "ADC33", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc34
  { "virtual_ch_e_num", 0x00000088,  2,  0,   CSR_RW, 0x00000006 },
  { "virtual_ch_e_seldiff", 0x00000088,  8,  8,   CSR_RW, 0x00000000 },
  { "virtual_ch_e_selref", 0x00000088, 16, 16,   CSR_RW, 0x00000001 },
  { "virtual_ch_e_selbg", 0x00000088, 24, 24,   CSR_RW, 0x00000001 },
  { "ADC34", 0x00000088, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc35
  { "virtual_ch_f_num", 0x0000008C,  2,  0,   CSR_RW, 0x00000007 },
  { "virtual_ch_f_seldiff", 0x0000008C,  8,  8,   CSR_RW, 0x00000000 },
  { "virtual_ch_f_selref", 0x0000008C, 16, 16,   CSR_RW, 0x00000001 },
  { "virtual_ch_f_selbg", 0x0000008C, 24, 24,   CSR_RW, 0x00000001 },
  { "ADC35", 0x0000008C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adc36
  { "reserved", 0x00000090, 31,  0,   CSR_RW, 0x00000000 },
  { "ADC36", 0x00000090, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_ADCCTR_H_
