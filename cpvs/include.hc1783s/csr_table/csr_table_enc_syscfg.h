#ifndef CSR_TABLE_ENC_SYSCFG_H_
#define CSR_TABLE_ENC_SYSCFG_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_enc_syscfg[] =
{
  // WORD cfg_sp
  { "cken_sp", 0x00000000,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_sp", 0x00000000,  8,  8,   CSR_RW, 0x00000000 },
  { "CFG_SP", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cfg_w1p_sp
  { "sw_rst_sp", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_sp", 0x00000004,  8,  8,  CSR_W1P, 0x00000000 },
  { "CFG_W1P_SP", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD cfg_jpeg
  { "cken_jpeg", 0x00000008,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_jpeg", 0x00000008,  8,  8,   CSR_RW, 0x00000000 },
  { "CFG_JPEG", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cfg_w1p_jpeg
  { "sw_rst_jpeg", 0x0000000C,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_jpeg", 0x0000000C,  8,  8,  CSR_W1P, 0x00000000 },
  { "CFG_W1P_JPEG", 0x0000000C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD cfg_venc
  { "cken_venc", 0x00000010,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_venc", 0x00000010,  8,  8,   CSR_RW, 0x00000000 },
  { "CFG_VENC", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cfg_w1p_venc
  { "sw_rst_venc", 0x00000014,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_venc", 0x00000014,  8,  8,  CSR_W1P, 0x00000000 },
  { "CFG_W1P_VENC", 0x00000014, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD cfg_srcr
  { "cken_srcr", 0x00000018,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_srcr", 0x00000018,  8,  8,   CSR_RW, 0x00000000 },
  { "CFG_SRCR", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cfg_w1p_srcr
  { "sw_rst_srcr", 0x0000001C,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_srcr", 0x0000001C,  8,  8,  CSR_W1P, 0x00000000 },
  { "CFG_W1P_SRCR", 0x0000001C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD cfg_bsw
  { "cken_bsw", 0x00000020,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_bsw", 0x00000020,  8,  8,   CSR_RW, 0x00000000 },
  { "CFG_BSW", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cfg_w1p_bsw
  { "sw_rst_bsw", 0x00000024,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_bsw", 0x00000024,  8,  8,  CSR_W1P, 0x00000000 },
  { "CFG_W1P_BSW", 0x00000024, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD cfg_osdr
  { "cken_osdr", 0x00000028,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_osdr", 0x00000028,  8,  8,   CSR_RW, 0x00000000 },
  { "CFG_OSDR", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cfg_w1p_osdr
  { "sw_rst_osdr", 0x0000002C,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_osdr", 0x0000002C,  8,  8,  CSR_W1P, 0x00000000 },
  { "CFG_W1P_OSDR", 0x0000002C, 31, 0,  CSR_W1P, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_ENC_SYSCFG_H_
