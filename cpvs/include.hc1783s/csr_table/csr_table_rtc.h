#ifndef CSR_TABLE_RTC_H_
#define CSR_TABLE_RTC_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_rtc[] =
{
  // WORD rtc00
  { "start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "RTC00", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rtc01
  { "irq_clear_match_0", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "RTC01", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rtc02
  { "status_match_0", 0x00000008,  0,  0,   CSR_RO, 0x00000000 },
  { "RTC02", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // WORD rtc03
  { "irq_mask_match_0", 0x0000000C,  0,  0,   CSR_RW, 0x00000001 },
  { "RTC03", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rtc04
  { "irq_clear_match_1", 0x00000010,  0,  0,  CSR_W1P, 0x00000000 },
  { "RTC04", 0x00000010, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rtc05
  { "status_match_1", 0x00000014,  0,  0,   CSR_RO, 0x00000000 },
  { "RTC05", 0x00000014, 31, 0,   CSR_RO, 0x00000000 },
  // WORD rtc06
  { "irq_mask_match_1", 0x00000018,  0,  0,   CSR_RW, 0x00000001 },
  { "RTC06", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rtc07
  { "enable", 0x0000001C,  0,  0,   CSR_RW, 0x00000000 },
  { "alarm_en", 0x0000001C,  8,  8,   CSR_RW, 0x00000000 },
  { "out_en", 0x0000001C, 17, 16,   CSR_RW, 0x00000001 },
  { "RTC07", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rtc08
  { "busy", 0x00000020,  0,  0,   CSR_RO, 0x00000000 },
  { "RTC08", 0x00000020, 31, 0,   CSR_RO, 0x00000000 },
  // WORD rtc09
  { "rtc_current", 0x00000024, 31,  0,   CSR_RO, 0x00000000 },
  { "RTC09", 0x00000024, 31, 0,   CSR_RO, 0x00000000 },
  // WORD rtc10
  { "prescaler_th", 0x00000028, 24,  0,   CSR_RW, 0x00000000 },
  { "RTC10", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rtc11
  { "rtc_load", 0x0000002C, 31,  0,   CSR_RW, 0x00000000 },
  { "RTC11", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rtc12
  { "rtc_match_0", 0x00000030, 31,  0,   CSR_RW, 0x00000000 },
  { "RTC12", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rtc13
  { "rtc_match_1", 0x00000034, 31,  0,   CSR_RW, 0x00000000 },
  { "RTC13", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_RTC_H_
