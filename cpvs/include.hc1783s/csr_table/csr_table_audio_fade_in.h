#ifndef CSR_TABLE_AUDIO_FADE_IN_H_
#define CSR_TABLE_AUDIO_FADE_IN_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_audio_fade_in[] =
{
  // WORD audio_in_w1p
  { "audio_in_audio_start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "audio_in_fade_start", 0x00000000,  8,  8,  CSR_W1P, 0x00000000 },
  { "audio_in_fade_stop", 0x00000000, 16, 16,  CSR_W1P, 0x00000000 },
  { "AUDIO_IN_W1P", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD audio_in_fade
  { "audio_in_fade_time", 0x00000004,  9,  0,   CSR_RW, 0x00000003 },
  { "AUDIO_IN_FADE", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_in_irq
  { "audio_in_irq_real_stop", 0x00000008,  0,  0,   CSR_RO, 0x00000000 },
  { "AUDIO_IN_IRQ", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_AUDIO_FADE_IN_H_
