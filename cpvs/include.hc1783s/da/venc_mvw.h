#ifndef HW_VENC_MVW_H
#define HW_VENC_MVW_H

#include "da_utils.h"
#include "csr_bank_venc_mvw.h"

#ifndef __KERNEL__
#include <stdint.h>
void hw_venc_mvw_frame_start();
void hw_venc_mvw_irq_clear();
uint32_t hw_venc_mvw_status_frame_end();
uint32_t hw_venc_mvw_status_bw_insufficient();
uint32_t hw_venc_mvw_status_access_violation();
void hw_venc_mvw_irq_mask(uint32_t frame_end, uint32_t bw_insufficient, uint32_t access_violation);
void hw_venc_mvw_dram_info(uint32_t col_addr_type, uint32_t bank_interleave_type, uint32_t bank_group_type);
void hw_venc_mvw_access_illegal(uint32_t hang, uint32_t mask);
void hw_venc_mvw_set_target_burst_len(uint32_t burst_len);
void hw_venc_mvw_init();
void hw_venc_mvw_set_addr(uint32_t addr);
void hw_venc_mvw_set_height(uint32_t height, uint32_t m_block_width);
void hw_venc_mvw_set_addr_new(uint32_t base, uint32_t add);
void hw_venc_mvw_set_init_csr(BlockAgentConfig *cfg, MvTileInfo *mv_tile, uint32_t addr);
void hw_venc_mvw_set_tile_csr(BlockAgentConfig *cfg, MvTileInfo *mv_tile, uint32_t addr);
#else
#include <linux/types.h>
#endif

void hw_venc_mvw_cal_tile_info(BlockAgentConfig *cfg, TileInfo *tile, MvTileInfo *mv_tile, uint8_t t);
void venc_mvw_set_init_csr(volatile CsrBankVenc_mvw *csr, BlockAgentConfig *cfg, MvTileInfo *mv_tile);
void venc_mvw_calc_tile_info(DramAgentConfig *cfg, FrameInfo *frame, TileInfo *tile, DaLinearTile *da,
                             uint16_t offset_x, uint16_t offset_y, int is_last_tile);
void venc_mvw_set_tile_csr(volatile CsrBankVenc_mvw *csr, DaLinearTile *da, uint32_t init_addr);
void venc_mvw_set_frame_csr(volatile CsrBankVenc_mvw *csr, DramAgentConfig *cfg, FrameInfo *frame);
uint32_t calc_venc_mvw_buffer_size(const uint32_t width, const uint32_t height,
                                   const struct dram_agent_config *dram_agent);
#endif