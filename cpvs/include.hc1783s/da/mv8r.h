#ifndef HW_MV8R_H
#define HW_MV8R_H

#include "da_utils.h"
#include "csr_bank_mv8r.h"

#ifndef __KERNEL__
#include <stdint.h>
void hw_mv8r_frame_start();
void hw_mv8r_irq_clear();
uint32_t hw_mv8r_status_frame_end();
uint32_t hw_mv8r_status_bw_insufficient();
uint32_t hw_mv8r_status_access_violation();
void hw_mv8r_irq_mask(uint32_t frame_end, uint32_t bw_insufficient, uint32_t access_violation);
void hw_mv8r_dram_info(uint32_t col_addr_type, uint32_t bank_interleave_type, uint32_t bank_group_type);
void hw_mv8r_access_illegal(uint32_t hang, uint32_t mask);
void hw_mv8r_set_target_burst_len(uint32_t burst_len);

void hw_mv8r_init();
void hw_mv8r_set_addr(uint32_t addr);
void hw_mv8r_set_height(uint32_t height, uint32_t block_width);

void hw_mv8r_set_addr_new(uint32_t base, uint32_t add);
void hw_mv8r_set_init_csr(BlockAgentConfig *cfg, MvTileInfo *mv_tile, uint32_t addr);
void hw_mv8r_set_tile_csr(BlockAgentConfig *cfg, MvTileInfo *mv_tile, uint32_t addr);
void hw_mv8r_cal_tile_info(BlockAgentConfig *cfg, TileInfo *tile, MvTileInfo *mv_tile);
void mv8r_set_init_csr(volatile CsrBankMv8r *csr, BlockAgentConfig *cfg, MvTileInfo *mv_tile);
void mv8r_calc_tile_info(DramAgentConfig *cfg, FrameInfo *frame, TileInfo *tile, DaLinearTile *da);
void mv8r_set_frame_csr(volatile CsrBankMv8r *csr, DramAgentConfig *cfg, FrameInfo *frame);
#else
#include <linux/types.h>
#endif

// mv8r
void mv8r_set_tile_csr(volatile CsrBankMv8r *csr, DaLinearTile *da, uint32_t init_addr);
#endif