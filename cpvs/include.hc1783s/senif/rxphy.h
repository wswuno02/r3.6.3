#ifndef RXPHY_H_
#define RXPHY_H_

#include "csr_bank_fm.h"
#include "csr_bank_rx_ctrl.h"
#include "csr_bank_rx_phycfg.h"

#define RXPHY_CTRL_BASE(x) (RXPHY##x##_CTRL_BASE)

typedef struct rx_ctrl_ln_cfg {
	uint8_t ib10u_minus;
	uint8_t ib10u_plus;
	uint8_t vth_sel;
	uint8_t delay_sel;
	uint8_t term_sel;
	uint8_t lpf_p;
	uint8_t lpf_n;
} RxCtrlLnCfg;

void senif_fm_start(void);
uint32_t senif_fm_status(void);
void senif_fm_set_cnt_th(uint16_t th);
void senif_fm_set_target(uint16_t target);
void senif_fm_set_tolerance(uint8_t tolerance);
uint16_t senif_fm_get_clk_cnt(void);
uint16_t senif_fm_get_meas_cnt(void);

void senif_rx_ctrl_sel_pwr_ctrl_sel(uint8_t sel);
//void senif_rx_ctrl_sel_ctrl(uint8_t sel);
void senif_rx_ctrl_ln_pwron_start(uint8_t lane);
void senif_rx_ctrl_ln_gpi_pwron_start(uint8_t lane);
uint32_t senif_rx_ctrl_get_pwr_st(void);

void senif_rx_phycfg_set_ln_cfg(RxCtrlLnCfg *);
void senif_rx_phycfg_enable_ln_clk(uint32_t ln);

#endif /* RXPHY_H_ */
