#ifndef MCVP_H_
#define MCVP_H_

#include "da_utils.h"
#include "stdint.h"

void mcvp_frame_start();
void mcvp_irq_clear_frame_end();
uint32_t mcvp_status_frame_end();
void mcvp_irq_mask_frame_end(uint32_t irq_mask_frame_end);
void mcvp_resolution(uint32_t width, uint32_t height, uint32_t block_width);
void mcvp_debug_mon_sel(uint32_t debug_mon_sel);
void mcvp_disable(uint32_t disable);
void mcvp_1f();
void mcvp_set_height(uint32_t height, uint32_t block_hight);
void mcvp_set_tile(TileInfo *tile, uint32_t block_width, uint8_t leftmost, uint8_t rightmost);
void mcvp_set_sr(uint8_t blk_ix, uint8_t blk_iy, uint8_t ix, uint8_t iy);

#endif