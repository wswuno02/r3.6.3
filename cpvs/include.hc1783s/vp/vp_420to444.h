#ifndef VPK_420TO444_H_
#define VPK_420TO444_H_

#include "stdint.h"

typedef struct vp_420to444_resolution {
	uint32_t width;
	uint32_t height;
} Vp420to444Resolution;

typedef struct vp_420to444_ctx {
	Vp420to444Resolution vp_420to444_res;

} Vp420to444Ctx;

typedef struct vp_420to444_attr {
	uint32_t vp_420to444_width;
	uint32_t vp_420to444_height;

} Vp420to444Attr;

void hw_vp_420to444_frame_start();
void hw_vp_420to444_irq_clear_frame_end();
uint32_t hw_vp_420to444_status_frame_end();
void hw_vp_420to444_irq_mask_frame_end(uint32_t irq_mask_frame_end);
void hw_vp_420to444_resolution(uint32_t width, uint32_t height);
void hw_vp_420to444_bypass();
void hw_vp_420to444_run(Vp420to444Attr *vp420to444attr);
void hw_vp_420to444_set_ctx(Vp420to444Ctx *vp420to444ctx, Vp420to444Attr *vp420to444attr);
void hw_vp_420to444_set_reg(Vp420to444Ctx *vp420to444ctx);
void hw_vp_420to444_debug_mon_sel(uint32_t debug_mon_sel);

#endif