#ifndef B2R_H_
#define B2R_H_

#include "stdint.h"

#define VP_B2R_BASE 0x82210000

void hw_b2r_frame_start();
void hw_b2r_resolution(uint32_t width, uint32_t height, uint32_t block_width);

#endif