#ifndef HW_VP_H_
#define HW_VP_H_

#ifndef __KERNEL__
#include <stdint.h>
#else
#include <linux/types.h>
#endif
#include "csr_bank_vp.h"

typedef enum isp12_dst {
	ISP12_YUV420TO444,
	ISP12_VPWRITE0,
	ISP12_VPWRITE1,
	ISP12_NONE = 7,
} Isp12Dst;

typedef enum b2r_dst {
	B2R_YUV420TO444,
	B2R_VPWRITE0,
	B2R_VPWRITE1,
	B2R_NONE = 7,
} B2rDst;

typedef enum yuv444_dst {
	YUV444_SC,
	YUV444_VPCST,
	YUV444_NONE = 7,
} Yuv444Dst;

typedef enum vp_dst {
	VP_VPWRITE0,
	VP_VPWRITE1,
	VP_NONE = 7,
} VpDst;

typedef enum vpcst_dst {
	VPCST_VPLP,
	VPCST_VPCFA,
	VPCST_NONE = 7,
} VpCstDst;

typedef enum unpacker_src {
	UNPACKER_NRINK,
	UNPACKER_NRDISP,
} UnpackerSrc;

typedef enum yuv420to444_src {
	YUV420TO444_ISP2,
	YUV420TO444_ISP1,
	YUV420TO444_B2R,
} Yuv420to444Src;

typedef enum vpw_src {
	VPW_VPCFA,
	VPW_SC,
	VPW_ISP2,
	VPW_ISP1,
	VPW_B2R,
	VPW_VPLP,
} VpwSrc;

void vp_clear_checksum(void);
void vp_routing_0(void);
void vp_routing_1(void);
void vp_routing_2(void);
void vp_routing_3(void);
void vp_routing_b2r(void);
void vp_routing_sc(void);
void vp_routing_lp(void);
void vp_routing_cfa(void);
void vp_routing_sc_no_mcvp(void);
void vp_routing_sc_and_cfa(void);
void hw_vp_setroute(uint32_t pat);
void vp_set_sd(uint32_t en);
void vp_set_slp(uint32_t en);
void vp_dump_debug_mon(void);
void vp_print_checksum(void);

void isp1_bro(Isp12Dst dst);
void isp2_bro(Isp12Dst dst);
void b2r_bro(B2rDst dst);
void yuv444_bro(Yuv444Dst dst);
void vp_sc_bro(VpDst dst);
void vpcst_bro(VpCstDst dst);
void vpcfa_bro(VpDst dst);
void vplp_bro(VpDst dst);

typedef union vp_ispout_brdcst {
	struct {
		uint8_t to_yuv420to444 : 1;
		uint8_t to_vpw0 : 1;
		uint8_t to_vpw1 : 1;
		uint8_t : 0; /* padding */
	};
	uint8_t value;
} VpIspoutBrdcst;

typedef union vp_b2r_brdcst {
	struct {
		uint8_t to_yuv420to444 : 1;
		uint8_t to_vpw0 : 1;
		uint8_t to_vpw1 : 1;
		uint8_t : 0; /* padding */
	};
	uint8_t value;
} VpB2rBrdcst;

typedef union vp_yuv444_brdcst {
	struct {
		uint8_t to_vpsc : 1;
		uint8_t to_vpcst : 1;
		uint8_t : 0; /* padding */
	};
	uint8_t value;
} VpYuv444Brdcst;

typedef union vp_sc_brdcst {
	struct {
		uint8_t to_vpw0 : 1;
		uint8_t to_vpw1 : 1;
		uint8_t : 0; /* padding */
	};
	uint8_t value;
} VpScBrdcst;

typedef union vp_cst_brdcst {
	struct {
		uint8_t to_vplp : 1;
		uint8_t to_vpcfa : 1;
		uint8_t : 0; /* padding */
	};
	uint8_t value;
} VpCstBrdcst;

typedef union vp_cfa_brdcst {
	struct {
		uint8_t to_vpw0 : 1;
		uint8_t to_vpw1 : 1;
		uint8_t : 0; /* padding */
	};
	uint8_t value;
} VpCfaBrdcst;

typedef union vp_lp_brdcst {
	struct {
		uint8_t to_vpw0 : 1;
		uint8_t to_vpw1 : 1;
		uint8_t : 0; /* padding */
	};
	uint8_t value;
} VpLpBrdcst;

typedef enum vp_unpacker_sel {
	VP_UNPACKER_SEL_NR_INK = 0,
	VP_UNPACKER_SEL_NR_DISP,
	VP_UNPACKER_SEL_NONE = 0
} VpUnpackerSel;

typedef enum vp_yuv420to444_sel {
	VP_YUV420TO444_SEL_ISP2 = 0,
	VP_YUV420TO444_SEL_ISP1,
	VP_YUV420TO444_SEL_B2R,
	VP_YUV420TO444_SEL_NONE = 0
} VpYuv420to444Sel;

typedef enum vp_vpw_sel {
	VP_VPW_SEL_VPCFA = 0,
	VP_VPW_SEL_VPSC,
	VP_VPW_SEL_ISP2,
	VP_VPW_SEL_ISP1,
	VP_VPW_SEL_B2R,
	VP_VPW_SEL_NONE = 0
} VpVpwSel;

typedef struct vp_path_cfg {
	VpIspoutBrdcst isp1_brdcst;
	VpIspoutBrdcst isp2_brdcst;
	VpB2rBrdcst b2r_brdcst;
	VpYuv444Brdcst yuv444_brdcst;
	VpScBrdcst sc_brdcst;
	VpCstBrdcst cst_brdcst;
	VpCfaBrdcst cfa_brdcst;
	VpLpBrdcst lp_brdcst;
	VpUnpackerSel unpacker_sel;
	VpYuv420to444Sel yuv420to444_sel;
	VpVpwSel vpw0_sel;
	VpVpwSel vpw1_sel;
} VpPathCfg;

void vp_set_path(volatile CsrBankVp *csr, const VpPathCfg *cfg);

#endif