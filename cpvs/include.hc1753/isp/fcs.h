#ifndef KYOTO_FCS_H_
#define KYOTO_FCS_H_

#include "isp_utils.h"
#include "csr_bank_fcs.h"

typedef enum isp_fcs_mode {
	ISP_FCS_MODE_NORMAL = 0,
	ISP_FCS_MODE_DISABLE = 1,
	ISP_FCS_MODE_NUM = 2,
} IspFcsMode;

typedef struct isp_fcs_cfg {
	enum isp_fcs_mode mode;
	struct trans_lv_det {
		uint8_t diff_level_max;
		uint8_t gain;
	} trans; /**< Transition level detection */
	struct grid_lv_det {
		uint8_t cnt_th_high;
		uint8_t cnt_th_low;
		uint8_t cnt_raw_th;
		uint16_t trans_level_th;
		uint8_t trans_round_bit_high;
		uint8_t trans_round_bit_low;
	} grid; /**< Grid level detection */
	struct color_type_det {
		uint8_t diff_self_th;
		uint8_t diff_neighbor_th;
		uint8_t coring_th;
	} color; /**< Color type detection */
	struct false_color_corr {
		uint8_t correction_gain;
		uint16_t target_bg_ratio;
		uint16_t target_rg_ratio;
	} corr; /**< False color correction */
} IspFcsCfg;

/* Frame start */
void hw_isp_fcs_set_frame_start(void);

/* IRQ clear */
void hw_isp_fcs_set_irq_clear_frame_end(void);

/* Frame end*/
uint8_t hw_isp_fcs_get_status_frame_end(void);

/* IRQ mask */
void hw_isp_fcs_set_irq_mask_frame_end(uint8_t irq_mask_frame_end);
uint8_t hw_isp_fcs_get_irq_mask_frame_end(void);

/* Resolution */
void hw_isp_fcs_set_res(const struct res *res);
void hw_isp_fcs_get_res(struct res *res);

/* FCS */
void hw_isp_fcs_set_cfg(const struct isp_fcs_cfg *cfg);
void hw_isp_fcs_get_cfg(struct isp_fcs_cfg *cfg);

/* Debug */
void hw_isp_fcs_set_dbg_mon_sel(uint8_t debug_mon_sel);
uint8_t hw_isp_fcs_get_dbg_mon_sel(void);

/* Reserved */
void hw_isp_fcs_set_reserved(uint32_t reserved);
uint32_t hw_isp_fcs_get_reserved(void);

/* ATPG */
void hw_isp_shp_set_atpg_test_enable_0(uint8_t atpg_test_enable_0);
uint8_t hw_isp_shp_get_atpg_test_enable_0(void);
void hw_isp_shp_set_atpg_test_enable_1(uint8_t atpg_test_enable_1);
uint8_t hw_isp_shp_get_atpg_test_enable_1(void);

//
/* IRQ mask */
void isp_fcs_set_irq_mask_frame_end(volatile CsrBankFcs *csr, uint8_t irq_mask_frame_end);
uint8_t isp_fcs_get_irq_mask_frame_end(volatile CsrBankFcs *csr);

/* Resolution */
void isp_fcs_set_res(volatile CsrBankFcs *csr, const struct res *res);
void isp_fcs_get_res(volatile CsrBankFcs *csr, struct res *res);

/* FCS */
void isp_fcs_set_cfg(volatile CsrBankFcs *csr, const struct isp_fcs_cfg *cfg);
void isp_fcs_get_cfg(volatile CsrBankFcs *csr, struct isp_fcs_cfg *cfg);

/* Debug */
void isp_fcs_set_dbg_mon_sel(volatile CsrBankFcs *csr, uint8_t debug_mon_sel);
uint8_t isp_fcs_get_dbg_mon_sel(volatile CsrBankFcs *csr);

/* Reserved */
void isp_fcs_set_reserved(volatile CsrBankFcs *csr, uint32_t reserved);
uint32_t isp_fcs_get_reserved(volatile CsrBankFcs *csr);

/* ATPG */
void isp_fcs_set_atpg_test_enable_0(volatile CsrBankFcs *csr, uint8_t atpg_test_enable_0);
uint8_t isp_fcs_get_atpg_test_enable_0(volatile CsrBankFcs *csr);
void isp_fcs_set_atpg_test_enable_1(volatile CsrBankFcs *csr, uint8_t atpg_test_enable_1);
uint8_t isp_fcs_get_atpg_test_enable_1(volatile CsrBankFcs *csr);

/* TODO - Move to test */
void hw_isp_fcs_res(uint32_t width, uint32_t height);
void hw_isp_fcs_bypass(void);

#endif /* KYOTO_FCS_H_ */