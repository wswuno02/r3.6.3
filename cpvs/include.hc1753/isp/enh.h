#ifndef KYOTO_ENH_H_
#define KYOTO_ENH_H_

#include "isp_utils.h"
#include "csr_bank_enh.h"

typedef enum isp_enh_sample_mode {
	ISP_ENH_SAMPLE_MODE_YUV444 = 0,
	ISP_ENH_SAMPLE_MODE_YUV420 = 1,
	ISP_ENH_SAMPLE_MODE_NUM = 2,
} IspEnhSampleMode;

typedef struct isp_enh_cac_center_cfg {
	uint16_t cac_center_x;
	uint16_t cac_center_y;
	uint16_t cac_center_diff_norm_x;
	uint16_t cac_center_diff_norm_y;
} IspEnhCacCenterCfg;

typedef struct isp_enh_cac_cfg {
	uint8_t cac_enable;
	uint16_t cac_color_u;
	uint16_t cac_color_v;
	uint16_t cac_color_diff_norm_u;
	uint16_t cac_color_diff_norm_v;
	struct cac_edge_curve {
		uint16_t x[ENH_EDGE_CURVE_CTRL_POINT_NUM]; /**< Curve bin */
		uint8_t m[ENH_EDGE_CURVE_CTRL_POINT_NUM - 1]; /**< Curve slpoe */
		uint16_t y[ENH_EDGE_CURVE_CTRL_POINT_NUM]; /**< Curve value */
	} edge_curve; /**< Eage curve */
} IspEnhCacCfg;

typedef enum isp_enh_guided_filter_mode {
	ISP_ENH_GUIDEED_FILTER_MODE_NORMAL = 0,
	ISP_ENH_GUIDEED_FILTER_MODE_DISABLE = 1,
	ISP_ENH_GUIDEED_FILTER_MODE_BYPASS = 2,
	ISP_ENH_GUIDEED_FILTER_MODE_NUM = 3,
} IspEnhGuidedFilterMode;

typedef struct isp_enh_cfg {
	struct {
		enum isp_enh_guided_filter_mode y_mode;
		uint8_t y_l_r;
		uint32_t y_l_eps;
		uint8_t y_l_edge_strength; //
		uint16_t y_l_mean_norm;
		uint16_t y_l_dt_mean_norm;
		uint16_t y_l_dt_corr_norm;
		uint32_t y_s_eps;
		uint8_t y_s_edge_strength; //
		uint16_t y_s_mean_norm; //
		uint16_t y_s_dt_mean_norm; //
		uint16_t y_s_dt_corr_norm; //
		uint8_t y_l_detail_gain;
		uint8_t y_s_detail_gain;
		struct luma_blend_curve {
			uint8_t x[ENH_BLEND_CURVE_CTRL_POINT_NUM]; /**< Curve bin */
			uint8_t m[ENH_BLEND_CURVE_CTRL_POINT_NUM - 1]; /**< Curve slpoe */
			uint8_t y[ENH_BLEND_CURVE_CTRL_POINT_NUM]; /**< Curve value */
		} blend_curve; /**< Blending curve */
	} luma;
	struct {
		enum isp_enh_guided_filter_mode c_mode;
		uint8_t c_r;
		uint32_t c_eps_y;
		uint32_t c_eps_c;
		uint16_t c_mean_norm;
		uint16_t c_dt_mean_norm;
		uint16_t c_dt_corr_norm;
		uint8_t c_edge_y_strength; //
		uint8_t c_edge_c_strength; //
	} chroma;
	struct luma_gain_curve {
		uint16_t x[ENH_GAIN_CURVE_CTRL_POINT_NUM]; /**< Curve bin */
		uint16_t m_2s[ENH_GAIN_CURVE_CTRL_POINT_NUM - 1]; /**< Curve slpoe */
		uint8_t y[ENH_GAIN_CURVE_CTRL_POINT_NUM]; /**< Curve value */
	} gain_curve;
} IspEnhCfg;

/* Frame start */
void hw_isp_enh_set_frame_start(void);

/* IRQ clear */
void hw_isp_enh_set_irq_clear_frame_end(void);

/* Frame end*/
uint8_t hw_isp_enh_get_status_frame_end(void);

/* IRQ mask */
void hw_isp_enh_set_irq_mask_frame_end(uint8_t irq_mask_frame_end);
uint8_t hw_isp_enh_get_irq_mask_frame_end(void);

/* Input format */
void hw_isp_enh_set_sample_mode(enum isp_enh_sample_mode in);
enum isp_enh_sample_mode hw_isp_enh_get_sample_mode(void);

/* Resolution */
void hw_isp_enh_set_res(const struct res *res);
void hw_isp_enh_get_res(struct res *res);
void hw_isp_enh_set_cac_center_cfg(const struct isp_enh_cac_center_cfg *cfg);
void hw_isp_enh_get_cac_center_cfg(struct isp_enh_cac_center_cfg *cfg);
void hw_isp_enh_set_tile_start_x(uint16_t sx);
uint16_t hw_isp_enh_get_tile_start_x(void);

/* ENH */
void hw_isp_enh_set_cac_cfg(const struct isp_enh_cac_cfg *cfg);
void hw_isp_enh_get_cac_cfg(struct isp_enh_cac_cfg *cfg);
void hw_isp_enh_set_cfg(const struct isp_enh_cfg *cfg);
void hw_isp_enh_get_cfg(struct isp_enh_cfg *cfg);

/* Debug */
void hw_isp_enh_set_dbg_mon_sel(uint8_t debug_mon_sel);
uint8_t hw_isp_enh_get_dbg_mon_sel(void);

/* ATPG */
void hw_isp_shp_set_atpg_ctrl(uint8_t atpg_ctrl);
uint8_t hw_isp_shp_get_atpg_ctrl(void);

//
/* IRQ mask */
void isp_enh_set_irq_mask_frame_end(volatile CsrBankEnh *csr, uint8_t irq_mask_frame_end);
uint8_t isp_enh_get_irq_mask_frame_end(volatile CsrBankEnh *csr);

/* Input format */
void isp_enh_set_sample_mode(volatile CsrBankEnh *csr, enum isp_enh_sample_mode in);
enum isp_enh_sample_mode isp_enh_get_sample_mode(volatile CsrBankEnh *csr);

/* Resolution */
void isp_enh_set_res(volatile CsrBankEnh *csr, const struct res *res);
void isp_enh_get_res(volatile CsrBankEnh *csr, struct res *res);
void isp_enh_set_cac_center_cfg(volatile CsrBankEnh *csr, const struct isp_enh_cac_center_cfg *cfg);
void isp_enh_get_cac_center_cfg(volatile CsrBankEnh *csr, struct isp_enh_cac_center_cfg *cfg);
void isp_enh_set_tile_start_x(volatile CsrBankEnh *csr, uint16_t sx);
uint16_t isp_enh_get_tile_start_x(volatile CsrBankEnh *csr);

/* ENH */
void isp_enh_set_cac_cfg(volatile CsrBankEnh *csr, const struct isp_enh_cac_cfg *cfg);
void isp_enh_get_cac_cfg(volatile CsrBankEnh *csr, struct isp_enh_cac_cfg *cfg);
void isp_enh_set_cfg(volatile CsrBankEnh *csr, const struct isp_enh_cfg *cfg);
void isp_enh_get_cfg(volatile CsrBankEnh *csr, struct isp_enh_cfg *cfg);

/* Debug */
void isp_enh_set_dbg_mon_sel(volatile CsrBankEnh *csr, uint8_t debug_mon_sel);
uint8_t isp_enh_get_dbg_mon_sel(volatile CsrBankEnh *csr);

/* ATPG */
void isp_shp_set_atpg_ctrl(volatile CsrBankEnh *csr, uint8_t atpg_ctrl);
uint8_t isp_shp_get_atpg_ctrl(volatile CsrBankEnh *csr);

/* TODO - Move to test */
void hw_isp_enh_res(uint32_t width, uint32_t height);
void hw_isp_enh_bypass(void);

#endif /* KYOTO_ENH_H_ */