#ifndef KYOTO_DHZ_H_
#define KYOTO_DHZ_H_

#include "isp_utils.h"
#include "csr_bank_dhz.h"

typedef struct isp_dhz_rgl_ctrl {
	uint32_t rgl_x_num;
	uint32_t rgl_y_num;
	uint32_t rgl_x_cnt_step;
	uint32_t rgl_y_cnt_step;
} IspDhzRglCtrl;

typedef struct isp_dhz_tile_rgl_ctrl {
	uint32_t rgl_x_cnt_ini;
	uint32_t rgl_y_cnt_ini;
} IspDhzTileRglCtrl;

typedef struct isp_dhz_rgl_cfg {
	uint16_t gain[DHZ_MAX_RGL_NUM];
	uint16_t offset[DHZ_MAX_RGL_NUM];
} IspDhzRglCfg;

typedef struct isp_dhz_cfg {
	uint16_t strength;
	uint16_t y_gain_max;
	uint16_t c_gain_max;
	uint16_t y_dc;
	uint16_t u_dc;
	uint16_t v_dc;
} IspDhzCfg;

typedef struct isp_dhz_roi_stat_frame_ctrl {
	uint16_t sy;
	uint16_t ey;
	uint32_t pix_num;
} IspDhzRoiStatFrameCtrl;

typedef struct isp_dhz_roi_stat_tile_ctrl {
	uint16_t sx;
	uint16_t ex;
} IspDhzRoiStatTileCtrl;

typedef struct isp_dhz_roi_stat {
	uint16_t luma_avg;
} IspDhzRoiStat;

/* Frame start */
void hw_isp_dhz_set_frame_start(void);

/* IRQ clear */
void hw_isp_dhz_set_irq_clear_frame_end(void);

/* Frame end*/
uint8_t hw_isp_dhz_get_status_frame_end(void);

/* IRQ mask */
void hw_isp_dhz_set_irq_mask_frame_end(uint8_t irq_mask_frame_end);
uint8_t hw_isp_dhz_get_irq_mask_frame_end(void);

/* Resolution */
void hw_isp_dhz_set_res(const Res *res);
void hw_isp_dhz_get_res(Res *res);

/* Dehaze */
void hw_isp_dhz_set_rgl_ctrl(const struct isp_dhz_rgl_ctrl *cfg);
void hw_isp_dhz_get_rgl_ctrl(struct isp_dhz_rgl_ctrl *cfg);
void hw_isp_dhz_set_tile_rgl_ctrl(const struct isp_dhz_tile_rgl_ctrl *cfg);
void hw_isp_dhz_get_tile_rgl_ctrl(struct isp_dhz_tile_rgl_ctrl *cfg);
uint8_t hw_isp_dhz_get_rgl_cfg_num_of_rgl(void);
void hw_isp_dhz_set_rgl_cfg(uint8_t num_of_rgl, const struct isp_dhz_rgl_cfg *cfg);
void hw_isp_dhz_get_rgl_cfg(uint8_t num_of_rgl, struct isp_dhz_rgl_cfg *cfg);
uint8_t hw_isp_dhz_get_rgl_attr_clk_sel(void);
void hw_isp_dhz_set_cfg(const struct isp_dhz_cfg *cfg);
void hw_isp_dhz_get_cfg(struct isp_dhz_cfg *cfg);

/* Statistics */
/*
 * The following sequence is used to obtain ROI statistics of DHZ by frame
 *
 * for frame N
 *		hw_isp_dhz_set_rgl_stat_enable(1);
 *		hw_isp_dhz_set_rgl_stat_clear(1);
 *		hw_isp_dhz_set_rgl_stat_clear(0);
 *		hw_isp_dhz_set_roi_stat_frame_ctrl(&frame_cfg);
 *		for tile 0
 *				hw_isp_dhz_set_roi_stat_tile_ctrl(&tile_cfg);
 *			tile 1
 *				hw_isp_dhz_set_roi_stat_tile_ctrl(&tile_cfg);
 *			...
 *			tile M
 *				hw_isp_dhz_set_roi_stat_tile_ctrl(&tile_cfg);
 *		hw_isp_dhz_get_roi_stat(&stat);
 */
void hw_isp_dhz_set_roi_stat_frame_ctrl(const struct isp_dhz_roi_stat_frame_ctrl *cfg);
void hw_isp_dhz_get_roi_stat_frame_ctrl(struct isp_dhz_roi_stat_frame_ctrl *cfg);
void hw_isp_dhz_set_roi_stat_tile_ctrl(const struct isp_dhz_roi_stat_tile_ctrl *cfg);
void hw_isp_dhz_get_roi_stat_tile_ctrl(struct isp_dhz_roi_stat_tile_ctrl *cfg);
void hw_isp_dhz_set_roi_stat_clear(uint8_t clear);
uint8_t hw_isp_dhz_get_roi_stat_clear(void);
void hw_isp_dhz_set_roi_stat_enable(uint8_t enable);
uint8_t hw_isp_dhz_get_roi_stat_enable(void);
void hw_isp_dhz_get_roi_stat(struct isp_dhz_roi_stat *stat);

/* Debug */
void hw_isp_dhz_set_dbg_mon_sel(uint8_t debug_mon_sel);
uint8_t hw_isp_dhz_get_dbg_mon_sel(void);

//
/* IRQ mask */
void isp_dhz_set_irq_mask_frame_end(volatile CsrBankDhz *csr, uint8_t irq_mask_frame_end);
uint8_t isp_dhz_get_irq_mask_frame_end(volatile CsrBankDhz *csr);

/* Resolution */
void isp_dhz_set_res(volatile CsrBankDhz *csr, const Res *res);
void isp_dhz_get_res(volatile CsrBankDhz *csr, Res *res);

/* Dehaze */
void isp_dhz_set_rgl_ctrl(volatile CsrBankDhz *csr, const struct isp_dhz_rgl_ctrl *cfg);
void isp_dhz_get_rgl_ctrl(volatile CsrBankDhz *csr, struct isp_dhz_rgl_ctrl *cfg);
void isp_dhz_set_tile_rgl_ctrl(volatile CsrBankDhz *csr, const struct isp_dhz_tile_rgl_ctrl *cfg);
void isp_dhz_get_tile_rgl_ctrl(volatile CsrBankDhz *csr, struct isp_dhz_tile_rgl_ctrl *cfg);
uint8_t isp_dhz_get_rgl_cfg_num_of_rgl(volatile CsrBankDhz *csr);
void isp_dhz_set_rgl_cfg(volatile CsrBankDhz *csr, uint8_t num_of_rgl, const struct isp_dhz_rgl_cfg *cfg);
void isp_dhz_get_rgl_cfg(volatile CsrBankDhz *csr, uint8_t num_of_rgl, struct isp_dhz_rgl_cfg *cfg);
uint8_t isp_dhz_get_rgl_attr_clk_sel(volatile CsrBankDhz *csr);
void isp_dhz_set_cfg(volatile CsrBankDhz *csr, const struct isp_dhz_cfg *cfg);
void isp_dhz_get_cfg(volatile CsrBankDhz *csr, struct isp_dhz_cfg *cfg);

/* Statistics */
void isp_dhz_set_roi_stat_frame_ctrl(volatile CsrBankDhz *csr, const struct isp_dhz_roi_stat_frame_ctrl *cfg);
void isp_dhz_get_roi_stat_frame_ctrl(volatile CsrBankDhz *csr, struct isp_dhz_roi_stat_frame_ctrl *cfg);
void isp_dhz_set_roi_stat_tile_ctrl(volatile CsrBankDhz *csr, const struct isp_dhz_roi_stat_tile_ctrl *cfg);
void isp_dhz_get_roi_stat_tile_ctrl(volatile CsrBankDhz *csr, struct isp_dhz_roi_stat_tile_ctrl *cfg);
void isp_dhz_set_roi_stat_clear(volatile CsrBankDhz *csr, uint8_t clear);
uint8_t isp_dhz_get_roi_stat_clear(volatile CsrBankDhz *csr);
void isp_dhz_set_roi_stat_enable(volatile CsrBankDhz *csr, uint8_t enable);
uint8_t isp_dhz_get_roi_stat_enable(volatile CsrBankDhz *csr);
void isp_dhz_get_roi_stat(volatile CsrBankDhz *csr, struct isp_dhz_roi_stat *stat);

/* Debug */
void isp_dhz_set_dbg_mon_sel(volatile CsrBankDhz *csr, uint8_t debug_mon_sel);
uint8_t isp_dhz_get_dbg_mon_sel(volatile CsrBankDhz *csr);

/* TODO - Move to test */
void hw_isp_dhz_res(uint32_t width, uint32_t height);
void hw_isp_dhz_bypass(void);

#endif /* KYOTO_DHZ_H_ */