#ifndef CSR_BANK_DDRPHY_H_
#define CSR_BANK_DDRPHY_H_

#ifndef __KERNEL__
#include <stdint.h>
#else
#include <linux/types.h>
#endif

/***  C struct generated from ddrphy  ***/
typedef struct csr_bank_ddrphy {
	/* IP00 10'h00 */
	union {
		uint32_t ip00; // word name
		struct {
			uint32_t ip : 1;
			uint32_t : 7; // padding bits
			uint32_t : 8; // padding bits
			uint32_t : 8; // padding bits
			uint32_t : 8; // padding bits
		};
	};
} CsrBankDdrphy;

#endif