#ifndef DA_H_
#define DA_H_

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif
#include "da_define.h"

int gen_da_setting(DramAgentConfig *da_table, const uint8_t start_item, const uint8_t end_item);
void print_da_config(struct dram_agent_config *da);
void print_pixel_tile(struct da_pixel_tile *da, int t);
void print_block_tile(struct da_block_tile *da, int t);
void print_linear_tile(struct da_linear_tile *da, int t);
#endif
