#ifndef GFX_WEIGHTR_H
#define GFX_WEIGHTR_H

#include "da_utils.h"

#ifndef __KERNEL__
void hw_coordr_frame_start(uint8_t view);

void hw_coordr_set_dram_info(uint8_t view, uint32_t col_addr_type, uint32_t bank_interleave_type,
                         uint32_t bank_group_type);
void hw_coordr_access_illegal(uint8_t view, uint32_t hang, uint32_t mask);
void hw_coordr_set_target_burst_len(uint8_t view, uint32_t burst_len);
void hw_coordr_access_end_sel(uint8_t view, uint32_t sel);
void hw_coordr_debugmon_sel(uint8_t view, uint32_t sel);
void hw_coordr_set_fifo(uint8_t view, uint32_t target_fifo_level, uint32_t fifo_full_level);
void hw_coordr_init(uint8_t view);
void hw_coordr_set_addr(uint8_t view, uint32_t addr);
void hw_coordr_set_addr_new(uint8_t view, uint32_t base, uint32_t add);
void hw_coordr_cal_tile_info(BlockAgentConfig *cfg, TileInfo *tile, MvTileInfo *mv_tile);
void hw_coordr_set_init_csr(uint8_t view, BlockAgentConfig *cfg, MvTileInfo *mv_tile, uint32_t addr);
void hw_coordr_set_tile_csr(uint8_t view, BlockAgentConfig *cfg, MvTileInfo *mv_tile, uint32_t addr);
#endif

#endif