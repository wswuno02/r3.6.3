#ifndef DPCR_H_
#define DPCR_H_

#include "is_utils.h"

void hw_is_dpcr_set_frame_start(const enum is_kel_no no);
/* IRQ clear */
void hw_is_dpcr_set_irq_clear(const enum is_kel_no no);
/* Status */
uint8_t hw_is_dpcr_get_status_frame_end(const enum is_kel_no no);
uint8_t hw_is_dpcr_get_status_access_violation(const enum is_kel_no no);
uint8_t hw_is_dpcr_get_status_bw_insufficient(const enum is_kel_no no);
/* IRQ mask */
void hw_is_dpcr_set_irq_mask_frame_end(const enum is_kel_no no, uint8_t irq_mask_frame_end);
void hw_is_dpcr_set_irq_mask_access_violation(const enum is_kel_no no, uint8_t irq_mask_frame_end);
void hw_is_dpcr_set_irq_mask_bw_insufficient(const enum is_kel_no no, uint8_t irq_mask_frame_end);
uint8_t hw_is_dpcr_get_irq_mask_frame_end(const enum is_kel_no no);
uint8_t hw_is_dpcr_get_irq_mask_access_violation(const enum is_kel_no no);
uint8_t hw_is_dpcr_get_irq_mask_bw_insufficient(const enum is_kel_no no);
void hw_is_dpcr_set_num(const enum is_kel_no no, uint32_t pxl_num);
void hw_is_dpcr_set_addr(const enum is_kel_no no, uint32_t addr);
void hw_is_dpcr_init(const enum is_kel_no no, uint32_t addr, uint32_t pxl_num);

#endif