#ifndef ISK0_CS_H_
#define ISK0_CS_H_

#include <stdint.h>

void hw_isk0_cs_frame_start(void);
void hw_isk0_cs_irq_clear_frame_end(void);
uint32_t hw_isk0_cs_status_frame_end(void);
void hw_isk0_cs_irq_mask_frame_end(uint32_t irq_mask_frame_end);
void hw_isk0_cs_resolution(uint32_t width, uint32_t height);
void hw_isk0_cs_bypass(void);
void hw_isk0_cs_debug_mon_sel(uint32_t debug_mon_sel);

#endif