#ifndef VP_UTILS_H_
#define VP_UTILS_H_

#include <stdint.h>
#include "printf.h"

#define VP_FRAME_END(func, name)                                                                                       \
	if (func) {                                                                                                    \
		printf("%8s frame end!\n", name);                                                                      \
	}

void hw_vp_init_setting(uint32_t width, uint32_t height, uint32_t block_width);
void hw_vp_eng_bypass();
void hw_vp_unmask_irq_frame_end();
void hw_vp_clear_irq();
void hw_vp_frame_start();
void hw_vp_check_frame_end();
void dump_isp_vp_csr();

#endif