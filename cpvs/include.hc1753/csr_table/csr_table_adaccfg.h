#ifndef CSR_TABLE_ADACCFG_H_
#define CSR_TABLE_ADACCFG_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_adaccfg[] =
{
  // WORD adaccfg00
  { "adac_endac", 0x00000000,  0,  0,   CSR_RW, 0x00000000 },
  { "adac_envbg", 0x00000000,  8,  8,   CSR_RW, 0x00000000 },
  { "ADACCFG00", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adaccfg02
  { "adac_in_mon", 0x00000008, 11,  0,   CSR_RO, 0x00000000 },
  { "ADACCFG02", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_ADACCFG_H_
