#ifndef CSR_TABLE_ISP_H_
#define CSR_TABLE_ISP_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_isp[] =
{
  // WORD ispr0_broadcast
  { "ispr0_broadcast_to_pg_enable", 0x0000000C,  0,  0,   CSR_RW, 0x00000000 },
  { "ispr0_broadcast_to_gfxmux_enable", 0x0000000C,  8,  8,   CSR_RW, 0x00000001 },
  { "ispr0_broadcast_ack_not_sel", 0x0000000C, 16, 16,   CSR_RW, 0x00000000 },
  { "ISPR0_BROADCAST", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ispr1_broadcast
  { "ispr1_broadcast_to_pg_enable", 0x00000010,  0,  0,   CSR_RW, 0x00000000 },
  { "ispr1_broadcast_to_gfxmux_enable", 0x00000010,  8,  8,   CSR_RW, 0x00000001 },
  { "ispr1_broadcast_ack_not_sel", 0x00000010, 16, 16,   CSR_RW, 0x00000000 },
  { "ISPR1_BROADCAST", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pg0_broadcast
  { "pg0_broadcast_to_cs_enable", 0x00000014,  0,  0,   CSR_RW, 0x00000000 },
  { "pg0_broadcast_to_gfxmux_enable", 0x00000014,  8,  8,   CSR_RW, 0x00000001 },
  { "pg0_broadcast_ack_not_sel", 0x00000014, 16, 16,   CSR_RW, 0x00000000 },
  { "PG0_BROADCAST", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pg1_broadcast
  { "pg1_broadcast_to_cs_enable", 0x00000018,  0,  0,   CSR_RW, 0x00000000 },
  { "pg1_broadcast_to_gfxmux_enable", 0x00000018,  8,  8,   CSR_RW, 0x00000001 },
  { "pg1_broadcast_ack_not_sel", 0x00000018, 16, 16,   CSR_RW, 0x00000000 },
  { "PG1_BROADCAST", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gfx0_mux
  { "gfx0_din_sel", 0x0000001C,  0,  0,   CSR_RW, 0x00000000 },
  { "gfx0_din_mux_ack_not_sel", 0x0000001C,  8,  8,   CSR_RW, 0x00000000 },
  { "GFX0_MUX", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gfx1_mux
  { "gfx1_din_sel", 0x00000020,  0,  0,   CSR_RW, 0x00000000 },
  { "gfx1_din_mux_ack_not_sel", 0x00000020,  8,  8,   CSR_RW, 0x00000000 },
  { "GFX1_MUX", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD prd_mode
  { "prd0_mode", 0x00000024,  0,  0,   CSR_RW, 0x00000001 },
  { "prd1_mode", 0x00000024,  8,  8,   CSR_RW, 0x00000001 },
  { "PRD_MODE", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gfxbld_bypass
  { "gfxbld_bypass", 0x00000028,  0,  0,   CSR_RW, 0x00000000 },
  { "WORD_GFXBLD_BYPASS", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gfxbld_in0_mux
  { "gfxbld_in0_sel", 0x0000002C,  0,  0,   CSR_RW, 0x00000000 },
  { "gfxbld_in0_mux_ack_not_sel", 0x0000002C,  8,  8,   CSR_RW, 0x00000000 },
  { "GFXBLD_IN0_MUX", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gfxbld_in1_mux
  { "gfxbld_in1_sel", 0x00000030,  0,  0,   CSR_RW, 0x00000000 },
  { "gfxbld_in1_mux_ack_not_sel", 0x00000030,  8,  8,   CSR_RW, 0x00000000 },
  { "GFXBLD_IN1_MUX", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gfxbld_in1_broadcast
  { "gfxbld1_broadcast_to_gfxbld_enable", 0x00000034,  0,  0,   CSR_RW, 0x00000001 },
  { "gfxbld1_broadcast_to_out_enable", 0x00000034,  8,  8,   CSR_RW, 0x00000000 },
  { "gfxbld1_broadcast_ack_not_sel", 0x00000034, 16, 16,   CSR_RW, 0x00000000 },
  { "GFXBLD_IN1_BROADCAST", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ispin0_yc_broadcast
  { "ispin0_y_broadcast_enable", 0x00000038,  0,  0,   CSR_RW, 0x00000001 },
  { "ispin0_c_broadcast_enable", 0x00000038,  8,  8,   CSR_RW, 0x00000001 },
  { "ispin0_yc_broadcast_ack_not_sel", 0x00000038, 16, 16,   CSR_RW, 0x00000000 },
  { "ISPIN0_YC_BROADCAST", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_ispin0_y_broadcast
  { "ispin0_y_broadcast", 0x0000003C,  6,  0,   CSR_RW, 0x00000001 },
  { "ispin0_y_broadcast_ack_not_sel", 0x0000003C,  8,  8,   CSR_RW, 0x00000000 },
  { "WORD_ISPIN0_Y_BROADCAST", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_ispin0_c_broadcast
  { "ispin0_c_broadcast", 0x00000040,  4,  0,   CSR_RW, 0x00000001 },
  { "ispin0_c_broadcast_ack_not_sel", 0x00000040,  8,  8,   CSR_RW, 0x00000000 },
  { "WORD_ISPIN0_C_BROADCAST", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ispin1_yc_broadcast
  { "ispin1_y_broadcast_enable", 0x00000044,  0,  0,   CSR_RW, 0x00000001 },
  { "ispin1_c_broadcast_enable", 0x00000044,  8,  8,   CSR_RW, 0x00000001 },
  { "ispin1_yc_broadcast_ack_not_sel", 0x00000044, 16, 16,   CSR_RW, 0x00000000 },
  { "ISPIN1_YC_BROADCAST", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_ispin1_y_broadcast
  { "ispin1_y_broadcast", 0x00000048,  6,  0,   CSR_RW, 0x00000001 },
  { "ispin1_y_broadcast_ack_not_sel", 0x00000048,  8,  8,   CSR_RW, 0x00000000 },
  { "WORD_ISPIN1_Y_BROADCAST", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_ispin1_c_broadcast
  { "ispin1_c_broadcast", 0x0000004C,  4,  0,   CSR_RW, 0x00000001 },
  { "ispin1_c_broadcast_ack_not_sel", 0x0000004C,  8,  8,   CSR_RW, 0x00000000 },
  { "WORD_ISPIN1_C_BROADCAST", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_hdr_bypass
  { "hdr_bypass", 0x00000050,  0,  0,   CSR_RW, 0x00000000 },
  { "WORD_HDR_BYPASS", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dpchdr_c_mux
  { "dpchdr_c_mux_sel", 0x00000054,  0,  0,   CSR_RW, 0x00000000 },
  { "dpchdr_c_mux_ack_not_sel", 0x00000054,  8,  8,   CSR_RW, 0x00000000 },
  { "DPCHDR_C_MUX", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dpchdr_yc_broadcast
  { "dpchdr_y_broadcast_enable", 0x00000058,  0,  0,   CSR_RW, 0x00000001 },
  { "dpchdr_c_broadcast_enable", 0x00000058,  8,  8,   CSR_RW, 0x00000001 },
  { "dpchdr_yc_broadcast_ack_not_sel", 0x00000058, 16, 16,   CSR_RW, 0x00000000 },
  { "DPCHDR_YC_BROADCAST", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_dpchdr_y_broadcast
  { "dpchdr_y_broadcast", 0x0000005C,  4,  0,   CSR_RW, 0x00000001 },
  { "dpchdr_y_broadcast_ack_not_sel", 0x0000005C,  8,  8,   CSR_RW, 0x00000000 },
  { "WORD_DPCHDR_Y_BROADCAST", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_dpchdr_c_broadcast
  { "dpchdr_c_broadcast", 0x00000060,  3,  0,   CSR_RW, 0x00000001 },
  { "dpchdr_c_broadcast_ack_not_sel", 0x00000060,  8,  8,   CSR_RW, 0x00000000 },
  { "WORD_DPCHDR_C_BROADCAST", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rgbp_mux
  { "rgbp_mux_sel", 0x00000064,  1,  0,   CSR_RW, 0x00000000 },
  { "rgbp_mux_ack_not_sel", 0x00000064,  8,  8,   CSR_RW, 0x00000000 },
  { "RGBP_MUX", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rgbp_yc_broadcast
  { "rgbp_y_broadcast_enable", 0x00000068,  0,  0,   CSR_RW, 0x00000001 },
  { "rgbp_c_broadcast_enable", 0x00000068,  8,  8,   CSR_RW, 0x00000001 },
  { "rgbp_yc_broadcast_ack_not_sel", 0x00000068, 16, 16,   CSR_RW, 0x00000000 },
  { "RGBP_YC_BROADCAST", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD buf_update
  { "double_buf_update", 0x0000006C,  0,  0,  CSR_W1P, 0x00000000 },
  { "BUF_UPDATE", 0x0000006C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rgbp_gma_mode
  { "rgbp_gamma_mode", 0x00000070,  0,  0,   CSR_RW, 0x00000000 },
  { "RGBP_GMA_MODE", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_rgbp_y_broadcast
  { "rgbp_y_broadcast", 0x00000074,  1,  0,   CSR_RW, 0x00000001 },
  { "rgbp_y_broadcast_ack_not_sel", 0x00000074,  8,  8,   CSR_RW, 0x00000000 },
  { "WORD_RGBP_Y_BROADCAST", 0x00000074, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_rgbp_c_broadcast
  { "rgbp_c_broadcast", 0x00000078,  1,  0,   CSR_RW, 0x00000001 },
  { "rgbp_c_broadcast_ack_not_sel", 0x00000078,  8,  8,   CSR_RW, 0x00000000 },
  { "WORD_RGBP_C_BROADCAST", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sc_y_mux
  { "sc_y_mux_sel", 0x0000007C,  1,  0,   CSR_RW, 0x00000000 },
  { "sc_y_mux_ack_not_sel", 0x0000007C,  8,  8,   CSR_RW, 0x00000000 },
  { "SC_Y_MUX", 0x0000007C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sc_c_mux
  { "sc_c_mux_sel", 0x00000080,  0,  0,   CSR_RW, 0x00000000 },
  { "sc_c_mux_ack_not_sel", 0x00000080,  8,  8,   CSR_RW, 0x00000000 },
  { "SC_C_MUX", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sc_yc_broadcast
  { "sc_y_broadcast_enable", 0x00000084,  0,  0,   CSR_RW, 0x00000001 },
  { "sc_c_broadcast_enable", 0x00000084,  8,  8,   CSR_RW, 0x00000001 },
  { "sc_yc_broadcast_ack_not_sel", 0x00000084, 16, 16,   CSR_RW, 0x00000000 },
  { "SC_YC_BROADCAST", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_sc_y_broadcast
  { "sc_y_broadcast", 0x00000088,  2,  0,   CSR_RW, 0x00000001 },
  { "sc_y_broadcast_ack_not_sel", 0x00000088,  8,  8,   CSR_RW, 0x00000000 },
  { "WORD_SC_Y_BROADCAST", 0x00000088, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_sc_c_broadcast
  { "sc_c_broadcast", 0x0000008C,  2,  0,   CSR_RW, 0x00000001 },
  { "sc_c_broadcast_ack_not_sel", 0x0000008C,  8,  8,   CSR_RW, 0x00000000 },
  { "WORD_SC_C_BROADCAST", 0x0000008C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD enh_y_mux
  { "enh_y_mux_sel", 0x00000090,  2,  0,   CSR_RW, 0x00000000 },
  { "enh_y_mux_ack_not_sel", 0x00000090,  8,  8,   CSR_RW, 0x00000000 },
  { "ENH_Y_MUX", 0x00000090, 31, 0,   CSR_RW, 0x00000000 },
  // WORD enh_c_mux
  { "enh_c_mux_sel", 0x00000094,  2,  0,   CSR_RW, 0x00000000 },
  { "enh_c_mux_ack_not_sel", 0x00000094,  8,  8,   CSR_RW, 0x00000000 },
  { "ENH_C_MUX", 0x00000094, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dhz_yc_broadcast
  { "dhz_y_broadcast_enable", 0x00000098,  0,  0,   CSR_RW, 0x00000001 },
  { "dhz_c_broadcast_enable", 0x00000098,  8,  8,   CSR_RW, 0x00000001 },
  { "dhz_yc_broadcast_ack_not_sel", 0x00000098, 16, 16,   CSR_RW, 0x00000000 },
  { "DHZ_YC_BROADCAST", 0x00000098, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_dhz_y_broadcast
  { "dhz_y_broadcast", 0x0000009C,  2,  0,   CSR_RW, 0x00000001 },
  { "dhz_y_broadcast_ack_not_sel", 0x0000009C,  8,  8,   CSR_RW, 0x00000000 },
  { "WORD_DHZ_Y_BROADCAST", 0x0000009C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_dhz_c_broadcast
  { "dhz_c_broadcast", 0x000000A0,  2,  0,   CSR_RW, 0x00000001 },
  { "dhz_c_broadcast_ack_not_sel", 0x000000A0,  8,  8,   CSR_RW, 0x00000000 },
  { "WORD_DHZ_C_BROADCAST", 0x000000A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD fifo0_y_mux
  { "fifo0_y_mux_sel", 0x000000A4,  1,  0,   CSR_RW, 0x00000000 },
  { "fifo0_y_mux_ack_not_sel", 0x000000A4,  8,  8,   CSR_RW, 0x00000000 },
  { "FIFO0_Y_MUX", 0x000000A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD fifo0_c_mux
  { "fifo0_c_mux_sel", 0x000000A8,  1,  0,   CSR_RW, 0x00000000 },
  { "fifo0_c_mux_ack_not_sel", 0x000000A8,  8,  8,   CSR_RW, 0x00000000 },
  { "FIFO0_C_MUX", 0x000000A8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD fifo1_y_mux
  { "fifo1_y_mux_sel", 0x000000AC,  2,  0,   CSR_RW, 0x00000000 },
  { "fifo1_y_mux_ack_not_sel", 0x000000AC,  8,  8,   CSR_RW, 0x00000000 },
  { "FIFO1_Y_MUX", 0x000000AC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD fifo1_c_mux
  { "fifo1_c_mux_sel", 0x000000B0,  2,  0,   CSR_RW, 0x00000000 },
  { "fifo1_c_mux_ack_not_sel", 0x000000B0,  8,  8,   CSR_RW, 0x00000000 },
  { "FIFO1_C_MUX", 0x000000B0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD fifo2_y_mux
  { "fifo2_y_mux_sel", 0x000000B4,  2,  0,   CSR_RW, 0x00000000 },
  { "fifo2_y_mux_ack_not_sel", 0x000000B4,  8,  8,   CSR_RW, 0x00000000 },
  { "FIFO2_Y_MUX", 0x000000B4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD fifo2_c_mux
  { "fifo2_c_mux_sel", 0x000000B8,  2,  0,   CSR_RW, 0x00000000 },
  { "fifo2_c_mux_ack_not_sel", 0x000000B8,  8,  8,   CSR_RW, 0x00000000 },
  { "FIFO2_C_MUX", 0x000000B8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mem_lp_ctrl
  { "sd", 0x000000BC,  0,  0,   CSR_RW, 0x00000000 },
  { "slp", 0x000000BC,  8,  8,   CSR_RW, 0x00000000 },
  { "MEM_LP_CTRL", 0x000000BC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dbg_mon_sel
  { "debug_mon_sel", 0x000000C0,  5,  0,   CSR_RW, 0x00000000 },
  { "DBG_MON_SEL", 0x000000C0, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_ISP_H_
