#ifndef CSR_TABLE_TX_ENC_H_
#define CSR_TABLE_TX_ENC_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_tx_enc[] =
{
  // WORD en
  { "enable", 0x00000000,  0,  0,   CSR_RW, 0x00000000 },
  { "EN", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rst
  { "frame_reset", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "soft_reset", 0x00000004, 16, 16,  CSR_W1P, 0x00000000 },
  { "RST", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD ctrl
  { "start", 0x00000008,  0,  0,  CSR_W1P, 0x00000000 },
  { "stop", 0x00000008,  8,  8,  CSR_W1P, 0x00000000 },
  { "CTRL", 0x00000008, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD irqsta
  { "status_frame_end", 0x0000000C,  0,  0,   CSR_RO, 0x00000000 },
  { "status_frame_start", 0x0000000C,  1,  1,   CSR_RO, 0x00000000 },
  { "status_fifo_undrflow", 0x0000000C,  2,  2,   CSR_RO, 0x00000000 },
  { "status_tearing_effect", 0x0000000C,  3,  3,   CSR_RO, 0x00000000 },
  { "status_err_read_fifo_over_write", 0x0000000C,  4,  4,   CSR_RO, 0x00000000 },
  { "status_inf_check_cmd_ecc", 0x0000000C,  5,  5,   CSR_RO, 0x00000000 },
  { "status_err_check_cmd_ecc", 0x0000000C,  6,  6,   CSR_RO, 0x00000000 },
  { "status_err_check_crc", 0x0000000C,  7,  7,   CSR_RO, 0x00000000 },
  { "status_err_bit_format", 0x0000000C,  8,  8,   CSR_RO, 0x00000000 },
  { "status_ta_to", 0x0000000C,  9,  9,   CSR_RO, 0x00000000 },
  { "status_presp_to", 0x0000000C, 10, 10,   CSR_RO, 0x00000000 },
  { "IRQSTA", 0x0000000C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD irqmsk
  { "irq_mask_frame_end", 0x00000010,  0,  0,   CSR_RW, 0x00000000 },
  { "irq_mask_frame_start", 0x00000010,  1,  1,   CSR_RW, 0x00000000 },
  { "irq_mask_fifo_undrflow", 0x00000010,  2,  2,   CSR_RW, 0x00000000 },
  { "irq_mask_tearing_effect", 0x00000010,  3,  3,   CSR_RW, 0x00000000 },
  { "irq_mask_err_read_fifo_over_write", 0x00000010,  4,  4,   CSR_RW, 0x00000000 },
  { "irq_mask_inf_check_cmd_ecc", 0x00000010,  5,  5,   CSR_RW, 0x00000000 },
  { "irq_mask_err_check_cmd_ecc", 0x00000010,  6,  6,   CSR_RW, 0x00000000 },
  { "irq_mask_err_check_crc", 0x00000010,  7,  7,   CSR_RW, 0x00000000 },
  { "irq_mask_err_bit_format", 0x00000010,  8,  8,   CSR_RW, 0x00000000 },
  { "irq_mask_ta_to", 0x00000010,  9,  9,   CSR_RW, 0x00000000 },
  { "irq_mask_presp_to", 0x00000010, 10, 10,   CSR_RW, 0x00000000 },
  { "IRQMSK", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD irqack
  { "irq_clear_frame_end", 0x00000014,  0,  0,  CSR_W1P, 0x00000000 },
  { "irq_clear_frame_start", 0x00000014,  1,  1,  CSR_W1P, 0x00000000 },
  { "irq_clear_fifo_undrflow", 0x00000014,  2,  2,  CSR_W1P, 0x00000000 },
  { "irq_clear_tearing_effect", 0x00000014,  3,  3,  CSR_W1P, 0x00000000 },
  { "irq_clear_err_read_fifo_over_write", 0x00000014,  4,  4,  CSR_W1P, 0x00000000 },
  { "irq_clear_inf_check_cmd_ecc", 0x00000014,  5,  5,  CSR_W1P, 0x00000000 },
  { "irq_clear_err_check_cmd_ecc", 0x00000014,  6,  6,  CSR_W1P, 0x00000000 },
  { "irq_clear_err_check_crc", 0x00000014,  7,  7,  CSR_W1P, 0x00000000 },
  { "irq_clear_err_bit_format", 0x00000014,  8,  8,  CSR_W1P, 0x00000000 },
  { "irq_clear_ta_to", 0x00000014,  9,  9,  CSR_W1P, 0x00000000 },
  { "irq_clear_presp_to", 0x00000014, 10, 10,  CSR_W1P, 0x00000000 },
  { "IRQACK", 0x00000014, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD spec
  { "lane_sel", 0x00000018,  1,  0,   CSR_RW, 0x00000000 },
  { "mipi_spec", 0x00000018,  8,  8,   CSR_RW, 0x00000000 },
  { "SPEC", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD fmt
  { "data_type", 0x0000001C,  3,  0,   CSR_RW, 0x00000000 },
  { "FMT", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pix
  { "width", 0x00000020, 15,  0,   CSR_RW, 0x00000000 },
  { "height", 0x00000020, 31, 16,   CSR_RW, 0x00000000 },
  { "PIX", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wc
  { "word_cnt", 0x00000024, 15,  0,   CSR_RW, 0x00000000 },
  { "WC", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dsi
  { "dsi_mode", 0x00000028,  0,  0,   CSR_RW, 0x00000000 },
  { "eotp_en", 0x00000028,  8,  8,   CSR_RW, 0x00000000 },
  { "cm_mode_sel", 0x00000028, 16, 16,   CSR_RW, 0x00000000 },
  { "dis_ecc_crt", 0x00000028, 24, 24,   CSR_RW, 0x00000000 },
  { "DSI", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD csi
  { "line_sync", 0x0000002C,  0,  0,   CSR_RW, 0x00000000 },
  { "CSI", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mipi
  { "clk_mode", 0x00000030,  1,  0,   CSR_RW, 0x00000000 },
  { "clk_freerun", 0x00000030,  8,  8,   CSR_RW, 0x00000000 },
  { "MIPI", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pkt
  { "vc_num", 0x00000034,  1,  0,   CSR_RW, 0x00000000 },
  { "PKT", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD vm0
  { "vm_type", 0x00000038,  1,  0,   CSR_RW, 0x00000000 },
  { "VM0", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD vm1
  { "perd_hsa", 0x0000003C, 11,  0,   CSR_RW, 0x00000000 },
  { "VM1", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD vm2
  { "perd_hbp", 0x00000040, 11,  0,   CSR_RW, 0x00000000 },
  { "VM2", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD vm3
  { "perd_hfp", 0x00000044, 11,  0,   CSR_RW, 0x00000000 },
  { "VM3", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD vm4
  { "line_vsa", 0x00000048, 11,  0,   CSR_RW, 0x00000000 },
  { "VM4", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD vm5
  { "line_vbp", 0x0000004C, 11,  0,   CSR_RW, 0x00000000 },
  { "VM5", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD vm6
  { "line_vfp", 0x00000050, 11,  0,   CSR_RW, 0x00000000 },
  { "VM6", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD vm7
  { "lp_hsa", 0x00000054,  0,  0,   CSR_RW, 0x00000000 },
  { "lp_hbp", 0x00000054,  8,  8,   CSR_RW, 0x00000000 },
  { "lp_hfp", 0x00000054, 16, 16,   CSR_RW, 0x00000000 },
  { "VM7", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cm0
  { "auto_mode", 0x00000060,  0,  0,   CSR_RW, 0x00000000 },
  { "read_cmdq", 0x00000060,  8,  8,   CSR_RW, 0x00000000 },
  { "CM0", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cm1
  { "te_en", 0x00000064,  0,  0,   CSR_RW, 0x00000000 },
  { "te_src", 0x00000064,  9,  8,   CSR_RW, 0x00000000 },
  { "te_detect", 0x00000064, 16, 16,   CSR_RW, 0x00000000 },
  { "CM1", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cm2
  { "te_toggle", 0x00000068,  0,  0,   CSR_RW, 0x00000000 },
  { "te_cmd_header", 0x00000068, 15,  8,   CSR_RW, 0x00000002 },
  { "te_esc_header", 0x00000068, 23, 16,   CSR_RW, 0x000000BA },
  { "CM2", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dcs
  { "dcs_wm_start", 0x00000070,  7,  0,   CSR_RW, 0x0000002C },
  { "dcs_wm_cont", 0x00000070, 15,  8,   CSR_RW, 0x0000003C },
  { "DCS", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // WORD time0
  { "time_lpx", 0x00000080,  7,  0,   CSR_RW, 0x00000000 },
  { "TIME0", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // WORD time1
  { "time_hs_prepare", 0x00000084,  7,  0,   CSR_RW, 0x00000000 },
  { "time_hs_zero", 0x00000084, 15,  8,   CSR_RW, 0x00000000 },
  { "time_hs_trail", 0x00000084, 23, 16,   CSR_RW, 0x00000000 },
  { "time_hs_exit", 0x00000084, 31, 24,   CSR_RW, 0x00000000 },
  { "TIME1", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD time2
  { "time_clk_prepare", 0x00000088,  7,  0,   CSR_RW, 0x00000000 },
  { "time_clk_zero", 0x00000088, 15,  8,   CSR_RW, 0x00000000 },
  { "time_clk_trail", 0x00000088, 23, 16,   CSR_RW, 0x00000000 },
  { "time_clk_exit", 0x00000088, 31, 24,   CSR_RW, 0x00000000 },
  { "TIME2", 0x00000088, 31, 0,   CSR_RW, 0x00000000 },
  // WORD time3
  { "time_clk_pre", 0x0000008C,  7,  0,   CSR_RW, 0x00000000 },
  { "time_clk_post", 0x0000008C, 15,  8,   CSR_RW, 0x00000000 },
  { "TIME3", 0x0000008C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD time4
  { "time_wakeup", 0x00000090, 11,  0,   CSR_RW, 0x00000000 },
  { "time_oe", 0x00000090, 23, 16,   CSR_RW, 0x00000000 },
  { "TIME4", 0x00000090, 31, 0,   CSR_RW, 0x00000000 },
  // WORD time5
  { "time_lp_hsa", 0x00000094, 11,  0,   CSR_RW, 0x00000000 },
  { "time_lp_hbp", 0x00000094, 27, 16,   CSR_RW, 0x00000000 },
  { "TIME5", 0x00000094, 31, 0,   CSR_RW, 0x00000000 },
  // WORD time6
  { "time_lp_hfp", 0x00000098, 11,  0,   CSR_RW, 0x00000000 },
  { "time_frame_blanking", 0x00000098, 27, 16,   CSR_RW, 0x00000001 },
  { "TIME6", 0x00000098, 31, 0,   CSR_RW, 0x00000000 },
  // WORD time7
  { "time_lp_bllp_trail", 0x0000009C, 11,  0,   CSR_RW, 0x00000000 },
  { "time_frame_blanking_max", 0x0000009C, 31, 16,   CSR_RW, 0x00000001 },
  { "TIME7", 0x0000009C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD time8
  { "time_bta_go", 0x000000A0, 11,  0,   CSR_RW, 0x00000028 },
  { "time_bta_sure", 0x000000A0, 27, 16,   CSR_RW, 0x0000000A },
  { "TIME8", 0x000000A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD time9
  { "time_bta_get", 0x000000A4, 11,  0,   CSR_RW, 0x00000032 },
  { "time_bta_go_lprx_en", 0x000000A4, 27, 16,   CSR_RW, 0x0000001E },
  { "TIME9", 0x000000A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gsta
  { "enc_busy", 0x000000A8,  0,  0,   CSR_RO, 0x00000000 },
  { "enc_sta", 0x000000A8, 11,  8,   CSR_RO, 0x00000000 },
  { "GSTA", 0x000000A8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD fcnt
  { "frame_cnt", 0x000000AC, 15,  0,   CSR_RO, 0x00000000 },
  { "FCNT", 0x000000AC, 31, 0,   CSR_RO, 0x00000000 },
  // WORD td0
  { "time_esc_esc", 0x000000B8,  7,  0,   CSR_RW, 0x00000000 },
  { "time_esc_hs", 0x000000B8, 15,  8,   CSR_RW, 0x00000000 },
  { "time_esc_bta", 0x000000B8, 23, 16,   CSR_RW, 0x00000000 },
  { "time_hs_hs", 0x000000B8, 31, 24,   CSR_RW, 0x00000000 },
  { "TD0", 0x000000B8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD td1
  { "time_hs_bta", 0x000000BC,  7,  0,   CSR_RW, 0x00000000 },
  { "time_bta_bta", 0x000000BC, 15,  8,   CSR_RW, 0x00000000 },
  { "TD1", 0x000000BC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD esccmd0
  { "esc_cmd", 0x000000C0,  7,  0,   CSR_RW, 0x00000087 },
  { "ESCCMD0", 0x000000C0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD esccmd1
  { "esc_finish", 0x000000C4,  0,  0,  CSR_W1P, 0x00000000 },
  { "ESCCMD1", 0x000000C4, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD esctime
  { "esc_time_wait", 0x000000C8,  7,  0,   CSR_RW, 0x00000000 },
  { "esc_time_mark1", 0x000000C8, 31, 16,   CSR_RW, 0x00004E20 },
  { "ESCTIME", 0x000000C8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wcmd0
  { "w_cmd_word_0", 0x000000CC, 31,  0,   CSR_RW, 0x00000000 },
  { "WCMD0", 0x000000CC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wcmd1
  { "w_cmd_word_1", 0x000000D0, 31,  0,   CSR_RW, 0x00000000 },
  { "WCMD1", 0x000000D0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wcmd2
  { "w_cmd_word_2", 0x000000D4, 31,  0,   CSR_RW, 0x00000000 },
  { "WCMD2", 0x000000D4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wcmd3
  { "w_cmd_word_3", 0x000000D8, 31,  0,   CSR_RW, 0x00000000 },
  { "WCMD3", 0x000000D8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wcmd4
  { "w_cmd_valid", 0x000000DC,  3,  0,   CSR_RW, 0x00000000 },
  { "w_cmd_byte_valid", 0x000000DC, 11,  8,   CSR_RW, 0x00000000 },
  { "WCMD4", 0x000000DC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wcmd5
  { "w_is_long_packet", 0x000000E0,  0,  0,   CSR_RW, 0x00000000 },
  { "w_packet_conti", 0x000000E0,  8,  8,   CSR_RW, 0x00000000 },
  { "w_cal_ecc", 0x000000E0, 16, 16,   CSR_RW, 0x00000000 },
  { "w_cal_cs", 0x000000E0, 24, 24,   CSR_RW, 0x00000000 },
  { "WCMD5", 0x000000E0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wcmd6
  { "w_cmd_update", 0x000000E4,  0,  0,  CSR_W1P, 0x00000000 },
  { "WCMD6", 0x000000E4, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rcmd0
  { "r_receive_cmd_ready", 0x000000F0,  0,  0,   CSR_RO, 0x00000000 },
  { "RCMD0", 0x000000F0, 31, 0,   CSR_RO, 0x00000000 },
  // WORD rcmd1
  { "r_lprx_mode_valid", 0x000000F4,  0,  0,   CSR_RO, 0x00000000 },
  { "r_lprx_cmd_valid", 0x000000F4,  8,  8,   CSR_RO, 0x00000000 },
  { "RCMD1", 0x000000F4, 31, 0,   CSR_RO, 0x00000000 },
  // WORD rcmd2
  { "r_lprx_mode", 0x000000F8,  7,  0,   CSR_RO, 0x00000000 },
  { "RCMD2", 0x000000F8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD rcmd3
  { "r_lprx_cmd", 0x000000FC, 31,  0,   CSR_RO, 0x00000000 },
  { "RCMD3", 0x000000FC, 31, 0,   CSR_RO, 0x00000000 },
  // WORD rcmd4
  { "w_lprx_cmd_read_done", 0x00000100,  0,  0,  CSR_W1P, 0x00000000 },
  { "w_lprx_mode_read_done", 0x00000100,  8,  8,  CSR_W1P, 0x00000000 },
  { "RCMD4", 0x00000100, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rcmd5
  { "user_def_longid", 0x00000104, 29,  0,   CSR_RW, 0x00000000 },
  { "RCMD5", 0x00000104, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cs0
  { "line_emb", 0x00000120, 11,  0,   CSR_RW, 0x00000000 },
  { "CS0", 0x00000120, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cs1
  { "line_blk", 0x00000124, 11,  0,   CSR_RW, 0x00000000 },
  { "CS1", 0x00000124, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cs2
  { "perd_vbp", 0x00000128, 11,  0,   CSR_RW, 0x00000000 },
  { "CS2", 0x00000128, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cs3
  { "perd_emb", 0x0000012C, 11,  0,   CSR_RW, 0x00000000 },
  { "CS3", 0x0000012C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cs4
  { "perd_blk", 0x00000130, 11,  0,   CSR_RW, 0x00000000 },
  { "CS4", 0x00000130, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cs5
  { "perd_vfp", 0x00000134, 11,  0,   CSR_RW, 0x00000000 },
  { "CS5", 0x00000134, 31, 0,   CSR_RW, 0x00000000 },
  // WORD to0
  { "time_ta_to", 0x00000154, 11,  0,   CSR_RW, 0x00000064 },
  { "TO0", 0x00000154, 31, 0,   CSR_RW, 0x00000000 },
  // WORD to1
  { "time_presp_to_start", 0x00000158,  0,  0,  CSR_W1P, 0x00000000 },
  { "time_presp_to_reset", 0x00000158,  8,  8,  CSR_W1P, 0x00000000 },
  { "TO1", 0x00000158, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD to2
  { "time_presp_to", 0x0000015C, 19,  0,   CSR_RW, 0x00002710 },
  { "time_presp_to_mask", 0x0000015C, 24, 24,   CSR_RW, 0x00000000 },
  { "TO2", 0x0000015C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dbg0
  { "pix_debug_sel", 0x00000164,  3,  0,   CSR_RW, 0x00000000 },
  { "debug_sel", 0x00000164, 13,  8,   CSR_RW, 0x00000000 },
  { "DBG0", 0x00000164, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dbg1
  { "pix_debug_mon", 0x00000168, 31,  0,   CSR_RO, 0x00000000 },
  { "DBG1", 0x00000168, 31, 0,   CSR_RO, 0x00000000 },
  // WORD dbg2
  { "debug_mon", 0x0000016C, 31,  0,   CSR_RO, 0x00000000 },
  { "DBG2", 0x0000016C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD resv
  { "reserved", 0x00000180, 31,  0,   CSR_RW, 0x00000000 },
  { "RESV", 0x00000180, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_TX_ENC_H_
