#ifndef CSR_TABLE_AIOC_H_
#define CSR_TABLE_AIOC_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_aioc[] =
{
  // WORD xin_iocfg
  { "pad_xin_pcfg", 0x00000000, 17, 16,   CSR_RW, 0x00000001 },
  { "XIN_IOCFG", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD resetb_iocfg
  { "pad_resetb_pcfg", 0x00000004, 18, 16,   CSR_RW, 0x00000000 },
  { "RESETB_IOCFG", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD power_ctrl_0_iocfg
  { "pad_power_ctrl_0_pu", 0x00000008,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_power_ctrl_0_pd", 0x00000008,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_power_ctrl_0_pcfg", 0x00000008, 18, 16,   CSR_RW, 0x00000000 },
  { "POWER_CTRL_0_IOCFG", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD power_ctrl_1_iocfg
  { "pad_power_ctrl_1_pu", 0x0000000C,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_power_ctrl_1_pd", 0x0000000C,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_power_ctrl_1_pcfg", 0x0000000C, 18, 16,   CSR_RW, 0x00000000 },
  { "POWER_CTRL_1_IOCFG", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dvp_0_iocfg
  { "pad_dvp_0_pu", 0x00000010,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_dvp_0_pd", 0x00000010,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_dvp_0_pcfg", 0x00000010, 18, 16,   CSR_RW, 0x00000000 },
  { "DVP_0_IOCFG", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dvp_1_iocfg
  { "pad_dvp_1_pu", 0x00000014,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_dvp_1_pd", 0x00000014,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_dvp_1_pcfg", 0x00000014, 18, 16,   CSR_RW, 0x00000000 },
  { "DVP_1_IOCFG", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dvp_2_iocfg
  { "pad_dvp_2_pu", 0x00000018,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_dvp_2_pd", 0x00000018,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_dvp_2_pcfg", 0x00000018, 18, 16,   CSR_RW, 0x00000000 },
  { "DVP_2_IOCFG", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dvp_3_iocfg
  { "pad_dvp_3_pu", 0x0000001C,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_dvp_3_pd", 0x0000001C,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_dvp_3_pcfg", 0x0000001C, 18, 16,   CSR_RW, 0x00000000 },
  { "DVP_3_IOCFG", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dvp_4_iocfg
  { "pad_dvp_4_pu", 0x00000020,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_dvp_4_pd", 0x00000020,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_dvp_4_pcfg", 0x00000020, 18, 16,   CSR_RW, 0x00000000 },
  { "DVP_4_IOCFG", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dvp_5_iocfg
  { "pad_dvp_5_pu", 0x00000024,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_dvp_5_pd", 0x00000024,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_dvp_5_pcfg", 0x00000024, 18, 16,   CSR_RW, 0x00000000 },
  { "DVP_5_IOCFG", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dvp_6_iocfg
  { "pad_dvp_6_pu", 0x00000028,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_dvp_6_pd", 0x00000028,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_dvp_6_pcfg", 0x00000028, 18, 16,   CSR_RW, 0x00000000 },
  { "DVP_6_IOCFG", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dvp_7_iocfg
  { "pad_dvp_7_pu", 0x0000002C,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_dvp_7_pd", 0x0000002C,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_dvp_7_pcfg", 0x0000002C, 18, 16,   CSR_RW, 0x00000000 },
  { "DVP_7_IOCFG", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dvp_8_iocfg
  { "pad_dvp_8_pu", 0x00000030,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_dvp_8_pd", 0x00000030,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_dvp_8_pcfg", 0x00000030, 18, 16,   CSR_RW, 0x00000000 },
  { "DVP_8_IOCFG", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ao_button_iocfg
  { "pad_ao_button_pu", 0x00000034,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_ao_button_pd", 0x00000034,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_ao_button_pcfg", 0x00000034, 18, 16,   CSR_RW, 0x00000000 },
  { "AO_BUTTON_IOCFG", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ao_wakeup_iocfg
  { "pad_ao_wakeup_pu", 0x00000038,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_ao_wakeup_pd", 0x00000038,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_ao_wakeup_pcfg", 0x00000038, 18, 16,   CSR_RW, 0x00000000 },
  { "AO_WAKEUP_IOCFG", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD power_ctrl_0_iosel
  { "pad_power_ctrl_0_iosel", 0x0000003C,  1,  0,   CSR_RW, 0x00000001 },
  { "POWER_CTRL_0_IOSEL", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD power_ctrl_1_iosel
  { "pad_power_ctrl_1_iosel", 0x00000040,  1,  0,   CSR_RW, 0x00000001 },
  { "POWER_CTRL_1_IOSEL", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dvp_0_iosel
  { "pad_dvp_0_iosel", 0x00000044,  1,  0,   CSR_RW, 0x00000000 },
  { "DVP_0_IOSEL", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dvp_1_iosel
  { "pad_dvp_1_iosel", 0x00000048,  1,  0,   CSR_RW, 0x00000000 },
  { "DVP_1_IOSEL", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dvp_2_iosel
  { "pad_dvp_2_iosel", 0x0000004C,  1,  0,   CSR_RW, 0x00000000 },
  { "DVP_2_IOSEL", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dvp_3_iosel
  { "pad_dvp_3_iosel", 0x00000050,  1,  0,   CSR_RW, 0x00000000 },
  { "DVP_3_IOSEL", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dvp_4_iosel
  { "pad_dvp_4_iosel", 0x00000054,  1,  0,   CSR_RW, 0x00000000 },
  { "DVP_4_IOSEL", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dvp_5_iosel
  { "pad_dvp_5_iosel", 0x00000058,  1,  0,   CSR_RW, 0x00000000 },
  { "DVP_5_IOSEL", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dvp_6_iosel
  { "pad_dvp_6_iosel", 0x0000005C,  1,  0,   CSR_RW, 0x00000000 },
  { "DVP_6_IOSEL", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dvp_7_iosel
  { "pad_dvp_7_iosel", 0x00000060,  1,  0,   CSR_RW, 0x00000000 },
  { "DVP_7_IOSEL", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dvp_8_iosel
  { "pad_dvp_8_iosel", 0x00000064,  1,  0,   CSR_RW, 0x00000000 },
  { "DVP_8_IOSEL", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ao_button_iosel
  { "pad_ao_button_iosel", 0x00000068,  1,  0,   CSR_RW, 0x00000000 },
  { "AO_BUTTON_IOSEL", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ao_wakeup_iosel
  { "pad_ao_wakeup_iosel", 0x0000006C,  1,  0,   CSR_RW, 0x00000000 },
  { "AO_WAKEUP_IOSEL", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_tst_sel
  { "tst_sel", 0x00000070,  1,  0,   CSR_RW, 0x00000000 },
  { "WORD_TST_SEL", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_AIOC_H_
