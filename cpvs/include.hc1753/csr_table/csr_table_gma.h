#ifndef CSR_TABLE_GMA_H_
#define CSR_TABLE_GMA_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_gma[] =
{
  // WORD word_mode
  { "mode", 0x00000000,  0,  0,   CSR_RW, 0x00000000 },
  { "WORD_MODE", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_0
  { "gamma_curve_1", 0x00000004, 13,  0,   CSR_RW, 0x00000120 },
  { "gamma_curve_2", 0x00000004, 29, 16,   CSR_RW, 0x00000240 },
  { "GAMMA_CURVE_0", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_1
  { "gamma_curve_3", 0x00000008, 13,  0,   CSR_RW, 0x00000360 },
  { "gamma_curve_4", 0x00000008, 29, 16,   CSR_RW, 0x00000480 },
  { "GAMMA_CURVE_1", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_2
  { "gamma_curve_5", 0x0000000C, 13,  0,   CSR_RW, 0x000005A0 },
  { "gamma_curve_6", 0x0000000C, 29, 16,   CSR_RW, 0x000006A8 },
  { "GAMMA_CURVE_2", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_3
  { "gamma_curve_7", 0x00000010, 13,  0,   CSR_RW, 0x00000798 },
  { "gamma_curve_8", 0x00000010, 29, 16,   CSR_RW, 0x00000874 },
  { "GAMMA_CURVE_3", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_4
  { "gamma_curve_9", 0x00000014, 13,  0,   CSR_RW, 0x00000A04 },
  { "gamma_curve_10", 0x00000014, 29, 16,   CSR_RW, 0x00000B68 },
  { "GAMMA_CURVE_4", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_5
  { "gamma_curve_11", 0x00000018, 13,  0,   CSR_RW, 0x00000CB0 },
  { "gamma_curve_12", 0x00000018, 29, 16,   CSR_RW, 0x00000DDC },
  { "GAMMA_CURVE_5", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_6
  { "gamma_curve_13", 0x0000001C, 13,  0,   CSR_RW, 0x00000EF8 },
  { "gamma_curve_14", 0x0000001C, 29, 16,   CSR_RW, 0x00001000 },
  { "GAMMA_CURVE_6", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_7
  { "gamma_curve_15", 0x00000020, 13,  0,   CSR_RW, 0x000010F8 },
  { "gamma_curve_16", 0x00000020, 29, 16,   CSR_RW, 0x000011E8 },
  { "GAMMA_CURVE_7", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_8
  { "gamma_curve_17", 0x00000024, 13,  0,   CSR_RW, 0x000012CC },
  { "gamma_curve_18", 0x00000024, 29, 16,   CSR_RW, 0x000013A8 },
  { "GAMMA_CURVE_8", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_9
  { "gamma_curve_19", 0x00000028, 13,  0,   CSR_RW, 0x00001478 },
  { "gamma_curve_20", 0x00000028, 29, 16,   CSR_RW, 0x00001544 },
  { "GAMMA_CURVE_9", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_10
  { "gamma_curve_21", 0x0000002C, 13,  0,   CSR_RW, 0x00001600 },
  { "gamma_curve_22", 0x0000002C, 29, 16,   CSR_RW, 0x000016C4 },
  { "GAMMA_CURVE_10", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_11
  { "gamma_curve_23", 0x00000030, 13,  0,   CSR_RW, 0x00001770 },
  { "gamma_curve_24", 0x00000030, 29, 16,   CSR_RW, 0x0000182C },
  { "GAMMA_CURVE_11", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_12
  { "gamma_curve_25", 0x00000034, 13,  0,   CSR_RW, 0x00001984 },
  { "gamma_curve_26", 0x00000034, 29, 16,   CSR_RW, 0x00001AC8 },
  { "GAMMA_CURVE_12", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_13
  { "gamma_curve_27", 0x00000038, 13,  0,   CSR_RW, 0x00001C00 },
  { "gamma_curve_28", 0x00000038, 29, 16,   CSR_RW, 0x00001D28 },
  { "GAMMA_CURVE_13", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_14
  { "gamma_curve_29", 0x0000003C, 13,  0,   CSR_RW, 0x00001E48 },
  { "gamma_curve_30", 0x0000003C, 29, 16,   CSR_RW, 0x00001F5C },
  { "GAMMA_CURVE_14", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_15
  { "gamma_curve_31", 0x00000040, 13,  0,   CSR_RW, 0x00002060 },
  { "gamma_curve_32", 0x00000040, 29, 16,   CSR_RW, 0x00002168 },
  { "GAMMA_CURVE_15", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_16
  { "gamma_curve_33", 0x00000044, 13,  0,   CSR_RW, 0x00002260 },
  { "gamma_curve_34", 0x00000044, 29, 16,   CSR_RW, 0x00002358 },
  { "GAMMA_CURVE_16", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_17
  { "gamma_curve_35", 0x00000048, 13,  0,   CSR_RW, 0x00002440 },
  { "gamma_curve_36", 0x00000048, 29, 16,   CSR_RW, 0x0000252C },
  { "GAMMA_CURVE_17", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_18
  { "gamma_curve_37", 0x0000004C, 13,  0,   CSR_RW, 0x0000260C },
  { "gamma_curve_38", 0x0000004C, 29, 16,   CSR_RW, 0x000026E8 },
  { "GAMMA_CURVE_18", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_19
  { "gamma_curve_39", 0x00000050, 13,  0,   CSR_RW, 0x000027C0 },
  { "gamma_curve_40", 0x00000050, 29, 16,   CSR_RW, 0x00002890 },
  { "GAMMA_CURVE_19", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_20
  { "gamma_curve_41", 0x00000054, 13,  0,   CSR_RW, 0x00002A28 },
  { "gamma_curve_42", 0x00000054, 29, 16,   CSR_RW, 0x00002BB0 },
  { "GAMMA_CURVE_20", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_21
  { "gamma_curve_43", 0x00000058, 13,  0,   CSR_RW, 0x00002D28 },
  { "gamma_curve_44", 0x00000058, 29, 16,   CSR_RW, 0x00002E90 },
  { "GAMMA_CURVE_21", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_22
  { "gamma_curve_45", 0x0000005C, 13,  0,   CSR_RW, 0x00002FF4 },
  { "gamma_curve_46", 0x0000005C, 29, 16,   CSR_RW, 0x00003140 },
  { "GAMMA_CURVE_22", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_23
  { "gamma_curve_47", 0x00000060, 13,  0,   CSR_RW, 0x00003298 },
  { "gamma_curve_48", 0x00000060, 29, 16,   CSR_RW, 0x000033D4 },
  { "GAMMA_CURVE_23", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_24
  { "gamma_curve_49", 0x00000064, 13,  0,   CSR_RW, 0x00003514 },
  { "gamma_curve_50", 0x00000064, 29, 16,   CSR_RW, 0x00003640 },
  { "GAMMA_CURVE_24", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_25
  { "gamma_curve_51", 0x00000068, 13,  0,   CSR_RW, 0x00003774 },
  { "gamma_curve_52", 0x00000068, 29, 16,   CSR_RW, 0x00003894 },
  { "GAMMA_CURVE_25", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_26
  { "gamma_curve_53", 0x0000006C, 13,  0,   CSR_RW, 0x000039B8 },
  { "gamma_curve_54", 0x0000006C, 29, 16,   CSR_RW, 0x00003AD0 },
  { "GAMMA_CURVE_26", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_27
  { "gamma_curve_55", 0x00000070, 13,  0,   CSR_RW, 0x00003BE4 },
  { "gamma_curve_56", 0x00000070, 29, 16,   CSR_RW, 0x00003CF0 },
  { "GAMMA_CURVE_27", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gamma_curve_28
  { "gamma_curve_57", 0x00000074, 13,  0,   CSR_RW, 0x00003DFC },
  { "gamma_curve_58", 0x00000074, 29, 16,   CSR_RW, 0x00003F00 },
  { "GAMMA_CURVE_28", 0x00000074, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_debug_mon_sel
  { "debug_mon_sel", 0x00000078,  0,  0,   CSR_RW, 0x00000000 },
  { "WORD_DEBUG_MON_SEL", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_GMA_H_
