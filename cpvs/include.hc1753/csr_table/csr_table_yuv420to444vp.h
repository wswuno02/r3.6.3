#ifndef CSR_TABLE_YUV420TO444VP_H_
#define CSR_TABLE_YUV420TO444VP_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_yuv420to444vp[] =
{
  // WORD frame_st
  { "frame_start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "FRAME_ST", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD irq_status
  { "status_frame_end", 0x00000004,  0,  0,   CSR_RO, 0x00000000 },
  { "IRQ_STATUS", 0x00000004, 31, 0,   CSR_RO, 0x00000000 },
  // WORD irq_clr
  { "irq_clear_frame_end", 0x00000008,  0,  0,  CSR_W1P, 0x00000000 },
  { "IRQ_CLR", 0x00000008, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD irq_mask
  { "irq_mask_frame_end", 0x0000000C,  0,  0,   CSR_RW, 0x00000001 },
  { "IRQ_MASK", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD resolution
  { "width", 0x00000010, 15,  0,   CSR_RW, 0x00000780 },
  { "height", 0x00000010, 31, 16,   CSR_RW, 0x00000438 },
  { "RESOLUTION", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dbg_mon_sel
  { "debug_mon_sel", 0x00000014,  1,  0,   CSR_RW, 0x00000000 },
  { "DBG_MON_SEL", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_YUV420TO444VP_H_
