#ifndef CSR_TABLE_HDR_H_
#define CSR_TABLE_HDR_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_hdr[] =
{
  // WORD hdr00
  { "frame_start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "HDR00", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD hdr01
  { "irq_clear_frame_end", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "HDR01", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD hdr02
  { "status_frame_end", 0x00000008,  0,  0,   CSR_RO, 0x00000000 },
  { "HDR02", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // WORD hdr03
  { "irq_mask_frame_end", 0x0000000C,  0,  0,   CSR_RW, 0x00000001 },
  { "HDR03", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr05
  { "width", 0x00000014, 15,  0,   CSR_RW, 0x00000100 },
  { "height", 0x00000014, 31, 16,   CSR_RW, 0x00000438 },
  { "HDR05", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr06
  { "mode", 0x00000018,  1,  0,   CSR_RW, 0x00000000 },
  { "bayer_ini_phase_i", 0x00000018,  9,  8,   CSR_RW, 0x00000000 },
  { "exp_long_early", 0x00000018, 16, 16,   CSR_RW, 0x00000000 },
  { "hdr_en", 0x00000018, 24, 24,   CSR_RW, 0x00000000 },
  { "HDR06", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr07
  { "exp_ratio", 0x0000001C,  8,  0,   CSR_RW, 0x00000010 },
  { "exp_ratio_inv", 0x0000001C, 24, 16,   CSR_RW, 0x00000100 },
  { "HDR07", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr08
  { "se_anti_gamma_mode", 0x00000020,  0,  0,   CSR_RW, 0x00000000 },
  { "le_anti_gamma_mode", 0x00000020,  8,  8,   CSR_RW, 0x00000000 },
  { "HDR08", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr09
  { "se_weight_th_min", 0x00000024, 12,  0,   CSR_RW, 0x00000108 },
  { "se_weight_slope", 0x00000024, 22, 16,   CSR_RW, 0x00000038 },
  { "HDR09", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr10
  { "se_weight_min", 0x00000028,  5,  0,   CSR_RW, 0x00000001 },
  { "se_weight_max", 0x00000028, 21, 16,   CSR_RW, 0x0000003F },
  { "HDR10", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr11
  { "le_weight_th_max", 0x0000002C, 12,  0,   CSR_RW, 0x00000F00 },
  { "le_weight_slope", 0x0000002C, 22, 16,   CSR_RW, 0x0000000E },
  { "HDR11", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr12
  { "le_weight_min", 0x00000030,  5,  0,   CSR_RW, 0x00000001 },
  { "le_weight_max", 0x00000030, 21, 16,   CSR_RW, 0x0000003F },
  { "HDR12", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr13
  { "mismatch_var_gain_2s", 0x00000034,  5,  0,   CSR_RW, 0x00000008 },
  { "HDR13", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr14
  { "local_fb_th", 0x00000038, 11,  0,   CSR_RW, 0x00000180 },
  { "local_fb_slope", 0x00000038, 20, 16,   CSR_RW, 0x00000007 },
  { "HDR14", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr15
  { "local_fb_min", 0x0000003C,  4,  0,   CSR_RW, 0x00000000 },
  { "local_fb_max", 0x0000003C, 20, 16,   CSR_RW, 0x00000010 },
  { "HDR15", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr16
  { "frame_fb_strength", 0x00000040,  4,  0,   CSR_RW, 0x00000000 },
  { "fb_target", 0x00000040,  8,  8,   CSR_RW, 0x00000000 },
  { "fb_alpha", 0x00000040, 22, 16,   CSR_RW, 0x00000000 },
  { "HDR16", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr17
  { "se_exp_ratio_min_int", 0x00000044,  4,  0,   CSR_RW, 0x0000000B },
  { "tm_en", 0x00000044,  8,  8,   CSR_RW, 0x00000001 },
  { "HDR17", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr18
  { "tone_curve_1", 0x00000048, 13,  0,   CSR_RW, 0x00000120 },
  { "tone_curve_2", 0x00000048, 29, 16,   CSR_RW, 0x00000240 },
  { "HDR18", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr19
  { "tone_curve_3", 0x0000004C, 13,  0,   CSR_RW, 0x00000360 },
  { "tone_curve_4", 0x0000004C, 29, 16,   CSR_RW, 0x00000480 },
  { "HDR19", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr20
  { "tone_curve_5", 0x00000050, 13,  0,   CSR_RW, 0x000005A2 },
  { "tone_curve_6", 0x00000050, 29, 16,   CSR_RW, 0x000006A8 },
  { "HDR20", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr21
  { "tone_curve_7", 0x00000054, 13,  0,   CSR_RW, 0x00000797 },
  { "tone_curve_8", 0x00000054, 29, 16,   CSR_RW, 0x00000873 },
  { "HDR21", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr22
  { "tone_curve_9", 0x00000058, 13,  0,   CSR_RW, 0x00000A03 },
  { "tone_curve_10", 0x00000058, 29, 16,   CSR_RW, 0x00000B69 },
  { "HDR22", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr23
  { "tone_curve_11", 0x0000005C, 13,  0,   CSR_RW, 0x00000CAF },
  { "tone_curve_12", 0x0000005C, 29, 16,   CSR_RW, 0x00000DDD },
  { "HDR23", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr24
  { "tone_curve_13", 0x00000060, 13,  0,   CSR_RW, 0x00000EF6 },
  { "tone_curve_14", 0x00000060, 29, 16,   CSR_RW, 0x00000FFF },
  { "HDR24", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr25
  { "tone_curve_15", 0x00000064, 13,  0,   CSR_RW, 0x000010FA },
  { "tone_curve_16", 0x00000064, 29, 16,   CSR_RW, 0x000011E8 },
  { "HDR25", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr26
  { "tone_curve_17", 0x00000068, 13,  0,   CSR_RW, 0x000012CC },
  { "tone_curve_18", 0x00000068, 29, 16,   CSR_RW, 0x000013A6 },
  { "HDR26", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr27
  { "tone_curve_19", 0x0000006C, 13,  0,   CSR_RW, 0x00001478 },
  { "tone_curve_20", 0x0000006C, 29, 16,   CSR_RW, 0x00001542 },
  { "HDR27", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr28
  { "tone_curve_21", 0x00000070, 13,  0,   CSR_RW, 0x00001605 },
  { "tone_curve_22", 0x00000070, 29, 16,   CSR_RW, 0x000016C2 },
  { "HDR28", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr29
  { "tone_curve_23", 0x00000074, 13,  0,   CSR_RW, 0x0000177A },
  { "tone_curve_24", 0x00000074, 29, 16,   CSR_RW, 0x0000182C },
  { "HDR29", 0x00000074, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr30
  { "tone_curve_25", 0x00000078, 13,  0,   CSR_RW, 0x00001982 },
  { "tone_curve_26", 0x00000078, 29, 16,   CSR_RW, 0x00001AC8 },
  { "HDR30", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr31
  { "tone_curve_27", 0x0000007C, 13,  0,   CSR_RW, 0x00001BFF },
  { "tone_curve_28", 0x0000007C, 29, 16,   CSR_RW, 0x00001D29 },
  { "HDR31", 0x0000007C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr32
  { "tone_curve_29", 0x00000080, 13,  0,   CSR_RW, 0x00001E47 },
  { "tone_curve_30", 0x00000080, 29, 16,   CSR_RW, 0x00001F5B },
  { "HDR32", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr33
  { "tone_curve_31", 0x00000084, 13,  0,   CSR_RW, 0x00002066 },
  { "tone_curve_32", 0x00000084, 29, 16,   CSR_RW, 0x00002169 },
  { "HDR33", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr34
  { "tone_curve_33", 0x00000088, 13,  0,   CSR_RW, 0x00002263 },
  { "tone_curve_34", 0x00000088, 29, 16,   CSR_RW, 0x00002357 },
  { "HDR34", 0x00000088, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr35
  { "tone_curve_35", 0x0000008C, 13,  0,   CSR_RW, 0x00002443 },
  { "tone_curve_36", 0x0000008C, 29, 16,   CSR_RW, 0x0000252A },
  { "HDR35", 0x0000008C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr36
  { "tone_curve_37", 0x00000090, 13,  0,   CSR_RW, 0x0000260B },
  { "tone_curve_38", 0x00000090, 29, 16,   CSR_RW, 0x000026E7 },
  { "HDR36", 0x00000090, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr37
  { "tone_curve_39", 0x00000094, 13,  0,   CSR_RW, 0x000027BD },
  { "tone_curve_40", 0x00000094, 29, 16,   CSR_RW, 0x0000288F },
  { "HDR37", 0x00000094, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr38
  { "tone_curve_41", 0x00000098, 13,  0,   CSR_RW, 0x00002A27 },
  { "tone_curve_42", 0x00000098, 29, 16,   CSR_RW, 0x00002BAE },
  { "HDR38", 0x00000098, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr39
  { "tone_curve_43", 0x0000009C, 13,  0,   CSR_RW, 0x00002D27 },
  { "tone_curve_44", 0x0000009C, 29, 16,   CSR_RW, 0x00002E94 },
  { "HDR39", 0x0000009C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr40
  { "tone_curve_45", 0x000000A0, 13,  0,   CSR_RW, 0x00002FF5 },
  { "tone_curve_46", 0x000000A0, 29, 16,   CSR_RW, 0x0000314B },
  { "HDR40", 0x000000A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr41
  { "tone_curve_47", 0x000000A4, 13,  0,   CSR_RW, 0x00003298 },
  { "tone_curve_48", 0x000000A4, 29, 16,   CSR_RW, 0x000033DB },
  { "HDR41", 0x000000A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr42
  { "tone_curve_49", 0x000000A8, 13,  0,   CSR_RW, 0x00003516 },
  { "tone_curve_50", 0x000000A8, 29, 16,   CSR_RW, 0x0000364A },
  { "HDR42", 0x000000A8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr43
  { "tone_curve_51", 0x000000AC, 13,  0,   CSR_RW, 0x00003776 },
  { "tone_curve_52", 0x000000AC, 29, 16,   CSR_RW, 0x0000389B },
  { "HDR43", 0x000000AC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr44
  { "tone_curve_53", 0x000000B0, 13,  0,   CSR_RW, 0x000039BA },
  { "tone_curve_54", 0x000000B0, 29, 16,   CSR_RW, 0x00003AD3 },
  { "HDR44", 0x000000B0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr45
  { "tone_curve_55", 0x000000B4, 13,  0,   CSR_RW, 0x00003BE6 },
  { "tone_curve_56", 0x000000B4, 29, 16,   CSR_RW, 0x00003CF4 },
  { "HDR45", 0x000000B4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr46
  { "tone_curve_57", 0x000000B8, 13,  0,   CSR_RW, 0x00003DFD },
  { "tone_curve_58", 0x000000B8, 29, 16,   CSR_RW, 0x00003F01 },
  { "HDR46", 0x000000B8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr47
  { "awb_gain_0", 0x000000BC, 11,  0,   CSR_RW, 0x00000100 },
  { "awb_gain_1", 0x000000BC, 27, 16,   CSR_RW, 0x00000100 },
  { "HDR47", 0x000000BC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr48
  { "awb_gain_2", 0x000000C0, 11,  0,   CSR_RW, 0x00000100 },
  { "awb_gain_3", 0x000000C0, 27, 16,   CSR_RW, 0x00000100 },
  { "HDR48", 0x000000C0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr49
  { "gamma_en", 0x000000C4,  0,  0,   CSR_RW, 0x00000000 },
  { "HDR49", 0x000000C4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr50
  { "efuse_dis_hdr_violation", 0x000000C8,  0,  0,   CSR_RO, 0x00000000 },
  { "HDR50", 0x000000C8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD hdr51
  { "reserved_0", 0x000000CC, 31,  0,   CSR_RW, 0x00000000 },
  { "HDR51", 0x000000CC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr52
  { "reserved_1", 0x000000D0, 31,  0,   CSR_RW, 0x00000000 },
  { "HDR52", 0x000000D0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD hdr53
  { "debug_mon_sel", 0x000000D4,  1,  0,   CSR_RW, 0x00000000 },
  { "HDR53", 0x000000D4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD awb_en
  { "awb_enable", 0x000000D8,  0,  0,   CSR_RW, 0x00000001 },
  { "AWB_EN", 0x000000D8, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_HDR_H_
