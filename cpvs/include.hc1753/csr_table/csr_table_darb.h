#ifndef CSR_TABLE_DARB_H_
#define CSR_TABLE_DARB_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_darb[] =
{
  // WORD chksum_clear
  { "checksum_0_clear", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "checksum_1_clear", 0x00000000,  8,  8,  CSR_W1P, 0x00000000 },
  { "checksum_2_clear", 0x00000000, 16, 16,  CSR_W1P, 0x00000000 },
  { "checksum_3_clear", 0x00000000, 24, 24,  CSR_W1P, 0x00000000 },
  { "CHKSUM_CLEAR", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD irq_clear_a
  { "irq_clear_w_resp_error", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "irq_clear_r_resp_error", 0x00000004,  8,  8,  CSR_W1P, 0x00000000 },
  { "irq_clear_timeout_w_addr", 0x00000004, 16, 16,  CSR_W1P, 0x00000000 },
  { "irq_clear_timeout_w_data", 0x00000004, 24, 24,  CSR_W1P, 0x00000000 },
  { "IRQ_CLEAR_A", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD irq_clear_b
  { "irq_clear_timeout_w_resp", 0x00000008,  0,  0,  CSR_W1P, 0x00000000 },
  { "irq_clear_timeout_r_addr", 0x00000008,  8,  8,  CSR_W1P, 0x00000000 },
  { "irq_clear_timeout_r_data", 0x00000008, 16, 16,  CSR_W1P, 0x00000000 },
  { "IRQ_CLEAR_B", 0x00000008, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD status_a
  { "status_w_resp_error", 0x0000000C,  0,  0,   CSR_RO, 0x00000000 },
  { "status_r_resp_error", 0x0000000C,  8,  8,   CSR_RO, 0x00000000 },
  { "status_timeout_w_addr", 0x0000000C, 16, 16,   CSR_RO, 0x00000000 },
  { "status_timeout_w_data", 0x0000000C, 24, 24,   CSR_RO, 0x00000000 },
  { "STATUS_A", 0x0000000C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD status_b
  { "status_timeout_w_resp", 0x00000010,  0,  0,   CSR_RO, 0x00000000 },
  { "status_timeout_r_addr", 0x00000010,  8,  8,   CSR_RO, 0x00000000 },
  { "status_timeout_r_data", 0x00000010, 16, 16,   CSR_RO, 0x00000000 },
  { "STATUS_B", 0x00000010, 31, 0,   CSR_RO, 0x00000000 },
  // WORD irq_mask_a
  { "irq_mask_w_resp_error", 0x00000014,  0,  0,   CSR_RW, 0x00000001 },
  { "irq_mask_r_resp_error", 0x00000014,  8,  8,   CSR_RW, 0x00000001 },
  { "irq_mask_timeout_w_addr", 0x00000014, 16, 16,   CSR_RW, 0x00000001 },
  { "irq_mask_timeout_w_data", 0x00000014, 24, 24,   CSR_RW, 0x00000001 },
  { "IRQ_MASK_A", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD irq_mask_b
  { "irq_mask_timeout_w_resp", 0x00000018,  0,  0,   CSR_RW, 0x00000001 },
  { "irq_mask_timeout_r_addr", 0x00000018,  8,  8,   CSR_RW, 0x00000001 },
  { "irq_mask_timeout_r_data", 0x00000018, 16, 16,   CSR_RW, 0x00000001 },
  { "IRQ_MASK_B", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD priority_w_0
  { "priority_w_00", 0x0000001C,  1,  0,   CSR_RW, 0x00000000 },
  { "priority_w_01", 0x0000001C,  9,  8,   CSR_RW, 0x00000000 },
  { "priority_w_02", 0x0000001C, 17, 16,   CSR_RW, 0x00000000 },
  { "priority_w_03", 0x0000001C, 25, 24,   CSR_RW, 0x00000000 },
  { "PRIORITY_W_0", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD priority_w_1
  { "priority_w_04", 0x00000020,  1,  0,   CSR_RW, 0x00000000 },
  { "priority_w_05", 0x00000020,  9,  8,   CSR_RW, 0x00000000 },
  { "priority_w_06", 0x00000020, 17, 16,   CSR_RW, 0x00000000 },
  { "priority_w_07", 0x00000020, 25, 24,   CSR_RW, 0x00000000 },
  { "PRIORITY_W_1", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD priority_w_2
  { "priority_w_08", 0x00000024,  1,  0,   CSR_RW, 0x00000000 },
  { "priority_w_09", 0x00000024,  9,  8,   CSR_RW, 0x00000000 },
  { "priority_w_10", 0x00000024, 17, 16,   CSR_RW, 0x00000000 },
  { "priority_w_11", 0x00000024, 25, 24,   CSR_RW, 0x00000000 },
  { "PRIORITY_W_2", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD priority_w_3
  { "priority_w_12", 0x00000028,  1,  0,   CSR_RW, 0x00000000 },
  { "priority_w_13", 0x00000028,  9,  8,   CSR_RW, 0x00000000 },
  { "priority_w_14", 0x00000028, 17, 16,   CSR_RW, 0x00000000 },
  { "priority_w_15", 0x00000028, 25, 24,   CSR_RW, 0x00000000 },
  { "PRIORITY_W_3", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD priority_w_4
  { "priority_w_16", 0x0000002C,  1,  0,   CSR_RW, 0x00000000 },
  { "priority_w_17", 0x0000002C,  9,  8,   CSR_RW, 0x00000000 },
  { "priority_w_18", 0x0000002C, 17, 16,   CSR_RW, 0x00000000 },
  { "priority_w_19", 0x0000002C, 25, 24,   CSR_RW, 0x00000000 },
  { "PRIORITY_W_4", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD priority_w_5
  { "priority_w_20", 0x00000030,  1,  0,   CSR_RW, 0x00000000 },
  { "priority_w_21", 0x00000030,  9,  8,   CSR_RW, 0x00000000 },
  { "priority_w_22", 0x00000030, 17, 16,   CSR_RW, 0x00000000 },
  { "priority_w_23", 0x00000030, 25, 24,   CSR_RW, 0x00000000 },
  { "PRIORITY_W_5", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD priority_w_6
  { "priority_w_24", 0x00000034,  1,  0,   CSR_RW, 0x00000000 },
  { "priority_w_25", 0x00000034,  9,  8,   CSR_RW, 0x00000000 },
  { "priority_w_26", 0x00000034, 17, 16,   CSR_RW, 0x00000000 },
  { "priority_w_27", 0x00000034, 25, 24,   CSR_RW, 0x00000000 },
  { "PRIORITY_W_6", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD priority_w_7
  { "priority_w_28", 0x00000038,  1,  0,   CSR_RW, 0x00000000 },
  { "priority_w_29", 0x00000038,  9,  8,   CSR_RW, 0x00000000 },
  { "priority_w_30", 0x00000038, 17, 16,   CSR_RW, 0x00000000 },
  { "priority_w_31", 0x00000038, 25, 24,   CSR_RW, 0x00000000 },
  { "PRIORITY_W_7", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD priority_r_0
  { "priority_r_00", 0x0000003C,  1,  0,   CSR_RW, 0x00000000 },
  { "priority_r_01", 0x0000003C,  9,  8,   CSR_RW, 0x00000000 },
  { "priority_r_02", 0x0000003C, 17, 16,   CSR_RW, 0x00000000 },
  { "priority_r_03", 0x0000003C, 25, 24,   CSR_RW, 0x00000000 },
  { "PRIORITY_R_0", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD priority_r_1
  { "priority_r_04", 0x00000040,  1,  0,   CSR_RW, 0x00000000 },
  { "priority_r_05", 0x00000040,  9,  8,   CSR_RW, 0x00000000 },
  { "priority_r_06", 0x00000040, 17, 16,   CSR_RW, 0x00000000 },
  { "priority_r_07", 0x00000040, 25, 24,   CSR_RW, 0x00000000 },
  { "PRIORITY_R_1", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD priority_r_2
  { "priority_r_08", 0x00000044,  1,  0,   CSR_RW, 0x00000000 },
  { "priority_r_09", 0x00000044,  9,  8,   CSR_RW, 0x00000000 },
  { "priority_r_10", 0x00000044, 17, 16,   CSR_RW, 0x00000000 },
  { "priority_r_11", 0x00000044, 25, 24,   CSR_RW, 0x00000000 },
  { "PRIORITY_R_2", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD priority_r_3
  { "priority_r_12", 0x00000048,  1,  0,   CSR_RW, 0x00000000 },
  { "priority_r_13", 0x00000048,  9,  8,   CSR_RW, 0x00000000 },
  { "priority_r_14", 0x00000048, 17, 16,   CSR_RW, 0x00000000 },
  { "priority_r_15", 0x00000048, 25, 24,   CSR_RW, 0x00000000 },
  { "PRIORITY_R_3", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD priority_r_4
  { "priority_r_16", 0x0000004C,  1,  0,   CSR_RW, 0x00000000 },
  { "priority_r_17", 0x0000004C,  9,  8,   CSR_RW, 0x00000000 },
  { "priority_r_18", 0x0000004C, 17, 16,   CSR_RW, 0x00000000 },
  { "priority_r_19", 0x0000004C, 25, 24,   CSR_RW, 0x00000000 },
  { "PRIORITY_R_4", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD priority_r_5
  { "priority_r_20", 0x00000050,  1,  0,   CSR_RW, 0x00000000 },
  { "priority_r_21", 0x00000050,  9,  8,   CSR_RW, 0x00000000 },
  { "priority_r_22", 0x00000050, 17, 16,   CSR_RW, 0x00000000 },
  { "priority_r_23", 0x00000050, 25, 24,   CSR_RW, 0x00000000 },
  { "PRIORITY_R_5", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD priority_r_6
  { "priority_r_24", 0x00000054,  1,  0,   CSR_RW, 0x00000000 },
  { "priority_r_25", 0x00000054,  9,  8,   CSR_RW, 0x00000000 },
  { "priority_r_26", 0x00000054, 17, 16,   CSR_RW, 0x00000000 },
  { "priority_r_27", 0x00000054, 25, 24,   CSR_RW, 0x00000000 },
  { "PRIORITY_R_6", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD priority_r_7
  { "priority_r_28", 0x00000058,  1,  0,   CSR_RW, 0x00000000 },
  { "priority_r_29", 0x00000058,  9,  8,   CSR_RW, 0x00000000 },
  { "priority_r_30", 0x00000058, 17, 16,   CSR_RW, 0x00000000 },
  { "priority_r_31", 0x00000058, 25, 24,   CSR_RW, 0x00000000 },
  { "PRIORITY_R_7", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ini_score_w_0
  { "ini_score_w_00", 0x0000005C,  4,  0,   CSR_RW, 0x00000000 },
  { "ini_score_w_01", 0x0000005C, 12,  8,   CSR_RW, 0x00000000 },
  { "ini_score_w_02", 0x0000005C, 20, 16,   CSR_RW, 0x00000000 },
  { "ini_score_w_03", 0x0000005C, 28, 24,   CSR_RW, 0x00000000 },
  { "INI_SCORE_W_0", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ini_score_w_1
  { "ini_score_w_04", 0x00000060,  4,  0,   CSR_RW, 0x00000000 },
  { "ini_score_w_05", 0x00000060, 12,  8,   CSR_RW, 0x00000000 },
  { "ini_score_w_06", 0x00000060, 20, 16,   CSR_RW, 0x00000000 },
  { "ini_score_w_07", 0x00000060, 28, 24,   CSR_RW, 0x00000000 },
  { "INI_SCORE_W_1", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ini_score_w_2
  { "ini_score_w_08", 0x00000064,  4,  0,   CSR_RW, 0x00000000 },
  { "ini_score_w_09", 0x00000064, 12,  8,   CSR_RW, 0x00000000 },
  { "ini_score_w_10", 0x00000064, 20, 16,   CSR_RW, 0x00000000 },
  { "ini_score_w_11", 0x00000064, 28, 24,   CSR_RW, 0x00000000 },
  { "INI_SCORE_W_2", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ini_score_w_3
  { "ini_score_w_12", 0x00000068,  4,  0,   CSR_RW, 0x00000000 },
  { "ini_score_w_13", 0x00000068, 12,  8,   CSR_RW, 0x00000000 },
  { "ini_score_w_14", 0x00000068, 20, 16,   CSR_RW, 0x00000000 },
  { "ini_score_w_15", 0x00000068, 28, 24,   CSR_RW, 0x00000000 },
  { "INI_SCORE_W_3", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ini_score_w_4
  { "ini_score_w_16", 0x0000006C,  4,  0,   CSR_RW, 0x00000000 },
  { "ini_score_w_17", 0x0000006C, 12,  8,   CSR_RW, 0x00000000 },
  { "ini_score_w_18", 0x0000006C, 20, 16,   CSR_RW, 0x00000000 },
  { "ini_score_w_19", 0x0000006C, 28, 24,   CSR_RW, 0x00000000 },
  { "INI_SCORE_W_4", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ini_score_w_5
  { "ini_score_w_20", 0x00000070,  4,  0,   CSR_RW, 0x00000000 },
  { "ini_score_w_21", 0x00000070, 12,  8,   CSR_RW, 0x00000000 },
  { "ini_score_w_22", 0x00000070, 20, 16,   CSR_RW, 0x00000000 },
  { "ini_score_w_23", 0x00000070, 28, 24,   CSR_RW, 0x00000000 },
  { "INI_SCORE_W_5", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ini_score_w_6
  { "ini_score_w_24", 0x00000074,  4,  0,   CSR_RW, 0x00000000 },
  { "ini_score_w_25", 0x00000074, 12,  8,   CSR_RW, 0x00000000 },
  { "ini_score_w_26", 0x00000074, 20, 16,   CSR_RW, 0x00000000 },
  { "ini_score_w_27", 0x00000074, 28, 24,   CSR_RW, 0x00000000 },
  { "INI_SCORE_W_6", 0x00000074, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ini_score_w_7
  { "ini_score_w_28", 0x00000078,  4,  0,   CSR_RW, 0x00000000 },
  { "ini_score_w_29", 0x00000078, 12,  8,   CSR_RW, 0x00000000 },
  { "ini_score_w_30", 0x00000078, 20, 16,   CSR_RW, 0x00000000 },
  { "ini_score_w_31", 0x00000078, 28, 24,   CSR_RW, 0x00000000 },
  { "INI_SCORE_W_7", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ini_score_r_0
  { "ini_score_r_00", 0x0000007C,  4,  0,   CSR_RW, 0x00000000 },
  { "ini_score_r_01", 0x0000007C, 12,  8,   CSR_RW, 0x00000000 },
  { "ini_score_r_02", 0x0000007C, 20, 16,   CSR_RW, 0x00000000 },
  { "ini_score_r_03", 0x0000007C, 28, 24,   CSR_RW, 0x00000000 },
  { "INI_SCORE_R_0", 0x0000007C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ini_score_r_1
  { "ini_score_r_04", 0x00000080,  4,  0,   CSR_RW, 0x00000000 },
  { "ini_score_r_05", 0x00000080, 12,  8,   CSR_RW, 0x00000000 },
  { "ini_score_r_06", 0x00000080, 20, 16,   CSR_RW, 0x00000000 },
  { "ini_score_r_07", 0x00000080, 28, 24,   CSR_RW, 0x00000000 },
  { "INI_SCORE_R_1", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ini_score_r_2
  { "ini_score_r_08", 0x00000084,  4,  0,   CSR_RW, 0x00000000 },
  { "ini_score_r_09", 0x00000084, 12,  8,   CSR_RW, 0x00000000 },
  { "ini_score_r_10", 0x00000084, 20, 16,   CSR_RW, 0x00000000 },
  { "ini_score_r_11", 0x00000084, 28, 24,   CSR_RW, 0x00000000 },
  { "INI_SCORE_R_2", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ini_score_r_3
  { "ini_score_r_12", 0x00000088,  4,  0,   CSR_RW, 0x00000000 },
  { "ini_score_r_13", 0x00000088, 12,  8,   CSR_RW, 0x00000000 },
  { "ini_score_r_14", 0x00000088, 20, 16,   CSR_RW, 0x00000000 },
  { "ini_score_r_15", 0x00000088, 28, 24,   CSR_RW, 0x00000000 },
  { "INI_SCORE_R_3", 0x00000088, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ini_score_r_4
  { "ini_score_r_16", 0x0000008C,  4,  0,   CSR_RW, 0x00000000 },
  { "ini_score_r_17", 0x0000008C, 12,  8,   CSR_RW, 0x00000000 },
  { "ini_score_r_18", 0x0000008C, 20, 16,   CSR_RW, 0x00000000 },
  { "ini_score_r_19", 0x0000008C, 28, 24,   CSR_RW, 0x00000000 },
  { "INI_SCORE_R_4", 0x0000008C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ini_score_r_5
  { "ini_score_r_20", 0x00000090,  4,  0,   CSR_RW, 0x00000000 },
  { "ini_score_r_21", 0x00000090, 12,  8,   CSR_RW, 0x00000000 },
  { "ini_score_r_22", 0x00000090, 20, 16,   CSR_RW, 0x00000000 },
  { "ini_score_r_23", 0x00000090, 28, 24,   CSR_RW, 0x00000000 },
  { "INI_SCORE_R_5", 0x00000090, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ini_score_r_6
  { "ini_score_r_24", 0x00000094,  4,  0,   CSR_RW, 0x00000000 },
  { "ini_score_r_25", 0x00000094, 12,  8,   CSR_RW, 0x00000000 },
  { "ini_score_r_26", 0x00000094, 20, 16,   CSR_RW, 0x00000000 },
  { "ini_score_r_27", 0x00000094, 28, 24,   CSR_RW, 0x00000000 },
  { "INI_SCORE_R_6", 0x00000094, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ini_score_r_7
  { "ini_score_r_28", 0x00000098,  4,  0,   CSR_RW, 0x00000000 },
  { "ini_score_r_29", 0x00000098, 12,  8,   CSR_RW, 0x00000000 },
  { "ini_score_r_30", 0x00000098, 20, 16,   CSR_RW, 0x00000000 },
  { "ini_score_r_31", 0x00000098, 28, 24,   CSR_RW, 0x00000000 },
  { "INI_SCORE_R_7", 0x00000098, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wait_cnt_w_0
  { "wait_cnt_norm_w_00", 0x0000009C,  2,  0,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_01", 0x0000009C, 10,  8,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_02", 0x0000009C, 18, 16,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_03", 0x0000009C, 26, 24,   CSR_RW, 0x00000004 },
  { "WAIT_CNT_W_0", 0x0000009C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wait_cnt_w_1
  { "wait_cnt_norm_w_04", 0x000000A0,  2,  0,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_05", 0x000000A0, 10,  8,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_06", 0x000000A0, 18, 16,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_07", 0x000000A0, 26, 24,   CSR_RW, 0x00000004 },
  { "WAIT_CNT_W_1", 0x000000A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wait_cnt_w_2
  { "wait_cnt_norm_w_08", 0x000000A4,  2,  0,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_09", 0x000000A4, 10,  8,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_10", 0x000000A4, 18, 16,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_11", 0x000000A4, 26, 24,   CSR_RW, 0x00000004 },
  { "WAIT_CNT_W_2", 0x000000A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wait_cnt_w_3
  { "wait_cnt_norm_w_12", 0x000000A8,  2,  0,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_13", 0x000000A8, 10,  8,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_14", 0x000000A8, 18, 16,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_15", 0x000000A8, 26, 24,   CSR_RW, 0x00000004 },
  { "WAIT_CNT_W_3", 0x000000A8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wait_cnt_w_4
  { "wait_cnt_norm_w_16", 0x000000AC,  2,  0,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_17", 0x000000AC, 10,  8,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_18", 0x000000AC, 18, 16,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_19", 0x000000AC, 26, 24,   CSR_RW, 0x00000004 },
  { "WAIT_CNT_W_4", 0x000000AC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wait_cnt_w_5
  { "wait_cnt_norm_w_20", 0x000000B0,  2,  0,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_21", 0x000000B0, 10,  8,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_22", 0x000000B0, 18, 16,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_23", 0x000000B0, 26, 24,   CSR_RW, 0x00000004 },
  { "WAIT_CNT_W_5", 0x000000B0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wait_cnt_w_6
  { "wait_cnt_norm_w_24", 0x000000B4,  2,  0,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_25", 0x000000B4, 10,  8,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_26", 0x000000B4, 18, 16,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_27", 0x000000B4, 26, 24,   CSR_RW, 0x00000004 },
  { "WAIT_CNT_W_6", 0x000000B4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wait_cnt_w_7
  { "wait_cnt_norm_w_28", 0x000000B8,  2,  0,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_29", 0x000000B8, 10,  8,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_30", 0x000000B8, 18, 16,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_w_31", 0x000000B8, 26, 24,   CSR_RW, 0x00000004 },
  { "WAIT_CNT_W_7", 0x000000B8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wait_cnt_r_0
  { "wait_cnt_norm_r_00", 0x000000BC,  2,  0,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_01", 0x000000BC, 10,  8,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_02", 0x000000BC, 18, 16,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_03", 0x000000BC, 26, 24,   CSR_RW, 0x00000004 },
  { "WAIT_CNT_R_0", 0x000000BC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wait_cnt_r_1
  { "wait_cnt_norm_r_04", 0x000000C0,  2,  0,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_05", 0x000000C0, 10,  8,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_06", 0x000000C0, 18, 16,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_07", 0x000000C0, 26, 24,   CSR_RW, 0x00000004 },
  { "WAIT_CNT_R_1", 0x000000C0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wait_cnt_r_2
  { "wait_cnt_norm_r_08", 0x000000C4,  2,  0,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_09", 0x000000C4, 10,  8,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_10", 0x000000C4, 18, 16,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_11", 0x000000C4, 26, 24,   CSR_RW, 0x00000004 },
  { "WAIT_CNT_R_2", 0x000000C4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wait_cnt_r_3
  { "wait_cnt_norm_r_12", 0x000000C8,  2,  0,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_13", 0x000000C8, 10,  8,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_14", 0x000000C8, 18, 16,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_15", 0x000000C8, 26, 24,   CSR_RW, 0x00000004 },
  { "WAIT_CNT_R_3", 0x000000C8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wait_cnt_r_4
  { "wait_cnt_norm_r_16", 0x000000CC,  2,  0,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_17", 0x000000CC, 10,  8,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_18", 0x000000CC, 18, 16,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_19", 0x000000CC, 26, 24,   CSR_RW, 0x00000004 },
  { "WAIT_CNT_R_4", 0x000000CC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wait_cnt_r_5
  { "wait_cnt_norm_r_20", 0x000000D0,  2,  0,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_21", 0x000000D0, 10,  8,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_22", 0x000000D0, 18, 16,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_23", 0x000000D0, 26, 24,   CSR_RW, 0x00000004 },
  { "WAIT_CNT_R_5", 0x000000D0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wait_cnt_r_6
  { "wait_cnt_norm_r_24", 0x000000D4,  2,  0,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_25", 0x000000D4, 10,  8,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_26", 0x000000D4, 18, 16,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_27", 0x000000D4, 26, 24,   CSR_RW, 0x00000004 },
  { "WAIT_CNT_R_6", 0x000000D4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wait_cnt_r_7
  { "wait_cnt_norm_r_28", 0x000000D8,  2,  0,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_29", 0x000000D8, 10,  8,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_30", 0x000000D8, 18, 16,   CSR_RW, 0x00000004 },
  { "wait_cnt_norm_r_31", 0x000000D8, 26, 24,   CSR_RW, 0x00000004 },
  { "WAIT_CNT_R_7", 0x000000D8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD page_match_w_0
  { "page_match_score_w_00", 0x000000DC,  3,  0,   CSR_RW, 0x00000008 },
  { "page_match_score_w_01", 0x000000DC, 11,  8,   CSR_RW, 0x00000008 },
  { "page_match_score_w_02", 0x000000DC, 19, 16,   CSR_RW, 0x00000008 },
  { "page_match_score_w_03", 0x000000DC, 27, 24,   CSR_RW, 0x00000008 },
  { "PAGE_MATCH_W_0", 0x000000DC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD page_match_w_1
  { "page_match_score_w_04", 0x000000E0,  3,  0,   CSR_RW, 0x00000008 },
  { "page_match_score_w_05", 0x000000E0, 11,  8,   CSR_RW, 0x00000008 },
  { "page_match_score_w_06", 0x000000E0, 19, 16,   CSR_RW, 0x00000008 },
  { "page_match_score_w_07", 0x000000E0, 27, 24,   CSR_RW, 0x00000008 },
  { "PAGE_MATCH_W_1", 0x000000E0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD page_match_w_2
  { "page_match_score_w_08", 0x000000E4,  3,  0,   CSR_RW, 0x00000008 },
  { "page_match_score_w_09", 0x000000E4, 11,  8,   CSR_RW, 0x00000008 },
  { "page_match_score_w_10", 0x000000E4, 19, 16,   CSR_RW, 0x00000008 },
  { "page_match_score_w_11", 0x000000E4, 27, 24,   CSR_RW, 0x00000008 },
  { "PAGE_MATCH_W_2", 0x000000E4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD page_match_w_3
  { "page_match_score_w_12", 0x000000E8,  3,  0,   CSR_RW, 0x00000008 },
  { "page_match_score_w_13", 0x000000E8, 11,  8,   CSR_RW, 0x00000008 },
  { "page_match_score_w_14", 0x000000E8, 19, 16,   CSR_RW, 0x00000008 },
  { "page_match_score_w_15", 0x000000E8, 27, 24,   CSR_RW, 0x00000008 },
  { "PAGE_MATCH_W_3", 0x000000E8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD page_match_w_4
  { "page_match_score_w_16", 0x000000EC,  3,  0,   CSR_RW, 0x00000008 },
  { "page_match_score_w_17", 0x000000EC, 11,  8,   CSR_RW, 0x00000008 },
  { "page_match_score_w_18", 0x000000EC, 19, 16,   CSR_RW, 0x00000008 },
  { "page_match_score_w_19", 0x000000EC, 27, 24,   CSR_RW, 0x00000008 },
  { "PAGE_MATCH_W_4", 0x000000EC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD page_match_w_5
  { "page_match_score_w_20", 0x000000F0,  3,  0,   CSR_RW, 0x00000008 },
  { "page_match_score_w_21", 0x000000F0, 11,  8,   CSR_RW, 0x00000008 },
  { "page_match_score_w_22", 0x000000F0, 19, 16,   CSR_RW, 0x00000008 },
  { "page_match_score_w_23", 0x000000F0, 27, 24,   CSR_RW, 0x00000008 },
  { "PAGE_MATCH_W_5", 0x000000F0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD page_match_w_6
  { "page_match_score_w_24", 0x000000F4,  3,  0,   CSR_RW, 0x00000008 },
  { "page_match_score_w_25", 0x000000F4, 11,  8,   CSR_RW, 0x00000008 },
  { "page_match_score_w_26", 0x000000F4, 19, 16,   CSR_RW, 0x00000008 },
  { "page_match_score_w_27", 0x000000F4, 27, 24,   CSR_RW, 0x00000008 },
  { "PAGE_MATCH_W_6", 0x000000F4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD page_match_w_7
  { "page_match_score_w_28", 0x000000F8,  3,  0,   CSR_RW, 0x00000008 },
  { "page_match_score_w_29", 0x000000F8, 11,  8,   CSR_RW, 0x00000008 },
  { "page_match_score_w_30", 0x000000F8, 19, 16,   CSR_RW, 0x00000008 },
  { "page_match_score_w_31", 0x000000F8, 27, 24,   CSR_RW, 0x00000008 },
  { "PAGE_MATCH_W_7", 0x000000F8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD page_match_r_0
  { "page_match_score_r_00", 0x000000FC,  3,  0,   CSR_RW, 0x00000008 },
  { "page_match_score_r_01", 0x000000FC, 11,  8,   CSR_RW, 0x00000008 },
  { "page_match_score_r_02", 0x000000FC, 19, 16,   CSR_RW, 0x00000008 },
  { "page_match_score_r_03", 0x000000FC, 27, 24,   CSR_RW, 0x00000008 },
  { "PAGE_MATCH_R_0", 0x000000FC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD page_match_r_1
  { "page_match_score_r_04", 0x00000100,  3,  0,   CSR_RW, 0x00000008 },
  { "page_match_score_r_05", 0x00000100, 11,  8,   CSR_RW, 0x00000008 },
  { "page_match_score_r_06", 0x00000100, 19, 16,   CSR_RW, 0x00000008 },
  { "page_match_score_r_07", 0x00000100, 27, 24,   CSR_RW, 0x00000008 },
  { "PAGE_MATCH_R_1", 0x00000100, 31, 0,   CSR_RW, 0x00000000 },
  // WORD page_match_r_2
  { "page_match_score_r_08", 0x00000104,  3,  0,   CSR_RW, 0x00000008 },
  { "page_match_score_r_09", 0x00000104, 11,  8,   CSR_RW, 0x00000008 },
  { "page_match_score_r_10", 0x00000104, 19, 16,   CSR_RW, 0x00000008 },
  { "page_match_score_r_11", 0x00000104, 27, 24,   CSR_RW, 0x00000008 },
  { "PAGE_MATCH_R_2", 0x00000104, 31, 0,   CSR_RW, 0x00000000 },
  // WORD page_match_r_3
  { "page_match_score_r_12", 0x00000108,  3,  0,   CSR_RW, 0x00000008 },
  { "page_match_score_r_13", 0x00000108, 11,  8,   CSR_RW, 0x00000008 },
  { "page_match_score_r_14", 0x00000108, 19, 16,   CSR_RW, 0x00000008 },
  { "page_match_score_r_15", 0x00000108, 27, 24,   CSR_RW, 0x00000008 },
  { "PAGE_MATCH_R_3", 0x00000108, 31, 0,   CSR_RW, 0x00000000 },
  // WORD page_match_r_4
  { "page_match_score_r_16", 0x0000010C,  3,  0,   CSR_RW, 0x00000008 },
  { "page_match_score_r_17", 0x0000010C, 11,  8,   CSR_RW, 0x00000008 },
  { "page_match_score_r_18", 0x0000010C, 19, 16,   CSR_RW, 0x00000008 },
  { "page_match_score_r_19", 0x0000010C, 27, 24,   CSR_RW, 0x00000008 },
  { "PAGE_MATCH_R_4", 0x0000010C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD page_match_r_5
  { "page_match_score_r_20", 0x00000110,  3,  0,   CSR_RW, 0x00000008 },
  { "page_match_score_r_21", 0x00000110, 11,  8,   CSR_RW, 0x00000008 },
  { "page_match_score_r_22", 0x00000110, 19, 16,   CSR_RW, 0x00000008 },
  { "page_match_score_r_23", 0x00000110, 27, 24,   CSR_RW, 0x00000008 },
  { "PAGE_MATCH_R_5", 0x00000110, 31, 0,   CSR_RW, 0x00000000 },
  // WORD page_match_r_6
  { "page_match_score_r_24", 0x00000114,  3,  0,   CSR_RW, 0x00000008 },
  { "page_match_score_r_25", 0x00000114, 11,  8,   CSR_RW, 0x00000008 },
  { "page_match_score_r_26", 0x00000114, 19, 16,   CSR_RW, 0x00000008 },
  { "page_match_score_r_27", 0x00000114, 27, 24,   CSR_RW, 0x00000008 },
  { "PAGE_MATCH_R_6", 0x00000114, 31, 0,   CSR_RW, 0x00000000 },
  // WORD page_match_r_7
  { "page_match_score_r_28", 0x00000118,  3,  0,   CSR_RW, 0x00000008 },
  { "page_match_score_r_29", 0x00000118, 11,  8,   CSR_RW, 0x00000008 },
  { "page_match_score_r_30", 0x00000118, 19, 16,   CSR_RW, 0x00000008 },
  { "page_match_score_r_31", 0x00000118, 27, 24,   CSR_RW, 0x00000008 },
  { "PAGE_MATCH_R_7", 0x00000118, 31, 0,   CSR_RW, 0x00000000 },
  // WORD general_score
  { "bank_switch_score", 0x0000011C,  3,  0,   CSR_RW, 0x00000004 },
  { "keep_agent_score", 0x0000011C, 11,  8,   CSR_RW, 0x00000004 },
  { "keep_rw_score", 0x0000011C, 19, 16,   CSR_RW, 0x00000002 },
  { "debug_mon_sel", 0x0000011C, 28, 24,   CSR_RW, 0x00000000 },
  { "GENERAL_SCORE", 0x0000011C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dram_type
  { "row_before_bank", 0x00000120,  0,  0,   CSR_RW, 0x00000001 },
  { "bank_addr_type", 0x00000120,  8,  8,   CSR_RW, 0x00000001 },
  { "row_addr_type", 0x00000120, 18, 16,   CSR_RW, 0x00000000 },
  { "col_addr_type", 0x00000120, 25, 24,   CSR_RW, 0x00000000 },
  { "DRAM_TYPE", 0x00000120, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pat_gen_0_ctrl_0
  { "pat_gen_0_en", 0x00000124,  0,  0,   CSR_RW, 0x00000000 },
  { "pat_gen_0_write", 0x00000124,  8,  8,   CSR_RW, 0x00000000 },
  { "pat_gen_0_all_agent", 0x00000124, 16, 16,   CSR_RW, 0x00000000 },
  { "pat_gen_0_agent", 0x00000124, 28, 24,   CSR_RW, 0x00000000 },
  { "PAT_GEN_0_CTRL_0", 0x00000124, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pat_gen_0_ctrl_1
  { "pat_gen_0_shuffle", 0x00000128,  0,  0,   CSR_RW, 0x00000000 },
  { "pat_gen_0_wstrb_en", 0x00000128,  8,  8,   CSR_RW, 0x00000000 },
  { "pat_gen_0_wstrb", 0x00000128, 23, 16,   CSR_RW, 0x000000FF },
  { "PAT_GEN_0_CTRL_1", 0x00000128, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_pat_gen_0_data_0
  { "pat_gen_0_data_0", 0x0000012C, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_PAT_GEN_0_DATA_0", 0x0000012C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_pat_gen_0_data_1
  { "pat_gen_0_data_1", 0x00000130, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_PAT_GEN_0_DATA_1", 0x00000130, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pat_gen_1_ctrl_0
  { "pat_gen_1_en", 0x00000134,  0,  0,   CSR_RW, 0x00000000 },
  { "pat_gen_1_write", 0x00000134,  8,  8,   CSR_RW, 0x00000000 },
  { "pat_gen_1_all_agent", 0x00000134, 16, 16,   CSR_RW, 0x00000000 },
  { "pat_gen_1_agent", 0x00000134, 28, 24,   CSR_RW, 0x00000000 },
  { "PAT_GEN_1_CTRL_0", 0x00000134, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pat_gen_1_ctrl_1
  { "pat_gen_1_shuffle", 0x00000138,  0,  0,   CSR_RW, 0x00000000 },
  { "pat_gen_1_wstrb_en", 0x00000138,  8,  8,   CSR_RW, 0x00000000 },
  { "pat_gen_1_wstrb", 0x00000138, 23, 16,   CSR_RW, 0x000000FF },
  { "PAT_GEN_1_CTRL_1", 0x00000138, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_pat_gen_1_data_0
  { "pat_gen_1_data_0", 0x0000013C, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_PAT_GEN_1_DATA_0", 0x0000013C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_pat_gen_1_data_1
  { "pat_gen_1_data_1", 0x00000140, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_PAT_GEN_1_DATA_1", 0x00000140, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pat_gen_2_ctrl_0
  { "pat_gen_2_en", 0x00000144,  0,  0,   CSR_RW, 0x00000000 },
  { "pat_gen_2_write", 0x00000144,  8,  8,   CSR_RW, 0x00000000 },
  { "pat_gen_2_all_agent", 0x00000144, 16, 16,   CSR_RW, 0x00000000 },
  { "pat_gen_2_agent", 0x00000144, 28, 24,   CSR_RW, 0x00000000 },
  { "PAT_GEN_2_CTRL_0", 0x00000144, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pat_gen_2_ctrl_1
  { "pat_gen_2_shuffle", 0x00000148,  0,  0,   CSR_RW, 0x00000000 },
  { "pat_gen_2_wstrb_en", 0x00000148,  8,  8,   CSR_RW, 0x00000000 },
  { "pat_gen_2_wstrb", 0x00000148, 23, 16,   CSR_RW, 0x000000FF },
  { "PAT_GEN_2_CTRL_1", 0x00000148, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_pat_gen_2_data_0
  { "pat_gen_2_data_0", 0x0000014C, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_PAT_GEN_2_DATA_0", 0x0000014C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_pat_gen_2_data_1
  { "pat_gen_2_data_1", 0x00000150, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_PAT_GEN_2_DATA_1", 0x00000150, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pat_gen_3_ctrl_0
  { "pat_gen_3_en", 0x00000154,  0,  0,   CSR_RW, 0x00000000 },
  { "pat_gen_3_write", 0x00000154,  8,  8,   CSR_RW, 0x00000000 },
  { "pat_gen_3_all_agent", 0x00000154, 16, 16,   CSR_RW, 0x00000000 },
  { "pat_gen_3_agent", 0x00000154, 28, 24,   CSR_RW, 0x00000000 },
  { "PAT_GEN_3_CTRL_0", 0x00000154, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pat_gen_3_ctrl_1
  { "pat_gen_3_shuffle", 0x00000158,  0,  0,   CSR_RW, 0x00000000 },
  { "pat_gen_3_wstrb_en", 0x00000158,  8,  8,   CSR_RW, 0x00000000 },
  { "pat_gen_3_wstrb", 0x00000158, 23, 16,   CSR_RW, 0x000000FF },
  { "PAT_GEN_3_CTRL_1", 0x00000158, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_pat_gen_3_data_0
  { "pat_gen_3_data_0", 0x0000015C, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_PAT_GEN_3_DATA_0", 0x0000015C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_pat_gen_3_data_1
  { "pat_gen_3_data_1", 0x00000160, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_PAT_GEN_3_DATA_1", 0x00000160, 31, 0,   CSR_RW, 0x00000000 },
  // WORD chksum_0_ctrl
  { "checksum_0_en", 0x00000164,  0,  0,   CSR_RW, 0x00000000 },
  { "checksum_0_type", 0x00000164,  9,  8,   CSR_RW, 0x00000000 },
  { "checksum_0_agent", 0x00000164, 20, 16,   CSR_RW, 0x00000000 },
  { "CHKSUM_0_CTRL", 0x00000164, 31, 0,   CSR_RW, 0x00000000 },
  // WORD chksum_0_0
  { "checksum_0_0", 0x00000168, 31,  0,   CSR_RO, 0x00000000 },
  { "CHKSUM_0_0", 0x00000168, 31, 0,   CSR_RO, 0x00000000 },
  // WORD chksum_0_1
  { "checksum_0_1", 0x0000016C, 31,  0,   CSR_RO, 0x00000000 },
  { "CHKSUM_0_1", 0x0000016C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD chksum_1_ctrl
  { "checksum_1_en", 0x00000170,  0,  0,   CSR_RW, 0x00000000 },
  { "checksum_1_type", 0x00000170,  9,  8,   CSR_RW, 0x00000000 },
  { "checksum_1_agent", 0x00000170, 20, 16,   CSR_RW, 0x00000000 },
  { "CHKSUM_1_CTRL", 0x00000170, 31, 0,   CSR_RW, 0x00000000 },
  // WORD chksum_1_0
  { "checksum_1_0", 0x00000174, 31,  0,   CSR_RO, 0x00000000 },
  { "CHKSUM_1_0", 0x00000174, 31, 0,   CSR_RO, 0x00000000 },
  // WORD chksum_1_1
  { "checksum_1_1", 0x00000178, 31,  0,   CSR_RO, 0x00000000 },
  { "CHKSUM_1_1", 0x00000178, 31, 0,   CSR_RO, 0x00000000 },
  // WORD chksum_2_ctrl
  { "checksum_2_en", 0x0000017C,  0,  0,   CSR_RW, 0x00000000 },
  { "checksum_2_type", 0x0000017C,  9,  8,   CSR_RW, 0x00000000 },
  { "checksum_2_agent", 0x0000017C, 20, 16,   CSR_RW, 0x00000000 },
  { "CHKSUM_2_CTRL", 0x0000017C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD chksum_2_0
  { "checksum_2_0", 0x00000180, 31,  0,   CSR_RO, 0x00000000 },
  { "CHKSUM_2_0", 0x00000180, 31, 0,   CSR_RO, 0x00000000 },
  // WORD chksum_2_1
  { "checksum_2_1", 0x00000184, 31,  0,   CSR_RO, 0x00000000 },
  { "CHKSUM_2_1", 0x00000184, 31, 0,   CSR_RO, 0x00000000 },
  // WORD chksum_3_ctrl
  { "checksum_3_en", 0x00000188,  0,  0,   CSR_RW, 0x00000000 },
  { "checksum_3_type", 0x00000188,  9,  8,   CSR_RW, 0x00000000 },
  { "checksum_3_agent", 0x00000188, 20, 16,   CSR_RW, 0x00000000 },
  { "CHKSUM_3_CTRL", 0x00000188, 31, 0,   CSR_RW, 0x00000000 },
  // WORD chksum_3_0
  { "checksum_3_0", 0x0000018C, 31,  0,   CSR_RO, 0x00000000 },
  { "CHKSUM_3_0", 0x0000018C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD chksum_3_1
  { "checksum_3_1", 0x00000190, 31,  0,   CSR_RO, 0x00000000 },
  { "CHKSUM_3_1", 0x00000190, 31, 0,   CSR_RO, 0x00000000 },
  // WORD mon_agent
  { "w_addr_mon_agent", 0x00000194,  4,  0,   CSR_RW, 0x00000000 },
  { "r_addr_mon_agent", 0x00000194, 12,  8,   CSR_RW, 0x00000000 },
  { "w_data_mon_agent", 0x00000194, 20, 16,   CSR_RW, 0x00000000 },
  { "r_data_mon_agent", 0x00000194, 28, 24,   CSR_RW, 0x00000000 },
  { "MON_AGENT", 0x00000194, 31, 0,   CSR_RW, 0x00000000 },
  // WORD last_status_en
  { "last_w_addr_en", 0x00000198,  0,  0,   CSR_RW, 0x00000000 },
  { "last_r_addr_en", 0x00000198,  8,  8,   CSR_RW, 0x00000000 },
  { "last_w_data_en", 0x00000198, 16, 16,   CSR_RW, 0x00000000 },
  { "last_r_data_en", 0x00000198, 24, 24,   CSR_RW, 0x00000000 },
  { "LAST_STATUS_EN", 0x00000198, 31, 0,   CSR_RW, 0x00000000 },
  // WORD last_status_all_agent
  { "last_w_addr_all_agent", 0x0000019C,  0,  0,   CSR_RW, 0x00000001 },
  { "last_r_addr_all_agent", 0x0000019C,  8,  8,   CSR_RW, 0x00000001 },
  { "last_w_data_all_agent", 0x0000019C, 16, 16,   CSR_RW, 0x00000001 },
  { "last_r_data_all_agent", 0x0000019C, 24, 24,   CSR_RW, 0x00000001 },
  { "LAST_STATUS_ALL_AGENT", 0x0000019C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_last_w_addr
  { "last_w_addr", 0x000001A0, 30,  0,   CSR_RO, 0x00000000 },
  { "WORD_LAST_W_ADDR", 0x000001A0, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_last_r_addr
  { "last_r_addr", 0x000001A4, 30,  0,   CSR_RO, 0x00000000 },
  { "WORD_LAST_R_ADDR", 0x000001A4, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_last_w_data_0
  { "last_w_data_0", 0x000001A8, 31,  0,   CSR_RO, 0x00000000 },
  { "WORD_LAST_W_DATA_0", 0x000001A8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_last_w_data_1
  { "last_w_data_1", 0x000001AC, 31,  0,   CSR_RO, 0x00000000 },
  { "WORD_LAST_W_DATA_1", 0x000001AC, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_last_r_data_0
  { "last_r_data_0", 0x000001B0, 31,  0,   CSR_RO, 0x00000000 },
  { "WORD_LAST_R_DATA_0", 0x000001B0, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_last_r_data_1
  { "last_r_data_1", 0x000001B4, 31,  0,   CSR_RO, 0x00000000 },
  { "WORD_LAST_R_DATA_1", 0x000001B4, 31, 0,   CSR_RO, 0x00000000 },
  // WORD resp_error_agent
  { "w_resp_error_agent", 0x000001B8,  4,  0,   CSR_RO, 0x00000000 },
  { "r_resp_error_agent", 0x000001B8, 12,  8,   CSR_RO, 0x00000000 },
  { "RESP_ERROR_AGENT", 0x000001B8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD timeout_ctrl
  { "timeout_en", 0x000001BC,  0,  0,   CSR_RW, 0x00000000 },
  { "timeout_cycle_th", 0x000001BC, 31, 16,   CSR_RW, 0x0000FFFF },
  { "TIMEOUT_CTRL", 0x000001BC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD timeout_agent_0
  { "timeout_w_resp_agent", 0x000001C0,  4,  0,   CSR_RO, 0x00000000 },
  { "timeout_r_addr_agent", 0x000001C0, 12,  8,   CSR_RO, 0x00000000 },
  { "timeout_w_addr_agent", 0x000001C0, 20, 16,   CSR_RO, 0x00000000 },
  { "timeout_w_data_agent", 0x000001C0, 28, 24,   CSR_RO, 0x00000000 },
  { "TIMEOUT_AGENT_0", 0x000001C0, 31, 0,   CSR_RO, 0x00000000 },
  // WORD timeout_agent_1
  { "timeout_r_data_agent", 0x000001C4, 20, 16,   CSR_RO, 0x00000000 },
  { "TIMEOUT_AGENT_1", 0x000001C4, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_reserved_0
  { "reserved_0", 0x000001C8, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED_0", 0x000001C8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_reserved_1
  { "reserved_1", 0x000001CC, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED_1", 0x000001CC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_reserved_2
  { "reserved_2", 0x000001D0, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED_2", 0x000001D0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_reserved_3
  { "reserved_3", 0x000001D4, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED_3", 0x000001D4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_reserved_4
  { "reserved_4", 0x000001D8, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED_4", 0x000001D8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_reserved_5
  { "reserved_5", 0x000001DC, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED_5", 0x000001DC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_reserved_6
  { "reserved_6", 0x000001E0, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED_6", 0x000001E0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_reserved_7
  { "reserved_7", 0x000001E4, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED_7", 0x000001E4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD general_score_1
  { "ini_r_score", 0x000001E8,  3,  0,   CSR_RW, 0x00000000 },
  { "ini_w_score", 0x000001E8, 11,  8,   CSR_RW, 0x00000000 },
  { "GENERAL_SCORE_1", 0x000001E8, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_DARB_H_
