#ifndef CSR_TABLE_DISP_CFG_H_
#define CSR_TABLE_DISP_CFG_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_disp_cfg[] =
{
  // WORD conf0_dispr
  { "cken_dispr", 0x00000000,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_dispr", 0x00000000,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_DISPR", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_dispr
  { "sw_rst_dispr", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_dispr", 0x00000004,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_DISPR", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_cs
  { "cken_cs", 0x00000008,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_cs", 0x00000008,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_CS", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_cs
  { "sw_rst_cs", 0x0000000C,  0,  0,  CSR_W1P, 0x00000000 },
  { "CONF1_CS", 0x0000000C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_yuv422
  { "cken_yuv422", 0x00000010,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_yuv422", 0x00000010,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_YUV422", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_yuv422
  { "sw_rst_yuv422", 0x00000014,  0,  0,  CSR_W1P, 0x00000000 },
  { "CONF1_YUV422", 0x00000014, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_yuv444
  { "cken_yuv444", 0x00000018,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_yuv444", 0x00000018,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_YUV444", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_yuv444
  { "sw_rst_yuv444", 0x0000001C,  0,  0,  CSR_W1P, 0x00000000 },
  { "CONF1_YUV444", 0x0000001C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_dosd
  { "cken_dosd", 0x00000020,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_dosd", 0x00000020,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_DOSD", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_dosd
  { "sw_rst_dosd", 0x00000024,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_dosd", 0x00000024,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_DOSD", 0x00000024, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_dcst
  { "cken_dcst", 0x00000028,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_dcst", 0x00000028,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_DCST", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_dcst
  { "sw_rst_dcst", 0x0000002C,  0,  0,  CSR_W1P, 0x00000000 },
  { "CONF1_DCST", 0x0000002C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_dgma
  { "cken_dgma", 0x00000030,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_dgma", 0x00000030,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_DGMA", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_dgma
  { "sw_rst_dgma", 0x00000034,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_dgma", 0x00000034,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_DGMA", 0x00000034, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_dcs
  { "cken_dcs", 0x00000038,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_dcs", 0x00000038,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_DCS", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_dcs
  { "sw_rst_dcs", 0x0000003C,  0,  0,  CSR_W1P, 0x00000000 },
  { "CONF1_DCS", 0x0000003C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_dcfa
  { "cken_dcfa", 0x00000040,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_dcfa", 0x00000040,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_DCFA", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_dcfa
  { "sw_rst_dcfa", 0x00000044,  0,  0,  CSR_W1P, 0x00000000 },
  { "CONF1_DCFA", 0x00000044, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_buf
  { "cken_buf", 0x00000048,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_buf", 0x00000048,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_BUF", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_buf
  { "sw_rst_buf", 0x0000004C,  0,  0,  CSR_W1P, 0x00000000 },
  { "CONF1_BUF", 0x0000004C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_ctrl
  { "cken_ctrl", 0x00000050,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_ctrl", 0x00000050,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_CTRL", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_ctrl
  { "sw_rst_ctrl", 0x00000054,  0,  0,  CSR_W1P, 0x00000000 },
  { "CONF1_CTRL", 0x00000054, 31, 0,  CSR_W1P, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_DISP_CFG_H_
