#ifndef __AES__H__
#define __AES__H__


#include <stdint.h>

typedef enum {
    AES_CYPHER_128,
    AES_CYPHER_256
} AES_CYPHER_T;

extern int g_aes_rounds[];
extern int g_aes_nk[];
extern int g_aes_nb[];

uint32_t aes_swap_dword(uint32_t val);
void aes_key_expansion(AES_CYPHER_T mode, uint32_t *key, uint32_t *result);

#endif  /* __AES__H__ */