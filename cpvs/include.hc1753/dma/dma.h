#ifndef __DMA__H__
#define __DMA__H__

#include <stdint.h>
#include "address_map.h"

#define TEST_DMA_IN_BUF_0_ADDR (0x01000000 >> 3)
#define TEST_DMA_IN_BUF_1_ADDR (0x01001000 >> 3)
#define TEST_DMA_IN_BUF_2_ADDR (0x01002000 >> 3)
#define TEST_DMA_IN_BUF_3_ADDR (0x01003000 >> 3)
#define TEST_DMA_IN_BUF_4_ADDR (0x01004000 >> 3)
#define TEST_DMA_IN_BUF_5_ADDR (0x01005000 >> 3)
#define TEST_DMA_IN_BUF_6_ADDR (0x01006000 >> 3)
#define TEST_DMA_IN_BUF_7_ADDR (0x01007000 >> 3)
#define TEST_DMA_OUT_BUF_0_ADDR (0x02000000 >> 3)
#define TEST_DMA_OUT_BUF_1_ADDR (0x02001000 >> 3)
#define TEST_DMA_OUT_BUF_2_ADDR (0x02002000 >> 3)
#define TEST_DMA_OUT_BUF_3_ADDR (0x02003000 >> 3)
#define TEST_DMA_OUT_BUF_4_ADDR (0x02004000 >> 3)
#define TEST_DMA_OUT_BUF_5_ADDR (0x02005000 >> 3)
#define TEST_DMA_OUT_BUF_6_ADDR (0x02006000 >> 3)
#define TEST_DMA_OUT_BUF_7_ADDR (0x02007000 >> 3)

#define DMA_ACCESS_LENGTH__MAX 0x8000

#define DMA_TOTAL_ACCESS_CNT__MAX 0x8

#define DMA_AES__ENC    0x0
#define DMA_AES__BYPASS 0x1
#define DMA_AES__DEC    0x3

#define DMA_ACCESS_MODE__LINEAR 0x0
#define DMA_ACCESS_MODE__YORC   0x1
#define DMA_ACCESS_MODE__CR     0x2

#define DMA_BANK_INTERLEAVE_TYPE__NO_BANK 0x0
#define DMA_BANK_INTERLEAVE_TYPE__2_BANK  0x1
#define DMA_BANK_INTERLEAVE_TYPE__4_BANK  0x2
#define DMA_BANK_INTERLEAVE_TYPE__8_BANK  0x3

#define DMA_START_ADDR 0x0
#define DMA_END_ADDR   0xFFFFFFF

#define AES_KEY_MODE__128 0x0
#define AES_KEY_MODE__256 0x1

#define AES_MODE__ECB     0x0
#define AES_MODE__CBC     0x1
#define AES_MODE__CFB_ENC 0x2
#define AES_MODE__CFB_DEC 0x3
#define AES_MODE__OFB_ENC 0x4
#define AES_MODE__OFB_DEC 0x5
#define AES_MODE__CTR_ENC 0x6
#define AES_MODE__CTR_DEC 0x7

#define AES_SWAP__LITTLE_ENDIAN 0x0
#define AES_SWAP__BIG_ENDIAN    0x1

typedef struct aes_dev {
    uint8_t key_mode; /* 0: 128 key length, 1: 256 key length */
    uint32_t key[8];
    uint8_t mode;  /* 
                    * 0: ECB, 1: CBC, 
                    * 2: CFB_ENC, 3: CFB_DEC, 
                    * 4: OFB_ENC, 5: OFB_DEC, 
                    * 6: CTR_ENC, 7: CTR_DEC 
                    */
    uint32_t iv[4];
    uint8_t byte_swap;
    uint8_t word_swap;
} aes;

typedef struct adocodec_dev {
    uint8_t codec_mode; /* 0: G711 enc, 1: G711 dec, 2: G726 enc, 3: G726 dec */
	uint8_t g711_mode; /* 0: A law, 1: U law */
	uint8_t g726_mode; /* 0: 16 kbits/s, 1: 32 kbits/s */
    uint8_t pki; /* 0: big endian (default), 1: little endian */
    uint8_t pko; /* 0: big endian (default), 1: little endian */
} adocodec;

typedef struct dma_dev {
    struct aes_dev aes;
    struct adocodec_dev adocodec;
    uint8_t col_addr_type;
    uint8_t bank_addr_type;
    uint8_t total_access_cnt;
    uint8_t bypass_ado;
    uint8_t aes_mode;
    uint8_t access_mode;
    uint32_t input_addr;
    uint32_t output_addr;
    uint32_t data_size;
} DmaDev;

void dma_init(struct dma_dev *dev);
void dma_trigger(void);
void dma_clk(uint8_t en);

#endif  /* __DMA__H__ */