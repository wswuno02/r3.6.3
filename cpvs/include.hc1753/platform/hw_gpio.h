/**
 * @file hw_gpio.h
 * @brief gpio 
 */

#ifndef HW_GPIO_H
#define HW_GPIO_H

#ifndef __KERNEL__
#include <stdint.h>
#else
#include <linux/types.h>
#endif

#define GPIO_IN 0
#define GPIO_OUT 1

void enable_gpio_pin(uint32_t n);
void set_gpio_mode(uint32_t mode);
void gpio_out(uint8_t v);
uint8_t gpio_in();

#endif