/**
 * @file hw_pll.h
 * @brief pll 
 */

#ifndef HW_PLL_H
#define HW_PLL_H

#ifndef __KERNEL__
#include <stdint.h>
#else
#include <linux/types.h>
#endif

typedef enum pll_type {
	PLL_TYPE_IPLL,
	PLL_TYPE_FPLL,
} PllType;

/* Fvco = in_freq * (n/m) */
typedef struct pll_dev {
	uint32_t base_addr;
	uint32_t m;
	uint32_t n;
	enum pll_type type;
} PllDev;

typedef enum pll_ref_clk {
	PLL_REF_XTAL, // 24M
	PLL_REF_CAS1, // 75M
	PLL_REF_CAS2, // 25M
	PLL_REF_CAS3, // 27M
} PllRefClk;

typedef enum pll_ref_div {
	PLL_RDIV_1 = 0,
	PLL_RDIV_2,
	PLL_RDIV_NULL,
	PLL_RDIV_4,
} PllRefDiv;

typedef enum pll_half_div {
	PLL_HDIV_1,
	PLL_HDIV_1P5,
	PLL_HDIV_2P5,
	PLL_HDIV_3P5,
} PllHalfDiv;

typedef enum pll_fout_src {
	PLL_FOUT_SRC_FPRE,
	PLL_FOUT_SRC_FHALF,
} PllFoutSrc;

typedef enum pll_fm_sel {
	PLL_FM_SEL_REF_CLK = 2,
	PLL_FM_SEL_FB,
	PLL_FM_SEL_PORT3,
	PLL_FM_SEL_PORT4,
	PLL_FM_SEL_PORT5,
	PLL_FM_SEL_PORT6
} PllFmSel;

typedef struct pll_port {
	int en;
	uint32_t div;
	enum pll_fout_src src;
} PllPort;

int32_t pll_set_ref_clk(struct pll_dev *dev, enum pll_ref_clk ref_clk);
void pll_en(struct pll_dev *dev, int en);
int32_t pll_set_port(struct pll_dev *dev, uint32_t id, int en, enum pll_fout_src clkin_sel, uint32_t div);
void pll_set_fm_sel(struct pll_dev *dev, int fm_en, enum pll_fm_sel fm_sel);
int32_t fpll_set_sscg(struct pll_dev *dev, int en, int deviation_unit, int freq_cnt);
int32_t ipll_startup(struct pll_dev *dev, enum pll_ref_clk ref_clk, enum pll_ref_div ref_div, uint32_t pre_div,
                     enum pll_half_div hdiv, struct pll_port p3, struct pll_port p4, struct pll_port p5);
int32_t fpll_startup(struct pll_dev *dev, int frac_en, enum pll_ref_clk ref_clk, enum pll_ref_div ref_div,
                     uint32_t pre_div, enum pll_half_div hdiv, struct pll_port p3, struct pll_port p4,
                     struct pll_port p5);
void hw_pll_print_info(void);
void pll_all_setting_by_uart_boot(int arg);
void pll_all_setting_new(void);

#endif
