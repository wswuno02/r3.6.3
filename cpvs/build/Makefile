################################################################################
# SDK related definitions
################################################################################

SDKSRC_DIR ?= $(realpath $(CURDIR)/../../)
include $(SDKSRC_DIR)/build/sdksrc.mk
include $(SDKSRC_DIR)/build/build_utils/lib.mk
include $(ABIVER_MAKE)

################################################################################
# Build tools and commands
################################################################################

### First, specify the build tools.

CC := $(CROSS_COMPILE)gcc
AS := $(CROSS_COMPILE)as
AR := $(CROSS_COMPILE)ar
NM := $(CROSS_COMPILE)nm
LD := $(CROSS_COMPILE)ld
OBJDUMP := $(CROSS_COMPILE)objdump
OBJCOPY := $(CROSS_COMPILE)objcopy
RANLIB := $(CROSS_COMPILE)ranlib
STRIP := $(CROSS_COMPILE)strip
RM := rm
LN := ln
MKDIR := mkdir
RMDIR := rmdir
CHMOD := chmod
INSTALL := install

### Then, define global compilation and linking flags.

CPVS_CHIP  := $(call chip,$(CONFIG_CHIP))

AFLAGS_ALL := -g -mcpu=$(CONFIG_TARGET_CPU)
CFLAGS_ALL := -g -mcpu=$(CONFIG_TARGET_CPU) \
              -fstrict-volatile-bitfields -mno-unaligned-access \
              -ffreestanding -ffunction-sections -fdata-sections -fno-short-enums \
              -MMD -O2 -std=gnu99 -fPIC

ifeq ($(CROSS_COMPILE), arm-linux-gnueabi-)
	LFLAGS_ALL := --gc-sections -L/tools/arm/gcc-linaro-4.9.4-2017.01-x86_64_arm-linux-gnueabi/lib/gcc/arm-linux-gnueabi/4.9.4 -lgcc
else ifeq ($(CROSS_COMPILE), arm-linux-gnueabihf-)
	LFLAGS_ALL := --gc-sections -L/tools/arm/gcc-linaro-4.9-2016.02-x86_64_arm-linux-gnueabihf/lib/gcc/arm-linux-gnueabihf/4.9.4 -lgcc
else
	LFLAGS_ALL := --gc-sections -L/tools/arm/arm-augentix-linux-gnueabi/lib/gcc/arm-augentix-linux-gnueabi/4.9.3 -lgcc
endif

### Finally, define complete commands for all actions.

ASSEMBLE = $(AS) $(AFLAGS_ALL) $(AFLAGS_LOCAL) -o $@ $<
COMPILE = $(CC) $(CFLAGS_ALL) $(CFLAGS_LOCAL) -o $@ -c $<
LINK = $(LD) -o $@ --start-group $^ --end-group $(LFLAGS_ALL) $(LFLAGS_LOCAL)
ARCHIVE_STATIC = $(AR) rcsD $@ $^
ARCHIVE_SHARED = $(CC) -shared -Wl,--build-id -Wl,--soname,$(basename $(basename $(notdir $@))) -o $@ \
		-Wl,--whole-archive $^ -Wl,--no-whole-archive

################################################################################
# Build rules
################################################################################

### First, define default build target.

all: primary

### Second, define variables for collecting outputs and temporarily objects.

TARGET_SLIBS  :=
TARGET_DLIBS  :=
TARGET_MODS   :=
TARGET_TESTS  :=
TARGET_DOCS   :=
CLEAN_FILES   :=
CLEAN_DIRS    :=
CLEAN_TARGETS :=

### Then, collect the knowledge about how to build all outputs.

root := $(realpath ..)
dir := $(root)/build

# FIXME: 'include Rules.mk' should independent from $(CONFIG_ARCH).
#        Then, target 'clean' & 'uninstall' can clean the files anytime.
ifeq ($(CONFIG_HC1703_1723_1753_1783S),y)
    ### CPVS is available in hc1703/23/53/83 only
ifneq ($(CONFIG_RELEASE),y)
    ### In SDK manifest, traverse directory tree to include Rules.mk files.
    subdir := $(root)/$(CPVS_CHIP)/src
    include $(subdir)/Rules.mk

    subdir := $(root)/$(CPVS_CHIP)/test
    include $(subdir)/Rules.mk

    subdir := $(root)/$(CPVS_CHIP)/doc
    include $(subdir)/Rules.mk
else
    ### In APP manifest, specify outputs manually.
    TARGET_SLIBS += $(root)/lib/libpid.a
    TARGET_SLIBS += $(root)/lib/libutil.a
    TARGET_DLIBS += $(root)/lib/libpid.so.$(REAL_SUFFIX)
    TARGET_DLIBS += $(root)/lib/libutil.so.$(REAL_SUFFIX)
    TARGET_MODS  += $(root)/ko/pid.ko
endif # ifneq ($(CONFIG_RELEASE),y)
endif # ifeq ($(CONFIG_HC1703_1723_1753_1783S),y)

### Finally, define essential build targets.

CLEAN_FILES += $(root)/include

.PHONY:	primary
ifeq ($(CONFIG_HC1703_1723_1753_1783S),y)
    ### CPVS is available in hc1703/23/53/83 only
ifneq ($(CONFIG_RELEASE),y)
    ### In SDK manifest, build primary outputs if necessary.
    primary: $(TARGET_SLIBS) $(TARGET_DLIBS) $(TARGET_MODS)
	$(Q)$(LN) -sfT $(CPVS_CHIP)/include $(root)/include
else
    ### In APP manifest, create link to pre-built primary outputs.
    primary:
	$(Q)$(LN) -sfT include.$(CPVS_CHIP) $(root)/include
	$(Q)$(LN) -sfT lib.$(CPVS_CHIP) $(root)/lib
	$(Q)$(LN) -sfT ko.$(CPVS_CHIP) $(root)/ko
endif # ifneq ($(CONFIG_RELEASE),y)
else
    primary:
endif # ifeq ($(CONFIG_HC1703_1723_1753_1783S),y)

.PHONY: test
ifeq ($(CONFIG_HC1703_1723_1753_1783S),y)
    ### CPVS is available in hc1703/23/53/83 only
ifneq ($(CONFIG_RELEASE),y)
    ### In SDK manifest, build the test outputs if necessary.
    test: all $(TARGET_TESTS)
else
    ### In APP manifest, do nothing.
    test:
endif # ifneq ($(CONFIG_RELEASE),y)
else
    test:
endif # ifeq ($(CONFIG_HC1703_1723_1753_1783S),y)

.PHONY: doc
ifeq ($(CONFIG_HC1703_1723_1753_1783S),y)
    ### CPVS is available in hc1703/23/53/83 only
ifneq ($(CONFIG_RELEASE),y)
    ### In SDK manifest, build the doc outputs if necessary.
    doc: $(TARGET_DOCS)
else
    ### In APP manifest, do nothing.
    doc:
endif # ifneq ($(CONFIG_RELEASE),y)
else
    doc:
endif # ifeq ($(CONFIG_HC1703_1723_1753_1783S),y)

# FIXME: Cannot clean targets generated from hc1703/23/53/83/ when $(CONFIG_HC1703_1723_1753_1783S) != y
.PHONY: clean
ifneq ($(CONFIG_RELEASE),y)
    ### In SDK manifest, remove primary outputs and temporarily objects.
    clean: $(CLEAN_TARGETS)
	@printf "  %-8s$(root)\n" "CLEAN"
	$(Q)$(RM) -f $(CLEAN_FILES)
	$(Q)$(foreach d, $(CLEAN_DIRS), $(RM) -rf $(d); )
else
    ### In APP manifest, remove the link to pre-build primary outputs.
    clean:
	@printf "  %-8s$(root)\n" "CLEAN"
	$(Q)$(RM) -f $(root)/include
	$(Q)$(RM) -f $(root)/lib
	$(Q)$(RM) -f $(root)/ko
endif

.PHONY: distclean
distclean: clean

.PHONY: install
ifeq ($(CONFIG_HC1703_1723_1753_1783S),y)
    ### CPVS is available in hc1703/23/53/83 only
    install: all
	$(Q)$(MKDIR) -p $(CUSTOM_LIB)
	$(Q)$(foreach f, $(TARGET_DLIBS), \
		printf "  %-8s$(CUSTOM_LIB)/$(notdir $(f))\n" "INSTALL"; \
		find $(CUSTOM_LIB) -type f \( -name $(basename $(basename $(basename $(notdir $(f))))).* -not -name $(notdir $(f)) \) -exec rm -rf {} \;; \
		$(INSTALL) -m 644 -t $(CUSTOM_LIB) $(f); \
		$(LN) -sfT $(notdir $(f)) $(CUSTOM_LIB)/$(basename $(basename $(notdir $(f)))); \
	)
	$(Q)$(foreach f, $(TARGET_MODS), \
		printf "  %-8s$(CUSTOM_LIB)/$(notdir $(f))\n" "INSTALL"; \
		$(INSTALL) -m 644 -t $(CUSTOM_LIB) $(f); \
	)
else
    install:
endif

# FIXME: Cannot uninstall targets generated from hc1703/23/53/83/ when $(CONFIG_HC1703_1723_1753_1783S) != y
.PHONY: uninstall
uninstall:
	$(Q)$(foreach f, $(TARGET_DLIBS), \
		printf "  %-8s$(CUSTOM_LIB)/$(notdir $(f))\n" "RM"; \
		$(RM) -f $(CUSTOM_LIB)/$(basename $(basename $(basename $(notdir $(f))))).*; \
	)
	$(Q)$(foreach f, $(TARGET_MODS), \
		printf "  %-8s$(CUSTOM_LIB)/$(notdir $(f))\n" "RM"; \
		$(RM) -f $(CUSTOM_LIB)/$(notdir $(f)); \
	)
	$(Q)if [ -d $(CUSTOM_LIB) ]; then \
		$(RMDIR) --ignore-fail-on-non-empty $(CUSTOM_LIB); \
	fi

.PHONY: help
help:
	@echo ""
	@echo "  Syntax:"
	@echo "    make [options] <target>"
	@echo ""
	@echo "  Options:"
	@echo "    V=0         Hide command lines. (default)"
	@echo "    V=1         Show command lines."
	@echo ""
	@echo "  Targets:"
	@echo "    all         Build primary outputs. (default)"
	@echo "    test        Build test programs."
	@echo "    doc         Build documents."
	@echo "    clean       Clean all outputs."
	@echo "    distclean   Clean all outputs and configurations."
	@echo "    install     Install primary outputs"
	@echo "    uninstall   Uninstall primary outputs"
	@echo "    help        Show help messages."
	@echo ""

################################################################################
# General rules
################################################################################

%.obj: %.c
	@printf "  %-8s$@\n" "CC"
	$(Q)$(COMPILE)

%.obj: %.S
	@printf "  %-8s$@\n" "AS"
	$(Q)$(ASSEMBLE)

################################################################################
# Quiet options
################################################################################

V ?= 0
ifeq ($(V),1)
    Q :=
    VOUT :=
else
    Q := @
    VOUT := 2>&1 1>/dev/null
endif

################################################################################
# Prevent make from removing any build targets, including intermediate ones
################################################################################

.SECONDARY: $(CLEAN_FILES) $(CLEAN_DIRS)

