#ifndef KYOTO_CCM_H_
#define KYOTO_CCM_H_

#include "isp_utils.h"
#include "csr_bank_ccm.h"

typedef struct isp_ccm_cfg {
	uint16_t coeff_2s[COLOR_CHN_NUM * COLOR_CHN_NUM]; /**< Matrix for calibrate the image color in RGB color space */
	uint16_t offset_2s[COLOR_CHN_NUM]; /**< Set offset */
} IspCcmCfg;

/* CCM */
void hw_isp_ccm_set_cfg(const struct isp_ccm_cfg *cfg);
void hw_isp_ccm_get_cfg(struct isp_ccm_cfg *cfg);

/* Debug */
void hw_isp_ccm_set_dbg_mon_sel(uint8_t debug_mon_sel);
uint8_t hw_isp_ccm_get_dbg_mon_sel(void);

//
/* CCM */
void isp_ccm_set_cfg(volatile CsrBankCcm *csr, const struct isp_ccm_cfg *cfg);
void isp_ccm_get_cfg(volatile CsrBankCcm *csr, struct isp_ccm_cfg *cfg);

/* Debug */
void isp_ccm_set_dbg_mon_sel(volatile CsrBankCcm *csr, uint8_t debug_mon_sel);
uint8_t isp_ccm_get_dbg_mon_sel(volatile CsrBankCcm *csr);

/* TODO - Move to test */
void hw_isp_ccm_bypass(void);

#endif /* KYOTO_CCM_H_ */