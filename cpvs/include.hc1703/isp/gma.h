#ifndef KYOTO_GMA_H_
#define KYOTO_GMA_H_

#include "isp_utils.h"
#include "csr_bank_gma.h"

typedef enum isp_gma_mode {
	ISP_GMA_MODE_NORMAL = 0,
	ISP_GMA_MODE_DISABLE = 1,
	ISP_GMA_MODE_NUM = 2,
} IspGmaMode;

typedef struct isp_gma_cfg {
	enum isp_gma_mode mode;
	struct gma_curve {
		uint32_t val[GMA_CURVE_ENTRY_NUM];
	} curve; /**< Gamma curve */
} IspGmaCfg;

/* GMA */
void hw_isp_gma_set_cfg(const struct isp_gma_cfg *cfg);
void hw_isp_gma_get_cfg(struct isp_gma_cfg *cfg);

/* Debug */
void hw_isp_gma_set_dbg_mon_sel(uint8_t debug_mon_sel);
uint8_t hw_isp_gma_get_dbg_mon_sel(void);

//
/* GMA */
void isp_gma_set_cfg(volatile CsrBankGma *csr, const struct isp_gma_cfg *cfg);
void isp_gma_get_cfg(volatile CsrBankGma *csr, struct isp_gma_cfg *cfg);

/* Debug */
void isp_gma_set_dbg_mon_sel(volatile CsrBankGma *csr, uint8_t debug_mon_sel);
uint8_t isp_gma_get_dbg_mon_sel(volatile CsrBankGma *csr);

/* TODO - Move to test */
void hw_isp_gma_bypass(void);

#endif /* KYOTO_GMA_H_ */