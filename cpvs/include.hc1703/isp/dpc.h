#ifndef KYOTO_DPC_H_
#define KYOTO_DPC_H_

#include "isp_utils.h"
#include "csr_bank_dpc.h"

typedef enum isp_dpc_opt {
	ISP_DPC_OPT_DPC = 0,
	ISP_DPC_OPT_YUV420_TO_YUV444 = 1,
	ISP_DPC_OPT_VERTICAL_DEINTERLACING = 2,
	ISP_DPC_OPT_NUM = 3,
} IspDpcOpt;

typedef struct isp_dpc_path_sel {
	enum isp_dpc_opt operation;
	uint8_t yuv_chroma_pos;
	uint8_t input_swap;
	uint8_t output_swap;
	uint8_t main_sub_align;
	uint8_t main_hdi_en;
	uint8_t main_hdi_at_left;
	uint8_t sub_hdi_en;
	uint8_t sub_hdi_at_left;
} IspDpcPathSel;

typedef enum isp_dpc_mode {
	ISP_DPC_MODE_NORMAL = 0,
	ISP_DPC_MODE_MONO = 1,
	ISP_DPC_MODE_DISABLE = 2,
	ISP_DPC_MODE_BYPASS = 3,
	ISP_DPC_MODE_NUM = 4,
} IspDpcMode;

typedef struct isp_dpc_cfg {
	enum isp_dpc_mode dpc_mode;
	uint8_t g_dislike_th;
	uint8_t br_dislike_th;
	uint8_t g_correct_overshoot;
	uint8_t br_correct_overshoot;
	uint8_t g_sad_gain;
	uint8_t br_sad_gain;
	uint8_t correct_bright_en;
	uint8_t correct_dark_en;
	uint8_t correct_mid_en;
	uint8_t bright_th;
	uint8_t dark_th;
} IspDpcCfg;

/* Frame start */
void hw_isp_dpc_set_frame_start(void);

/* IRQ clear */
void hw_isp_dpc_set_irq_clear_frame_end(void);

/* Frame end*/
uint8_t hw_isp_dpc_get_status_frame_end(void);

/* IRQ mask */
void hw_isp_dpc_set_irq_mask_frame_end(uint8_t irq_mask_frame_end);
uint8_t hw_isp_dpc_get_irq_mask_frame_end(void);

/* Resolution */
void hw_isp_dpc_set_res(const struct res *res);
void hw_isp_dpc_get_res(struct res *res);

/* Bayer */
void hw_isp_dpc_set_ini_bayer_phase(enum ini_bayer_phase bayer);
enum ini_bayer_phase hw_isp_dpc_get_ini_bayer_phase(void);

/* Path selection */
void hw_isp_dpc_set_path_sel(const struct isp_dpc_path_sel *cfg);
void hw_isp_dpc_get_path_sel(struct isp_dpc_path_sel *cfg);

/* DPC */
void hw_isp_dpc_set_cfg(const struct isp_dpc_cfg *cfg);
void hw_isp_dpc_get_cfg(struct isp_dpc_cfg *cfg);

/* Debug */
void hw_isp_dpc_set_dbg_mon_sel(uint8_t debug_mon_sel);
uint8_t hw_isp_dpc_get_dbg_mon_sel(void);
void hw_isp_dpc_set_dbg_mon_sel_dpc(uint8_t debug_mon_sel_dpc);
uint8_t hw_isp_dpc_get_dbg_mon_sel_dpc(void);
void hw_isp_dpc_set_dbg_mon_sel_vdi(uint8_t debug_mon_sel_vdi);
uint8_t hw_isp_dpc_get_dbg_mon_sel_vdi(void);

/* Reserved */
void hw_isp_dpc_set_reserved(uint32_t reserved);
uint32_t hw_isp_dpc_get_reserved(void);

//
/* IRQ mask */
void isp_dpc_set_irq_mask_frame_end(volatile CsrBankDpc *csr, uint8_t irq_mask_frame_end);
uint8_t isp_dpc_get_irq_mask_frame_end(volatile CsrBankDpc *csr);

/* Resolution */
void isp_dpc_set_res(volatile CsrBankDpc *csr, const struct res *res);
void isp_dpc_get_res(volatile CsrBankDpc *csr, struct res *res);

/* Bayer */
void isp_dpc_set_ini_bayer_phase(volatile CsrBankDpc *csr, enum ini_bayer_phase bayer);
enum ini_bayer_phase isp_dpc_get_ini_bayer_phase(volatile CsrBankDpc *csr);

/* Path selection */
void isp_dpc_set_path_sel(volatile CsrBankDpc *csr, const struct isp_dpc_path_sel *cfg);
void isp_dpc_get_path_sel(volatile CsrBankDpc *csr, struct isp_dpc_path_sel *cfg);

/* DPC */
void isp_dpc_set_cfg(volatile CsrBankDpc *csr, const struct isp_dpc_cfg *cfg);
void isp_dpc_get_cfg(volatile CsrBankDpc *csr, struct isp_dpc_cfg *cfg);

/* Debug */
void isp_dpc_set_dbg_mon_sel(volatile CsrBankDpc *csr, uint8_t debug_mon_sel);
uint8_t isp_dpc_get_dbg_mon_sel(volatile CsrBankDpc *csr);
void isp_dpc_set_dbg_mon_sel_dpc(volatile CsrBankDpc *csr, uint8_t debug_mon_sel_dpc);
uint8_t isp_dpc_get_dbg_mon_sel_dpc(volatile CsrBankDpc *csr);
void isp_dpc_set_dbg_mon_sel_vdi(volatile CsrBankDpc *csr, uint8_t debug_mon_sel_vdi);
uint8_t isp_dpc_get_dbg_mon_sel_vdi(volatile CsrBankDpc *csr);

/* Reserved */
void isp_dpc_set_reserved(volatile CsrBankDpc *csr, uint32_t reserved);
uint32_t isp_dpc_get_reserved(volatile CsrBankDpc *csr);

/* TODO - Move to test */
void hw_isp_dpc_res(uint32_t width, uint32_t height);
void hw_isp_dpc_bypass(void);
void hw_isp_dpc_for_hdr(void);

#endif /* KYOTO_DPC_H_ */