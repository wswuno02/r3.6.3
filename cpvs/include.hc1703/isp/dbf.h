#ifndef KYOTO_DBF_H_
#define KYOTO_DBF_H_

#include "isp_utils.h"
#include "csr_bank_dbf.h"

typedef struct isp_dbf_cfg {
	uint8_t enable;
	uint8_t filter_type;
	uint8_t range;
	uint8_t alpha_slope;
	uint8_t alpha_max;
} IspDbfCfg;

/* DBF */
void hw_isp_dbf_set_cfg(const struct isp_dbf_cfg *cfg);
void hw_isp_dbf_get_cfg(struct isp_dbf_cfg *cfg);
void hw_isp_dbf_set_width(uint16_t width);
uint16_t hw_isp_dbf_get_width(void);
void hw_isp_dbf_set_block_x(uint16_t block_x);
uint16_t hw_isp_dbf_get_block_x(void);

/* Debug */
void hw_isp_dbf_set_dbg_mon_sel(uint8_t debug_mon_sel);
uint8_t hw_isp_dbf_get_dbg_mon_sel(void);

//
/* DBF */
void isp_dbf_set_cfg(volatile CsrBankDbf *csr, const struct isp_dbf_cfg *cfg);
void isp_dbf_get_cfg(volatile CsrBankDbf *csr, struct isp_dbf_cfg *cfg);
void isp_dbf_set_width(volatile CsrBankDbf *csr, uint16_t width);
uint16_t isp_dbf_get_width(volatile CsrBankDbf *csr);
void isp_dbf_set_block_x(volatile CsrBankDbf *csr, uint16_t block_x);
uint16_t isp_dbf_get_block_x(volatile CsrBankDbf *csr);

/* Debug */
void isp_dbf_set_dbg_mon_sel(volatile CsrBankDbf *csr, uint8_t debug_mon_sel);
uint8_t isp_dbf_get_dbg_mon_sel(volatile CsrBankDbf *csr);

/* TODO - Move to test */
void hw_isp_dbf_bypass(void);

#endif /* KYOTO_DBF_H_ */