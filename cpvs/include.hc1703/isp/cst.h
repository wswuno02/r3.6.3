#ifndef KYOTO_CST_H_
#define KYOTO_CST_H_

#include "isp_utils.h"
#include "csr_bank_cst.h"

typedef struct isp_cst_cfg {
	uint16_t coeff_2s[COLOR_CHN_NUM * COLOR_CHN_NUM]; /**< Matrix for RGB to YUV */
	uint16_t offset_2s[COLOR_CHN_NUM]; /**< Set offset */
} IspCstCfg;

/* CST */
void hw_isp_cst_set_cfg(const struct isp_cst_cfg *cfg);
void hw_isp_cst_get_cfg(struct isp_cst_cfg *cfg);

/* Debug */
void hw_isp_cst_set_dbg_mon_sel(uint8_t debug_mon_sel);
uint8_t hw_isp_cst_get_dbg_mon_sel(void);

/* Reserved */
void hw_isp_cst_set_reserved(uint32_t reserved);
uint32_t hw_isp_cst_get_reserved(void);

//
/* CST */
void isp_cst_set_cfg(volatile CsrBankCst *csr, const struct isp_cst_cfg *cfg);
void isp_cst_get_cfg(volatile CsrBankCst *csr, struct isp_cst_cfg *cfg);

/* Debug */
void isp_cst_set_dbg_mon_sel(volatile CsrBankCst *csr, uint8_t debug_mon_sel);
uint8_t isp_cst_get_dbg_mon_sel(volatile CsrBankCst *csr);

/* Reserved */
void isp_cst_set_reserved(volatile CsrBankCst *csr, uint32_t reserved);
uint32_t isp_cst_get_reserved(volatile CsrBankCst *csr);

/* TODO - Move to test */
void hw_isp_cst_bypass(void);

#endif /* KYOTO_CST_H_ */