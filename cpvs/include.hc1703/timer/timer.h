#ifndef __TIMER__H__
#define __TIMER__H__

#include <stdint.h>
#include <time.h>

#define TIMER0_BASE_ADDR 0x80050000
#define TIMER1_BASE_ADDR 0x80050020
#define TIMER2_BASE_ADDR 0x80050040
#define TIMER3_BASE_ADDR 0x80050060

#define TIMER_MODE__ONESHOT  0x0
#define TIMER_MODE__PERIODIC 0x1

#define TIMER_REF_CLK_24MHZ 24000000
#define MAX_PRESCALER 0xFF
#define MIN_PRESCALER 0x00
#define MAX_COUNT__LOAD 0xFFFFFFFE
#define MIN_COUNT__LOAD 0x1

typedef struct timer_dev {
    uint32_t ref_clk_rate;
    uint32_t mode;
    uint32_t counting_rate;
    struct timespec counting_times;
} TimerDev;

void timer_prevent_overflow(uint32_t *para, uint32_t *rnd, uint32_t *div);
void timer_init(struct timer_dev *dev);
void timer_trigger();
void timer_mask(int32_t en);
void timer_clr__match();
uint32_t timer_status();
uint32_t timer_cnt_val();
void timer_extremum_init();

#endif  /* __TIMER__H__ */