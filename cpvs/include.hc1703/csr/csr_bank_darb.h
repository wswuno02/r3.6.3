#ifndef CSR_BANK_DARB_H_
#define CSR_BANK_DARB_H_

#ifndef __KERNEL__
#include <stdint.h>
#else
#include <linux/types.h>
#endif

/***  C struct generated from darb  ***/
typedef struct csr_bank_darb {
	/* CHKSUM_CLEAR 9'h000 */
	union {
		uint32_t chksum_clear; // word name
		struct {
			uint32_t checksum_0_clear : 1;
			uint32_t : 7; // padding bits
			uint32_t checksum_1_clear : 1;
			uint32_t : 7; // padding bits
			uint32_t checksum_2_clear : 1;
			uint32_t : 7; // padding bits
			uint32_t checksum_3_clear : 1;
			uint32_t : 7; // padding bits
		};
	};
	/* IRQ_CLEAR_A 9'h004 */
	union {
		uint32_t irq_clear_a; // word name
		struct {
			uint32_t irq_clear_w_resp_error : 1;
			uint32_t : 7; // padding bits
			uint32_t irq_clear_r_resp_error : 1;
			uint32_t : 7; // padding bits
			uint32_t irq_clear_timeout_w_addr : 1;
			uint32_t : 7; // padding bits
			uint32_t irq_clear_timeout_w_data : 1;
			uint32_t : 7; // padding bits
		};
	};
	/* IRQ_CLEAR_B 9'h008 */
	union {
		uint32_t irq_clear_b; // word name
		struct {
			uint32_t irq_clear_timeout_w_resp : 1;
			uint32_t : 7; // padding bits
			uint32_t irq_clear_timeout_r_addr : 1;
			uint32_t : 7; // padding bits
			uint32_t irq_clear_timeout_r_data : 1;
			uint32_t : 7; // padding bits
			uint32_t : 8; // padding bits
		};
	};
	/* STATUS_A 9'h00C */
	union {
		uint32_t status_a; // word name
		struct {
			uint32_t status_w_resp_error : 1;
			uint32_t : 7; // padding bits
			uint32_t status_r_resp_error : 1;
			uint32_t : 7; // padding bits
			uint32_t status_timeout_w_addr : 1;
			uint32_t : 7; // padding bits
			uint32_t status_timeout_w_data : 1;
			uint32_t : 7; // padding bits
		};
	};
	/* STATUS_B 9'h010 */
	union {
		uint32_t status_b; // word name
		struct {
			uint32_t status_timeout_w_resp : 1;
			uint32_t : 7; // padding bits
			uint32_t status_timeout_r_addr : 1;
			uint32_t : 7; // padding bits
			uint32_t status_timeout_r_data : 1;
			uint32_t : 7; // padding bits
			uint32_t : 8; // padding bits
		};
	};
	/* IRQ_MASK_A 9'h014 */
	union {
		uint32_t irq_mask_a; // word name
		struct {
			uint32_t irq_mask_w_resp_error : 1;
			uint32_t : 7; // padding bits
			uint32_t irq_mask_r_resp_error : 1;
			uint32_t : 7; // padding bits
			uint32_t irq_mask_timeout_w_addr : 1;
			uint32_t : 7; // padding bits
			uint32_t irq_mask_timeout_w_data : 1;
			uint32_t : 7; // padding bits
		};
	};
	/* IRQ_MASK_B 9'h018 */
	union {
		uint32_t irq_mask_b; // word name
		struct {
			uint32_t irq_mask_timeout_w_resp : 1;
			uint32_t : 7; // padding bits
			uint32_t irq_mask_timeout_r_addr : 1;
			uint32_t : 7; // padding bits
			uint32_t irq_mask_timeout_r_data : 1;
			uint32_t : 7; // padding bits
			uint32_t : 8; // padding bits
		};
	};
	/* PRIORITY_W_0 9'h01C */
	union {
		uint32_t priority_w_0; // word name
		struct {
			uint32_t priority_w_00 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_01 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_02 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_03 : 2;
			uint32_t : 6; // padding bits
		};
	};
	/* PRIORITY_W_1 9'h020 */
	union {
		uint32_t priority_w_1; // word name
		struct {
			uint32_t priority_w_04 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_05 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_06 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_07 : 2;
			uint32_t : 6; // padding bits
		};
	};
	/* PRIORITY_W_2 9'h024 */
	union {
		uint32_t priority_w_2; // word name
		struct {
			uint32_t priority_w_08 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_09 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_10 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_11 : 2;
			uint32_t : 6; // padding bits
		};
	};
	/* PRIORITY_W_3 9'h028 */
	union {
		uint32_t priority_w_3; // word name
		struct {
			uint32_t priority_w_12 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_13 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_14 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_15 : 2;
			uint32_t : 6; // padding bits
		};
	};
	/* PRIORITY_W_4 9'h02C */
	union {
		uint32_t priority_w_4; // word name
		struct {
			uint32_t priority_w_16 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_17 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_18 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_19 : 2;
			uint32_t : 6; // padding bits
		};
	};
	/* PRIORITY_W_5 9'h030 */
	union {
		uint32_t priority_w_5; // word name
		struct {
			uint32_t priority_w_20 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_21 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_22 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_23 : 2;
			uint32_t : 6; // padding bits
		};
	};
	/* PRIORITY_W_6 9'h034 */
	union {
		uint32_t priority_w_6; // word name
		struct {
			uint32_t priority_w_24 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_25 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_26 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_27 : 2;
			uint32_t : 6; // padding bits
		};
	};
	/* PRIORITY_W_7 9'h038 */
	union {
		uint32_t priority_w_7; // word name
		struct {
			uint32_t priority_w_28 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_29 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_30 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_w_31 : 2;
			uint32_t : 6; // padding bits
		};
	};
	/* PRIORITY_R_0 9'h03C */
	union {
		uint32_t priority_r_0; // word name
		struct {
			uint32_t priority_r_00 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_01 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_02 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_03 : 2;
			uint32_t : 6; // padding bits
		};
	};
	/* PRIORITY_R_1 9'h040 */
	union {
		uint32_t priority_r_1; // word name
		struct {
			uint32_t priority_r_04 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_05 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_06 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_07 : 2;
			uint32_t : 6; // padding bits
		};
	};
	/* PRIORITY_R_2 9'h044 */
	union {
		uint32_t priority_r_2; // word name
		struct {
			uint32_t priority_r_08 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_09 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_10 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_11 : 2;
			uint32_t : 6; // padding bits
		};
	};
	/* PRIORITY_R_3 9'h048 */
	union {
		uint32_t priority_r_3; // word name
		struct {
			uint32_t priority_r_12 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_13 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_14 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_15 : 2;
			uint32_t : 6; // padding bits
		};
	};
	/* PRIORITY_R_4 9'h04C */
	union {
		uint32_t priority_r_4; // word name
		struct {
			uint32_t priority_r_16 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_17 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_18 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_19 : 2;
			uint32_t : 6; // padding bits
		};
	};
	/* PRIORITY_R_5 9'h050 */
	union {
		uint32_t priority_r_5; // word name
		struct {
			uint32_t priority_r_20 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_21 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_22 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_23 : 2;
			uint32_t : 6; // padding bits
		};
	};
	/* PRIORITY_R_6 9'h054 */
	union {
		uint32_t priority_r_6; // word name
		struct {
			uint32_t priority_r_24 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_25 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_26 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_27 : 2;
			uint32_t : 6; // padding bits
		};
	};
	/* PRIORITY_R_7 9'h058 */
	union {
		uint32_t priority_r_7; // word name
		struct {
			uint32_t priority_r_28 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_29 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_30 : 2;
			uint32_t : 6; // padding bits
			uint32_t priority_r_31 : 2;
			uint32_t : 6; // padding bits
		};
	};
	/* INI_SCORE_W_0 9'h05C */
	union {
		uint32_t ini_score_w_0; // word name
		struct {
			uint32_t ini_score_w_00 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_01 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_02 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_03 : 5;
			uint32_t : 3; // padding bits
		};
	};
	/* INI_SCORE_W_1 9'h060 */
	union {
		uint32_t ini_score_w_1; // word name
		struct {
			uint32_t ini_score_w_04 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_05 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_06 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_07 : 5;
			uint32_t : 3; // padding bits
		};
	};
	/* INI_SCORE_W_2 9'h064 */
	union {
		uint32_t ini_score_w_2; // word name
		struct {
			uint32_t ini_score_w_08 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_09 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_10 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_11 : 5;
			uint32_t : 3; // padding bits
		};
	};
	/* INI_SCORE_W_3 9'h068 */
	union {
		uint32_t ini_score_w_3; // word name
		struct {
			uint32_t ini_score_w_12 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_13 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_14 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_15 : 5;
			uint32_t : 3; // padding bits
		};
	};
	/* INI_SCORE_W_4 9'h06C */
	union {
		uint32_t ini_score_w_4; // word name
		struct {
			uint32_t ini_score_w_16 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_17 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_18 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_19 : 5;
			uint32_t : 3; // padding bits
		};
	};
	/* INI_SCORE_W_5 9'h070 */
	union {
		uint32_t ini_score_w_5; // word name
		struct {
			uint32_t ini_score_w_20 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_21 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_22 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_23 : 5;
			uint32_t : 3; // padding bits
		};
	};
	/* INI_SCORE_W_6 9'h074 */
	union {
		uint32_t ini_score_w_6; // word name
		struct {
			uint32_t ini_score_w_24 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_25 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_26 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_27 : 5;
			uint32_t : 3; // padding bits
		};
	};
	/* INI_SCORE_W_7 9'h078 */
	union {
		uint32_t ini_score_w_7; // word name
		struct {
			uint32_t ini_score_w_28 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_29 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_30 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_w_31 : 5;
			uint32_t : 3; // padding bits
		};
	};
	/* INI_SCORE_R_0 9'h07C */
	union {
		uint32_t ini_score_r_0; // word name
		struct {
			uint32_t ini_score_r_00 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_01 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_02 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_03 : 5;
			uint32_t : 3; // padding bits
		};
	};
	/* INI_SCORE_R_1 9'h080 */
	union {
		uint32_t ini_score_r_1; // word name
		struct {
			uint32_t ini_score_r_04 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_05 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_06 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_07 : 5;
			uint32_t : 3; // padding bits
		};
	};
	/* INI_SCORE_R_2 9'h084 */
	union {
		uint32_t ini_score_r_2; // word name
		struct {
			uint32_t ini_score_r_08 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_09 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_10 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_11 : 5;
			uint32_t : 3; // padding bits
		};
	};
	/* INI_SCORE_R_3 9'h088 */
	union {
		uint32_t ini_score_r_3; // word name
		struct {
			uint32_t ini_score_r_12 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_13 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_14 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_15 : 5;
			uint32_t : 3; // padding bits
		};
	};
	/* INI_SCORE_R_4 9'h08C */
	union {
		uint32_t ini_score_r_4; // word name
		struct {
			uint32_t ini_score_r_16 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_17 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_18 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_19 : 5;
			uint32_t : 3; // padding bits
		};
	};
	/* INI_SCORE_R_5 9'h090 */
	union {
		uint32_t ini_score_r_5; // word name
		struct {
			uint32_t ini_score_r_20 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_21 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_22 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_23 : 5;
			uint32_t : 3; // padding bits
		};
	};
	/* INI_SCORE_R_6 9'h094 */
	union {
		uint32_t ini_score_r_6; // word name
		struct {
			uint32_t ini_score_r_24 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_25 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_26 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_27 : 5;
			uint32_t : 3; // padding bits
		};
	};
	/* INI_SCORE_R_7 9'h098 */
	union {
		uint32_t ini_score_r_7; // word name
		struct {
			uint32_t ini_score_r_28 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_29 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_30 : 5;
			uint32_t : 3; // padding bits
			uint32_t ini_score_r_31 : 5;
			uint32_t : 3; // padding bits
		};
	};
	/* WAIT_CNT_W_0 9'h09C */
	union {
		uint32_t wait_cnt_w_0; // word name
		struct {
			uint32_t wait_cnt_norm_w_00 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_01 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_02 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_03 : 3;
			uint32_t : 5; // padding bits
		};
	};
	/* WAIT_CNT_W_1 9'h0A0 */
	union {
		uint32_t wait_cnt_w_1; // word name
		struct {
			uint32_t wait_cnt_norm_w_04 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_05 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_06 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_07 : 3;
			uint32_t : 5; // padding bits
		};
	};
	/* WAIT_CNT_W_2 9'h0A4 */
	union {
		uint32_t wait_cnt_w_2; // word name
		struct {
			uint32_t wait_cnt_norm_w_08 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_09 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_10 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_11 : 3;
			uint32_t : 5; // padding bits
		};
	};
	/* WAIT_CNT_W_3 9'h0A8 */
	union {
		uint32_t wait_cnt_w_3; // word name
		struct {
			uint32_t wait_cnt_norm_w_12 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_13 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_14 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_15 : 3;
			uint32_t : 5; // padding bits
		};
	};
	/* WAIT_CNT_W_4 9'h0AC */
	union {
		uint32_t wait_cnt_w_4; // word name
		struct {
			uint32_t wait_cnt_norm_w_16 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_17 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_18 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_19 : 3;
			uint32_t : 5; // padding bits
		};
	};
	/* WAIT_CNT_W_5 9'h0B0 */
	union {
		uint32_t wait_cnt_w_5; // word name
		struct {
			uint32_t wait_cnt_norm_w_20 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_21 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_22 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_23 : 3;
			uint32_t : 5; // padding bits
		};
	};
	/* WAIT_CNT_W_6 9'h0B4 */
	union {
		uint32_t wait_cnt_w_6; // word name
		struct {
			uint32_t wait_cnt_norm_w_24 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_25 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_26 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_27 : 3;
			uint32_t : 5; // padding bits
		};
	};
	/* WAIT_CNT_W_7 9'h0B8 */
	union {
		uint32_t wait_cnt_w_7; // word name
		struct {
			uint32_t wait_cnt_norm_w_28 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_29 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_30 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_w_31 : 3;
			uint32_t : 5; // padding bits
		};
	};
	/* WAIT_CNT_R_0 9'h0BC */
	union {
		uint32_t wait_cnt_r_0; // word name
		struct {
			uint32_t wait_cnt_norm_r_00 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_01 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_02 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_03 : 3;
			uint32_t : 5; // padding bits
		};
	};
	/* WAIT_CNT_R_1 9'h0C0 */
	union {
		uint32_t wait_cnt_r_1; // word name
		struct {
			uint32_t wait_cnt_norm_r_04 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_05 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_06 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_07 : 3;
			uint32_t : 5; // padding bits
		};
	};
	/* WAIT_CNT_R_2 9'h0C4 */
	union {
		uint32_t wait_cnt_r_2; // word name
		struct {
			uint32_t wait_cnt_norm_r_08 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_09 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_10 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_11 : 3;
			uint32_t : 5; // padding bits
		};
	};
	/* WAIT_CNT_R_3 9'h0C8 */
	union {
		uint32_t wait_cnt_r_3; // word name
		struct {
			uint32_t wait_cnt_norm_r_12 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_13 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_14 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_15 : 3;
			uint32_t : 5; // padding bits
		};
	};
	/* WAIT_CNT_R_4 9'h0CC */
	union {
		uint32_t wait_cnt_r_4; // word name
		struct {
			uint32_t wait_cnt_norm_r_16 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_17 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_18 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_19 : 3;
			uint32_t : 5; // padding bits
		};
	};
	/* WAIT_CNT_R_5 9'h0D0 */
	union {
		uint32_t wait_cnt_r_5; // word name
		struct {
			uint32_t wait_cnt_norm_r_20 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_21 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_22 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_23 : 3;
			uint32_t : 5; // padding bits
		};
	};
	/* WAIT_CNT_R_6 9'h0D4 */
	union {
		uint32_t wait_cnt_r_6; // word name
		struct {
			uint32_t wait_cnt_norm_r_24 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_25 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_26 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_27 : 3;
			uint32_t : 5; // padding bits
		};
	};
	/* WAIT_CNT_R_7 9'h0D8 */
	union {
		uint32_t wait_cnt_r_7; // word name
		struct {
			uint32_t wait_cnt_norm_r_28 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_29 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_30 : 3;
			uint32_t : 5; // padding bits
			uint32_t wait_cnt_norm_r_31 : 3;
			uint32_t : 5; // padding bits
		};
	};
	/* PAGE_MATCH_W_0 9'h0DC */
	union {
		uint32_t page_match_w_0; // word name
		struct {
			uint32_t page_match_score_w_00 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_01 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_02 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_03 : 4;
			uint32_t : 4; // padding bits
		};
	};
	/* PAGE_MATCH_W_1 9'h0E0 */
	union {
		uint32_t page_match_w_1; // word name
		struct {
			uint32_t page_match_score_w_04 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_05 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_06 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_07 : 4;
			uint32_t : 4; // padding bits
		};
	};
	/* PAGE_MATCH_W_2 9'h0E4 */
	union {
		uint32_t page_match_w_2; // word name
		struct {
			uint32_t page_match_score_w_08 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_09 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_10 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_11 : 4;
			uint32_t : 4; // padding bits
		};
	};
	/* PAGE_MATCH_W_3 9'h0E8 */
	union {
		uint32_t page_match_w_3; // word name
		struct {
			uint32_t page_match_score_w_12 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_13 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_14 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_15 : 4;
			uint32_t : 4; // padding bits
		};
	};
	/* PAGE_MATCH_W_4 9'h0EC */
	union {
		uint32_t page_match_w_4; // word name
		struct {
			uint32_t page_match_score_w_16 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_17 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_18 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_19 : 4;
			uint32_t : 4; // padding bits
		};
	};
	/* PAGE_MATCH_W_5 9'h0F0 */
	union {
		uint32_t page_match_w_5; // word name
		struct {
			uint32_t page_match_score_w_20 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_21 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_22 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_23 : 4;
			uint32_t : 4; // padding bits
		};
	};
	/* PAGE_MATCH_W_6 9'h0F4 */
	union {
		uint32_t page_match_w_6; // word name
		struct {
			uint32_t page_match_score_w_24 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_25 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_26 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_27 : 4;
			uint32_t : 4; // padding bits
		};
	};
	/* PAGE_MATCH_W_7 9'h0F8 */
	union {
		uint32_t page_match_w_7; // word name
		struct {
			uint32_t page_match_score_w_28 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_29 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_30 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_w_31 : 4;
			uint32_t : 4; // padding bits
		};
	};
	/* PAGE_MATCH_R_0 9'h0FC */
	union {
		uint32_t page_match_r_0; // word name
		struct {
			uint32_t page_match_score_r_00 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_01 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_02 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_03 : 4;
			uint32_t : 4; // padding bits
		};
	};
	/* PAGE_MATCH_R_1 9'h100 */
	union {
		uint32_t page_match_r_1; // word name
		struct {
			uint32_t page_match_score_r_04 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_05 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_06 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_07 : 4;
			uint32_t : 4; // padding bits
		};
	};
	/* PAGE_MATCH_R_2 9'h104 */
	union {
		uint32_t page_match_r_2; // word name
		struct {
			uint32_t page_match_score_r_08 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_09 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_10 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_11 : 4;
			uint32_t : 4; // padding bits
		};
	};
	/* PAGE_MATCH_R_3 9'h108 */
	union {
		uint32_t page_match_r_3; // word name
		struct {
			uint32_t page_match_score_r_12 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_13 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_14 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_15 : 4;
			uint32_t : 4; // padding bits
		};
	};
	/* PAGE_MATCH_R_4 9'h10C */
	union {
		uint32_t page_match_r_4; // word name
		struct {
			uint32_t page_match_score_r_16 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_17 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_18 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_19 : 4;
			uint32_t : 4; // padding bits
		};
	};
	/* PAGE_MATCH_R_5 9'h110 */
	union {
		uint32_t page_match_r_5; // word name
		struct {
			uint32_t page_match_score_r_20 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_21 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_22 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_23 : 4;
			uint32_t : 4; // padding bits
		};
	};
	/* PAGE_MATCH_R_6 9'h114 */
	union {
		uint32_t page_match_r_6; // word name
		struct {
			uint32_t page_match_score_r_24 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_25 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_26 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_27 : 4;
			uint32_t : 4; // padding bits
		};
	};
	/* PAGE_MATCH_R_7 9'h118 */
	union {
		uint32_t page_match_r_7; // word name
		struct {
			uint32_t page_match_score_r_28 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_29 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_30 : 4;
			uint32_t : 4; // padding bits
			uint32_t page_match_score_r_31 : 4;
			uint32_t : 4; // padding bits
		};
	};
	/* GENERAL_SCORE 9'h11C */
	union {
		uint32_t general_score; // word name
		struct {
			uint32_t bank_switch_score : 4;
			uint32_t : 4; // padding bits
			uint32_t keep_agent_score : 4;
			uint32_t : 4; // padding bits
			uint32_t keep_rw_score : 4;
			uint32_t : 4; // padding bits
			uint32_t debug_mon_sel : 5;
			uint32_t : 3; // padding bits
		};
	};
	/* DRAM_TYPE 9'h120 */
	union {
		uint32_t dram_type; // word name
		struct {
			uint32_t row_before_bank : 1;
			uint32_t : 7; // padding bits
			uint32_t bank_addr_type : 1;
			uint32_t : 7; // padding bits
			uint32_t row_addr_type : 3;
			uint32_t : 5; // padding bits
			uint32_t col_addr_type : 2;
			uint32_t : 6; // padding bits
		};
	};
	/* PAT_GEN_0_CTRL_0 9'h124 */
	union {
		uint32_t pat_gen_0_ctrl_0; // word name
		struct {
			uint32_t pat_gen_0_en : 1;
			uint32_t : 7; // padding bits
			uint32_t pat_gen_0_write : 1;
			uint32_t : 7; // padding bits
			uint32_t pat_gen_0_all_agent : 1;
			uint32_t : 7; // padding bits
			uint32_t pat_gen_0_agent : 5;
			uint32_t : 3; // padding bits
		};
	};
	/* PAT_GEN_0_CTRL_1 9'h128 */
	union {
		uint32_t pat_gen_0_ctrl_1; // word name
		struct {
			uint32_t pat_gen_0_shuffle : 1;
			uint32_t : 7; // padding bits
			uint32_t pat_gen_0_wstrb_en : 1;
			uint32_t : 7; // padding bits
			uint32_t pat_gen_0_wstrb : 8;
			uint32_t : 8; // padding bits
		};
	};
	/* WORD_PAT_GEN_0_DATA_0 9'h12C */
	union {
		uint32_t word_pat_gen_0_data_0; // word name
		struct {
			uint32_t pat_gen_0_data_0 : 32;
		};
	};
	/* WORD_PAT_GEN_0_DATA_1 9'h130 */
	union {
		uint32_t word_pat_gen_0_data_1; // word name
		struct {
			uint32_t pat_gen_0_data_1 : 32;
		};
	};
	/* PAT_GEN_1_CTRL_0 9'h134 */
	union {
		uint32_t pat_gen_1_ctrl_0; // word name
		struct {
			uint32_t pat_gen_1_en : 1;
			uint32_t : 7; // padding bits
			uint32_t pat_gen_1_write : 1;
			uint32_t : 7; // padding bits
			uint32_t pat_gen_1_all_agent : 1;
			uint32_t : 7; // padding bits
			uint32_t pat_gen_1_agent : 5;
			uint32_t : 3; // padding bits
		};
	};
	/* PAT_GEN_1_CTRL_1 9'h138 */
	union {
		uint32_t pat_gen_1_ctrl_1; // word name
		struct {
			uint32_t pat_gen_1_shuffle : 1;
			uint32_t : 7; // padding bits
			uint32_t pat_gen_1_wstrb_en : 1;
			uint32_t : 7; // padding bits
			uint32_t pat_gen_1_wstrb : 8;
			uint32_t : 8; // padding bits
		};
	};
	/* WORD_PAT_GEN_1_DATA_0 9'h13C */
	union {
		uint32_t word_pat_gen_1_data_0; // word name
		struct {
			uint32_t pat_gen_1_data_0 : 32;
		};
	};
	/* WORD_PAT_GEN_1_DATA_1 9'h140 */
	union {
		uint32_t word_pat_gen_1_data_1; // word name
		struct {
			uint32_t pat_gen_1_data_1 : 32;
		};
	};
	/* PAT_GEN_2_CTRL_0 9'h144 */
	union {
		uint32_t pat_gen_2_ctrl_0; // word name
		struct {
			uint32_t pat_gen_2_en : 1;
			uint32_t : 7; // padding bits
			uint32_t pat_gen_2_write : 1;
			uint32_t : 7; // padding bits
			uint32_t pat_gen_2_all_agent : 1;
			uint32_t : 7; // padding bits
			uint32_t pat_gen_2_agent : 5;
			uint32_t : 3; // padding bits
		};
	};
	/* PAT_GEN_2_CTRL_1 9'h148 */
	union {
		uint32_t pat_gen_2_ctrl_1; // word name
		struct {
			uint32_t pat_gen_2_shuffle : 1;
			uint32_t : 7; // padding bits
			uint32_t pat_gen_2_wstrb_en : 1;
			uint32_t : 7; // padding bits
			uint32_t pat_gen_2_wstrb : 8;
			uint32_t : 8; // padding bits
		};
	};
	/* WORD_PAT_GEN_2_DATA_0 9'h14C */
	union {
		uint32_t word_pat_gen_2_data_0; // word name
		struct {
			uint32_t pat_gen_2_data_0 : 32;
		};
	};
	/* WORD_PAT_GEN_2_DATA_1 9'h150 */
	union {
		uint32_t word_pat_gen_2_data_1; // word name
		struct {
			uint32_t pat_gen_2_data_1 : 32;
		};
	};
	/* PAT_GEN_3_CTRL_0 9'h154 */
	union {
		uint32_t pat_gen_3_ctrl_0; // word name
		struct {
			uint32_t pat_gen_3_en : 1;
			uint32_t : 7; // padding bits
			uint32_t pat_gen_3_write : 1;
			uint32_t : 7; // padding bits
			uint32_t pat_gen_3_all_agent : 1;
			uint32_t : 7; // padding bits
			uint32_t pat_gen_3_agent : 5;
			uint32_t : 3; // padding bits
		};
	};
	/* PAT_GEN_3_CTRL_1 9'h158 */
	union {
		uint32_t pat_gen_3_ctrl_1; // word name
		struct {
			uint32_t pat_gen_3_shuffle : 1;
			uint32_t : 7; // padding bits
			uint32_t pat_gen_3_wstrb_en : 1;
			uint32_t : 7; // padding bits
			uint32_t pat_gen_3_wstrb : 8;
			uint32_t : 8; // padding bits
		};
	};
	/* WORD_PAT_GEN_3_DATA_0 9'h15C */
	union {
		uint32_t word_pat_gen_3_data_0; // word name
		struct {
			uint32_t pat_gen_3_data_0 : 32;
		};
	};
	/* WORD_PAT_GEN_3_DATA_1 9'h160 */
	union {
		uint32_t word_pat_gen_3_data_1; // word name
		struct {
			uint32_t pat_gen_3_data_1 : 32;
		};
	};
	/* CHKSUM_0_CTRL 9'h164 */
	union {
		uint32_t chksum_0_ctrl; // word name
		struct {
			uint32_t checksum_0_en : 1;
			uint32_t : 7; // padding bits
			uint32_t checksum_0_type : 2;
			uint32_t : 6; // padding bits
			uint32_t checksum_0_agent : 5;
			uint32_t : 3; // padding bits
			uint32_t : 8; // padding bits
		};
	};
	/* CHKSUM_0_0 9'h168 */
	union {
		uint32_t chksum_0_0; // word name
		struct {
			uint32_t checksum_0_0 : 32;
		};
	};
	/* CHKSUM_0_1 9'h16C */
	union {
		uint32_t chksum_0_1; // word name
		struct {
			uint32_t checksum_0_1 : 32;
		};
	};
	/* CHKSUM_1_CTRL 9'h170 */
	union {
		uint32_t chksum_1_ctrl; // word name
		struct {
			uint32_t checksum_1_en : 1;
			uint32_t : 7; // padding bits
			uint32_t checksum_1_type : 2;
			uint32_t : 6; // padding bits
			uint32_t checksum_1_agent : 5;
			uint32_t : 3; // padding bits
			uint32_t : 8; // padding bits
		};
	};
	/* CHKSUM_1_0 9'h174 */
	union {
		uint32_t chksum_1_0; // word name
		struct {
			uint32_t checksum_1_0 : 32;
		};
	};
	/* CHKSUM_1_1 9'h178 */
	union {
		uint32_t chksum_1_1; // word name
		struct {
			uint32_t checksum_1_1 : 32;
		};
	};
	/* CHKSUM_2_CTRL 9'h17C */
	union {
		uint32_t chksum_2_ctrl; // word name
		struct {
			uint32_t checksum_2_en : 1;
			uint32_t : 7; // padding bits
			uint32_t checksum_2_type : 2;
			uint32_t : 6; // padding bits
			uint32_t checksum_2_agent : 5;
			uint32_t : 3; // padding bits
			uint32_t : 8; // padding bits
		};
	};
	/* CHKSUM_2_0 9'h180 */
	union {
		uint32_t chksum_2_0; // word name
		struct {
			uint32_t checksum_2_0 : 32;
		};
	};
	/* CHKSUM_2_1 9'h184 */
	union {
		uint32_t chksum_2_1; // word name
		struct {
			uint32_t checksum_2_1 : 32;
		};
	};
	/* CHKSUM_3_CTRL 9'h188 */
	union {
		uint32_t chksum_3_ctrl; // word name
		struct {
			uint32_t checksum_3_en : 1;
			uint32_t : 7; // padding bits
			uint32_t checksum_3_type : 2;
			uint32_t : 6; // padding bits
			uint32_t checksum_3_agent : 5;
			uint32_t : 3; // padding bits
			uint32_t : 8; // padding bits
		};
	};
	/* CHKSUM_3_0 9'h18C */
	union {
		uint32_t chksum_3_0; // word name
		struct {
			uint32_t checksum_3_0 : 32;
		};
	};
	/* CHKSUM_3_1 9'h190 */
	union {
		uint32_t chksum_3_1; // word name
		struct {
			uint32_t checksum_3_1 : 32;
		};
	};
	/* MON_AGENT 9'h194 */
	union {
		uint32_t mon_agent; // word name
		struct {
			uint32_t w_addr_mon_agent : 5;
			uint32_t : 3; // padding bits
			uint32_t r_addr_mon_agent : 5;
			uint32_t : 3; // padding bits
			uint32_t w_data_mon_agent : 5;
			uint32_t : 3; // padding bits
			uint32_t r_data_mon_agent : 5;
			uint32_t : 3; // padding bits
		};
	};
	/* LAST_STATUS_EN 9'h198 */
	union {
		uint32_t last_status_en; // word name
		struct {
			uint32_t last_w_addr_en : 1;
			uint32_t : 7; // padding bits
			uint32_t last_r_addr_en : 1;
			uint32_t : 7; // padding bits
			uint32_t last_w_data_en : 1;
			uint32_t : 7; // padding bits
			uint32_t last_r_data_en : 1;
			uint32_t : 7; // padding bits
		};
	};
	/* LAST_STATUS_ALL_AGENT 9'h19C */
	union {
		uint32_t last_status_all_agent; // word name
		struct {
			uint32_t last_w_addr_all_agent : 1;
			uint32_t : 7; // padding bits
			uint32_t last_r_addr_all_agent : 1;
			uint32_t : 7; // padding bits
			uint32_t last_w_data_all_agent : 1;
			uint32_t : 7; // padding bits
			uint32_t last_r_data_all_agent : 1;
			uint32_t : 7; // padding bits
		};
	};
	/* WORD_LAST_W_ADDR 9'h1A0 */
	union {
		uint32_t word_last_w_addr; // word name
		struct {
			uint32_t last_w_addr : 31;
			uint32_t : 1; // padding bits
		};
	};
	/* WORD_LAST_R_ADDR 9'h1A4 */
	union {
		uint32_t word_last_r_addr; // word name
		struct {
			uint32_t last_r_addr : 31;
			uint32_t : 1; // padding bits
		};
	};
	/* WORD_LAST_W_DATA_0 9'h1A8 */
	union {
		uint32_t word_last_w_data_0; // word name
		struct {
			uint32_t last_w_data_0 : 32;
		};
	};
	/* WORD_LAST_W_DATA_1 9'h1AC */
	union {
		uint32_t word_last_w_data_1; // word name
		struct {
			uint32_t last_w_data_1 : 32;
		};
	};
	/* WORD_LAST_R_DATA_0 9'h1B0 */
	union {
		uint32_t word_last_r_data_0; // word name
		struct {
			uint32_t last_r_data_0 : 32;
		};
	};
	/* WORD_LAST_R_DATA_1 9'h1B4 */
	union {
		uint32_t word_last_r_data_1; // word name
		struct {
			uint32_t last_r_data_1 : 32;
		};
	};
	/* RESP_ERROR_AGENT 9'h1B8 */
	union {
		uint32_t resp_error_agent; // word name
		struct {
			uint32_t w_resp_error_agent : 5;
			uint32_t : 3; // padding bits
			uint32_t r_resp_error_agent : 5;
			uint32_t : 3; // padding bits
			uint32_t : 8; // padding bits
			uint32_t : 8; // padding bits
		};
	};
	/* TIMEOUT_CTRL 9'h1BC */
	union {
		uint32_t timeout_ctrl; // word name
		struct {
			uint32_t timeout_en : 1;
			uint32_t : 7; // padding bits
			uint32_t : 8; // padding bits
			uint32_t timeout_cycle_th : 16;
		};
	};
	/* TIMEOUT_AGENT_0 9'h1C0 */
	union {
		uint32_t timeout_agent_0; // word name
		struct {
			uint32_t timeout_w_resp_agent : 5;
			uint32_t : 3; // padding bits
			uint32_t timeout_r_addr_agent : 5;
			uint32_t : 3; // padding bits
			uint32_t timeout_w_addr_agent : 5;
			uint32_t : 3; // padding bits
			uint32_t timeout_w_data_agent : 5;
			uint32_t : 3; // padding bits
		};
	};
	/* TIMEOUT_AGENT_1 9'h1C4 */
	union {
		uint32_t timeout_agent_1; // word name
		struct {
			uint32_t : 8; // padding bits
			uint32_t : 8; // padding bits
			uint32_t timeout_r_data_agent : 5;
			uint32_t : 3; // padding bits
			uint32_t : 8; // padding bits
		};
	};
	/* WORD_RESERVED_0 9'h1C8 */
	union {
		uint32_t word_reserved_0; // word name
		struct {
			uint32_t reserved_0 : 32;
		};
	};
	/* WORD_RESERVED_1 9'h1CC */
	union {
		uint32_t word_reserved_1; // word name
		struct {
			uint32_t reserved_1 : 32;
		};
	};
	/* WORD_RESERVED_2 9'h1D0 */
	union {
		uint32_t word_reserved_2; // word name
		struct {
			uint32_t reserved_2 : 32;
		};
	};
	/* WORD_RESERVED_3 9'h1D4 */
	union {
		uint32_t word_reserved_3; // word name
		struct {
			uint32_t reserved_3 : 32;
		};
	};
	/* WORD_RESERVED_4 9'h1D8 */
	union {
		uint32_t word_reserved_4; // word name
		struct {
			uint32_t reserved_4 : 32;
		};
	};
	/* WORD_RESERVED_5 9'h1DC */
	union {
		uint32_t word_reserved_5; // word name
		struct {
			uint32_t reserved_5 : 32;
		};
	};
	/* WORD_RESERVED_6 9'h1E0 */
	union {
		uint32_t word_reserved_6; // word name
		struct {
			uint32_t reserved_6 : 32;
		};
	};
	/* WORD_RESERVED_7 9'h1E4 */
	union {
		uint32_t word_reserved_7; // word name
		struct {
			uint32_t reserved_7 : 32;
		};
	};
	/* GENERAL_SCORE_1 9'h1E8 */
	union {
		uint32_t general_score_1; // word name
		struct {
			uint32_t ini_r_score : 4;
			uint32_t : 4; // padding bits
			uint32_t ini_w_score : 4;
			uint32_t : 4; // padding bits
			uint32_t : 8; // padding bits
			uint32_t : 8; // padding bits
		};
	};
} CsrBankDarb;

#endif