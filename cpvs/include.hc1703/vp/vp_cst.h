#ifndef VPK_CST_H_
#define VPK_CST_H_

#include "stdint.h"
#define VP_COEFF_IDX 2
#define VP_COEFF_NUM 9
#define VP_OFFSET_IDX 2
#define VP_OFFSET_NUM 3

typedef struct vp_cst_ctx {
	uint32_t cst_coeff[VP_COEFF_NUM];
	uint32_t cst_offset[VP_OFFSET_NUM];

} VpCstCtx;

typedef struct vp_cst_attr {
	uint32_t cst_coeff_idx;
	uint32_t cst_offset_idx;

} VpCstAttr;

void hw_vp_cst_bypass();
void hw_vp_cst_run(VpCstAttr *vpcstattr);
void hw_vp_cst_set_ctx(VpCstCtx *vpcstctx, VpCstAttr *vpcstattr);
void hw_vp_cst_set_reg(VpCstCtx *vpcstctx);
void hw_vp_cst_debug_mon_sel(uint32_t debug_mon_sel);

#endif