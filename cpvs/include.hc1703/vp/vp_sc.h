#ifndef KYOTO_VPSC_H_
#define KYOTO_VPSC_H_

#include "csr_bank_sc.h"

void hw_vp_sc_frame_start();
void hw_vp_sc_irq_clear_frame_end();
uint32_t hw_vp_sc_status_frame_end();
void hw_vp_sc_irq_mask_frame_end(uint32_t irq_mask_frame_end);
void hw_vp_sc_res(uint32_t width, uint32_t height);
void hw_vp_sc_bypass();
void hw_vp_sc_debug_mon_sel(uint32_t debug_mon_sel);

#endif /* KYOTO_VPSC_H_ */