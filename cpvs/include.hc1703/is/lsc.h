#ifndef KYOTO_LSC_H_
#define KYOTO_LSC_H_

#include "is_utils.h"
#include "csr_bank_lsc.h"

typedef enum iss_lsc_mode {
	IS_LSC_MODE_NORMAL = 0,
	IS_LSC_MODE_DISABLE = 1,
	IS_LSC_MODE_NUM = 2,
} IssLscMode;

typedef struct is_lsc_cfg {
	enum iss_lsc_mode mode;
	uint32_t bayer_phase_srep;
} IsLscCfg;

typedef struct is_lsc_curve_coeff {
	uint32_t fit_y0x0;
	uint32_t dx_y0x0_2s;
	uint32_t dy_y0x0_2s;
	uint32_t dx2;
	uint32_t dy2;
	uint32_t dydx_2s;
} IsLscCurveCoeff;

typedef struct is_lsc_table_coeff {
	int ee[LSC_TABLE_EE_WORD_NUM];
	int eo[LSC_TABLE_EO_WORD_NUM];
	int oe[LSC_TABLE_OE_WORD_NUM];
	int oo[LSC_TABLE_OO_WORD_NUM];
} IsLsctableCoeff;

typedef struct is_lsc_curve_cfg {
	uint8_t enable;
	struct is_lsc_curve_coeff coeff[INI_BAYER_PHASE_NUM];
} IsLscCurveCfg;

typedef enum is_lsc_table_gain_prec {
	IS_LSC_TABLE_GAIN_PREC_8 = 0,
	IS_LSC_TABLE_GAIN_PREC_9 = 1,
	IS_LSC_TABLE_GAIN_PREC_10 = 2,
	IS_LSC_TABLE_GAIN_PREC_11 = 3,
	IS_LSC_TABLE_GAIN_PREC_12 = 4,
	IS_LSC_TABLE_GAIN_PREC_NUM = 5,
} IsLscTableGainPrec;

typedef struct is_lsc_table_cfg {
	uint8_t enable;
	uint8_t merge_en;
	enum is_lsc_table_gain_prec prec;
	uint16_t gain_mod;
	uint8_t hori_start_int;
	uint32_t hori_start_frac;
	uint32_t hori_step;
	uint8_t verti_start_int;
	uint32_t verti_start_frac;
	uint32_t verti_step;
	struct is_lsc_table_coeff coeff;
} IsLscTableCfg;

/* Frame start */
void hw_is_lsc_set_frame_start(const enum is_kel_no no);

/* IRQ clear */
void hw_is_lsc_set_irq_clear_frame_end(const enum is_kel_no no);

/* Frame end*/
uint8_t hw_is_lsc_get_status_frame_end(const enum is_kel_no no);

/* IRQ mask */
void hw_is_lsc_set_irq_mask_frame_end(const enum is_kel_no no, uint8_t irq_mask_frame_end);
uint8_t hw_is_lsc_get_irq_mask_frame_end(const enum is_kel_no no);

/* Resolution */
void hw_is_lsc_set_res(const enum is_kel_no no, const struct res *res);
void hw_is_lsc_get_res(const enum is_kel_no no, struct res *res);

/* Input format */
void hw_is_lsc_set_input_format(const enum is_kel_no no, const struct is_input_format *cfg);
void hw_is_lsc_get_input_format(const enum is_kel_no no, struct is_input_format *cfg);

/* LSC */
// In the Kyoto, bayer_phase_srep always set as 3
void hw_is_lsc_set_cfg(const enum is_kel_no no, const struct is_lsc_cfg *cfg);
void hw_is_lsc_get_cfg(const enum is_kel_no no, struct is_lsc_cfg *cfg);
void hw_is_lsc_set_curve_cfg(const enum is_kel_no no, const struct is_lsc_curve_cfg *cfg);
void hw_is_lsc_get_curve_cfg(const enum is_kel_no no, struct is_lsc_curve_cfg *cfg);
void hw_is_lsc_set_table_cfg(const enum is_kel_no no, const struct is_lsc_table_cfg *cfg);
void hw_is_lsc_get_table_cfg(const enum is_kel_no no, struct is_lsc_table_cfg *cfg);
uint8_t hw_is_lsc_get_csr2sram_en(const enum is_kel_no no);

/* Debug */
void hw_is_lsc_set_dbg_mon_sel(const enum is_kel_no no, uint8_t debug_mon_sel);
uint8_t hw_is_lsc_get_dbg_mon_sel(const enum is_kel_no no);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//* for mpp is *//

/* IRQ mask */
void is_lsc_set_irq_mask_frame_end(volatile CsrBankLsc *csr, uint8_t irq_mask_frame_end);
uint8_t is_lsc_get_irq_mask_frame_end(volatile CsrBankLsc *csr);

/* Resolution */
void is_lsc_set_res(volatile CsrBankLsc *csr, const struct res *res);
void is_lsc_get_res(volatile CsrBankLsc *csr, struct res *res);

/* Input format */
void is_lsc_set_input_format(volatile CsrBankLsc *csr, const struct is_input_format *cfg);
void is_lsc_get_input_format(volatile CsrBankLsc *csr, struct is_input_format *cfg);

// In the Kyoto, bayer_phase_srep always set as 3
void is_lsc_set_cfg(volatile CsrBankLsc *csr, const struct is_lsc_cfg *cfg);
void is_lsc_get_cfg(volatile CsrBankLsc *csr, struct is_lsc_cfg *cfg);
void is_lsc_set_curve_cfg(volatile CsrBankLsc *csr, const struct is_lsc_curve_cfg *cfg);
void is_lsc_get_curve_cfg(volatile CsrBankLsc *csr, struct is_lsc_curve_cfg *cfg);
void is_lsc_set_table_cfg(volatile CsrBankLsc *csr, const struct is_lsc_table_cfg *cfg);
void is_lsc_get_table_cfg(volatile CsrBankLsc *csr, struct is_lsc_table_cfg *cfg);
uint8_t is_lsc_get_csr2sram_en(volatile CsrBankLsc *csr);

/* Debug */
void is_lsc_set_dbg_mon_sel(volatile CsrBankLsc *csr, uint8_t debug_mon_sel);
uint8_t is_lsc_get_dbg_mon_sel(volatile CsrBankLsc *csr);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//* neef to remove *//
// Move to test

#define IS_LSC_CFA_PHASE_IDX 3
#define IS_LSC_CFA_PHASE_NUM 16

#define IS_LSC_CURVE_NUM 5
#define IS_LSC_CURVE_POINT_NUM 6
#define IS_LSC_TABLE_POINT_NUM 4

#define IS_LSC_SRAM_NUM 1024
#define IS_LSC_TABLE_MERGE_NUM 64

#define IS_LSC_CHANNEL_GAIN_TABLE_idx 2
#define IS_LSC_CHANNEL_GAIN_TABLE_NUM 32
#define IS_LSC_MERGE_GAIN_TABLE_idx 2
#define IS_LSC_MERGE_GAIN_TABLE_NUM 64

typedef struct is_lsc_resolution {
	uint32_t width;
	uint32_t height;
} IsLscResolution;

typedef struct is_lsc_cfa_set {
	uint32_t cfa_mode;
	uint32_t bayer_ini_phase;
	uint32_t bayer_phase_srep;
	uint32_t cfa_phase[IS_LSC_CFA_PHASE_NUM];
} IsLscCfaSet;

typedef struct is_lsc_ctx {
	uint32_t lsc_mode;
	IsLscResolution lsc_res;
	IsLscCfaSet lsc_cfa_set;
	uint32_t lsc_curve_enable;
	uint32_t lsc_table_enable;
	uint32_t lsc_table_merge_en;
	uint32_t lsc_table_gain_prec_m8;
	uint32_t lsc_curve_g[IS_LSC_CURVE_POINT_NUM];
	uint32_t lsc_curve_r[IS_LSC_CURVE_POINT_NUM];
	uint32_t lsc_curve_b[IS_LSC_CURVE_POINT_NUM];
	uint32_t lsc_curve_s[IS_LSC_CURVE_POINT_NUM];
	uint32_t lsc_table_ee[IS_LSC_SRAM_NUM];
	uint32_t lsc_table_eo[IS_LSC_SRAM_NUM];
	uint32_t lsc_table_oo[IS_LSC_SRAM_NUM];
	uint32_t lsc_table_oe[IS_LSC_SRAM_NUM];
	uint32_t lsc_gain_mod;
	uint32_t lsc_hori_start_int;
	uint32_t lsc_hori_start_frac;
	uint32_t hori_step;
	uint32_t lsc_verti_start_int;
	uint32_t lsc_verti_start_frac;
	uint32_t verti_step;

} IsLscCtx;

typedef struct is_lsc_attr {
	uint32_t lsc_mode;
	uint32_t lsc_width;
	uint32_t lsc_height;
	uint32_t lsc_cfa_mode_idx;
	uint32_t lsc_bayer_ini_phase;
	uint32_t lsc_bayer_phase_srep;
	uint32_t lsc_curve_enable;
	uint32_t lsc_table_enable;
	uint32_t lsc_table_merge_enable;
	uint32_t lsc_curve_idx;
	uint32_t lsc_table_gain_prec_m8;
	uint32_t lsc_table_0_idx;
	uint32_t lsc_table_1_idx;
	uint32_t lsc_table_2_idx;
	uint32_t lsc_table_3_idx;
	uint32_t lsc_table_merge_idx;

} IsLscAttr;

void hw_is_lsc_frame_start(void);
void hw_is_lsc_irq_clear_frame_end(void);
uint32_t hw_is_lsc_status_frame_end(void);
void hw_is_lsc_irq_mask_frame_end(uint32_t irq_mask_frame_end);
void hw_is_lsc_resolution(uint32_t width, uint32_t height);
void hw_is_lsc_bypass(uint32_t cfa_mode, uint32_t bayer_ini_phase);
void hw_is_lsc_run(IsLscAttr *lscattr);
void hw_is_lsc_set_ctx(IsLscCtx *lscctx, IsLscAttr *lscattr);
void hw_is_lsc_set_ctx_no_sram(IsLscCtx *lscctx, IsLscAttr *lscattr);
void hw_is_lsc_set_reg(IsLscCtx *lscctx);
void hw_is_lsc_set_reg_no_sram(IsLscCtx *lscctx);
void hw_is_lsc_debug_mon_sel(uint32_t debug_mon_sel);

#endif /* KYOTO_LSC_H_ */