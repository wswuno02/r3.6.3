#ifndef KYOTO_BSP_H_
#define KYOTO_BSP_H_

#include "is_utils.h"
#include "csr_bank_bsp.h"

typedef enum is_dpd_g_pattern_mode {
	IS_DPD_G_PATTERN_MODE_OUTER_8 = 0,
	IS_DPD_G_PATTERN_MODE_DIAMOND = 1,
	IS_DPD_G_PATTERN_MODE_OUTER_4 = 2,
	IS_DPD_G_PATTERN_MODE_NUM = 3,
} IsDpdGPatternMode;

typedef enum is_dpd_pix_num_mode {
	IS_DPD_PIX_NUM_MODE_8 = 0,
	IS_DPD_PIX_NUM_MODE_6 = 1,
	IS_DPD_PIX_NUM_MODE_NUM = 2,
} IsDpdPixNumMode;

typedef struct is_bsp_dpd_cfg {
	struct is_static_dpd {
		uint8_t enable;
		uint16_t offset_x;
		uint16_t offset_y;
	} sdpd;
	struct is_dynamic_dpd {
		uint8_t enable;
		enum is_dpd_g_pattern_mode g_pattern_mode;
		enum is_dpd_pix_num_mode pix_num_mode;
		uint8_t diff_ratio;
		uint8_t avg_ratio;
		uint8_t edge_diff_ratio;
		uint8_t edge_avg_ratio;
		uint8_t edge_m;
		uint32_t dp_num;
	} ddpd;
} IsBspDpdCfg;

typedef struct is_bsp_dpc_cfg {
	uint8_t enable;
	uint8_t dir_enable;
	uint8_t dir_th_ratio;
	uint8_t dir_str_m;
} IsBspDpcCfg;

typedef struct is_bsp_ct_cfg {
	uint8_t enable;
	uint16_t avg_lb;
	uint16_t avg_ratio;
	uint16_t str_lb;
	uint16_t str_ratio;
	uint8_t str_m_2s;
} IsBspCtCfg;

/*
 * _______________________________   _______________________________
 * | 0 0 | 0 1 | 0 2 | 0 1 | 0 0 |   |  0  |  1  |  2  |  1  |  0  |
 * _______________________________   _______________________________
 * | 0 1 | 1 1 | 1 2 | 1 1 | 0 1 |   |  1  |  3  |  4  |  3  |  1  |
 * _______________________________   _______________________________
 * | 0 2 | 0 1 | 2 2 | 1 2 | 0 2 |   |  2  |  1  |  5  |  1  |  2  |
 * _______________________________   _______________________________
 * | 0 1 | 1 1 | 1 2 | 1 1 | 0 1 |   |  1  |  3  |  4  |  3  |  1  |
 * _______________________________   _______________________________
 * | 0 0 | 0 1 | 0 2 | 0 1 | 0 0 |   |  0  |  1  |  2  |  1  |  0  |
 * _______________________________   _______________________________
 */
typedef struct is_bsp_chn_wgt_cfg {
	uint8_t coeff[BSP_CHN_WGT_ENTRY_NUM];
} IsBspChnWgtCfg;

typedef struct is_bsp_luma_est_cfg {
	uint8_t weight[INI_BAYER_PHASE_NUM];
} IsBspLumaEstCfg;

typedef struct is_bsp_remosaic_cfg {
	uint8_t enable;
	uint16_t coeff_2s[COLOR_CHN_NUM * INI_BAYER_PHASE_NUM];
} IsBspRemosaicCfg;

typedef struct is_bsp_y_est_mode_ctrl {
	enum is_luma_input_mode mode;
} IsBspYEstModeCtrl;

typedef struct is_bsp_tm_cfg {
	uint8_t enable;
	struct is_tone_curve {
		uint16_t val[TM_CURVE_ENTRY_NUM];
	} curve; /**< Tone Mapping curve */
} IsBspTmCfg;

typedef struct is_bsp_wb_cfg {
	uint8_t enable;
	struct is_wb_gain {
		uint16_t val[INI_BAYER_PHASE_NUM]; /**< White balance gain */
	} gain;
} IsBspWbCfg;

typedef struct is_bsp_y_hist_roi_ctrl {
	enum is_luma_input_mode mode;
	uint16_t offset;
} IsBspYHistRoiCtrl;

typedef struct is_bsp_y_hist_roi_stat_frame_ctrl {
	struct rect_point roi;
} IsBspYHistRoiStatFrameCtrl;

typedef struct is_bsp_y_hist_roi_stat {
	uint8_t overflow;
	uint32_t hist[BSP_Y_HIST_ENTRY_NUM];
} IsBspYHistRoiStat;

typedef struct is_bsp_y_avg_roi_ctrl {
	enum is_luma_input_mode mode;
} IsBspYAvgRoiCtrl;

typedef struct is_bsp_y_avg_roi_stat_frame_ctrl {
	struct rect_point roi;
	uint32_t pix_num;
} IsBspYAvgRoiStatFrameCtrl;

typedef struct is_bsp_y_avg_roi_stat {
	uint16_t avg;
	uint32_t remainder;
} IsBspYAvgRoiStat;

typedef struct is_bsp_y_avg_rgl_ctrl {
	enum is_luma_input_mode mode;
} IsBspYAvgRglCtrl;

typedef struct is_bsp_y_avg_rgl_stat_frame_ctrl {
	struct rect_point roi;
	uint16_t blk_ht;
	uint16_t blk_wd;
	uint32_t pix_num;
} IsBspYAvgRglStatFrameCtrl;

typedef struct is_bsp_y_avg_rgl_stat {
	uint8_t avg[BSP_Y_AVG_MAX_RGL_NUM];
} IsBspYAvgRglStat;

typedef enum is_rtx_region_mode {
	IS_RTX_REGION_MODE_32x32 = 0,
	IS_RTX_REGION_MODE_64x64 = 1,
	IS_RTX_REGION_MODE_128x128 = 2,
	IS_RTX_REGION_MODE_256x256 = 3,
	IS_RTX_REGION_MODE_512x512 = 4,
	IS_RTX_REGION_MODE_NUM = 5,
} IsRtxRegionMode;

typedef struct is_bsp_rtx_ctrl {
	enum is_luma_input_mode mode;
	enum is_rtx_region_mode region_mode;
	uint8_t y_th_min;
	uint8_t y_th_max;
	uint8_t y_m;
} IsBspRtxCtrl;

typedef struct is_bsp_gwd_ctrl {
	enum is_luma_input_mode mode;
	uint8_t y_th_min;
	uint8_t y_th_max;
	uint8_t y_m;
	struct is_gwd_ratio_gain_curve {
		uint8_t rg[BSP_GWD_RATIO_GAIN_CURVE_CTRL_POINT_NUM];
		uint8_t bg[BSP_GWD_RATIO_GAIN_CURVE_CTRL_POINT_NUM];
		uint16_t a_2s[BSP_GWD_RATIO_GAIN_CURVE_CTRL_POINT_NUM - 1];
		uint16_t b_2s[BSP_GWD_RATIO_GAIN_CURVE_CTRL_POINT_NUM - 1];
		uint16_t c_2s[BSP_GWD_RATIO_GAIN_CURVE_CTRL_POINT_NUM - 1];
		uint8_t rto_bg_th[BSP_GWD_RATIO_GAIN_CURVE_CTRL_POINT_NUM - 1];
		uint8_t rto_bg_m_2s[BSP_GWD_RATIO_GAIN_CURVE_CTRL_POINT_NUM - 1];
	} curve;
} IsBspGwdCtrl;

typedef struct is_bsp_rtx_roi_stat_frame_ctrl {
	struct rect_point roi;
} IsBspRtxRoiStatFrameCtrl;

typedef struct is_rtx_roi_param {
	uint16_t max_pix;
	uint8_t wgt;
	uint8_t x;
	uint8_t y;
} IsRtxRoiParam;

typedef struct is_bsp_rtx_roi_stat {
	uint32_t sum[INI_BAYER_PHASE_NUM];
	uint8_t norm[INI_BAYER_PHASE_NUM];
	struct is_rtx_roi_param g0[BSP_RTX_MAX_ROI_WHITE_POINT_NUM];
	struct is_rtx_roi_param r[BSP_RTX_MAX_ROI_WHITE_POINT_NUM];
	struct is_rtx_roi_param b[BSP_RTX_MAX_ROI_WHITE_POINT_NUM];
	struct is_rtx_roi_param g1[BSP_RTX_MAX_ROI_WHITE_POINT_NUM];
} IsBspRtxRoiStat;

typedef struct is_bsp_rtx_rgl_stat_frame_ctrl {
	struct rect_point roi;
	uint16_t blk_ht;
	uint16_t blk_wd;
} IsBspRtxRglStatFrameCtrl;

typedef struct is_rtx_rgl_param {
	uint16_t max_pix;
	uint8_t wgt;
} IsRtxRglParam;

typedef struct is_bsp_rtx_rgl_stat {
	struct is_rtx_rgl_param g0[BSP_RTX_MAX_RGL_NUM];
	struct is_rtx_rgl_param r[BSP_RTX_MAX_RGL_NUM];
	struct is_rtx_rgl_param b[BSP_RTX_MAX_RGL_NUM];
	struct is_rtx_rgl_param g1[BSP_RTX_MAX_RGL_NUM];
} IsBspRtxRglStat;

typedef struct is_bsp_gwd_roi_stat_frame_ctrl {
	struct rect_point roi;
} IsBspGwdRoiStatFrameCtrl;

typedef struct is_bsp_gwd_roi_stat {
	uint32_t sum[INI_BAYER_PHASE_NUM];
	uint32_t norm[INI_BAYER_PHASE_NUM];
} IsBspGwdRoiStat;

typedef struct is_bsp_gwd_rgl_stat_frame_ctrl {
	struct rect_point roi;
	uint16_t blk_ht;
	uint16_t blk_wd;
} IsBspGwdRglStatFrameCtrl;

typedef struct is_gwd_rgl_param {
	uint8_t avg;
} IsGwdRglParam;

typedef struct is_bsp_gwd_rgl_stat {
	struct is_gwd_rgl_param g[BSP_GWD_MAX_RGL_NUM];
	struct is_gwd_rgl_param r[BSP_GWD_MAX_RGL_NUM];
	struct is_gwd_rgl_param b[BSP_GWD_MAX_RGL_NUM];
} IsBspGwdRglStat;

typedef struct is_bsp_contrast_ctrl {
	uint8_t cfa_phase_en[CFA_PHASE_NUM];
	enum is_luma_input_mode mode;
	uint8_t y_th_min;
	uint8_t y_th_max;
	uint8_t y_m;
	struct contrast_h_iir_weight {
		uint8_t h1[BSP_CONTRAST_H_WGT_ENTRY_NUM];
		uint8_t h2[BSP_CONTRAST_H_WGT_ENTRY_NUM];
	} iir_weight;
	struct contrast_v_fir_weight {
		uint16_t v1[BSP_CONTRAST_V_WGT_ENTRY_NUM];
		uint16_t v2[BSP_CONTRAST_V_WGT_ENTRY_NUM];
	} fir_weight;
	uint16_t coring_th;
} IsBspContrastCtrl;

typedef struct is_bsp_contrast_roi_stat_frame_ctrl {
	struct rect_point roi;
	uint32_t pix_num;
} IsBspContrastRoiStatFrameCtrl;

typedef struct is_bsp_contrast_roi_stat {
	uint16_t h1_avg;
	uint16_t h2_avg;
	uint16_t v1_avg;
	uint16_t v2_avg;
} IsBspContrastRoiStat;

typedef struct is_bsp_contrast_rgl_stat_frame_ctrl {
	struct rect_point roi;
	uint16_t blk_ht;
	uint16_t blk_wd;
	uint32_t pix_num;
} IsBspContrastRglStatFrameCtrl;

typedef struct is_bsp_contrast_rgl_stat {
	uint16_t h1_avg[BSP_CONTRAST_MAX_RGL_NUM];
	uint16_t h2_avg[BSP_CONTRAST_MAX_RGL_NUM];
	uint16_t v1_avg[BSP_CONTRAST_MAX_RGL_NUM];
	uint16_t v2_avg[BSP_CONTRAST_MAX_RGL_NUM];
} IsBspContrastRglStat;

typedef struct is_bsp_cfa_phase_enable_cfg {
	uint8_t enable[CFA_PHASE_ENTRY_NUM];
} IsBspCfaPhaseEnableCfg;

/* Frame start */
void hw_is_bsp_set_frame_start(const enum is_kel_no no);

/* IRQ clear */
void hw_is_bsp_set_irq_clear_frame_end(const enum is_kel_no no);

/* Frame end*/
uint8_t hw_is_bsp_get_status_frame_end(const enum is_kel_no no);

/* IRQ mask */
void hw_is_bsp_set_irq_mask_frame_end(const enum is_kel_no no, uint8_t irq_mask_frame_end);
uint8_t hw_is_bsp_get_irq_mask_frame_end(const enum is_kel_no no);

/* Resolution */
void hw_is_bsp_set_res(const enum is_kel_no no, const struct res *res);
void hw_is_bsp_get_res(const enum is_kel_no no, struct res *res);

/* Input format */
void hw_is_bsp_set_input_format(const enum is_kel_no no, const struct is_input_format *cfg);
void hw_is_bsp_get_input_format(const enum is_kel_no no, struct is_input_format *cfg);

/* Output format */
void hw_is_bsp_set_output_format(const enum is_kel_no no, const struct is_output_format *cfg);
void hw_is_bsp_get_output_format(const enum is_kel_no no, struct is_output_format *cfg);

/* DPD + DPC */
void hw_is_bsp_set_dpd_cfg(const enum is_kel_no no, const struct is_bsp_dpd_cfg *cfg);
void hw_is_bsp_get_dpd_cfg(const enum is_kel_no no, struct is_bsp_dpd_cfg *cfg);
void hw_is_bsp_set_dpc_cfg(const enum is_kel_no no, const struct is_bsp_dpc_cfg *cfg);
void hw_is_bsp_get_dpc_cfg(const enum is_kel_no no, struct is_bsp_dpc_cfg *cfg);

/* Crosstalk */
void hw_is_bsp_set_ct_cfg(const enum is_kel_no no, const struct is_bsp_ct_cfg *cfg);
void hw_is_bsp_get_ct_cfg(const enum is_kel_no no, struct is_bsp_ct_cfg *cfg);

/* Channel Weighted Sum and Average */
void hw_is_bsp_set_chn_wgt_cfg(const enum is_kel_no no, const struct is_bsp_chn_wgt_cfg *cfg);
void hw_is_bsp_get_chn_wgt_cfg(const enum is_kel_no no, struct is_bsp_chn_wgt_cfg *cfg);

/* Luma Estimation*/
void hw_is_bsp_set_luma_est_cfg(const enum is_kel_no no, const struct is_bsp_luma_est_cfg *cfg);
void hw_is_bsp_get_luma_est_cfg(const enum is_kel_no no, struct is_bsp_luma_est_cfg *cfg);

/* Re-mosaic */
void hw_is_bsp_set_remosaic_cfg(const enum is_kel_no no, const struct is_bsp_remosaic_cfg *cfg);
void hw_is_bsp_get_remosaic_cfg(const enum is_kel_no no, struct is_bsp_remosaic_cfg *cfg);

/* TM */
void hw_is_bsp_set_y_est_mode_ctrl(const enum is_kel_no no, const struct is_bsp_y_est_mode_ctrl *cfg);
void hw_is_bsp_get_y_est_mode_ctrl(const enum is_kel_no no, struct is_bsp_y_est_mode_ctrl *cfg);
void hw_is_bsp_set_tm_cfg(const enum is_kel_no no, const struct is_bsp_tm_cfg *cfg);
void hw_is_bsp_get_tm_cfg(const enum is_kel_no no, struct is_bsp_tm_cfg *cfg);

/* WB */
void hw_is_bsp_set_wb_cfg(const enum is_kel_no no, const struct is_bsp_wb_cfg *cfg);
void hw_is_bsp_get_wb_cfg(const enum is_kel_no no, struct is_bsp_wb_cfg *cfg);

/* Statistics */

// CSR2SRAM
uint8_t hw_is_bsp_get_csr2sram_sel(const enum is_kel_no no);

// AE
// Ctrl
void hw_is_bsp_set_y_hist_roi_ctrl(const enum is_kel_no no, const struct is_bsp_y_hist_roi_ctrl *cfg);
void hw_is_bsp_get_y_hist_roi_ctrl(const enum is_kel_no no, struct is_bsp_y_hist_roi_ctrl *cfg);

void hw_is_bsp_set_y_avg_roi_ctrl(const enum is_kel_no no, const struct is_bsp_y_avg_roi_ctrl *cfg);
void hw_is_bsp_get_y_avg_roi_ctrl(const enum is_kel_no no, struct is_bsp_y_avg_roi_ctrl *cfg);

void hw_is_bsp_set_y_avg_rgl_ctrl(const enum is_kel_no no, const struct is_bsp_y_avg_rgl_ctrl *cfg);
void hw_is_bsp_get_y_avg_rgl_ctrl(const enum is_kel_no no, struct is_bsp_y_avg_rgl_ctrl *cfg);

// ROI Y hist
void hw_is_bsp_set_y_hist_roi_stat_frame_ctrl(const enum is_kel_no no, const struct is_bsp_y_hist_roi_stat_frame_ctrl *cfg);
void hw_is_bsp_get_y_hist_roi_stat_frame_ctrl(const enum is_kel_no no, struct is_bsp_y_hist_roi_stat_frame_ctrl *cfg);
void hw_is_bsp_set_y_hist_roi_stat_enable(const enum is_kel_no no, uint8_t enable);
uint8_t hw_is_bsp_get_y_hist_roi_stat_enable(const enum is_kel_no no);
void hw_is_bsp_get_y_hist_roi_stat(const enum is_kel_no no, struct is_bsp_y_hist_roi_stat *stat);

// ROI Y avg
void hw_is_bsp_set_y_avg_roi_stat_frame_ctrl(const enum is_kel_no no, uint8_t idx, const struct is_bsp_y_avg_roi_stat_frame_ctrl *cfg);
void hw_is_bsp_get_y_avg_roi_stat_frame_ctrl(const enum is_kel_no no, uint8_t idx, struct is_bsp_y_avg_roi_stat_frame_ctrl *cfg);
void hw_is_bsp_set_y_avg_roi_stat_enable(const enum is_kel_no no, uint8_t idx, uint8_t enable);
uint8_t hw_is_bsp_get_y_avg_roi_stat_enable(const enum is_kel_no no, uint8_t idx);
void hw_is_bsp_get_y_avg_roi_stat(const enum is_kel_no no, uint8_t idx, struct is_bsp_y_avg_roi_stat *stat);

// RGL Y avg
void hw_is_bsp_set_y_avg_rgl_stat_frame_ctrl(const enum is_kel_no no, const struct is_bsp_y_avg_rgl_stat_frame_ctrl *cfg);
void hw_is_bsp_get_y_avg_rgl_stat_frame_ctrl(const enum is_kel_no no, struct is_bsp_y_avg_rgl_stat_frame_ctrl *cfg);
void hw_is_bsp_set_y_avg_rgl_stat_enable(const enum is_kel_no no, uint8_t enable);
uint8_t hw_is_bsp_get_y_avg_rgl_stat_enable(const enum is_kel_no no);
void hw_is_bsp_get_y_avg_rgl_stat(const enum is_kel_no no, struct is_bsp_y_avg_rgl_stat *stat);

// AWB
// Ctrl
void hw_is_bsp_set_rtx_ctrl(const enum is_kel_no no, const struct is_bsp_rtx_ctrl *cfg);
void hw_is_bsp_get_rtx_ctrl(const enum is_kel_no no, struct is_bsp_rtx_ctrl *cfg);

void hw_is_bsp_set_gwd_ctrl(const enum is_kel_no no, const struct is_bsp_gwd_ctrl *cfg);
void hw_is_bsp_get_gwd_ctrl(const enum is_kel_no no, struct is_bsp_gwd_ctrl *cfg);

// ROI Rtx
void hw_is_bsp_set_rtx_roi_stat_frame_ctrl(const enum is_kel_no no, const struct is_bsp_rtx_roi_stat_frame_ctrl *cfg);
void hw_is_bsp_get_rtx_roi_stat_frame_ctrl(const enum is_kel_no no, struct is_bsp_rtx_roi_stat_frame_ctrl *cfg);
void hw_is_bsp_set_rtx_roi_stat_enable(const enum is_kel_no no, uint8_t enable);
uint8_t hw_is_bsp_get_rtx_roi_stat_enable(const enum is_kel_no no);
void hw_is_bsp_get_rtx_roi_stat(const enum is_kel_no no, struct is_bsp_rtx_roi_stat *stat);

// RGL Rtx
void hw_is_bsp_set_rtx_rgl_stat_frame_ctrl(const enum is_kel_no no, const struct is_bsp_rtx_rgl_stat_frame_ctrl *cfg);
void hw_is_bsp_get_rtx_rgl_stat_frame_ctrl(const enum is_kel_no no, struct is_bsp_rtx_rgl_stat_frame_ctrl *cfg);
void hw_is_bsp_set_rtx_rgl_stat_enable(const enum is_kel_no no, uint8_t enable);
uint8_t hw_is_bsp_get_rtx_rgl_stat_enable(const enum is_kel_no no);
void hw_is_bsp_get_rtx_rgl_stat(const enum is_kel_no no, struct is_bsp_rtx_rgl_stat *stat);

// ROI Gwd
void hw_is_bsp_set_gwd_roi_stat_frame_ctrl(const enum is_kel_no no, uint8_t idx, const struct is_bsp_gwd_roi_stat_frame_ctrl *cfg);
void hw_is_bsp_get_gwd_roi_stat_frame_ctrl(const enum is_kel_no no, uint8_t idx, struct is_bsp_gwd_roi_stat_frame_ctrl *cfg);
void hw_is_bsp_set_gwd_roi_stat_enable(const enum is_kel_no no, uint8_t idx, uint8_t enable);
uint8_t hw_is_bsp_get_gwd_roi_stat_enable(const enum is_kel_no no, uint8_t idx);
void hw_is_bsp_get_gwd_roi_stat(const enum is_kel_no no, uint8_t idx, struct is_bsp_gwd_roi_stat *stat);
// RGL Gwd
void hw_is_bsp_set_gwd_rgl_stat_frame_ctrl(const enum is_kel_no no, const struct is_bsp_gwd_rgl_stat_frame_ctrl *cfg);
void hw_is_bsp_get_gwd_rgl_stat_frame_ctrl(const enum is_kel_no no, struct is_bsp_gwd_rgl_stat_frame_ctrl *cfg);
void hw_is_bsp_set_gwd_rgl_stat_enable(const enum is_kel_no no, uint8_t enable);
uint8_t hw_is_bsp_get_gwd_rgl_stat_enable(const enum is_kel_no no);
void hw_is_bsp_get_gwd_rgl_stat(const enum is_kel_no no, struct is_bsp_gwd_rgl_stat *stat);

// AF
// Ctrl
void hw_is_bsp_set_contrast_ctrl(const enum is_kel_no no, const struct is_bsp_contrast_ctrl *cfg);
void hw_is_bsp_get_contrast_ctrl(const enum is_kel_no no, struct is_bsp_contrast_ctrl *cfg);

// ROI Contrast
void hw_is_bsp_set_contrast_roi_stat_frame_ctrl(const enum is_kel_no no, uint8_t idx, const struct is_bsp_contrast_roi_stat_frame_ctrl *cfg);
void hw_is_bsp_get_contrast_roi_stat_frame_ctrl(const enum is_kel_no no, uint8_t idx, struct is_bsp_contrast_roi_stat_frame_ctrl *cfg);
void hw_is_bsp_set_contrast_roi_stat_enable(const enum is_kel_no no, uint8_t idx, uint8_t enable);
uint8_t hw_is_bsp_get_contrast_roi_stat_enable(const enum is_kel_no no, uint8_t idx);
void hw_is_bsp_get_contrast_roi_stat(const enum is_kel_no no, uint8_t idx, struct is_bsp_contrast_roi_stat *stat);
// RGL Contrast
void hw_is_bsp_set_contrast_rgl_stat_frame_ctrl(const enum is_kel_no no, const struct is_bsp_contrast_rgl_stat_frame_ctrl *cfg);
void hw_is_bsp_get_contrast_rgl_stat_frame_ctrl(const enum is_kel_no no, struct is_bsp_contrast_rgl_stat_frame_ctrl *cfg);
void hw_is_bsp_set_contrast_rgl_stat_enable(const enum is_kel_no no, uint8_t enable);
uint8_t hw_is_bsp_get_contrast_rgl_stat_enable(const enum is_kel_no no);
void hw_is_bsp_get_contrast_rgl_stat(const enum is_kel_no no, struct is_bsp_contrast_rgl_stat *stat);

/* Cfa phase en */
void hw_is_bsp_set_cfa_phase_enable_cfg(const enum is_kel_no no, const struct is_bsp_cfa_phase_enable_cfg *cfg);
void hw_is_bsp_get_cfa_phase_enable_cfg(const enum is_kel_no no, struct is_bsp_cfa_phase_enable_cfg *cfg);

/* Debug */
void hw_is_bsp_set_dbg_mon_sel(const enum is_kel_no no, uint8_t debug_mon_sel);
uint8_t hw_is_bsp_get_dbg_mon_sel(const enum is_kel_no no);

/* ATPG */
void hw_is_bsp_set_atpg_ctrl(const enum is_kel_no no, uint8_t atpg_ctrl_0);
uint8_t hw_is_bsp_get_atpg_ctrl(const enum is_kel_no no);

/* Efuse */
uint8_t hw_is_bsp_get_efuse_sensor_cfa_violation(const enum is_kel_no no);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//* for mpp is

/* IRQ mask */
void is_bsp_set_irq_mask_frame_end(volatile CsrBankBsp *csr, uint8_t irq_mask_frame_end);
uint8_t is_bsp_get_irq_mask_frame_end(volatile CsrBankBsp *csr);

/* Resolution */
void is_bsp_set_res(volatile CsrBankBsp *csr, const struct res *res);
void is_bsp_get_res(volatile CsrBankBsp *csr, struct res *res);

/* Input format */
void is_bsp_set_input_format(volatile CsrBankBsp *csr, const struct is_input_format *cfg);
void is_bsp_get_input_format(volatile CsrBankBsp *csr, struct is_input_format *cfg);

/* Output format */
void is_bsp_set_output_format(volatile CsrBankBsp *csr, const struct is_output_format *cfg);
void is_bsp_get_output_format(volatile CsrBankBsp *csr, struct is_output_format *cfg);

/* DPD + DPC */
void is_bsp_set_dpd_cfg(volatile CsrBankBsp *csr, const struct is_bsp_dpd_cfg *cfg);
void is_bsp_get_dpd_cfg(volatile CsrBankBsp *csr, struct is_bsp_dpd_cfg *cfg);
void is_bsp_set_dpc_cfg(volatile CsrBankBsp *csr, const struct is_bsp_dpc_cfg *cfg);
void is_bsp_get_dpc_cfg(volatile CsrBankBsp *csr, struct is_bsp_dpc_cfg *cfg);

/* Crosstalk */
void is_bsp_set_ct_cfg(volatile CsrBankBsp *csr, const struct is_bsp_ct_cfg *cfg);
void is_bsp_get_ct_cfg(volatile CsrBankBsp *csr, struct is_bsp_ct_cfg *cfg);

/* Channel Weighted Sum and Average */
void is_bsp_set_chn_wgt_cfg(volatile CsrBankBsp *csr, const struct is_bsp_chn_wgt_cfg *cfg);
void is_bsp_get_chn_wgt_cfg(volatile CsrBankBsp *csr, struct is_bsp_chn_wgt_cfg *cfg);

/* Luma Estimation*/
void is_bsp_set_luma_est_cfg(volatile CsrBankBsp *csr, const struct is_bsp_luma_est_cfg *cfg);
void is_bsp_get_luma_est_cfg(volatile CsrBankBsp *csr, struct is_bsp_luma_est_cfg *cfg);

/* Re-mosaic */
void is_bsp_set_remosaic_cfg(volatile CsrBankBsp *csr, const struct is_bsp_remosaic_cfg *cfg);
void is_bsp_get_remosaic_cfg(volatile CsrBankBsp *csr, struct is_bsp_remosaic_cfg *cfg);

/* TM */
void is_bsp_set_y_est_mode_ctrl(volatile CsrBankBsp *csr, const struct is_bsp_y_est_mode_ctrl *cfg);
void is_bsp_get_y_est_mode_ctrl(volatile CsrBankBsp *csr, struct is_bsp_y_est_mode_ctrl *cfg);
void is_bsp_set_tm_cfg(volatile CsrBankBsp *csr, const struct is_bsp_tm_cfg *cfg);
void is_bsp_get_tm_cfg(volatile CsrBankBsp *csr, struct is_bsp_tm_cfg *cfg);

/* WB */
void is_bsp_set_wb_cfg(volatile CsrBankBsp *csr, const struct is_bsp_wb_cfg *cfg);
void is_bsp_get_wb_cfg(volatile CsrBankBsp *csr, struct is_bsp_wb_cfg *cfg);

/* Statistics */

// CSR2SRAM
void is_bsp_set_csr2sram_sel(volatile CsrBankBsp *csr, uint8_t enable);
uint8_t is_bsp_get_csr2sram_sel(volatile CsrBankBsp *csr);

// AE
// Ctrl
void is_bsp_set_y_hist_roi_ctrl(volatile CsrBankBsp *csr, const struct is_bsp_y_hist_roi_ctrl *cfg);
void is_bsp_get_y_hist_roi_ctrl(volatile CsrBankBsp *csr, struct is_bsp_y_hist_roi_ctrl *cfg);

void is_bsp_set_y_avg_roi_ctrl(volatile CsrBankBsp *csr, const struct is_bsp_y_avg_roi_ctrl *cfg);
void is_bsp_get_y_avg_roi_ctrl(volatile CsrBankBsp *csr, struct is_bsp_y_avg_roi_ctrl *cfg);

void is_bsp_set_y_avg_rgl_ctrl(volatile CsrBankBsp *csr, const struct is_bsp_y_avg_rgl_ctrl *cfg);
void is_bsp_get_y_avg_rgl_ctrl(volatile CsrBankBsp *csr, struct is_bsp_y_avg_rgl_ctrl *cfg);

// ROI Y hist
void is_bsp_set_y_hist_roi_stat_frame_ctrl(volatile CsrBankBsp *csr, const struct is_bsp_y_hist_roi_stat_frame_ctrl *cfg);
void is_bsp_get_y_hist_roi_stat_frame_ctrl(volatile CsrBankBsp *csr, struct is_bsp_y_hist_roi_stat_frame_ctrl *cfg);
void is_bsp_set_y_hist_roi_stat_enable(volatile CsrBankBsp *csr, uint8_t enable);
uint8_t is_bsp_get_y_hist_roi_stat_enable(volatile CsrBankBsp *csr);
void is_bsp_get_y_hist_roi_stat(volatile CsrBankBsp *csr, struct is_bsp_y_hist_roi_stat *stat);

// ROI Y avg
void is_bsp_set_y_avg_roi_stat_frame_ctrl(volatile CsrBankBsp *csr, uint8_t idx, const struct is_bsp_y_avg_roi_stat_frame_ctrl *cfg);
void is_bsp_get_y_avg_roi_stat_frame_ctrl(volatile CsrBankBsp *csr, uint8_t idx, struct is_bsp_y_avg_roi_stat_frame_ctrl *cfg);
void is_bsp_set_y_avg_roi_stat_enable(volatile CsrBankBsp *csr, uint8_t idx, uint8_t enable);
uint8_t is_bsp_get_y_avg_roi_stat_enable(volatile CsrBankBsp *csr, uint8_t idx);
void is_bsp_get_y_avg_roi_stat(volatile CsrBankBsp *csr, uint8_t idx, struct is_bsp_y_avg_roi_stat *stat);

// RGL Y avg
void is_bsp_set_y_avg_rgl_stat_frame_ctrl(volatile CsrBankBsp *csr, const struct is_bsp_y_avg_rgl_stat_frame_ctrl *cfg);
void is_bsp_get_y_avg_rgl_stat_frame_ctrl(volatile CsrBankBsp *csr, struct is_bsp_y_avg_rgl_stat_frame_ctrl *cfg);
void is_bsp_set_y_avg_rgl_stat_enable(volatile CsrBankBsp *csr, uint8_t enable);
uint8_t is_bsp_get_y_avg_rgl_stat_enable(volatile CsrBankBsp *csr);
void is_bsp_get_y_avg_rgl_stat(volatile CsrBankBsp *csr, struct is_bsp_y_avg_rgl_stat *stat);

// AWB
// Ctrl
void is_bsp_set_rtx_ctrl(volatile CsrBankBsp *csr, const struct is_bsp_rtx_ctrl *cfg);
void is_bsp_get_rtx_ctrl(volatile CsrBankBsp *csr, struct is_bsp_rtx_ctrl *cfg);

void is_bsp_set_gwd_ctrl(volatile CsrBankBsp *csr, const struct is_bsp_gwd_ctrl *cfg);
void is_bsp_get_gwd_ctrl(volatile CsrBankBsp *csr, struct is_bsp_gwd_ctrl *cfg);

// ROI Rtx
void is_bsp_set_rtx_roi_stat_frame_ctrl(volatile CsrBankBsp *csr, const struct is_bsp_rtx_roi_stat_frame_ctrl *cfg);
void is_bsp_get_rtx_roi_stat_frame_ctrl(volatile CsrBankBsp *csr, struct is_bsp_rtx_roi_stat_frame_ctrl *cfg);
void is_bsp_set_rtx_roi_stat_enable(volatile CsrBankBsp *csr, uint8_t enable);
uint8_t is_bsp_get_rtx_roi_stat_enable(volatile CsrBankBsp *csr);
void is_bsp_get_rtx_roi_stat(volatile CsrBankBsp *csr, struct is_bsp_rtx_roi_stat *stat);

// RGL Rtx
void is_bsp_set_rtx_rgl_stat_frame_ctrl(volatile CsrBankBsp *csr, const struct is_bsp_rtx_rgl_stat_frame_ctrl *cfg);
void is_bsp_get_rtx_rgl_stat_frame_ctrl(volatile CsrBankBsp *csr, struct is_bsp_rtx_rgl_stat_frame_ctrl *cfg);
void is_bsp_set_rtx_rgl_stat_enable(volatile CsrBankBsp *csr, uint8_t enable);
uint8_t is_bsp_get_rtx_rgl_stat_enable(volatile CsrBankBsp *csr);
void is_bsp_get_rtx_rgl_stat(volatile CsrBankBsp *csr, struct is_bsp_rtx_rgl_stat *stat);

// ROI Gwd
void is_bsp_set_gwd_roi_stat_frame_ctrl(volatile CsrBankBsp *csr, uint8_t idx, const struct is_bsp_gwd_roi_stat_frame_ctrl *cfg);
void is_bsp_get_gwd_roi_stat_frame_ctrl(volatile CsrBankBsp *csr, uint8_t idx, struct is_bsp_gwd_roi_stat_frame_ctrl *cfg);
void is_bsp_set_gwd_roi_stat_enable(volatile CsrBankBsp *csr, uint8_t idx, uint8_t enable);
uint8_t is_bsp_get_gwd_roi_stat_enable(volatile CsrBankBsp *csr, uint8_t idx);
void is_bsp_get_gwd_roi_stat(volatile CsrBankBsp *csr, uint8_t idx, struct is_bsp_gwd_roi_stat *stat);
// RGL Gwd
void is_bsp_set_gwd_rgl_stat_frame_ctrl(volatile CsrBankBsp *csr, const struct is_bsp_gwd_rgl_stat_frame_ctrl *cfg);
void is_bsp_get_gwd_rgl_stat_frame_ctrl(volatile CsrBankBsp *csr, struct is_bsp_gwd_rgl_stat_frame_ctrl *cfg);
void is_bsp_set_gwd_rgl_stat_enable(volatile CsrBankBsp *csr, uint8_t enable);
uint8_t is_bsp_get_gwd_rgl_stat_enable(volatile CsrBankBsp *csr);
void is_bsp_get_gwd_rgl_stat(volatile CsrBankBsp *csr, struct is_bsp_gwd_rgl_stat *stat);

// AF
// Ctrl
void is_bsp_set_contrast_ctrl(volatile CsrBankBsp *csr, const struct is_bsp_contrast_ctrl *cfg);
void is_bsp_get_contrast_ctrl(volatile CsrBankBsp *csr, struct is_bsp_contrast_ctrl *cfg);

// ROI Contrast
void is_bsp_set_contrast_roi_stat_frame_ctrl(volatile CsrBankBsp *csr, uint8_t idx, const struct is_bsp_contrast_roi_stat_frame_ctrl *cfg);
void is_bsp_get_contrast_roi_stat_frame_ctrl(volatile CsrBankBsp *csr, uint8_t idx, struct is_bsp_contrast_roi_stat_frame_ctrl *cfg);
void is_bsp_set_contrast_roi_stat_enable(volatile CsrBankBsp *csr, uint8_t idx, uint8_t enable);
uint8_t is_bsp_get_contrast_roi_stat_enable(volatile CsrBankBsp *csr, uint8_t idx);
void is_bsp_get_contrast_roi_stat(volatile CsrBankBsp *csr, uint8_t idx, struct is_bsp_contrast_roi_stat *stat);
// RGL Contrast
void is_bsp_set_contrast_rgl_stat_frame_ctrl(volatile CsrBankBsp *csr, const struct is_bsp_contrast_rgl_stat_frame_ctrl *cfg);
void is_bsp_get_contrast_rgl_stat_frame_ctrl(volatile CsrBankBsp *csr, struct is_bsp_contrast_rgl_stat_frame_ctrl *cfg);
void is_bsp_set_contrast_rgl_stat_enable(volatile CsrBankBsp *csr, uint8_t enable);
uint8_t is_bsp_get_contrast_rgl_stat_enable(volatile CsrBankBsp *csr);
void is_bsp_get_contrast_rgl_stat(volatile CsrBankBsp *csr, struct is_bsp_contrast_rgl_stat *stat);

/* Cfa phase en */
void is_bsp_set_cfa_phase_enable_cfg(volatile CsrBankBsp *csr, const struct is_bsp_cfa_phase_enable_cfg *cfg);
void is_bsp_get_cfa_phase_enable_cfg(volatile CsrBankBsp *csr, struct is_bsp_cfa_phase_enable_cfg *cfg);

/* Debug */
void is_bsp_set_dbg_mon_sel(volatile CsrBankBsp *csr, uint8_t debug_mon_sel);
uint8_t is_bsp_get_dbg_mon_sel(volatile CsrBankBsp *csr);

/* ATPG */
void is_bsp_set_atpg_ctrl(volatile CsrBankBsp *csr, uint8_t atpg_ctrl);
uint8_t is_bsp_get_atpg_ctrl(volatile CsrBankBsp *csr);

/* Efuse */
uint8_t is_bsp_get_efuse_sensor_cfa_violation(volatile CsrBankBsp *csr);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//* need to remove *//

// Move to test
#define IS_BSP_CFA_PHASE_IDX 3
#define IS_BSP_CFA_PHASE_NUM 16
#define IS_CHANNEL_WEIGHT_IDX 2
#define IS_CHANNEL_WEIGHT_NUM 6
#define IS_LUMA_EST_WEIGHT_IDX 3
#define IS_LUMA_EST_WEIGHT_NUM 4
#define IS_REMOSAIC_IDX 2
#define IS_REMOSAIC_NUM 12
#define IS_BSP_TONE_MAPPING_IDX 2
#define IS_BSP_TONE_MAPPING_NUM 58
#define IS_BSP_WB_GAIN_IDX 2
#define IS_BSP_WB_GAIN_NUM 4
#define IS_BSP_AE_STA_ROI_NUM 4
#define IS_BSP_AWB_GWD_RG_IDX 2
#define IS_BSP_AWB_GWD_RG_NUM 5
#define IS_BSP_AWB_GWD_BG_IDX 2
#define IS_BSP_AWB_GWD_BG_NUM 5
#define IS_BSP_AWB_GWD_IDX 2
#define IS_BSP_AWB_GWD_NUM 4
#define IS_BSP_AF_CFA_PHASE_EN_NUM 5
#define IS_BSP_AF_H_WEIGHT_IDX 2
#define IS_BSP_AF_H_WEIGHT_NUM 10
#define IS_BSP_AF_V_WEIGHT_IDX 2
#define IS_BSP_AF_V_WEIGHT_NUM 3
#define IS_BSP_AF_ROI_NUM 4
#define IS_BSP_AF_STA_ROI_NUM 4

typedef struct is_bsp_roi {
	uint32_t sx;
	uint32_t ex;
	uint32_t sy;
	uint32_t ey;
} Is_Bsp_Roi;

typedef struct is_bsp_resolution {
	uint32_t width;
	uint32_t height;
} IsBspResolution;

typedef struct is_bsp_cfa_set {
	uint32_t cfa_mode;
	uint32_t bayer_ini_phase_i;
	uint32_t bayer_ini_phase_o;
	uint32_t mono_o;
	uint32_t cfa_phase[IS_BSP_CFA_PHASE_NUM];
} IsBspCfaSet;

typedef struct is_bsp_dpc {
	uint32_t sdpd_enable;
	uint32_t sdpd_offset_x;
	uint32_t sdpd_offset_y;
	uint32_t ddpd_enable;
	uint32_t ddpd_g_pattern_mode;
	uint32_t ddpd_pix_num_mode;
	uint32_t ddpd_diff_ratio;
	uint32_t ddpd_avg_ratio;
	uint32_t ddpd_edge_diff_ratio;
	uint32_t ddpd_edge_avg_ratio;
	uint32_t ddpd_edge_m;
	//uint32_t ddpd_dp_num;
	uint32_t dpc_enable;
	uint32_t dpc_dir_enable;
	uint32_t dpc_dir_th_ratio;
	uint32_t dpc_dir_str_m;
} IsBspDpc;

typedef struct is_bsp_crosstalk {
	uint32_t ct_enable;
	uint32_t ct_avg_lb;
	uint32_t ct_avg_ratio;
	uint32_t ct_str_lb;
	uint32_t ct_str_ratio;
	uint32_t ct_str_m;
} IsBspCrossTalk;

typedef struct is_bsp_channe_lweight {
	uint32_t channel_weight[IS_CHANNEL_WEIGHT_NUM];
} IsBspChannelWeight;

typedef struct is_bsp_luma_estimation {
	uint32_t y_est_mode;
	uint32_t y_est_weight[IS_LUMA_EST_WEIGHT_NUM];
} IsBspLumaEstimation;

typedef struct is_bsp_remosaic {
	uint32_t remos_enable;
	uint32_t remos_coeff[IS_REMOSAIC_NUM];
} IsBspRemosaic;

typedef struct is_bsp_tone_mapping {
	uint32_t tm_enable;
	uint32_t tone_curve[IS_BSP_TONE_MAPPING_NUM];
} IsBspToneMapping;

typedef struct is_bsp_wb_gain {
	uint32_t wb_enable;
	uint32_t wb_gain[IS_BSP_WB_GAIN_NUM];
} IsBspWbGain;

typedef struct is_bsp_ae_statistic {
	uint32_t y_hist_in_mode;
	uint32_t y_hist_clear;
	uint32_t y_hist_roi_en;
	Is_Bsp_Roi y_hist_roi;
	uint32_t y_hist_offset;

	uint32_t roi_y_avg_in_mode;
	uint32_t roi_y_avg_en[IS_BSP_AE_STA_ROI_NUM];
	uint32_t roi_y_avg_clear[IS_BSP_AE_STA_ROI_NUM];
	Is_Bsp_Roi roi_y_avg_roi_0;
	Is_Bsp_Roi roi_y_avg_roi_1;
	Is_Bsp_Roi roi_y_avg_roi_2;
	Is_Bsp_Roi roi_y_avg_roi_3;
	uint32_t roi_y_avg_pix_num[IS_BSP_AE_STA_ROI_NUM];

	uint32_t rgl_y_avg_en;
	uint32_t rgl_y_avg_clear;
	uint32_t rgl_y_avg_in_mode;
	uint32_t rgl_y_avg_blk_ht;
	uint32_t rgl_y_avg_blk_wd;
	uint32_t rgl_y_avg_pix_num;
	Is_Bsp_Roi rgl_y_avg_roi;

} IsBspAeStatistic;

typedef struct is_bsp_awb_statistic {
	uint32_t awb_rtx_region_mode;
	uint32_t awb_rtx_y_mode;
	uint32_t awb_rtx_y_th_min;
	uint32_t awb_rtx_y_th_max;
	uint32_t awb_rtx_y_m;

	uint32_t awb_rtx_roi_en;
	uint32_t awb_rtx_clear;
	Is_Bsp_Roi awb_rtx_roi;

	uint32_t rgl_awb_rtx_en;
	uint32_t rgl_awb_rtx_clear;
	uint32_t rgl_awb_rtx_blk_ht;
	uint32_t rgl_awb_rtx_blk_wd;
	Is_Bsp_Roi rgl_awb_rtx_roi;

	uint32_t awb_gwd_y_mode;
	uint32_t awb_gwd_y_th_max;
	uint32_t awb_gwd_y_th_min;
	uint32_t awb_gwd_y_m;
	uint32_t awb_gwd_rg[IS_BSP_AWB_GWD_RG_NUM];
	uint32_t awb_gwd_bg[IS_BSP_AWB_GWD_BG_NUM];
	uint32_t awb_gwd_a_2s[IS_BSP_AWB_GWD_NUM];
	uint32_t awb_gwd_b_2s[IS_BSP_AWB_GWD_NUM];
	uint32_t awb_gwd_c_2s[IS_BSP_AWB_GWD_NUM];
	uint32_t awb_gwd_rto_bg_th[IS_BSP_AWB_GWD_NUM];
	uint32_t awb_gwd_rto_bg_m[IS_BSP_AWB_GWD_NUM];
	uint32_t roi_awb_gwd_en[IS_BSP_AWB_GWD_NUM];
	uint32_t roi_awb_gwd_clear[IS_BSP_AWB_GWD_NUM];
	Is_Bsp_Roi roi_awb_gwd_0;
	Is_Bsp_Roi roi_awb_gwd_1;
	Is_Bsp_Roi roi_awb_gwd_2;
	Is_Bsp_Roi roi_awb_gwd_3;

	uint32_t rgl_awb_gwd_en;
	uint32_t rgl_awb_gwd_clear;
	uint32_t rgl_awb_gwd_blk_ht;
	uint32_t rgl_awb_gwd_blk_wd;
	uint32_t rgl_awb_gwd_sx;
	uint32_t rgl_awb_gwd_ex;
	uint32_t rgl_awb_gwd_sy;
	uint32_t rgl_awb_gwd_ey;
} IsBspAwbStatistic;

typedef struct is_bsp_af_statistic {
	uint32_t contrast_y_mode;
	uint32_t contrast_cfa_phase_en[IS_BSP_AF_CFA_PHASE_EN_NUM];
	uint32_t contrast_y_gain_th_max;
	uint32_t contrast_y_gain_th_min;
	uint32_t contrast_y_gain_m;

	uint32_t contrast_h1_weight[IS_BSP_AF_H_WEIGHT_NUM];
	uint32_t contrast_h2_weight[IS_BSP_AF_H_WEIGHT_NUM];
	uint32_t contrast_v1_weight[IS_BSP_AF_V_WEIGHT_NUM];
	uint32_t contrast_v2_weight[IS_BSP_AF_V_WEIGHT_NUM];

	uint32_t roi_contrast_en[IS_BSP_AF_ROI_NUM];
	uint32_t roi_contrast_clear[IS_BSP_AF_ROI_NUM];
	Is_Bsp_Roi roi_contrast_0;
	Is_Bsp_Roi roi_contrast_1;
	Is_Bsp_Roi roi_contrast_2;
	Is_Bsp_Roi roi_contrast_3;
	uint32_t roi_contrast_pix_num[IS_BSP_AF_STA_ROI_NUM];
	uint32_t contrast_coring_th;

	uint32_t rgl_contrast_en;
	uint32_t rgl_contrast_clear;
	uint32_t rgl_contrast_blk_ht;
	uint32_t rgl_contrast_blk_wd;
	Is_Bsp_Roi rgl_contrast;
	uint32_t rgl_contrast_pix_num;

} IsBspAfStatistic;

typedef struct is_bsp_ctx {
	IsBspResolution bsp_res;
	IsBspCfaSet bsp_cfa_set;
	IsBspDpc bsp_dpc;
	IsBspCrossTalk bsp_ct;
	IsBspChannelWeight bsp_channel_weight;
	IsBspLumaEstimation bsp_luma_est;
	IsBspRemosaic bsp_remosaic;
	IsBspToneMapping bsp_tm;
	IsBspWbGain bsp_wb_gain;
	IsBspAeStatistic bsp_ae_sta;
	IsBspAwbStatistic bsp_awb_sta;
	IsBspAfStatistic bsp_af_sta;
} IsBspCtx;

typedef struct is_bsp_attr {
	uint32_t bsp_cfa_mode;
	uint32_t bsp_bayer_ini_phase_i;
	uint32_t bsp_bayer_ini_phase_o;
	uint32_t bsp_mono_o;
	uint32_t bsp_width;
	uint32_t bsp_height;
	uint32_t bsp_sdpd_enable;
	uint32_t bsp_ddpd_enable;
	uint32_t bsp_dpc_enable;
	uint32_t bsp_dpc_dir_enable;
	uint32_t bsp_ct_enable;
	uint32_t bsp_y_est_mode;
	uint32_t bsp_y_est_weight_idx;
	uint32_t bsp_remos_enable;
	uint32_t bsp_tm_enable;
	uint32_t bsp_wb_enable;
	uint32_t bsp_y_hist_roi_en;
	uint32_t bsp_y_hist_in_mode;
	Is_Bsp_Roi bsp_y_hist_roi;
	uint32_t bsp_y_hist_offset;
	uint32_t bsp_roi_y_avg_en[IS_BSP_AE_STA_ROI_NUM];
	uint32_t bsp_roi_y_avg_in_mode;
	Is_Bsp_Roi bsp_roi_y_avg_roi_0;
	Is_Bsp_Roi bsp_roi_y_avg_roi_1;
	Is_Bsp_Roi bsp_roi_y_avg_roi_2;
	Is_Bsp_Roi bsp_roi_y_avg_roi_3;
	uint32_t bsp_rgl_y_avg_en;
	uint32_t bsp_rgl_y_avg_in_mode;
	uint32_t bsp_rgl_y_avg_blk_ht;
	uint32_t bsp_rgl_y_avg_blk_wd;
	Is_Bsp_Roi bsp_rgl_y_avg_roi;
	uint32_t bsp_awb_rtx_y_mode;
	uint32_t bsp_awb_rtx_region_mode;
	uint32_t bsp_awb_rtx_roi_en;
	Is_Bsp_Roi bsp_awb_rtx_roi;
	uint32_t bsp_rgl_awb_rtx_en;
	Is_Bsp_Roi bsp_rgl_awb_rtx_roi;
	uint32_t bsp_rgl_awb_rtx_blk_ht;
	uint32_t bsp_rgl_awb_rtx_blk_wd;
	uint32_t bsp_awb_gwd_y_mode;
	uint32_t bsp_awb_gwd_rg[IS_BSP_AWB_GWD_RG_NUM];
	uint32_t bsp_awb_gwd_bg[IS_BSP_AWB_GWD_BG_NUM];
	uint32_t bsp_awb_gwd_a_2s[IS_BSP_AWB_GWD_NUM];
	uint32_t bsp_awb_gwd_b_2s[IS_BSP_AWB_GWD_NUM];
	uint32_t bsp_awb_gwd_c_2s[IS_BSP_AWB_GWD_NUM];
	uint32_t bsp_awb_gwd_rto_bg_th[IS_BSP_AWB_GWD_NUM];
	uint32_t bsp_awb_gwd_rto_bg_m[IS_BSP_AWB_GWD_NUM];
	uint32_t bsp_roi_awb_gwd_en[4];
	Is_Bsp_Roi bsp_roi_awb_gwd_roi_0;
	Is_Bsp_Roi bsp_roi_awb_gwd_roi_1;
	Is_Bsp_Roi bsp_roi_awb_gwd_roi_2;
	Is_Bsp_Roi bsp_roi_awb_gwd_roi_3;
	uint32_t bsp_rgl_awb_gwd_en;
	Is_Bsp_Roi bsp_rgl_awb_gwd_roi;
	uint32_t bsp_rgl_awb_gwd_blk_ht;
	uint32_t bsp_rgl_awb_gwd_blk_wd;
	uint32_t bsp_contrast_y_mode;
	uint32_t bsp_contrast_cfa_phase_en[5];
	uint32_t contrast_y_gain_th_max;
	uint32_t contrast_y_gain_th_min;
	uint32_t contrast_y_gain_m;
	uint32_t contrast_coring_th;
	uint32_t bsp_contrast_h1_weight[IS_BSP_AF_H_WEIGHT_NUM];
	uint32_t bsp_contrast_h2_weight[IS_BSP_AF_H_WEIGHT_NUM];
	uint32_t bsp_contrast_v1_weight[IS_BSP_AF_V_WEIGHT_NUM];
	uint32_t bsp_contrast_v2_weight[IS_BSP_AF_V_WEIGHT_NUM];
	uint32_t bsp_roi_contrast_en[4];
	Is_Bsp_Roi bsp_roi_contrast_0;
	Is_Bsp_Roi bsp_roi_contrast_1;
	Is_Bsp_Roi bsp_roi_contrast_2;
	Is_Bsp_Roi bsp_roi_contrast_3;
	uint32_t bsp_rgl_contrast_en;
	uint32_t bsp_rgl_contrast_blk_ht;
	uint32_t bsp_rgl_contrast_blk_wd;
	Is_Bsp_Roi bsp_rgl_contrast;

} IsBspAttr;

void hw_is_bsp_frame_start(void);
void hw_is_bsp_irq_clear_frame_end(void);
uint32_t hw_is_bsp_status_frame_end(void);
void hw_is_bsp_irq_mask_frame_end(uint32_t irq_mask_frame_end);
void hw_is_bsp_resolution(uint32_t width, uint32_t height);
void hw_is_bsp_bypass(uint32_t bayer_ini_phase_i, uint32_t bayer_ini_phase_o);
void hww_is_bsp_bypass(const enum is_kel_no no, uint32_t bayer_ini_phase_i, uint32_t bayer_ini_phase_o);
void hw_is_bsp_debug_mon_sel(uint32_t debug_mon_sel);
void hw_is_bsp_dump_reg(void);

void hw_is_bsp_run(IsBspAttr *bspattr);
void hw_is_bsp_set_res_ctx(IsBspCtx *bspctx, IsBspAttr *bspattr);
void hw_is_bsp_set_cfa_ctx(IsBspCtx *bspctx, IsBspAttr *bspattr);
void hw_is_bsp_set_dpc_ctx(IsBspCtx *bspctx, IsBspAttr *bspattr);
void hw_is_bsp_set_ct_ctx(IsBspCtx *bspctx, IsBspAttr *bspattr);
void hw_is_bsp_set_chhannelweighting_ctx(IsBspCtx *bspctx);
void hw_is_bsp_set_lumaest_ctx(IsBspCtx *bspctx, IsBspAttr *bspattr);
void hw_is_bsp_set_remosaic_ctx(IsBspCtx *bspctx, IsBspAttr *bspattr);
void hw_is_bsp_set_tm_ctx(IsBspCtx *bspctx, IsBspAttr *bspattr);
void hw_is_bsp_set_wb_gain_ctx(IsBspCtx *bspctx, IsBspAttr *bspattr);
void hw_is_bsp_set_ae_statistic_ctx(IsBspCtx *bspctx, IsBspAttr *bspattr);
void hw_is_bsp_set_awb_statistic_ctx(IsBspCtx *bspctx, IsBspAttr *bspattr);
void hw_is_bsp_set_af_statistic_ctx(IsBspCtx *bspctx, IsBspAttr *bspattr);
void hw_is_bsp_set_reg(IsBspCtx *bspctx);

void hw_is_bsp_get_ae_statistic_ctx(void);

#endif /* KYOTO_BSP_H_ */