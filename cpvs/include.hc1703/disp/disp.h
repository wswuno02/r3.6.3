#ifndef KYOTO_DISP_H_
#define KYOTO_DISP_H_

#include "printf.h"
#include "da/da_define.h"

#define u64 uint64_t
#define u32 uint32_t
#define s32 int
#define s16 short
#define u16 unsigned short
#define u8 uint8_t

#define FPGA 0

#define DISP_DISP_BASE ((CsrBankDisp *)0x83300000)
#define DISP_CHECKSUM_BASE ((CsrBankDisp_checksum *)0x83300800)
#define DISP_CFG_BASE ((CsrBankDisp_cfg *)0x83300400)
#define DISP_CCQ_BASE ((CsrBankCcq *)0x83310000)
#define DISP_CCQW_BASE ((CsrBankCcqw *)0x83310800)
#define DISP_CCQR_BASE ((CsrBankOsdr *)0x83310400)
#define DISP_PXR_BASE ((CsrBankPxr *)0x83320000)
#define DISP_CS_BASE ((CsrBankCs *)0x83330000)
#define DISP_PG_BASE ((CsrBankPg *)0x83340000)
#define DISP_FRAME_TIME_GEN_BASE ((CsrBankFrame_time_gen *)0x83340400)
#define DISP_420_TO_422_BASE ((CsrBankD420to422 *)0x83350000)
#define DISP_422_TO_444_BASE ((CsrBankD422to444 *)0x83360000)
#define DISP_OSD_BASE ((CsrBankDosd *)0x83370000)
#define DISP_OSD_READ0_BASE ((CsrBankDosd_read *)0x83380000)
#define DISP_OSD_READ1_BASE ((CsrBankDosd_read *)0x83390000)
#define DISP_CST_BASE ((CsrBankDcst *)0x833A0000)
#define DISP_DGMA_BASE ((CsrBankDgma *)0x833B0000)
#define DISP_CFA_BASE ((CsrBankDcfa *)0x833C0000)
#define DISP_DCS_BASE ((CsrBankCs *)0x833D0000)
#define DISP_TX_CTRL_BASE ((CsrBankTx_ctrl *)0x83400400)
#define DISP_FM_BASE ((CsrBankFm *)0x83400800)
#define DISP_TX_PHYCFG_BASE ((CsrBankTx_phycfg *)0x83400000)
#define DISP_TX_ENC_BASE ((CsrBankTx_enc *)0x83410400)
#define DISP_TX_OUT_BASE ((CsrBankTx_out *)0x83410000)
#define DISP_PDI_BASE ((CsrBankPdi *)0x83420000)
#define DISP_DISP_SYSCFG_BASE ((CsrBankDispif_syscfg *)0x83430000)
#define DISP_DISP_CTR_BASE ((CsrBankDispif_ctr *)0x83430400)

#define DISP_DRAM_AGENT_DISPR (DRAM_AGENT_DISPR - DRAM_AGENT_ENC_END)
#define DISP_DRAM_AGENT_DOSDR (DRAM_AGENT_DOSDR - DRAM_AGENT_ENC_END)

#define MAX_DSI_COMMAND 256
#define DISP_DRAM_AGENT_DISP_NUM 2
#define MAX_OSD_REGION 4
#define MAX_BOUNDING_BOX 8

#if 1
#define disp_print(format, args...) printf("[%s:%d] " format, __func__, __LINE__, ##args)
#define disp_dump_print(format, args...) printf("[%s:%d] " format, __func__, __LINE__, ##args, ##args)
#else
#define disp_print(format, args...) ;
#define disp_dump_print(format, args...) ;
#endif

typedef enum disp_pattern_gen_mode {
	DISP_PG_RAMP = 0,
	DISP_PG_COLOR_BAR,
	DISP_PG_GRID,
	DISP_PG_RANDOM,
} DispPatternGenMode;

typedef enum disp_mipi_enc_status {
	DISP_MIPI_ENC_IDLE = 1,
	DISP_MIPI_ENC_START = 2,
	DISP_MIPI_ENC_RUN = 4,
	DISP_MIPI_ENC_READY_TO_STOP = 8,
} DispMipiEncStatus;

typedef enum disp_dump_reg {
	DISP_DUMP_REG_DISP,
	DISP_DUMP_REG_CHECKSUM,
	DISP_DUMP_REG_CFG,
	DISP_DUMP_REG_CCQ,
	DISP_DUMP_REG_CCQW,
	DISP_DUMP_REG_CCQR,
	DISP_DUMP_REG_PXR,
	DISP_DUMP_REG_CS,
	DISP_DUMP_REG_PG,
	DISP_DUMP_REG_FRAME_TIME_GEN,
	DISP_DUMP_REG_420_TO_422,
	DISP_DUMP_REG_422_TO_444,
	DISP_DUMP_REG_OSD,
	DISP_DUMP_REG_OSD_READ_0,
	DISP_DUMP_REG_OSD_READ_1,
	DISP_DUMP_REG_CST,
	DISP_DUMP_REG_DGMA,
	DISP_DUMP_REG_CFA,
	DISP_DUMP_REG_DCS,
	DISP_DUMP_REG_TX_CTRL,
	DISP_DUMP_REG_FM,
	DISP_DUMP_REG_TX_PHYCFG,
	DISP_DUMP_REG_MIPI_TX_ENC,
	DISP_DUMP_REG_TX_OUT,
	DISP_DUMP_REG_PDI,
	DISP_DUMP_REG_DISPIF_SYSCFG,
	DISP_DUMP_REG_CTR,
	DISP_DUMP_REG_ALL,
} DispDumpReg;

typedef enum disp_output_interface {
	DISP_OUTPUT_INTERFACE_MIPI,
	DISP_OUTPUT_INTERFACE_PDI,
} DispOutputInterface;

typedef enum disp_pdi_format {
	DISP_PDI_BT1120,
	DISP_PDI_BT656,
	DISP_PDI_RGB888,
	DISP_PDI_RGB666,
	DISP_PDI_RGB565,
	DISP_PDI_RGB444,
	DISP_PDI_RGB_INTERFACE,
	DISP_PDI_MCU_UP,
	DISP_PDI_MCU_DN,
} DispPdiFormat;

typedef enum disp_mipi_spec {
	DISP_SPEC_DSI,
	DISP_SPEC_CSI,
} DispMipiSpec;

typedef enum disp_input_type {
	DISP_INPUT_420D,
	DISP_INPUT_420U,
	DISP_INPUT_BAYER,
} DispInputType;

typedef enum disp_output_format {
	DISP_OUTPUT_CFA,
	DISP_OUTPUT_RGB,
	DISP_OUTPUT_422,
	DISP_OUTPUT_420,
	DISP_OUTPUT_OSD_BAYER,
	DISP_OUTPUT_BAYER,
} DispOutputFormat;

typedef enum disp_csi_output_foramt {
	DISP_CSI_RAW8 = 3,
	DISP_CSI_RAW10,
	DISP_CSI_RAW12,
	DISP_CSI_RAW14,
	DISP_CSI_RAW16,
	DISP_CSI_YUV420B8,
	DISP_CSI_YUV420B10,
	DISP_CSI_YUV420LB8, //not support
	DISP_CSI_YUV420CSB8 = 12,
	DISP_CSI_YUV420CSB10 = 13,
	DISP_CSI_YUV422B8 = 14,
	DISP_CSI_YUV422B10 = 15,
} DispCsiOutputFormat;

typedef enum disp_dsi_output_foramt {
	DISP_DSI_RGB565 = 0,
	DISP_DSI_RGB666,
	DISP_DSI_RGB666L,
	DISP_DSI_RGB888,
	DISP_DSI_RGB10,
	DISP_DSI_RGB12,
	DISP_DSI_YUV420B8 = 8,
	DISP_DSI_YUV422B8 = 12,
	DISP_DSI_YUV422B10 = 13,
	DISP_DSI_YUV422B12 = 14,
} DispDsiOutputFormat;

typedef enum disp_dsi_mode {
	DISP_DSI_VIDEO,
	DISP_DSI_HS_COMMAND,
	DISP_DSI_COMMAND,
	DISP_DSI_BTA,
} DispDsiMode;

typedef enum osd_mode {
	OSD_PALETTE_16BIT_MODE,
	OSD_AYUV_3544_MODE,
	OSD_ALPHA_MODE = 8,
	OSD_PALETTE_8IT_MODE = 9,
} OsdMode;

typedef enum osd_yuv_sel {
	OSD_YUV_DISABLE = 0,
	OSD_Y_ENABLE = 1,
	OSD_U_ENABLE = 2,
	OSD_V_ENABLE = 4,
	OSD_UV_ENABLE = 6,
	OSD_YUV_ENABLE = 7,
} OsdYuvSel;

typedef enum disp_buffer_type {
	DISP_BUFEER_TYPE_DISPR,
} DispBufferType;

typedef struct disp_bounding_box_attr {
	u32 enable;
	u32 mask; /*fill bounding box rectangle region*/
	u32 start_x;
	u32 start_y;
	u32 end_x;
	u32 end_y;
	u32 yuv_y;
	u32 yuv_u;
	u32 yuv_v;
	u32 line_width;
	u32 line_alpha;
} DispBoundingBoxAttr;

typedef struct enc_bounding_box_config {
	u32 bb_num;
	u32 yuv_select; //0:disable, 1:Y Enable, 2:U Enable 3:V Enable, 6:UV enable, 7:YUV Enable
	DispBoundingBoxAttr bb_attr[MAX_BOUNDING_BOX];
} DispBoundingBoxConfig;

typedef struct disp_osd_attr {
	u32 enable;
	u32 start_x;
	u32 start_y;
	u32 end_x;
	u32 end_y;
} DispOsdAttr;

typedef struct disp_osd_config {
	u32 osd_num;
	u32 osd_mode;
	u32 osd_yuv_sel;
	DispOsdAttr osd_attr[MAX_OSD_REGION];
} DispOsdConfig;

typedef struct disp_buffer_setting {
	u32 dispr_phy_addr;
	u32 dispr_size;
	u32 osdr0_phy_addr;
	u32 osdr0_size;
	u32 osdr1_phy_addr;
	u32 osdr1_size;
} DispBufferSetting;

typedef struct disp_dsi_cmd {
	u32 mode;
	u32 len; //byte
	u32 packet[MAX_DSI_COMMAND];
} DispDsiCmd;

typedef struct disp_mipi_timing {
	u32 time_lpx;
	u32 time_hs_prepare;
	u32 time_hs_zero;
	u32 time_hs_trail;
	u32 time_hs_exit;
	u32 time_clk_prepare;
	u32 time_clk_zero;
	u32 time_clk_trail;
	u32 time_clk_exit;
	u32 time_clk_pre;
	u32 time_clk_post;
	u32 time_lp_hsa;
	u32 time_lp_hbp;
	u32 time_lp_hfp;
	u32 time_frame_blanking;
	u32 time_lp_bllp_trail;
	u32 time_frame_blanking_max;
	u32 time_bta_sure;
	u32 time_bta_go;
	u32 time_bta_go_lprx_en;
	u32 time_bta_get;
	u32 time_wakeup;
	u32 time_oe;
	u32 time_ta_to;
	u32 lp_hsa;
	u32 lp_hfp;
	u32 lp_hbp;
	u32 perd_hsa;
	u32 perd_hfp;
	u32 perd_hbp;
	u32 line_vsa;
	u32 line_vbp;
	u32 line_vfp;
} DispMipiTiming;

typedef struct disp_mipi_config {
	u32 width;
	u32 height;
	u32 lane_num;
	u32 mipi_spec;
	u32 mipi_video_format;
	DispMipiTiming timing;
} DispMipiCfg;

typedef struct disp_btxxx_config {
	u32 eav;
	u32 sav;
	u32 ln;
	u32 crc;
	u32 eav_user_mode;
	u32 eav_user_setting[8];
	u32 sav_user_mode;
	u32 sav_user_setting[8];
	u32 ln_height;
	u32 crc_empty_mode;
} DispBtxxxConfig;

typedef struct disp_pdi_config {
	u32 width;
	u32 height;
	u32 data_format;
	u32 rgb_bitwidth_sel;
	u32 mcu_format_en;
	u32 mapping_sel;
	u32 up_sampling;
	u32 double_sampling;
	u32 blank_data; //pluse width
	u32 h_back_porch; //back_porch
	u32 active_data; //display period
	u32 h_front_porch; //front_porch
	u32 field0_vsync; //
	u32 field0_blanking_top;
	u32 field0_active_data;
	u32 field0_blanking_bottom;
	u32 field1_vsync;
	u32 field1_blanking_top;
	u32 field1_active_data;
	u32 field1_blanking_bottom;
	DispBtxxxConfig btxxx_config;
} DispPdiConfig;

typedef struct disp_top_config {
	u32 width;
	u32 height;
	u32 en_pg; //enable pattern gen
	u32 osd0_enable; //enable osd0
	u32 osd1_enable; //enable osd1
	u32 intput_foramt;
	u32 output_foramt;
	u32 flip;
	u32 mirror;
} DispTopCfg;

void disp_delay(u32 n);
void hw_dsip_dump_reg(DispDumpReg bank);
u32 hw_disp_check_cfa_irq(void);
u32 hw_disp_check_420_to_422_irq(void);
u32 hw_disp_check_422_to_444_irq(void);
u32 hw_disp_check_frame_time_gen_irq(void);
u32 hw_disp_check_dispr_irq(void);
void hw_disp_pdi_irq(void);
void hw_disp_set_irq_mask(void);
void hw_disp_pdi_setting(DispPdiConfig *pdi_cfg);
void hw_disp_pdi_start_video(void);
void hw_disp_pxr_setting(DispTopCfg *cfg);
void hw_disp_gamma_setting(u32 enable, u32 G, u32 B, u32 R);
void hw_disp_disp_path(DispTopCfg *disp_cfg);
void hw_disp_mipi_lane_setting(void);
void hw_disp_mipi_timing_setting(DispMipiTiming *timing);
void hw_disp_mipi_tx_enc_setting(void);
void hw_disp_mipi_dsi_stop_video(void);
void hw_disp_mipi_dsi_start_video(DispMipiCfg *mipi_cfg);
void hw_disp_mipi_init(DispMipiCfg *mipi_cfg);
void hw_disp_select_output_type(int type);
void hw_disp_mipi_dsi_mode(u32 mode);
u16 hw_disp_calculate_crc16(u32 *data_buf, u16 data_len);
u8 hw_disp_add_software_ecc(unsigned long cmd);
void hw_disp_mipi_dsi_receive_responds(void);
void hw_disp_mipi_dsi_send_command(DispDsiCmd *cmd);
void hw_disp_enable_pattern_gen(u32 enable);
void hw_disp_cst_setting(void);
void hw_disp_bounding_box(DispBoundingBoxConfig *bb_cfg);
void hw_disp_osd_setting(DispTopCfg *disp_cfg, DispOsdConfig *osd0_cfg, DispOsdConfig *osd1_cfg);
void hw_disp_pg_setting(u32 enable, DispPatternGenMode mode, u32 width, u32 height);
void hw_disp_buffer_setting(DispTopCfg *disp_cfg, DispBufferSetting *buffer_setting);
void hw_disp_frame_start(DispTopCfg *disp_cfg);
void disp_reset(void);
u32 hw_disp_get_buffer_size(DispTopCfg *disp_cfg, u32 buffer_type);
void hw_disp_dram_config(void);
void hw_disp_init(void);

void disp_set_drvstr(void);
void disp_set_iomux(void);
#endif
