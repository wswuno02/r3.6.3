#ifndef CSR_TABLE_ADODEC_H_
#define CSR_TABLE_ADODEC_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_adodec[] =
{
  // WORD adodec00
  { "clear", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "ADODEC00", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD adodec04
  { "decoder_ch_mode", 0x00000010,  2,  0,   CSR_RW, 0x00000000 },
  { "compress_mode", 0x00000010,  9,  8,   CSR_RW, 0x00000000 },
  { "g711_mode", 0x00000010, 16, 16,   CSR_RW, 0x00000000 },
  { "g726_mode", 0x00000010, 24, 24,   CSR_RW, 0x00000001 },
  { "ADODEC04", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adodec05
  { "pack_mode", 0x00000014,  2,  0,   CSR_RW, 0x00000000 },
  { "little_endian", 0x00000014,  8,  8,   CSR_RW, 0x00000000 },
  { "debug_mon_sel", 0x00000014, 26, 24,   CSR_RW, 0x00000000 },
  { "ADODEC05", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adodec06
  { "pre_shift_2s", 0x00000018, 31,  0,   CSR_RW, 0x00000000 },
  { "ADODEC06", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adodec07
  { "post_shift_2s", 0x0000001C, 31,  0,   CSR_RW, 0x00000000 },
  { "ADODEC07", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adodec08
  { "gain", 0x00000020, 15,  0,   CSR_RW, 0x00000100 },
  { "ADODEC08", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adodec09
  { "reserved", 0x00000024, 31,  0,   CSR_RW, 0x00000000 },
  { "ADODEC09", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_ADODEC_H_
