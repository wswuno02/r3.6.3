#ifndef CSR_TABLE_VP_H_
#define CSR_TABLE_VP_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_vp[] =
{
  // WORD word_debug_mon_sel
  { "debug_mon_sel", 0x00000004,  4,  0,   CSR_RW, 0x00000000 },
  { "WORD_DEBUG_MON_SEL", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD buf_update
  { "double_buf_update", 0x00000008,  0,  0,  CSR_W1P, 0x00000000 },
  { "BUF_UPDATE", 0x00000008, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD isp1_broadcast
  { "isp1_broadcast_yuv420to444", 0x00000018,  0,  0,   CSR_RW, 0x00000000 },
  { "isp1_broadcast_vpwrite0", 0x00000018,  8,  8,   CSR_RW, 0x00000000 },
  { "isp1_broadcast_vpwrite1", 0x00000018, 16, 16,   CSR_RW, 0x00000000 },
  { "ISP1_BROADCAST", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD isp2_broadcast
  { "isp2_broadcast_yuv420to444", 0x0000001C,  0,  0,   CSR_RW, 0x00000000 },
  { "isp2_broadcast_vpwrite0", 0x0000001C,  8,  8,   CSR_RW, 0x00000000 },
  { "isp2_broadcast_vpwrite1", 0x0000001C, 16, 16,   CSR_RW, 0x00000000 },
  { "ISP2_BROADCAST", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD b2r_broadcast
  { "b2r_broadcast_yuv420to444", 0x00000020,  0,  0,   CSR_RW, 0x00000000 },
  { "b2r_broadcast_vpwrite0", 0x00000020,  8,  8,   CSR_RW, 0x00000000 },
  { "b2r_broadcast_vpwrite1", 0x00000020, 16, 16,   CSR_RW, 0x00000000 },
  { "B2R_BROADCAST", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD yv444_broadcast
  { "yuv444_broadcast_sc", 0x00000024,  0,  0,   CSR_RW, 0x00000000 },
  { "yuv444_broadcast_vpcst", 0x00000024,  8,  8,   CSR_RW, 0x00000000 },
  { "YV444_BROADCAST", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD scaler_broadcast
  { "scaler_broadcast_vpwrite0", 0x00000028,  0,  0,   CSR_RW, 0x00000000 },
  { "scaler_broadcast_vpwrite1", 0x00000028,  8,  8,   CSR_RW, 0x00000000 },
  { "SCALER_BROADCAST", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD vpcst_broadcast
  { "vpcst_broadcast_vplp", 0x0000002C,  0,  0,   CSR_RW, 0x00000000 },
  { "vpcst_broadcast_vpcfa", 0x0000002C,  8,  8,   CSR_RW, 0x00000000 },
  { "VPCST_BROADCAST", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD vpcfa_broadcast
  { "vpcfa_broadcast_vpwrite0", 0x00000030,  0,  0,   CSR_RW, 0x00000000 },
  { "vpcfa_broadcast_vpwrite1", 0x00000030,  8,  8,   CSR_RW, 0x00000000 },
  { "VPCFA_BROADCAST", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD vplp_broadcast
  { "vplp_broadcast_vpwrite0", 0x00000034,  0,  0,   CSR_RW, 0x00000000 },
  { "vplp_broadcast_vpwrite1", 0x00000034,  8,  8,   CSR_RW, 0x00000000 },
  { "VPLP_BROADCAST", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sel_unpacker
  { "unpacker_sel", 0x00000038,  0,  0,   CSR_RW, 0x00000000 },
  { "SEL_UNPACKER", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sel_yuv420to444
  { "yuv420to444_sel", 0x0000003C,  1,  0,   CSR_RW, 0x00000000 },
  { "SEL_YUV420TO444", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sel_vpwrite0
  { "vpwrite0_sel", 0x00000040,  2,  0,   CSR_RW, 0x00000000 },
  { "SEL_VPWRITE0", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sel_vpwrite1
  { "vpwrite1_sel", 0x00000044,  2,  0,   CSR_RW, 0x00000000 },
  { "SEL_VPWRITE1", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ack_not_isp1
  { "isp1_broadcast_ack_not_sel", 0x0000004C,  0,  0,   CSR_RW, 0x00000000 },
  { "ACK_NOT_ISP1", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ack_not_isp2
  { "isp2_broadcast_ack_not_sel", 0x00000050,  0,  0,   CSR_RW, 0x00000000 },
  { "ACK_NOT_ISP2", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ack_not_b2r
  { "b2r_broadcast_ack_not_sel", 0x00000054,  0,  0,   CSR_RW, 0x00000000 },
  { "ACK_NOT_B2R", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ack_not_scaler
  { "scaler_broadcast_ack_not_sel", 0x00000058,  0,  0,   CSR_RW, 0x00000000 },
  { "ACK_NOT_SCALER", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ack_not_unpacker
  { "unpacker_mux_ack_not_sel", 0x0000005C,  0,  0,   CSR_RW, 0x00000000 },
  { "ACK_NOT_UNPACKER", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ack_not_yuv420to444
  { "yuv420to444_mux_ack_not_sel", 0x00000060,  0,  0,   CSR_RW, 0x00000000 },
  { "ACK_NOT_YUV420TO444", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ack_not_vpwrite0
  { "vpwrite0_mux_ack_not_sel", 0x00000064,  0,  0,   CSR_RW, 0x00000000 },
  { "ACK_NOT_VPWRITE0", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ack_not_vpwrite1
  { "vpwrite1_mux_ack_not_sel", 0x00000068,  0,  0,   CSR_RW, 0x00000000 },
  { "ACK_NOT_VPWRITE1", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ack_not_yuv444
  { "yuv444_broadcast_ack_not_sel", 0x0000006C,  0,  0,   CSR_RW, 0x00000000 },
  { "ACK_NOT_YUV444", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ack_not_vpcst
  { "vpcst_broadcast_ack_not_sel", 0x00000070,  0,  0,   CSR_RW, 0x00000000 },
  { "ACK_NOT_VPCST", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ack_not_vpcfa
  { "vpcfa_broadcast_ack_not_sel", 0x00000074,  0,  0,   CSR_RW, 0x00000000 },
  { "ACK_NOT_VPCFA", 0x00000074, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ack_not_vplp
  { "vplp_broadcast_ack_not_sel", 0x00000078,  0,  0,   CSR_RW, 0x00000000 },
  { "ACK_NOT_VPLP", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mem_lp_ctrl
  { "sd", 0x00000080,  0,  0,   CSR_RW, 0x00000000 },
  { "slp", 0x00000080,  8,  8,   CSR_RW, 0x00000000 },
  { "MEM_LP_CTRL", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_VP_H_
