#ifndef CSR_TABLE_AMC_H_
#define CSR_TABLE_AMC_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_amc[] =
{
  // WORD irq_clear
  { "irq_clear_psd_sync_code_error_event", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "irq_clear_psd_out_overrun_error_event", 0x00000004,  1,  1,  CSR_W1P, 0x00000000 },
  { "irq_clear_psd_fifo_over_error_event", 0x00000004,  2,  2,  CSR_W1P, 0x00000000 },
  { "irq_clear_psd_frame_size_error_event", 0x00000004,  3,  3,  CSR_W1P, 0x00000000 },
  { "irq_clear_psd_eol_error_event", 0x00000004,  4,  4,  CSR_W1P, 0x00000000 },
  { "irq_clear_psd_sol_error_event", 0x00000004,  5,  5,  CSR_W1P, 0x00000000 },
  { "irq_clear_psd_eof_error_event", 0x00000004,  6,  6,  CSR_W1P, 0x00000000 },
  { "irq_clear_psd_sof_error_event", 0x00000004,  7,  7,  CSR_W1P, 0x00000000 },
  { "irq_clear_lpmd_missing_pixel_error_event", 0x00000004,  8,  8,  CSR_W1P, 0x00000000 },
  { "irq_clear_wdt_timeout_error_event", 0x00000004,  9,  9,  CSR_W1P, 0x00000000 },
  { "irq_clear_psd_eol_alarm_event", 0x00000004, 10, 10,  CSR_W1P, 0x00000000 },
  { "irq_clear_psd_sol_alarm_event", 0x00000004, 11, 11,  CSR_W1P, 0x00000000 },
  { "irq_clear_psd_eof_alarm_event", 0x00000004, 12, 12,  CSR_W1P, 0x00000000 },
  { "irq_clear_psd_sof_alarm_event", 0x00000004, 13, 13,  CSR_W1P, 0x00000000 },
  { "irq_clear_lpmd_detect_motion_alarm_event", 0x00000004, 14, 14,  CSR_W1P, 0x00000000 },
  { "irq_clear_lpmd_frame_end_alarm_event", 0x00000004, 15, 15,  CSR_W1P, 0x00000000 },
  { "irq_clear_rtc_periodic_time_alarm_event", 0x00000004, 16, 16,  CSR_W1P, 0x00000000 },
  { "irq_clear_external_wakeup_alarm_event", 0x00000004, 17, 17,  CSR_W1P, 0x00000000 },
  { "irq_clear_external_button_alarm_event", 0x00000004, 18, 18,  CSR_W1P, 0x00000000 },
  { "IRQ_CLEAR", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD irq_mask
  { "irq_mask_psd_sync_code_error_event", 0x00000008,  0,  0,   CSR_RW, 0x00000001 },
  { "irq_mask_psd_out_overrun_error_event", 0x00000008,  1,  1,   CSR_RW, 0x00000001 },
  { "irq_mask_psd_fifo_over_error_event", 0x00000008,  2,  2,   CSR_RW, 0x00000001 },
  { "irq_mask_psd_frame_size_error_event", 0x00000008,  3,  3,   CSR_RW, 0x00000001 },
  { "irq_mask_psd_eol_error_event", 0x00000008,  4,  4,   CSR_RW, 0x00000001 },
  { "irq_mask_psd_sol_error_event", 0x00000008,  5,  5,   CSR_RW, 0x00000001 },
  { "irq_mask_psd_eof_error_event", 0x00000008,  6,  6,   CSR_RW, 0x00000001 },
  { "irq_mask_psd_sof_error_event", 0x00000008,  7,  7,   CSR_RW, 0x00000001 },
  { "irq_mask_lpmd_missing_pixel_error_event", 0x00000008,  8,  8,   CSR_RW, 0x00000001 },
  { "irq_mask_wdt_timeout_error_event", 0x00000008,  9,  9,   CSR_RW, 0x00000001 },
  { "irq_mask_psd_eol_alarm_event", 0x00000008, 10, 10,   CSR_RW, 0x00000001 },
  { "irq_mask_psd_sol_alarm_event", 0x00000008, 11, 11,   CSR_RW, 0x00000001 },
  { "irq_mask_psd_eof_alarm_event", 0x00000008, 12, 12,   CSR_RW, 0x00000001 },
  { "irq_mask_psd_sof_alarm_event", 0x00000008, 13, 13,   CSR_RW, 0x00000001 },
  { "irq_mask_lpmd_detect_motion_alarm_event", 0x00000008, 14, 14,   CSR_RW, 0x00000001 },
  { "irq_mask_lpmd_frame_end_alarm_event", 0x00000008, 15, 15,   CSR_RW, 0x00000001 },
  { "irq_mask_rtc_periodic_time_alarm_event", 0x00000008, 16, 16,   CSR_RW, 0x00000001 },
  { "irq_mask_external_wakeup_alarm_event", 0x00000008, 17, 17,   CSR_RW, 0x00000001 },
  { "irq_mask_external_button_alarm_event", 0x00000008, 18, 18,   CSR_RW, 0x00000001 },
  { "IRQ_MASK", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD irq_status
  { "status_psd_sync_code_error_event", 0x0000000C,  0,  0,   CSR_RO, 0x00000000 },
  { "status_psd_out_overrun_error_event", 0x0000000C,  1,  1,   CSR_RO, 0x00000000 },
  { "status_psd_fifo_over_error_event", 0x0000000C,  2,  2,   CSR_RO, 0x00000000 },
  { "status_psd_frame_size_error_event", 0x0000000C,  3,  3,   CSR_RO, 0x00000000 },
  { "status_psd_eol_error_event", 0x0000000C,  4,  4,   CSR_RO, 0x00000000 },
  { "status_psd_sol_error_event", 0x0000000C,  5,  5,   CSR_RO, 0x00000000 },
  { "status_psd_eof_error_event", 0x0000000C,  6,  6,   CSR_RO, 0x00000000 },
  { "status_psd_sof_error_event", 0x0000000C,  7,  7,   CSR_RO, 0x00000000 },
  { "status_lpmd_missing_pixel_error_event", 0x0000000C,  8,  8,   CSR_RO, 0x00000000 },
  { "status_wdt_timeout_error_event", 0x0000000C,  9,  9,   CSR_RO, 0x00000000 },
  { "status_psd_eol_alarm_event", 0x0000000C, 10, 10,   CSR_RO, 0x00000000 },
  { "status_psd_sol_alarm_event", 0x0000000C, 11, 11,   CSR_RO, 0x00000000 },
  { "status_psd_eof_alarm_event", 0x0000000C, 12, 12,   CSR_RO, 0x00000000 },
  { "status_psd_sof_alarm_event", 0x0000000C, 13, 13,   CSR_RO, 0x00000000 },
  { "status_lpmd_detect_motion_alarm_event", 0x0000000C, 14, 14,   CSR_RO, 0x00000000 },
  { "status_lpmd_frame_end_alarm_event", 0x0000000C, 15, 15,   CSR_RO, 0x00000000 },
  { "status_rtc_periodic_time_alarm_event", 0x0000000C, 16, 16,   CSR_RO, 0x00000000 },
  { "status_external_wakeup_alarm_event", 0x0000000C, 17, 17,   CSR_RO, 0x00000000 },
  { "status_external_button_alarm_event", 0x0000000C, 18, 18,   CSR_RO, 0x00000000 },
  { "IRQ_STATUS", 0x0000000C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD sleep_aon
  { "sleep_aon_pluse", 0x00000018,  0,  0,  CSR_W1P, 0x00000000 },
  { "SLEEP_AON", 0x00000018, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD wdt_en
  { "wdt_en_src", 0x00000020,  0,  0,   CSR_RW, 0x00000000 },
  { "wdt_en_csr", 0x00000020,  8,  8,   CSR_RW, 0x00000000 },
  { "WDT_EN", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_pwr1p8_charging_period
  { "pwr1p8_charging_period", 0x00000028, 15,  0,   CSR_RW, 0x00005DC0 },
  { "WORD_PWR1P8_CHARGING_PERIOD", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_pwr3p3_charging_period
  { "pwr3p3_charging_period", 0x0000002C, 15,  0,   CSR_RW, 0x0000BB80 },
  { "WORD_PWR3P3_CHARGING_PERIOD", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_pwr1p8_charging_timing_scaler
  { "pwr1p8_charging_timing_scaler", 0x00000030,  7,  0,   CSR_RW, 0x00000000 },
  { "WORD_PWR1P8_CHARGING_TIMING_SCALER", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_pwr3p3_charging_timing_scaler
  { "pwr3p3_charging_timing_scaler", 0x00000034,  7,  0,   CSR_RW, 0x00000000 },
  { "WORD_PWR3P3_CHARGING_TIMING_SCALER", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_pwr_switching_blanking_period
  { "pwr_switching_blanking_period", 0x00000038, 15,  0,   CSR_RW, 0x00000008 },
  { "WORD_PWR_SWITCHING_BLANKING_PERIOD", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_pwr_switching_blanking_timing_scaler
  { "pwr_switching_blanking_timing_scaler", 0x0000003C,  7,  0,   CSR_RW, 0x00000000 },
  { "WORD_PWR_SWITCHING_BLANKING_TIMING_SCALER", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_pwron_wakeup_period
  { "pwron_wakeup_period", 0x00000040, 15,  0,   CSR_RW, 0x00000200 },
  { "WORD_PWRON_WAKEUP_PERIOD", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_pwroff_button_period
  { "pwroff_button_period", 0x00000044, 15,  0,   CSR_RW, 0x0000249F },
  { "WORD_PWROFF_BUTTON_PERIOD", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_pwron_button_period
  { "pwron_button_period", 0x00000048, 15,  0,   CSR_RW, 0x0000FFFF },
  { "WORD_PWRON_BUTTON_PERIOD", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_pwroff_button_timing_scaler
  { "pwroff_button_timing_scaler", 0x0000004C,  7,  0,   CSR_RW, 0x000000FF },
  { "WORD_PWROFF_BUTTON_TIMING_SCALER", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_pwron_button_timing_scaler
  { "pwron_button_timing_scaler", 0x00000050,  7,  0,   CSR_RW, 0x000000FF },
  { "WORD_PWRON_BUTTON_TIMING_SCALER", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_pwroff_button_extra_period
  { "pwroff_button_extra_period", 0x00000054,  7,  0,   CSR_RW, 0x00000000 },
  { "WORD_PWROFF_BUTTON_EXTRA_PERIOD", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_pwron_button_extra_period
  { "pwron_button_extra_period", 0x00000058,  7,  0,   CSR_RW, 0x0000000E },
  { "WORD_PWRON_BUTTON_EXTRA_PERIOD", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwr_mode
  { "pwr_wakeup_mode", 0x00000068,  1,  0,   CSR_RW, 0x00000001 },
  { "pwr_button_mode", 0x00000068,  8,  8,   CSR_RW, 0x00000000 },
  { "pwd_wakeup_en", 0x00000068, 16, 16,   CSR_RW, 0x00000000 },
  { "PWR_MODE", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD event_error_mask
  { "psd_sync_code_error_mask", 0x00000078,  0,  0,   CSR_RW, 0x00000000 },
  { "psd_out_overrun_error_mask", 0x00000078,  1,  1,   CSR_RW, 0x00000000 },
  { "psd_fifo_over_error_mask", 0x00000078,  2,  2,   CSR_RW, 0x00000000 },
  { "psd_frame_size_error_mask", 0x00000078,  3,  3,   CSR_RW, 0x00000000 },
  { "psd_eol_error_mask", 0x00000078,  4,  4,   CSR_RW, 0x00000000 },
  { "psd_sol_error_mask", 0x00000078,  5,  5,   CSR_RW, 0x00000000 },
  { "psd_eof_error_mask", 0x00000078,  6,  6,   CSR_RW, 0x00000000 },
  { "psd_sof_error_mask", 0x00000078,  7,  7,   CSR_RW, 0x00000000 },
  { "lpmd_missing_pixel_error_mask", 0x00000078,  8,  8,   CSR_RW, 0x00000000 },
  { "wdt_timeout_error_mask", 0x00000078,  9,  9,   CSR_RW, 0x00000000 },
  { "EVENT_ERROR_MASK", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD event_alarm_mask
  { "psd_eol_alarm_mask", 0x0000007C,  0,  0,   CSR_RW, 0x00000000 },
  { "psd_sol_alarm_mask", 0x0000007C,  1,  1,   CSR_RW, 0x00000000 },
  { "psd_eof_alarm_mask", 0x0000007C,  2,  2,   CSR_RW, 0x00000000 },
  { "psd_sof_alarm_mask", 0x0000007C,  3,  3,   CSR_RW, 0x00000000 },
  { "lpmd_detect_motion_alarm_mask", 0x0000007C,  4,  4,   CSR_RW, 0x00000000 },
  { "lpmd_frame_end_alarm_mask", 0x0000007C,  5,  5,   CSR_RW, 0x00000000 },
  { "rtc_periodic_time_alarm_mask", 0x0000007C,  6,  6,   CSR_RW, 0x00000000 },
  { "pwr_wakeup_alarm_mask", 0x0000007C,  7,  7,   CSR_RW, 0x00000000 },
  { "pwr_button_alarm_mask", 0x0000007C,  8,  8,   CSR_RW, 0x00000000 },
  { "EVENT_ALARM_MASK", 0x0000007C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_event_clear
  { "event_clear", 0x00000080,  0,  0,  CSR_W1P, 0x00000000 },
  { "WORD_EVENT_CLEAR", 0x00000080, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD debug_sel
  { "debug_mon_sel", 0x00000084,  1,  0,   CSR_RW, 0x00000000 },
  { "DEBUG_SEL", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD debug_data
  { "debug_mon", 0x00000088, 31,  0,   CSR_RO, 0x00000000 },
  { "DEBUG_DATA", 0x00000088, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_reserved_0
  { "reserved_0", 0x00000098, 15,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED_0", 0x00000098, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_reserved_1
  { "reserved_1", 0x0000009C, 15,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED_1", 0x0000009C, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_AMC_H_
