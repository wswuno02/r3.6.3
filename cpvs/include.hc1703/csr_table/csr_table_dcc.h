#ifndef CSR_TABLE_DCC_H_
#define CSR_TABLE_DCC_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_dcc[] =
{
  // WORD wore_frame_start
  { "frame_start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "WORE_FRAME_START", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD irq_clear
  { "irq_clear_frame_end", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "IRQ_CLEAR", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD status
  { "status_frame_end", 0x00000008,  0,  0,   CSR_RO, 0x00000000 },
  { "STATUS", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // WORD irq_mask
  { "irq_mask_frame_end", 0x0000000C,  0,  0,   CSR_RW, 0x00000001 },
  { "IRQ_MASK", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cfa_format
  { "cfa_mode", 0x00000010,  1,  0,   CSR_RW, 0x00000000 },
  { "bayer_ini_phase", 0x00000010,  9,  8,   CSR_RW, 0x00000000 },
  { "CFA_FORMAT", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wore_cfa_phase_0
  { "cfa_phase_0", 0x00000014,  2,  0,   CSR_RW, 0x00000000 },
  { "cfa_phase_1", 0x00000014, 10,  8,   CSR_RW, 0x00000000 },
  { "cfa_phase_2", 0x00000014, 18, 16,   CSR_RW, 0x00000000 },
  { "cfa_phase_3", 0x00000014, 26, 24,   CSR_RW, 0x00000000 },
  { "WORE_CFA_PHASE_0", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_cfa_phase_1
  { "cfa_phase_4", 0x00000018,  2,  0,   CSR_RW, 0x00000000 },
  { "cfa_phase_5", 0x00000018, 10,  8,   CSR_RW, 0x00000000 },
  { "cfa_phase_6", 0x00000018, 18, 16,   CSR_RW, 0x00000000 },
  { "cfa_phase_7", 0x00000018, 26, 24,   CSR_RW, 0x00000000 },
  { "CFA_PHASE_1", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_cfa_phase_2
  { "cfa_phase_8", 0x0000001C,  2,  0,   CSR_RW, 0x00000000 },
  { "cfa_phase_9", 0x0000001C, 10,  8,   CSR_RW, 0x00000000 },
  { "cfa_phase_10", 0x0000001C, 18, 16,   CSR_RW, 0x00000000 },
  { "cfa_phase_11", 0x0000001C, 26, 24,   CSR_RW, 0x00000000 },
  { "CFA_PHASE_2", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_cfa_phase_3
  { "cfa_phase_12", 0x00000020,  2,  0,   CSR_RW, 0x00000000 },
  { "cfa_phase_13", 0x00000020, 10,  8,   CSR_RW, 0x00000000 },
  { "cfa_phase_14", 0x00000020, 18, 16,   CSR_RW, 0x00000000 },
  { "cfa_phase_15", 0x00000020, 26, 24,   CSR_RW, 0x00000000 },
  { "CFA_PHASE_3", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD resolution
  { "width", 0x00000024, 15,  0,   CSR_RW, 0x00000780 },
  { "height", 0x00000024, 31, 16,   CSR_RW, 0x00000438 },
  { "RESOLUTION", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wore_mode
  { "mode", 0x00000028,  0,  0,   CSR_RW, 0x00000000 },
  { "WORE_MODE", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_0
  { "gain_g0", 0x0000002C, 18,  0,   CSR_RW, 0x00000400 },
  { "GAIN_0", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_1
  { "gain_r", 0x00000030, 18,  0,   CSR_RW, 0x00000400 },
  { "GAIN_1", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_2
  { "gain_b", 0x00000034, 18,  0,   CSR_RW, 0x00000400 },
  { "GAIN_2", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_3
  { "gain_g1", 0x00000038, 18,  0,   CSR_RW, 0x00000400 },
  { "GAIN_3", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_4
  { "gain_s", 0x0000003C, 18,  0,   CSR_RW, 0x00000400 },
  { "GAIN_4", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD offset_0
  { "offset_g0_2s", 0x00000040, 16,  0,   CSR_RW, 0x00000000 },
  { "OFFSET_0", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD offset_1
  { "offset_r_2s", 0x00000044, 16,  0,   CSR_RW, 0x00000000 },
  { "OFFSET_1", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD offset_2
  { "offset_b_2s", 0x00000048, 16,  0,   CSR_RW, 0x00000000 },
  { "OFFSET_2", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD offset_3
  { "offset_g1_2s", 0x0000004C, 16,  0,   CSR_RW, 0x00000000 },
  { "OFFSET_3", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD offset_4
  { "offset_s_2s", 0x00000050, 16,  0,   CSR_RW, 0x00000000 },
  { "OFFSET_4", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_y_ini_0
  { "mapping_curve_g0_y_ini", 0x00000054, 15,  0,   CSR_RW, 0x00000000 },
  { "mapping_curve_r_y_ini", 0x00000054, 31, 16,   CSR_RW, 0x00000000 },
  { "CURVE_Y_INI_0", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_y_ini_1
  { "mapping_curve_b_y_ini", 0x00000058, 15,  0,   CSR_RW, 0x00000000 },
  { "mapping_curve_g1_y_ini", 0x00000058, 31, 16,   CSR_RW, 0x00000000 },
  { "CURVE_Y_INI_1", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_y_ini_2
  { "mapping_curve_s_y_ini", 0x0000005C, 15,  0,   CSR_RW, 0x00000000 },
  { "CURVE_Y_INI_2", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_g0_y_0
  { "mapping_curve_g0_y_0", 0x00000060, 15,  0,   CSR_RW, 0x000001FF },
  { "mapping_curve_g0_y_1", 0x00000060, 31, 16,   CSR_RW, 0x000003FF },
  { "CURVE_G0_Y_0", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_g0_y_1
  { "mapping_curve_g0_y_2", 0x00000064, 15,  0,   CSR_RW, 0x000007FF },
  { "mapping_curve_g0_y_3", 0x00000064, 31, 16,   CSR_RW, 0x00000FFF },
  { "CURVE_G0_Y_1", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_g0_y_2
  { "mapping_curve_g0_y_4", 0x00000068, 15,  0,   CSR_RW, 0x00001FFF },
  { "mapping_curve_g0_y_5", 0x00000068, 31, 16,   CSR_RW, 0x00003FFF },
  { "CURVE_G0_Y_2", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_g0_y_3
  { "mapping_curve_g0_y_6", 0x0000006C, 15,  0,   CSR_RW, 0x00007FFF },
  { "mapping_curve_g0_y_7", 0x0000006C, 31, 16,   CSR_RW, 0x0000FFFF },
  { "CURVE_G0_Y_3", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_g0_x_0
  { "mapping_curve_g0_x_0", 0x00000070, 15,  0,   CSR_RW, 0x000001FF },
  { "mapping_curve_g0_x_1", 0x00000070, 31, 16,   CSR_RW, 0x000003FF },
  { "CURVE_G0_X_0", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_g0_x_1
  { "mapping_curve_g0_x_2", 0x00000074, 15,  0,   CSR_RW, 0x000007FF },
  { "mapping_curve_g0_x_3", 0x00000074, 31, 16,   CSR_RW, 0x00000FFF },
  { "CURVE_G0_X_1", 0x00000074, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_g0_x_2
  { "mapping_curve_g0_x_4", 0x00000078, 15,  0,   CSR_RW, 0x00001FFF },
  { "mapping_curve_g0_x_5", 0x00000078, 31, 16,   CSR_RW, 0x00003FFF },
  { "CURVE_G0_X_2", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_g0_x_3
  { "mapping_curve_g0_x_6", 0x0000007C, 15,  0,   CSR_RW, 0x00007FFF },
  { "mapping_curve_g0_x_7", 0x0000007C, 31, 16,   CSR_RW, 0x0000FFFF },
  { "CURVE_G0_X_3", 0x0000007C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_g0_m_0
  { "mapping_curve_g0_m_0", 0x00000080,  3,  0,   CSR_RW, 0x00000008 },
  { "mapping_curve_g0_m_1", 0x00000080, 11,  8,   CSR_RW, 0x00000008 },
  { "mapping_curve_g0_m_2", 0x00000080, 19, 16,   CSR_RW, 0x00000008 },
  { "mapping_curve_g0_m_3", 0x00000080, 27, 24,   CSR_RW, 0x00000008 },
  { "CURVE_G0_M_0", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_g0_m_1
  { "mapping_curve_g0_m_4", 0x00000084,  3,  0,   CSR_RW, 0x00000008 },
  { "mapping_curve_g0_m_5", 0x00000084, 11,  8,   CSR_RW, 0x00000008 },
  { "mapping_curve_g0_m_6", 0x00000084, 19, 16,   CSR_RW, 0x00000008 },
  { "mapping_curve_g0_m_7", 0x00000084, 27, 24,   CSR_RW, 0x00000008 },
  { "CURVE_G0_M_1", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_r_y_0
  { "mapping_curve_r_y_0", 0x00000088, 15,  0,   CSR_RW, 0x000001FF },
  { "mapping_curve_r_y_1", 0x00000088, 31, 16,   CSR_RW, 0x000003FF },
  { "CURVE_R_Y_0", 0x00000088, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_r_y_1
  { "mapping_curve_r_y_2", 0x0000008C, 15,  0,   CSR_RW, 0x000007FF },
  { "mapping_curve_r_y_3", 0x0000008C, 31, 16,   CSR_RW, 0x00000FFF },
  { "CURVE_R_Y_1", 0x0000008C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_r_y_2
  { "mapping_curve_r_y_4", 0x00000090, 15,  0,   CSR_RW, 0x00001FFF },
  { "mapping_curve_r_y_5", 0x00000090, 31, 16,   CSR_RW, 0x00003FFF },
  { "CURVE_R_Y_2", 0x00000090, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_r_y_3
  { "mapping_curve_r_y_6", 0x00000094, 15,  0,   CSR_RW, 0x00007FFF },
  { "mapping_curve_r_y_7", 0x00000094, 31, 16,   CSR_RW, 0x0000FFFF },
  { "CURVE_R_Y_3", 0x00000094, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_r_x_0
  { "mapping_curve_r_x_0", 0x00000098, 15,  0,   CSR_RW, 0x000001FF },
  { "mapping_curve_r_x_1", 0x00000098, 31, 16,   CSR_RW, 0x000003FF },
  { "CURVE_R_X_0", 0x00000098, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_r_x_1
  { "mapping_curve_r_x_2", 0x0000009C, 15,  0,   CSR_RW, 0x000007FF },
  { "mapping_curve_r_x_3", 0x0000009C, 31, 16,   CSR_RW, 0x00000FFF },
  { "CURVE_R_X_1", 0x0000009C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_r_x_2
  { "mapping_curve_r_x_4", 0x000000A0, 15,  0,   CSR_RW, 0x00001FFF },
  { "mapping_curve_r_x_5", 0x000000A0, 31, 16,   CSR_RW, 0x00003FFF },
  { "CURVE_R_X_2", 0x000000A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_r_x_3
  { "mapping_curve_r_x_6", 0x000000A4, 15,  0,   CSR_RW, 0x00007FFF },
  { "mapping_curve_r_x_7", 0x000000A4, 31, 16,   CSR_RW, 0x0000FFFF },
  { "CURVE_R_X_3", 0x000000A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_r_m_0
  { "mapping_curve_r_m_0", 0x000000A8,  3,  0,   CSR_RW, 0x00000008 },
  { "mapping_curve_r_m_1", 0x000000A8, 11,  8,   CSR_RW, 0x00000008 },
  { "mapping_curve_r_m_2", 0x000000A8, 19, 16,   CSR_RW, 0x00000008 },
  { "mapping_curve_r_m_3", 0x000000A8, 27, 24,   CSR_RW, 0x00000008 },
  { "CURVE_R_M_0", 0x000000A8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_r_m_1
  { "mapping_curve_r_m_4", 0x000000AC,  3,  0,   CSR_RW, 0x00000008 },
  { "mapping_curve_r_m_5", 0x000000AC, 11,  8,   CSR_RW, 0x00000008 },
  { "mapping_curve_r_m_6", 0x000000AC, 19, 16,   CSR_RW, 0x00000008 },
  { "mapping_curve_r_m_7", 0x000000AC, 27, 24,   CSR_RW, 0x00000008 },
  { "CURVE_R_M_1", 0x000000AC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_b_y_0
  { "mapping_curve_b_y_0", 0x000000B0, 15,  0,   CSR_RW, 0x000001FF },
  { "mapping_curve_b_y_1", 0x000000B0, 31, 16,   CSR_RW, 0x000003FF },
  { "CURVE_B_Y_0", 0x000000B0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_b_y_1
  { "mapping_curve_b_y_2", 0x000000B4, 15,  0,   CSR_RW, 0x000007FF },
  { "mapping_curve_b_y_3", 0x000000B4, 31, 16,   CSR_RW, 0x00000FFF },
  { "CURVE_B_Y_1", 0x000000B4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_b_y_2
  { "mapping_curve_b_y_4", 0x000000B8, 15,  0,   CSR_RW, 0x00001FFF },
  { "mapping_curve_b_y_5", 0x000000B8, 31, 16,   CSR_RW, 0x00003FFF },
  { "CURVE_B_Y_2", 0x000000B8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_b_y_3
  { "mapping_curve_b_y_6", 0x000000BC, 15,  0,   CSR_RW, 0x00007FFF },
  { "mapping_curve_b_y_7", 0x000000BC, 31, 16,   CSR_RW, 0x0000FFFF },
  { "CURVE_B_Y_3", 0x000000BC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_b_x_0
  { "mapping_curve_b_x_0", 0x000000C0, 15,  0,   CSR_RW, 0x000001FF },
  { "mapping_curve_b_x_1", 0x000000C0, 31, 16,   CSR_RW, 0x000003FF },
  { "CURVE_B_X_0", 0x000000C0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_b_x_1
  { "mapping_curve_b_x_2", 0x000000C4, 15,  0,   CSR_RW, 0x000007FF },
  { "mapping_curve_b_x_3", 0x000000C4, 31, 16,   CSR_RW, 0x00000FFF },
  { "CURVE_B_X_1", 0x000000C4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_b_x_2
  { "mapping_curve_b_x_4", 0x000000C8, 15,  0,   CSR_RW, 0x00001FFF },
  { "mapping_curve_b_x_5", 0x000000C8, 31, 16,   CSR_RW, 0x00003FFF },
  { "CURVE_B_X_2", 0x000000C8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_b_x_3
  { "mapping_curve_b_x_6", 0x000000CC, 15,  0,   CSR_RW, 0x00007FFF },
  { "mapping_curve_b_x_7", 0x000000CC, 31, 16,   CSR_RW, 0x0000FFFF },
  { "CURVE_B_X_3", 0x000000CC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_b_m_0
  { "mapping_curve_b_m_0", 0x000000D0,  3,  0,   CSR_RW, 0x00000008 },
  { "mapping_curve_b_m_1", 0x000000D0, 11,  8,   CSR_RW, 0x00000008 },
  { "mapping_curve_b_m_2", 0x000000D0, 19, 16,   CSR_RW, 0x00000008 },
  { "mapping_curve_b_m_3", 0x000000D0, 27, 24,   CSR_RW, 0x00000008 },
  { "CURVE_B_M_0", 0x000000D0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_b_m_1
  { "mapping_curve_b_m_4", 0x000000D4,  3,  0,   CSR_RW, 0x00000008 },
  { "mapping_curve_b_m_5", 0x000000D4, 11,  8,   CSR_RW, 0x00000008 },
  { "mapping_curve_b_m_6", 0x000000D4, 19, 16,   CSR_RW, 0x00000008 },
  { "mapping_curve_b_m_7", 0x000000D4, 27, 24,   CSR_RW, 0x00000008 },
  { "CURVE_B_M_1", 0x000000D4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_g1_y_0
  { "mapping_curve_g1_y_0", 0x000000D8, 15,  0,   CSR_RW, 0x000001FF },
  { "mapping_curve_g1_y_1", 0x000000D8, 31, 16,   CSR_RW, 0x000003FF },
  { "CURVE_G1_Y_0", 0x000000D8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_g1_y_1
  { "mapping_curve_g1_y_2", 0x000000DC, 15,  0,   CSR_RW, 0x000007FF },
  { "mapping_curve_g1_y_3", 0x000000DC, 31, 16,   CSR_RW, 0x00000FFF },
  { "CURVE_G1_Y_1", 0x000000DC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_g1_y_2
  { "mapping_curve_g1_y_4", 0x000000E0, 15,  0,   CSR_RW, 0x00001FFF },
  { "mapping_curve_g1_y_5", 0x000000E0, 31, 16,   CSR_RW, 0x00003FFF },
  { "CURVE_G1_Y_2", 0x000000E0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_g1_y_3
  { "mapping_curve_g1_y_6", 0x000000E4, 15,  0,   CSR_RW, 0x00007FFF },
  { "mapping_curve_g1_y_7", 0x000000E4, 31, 16,   CSR_RW, 0x0000FFFF },
  { "CURVE_G1_Y_3", 0x000000E4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_g1_x_0
  { "mapping_curve_g1_x_0", 0x000000E8, 15,  0,   CSR_RW, 0x000001FF },
  { "mapping_curve_g1_x_1", 0x000000E8, 31, 16,   CSR_RW, 0x000003FF },
  { "CURVE_G1_X_0", 0x000000E8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_g1_x_1
  { "mapping_curve_g1_x_2", 0x000000EC, 15,  0,   CSR_RW, 0x000007FF },
  { "mapping_curve_g1_x_3", 0x000000EC, 31, 16,   CSR_RW, 0x00000FFF },
  { "CURVE_G1_X_1", 0x000000EC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_g1_x_2
  { "mapping_curve_g1_x_4", 0x000000F0, 15,  0,   CSR_RW, 0x00001FFF },
  { "mapping_curve_g1_x_5", 0x000000F0, 31, 16,   CSR_RW, 0x00003FFF },
  { "CURVE_G1_X_2", 0x000000F0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_g1_x_3
  { "mapping_curve_g1_x_6", 0x000000F4, 15,  0,   CSR_RW, 0x00007FFF },
  { "mapping_curve_g1_x_7", 0x000000F4, 31, 16,   CSR_RW, 0x0000FFFF },
  { "CURVE_G1_X_3", 0x000000F4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_g1_m_0
  { "mapping_curve_g1_m_0", 0x000000F8,  3,  0,   CSR_RW, 0x00000008 },
  { "mapping_curve_g1_m_1", 0x000000F8, 11,  8,   CSR_RW, 0x00000008 },
  { "mapping_curve_g1_m_2", 0x000000F8, 19, 16,   CSR_RW, 0x00000008 },
  { "mapping_curve_g1_m_3", 0x000000F8, 27, 24,   CSR_RW, 0x00000008 },
  { "CURVE_G1_M_0", 0x000000F8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_g1_m_1
  { "mapping_curve_g1_m_4", 0x000000FC,  3,  0,   CSR_RW, 0x00000008 },
  { "mapping_curve_g1_m_5", 0x000000FC, 11,  8,   CSR_RW, 0x00000008 },
  { "mapping_curve_g1_m_6", 0x000000FC, 19, 16,   CSR_RW, 0x00000008 },
  { "mapping_curve_g1_m_7", 0x000000FC, 27, 24,   CSR_RW, 0x00000008 },
  { "CURVE_G1_M_1", 0x000000FC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_s_y_0
  { "mapping_curve_s_y_0", 0x00000100, 15,  0,   CSR_RW, 0x000001FF },
  { "mapping_curve_s_y_1", 0x00000100, 31, 16,   CSR_RW, 0x000003FF },
  { "CURVE_S_Y_0", 0x00000100, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_s_y_1
  { "mapping_curve_s_y_2", 0x00000104, 15,  0,   CSR_RW, 0x000007FF },
  { "mapping_curve_s_y_3", 0x00000104, 31, 16,   CSR_RW, 0x00000FFF },
  { "CURVE_S_Y_1", 0x00000104, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_s_y_2
  { "mapping_curve_s_y_4", 0x00000108, 15,  0,   CSR_RW, 0x00001FFF },
  { "mapping_curve_s_y_5", 0x00000108, 31, 16,   CSR_RW, 0x00003FFF },
  { "CURVE_S_Y_2", 0x00000108, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_s_y_3
  { "mapping_curve_s_y_6", 0x0000010C, 15,  0,   CSR_RW, 0x00007FFF },
  { "mapping_curve_s_y_7", 0x0000010C, 31, 16,   CSR_RW, 0x0000FFFF },
  { "CURVE_S_Y_3", 0x0000010C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_s_x_0
  { "mapping_curve_s_x_0", 0x00000110, 15,  0,   CSR_RW, 0x000001FF },
  { "mapping_curve_s_x_1", 0x00000110, 31, 16,   CSR_RW, 0x000003FF },
  { "CURVE_S_X_0", 0x00000110, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_s_x_1
  { "mapping_curve_s_x_2", 0x00000114, 15,  0,   CSR_RW, 0x000007FF },
  { "mapping_curve_s_x_3", 0x00000114, 31, 16,   CSR_RW, 0x00000FFF },
  { "CURVE_S_X_1", 0x00000114, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_s_x_2
  { "mapping_curve_s_x_4", 0x00000118, 15,  0,   CSR_RW, 0x00001FFF },
  { "mapping_curve_s_x_5", 0x00000118, 31, 16,   CSR_RW, 0x00003FFF },
  { "CURVE_S_X_2", 0x00000118, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_s_x_3
  { "mapping_curve_s_x_6", 0x0000011C, 15,  0,   CSR_RW, 0x00007FFF },
  { "mapping_curve_s_x_7", 0x0000011C, 31, 16,   CSR_RW, 0x0000FFFF },
  { "CURVE_S_X_3", 0x0000011C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_s_m_0
  { "mapping_curve_s_m_0", 0x00000120,  3,  0,   CSR_RW, 0x00000008 },
  { "mapping_curve_s_m_1", 0x00000120, 11,  8,   CSR_RW, 0x00000008 },
  { "mapping_curve_s_m_2", 0x00000120, 19, 16,   CSR_RW, 0x00000008 },
  { "mapping_curve_s_m_3", 0x00000120, 27, 24,   CSR_RW, 0x00000008 },
  { "CURVE_S_M_0", 0x00000120, 31, 0,   CSR_RW, 0x00000000 },
  // WORD curve_s_m_1
  { "mapping_curve_s_m_4", 0x00000124,  3,  0,   CSR_RW, 0x00000008 },
  { "mapping_curve_s_m_5", 0x00000124, 11,  8,   CSR_RW, 0x00000008 },
  { "mapping_curve_s_m_6", 0x00000124, 19, 16,   CSR_RW, 0x00000008 },
  { "mapping_curve_s_m_7", 0x00000124, 27, 24,   CSR_RW, 0x00000008 },
  { "CURVE_S_M_1", 0x00000124, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wore_debug_mon_sel
  { "debug_mon_sel", 0x00000128,  1,  0,   CSR_RW, 0x00000000 },
  { "WORE_DEBUG_MON_SEL", 0x00000128, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_DCC_H_
