#ifndef CSR_TABLE_AUDIO_HPF_H_
#define CSR_TABLE_AUDIO_HPF_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_audio_hpf[] =
{
  // WORD audio_hpf_b0
  { "hpf_b0", 0x00000000, 15,  0,   CSR_RW, 0x00001FD2 },
  { "AUDIO_HPF_B0", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_hpf_b1
  { "hpf_b1", 0x00000004, 15,  0,   CSR_RW, 0x0000E02E },
  { "AUDIO_HPF_B1", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_hpf_a
  { "hpf_a", 0x00000008, 15,  0,   CSR_RW, 0x00003F48 },
  { "AUDIO_HPF_A", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_AUDIO_HPF_H_
