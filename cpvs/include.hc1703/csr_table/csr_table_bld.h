#ifndef CSR_TABLE_BLD_H_
#define CSR_TABLE_BLD_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_bld[] =
{
  // WORD word_frame_start
  { "frame_start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "WORD_FRAME_START", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD irq_clear
  { "irq_clear_frame_end", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "irq_clear_weightr_frame_end", 0x00000004,  1,  1,  CSR_W1P, 0x00000000 },
  { "irq_clear_weightr_bw_insufficient", 0x00000004,  2,  2,  CSR_W1P, 0x00000000 },
  { "irq_clear_weightr_access_violation", 0x00000004,  3,  3,  CSR_W1P, 0x00000000 },
  { "IRQ_CLEAR", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD status
  { "status_frame_end", 0x00000008,  0,  0,   CSR_RO, 0x00000000 },
  { "status_weightr_frame_end", 0x00000008,  1,  1,   CSR_RO, 0x00000000 },
  { "status_weightr_bw_insufficient", 0x00000008,  2,  2,   CSR_RO, 0x00000000 },
  { "status_weightr_access_violation", 0x00000008,  3,  3,   CSR_RO, 0x00000000 },
  { "STATUS", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // WORD irq_mask
  { "irq_mask_frame_end", 0x0000000C,  0,  0,   CSR_RW, 0x00000001 },
  { "irq_mask_weightr_frame_end", 0x0000000C,  1,  1,   CSR_RW, 0x00000001 },
  { "irq_mask_weightr_bw_insufficient", 0x0000000C,  2,  2,   CSR_RW, 0x00000001 },
  { "irq_mask_weightr_access_violation", 0x0000000C,  3,  3,   CSR_RW, 0x00000001 },
  { "IRQ_MASK", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD resolution
  { "width", 0x00000010, 15,  0,   CSR_RW, 0x00000100 },
  { "height", 0x00000010, 31, 16,   CSR_RW, 0x00000438 },
  { "RESOLUTION", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_table_x_num
  { "table_x_num", 0x00000014,  5,  0,   CSR_RW, 0x00000021 },
  { "WORD_TABLE_X_NUM", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_table_cnt_ini
  { "table_x_cnt_ini", 0x00000018,  6,  0,   CSR_RW, 0x00000000 },
  { "table_y_cnt_ini", 0x00000018, 22, 16,   CSR_RW, 0x00000000 },
  { "WORD_TABLE_CNT_INI", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_table_cnt_step
  { "table_x_cnt_step", 0x0000001C,  6,  0,   CSR_RW, 0x00000000 },
  { "table_y_cnt_step", 0x0000001C, 22, 16,   CSR_RW, 0x00000000 },
  { "WORD_TABLE_CNT_STEP", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_debug_mon_sel
  { "debug_mon_sel", 0x00000020,  1,  0,   CSR_RW, 0x00000000 },
  { "WORD_DEBUG_MON_SEL", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_reserved_0
  { "reserved_0", 0x00000024, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED_0", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_reserved_1
  { "reserved_1", 0x00000028, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED_1", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_BLD_H_
