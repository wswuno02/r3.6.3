#ifndef CSR_TABLE_ENC_H_
#define CSR_TABLE_ENC_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_enc[] =
{
  // WORD enc001
  { "encoder_mode", 0x00000000,  1,  0,   CSR_RW, 0x00000000 },
  { "ENC001", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD enc002
  { "efuse_enc_pix_rate_violation", 0x00000004,  0,  0,   CSR_RO, 0x00000000 },
  { "ENC002", 0x00000004, 31, 0,   CSR_RO, 0x00000000 },
  // WORD enc003
  { "debug_mon_sel", 0x00000008,  2,  0,   CSR_RW, 0x00000000 },
  { "ENC003", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD enc004
  { "debug_mon_reg", 0x0000000C, 31,  0,   CSR_RO, 0x00000000 },
  { "ENC004", 0x0000000C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_layer_0_osd_mode
  { "layer_0_osd_mode", 0x00000010,  3,  0,   CSR_RW, 0x00000000 },
  { "layer_0_osd_yuv_sel", 0x00000010, 18, 16,   CSR_RW, 0x00000000 },
  { "WORD_LAYER_0_OSD_MODE", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_0_osd_0_en
  { "layer_0_osd_0_enable", 0x00000014,  0,  0,   CSR_RW, 0x00000000 },
  { "LAYER_0_OSD_0_EN", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_0_osd_0_start
  { "layer_0_osd_0_start_x", 0x00000018, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_0_osd_0_start_y", 0x00000018, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_0_OSD_0_START", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_0_osd_0_end
  { "layer_0_osd_0_end_x", 0x0000001C, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_0_osd_0_end_y", 0x0000001C, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_0_OSD_0_END", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_0_osd_1_en
  { "layer_0_osd_1_enable", 0x00000020,  0,  0,   CSR_RW, 0x00000000 },
  { "LAYER_0_OSD_1_EN", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_0_osd_1_start
  { "layer_0_osd_1_start_x", 0x00000024, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_0_osd_1_start_y", 0x00000024, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_0_OSD_1_START", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_0_osd_1_end
  { "layer_0_osd_1_end_x", 0x00000028, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_0_osd_1_end_y", 0x00000028, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_0_OSD_1_END", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_0_osd_2_en
  { "layer_0_osd_2_enable", 0x0000002C,  0,  0,   CSR_RW, 0x00000000 },
  { "LAYER_0_OSD_2_EN", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_0_osd_2_start
  { "layer_0_osd_2_start_x", 0x00000030, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_0_osd_2_start_y", 0x00000030, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_0_OSD_2_START", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_0_osd_2_end
  { "layer_0_osd_2_end_x", 0x00000034, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_0_osd_2_end_y", 0x00000034, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_0_OSD_2_END", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_0_osd_3_en
  { "layer_0_osd_3_enable", 0x00000038,  0,  0,   CSR_RW, 0x00000000 },
  { "LAYER_0_OSD_3_EN", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_0_osd_3_start
  { "layer_0_osd_3_start_x", 0x0000003C, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_0_osd_3_start_y", 0x0000003C, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_0_OSD_3_START", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_0_osd_3_end
  { "layer_0_osd_3_end_x", 0x00000040, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_0_osd_3_end_y", 0x00000040, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_0_OSD_3_END", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_mode
  { "layer_bb_osd_yuv_sel", 0x00000044,  2,  0,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_MODE", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_0_en
  { "layer_bb_osd_0_enable", 0x00000048,  0,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_0_mask", 0x00000048, 16, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_0_EN", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_0_start
  { "layer_bb_osd_0_start_x", 0x0000004C, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_0_start_y", 0x0000004C, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_0_START", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_0_end
  { "layer_bb_osd_0_end_x", 0x00000050, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_0_end_y", 0x00000050, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_0_END", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_0_color
  { "layer_bb_osd_0_y", 0x00000054,  7,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_0_u", 0x00000054, 15,  8,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_0_v", 0x00000054, 23, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_0_COLOR", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_0_line
  { "layer_bb_osd_0_line_width", 0x00000058,  3,  0,   CSR_RW, 0x00000002 },
  { "layer_bb_osd_0_line_alpha", 0x00000058, 23, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_0_LINE", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_1_en
  { "layer_bb_osd_1_enable", 0x0000005C,  0,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_1_mask", 0x0000005C, 16, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_1_EN", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_1_start
  { "layer_bb_osd_1_start_x", 0x00000060, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_1_start_y", 0x00000060, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_1_START", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_1_end
  { "layer_bb_osd_1_end_x", 0x00000064, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_1_end_y", 0x00000064, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_1_END", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_1_color
  { "layer_bb_osd_1_y", 0x00000068,  7,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_1_u", 0x00000068, 15,  8,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_1_v", 0x00000068, 23, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_1_COLOR", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_1_line
  { "layer_bb_osd_1_line_width", 0x0000006C,  3,  0,   CSR_RW, 0x00000002 },
  { "layer_bb_osd_1_line_alpha", 0x0000006C, 23, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_1_LINE", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_2_en
  { "layer_bb_osd_2_enable", 0x00000070,  0,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_2_mask", 0x00000070, 16, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_2_EN", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_2_start
  { "layer_bb_osd_2_start_x", 0x00000074, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_2_start_y", 0x00000074, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_2_START", 0x00000074, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_2_end
  { "layer_bb_osd_2_end_x", 0x00000078, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_2_end_y", 0x00000078, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_2_END", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_2_color
  { "layer_bb_osd_2_y", 0x0000007C,  7,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_2_u", 0x0000007C, 15,  8,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_2_v", 0x0000007C, 23, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_2_COLOR", 0x0000007C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_2_line
  { "layer_bb_osd_2_line_width", 0x00000080,  3,  0,   CSR_RW, 0x00000002 },
  { "layer_bb_osd_2_line_alpha", 0x00000080, 23, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_2_LINE", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_3_en
  { "layer_bb_osd_3_enable", 0x00000084,  0,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_3_mask", 0x00000084, 16, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_3_EN", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_3_start
  { "layer_bb_osd_3_start_x", 0x00000088, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_3_start_y", 0x00000088, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_3_START", 0x00000088, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_3_end
  { "layer_bb_osd_3_end_x", 0x0000008C, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_3_end_y", 0x0000008C, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_3_END", 0x0000008C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_3_color
  { "layer_bb_osd_3_y", 0x00000090,  7,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_3_u", 0x00000090, 15,  8,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_3_v", 0x00000090, 23, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_3_COLOR", 0x00000090, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_3_line
  { "layer_bb_osd_3_line_width", 0x00000094,  3,  0,   CSR_RW, 0x00000002 },
  { "layer_bb_osd_3_line_alpha", 0x00000094, 23, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_3_LINE", 0x00000094, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_4_en
  { "layer_bb_osd_4_enable", 0x00000098,  0,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_4_mask", 0x00000098, 16, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_4_EN", 0x00000098, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_4_start
  { "layer_bb_osd_4_start_x", 0x0000009C, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_4_start_y", 0x0000009C, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_4_START", 0x0000009C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_4_end
  { "layer_bb_osd_4_end_x", 0x000000A0, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_4_end_y", 0x000000A0, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_4_END", 0x000000A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_4_color
  { "layer_bb_osd_4_y", 0x000000A4,  7,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_4_u", 0x000000A4, 15,  8,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_4_v", 0x000000A4, 23, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_4_COLOR", 0x000000A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_4_line
  { "layer_bb_osd_4_line_width", 0x000000A8,  3,  0,   CSR_RW, 0x00000002 },
  { "layer_bb_osd_4_line_alpha", 0x000000A8, 23, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_4_LINE", 0x000000A8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_5_en
  { "layer_bb_osd_5_enable", 0x000000AC,  0,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_5_mask", 0x000000AC, 16, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_5_EN", 0x000000AC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_5_start
  { "layer_bb_osd_5_start_x", 0x000000B0, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_5_start_y", 0x000000B0, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_5_START", 0x000000B0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_5_end
  { "layer_bb_osd_5_end_x", 0x000000B4, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_5_end_y", 0x000000B4, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_5_END", 0x000000B4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_5_color
  { "layer_bb_osd_5_y", 0x000000B8,  7,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_5_u", 0x000000B8, 15,  8,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_5_v", 0x000000B8, 23, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_5_COLOR", 0x000000B8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_5_line
  { "layer_bb_osd_5_line_width", 0x000000BC,  3,  0,   CSR_RW, 0x00000002 },
  { "layer_bb_osd_5_line_alpha", 0x000000BC, 23, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_5_LINE", 0x000000BC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_6_en
  { "layer_bb_osd_6_enable", 0x000000C0,  0,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_6_mask", 0x000000C0, 16, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_6_EN", 0x000000C0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_6_start
  { "layer_bb_osd_6_start_x", 0x000000C4, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_6_start_y", 0x000000C4, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_6_START", 0x000000C4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_6_end
  { "layer_bb_osd_6_end_x", 0x000000C8, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_6_end_y", 0x000000C8, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_6_END", 0x000000C8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_6_color
  { "layer_bb_osd_6_y", 0x000000CC,  7,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_6_u", 0x000000CC, 15,  8,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_6_v", 0x000000CC, 23, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_6_COLOR", 0x000000CC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_6_line
  { "layer_bb_osd_6_line_width", 0x000000D0,  3,  0,   CSR_RW, 0x00000002 },
  { "layer_bb_osd_6_line_alpha", 0x000000D0, 23, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_6_LINE", 0x000000D0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_7_en
  { "layer_bb_osd_7_enable", 0x000000D4,  0,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_7_mask", 0x000000D4, 16, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_7_EN", 0x000000D4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_7_start
  { "layer_bb_osd_7_start_x", 0x000000D8, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_7_start_y", 0x000000D8, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_7_START", 0x000000D8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_7_end
  { "layer_bb_osd_7_end_x", 0x000000DC, 15,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_7_end_y", 0x000000DC, 31, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_7_END", 0x000000DC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_7_color
  { "layer_bb_osd_7_y", 0x000000E0,  7,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_7_u", 0x000000E0, 15,  8,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_7_v", 0x000000E0, 23, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_7_COLOR", 0x000000E0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD layer_bb_osd_7_line
  { "layer_bb_osd_7_line_width", 0x000000E4,  3,  0,   CSR_RW, 0x00000002 },
  { "layer_bb_osd_7_line_alpha", 0x000000E4, 23, 16,   CSR_RW, 0x00000000 },
  { "LAYER_BB_OSD_7_LINE", 0x000000E4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wildcard_0
  { "wildcard_0_y", 0x000000E8,  7,  0,   CSR_RW, 0x00000010 },
  { "wildcard_0_u", 0x000000E8, 15,  8,   CSR_RW, 0x00000080 },
  { "wildcard_0_v", 0x000000E8, 23, 16,   CSR_RW, 0x00000080 },
  { "wildcard_0_valid", 0x000000E8, 24, 24,   CSR_RW, 0x00000000 },
  { "WILDCARD_0", 0x000000E8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wildcard_1
  { "wildcard_1_y", 0x000000EC,  7,  0,   CSR_RW, 0x00000010 },
  { "wildcard_1_u", 0x000000EC, 15,  8,   CSR_RW, 0x00000080 },
  { "wildcard_1_v", 0x000000EC, 23, 16,   CSR_RW, 0x00000080 },
  { "wildcard_1_valid", 0x000000EC, 24, 24,   CSR_RW, 0x00000000 },
  { "WILDCARD_1", 0x000000EC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wildcard_2
  { "wildcard_2_y", 0x000000F0,  7,  0,   CSR_RW, 0x00000010 },
  { "wildcard_2_u", 0x000000F0, 15,  8,   CSR_RW, 0x00000080 },
  { "wildcard_2_v", 0x000000F0, 23, 16,   CSR_RW, 0x00000080 },
  { "wildcard_2_valid", 0x000000F0, 24, 24,   CSR_RW, 0x00000000 },
  { "WILDCARD_2", 0x000000F0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wildcard_3
  { "wildcard_3_y", 0x000000F4,  7,  0,   CSR_RW, 0x00000010 },
  { "wildcard_3_u", 0x000000F4, 15,  8,   CSR_RW, 0x00000080 },
  { "wildcard_3_v", 0x000000F4, 23, 16,   CSR_RW, 0x00000080 },
  { "wildcard_3_valid", 0x000000F4, 24, 24,   CSR_RW, 0x00000000 },
  { "WILDCARD_3", 0x000000F4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wildcard_4
  { "wildcard_4_y", 0x000000F8,  7,  0,   CSR_RW, 0x00000010 },
  { "wildcard_4_u", 0x000000F8, 15,  8,   CSR_RW, 0x00000080 },
  { "wildcard_4_v", 0x000000F8, 23, 16,   CSR_RW, 0x00000080 },
  { "wildcard_4_valid", 0x000000F8, 24, 24,   CSR_RW, 0x00000000 },
  { "WILDCARD_4", 0x000000F8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wildcard_5
  { "wildcard_5_y", 0x000000FC,  7,  0,   CSR_RW, 0x00000010 },
  { "wildcard_5_u", 0x000000FC, 15,  8,   CSR_RW, 0x00000080 },
  { "wildcard_5_v", 0x000000FC, 23, 16,   CSR_RW, 0x00000080 },
  { "wildcard_5_valid", 0x000000FC, 24, 24,   CSR_RW, 0x00000000 },
  { "WILDCARD_5", 0x000000FC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wildcard_6
  { "wildcard_6_y", 0x00000100,  7,  0,   CSR_RW, 0x00000010 },
  { "wildcard_6_u", 0x00000100, 15,  8,   CSR_RW, 0x00000080 },
  { "wildcard_6_v", 0x00000100, 23, 16,   CSR_RW, 0x00000080 },
  { "wildcard_6_valid", 0x00000100, 24, 24,   CSR_RW, 0x00000000 },
  { "WILDCARD_6", 0x00000100, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wildcard_7
  { "wildcard_7_y", 0x00000104,  7,  0,   CSR_RW, 0x00000010 },
  { "wildcard_7_u", 0x00000104, 15,  8,   CSR_RW, 0x00000080 },
  { "wildcard_7_v", 0x00000104, 23, 16,   CSR_RW, 0x00000080 },
  { "wildcard_7_valid", 0x00000104, 24, 24,   CSR_RW, 0x00000000 },
  { "WILDCARD_7", 0x00000104, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mem_ctrl
  { "sd", 0x00000108,  0,  0,   CSR_RW, 0x00000000 },
  { "slp", 0x00000108, 16, 16,   CSR_RW, 0x00000000 },
  { "MEM_CTRL", 0x00000108, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_reserved_0
  { "reserved_0", 0x0000010C, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED_0", 0x0000010C, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_ENC_H_
