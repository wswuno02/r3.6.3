#ifndef CSR_TABLE_CK_AON_H_
#define CSR_TABLE_CK_AON_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_ck_aon[] =
{
  // WORD ckg_aon_0
  { "cken_psd", 0x00000000,  0,  0,   CSR_RW, 0x00000001 },
  { "cken_lpmd", 0x00000000,  8,  8,   CSR_RW, 0x00000001 },
  { "cken_rtc", 0x00000000, 16, 16,   CSR_RW, 0x00000001 },
  { "cken_wdt", 0x00000000, 24, 24,   CSR_RW, 0x00000001 },
  { "CKG_AON_0", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_aon_1
  { "cken_amc", 0x00000004,  0,  0,   CSR_RW, 0x00000001 },
  { "cken_irq", 0x00000004,  8,  8,   CSR_RW, 0x00000001 },
  { "cken_dvp", 0x00000004, 16, 16,   CSR_RW, 0x00000001 },
  { "CKG_AON_1", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_reserved_0
  { "reserved_0", 0x00000008, 15,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED_0", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_reserved_1
  { "reserved_1", 0x0000000C, 15,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED_1", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_CK_AON_H_
