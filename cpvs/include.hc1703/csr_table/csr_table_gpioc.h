#ifndef CSR_TABLE_GPIOC_H_
#define CSR_TABLE_GPIOC_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_gpioc[] =
{
  // WORD aon_gpio_o
  { "aon_gpio_o_0", 0x00000000, 12,  0,   CSR_RW, 0x00000000 },
  { "AON_GPIO_O", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD aon_gpio_oe
  { "aon_gpio_oe_0", 0x00000004, 12,  0,   CSR_RW, 0x00000000 },
  { "AON_GPIO_OE", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD aon_gpio_i
  { "aon_gpio_i_0", 0x00000008, 12,  0,   CSR_RO, 0x00000000 },
  { "AON_GPIO_I", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_gpio_o_0
  { "gpio_o_0", 0x0000000C, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_GPIO_O_0", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gpio_oe_0
  { "gpio_oe_0", 0x00000010, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_GPIO_OE_0", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gpio_i_0
  { "gpio_i_0", 0x00000014, 31,  0,   CSR_RO, 0x00000000 },
  { "WORD_GPIO_I_0", 0x00000014, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_gpio_o_1
  { "gpio_o_1", 0x00000018, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_GPIO_O_1", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gpio_oe_1
  { "gpio_oe_1", 0x0000001C, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_GPIO_OE_1", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gpio_i_1
  { "gpio_i_1", 0x00000020, 31,  0,   CSR_RO, 0x00000000 },
  { "WORD_GPIO_I_1", 0x00000020, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_gpio_o_2
  { "gpio_o_2", 0x00000024,  3,  0,   CSR_RW, 0x00000000 },
  { "WORD_GPIO_O_2", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gpio_oe_2
  { "gpio_oe_2", 0x00000028,  3,  0,   CSR_RW, 0x00000000 },
  { "WORD_GPIO_OE_2", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_gpio_i_2
  { "gpio_i_2", 0x00000030,  3,  0,   CSR_RO, 0x00000000 },
  { "WORD_GPIO_I_2", 0x00000030, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_mipi_tx_gpio_o_0
  { "mipi_tx_gpio_o_0", 0x00000034,  9,  0,   CSR_RW, 0x00000000 },
  { "WORD_MIPI_TX_GPIO_O_0", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_mipi_rx_gpio_i_0
  { "mipi_rx_gpio_i_0", 0x00000038,  9,  0,   CSR_RO, 0x00000000 },
  { "WORD_MIPI_RX_GPIO_I_0", 0x00000038, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_mipi_rx_gpio_i_1
  { "mipi_rx_gpio_i_1", 0x0000003C,  9,  0,   CSR_RO, 0x00000000 },
  { "WORD_MIPI_RX_GPIO_I_1", 0x0000003C, 31, 0,   CSR_RO, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_GPIOC_H_
