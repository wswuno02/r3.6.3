#ifndef CSR_TABLE_WDT_H_
#define CSR_TABLE_WDT_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_wdt[] =
{
  // WORD wdt00
  { "trigger", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "WDT00", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD wdt01
  { "irq_clear_wdt", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "WDT01", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD wdt02
  { "status_wdt", 0x00000008,  0,  0,   CSR_RO, 0x00000000 },
  { "WDT02", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // WORD wdt03
  { "lock_status", 0x0000000C,  0,  0,   CSR_RO, 0x00000000 },
  { "stage", 0x0000000C,  9,  8,   CSR_RO, 0x00000000 },
  { "count_value", 0x0000000C, 31, 16,   CSR_RO, 0x00000000 },
  { "WDT03", 0x0000000C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD wdt04
  { "unlock_reg", 0x00000010,  7,  0,   CSR_RW, 0x00000000 },
  { "WDT04", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wdt05
  { "sw_disable", 0x00000014,  0,  0,   CSR_RW, 0x00000000 },
  { "polarity", 0x00000014,  8,  8,   CSR_RW, 0x00000000 },
  { "prescaler_0", 0x00000014, 19, 16,   CSR_RW, 0x0000000F },
  { "prescaler_1", 0x00000014, 27, 24,   CSR_RW, 0x00000006 },
  { "WDT05", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wdt06
  { "count_initial", 0x00000018, 15,  0,   CSR_RW, 0x000055D5 },
  { "count_stage1_min", 0x00000018, 31, 16,   CSR_RW, 0x000015F9 },
  { "WDT06", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wdt07
  { "count_stage1_max", 0x0000001C, 15,  0,   CSR_RW, 0x0000186A },
  { "count_stage2", 0x0000001C, 31, 16,   CSR_RW, 0x0000186A },
  { "WDT07", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD wdt08
  { "reserved", 0x00000020, 31,  0,   CSR_RW, 0x00000000 },
  { "WDT08", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_WDT_H_
