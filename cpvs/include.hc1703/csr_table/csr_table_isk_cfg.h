#ifndef CSR_TABLE_ISK_CFG_H_
#define CSR_TABLE_ISK_CFG_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_isk_cfg[] =
{
  // WORD conf0_agma
  { "cken_agma", 0x00000000,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_agma", 0x00000000,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_AGMA", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_agma
  { "sw_rst_agma", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_agma", 0x00000004,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_AGMA", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_cvs
  { "cken_cvs", 0x00000008,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_cvs", 0x00000008,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_CVS", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_cvs
  { "sw_rst_cvs", 0x0000000C,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_cvs", 0x0000000C,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_CVS", 0x0000000C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_cs
  { "cken_cs", 0x00000010,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_cs", 0x00000010,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_CS", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_cs
  { "sw_rst_cs", 0x00000014,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_cs", 0x00000014,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_CS", 0x00000014, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_fpnr
  { "cken_fpnr", 0x00000018,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_fpnr", 0x00000018,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_FPNR", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_fpnr
  { "sw_rst_fpnr", 0x0000001C,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_fpnr", 0x0000001C,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_FPNR", 0x0000001C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_bsp
  { "cken_bsp", 0x00000020,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_bsp", 0x00000020,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_BSP", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_bsp
  { "sw_rst_bsp", 0x00000024,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_bsp", 0x00000024,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_BSP", 0x00000024, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_fsc
  { "cken_fsc", 0x00000028,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_fsc", 0x00000028,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_FSC", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_fsc
  { "sw_rst_fsc", 0x0000002C,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_fsc", 0x0000002C,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_FSC", 0x0000002C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_fgma
  { "cken_fgma", 0x00000030,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_fgma", 0x00000030,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_FGMA", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_fgma
  { "sw_rst_fgma", 0x00000034,  0,  0,  CSR_W1P, 0x00000000 },
  { "CONF1_FGMA", 0x00000034, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_ctrl
  { "cken_ctrl", 0x00000038,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_ctrl", 0x00000038,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_CTRL", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_ctrl
  { "sw_rst_ctrl", 0x0000003C,  0,  0,  CSR_W1P, 0x00000000 },
  { "CONF1_CTRL", 0x0000003C, 31, 0,  CSR_W1P, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_ISK_CFG_H_
