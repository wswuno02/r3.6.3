#ifndef CSR_TABLE_I2CM_H_
#define CSR_TABLE_I2CM_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_i2cm[] =
{
  // WORD i2cm00
  { "action_start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "I2CM00", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD i2cm01
  { "irq_clear_action_end", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "I2CM01", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD i2cm02
  { "irq_action_action_end", 0x00000008,  0,  0,   CSR_RO, 0x00000000 },
  { "I2CM02", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // WORD i2cm03
  { "irq_mask_action_end", 0x0000000C,  0,  0,   CSR_RW, 0x00000001 },
  { "I2CM03", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2cm04
  { "engine_status", 0x00000010,  0,  0,   CSR_RO, 0x00000000 },
  { "action_status", 0x00000010, 17, 16,   CSR_RO, 0x00000000 },
  { "I2CM04", 0x00000010, 31, 0,   CSR_RO, 0x00000000 },
  // WORD i2cm05
  { "write_byte_cnt", 0x00000014,  4,  0,   CSR_RO, 0x00000000 },
  { "write_trans_cnt", 0x00000014, 12,  8,   CSR_RO, 0x00000000 },
  { "read_byte_cnt", 0x00000014, 20, 16,   CSR_RO, 0x00000000 },
  { "I2CM05", 0x00000014, 31, 0,   CSR_RO, 0x00000000 },
  // WORD i2cm06
  { "sda_deglitch_th", 0x00000018,  5,  0,   CSR_RW, 0x00000000 },
  { "sdo_delay_cycle", 0x00000018, 15,  8,   CSR_RW, 0x00000000 },
  { "scl_rate_cycle", 0x00000018, 23, 16,   CSR_RW, 0x00000078 },
  { "I2CM06", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2cm07
  { "read_mode", 0x0000001C,  0,  0,   CSR_RW, 0x00000000 },
  { "restart_mode", 0x0000001C, 16, 16,   CSR_RW, 0x00000000 },
  { "I2CM07", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2cm08
  { "target_address", 0x00000020,  6,  0,   CSR_RW, 0x00000000 },
  { "operation", 0x00000020,  8,  8,   CSR_RW, 0x00000000 },
  { "I2CM08", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2cm09
  { "write_byte_num_m1", 0x00000024,  4,  0,   CSR_RW, 0x00000003 },
  { "write_trans_num_m1", 0x00000024, 12,  8,   CSR_RW, 0x00000000 },
  { "read_byte_num_m1", 0x00000024, 20, 16,   CSR_RW, 0x00000001 },
  { "I2CM09", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2cm10
  { "wdata_0", 0x00000028,  7,  0,   CSR_RW, 0x00000000 },
  { "wdata_1", 0x00000028, 15,  8,   CSR_RW, 0x00000000 },
  { "wdata_2", 0x00000028, 23, 16,   CSR_RW, 0x00000000 },
  { "wdata_3", 0x00000028, 31, 24,   CSR_RW, 0x00000000 },
  { "I2CM10", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2cm11
  { "wdata_4", 0x0000002C,  7,  0,   CSR_RW, 0x00000000 },
  { "wdata_5", 0x0000002C, 15,  8,   CSR_RW, 0x00000000 },
  { "wdata_6", 0x0000002C, 23, 16,   CSR_RW, 0x00000000 },
  { "wdata_7", 0x0000002C, 31, 24,   CSR_RW, 0x00000000 },
  { "I2CM11", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2cm12
  { "wdata_8", 0x00000030,  7,  0,   CSR_RW, 0x00000000 },
  { "wdata_9", 0x00000030, 15,  8,   CSR_RW, 0x00000000 },
  { "wdata_10", 0x00000030, 23, 16,   CSR_RW, 0x00000000 },
  { "wdata_11", 0x00000030, 31, 24,   CSR_RW, 0x00000000 },
  { "I2CM12", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2cm13
  { "wdata_12", 0x00000034,  7,  0,   CSR_RW, 0x00000000 },
  { "wdata_13", 0x00000034, 15,  8,   CSR_RW, 0x00000000 },
  { "wdata_14", 0x00000034, 23, 16,   CSR_RW, 0x00000000 },
  { "wdata_15", 0x00000034, 31, 24,   CSR_RW, 0x00000000 },
  { "I2CM13", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2cm14
  { "wdata_16", 0x00000038,  7,  0,   CSR_RW, 0x00000000 },
  { "wdata_17", 0x00000038, 15,  8,   CSR_RW, 0x00000000 },
  { "wdata_18", 0x00000038, 23, 16,   CSR_RW, 0x00000000 },
  { "wdata_19", 0x00000038, 31, 24,   CSR_RW, 0x00000000 },
  { "I2CM14", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2cm15
  { "wdata_20", 0x0000003C,  7,  0,   CSR_RW, 0x00000000 },
  { "wdata_21", 0x0000003C, 15,  8,   CSR_RW, 0x00000000 },
  { "wdata_22", 0x0000003C, 23, 16,   CSR_RW, 0x00000000 },
  { "wdata_23", 0x0000003C, 31, 24,   CSR_RW, 0x00000000 },
  { "I2CM15", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2cm16
  { "wdata_24", 0x00000040,  7,  0,   CSR_RW, 0x00000000 },
  { "wdata_25", 0x00000040, 15,  8,   CSR_RW, 0x00000000 },
  { "wdata_26", 0x00000040, 23, 16,   CSR_RW, 0x00000000 },
  { "wdata_27", 0x00000040, 31, 24,   CSR_RW, 0x00000000 },
  { "I2CM16", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2cm17
  { "wdata_28", 0x00000044,  7,  0,   CSR_RW, 0x00000000 },
  { "wdata_29", 0x00000044, 15,  8,   CSR_RW, 0x00000000 },
  { "wdata_30", 0x00000044, 23, 16,   CSR_RW, 0x00000000 },
  { "wdata_31", 0x00000044, 31, 24,   CSR_RW, 0x00000000 },
  { "I2CM17", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2cm18
  { "rdata_0", 0x00000048,  7,  0,   CSR_RO, 0x00000000 },
  { "rdata_1", 0x00000048, 15,  8,   CSR_RO, 0x00000000 },
  { "rdata_2", 0x00000048, 23, 16,   CSR_RO, 0x00000000 },
  { "rdata_3", 0x00000048, 31, 24,   CSR_RO, 0x00000000 },
  { "I2CM18", 0x00000048, 31, 0,   CSR_RO, 0x00000000 },
  // WORD i2cm19
  { "rdata_4", 0x0000004C,  7,  0,   CSR_RO, 0x00000000 },
  { "rdata_5", 0x0000004C, 15,  8,   CSR_RO, 0x00000000 },
  { "rdata_6", 0x0000004C, 23, 16,   CSR_RO, 0x00000000 },
  { "rdata_7", 0x0000004C, 31, 24,   CSR_RO, 0x00000000 },
  { "I2CM19", 0x0000004C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD i2cm20
  { "debug_mon_sel", 0x00000050,  0,  0,   CSR_RW, 0x00000000 },
  { "I2CM20", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2cm21
  { "reserved", 0x00000054, 31,  0,   CSR_RW, 0x00000000 },
  { "I2CM21", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_I2CM_H_
