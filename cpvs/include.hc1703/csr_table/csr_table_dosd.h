#ifndef CSR_TABLE_DOSD_H_
#define CSR_TABLE_DOSD_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_dosd[] =
{
  // WORD dosd000
  { "frame_start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "DOSD000", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD dosd001
  { "irq_clear_frame_end", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "DOSD001", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD dosd002
  { "status_frame_end", 0x00000008,  0,  0,   CSR_RO, 0x00000000 },
  { "DOSD002", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // WORD dosd003
  { "irq_mask_frame_end", 0x0000000C,  0,  0,   CSR_RW, 0x00000001 },
  { "DOSD003", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd004
  { "frame_width", 0x00000010, 12,  0,   CSR_RW, 0x00000800 },
  { "frame_height", 0x00000010, 28, 16,   CSR_RW, 0x00000480 },
  { "DOSD004", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd005
  { "dosd_format", 0x00000014,  1,  0,   CSR_RW, 0x00000000 },
  { "dosd_bypass_mode", 0x00000014, 16, 16,   CSR_RW, 0x00000000 },
  { "DOSD005", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd010
  { "layer_0_osd_mode", 0x00000028,  3,  0,   CSR_RW, 0x00000000 },
  { "layer_0_osd_yuv_sel", 0x00000028, 18, 16,   CSR_RW, 0x00000000 },
  { "DOSD010", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd011
  { "layer_0_osd_0_enable", 0x0000002C,  0,  0,   CSR_RW, 0x00000000 },
  { "DOSD011", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd012
  { "layer_0_osd_0_start_x", 0x00000030, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_0_osd_0_start_y", 0x00000030, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD012", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd013
  { "layer_0_osd_0_end_x", 0x00000034, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_0_osd_0_end_y", 0x00000034, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD013", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd014
  { "layer_0_osd_1_enable", 0x00000038,  0,  0,   CSR_RW, 0x00000000 },
  { "DOSD014", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd015
  { "layer_0_osd_1_start_x", 0x0000003C, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_0_osd_1_start_y", 0x0000003C, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD015", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd016
  { "layer_0_osd_1_end_x", 0x00000040, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_0_osd_1_end_y", 0x00000040, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD016", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd017
  { "layer_0_osd_2_enable", 0x00000044,  0,  0,   CSR_RW, 0x00000000 },
  { "DOSD017", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd018
  { "layer_0_osd_2_start_x", 0x00000048, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_0_osd_2_start_y", 0x00000048, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD018", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd019
  { "layer_0_osd_2_end_x", 0x0000004C, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_0_osd_2_end_y", 0x0000004C, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD019", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd020
  { "layer_0_osd_3_enable", 0x00000050,  0,  0,   CSR_RW, 0x00000000 },
  { "DOSD020", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd021
  { "layer_0_osd_3_start_x", 0x00000054, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_0_osd_3_start_y", 0x00000054, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD021", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd022
  { "layer_0_osd_3_end_x", 0x00000058, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_0_osd_3_end_y", 0x00000058, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD022", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd030
  { "layer_1_osd_mode", 0x00000078,  3,  0,   CSR_RW, 0x00000000 },
  { "layer_1_osd_yuv_sel", 0x00000078, 18, 16,   CSR_RW, 0x00000000 },
  { "DOSD030", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd031
  { "layer_1_osd_0_enable", 0x0000007C,  0,  0,   CSR_RW, 0x00000000 },
  { "DOSD031", 0x0000007C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd032
  { "layer_1_osd_0_start_x", 0x00000080, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_1_osd_0_start_y", 0x00000080, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD032", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd033
  { "layer_1_osd_0_end_x", 0x00000084, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_1_osd_0_end_y", 0x00000084, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD033", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd034
  { "layer_1_osd_1_enable", 0x00000088,  0,  0,   CSR_RW, 0x00000000 },
  { "DOSD034", 0x00000088, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd035
  { "layer_1_osd_1_start_x", 0x0000008C, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_1_osd_1_start_y", 0x0000008C, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD035", 0x0000008C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd036
  { "layer_1_osd_1_end_x", 0x00000090, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_1_osd_1_end_y", 0x00000090, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD036", 0x00000090, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd037
  { "layer_1_osd_2_enable", 0x00000094,  0,  0,   CSR_RW, 0x00000000 },
  { "DOSD037", 0x00000094, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd038
  { "layer_1_osd_2_start_x", 0x00000098, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_1_osd_2_start_y", 0x00000098, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD038", 0x00000098, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd039
  { "layer_1_osd_2_end_x", 0x0000009C, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_1_osd_2_end_y", 0x0000009C, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD039", 0x0000009C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd040
  { "layer_1_osd_3_enable", 0x000000A0,  0,  0,   CSR_RW, 0x00000000 },
  { "DOSD040", 0x000000A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd041
  { "layer_1_osd_3_start_x", 0x000000A4, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_1_osd_3_start_y", 0x000000A4, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD041", 0x000000A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd042
  { "layer_1_osd_3_end_x", 0x000000A8, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_1_osd_3_end_y", 0x000000A8, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD042", 0x000000A8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd050
  { "layer_bb_osd_yuv_sel", 0x000000C8,  2,  0,   CSR_RW, 0x00000000 },
  { "DOSD050", 0x000000C8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd051
  { "layer_bb_osd_0_enable", 0x000000CC,  0,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_0_mask", 0x000000CC, 16, 16,   CSR_RW, 0x00000000 },
  { "DOSD051", 0x000000CC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd052
  { "layer_bb_osd_0_start_x", 0x000000D0, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_0_start_y", 0x000000D0, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD052", 0x000000D0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd053
  { "layer_bb_osd_0_end_x", 0x000000D4, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_0_end_y", 0x000000D4, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD053", 0x000000D4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd054
  { "layer_bb_osd_0_y", 0x000000D8,  7,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_0_u", 0x000000D8, 15,  8,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_0_v", 0x000000D8, 23, 16,   CSR_RW, 0x00000000 },
  { "DOSD054", 0x000000D8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd055
  { "layer_bb_osd_0_line_width", 0x000000DC,  3,  0,   CSR_RW, 0x00000002 },
  { "layer_bb_osd_0_line_alpha", 0x000000DC, 23, 16,   CSR_RW, 0x00000000 },
  { "DOSD055", 0x000000DC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd061
  { "layer_bb_osd_1_enable", 0x000000F4,  0,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_1_mask", 0x000000F4, 16, 16,   CSR_RW, 0x00000000 },
  { "DOSD061", 0x000000F4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd062
  { "layer_bb_osd_1_start_x", 0x000000F8, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_1_start_y", 0x000000F8, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD062", 0x000000F8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd063
  { "layer_bb_osd_1_end_x", 0x000000FC, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_1_end_y", 0x000000FC, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD063", 0x000000FC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd064
  { "layer_bb_osd_1_y", 0x00000100,  7,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_1_u", 0x00000100, 15,  8,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_1_v", 0x00000100, 23, 16,   CSR_RW, 0x00000000 },
  { "DOSD064", 0x00000100, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd065
  { "layer_bb_osd_1_line_width", 0x00000104,  3,  0,   CSR_RW, 0x00000002 },
  { "layer_bb_osd_1_line_alpha", 0x00000104, 23, 16,   CSR_RW, 0x00000000 },
  { "DOSD065", 0x00000104, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd071
  { "layer_bb_osd_2_enable", 0x0000011C,  0,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_2_mask", 0x0000011C, 16, 16,   CSR_RW, 0x00000000 },
  { "DOSD071", 0x0000011C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd072
  { "layer_bb_osd_2_start_x", 0x00000120, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_2_start_y", 0x00000120, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD072", 0x00000120, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd073
  { "layer_bb_osd_2_end_x", 0x00000124, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_2_end_y", 0x00000124, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD073", 0x00000124, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd074
  { "layer_bb_osd_2_y", 0x00000128,  7,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_2_u", 0x00000128, 15,  8,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_2_v", 0x00000128, 23, 16,   CSR_RW, 0x00000000 },
  { "DOSD074", 0x00000128, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd075
  { "layer_bb_osd_2_line_width", 0x0000012C,  3,  0,   CSR_RW, 0x00000002 },
  { "layer_bb_osd_2_line_alpha", 0x0000012C, 23, 16,   CSR_RW, 0x00000000 },
  { "DOSD075", 0x0000012C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd081
  { "layer_bb_osd_3_enable", 0x00000144,  0,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_3_mask", 0x00000144, 16, 16,   CSR_RW, 0x00000000 },
  { "DOSD081", 0x00000144, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd082
  { "layer_bb_osd_3_start_x", 0x00000148, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_3_start_y", 0x00000148, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD082", 0x00000148, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd083
  { "layer_bb_osd_3_end_x", 0x0000014C, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_3_end_y", 0x0000014C, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD083", 0x0000014C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd084
  { "layer_bb_osd_3_y", 0x00000150,  7,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_3_u", 0x00000150, 15,  8,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_3_v", 0x00000150, 23, 16,   CSR_RW, 0x00000000 },
  { "DOSD084", 0x00000150, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd085
  { "layer_bb_osd_3_line_width", 0x00000154,  3,  0,   CSR_RW, 0x00000002 },
  { "layer_bb_osd_3_line_alpha", 0x00000154, 23, 16,   CSR_RW, 0x00000000 },
  { "DOSD085", 0x00000154, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd091
  { "layer_bb_osd_4_enable", 0x0000016C,  0,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_4_mask", 0x0000016C, 16, 16,   CSR_RW, 0x00000000 },
  { "DOSD091", 0x0000016C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd092
  { "layer_bb_osd_4_start_x", 0x00000170, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_4_start_y", 0x00000170, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD092", 0x00000170, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd093
  { "layer_bb_osd_4_end_x", 0x00000174, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_4_end_y", 0x00000174, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD093", 0x00000174, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd094
  { "layer_bb_osd_4_y", 0x00000178,  7,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_4_u", 0x00000178, 15,  8,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_4_v", 0x00000178, 23, 16,   CSR_RW, 0x00000000 },
  { "DOSD094", 0x00000178, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd095
  { "layer_bb_osd_4_line_width", 0x0000017C,  3,  0,   CSR_RW, 0x00000002 },
  { "layer_bb_osd_4_line_alpha", 0x0000017C, 23, 16,   CSR_RW, 0x00000000 },
  { "DOSD095", 0x0000017C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd101
  { "layer_bb_osd_5_enable", 0x00000194,  0,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_5_mask", 0x00000194, 16, 16,   CSR_RW, 0x00000000 },
  { "DOSD101", 0x00000194, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd102
  { "layer_bb_osd_5_start_x", 0x00000198, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_5_start_y", 0x00000198, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD102", 0x00000198, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd103
  { "layer_bb_osd_5_end_x", 0x0000019C, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_5_end_y", 0x0000019C, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD103", 0x0000019C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd104
  { "layer_bb_osd_5_y", 0x000001A0,  7,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_5_u", 0x000001A0, 15,  8,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_5_v", 0x000001A0, 23, 16,   CSR_RW, 0x00000000 },
  { "DOSD104", 0x000001A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd105
  { "layer_bb_osd_5_line_width", 0x000001A4,  3,  0,   CSR_RW, 0x00000002 },
  { "layer_bb_osd_5_line_alpha", 0x000001A4, 23, 16,   CSR_RW, 0x00000000 },
  { "DOSD105", 0x000001A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd111
  { "layer_bb_osd_6_enable", 0x000001BC,  0,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_6_mask", 0x000001BC, 16, 16,   CSR_RW, 0x00000000 },
  { "DOSD111", 0x000001BC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd112
  { "layer_bb_osd_6_start_x", 0x000001C0, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_6_start_y", 0x000001C0, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD112", 0x000001C0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd113
  { "layer_bb_osd_6_end_x", 0x000001C4, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_6_end_y", 0x000001C4, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD113", 0x000001C4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd114
  { "layer_bb_osd_6_y", 0x000001C8,  7,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_6_u", 0x000001C8, 15,  8,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_6_v", 0x000001C8, 23, 16,   CSR_RW, 0x00000000 },
  { "DOSD114", 0x000001C8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd115
  { "layer_bb_osd_6_line_width", 0x000001CC,  3,  0,   CSR_RW, 0x00000002 },
  { "layer_bb_osd_6_line_alpha", 0x000001CC, 23, 16,   CSR_RW, 0x00000000 },
  { "DOSD115", 0x000001CC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd121
  { "layer_bb_osd_7_enable", 0x000001E4,  0,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_7_mask", 0x000001E4, 16, 16,   CSR_RW, 0x00000000 },
  { "DOSD121", 0x000001E4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd122
  { "layer_bb_osd_7_start_x", 0x000001E8, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_7_start_y", 0x000001E8, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD122", 0x000001E8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd123
  { "layer_bb_osd_7_end_x", 0x000001EC, 12,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_7_end_y", 0x000001EC, 28, 16,   CSR_RW, 0x00000000 },
  { "DOSD123", 0x000001EC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd124
  { "layer_bb_osd_7_y", 0x000001F0,  7,  0,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_7_u", 0x000001F0, 15,  8,   CSR_RW, 0x00000000 },
  { "layer_bb_osd_7_v", 0x000001F0, 23, 16,   CSR_RW, 0x00000000 },
  { "DOSD124", 0x000001F0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd125
  { "layer_bb_osd_7_line_width", 0x000001F4,  3,  0,   CSR_RW, 0x00000002 },
  { "layer_bb_osd_7_line_alpha", 0x000001F4, 23, 16,   CSR_RW, 0x00000000 },
  { "DOSD125", 0x000001F4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd131
  { "wildcard_0_valid", 0x0000020C,  0,  0,   CSR_RW, 0x00000000 },
  { "wildcard_0_y", 0x0000020C, 15,  8,   CSR_RW, 0x00000010 },
  { "wildcard_0_u", 0x0000020C, 23, 16,   CSR_RW, 0x00000080 },
  { "wildcard_0_v", 0x0000020C, 31, 24,   CSR_RW, 0x00000080 },
  { "DOSD131", 0x0000020C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd132
  { "wildcard_1_valid", 0x00000210,  0,  0,   CSR_RW, 0x00000000 },
  { "wildcard_1_y", 0x00000210, 15,  8,   CSR_RW, 0x00000010 },
  { "wildcard_1_u", 0x00000210, 23, 16,   CSR_RW, 0x00000080 },
  { "wildcard_1_v", 0x00000210, 31, 24,   CSR_RW, 0x00000080 },
  { "DOSD132", 0x00000210, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd133
  { "wildcard_2_valid", 0x00000214,  0,  0,   CSR_RW, 0x00000000 },
  { "wildcard_2_y", 0x00000214, 15,  8,   CSR_RW, 0x00000010 },
  { "wildcard_2_u", 0x00000214, 23, 16,   CSR_RW, 0x00000080 },
  { "wildcard_2_v", 0x00000214, 31, 24,   CSR_RW, 0x00000080 },
  { "DOSD133", 0x00000214, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd134
  { "wildcard_3_valid", 0x00000218,  0,  0,   CSR_RW, 0x00000000 },
  { "wildcard_3_y", 0x00000218, 15,  8,   CSR_RW, 0x00000010 },
  { "wildcard_3_u", 0x00000218, 23, 16,   CSR_RW, 0x00000080 },
  { "wildcard_3_v", 0x00000218, 31, 24,   CSR_RW, 0x00000080 },
  { "DOSD134", 0x00000218, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd135
  { "wildcard_4_valid", 0x0000021C,  0,  0,   CSR_RW, 0x00000000 },
  { "wildcard_4_y", 0x0000021C, 15,  8,   CSR_RW, 0x00000010 },
  { "wildcard_4_u", 0x0000021C, 23, 16,   CSR_RW, 0x00000080 },
  { "wildcard_4_v", 0x0000021C, 31, 24,   CSR_RW, 0x00000080 },
  { "DOSD135", 0x0000021C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd136
  { "wildcard_5_valid", 0x00000220,  0,  0,   CSR_RW, 0x00000000 },
  { "wildcard_5_y", 0x00000220, 15,  8,   CSR_RW, 0x00000010 },
  { "wildcard_5_u", 0x00000220, 23, 16,   CSR_RW, 0x00000080 },
  { "wildcard_5_v", 0x00000220, 31, 24,   CSR_RW, 0x00000080 },
  { "DOSD136", 0x00000220, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd137
  { "wildcard_6_valid", 0x00000224,  0,  0,   CSR_RW, 0x00000000 },
  { "wildcard_6_y", 0x00000224, 15,  8,   CSR_RW, 0x00000010 },
  { "wildcard_6_u", 0x00000224, 23, 16,   CSR_RW, 0x00000080 },
  { "wildcard_6_v", 0x00000224, 31, 24,   CSR_RW, 0x00000080 },
  { "DOSD137", 0x00000224, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd138
  { "wildcard_7_valid", 0x00000228,  0,  0,   CSR_RW, 0x00000000 },
  { "wildcard_7_y", 0x00000228, 15,  8,   CSR_RW, 0x00000010 },
  { "wildcard_7_u", 0x00000228, 23, 16,   CSR_RW, 0x00000080 },
  { "wildcard_7_v", 0x00000228, 31, 24,   CSR_RW, 0x00000080 },
  { "DOSD138", 0x00000228, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd139
  { "reserved_0", 0x0000022C, 31,  0,   CSR_RW, 0x00000000 },
  { "DOSD139", 0x0000022C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd140
  { "reserved_1", 0x00000230, 31,  0,   CSR_RW, 0x00000000 },
  { "DOSD140", 0x00000230, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd141
  { "debug_mon_sel", 0x00000234,  1,  0,   CSR_RW, 0x00000000 },
  { "DOSD141", 0x00000234, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_DOSD_H_
