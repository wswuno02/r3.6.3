#ifndef CSR_TABLE_ISP_CHECKSUM_H_
#define CSR_TABLE_ISP_CHECKSUM_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_isp_checksum[] =
{
  // WORD clr
  { "clear", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "CLR", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD ispin0
  { "checksum_ispin0", 0x00000004, 31,  0,   CSR_RO, 0x00000000 },
  { "ISPIN0", 0x00000004, 31, 0,   CSR_RO, 0x00000000 },
  // WORD ispin1
  { "checksum_ispin1", 0x00000008, 31,  0,   CSR_RO, 0x00000000 },
  { "ISPIN1", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // WORD dpchdr
  { "checksum_dpchdr", 0x0000000C, 31,  0,   CSR_RO, 0x00000000 },
  { "DPCHDR", 0x0000000C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD rgbp
  { "checksum_rgbp", 0x00000010, 31,  0,   CSR_RO, 0x00000000 },
  { "RGBP", 0x00000010, 31, 0,   CSR_RO, 0x00000000 },
  // WORD sc
  { "checksum_sc", 0x00000014, 31,  0,   CSR_RO, 0x00000000 },
  { "SC", 0x00000014, 31, 0,   CSR_RO, 0x00000000 },
  // WORD enh
  { "checksum_enh", 0x00000018, 31,  0,   CSR_RO, 0x00000000 },
  { "ENH", 0x00000018, 31, 0,   CSR_RO, 0x00000000 },
  // WORD dhz
  { "checksum_dhz", 0x0000001C, 31,  0,   CSR_RO, 0x00000000 },
  { "DHZ", 0x0000001C, 31, 0,   CSR_RO, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_ISP_CHECKSUM_H_
