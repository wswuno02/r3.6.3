#ifndef CSR_TABLE_AUDIOW_H_
#define CSR_TABLE_AUDIOW_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_audiow[] =
{
  // WORD lw332_00
  { "start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "buffer_switch", 0x00000000,  8,  8,  CSR_W1P, 0x00000000 },
  { "write_end_req", 0x00000000, 16, 16,  CSR_W1P, 0x00000000 },
  { "LW332_00", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD lw332_01
  { "access_illegal_hang", 0x00000004,  0,  0,   CSR_RW, 0x00000001 },
  { "access_illegal_mask", 0x00000004,  8,  8,   CSR_RW, 0x00000001 },
  { "LW332_01", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lw332_04
  { "col_addr_type", 0x00000010,  9,  8,   CSR_RW, 0x00000000 },
  { "debug_mon_sel", 0x00000010, 25, 24,   CSR_RW, 0x00000000 },
  { "LW332_04", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lw332_07
  { "target_burst_len", 0x0000001C,  4,  0,   CSR_RW, 0x00000008 },
  { "access_end_sel", 0x0000001C,  8,  8,   CSR_RW, 0x00000001 },
  { "bank_addr_type", 0x0000001C, 16, 16,   CSR_RW, 0x00000001 },
  { "LW332_07", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lw332_08
  { "target_fifo_level", 0x00000020,  5,  0,   CSR_RW, 0x00000010 },
  { "fifo_full_level", 0x00000020, 21, 16,   CSR_RW, 0x00000020 },
  { "LW332_08", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lw332_09
  { "access_length", 0x00000024, 15,  0,   CSR_RW, 0x00000000 },
  { "LW332_09", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lw332_10
  { "start_addr", 0x00000028, 27,  0,   CSR_RW, 0x00000000 },
  { "LW332_10", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lw332_11
  { "end_addr", 0x0000002C, 27,  0,   CSR_RW, 0x0FFFFFFF },
  { "LW332_11", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lw332_13
  { "reserved", 0x00000034, 31,  0,   CSR_RW, 0x00000000 },
  { "LW332_13", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lw332_14
  { "ini_addr_linear", 0x00000038, 27,  0,   CSR_RW, 0x00000000 },
  { "LW332_14", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lw332_15
  { "sample_count", 0x0000003C, 15,  0,   CSR_RO, 0x00000000 },
  { "LW332_15", 0x0000003C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD lw332_16
  { "time_stamp_clear", 0x00000040,  0,  0,  CSR_W1P, 0x00000000 },
  { "irq_clear_buffer_full_pulse_avail", 0x00000040,  8,  8,  CSR_W1P, 0x00000000 },
  { "LW332_16", 0x00000040, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD lw332_17
  { "sample_target", 0x00000044, 16,  0,   CSR_RW, 0x00000000 },
  { "LW332_17", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lw332_18
  { "capture_time_stamp", 0x00000048, 31,  0,   CSR_RO, 0x00000000 },
  { "LW332_18", 0x00000048, 31, 0,   CSR_RO, 0x00000000 },
  // WORD lw332_19
  { "irq_mask_buffer_full_pulse_avail", 0x0000004C,  0,  0,   CSR_RW, 0x00000000 },
  { "LW332_19", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lw332_20
  { "status_buffer_full_pulse_avail", 0x00000050,  0,  0,   CSR_RO, 0x00000000 },
  { "LW332_20", 0x00000050, 31, 0,   CSR_RO, 0x00000000 },
  // WORD lw332_21
  { "scaler_factor", 0x00000054,  3,  0,   CSR_RW, 0x00000003 },
  { "LW332_21", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lw332_22
  { "capture_time_stamp_last", 0x00000058, 31,  0,   CSR_RO, 0x00000000 },
  { "LW332_22", 0x00000058, 31, 0,   CSR_RO, 0x00000000 },
  // WORD lw332_23
  { "time_stamp_enable", 0x0000005C,  0,  0,   CSR_RW, 0x00000000 },
  { "LW332_23", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lw332_24
  { "time_stamp_clear_value", 0x00000060, 31,  0,   CSR_RW, 0x00000000 },
  { "LW332_24", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lw332_25
  { "capture_sample_cnt", 0x00000064, 16,  0,   CSR_RO, 0x00000000 },
  { "LW332_25", 0x00000064, 31, 0,   CSR_RO, 0x00000000 },
  // WORD lw332_26
  { "irq_clear_fifo_empty", 0x00000068,  0,  0,  CSR_W1P, 0x00000000 },
  { "LW332_26", 0x00000068, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD lw332_27
  { "status_fifo_empty", 0x0000006C,  0,  0,   CSR_RO, 0x00000000 },
  { "LW332_27", 0x0000006C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD lw332_28
  { "irq_mask_fifo_empty", 0x00000070,  0,  0,   CSR_RW, 0x00000001 },
  { "LW332_28", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_AUDIOW_H_
