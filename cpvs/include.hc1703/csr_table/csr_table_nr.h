#ifndef CSR_TABLE_NR_H_
#define CSR_TABLE_NR_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_nr[] =
{
  // WORD word_mode
  { "mode", 0x00000010,  2,  0,   CSR_RW, 0x00000000 },
  { "WORD_MODE", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD fir_mode
  { "fir_kernel_sel_y", 0x00000014,  0,  0,   CSR_RW, 0x00000000 },
  { "fir_weight_y", 0x00000014, 12,  8,   CSR_RW, 0x00000008 },
  { "fir_weight_c", 0x00000014, 20, 16,   CSR_RW, 0x0000000C },
  { "FIR_MODE", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD iir_mode
  { "iir_y_conf_sel", 0x00000020,  1,  0,   CSR_RW, 0x00000000 },
  { "iir_alpha_y_gain", 0x00000020, 10,  8,   CSR_RW, 0x00000003 },
  { "iir_alpha_c_gain", 0x00000020, 18, 16,   CSR_RW, 0x00000002 },
  { "IIR_MODE", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_y_lut_mode
  { "ma_y_lut_step", 0x0000003C,  1,  0,   CSR_RW, 0x00000000 },
  { "MA_Y_LUT_MODE", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_y_lut_i_0_3
  { "ma_y_lut_i_0", 0x00000040,  7,  0,   CSR_RW, 0x00000000 },
  { "ma_y_lut_i_1", 0x00000040, 15,  8,   CSR_RW, 0x0000002D },
  { "ma_y_lut_i_2", 0x00000040, 23, 16,   CSR_RW, 0x00000033 },
  { "ma_y_lut_i_3", 0x00000040, 31, 24,   CSR_RW, 0x0000003B },
  { "MA_Y_LUT_I_0_3", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_y_lut_i_4_7
  { "ma_y_lut_i_4", 0x00000044,  7,  0,   CSR_RW, 0x0000003E },
  { "ma_y_lut_i_5", 0x00000044, 15,  8,   CSR_RW, 0x00000044 },
  { "ma_y_lut_i_6", 0x00000044, 23, 16,   CSR_RW, 0x00000049 },
  { "ma_y_lut_i_7", 0x00000044, 31, 24,   CSR_RW, 0x00000052 },
  { "MA_Y_LUT_I_4_7", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_y_lut_i_8_8
  { "ma_y_lut_i_8", 0x00000048,  7,  0,   CSR_RW, 0x0000005B },
  { "MA_Y_LUT_I_8_8", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_y_lut_o_0_3
  { "ma_y_lut_o_0", 0x0000004C,  5,  0,   CSR_RW, 0x0000001C },
  { "ma_y_lut_o_1", 0x0000004C, 13,  8,   CSR_RW, 0x0000001A },
  { "ma_y_lut_o_2", 0x0000004C, 21, 16,   CSR_RW, 0x00000018 },
  { "ma_y_lut_o_3", 0x0000004C, 29, 24,   CSR_RW, 0x00000018 },
  { "MA_Y_LUT_O_0_3", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_y_lut_o_4_7
  { "ma_y_lut_o_4", 0x00000050,  5,  0,   CSR_RW, 0x00000010 },
  { "ma_y_lut_o_5", 0x00000050, 13,  8,   CSR_RW, 0x00000010 },
  { "ma_y_lut_o_6", 0x00000050, 21, 16,   CSR_RW, 0x00000008 },
  { "ma_y_lut_o_7", 0x00000050, 29, 24,   CSR_RW, 0x00000004 },
  { "MA_Y_LUT_O_4_7", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_y_lut_o_8_8
  { "ma_y_lut_o_8", 0x00000054,  5,  0,   CSR_RW, 0x00000000 },
  { "MA_Y_LUT_O_8_8", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_y_lut_slope_0_1
  { "ma_y_lut_slope_0", 0x00000058, 13,  0,   CSR_RW, 0x00000000 },
  { "ma_y_lut_slope_1", 0x00000058, 29, 16,   CSR_RW, 0x00000000 },
  { "MA_Y_LUT_SLOPE_0_1", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_y_lut_slope_2_3
  { "ma_y_lut_slope_2", 0x0000005C, 13,  0,   CSR_RW, 0x00000000 },
  { "ma_y_lut_slope_3", 0x0000005C, 29, 16,   CSR_RW, 0x00000000 },
  { "MA_Y_LUT_SLOPE_2_3", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_y_lut_slope_4_5
  { "ma_y_lut_slope_4", 0x00000060, 13,  0,   CSR_RW, 0x00000000 },
  { "ma_y_lut_slope_5", 0x00000060, 29, 16,   CSR_RW, 0x00000000 },
  { "MA_Y_LUT_SLOPE_4_5", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_y_lut_slope_6_7
  { "ma_y_lut_slope_6", 0x00000064, 13,  0,   CSR_RW, 0x00000000 },
  { "ma_y_lut_slope_7", 0x00000064, 29, 16,   CSR_RW, 0x00000000 },
  { "MA_Y_LUT_SLOPE_6_7", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_y_lp_dyn_gain_0
  { "ma_y_lp_dyn_gain_th_min", 0x00000068,  7,  0,   CSR_RW, 0x0000000A },
  { "ma_y_lp_dyn_gain_th_max", 0x00000068, 15,  8,   CSR_RW, 0x0000004A },
  { "ma_y_lp_dyn_gain_min", 0x00000068, 20, 16,   CSR_RW, 0x00000010 },
  { "ma_y_lp_dyn_gain_max", 0x00000068, 28, 24,   CSR_RW, 0x00000005 },
  { "MA_Y_LP_DYN_GAIN_0", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_y_lp_dyn_gain_1
  { "ma_y_lp_dyn_gain_slope", 0x0000006C, 13,  0,   CSR_RW, 0x00000000 },
  { "MA_Y_LP_DYN_GAIN_1", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_y_lp_diff_gain
  { "ma_y_lp_diff_gain_th_min", 0x00000070,  7,  0,   CSR_RW, 0x00000004 },
  { "ma_y_lp_diff_gain_th_range", 0x00000070, 11,  8,   CSR_RW, 0x00000003 },
  { "ma_y_lp_diff_gain_min", 0x00000070, 20, 16,   CSR_RW, 0x00000010 },
  { "ma_y_lp_diff_gain_max", 0x00000070, 28, 24,   CSR_RW, 0x00000001 },
  { "MA_Y_LP_DIFF_GAIN", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mc_y_lut_mode
  { "mc_y_lut_step", 0x00000078,  1,  0,   CSR_RW, 0x00000000 },
  { "MC_Y_LUT_MODE", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mc_y_lut_i_0_3
  { "mc_y_lut_i_0", 0x0000007C,  7,  0,   CSR_RW, 0x00000000 },
  { "mc_y_lut_i_1", 0x0000007C, 15,  8,   CSR_RW, 0x0000002D },
  { "mc_y_lut_i_2", 0x0000007C, 23, 16,   CSR_RW, 0x00000033 },
  { "mc_y_lut_i_3", 0x0000007C, 31, 24,   CSR_RW, 0x0000003B },
  { "MC_Y_LUT_I_0_3", 0x0000007C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mc_y_lut_i_4_7
  { "mc_y_lut_i_4", 0x00000080,  7,  0,   CSR_RW, 0x0000003E },
  { "mc_y_lut_i_5", 0x00000080, 15,  8,   CSR_RW, 0x00000044 },
  { "mc_y_lut_i_6", 0x00000080, 23, 16,   CSR_RW, 0x00000049 },
  { "mc_y_lut_i_7", 0x00000080, 31, 24,   CSR_RW, 0x00000052 },
  { "MC_Y_LUT_I_4_7", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mc_y_lut_i_8_8
  { "mc_y_lut_i_8", 0x00000084,  7,  0,   CSR_RW, 0x0000005B },
  { "MC_Y_LUT_I_8_8", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mc_y_lut_o_0_3
  { "mc_y_lut_o_0", 0x00000088,  5,  0,   CSR_RW, 0x0000001C },
  { "mc_y_lut_o_1", 0x00000088, 13,  8,   CSR_RW, 0x0000001A },
  { "mc_y_lut_o_2", 0x00000088, 21, 16,   CSR_RW, 0x00000018 },
  { "mc_y_lut_o_3", 0x00000088, 29, 24,   CSR_RW, 0x00000018 },
  { "MC_Y_LUT_O_0_3", 0x00000088, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mc_y_lut_o_4_7
  { "mc_y_lut_o_4", 0x0000008C,  5,  0,   CSR_RW, 0x00000010 },
  { "mc_y_lut_o_5", 0x0000008C, 13,  8,   CSR_RW, 0x00000010 },
  { "mc_y_lut_o_6", 0x0000008C, 21, 16,   CSR_RW, 0x00000008 },
  { "mc_y_lut_o_7", 0x0000008C, 29, 24,   CSR_RW, 0x00000004 },
  { "MC_Y_LUT_O_4_7", 0x0000008C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mc_y_lut_o_8_8
  { "mc_y_lut_o_8", 0x00000090,  5,  0,   CSR_RW, 0x00000000 },
  { "MC_Y_LUT_O_8_8", 0x00000090, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mc_y_lut_slope_0_1
  { "mc_y_lut_slope_0", 0x00000094, 13,  0,   CSR_RW, 0x00000000 },
  { "mc_y_lut_slope_1", 0x00000094, 29, 16,   CSR_RW, 0x00000000 },
  { "MC_Y_LUT_SLOPE_0_1", 0x00000094, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mc_y_lut_slope_2_3
  { "mc_y_lut_slope_2", 0x00000098, 13,  0,   CSR_RW, 0x00000000 },
  { "mc_y_lut_slope_3", 0x00000098, 29, 16,   CSR_RW, 0x00000000 },
  { "MC_Y_LUT_SLOPE_2_3", 0x00000098, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mc_y_lut_slope_4_5
  { "mc_y_lut_slope_4", 0x0000009C, 13,  0,   CSR_RW, 0x00000000 },
  { "mc_y_lut_slope_5", 0x0000009C, 29, 16,   CSR_RW, 0x00000000 },
  { "MC_Y_LUT_SLOPE_4_5", 0x0000009C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mc_y_lut_slope_6_7
  { "mc_y_lut_slope_6", 0x000000A0, 13,  0,   CSR_RW, 0x00000000 },
  { "mc_y_lut_slope_7", 0x000000A0, 29, 16,   CSR_RW, 0x00000000 },
  { "MC_Y_LUT_SLOPE_6_7", 0x000000A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mc_y_lp_dyn_gain_0
  { "mc_y_lp_dyn_gain_th_min", 0x000000A4,  7,  0,   CSR_RW, 0x0000000A },
  { "mc_y_lp_dyn_gain_th_max", 0x000000A4, 15,  8,   CSR_RW, 0x0000004A },
  { "mc_y_lp_dyn_gain_min", 0x000000A4, 20, 16,   CSR_RW, 0x00000010 },
  { "mc_y_lp_dyn_gain_max", 0x000000A4, 28, 24,   CSR_RW, 0x00000005 },
  { "MC_Y_LP_DYN_GAIN_0", 0x000000A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mc_y_lp_dyn_gain_1
  { "mc_y_lp_dyn_gain_slope", 0x000000A8, 13,  0,   CSR_RW, 0x00000000 },
  { "MC_Y_LP_DYN_GAIN_1", 0x000000A8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mc_y_lp_diff_gain
  { "mc_y_lp_diff_gain_th_min", 0x000000AC,  7,  0,   CSR_RW, 0x00000004 },
  { "mc_y_lp_diff_gain_th_range", 0x000000AC, 11,  8,   CSR_RW, 0x00000003 },
  { "mc_y_lp_diff_gain_min", 0x000000AC, 20, 16,   CSR_RW, 0x00000010 },
  { "mc_y_lp_diff_gain_max", 0x000000AC, 28, 24,   CSR_RW, 0x00000001 },
  { "MC_Y_LP_DIFF_GAIN", 0x000000AC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mc_y_ratio
  { "mc_y_base_ratio", 0x000000B0,  3,  0,   CSR_RW, 0x00000003 },
  { "mc_y_base_dyn", 0x000000B0, 11,  8,   CSR_RW, 0x00000004 },
  { "MC_Y_RATIO", 0x000000B0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_c_lut_mode
  { "ma_c_lut_step", 0x000000B4,  1,  0,   CSR_RW, 0x00000000 },
  { "MA_C_LUT_MODE", 0x000000B4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_c_lut_i_0_3
  { "ma_c_lut_i_0", 0x000000B8,  7,  0,   CSR_RW, 0x00000000 },
  { "ma_c_lut_i_1", 0x000000B8, 15,  8,   CSR_RW, 0x0000002D },
  { "ma_c_lut_i_2", 0x000000B8, 23, 16,   CSR_RW, 0x00000033 },
  { "ma_c_lut_i_3", 0x000000B8, 31, 24,   CSR_RW, 0x0000003B },
  { "MA_C_LUT_I_0_3", 0x000000B8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_c_lut_i_4_7
  { "ma_c_lut_i_4", 0x000000BC,  7,  0,   CSR_RW, 0x0000003E },
  { "ma_c_lut_i_5", 0x000000BC, 15,  8,   CSR_RW, 0x00000044 },
  { "ma_c_lut_i_6", 0x000000BC, 23, 16,   CSR_RW, 0x00000049 },
  { "ma_c_lut_i_7", 0x000000BC, 31, 24,   CSR_RW, 0x00000052 },
  { "MA_C_LUT_I_4_7", 0x000000BC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_c_lut_i_8_8
  { "ma_c_lut_i_8", 0x000000C0,  7,  0,   CSR_RW, 0x0000005B },
  { "MA_C_LUT_I_8_8", 0x000000C0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_c_lut_o_0_3
  { "ma_c_lut_o_0", 0x000000C4,  5,  0,   CSR_RW, 0x0000001C },
  { "ma_c_lut_o_1", 0x000000C4, 13,  8,   CSR_RW, 0x0000001A },
  { "ma_c_lut_o_2", 0x000000C4, 21, 16,   CSR_RW, 0x00000018 },
  { "ma_c_lut_o_3", 0x000000C4, 29, 24,   CSR_RW, 0x00000018 },
  { "MA_C_LUT_O_0_3", 0x000000C4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_c_lut_o_4_7
  { "ma_c_lut_o_4", 0x000000C8,  5,  0,   CSR_RW, 0x00000010 },
  { "ma_c_lut_o_5", 0x000000C8, 13,  8,   CSR_RW, 0x00000010 },
  { "ma_c_lut_o_6", 0x000000C8, 21, 16,   CSR_RW, 0x00000008 },
  { "ma_c_lut_o_7", 0x000000C8, 29, 24,   CSR_RW, 0x00000004 },
  { "MA_C_LUT_O_4_7", 0x000000C8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_c_lut_o_8_8
  { "ma_c_lut_o_8", 0x000000CC,  5,  0,   CSR_RW, 0x00000000 },
  { "MA_C_LUT_O_8_8", 0x000000CC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_c_lut_slope_0_1
  { "ma_c_lut_slope_0", 0x000000D0, 13,  0,   CSR_RW, 0x00000000 },
  { "ma_c_lut_slope_1", 0x000000D0, 29, 16,   CSR_RW, 0x00000000 },
  { "MA_C_LUT_SLOPE_0_1", 0x000000D0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_c_lut_slope_2_3
  { "ma_c_lut_slope_2", 0x000000D4, 13,  0,   CSR_RW, 0x00000000 },
  { "ma_c_lut_slope_3", 0x000000D4, 29, 16,   CSR_RW, 0x00000000 },
  { "MA_C_LUT_SLOPE_2_3", 0x000000D4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_c_lut_slope_4_5
  { "ma_c_lut_slope_4", 0x000000D8, 13,  0,   CSR_RW, 0x00000000 },
  { "ma_c_lut_slope_5", 0x000000D8, 29, 16,   CSR_RW, 0x00000000 },
  { "MA_C_LUT_SLOPE_4_5", 0x000000D8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_c_lut_slope_6_7
  { "ma_c_lut_slope_6", 0x000000DC, 13,  0,   CSR_RW, 0x00000000 },
  { "ma_c_lut_slope_7", 0x000000DC, 29, 16,   CSR_RW, 0x00000000 },
  { "MA_C_LUT_SLOPE_6_7", 0x000000DC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_c_y_ref_sel
  { "ma_c_weight_y_ref_sel", 0x000000E0,  1,  0,   CSR_RW, 0x00000000 },
  { "MA_C_Y_REF_SEL", 0x000000E0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_c_y_gain_0
  { "ma_c_weight_y_gain_th_min", 0x000000E4,  5,  0,   CSR_RW, 0x00000020 },
  { "ma_c_weight_y_gain_th_max", 0x000000E4, 13,  8,   CSR_RW, 0x00000000 },
  { "ma_c_weight_y_gain_min", 0x000000E4, 21, 16,   CSR_RW, 0x00000020 },
  { "ma_c_weight_y_gain_max", 0x000000E4, 29, 24,   CSR_RW, 0x00000020 },
  { "MA_C_Y_GAIN_0", 0x000000E4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ma_c_y_gain_1
  { "ma_c_weight_y_gain_slope", 0x000000E8, 12,  0,   CSR_RW, 0x00000000 },
  { "MA_C_Y_GAIN_1", 0x000000E8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_abs_diff_hist_en
  { "abs_diff_hist_en", 0x00000104,  0,  0,   CSR_RW, 0x00000000 },
  { "abs_diff_hist_clear", 0x00000104, 16, 16,   CSR_RW, 0x00000000 },
  { "WORD_ABS_DIFF_HIST_EN", 0x00000104, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_abs_diff_hist_mode
  { "abs_diff_hist_sel", 0x00000108,  1,  0,   CSR_RW, 0x00000000 },
  { "abs_diff_hist_mode", 0x00000108,  8,  8,   CSR_RW, 0x00000000 },
  { "abs_diff_hist_step", 0x00000108, 18, 16,   CSR_RW, 0x00000000 },
  { "WORD_ABS_DIFF_HIST_MODE", 0x00000108, 31, 0,   CSR_RW, 0x00000000 },
  // WORD abs_diff_hist_sxsy
  { "abs_diff_hist_sx", 0x0000010C, 15,  0,   CSR_RW, 0x00000000 },
  { "abs_diff_hist_sy", 0x0000010C, 31, 16,   CSR_RW, 0x00000000 },
  { "ABS_DIFF_HIST_SXSY", 0x0000010C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD abs_diff_hist_exey
  { "abs_diff_hist_ex", 0x00000110, 15,  0,   CSR_RW, 0x00000000 },
  { "abs_diff_hist_ey", 0x00000110, 31, 16,   CSR_RW, 0x00000000 },
  { "ABS_DIFF_HIST_EXEY", 0x00000110, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_abs_diff_hist_overflow
  { "abs_diff_hist_overflow", 0x0000011C,  0,  0,   CSR_RO, 0x00000000 },
  { "WORD_ABS_DIFF_HIST_OVERFLOW", 0x0000011C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_abs_diff_hist_0
  { "abs_diff_hist_0", 0x00000120, 24,  0,   CSR_RO, 0x00000000 },
  { "WORD_ABS_DIFF_HIST_0", 0x00000120, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_abs_diff_hist_1
  { "abs_diff_hist_1", 0x00000124, 24,  0,   CSR_RO, 0x00000000 },
  { "WORD_ABS_DIFF_HIST_1", 0x00000124, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_abs_diff_hist_2
  { "abs_diff_hist_2", 0x00000128, 24,  0,   CSR_RO, 0x00000000 },
  { "WORD_ABS_DIFF_HIST_2", 0x00000128, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_abs_diff_hist_3
  { "abs_diff_hist_3", 0x0000012C, 24,  0,   CSR_RO, 0x00000000 },
  { "WORD_ABS_DIFF_HIST_3", 0x0000012C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_abs_diff_hist_4
  { "abs_diff_hist_4", 0x00000130, 24,  0,   CSR_RO, 0x00000000 },
  { "WORD_ABS_DIFF_HIST_4", 0x00000130, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_abs_diff_hist_5
  { "abs_diff_hist_5", 0x00000134, 24,  0,   CSR_RO, 0x00000000 },
  { "WORD_ABS_DIFF_HIST_5", 0x00000134, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_abs_diff_hist_6
  { "abs_diff_hist_6", 0x00000138, 24,  0,   CSR_RO, 0x00000000 },
  { "WORD_ABS_DIFF_HIST_6", 0x00000138, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_abs_diff_hist_7
  { "abs_diff_hist_7", 0x0000013C, 24,  0,   CSR_RO, 0x00000000 },
  { "WORD_ABS_DIFF_HIST_7", 0x0000013C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_abs_diff_hist_8
  { "abs_diff_hist_8", 0x00000140, 24,  0,   CSR_RO, 0x00000000 },
  { "WORD_ABS_DIFF_HIST_8", 0x00000140, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_abs_diff_hist_9
  { "abs_diff_hist_9", 0x00000144, 24,  0,   CSR_RO, 0x00000000 },
  { "WORD_ABS_DIFF_HIST_9", 0x00000144, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_abs_diff_hist_10
  { "abs_diff_hist_10", 0x00000148, 24,  0,   CSR_RO, 0x00000000 },
  { "WORD_ABS_DIFF_HIST_10", 0x00000148, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_abs_diff_hist_11
  { "abs_diff_hist_11", 0x0000014C, 24,  0,   CSR_RO, 0x00000000 },
  { "WORD_ABS_DIFF_HIST_11", 0x0000014C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_abs_diff_hist_12
  { "abs_diff_hist_12", 0x00000150, 24,  0,   CSR_RO, 0x00000000 },
  { "WORD_ABS_DIFF_HIST_12", 0x00000150, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_abs_diff_hist_13
  { "abs_diff_hist_13", 0x00000154, 24,  0,   CSR_RO, 0x00000000 },
  { "WORD_ABS_DIFF_HIST_13", 0x00000154, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_abs_diff_hist_14
  { "abs_diff_hist_14", 0x00000158, 24,  0,   CSR_RO, 0x00000000 },
  { "WORD_ABS_DIFF_HIST_14", 0x00000158, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_abs_diff_hist_15
  { "abs_diff_hist_15", 0x0000015C, 24,  0,   CSR_RO, 0x00000000 },
  { "WORD_ABS_DIFF_HIST_15", 0x0000015C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_abs_diff_hist_16
  { "abs_diff_hist_16", 0x00000160, 24,  0,   CSR_RO, 0x00000000 },
  { "WORD_ABS_DIFF_HIST_16", 0x00000160, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_abs_diff_hist_17
  { "abs_diff_hist_17", 0x00000164, 24,  0,   CSR_RO, 0x00000000 },
  { "WORD_ABS_DIFF_HIST_17", 0x00000164, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_abs_diff_hist_18
  { "abs_diff_hist_18", 0x00000168, 24,  0,   CSR_RO, 0x00000000 },
  { "WORD_ABS_DIFF_HIST_18", 0x00000168, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_tdiff_roi_0_en
  { "tdiff_roi_0_en", 0x0000016C,  0,  0,   CSR_RW, 0x00000000 },
  { "tdiff_roi_0_clear", 0x0000016C,  8,  8,   CSR_RW, 0x00000000 },
  { "WORD_TDIFF_ROI_0_EN", 0x0000016C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tdiff_roi_0_sxsy
  { "tdiff_roi_0_sx", 0x00000170, 15,  0,   CSR_RW, 0x00000000 },
  { "tdiff_roi_0_sy", 0x00000170, 31, 16,   CSR_RW, 0x00000000 },
  { "TDIFF_ROI_0_SXSY", 0x00000170, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tdiff_roi_0_exey
  { "tdiff_roi_0_ex", 0x00000174, 15,  0,   CSR_RW, 0x00000000 },
  { "tdiff_roi_0_ey", 0x00000174, 31, 16,   CSR_RW, 0x00000000 },
  { "TDIFF_ROI_0_EXEY", 0x00000174, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tdiff_roi_0_th_y
  { "tdiff_roi_0_acc_th_y", 0x00000178, 24,  0,   CSR_RW, 0x00000000 },
  { "TDIFF_ROI_0_TH_Y", 0x00000178, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tdiff_roi_0_th_c
  { "tdiff_roi_0_acc_th_c", 0x0000017C, 24,  0,   CSR_RW, 0x00000000 },
  { "TDIFF_ROI_0_TH_C", 0x0000017C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tdiff_roi_0_stat
  { "tdiff_roi_0_avg_y", 0x00000180, 15,  0,   CSR_RO, 0x00000000 },
  { "tdiff_roi_0_avg_c", 0x00000180, 31, 16,   CSR_RO, 0x00000000 },
  { "TDIFF_ROI_0_STAT", 0x00000180, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_tdiff_roi_1_en
  { "tdiff_roi_1_en", 0x00000184,  0,  0,   CSR_RW, 0x00000000 },
  { "tdiff_roi_1_clear", 0x00000184,  8,  8,   CSR_RW, 0x00000000 },
  { "WORD_TDIFF_ROI_1_EN", 0x00000184, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tdiff_roi_1_sxsy
  { "tdiff_roi_1_sx", 0x00000188, 15,  0,   CSR_RW, 0x00000000 },
  { "tdiff_roi_1_sy", 0x00000188, 31, 16,   CSR_RW, 0x00000000 },
  { "TDIFF_ROI_1_SXSY", 0x00000188, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tdiff_roi_1_exey
  { "tdiff_roi_1_ex", 0x0000018C, 15,  0,   CSR_RW, 0x00000000 },
  { "tdiff_roi_1_ey", 0x0000018C, 31, 16,   CSR_RW, 0x00000000 },
  { "TDIFF_ROI_1_EXEY", 0x0000018C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tdiff_roi_1_th_y
  { "tdiff_roi_1_acc_th_y", 0x00000190, 24,  0,   CSR_RW, 0x00000000 },
  { "TDIFF_ROI_1_TH_Y", 0x00000190, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tdiff_roi_1_th_c
  { "tdiff_roi_1_acc_th_c", 0x00000194, 24,  0,   CSR_RW, 0x00000000 },
  { "TDIFF_ROI_1_TH_C", 0x00000194, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tdiff_roi_1_stat
  { "tdiff_roi_1_avg_y", 0x00000198, 15,  0,   CSR_RO, 0x00000000 },
  { "tdiff_roi_1_avg_c", 0x00000198, 31, 16,   CSR_RO, 0x00000000 },
  { "TDIFF_ROI_1_STAT", 0x00000198, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_tdiff_roi_2_en
  { "tdiff_roi_2_en", 0x0000019C,  0,  0,   CSR_RW, 0x00000000 },
  { "tdiff_roi_2_clear", 0x0000019C,  8,  8,   CSR_RW, 0x00000000 },
  { "WORD_TDIFF_ROI_2_EN", 0x0000019C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tdiff_roi_2_sxsy
  { "tdiff_roi_2_sx", 0x000001A0, 15,  0,   CSR_RW, 0x00000000 },
  { "tdiff_roi_2_sy", 0x000001A0, 31, 16,   CSR_RW, 0x00000000 },
  { "TDIFF_ROI_2_SXSY", 0x000001A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tdiff_roi_2_exey
  { "tdiff_roi_2_ex", 0x000001A4, 15,  0,   CSR_RW, 0x00000000 },
  { "tdiff_roi_2_ey", 0x000001A4, 31, 16,   CSR_RW, 0x00000000 },
  { "TDIFF_ROI_2_EXEY", 0x000001A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tdiff_roi_2_th_y
  { "tdiff_roi_2_acc_th_y", 0x000001A8, 24,  0,   CSR_RW, 0x00000000 },
  { "TDIFF_ROI_2_TH_Y", 0x000001A8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tdiff_roi_2_th_c
  { "tdiff_roi_2_acc_th_c", 0x000001AC, 24,  0,   CSR_RW, 0x00000000 },
  { "TDIFF_ROI_2_TH_C", 0x000001AC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tdiff_roi_2_stat
  { "tdiff_roi_2_avg_y", 0x000001B0, 15,  0,   CSR_RO, 0x00000000 },
  { "tdiff_roi_2_avg_c", 0x000001B0, 31, 16,   CSR_RO, 0x00000000 },
  { "TDIFF_ROI_2_STAT", 0x000001B0, 31, 0,   CSR_RO, 0x00000000 },
  // WORD word_tdiff_roi_3_en
  { "tdiff_roi_3_en", 0x000001B4,  0,  0,   CSR_RW, 0x00000000 },
  { "tdiff_roi_3_clear", 0x000001B4,  8,  8,   CSR_RW, 0x00000000 },
  { "WORD_TDIFF_ROI_3_EN", 0x000001B4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tdiff_roi_3_sxsy
  { "tdiff_roi_3_sx", 0x000001B8, 15,  0,   CSR_RW, 0x00000000 },
  { "tdiff_roi_3_sy", 0x000001B8, 31, 16,   CSR_RW, 0x00000000 },
  { "TDIFF_ROI_3_SXSY", 0x000001B8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tdiff_roi_3_exey
  { "tdiff_roi_3_ex", 0x000001BC, 15,  0,   CSR_RW, 0x00000000 },
  { "tdiff_roi_3_ey", 0x000001BC, 31, 16,   CSR_RW, 0x00000000 },
  { "TDIFF_ROI_3_EXEY", 0x000001BC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tdiff_roi_3_th_y
  { "tdiff_roi_3_acc_th_y", 0x000001C0, 24,  0,   CSR_RW, 0x00000000 },
  { "TDIFF_ROI_3_TH_Y", 0x000001C0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tdiff_roi_3_th_c
  { "tdiff_roi_3_acc_th_c", 0x000001C4, 24,  0,   CSR_RW, 0x00000000 },
  { "TDIFF_ROI_3_TH_C", 0x000001C4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD tdiff_roi_3_stat
  { "tdiff_roi_3_avg_y", 0x000001C8, 15,  0,   CSR_RO, 0x00000000 },
  { "tdiff_roi_3_avg_c", 0x000001C8, 31, 16,   CSR_RO, 0x00000000 },
  { "TDIFF_ROI_3_STAT", 0x000001C8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD demo_roi_mode
  { "demo_en", 0x000001E0,  0,  0,   CSR_RW, 0x00000000 },
  { "demo_mode", 0x000001E0, 18, 16,   CSR_RW, 0x00000000 },
  { "DEMO_ROI_MODE", 0x000001E0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD demo_roi_sxsy
  { "demo_sx", 0x000001E4, 15,  0,   CSR_RW, 0x00000000 },
  { "demo_sy", 0x000001E4, 31, 16,   CSR_RW, 0x00000000 },
  { "DEMO_ROI_SXSY", 0x000001E4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD demo_roi_exey
  { "demo_ex", 0x000001E8, 15,  0,   CSR_RW, 0x00000000 },
  { "demo_ey", 0x000001E8, 31, 16,   CSR_RW, 0x00000000 },
  { "DEMO_ROI_EXEY", 0x000001E8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_ink_mode
  { "ink_en", 0x000001F8,  0,  0,   CSR_RW, 0x00000001 },
  { "ink_mode", 0x000001F8, 20, 16,   CSR_RW, 0x00000000 },
  { "WORD_INK_MODE", 0x000001F8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_reserved_0
  { "reserved_0", 0x00000208, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED_0", 0x00000208, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_reserved_1
  { "reserved_1", 0x0000020C, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED_1", 0x0000020C, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_NR_H_
