#ifndef CSR_TABLE_ISP_CFG_H_
#define CSR_TABLE_ISP_CFG_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_isp_cfg[] =
{
  // WORD conf0_ispr0
  { "cken_ispr0", 0x00000000,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_ispr0", 0x00000000,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_ISPR0", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_ispr0
  { "sw_rst_ispr0", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_ispr0", 0x00000004,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_ISPR0", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_ispr1
  { "cken_ispr1", 0x00000008,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_ispr1", 0x00000008,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_ISPR1", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_ispr1
  { "sw_rst_ispr1", 0x0000000C,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_ispr1", 0x0000000C,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_ISPR1", 0x0000000C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_gfx0
  { "cken_gfx0", 0x00000010,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_gfx0", 0x00000010,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_GFX0", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_gfx0
  { "sw_rst_gfx0", 0x00000014,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_gfx0", 0x00000014,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_GFX0", 0x00000014, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_gfx1
  { "cken_gfx1", 0x00000018,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_gfx1", 0x00000018,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_GFX1", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_gfx1
  { "sw_rst_gfx1", 0x0000001C,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_gfx1", 0x0000001C,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_GFX1", 0x0000001C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_cs0
  { "cken_cs0", 0x00000020,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_cs0", 0x00000020,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_CS0", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_cs0
  { "sw_rst_cs0", 0x00000024,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_cs0", 0x00000024,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_CS0", 0x00000024, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_cs1
  { "cken_cs1", 0x00000028,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_cs1", 0x00000028,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_CS1", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_cs1
  { "sw_rst_cs1", 0x0000002C,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_cs1", 0x0000002C,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_CS1", 0x0000002C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_gfxbld
  { "cken_gfxbld", 0x00000030,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_gfxbld", 0x00000030,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_GFXBLD", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_gfxbld
  { "sw_rst_gfxbld", 0x00000034,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_gfxbld", 0x00000034,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_GFXBLD", 0x00000034, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_dpc
  { "cken_dpc", 0x00000038,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_dpc", 0x00000038,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_DPC", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_dpc
  { "sw_rst_dpc", 0x0000003C,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_dpc", 0x0000003C,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_DPC", 0x0000003C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_hdr
  { "cken_hdr", 0x00000040,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_hdr", 0x00000040,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_HDR", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_hdr
  { "sw_rst_hdr", 0x00000044,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_hdr", 0x00000044,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_HDR", 0x00000044, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_rgbp
  { "cken_rgbp", 0x00000048,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_rgbp", 0x00000048,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_RGBP", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_rgbp
  { "sw_rst_rgbp", 0x0000004C,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_rgbp", 0x0000004C,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_RGBP", 0x0000004C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_sc
  { "cken_sc", 0x00000050,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_sc", 0x00000050,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_SC", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_sc
  { "sw_rst_sc", 0x00000054,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_sc", 0x00000054,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_SC", 0x00000054, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_enh
  { "cken_enh", 0x00000058,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_enh", 0x00000058,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_ENH", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_enh
  { "sw_rst_enh", 0x0000005C,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_enh", 0x0000005C,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF1_ENH", 0x0000005C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf0_ctrl
  { "cken_ctrl", 0x00000060,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_ctrl", 0x00000060,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF0_CTRL", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf1_ctrl
  { "sw_rst_ctrl", 0x00000064,  0,  0,  CSR_W1P, 0x00000000 },
  { "CONF1_CTRL", 0x00000064, 31, 0,  CSR_W1P, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_ISP_CFG_H_
