#ifndef HW_PXW_H
#define HW_PXW_H

#include "csr_bank_pxw.h"
#include "da_utils.h"

#ifndef __KERNEL__
// pxw
void hw_pxw_frame_start(volatile CsrBankPxw *pxw);
void hw_pxw_irq_clear(volatile CsrBankPxw *pxw);
uint32_t hw_pxw_status_frame_end(volatile CsrBankPxw *pxw);
uint32_t hw_pxw_status_bw_insufficient(volatile CsrBankPxw *pxw);
uint32_t hw_pxw_status_access_violation(volatile CsrBankPxw *pxw);
void hw_pxw_irq_mask(volatile CsrBankPxw *pxw, uint32_t frame_end, uint32_t bw_insufficient, uint32_t access_violation);
void hw_pxw_dram_info(volatile CsrBankPxw *pxw, uint32_t col_addr_type, uint32_t bank_interleave_type,
                      uint32_t bank_group_type);
void hw_pxw_access_illegal(volatile CsrBankPxw *pxw, uint32_t hang, uint32_t mask);
void hw_pxw_set_target_burst_len(volatile CsrBankPxw *pxw, uint32_t burst_len);

void hw_pxw_cal_tile_info(PixelAgentConfig *cfg, TileInfo *tile, PaTileInfo *tile_cal);
void hw_pxw_set_addr_new(volatile CsrBankPxw *pxw, uint32_t init_addr, uint32_t add_e, uint32_t add_o);
void hw_pxw_set_tile_csr(volatile CsrBankPxw *pxw, PaTileInfo *tile, uint32_t init_addr);
void hw_pxw_set_init_csr(volatile CsrBankPxw *pxw, PixelAgentConfig *cfg, PaTileInfo *tile);

void hw_pxw_init(volatile CsrBankPxw *pxw);
void hw_pxw_set_pat_0(volatile CsrBankPxw *pxw);
void hw_pxw_set_pat_1(volatile CsrBankPxw *pxw);
void hw_pxw_set_pat_2(volatile CsrBankPxw *pxw);
void hw_pxw_set_pat_3(volatile CsrBankPxw *pxw);
void hw_pxw_set_pat_4(volatile CsrBankPxw *pxw);
void hw_pxw_set_pat_4u(volatile CsrBankPxw *pxw);
void hw_pxw_set_pat_5(volatile CsrBankPxw *pxw);
void hw_pxw_set_pat_6(volatile CsrBankPxw *pxw);
void hw_pxw_set_pat_7(volatile CsrBankPxw *pxw);
void hw_pxw_set_pat_8(volatile CsrBankPxw *pxw);
void hw_pxw_set_pat_9(volatile CsrBankPxw *pxw);
void hw_pxw_set_pat_10(volatile CsrBankPxw *pxw);
void hw_pxw_test_tile(volatile CsrBankPxw *pxw);
void hw_pxw_setpat(volatile CsrBankPxw *pxw, uint32_t pat);
void hw_pxw_set_addr(volatile CsrBankPxw *pxw, uint32_t addr);
void hw_pxw_8bit_tile(volatile CsrBankPxw *pxw, uint32_t width, uint32_t height);
void hw_pxw_256x1080_8b_yuv(volatile CsrBankPxw *pxw);
#endif

uint32_t calc_pxw_buffer_size(const uint32_t width, const uint32_t height, const struct dram_agent_config *dram_agent);
void pxw_calc_tile_info(DramAgentConfig *cfg, FrameInfo *frame, TileInfo *tile, DaPixelTile *da);
void pxw_set_tile_csr(volatile CsrBankPxw *pxw, DaPixelTile *da, uint32_t init_addr);
void pxw_set_frame_csr(volatile CsrBankPxw *pxw, DramAgentConfig *cfg, FrameInfo *frame);

#endif