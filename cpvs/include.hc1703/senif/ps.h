#ifndef PS_H_
#define PS_H_

#include "csr_bank_ps.h"

typedef enum ps_word_size {
	PS_WORD_SIZE_6_BIT = 0,
	PS_WORD_SIZE_7_BIT,
	PS_WORD_SIZE_8_BIT,
	PS_WORD_SIZE_10_BIT,
	PS_WORD_SIZE_12_BIT,
	PS_WORD_SIZE_14_BIT,
	PS_WORD_SIZE_16_BIT,
} PsWordSize;

typedef enum ps_des_bit_size {
	PS_DES_BIT_SIZE_1_BIT = 0,
	PS_DES_BIT_SIZE_2_BIT,
	PS_DES_BIT_SIZE_3_BIT,
	PS_DES_BIT_SIZE_4_BIT,
} PsDesBitSize;

typedef enum ps_data_type {
	PS_DATA_TYPE_RAW = 0,
	PS_DATA_TYPE_YUV420_LEGACY,
	PS_DATA_TYPE_YUV420_UV_IN_ODD_LINE,
	PS_DATA_TYPE_YUV420_UV_IN_EVEN_LINE,
	PS_DATA_TYPE_YUV422,
} PsDataType;

void ps_eanble_ps(uint32_t en);
void ps_set_ps_reset(void);
void ps_set_frame_reset(void);
void ps_set_dec_mode(uint32_t mode);
void ps_set_lp_mode(uint32_t mode);
void ps_set_word_size(PsWordSize size);
void ps_set_word_shift(uint32_t shift);
void ps_set_word_order(uint32_t order); /* 0: MSB, 1: LSB */
void ps_set_width(uint32_t width);
void ps_set_height(uint32_t height);
void ps_disalbe_ec(uint32_t dis);
void ps_hsync_polar(uint32_t polar); /* 0: high-active, 1: low-active */
void ps_vsync_polar(uint32_t polar); /* 0: high-active, 1: low-active */
void ps_enable_des(uint32_t en);
void ps_set_des_bit_size(PsDesBitSize size);
void ps_set_des_unpac_num(uint32_t num);
void ps_set_des_order(uint32_t order); /* 0: MSB, 1: LSB */
void ps_set_data_type(PsDataType type);
void ps_enable_sof_det(uint32_t en);
void ps_enable_eof_det(uint32_t en);
void ps_enable_eol_det(uint32_t en);
void ps_enable_sov_det(uint32_t en);
void ps_set_sync_code(uint32_t id, uint32_t code);
void ps_set_sof_code(uint32_t code);
void ps_set_eof_code(uint32_t code);
void ps_set_sol_code(uint32_t code);
void ps_set_sov_code(uint32_t code);
void ps_set_eov_code(uint32_t code);
uint16_t ps_get_frame_count(void);
uint16_t ps_get_active_line_count(void);
uint16_t ps_get_blank_line_count(void);
uint16_t ps_get_word_count(void);

#endif /* PS_H_ */
