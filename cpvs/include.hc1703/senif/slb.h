#ifndef SLB_H_
#define SLB_H_

#include "csr_bank_slb.h"

typedef enum slb_data_type {
	SLB_DATA_TYPE_RAW = 0,
	SLB_DATA_TYPE_YUV420_LEGACY,
	SLB_DATA_TYPE_YUV420_UV_IN_ODD_LINE,
	SLB_DATA_TYPE_YUV420_UV_IN_EVNE_LINE,
	SLB_DATA_TYPE_YUV422,
} SlbDataType;

#define slb_enable(base, en)                                                                                           \
	{                                                                                                              \
		(base)->enable = (uint8_t)(en)&0x1;                                                                    \
	}
#define slb_frame_reset(base)                                                                                          \
	{                                                                                                              \
		(base)->frame_reset = 1;                                                                               \
	}
#define slb_enable_lbuf(base, en)                                                                                      \
	{                                                                                                              \
		(base)->lbuf_en = (uint8_t)(en)&0x1;                                                                   \
	}
#define slb_enable_crop(base, en)                                                                                      \
	{                                                                                                              \
		(base)->crop_en = (uint8_t)(en)&0x1;                                                                   \
	}
#define slb_enable_comp(base, en)                                                                                      \
	{                                                                                                              \
		(base)->comp_en = (uint8_t)(en)&0x1;                                                                   \
	}
#define slb_set_src_res(base, width, height)                                                                           \
	{                                                                                                              \
		(base)->src_width = (uint16_t)(width);                                                                 \
		(base)->src_height = (uint16_t)(height);                                                               \
	}
#define slb_set_dst_res(base, width, height)                                                                           \
	{                                                                                                              \
		(base)->dst_width = (uint16_t)(width);                                                                 \
		(base)->dst_height = (uint16_t)(height);                                                               \
	}
#define slb_set_cord(base, left, right, top, bottom)        \
	{                                                   \
		(base)->cord_x_left = (uint16_t)(left);     \
		(base)->cord_x_right = (uint16_t)(right);   \
		(base)->cord_y_top = (uint16_t)(top);       \
		(base)->cord_y_bottom = (uint16_t)(bottom); \
	}
#define slb_set_data_type(base, type)                                                                                  \
	{                                                                                                              \
		(base)->data_type = (SlbDataType)(type);                                                               \
	}

#define slb_get_frame_count(base) ((uint16_t)((base)->frame_count))
#define slb_get_frame_drop_count(base) ((uint16_t)((base)->frame_drop_count))
#define slb_get_src_width_count(base) ((uint16_t)((base)->src_width_count))
#define slb_get_src_height_count(base) ((uint16_t)((base)->src_height_count))
#define slb_get_lbuf_wr_width_count(base) ((uint16_t)((base)->lbuf_wr_width_count))
#define slb_get_lbuf_wr_height_count(base) ((uint16_t)((base)->lbuf_wr_height_count))
#define slb_get_lbuf_rd_width_count(base) ((uint16_t)((base)->lbuf_rd_width_count))
#define slb_get_lbuf_rd_height_count(base) ((uint16_t)((base)->lbuf_rd_height_count))
#define slb_get_dst_width_count(base) ((uint16_t)((base)->dst_width_count))
#define slb_get_dst_height_count(base) ((uint16_t)((base)->dst_height_count))

#endif /* SLB_H_ */
