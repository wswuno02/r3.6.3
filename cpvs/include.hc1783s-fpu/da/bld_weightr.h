#ifndef HW_WEIGHTR_H
#define HW_WEIGHTR_H

#ifndef __KERNEL__
#include <stdint.h>
void hw_weightr_frame_start();
void hw_weightr_irq_clear();
uint32_t hw_weightr_status_frame_end();
uint32_t hw_weightr_status_bw_insufficient();
uint32_t hw_weightr_status_access_violation();
void hw_weightr_irq_mask(uint32_t frame_end, uint32_t bw_insufficient, uint32_t access_violation);
void hw_weightr_dram_info(uint32_t col_addr_type, uint32_t bank_interleave_type, uint32_t bank_group_type);
void hw_weightr_access_illegal(uint32_t hang, uint32_t mask);
void hw_weightr_set_target_burst_len(uint32_t burst_len);

void hw_weightr_init();
void hw_weightr_setpat(uint32_t pat);
void hw_weightr_set_height(uint32_t height);
void hw_weightr_set_addr(uint32_t addr);
#else
#include <linux/types.h>
#endif

#define BLD_WEIGHTR_BASE 0x82100400

#endif