#ifndef DA_UTILS_H_
#define DA_UTILS_H_

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

#include "da_define.h"
#define MSB 8
#define LSB 2

#define WORD 64
// #define PPW_MSB 8 // (WORD / MSB) = 64 / 8
// #define PPW_LSB 32 // (WORD / LSB) = 64 / 2

// #define BLOCK_WIDTH 8
// #define SUBBLOCK_WIDTH 8
// #define MACRO_BLOCK_WIDTH 16
#define MV8_PPW 2
// #define SB_PER_MB (MACRO_BLOCK_WIDTH / SUBBLOCK_WIDTH)

#define round_up_div(value, base) ((value) + (base - 1)) / (base)
#define round_up_base(value, base) (((value) + (base - 1)) / (base)) * (base)
// #define ceil(x, y) (((x) + (y)-1) / (y) * (y))
// #define ceil_div(x, y) (((x) + (y)-1) / (y))

// typedef struct frame_info {
// 	uint32_t width;
// 	uint32_t height;
// 	uint32_t bit;
// } FrameInfo;

typedef struct dram_info {
	uint8_t *pdata;
	uint8_t col_addr_type;
	uint8_t bank_interleave_type;
	uint8_t bank_group_type;
	uint8_t y_only;
	uint8_t bank_group_num;
	uint32_t dram_page_size;
	uint32_t size_per_bank_group;
} DramInfo;

typedef struct pixel_agent_config {
	// init (won't change)
	uint16_t frame_width;
	uint16_t frame_height;
	uint32_t init_addr;
	uint8_t msb_only;
	uint8_t y_only_e;
	uint8_t y_only_o;
	uint8_t tile_cnt;
	// cal info
	uint16_t addr_per_row;
	uint8_t pixel_per_package;
	uint8_t package_size;
	// final
	uint8_t package_size_last;
} PixelAgentConfig;

typedef struct block_agent_config {
	// init (won't change)
	uint16_t frame_width;
	uint16_t frame_height;
	uint8_t msb_only;
	uint8_t y_only;
	uint8_t tile_cnt;
	uint16_t search_range;
	// cal
	uint16_t fifo_per_block;
} BlockAgentConfig;

typedef struct pa_tile_info {
	// cal_info
	uint16_t width;
	uint16_t ini_addr_linear_e_add;
	uint16_t ini_addr_linear_o_add;
	uint16_t end_addr_add;
	// final
	uint8_t fifo_start_phase;
	uint8_t fifo_end_phase;
	uint8_t fifo_flush_len_last;
	uint16_t fifo_flush_len;
	uint16_t pixel_flush_len;
	uint16_t flush_addr_skip_e;
	uint16_t flush_addr_skip_o;
} PaTileInfo;

typedef struct block_tile_info {
	// cal_info
	uint32_t start_hor;
	uint32_t end_hor;
	uint32_t width;
	uint32_t addr_add;
	// final
	uint16_t block_cnt_hor;
	uint16_t fifo_flush_len;
	uint16_t pixel_flush_len;
	uint32_t flush_addr_skip;
} BlockTileInfo;

typedef struct mv_tile_info {
	// cal_info
	uint16_t width;
	uint16_t start_hor;
	uint16_t end_hor;
	uint16_t addr_per_line;
	uint32_t addr_add;
	// final
	uint8_t fifo_start_phase;
	uint8_t fifo_end_phase;
	uint16_t pixel_flush_len;
	uint16_t fifo_flush_len;
	uint16_t flush_addr_skip;
} MvTileInfo;

void cal_pa_info(PixelAgentConfig *cfg);
void cal_block_agent_info(BlockAgentConfig *cfg);

#endif