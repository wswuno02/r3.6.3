#ifndef HW_MER_H
#define HW_MER_H

#include "da_utils.h"
#include "csr_bank_mer.h"

#ifndef __KERNEL__
#include <stdint.h>
void hw_mer_frame_start();
void hw_mer_irq_clear();
uint32_t hw_mer_status_frame_end();
uint32_t hw_mer_status_bw_insufficient();
uint32_t hw_mer_status_access_violation();
void hw_mer_irq_mask(uint32_t frame_end, uint32_t bw_insufficient, uint32_t access_violation);
void hw_mer_dram_info(uint32_t col_addr_type, uint32_t bank_interleave_type, uint32_t bank_group_type);
void hw_mer_access_illegal(uint32_t hang, uint32_t mask);
void hw_mer_set_target_burst_len(uint32_t burst_len);

void hw_mer_init();
void hw_mer_256x1080_8b();
void hw_mer_set_addr(uint32_t addr);
void hw_mer_set_height(uint32_t height);

void hw_mer_set_addr_new(uint32_t base, uint32_t add);
void hw_mer_set_init_csr(BlockAgentConfig *cfg, BlockTileInfo *blk_tile);
void hw_mer_set_tile_csr(BlockAgentConfig *cfg, BlockTileInfo *blk_tile);
void hw_mer_cal_tile_info(BlockAgentConfig *cfg, TileInfo *tile, BlockTileInfo *blk_tile);
void mer_set_init_csr(volatile CsrBankMer *mer, BlockAgentConfig *cfg, BlockTileInfo *blk_tile);
void mer_calc_tile_info(DramAgentConfig *cfg, FrameInfo *frame, FrameInfo *window, TileInfo *tile, DaBlockTile *da,
                        uint16_t offset_x, uint16_t offset_y);
void mer_set_frame_csr(volatile CsrBankMer *csr, DramAgentConfig *cfg, FrameInfo *frame);
#else
#include <linux/types.h>
#endif

void mer_set_tile_csr(volatile CsrBankMer *csr, DaBlockTile *da, uint32_t init_addr);
#endif