#ifndef HW_PXR_H
#define HW_PXR_H

#include "csr_bank_pxr.h"
#include "da_utils.h"

#ifndef __KERNEL__

void hw_pxr_init(volatile CsrBankPxr *pxr);
void hw_pxr_set_pat_0(volatile CsrBankPxr *pxr);
void hw_pxr_set_pat_1(volatile CsrBankPxr *pxr);
void hw_pxr_set_pat_2(volatile CsrBankPxr *pxr);
void hw_pxr_set_pat_3(volatile CsrBankPxr *pxr);
void hw_pxr_set_pat_4(volatile CsrBankPxr *pxr);
void hw_pxr_set_pat_5(volatile CsrBankPxr *pxr);
void hw_pxr_set_pat_6(volatile CsrBankPxr *pxr);
void hw_pxr_set_pat_7(volatile CsrBankPxr *pxr);
void hw_pxr_set_pat_8(volatile CsrBankPxr *pxr);
void hw_pxr_set_pat_9(volatile CsrBankPxr *pxr);
void hw_pxr_set_pat_10(volatile CsrBankPxr *pxr);
void hw_pxr_test_tile(volatile CsrBankPxr *pxr);
void hw_pxr_setpat(volatile CsrBankPxr *pxr, uint32_t pat);
void hw_pxr_8b_tile(volatile CsrBankPxr *pxr, uint32_t width, uint32_t height, uint32_t start_x, uint32_t tile_width,
                    uint32_t addr);
#endif
// pxr
void hw_pxr_frame_start(volatile CsrBankPxr *pxr);
void hw_pxr_irq_clear(volatile CsrBankPxr *pxr);
uint32_t hw_pxr_status_frame_end(volatile CsrBankPxr *pxr);
uint32_t hw_pxr_status_bw_insufficient(volatile CsrBankPxr *pxr);
uint32_t hw_pxr_status_access_violation(volatile CsrBankPxr *pxr);
void hw_pxr_irq_mask(volatile CsrBankPxr *pxr, uint32_t frame_end, uint32_t bw_insufficient, uint32_t access_violation);
void hw_pxr_dram_info(volatile CsrBankPxr *pxr, uint32_t col_addr_type, uint32_t bank_interleave_type,
                      uint32_t bank_group_type);
void hw_pxr_access_illegal(volatile CsrBankPxr *pxr, uint32_t hang, uint32_t mask);
void hw_pxr_set_target_burst_len(volatile CsrBankPxr *pxr, uint32_t burst_len);

void hw_pxr_cal_tile_info(PixelAgentConfig *cfg, TileInfo *tile, PaTileInfo *tile_cal);
void hw_pxr_set_addr_new(volatile CsrBankPxr *pxr, uint32_t init_addr, uint32_t add_e, uint32_t add_o);
void hw_pxr_set_tile_csr(volatile CsrBankPxr *pxr, PaTileInfo *tile, uint32_t init_addr);
void hw_pxr_set_init_csr(volatile CsrBankPxr *pxr, PixelAgentConfig *cfg, PaTileInfo *tile_cal);

void hw_pxr_set_addr(volatile CsrBankPxr *pxr, uint32_t addr);
void hw_pxr_debug_mon_sel(volatile CsrBankPxr *pxr, uint32_t sel);
void hw_pxr_set_lsb_append_mode(volatile CsrBankPxr *pxr, uint32_t mode);

void pxr_calc_tile_info(DramAgentConfig *cfg, FrameInfo *frame, TileInfo *tile, DaPixelTile *da);
void pxr_set_tile_csr(volatile CsrBankPxr *pxr, DaPixelTile *tile, uint32_t init_addr);
void pxr_set_frame_csr(volatile CsrBankPxr *pxr, DramAgentConfig *cfg, FrameInfo *frame);
#endif