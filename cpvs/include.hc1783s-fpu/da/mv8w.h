#ifndef HW_MV8W_H
#define HW_MV8W_H

#include "da_utils.h"
#include "csr_bank_mv8w.h"

#ifndef __KERNEL__
#include <stdint.h>
void hw_mv8w_frame_start();
void hw_mv8w_irq_clear();
uint32_t hw_mv8w_status_frame_end();
uint32_t hw_mv8w_status_bw_insufficient();
uint32_t hw_mv8w_status_access_violation();
void hw_mv8w_irq_mask(uint32_t frame_end, uint32_t bw_insufficient, uint32_t access_violation);
void hw_mv8w_dram_info(uint32_t col_addr_type, uint32_t bank_interleave_type, uint32_t bank_group_type);
void hw_mv8w_access_illegal(uint32_t hang, uint32_t mask);
void hw_mv8w_set_target_burst_len(uint32_t burst_len);

void hw_mv8w_init();
void hw_mv8w_setpat(uint32_t pat);
void hw_mv8w_set_addr(uint32_t addr);
void hw_mv8w_set_height(uint32_t height, uint32_t block_width);

void hw_mv8w_set_addr_new(uint32_t base, uint32_t add);
void hw_mv8w_set_init_csr(BlockAgentConfig *cfg, MvTileInfo *mv_tile, uint32_t addr);
void hw_mv8w_set_tile_csr(BlockAgentConfig *cfg, MvTileInfo *mv_tile, uint32_t addr);
void hw_mv8w_cal_tile_info(BlockAgentConfig *cfg, TileInfo *tile, MvTileInfo *mv_tile);
void mv8w_set_init_csr(volatile CsrBankMv8w *csr, BlockAgentConfig *cfg, MvTileInfo *mv_tile);
void mv8w_calc_tile_info(DramAgentConfig *cfg, FrameInfo *frame, TileInfo *tile, DaLinearTile *da);
void mv8w_set_frame_csr(volatile CsrBankMv8w *csr, DramAgentConfig *cfg, FrameInfo *frame);
#else
#include <linux/types.h>
#endif
// mv8w
uint32_t calc_mvw_buffer_size(const uint32_t width, const uint32_t height, const struct dram_agent_config *dram_agent);
void mv8w_set_tile_csr(volatile CsrBankMv8w *csr, DaLinearTile *da, uint32_t init_addr);
#endif