/**
 * @file hw_efuse.h
 * @brief efuse ctrl
 */
#ifndef HW_EFUSE_CTRL_H
#define HW_EFUSE_CTRL_H

#ifndef __KERNEL__
#include <stdint.h>
#else
#include <linux/types.h>
#endif

extern volatile struct csr_bank_efuse_ctrl *efuse_ctrl_reg;

void hw_efuse_setlock(volatile struct csr_bank_efuse_ctrl *efuse_ctrl_reg, uint32_t lock);
void hw_efuse_setmode(volatile struct csr_bank_efuse_ctrl *efuse_ctrl_reg, uint32_t mode);
uint32_t hw_efuse_poll_rw_ready(volatile struct csr_bank_efuse_ctrl *efuse_ctrl_reg, uint32_t timeout, uint32_t value);
uint32_t hw_efuse_poll_rd_done(volatile struct csr_bank_efuse_ctrl *efuse_ctrl_reg, uint32_t timeout, uint32_t value);
uint32_t hw_efuse_poll_esc_done(volatile struct csr_bank_efuse_ctrl *efuse_ctrl_reg, uint32_t timeout, uint32_t value);
uint32_t hw_efuse_poll_power_dn(volatile struct csr_bank_efuse_ctrl *efuse_ctrl_reg, uint32_t timeout, uint32_t value);
uint32_t hw_efuse_power_down(volatile struct csr_bank_efuse_ctrl *efuse_ctrl_reg);
uint32_t hw_efuse_pre_array_program(volatile struct csr_bank_efuse_ctrl *efuse_ctrl_reg);
uint32_t hw_efuse_pre_redundancy_program(volatile struct csr_bank_efuse_ctrl *efuse_ctrl_reg);
uint32_t hw_efuse_array_program(volatile struct csr_bank_efuse_ctrl *efuse_ctrl_reg, uint32_t addr, uint32_t value);
uint32_t hw_efuse_pre_array_read(volatile struct csr_bank_efuse_ctrl *efuse_ctrl_reg);
uint32_t hw_efuse_array_read(volatile struct csr_bank_efuse_ctrl *efuse_ctrl_reg, uint32_t addr);


#endif
