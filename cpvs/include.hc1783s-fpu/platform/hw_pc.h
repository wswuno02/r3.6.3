/**
 * @file hw_pc.h
 * @brief pc 
 */

#ifndef HW_PC_H
#define HW_PC_H

#ifndef __KERNEL__
#include <stdint.h>
#else
#include <linux/types.h>
#endif

#include "csr_bank_ck.h"

#define CK_SRC_XTAL 0
#define CK_SRC_PLL 1

// volatile CsrBankCk *g_ck_base = (volatile CsrBankCk *)CK_BASE;
#define cken(name, en) g_ck_base->cken_##name = en
#define src_sel(name, sel) g_ck_base->name##_src_sel = sel

void switch_clk_src(void);

#endif