#ifndef KYOTO_PCA_H_
#define KYOTO_PCA_H_

#include "isp_utils.h"
#include "csr_bank_pca.h"

typedef struct isp_pca_coeff_cfg {
	struct rgb_to_hsl {
		uint16_t h_prime_from_rgb_shift;
		uint16_t h_prime_r_redefined;
		uint16_t h_prime_g_redefined;
		uint16_t h_prime_b_redefined;
		uint16_t h_prime_r_pos_60degree;
		uint16_t h_prime_r_neg_60degree;
		uint16_t h_prime_g_pos_60degree;
		uint16_t h_prime_g_neg_60degree;
		uint16_t h_prime_b_pos_60degree;
		uint16_t h_prime_b_neg_60degree;
		uint16_t h_prime_correction_term;
		uint16_t h_cal_60_degree;
	} rgb2hsl;
	struct hsl_to_rgb {
		uint16_t h_prime_shift;
	} hsl2rgb;
} IspPcaCoeffCfg;

typedef struct isp_pca_table_cfg {
	int eee[PCA_EEE_WORD_NUM];
	int eoe[PCA_EOE_WORD_NUM];
	int oee[PCA_OEE_WORD_NUM];
	int ooe[PCA_OOE_WORD_NUM];
	int eeo[PCA_EEO_WORD_NUM];
	int eoo[PCA_EOO_WORD_NUM];
	int oeo[PCA_OEO_WORD_NUM];
	int ooo[PCA_OOO_WORD_NUM];
} IspPcaTableCfg;

/* Frame start */
void hw_isp_pca_set_frame_start(void);

/* IRQ clear */
void hw_isp_pca_set_irq_clear_frame_end(void);

/* Frame end*/
uint8_t hw_isp_pca_get_status_frame_end(void);

/* IRQ mask */
void hw_isp_pca_set_irq_mask_frame_end(uint8_t irq_mask_frame_end);
uint8_t hw_isp_pca_get_irq_mask_frame_end(void);

/* Resolution */
void hw_isp_pca_set_res(const struct res *res);
void hw_isp_pca_get_res(struct res *res);

/* PCA */
void hw_isp_pca_set_coeff_cfg(const struct isp_pca_coeff_cfg *cfg);
void hw_isp_pca_get_coeff_cfg(struct isp_pca_coeff_cfg *cfg);
void hw_isp_pca_set_table_cfg(const struct isp_pca_table_cfg *cfg);
void hw_isp_pca_get_table_cfg(struct isp_pca_table_cfg *cfg);
uint8_t hw_isp_pca_get_table_en(void);

/* Debug */
void hw_isp_pca_set_dbg_mon_sel(uint8_t debug_mon_sel);
uint8_t hw_isp_pca_get_dbg_mon_sel(void);

/* Reserved */
void hw_isp_pca_set_reserved_0(uint32_t reserved_0);
uint32_t hw_isp_pca_get_reserved_0(void);

//
/* IRQ mask */
void isp_pca_set_irq_mask_frame_end(volatile CsrBankPca *csr, uint8_t irq_mask_frame_end);
uint8_t isp_pca_get_irq_mask_frame_end(volatile CsrBankPca *csr);

/* Resolution */
void isp_pca_set_res(volatile CsrBankPca *csr, const struct res *res);
void isp_pca_get_res(volatile CsrBankPca *csr, struct res *res);

/* PCA */
void isp_pca_set_coeff_cfg(volatile CsrBankPca *csr, const struct isp_pca_coeff_cfg *cfg);
void isp_pca_get_coeff_cfg(volatile CsrBankPca *csr, struct isp_pca_coeff_cfg *cfg);
void isp_pca_set_table_cfg(volatile CsrBankPca *csr, const struct isp_pca_table_cfg *cfg);
void isp_pca_get_table_cfg(volatile CsrBankPca *csr, struct isp_pca_table_cfg *cfg);
uint8_t isp_pca_get_table_en(volatile CsrBankPca *csr);

/* Debug */
void isp_pca_set_dbg_mon_sel(volatile CsrBankPca *csr, uint8_t debug_mon_sel);
uint8_t isp_pca_get_dbg_mon_sel(volatile CsrBankPca *csr);

/* Reserved */
void isp_pca_set_reserved_0(volatile CsrBankPca *csr, uint32_t reserved_0);
uint32_t isp_pca_get_reserved_0(volatile CsrBankPca *csr);

/* TODO - Move to test */
void hw_isp_pca_res(uint32_t width, uint32_t height);
void hw_isp_pca_bypass(void);
void hw_isp_pca_read_table(void);

#endif /* KYOTO_PCA_H_ */