#ifndef KYOTO_SC_H_
#define KYOTO_SC_H_

#include "csr_bank_sc.h"
#include "isp_utils.h"
#include "da_define.h"

#define ISP_SC_TAP (6)
#define ISP_SC_PHASE_PRECISION (20)

typedef struct tile_sc_param {
	uint16_t frame_width_i;
	uint16_t frame_width_o;
	uint16_t frame_height_i;
	uint16_t frame_height_o;
	// vertical
	uint32_t up_scaling_ver;
	int32_t ini_phase_ver;
	uint32_t phase_step_ver;
	int32_t ini_filt_phase_ds_ver;
	uint32_t filt_phase_step_ds_ver;
	int32_t ini_cnt_ver[ISP_SC_TAP];

	// horizontal
	uint16_t tile_width_i[MAX_TILE_NUM];
	uint16_t tile_width_o[MAX_TILE_NUM];
	uint32_t up_scaling_hor;
	int32_t ini_phase_hor[MAX_TILE_NUM];
	uint32_t phase_step_hor;
	int32_t ini_filt_phase_ds_hor[MAX_TILE_NUM];
	int32_t filt_phase_step_ds_hor;
	int32_t ini_cnt_hor[MAX_TILE_NUM][ISP_SC_TAP];
	uint32_t crop_o_left[MAX_TILE_NUM];
} TileScParam;

typedef struct sc_double {
	int32_t integer;
	int32_t remainder;
	int32_t divisor;
} ScDouble;

#ifndef __KERNEL__
void hw_isp_sc_frame_start(void);
void hw_isp_sc_irq_clear_frame_end(void);
uint32_t hw_isp_sc_status_frame_end(void);
void hw_isp_sc_irq_mask_frame_end(uint32_t irq_mask_frame_end);
void hw_isp_sc_res(uint32_t width, uint32_t height);
void hw_isp_sc_bypass(void);
void hw_isp_sc_debug_mon_sel(uint32_t debug_mon_sel);
void isp_sc_frame_start(volatile CsrBankSc *csr);
void isp_sc_irq_clear_frame_end(volatile CsrBankSc *csr);
uint32_t isp_sc_get_status_frame_end(volatile CsrBankSc *csr);
void isp_sc_set_irq_mask_frame_end(volatile CsrBankSc *csr, uint32_t irq_mask_frame_end);
void isp_sc_set_debug_mon_sel(volatile CsrBankSc *csr, uint32_t debug_mon_sel);
void pr_sc_param(struct tile_sc_param *sc_param, int tile_n);
#endif

void isp_calc_sc_ver_param(struct tile_sc_param *sc_param, uint16_t frame_height_i, uint16_t frame_height_o);
void isp_calc_sc_hor_param(struct tile_sc_param *sc_param, uint16_t frame_width_i, uint16_t frame_width_o,
                           const struct tile_info *tile_i, const struct tile_info *tile_o, int tile_n);
void isp_sc_set_tile_csr(volatile CsrBankSc *csr, struct tile_sc_param *sc_param, int t);
void isp_sc_set_frame_csr(volatile CsrBankSc *csr, struct tile_sc_param *sc_param);

#endif /* KYOTO_SC_H_ */