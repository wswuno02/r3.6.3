#ifndef HW_ISP_H_
#define HW_ISP_H_

#ifndef __KERNEL__
#include <stdint.h>
#else
#include <linux/types.h>
#endif
#include "csr_bank_isp.h"

#define isp_bro_en(src, dst, en) g_isp->src##_broadcast_to_##dst##_enable = en
#define isp_bro_mul(src, dst)                                                                                          \
	if ((dst) == 7) {                                                                                              \
		g_isp->src##_broadcast_enable = 0;                                                                     \
		g_isp->src##_broadcast = 0;                                                                            \
	} else {                                                                                                       \
		g_isp->src##_broadcast_enable = 1;                                                                     \
		g_isp->src##_broadcast |= (1 << (dst));                                                                \
	}

typedef enum ispr_dst {
	ISPR_PG,
	ISPR_GFXMUX,
	ISPR_NONE = 7,
} IsprDst;

typedef enum pg_dst {
	PG_CS,
	PG_GFXMUX,
	PG_NONE = 7,
} PgDst;

typedef enum gfx0_src {
	GFX0_ISPR,
	GFX0_PG,
} Gfx0Src;

typedef enum gfx1_src {
	GFX1_PG,
	GFX1_ISPR,
} Gfx1Src;

typedef enum gfxbld0_src {
	GFXBLD0_GFX,
	GFXBLD0_PRD,
} GfxBld0Src;

typedef enum gfxbld1_src {
	GFXBLD1_GFX,
	GFXBLD1_PRD,
} GfxBld1Src;

typedef enum gfxbld1_dst {
	GFX1_BLD,
	GFX1_OUT,
	GFX1_NONE,
} Gfxbld1Dst;

typedef enum ispin_y_dst {
	ISPIN_Y_DPCHDR,
	ISPIN_Y_RGBP,
	ISPIN_Y_SC,
	ISPIN_Y_ENH,
	ISPIN_Y_FIFO0,
	ISPIN_Y_FIFO1,
	ISPIN_Y_FIFO2,
	ISPIN_Y_NONE = 7,
} IspinYDst;

typedef enum ispin_c_dst {
	ISPIN_C_DPCHDR,
	ISPIN_C_ENH,
	ISPIN_C_FIFO0,
	ISPIN_C_FIFO1,
	ISPIN_C_FIFO2,
	ISPIN_C_NONE = 7,
} IspinCDst;

typedef enum dpchdr_y_dst {
	DPCHDR_Y_RGBP,
	DPCHDR_Y_SC,
	DPCHDR_Y_ENH,
	DPCHDR_Y_FIFO1,
	DPCHDR_Y_FIFO2,
	DPCHDR_Y_NONE = 7,
} DpchdrYDst;

typedef enum dpchdr_c_dst {
	DPCHDR_C_SC,
	DPCHDR_C_ENH,
	DPCHDR_C_FIFO1,
	DPCHDR_C_FIFO2,
	DPCHDR_C_NONE = 7,
} DpchdrCDst;

typedef enum rgbp_src {
	RGBP_DPCHDR,
	RGBP_ISPIN1,
	RGBP_ISPIN0,
} RgbpSrc;

typedef enum rgbp_dst {
	RGBP_SC,
	RGBP_ENH,
	RGBP_NONE = 7,
} RgbpDst;

typedef enum sc_y_src {
	SC_Y_RGBP,
	SC_Y_DPCHDR,
	SC_Y_ISPIN1,
	SC_Y_ISPIN0,
} ScYSrc;

typedef enum sc_c_src {
	SC_C_RGBP,
	SC_C_DPCHDR,
} ScCSrc;

typedef enum sc_dst {
	SC_ENH,
	SC_FIFO1,
	SC_FIFO2,
	SC_NONE = 7,
} ScDst;

typedef enum enh_src {
	ENH_SC,
	ENH_RGBP,
	ENH_DPCHDR,
	ENH_ISPIN1,
	ENH_ISPIN0,
} EnhSrc;

typedef enum dhz_dst {
	DHZ_FIFO0,
	DHZ_FIFO1,
	DHZ_FIFO2,
	DHZ_NONE = 7,
} DhzDst;

typedef enum fifo0_src {
	FIFO0_ISPIN0,
	FIFO0_ISPIN1,
	FIFO0_DHZ,
} Fifo0Src;

typedef enum fifo12_src {
	FIFO12_ISPIN0,
	FIFO12_ISPIN1,
	FIFO12_DPCHDR,
	FIFO12_SC,
	FIFO12_DHZ,
	FIFO12_ISP_INK,
} Fifo12Src;

void isp_clear_checksum(void);
void isp_sw_reset(void);
void isp_csr_reset(void);
void isp_routing_bypass(void);
void isp_routing_1(void);
void isp_routing_1_ispr1(void);
void isp_routing_1_gfx(void);
void isp_routing_1_pg_gfx(void);
void isp_routing_1_1(void);
void isp_routing_2(void);
void isp_routing_2_hdr(void);
void isp_routing_3(void);
void isp_routing_3_gfx(void);
void isp_routing_4(void);
void isp_routing_5(void);
void isp_routing_6(void);
void isp_routing_7(void);
void isp_routing_8(void);
void isp_routing_9(void);
void isp_routing_all(void);
void isp_routing_tovp(void);
void isp_routing_ispin_enh_dhz_tovp(void);
void isp_routing_gfx_tovp(void);
void isp_routing_gfx_tovp_and_tofifo1(void);
void isp_vp_routing_tovp(void);
void isp_routing_2ispr_gfx_bld(void);
void isp_routing_2ispr_bld(void);
void isp_routing_gfx_hdr(void);
void isp_routing_for_wvp(void);
void hw_isp_setroute(uint32_t pat);
void isp_dump_debug_mon(void);
void isp_routing_all_wdr(void);
void isp_routing_all_hdr(void);
void isp_routing_all_hdr_to_vp(void);

typedef struct ispr_brdcst {
	uint8_t to_pg;
	uint8_t to_gfxmux;
} IsprBrdcst;

typedef struct isp_pg_brdcst {
	uint8_t to_cs;
	uint8_t to_gfxmux;
} IspPgBrdcst;

typedef enum isp_gfxin0_sel { ISP_GFXIN0_SEL_ISPR = 0, ISP_GFXIN0_SEL_PG, ISP_GFXIN0_SEL_NONE = 0 } IspGfxin0Sel;
typedef enum isp_gfxin1_sel { ISP_GFXIN1_SEL_PG = 0, ISP_GFXIN1_SEL_ISPR, ISP_GFXIN1_SEL_NONE = 0 } IspGfxin1Sel;

typedef enum isp_gfxbld_sel { ISP_GFXBLD_SEL_GFX = 0, ISP_GFXBLD_SEL_PRD, ISP_GFXBLD_SEL_NONE = 0 } IspGfxbldSel;

typedef struct isp_gfxbld1_brdcst {
	uint8_t to_bld;
	uint8_t to_ispin_out;
} IspGfxbld1Brdcst;

typedef union ispin_y_brdcst {
	struct {
		uint8_t to_dpchdr : 1;
		uint8_t to_rgbp : 1;
		uint8_t to_sc : 1;
		uint8_t to_enh : 1;
		uint8_t to_fifo0 : 1;
		uint8_t to_fifo1 : 1;
		uint8_t to_fifo2 : 1;
		uint8_t : 0; /* padding */
	};
	uint8_t value;
} IspinYBrdcst;

typedef union ispin_c_brdcst {
	struct {
		uint8_t to_dpchdr : 1;
		uint8_t to_enh : 1;
		uint8_t to_fifo0 : 1;
		uint8_t to_fifo1 : 1;
		uint8_t to_fifo2 : 1;
		uint8_t : 0; /* padding */
	};
	uint8_t value;
} IspinCBrdcst;

typedef union isp_dpchdr_y_brdcst {
	struct {
		uint8_t to_rgbp : 1;
		uint8_t to_sc : 1;
		uint8_t to_enh : 1;
		uint8_t to_fifo1 : 1;
		uint8_t to_fifo2 : 1;
		uint8_t : 0; /* padding */
	};
	uint8_t value;
} IspDpchdrYBrdcst;

typedef union isp_dpchdr_c_brdcst {
	struct {
		uint8_t to_sc : 1;
		uint8_t to_enh : 1;
		uint8_t to_fifo1 : 1;
		uint8_t to_fifo2 : 1;
		uint8_t : 0; /* padding */
	};
	uint8_t value;
} IspDpchdrCBrdcst;

typedef enum isp_rgbp_sel {
	ISP_RGBP_SEL_DPCHDR = 0,
	ISP_RGBP_SEL_ISPIN1,
	ISP_RGBP_SEL_ISPIN0,
	ISP_RGBP_SEL_NONE = 0
} IspRgbpSel;

typedef union isp_rgbp_brdcst {
	struct {
		uint8_t to_sc : 1;
		uint8_t to_enh : 1;
		uint8_t : 0; /* padding */
	};
	uint8_t value;
} IspRgbpBrdcst;

typedef enum isp_sc_y_sel {
	ISP_SC_Y_SEL_RGBP = 0,
	ISP_SC_Y_SEL_DPCHDR,
	ISP_SC_Y_SEL_ISPIN1,
	ISP_SC_Y_SEL_ISPIN0,
	ISP_SC_Y_SEL_NONE = 0
} IspScYSel;

typedef enum isp_sc_c_sel {
	ISP_SC_C_SEL_RGBP = 0,
	ISP_SC_C_SEL_DPCHDR,
} IspScCSel;

typedef union isp_sc_brdcst {
	struct {
		uint8_t to_enh : 1;
		uint8_t to_fifo1 : 1;
		uint8_t to_fifo2 : 1;
		uint8_t : 0; /* padding */
	};
	uint8_t value;
} IspScBrdcst;

typedef enum isp_enh_sel {
	ISP_ENH_Y_SEL_SC = 0,
	ISP_ENH_Y_SEL_RGBP,
	ISP_ENH_Y_SEL_DPCHDR,
	ISP_ENH_Y_SEL_ISPIN1,
	ISP_ENH_Y_SEL_ISPIN0,
	ISP_ENH_Y_SEL_NONE = 0
} IspEnhSel;

typedef union isp_dhz_brdcst {
	struct {
		uint8_t to_fifo0 : 1;
		uint8_t to_fifo1 : 1;
		uint8_t to_fifo2 : 1;
		uint8_t : 0; /* padding */
	};
	uint8_t value;
} IspDhzBrdcst;

typedef enum isp_fifo0_sel {
	ISP_FIFO0_SEL_ISPIN0 = 0,
	ISP_FIFO0_SEL_ISPIN1,
	ISP_FIFO0_SEL_DHZ,
	ISP_FIFO0_SEL_NONE = 0
} IspFifo0Sel;

typedef enum isp_fifo12_sel {
	ISP_FIFO12_SEL_ISPIN0 = 0,
	ISP_FIFO012_SEL_ISPIN1,
	ISP_FIFO12_SEL_DPCHDR,
	ISP_FIFO12_SEL_SC,
	ISP_FIFO12_SEL_DHZ,
	ISP_FIFO12_SEL_ISPINK,
	ISP_FIFO12_SEL_NONE = 0
} IspFifo12Sel;

typedef struct isp_path_cfg {
	IsprBrdcst ispr0_brdcst;
	IsprBrdcst ispr1_brdcst;
	IspPgBrdcst pg0_brdcst;
	IspPgBrdcst pg1_brdcst;
	uint8_t gfxbld_bypass;
	IspGfxin0Sel gfxin0_sel;
	IspGfxin1Sel gfxin1_sel;
	IspGfxbldSel gfxbld0_sel;
	IspGfxbldSel gfxbld1_sel;
	IspGfxbld1Brdcst gfxbld1_brdcst;
	IspinYBrdcst ispin0_y_brdsct;
	IspinCBrdcst ispin0_c_brdsct;
	IspinYBrdcst ispin1_y_brdsct;
	IspinCBrdcst ispin1_c_brdsct;
	uint8_t hdr_bypass;
	uint8_t dpchdr_c_sel; /* 0: ispin0, 1: ispin1 */
	IspDpchdrYBrdcst dpchdr_y_brdsct;
	IspDpchdrCBrdcst dpchdr_c_brdsct;
	IspRgbpSel rgbp_sel;
	IspRgbpBrdcst rgbp_y_brdcst;
	IspRgbpBrdcst rgbp_c_brdcst;
	IspScYSel sc_y_sel;
	IspScCSel sc_c_sel;
	IspScBrdcst sc_y_brdcst;
	IspScBrdcst sc_c_brdcst;
	IspEnhSel enh_y_sel;
	IspEnhSel enh_c_sel;
	IspDhzBrdcst dhz_y_brdcst;
	IspDhzBrdcst dhz_c_brdcst;
	IspFifo0Sel fifo0_sel;
	IspFifo12Sel fifo1_sel;
	IspFifo12Sel fifo2_sel;
} IspPathCfg;

void isp_set_path(volatile CsrBankIsp *csr, const IspPathCfg *cfg);
#endif