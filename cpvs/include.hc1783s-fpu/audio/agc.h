#ifndef AGC_H_
#define AGC_H_

#include "csr_bank_audio_agc.h"

#define AGC_MODE_ENABLE (0)
#define AGC_MODE_A_CLOSED (1)
#define AGC_MODE_D_CLOSED (2)
#define AGC_MODE_DISABLE (3)

#define AGC_GAIN_CURVE_NODE_NUM 64

/* unit action */

void agc_set_again(uint32_t gain);
void agc_set_hpf(uint32_t b0, uint32_t b1, uint32_t a);
void agc_set_pd(uint32_t threshold, uint32_t filtered_alpha, uint32_t env_alpha);
void agc_set_gain_param(uint32_t mode, uint32_t pre_amp_mod, uint32_t upper_bond, uint32_t lower_bond,
                        uint32_t noise_gate, const uint32_t *dig_gain_sen, const uint32_t *ana_gain_sen,
                        const uint32_t *gain_curve);
void agc_update_double_buf(void);

#endif /* AGC_H_ */
