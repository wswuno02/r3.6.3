#ifndef AUDIO_IN_H_
#define AUDIO_IN_H_

#include "csr_bank_adoin.h"
#include "csr_bank_adoin_syscfg.h"
#include "csr_bank_audio_hpf.h"
#include "csr_bank_audio_fade_in.h"

typedef enum audio_src {
	AUDIO_SRC_I2S = 0,
	AUDIO_SRC_ADC,
} AudioSrc;

void adoin_syscfg_adc_cken(uint32_t en);
void adoin_syscfg_adc_lv_rst(uint32_t lvl);
void adoin_syscfg_adc_sw_rst(void);
void adoin_syscfg_adc_csr_rst(void);
void adoin_syscfg_enc_cken(uint32_t en);
void adoin_syscfg_enc_lv_rst(uint32_t lvl);
void adoin_syscfg_enc_sw_rst(void);
void adoin_syscfg_enc_csr_rst(void);
void adoin_syscfg_fade_cken(uint32_t en);
void adoin_syscfg_fade_lv_rst(uint32_t lvl);
void adoin_syscfg_fade_sw_rst(void);
void adoin_syscfg_fade_csr_rst(void);
void adoin_syscfg_i2s_cken(uint32_t en);
void adoin_syscfg_i2s_lv_rst(uint32_t lvl);
void adoin_syscfg_i2s_sw_rst(void);
void adoin_syscfg_i2s_csr_rst(void);
void adoin_syscfg_write_cken(uint32_t en);
void adoin_syscfg_write_lv_rst(uint32_t lvl);
void adoin_syscfg_write_sw_rst(void);
void adoin_syscfg_write_csr_rst(void);
void adoin_syscfg_agc_cken(uint32_t en);
void adoin_syscfg_agc_lv_rst(uint32_t lvl);
void adoin_syscfg_agc_sw_rst(void);
void adoin_syscfg_agc_csr_rst(void);
void adoin_syscfg_ares_cken(uint32_t en);
void adoin_syscfg_ares_lv_rst(uint32_t lvl);
void adoin_syscfg_ares_sw_rst(void);
void adoin_syscfg_ares_csr_rst(void);
void adoin_syscfg_anr_cken(uint32_t en);
void adoin_syscfg_anr_lv_rst(uint32_t lvl);
void adoin_syscfg_anr_sw_rst(void);
void adoin_syscfg_anr_csr_rst(void);
void adoin_syscfg_hpf_cken(uint32_t en);
void adoin_syscfg_hpf_lv_rst(uint32_t lvl);
void adoin_syscfg_hpf_sw_rst(void);
void adoin_syscfg_hpf_csr_rst(void);

void adoin_audiow_irq_clear_buffer_full(void);
void adoin_audiow_irq_clear_write_end(void);
void adoin_i2s_irq_clear_real_stop(void);
void adoin_adc_irq_clear_real_stop(void);
void adoin_adc_irq_clear_no_ack(void);
uint8_t adoin_audiow_status_buffer_full(void);
uint8_t adoin_audiow_status_write_end(void);
uint8_t adoin_i2s_status_real_stop(void);
uint8_t adoin_adc_status_real_stop(void);
uint8_t adoin_adc_status_no_ack(void);
void adoin_audiow_irq_mask_buffer_full(uint32_t mask);
void adoin_audiow_irq_mask_write_end(uint32_t mask);
void adoin_i2s_irq_mask_real_stop(uint32_t mask);
void adoin_adc_irq_mask_real_stop(uint32_t mask);
void adoin_adc_irq_mask_no_ack(uint32_t mask);
void adoin_sel_source(AudioSrc src);
void adoin_bypass_downsample(uint32_t bypass);
void adoin_set_hpf(uint32_t b0, uint32_t b1, uint32_t a);
void adoin_enable_fixgain(uint32_t enable);

void audio_fade_in_audio_start(void);
void audio_fade_in_fade_start(void);
void audio_fade_in_fade_stop(void);
void audio_fade_in_set_fade_time(uint32_t time);

#endif /* AUDIO_IN_H_ */
