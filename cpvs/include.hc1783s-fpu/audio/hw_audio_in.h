#ifndef HW_AUDIO_IN_H_
#define HW_AUDIO_IN_H_

#include "adc.h"
#include "adoenc.h"
#include "adow.h"
#include "agc.h"
#include "anr.h"
#include "adoin.h"
#include "i2s.h"

void hw_audio_in_sw_reset(void);
void hw_audio_in_csr_reset(void);
void hw_audio_in_reset(void);
void hw_audio_in_init_darb(uint32_t col_addr_type, uint32_t bank_addr_type);
void hw_audio_in_init(AudioSrc src);
void hw_audio_in_init_diff(void);
void hw_audio_in_set_buffer(uint32_t addr, uint32_t length);
void hw_audio_in_adc_stream_start(void);
void hw_audio_in_adc_stream_stop(void);
void hw_audio_in_i2s_stream_start(void);
void hw_audio_in_i2s_stream_stop(void);

/* Driver */
void hw_audio_in_mask_irq_all(void);
void hw_audio_in_clear_irq_all(void);
void hw_audio_in_unmask_irq(void);
int hw_audio_in_is_irq_audio_only(void);
int hw_audio_in_is_irq_ed_only(void);

#endif /* HW_AUDIO_IN_H_ */
