#ifndef AUDIO_OUT_H_
#define AUDIO_OUT_H_

#include "csr_bank_adoout.h"
#include "csr_bank_adoout_syscfg.h"
#include "csr_bank_audio_fade_out.h"

typedef enum audio_dst {
	AUDIO_DST_NONE = 0,
	AUDIO_DST_I2S,
	AUDIO_DST_DAC,
} AudioDst;

void adoout_syscfg_adac_cken(uint32_t en);
void adoout_syscfg_adac_lv_rst(uint32_t lvl);
void adoout_syscfg_adac_sw_rst(void);
void adoout_syscfg_adac_csr_rst(void);
void adoout_syscfg_dec_cken(uint32_t en);
void adoout_syscfg_dec_lv_rst(uint32_t lvl);
void adoout_syscfg_dec_sw_rst(void);
void adoout_syscfg_dec_csr_rst(void);
void adoout_syscfg_fade_cken(uint32_t en);
void adoout_syscfg_fade_lv_rst(uint32_t lvl);
void adoout_syscfg_fade_sw_rst(void);
void adoout_syscfg_fade_csr_rst(void);
void adoout_syscfg_i2s_cken(uint32_t en);
void adoout_syscfg_i2s_lv_rst(uint32_t lvl);
void adoout_syscfg_i2s_sw_rst(void);
void adoout_syscfg_i2s_csr_rst(void);
void adoout_syscfg_read_cken(uint32_t en);
void adoout_syscfg_read_lv_rst(uint32_t lvl);
void adoout_syscfg_read_sw_rst(void);
void adoout_syscfg_read_csr_rst(void);
void adoout_syscfg_up_sample_cken(uint32_t en);
void adoout_syscfg_up_sample_lv_rst(uint32_t lvl);
void adoout_syscfg_up_sample_sw_rst(void);
void adoout_syscfg_up_sample_csr_rst(void);

void adoout_audior_irq_clear_bw_insufficient(void);
void adoout_audior_irq_clear_buffer_full(void);
void adoout_audior_irq_clear_access_violation(void);
void adoout_audior_irq_clear_read_end(void);
void adoout_i2s_irq_clear_real_stop(void);
void adoout_i2s_irq_clear_error(void);
void adoout_dac_irq_clear_no_ready(void);
void adoout_dac_irq_clear_incr_arrival(void);
void adoout_dac_irq_clear_decr_arrival(void);
uint8_t adoout_audior_status_bw_insufficient(void);
uint8_t adoout_audior_status_buffer_full(void);
uint8_t adoout_audior_status_access_violation(void);
uint8_t adoout_audior_status_read_end(void);
uint8_t adoout_i2s_status_real_stop(void);
uint8_t adoout_i2s_status_error(void);
uint8_t adoout_dac_status_no_ready(void);
uint8_t adoout_dac_status_incr_arrival(void);
uint8_t adoout_dac_status_decr_arrival(void);
void adoout_audior_irq_mask_bw_insufficient(uint32_t mask);
void adoout_audior_irq_mask_buffer_full(uint32_t mask);
void adoout_audior_irq_mask_access_violation(uint32_t mask);
void adoout_audior_irq_mask_read_end(uint32_t mask);
void adoout_i2s_irq_mask_real_stop(uint32_t mask);
void adoout_i2s_irq_mask_error(uint32_t mask);
void adoout_dac_irq_mask_no_ready(uint32_t mask);
void adoout_dac_irq_mask_incr_arrival(uint32_t mask);
void adoout_dac_irq_mask_decr_arrival(uint32_t mask);
void adoout_sel_destination(AudioDst dst);
void adoout_set_up_sample(uint32_t scale);

void audio_fade_out_audio_start(void);
void audio_fade_out_fade_start(void);
void audio_fade_out_fade_stop(void);
void audio_fade_out_set_fade_time(uint32_t time);

#endif /* AUDIO_OUT_H_ */
