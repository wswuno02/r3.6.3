#ifndef ADC_H_
#define ADC_H_

#include "csr_bank_adccfg.h"
#include "csr_bank_adcctr.h"
#include "csr_bank_apreampcfg.h"
#include "csr_bank_audio_agc.h"
#include "csr_bank_audio_anr.h"

typedef enum apreamp_mode {
	APREAMP_MODE_INPUT_DIFFERENTIAL = 0,
	APREAMP_MODE_INPUT_SINGLE_ENDED,
	APREAMP_MODE_BYPASS,
} ApreampMode;

typedef enum adc_res {
	ADC_RES_6 = 0,
	ADC_RES_8,
	ADC_RES_10,
	ADC_RES_12,
} AdcRes;

/* unit action */
void preamp_enable_preamp(uint32_t en);
void preamp_enable_preamp_clk(uint32_t en);
void preamp_enable_preamp_ref(uint32_t en);
void preamp_enable_preamp_plus(uint32_t en);
void preamp_enable_preamp_minus(uint32_t en);
int preamp_set_mode(ApreampMode mode);

void adccfg_enable_adc(uint32_t en);
void adccfg_set_resolution(AdcRes res);

void adcctr_start_clk(void);
void adcctr_stop_clk(void);
void adcctr_start(void);
void adcctr_stop(void);
int adcctr_env_query(uint32_t ch);
void adcctr_env_init(void);
void adcctr_enable_init_phy_reset(uint32_t en);
void adcctr_enable_init_phy_soc(uint32_t en);
void adcctr_sel_phy_serial(uint32_t sel);
void adcctr_set_clock(uint32_t cycle_l, uint32_t cycle_h, uint32_t len);
int adcctr_set_type(uint32_t ch, uint32_t type);
void adcctr_set_format(uint32_t format);
void adcctr_set_lsb_repeat(uint32_t repeat);
void adcctr_set_resolution(AdcRes res);
void adcctr_set_mute(uint32_t mute);
void adcctr_set_latency(uint32_t latency);
int adcctr_get_env_data(uint32_t ch, uint32_t *data);
void adcctr_set_virtual_ch_cnt(uint32_t ch_cnt);
int adcctr_set_virtual_ch_param(volatile struct csr_bank_adcctr *adcctr, uint32_t ch, uint8_t ch_num, uint8_t seldiff,
                                uint8_t selref, uint8_t selbg);
int adcctr_set_virtual_ch(uint32_t chn, uint8_t chn_num, uint8_t seldiff, uint8_t selref, uint8_t selbg);

#endif /* ADC_H_ */
