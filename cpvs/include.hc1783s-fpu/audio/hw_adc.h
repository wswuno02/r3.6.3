#ifndef HW_ADC_H_
#define HW_ADC_H_

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

#include "adc.h"
#include "adoin.h"

#define HW_ADC_CHAN_NUM_MAX 6

#define ADOADC_CFG_ADCCFG_OFFSET (0x00000000)
#define ADOADC_CTR_ADCCTR_OFFSET (0x00008000)
#define ADOIN_ADOIN_OFFSET (0x00040000)
#define ADOIN_ADOIN_SYSCFG_OFFSET (0x00040400)

typedef struct adc_csr_baddr {
	volatile CsrBankAdccfg *adccfg;
	volatile CsrBankAdcctr *adcctr;
	volatile CsrBankAdoin *adoin;
	volatile CsrBankAdoin_syscfg *adoin_syscfg;
} AdcCsrBaddr;

/* Driver */
uint32_t hw_adc_env_irq_handler(volatile struct adc_csr_baddr *base);
int hw_adc_env_irq_unmask(volatile struct adc_csr_baddr *base, uint32_t ch);
int hw_adc_env_irq_mask(volatile struct adc_csr_baddr *base, uint32_t ch);
int hw_adc_env_irq_clear(volatile struct adc_csr_baddr *base, uint32_t ch);

void hw_adc_csr_init(void *addr, volatile struct adc_csr_baddr *base);
void hw_adc_init(volatile struct adc_csr_baddr *base);
void hw_adc_deinit(volatile struct adc_csr_baddr *base);
void hw_adc_workaround(volatile struct adc_csr_baddr *base);
void hw_adc_clk_start(volatile struct adc_csr_baddr *base);
void hw_adc_clk_stop(volatile struct adc_csr_baddr *base);
int hw_adc_env_query(volatile struct adc_csr_baddr *base, uint32_t ch);
int hw_adc_env_read(volatile struct adc_csr_baddr *base, uint32_t ch, int *val);

#endif /* HW_ADC_H_ */
