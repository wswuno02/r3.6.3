#ifndef ADODEC_H_
#define ADODEC_H_

#include "csr_bank_adodec.h"

typedef enum adodec_ch_mode {
	ADODEC_CH_MODE_PASS_ALL = 0,
	ADODEC_CH_MODE_PASS_NONE,
	ADODEC_CH_MODE_PASS_EVEN,
	ADODEC_CH_MODE_PASS_ODD,
	ADODEC_CH_MODE_DUPLICATE,
} AdodecChMode;

typedef enum adodec_compress_mode {
	ADODEC_COMPRESS_MODE_S16LE = 0,
	ADODEC_COMPRESS_MODE_S32LE,
	ADODEC_COMPRESS_MODE_A_LAW,
	ADODEC_COMPRESS_MODE_MU_LAW,
	ADODEC_COMPRESS_MODE_G726_4_LE,
	ADODEC_COMPRESS_MODE_G726_4_BE,
	ADODEC_COMPRESS_MODE_G726_2_LE,
	ADODEC_COMPRESS_MODE_G726_2_BE,
} AdodecCompressMode;

void adodec_clear(void);
void adodec_set_ch_mode(AdodecChMode mode);
AdodecChMode adodec_get_ch_mode(void);
int adodec_set_compress_mode(AdodecCompressMode mode);
void adodec_set_pre_shift(uint32_t shift);
void adodec_set_post_shift(uint32_t shift);
void adodec_set_gain(uint16_t gain);
uint16_t adodec_get_gain(void);

#endif /* ADODEC_H_ */
