#ifndef HW_AUDIO_H_
#define HW_AUDIO_H_

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

#include "hw_audio_in.h"
#include "hw_audio_out.h"

/* CSR Bank Offset */
#define ADOADC_CFG_ADCCFG_OFFSET (0x00000000)
#define ADOADC_CTR_ADCCTR_OFFSET (0x00008000)
#define ADODAC_CFG_ADACCFG_OFFSET (0x00010000)
#define ADODAC_CTR_ADAC_CTR_OFFSET (0x00020000)
#define ADOPREAMP_APREAMPCFG_OFFSET (0x00030000)
#define ADOIN_ADOIN_OFFSET (0x00040000)
#define ADOIN_ADOIN_SYSCFG_OFFSET (0x00040400)
#define ADOOUT_ADOOUT_OFFSET (0x00050000)
#define ADOOUT_ADOOUT_SYSCFG_OFFSET (0x00050400)
#define ADOR_AUDIOR_OFFSET (0x00060000)
#define ADOW_AUDIOW_OFFSET (0x00070000)
#define ADOI2STX_I2S_OFFSET (0x00080000)
#define ADOI2SRX_I2S_OFFSET (0x00090000)
#define ADOENC_ADOENC_OFFSET (0x000A0000)
#define ADODEC_ADODEC_OFFSET (0x000B0000)
#define ADOFADEIN_AUDIO_FADE_IN_OFFSET (0x000C0000)
#define ADOFADEOUT_AUDIO_FADE_OUT_OFFSET (0x000D0000)
#define ADOHPF_AUDIO_HPF_OFFSET (0x000E0000)
#define ADOANR_AUDIO_ANR_OFFSET (0x000F0000)
#define ADOAGC_AUDIO_AGC_OFFSET (0x00110000)

typedef struct audio_csr_baddr {
	volatile CsrBankAdccfg *adc_cfg;
	volatile CsrBankAdcctr *adc_ctr;
	volatile CsrBankAdaccfg *dac_cfg;
	volatile CsrBankAdac_ctr *dac_ctr;
	volatile CsrBankApreampcfg *preamp_cfg;
	volatile CsrBankAdoin *adoin;
	volatile CsrBankAdoin_syscfg *adoin_syscfg;
	volatile CsrBankAdoout *adoout;
	volatile CsrBankAdoout_syscfg *adoout_syscfg;
	volatile CsrBankAudior *audior;
	volatile CsrBankAudiow *audiow;
	volatile CsrBankI2s *i2s_out;
	volatile CsrBankI2s *i2s_in;
	volatile CsrBankAdoenc *adoenc;
	volatile CsrBankAdodec *adodec;
	volatile CsrBankAudio_fade_in *fade_in;
	volatile CsrBankAudio_fade_out *fade_out;
	volatile CsrBankAudio_hpf *hpf;
	volatile CsrBankAudio_anr *anr;
	volatile CsrBankAudio_agc *agc;
} AudioCsrBaddr;

void hw_audio_set_base(void *base);

#endif /* HW_AUDIO_H_ */
