#ifndef ADOR_H_
#define ADOR_H_

#include "csr_bank_audior.h"

void ador_set_dram_config(uint32_t col_addr_type, uint32_t bank_addr_type);
void ador_init(void);
void ador_start(void);
void ador_buffer_switch(void);
void ador_req_read_end(void);

void ador_set_burst_length(uint32_t length);
void ador_set_target_fifo_level(uint32_t level);
void ador_set_fifo_full_level(uint32_t level);
void ador_set_access_length(uint32_t length);
void ador_set_violation(uint32_t start, uint32_t end);
void ador_set_ini_addr_linear(uint32_t addr);
void ador_irq_clear_buffer_full_pulse_avail(void);
uint8_t ador_status_buffer_full_pulse_avail(void);
void ador_irq_mask_buffer_full_pulse_avail(uint8_t mask);
void ador_set_ts_scaler(uint32_t factor);
void ador_clear_ts(void);
void ador_set_sample_target(uint32_t target);
void ador_enable_ts(uint32_t en);
void ador_set_ts_clear_value(uint32_t value);
void ador_set_debug_mon_sel(uint32_t enable);

uint32_t ador_get_ts(void);
uint32_t ador_get_ts_last(void);
uint32_t ador_get_capture_sample_cnt(void);
uint32_t ador_get_sample_cnt(void);

#endif /* ADOR_H_ */
