#ifndef ANR_H_
#define ANR_H_

#include "csr_bank_audio_anr.h"

#define ANR_MODE_NORMAL (0)
#define ANR_MODE_DISABLE (1)
/* unit action */
void anr_set_anr0_coef(uint32_t mode, const uint32_t *weight);
void anr_set_anr1_coef(uint32_t mode, const uint32_t *weight);
void anr_set_anr0_mode(uint32_t mode);
void anr_set_anr1_mode(uint32_t mode);

#define anr_set_anr_coef(id, mode, weight) (anr_set_anr##id##coef(mode, weight))

#endif /* ANR_H_ */
