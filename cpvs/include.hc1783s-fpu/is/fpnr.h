#ifndef KYOTO_FPNR_H_
#define KYOTO_FPNR_H_

#include "is_utils.h"
#include "csr_bank_fpnr.h"

typedef enum is_fpnr_mode {
	IS_FPNR_MODE_NORMAL = 0,
	IS_FPNR_MODE_DISABLE = 1,
	IS_FPNR_MODE_NUM = 2,
} IsFpnrMode;

typedef struct is_fpnr_cfg {
	enum is_fpnr_mode mode;
	uint8_t offset_bw;
	uint8_t offset_shift_bit;
	uint8_t gain_bw;
	uint8_t gain_prec;
} IsFpnrCfg;

/* Frame start */
void hw_is_fpnr_set_frame_start(const enum is_kel_no no);

/* IRQ clear */
void hw_is_fpnr_set_irq_clear_frame_end(const enum is_kel_no no);

/* Frame end*/
uint8_t hw_is_fpnr_get_status_frame_end(const enum is_kel_no no);

/* IRQ mask */
void hw_is_fpnr_set_irq_mask_frame_end(const enum is_kel_no no, uint8_t irq_mask_frame_end);
uint8_t hw_is_fpnr_get_irq_mask_frame_end(const enum is_kel_no no);

/* Resolution */
void hw_is_fpnr_set_res(const enum is_kel_no no, const struct res *res);
void hw_is_fpnr_get_res(const enum is_kel_no no, struct res *res);

/* FPNR */
void hw_is_fpnr_set_roi_cfg(const enum is_kel_no no, const struct rect_point *cfg);
void hw_is_fpnr_get_roi_cfg(const enum is_kel_no no, struct rect_point *cfg);
void hw_is_fpnr_set_cfg(const enum is_kel_no no, const struct is_fpnr_cfg *cfg);
void hw_is_fpnr_get_cfg(const enum is_kel_no no, struct is_fpnr_cfg *cfg);

/* Debug */
void hw_is_fpnr_set_dbg_mon_sel(const enum is_kel_no no, uint8_t debug_mon_sel);
uint8_t hw_is_fpnr_get_dbg_mon_sel(const enum is_kel_no no);

/* ATPG */
void hw_is_fpnr_set_atpg_test_enable(const enum is_kel_no no, uint8_t atpg_test_enable);
uint8_t hw_is_fpnr_get_atpg_test_enable(const enum is_kel_no no);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//* for mpp is *//

/* IRQ mask */
void is_fpnr_set_irq_mask_frame_end(volatile CsrBankFpnr *csr, uint8_t irq_mask_frame_end);
uint8_t is_fpnr_get_irq_mask_frame_end(volatile CsrBankFpnr *csr);

/* Resolution */
void is_fpnr_set_res(volatile CsrBankFpnr *csr, const struct res *res);
void is_fpnr_get_res(volatile CsrBankFpnr *csr, struct res *res);

/* FPNR */
void is_fpnr_set_roi_cfg(volatile CsrBankFpnr *csr, const struct rect_point *cfg);
void is_fpnr_get_roi_cfg(volatile CsrBankFpnr *csr, struct rect_point *cfg);
void is_fpnr_set_cfg(volatile CsrBankFpnr *csr, const struct is_fpnr_cfg *cfg);
void is_fpnr_get_cfg(volatile CsrBankFpnr *csr, struct is_fpnr_cfg *cfg);

/* Debug */
void is_fpnr_set_dbg_mon_sel(volatile CsrBankFpnr *csr, uint8_t debug_mon_sel);
uint8_t is_fpnr_get_dbg_mon_sel(volatile CsrBankFpnr *csr);

/* ATPG */
void is_fpnr_set_atpg_test_enable(volatile CsrBankFpnr *csr, uint8_t atpg_test_enable);
uint8_t is_fpnr_get_atpg_test_enable(volatile CsrBankFpnr *csr);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//* need to remove *//
// Move to test
#define IS_FPNR_ATTR_NUM 3

typedef struct is_fpnr_resolution {
	uint32_t width;
	uint32_t height;
} IsFpnrResolution;

typedef struct is_fpnr_roi {
	uint32_t roi_sx;
	uint32_t roi_sy;
	uint32_t roi_ex;
	uint32_t roi_ey;
} IsFpnrRoi;

typedef struct is_fpnr_ctx {
	uint32_t fpnr_mode;
	IsFpnrResolution fpnr_res;
	IsFpnrRoi fpnr_roi;
	uint32_t fpnr_offset_bw;
	uint32_t fpnr_offset_shift_bit;
	uint32_t fpnr_gain_bw;
	uint32_t fpnr_gain_prec;
} IsFpnrCtx;

typedef struct is_fpnr_attr {
	uint32_t fpnr_mode;
	uint32_t fpnr_width;
	uint32_t fpnr_height;
	uint32_t fpnr_level;
} IsFpnrAttr;

void hw_is_fpnr_frame_start(void);
void hw_is_fpnr_irq_clear_frame_end(void);
uint32_t hw_is_fpnr_status_frame_end(void);
void hw_is_fpnr_irq_mask_frame_end(uint32_t irq_mask_frame_end);
void hw_is_fpnr_resolution(uint32_t width, uint32_t height);
void hw_is_fpnr_bypass(void);
void hw_is_fpnr_run(IsFpnrAttr *fpnrattr);
void hw_is_fpnr_set_ctx(IsFpnrCtx *fpnrctx, IsFpnrAttr *fpnrattr);
void hw_is_fpnr_set_reg(IsFpnrCtx *fpnrctx);
void hw_is_fpnr_debug_mon_sel(uint32_t debug_mon_sel);

#endif /* KYOTO_FPNR_H_ */