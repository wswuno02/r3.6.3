#ifndef KYOTO_BLS_H_
#define KYOTO_BLS_H_

#include "is_utils.h"
#include "csr_bank_bls.h"

typedef struct is_bls_stat_frame_ctrl {
	struct rect_point black_rgl;
	struct rect_point active_rgl;
	uint32_t level_pix_num[CFA_PHASE_NUM];
} IsBlsStatFrameCtrl;

typedef struct is_bls_stat {
	uint16_t level[CFA_PHASE_NUM];
	uint16_t remainder[CFA_PHASE_NUM];
} IsBlsStat;

/* Frame start */
void hw_is_bls_set_frame_start(const enum is_kel_no no);

/* IRQ clear */
void hw_is_bls_set_irq_clear_frame_end(const enum is_kel_no no);

/* Frame end*/
uint8_t hw_is_bls_get_status_frame_end(const enum is_kel_no no);

/* IRQ mask */
void hw_is_bls_set_irq_mask_frame_end(const enum is_kel_no no, uint8_t irq_mask_frame_end);
uint8_t hw_is_bls_get_irq_mask_frame_end(const enum is_kel_no no);

/* Resolution */
void hw_is_bls_set_res(const enum is_kel_no no, const struct res *res);
void hw_is_bls_get_res(const enum is_kel_no no, struct res *res);

/* Input format */
void hw_is_bls_set_input_format(const enum is_kel_no no, const struct is_input_format *cfg);
void hw_is_bls_get_input_format(const enum is_kel_no no, struct is_input_format *cfg);

/* Statistics */
void hw_is_bls_set_stat_frame_ctrl(const enum is_kel_no no, const struct is_bls_stat_frame_ctrl *cfg);
void hw_is_bls_get_stat_frame_ctrl(const enum is_kel_no no, struct is_bls_stat_frame_ctrl *cfg);
void hw_is_bls_set_stat_clear(const enum is_kel_no no, uint8_t clear);
uint8_t hw_is_bls_get_stat_clear(const enum is_kel_no no);
void hw_is_bls_set_stat_enable(const enum is_kel_no no, uint8_t enable);
uint8_t hw_is_bls_get_stat_enable(const enum is_kel_no no);
void hw_is_bls_get_stat(const enum is_kel_no no, struct is_bls_stat *stat);

/* Debug */
void hw_is_bls_set_dbg_mon_sel(const enum is_kel_no no, uint8_t debug_mon_sel);
uint8_t hw_is_bls_get_dbg_mon_sel(const enum is_kel_no no);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//* for mpp is *//

/* IRQ mask */
void is_bls_set_irq_mask_frame_end(volatile CsrBankBls *csr, uint8_t irq_mask_frame_end);
uint8_t is_bls_get_irq_mask_frame_end(volatile CsrBankBls *csr);

/* Resolution */
void is_bls_set_res(volatile CsrBankBls *csr, const struct res *res);
void is_bls_get_res(volatile CsrBankBls *csr, struct res *res);

/* Input format */
void is_bls_set_input_format(volatile CsrBankBls *csr, const struct is_input_format *cfg);
void is_bls_get_input_format(volatile CsrBankBls *csr, struct is_input_format *cfg);

/* Statistics */
void is_bls_set_stat_frame_ctrl(volatile CsrBankBls *csr, const struct is_bls_stat_frame_ctrl *cfg);
void is_bls_get_stat_frame_ctrl(volatile CsrBankBls *csr, struct is_bls_stat_frame_ctrl *cfg);
void is_bls_set_stat_clear(volatile CsrBankBls *csr, uint8_t clear);
uint8_t is_bls_get_stat_clear(volatile CsrBankBls *csr);
void is_bls_set_stat_enable(volatile CsrBankBls *csr, uint8_t enable);
uint8_t is_bls_get_stat_enable(volatile CsrBankBls *csr);
void is_bls_get_stat(volatile CsrBankBls *csr, struct is_bls_stat *stat);

/* Debug */
void is_bls_set_dbg_mon_sel(volatile CsrBankBls *csr, uint8_t debug_mon_sel);
uint8_t is_bls_get_dbg_mon_sel(volatile CsrBankBls *csr);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//* need to remove *//
// Move to test

#define IS_BLS_CFA_PHASE_IDX 3
#define IS_BLS_CFA_PHASE_NUM 16

typedef struct is_bls_resolution {
	uint32_t width;
	uint32_t height;
} IsBlsResolution;

typedef struct is_bls_cfa_set {
	uint32_t cfa_mode;
	uint32_t bayer_ini_phase;
	uint32_t cfa_phase[IS_BLS_CFA_PHASE_NUM];
} IsBlsCfaSet;

typedef struct is_bls_ctx {
	uint32_t bls_level_enable;
	uint32_t bls_level_clear;
	uint32_t bls_level_lf_sx;
	uint32_t bls_level_lf_ex;
	uint32_t bls_level_rt_sx;
	uint32_t bls_level_rt_ex;
	uint32_t bls_level_up_sy;
	uint32_t bls_level_up_ey;
	uint32_t bls_level_dn_sy;
	uint32_t bls_level_dn_ey;
	uint32_t bls_level_pix_num_g0;
	uint32_t bls_level_pix_num_r;
	uint32_t bls_level_pix_num_b;
	uint32_t bls_level_pix_num_g1;
	uint32_t bls_level_pix_num_s;
	IsBlsResolution bls_res;
	IsBlsCfaSet bls_cfa_set;

} IsBlsCtx;

typedef struct is_bls_attr {
	uint32_t bls_width;
	uint32_t bls_height;
	uint32_t bls_cfa_mode;
	uint32_t bls_bayer_ini_phase;
	uint32_t bls_level_enable;
	uint32_t bls_level_lf_sx;
	uint32_t bls_level_lf_ex;
	uint32_t bls_level_rt_sx;
	uint32_t bls_level_rt_ex;
	uint32_t bls_level_up_sy;
	uint32_t bls_level_up_ey;
	uint32_t bls_level_dn_sy;
	uint32_t bls_level_dn_ey;
	uint32_t bls_level_pix_num_g0;
	uint32_t bls_level_pix_num_r;
	uint32_t bls_level_pix_num_b;
	uint32_t bls_level_pix_num_g1;
	uint32_t bls_level_pix_num_s;
} IsBlsAttr;

void hw_is_bls_frame_start(void);
void hw_is_bls_irq_clear_frame_end(void);
uint32_t hw_is_bls_status_frame_end(void);
void hw_is_bls_irq_mask_frame_end(uint32_t irq_mask_frame_end);
void hw_is_bls_resolution(uint32_t width, uint32_t height);
void hw_is_bls_bypass(uint32_t cfa_mode, uint32_t bayer_ini_phase);
void hw_is_bls_run(IsBlsAttr *blsattr);
void hw_is_bls_set_ctx(IsBlsCtx *blsctx, IsBlsAttr *blsattr);
void hw_is_bls_set_reg(IsBlsCtx *blsctx);
void hw_is_bls_debug_mon_sel(uint32_t debug_mon_sel);

#endif /* KYOTO_BLS_H_ */