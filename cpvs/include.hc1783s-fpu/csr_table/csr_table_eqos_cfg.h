#ifndef CSR_TABLE_EQOS_CFG_H_
#define CSR_TABLE_EQOS_CFG_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_eqos_cfg[] =
{
  // WORD mac_ctrl
  { "mac_spec", 0x00000000,  0,  0,   CSR_RW, 0x00000000 },
  { "MAC_CTRL", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD div_ctrl
  { "mac_1000m_div_num_m1", 0x00000004,  7,  0,   CSR_RW, 0x00000002 },
  { "mac_100m_div_num_m1", 0x00000004, 15,  8,   CSR_RW, 0x0000000A },
  { "mac_10m_div_num_m1", 0x00000004, 23, 16,   CSR_RW, 0x00000064 },
  { "DIV_CTRL", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD div_sta
  { "rgmii_div_num_load", 0x00000008,  7,  0,   CSR_RO, 0x00000000 },
  { "rmii_div_num_load", 0x00000008, 15,  8,   CSR_RO, 0x00000000 },
  { "DIV_STA", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // WORD cfg_irq_sta_0
  { "status_perch_tx", 0x00000010,  0,  0,   CSR_RO, 0x00000000 },
  { "status_perch_rx", 0x00000010,  8,  8,   CSR_RO, 0x00000000 },
  { "status_lpi_exit", 0x00000010, 16, 16,   CSR_RO, 0x00000000 },
  { "status_axi_lp_done", 0x00000010, 24, 24,   CSR_RO, 0x00000000 },
  { "CFG_IRQ_STA_0", 0x00000010, 31, 0,   CSR_RO, 0x00000000 },
  // WORD cfg_irq_sta_1
  { "status_axi_lp_peri_exit", 0x00000014,  0,  0,   CSR_RO, 0x00000000 },
  { "CFG_IRQ_STA_1", 0x00000014, 31, 0,   CSR_RO, 0x00000000 },
  // WORD cfg_irq_ack_0
  { "irq_clear_perch_tx", 0x00000018,  0,  0,  CSR_W1P, 0x00000000 },
  { "irq_clear_perch_rx", 0x00000018,  8,  8,  CSR_W1P, 0x00000000 },
  { "irq_clear_lpi_exit", 0x00000018, 16, 16,  CSR_W1P, 0x00000000 },
  { "irq_clear_axi_lp_done", 0x00000018, 24, 24,  CSR_W1P, 0x00000000 },
  { "CFG_IRQ_ACK_0", 0x00000018, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD cfg_irq_ack_1
  { "irq_clear_axi_lp_peri_exit", 0x0000001C,  0,  0,  CSR_W1P, 0x00000000 },
  { "CFG_IRQ_ACK_1", 0x0000001C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD cfg_irq_msk_0
  { "irq_mask_perch_tx", 0x00000020,  0,  0,   CSR_RW, 0x00000000 },
  { "irq_mask_perch_rx", 0x00000020,  8,  8,   CSR_RW, 0x00000000 },
  { "irq_mask_lpi_exit", 0x00000020, 16, 16,   CSR_RW, 0x00000000 },
  { "irq_mask_axi_lp_done", 0x00000020, 24, 24,   CSR_RW, 0x00000000 },
  { "CFG_IRQ_MSK_0", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD cfg_irq_msk_1
  { "irq_mask_axi_lp_peri_exit", 0x00000024,  0,  0,   CSR_RW, 0x00000000 },
  { "CFG_IRQ_MSK_1", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD axi_lp_0
  { "lp_enter", 0x0000002C,  0,  0,  CSR_W1P, 0x00000000 },
  { "AXI_LP_0", 0x0000002C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD axi_lp_1
  { "lp_exit", 0x00000030,  0,  0,  CSR_W1P, 0x00000000 },
  { "AXI_LP_1", 0x00000030, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD axi_lp_2
  { "lp_peri_auto", 0x00000034,  0,  0,   CSR_RW, 0x00000000 },
  { "AXI_LP_2", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD axi_lp_3
  { "lp_status", 0x00000038,  0,  0,   CSR_RO, 0x00000000 },
  { "AXI_LP_3", 0x00000038, 31, 0,   CSR_RO, 0x00000000 },
  // WORD dbg_0
  { "debug_sel", 0x0000003C,  2,  0,   CSR_RW, 0x00000000 },
  { "DBG_0", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dbg_1
  { "debug_sta", 0x00000040, 31,  0,   CSR_RO, 0x00000000 },
  { "DBG_1", 0x00000040, 31, 0,   CSR_RO, 0x00000000 },
  // WORD phy
  { "phy_rst", 0x00000044,  0,  0,   CSR_RW, 0x00000000 },
  { "PHY", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mem_wrapper
  { "sd", 0x00000048,  8,  8,   CSR_RW, 0x00000000 },
  { "slp", 0x00000048, 16, 16,   CSR_RW, 0x00000000 },
  { "MEM_WRAPPER", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_EQOS_CFG_H_
