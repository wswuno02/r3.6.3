#ifndef CSR_TABLE_CK_H_
#define CSR_TABLE_CK_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_ck[] =
{
  // WORD hw_ckg_apb0
  { "dis_cg_apb_0", 0x00000000,  0,  0,   CSR_RW, 0x00000001 },
  { "cg_delay_m1_apb_0", 0x00000000, 10,  8,   CSR_RW, 0x00000002 },
  { "HW_CKG_APB0", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ca7_axi_enm
  { "aclkenm_num_m1", 0x00000004,  1,  0,   CSR_RW, 0x00000003 },
  { "aclkenm_init", 0x00000004,  8,  8,   CSR_RW, 0x00000001 },
  { "CA7_AXI_ENM", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ca7_apb_enm
  { "pclkendbg_num_m1", 0x00000008,  1,  0,   CSR_RW, 0x00000000 },
  { "pclkendbg_init", 0x00000008,  8,  8,   CSR_RW, 0x00000001 },
  { "CA7_APB_ENM", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_ddr
  { "cken_dramc", 0x0000000C,  0,  0,   CSR_RW, 0x00000001 },
  { "cken_dramc_hdr", 0x0000000C,  8,  8,   CSR_RW, 0x00000001 },
  { "CKG_DDR", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_axi_dramc
  { "cken_axi_dramc", 0x00000010,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_AXI_DRAMC", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_dma
  { "cken_dma", 0x00000014,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_DMA", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_apb_spi
  { "cken_apb_spi0", 0x00000018,  0,  0,   CSR_RW, 0x00000001 },
  { "cken_apb_spi1", 0x00000018,  8,  8,   CSR_RW, 0x00000001 },
  { "CKG_APB_SPI", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_apb_uart03
  { "cken_apb_uart0", 0x0000001C,  0,  0,   CSR_RW, 0x00000001 },
  { "cken_apb_uart1", 0x0000001C,  8,  8,   CSR_RW, 0x00000001 },
  { "cken_apb_uart2", 0x0000001C, 16, 16,   CSR_RW, 0x00000001 },
  { "cken_apb_uart3", 0x0000001C, 24, 24,   CSR_RW, 0x00000001 },
  { "CKG_APB_UART03", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_apb_uart45
  { "cken_apb_uart4", 0x00000020,  0,  0,   CSR_RW, 0x00000001 },
  { "cken_apb_uart5", 0x00000020,  8,  8,   CSR_RW, 0x00000001 },
  { "CKG_APB_UART45", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_apb_sdc
  { "cken_apb_sdc0", 0x00000024,  0,  0,   CSR_RW, 0x00000001 },
  { "cken_apb_sdc1", 0x00000024,  8,  8,   CSR_RW, 0x00000001 },
  { "CKG_APB_SDC", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_apb_emac
  { "cken_apb_emac", 0x00000028,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_APB_EMAC", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_axi_rom
  { "cken_axi_rom", 0x0000002C,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_AXI_ROM", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_axi_ram
  { "cken_axi_ram", 0x00000030,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_AXI_RAM", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_ahb0
  { "cken_ahb_master", 0x00000034,  0,  0,   CSR_RW, 0x00000001 },
  { "cken_ahb_usb", 0x00000034,  8,  8,   CSR_RW, 0x00000001 },
  { "CKG_AHB0", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_ahb1
  { "cken_ahb_sdc_0", 0x00000038,  0,  0,   CSR_RW, 0x00000001 },
  { "cken_ahb_sdc_1", 0x00000038,  8,  8,   CSR_RW, 0x00000001 },
  { "CKG_AHB1", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_emac
  { "cken_emac_rgmii", 0x0000003C,  0,  0,   CSR_RW, 0x00000001 },
  { "cken_emac_rmii", 0x0000003C,  8,  8,   CSR_RW, 0x00000001 },
  { "CKG_EMAC", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_qspi
  { "cken_qspi", 0x00000040,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_QSPI", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_isp
  { "cken_isp", 0x00000044,  0,  0,   CSR_RW, 0x00000001 },
  { "cken_vp", 0x00000044,  8,  8,   CSR_RW, 0x00000001 },
  { "CKG_ISP", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_snsr
  { "cken_sensor", 0x00000048,  0,  0,   CSR_RW, 0x00000001 },
  { "cken_senif", 0x00000048,  8,  8,   CSR_RW, 0x00000001 },
  { "CKG_SNSR", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_is
  { "cken_is", 0x0000004C,  8,  8,   CSR_RW, 0x00000001 },
  { "CKG_IS", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_disp
  { "cken_disp", 0x00000050,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_DISP", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_sdc
  { "cken_sdc_0", 0x00000054,  0,  0,   CSR_RW, 0x00000001 },
  { "cken_sdc_1", 0x00000054,  8,  8,   CSR_RW, 0x00000001 },
  { "CKG_SDC", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_enc
  { "cken_enc", 0x00000058,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_ENC", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_efuse
  { "cken_efuse", 0x0000005C,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_EFUSE", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_eirq
  { "cken_eirq", 0x00000060,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_EIRQ", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_islp
  { "cken_is_lp", 0x00000064,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_ISLP", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_i2cm
  { "cken_i2cm0", 0x00000068,  0,  0,   CSR_RW, 0x00000001 },
  { "cken_i2cm1", 0x00000068,  8,  8,   CSR_RW, 0x00000001 },
  { "CKG_I2CM", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_spi
  { "cken_spi0", 0x0000006C,  0,  0,   CSR_RW, 0x00000001 },
  { "cken_spi1", 0x0000006C,  8,  8,   CSR_RW, 0x00000001 },
  { "CKG_SPI", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_timer
  { "cken_timer0", 0x00000070,  0,  0,   CSR_RW, 0x00000001 },
  { "cken_timer1", 0x00000070,  8,  8,   CSR_RW, 0x00000001 },
  { "cken_timer2", 0x00000070, 16, 16,   CSR_RW, 0x00000001 },
  { "cken_timer3", 0x00000070, 24, 24,   CSR_RW, 0x00000001 },
  { "CKG_TIMER", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_pwm0
  { "cken_pwm_0", 0x00000074,  0,  0,   CSR_RW, 0x00000001 },
  { "cken_pwm_1", 0x00000074,  8,  8,   CSR_RW, 0x00000001 },
  { "cken_pwm_2", 0x00000074, 16, 16,   CSR_RW, 0x00000001 },
  { "cken_pwm_3", 0x00000074, 24, 24,   CSR_RW, 0x00000001 },
  { "CKG_PWM0", 0x00000074, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_pwm1
  { "cken_pwm_4", 0x00000078,  0,  0,   CSR_RW, 0x00000001 },
  { "cken_pwm_5", 0x00000078,  8,  8,   CSR_RW, 0x00000001 },
  { "CKG_PWM1", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_ado
  { "cken_audio_in", 0x0000007C,  0,  0,   CSR_RW, 0x00000001 },
  { "cken_audio_out", 0x0000007C,  8,  8,   CSR_RW, 0x00000001 },
  { "CKG_ADO", 0x0000007C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_usb
  { "cken_usb_utmi", 0x00000080,  8,  8,   CSR_RW, 0x00000001 },
  { "CKG_USB", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // WORD clk_def_ddr
  { "dramc_src_sel", 0x00000084,  0,  0,   CSR_RW, 0x00000000 },
  { "CLK_DEF_DDR", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD clk_def_ddr_axi
  { "axi_dramc_src_sel", 0x00000088,  0,  0,   CSR_RW, 0x00000000 },
  { "CLK_DEF_DDR_AXI", 0x00000088, 31, 0,   CSR_RW, 0x00000000 },
  // WORD clk_def_arm
  { "arm_src_sel", 0x0000008C,  0,  0,   CSR_RW, 0x00000000 },
  { "bus_src_sel", 0x0000008C,  8,  8,   CSR_RW, 0x00000000 },
  { "enm_mask_end_cnt", 0x0000008C, 23, 16,   CSR_RW, 0x00000010 },
  { "CLK_DEF_ARM", 0x0000008C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD clk_def_pwm2
  { "pwm_2_src_sel", 0x00000090,  0,  0,   CSR_RW, 0x00000000 },
  { "CLK_DEF_PWM2", 0x00000090, 31, 0,   CSR_RW, 0x00000000 },
  // WORD clk_def_pwm3
  { "pwm_3_src_sel", 0x00000094,  0,  0,   CSR_RW, 0x00000000 },
  { "CLK_DEF_PWM3", 0x00000094, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_disp_clk_div_sel
  { "disp_clk_div_sel", 0x00000098,  2,  0,   CSR_RW, 0x00000001 },
  { "WORD_DISP_CLK_DIV_SEL", 0x00000098, 31, 0,   CSR_RW, 0x00000000 },
  // WORD resv_0
  { "reserved0", 0x0000009C, 15,  0,   CSR_RW, 0x00000000 },
  { "RESV_0", 0x0000009C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD resv_1
  { "reserved1", 0x000000A0, 15,  0,   CSR_RW, 0x00000000 },
  { "RESV_1", 0x000000A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD resv_2
  { "reserved2", 0x000000A4, 15,  0,   CSR_RW, 0x00000000 },
  { "RESV_2", 0x000000A4, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_CK_H_
