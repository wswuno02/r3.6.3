#ifndef CSR_TABLE_ADOOUT_H_
#define CSR_TABLE_ADOOUT_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_adoout[] =
{
  // WORD adoout00
  { "audior_irq_clear_bw_insufficient", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "audior_irq_clear_buffer_full", 0x00000000,  8,  8,  CSR_W1P, 0x00000000 },
  { "audior_irq_clear_access_violation", 0x00000000, 16, 16,  CSR_W1P, 0x00000000 },
  { "audior_irq_clear_read_end", 0x00000000, 24, 24,  CSR_W1P, 0x00000000 },
  { "ADOOUT00", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD adoout01
  { "i2s_irq_clear_real_stop", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "i2s_irq_clear_error", 0x00000004,  8,  8,  CSR_W1P, 0x00000000 },
  { "ADOOUT01", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD adoout02
  { "adac_l_irq_clear_no_ready", 0x00000008,  0,  0,  CSR_W1P, 0x00000000 },
  { "adac_l_irq_clear_incr_arrival", 0x00000008,  8,  8,  CSR_W1P, 0x00000000 },
  { "adac_l_irq_clear_decr_arrival", 0x00000008, 16, 16,  CSR_W1P, 0x00000000 },
  { "ADOOUT02", 0x00000008, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD adoout03
  { "audior_status_bw_insufficient", 0x0000000C,  0,  0,   CSR_RO, 0x00000000 },
  { "audior_status_buffer_full", 0x0000000C,  8,  8,   CSR_RO, 0x00000000 },
  { "audior_status_access_violation", 0x0000000C, 16, 16,   CSR_RO, 0x00000000 },
  { "audior_status_read_end", 0x0000000C, 24, 24,   CSR_RO, 0x00000000 },
  { "ADOOUT03", 0x0000000C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD adoout04
  { "i2s_status_real_stop", 0x00000010,  0,  0,   CSR_RO, 0x00000000 },
  { "i2s_status_error", 0x00000010,  8,  8,   CSR_RO, 0x00000000 },
  { "ADOOUT04", 0x00000010, 31, 0,   CSR_RO, 0x00000000 },
  // WORD adoout05
  { "adac_l_status_no_ready", 0x00000014,  0,  0,   CSR_RO, 0x00000000 },
  { "adac_l_status_incr_arrival", 0x00000014,  8,  8,   CSR_RO, 0x00000000 },
  { "adac_l_status_decr_arrival", 0x00000014, 16, 16,   CSR_RO, 0x00000000 },
  { "ADOOUT05", 0x00000014, 31, 0,   CSR_RO, 0x00000000 },
  // WORD adoout06
  { "audior_irq_mask_bw_insufficient", 0x00000018,  0,  0,   CSR_RW, 0x00000001 },
  { "audior_irq_mask_buffer_full", 0x00000018,  8,  8,   CSR_RW, 0x00000001 },
  { "audior_irq_mask_access_violation", 0x00000018, 16, 16,   CSR_RW, 0x00000001 },
  { "audior_irq_mask_read_end", 0x00000018, 24, 24,   CSR_RW, 0x00000001 },
  { "ADOOUT06", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adoout07
  { "i2s_irq_mask_real_stop", 0x0000001C,  0,  0,   CSR_RW, 0x00000001 },
  { "i2s_irq_mask_error", 0x0000001C,  8,  8,   CSR_RW, 0x00000001 },
  { "ADOOUT07", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adoout08
  { "adac_l_irq_mask_no_ready", 0x00000020,  0,  0,   CSR_RW, 0x00000001 },
  { "adac_l_irq_mask_incr_arrival", 0x00000020,  8,  8,   CSR_RW, 0x00000001 },
  { "adac_l_irq_mask_decr_arrival", 0x00000020, 16, 16,   CSR_RW, 0x00000001 },
  { "ADOOUT08", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adoout09
  { "debug_mon_sel", 0x00000024,  2,  0,   CSR_RW, 0x00000000 },
  { "audio_destination", 0x00000024,  9,  8,   CSR_RW, 0x00000000 },
  { "ADOOUT09", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adoout10
  { "sd", 0x00000028,  0,  0,   CSR_RW, 0x00000000 },
  { "slp", 0x00000028,  8,  8,   CSR_RW, 0x00000000 },
  { "ADOOUT10", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adoout11
  { "debug_mon_reg", 0x0000002C, 31,  0,   CSR_RO, 0x00000000 },
  { "ADOOUT11", 0x0000002C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD adoout12
  { "reserved", 0x00000030, 31,  0,   CSR_RW, 0x00000000 },
  { "ADOOUT12", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adoupdate
  { "double_buf_update", 0x000000A0,  0,  0,  CSR_W1P, 0x00000000 },
  { "ADOUPDATE", 0x000000A0, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD adoupsamp
  { "up_sample_scale", 0x000000A4,  2,  0,   CSR_RW, 0x00000000 },
  { "ADOUPSAMP", 0x000000A4, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_ADOOUT_H_
