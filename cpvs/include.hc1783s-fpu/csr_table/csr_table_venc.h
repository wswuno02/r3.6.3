#ifndef CSR_TABLE_VENC_H_
#define CSR_TABLE_VENC_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_venc[] =
{
  // WORD venc00
  { "frame_start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "VENC00", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD venc01
  { "irq_clear_frame_end", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "irq_clear_mvr_frame_end", 0x00000004,  1,  1,  CSR_W1P, 0x00000000 },
  { "irq_clear_mvr_bw_insufficient", 0x00000004,  2,  2,  CSR_W1P, 0x00000000 },
  { "irq_clear_mvr_access_violation", 0x00000004,  4,  4,  CSR_W1P, 0x00000000 },
  { "irq_clear_refr_frame_end", 0x00000004,  5,  5,  CSR_W1P, 0x00000000 },
  { "irq_clear_refr_bw_insufficient", 0x00000004,  6,  6,  CSR_W1P, 0x00000000 },
  { "irq_clear_refr_access_violation", 0x00000004,  8,  8,  CSR_W1P, 0x00000000 },
  { "irq_clear_refw_frame_end", 0x00000004,  9,  9,  CSR_W1P, 0x00000000 },
  { "irq_clear_refw_bw_insufficient", 0x00000004, 10, 10,  CSR_W1P, 0x00000000 },
  { "irq_clear_refw_access_violation", 0x00000004, 12, 12,  CSR_W1P, 0x00000000 },
  { "irq_clear_seq_hdr_cont", 0x00000004, 13, 13,  CSR_W1P, 0x00000000 },
  { "VENC01", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD venc02
  { "status_frame_end", 0x00000008,  0,  0,   CSR_RO, 0x00000000 },
  { "status_mvr_frame_end", 0x00000008,  1,  1,   CSR_RO, 0x00000000 },
  { "status_mvr_bw_insufficient", 0x00000008,  2,  2,   CSR_RO, 0x00000000 },
  { "status_mvr_access_violation", 0x00000008,  4,  4,   CSR_RO, 0x00000000 },
  { "status_refr_frame_end", 0x00000008,  5,  5,   CSR_RO, 0x00000000 },
  { "status_refr_bw_insufficient", 0x00000008,  6,  6,   CSR_RO, 0x00000000 },
  { "status_refr_access_violation", 0x00000008,  8,  8,   CSR_RO, 0x00000000 },
  { "status_refw_frame_end", 0x00000008,  9,  9,   CSR_RO, 0x00000000 },
  { "status_refw_bw_insufficient", 0x00000008, 10, 10,   CSR_RO, 0x00000000 },
  { "status_refw_access_violation", 0x00000008, 12, 12,   CSR_RO, 0x00000000 },
  { "status_seq_hdr_cont", 0x00000008, 13, 13,   CSR_RO, 0x00000000 },
  { "VENC02", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // WORD venc03
  { "irq_mask_frame_end", 0x0000000C,  0,  0,   CSR_RW, 0x00000001 },
  { "irq_mask_mvr_frame_end", 0x0000000C,  1,  1,   CSR_RW, 0x00000001 },
  { "irq_mask_mvr_bw_insufficient", 0x0000000C,  2,  2,   CSR_RW, 0x00000001 },
  { "irq_mask_mvr_access_violation", 0x0000000C,  4,  4,   CSR_RW, 0x00000001 },
  { "irq_mask_refr_frame_end", 0x0000000C,  5,  5,   CSR_RW, 0x00000001 },
  { "irq_mask_refr_bw_insufficient", 0x0000000C,  6,  6,   CSR_RW, 0x00000001 },
  { "irq_mask_refr_access_violation", 0x0000000C,  8,  8,   CSR_RW, 0x00000001 },
  { "irq_mask_refw_frame_end", 0x0000000C,  9,  9,   CSR_RW, 0x00000001 },
  { "irq_mask_refw_bw_insufficient", 0x0000000C, 10, 10,   CSR_RW, 0x00000001 },
  { "irq_mask_refw_access_violation", 0x0000000C, 12, 12,   CSR_RW, 0x00000001 },
  { "irq_mask_seq_hdr_cont", 0x0000000C, 13, 13,   CSR_RW, 0x00000001 },
  { "VENC03", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_frame_size
  { "frm_mb_hor_num_m1", 0x0000001C, 11,  0,   CSR_RW, 0x00000077 },
  { "frm_mb_ver_num_m1", 0x0000001C, 27, 16,   CSR_RW, 0x00000043 },
  { "VENC_FRAME_SIZE", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_frame_type
  { "slice_type", 0x00000020,  1,  0,   CSR_RW, 0x00000000 },
  { "is_seq_header", 0x00000020,  8,  8,   CSR_RW, 0x00000000 },
  { "VENC_FRAME_TYPE", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_enc_tool_0
  { "entropy_coding_mode", 0x00000024,  0,  0,   CSR_RW, 0x00000000 },
  { "transform_8x8_mode_flag", 0x00000024, 16, 16,   CSR_RW, 0x00000000 },
  { "enable_long_term_ref_frm", 0x00000024, 24, 24,   CSR_RW, 0x00000000 },
  { "VENC_ENC_TOOL_0", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_enc_tool_1
  { "enable_sao", 0x00000028,  0,  0,   CSR_RW, 0x00000000 },
  { "hevc_seq_rc_enable", 0x00000028,  8,  8,   CSR_RW, 0x00000001 },
  { "VENC_ENC_TOOL_1", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_deblk_tool
  { "disable_deblocking_filter", 0x0000002C,  0,  0,   CSR_RW, 0x00000000 },
  { "alpha_c0_offset_div2", 0x0000002C, 11,  8,   CSR_RW, 0x00000000 },
  { "beta_offset_div2", 0x0000002C, 19, 16,   CSR_RW, 0x00000000 },
  { "VENC_DEBLK_TOOL", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_qp_mode
  { "frm_qp_change_mode", 0x00000030,  1,  0,   CSR_RW, 0x00000000 },
  { "qp_change_var_th", 0x00000030, 24,  8,   CSR_RW, 0x00000000 },
  { "VENC_QP_MODE", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_qp_chg_axis
  { "mb_qp_change_mb_x", 0x00000034, 11,  0,   CSR_RW, 0x00000000 },
  { "mb_qp_change_mb_y", 0x00000034, 27, 16,   CSR_RW, 0x00000000 },
  { "VENC_QP_CHG_AXIS", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_coef_cut
  { "luma_coef_cost_cut_th", 0x00000038,  7,  0,   CSR_RW, 0x00000004 },
  { "luma_8x8_coef_cost_cut_th", 0x00000038, 15,  8,   CSR_RW, 0x0000000A },
  { "VENC_COEF_CUT", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_mb_still
  { "set_normal_mb_still", 0x0000003C,  0,  0,   CSR_RW, 0x00000000 },
  { "set_roi_mb_still", 0x0000003C, 23, 16,   CSR_RW, 0x00000000 },
  { "VENC_MB_STILL", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_osd_qp_sel
  { "osd_qp_sel", 0x00000040,  1,  0,   CSR_RW, 0x00000000 },
  { "VENC_OSD_QP_SEL", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_roi_enable_0
  { "roi_0_enable", 0x00000044,  0,  0,   CSR_RW, 0x00000000 },
  { "roi_1_enable", 0x00000044,  8,  8,   CSR_RW, 0x00000000 },
  { "roi_2_enable", 0x00000044, 16, 16,   CSR_RW, 0x00000000 },
  { "roi_3_enable", 0x00000044, 24, 24,   CSR_RW, 0x00000000 },
  { "VENC_ROI_ENABLE_0", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_roi_enable_1
  { "roi_4_enable", 0x00000048,  0,  0,   CSR_RW, 0x00000000 },
  { "roi_5_enable", 0x00000048,  8,  8,   CSR_RW, 0x00000000 },
  { "roi_6_enable", 0x00000048, 16, 16,   CSR_RW, 0x00000000 },
  { "roi_7_enable", 0x00000048, 24, 24,   CSR_RW, 0x00000000 },
  { "VENC_ROI_ENABLE_1", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_roi_0_start_axis
  { "roi_0_start_mb_x", 0x0000004C, 11,  0,   CSR_RW, 0x00000000 },
  { "roi_0_start_mb_y", 0x0000004C, 27, 16,   CSR_RW, 0x00000000 },
  { "VENC_ROI_0_START_AXIS", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_roi_0_end_axis
  { "roi_0_end_mb_x", 0x00000050, 11,  0,   CSR_RW, 0x00000000 },
  { "roi_0_end_mb_y", 0x00000050, 27, 16,   CSR_RW, 0x00000000 },
  { "VENC_ROI_0_END_AXIS", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_roi_1_start_axis
  { "roi_1_start_mb_x", 0x00000054, 11,  0,   CSR_RW, 0x00000000 },
  { "roi_1_start_mb_y", 0x00000054, 27, 16,   CSR_RW, 0x00000000 },
  { "VENC_ROI_1_START_AXIS", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_roi_1_end_axis
  { "roi_1_end_mb_x", 0x00000058, 11,  0,   CSR_RW, 0x00000000 },
  { "roi_1_end_mb_y", 0x00000058, 27, 16,   CSR_RW, 0x00000000 },
  { "VENC_ROI_1_END_AXIS", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_roi_2_start_axis
  { "roi_2_start_mb_x", 0x0000005C, 11,  0,   CSR_RW, 0x00000000 },
  { "roi_2_start_mb_y", 0x0000005C, 27, 16,   CSR_RW, 0x00000000 },
  { "VENC_ROI_2_START_AXIS", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_roi_2_end_axis
  { "roi_2_end_mb_x", 0x00000060, 11,  0,   CSR_RW, 0x00000000 },
  { "roi_2_end_mb_y", 0x00000060, 27, 16,   CSR_RW, 0x00000000 },
  { "VENC_ROI_2_END_AXIS", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_roi_3_start_axis
  { "roi_3_start_mb_x", 0x00000064, 11,  0,   CSR_RW, 0x00000000 },
  { "roi_3_start_mb_y", 0x00000064, 27, 16,   CSR_RW, 0x00000000 },
  { "VENC_ROI_3_START_AXIS", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_roi_3_end_axis
  { "roi_3_end_mb_x", 0x00000068, 11,  0,   CSR_RW, 0x00000000 },
  { "roi_3_end_mb_y", 0x00000068, 27, 16,   CSR_RW, 0x00000000 },
  { "VENC_ROI_3_END_AXIS", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_roi_4_start_axis
  { "roi_4_start_mb_x", 0x0000006C, 11,  0,   CSR_RW, 0x00000000 },
  { "roi_4_start_mb_y", 0x0000006C, 27, 16,   CSR_RW, 0x00000000 },
  { "VENC_ROI_4_START_AXIS", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_roi_4_end_axis
  { "roi_4_end_mb_x", 0x00000070, 11,  0,   CSR_RW, 0x00000000 },
  { "roi_4_end_mb_y", 0x00000070, 27, 16,   CSR_RW, 0x00000000 },
  { "VENC_ROI_4_END_AXIS", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_roi_5_start_axis
  { "roi_5_start_mb_x", 0x00000074, 11,  0,   CSR_RW, 0x00000000 },
  { "roi_5_start_mb_y", 0x00000074, 27, 16,   CSR_RW, 0x00000000 },
  { "VENC_ROI_5_START_AXIS", 0x00000074, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_roi_5_end_axis
  { "roi_5_end_mb_x", 0x00000078, 11,  0,   CSR_RW, 0x00000000 },
  { "roi_5_end_mb_y", 0x00000078, 27, 16,   CSR_RW, 0x00000000 },
  { "VENC_ROI_5_END_AXIS", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_roi_6_start_axis
  { "roi_6_start_mb_x", 0x0000007C, 11,  0,   CSR_RW, 0x00000000 },
  { "roi_6_start_mb_y", 0x0000007C, 27, 16,   CSR_RW, 0x00000000 },
  { "VENC_ROI_6_START_AXIS", 0x0000007C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_roi_6_end_axis
  { "roi_6_end_mb_x", 0x00000080, 11,  0,   CSR_RW, 0x00000000 },
  { "roi_6_end_mb_y", 0x00000080, 27, 16,   CSR_RW, 0x00000000 },
  { "VENC_ROI_6_END_AXIS", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_roi_7_start_axis
  { "roi_7_start_mb_x", 0x00000084, 11,  0,   CSR_RW, 0x00000000 },
  { "roi_7_start_mb_y", 0x00000084, 27, 16,   CSR_RW, 0x00000000 },
  { "VENC_ROI_7_START_AXIS", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_roi_7_end_axis
  { "roi_7_end_mb_x", 0x00000088, 11,  0,   CSR_RW, 0x00000000 },
  { "roi_7_end_mb_y", 0x00000088, 27, 16,   CSR_RW, 0x00000000 },
  { "VENC_ROI_7_END_AXIS", 0x00000088, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_roi_qp_sel_0
  { "roi_0_qp_sel", 0x0000008C,  1,  0,   CSR_RW, 0x00000000 },
  { "roi_1_qp_sel", 0x0000008C,  9,  8,   CSR_RW, 0x00000000 },
  { "roi_2_qp_sel", 0x0000008C, 17, 16,   CSR_RW, 0x00000000 },
  { "roi_3_qp_sel", 0x0000008C, 25, 24,   CSR_RW, 0x00000000 },
  { "VENC_ROI_QP_SEL_0", 0x0000008C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_roi_qp_sel_1
  { "roi_4_qp_sel", 0x00000090,  1,  0,   CSR_RW, 0x00000000 },
  { "roi_5_qp_sel", 0x00000090,  9,  8,   CSR_RW, 0x00000000 },
  { "roi_6_qp_sel", 0x00000090, 17, 16,   CSR_RW, 0x00000000 },
  { "roi_7_qp_sel", 0x00000090, 25, 24,   CSR_RW, 0x00000000 },
  { "VENC_ROI_QP_SEL_1", 0x00000090, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_qp_idx_table
  { "slice_qp", 0x00000094,  5,  0,   CSR_RW, 0x0000001A },
  { "qp_1", 0x00000094, 13,  8,   CSR_RW, 0x0000001B },
  { "qp_2", 0x00000094, 21, 16,   CSR_RW, 0x00000014 },
  { "qp_3", 0x00000094, 29, 24,   CSR_RW, 0x00000020 },
  { "VENC_QP_IDX_TABLE", 0x00000094, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_qp_idx_0_lambda
  { "slice_qp_lambda", 0x00000098,  8,  0,   CSR_RW, 0x00000014 },
  { "slice_qp_sao_lambda", 0x00000098, 28, 16,   CSR_RW, 0x0000000C },
  { "VENC_QP_IDX_0_LAMBDA", 0x00000098, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_qp_idx_1_lambda
  { "qp_1_lambda", 0x0000009C,  8,  0,   CSR_RW, 0x00000018 },
  { "qp_1_sao_lambda", 0x0000009C, 28, 16,   CSR_RW, 0x0000000F },
  { "VENC_QP_IDX_1_LAMBDA", 0x0000009C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_qp_idx_2_lambda
  { "qp_2_lambda", 0x000000A0,  8,  0,   CSR_RW, 0x0000000C },
  { "qp_2_sao_lambda", 0x000000A0, 28, 16,   CSR_RW, 0x00000003 },
  { "VENC_QP_IDX_2_LAMBDA", 0x000000A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_qp_idx_3_lambda
  { "qp_3_lambda", 0x000000A4,  8,  0,   CSR_RW, 0x00000028 },
  { "qp_3_sao_lambda", 0x000000A4, 28, 16,   CSR_RW, 0x0000002F },
  { "VENC_QP_IDX_3_LAMBDA", 0x000000A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_hdr_setting
  { "hdr_data_length", 0x000000AC,  7,  0,   CSR_RW, 0x00000000 },
  { "hdr_data_last", 0x000000AC, 16, 16,   CSR_RW, 0x00000001 },
  { "VENC_HDR_SETTING", 0x000000AC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_frame_byte
  { "frame_bs_byte_length", 0x000000B0, 31,  0,   CSR_RO, 0x00000000 },
  { "VENC_FRAME_BYTE", 0x000000B0, 31, 0,   CSR_RO, 0x00000000 },
  // WORD venc_roi_0_bit
  { "roi_0_frame_bit_length", 0x000000B4, 31,  0,   CSR_RO, 0x00000000 },
  { "VENC_ROI_0_BIT", 0x000000B4, 31, 0,   CSR_RO, 0x00000000 },
  // WORD venc_roi_1_bit
  { "roi_1_frame_bit_length", 0x000000B8, 31,  0,   CSR_RO, 0x00000000 },
  { "VENC_ROI_1_BIT", 0x000000B8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD venc_roi_2_bit
  { "roi_2_frame_bit_length", 0x000000BC, 31,  0,   CSR_RO, 0x00000000 },
  { "VENC_ROI_2_BIT", 0x000000BC, 31, 0,   CSR_RO, 0x00000000 },
  // WORD venc_roi_3_bit
  { "roi_3_frame_bit_length", 0x000000C0, 31,  0,   CSR_RO, 0x00000000 },
  { "VENC_ROI_3_BIT", 0x000000C0, 31, 0,   CSR_RO, 0x00000000 },
  // WORD venc_roi_4_bit
  { "roi_4_frame_bit_length", 0x000000C4, 31,  0,   CSR_RO, 0x00000000 },
  { "VENC_ROI_4_BIT", 0x000000C4, 31, 0,   CSR_RO, 0x00000000 },
  // WORD venc_roi_5_bit
  { "roi_5_frame_bit_length", 0x000000C8, 31,  0,   CSR_RO, 0x00000000 },
  { "VENC_ROI_5_BIT", 0x000000C8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD venc_roi_6_bit
  { "roi_6_frame_bit_length", 0x000000CC, 31,  0,   CSR_RO, 0x00000000 },
  { "VENC_ROI_6_BIT", 0x000000CC, 31, 0,   CSR_RO, 0x00000000 },
  // WORD venc_roi_7_bit
  { "roi_7_frame_bit_length", 0x000000D0, 31,  0,   CSR_RO, 0x00000000 },
  { "VENC_ROI_7_BIT", 0x000000D0, 31, 0,   CSR_RO, 0x00000000 },
  // WORD venc_normal_bit
  { "nor_frame_bit_length", 0x000000D4, 31,  0,   CSR_RO, 0x00000000 },
  { "VENC_NORMAL_BIT", 0x000000D4, 31, 0,   CSR_RO, 0x00000000 },
  // WORD venc_osd_bit
  { "osd_frame_bit_length", 0x000000D8, 31,  0,   CSR_RO, 0x00000000 },
  { "VENC_OSD_BIT", 0x000000D8, 31, 0,   CSR_RO, 0x00000000 },
  // WORD venc_sr_up_set
  { "sr_up_mv_range", 0x000000DC,  2,  0,   CSR_RW, 0x00000000 },
  { "sr_up_mv_disable", 0x000000DC,  8,  8,   CSR_RW, 0x00000000 },
  { "VENC_SR_UP_SET", 0x000000DC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_sr_search_set
  { "sr_max_hor_range", 0x000000E0,  7,  0,   CSR_RW, 0x00000070 },
  { "sr_max_ver_range", 0x000000E0, 14,  8,   CSR_RW, 0x00000040 },
  { "sr_max_levelc_ver_range", 0x000000E0, 22, 16,   CSR_RW, 0x00000040 },
  { "sr_extend_refr", 0x000000E0, 24, 24,   CSR_RW, 0x00000000 },
  { "VENC_SR_SEARCH_SET", 0x000000E0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_merge_mv_cand
  { "max_num_merge_cand", 0x000000E4,  2,  0,   CSR_RW, 0x00000005 },
  { "VENC_MERGE_MV_CAND", 0x000000E4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_mv_quality
  { "texture_minmax_threshold", 0x000000E8,  3,  0,   CSR_RW, 0x00000004 },
  { "apply_mv_valid", 0x000000E8,  8,  8,   CSR_RW, 0x00000000 },
  { "VENC_MV_QUALITY", 0x000000E8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_inter_mode_param
  { "inter_2nx2n_offset_ratio", 0x000000EC,  6,  0,   CSR_RW, 0x00000000 },
  { "inter_2nxn_offset_ratio", 0x000000EC, 14,  8,   CSR_RW, 0x00000000 },
  { "inter_nx2n_offset_ratio", 0x000000EC, 22, 16,   CSR_RW, 0x00000000 },
  { "inter_nxn_offset_ratio", 0x000000EC, 30, 24,   CSR_RW, 0x00000000 },
  { "VENC_INTER_MODE_PARAM", 0x000000EC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_intra_mode_param
  { "intra_2nx2n_offset_ratio", 0x000000F0,  6,  0,   CSR_RW, 0x00000000 },
  { "intra_nxn_offset_ratio", 0x000000F0, 14,  8,   CSR_RW, 0x00000000 },
  { "intra_cip_offset_enable", 0x000000F0, 16, 16,   CSR_RW, 0x00000001 },
  { "VENC_INTRA_MODE_PARAM", 0x000000F0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_hdr_data_0
  { "hdr_data_0", 0x000000F4, 31,  0,   CSR_RW, 0x00000000 },
  { "VENC_HDR_DATA_0", 0x000000F4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_hdr_data_1
  { "hdr_data_1", 0x000000F8, 31,  0,   CSR_RW, 0x00000000 },
  { "VENC_HDR_DATA_1", 0x000000F8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_hdr_data_2
  { "hdr_data_2", 0x000000FC, 31,  0,   CSR_RW, 0x00000000 },
  { "VENC_HDR_DATA_2", 0x000000FC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_hdr_data_3
  { "hdr_data_3", 0x00000100, 31,  0,   CSR_RW, 0x00000000 },
  { "VENC_HDR_DATA_3", 0x00000100, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_minmax_ratio_0
  { "minmax_0_ratio", 0x00000104,  4,  0,   CSR_RW, 0x00000010 },
  { "minmax_1_ratio", 0x00000104, 12,  8,   CSR_RW, 0x00000010 },
  { "minmax_2_ratio", 0x00000104, 20, 16,   CSR_RW, 0x0000000C },
  { "minmax_3_ratio", 0x00000104, 28, 24,   CSR_RW, 0x0000000A },
  { "VENC_MINMAX_RATIO_0", 0x00000104, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_minmax_ratio_1
  { "minmax_4_ratio", 0x00000108,  4,  0,   CSR_RW, 0x00000008 },
  { "minmax_5_ratio", 0x00000108, 12,  8,   CSR_RW, 0x00000007 },
  { "minmax_6_ratio", 0x00000108, 20, 16,   CSR_RW, 0x00000006 },
  { "minmax_7_ratio", 0x00000108, 28, 24,   CSR_RW, 0x00000006 },
  { "VENC_MINMAX_RATIO_1", 0x00000108, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_minmax_ratio_2
  { "minmax_8_ratio", 0x0000010C,  4,  0,   CSR_RW, 0x00000006 },
  { "minmax_9_ratio", 0x0000010C, 12,  8,   CSR_RW, 0x00000005 },
  { "minmax_10_ratio", 0x0000010C, 20, 16,   CSR_RW, 0x00000005 },
  { "minmax_11_ratio", 0x0000010C, 28, 24,   CSR_RW, 0x00000004 },
  { "VENC_MINMAX_RATIO_2", 0x0000010C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_minmax_ratio_3
  { "minmax_12_ratio", 0x00000110,  4,  0,   CSR_RW, 0x00000004 },
  { "minmax_13_ratio", 0x00000110, 12,  8,   CSR_RW, 0x00000003 },
  { "minmax_14_ratio", 0x00000110, 20, 16,   CSR_RW, 0x00000003 },
  { "minmax_15_ratio", 0x00000110, 28, 24,   CSR_RW, 0x00000002 },
  { "VENC_MINMAX_RATIO_3", 0x00000110, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_minmax_ratio_4
  { "minmax_16_ratio", 0x00000114,  4,  0,   CSR_RW, 0x00000002 },
  { "minmax_other_ratio", 0x00000114, 12,  8,   CSR_RW, 0x00000001 },
  { "VENC_MINMAX_RATIO_4", 0x00000114, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_hevc_intra_mode_0
  { "hevc_i8_mode_num_m1", 0x00000118,  5,  0,   CSR_RW, 0x00000006 },
  { "hevc_i16_mode_num_m1", 0x00000118, 13,  8,   CSR_RW, 0x00000006 },
  { "hevc_cip_mode_num_m1", 0x00000118, 18, 16,   CSR_RW, 0x00000004 },
  { "VENC_HEVC_INTRA_MODE_0", 0x00000118, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_hevc_intra_mode_1
  { "hevc_intra_i8_mode_dir_th", 0x0000011C, 13,  0,   CSR_RW, 0x00000100 },
  { "hevc_intra_i16_mode_dir_th", 0x0000011C, 31, 16,   CSR_RW, 0x00000400 },
  { "VENC_HEVC_INTRA_MODE_1", 0x0000011C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_skip_cu_num
  { "skip_cu_num", 0x00000120, 18,  0,   CSR_RO, 0x00000000 },
  { "VENC_SKIP_CU_NUM", 0x00000120, 31, 0,   CSR_RO, 0x00000000 },
  // WORD venc_inter_2nx2n_cu_num
  { "inter_2nx2n_cu_num", 0x00000124, 18,  0,   CSR_RO, 0x00000000 },
  { "VENC_INTER_2NX2N_CU_NUM", 0x00000124, 31, 0,   CSR_RO, 0x00000000 },
  // WORD venc_inter_2nxn_cu_num
  { "inter_2nxn_cu_num", 0x00000128, 18,  0,   CSR_RO, 0x00000000 },
  { "VENC_INTER_2NXN_CU_NUM", 0x00000128, 31, 0,   CSR_RO, 0x00000000 },
  // WORD venc_inter_nx2n_cu_num
  { "inter_nx2n_cu_num", 0x0000012C, 18,  0,   CSR_RO, 0x00000000 },
  { "VENC_INTER_NX2N_CU_NUM", 0x0000012C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD venc_inter_nxn_cu_num
  { "inter_nxn_cu_num", 0x00000130, 18,  0,   CSR_RO, 0x00000000 },
  { "VENC_INTER_NXN_CU_NUM", 0x00000130, 31, 0,   CSR_RO, 0x00000000 },
  // WORD venc_intra_2nx2n_cu_num
  { "intra_2nx2n_cu_num", 0x00000134, 18,  0,   CSR_RO, 0x00000000 },
  { "VENC_INTRA_2Nx2N_CU_NUM", 0x00000134, 31, 0,   CSR_RO, 0x00000000 },
  // WORD venc_intra_nxn_cu_num
  { "intra_nxn_cu_num", 0x00000138, 18,  0,   CSR_RO, 0x00000000 },
  { "VENC_INTRA_NXN_CU_NUM", 0x00000138, 31, 0,   CSR_RO, 0x00000000 },
  // WORD venc_debug_mon_sel
  { "debug_mon_sel", 0x0000013C,  2,  0,   CSR_RW, 0x00000000 },
  { "VENC_DEBUG_MON_SEL", 0x0000013C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_debug_mon_reg
  { "debug_mon_reg", 0x00000140, 31,  0,   CSR_RO, 0x00000000 },
  { "VENC_DEBUG_MON_REG", 0x00000140, 31, 0,   CSR_RO, 0x00000000 },
  // WORD venc_reserved_0
  { "reserved_0", 0x00000144, 31,  0,   CSR_RW, 0x00000000 },
  { "VENC_RESERVED_0", 0x00000144, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_reserved_1
  { "reserved_1", 0x00000148, 31,  0,   CSR_RW, 0x00000000 },
  { "VENC_RESERVED_1", 0x00000148, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_reserved_2
  { "reserved_2", 0x0000014C, 31,  0,   CSR_RW, 0x00000000 },
  { "VENC_RESERVED_2", 0x0000014C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD venc_reserved_3
  { "reserved_3", 0x00000150, 31,  0,   CSR_RW, 0x00000000 },
  { "VENC_RESERVED_3", 0x00000150, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_VENC_H_
