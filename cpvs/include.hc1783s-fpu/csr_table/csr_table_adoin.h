#ifndef CSR_TABLE_ADOIN_H_
#define CSR_TABLE_ADOIN_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_adoin[] =
{
  // WORD adoin00
  { "audiow_irq_clear_bw_insufficient", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "audiow_irq_clear_buffer_full", 0x00000000,  8,  8,  CSR_W1P, 0x00000000 },
  { "audiow_irq_clear_access_violation", 0x00000000, 16, 16,  CSR_W1P, 0x00000000 },
  { "audiow_irq_clear_write_end", 0x00000000, 24, 24,  CSR_W1P, 0x00000000 },
  { "ADOIN00", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD adoin01
  { "i2s_irq_clear_real_stop", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "i2s_irq_clear_error", 0x00000004,  8,  8,  CSR_W1P, 0x00000000 },
  { "ADOIN01", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD adoin02
  { "adc_irq_clear_adc_real_stop", 0x00000008,  0,  0,  CSR_W1P, 0x00000000 },
  { "adc_irq_clear_no_ack", 0x00000008,  8,  8,  CSR_W1P, 0x00000000 },
  { "adc_irq_clear_no_eoc", 0x00000008, 16, 16,  CSR_W1P, 0x00000000 },
  { "ADOIN02", 0x00000008, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD adoin03
  { "adc_irq_clear_env_de_0", 0x0000000C,  0,  0,  CSR_W1P, 0x00000000 },
  { "adc_irq_clear_env_de_1", 0x0000000C,  8,  8,  CSR_W1P, 0x00000000 },
  { "adc_irq_clear_env_de_2", 0x0000000C, 16, 16,  CSR_W1P, 0x00000000 },
  { "adc_irq_clear_env_de_3", 0x0000000C, 24, 24,  CSR_W1P, 0x00000000 },
  { "ADOIN03", 0x0000000C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD adoin04
  { "audiow_status_bw_insufficient", 0x00000010,  0,  0,   CSR_RO, 0x00000000 },
  { "audiow_status_buffer_full", 0x00000010,  8,  8,   CSR_RO, 0x00000000 },
  { "audiow_status_access_violation", 0x00000010, 16, 16,   CSR_RO, 0x00000000 },
  { "audiow_status_write_end", 0x00000010, 24, 24,   CSR_RO, 0x00000000 },
  { "ADOIN04", 0x00000010, 31, 0,   CSR_RO, 0x00000000 },
  // WORD adoin05
  { "i2s_status_real_stop", 0x00000014,  0,  0,   CSR_RO, 0x00000000 },
  { "i2s_status_error", 0x00000014,  8,  8,   CSR_RO, 0x00000000 },
  { "ADOIN05", 0x00000014, 31, 0,   CSR_RO, 0x00000000 },
  // WORD adoin06
  { "adc_status_adc_real_stop", 0x00000018,  0,  0,   CSR_RO, 0x00000000 },
  { "adc_status_no_ack", 0x00000018,  8,  8,   CSR_RO, 0x00000000 },
  { "adc_status_no_eoc", 0x00000018, 16, 16,   CSR_RO, 0x00000000 },
  { "ADOIN06", 0x00000018, 31, 0,   CSR_RO, 0x00000000 },
  // WORD adoin07
  { "adc_status_env_de_0", 0x0000001C,  0,  0,   CSR_RO, 0x00000000 },
  { "adc_status_env_de_1", 0x0000001C,  8,  8,   CSR_RO, 0x00000000 },
  { "adc_status_env_de_2", 0x0000001C, 16, 16,   CSR_RO, 0x00000000 },
  { "adc_status_env_de_3", 0x0000001C, 24, 24,   CSR_RO, 0x00000000 },
  { "ADOIN07", 0x0000001C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD adoin08
  { "audiow_irq_mask_bw_insufficient", 0x00000020,  0,  0,   CSR_RW, 0x00000001 },
  { "audiow_irq_mask_buffer_full", 0x00000020,  8,  8,   CSR_RW, 0x00000001 },
  { "audiow_irq_mask_access_violation", 0x00000020, 16, 16,   CSR_RW, 0x00000001 },
  { "audiow_irq_mask_write_end", 0x00000020, 24, 24,   CSR_RW, 0x00000001 },
  { "ADOIN08", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adoin09
  { "i2s_irq_mask_real_stop", 0x00000024,  0,  0,   CSR_RW, 0x00000001 },
  { "i2s_irq_mask_error", 0x00000024,  8,  8,   CSR_RW, 0x00000001 },
  { "ADOIN09", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adoin10
  { "adc_irq_mask_adc_real_stop", 0x00000028,  0,  0,   CSR_RW, 0x00000001 },
  { "adc_irq_mask_no_ack", 0x00000028,  8,  8,   CSR_RW, 0x00000001 },
  { "adc_irq_mask_no_eoc", 0x00000028, 16, 16,   CSR_RW, 0x00000001 },
  { "ADOIN10", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adoin11
  { "adc_irq_mask_env_de_0", 0x0000002C,  0,  0,   CSR_RW, 0x00000001 },
  { "adc_irq_mask_env_de_1", 0x0000002C,  8,  8,   CSR_RW, 0x00000001 },
  { "adc_irq_mask_env_de_2", 0x0000002C, 16, 16,   CSR_RW, 0x00000001 },
  { "adc_irq_mask_env_de_3", 0x0000002C, 24, 24,   CSR_RW, 0x00000001 },
  { "ADOIN11", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adoin12
  { "debug_mon_sel", 0x00000030,  1,  0,   CSR_RW, 0x00000000 },
  { "ADOIN12", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adoin13
  { "sd", 0x00000034,  0,  0,   CSR_RW, 0x00000000 },
  { "slp", 0x00000034,  8,  8,   CSR_RW, 0x00000000 },
  { "ADOIN13", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adoin14
  { "debug_mon_reg", 0x00000038, 31,  0,   CSR_RO, 0x00000000 },
  { "ADOIN14", 0x00000038, 31, 0,   CSR_RO, 0x00000000 },
  // WORD adoin15
  { "adc_irq_clear_env_de_4", 0x0000003C,  0,  0,  CSR_W1P, 0x00000000 },
  { "adc_irq_clear_env_de_5", 0x0000003C,  8,  8,  CSR_W1P, 0x00000000 },
  { "ADOIN15", 0x0000003C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD adoin16
  { "adc_status_env_de_4", 0x00000040,  0,  0,   CSR_RO, 0x00000000 },
  { "adc_status_env_de_5", 0x00000040,  8,  8,   CSR_RO, 0x00000000 },
  { "ADOIN16", 0x00000040, 31, 0,   CSR_RO, 0x00000000 },
  // WORD adoin17
  { "adc_irq_mask_env_de_4", 0x00000044,  0,  0,   CSR_RW, 0x00000001 },
  { "adc_irq_mask_env_de_5", 0x00000044,  8,  8,   CSR_RW, 0x00000001 },
  { "ADOIN17", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD adoin18
  { "reserved", 0x00000048, 31,  0,   CSR_RW, 0x00000000 },
  { "ADOIN18", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD src_sel
  { "audio_source", 0x000000A8,  0,  0,   CSR_RW, 0x00000000 },
  { "audio_source_ack_not_sel", 0x000000A8,  8,  8,   CSR_RW, 0x00000000 },
  { "SRC_SEL", 0x000000A8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dn_bypass
  { "down_sample_bypass", 0x000000AC,  0,  0,   CSR_RW, 0x00000000 },
  { "DN_BYPASS", 0x000000AC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_double_buf_update
  { "double_buf_update", 0x000000B0,  0,  0,  CSR_W1P, 0x00000000 },
  { "WORD_DOUBLE_BUF_UPDATE", 0x000000B0, 31, 0,  CSR_W1P, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_ADOIN_H_
