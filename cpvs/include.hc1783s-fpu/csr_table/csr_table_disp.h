#ifndef CSR_TABLE_DISP_H_
#define CSR_TABLE_DISP_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_disp[] =
{
  // WORD efuse_vio
  { "disp_efuse_resolution_violation", 0x00000000,  0,  0,   CSR_RO, 0x00000000 },
  { "disp_efuse_data_rate_violation", 0x00000000,  8,  8,   CSR_RO, 0x00000000 },
  { "EFUSE_VIO", 0x00000000, 31, 0,   CSR_RO, 0x00000000 },
  // WORD pg_broadcast
  { "pg_broadcast_to_cs_enable", 0x00000004,  0,  0,   CSR_RW, 0x00000001 },
  { "pg_broadcast_to_pg_mux_enable", 0x00000004,  8,  8,   CSR_RW, 0x00000000 },
  { "PG_BROADCAST", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pg_mux_broadcast
  { "pg_mux_broadcast_to_420to422_enable", 0x00000008,  0,  0,   CSR_RW, 0x00000000 },
  { "pg_mux_broadcast_to_dosd_mux_enable", 0x00000008,  8,  8,   CSR_RW, 0x00000001 },
  { "pg_mux_broadcast_to_out_mux_enable", 0x00000008, 16, 16,   CSR_RW, 0x00000000 },
  { "PG_MUX_BROADCAST", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD yuv422_broadcast
  { "yuv422_broadcast_to_yuv444_enable", 0x0000000C,  0,  0,   CSR_RW, 0x00000000 },
  { "yuv422_broadcast_to_dosd_mux_enable", 0x0000000C,  8,  8,   CSR_RW, 0x00000001 },
  { "YUV422_BROADCAST", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dosd_broadcast
  { "dosd_broadcast_to_dcst_enable", 0x00000010,  0,  0,   CSR_RW, 0x00000001 },
  { "dosd_broadcast_to_out_mux_enable", 0x00000010,  8,  8,   CSR_RW, 0x00000000 },
  { "dosd_broadcast_to_dcs_enable", 0x00000010, 16, 16,   CSR_RW, 0x00000000 },
  { "DOSD_BROADCAST", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dgma_broadcast
  { "dgma_broadcast_to_dcfa_enable", 0x00000014,  0,  0,   CSR_RW, 0x00000001 },
  { "dgma_broadcast_to_out_mux_enable", 0x00000014,  8,  8,   CSR_RW, 0x00000000 },
  { "DGMA_BROADCAST", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sel0
  { "pg_sel", 0x00000018,  0,  0,   CSR_RW, 0x00000000 },
  { "dosd_sel", 0x00000018,  9,  8,   CSR_RW, 0x00000000 },
  { "out_sel", 0x00000018, 18, 16,   CSR_RW, 0x00000000 },
  { "SEL0", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD fn_bypass
  { "disp_cs_bypass", 0x0000001C,  0,  0,   CSR_RW, 0x00000000 },
  { "disp_dosd_bypass", 0x0000001C,  8,  8,   CSR_RW, 0x00000000 },
  { "FN_BYPASS", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD buf_update
  { "double_buf_update", 0x00000020,  0,  0,  CSR_W1P, 0x00000000 },
  { "BUF_UPDATE", 0x00000020, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD ack_not_sel0
  { "pg_broadcast_ack_not_sel", 0x00000024,  0,  0,   CSR_RW, 0x00000000 },
  { "pg_mux_broadcast_ack_not_sel", 0x00000024,  8,  8,   CSR_RW, 0x00000000 },
  { "yuv422_broadcast_ack_not_sel", 0x00000024, 16, 16,   CSR_RW, 0x00000000 },
  { "dosd_broadcast_ack_not_sel", 0x00000024, 24, 24,   CSR_RW, 0x00000000 },
  { "ACK_NOT_SEL0", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ack_not_sel1
  { "dgma_broadcast_ack_not_sel", 0x00000028,  0,  0,   CSR_RW, 0x00000000 },
  { "pg_mux_ack_not_sel", 0x00000028,  8,  8,   CSR_RW, 0x00000000 },
  { "dosd_mux_ack_not_sel", 0x00000028, 16, 16,   CSR_RW, 0x00000000 },
  { "out_mux_ack_not_sel", 0x00000028, 24, 24,   CSR_RW, 0x00000000 },
  { "ACK_NOT_SEL1", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mask_en
  { "ready_en_00", 0x0000002C,  0,  0,   CSR_RW, 0x00000001 },
  { "ready_en_01", 0x0000002C,  8,  8,   CSR_RW, 0x00000001 },
  { "ready_en_10", 0x0000002C, 16, 16,   CSR_RW, 0x00000001 },
  { "ready_en_11", 0x0000002C, 24, 24,   CSR_RW, 0x00000001 },
  { "MASK_EN", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD resoltion
  { "width_o", 0x00000030, 15,  0,   CSR_RW, 0x00000780 },
  { "RESOLTION", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD prd_mode
  { "pixel_read_decode_mode", 0x00000034,  0,  0,   CSR_RW, 0x00000001 },
  { "PRD_MODE", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD mem_lp_ctrl
  { "sd", 0x00000038,  0,  0,   CSR_RW, 0x00000000 },
  { "slp", 0x00000038,  8,  8,   CSR_RW, 0x00000000 },
  { "sd_senif", 0x00000038, 16, 16,   CSR_RW, 0x00000000 },
  { "slp_senif", 0x00000038, 24, 24,   CSR_RW, 0x00000000 },
  { "MEM_LP_CTRL", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_debug_mon_sel
  { "debug_mon_sel", 0x0000003C,  4,  0,   CSR_RW, 0x00000000 },
  { "WORD_DEBUG_MON_SEL", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_DISP_H_
