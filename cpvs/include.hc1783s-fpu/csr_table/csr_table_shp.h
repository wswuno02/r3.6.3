#ifndef CSR_TABLE_SHP_H_
#define CSR_TABLE_SHP_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_shp[] =
{
  // WORD word_frame_start
  { "frame_start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "WORD_FRAME_START", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD irq_clear
  { "irq_clear_frame_end", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "IRQ_CLEAR", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD status
  { "status_frame_end", 0x00000008,  0,  0,   CSR_RO, 0x00000000 },
  { "STATUS", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // WORD irq_mask
  { "irq_mask_frame_end", 0x0000000C,  0,  0,   CSR_RW, 0x00000001 },
  { "IRQ_MASK", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_mode
  { "mode", 0x00000010,  1,  0,   CSR_RW, 0x00000000 },
  { "sample_mode", 0x00000010, 16, 16,   CSR_RW, 0x00000000 },
  { "WORD_MODE", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD resolution
  { "width", 0x00000014, 15,  0,   CSR_RW, 0x00000100 },
  { "height", 0x00000014, 31, 16,   CSR_RW, 0x00000438 },
  { "RESOLUTION", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_level_gain
  { "level_gain", 0x00000018,  8,  0,   CSR_RW, 0x00000010 },
  { "WORD_LEVEL_GAIN", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD level_gain_roi_0_set
  { "level_gain_roi_0_en", 0x0000001C,  0,  0,   CSR_RW, 0x00000000 },
  { "level_gain_roi_0", 0x0000001C, 24, 16,   CSR_RW, 0x00000010 },
  { "LEVEL_GAIN_ROI_0_SET", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD level_gain_roi_0_sxsy
  { "level_gain_roi0_sx", 0x00000020, 15,  0,   CSR_RW, 0x00000000 },
  { "level_gain_roi0_sy", 0x00000020, 31, 16,   CSR_RW, 0x00000000 },
  { "LEVEL_GAIN_ROI_0_SXSY", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD level_gain_roi_0_exey
  { "level_gain_roi0_ex", 0x00000024, 15,  0,   CSR_RW, 0x00000000 },
  { "level_gain_roi0_ey", 0x00000024, 31, 16,   CSR_RW, 0x00000000 },
  { "LEVEL_GAIN_ROI_0_EXEY", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD level_gain_roi_1_set
  { "level_gain_roi_1_en", 0x00000028,  0,  0,   CSR_RW, 0x00000000 },
  { "level_gain_roi_1", 0x00000028, 24, 16,   CSR_RW, 0x00000010 },
  { "LEVEL_GAIN_ROI_1_SET", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD level_gain_roi_1_sxsy
  { "level_gain_roi1_sx", 0x0000002C, 15,  0,   CSR_RW, 0x00000000 },
  { "level_gain_roi1_sy", 0x0000002C, 31, 16,   CSR_RW, 0x00000000 },
  { "LEVEL_GAIN_ROI_1_SXSY", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD level_gain_roi_1_exey
  { "level_gain_roi1_ex", 0x00000030, 15,  0,   CSR_RW, 0x00000000 },
  { "level_gain_roi1_ey", 0x00000030, 31, 16,   CSR_RW, 0x00000000 },
  { "LEVEL_GAIN_ROI_1_EXEY", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD level_gain_roi_2_set
  { "level_gain_roi_2_en", 0x00000034,  0,  0,   CSR_RW, 0x00000000 },
  { "level_gain_roi_2", 0x00000034, 24, 16,   CSR_RW, 0x00000010 },
  { "LEVEL_GAIN_ROI_2_SET", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD level_gain_roi_2_sxsy
  { "level_gain_roi2_sx", 0x00000038, 15,  0,   CSR_RW, 0x00000000 },
  { "level_gain_roi2_sy", 0x00000038, 31, 16,   CSR_RW, 0x00000000 },
  { "LEVEL_GAIN_ROI_2_SXSY", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD level_gain_roi_2_exey
  { "level_gain_roi2_ex", 0x0000003C, 15,  0,   CSR_RW, 0x00000000 },
  { "level_gain_roi2_ey", 0x0000003C, 31, 16,   CSR_RW, 0x00000000 },
  { "LEVEL_GAIN_ROI_2_EXEY", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD level_gain_roi_3_set
  { "level_gain_roi_3_en", 0x00000040,  0,  0,   CSR_RW, 0x00000000 },
  { "level_gain_roi_3", 0x00000040, 24, 16,   CSR_RW, 0x00000010 },
  { "LEVEL_GAIN_ROI_3_SET", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD level_gain_roi_3_sxsy
  { "level_gain_roi3_sx", 0x00000044, 15,  0,   CSR_RW, 0x00000000 },
  { "level_gain_roi3_sy", 0x00000044, 31, 16,   CSR_RW, 0x00000000 },
  { "LEVEL_GAIN_ROI_3_SXSY", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD level_gain_roi_3_exey
  { "level_gain_roi3_ex", 0x00000048, 15,  0,   CSR_RW, 0x00000000 },
  { "level_gain_roi3_ey", 0x00000048, 31, 16,   CSR_RW, 0x00000000 },
  { "LEVEL_GAIN_ROI_3_EXEY", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain
  { "bpf_gain", 0x0000004C,  7,  0,   CSR_RW, 0x00000010 },
  { "hpf_gain", 0x0000004C, 15,  8,   CSR_RW, 0x00000010 },
  { "GAIN", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_curve_ini
  { "gain_curve_y_ini", 0x00000050,  9,  0,   CSR_RW, 0x00000000 },
  { "GAIN_CURVE_INI", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_curve_0
  { "gain_curve_x_0", 0x00000054,  9,  0,   CSR_RW, 0x00000010 },
  { "gain_curve_y_0", 0x00000054, 25, 16,   CSR_RW, 0x00000000 },
  { "GAIN_CURVE_0", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_curve_1
  { "gain_curve_x_1", 0x00000058,  9,  0,   CSR_RW, 0x00000020 },
  { "gain_curve_y_1", 0x00000058, 25, 16,   CSR_RW, 0x00000004 },
  { "GAIN_CURVE_1", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_curve_2
  { "gain_curve_x_2", 0x0000005C,  9,  0,   CSR_RW, 0x000003DE },
  { "gain_curve_y_2", 0x0000005C, 25, 16,   CSR_RW, 0x000003C2 },
  { "GAIN_CURVE_2", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_curve_3
  { "gain_curve_x_3", 0x00000060,  9,  0,   CSR_RW, 0x000003EE },
  { "gain_curve_y_3", 0x00000060, 25, 16,   CSR_RW, 0x000003C6 },
  { "GAIN_CURVE_3", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_curve_4
  { "gain_curve_x_4", 0x00000064,  9,  0,   CSR_RW, 0x000003FE },
  { "gain_curve_y_4", 0x00000064, 25, 16,   CSR_RW, 0x000003C2 },
  { "GAIN_CURVE_4", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_curve_m_0
  { "gain_curve_m_0_2s", 0x00000068,  5,  0,   CSR_RW, 0x00000000 },
  { "gain_curve_m_1_2s", 0x00000068, 13,  8,   CSR_RW, 0x00000004 },
  { "gain_curve_m_2_2s", 0x00000068, 21, 16,   CSR_RW, 0x00000010 },
  { "gain_curve_m_3_2s", 0x00000068, 29, 24,   CSR_RW, 0x00000004 },
  { "GAIN_CURVE_M_0", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gain_curve_m_1
  { "gain_curve_m_4_2s", 0x0000006C,  5,  0,   CSR_RW, 0x0000003C },
  { "GAIN_CURVE_M_1", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD luma_lut_ini
  { "luma_lut_y_ini", 0x00000070,  9,  0,   CSR_RW, 0x00000000 },
  { "LUMA_LUT_INI", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // WORD luma_lut_0_xy
  { "luma_lut_x_0", 0x00000074,  9,  0,   CSR_RW, 0x00000080 },
  { "luma_lut_y_0", 0x00000074, 25, 16,   CSR_RW, 0x00000000 },
  { "LUMA_LUT_0_XY", 0x00000074, 31, 0,   CSR_RW, 0x00000000 },
  // WORD luma_lut_0_m
  { "luma_lut_m_0_2s", 0x00000078,  6,  0,   CSR_RW, 0x00000000 },
  { "LUMA_LUT_0_M", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD luma_lut_1_xy
  { "luma_lut_x_1", 0x0000007C,  9,  0,   CSR_RW, 0x00000084 },
  { "luma_lut_y_1", 0x0000007C, 25, 16,   CSR_RW, 0x00000040 },
  { "LUMA_LUT_1_XY", 0x0000007C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD luma_lut_1_m
  { "luma_lut_m_1_2s", 0x00000080,  6,  0,   CSR_RW, 0x00000001 },
  { "LUMA_LUT_1_M", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // WORD luma_lut_2_xy
  { "luma_lut_x_2", 0x00000084,  9,  0,   CSR_RW, 0x00000086 },
  { "luma_lut_y_2", 0x00000084, 25, 16,   CSR_RW, 0x00000080 },
  { "LUMA_LUT_2_XY", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD luma_lut_2_m
  { "luma_lut_m_2_2s", 0x00000088,  6,  0,   CSR_RW, 0x00000002 },
  { "LUMA_LUT_2_M", 0x00000088, 31, 0,   CSR_RW, 0x00000000 },
  // WORD luma_lut_3_xy
  { "luma_lut_x_3", 0x0000008C,  9,  0,   CSR_RW, 0x00000088 },
  { "luma_lut_y_3", 0x0000008C, 25, 16,   CSR_RW, 0x00000100 },
  { "LUMA_LUT_3_XY", 0x0000008C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD luma_lut_3_m
  { "luma_lut_m_3_2s", 0x00000090,  6,  0,   CSR_RW, 0x00000004 },
  { "LUMA_LUT_3_M", 0x00000090, 31, 0,   CSR_RW, 0x00000000 },
  // WORD luma_lut_4_xy
  { "luma_lut_x_4", 0x00000094,  9,  0,   CSR_RW, 0x0000008E },
  { "luma_lut_y_4", 0x00000094, 25, 16,   CSR_RW, 0x00000180 },
  { "LUMA_LUT_4_XY", 0x00000094, 31, 0,   CSR_RW, 0x00000000 },
  // WORD luma_lut_4_m
  { "luma_lut_m_4_2s", 0x00000098,  6,  0,   CSR_RW, 0x00000002 },
  { "LUMA_LUT_4_M", 0x00000098, 31, 0,   CSR_RW, 0x00000000 },
  // WORD luma_lut_5_xy
  { "luma_lut_x_5", 0x0000009C,  9,  0,   CSR_RW, 0x00000092 },
  { "luma_lut_y_5", 0x0000009C, 25, 16,   CSR_RW, 0x00000200 },
  { "LUMA_LUT_5_XY", 0x0000009C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD luma_lut_5_m
  { "luma_lut_m_5_2s", 0x000000A0,  6,  0,   CSR_RW, 0x00000001 },
  { "LUMA_LUT_5_M", 0x000000A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD luma_lut_6_xy
  { "luma_lut_x_6", 0x000000A4,  9,  0,   CSR_RW, 0x00000374 },
  { "luma_lut_y_6", 0x000000A4, 25, 16,   CSR_RW, 0x00000200 },
  { "LUMA_LUT_6_XY", 0x000000A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD luma_lut_6_m
  { "luma_lut_m_6_2s", 0x000000A8,  6,  0,   CSR_RW, 0x00000000 },
  { "LUMA_LUT_6_M", 0x000000A8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD luma_lut_7_xy
  { "luma_lut_x_7", 0x000000AC,  9,  0,   CSR_RW, 0x00000378 },
  { "luma_lut_y_7", 0x000000AC, 25, 16,   CSR_RW, 0x00000100 },
  { "LUMA_LUT_7_XY", 0x000000AC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD luma_lut_7_m
  { "luma_lut_m_7_2s", 0x000000B0,  6,  0,   CSR_RW, 0x00000000 },
  { "LUMA_LUT_7_M", 0x000000B0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD luma_lut_8_xy
  { "luma_lut_x_8", 0x000000B4,  9,  0,   CSR_RW, 0x00000380 },
  { "luma_lut_y_8", 0x000000B4, 25, 16,   CSR_RW, 0x00000000 },
  { "LUMA_LUT_8_XY", 0x000000B4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD luma_lut_8_m
  { "luma_lut_m_8_2s", 0x000000B8,  6,  0,   CSR_RW, 0x00000000 },
  { "LUMA_LUT_8_M", 0x000000B8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD luma_lut_9_m
  { "luma_lut_m_9_2s", 0x000000BC, 14,  8,   CSR_RW, 0x00000000 },
  { "LUMA_LUT_9_M", 0x000000BC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD soft_clip
  { "soft_clip_slope", 0x000000C0,  4,  0,   CSR_RW, 0x0000000C },
  { "SOFT_CLIP", 0x000000C0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rgl_y_lpf_set
  { "rgl_y_lpf_en", 0x000000C4,  0,  0,   CSR_RW, 0x00000000 },
  { "rgl_y_lpf_clear", 0x000000C4,  8,  8,   CSR_RW, 0x00000000 },
  { "RGL_Y_LPF_SET", 0x000000C4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rgl_y_lpf_rgl_wh
  { "rgl_y_lpf_rgl_wd", 0x000000C8, 12,  0,   CSR_RW, 0x00000000 },
  { "rgl_y_lpf_rgl_ht", 0x000000C8, 28, 16,   CSR_RW, 0x00000000 },
  { "RGL_Y_LPF_RGL_WH", 0x000000C8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rgl_y_lpf_sxsy
  { "rgl_y_lpf_sx", 0x000000CC, 15,  0,   CSR_RW, 0x00000000 },
  { "rgl_y_lpf_sy", 0x000000CC, 31, 16,   CSR_RW, 0x00000000 },
  { "RGL_Y_LPF_SXSY", 0x000000CC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rgl_y_lpf_exey
  { "rgl_y_lpf_ex", 0x000000D0, 15,  0,   CSR_RW, 0x00000000 },
  { "rgl_y_lpf_ey", 0x000000D0, 31, 16,   CSR_RW, 0x00000000 },
  { "RGL_Y_LPF_EXEY", 0x000000D0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rgl_y_lpf_xy_cnt_ini
  { "rgl_y_lpf_x_cnt_ini", 0x000000D4, 12,  0,   CSR_RW, 0x00000000 },
  { "rgl_y_lpf_y_cnt_ini", 0x000000D4, 31, 16,   CSR_RW, 0x00000000 },
  { "RGL_Y_LPF_XY_CNT_INI", 0x000000D4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rgl_y_lpf_r_cnt_set
  { "rgl_y_lpf_r_cnt_ini", 0x000000D8,  2,  0,   CSR_RW, 0x00000000 },
  { "RGL_Y_LPF_R_CNT_SET", 0x000000D8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_rgl_y_lpf_pix_num
  { "rgl_y_lpf_pix_num", 0x000000DC, 23,  0,   CSR_RW, 0x00002710 },
  { "WORD_RGL_Y_LPF_PIX_NUM", 0x000000DC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD roi_y_lpf_avg_0_set
  { "roi_y_lpf_avg_0_clear", 0x000000E0,  0,  0,   CSR_RW, 0x00000000 },
  { "roi_y_lpf_avg_0_en", 0x000000E0,  8,  8,   CSR_RW, 0x00000000 },
  { "ROI_Y_LPF_AVG_0_SET", 0x000000E0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD roi_y_lpf_avg_0_sxsy
  { "roi_y_lpf_avg_0_sx", 0x000000E4, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_y_lpf_avg_0_sy", 0x000000E4, 31, 16,   CSR_RW, 0x00000000 },
  { "ROI_Y_LPF_AVG_0_SXSY", 0x000000E4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD roi_y_lpf_avg_0_exey
  { "roi_y_lpf_avg_0_ex", 0x000000E8, 15,  0,   CSR_RW, 0x00000000 },
  { "roi_y_lpf_avg_0_ey", 0x000000E8, 31, 16,   CSR_RW, 0x00000000 },
  { "ROI_Y_LPF_AVG_0_EXEY", 0x000000E8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_roi_y_lpf_avg_0_pix_num
  { "roi_y_lpf_avg_0_pix_num", 0x000000EC, 23,  0,   CSR_RW, 0x00002710 },
  { "WORD_ROI_Y_LPF_AVG_0_PIX_NUM", 0x000000EC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_roi_y_lpf_avg_0_avg
  { "roi_y_lpf_avg_0_avg", 0x000000F0,  9,  0,   CSR_RO, 0x00000000 },
  { "WORD_ROI_Y_LPF_AVG_0_AVG", 0x000000F0, 31, 0,   CSR_RO, 0x00000000 },
  // WORD rgl_y_stat_ctrl
  { "rgl_y_stat_en", 0x000000F4,  0,  0,   CSR_RW, 0x00000000 },
  { "RGL_Y_STAT_CTRL", 0x000000F4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_rgl_y_stat_r_addr
  { "rgl_y_stat_r_addr", 0x000000F8,  6,  0,   CSR_RW, 0x00000000 },
  { "WORD_RGL_Y_STAT_R_ADDR", 0x000000F8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_rgl_y_stat_r_data
  { "rgl_y_stat_r_data", 0x000000FC, 19,  0,   CSR_RO, 0x00000000 },
  { "WORD_RGL_Y_STAT_R_DATA", 0x000000FC, 31, 0,   CSR_RO, 0x00000000 },
  // WORD demo_set
  { "demo_en", 0x00000100,  0,  0,   CSR_RW, 0x00000000 },
  { "DEMO_SET", 0x00000100, 31, 0,   CSR_RW, 0x00000000 },
  // WORD demo_x
  { "demo_x_min", 0x00000104, 15,  0,   CSR_RW, 0x00000000 },
  { "demo_x_max", 0x00000104, 31, 16,   CSR_RW, 0x00000000 },
  { "DEMO_X", 0x00000104, 31, 0,   CSR_RW, 0x00000000 },
  // WORD demo_y
  { "demo_y_min", 0x00000108, 15,  0,   CSR_RW, 0x00000000 },
  { "demo_y_max", 0x00000108, 31, 16,   CSR_RW, 0x00000000 },
  { "DEMO_Y", 0x00000108, 31, 0,   CSR_RW, 0x00000000 },
  // WORD debug_mon
  { "debug_mon_sel", 0x0000010C,  0,  0,   CSR_RW, 0x00000000 },
  { "DEBUG_MON", 0x0000010C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_reserved
  { "reserved", 0x00000110, 31,  0,   CSR_RW, 0x00000000 },
  { "WORD_RESERVED", 0x00000110, 31, 0,   CSR_RW, 0x00000000 },
  // WORD atpg_ctrl
  { "atpg_ctrl_0", 0x00000114,  0,  0,   CSR_RW, 0x00000000 },
  { "atpg_ctrl_1", 0x00000114,  8,  8,   CSR_RW, 0x00000000 },
  { "ATPG_CTRL", 0x00000114, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_SHP_H_
