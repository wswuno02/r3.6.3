#ifndef CSR_TABLE_DMA_8_C1_D64_H_
#define CSR_TABLE_DMA_8_C1_D64_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_dma_8_c1_d64[] =
{
  // WORD dma_8_00
  { "access_start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "DMA_8_00", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD dma_8_01
  { "irq_clear_access_end", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "irq_clear_bw_insufficient_r", 0x00000004,  8,  8,  CSR_W1P, 0x00000000 },
  { "irq_clear_access_violation_r", 0x00000004, 16, 16,  CSR_W1P, 0x00000000 },
  { "irq_clear_bw_insufficient_w", 0x00000004, 24, 24,  CSR_W1P, 0x00000000 },
  { "DMA_8_01", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD dma_8_02
  { "irq_clear_access_violation_w", 0x00000008,  0,  0,  CSR_W1P, 0x00000000 },
  { "DMA_8_02", 0x00000008, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD dma_8_03
  { "status_access_end", 0x0000000C,  0,  0,   CSR_RO, 0x00000000 },
  { "status_bw_insufficient_r", 0x0000000C,  8,  8,   CSR_RO, 0x00000000 },
  { "status_access_violation_r", 0x0000000C, 16, 16,   CSR_RO, 0x00000000 },
  { "status_bw_insufficient_w", 0x0000000C, 24, 24,   CSR_RO, 0x00000000 },
  { "DMA_8_03", 0x0000000C, 31, 0,   CSR_RO, 0x00000000 },
  // WORD dma_8_04
  { "status_access_violation_w", 0x00000010,  0,  0,   CSR_RO, 0x00000000 },
  { "DMA_8_04", 0x00000010, 31, 0,   CSR_RO, 0x00000000 },
  // WORD dma_8_05
  { "irq_mask_access_end", 0x00000014,  0,  0,   CSR_RW, 0x00000001 },
  { "irq_mask_bw_insufficient_r", 0x00000014,  8,  8,   CSR_RW, 0x00000001 },
  { "irq_mask_access_violation_r", 0x00000014, 16, 16,   CSR_RW, 0x00000001 },
  { "irq_mask_bw_insufficient_w", 0x00000014, 24, 24,   CSR_RW, 0x00000001 },
  { "DMA_8_05", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_06
  { "irq_mask_access_violation_w", 0x00000018,  0,  0,   CSR_RW, 0x00000001 },
  { "DMA_8_06", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_07
  { "access_illegal_hang_r", 0x0000001C,  0,  0,   CSR_RW, 0x00000001 },
  { "access_illegal_mask_r", 0x0000001C,  8,  8,   CSR_RW, 0x00000001 },
  { "access_illegal_hang_w", 0x0000001C, 16, 16,   CSR_RW, 0x00000001 },
  { "access_illegal_mask_w", 0x0000001C, 24, 24,   CSR_RW, 0x00000001 },
  { "DMA_8_07", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_10
  { "col_addr_type", 0x00000028,  9,  8,   CSR_RW, 0x00000000 },
  { "total_access_cnt", 0x00000028, 19, 16,   CSR_RW, 0x00000000 },
  { "debug_mon_sel", 0x00000028, 25, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_10", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_11
  { "target_burst_len_r", 0x0000002C,  4,  0,   CSR_RW, 0x00000010 },
  { "target_burst_len_w", 0x0000002C, 12,  8,   CSR_RW, 0x00000010 },
  { "bank_addr_type", 0x0000002C, 16, 16,   CSR_RW, 0x00000000 },
  { "access_end_sel", 0x0000002C, 24, 24,   CSR_RW, 0x00000001 },
  { "DMA_8_11", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_12
  { "target_fifo_level_r", 0x00000030,  5,  0,   CSR_RW, 0x00000010 },
  { "target_fifo_level_w", 0x00000030, 13,  8,   CSR_RW, 0x00000010 },
  { "fifo_full_level_r", 0x00000030, 21, 16,   CSR_RW, 0x00000020 },
  { "fifo_full_level_w", 0x00000030, 29, 24,   CSR_RW, 0x00000020 },
  { "DMA_8_12", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_13
  { "access_length_r_1", 0x00000034, 19,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_13", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_14
  { "efuse_dis_aes_violation", 0x00000038,  0,  0,   CSR_RO, 0x00000000 },
  { "DMA_8_14", 0x00000038, 31, 0,   CSR_RO, 0x00000000 },
  // WORD dma_8_15
  { "access_length_r_0", 0x0000003C, 19,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_15", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_16
  { "loop_cnt_0", 0x00000040, 30, 16,   CSR_RW, 0x00000001 },
  { "DMA_8_16", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_17
  { "aes_mode_0", 0x00000044,  1,  0,   CSR_RW, 0x00000002 },
  { "access_mode_0", 0x00000044,  9,  8,   CSR_RW, 0x00000000 },
  { "ini_addr_word_r_0", 0x00000044, 18, 16,   CSR_RW, 0x00000000 },
  { "ini_addr_word_w_0", 0x00000044, 26, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_17", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_18
  { "loop_cnt_1", 0x00000048, 30, 16,   CSR_RW, 0x00000002 },
  { "DMA_8_18", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_21
  { "aes_mode_1", 0x00000054,  1,  0,   CSR_RW, 0x00000002 },
  { "access_mode_1", 0x00000054,  9,  8,   CSR_RW, 0x00000000 },
  { "ini_addr_word_r_1", 0x00000054, 18, 16,   CSR_RW, 0x00000000 },
  { "ini_addr_word_w_1", 0x00000054, 26, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_21", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_22
  { "access_length_r_2", 0x00000058, 19,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_22", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_25
  { "aes_mode_2", 0x00000064,  1,  0,   CSR_RW, 0x00000002 },
  { "access_mode_2", 0x00000064,  9,  8,   CSR_RW, 0x00000000 },
  { "ini_addr_word_r_2", 0x00000064, 18, 16,   CSR_RW, 0x00000000 },
  { "ini_addr_word_w_2", 0x00000064, 26, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_25", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_26
  { "access_length_r_3", 0x00000068, 19,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_26", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_29
  { "aes_mode_3", 0x00000074,  1,  0,   CSR_RW, 0x00000002 },
  { "access_mode_3", 0x00000074,  9,  8,   CSR_RW, 0x00000000 },
  { "ini_addr_word_r_3", 0x00000074, 18, 16,   CSR_RW, 0x00000000 },
  { "ini_addr_word_w_3", 0x00000074, 26, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_29", 0x00000074, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_30
  { "access_length_r_4", 0x00000078, 19,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_30", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_33
  { "aes_mode_4", 0x00000084,  1,  0,   CSR_RW, 0x00000002 },
  { "access_mode_4", 0x00000084,  9,  8,   CSR_RW, 0x00000000 },
  { "ini_addr_word_r_4", 0x00000084, 18, 16,   CSR_RW, 0x00000000 },
  { "ini_addr_word_w_4", 0x00000084, 26, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_33", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_34
  { "access_length_r_5", 0x00000088, 19,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_34", 0x00000088, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_37
  { "aes_mode_5", 0x00000094,  1,  0,   CSR_RW, 0x00000002 },
  { "access_mode_5", 0x00000094,  9,  8,   CSR_RW, 0x00000000 },
  { "ini_addr_word_r_5", 0x00000094, 18, 16,   CSR_RW, 0x00000000 },
  { "ini_addr_word_w_5", 0x00000094, 26, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_37", 0x00000094, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_38
  { "access_length_r_6", 0x00000098, 19,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_38", 0x00000098, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_41
  { "aes_mode_6", 0x000000A4,  1,  0,   CSR_RW, 0x00000002 },
  { "access_mode_6", 0x000000A4,  9,  8,   CSR_RW, 0x00000000 },
  { "ini_addr_word_r_6", 0x000000A4, 18, 16,   CSR_RW, 0x00000000 },
  { "ini_addr_word_w_6", 0x000000A4, 26, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_41", 0x000000A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_42
  { "access_length_r_7", 0x000000A8, 19,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_42", 0x000000A8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_45
  { "aes_mode_7", 0x000000B4,  1,  0,   CSR_RW, 0x00000002 },
  { "access_mode_7", 0x000000B4,  9,  8,   CSR_RW, 0x00000000 },
  { "ini_addr_word_r_7", 0x000000B4, 18, 16,   CSR_RW, 0x00000000 },
  { "ini_addr_word_w_7", 0x000000B4, 26, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_45", 0x000000B4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_46
  { "bank_interleave_type_0", 0x000000B8,  1,  0,   CSR_RW, 0x00000003 },
  { "bank_interleave_type_1", 0x000000B8,  9,  8,   CSR_RW, 0x00000003 },
  { "bank_interleave_type_2", 0x000000B8, 17, 16,   CSR_RW, 0x00000003 },
  { "bank_interleave_type_3", 0x000000B8, 25, 24,   CSR_RW, 0x00000003 },
  { "DMA_8_46", 0x000000B8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_47
  { "bank_interleave_type_4", 0x000000BC,  1,  0,   CSR_RW, 0x00000003 },
  { "bank_interleave_type_5", 0x000000BC,  9,  8,   CSR_RW, 0x00000003 },
  { "bank_interleave_type_6", 0x000000BC, 17, 16,   CSR_RW, 0x00000003 },
  { "bank_interleave_type_7", 0x000000BC, 25, 24,   CSR_RW, 0x00000003 },
  { "DMA_8_47", 0x000000BC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_48
  { "ini_addr_bank_offset_r_0", 0x000000C0,  2,  0,   CSR_RW, 0x00000000 },
  { "ini_addr_bank_offset_r_1", 0x000000C0, 10,  8,   CSR_RW, 0x00000000 },
  { "ini_addr_bank_offset_r_2", 0x000000C0, 18, 16,   CSR_RW, 0x00000000 },
  { "ini_addr_bank_offset_r_3", 0x000000C0, 26, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_48", 0x000000C0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_49
  { "ini_addr_bank_offset_r_4", 0x000000C4,  2,  0,   CSR_RW, 0x00000000 },
  { "ini_addr_bank_offset_r_5", 0x000000C4, 10,  8,   CSR_RW, 0x00000000 },
  { "ini_addr_bank_offset_r_6", 0x000000C4, 18, 16,   CSR_RW, 0x00000000 },
  { "ini_addr_bank_offset_r_7", 0x000000C4, 26, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_49", 0x000000C4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_50
  { "ini_addr_linear_w_0", 0x000000C8, 27,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_50", 0x000000C8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_51
  { "ini_addr_linear_w_1", 0x000000CC, 27,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_51", 0x000000CC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_52
  { "ini_addr_linear_w_2", 0x000000D0, 27,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_52", 0x000000D0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_53
  { "ini_addr_linear_w_3", 0x000000D4, 27,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_53", 0x000000D4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_54
  { "ini_addr_linear_w_4", 0x000000D8, 27,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_54", 0x000000D8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_55
  { "ini_addr_linear_w_5", 0x000000DC, 27,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_55", 0x000000DC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_56
  { "ini_addr_linear_w_6", 0x000000E0, 27,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_56", 0x000000E0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_57
  { "ini_addr_linear_w_7", 0x000000E4, 27,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_57", 0x000000E4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_58
  { "ini_addr_linear_r_0", 0x000000E8, 27,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_58", 0x000000E8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_59
  { "ini_addr_linear_r_1", 0x000000EC, 27,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_59", 0x000000EC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_60
  { "ini_addr_linear_r_2", 0x000000F0, 27,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_60", 0x000000F0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_61
  { "ini_addr_linear_r_3", 0x000000F4, 27,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_61", 0x000000F4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_62
  { "ini_addr_linear_r_4", 0x000000F8, 27,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_62", 0x000000F8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_63
  { "ini_addr_linear_r_5", 0x000000FC, 27,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_63", 0x000000FC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_64
  { "ini_addr_linear_r_6", 0x00000100, 27,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_64", 0x00000100, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_65
  { "ini_addr_linear_r_7", 0x00000104, 27,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_65", 0x00000104, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_66
  { "flush_addr_skip_r_0", 0x00000108, 19,  0,   CSR_RW, 0x00000000 },
  { "flush_addr_skip_sign_r_0", 0x00000108, 24, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_66", 0x00000108, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_67
  { "flush_addr_skip_r_1", 0x0000010C, 19,  0,   CSR_RW, 0x00000000 },
  { "flush_addr_skip_sign_r_1", 0x0000010C, 24, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_67", 0x0000010C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_68
  { "flush_addr_skip_r_2", 0x00000110, 19,  0,   CSR_RW, 0x00000000 },
  { "flush_addr_skip_sign_r_2", 0x00000110, 24, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_68", 0x00000110, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_69
  { "flush_addr_skip_r_3", 0x00000114, 19,  0,   CSR_RW, 0x00000000 },
  { "flush_addr_skip_sign_r_3", 0x00000114, 24, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_69", 0x00000114, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_70
  { "flush_addr_skip_r_4", 0x00000118, 19,  0,   CSR_RW, 0x00000000 },
  { "flush_addr_skip_sign_r_4", 0x00000118, 24, 24,   CSR_RW, 0x00000000 },
  { "bypass_ado", 0x00000118, 27, 27,   CSR_RW, 0x00000001 },
  { "DMA_8_70", 0x00000118, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_71
  { "flush_addr_skip_r_5", 0x0000011C, 19,  0,   CSR_RW, 0x00000000 },
  { "flush_addr_skip_sign_r_5", 0x0000011C, 24, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_71", 0x0000011C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_72
  { "flush_addr_skip_r_6", 0x00000120, 19,  0,   CSR_RW, 0x00000000 },
  { "flush_addr_skip_sign_r_6", 0x00000120, 24, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_72", 0x00000120, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_73
  { "flush_addr_skip_r_7", 0x00000124, 19,  0,   CSR_RW, 0x00000000 },
  { "flush_addr_skip_sign_r_7", 0x00000124, 24, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_73", 0x00000124, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_76
  { "loop_cnt_2", 0x00000130, 30, 16,   CSR_RW, 0x00000003 },
  { "DMA_8_76", 0x00000130, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_77
  { "loop_cnt_3", 0x00000134, 30, 16,   CSR_RW, 0x00000004 },
  { "DMA_8_77", 0x00000134, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_78
  { "loop_cnt_4", 0x00000138, 30, 16,   CSR_RW, 0x00000005 },
  { "DMA_8_78", 0x00000138, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_79
  { "loop_cnt_5", 0x0000013C, 30, 16,   CSR_RW, 0x00000006 },
  { "DMA_8_79", 0x0000013C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_80
  { "loop_cnt_6", 0x00000140, 30, 16,   CSR_RW, 0x00000007 },
  { "DMA_8_80", 0x00000140, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_81
  { "loop_cnt_7", 0x00000144, 30, 16,   CSR_RW, 0x00000008 },
  { "DMA_8_81", 0x00000144, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_82
  { "flush_addr_skip_w_0", 0x00000148, 19,  0,   CSR_RW, 0x00000000 },
  { "flush_addr_skip_sign_w_0", 0x00000148, 24, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_82", 0x00000148, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_83
  { "flush_addr_skip_w_1", 0x0000014C, 19,  0,   CSR_RW, 0x00000000 },
  { "flush_addr_skip_sign_w_1", 0x0000014C, 24, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_83", 0x0000014C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_84
  { "flush_addr_skip_w_2", 0x00000150, 19,  0,   CSR_RW, 0x00000000 },
  { "flush_addr_skip_sign_w_2", 0x00000150, 24, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_84", 0x00000150, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_85
  { "flush_addr_skip_w_3", 0x00000154, 19,  0,   CSR_RW, 0x00000000 },
  { "flush_addr_skip_sign_w_3", 0x00000154, 24, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_85", 0x00000154, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_86
  { "flush_addr_skip_w_4", 0x00000158, 19,  0,   CSR_RW, 0x00000000 },
  { "flush_addr_skip_sign_w_4", 0x00000158, 24, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_86", 0x00000158, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_87
  { "flush_addr_skip_w_5", 0x0000015C, 19,  0,   CSR_RW, 0x00000000 },
  { "flush_addr_skip_sign_w_5", 0x0000015C, 24, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_87", 0x0000015C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_88
  { "flush_addr_skip_w_6", 0x00000160, 19,  0,   CSR_RW, 0x00000000 },
  { "flush_addr_skip_sign_w_6", 0x00000160, 24, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_88", 0x00000160, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_89
  { "flush_addr_skip_w_7", 0x00000164, 19,  0,   CSR_RW, 0x00000000 },
  { "flush_addr_skip_sign_w_7", 0x00000164, 24, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_89", 0x00000164, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_90
  { "bank_interleave_type_w_0", 0x00000168,  1,  0,   CSR_RW, 0x00000003 },
  { "bank_interleave_type_w_1", 0x00000168,  9,  8,   CSR_RW, 0x00000003 },
  { "bank_interleave_type_w_2", 0x00000168, 17, 16,   CSR_RW, 0x00000003 },
  { "bank_interleave_type_w_3", 0x00000168, 25, 24,   CSR_RW, 0x00000003 },
  { "DMA_8_90", 0x00000168, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_91
  { "bank_interleave_type_w_4", 0x0000016C,  1,  0,   CSR_RW, 0x00000003 },
  { "bank_interleave_type_w_5", 0x0000016C,  9,  8,   CSR_RW, 0x00000003 },
  { "bank_interleave_type_w_6", 0x0000016C, 17, 16,   CSR_RW, 0x00000003 },
  { "bank_interleave_type_w_7", 0x0000016C, 25, 24,   CSR_RW, 0x00000003 },
  { "DMA_8_91", 0x0000016C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_92
  { "ini_addr_bank_offset_w_0", 0x00000170,  2,  0,   CSR_RW, 0x00000000 },
  { "ini_addr_bank_offset_w_1", 0x00000170, 10,  8,   CSR_RW, 0x00000000 },
  { "ini_addr_bank_offset_w_2", 0x00000170, 18, 16,   CSR_RW, 0x00000000 },
  { "ini_addr_bank_offset_w_3", 0x00000170, 26, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_92", 0x00000170, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_93
  { "ini_addr_bank_offset_w_4", 0x00000174,  2,  0,   CSR_RW, 0x00000000 },
  { "ini_addr_bank_offset_w_5", 0x00000174, 10,  8,   CSR_RW, 0x00000000 },
  { "ini_addr_bank_offset_w_6", 0x00000174, 18, 16,   CSR_RW, 0x00000000 },
  { "ini_addr_bank_offset_w_7", 0x00000174, 26, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_93", 0x00000174, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_94
  { "start_addr_r", 0x00000178, 27,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_94", 0x00000178, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_95
  { "end_addr_r", 0x0000017C, 27,  0,   CSR_RW, 0x0FFFFFFF },
  { "DMA_8_95", 0x0000017C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_96
  { "start_addr_w", 0x00000180, 27,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_96", 0x00000180, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_97
  { "end_addr_w", 0x00000184, 27,  0,   CSR_RW, 0x0FFFFFFF },
  { "DMA_8_97", 0x00000184, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_98
  { "access_length_w_0", 0x00000188, 19,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_98", 0x00000188, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_99
  { "access_length_w_1", 0x0000018C, 19,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_99", 0x0000018C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_100
  { "access_length_w_2", 0x00000190, 19,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_100", 0x00000190, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_101
  { "access_length_w_3", 0x00000194, 19,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_101", 0x00000194, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_102
  { "access_length_w_4", 0x00000198, 19,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_102", 0x00000198, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_103
  { "access_length_w_5", 0x0000019C, 19,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_103", 0x0000019C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_104
  { "access_length_w_6", 0x000001A0, 19,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_104", 0x000001A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_105
  { "access_length_w_7", 0x000001A4, 19,  0,   CSR_RW, 0x00000000 },
  { "DMA_8_105", 0x000001A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dma_8_106
  { "scan_mode", 0x000001A8,  0,  0,   CSR_RW, 0x00000000 },
  { "sd", 0x000001A8,  8,  8,   CSR_RW, 0x00000000 },
  { "slp", 0x000001A8, 24, 24,   CSR_RW, 0x00000000 },
  { "DMA_8_106", 0x000001A8, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_DMA_8_C1_D64_H_
