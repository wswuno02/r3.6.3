#ifndef CSR_TABLE_RST_H_
#define CSR_TABLE_RST_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_rst[] =
{
  // WORD rst_axi
  { "sw_rst_axi_cpu", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "sw_rst_axi_sys", 0x00000000,  8,  8,  CSR_W1P, 0x00000000 },
  { "sw_rst_axi_dramc", 0x00000000, 16, 16,  CSR_W1P, 0x00000000 },
  { "RST_AXI", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_apb
  { "sw_rst_apb_0", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "sw_rst_apb_1", 0x00000004,  8,  8,  CSR_W1P, 0x00000000 },
  { "sw_rst_apb_2", 0x00000004, 16, 16,  CSR_W1P, 0x00000000 },
  { "sw_rst_apb_3", 0x00000004, 24, 24,  CSR_W1P, 0x00000000 },
  { "RST_APB", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_usb
  { "sw_rst_apb_usb", 0x00000008,  0,  0,  CSR_W1P, 0x00000000 },
  { "sw_rst_usb_utmi", 0x00000008,  8,  8,  CSR_W1P, 0x00000000 },
  { "sw_rst_usb_phy", 0x00000008, 16, 16,  CSR_W1P, 0x00000000 },
  { "RST_USB", 0x00000008, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_ddr
  { "sw_rst_dramc_hdr", 0x0000000C,  0,  0,  CSR_W1P, 0x00000000 },
  { "sw_rst_ddrphy", 0x0000000C,  8,  8,  CSR_W1P, 0x00000000 },
  { "sw_rst_ddrphy_dll", 0x0000000C, 16, 16,  CSR_W1P, 0x00000000 },
  { "sw_rst_ddrphy_hdr", 0x0000000C, 24, 24,  CSR_W1P, 0x00000000 },
  { "RST_DDR", 0x0000000C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_emac
  { "sw_rst_apb_emac", 0x00000010,  0,  0,  CSR_W1P, 0x00000000 },
  { "RST_EMAC", 0x00000010, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_sdc
  { "sw_rst_sdc_0", 0x00000014,  0,  0,  CSR_W1P, 0x00000000 },
  { "sw_rst_sdc_1", 0x00000014,  8,  8,  CSR_W1P, 0x00000000 },
  { "sw_rst_qspi", 0x00000014, 16, 16,  CSR_W1P, 0x00000000 },
  { "RST_SDC", 0x00000014, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_sdc_1
  { "sw_rst_apb_sdc_0", 0x00000018,  0,  0,  CSR_W1P, 0x00000000 },
  { "sw_rst_apb_sdc_1", 0x00000018,  8,  8,  CSR_W1P, 0x00000000 },
  { "RST_SDC_1", 0x00000018, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_snsr
  { "sw_rst_sensor", 0x0000001C,  0,  0,  CSR_W1P, 0x00000000 },
  { "sw_rst_senif", 0x0000001C,  8,  8,  CSR_W1P, 0x00000000 },
  { "RST_SNSR", 0x0000001C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_enc
  { "sw_rst_enc", 0x00000020,  0,  0,  CSR_W1P, 0x00000000 },
  { "RST_ENC", 0x00000020, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_is
  { "sw_rst_is", 0x00000024,  0,  0,  CSR_W1P, 0x00000000 },
  { "RST_IS", 0x00000024, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_dma
  { "sw_rst_dma", 0x00000028,  0,  0,  CSR_W1P, 0x00000000 },
  { "RST_DMA", 0x00000028, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_ispvp
  { "sw_rst_isp", 0x0000002C,  0,  0,  CSR_W1P, 0x00000000 },
  { "sw_rst_vp", 0x0000002C,  8,  8,  CSR_W1P, 0x00000000 },
  { "RST_ISPVP", 0x0000002C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_disp
  { "sw_rst_disp", 0x00000030,  0,  0,  CSR_W1P, 0x00000000 },
  { "RST_DISP", 0x00000030, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_ddr_1
  { "sw_rst_apb_dramc", 0x00000034,  0,  0,  CSR_W1P, 0x00000000 },
  { "sw_rst_apb_ddrphy", 0x00000034,  8,  8,  CSR_W1P, 0x00000000 },
  { "RST_DDR_1", 0x00000034, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_audio
  { "sw_rst_audio_in", 0x00000038,  0,  0,  CSR_W1P, 0x00000000 },
  { "sw_rst_audio_out", 0x00000038,  8,  8,  CSR_W1P, 0x00000000 },
  { "RST_AUDIO", 0x00000038, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_plat0
  { "sw_rst_i2c_0", 0x0000003C,  0,  0,  CSR_W1P, 0x00000000 },
  { "sw_rst_i2c_1", 0x0000003C,  8,  8,  CSR_W1P, 0x00000000 },
  { "sw_rst_spi_0", 0x0000003C, 16, 16,  CSR_W1P, 0x00000000 },
  { "sw_rst_spi_1", 0x0000003C, 24, 24,  CSR_W1P, 0x00000000 },
  { "RST_PLAT0", 0x0000003C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_plat1
  { "sw_rst_uart_0", 0x00000040,  0,  0,  CSR_W1P, 0x00000000 },
  { "sw_rst_uart_1", 0x00000040,  8,  8,  CSR_W1P, 0x00000000 },
  { "sw_rst_uart_2", 0x00000040, 16, 16,  CSR_W1P, 0x00000000 },
  { "sw_rst_uart_3", 0x00000040, 24, 24,  CSR_W1P, 0x00000000 },
  { "RST_PLAT1", 0x00000040, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_plat2
  { "sw_rst_uart_4", 0x00000044,  0,  0,  CSR_W1P, 0x00000000 },
  { "sw_rst_uart_5", 0x00000044,  8,  8,  CSR_W1P, 0x00000000 },
  { "sw_rst_timer_0", 0x00000044, 16, 16,  CSR_W1P, 0x00000000 },
  { "sw_rst_timer_1", 0x00000044, 24, 24,  CSR_W1P, 0x00000000 },
  { "RST_PLAT2", 0x00000044, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_plat3
  { "sw_rst_timer_2", 0x00000048,  0,  0,  CSR_W1P, 0x00000000 },
  { "sw_rst_timer_3", 0x00000048,  8,  8,  CSR_W1P, 0x00000000 },
  { "sw_rst_pwm_0", 0x00000048, 16, 16,  CSR_W1P, 0x00000000 },
  { "sw_rst_pwm_1", 0x00000048, 24, 24,  CSR_W1P, 0x00000000 },
  { "RST_PLAT3", 0x00000048, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_plat4
  { "sw_rst_pwm_2", 0x0000004C,  0,  0,  CSR_W1P, 0x00000000 },
  { "sw_rst_pwm_3", 0x0000004C,  8,  8,  CSR_W1P, 0x00000000 },
  { "sw_rst_pwm_4", 0x0000004C, 16, 16,  CSR_W1P, 0x00000000 },
  { "sw_rst_pwm_5", 0x0000004C, 24, 24,  CSR_W1P, 0x00000000 },
  { "RST_PLAT4", 0x0000004C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_eirq
  { "sw_rst_eirq", 0x00000100,  0,  0,  CSR_W1P, 0x00000000 },
  { "RST_EIRQ", 0x00000100, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD lvrst_axi
  { "lv_rst_axi_cpu", 0x00000104,  0,  0,   CSR_RW, 0x00000000 },
  { "lv_rst_axi_sys", 0x00000104,  8,  8,   CSR_RW, 0x00000000 },
  { "lv_rst_axi_dramc", 0x00000104, 16, 16,   CSR_RW, 0x00000000 },
  { "LVRST_AXI", 0x00000104, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_apb
  { "lv_rst_apb_0", 0x00000108,  0,  0,   CSR_RW, 0x00000000 },
  { "lv_rst_apb_1", 0x00000108,  8,  8,   CSR_RW, 0x00000000 },
  { "lv_rst_apb_2", 0x00000108, 16, 16,   CSR_RW, 0x00000000 },
  { "lv_rst_apb_3", 0x00000108, 24, 24,   CSR_RW, 0x00000000 },
  { "LVRST_APB", 0x00000108, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_usb
  { "lv_rst_apb_usb", 0x0000010C,  0,  0,   CSR_RW, 0x00000000 },
  { "lv_rst_usb_utmi", 0x0000010C,  8,  8,   CSR_RW, 0x00000000 },
  { "lv_rst_usb_phy", 0x0000010C, 16, 16,   CSR_RW, 0x00000000 },
  { "LVRST_USB", 0x0000010C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_ddr
  { "lv_rst_dramc_hdr", 0x00000110,  0,  0,   CSR_RW, 0x00000000 },
  { "lv_rst_ddrphy", 0x00000110,  8,  8,   CSR_RW, 0x00000000 },
  { "lv_rst_ddrphy_dll", 0x00000110, 16, 16,   CSR_RW, 0x00000000 },
  { "lv_rst_ddrphy_hdr", 0x00000110, 24, 24,   CSR_RW, 0x00000000 },
  { "LVRST_DDR", 0x00000110, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_emac
  { "lv_rst_apb_emac", 0x00000114,  0,  0,   CSR_RW, 0x00000000 },
  { "LVRST_EMAC", 0x00000114, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_sdc
  { "lv_rst_sdc_0", 0x00000118,  0,  0,   CSR_RW, 0x00000000 },
  { "lv_rst_sdc_1", 0x00000118,  8,  8,   CSR_RW, 0x00000000 },
  { "lv_rst_qspi", 0x00000118, 16, 16,   CSR_RW, 0x00000000 },
  { "LVRST_SDC", 0x00000118, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_sdc_1
  { "lv_rst_apb_sdc_0", 0x0000011C,  0,  0,   CSR_RW, 0x00000000 },
  { "lv_rst_apb_sdc_1", 0x0000011C,  8,  8,   CSR_RW, 0x00000000 },
  { "LVRST_SDC_1", 0x0000011C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_snsr
  { "lv_rst_sensor", 0x00000120,  0,  0,   CSR_RW, 0x00000000 },
  { "lv_rst_senif", 0x00000120,  8,  8,   CSR_RW, 0x00000000 },
  { "LVRST_SNSR", 0x00000120, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_enc
  { "lv_rst_enc", 0x00000124,  0,  0,   CSR_RW, 0x00000000 },
  { "LVRST_ENC", 0x00000124, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_is
  { "lv_rst_is", 0x00000128,  0,  0,   CSR_RW, 0x00000000 },
  { "LVRST_IS", 0x00000128, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_dma
  { "lv_rst_dma", 0x0000012C,  0,  0,   CSR_RW, 0x00000000 },
  { "LVRST_DMA", 0x0000012C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_ispvp
  { "lv_rst_isp", 0x00000130,  0,  0,   CSR_RW, 0x00000000 },
  { "lv_rst_vp", 0x00000130,  8,  8,   CSR_RW, 0x00000000 },
  { "LVRST_ISPVP", 0x00000130, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_disp
  { "lv_rst_disp", 0x00000134,  0,  0,   CSR_RW, 0x00000000 },
  { "LVRST_DISP", 0x00000134, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_ddr_1
  { "lv_rst_apb_dramc", 0x00000138,  0,  0,   CSR_RW, 0x00000000 },
  { "lv_rst_apb_ddrphy", 0x00000138,  8,  8,   CSR_RW, 0x00000000 },
  { "LVRST_DDR_1", 0x00000138, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_audio
  { "lv_rst_audio_in", 0x0000013C,  0,  0,   CSR_RW, 0x00000000 },
  { "lv_rst_audio_out", 0x0000013C,  8,  8,   CSR_RW, 0x00000000 },
  { "LVRST_AUDIO", 0x0000013C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_plat0
  { "lv_rst_i2c_0", 0x00000140,  0,  0,   CSR_RW, 0x00000000 },
  { "lv_rst_i2c_1", 0x00000140,  8,  8,   CSR_RW, 0x00000000 },
  { "lv_rst_spi_0", 0x00000140, 16, 16,   CSR_RW, 0x00000000 },
  { "lv_rst_spi_1", 0x00000140, 24, 24,   CSR_RW, 0x00000000 },
  { "LVRST_PLAT0", 0x00000140, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_plat1
  { "lv_rst_uart_0", 0x00000144,  0,  0,   CSR_RW, 0x00000000 },
  { "lv_rst_uart_1", 0x00000144,  8,  8,   CSR_RW, 0x00000000 },
  { "lv_rst_uart_2", 0x00000144, 16, 16,   CSR_RW, 0x00000000 },
  { "lv_rst_uart_3", 0x00000144, 24, 24,   CSR_RW, 0x00000000 },
  { "LVRST_PLAT1", 0x00000144, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_plat2
  { "lv_rst_uart_4", 0x00000148,  0,  0,   CSR_RW, 0x00000000 },
  { "lv_rst_uart_5", 0x00000148,  8,  8,   CSR_RW, 0x00000000 },
  { "lv_rst_timer_0", 0x00000148, 16, 16,   CSR_RW, 0x00000000 },
  { "lv_rst_timer_1", 0x00000148, 24, 24,   CSR_RW, 0x00000000 },
  { "LVRST_PLAT2", 0x00000148, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_plat3
  { "lv_rst_timer_2", 0x0000014C,  0,  0,   CSR_RW, 0x00000000 },
  { "lv_rst_timer_3", 0x0000014C,  8,  8,   CSR_RW, 0x00000000 },
  { "lv_rst_pwm_0", 0x0000014C, 16, 16,   CSR_RW, 0x00000000 },
  { "lv_rst_pwm_1", 0x0000014C, 24, 24,   CSR_RW, 0x00000000 },
  { "LVRST_PLAT3", 0x0000014C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_plat4
  { "lv_rst_pwm_2", 0x00000150,  0,  0,   CSR_RW, 0x00000000 },
  { "lv_rst_pwm_3", 0x00000150,  8,  8,   CSR_RW, 0x00000000 },
  { "lv_rst_pwm_4", 0x00000150, 16, 16,   CSR_RW, 0x00000000 },
  { "lv_rst_pwm_5", 0x00000150, 24, 24,   CSR_RW, 0x00000000 },
  { "LVRST_PLAT4", 0x00000150, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_eirq
  { "lv_rst_eirq", 0x00000154,  0,  0,   CSR_RW, 0x00000000 },
  { "LVRST_EIRQ", 0x00000154, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_RST_H_
