#ifndef CSR_TABLE_AUDIO_FADE_OUT_H_
#define CSR_TABLE_AUDIO_FADE_OUT_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_audio_fade_out[] =
{
  // WORD audio_out_w1p
  { "audio_out_audio_start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "audio_out_fade_start", 0x00000000,  8,  8,  CSR_W1P, 0x00000000 },
  { "audio_out_fade_stop", 0x00000000, 16, 16,  CSR_W1P, 0x00000000 },
  { "AUDIO_OUT_W1P", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD audio_out_fade
  { "audio_out_fade_time", 0x00000004,  9,  0,   CSR_RW, 0x00000003 },
  { "AUDIO_OUT_FADE", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD audio_out_irq
  { "audio_out_irq_real_stop", 0x00000008,  0,  0,   CSR_RO, 0x00000000 },
  { "AUDIO_OUT_IRQ", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_AUDIO_FADE_OUT_H_
