#ifndef CSR_TABLE_DPC_H_
#define CSR_TABLE_DPC_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_dpc[] =
{
  // WORD dpc00
  { "frame_start", 0x00000000,  0,  0,  CSR_W1P, 0x00000000 },
  { "DPC00", 0x00000000, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD dpc01
  { "irq_clear_frame_end", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "DPC01", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD dpc02
  { "status_frame_end", 0x00000008,  0,  0,   CSR_RO, 0x00000000 },
  { "DPC02", 0x00000008, 31, 0,   CSR_RO, 0x00000000 },
  // WORD dpc03
  { "irq_mask_frame_end", 0x0000000C,  0,  0,   CSR_RW, 0x00000001 },
  { "DPC03", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dpc04
  { "width", 0x00000010, 15,  0,   CSR_RW, 0x00000100 },
  { "height", 0x00000010, 31, 16,   CSR_RW, 0x00000438 },
  { "DPC04", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dpc05
  { "operation", 0x00000014,  1,  0,   CSR_RW, 0x00000000 },
  { "yuv_chroma_pos", 0x00000014,  8,  8,   CSR_RW, 0x00000000 },
  { "dpc_mode", 0x00000014, 17, 16,   CSR_RW, 0x00000000 },
  { "ini_bayer_phase", 0x00000014, 25, 24,   CSR_RW, 0x00000000 },
  { "DPC05", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dpc06
  { "input_swap", 0x00000018,  0,  0,   CSR_RW, 0x00000000 },
  { "output_swap", 0x00000018,  8,  8,   CSR_RW, 0x00000000 },
  { "main_sub_align", 0x00000018, 16, 16,   CSR_RW, 0x00000000 },
  { "DPC06", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dpc07
  { "main_hdi_en", 0x0000001C,  0,  0,   CSR_RW, 0x00000000 },
  { "main_hdi_at_left", 0x0000001C,  8,  8,   CSR_RW, 0x00000000 },
  { "sub_hdi_en", 0x0000001C, 16, 16,   CSR_RW, 0x00000000 },
  { "sub_hdi_at_left", 0x0000001C, 24, 24,   CSR_RW, 0x00000000 },
  { "DPC07", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dpc08
  { "g_dislike_th", 0x00000020,  7,  0,   CSR_RW, 0x00000018 },
  { "br_dislike_th", 0x00000020, 23, 16,   CSR_RW, 0x00000018 },
  { "DPC08", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dpc09
  { "g_correct_overshoot", 0x00000024,  7,  0,   CSR_RW, 0x00000008 },
  { "br_correct_overshoot", 0x00000024, 23, 16,   CSR_RW, 0x00000008 },
  { "DPC09", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dpc10
  { "g_sad_gain", 0x00000028,  2,  0,   CSR_RW, 0x00000004 },
  { "br_sad_gain", 0x00000028, 18, 16,   CSR_RW, 0x00000004 },
  { "DPC10", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dpc11
  { "correct_bright_en", 0x0000002C,  0,  0,   CSR_RW, 0x00000001 },
  { "correct_dark_en", 0x0000002C,  8,  8,   CSR_RW, 0x00000001 },
  { "correct_mid_en", 0x0000002C, 16, 16,   CSR_RW, 0x00000001 },
  { "DPC11", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dpc12
  { "bright_th", 0x00000030,  5,  0,   CSR_RW, 0x0000003C },
  { "dark_th", 0x00000030, 21, 16,   CSR_RW, 0x00000004 },
  { "DPC12", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD word_debug_mon_sel
  { "debug_mon_sel", 0x00000034,  2,  0,   CSR_RW, 0x00000000 },
  { "debug_mon_sel_dpc", 0x00000034,  9,  8,   CSR_RW, 0x00000000 },
  { "debug_mon_sel_vdi", 0x00000034, 17, 16,   CSR_RW, 0x00000000 },
  { "WORD_DEBUG_MON_SEL", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD dpc14
  { "reserved", 0x00000038, 31,  0,   CSR_RW, 0x00000000 },
  { "DPC14", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_DPC_H_
