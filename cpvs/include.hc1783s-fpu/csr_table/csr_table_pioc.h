#ifndef CSR_TABLE_PIOC_H_
#define CSR_TABLE_PIOC_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_pioc[] =
{
  // WORD spi0_sck_iocfg
  { "pad_spi0_sck_pu", 0x00000000,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_spi0_sck_pd", 0x00000000,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_spi0_sck_pcfg", 0x00000000, 19, 16,   CSR_RW, 0x00000000 },
  { "SPI0_SCK_IOCFG", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD spi0_sdi_iocfg
  { "pad_spi0_sdi_pu", 0x00000004,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_spi0_sdi_pd", 0x00000004,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_spi0_sdi_pcfg", 0x00000004, 19, 16,   CSR_RW, 0x00000000 },
  { "SPI0_SDI_IOCFG", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD spi0_sdo_iocfg
  { "pad_spi0_sdo_pu", 0x00000008,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_spi0_sdo_pd", 0x00000008,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_spi0_sdo_pcfg", 0x00000008, 19, 16,   CSR_RW, 0x00000000 },
  { "SPI0_SDO_IOCFG", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD eirq0_iocfg
  { "pad_eirq0_pu", 0x0000000C,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_eirq0_pd", 0x0000000C,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_eirq0_pcfg", 0x0000000C, 19, 16,   CSR_RW, 0x00000000 },
  { "EIRQ0_IOCFG", 0x0000000C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sd_cd_iocfg
  { "pad_sd_cd_pu", 0x00000010,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_sd_cd_pd", 0x00000010,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_sd_cd_pcfg", 0x00000010, 19, 16,   CSR_RW, 0x00000000 },
  { "SD_CD_IOCFG", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sd_d2_iocfg
  { "pad_sd_d2_pu", 0x00000014,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_sd_d2_pd", 0x00000014,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_sd_d2_pcfg", 0x00000014, 20, 16,   CSR_RW, 0x00000000 },
  { "SD_D2_IOCFG", 0x00000014, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sd_d3_iocfg
  { "pad_sd_d3_pu", 0x00000018,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_sd_d3_pd", 0x00000018,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_sd_d3_pcfg", 0x00000018, 20, 16,   CSR_RW, 0x00000000 },
  { "SD_D3_IOCFG", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sd_cmd_iocfg
  { "pad_sd_cmd_pu", 0x0000001C,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_sd_cmd_pd", 0x0000001C,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_sd_cmd_pcfg", 0x0000001C, 20, 16,   CSR_RW, 0x00000000 },
  { "SD_CMD_IOCFG", 0x0000001C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sd_ck_iocfg
  { "pad_sd_ck_pu", 0x00000020,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_sd_ck_pd", 0x00000020,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_sd_ck_pcfg", 0x00000020, 20, 16,   CSR_RW, 0x00000000 },
  { "SD_CK_IOCFG", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sd_d0_iocfg
  { "pad_sd_d0_pu", 0x00000024,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_sd_d0_pd", 0x00000024,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_sd_d0_pcfg", 0x00000024, 20, 16,   CSR_RW, 0x00000000 },
  { "SD_D0_IOCFG", 0x00000024, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sd_d1_iocfg
  { "pad_sd_d1_pu", 0x00000028,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_sd_d1_pd", 0x00000028,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_sd_d1_pcfg", 0x00000028, 20, 16,   CSR_RW, 0x00000000 },
  { "SD_D1_IOCFG", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sd_poc_iocfg
  { "pad_sd_poc_pcfg", 0x0000002C, 16, 16,   CSR_RW, 0x00000000 },
  { "SD_POC_IOCFG", 0x0000002C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio3_iocfg
  { "pad_gpio3_pu", 0x00000030,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_gpio3_pd", 0x00000030,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_gpio3_pcfg", 0x00000030, 19, 16,   CSR_RW, 0x00000000 },
  { "GPIO3_IOCFG", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm1_iocfg
  { "pad_pwm1_pu", 0x00000034,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_pwm1_pd", 0x00000034,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_pwm1_pcfg", 0x00000034, 19, 16,   CSR_RW, 0x00000000 },
  { "PWM1_IOCFG", 0x00000034, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm4_iocfg
  { "pad_pwm4_pu", 0x00000038,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_pwm4_pd", 0x00000038,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_pwm4_pcfg", 0x00000038, 19, 16,   CSR_RW, 0x00000000 },
  { "PWM4_IOCFG", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2c1_scl_iocfg
  { "pad_i2c1_scl_pu", 0x0000003C,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_i2c1_scl_pd", 0x0000003C,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_i2c1_scl_pcfg", 0x0000003C, 19, 16,   CSR_RW, 0x00000000 },
  { "I2C1_SCL_IOCFG", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2c1_sda_iocfg
  { "pad_i2c1_sda_pu", 0x00000040,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_i2c1_sda_pd", 0x00000040,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_i2c1_sda_pcfg", 0x00000040, 19, 16,   CSR_RW, 0x00000000 },
  { "I2C1_SDA_IOCFG", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD uart1_txd_iocfg
  { "pad_uart1_txd_pu", 0x00000044,  0,  0,   CSR_RW, 0x00000001 },
  { "pad_uart1_txd_pd", 0x00000044,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_uart1_txd_pcfg", 0x00000044, 19, 16,   CSR_RW, 0x00000000 },
  { "UART1_TXD_IOCFG", 0x00000044, 31, 0,   CSR_RW, 0x00000000 },
  // WORD uart1_rxd_iocfg
  { "pad_uart1_rxd_pu", 0x00000048,  0,  0,   CSR_RW, 0x00000001 },
  { "pad_uart1_rxd_pd", 0x00000048,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_uart1_rxd_pcfg", 0x00000048, 19, 16,   CSR_RW, 0x00000000 },
  { "UART1_RXD_IOCFG", 0x00000048, 31, 0,   CSR_RW, 0x00000000 },
  // WORD uart2_txd_iocfg
  { "pad_uart2_txd_pu", 0x0000004C,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_uart2_txd_pd", 0x0000004C,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_uart2_txd_pcfg", 0x0000004C, 19, 16,   CSR_RW, 0x00000000 },
  { "UART2_TXD_IOCFG", 0x0000004C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD uart2_rxd_iocfg
  { "pad_uart2_rxd_pu", 0x00000050,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_uart2_rxd_pd", 0x00000050,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_uart2_rxd_pcfg", 0x00000050, 19, 16,   CSR_RW, 0x00000000 },
  { "UART2_RXD_IOCFG", 0x00000050, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi_ce_n_iocfg
  { "pad_qspi_ce_n_pu", 0x00000054,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_qspi_ce_n_pd", 0x00000054,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_qspi_ce_n_pcfg", 0x00000054, 19, 16,   CSR_RW, 0x00000000 },
  { "QSPI_CE_N_IOCFG", 0x00000054, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi_d1_iocfg
  { "pad_qspi_d1_pu", 0x00000058,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_qspi_d1_pd", 0x00000058,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_qspi_d1_pcfg", 0x00000058, 19, 16,   CSR_RW, 0x00000000 },
  { "QSPI_D1_IOCFG", 0x00000058, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi_d2_iocfg
  { "pad_qspi_d2_pu", 0x0000005C,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_qspi_d2_pd", 0x0000005C,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_qspi_d2_pcfg", 0x0000005C, 19, 16,   CSR_RW, 0x00000000 },
  { "QSPI_D2_IOCFG", 0x0000005C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi_d3_iocfg
  { "pad_qspi_d3_pu", 0x00000060,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_qspi_d3_pd", 0x00000060,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_qspi_d3_pcfg", 0x00000060, 19, 16,   CSR_RW, 0x00000000 },
  { "QSPI_D3_IOCFG", 0x00000060, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi_ck_iocfg
  { "pad_qspi_ck_pu", 0x00000064,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_qspi_ck_pd", 0x00000064,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_qspi_ck_pcfg", 0x00000064, 19, 16,   CSR_RW, 0x00000000 },
  { "QSPI_CK_IOCFG", 0x00000064, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi_d0_iocfg
  { "pad_qspi_d0_pu", 0x00000068,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_qspi_d0_pd", 0x00000068,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_qspi_d0_pcfg", 0x00000068, 19, 16,   CSR_RW, 0x00000000 },
  { "QSPI_D0_IOCFG", 0x00000068, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ephy_rst_iocfg
  { "pad_ephy_rst_pu", 0x0000006C,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_ephy_rst_pd", 0x0000006C,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_ephy_rst_pcfg", 0x0000006C, 19, 16,   CSR_RW, 0x00000000 },
  { "EPHY_RST_IOCFG", 0x0000006C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_tx_ck_iocfg
  { "pad_emac_tx_ck_pu", 0x00000070,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_emac_tx_ck_pd", 0x00000070,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_emac_tx_ck_pcfg", 0x00000070, 19, 16,   CSR_RW, 0x00000000 },
  { "EMAC_TX_CK_IOCFG", 0x00000070, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_tx_ctl_iocfg
  { "pad_emac_tx_ctl_pu", 0x00000074,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_emac_tx_ctl_pd", 0x00000074,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_emac_tx_ctl_pcfg", 0x00000074, 19, 16,   CSR_RW, 0x00000000 },
  { "EMAC_TX_CTL_IOCFG", 0x00000074, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_tx_d3_iocfg
  { "pad_emac_tx_d3_pu", 0x00000078,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_emac_tx_d3_pd", 0x00000078,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_emac_tx_d3_pcfg", 0x00000078, 19, 16,   CSR_RW, 0x00000000 },
  { "EMAC_TX_D3_IOCFG", 0x00000078, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_tx_d2_iocfg
  { "pad_emac_tx_d2_pu", 0x0000007C,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_emac_tx_d2_pd", 0x0000007C,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_emac_tx_d2_pcfg", 0x0000007C, 19, 16,   CSR_RW, 0x00000000 },
  { "EMAC_TX_D2_IOCFG", 0x0000007C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_tx_d1_iocfg
  { "pad_emac_tx_d1_pu", 0x00000080,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_emac_tx_d1_pd", 0x00000080,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_emac_tx_d1_pcfg", 0x00000080, 19, 16,   CSR_RW, 0x00000000 },
  { "EMAC_TX_D1_IOCFG", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_tx_d0_iocfg
  { "pad_emac_tx_d0_pu", 0x00000084,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_emac_tx_d0_pd", 0x00000084,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_emac_tx_d0_pcfg", 0x00000084, 19, 16,   CSR_RW, 0x00000000 },
  { "EMAC_TX_D0_IOCFG", 0x00000084, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_rx_ck_iocfg
  { "pad_emac_rx_ck_pu", 0x00000088,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_emac_rx_ck_pd", 0x00000088,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_emac_rx_ck_pcfg", 0x00000088, 19, 16,   CSR_RW, 0x00000000 },
  { "EMAC_RX_CK_IOCFG", 0x00000088, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_rx_d0_iocfg
  { "pad_emac_rx_d0_pu", 0x0000008C,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_emac_rx_d0_pd", 0x0000008C,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_emac_rx_d0_pcfg", 0x0000008C, 19, 16,   CSR_RW, 0x00000000 },
  { "EMAC_RX_D0_IOCFG", 0x0000008C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_rx_d1_iocfg
  { "pad_emac_rx_d1_pu", 0x00000090,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_emac_rx_d1_pd", 0x00000090,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_emac_rx_d1_pcfg", 0x00000090, 19, 16,   CSR_RW, 0x00000000 },
  { "EMAC_RX_D1_IOCFG", 0x00000090, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_rx_d2_iocfg
  { "pad_emac_rx_d2_pu", 0x00000094,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_emac_rx_d2_pd", 0x00000094,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_emac_rx_d2_pcfg", 0x00000094, 19, 16,   CSR_RW, 0x00000000 },
  { "EMAC_RX_D2_IOCFG", 0x00000094, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_rx_d3_iocfg
  { "pad_emac_rx_d3_pu", 0x00000098,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_emac_rx_d3_pd", 0x00000098,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_emac_rx_d3_pcfg", 0x00000098, 19, 16,   CSR_RW, 0x00000000 },
  { "EMAC_RX_D3_IOCFG", 0x00000098, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_rx_ctl_iocfg
  { "pad_emac_rx_ctl_pu", 0x0000009C,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_emac_rx_ctl_pd", 0x0000009C,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_emac_rx_ctl_pcfg", 0x0000009C, 19, 16,   CSR_RW, 0x00000000 },
  { "EMAC_RX_CTL_IOCFG", 0x0000009C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_mdc_iocfg
  { "pad_emac_mdc_pu", 0x000000A0,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_emac_mdc_pd", 0x000000A0,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_emac_mdc_pcfg", 0x000000A0, 19, 16,   CSR_RW, 0x00000000 },
  { "EMAC_MDC_IOCFG", 0x000000A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_mdio_iocfg
  { "pad_emac_mdio_pu", 0x000000A4,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_emac_mdio_pd", 0x000000A4,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_emac_mdio_pcfg", 0x000000A4, 19, 16,   CSR_RW, 0x00000000 },
  { "EMAC_MDIO_IOCFG", 0x000000A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD spi1_sdi_iocfg
  { "pad_spi1_sdi_pu", 0x000000A8,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_spi1_sdi_pd", 0x000000A8,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_spi1_sdi_pcfg", 0x000000A8, 19, 16,   CSR_RW, 0x00000000 },
  { "SPI1_SDI_IOCFG", 0x000000A8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD spi1_sdo_iocfg
  { "pad_spi1_sdo_pu", 0x000000AC,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_spi1_sdo_pd", 0x000000AC,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_spi1_sdo_pcfg", 0x000000AC, 19, 16,   CSR_RW, 0x00000000 },
  { "SPI1_SDO_IOCFG", 0x000000AC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD spi1_sck_iocfg
  { "pad_spi1_sck_pu", 0x000000B0,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_spi1_sck_pd", 0x000000B0,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_spi1_sck_pcfg", 0x000000B0, 19, 16,   CSR_RW, 0x00000000 },
  { "SPI1_SCK_IOCFG", 0x000000B0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio7_iocfg
  { "pad_gpio7_pu", 0x000000B4,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_gpio7_pd", 0x000000B4,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_gpio7_pcfg", 0x000000B4, 19, 16,   CSR_RW, 0x00000000 },
  { "GPIO7_IOCFG", 0x000000B4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2c0_sda_iocfg
  { "pad_i2c0_sda_pu", 0x000000B8,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_i2c0_sda_pd", 0x000000B8,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_i2c0_sda_pcfg", 0x000000B8, 18, 16,   CSR_RW, 0x00000000 },
  { "I2C0_SDA_IOCFG", 0x000000B8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2c0_scl_iocfg
  { "pad_i2c0_scl_pu", 0x000000BC,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_i2c0_scl_pd", 0x000000BC,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_i2c0_scl_pcfg", 0x000000BC, 18, 16,   CSR_RW, 0x00000000 },
  { "I2C0_SCL_IOCFG", 0x000000BC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sensor_clk_iocfg
  { "pad_sensor_clk_pu", 0x000000C0,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_sensor_clk_pd", 0x000000C0,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_sensor_clk_pcfg", 0x000000C0, 18, 16,   CSR_RW, 0x00000000 },
  { "SENSOR_CLK_IOCFG", 0x000000C0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm2_iocfg
  { "pad_pwm2_pu", 0x000000C4,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_pwm2_pd", 0x000000C4,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_pwm2_pcfg", 0x000000C4, 18, 16,   CSR_RW, 0x00000000 },
  { "PWM2_IOCFG", 0x000000C4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sensor_rstb_iocfg
  { "pad_sensor_rstb_pu", 0x000000C8,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_sensor_rstb_pd", 0x000000C8,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_sensor_rstb_pcfg", 0x000000C8, 18, 16,   CSR_RW, 0x00000000 },
  { "SENSOR_RSTB_IOCFG", 0x000000C8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sensor_pwdn_iocfg
  { "pad_sensor_pwdn_pu", 0x000000CC,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_sensor_pwdn_pd", 0x000000CC,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_sensor_pwdn_pcfg", 0x000000CC, 18, 16,   CSR_RW, 0x00000000 },
  { "SENSOR_PWDN_IOCFG", 0x000000CC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio4_iocfg
  { "pad_gpio4_pu", 0x000000D0,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_gpio4_pd", 0x000000D0,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_gpio4_pcfg", 0x000000D0, 19, 16,   CSR_RW, 0x00000000 },
  { "GPIO4_IOCFG", 0x000000D0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio5_iocfg
  { "pad_gpio5_pu", 0x000000D4,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_gpio5_pd", 0x000000D4,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_gpio5_pcfg", 0x000000D4, 19, 16,   CSR_RW, 0x00000000 },
  { "GPIO5_IOCFG", 0x000000D4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio6_iocfg
  { "pad_gpio6_pu", 0x000000D8,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_gpio6_pd", 0x000000D8,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_gpio6_pcfg", 0x000000D8, 19, 16,   CSR_RW, 0x00000000 },
  { "GPIO6_IOCFG", 0x000000D8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2s_tx_ck_iocfg
  { "pad_i2s_tx_ck_pu", 0x000000DC,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_i2s_tx_ck_pd", 0x000000DC,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_i2s_tx_ck_pcfg", 0x000000DC, 19, 16,   CSR_RW, 0x00000000 },
  { "I2S_TX_CK_IOCFG", 0x000000DC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2s_rx_ck_iocfg
  { "pad_i2s_rx_ck_pu", 0x000000E0,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_i2s_rx_ck_pd", 0x000000E0,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_i2s_rx_ck_pcfg", 0x000000E0, 19, 16,   CSR_RW, 0x00000000 },
  { "I2S_RX_CK_IOCFG", 0x000000E0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2s_rx_sd_iocfg
  { "pad_i2s_rx_sd_pu", 0x000000E4,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_i2s_rx_sd_pd", 0x000000E4,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_i2s_rx_sd_pcfg", 0x000000E4, 19, 16,   CSR_RW, 0x00000000 },
  { "I2S_RX_SD_IOCFG", 0x000000E4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2s_tx_sd_iocfg
  { "pad_i2s_tx_sd_pu", 0x000000E8,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_i2s_tx_sd_pd", 0x000000E8,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_i2s_tx_sd_pcfg", 0x000000E8, 19, 16,   CSR_RW, 0x00000000 },
  { "I2S_TX_SD_IOCFG", 0x000000E8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2s_rx_ws_iocfg
  { "pad_i2s_rx_ws_pu", 0x000000EC,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_i2s_rx_ws_pd", 0x000000EC,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_i2s_rx_ws_pcfg", 0x000000EC, 19, 16,   CSR_RW, 0x00000000 },
  { "I2S_RX_WS_IOCFG", 0x000000EC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2s_tx_ws_iocfg
  { "pad_i2s_tx_ws_pu", 0x000000F0,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_i2s_tx_ws_pd", 0x000000F0,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_i2s_tx_ws_pcfg", 0x000000F0, 19, 16,   CSR_RW, 0x00000000 },
  { "I2S_TX_WS_IOCFG", 0x000000F0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio2_iocfg
  { "pad_gpio2_pu", 0x000000F4,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_gpio2_pd", 0x000000F4,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_gpio2_pcfg", 0x000000F4, 19, 16,   CSR_RW, 0x00000000 },
  { "GPIO2_IOCFG", 0x000000F4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD uart0_txd_iocfg
  { "pad_uart0_txd_pu", 0x000000F8,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_uart0_txd_pd", 0x000000F8,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_uart0_txd_pcfg", 0x000000F8, 19, 16,   CSR_RW, 0x00000000 },
  { "UART0_TXD_IOCFG", 0x000000F8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD uart0_rxd_iocfg
  { "pad_uart0_rxd_pu", 0x000000FC,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_uart0_rxd_pd", 0x000000FC,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_uart0_rxd_pcfg", 0x000000FC, 19, 16,   CSR_RW, 0x00000000 },
  { "UART0_RXD_IOCFG", 0x000000FC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD eirq1_iocfg
  { "pad_eirq1_pu", 0x00000100,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_eirq1_pd", 0x00000100,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_eirq1_pcfg", 0x00000100, 19, 16,   CSR_RW, 0x00000000 },
  { "EIRQ1_IOCFG", 0x00000100, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm3_iocfg
  { "pad_pwm3_pu", 0x00000104,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_pwm3_pd", 0x00000104,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_pwm3_pcfg", 0x00000104, 19, 16,   CSR_RW, 0x00000000 },
  { "PWM3_IOCFG", 0x00000104, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm0_iocfg
  { "pad_pwm0_pu", 0x00000108,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_pwm0_pd", 0x00000108,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_pwm0_pcfg", 0x00000108, 19, 16,   CSR_RW, 0x00000000 },
  { "PWM0_IOCFG", 0x00000108, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio0_iocfg
  { "pad_gpio0_pu", 0x0000010C,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_gpio0_pd", 0x0000010C,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_gpio0_pcfg", 0x0000010C, 19, 16,   CSR_RW, 0x00000000 },
  { "GPIO0_IOCFG", 0x0000010C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio1_iocfg
  { "pad_gpio1_pu", 0x00000110,  0,  0,   CSR_RW, 0x00000000 },
  { "pad_gpio1_pd", 0x00000110,  1,  1,   CSR_RW, 0x00000000 },
  { "pad_gpio1_pcfg", 0x00000110, 19, 16,   CSR_RW, 0x00000000 },
  { "GPIO1_IOCFG", 0x00000110, 31, 0,   CSR_RW, 0x00000000 },
  // WORD spi0_sck_iosel
  { "pad_spi0_sck_iosel", 0x00000114,  2,  0,   CSR_RW, 0x00000000 },
  { "SPI0_SCK_IOSEL", 0x00000114, 31, 0,   CSR_RW, 0x00000000 },
  // WORD spi0_sdi_iosel
  { "pad_spi0_sdi_iosel", 0x00000118,  2,  0,   CSR_RW, 0x00000000 },
  { "SPI0_SDI_IOSEL", 0x00000118, 31, 0,   CSR_RW, 0x00000000 },
  // WORD spi0_sdo_iosel
  { "pad_spi0_sdo_iosel", 0x0000011C,  2,  0,   CSR_RW, 0x00000000 },
  { "SPI0_SDO_IOSEL", 0x0000011C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD eirq0_iosel
  { "pad_eirq0_iosel", 0x00000120,  2,  0,   CSR_RW, 0x00000000 },
  { "EIRQ0_IOSEL", 0x00000120, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sd_cd_iosel
  { "pad_sd_cd_iosel", 0x00000124,  2,  0,   CSR_RW, 0x00000001 },
  { "SD_CD_IOSEL", 0x00000124, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sd_d2_iosel
  { "pad_sd_d2_iosel", 0x00000128,  2,  0,   CSR_RW, 0x00000001 },
  { "SD_D2_IOSEL", 0x00000128, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sd_d3_iosel
  { "pad_sd_d3_iosel", 0x0000012C,  2,  0,   CSR_RW, 0x00000001 },
  { "SD_D3_IOSEL", 0x0000012C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sd_cmd_iosel
  { "pad_sd_cmd_iosel", 0x00000130,  2,  0,   CSR_RW, 0x00000001 },
  { "SD_CMD_IOSEL", 0x00000130, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sd_ck_iosel
  { "pad_sd_ck_iosel", 0x00000134,  2,  0,   CSR_RW, 0x00000001 },
  { "SD_CK_IOSEL", 0x00000134, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sd_d0_iosel
  { "pad_sd_d0_iosel", 0x00000138,  2,  0,   CSR_RW, 0x00000001 },
  { "SD_D0_IOSEL", 0x00000138, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sd_d1_iosel
  { "pad_sd_d1_iosel", 0x0000013C,  2,  0,   CSR_RW, 0x00000001 },
  { "SD_D1_IOSEL", 0x0000013C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio3_iosel
  { "pad_gpio3_iosel", 0x00000140,  2,  0,   CSR_RW, 0x00000000 },
  { "GPIO3_IOSEL", 0x00000140, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm1_iosel
  { "pad_pwm1_iosel", 0x00000144,  2,  0,   CSR_RW, 0x00000005 },
  { "PWM1_IOSEL", 0x00000144, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm4_iosel
  { "pad_pwm4_iosel", 0x00000148,  2,  0,   CSR_RW, 0x00000000 },
  { "PWM4_IOSEL", 0x00000148, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2c1_scl_iosel
  { "pad_i2c1_scl_iosel", 0x0000014C,  2,  0,   CSR_RW, 0x00000001 },
  { "I2C1_SCL_IOSEL", 0x0000014C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2c1_sda_iosel
  { "pad_i2c1_sda_iosel", 0x00000150,  2,  0,   CSR_RW, 0x00000001 },
  { "I2C1_SDA_IOSEL", 0x00000150, 31, 0,   CSR_RW, 0x00000000 },
  // WORD uart1_txd_iosel
  { "pad_uart1_txd_iosel", 0x00000154,  2,  0,   CSR_RW, 0x00000001 },
  { "UART1_TXD_IOSEL", 0x00000154, 31, 0,   CSR_RW, 0x00000000 },
  // WORD uart1_rxd_iosel
  { "pad_uart1_rxd_iosel", 0x00000158,  2,  0,   CSR_RW, 0x00000001 },
  { "UART1_RXD_IOSEL", 0x00000158, 31, 0,   CSR_RW, 0x00000000 },
  // WORD uart2_txd_iosel
  { "pad_uart2_txd_iosel", 0x0000015C,  2,  0,   CSR_RW, 0x00000000 },
  { "UART2_TXD_IOSEL", 0x0000015C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD uart2_rxd_iosel
  { "pad_uart2_rxd_iosel", 0x00000160,  2,  0,   CSR_RW, 0x00000000 },
  { "UART2_RXD_IOSEL", 0x00000160, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi_ce_n_iosel
  { "pad_qspi_ce_n_iosel", 0x00000198,  2,  0,   CSR_RW, 0x00000001 },
  { "QSPI_CE_N_IOSEL", 0x00000198, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi_d1_iosel
  { "pad_qspi_d1_iosel", 0x0000019C,  2,  0,   CSR_RW, 0x00000001 },
  { "QSPI_D1_IOSEL", 0x0000019C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi_d2_iosel
  { "pad_qspi_d2_iosel", 0x000001A0,  2,  0,   CSR_RW, 0x00000001 },
  { "QSPI_D2_IOSEL", 0x000001A0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi_d3_iosel
  { "pad_qspi_d3_iosel", 0x000001A4,  2,  0,   CSR_RW, 0x00000001 },
  { "QSPI_D3_IOSEL", 0x000001A4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi_ck_iosel
  { "pad_qspi_ck_iosel", 0x000001A8,  2,  0,   CSR_RW, 0x00000001 },
  { "QSPI_CK_IOSEL", 0x000001A8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD qspi_d0_iosel
  { "pad_qspi_d0_iosel", 0x000001AC,  2,  0,   CSR_RW, 0x00000001 },
  { "QSPI_D0_IOSEL", 0x000001AC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ephy_rst_iosel
  { "pad_ephy_rst_iosel", 0x000001B0,  2,  0,   CSR_RW, 0x00000000 },
  { "EPHY_RST_IOSEL", 0x000001B0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_tx_ck_iosel
  { "pad_emac_tx_ck_iosel", 0x000001B4,  2,  0,   CSR_RW, 0x00000000 },
  { "EMAC_TX_CK_IOSEL", 0x000001B4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_tx_ctl_iosel
  { "pad_emac_tx_ctl_iosel", 0x000001B8,  2,  0,   CSR_RW, 0x00000000 },
  { "EMAC_TX_CTL_IOSEL", 0x000001B8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_tx_d3_iosel
  { "pad_emac_tx_d3_iosel", 0x000001BC,  2,  0,   CSR_RW, 0x00000000 },
  { "EMAC_TX_D3_IOSEL", 0x000001BC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_tx_d2_iosel
  { "pad_emac_tx_d2_iosel", 0x000001C0,  2,  0,   CSR_RW, 0x00000000 },
  { "EMAC_TX_D2_IOSEL", 0x000001C0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_tx_d1_iosel
  { "pad_emac_tx_d1_iosel", 0x000001C4,  2,  0,   CSR_RW, 0x00000000 },
  { "EMAC_TX_D1_IOSEL", 0x000001C4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_tx_d0_iosel
  { "pad_emac_tx_d0_iosel", 0x000001C8,  2,  0,   CSR_RW, 0x00000000 },
  { "EMAC_TX_D0_IOSEL", 0x000001C8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_rx_ck_iosel
  { "pad_emac_rx_ck_iosel", 0x000001CC,  2,  0,   CSR_RW, 0x00000000 },
  { "EMAC_RX_CK_IOSEL", 0x000001CC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_rx_d0_iosel
  { "pad_emac_rx_d0_iosel", 0x000001D0,  2,  0,   CSR_RW, 0x00000000 },
  { "EMAC_RX_D0_IOSEL", 0x000001D0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_rx_d1_iosel
  { "pad_emac_rx_d1_iosel", 0x000001D4,  2,  0,   CSR_RW, 0x00000000 },
  { "EMAC_RX_D1_IOSEL", 0x000001D4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_rx_d2_iosel
  { "pad_emac_rx_d2_iosel", 0x000001D8,  2,  0,   CSR_RW, 0x00000000 },
  { "EMAC_RX_D2_IOSEL", 0x000001D8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_rx_d3_iosel
  { "pad_emac_rx_d3_iosel", 0x000001DC,  2,  0,   CSR_RW, 0x00000000 },
  { "EMAC_RX_D3_IOSEL", 0x000001DC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_rx_ctl_iosel
  { "pad_emac_rx_ctl_iosel", 0x000001E0,  2,  0,   CSR_RW, 0x00000000 },
  { "EMAC_RX_CTL_IOSEL", 0x000001E0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_mdc_iosel
  { "pad_emac_mdc_iosel", 0x000001E4,  2,  0,   CSR_RW, 0x00000000 },
  { "EMAC_MDC_IOSEL", 0x000001E4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD emac_mdio_iosel
  { "pad_emac_mdio_iosel", 0x000001E8,  2,  0,   CSR_RW, 0x00000000 },
  { "EMAC_MDIO_IOSEL", 0x000001E8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD spi1_sdi_iosel
  { "pad_spi1_sdi_iosel", 0x000001EC,  2,  0,   CSR_RW, 0x00000000 },
  { "SPI1_SDI_IOSEL", 0x000001EC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD spi1_sdo_iosel
  { "pad_spi1_sdo_iosel", 0x000001F0,  2,  0,   CSR_RW, 0x00000000 },
  { "SPI1_SDO_IOSEL", 0x000001F0, 31, 0,   CSR_RW, 0x00000000 },
  // WORD spi1_sck_iosel
  { "pad_spi1_sck_iosel", 0x000001F4,  2,  0,   CSR_RW, 0x00000000 },
  { "SPI1_SCK_IOSEL", 0x000001F4, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio7_iosel
  { "pad_gpio7_iosel", 0x000001F8,  2,  0,   CSR_RW, 0x00000000 },
  { "GPIO7_IOSEL", 0x000001F8, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2c0_sda_iosel
  { "pad_i2c0_sda_iosel", 0x000001FC,  2,  0,   CSR_RW, 0x00000001 },
  { "I2C0_SDA_IOSEL", 0x000001FC, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2c0_scl_iosel
  { "pad_i2c0_scl_iosel", 0x00000200,  2,  0,   CSR_RW, 0x00000001 },
  { "I2C0_SCL_IOSEL", 0x00000200, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sensor_clk_iosel
  { "pad_sensor_clk_iosel", 0x00000204,  2,  0,   CSR_RW, 0x00000001 },
  { "SENSOR_CLK_IOSEL", 0x00000204, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm2_iosel
  { "pad_pwm2_iosel", 0x00000208,  2,  0,   CSR_RW, 0x00000000 },
  { "PWM2_IOSEL", 0x00000208, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sensor_rstb_iosel
  { "pad_sensor_rstb_iosel", 0x0000020C,  2,  0,   CSR_RW, 0x00000000 },
  { "SENSOR_RSTB_IOSEL", 0x0000020C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD sensor_pwdn_iosel
  { "pad_sensor_pwdn_iosel", 0x00000210,  2,  0,   CSR_RW, 0x00000000 },
  { "SENSOR_PWDN_IOSEL", 0x00000210, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio4_iosel
  { "pad_gpio4_iosel", 0x00000214,  2,  0,   CSR_RW, 0x00000000 },
  { "GPIO4_IOSEL", 0x00000214, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio5_iosel
  { "pad_gpio5_iosel", 0x00000218,  2,  0,   CSR_RW, 0x00000000 },
  { "GPIO5_IOSEL", 0x00000218, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio6_iosel
  { "pad_gpio6_iosel", 0x0000021C,  2,  0,   CSR_RW, 0x00000000 },
  { "GPIO6_IOSEL", 0x0000021C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2s_tx_ck_iosel
  { "pad_i2s_tx_ck_iosel", 0x00000220,  2,  0,   CSR_RW, 0x00000000 },
  { "I2S_TX_CK_IOSEL", 0x00000220, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2s_rx_ck_iosel
  { "pad_i2s_rx_ck_iosel", 0x00000224,  2,  0,   CSR_RW, 0x00000000 },
  { "I2S_RX_CK_IOSEL", 0x00000224, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2s_rx_sd_iosel
  { "pad_i2s_rx_sd_iosel", 0x00000228,  2,  0,   CSR_RW, 0x00000000 },
  { "I2S_RX_SD_IOSEL", 0x00000228, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2s_tx_sd_iosel
  { "pad_i2s_tx_sd_iosel", 0x0000022C,  2,  0,   CSR_RW, 0x00000000 },
  { "I2S_TX_SD_IOSEL", 0x0000022C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2s_rx_ws_iosel
  { "pad_i2s_rx_ws_iosel", 0x00000230,  2,  0,   CSR_RW, 0x00000000 },
  { "I2S_RX_WS_IOSEL", 0x00000230, 31, 0,   CSR_RW, 0x00000000 },
  // WORD i2s_tx_ws_iosel
  { "pad_i2s_tx_ws_iosel", 0x00000234,  2,  0,   CSR_RW, 0x00000000 },
  { "I2S_TX_WS_IOSEL", 0x00000234, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio2_iosel
  { "pad_gpio2_iosel", 0x00000238,  2,  0,   CSR_RW, 0x00000000 },
  { "GPIO2_IOSEL", 0x00000238, 31, 0,   CSR_RW, 0x00000000 },
  // WORD uart0_txd_iosel
  { "pad_uart0_txd_iosel", 0x0000023C,  2,  0,   CSR_RW, 0x00000001 },
  { "UART0_TXD_IOSEL", 0x0000023C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD uart0_rxd_iosel
  { "pad_uart0_rxd_iosel", 0x00000240,  2,  0,   CSR_RW, 0x00000001 },
  { "UART0_RXD_IOSEL", 0x00000240, 31, 0,   CSR_RW, 0x00000000 },
  // WORD eirq1_iosel
  { "pad_eirq1_iosel", 0x00000244,  2,  0,   CSR_RW, 0x00000000 },
  { "EIRQ1_IOSEL", 0x00000244, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm3_iosel
  { "pad_pwm3_iosel", 0x00000248,  2,  0,   CSR_RW, 0x00000005 },
  { "PWM3_IOSEL", 0x00000248, 31, 0,   CSR_RW, 0x00000000 },
  // WORD pwm0_iosel
  { "pad_pwm0_iosel", 0x0000024C,  2,  0,   CSR_RW, 0x00000005 },
  { "PWM0_IOSEL", 0x0000024C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio0_iosel
  { "pad_gpio0_iosel", 0x00000250,  2,  0,   CSR_RW, 0x00000005 },
  { "GPIO0_IOSEL", 0x00000250, 31, 0,   CSR_RW, 0x00000000 },
  // WORD gpio1_iosel
  { "pad_gpio1_iosel", 0x00000254,  2,  0,   CSR_RW, 0x00000005 },
  { "GPIO1_IOSEL", 0x00000254, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_PIOC_H_
