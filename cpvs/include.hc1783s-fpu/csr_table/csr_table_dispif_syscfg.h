#ifndef CSR_TABLE_DISPIF_SYSCFG_H_
#define CSR_TABLE_DISPIF_SYSCFG_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_dispif_syscfg[] =
{
  // WORD ckg_rx0
  { "cken_ctrl", 0x00000000,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_RX0", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_rx1
  { "cken_tx_pix", 0x00000004,  0,  0,   CSR_RW, 0x00000001 },
  { "cken_tx_enc", 0x00000004,  8,  8,   CSR_RW, 0x00000001 },
  { "CKG_RX1", 0x00000004, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_dec0
  { "cken_tx_sys_phy", 0x00000008,  0,  0,   CSR_RW, 0x00000001 },
  { "cken_tx_phy", 0x00000008,  8,  8,   CSR_RW, 0x00000001 },
  { "CKG_DEC0", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD ckg_ps
  { "cken_pd", 0x00000010,  0,  0,   CSR_RW, 0x00000001 },
  { "CKG_PS", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD resv
  { "reserved", 0x0000003C, 15,  0,   CSR_RW, 0x00000000 },
  { "RESV", 0x0000003C, 31, 0,   CSR_RW, 0x00000000 },
  // WORD rst_rx0
  { "sw_rst_tx_enc_pix_clk", 0x00000040,  0,  0,  CSR_W1P, 0x00000000 },
  { "sw_rst_tx_enc_enc_clk", 0x00000040,  8,  8,  CSR_W1P, 0x00000000 },
  { "csr_rst_tx_enc_p_clk_g", 0x00000040, 24, 24,  CSR_W1P, 0x00000000 },
  { "RST_RX0", 0x00000040, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_rx1
  { "csr_rst_tx_output_p_clk_g", 0x00000044,  0,  0,  CSR_W1P, 0x00000000 },
  { "RST_RX1", 0x00000044, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_dec0
  { "sw_rst_tx_phy_sys_clk", 0x00000048,  0,  0,  CSR_W1P, 0x00000000 },
  { "sw_rst_tx_phy_phy_clk", 0x00000048,  8,  8,  CSR_W1P, 0x00000000 },
  { "csr_rst_tx_phy_p_clk_g", 0x00000048, 16, 16,  CSR_W1P, 0x00000000 },
  { "RST_DEC0", 0x00000048, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_dec1
  { "sw_rst_pd_pix_clk", 0x0000004C,  0,  0,  CSR_W1P, 0x00000000 },
  { "sw_rst_pd_tx_pll_out_clk", 0x0000004C,  8,  8,  CSR_W1P, 0x00000000 },
  { "csr_rst_pd_p_clk_g", 0x0000004C, 16, 16,  CSR_W1P, 0x00000000 },
  { "RST_DEC1", 0x0000004C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD rst_ps
  { "csr_rst_ctrl_p_clk_g", 0x00000060,  0,  0,  CSR_W1P, 0x00000000 },
  { "RST_PS", 0x00000060, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD lvrst_rx0
  { "lvl_rst_tx_enc_pix_clk", 0x00000080,  0,  0,   CSR_RW, 0x00000000 },
  { "lvl_rst_tx_enc_enc_clk", 0x00000080,  8,  8,   CSR_RW, 0x00000000 },
  { "LVRST_RX0", 0x00000080, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_dec0
  { "lvl_rst_tx_phy_sys_clk", 0x00000088,  0,  0,   CSR_RW, 0x00000000 },
  { "lvl_rst_tx_phy_phy_clk", 0x00000088,  8,  8,   CSR_RW, 0x00000000 },
  { "LVRST_DEC0", 0x00000088, 31, 0,   CSR_RW, 0x00000000 },
  // WORD lvrst_dec1
  { "lvl_rst_pd_pix_clk", 0x0000008C,  0,  0,   CSR_RW, 0x00000000 },
  { "lvl_rst_pd_tx_pll_out_clk", 0x0000008C,  8,  8,   CSR_RW, 0x00000000 },
  { "LVRST_DEC1", 0x0000008C, 31, 0,   CSR_RW, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_DISPIF_SYSCFG_H_
