#ifndef CSR_TABLE_H_
#define CSR_TABLE_H_

#include <stdint.h>

#define CSR_RW  0
#define CSR_WO  1
#define CSR_RO  2
#define CSR_W1P 3

typedef struct
{
  char * field_name;
  uint32_t offset;
  uint32_t msb;
  uint32_t lsb;
  int access_type;
  uint32_t default_value;
} CsrFieldEntry;

typedef struct
{
  char * region_bank_name;
  uint32_t start_address;
  uint32_t end_address;
  uint32_t start_offset;
  uint32_t end_offset;
  CsrFieldEntry * field_table;
} CsrRegionBankEntry;

#include "csr_table_audio_hpf.h"
#include "csr_table_tx_enc.h"
#include "csr_table_adocodec.h"
#include "csr_table_slb.h"
#include "csr_table_rtc.h"
#include "csr_table_emac.h"
#include "csr_table_jpeg.h"
#include "csr_table_ipll.h"
#include "csr_table_syscfg.h"
#include "csr_table_ps.h"
#include "csr_table_adodec.h"
#include "csr_table_ccq.h"
#include "csr_table_dwb2r.h"
#include "csr_table_dma_8_c1_d64_syscfg.h"
#include "csr_table_bld.h"
#include "csr_table_sdc_cfg.h"
#include "csr_table_isp_cfg.h"
#include "csr_table_ccm.h"
#include "csr_table_is_checksum.h"
#include "csr_table_rst.h"
#include "csr_table_fcs.h"
#include "csr_table_adoin_syscfg.h"
#include "csr_table_dms.h"
#include "csr_table_sdc.h"
#include "csr_table_tx_ctrl.h"
#include "csr_table_yuv420to444vp.h"
#include "csr_table_i2cs.h"
#include "csr_table_amc.h"
#include "csr_table_nr.h"
#include "csr_table_eirq.h"
#include "csr_table_adoenc.h"
#include "csr_table_ispin_checksum.h"
#include "csr_table_pxw.h"
#include "csr_table_apreampcfg.h"
#include "csr_table_refw.h"
#include "csr_table_wdt_aon.h"
#include "csr_table_audio_fade_out.h"
#include "csr_table_eqos_cfg.h"
#include "csr_table_axiqos.h"
#include "csr_table_aioc.h"
#include "csr_table_fpnr.h"
#include "csr_table_adoout_syscfg.h"
#include "csr_table_cst.h"
#include "csr_table_gpioc.h"
#include "csr_table_peri.h"
#include "csr_table_dosd_read.h"
#include "csr_table_fm.h"
#include "csr_table_tx_out.h"
#include "csr_table_enh.h"
#include "csr_table_usbcfg.h"
#include "csr_table_enc_syscfg.h"
#include "csr_table_aes_dec.h"
#include "csr_table_dispif_syscfg.h"
#include "csr_table_disp.h"
#include "csr_table_mv8w.h"
#include "csr_table_perf.h"
#include "csr_table_qspiw.h"
#include "csr_table_dma_8_c1_d64.h"
#include "csr_table_spi.h"
#include "csr_table_rx_ctrl.h"
#include "csr_table_pioc.h"
#include "csr_table_audiow.h"
#include "csr_table_isp_checksum.h"
#include "csr_table_accq.h"
#include "csr_table_is.h"
#include "csr_table_pxr.h"
#include "csr_table_dhz.h"
#include "csr_table_weightr.h"
#include "csr_table_rst_aon.h"
#include "csr_table_pca.h"
#include "csr_table_senif_syscfg.h"
#include "csr_table_isp.h"
#include "csr_table_timer.h"
#include "csr_table_refr.h"
#include "csr_table_dcfa.h"
#include "csr_table_nrw.h"
#include "csr_table_dgma.h"
#include "csr_table_ft.h"
#include "csr_table_mcvp.h"
#include "csr_table_gma.h"
#include "csr_table_qspi.h"
#include "csr_table_isk_checksum.h"
#include "csr_table_sc.h"
#include "csr_table_dec.h"
#include "csr_table_disp_checksum.h"
#include "csr_table_i2cm.h"
#include "csr_table_shp.h"
#include "csr_table_enh_checksum.h"
#include "csr_table_darb.h"
#include "csr_table_adaccfg.h"
#include "csr_table_pg.h"
#include "csr_table_venc.h"
#include "csr_table_ck.h"
#include "csr_table_dpchdr_checksum.h"
#include "csr_table_wdt.h"
#include "csr_table_mer.h"
#include "csr_table_rx_phycfg.h"
#include "csr_table_tx_phycfg.h"
#include "csr_table_dbf.h"
#include "csr_table_gic.h"
#include "csr_table_dramcontroller.h"
#include "csr_table_d422to444.h"
#include "csr_table_dbc.h"
#include "csr_table_d420to422.h"
#include "csr_table_isk.h"
#include "csr_table_ccqw.h"
#include "csr_table_srcr.h"
#include "csr_table_rx.h"
#include "csr_table_audio_fade_in.h"
#include "csr_table_lpmd.h"
#include "csr_table_cvs.h"
#include "csr_table_enc.h"
#include "csr_table_crop.h"
#include "csr_table_gfx.h"
#include "csr_table_pwm.h"
#include "csr_table_venc_mvw.h"
#include "csr_table_frame_time_gen.h"
#include "csr_table_vpcst.h"
#include "csr_table_bls.h"
#include "csr_table_adccfg.h"
#include "csr_table_dispif_ctr.h"
#include "csr_table_fpll.h"
#include "csr_table_osdr.h"
#include "csr_table_fpga_cfg.h"
#include "csr_table_fsc.h"
#include "csr_table_dosd.h"
#include "csr_table_usbotg.h"
#include "csr_table_bsp.h"
#include "csr_table_cs.h"
#include "csr_table_edp.h"
#include "csr_table_vp_checksum.h"
#include "csr_table_bsw.h"
#include "csr_table_audio_agc.h"
#include "csr_table_vp.h"
#include "csr_table_qspir.h"
#include "csr_table_ddrphy.h"
#include "csr_table_lsc.h"
#include "csr_table_ck_aon.h"
#include "csr_table_efuse_ctrl.h"
#include "csr_table_audior.h"
#include "csr_table_uart.h"
#include "csr_table_adoout.h"
#include "csr_table_isk_cfg.h"
#include "csr_table_pdi.h"
#include "csr_table_me.h"
#include "csr_table_senif_ctrl.h"
#include "csr_table_aes_enc.h"
#include "csr_table_adoin.h"
#include "csr_table_hdr.h"
#include "csr_table_is_cfg.h"
#include "csr_table_disp_cfg.h"
#include "csr_table_adac_ctr.h"
#include "csr_table_dpc.h"
#include "csr_table_ck_apb2.h"
#include "csr_table_ck_apb3.h"
#include "csr_table_ck_apb1.h"
#include "csr_table_aon.h"
#include "csr_table_venc_mvr.h"
#include "csr_table_cpudbg.h"
#include "csr_table_dcc.h"
#include "csr_table_mv8r.h"
#include "csr_table_adcctr.h"
#include "csr_table_dcst.h"
#include "csr_table_coordr.h"
#include "csr_table_i2s.h"
#include "csr_table_audio_anr.h"
#include "csr_table_ccfg.h"

CsrRegionBankEntry csr_region_bank_table[] =
{
  { "pc.ck", 0x80000000, 0x800000FF, 0x00000000, 0x000000FF, csr_field_table_ck },
  { "pc.rst", 0x80000400, 0x800007FF, 0x00000400, 0x000007FF, csr_field_table_rst },
  { "pc.syscfg", 0x80000800, 0x8000083F, 0x00000800, 0x0000083F, csr_field_table_syscfg },
  { "io.aioc", 0x80001000, 0x800013FF, 0x00001000, 0x000013FF, csr_field_table_aioc },
  { "io.pioc", 0x80001400, 0x800017FF, 0x00001400, 0x000017FF, csr_field_table_pioc },
  { "io.gpioc", 0x80001800, 0x80001BFF, 0x00001800, 0x00001BFF, csr_field_table_gpioc },
  { "eirq.eirq", 0x80002000, 0x80002FFF, 0x00002000, 0x00002FFF, csr_field_table_eirq },
  { "pll_cascade.ipll", 0x80010000, 0x8001007F, 0x00010000, 0x0001007F, csr_field_table_ipll },
  { "pll_cascade.fm", 0x80010400, 0x800107FF, 0x00010400, 0x000107FF, csr_field_table_fm },
  { "pll_ddr.fpll", 0x80011000, 0x8001107F, 0x00011000, 0x0001107F, csr_field_table_fpll },
  { "pll_ddr.fm", 0x80011400, 0x800117FF, 0x00011400, 0x000117FF, csr_field_table_fm },
  { "pll_cpu.ipll", 0x80012000, 0x8001207F, 0x00012000, 0x0001207F, csr_field_table_ipll },
  { "pll_cpu.fm", 0x80012400, 0x800127FF, 0x00012400, 0x000127FF, csr_field_table_fm },
  { "pll_emac.ipll", 0x80013000, 0x8001307F, 0x00013000, 0x0001307F, csr_field_table_ipll },
  { "pll_emac.fm", 0x80013400, 0x800137FF, 0x00013400, 0x000137FF, csr_field_table_fm },
  { "pll_sensor.ipll", 0x80014000, 0x8001407F, 0x00014000, 0x0001407F, csr_field_table_ipll },
  { "pll_sensor.fm", 0x80014400, 0x800147FF, 0x00014400, 0x000147FF, csr_field_table_fm },
  { "pll_venc.ipll", 0x80015000, 0x8001507F, 0x00015000, 0x0001507F, csr_field_table_ipll },
  { "pll_venc.fm", 0x80015400, 0x800157FF, 0x00015400, 0x000157FF, csr_field_table_fm },
  { "pll_audio.fpll", 0x80016000, 0x8001607F, 0x00016000, 0x0001607F, csr_field_table_fpll },
  { "pll_audio.fm", 0x80016400, 0x800167FF, 0x00016400, 0x000167FF, csr_field_table_fm },
  { "peri.peri", 0x80020000, 0x8002FFFF, 0x00020000, 0x0002FFFF, csr_field_table_peri },
  { "ft.ft", 0x80030000, 0x8003FFFF, 0x00030000, 0x0003FFFF, csr_field_table_ft },
  { "i2cs.i2cs", 0x80040000, 0x8004FFFF, 0x00040000, 0x0004FFFF, csr_field_table_i2cs },
  { "timer.timer", 0x80050000, 0x800503FF, 0x00050000, 0x000503FF, csr_field_table_timer },
  { "timer.wdt", 0x80050400, 0x800507FF, 0x00050400, 0x000507FF, csr_field_table_wdt },
  { "pwm.pwm", 0x80060000, 0x8006FFFF, 0x00060000, 0x0006FFFF, csr_field_table_pwm },
  { "i2cm0.i2cm", 0x80070000, 0x8007FFFF, 0x00070000, 0x0007FFFF, csr_field_table_i2cm },
  { "i2cm1.i2cm", 0x80080000, 0x8008FFFF, 0x00080000, 0x0008FFFF, csr_field_table_i2cm },
  { "efuse_ctrl.efuse_ctrl", 0x80090000, 0x800903FF, 0x00090000, 0x000903FF, csr_field_table_efuse_ctrl },
  { "adoadc_cfg.adccfg", 0x80100000, 0x801000FF, 0x00100000, 0x001000FF, csr_field_table_adccfg },
  { "adoadc_ctr.adcctr", 0x80108000, 0x801083FF, 0x00108000, 0x001083FF, csr_field_table_adcctr },
  { "adodac_cfg.adaccfg", 0x80110000, 0x801100FF, 0x00110000, 0x001100FF, csr_field_table_adaccfg },
  { "adodac_ctr.adac_ctr", 0x80120000, 0x801203FF, 0x00120000, 0x001203FF, csr_field_table_adac_ctr },
  { "adopreamp.apreampcfg", 0x80130000, 0x801300FF, 0x00130000, 0x001300FF, csr_field_table_apreampcfg },
  { "adoin.adoin", 0x80140000, 0x801403FF, 0x00140000, 0x001403FF, csr_field_table_adoin },
  { "adoin.adoin_syscfg", 0x80140400, 0x801407FF, 0x00140400, 0x001407FF, csr_field_table_adoin_syscfg },
  { "adoout.adoout", 0x80150000, 0x801503FF, 0x00150000, 0x001503FF, csr_field_table_adoout },
  { "adoout.adoout_syscfg", 0x80150400, 0x801507FF, 0x00150400, 0x001507FF, csr_field_table_adoout_syscfg },
  { "ador.audior", 0x80160000, 0x801603FF, 0x00160000, 0x001603FF, csr_field_table_audior },
  { "adow.audiow", 0x80170000, 0x801703FF, 0x00170000, 0x001703FF, csr_field_table_audiow },
  { "adoi2stx.i2s", 0x80180000, 0x8018FFFF, 0x00180000, 0x0018FFFF, csr_field_table_i2s },
  { "adoi2srx.i2s", 0x80190000, 0x8019FFFF, 0x00190000, 0x0019FFFF, csr_field_table_i2s },
  { "adoenc.adoenc", 0x801A0000, 0x801AFFFF, 0x001A0000, 0x001AFFFF, csr_field_table_adoenc },
  { "adodec.adodec", 0x801B0000, 0x801BFFFF, 0x001B0000, 0x001BFFFF, csr_field_table_adodec },
  { "adofadein.audio_fade_in", 0x801C0000, 0x801CFFFF, 0x001C0000, 0x001CFFFF, csr_field_table_audio_fade_in },
  { "adofadeout.audio_fade_out", 0x801D0000, 0x801DFFFF, 0x001D0000, 0x001DFFFF, csr_field_table_audio_fade_out },
  { "adohpf.audio_hpf", 0x801E0000, 0x801EFFFF, 0x001E0000, 0x001EFFFF, csr_field_table_audio_hpf },
  { "adoanr.audio_anr", 0x801F0000, 0x801FFFFF, 0x001F0000, 0x001FFFFF, csr_field_table_audio_anr },
  { "adoagc.audio_agc", 0x80210000, 0x8021FFFF, 0x00210000, 0x0021FFFF, csr_field_table_audio_agc },
  { "aoccq.accq", 0x80420000, 0x804200FF, 0x00420000, 0x004200FF, csr_field_table_accq },
  { "aon.aon", 0x80500000, 0x805003FF, 0x00500000, 0x005003FF, csr_field_table_aon },
  { "aon.ck_aon", 0x80500400, 0x805007FF, 0x00500400, 0x005007FF, csr_field_table_ck_aon },
  { "aon.rst_aon", 0x80500800, 0x80500BFF, 0x00500800, 0x00500BFF, csr_field_table_rst_aon },
  { "lpmd.lpmd", 0x80510000, 0x805103FF, 0x00510000, 0x005103FF, csr_field_table_lpmd },
  { "psd.ps", 0x80520000, 0x805203FF, 0x00520000, 0x005203FF, csr_field_table_ps },
  { "amc.amc", 0x80530000, 0x805303FF, 0x00530000, 0x005303FF, csr_field_table_amc },
  { "rtc.rtc", 0x80540000, 0x805403FF, 0x00540000, 0x005403FF, csr_field_table_rtc },
  { "wdt_aon.wdt_aon", 0x80550000, 0x805500FF, 0x00550000, 0x005500FF, csr_field_table_wdt_aon },
  { "spi0.spi", 0x80800000, 0x808000FF, 0x00800000, 0x008000FF, csr_field_table_spi },
  { "spi1.spi", 0x80810000, 0x808100FF, 0x00810000, 0x008100FF, csr_field_table_spi },
  { "uart0.uart", 0x80820000, 0x808200FF, 0x00820000, 0x008200FF, csr_field_table_uart },
  { "uart1.uart", 0x80830000, 0x808300FF, 0x00830000, 0x008300FF, csr_field_table_uart },
  { "uart2.uart", 0x80840000, 0x808400FF, 0x00840000, 0x008400FF, csr_field_table_uart },
  { "uart3.uart", 0x80850000, 0x808500FF, 0x00850000, 0x008500FF, csr_field_table_uart },
  { "uart4.uart", 0x80860000, 0x808600FF, 0x00860000, 0x008600FF, csr_field_table_uart },
  { "uart5.uart", 0x80870000, 0x808700FF, 0x00870000, 0x008700FF, csr_field_table_uart },
  { "cpudbg.cpudbg", 0x80A00000, 0x80BFFFFF, 0x00A00000, 0x00BFFFFF, csr_field_table_cpudbg },
  { "fpga_efuse.efuse_ctrl", 0x80C00000, 0x80C003FF, 0x00C00000, 0x00C003FF, csr_field_table_efuse_ctrl },
  { "fpga_cfg.fpga_cfg", 0x80FF0000, 0x80FFFFFF, 0x00FF0000, 0x00FFFFFF, csr_field_table_fpga_cfg },
  { "enc.enc", 0x81000000, 0x810003FF, 0x01000000, 0x010003FF, csr_field_table_enc },
  { "enc.osdr", 0x81000400, 0x810007FF, 0x01000400, 0x010007FF, csr_field_table_osdr },
  { "enc.srcr", 0x81000800, 0x81000BFF, 0x01000800, 0x01000BFF, csr_field_table_srcr },
  { "enc.bsw", 0x81000C00, 0x81000FFF, 0x01000C00, 0x01000FFF, csr_field_table_bsw },
  { "enc.enc_syscfg", 0x81001000, 0x810010FF, 0x01001000, 0x010010FF, csr_field_table_enc_syscfg },
  { "jpeg.jpeg", 0x81010000, 0x810103FF, 0x01010000, 0x010103FF, csr_field_table_jpeg },
  { "venc.venc", 0x81020000, 0x810203FF, 0x01020000, 0x010203FF, csr_field_table_venc },
  { "venc.venc_mvr", 0x81020400, 0x810207FF, 0x01020400, 0x010207FF, csr_field_table_venc_mvr },
  { "venc.refr", 0x81020800, 0x81020BFF, 0x01020800, 0x01020BFF, csr_field_table_refr },
  { "venc.refw", 0x81020C00, 0x81020FFF, 0x01020C00, 0x01020FFF, csr_field_table_refw },
  { "darb.darb", 0x81300000, 0x813001FF, 0x01300000, 0x013001FF, csr_field_table_darb },
  { "dramccfg.ccfg", 0x81310000, 0x813103FF, 0x01310000, 0x013103FF, csr_field_table_ccfg },
  { "dramccfg.perf", 0x81310400, 0x813107FF, 0x01310400, 0x013107FF, csr_field_table_perf },
  { "axiqos0.axiqos", 0x81320000, 0x813200FF, 0x01320000, 0x013200FF, csr_field_table_axiqos },
  { "axiqos1.axiqos", 0x81330000, 0x813300FF, 0x01330000, 0x013300FF, csr_field_table_axiqos },
  { "sdc_cfg0.sdc_cfg", 0x81340000, 0x8134FFFF, 0x01340000, 0x0134FFFF, csr_field_table_sdc_cfg },
  { "sdc_cfg1.sdc_cfg", 0x81350000, 0x8135FFFF, 0x01350000, 0x0135FFFF, csr_field_table_sdc_cfg },
  { "eqos_cfg.eqos_cfg", 0x81360000, 0x8136FFFF, 0x01360000, 0x0136FFFF, csr_field_table_eqos_cfg },
  { "usbcfg.usbcfg", 0x81370000, 0x813700FF, 0x01370000, 0x013700FF, csr_field_table_usbcfg },
  { "ck_apb1.ck_apb1", 0x81400000, 0x81400FFF, 0x01400000, 0x01400FFF, csr_field_table_ck_apb1 },
  { "ddrphy.ddrphy", 0x81840000, 0x818403FF, 0x01840000, 0x018403FF, csr_field_table_ddrphy },
  { "dramcontroller.dramcontroller", 0x81860000, 0x81867FFF, 0x01860000, 0x01867FFF, csr_field_table_dramcontroller },
  { "emacqoscontroller.emac", 0x81880000, 0x81881FFF, 0x01880000, 0x01881FFF, csr_field_table_emac },
  { "sdc0.sdc", 0x818A0000, 0x818A03FF, 0x018A0000, 0x018A03FF, csr_field_table_sdc },
  { "sdc1.sdc", 0x818C0000, 0x818C03FF, 0x018C0000, 0x018C03FF, csr_field_table_sdc },
  { "isp.isp", 0x82000000, 0x820000FF, 0x02000000, 0x020000FF, csr_field_table_isp },
  { "isp.isp_cfg", 0x82000400, 0x820007FF, 0x02000400, 0x020007FF, csr_field_table_isp_cfg },
  { "isp.isp_checksum", 0x82000800, 0x82000BFF, 0x02000800, 0x02000BFF, csr_field_table_isp_checksum },
  { "isp.ispin_checksum", 0x82000C00, 0x82000FFF, 0x02000C00, 0x02000FFF, csr_field_table_ispin_checksum },
  { "isp.dpchdr_checksum", 0x82001000, 0x820013FF, 0x02001000, 0x020013FF, csr_field_table_dpchdr_checksum },
  { "isp.enh_checksum", 0x82001400, 0x820017FF, 0x02001400, 0x020017FF, csr_field_table_enh_checksum },
  { "ispccq.ccq", 0x82010000, 0x820100FF, 0x02010000, 0x020100FF, csr_field_table_ccq },
  { "ispccq.osdr", 0x82010400, 0x820107FF, 0x02010400, 0x020107FF, csr_field_table_osdr },
  { "ispccq.ccqw", 0x82010800, 0x82010BFF, 0x02010800, 0x02010BFF, csr_field_table_ccqw },
  { "isppg0.pg", 0x82020000, 0x820203FF, 0x02020000, 0x020203FF, csr_field_table_pg },
  { "isppg1.pg", 0x82030000, 0x820303FF, 0x02030000, 0x020303FF, csr_field_table_pg },
  { "ispr0.pxr", 0x82040000, 0x820403FF, 0x02040000, 0x020403FF, csr_field_table_pxr },
  { "isprcs0.cs", 0x82050000, 0x8205FFFF, 0x02050000, 0x0205FFFF, csr_field_table_cs },
  { "ispr1.pxr", 0x82060000, 0x820603FF, 0x02060000, 0x020603FF, csr_field_table_pxr },
  { "isprcs1.cs", 0x82070000, 0x8207FFFF, 0x02070000, 0x0207FFFF, csr_field_table_cs },
  { "gfx0.gfx", 0x82080000, 0x820803FF, 0x02080000, 0x020803FF, csr_field_table_gfx },
  { "gfx0.coordr", 0x82080400, 0x820807FF, 0x02080400, 0x020807FF, csr_field_table_coordr },
  { "gfx1.gfx", 0x82090000, 0x820903FF, 0x02090000, 0x020903FF, csr_field_table_gfx },
  { "gfx1.coordr", 0x82090400, 0x820907FF, 0x02090400, 0x020907FF, csr_field_table_coordr },
  { "bld.bld", 0x82100000, 0x821001FF, 0x02100000, 0x021001FF, csr_field_table_bld },
  { "bld.weightr", 0x82100400, 0x821007FF, 0x02100400, 0x021007FF, csr_field_table_weightr },
  { "dpc.dpc", 0x82110000, 0x8211FFFF, 0x02110000, 0x0211FFFF, csr_field_table_dpc },
  { "hdr.hdr", 0x82120000, 0x821201FF, 0x02120000, 0x021201FF, csr_field_table_hdr },
  { "dms.dms", 0x82130000, 0x8213003F, 0x02130000, 0x0213003F, csr_field_table_dms },
  { "fcs.fcs", 0x82140000, 0x821400FF, 0x02140000, 0x021400FF, csr_field_table_fcs },
  { "dbf.dbf", 0x82150000, 0x821500FF, 0x02150000, 0x021500FF, csr_field_table_dbf },
  { "ccm.ccm", 0x82160000, 0x8216FFFF, 0x02160000, 0x0216FFFF, csr_field_table_ccm },
  { "gma.gma", 0x82170000, 0x8217FFFF, 0x02170000, 0x0217FFFF, csr_field_table_gma },
  { "pca.pca", 0x82180000, 0x821800FF, 0x02180000, 0x021800FF, csr_field_table_pca },
  { "cst.cst", 0x82190000, 0x8219FFFF, 0x02190000, 0x0219FFFF, csr_field_table_cst },
  { "sc.sc", 0x821A0000, 0x821A00FF, 0x021A0000, 0x021A00FF, csr_field_table_sc },
  { "enh.enh", 0x821B0000, 0x821B00FF, 0x021B0000, 0x021B00FF, csr_field_table_enh },
  { "shp.shp", 0x821C0000, 0x821C01FF, 0x021C0000, 0x021C01FF, csr_field_table_shp },
  { "dhz.dhz", 0x821D0000, 0x821D007F, 0x021D0000, 0x021D007F, csr_field_table_dhz },
  { "vp.mcvp", 0x82200000, 0x822003FF, 0x02200000, 0x022003FF, csr_field_table_mcvp },
  { "vp.me", 0x82200400, 0x822007FF, 0x02200400, 0x022007FF, csr_field_table_me },
  { "vp.nr", 0x82200800, 0x82200BFF, 0x02200800, 0x02200BFF, csr_field_table_nr },
  { "vp.mer", 0x82200C00, 0x82200FFF, 0x02200C00, 0x02200FFF, csr_field_table_mer },
  { "vp.nrw", 0x82201000, 0x822013FF, 0x02201000, 0x022013FF, csr_field_table_nrw },
  { "vp.mv8w", 0x82201400, 0x822017FF, 0x02201400, 0x022017FF, csr_field_table_mv8w },
  { "vp.mv8r", 0x82201800, 0x82201BFF, 0x02201800, 0x02201BFF, csr_field_table_mv8r },
  { "vp.venc_mvw", 0x82201C00, 0x82201FFF, 0x02201C00, 0x02201FFF, csr_field_table_venc_mvw },
  { "vp.vp", 0x82202000, 0x822023FF, 0x02202000, 0x022023FF, csr_field_table_vp },
  { "vp.vp_checksum", 0x82202400, 0x822027FF, 0x02202400, 0x022027FF, csr_field_table_vp_checksum },
  { "vpb2r.dwb2r", 0x82210000, 0x822103FF, 0x02210000, 0x022103FF, csr_field_table_dwb2r },
  { "vp420to444.yuv420to444vp", 0x82220000, 0x8222FFFF, 0x02220000, 0x0222FFFF, csr_field_table_yuv420to444vp },
  { "vpsc.sc", 0x82230000, 0x822300FF, 0x02230000, 0x022300FF, csr_field_table_sc },
  { "vpw0.pxw", 0x82240000, 0x822403FF, 0x02240000, 0x022403FF, csr_field_table_pxw },
  { "vpw1.pxw", 0x82250000, 0x822503FF, 0x02250000, 0x022503FF, csr_field_table_pxw },
  { "vpcst.vpcst", 0x82260000, 0x8226FFFF, 0x02260000, 0x0226FFFF, csr_field_table_vpcst },
  { "vpcfa.dcfa", 0x82270000, 0x822700FF, 0x02270000, 0x022700FF, csr_field_table_dcfa },
  { "ck_apb2.ck_apb2", 0x82300000, 0x82300FFF, 0x02300000, 0x02300FFF, csr_field_table_ck_apb2 },
  { "is.is", 0x83000000, 0x830003FF, 0x03000000, 0x030003FF, csr_field_table_is },
  { "is.is_cfg", 0x83000400, 0x830007FF, 0x03000400, 0x030007FF, csr_field_table_is_cfg },
  { "is.is_checksum", 0x83000800, 0x83000BFF, 0x03000800, 0x03000BFF, csr_field_table_is_checksum },
  { "isccq.ccq", 0x83010000, 0x830100FF, 0x03010000, 0x030100FF, csr_field_table_ccq },
  { "isccq.osdr", 0x83010400, 0x830107FF, 0x03010400, 0x030107FF, csr_field_table_osdr },
  { "isccq.ccqw", 0x83010800, 0x83010BFF, 0x03010800, 0x03010BFF, csr_field_table_ccqw },
  { "ispg0.pg", 0x83020000, 0x830203FF, 0x03020000, 0x030203FF, csr_field_table_pg },
  { "ispg0.frame_time_gen", 0x83020400, 0x830207FF, 0x03020400, 0x030207FF, csr_field_table_frame_time_gen },
  { "isr0.pxr", 0x83030000, 0x830303FF, 0x03030000, 0x030303FF, csr_field_table_pxr },
  { "edp0.edp", 0x83040000, 0x830401FF, 0x03040000, 0x030401FF, csr_field_table_edp },
  { "ispg1.pg", 0x83050000, 0x830503FF, 0x03050000, 0x030503FF, csr_field_table_pg },
  { "ispg1.frame_time_gen", 0x83050400, 0x830507FF, 0x03050400, 0x030507FF, csr_field_table_frame_time_gen },
  { "isr1.pxr", 0x83060000, 0x830603FF, 0x03060000, 0x030603FF, csr_field_table_pxr },
  { "edp1.edp", 0x83070000, 0x830701FF, 0x03070000, 0x030701FF, csr_field_table_edp },
  { "iswroi0.pxw", 0x83080000, 0x830803FF, 0x03080000, 0x030803FF, csr_field_table_pxw },
  { "iswroi1.pxw", 0x83090000, 0x830903FF, 0x03090000, 0x030903FF, csr_field_table_pxw },
  { "isw0.pxw", 0x830A0000, 0x830A03FF, 0x030A0000, 0x030A03FF, csr_field_table_pxw },
  { "isw1.pxw", 0x830B0000, 0x830B03FF, 0x030B0000, 0x030B03FF, csr_field_table_pxw },
  { "isk0.isk", 0x83100000, 0x831003FF, 0x03100000, 0x031003FF, csr_field_table_isk },
  { "isk0.isk_cfg", 0x83100400, 0x831007FF, 0x03100400, 0x031007FF, csr_field_table_isk_cfg },
  { "isk0.isk_checksum", 0x83100800, 0x83100BFF, 0x03100800, 0x03100BFF, csr_field_table_isk_checksum },
  { "isagma0.gma", 0x83110000, 0x8311FFFF, 0x03110000, 0x0311FFFF, csr_field_table_gma },
  { "fpnr0.fpnr", 0x83120000, 0x8312FFFF, 0x03120000, 0x0312FFFF, csr_field_table_fpnr },
  { "iscrop0.crop", 0x83130000, 0x831301FF, 0x03130000, 0x031301FF, csr_field_table_crop },
  { "bls0.bls", 0x83140000, 0x8314FFFF, 0x03140000, 0x0314FFFF, csr_field_table_bls },
  { "dbc0.dbc", 0x83150000, 0x8315FFFF, 0x03150000, 0x0315FFFF, csr_field_table_dbc },
  { "dcc0.dcc", 0x83160000, 0x8316FFFF, 0x03160000, 0x0316FFFF, csr_field_table_dcc },
  { "lsc0.lsc", 0x83170000, 0x831700FF, 0x03170000, 0x031700FF, csr_field_table_lsc },
  { "bsp0.bsp", 0x83180000, 0x831807FF, 0x03180000, 0x031807FF, csr_field_table_bsp },
  { "dpcr0.qspir", 0x83190000, 0x831903FF, 0x03190000, 0x031903FF, csr_field_table_qspir },
  { "iscvs0.cvs", 0x831A0000, 0x831AFFFF, 0x031A0000, 0x031AFFFF, csr_field_table_cvs },
  { "iscs0.cs", 0x831B0000, 0x831BFFFF, 0x031B0000, 0x031BFFFF, csr_field_table_cs },
  { "fsc0.fsc", 0x831C0000, 0x831C003F, 0x031C0000, 0x031C003F, csr_field_table_fsc },
  { "isk1.isk", 0x83200000, 0x832003FF, 0x03200000, 0x032003FF, csr_field_table_isk },
  { "isk1.isk_cfg", 0x83200400, 0x832007FF, 0x03200400, 0x032007FF, csr_field_table_isk_cfg },
  { "isk1.isk_checksum", 0x83200800, 0x83200BFF, 0x03200800, 0x03200BFF, csr_field_table_isk_checksum },
  { "isagma1.gma", 0x83210000, 0x8321FFFF, 0x03210000, 0x0321FFFF, csr_field_table_gma },
  { "fpnr1.fpnr", 0x83220000, 0x8322FFFF, 0x03220000, 0x0322FFFF, csr_field_table_fpnr },
  { "iscrop1.crop", 0x83230000, 0x832301FF, 0x03230000, 0x032301FF, csr_field_table_crop },
  { "bls1.bls", 0x83240000, 0x8324FFFF, 0x03240000, 0x0324FFFF, csr_field_table_bls },
  { "dbc1.dbc", 0x83250000, 0x8325FFFF, 0x03250000, 0x0325FFFF, csr_field_table_dbc },
  { "dcc1.dcc", 0x83260000, 0x8326FFFF, 0x03260000, 0x0326FFFF, csr_field_table_dcc },
  { "lsc1.lsc", 0x83270000, 0x832700FF, 0x03270000, 0x032700FF, csr_field_table_lsc },
  { "bsp1.bsp", 0x83280000, 0x832807FF, 0x03280000, 0x032807FF, csr_field_table_bsp },
  { "dpcr1.qspir", 0x83290000, 0x832903FF, 0x03290000, 0x032903FF, csr_field_table_qspir },
  { "iscvs1.cvs", 0x832A0000, 0x832AFFFF, 0x032A0000, 0x032AFFFF, csr_field_table_cvs },
  { "iscs1.cs", 0x832B0000, 0x832BFFFF, 0x032B0000, 0x032BFFFF, csr_field_table_cs },
  { "fsc1.fsc", 0x832C0000, 0x832C003F, 0x032C0000, 0x032C003F, csr_field_table_fsc },
  { "disp.disp", 0x83300000, 0x833003FF, 0x03300000, 0x033003FF, csr_field_table_disp },
  { "disp.disp_cfg", 0x83300400, 0x833007FF, 0x03300400, 0x033007FF, csr_field_table_disp_cfg },
  { "disp.disp_checksum", 0x83300800, 0x83300BFF, 0x03300800, 0x03300BFF, csr_field_table_disp_checksum },
  { "dispccq.ccq", 0x83310000, 0x833100FF, 0x03310000, 0x033100FF, csr_field_table_ccq },
  { "dispccq.osdr", 0x83310400, 0x833107FF, 0x03310400, 0x033107FF, csr_field_table_osdr },
  { "dispccq.ccqw", 0x83310800, 0x83310BFF, 0x03310800, 0x03310BFF, csr_field_table_ccqw },
  { "dispr.pxr", 0x83320000, 0x833203FF, 0x03320000, 0x033203FF, csr_field_table_pxr },
  { "disprcs.cs", 0x83330000, 0x8333FFFF, 0x03330000, 0x0333FFFF, csr_field_table_cs },
  { "disppg.pg", 0x83340000, 0x833403FF, 0x03340000, 0x033403FF, csr_field_table_pg },
  { "disppg.frame_time_gen", 0x83340400, 0x833407FF, 0x03340400, 0x033407FF, csr_field_table_frame_time_gen },
  { "d420to422.d420to422", 0x83350000, 0x8335FFFF, 0x03350000, 0x0335FFFF, csr_field_table_d420to422 },
  { "d422to444.d422to444", 0x83360000, 0x8336FFFF, 0x03360000, 0x0336FFFF, csr_field_table_d422to444 },
  { "dosd.dosd", 0x83370000, 0x8337FFFF, 0x03370000, 0x0337FFFF, csr_field_table_dosd },
  { "dosd_read0.dosd_read", 0x83380000, 0x833803FF, 0x03380000, 0x033803FF, csr_field_table_dosd_read },
  { "dosd_read1.dosd_read", 0x83390000, 0x833903FF, 0x03390000, 0x033903FF, csr_field_table_dosd_read },
  { "dcst.dcst", 0x833A0000, 0x833AFFFF, 0x033A0000, 0x033AFFFF, csr_field_table_dcst },
  { "dgma.dgma", 0x833B0000, 0x833BFFFF, 0x033B0000, 0x033BFFFF, csr_field_table_dgma },
  { "dcfa.dcfa", 0x833C0000, 0x833C00FF, 0x033C0000, 0x033C00FF, csr_field_table_dcfa },
  { "dcs.cs", 0x833D0000, 0x833DFFFF, 0x033D0000, 0x033DFFFF, csr_field_table_cs },
  { "mipitxphy.tx_phycfg", 0x83400000, 0x834003FF, 0x03400000, 0x034003FF, csr_field_table_tx_phycfg },
  { "mipitxphy.tx_ctrl", 0x83400400, 0x834007FF, 0x03400400, 0x034007FF, csr_field_table_tx_ctrl },
  { "mipitxphy.fm", 0x83400800, 0x83400BFF, 0x03400800, 0x03400BFF, csr_field_table_fm },
  { "mipitxenc.tx_out", 0x83410000, 0x834103FF, 0x03410000, 0x034103FF, csr_field_table_tx_out },
  { "mipitxenc.tx_enc", 0x83410400, 0x834107FF, 0x03410400, 0x034107FF, csr_field_table_tx_enc },
  { "pdi.pdi", 0x83420000, 0x834201FF, 0x03420000, 0x034201FF, csr_field_table_pdi },
  { "dispif.dispif_syscfg", 0x83430000, 0x834303FF, 0x03430000, 0x034303FF, csr_field_table_dispif_syscfg },
  { "dispif.dispif_ctr", 0x83430400, 0x834307FF, 0x03430400, 0x034307FF, csr_field_table_dispif_ctr },
  { "rxphy0.rx_phycfg", 0x83500000, 0x835003FF, 0x03500000, 0x035003FF, csr_field_table_rx_phycfg },
  { "rxphy0.rx_ctrl", 0x83500400, 0x835007FF, 0x03500400, 0x035007FF, csr_field_table_rx_ctrl },
  { "rxphy0.fm", 0x83500800, 0x83500BFF, 0x03500800, 0x03500BFF, csr_field_table_fm },
  { "rxphy1.rx_phycfg", 0x83510000, 0x835103FF, 0x03510000, 0x035103FF, csr_field_table_rx_phycfg },
  { "rxphy1.rx_ctrl", 0x83510400, 0x835107FF, 0x03510400, 0x035107FF, csr_field_table_rx_ctrl },
  { "rxphy1.fm", 0x83510800, 0x83510BFF, 0x03510800, 0x03510BFF, csr_field_table_fm },
  { "lvds0.rx", 0x83520000, 0x835203FF, 0x03520000, 0x035203FF, csr_field_table_rx },
  { "lvds0.dec", 0x83520400, 0x835207FF, 0x03520400, 0x035207FF, csr_field_table_dec },
  { "lvds1.rx", 0x83530000, 0x835303FF, 0x03530000, 0x035303FF, csr_field_table_rx },
  { "lvds1.dec", 0x83530400, 0x835307FF, 0x03530400, 0x035307FF, csr_field_table_dec },
  { "ps.ps", 0x83540000, 0x835403FF, 0x03540000, 0x035403FF, csr_field_table_ps },
  { "slb0.slb", 0x83550000, 0x83550FFF, 0x03550000, 0x03550FFF, csr_field_table_slb },
  { "slb1.slb", 0x83560000, 0x83560FFF, 0x03560000, 0x03560FFF, csr_field_table_slb },
  { "senif.senif_syscfg", 0x83570000, 0x835703FF, 0x03570000, 0x035703FF, csr_field_table_senif_syscfg },
  { "senif.senif_ctrl", 0x83570400, 0x835707FF, 0x03570400, 0x035707FF, csr_field_table_senif_ctrl },
  { "dma.dma_8_c1_d64", 0x83600000, 0x836003FF, 0x03600000, 0x036003FF, csr_field_table_dma_8_c1_d64 },
  { "dma.dma_8_c1_d64_syscfg", 0x83600400, 0x836007FF, 0x03600400, 0x036007FF, csr_field_table_dma_8_c1_d64_syscfg },
  { "dma.aes_enc", 0x83600800, 0x83600BFF, 0x03600800, 0x03600BFF, csr_field_table_aes_enc },
  { "dma.aes_dec", 0x83600C00, 0x83600FFF, 0x03600C00, 0x03600FFF, csr_field_table_aes_dec },
  { "dma.adocodec", 0x83601000, 0x836013FF, 0x03601000, 0x036013FF, csr_field_table_adocodec },
  { "qspi.qspi", 0x83610000, 0x8361FFFF, 0x03610000, 0x0361FFFF, csr_field_table_qspi },
  { "qspir.qspir", 0x83620000, 0x836203FF, 0x03620000, 0x036203FF, csr_field_table_qspir },
  { "qspiw.qspiw", 0x83630000, 0x836303FF, 0x03630000, 0x036303FF, csr_field_table_qspiw },
  { "ck_apb3.ck_apb3", 0x83700000, 0x83700FFF, 0x03700000, 0x03700FFF, csr_field_table_ck_apb3 },
  { "gic.gic", 0x84000000, 0x840000FF, 0x04000000, 0x040000FF, csr_field_table_gic },
  { "usbotg.usbotg", 0xA0000000, 0xA00000FF, 0x20000000, 0x200000FF, csr_field_table_usbotg },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_H_
