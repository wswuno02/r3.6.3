#ifndef CSR_TABLE_ADOIN_SYSCFG_H_
#define CSR_TABLE_ADOIN_SYSCFG_H_

#include <stdint.h>

CsrFieldEntry csr_field_table_adoin_syscfg[] =
{
  // WORD conf_adc0
  { "cken_adc", 0x00000000,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_adc", 0x00000000,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF_ADC0", 0x00000000, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf_adc1
  { "sw_rst_adc", 0x00000004,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_adc", 0x00000004,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF_ADC1", 0x00000004, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf_enc0
  { "cken_enc", 0x00000008,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_enc", 0x00000008,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF_ENC0", 0x00000008, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf_enc1
  { "sw_rst_enc", 0x0000000C,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_enc", 0x0000000C,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF_ENC1", 0x0000000C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf_fade0
  { "cken_fade", 0x00000010,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_fade", 0x00000010,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF_FADE0", 0x00000010, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf_fade1
  { "sw_rst_fade", 0x00000014,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_fade", 0x00000014,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF_FADE1", 0x00000014, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf_i2s0
  { "cken_i2s", 0x00000018,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_i2s", 0x00000018,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF_I2S0", 0x00000018, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf_i2s1
  { "sw_rst_i2s", 0x0000001C,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_i2s", 0x0000001C,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF_I2S1", 0x0000001C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf_write0
  { "cken_write", 0x00000020,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_write", 0x00000020,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF_WRITE0", 0x00000020, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf_write1
  { "sw_rst_write", 0x00000024,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_write", 0x00000024,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF_WRITE1", 0x00000024, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf_agc0
  { "cken_agc", 0x00000028,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_agc", 0x00000028,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF_AGC0", 0x00000028, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf_agc1
  { "sw_rst_agc", 0x0000002C,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_agc", 0x0000002C,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF_AGC1", 0x0000002C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf_ares0
  { "cken_ares", 0x00000030,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_ares", 0x00000030,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF_ARES0", 0x00000030, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf_ares1
  { "sw_rst_ares", 0x00000034,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_ares", 0x00000034,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF_ARES1", 0x00000034, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf_anr0
  { "cken_anr", 0x00000038,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_anr", 0x00000038,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF_ANR0", 0x00000038, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf_anr1
  { "sw_rst_anr", 0x0000003C,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_anr", 0x0000003C,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF_ANR1", 0x0000003C, 31, 0,  CSR_W1P, 0x00000000 },
  // WORD conf_hpf0
  { "cken_hpf", 0x00000040,  0,  0,   CSR_RW, 0x00000001 },
  { "lv_rst_hpf", 0x00000040,  8,  8,   CSR_RW, 0x00000000 },
  { "CONF_HPF0", 0x00000040, 31, 0,   CSR_RW, 0x00000000 },
  // WORD conf_hpf1
  { "sw_rst_hpf", 0x00000044,  0,  0,  CSR_W1P, 0x00000000 },
  { "csr_rst_hpf", 0x00000044,  8,  8,  CSR_W1P, 0x00000000 },
  { "CONF_HPF1", 0x00000044, 31, 0,  CSR_W1P, 0x00000000 },
  // end of table
  { 0, 0, 0, 0, 0, 0 }
};

#endif // CSR_TABLE_ADOIN_SYSCFG_H_
