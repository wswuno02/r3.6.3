#ifndef __CHECKSUM_H_
#define __CHECKSUM_H_
#include <stdint.h>

uint32_t get_checksum(uint32_t *data, uint32_t count);

#endif /* __CHECKSUM_H_ */
