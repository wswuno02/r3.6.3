#ifndef LVDS_H_
#define LVDS_H_

#include "csr_bank_dec.h"
#include "csr_bank_rx.h"

typedef enum lvds_dec_spec {
	LVDS_DEC_SPEC_HISPI = 0,
	LVDS_DEC_SPEC_MIPI_CSI_2 = 4,
	LVDS_DEC_SPEC_USER_DEFINED = 7,
} LvdsDecSpec;

typedef enum lvds_dec_mode {
	LVDS_DEC_MODE_HISPI_PACKETIZED_SP = 0,
	LVDS_DEC_MODE_HISPI_STREAMING_SP,
	LVDS_DEC_MODE_HISPI_STREAMING_S,
	LVDS_DEC_MODE_SONY_DDR_LVDS = 0,
} LvdsDecMode;

typedef enum lvds_dec_lane {
	LVDS_DEC_LANE_1 = 0,
	LVDS_DEC_LANE_2,
	LVDS_DEC_LANE_3,
	LVDS_DEC_LANE_4,
} LvdsDecLane;

typedef enum lvds_dec_word_size {
	LVDS_DEC_WORD_SIZE_6_BIT = 0,
	LVDS_DEC_WORD_SIZE_7_BIT,
	LVDS_DEC_WORD_SIZE_8_BIT,
	LVDS_DEC_WORD_SIZE_10_BIT,
	LVDS_DEC_WORD_SIZE_12_BIT,
	LVDS_DEC_WORD_SIZE_14_BIT,
	LVDS_DEC_WORD_SIZE_16_BIT,
	LVDS_DEC_WORD_SIZE_RESERVED,
} LvdsDecWordSize;

typedef enum lvds_dec_dcmp_mode {
	LVDS_DEC_DCMP_MODE_10_8_10 = 0,
	LVDS_DEC_DCMP_MODE_12_8_12,
} LvdsDecDcmpMode;

typedef struct lvds_rx_bist_status {
	uint8_t bist_done;
	uint8_t bist_fail;
	uint8_t offset;
	uint16_t bist_sta;
	uint16_t bist_wc;
	uint16_t bist_crc;
	uint16_t calc_wc;
	uint16_t calc_crc;
} LvdsRxBistStatus;

#define lvds_dec_enable_dec(base, en)                                                                                  \
	{                                                                                                              \
		(base)->dec_en = (uint8_t)(en);                                                                        \
	}
#define lvds_dec_dec_reset(base)                                                                                       \
	{                                                                                                              \
		(base)->dec_reset = 1;                                                                                 \
	}
#define lvds_dec_frame_reset(base)                                                                                     \
	{                                                                                                              \
		(base)->frame_reset = 1;                                                                               \
	}
#define lvds_dec_set_cfg(base, spec, mode, ln)                                                                         \
	{                                                                                                              \
		(base)->spec_sel = (LvdsDecSpec)(spec);                                                                \
		(base)->mode_sel = (LvdsDecMode)(mode);                                                                \
		(base)->lane_sel = (LvdsDecLane)(ln);                                                                  \
	}
#define lvds_dec_set_word_size(base, size)                                                                             \
	{                                                                                                              \
		(base)->word_size = (LvdsDecWordSize)(size);                                                           \
	}
#define lvds_dec_set_msb_first(base, msb_1st)                                                                          \
	{                                                                                                              \
		(base)->msb_first = (uint8_t)(msb_1st);                                                                \
	}
#define lvds_dec_set_word_num(base, num)                                                                               \
	{                                                                                                              \
		(base)->word_num = (uint16_t)(num);                                                                    \
	}
#define lvds_dec_set_line_num(base, num)                                                                               \
	{                                                                                                              \
		(base)->line_num = (uint16_t)(num);                                                                    \
	}

/* HiSpi */
#define lvds_dec_enable_word_flr(base, en)                                                                             \
	{                                                                                                              \
		(base)->enable_word_flr = (uint8_t)(en);                                                               \
	}
#define lvds_dec_enable_word_crc(base, en)                                                                             \
	{                                                                                                              \
		(base)->enable_word_crc = (uint8_t)(en);                                                               \
	}
#define lvds_dec_set_ail_msb_first(base, msb_first)                                                                    \
	{                                                                                                              \
		(base)->ail_msb_first = (uint8_t)(mst_first);                                                          \
	}
#define lvds_dec_set_flr_word_num(base, word_num)                                                                      \
	{                                                                                                              \
		(base)->flr_word_num = (uint8_t)(word_num);                                                            \
	}

#define lvds_dec_set_vbp_line_num(base, line_num)                                                                      \
	{                                                                                                              \
		(base)->vbp_line_num = (uint16_t)(line_num);                                                           \
	}
#define lvds_dec_set_vfp_line_num(base, line_num)                                                                      \
	{                                                                                                              \
		(base)->vfp_line_num = (uint16_t)(line_num);                                                           \
	}
#define lvds_dec_set_hbp_word_num(base, word_num)                                                                      \
	{                                                                                                              \
		(base)->hbp_word_num = (uint16_t)(word_num);                                                           \
	}
#define lvds_dec_set_hfp_word_num(base, word_num)                                                                      \
	{                                                                                                              \
		(base)->hfp_word_num = (uint16_t)(word_num);                                                           \
	}

#define lvds_dec_dis_ecc_crt(base, dis)                                                                                \
	{                                                                                                              \
		(base)->dis_ecc_crt = (uint8_t)(dis);                                                                  \
	}
#define lvds_dec_dis_crc_check(base, dis)                                                                              \
	{                                                                                                              \
		(base)->dis_crc_check = (uint8_t)(dis);                                                                \
	}
#define lvds_dec_enable_short_pkt(base, en_bitmap)                                                                     \
	{                                                                                                              \
		(base)->short_pkt_en = (uint8_t)(en_bitmap);                                                           \
	} /* ID 0x08 + N, N = 0~7 */
#define lvds_dec_enable_user_pkt(base, en_bitmap)                                                                      \
	{                                                                                                              \
		(base)->user_pkt_en = (uint8_t)(en_bitmap);                                                            \
	} /* ID 0x30 + N, N = 0~7 */

#define lvds_dec_set_out_vc_vld(base, port, ch_bitmap)                                                                 \
	{                                                                                                              \
		if (port)                                                                                              \
			(base)->out1_vc_vld = (uint16_t)(ch_bitmap);                                                   \
		else                                                                                                   \
			(base)->out0_vc_vld = (uint16_t)(ch_bitmap);                                                   \
	}
#define lvds_dec_enable_ec(base, en)                                                                                   \
	{                                                                                                              \
		(base)->ec_en = (uint8_t)(en);                                                                         \
	}
#define lvds_dec_set_output_res(base, w, h)                                                                            \
	{                                                                                                              \
		(base)->width = (uint16_t)(w);                                                                         \
		(base)->height = (uint16_t)(h);                                                                        \
	}
#define lvds_dec_sel_frame_start_src(base, src)                                                                        \
	{                                                                                                              \
		(base)->frame_start_sel = (src);                                                                       \
	}
#define lvds_dec_sel_frame_done_src(base, src)                                                                         \
	{                                                                                                              \
		(base)->frame_done_sel = (src);                                                                        \
	}
#define lvds_dec_sel_frame_end_src(base, src)                                                                          \
	{                                                                                                              \
		(base)->frame_end_sel = (src);                                                                         \
	}

/* MIPI */
#define lvds_dec_enable_dcmp(base, en)                                                                                 \
	{                                                                                                              \
		(base)->dcmp_en = (uint8_t)(en);                                                                       \
	}
#define lvds_dec_set_dcmp_mode(base, mode)                                                                             \
	{                                                                                                              \
		(base)->dcmp_mode = (LvdsDecDcmpMode)(mode);                                                           \
	}

/* Sony */
#define lvds_dec_enable_dol_sydinfo(base, en)                                                                          \
	{                                                                                                              \
		(base)->dol_sydinfo_en = (uint8_t)(en);                                                                \
	}

#define lvds_dec_get_frame_count(base, port)                                                                           \
	(((port) == 0) ? (uint16_t)(base)->frame_count_0 : (uint16_t)(base)->frame_count_1)

/* MIPI */
#define lvds_dec_get_gene_short_pkt(base) ((uint32_t)(base)->gene_short_pkt)
/* Skip lots of custom part */

#define lvds_dec_get_sync_bytes(base) ((uint32_t)((base)->sync_bytes))
#define lvds_dec_get_mipi_hs_ln_sta(base, ln) ((uint8_t)(((base)->lvds_dec54 >> (8 * (ln))) & 0xFF))
#define lvds_dec_get_mipi_active_count(base, port)                                                                     \
	(((port) == 0) ? (uint16_t)(base)->mipi_active_count : (uint16_t)(base)->mipi_active_count_1)
#define lvds_dec_get_mipi_blank_count(base, port)                                                                      \
	(((port) == 0) ? (uint16_t)(base)->mipi_blank_count : (uint16_t)(base)->mipi_blank_count_1)
#define lvds_dec_get_mipi_word_count(base) ((uint16_t)(base)->mipi_word_count)
#define lvds_dec_get_mipi_frame_number(base, port)                                                                     \
	(((port) == 0) ? (uint16_t)(base)->mipi_frame_number : (uint16_t)(base)->mipi_frame_number_1)
#define lvds_dec_get_mipi_crc_word(base) ((uint16_t)(base)->mipi_crc_word)
#define lvds_dec_get_mipi_crc_calc(base) ((uint16_t)(base)->mipi_crc_calc)
#define lvds_dec_get_pkt_hdr_rec_0(base) ((uint32_t)(base)->pkt_hdr_rec_0)
#define lvds_dec_get_pkt_hdr_rec_1(base) ((uint32_t)(base)->pkt_hdr_rec_1)
#define lvds_dec_get_pkt_hdr_rec_2(base) ((uint32_t)(base)->pkt_hdr_rec_2)
#define lvds_dec_get_pkt_hdr_rec_3(base) ((uint32_t)(base)->pkt_hdr_rec_3)

/* HiSpi */
#define lvds_dec_get_hispi_status(base) ((uint8_t)(base)->hispi_status)
#define lvds_dec_get_hispi_active_count(base) ((uint16_t)(base)->hispi_active_count)
#define lvds_dec_get_hispi_blank_count(base) ((uint16_t)(base)->hispi_blank_count)
#define lvds_dec_get_hispi_word_count(base) ((uint16_t)(base)->hispi_word_count)

/* Sony */
#define lvds_dec_get_sony_status(base) ((uint16_t)(base)->sony_status)
#define lvds_dec_get_sony_active_count(base) ((uint16_t)(base)->sony_active_count)
#define lvds_dec_get_sony_blank_count(base) ((uint16_t)(base)->sony_blank_count)
#define lvds_dec_get_sony_word_count(base) ((uint16_t)(base)->sony_word_count)

/* LVDS RX */

#define lvds_rx_enable_rx(base, en)                                                                                    \
	{                                                                                                              \
		(base)->rx_en = (uint8_t)(en)&0x1;                                                                     \
	}
#define lvds_rx_enable_lane(base, en_bitmap)                                                                           \
	{                                                                                                              \
		(base)->lane_en = (uint8_t)(en_bitmap)&0x1F;                                                           \
	}
#define lvds_rx_set_spec_mipi(base, is_mipi)                                                                           \
	{                                                                                                              \
		(base)->spec_mipi = (uint8_t)(is_mipi)&0x1;                                                            \
	}
#define lvds_rx_enable_lane_ck(base, en_bitmap)                                                                        \
	{                                                                                                              \
		(base)->lane_ck_en = (uint8_t)(en_bitmap)&0x1F;                                                        \
	}
#define lvds_rx_sel_ln_rx(base, rx_ln, phy_ln)                                                                         \
	{                                                                                                              \
		switch (rx_ln) {                                                                                       \
		case 0:                                                                                                \
			(base)->ln0_rx_sel = (uint8_t)(phy_ln)&0x7;                                                    \
			break;                                                                                         \
		case 1:                                                                                                \
			(base)->ln1_rx_sel = (uint8_t)(phy_ln)&0x7;                                                    \
			break;                                                                                         \
		case 2:                                                                                                \
			(base)->ln2_rx_sel = (uint8_t)(phy_ln)&0x7;                                                    \
			break;                                                                                         \
		case 3:                                                                                                \
			(base)->ln3_rx_sel = (uint8_t)(phy_ln)&0x7;                                                    \
			break;                                                                                         \
		default:                                                                                               \
			break;                                                                                         \
		}                                                                                                      \
	}
#define lvds_rx_set_t_da_settle(base, time)                                                                            \
	{                                                                                                              \
		(base)->t_da_settle = (uint8_t)(time);                                                                 \
	}
#define lvds_rx_set_t_da_term_en(base, time)                                                                           \
	{                                                                                                              \
		(base)->t_da_term_en = (uint8_t)(time);                                                                \
	}
#define lvds_rx_set_t_da_rst_src(base, time)                                                                           \
	{                                                                                                              \
		(base)->t_da_rst_src = (uint8_t)(time);                                                                \
	}
#define lvds_rx_set_t_da_rst_settle(base, time)                                                                        \
	{                                                                                                              \
		(base)->t_da_rst_settle = (uint8_t)(time);                                                             \
	}
#define lvds_rx_set_t_ck_settle(base, time)                                                                            \
	{                                                                                                              \
		(base)->t_ck_settle = (uint8_t)(time);                                                                 \
	}
#define lvds_rx_set_t_ck_term_en(base, time)                                                                           \
	{                                                                                                              \
		(base)->t_ck_term_en = (uint8_t)(time);                                                                \
	}
#define lvds_rx_set_t_ck_rst_src(base, time)                                                                           \
	{                                                                                                              \
		(base)->t_ck_rst_src = (uint8_t)(time);                                                                \
	}
#define lvds_rx_set_t_ck_rst_settle(base, time)                                                                        \
	{                                                                                                              \
		(base)->t_ck_rst_settle = (uint8_t)(time);                                                             \
	}
#define lvds_rx_set_ln_hsrx_en_ctrl(base, phy_ln, en)                                                                  \
	{                                                                                                              \
		switch (phy_ln) {                                                                                      \
		case 0:                                                                                                \
			(base)->phy_ln0_hsrx_en_ctrl = (uint8_t)(en)&0x1;                                              \
			break;                                                                                         \
		case 1:                                                                                                \
			(base)->phy_ln1_hsrx_en_ctrl = (uint8_t)(en)&0x1;                                              \
			break;                                                                                         \
		case 2:                                                                                                \
			(base)->phy_ln2_hsrx_en_ctrl = (uint8_t)(en)&0x1;                                              \
			break;                                                                                         \
		case 3:                                                                                                \
			(base)->phy_ln3_hsrx_en_ctrl = (uint8_t)(en)&0x1;                                              \
			break;                                                                                         \
		case 4:                                                                                                \
			(base)->phy_lnc_hsrx_en_ctrl = (uint8_t)(en)&0x1;                                              \
			break;                                                                                         \
		default:                                                                                               \
			break;                                                                                         \
		}                                                                                                      \
	}
#define lvds_rx_set_ln_term_en_ctrl(base, phy_ln, en)                                                                  \
	{                                                                                                              \
		switch (phy_ln) {                                                                                      \
		case 0:                                                                                                \
			(base)->phy_ln0_term_en_ctrl = (uint8_t)(en)&0x1;                                              \
			break;                                                                                         \
		case 1:                                                                                                \
			(base)->phy_ln1_term_en_ctrl = (uint8_t)(en)&0x1;                                              \
			break;                                                                                         \
		case 2:                                                                                                \
			(base)->phy_ln2_term_en_ctrl = (uint8_t)(en)&0x1;                                              \
			break;                                                                                         \
		case 3:                                                                                                \
			(base)->phy_ln3_term_en_ctrl = (uint8_t)(en)&0x1;                                              \
			break;                                                                                         \
		case 4:                                                                                                \
			(base)->phy_lnc_term_en_ctrl = (uint8_t)(en)&0x1;                                              \
			break;                                                                                         \
		default:                                                                                               \
			break;                                                                                         \
		}                                                                                                      \
	}
#define lvds_rx_set_ln_hssw_en_ctrl(base, phy_ln, en)                                                                  \
	{                                                                                                              \
		switch (phy_ln) {                                                                                      \
		case 0:                                                                                                \
			(base)->phy_ln0_hssw_en_ctrl = (uint8_t)(en)&0x1;                                              \
			break;                                                                                         \
		case 1:                                                                                                \
			(base)->phy_ln1_hssw_en_ctrl = (uint8_t)(en)&0x1;                                              \
			break;                                                                                         \
		case 2:                                                                                                \
			(base)->phy_ln2_hssw_en_ctrl = (uint8_t)(en)&0x1;                                              \
			break;                                                                                         \
		case 3:                                                                                                \
			(base)->phy_ln3_hssw_en_ctrl = (uint8_t)(en)&0x1;                                              \
			break;                                                                                         \
		case 4:                                                                                                \
			(base)->phy_lnc_hssw_en_ctrl = (uint8_t)(en)&0x1;                                              \
			break;                                                                                         \
		default:                                                                                               \
			break;                                                                                         \
		}                                                                                                      \
	}
#define lvds_rx_sel_ln_hsrx_en_src(base, phy_ln, src)                                                                  \
	{                                                                                                              \
		switch (phy_ln) {                                                                                      \
		case 0:                                                                                                \
			(base)->phy_ln0_hsrx_en_sel = (uint8_t)(src)&0x1;                                              \
			break;                                                                                         \
		case 1:                                                                                                \
			(base)->phy_ln1_hsrx_en_sel = (uint8_t)(src)&0x1;                                              \
			break;                                                                                         \
		case 2:                                                                                                \
			(base)->phy_ln2_hsrx_en_sel = (uint8_t)(src)&0x1;                                              \
			break;                                                                                         \
		case 3:                                                                                                \
			(base)->phy_ln3_hsrx_en_sel = (uint8_t)(src)&0x1;                                              \
			break;                                                                                         \
		case 4:                                                                                                \
			(base)->phy_lnc_hsrx_en_sel = (uint8_t)(src)&0x1;                                              \
			break;                                                                                         \
		default:                                                                                               \
			break;                                                                                         \
		}                                                                                                      \
	}
#define lvds_rx_sel_ln_term_en_src(base, phy_ln, src)                                                                  \
	{                                                                                                              \
		switch (phy_ln) {                                                                                      \
		case 0:                                                                                                \
			(base)->phy_ln0_hsrx_en_sel = (uint8_t)(src)&0x1;                                              \
			break;                                                                                         \
		case 1:                                                                                                \
			(base)->phy_ln1_hsrx_en_sel = (uint8_t)(src)&0x1;                                              \
			break;                                                                                         \
		case 2:                                                                                                \
			(base)->phy_ln2_hsrx_en_sel = (uint8_t)(src)&0x1;                                              \
			break;                                                                                         \
		case 3:                                                                                                \
			(base)->phy_ln3_hsrx_en_sel = (uint8_t)(src)&0x1;                                              \
			break;                                                                                         \
		case 4:                                                                                                \
			(base)->phy_lnc_hsrx_en_sel = (uint8_t)(src)&0x1;                                              \
			break;                                                                                         \
		default:                                                                                               \
			break;                                                                                         \
		}                                                                                                      \
	}
#define lvds_rx_sel_ln_hssw_en_src(base, phy_ln, src)                                                                  \
	{                                                                                                              \
		switch (phy_ln) {                                                                                      \
		case 0:                                                                                                \
			(base)->phy_ln0_hsrx_en_sel = (uint8_t)(src)&0x1;                                              \
			break;                                                                                         \
		case 1:                                                                                                \
			(base)->phy_ln1_hsrx_en_sel = (uint8_t)(src)&0x1;                                              \
			break;                                                                                         \
		case 2:                                                                                                \
			(base)->phy_ln2_hsrx_en_sel = (uint8_t)(src)&0x1;                                              \
			break;                                                                                         \
		case 3:                                                                                                \
			(base)->phy_ln3_hsrx_en_sel = (uint8_t)(src)&0x1;                                              \
			break;                                                                                         \
		case 4:                                                                                                \
			(base)->phy_lnc_hsrx_en_sel = (uint8_t)(src)&0x1;                                              \
			break;                                                                                         \
		default:                                                                                               \
			break;                                                                                         \
		}                                                                                                      \
	}
#define lvds_rx_set_phy_clk_rstb_ctrl(base, en)                                                                        \
	{                                                                                                              \
		(base)->phy_clk_rstb_ctrl = (uint8_t)(en)&0x1;                                                         \
	}
#define lvds_rx_set_phy_data_rstb_ctrl(base, en)                                                                       \
	{                                                                                                              \
		(base)->phy_data_rstb_ctrl = (uint8_t)(en)&0x1;                                                        \
	}
#define lvds_rx_sel_phy_clk_rstb_src(base, src)                                                                        \
	{                                                                                                              \
		(base)->phy_clk_rstb_sel = (uint8_t)(src)&0x1;                                                         \
	}
#define lvds_rx_sel_phy_data_rstb_src(base, src)                                                                       \
	{                                                                                                              \
		(base)->phy_data_rstb_sel = (uint8_t)(src)&0x1;                                                        \
	}
#define lvds_rx_enable_phy_ln_pn_swap(base, phy_ln, en)                                                                \
	{                                                                                                              \
		switch (phy_ln) {                                                                                      \
		case 0:                                                                                                \
			(base)->phy_ln0_pn_sel = (uint8_t)(en)&0x1;                                                    \
			break;                                                                                         \
		case 1:                                                                                                \
			(base)->phy_ln1_pn_sel = (uint8_t)(en)&0x1;                                                    \
			break;                                                                                         \
		case 2:                                                                                                \
			(base)->phy_ln2_pn_sel = (uint8_t)(en)&0x1;                                                    \
			break;                                                                                         \
		case 3:                                                                                                \
			(base)->phy_ln3_pn_sel = (uint8_t)(en)&0x1;                                                    \
			break;                                                                                         \
		case 4:                                                                                                \
			(base)->phy_ln4_pn_sel = (uint8_t)(en)&0x1;                                                    \
			break;                                                                                         \
		default:                                                                                               \
			break;                                                                                         \
		}                                                                                                      \
	}
#define lvds_rx_set_bist_mode(base, bist_mode)                                                                         \
	{                                                                                                              \
		(base)->bist_mode = (uint8_t)(bist_mode)&0x1;                                                          \
	}
#define lvds_rx_enable_bist_crc(base, en)                                                                              \
	{                                                                                                              \
		(base)->bist_crc_en = (uint8_t)(en)&0x1;                                                               \
	}
#define lvds_rx_set_bist_pac_di(base, id)                                                                              \
	{                                                                                                              \
		(base)->bist_pac_di = (uint8_t)(id);                                                                   \
	}
#define lvds_rx_enable_bist_lane(base, ln_bitmap)                                                                      \
	{                                                                                                              \
		(base)->bist_lane_en = (uint8_t)(ln_bitmap)&0xF;                                                       \
	}
#define lvds_rx_set_bist_pac_wc(base, word_cnt)                                                                        \
	{                                                                                                              \
		(base)->bist_pac_ws = (uint16_t)(word_cnt);                                                            \
	}
#define lvds_rx_set_bist_pac_crc(base, crc)                                                                            \
	{                                                                                                              \
		(base)->bist_pac_crc = (uint16_t)(crc);                                                                \
	}
#define lvds_rx_set_bist_pac_err(base, ecc)                                                                            \
	{                                                                                                              \
		(base)->bist_pac_ecc = (uint8_t)(ecc);                                                                 \
	}
#define lvds_rx_set_bist_pac_src(base, src)                                                                            \
	{                                                                                                              \
		(base)->bist_pac_src = (uint8_t)(src)&0x1;                                                             \
	}
#define _lvds_rx_get_bist_status(base, lane, status)                                                                   \
	{                                                                                                              \
		(status)->bist_done = (uint8_t)((base)->ln##lane##_bist_done);                                         \
		(status)->bist_fail = (uint8_t)((base)->ln##lane##_bist_fail);                                         \
		(status)->offset = (uint8_t)((base)->ln##lane##_offset);                                               \
		(status)->bist_sta = (uint16_t)((base)->ln##lane##_bist_sta);                                          \
		(status)->bist_wc = (uint16_t)((base)->ln##lane##_bist_wc);                                            \
		(status)->bist_crc = (uint16_t)((base)->ln##lane##_bist_crc);                                          \
		(status)->calc_wc = (uint16_t)((base)->ln##lane##_calc_ws);                                            \
		(status)->calc_crc = (uint16_t)((base)->ln##lane##_calc_crc);                                          \
	}
#define lvds_rx_get_bist_status(base, ln, status)                                                                      \
	{                                                                                                              \
		switch (ln) {                                                                                          \
		case 0:                                                                                                \
			_lvds_rx_get_bist_status(base, 0, status);                                                     \
			break;                                                                                         \
		case 1:                                                                                                \
			_lvds_rx_get_bist_status(base, 1, status);                                                     \
			break;                                                                                         \
		case 2:                                                                                                \
			_lvds_rx_get_bist_status(base, 2, status);                                                     \
			break;                                                                                         \
		case 3:                                                                                                \
			_lvds_rx_get_bist_status(base, 3, status);                                                     \
			break;                                                                                         \
		default:                                                                                               \
			break;                                                                                         \
		}                                                                                                      \
	}
#define lvds_rx_get_bist_done(base) ((uint8_t)((base)->bist_done))
#define lvds_rx_get_bist_fail(base) ((uint8_t)((base)->bist_fail))
#define lvds_rx_get_ln_lprx_sta(base, ln, sta)                                                                         \
	{                                                                                                              \
		switch (ln) {                                                                                          \
		case 0:                                                                                                \
			*(uint16_t *)(sta) = (uint16_t)((base)->ln0_lprx_sta);                                         \
			break;                                                                                         \
		case 1:                                                                                                \
			*(uint16_t *)(sta) = (uint16_t)((base)->ln1_lprx_sta);                                         \
			break;                                                                                         \
		case 2:                                                                                                \
			*(uint16_t *)(sta) = (uint16_t)((base)->ln2_lprx_sta);                                         \
			break;                                                                                         \
		case 3:                                                                                                \
			*(uint16_t *)(sta) = (uint16_t)((base)->ln3_lprx_sta);                                         \
			break;                                                                                         \
		case 4:                                                                                                \
			*(uint16_t *)(sta) = (uint16_t)((base)->lnc_lprx_sta);                                         \
			break;                                                                                         \
		default:                                                                                               \
			break;                                                                                         \
		}                                                                                                      \
	}
#define lvds_rx_get_ln_esc_cmd_sta(base, ln, sta)                                                                      \
	{                                                                                                              \
		switch (ln) {                                                                                          \
		case 0:                                                                                                \
			*(uint16_t *)(sta) = (uint16_t)((base)->ln0_esc_cmd_sta);                                      \
			break;                                                                                         \
		case 1:                                                                                                \
			*(uint16_t *)(sta) = (uint16_t)((base)->ln1_esc_cmd_sta);                                      \
			break;                                                                                         \
		case 2:                                                                                                \
			*(uint16_t *)(sta) = (uint16_t)((base)->ln2_esc_cmd_sta);                                      \
			break;                                                                                         \
		case 3:                                                                                                \
			*(uint16_t *)(sta) = (uint16_t)((base)->ln3_esc_cmd_sta);                                      \
			break;                                                                                         \
		case 4:                                                                                                \
			*(uint16_t *)(sta) = (uint16_t)((base)->lnc_esc_cmd_sta);                                      \
			break;                                                                                         \
		default:                                                                                               \
			break;                                                                                         \
		}                                                                                                      \
	}
#define lvds_rx_get_lprx_mon(base) ((uint32_t)(base)->lprx_mon)
#define lvds_rx_get_hsrx_sta(base) ((uint8_t)(base)->hsrx_sta)
#define lvds_rx_get_hsrx_fifo_full_err(base) ((uint8_t)(base)->fifo_full_err)
#define lvds_rx_get_irq_sta(base) ((uint32_t)(base)->irq_sta)
#define lvds_rx_set_irq_msk(base, mask_bitmap)                                                                         \
	{                                                                                                              \
		(base)->irq_mask = (uint32_t)(mask_bitmap);                                                            \
	}
#define lvds_rx_clear_irq(base, clear_bitmap)                                                                          \
	{                                                                                                              \
		(base)->irq_ack = (uint32_t)(clear_bitmap);                                                            \
	}

#endif /* LVDS_H_ */
