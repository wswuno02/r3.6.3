#ifndef SENIF_H_
#define SENIF_H_

#include "csr_bank_senif_syscfg.h"
#include "csr_bank_senif_ctrl.h"

/* cken */
#define senif_syscfg_cken_rx(base, id, en)                                                                             \
	{                                                                                                              \
		if (id)                                                                                                \
			(base)->cken_rx1 = (en);                                                                       \
		else                                                                                                   \
			(base)->cken_rx0 = (en);                                                                       \
	}
#define senif_syscfg_cken_dec(base, id, en)                                                                            \
	{                                                                                                              \
		if (id)                                                                                                \
			(base)->cken_dec1 = (en);                                                                      \
		else                                                                                                   \
			(base)->cken_dec0 = (en);                                                                      \
	}
#define senif_syscfg_cken_ps(base, en)                                                                                 \
	{                                                                                                              \
		(base)->cken_ps = (en);                                                                                \
	}
#define senif_syscfg_cken_slb(base, id, en)                                                                            \
	{                                                                                                              \
		if (id)                                                                                                \
			(base)->cken_slb1 = (en);                                                                      \
		else                                                                                                   \
			(base)->cken_slb0 = (en);                                                                      \
	}
#define senif_syscfg_cken_ctrl(base, en)                                                                               \
	{                                                                                                              \
		(base)->cken_ctrl = (en);                                                                              \
	}

/* sw_rst */
#define senif_syscfg_sw_rst_rx_senif_clk(base, id)                                                                     \
	{                                                                                                              \
		if (id)                                                                                                \
			(base)->sw_rst_rx1_senif_clk = 1;                                                              \
		else                                                                                                   \
			(base)->sw_rst_rx0_senif_clk = 1;                                                              \
	}
#define senif_syscfg_sw_rst_dec_senif_clk(base, id)                                                                    \
	{                                                                                                              \
		if (id)                                                                                                \
			(base)->sw_rst_dec1_senif_clk = 1;                                                             \
		else                                                                                                   \
			(base)->sw_rst_dec0_senif_clk = 1;                                                             \
	}
#define senif_syscfg_sw_rst_dec_out_clk(base, id)                                                                      \
	{                                                                                                              \
		if (id)                                                                                                \
			(base)->sw_rst_dec1_out_clk = 1;                                                               \
		else                                                                                                   \
			(base)->sw_rst_dec0_out_clk = 1;                                                               \
	}
#define senif_syscfg_sw_rst_ps_senif_clk(base)                                                                         \
	{                                                                                                              \
		(base)->sw_rst_ps_senif_clk = 1;                                                                       \
	}
#define senif_syscfg_sw_rst_ps_out_clk(base)                                                                           \
	{                                                                                                              \
		(base)->sw_rst_ps_out_clk = 1;                                                                         \
	}
#define senif_syscfg_sw_rst_slb_out_clk(base, id)                                                                      \
	{                                                                                                              \
		if (id)                                                                                                \
			(base)->sw_rst_slb1_out_clk = 1;                                                               \
		else                                                                                                   \
			(base)->sw_rst_slb0_out_clk = 1;                                                               \
	}
#define senif_syscfg_sw_rst_ctrl_sensor_clk(base)                                                                      \
	{                                                                                                              \
		(base)->sw_rst_ctrl_sensor_clk = 1;                                                                    \
	}

/* csr_rst */
#define senif_syscfg_csr_rst_rx(base, id)                                                                              \
	{                                                                                                              \
		if (id)                                                                                                \
			(base)->csr_rst_rx1 = 1;                                                                       \
		else                                                                                                   \
			(base)->csr_rst_rx0 = 1;                                                                       \
	}
#define senif_syscfg_csr_rst_dec(base, id)                                                                             \
	{                                                                                                              \
		if (id)                                                                                                \
			(base)->csr_rst_dec1 = 1;                                                                      \
		else                                                                                                   \
			(base)->csr_rst_dec0 = 1;                                                                      \
	}
#define senif_syscfg_csr_rst_ps(base)                                                                                  \
	{                                                                                                              \
		(base)->csr_rst_ps = 1;                                                                                \
	}
#define senif_syscfg_csr_rst_slb(base, id)                                                                             \
	{                                                                                                              \
		if (id)                                                                                                \
			(base)->csr_rst_slb1 = 1;                                                                      \
		else                                                                                                   \
			(base)->csr_rst_slb0 = 1;                                                                      \
	}
#define senif_syscfg_csr_rst_ctrl(base)                                                                                \
	{                                                                                                              \
		(base)->csr_rst_ctrl = 1;                                                                              \
	}

/* lv_rst */
#define senif_syscfg_lv_rst_rx_senif_clk(base, id, lv)                                                                 \
	{                                                                                                              \
		if (id)                                                                                                \
			(base)->lv_rst_rx1_senif_clk = (lv);                                                           \
		else                                                                                                   \
			(base)->lv_rst_rx0_senif_clk = (lv);                                                           \
	}
#define senif_syscfg_lv_rst_dec_senif_clk(base, id, lv)                                                                \
	{                                                                                                              \
		if (id)                                                                                                \
			(base)->lv_rst_dec1_senif_clk = (lv);                                                          \
		else                                                                                                   \
			(base)->lv_rst_dec0_senif_clk = (lv);                                                          \
	}
#define senif_syscfg_lv_rst_dec_out_clk(base, id, lv)                                                                  \
	{                                                                                                              \
		if (id)                                                                                                \
			(base)->lv_rst_dec1_out_clk = (lv);                                                            \
		else                                                                                                   \
			(base)->lv_rst_dec0_out_clk = (lv);                                                            \
	}
#define senif_syscfg_lv_rst_ps_senif_clk(base, lv)                                                                     \
	{                                                                                                              \
		(base)->lv_rst_ps_senif_clk = (lv);                                                                    \
	}
#define senif_syscfg_lv_rst_ps_out_clk(base, lv)                                                                       \
	{                                                                                                              \
		(base)->lv_rst_ps_out_clk = (lv);                                                                      \
	}
#define senif_syscfg_lv_rst_slb_out_clk(base, id, lv)                                                                  \
	{                                                                                                              \
		if (id)                                                                                                \
			(base)->lv_rst_slb1_out_clk = (lv);                                                            \
		else                                                                                                   \
			(base)->lv_rst_slb0_out_clk = (lv);                                                            \
	}
#define senif_syscfg_lv_rst_ctrl_sensor_clk(base, lv)                                                                  \
	{                                                                                                              \
		(base)->lv_rst_ctrl_sensor_clk = (lv);                                                                 \
	}

#define senif_syscfg_sd(base, en)                                                                                      \
	{                                                                                                              \
		(base)->sd = (en);                                                                                     \
	}
#define senif_syscfg_slp(base, en)                                                                                     \
	{                                                                                                              \
		(base)->slp = (en);                                                                                    \
	}

#define senif_ctrl_mux_sel(base, id, sel)                                                                              \
	{                                                                                                              \
		if (id)                                                                                                \
			(base)->mux1_sel = (sel);                                                                      \
		else                                                                                                   \
			(base)->mux0_sel = (sel);                                                                      \
	}
#define senif_ctrl_set_lbuf_share(base, share)                                                                         \
	{                                                                                                              \
		(base)->lbuf_share = (share);                                                                          \
	}
#define senif_ctrl_slave_start(base)                                                                                   \
	{                                                                                                              \
		(base)->slave_start = 1;                                                                               \
	}
#define senif_ctrl_slave_stop(base)                                                                                    \
	{                                                                                                              \
		(base)->slave_stop = 1;                                                                                \
	}
#define senif_ctrl_set_slave_mode(base, slave_mode)                                                                    \
	{                                                                                                              \
		(base)->slave_mode = (slave_mode);                                                                     \
	}
#define senif_ctrl_set_slave_hpolar(base, polar)                                                                       \
	{                                                                                                              \
		(base)->slave_hpolar = (polar);                                                                        \
	}
#define senif_ctrl_set_slave_vpolar(base, polar)                                                                       \
	{                                                                                                              \
		(base)->slave_vpolar = (polar);                                                                        \
	}
#define senif_ctrl_get_slave_status(base) ((uint32_t)((base)->sssta))
#define senif_ctrl_set_slave_hactive(base, region)                                                                     \
	{                                                                                                              \
		(base)->slave_hactive = (region);                                                                      \
	}
#define senif_ctrl_set_slave_hbp(base, region)                                                                         \
	{                                                                                                              \
		(base)->slave_hbp = (region);                                                                          \
	}
#define senif_ctrl_set_slave_hfp(base, region)                                                                         \
	{                                                                                                              \
		(base)->slave_hfp = (region);                                                                          \
	}
#define senif_ctrl_set_slave_vactive(base, region)                                                                     \
	{                                                                                                              \
		(base)->slave_vactive = (region);                                                                      \
	}
#define senif_ctrl_set_slave_vbp(base, region)                                                                         \
	{                                                                                                              \
		(base)->slave_vbp = (region);                                                                          \
	}
#define senif_ctrl_set_slave_vfp(base, region)                                                                         \
	{                                                                                                              \
		(base)->slave_vfp = (region);                                                                          \
	}

#endif /* SENIF_H_ */
