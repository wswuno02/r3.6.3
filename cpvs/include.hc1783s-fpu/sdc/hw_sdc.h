#ifndef HW_SDC_H_
#define HW_SDC_H_

#include <stdint.h>

#define SDC_MM_CTRL (0x00)
#define SDC_MM_PWREN (0x04)
#define SDC_MM_CLKDIV (0x08)
#define SDC_MM_CLKSRC (0x0C)
#define SDC_MM_CLKENA (0x10)
#define SDC_MM_TMOUT (0x14)
#define SDC_MM_CTYPE (0x18)
#define SDC_MM_BLKSIZ (0x1C)
#define SDC_MM_BYTCNT (0x20)
#define SDC_MM_INTMASK (0x24)
#define SDC_MM_CMDARG (0x28)
#define SDC_MM_CMD (0x2C)
#define SDC_MM_RESP0 (0x30)
#define SDC_MM_RESP1 (0x34)
#define SDC_MM_RESP2 (0x38)
#define SDC_MM_RESP3 (0x3C)
#define SDC_MM_MINTSTS (0x40)
#define SDC_MM_RINTSTS (0x44)
#define SDC_MM_STATUS (0x48)
#define SDC_MM_FIFOTH (0x4C)
#define SDC_MM_CDETECT (0x50)
#define SDC_MM_WRTPRT (0x54)
#define SDC_MM_GPIO (0x58)
#define SDC_MM_TCBCNT (0x5C)
#define SDC_MM_TBBCNT (0x60)
#define SDC_MM_DEBNCE (0x64)
#define SDC_MM_USRID (0x68)
#define SDC_MM_VERID (0x6C)
#define SDC_MM_HCON (0x70)
#define SDC_MM_UHS_REG (0x74)
#define SDC_MM_RST_N (0x78)
#define SDC_MM_BMOD (0x80)
#define SDC_MM_PLDMND (0x84)
#define SDC_MM_DBADDR (0x88)
#define SDC_MM_IDSTS (0x8C)
#define SDC_MM_IDINTEN (0x90)
#define SDC_MM_DSCADDR (0x94)
#define SDC_MM_BUFADDR (0x98)
#define SDC_MM_CARDTHRCTL (0x100)
#define SDC_MM_BACK_END_POWER (0x104)
#define SDC_MM_UHS_REG_EXT (0x108)
#define SDC_MM_EMMC_DDR_REG (0x10C)
#define SDC_MM_ENABLE_SHIFT (0x110)
#define SDC_MM_DATA (0x200)
/* 64 bit address */
#define SDC_MM_DBADDRL (0x88)
#define SDC_MM_DBADDRU (0x8C)
#define SDC_MM_IDSTS64 (0x90)
#define SDC_MM_IDINTEN64 (0x94)
#define SDC_MM_DSCADDRL (0x98)
#define SDC_MM_DSCADDRU (0x9C)
#define SDC_MM_BUFADDRL (0xA0)
#define SDC_MM_BUFADDRU (0xA4)

#define SDC_REF_CLK_50MHZ (50000000)
#define HW_SDC_MMC_RCA (1)
#define HW_SDC_TIMEOUT (0x1000)

typedef enum mmc_boot_ack {
	MMC_BOOT_ACK_DISABLE = 0,
	MMC_BOOT_ACK_ENABLE,
} MmcBootAck;

typedef enum mmc_boot_partition_enable {
	MMC_BOOT_PARTITION_DISABLE = 0,
	MMC_BOOT_PARTITION_1_ENABLE,
	MMC_BOOT_PARTITION_2_ENABLE,
	MMC_BOOT_PARTITION_USER_ENABLE = 7,
} MmcBootPartitionEnable;

typedef enum mmc_boot_partition_access {
	MMC_BOOT_PARTITION_ACCESS_DISABLE = 0,
	MMC_BOOT_PARTITION_ACCESS_1,
	MMC_BOOT_PARTITION_ACCESS_2,
	MMC_BOOT_PARTITION_ACCESS_RPMB,
	MMC_BOOT_PARTITION_ACCESS_GP_1,
	MMC_BOOT_PARTITION_ACCESS_GP_2,
	MMC_BOOT_PARTITION_ACCESS_GP_3,
	MMC_BOOT_PARTITION_ACCESS_GP_4,
} MmcBootPartitionAccess;

typedef enum mmc_reset_boot_bus_width {
	MMC_RESET_BOOT_BUS_WIDTH = 0,
	MMC_RETAIN_BOOT_BUS_WIDTH,
} MmcResetBootBusWidth;

typedef enum mmc_boot_bus_width {
	MMC_BOOT_BUS_WIDTH_1X = 0,
	MMC_BOOT_BUS_WIDTH_4X,
} MmcBootBusWidth;

typedef enum mmc_partition {
	MMC_PARTITION_USER = 0,
	MMC_PARTITION_BOOT_1 = 1,
	MMC_PARTITION_BOOT_2 = 2,
} MmcPartition;

typedef struct sdc_dev {
	uint32_t sdc_base_addr;
	uint32_t sdc_cfg_base_addr;
	uint32_t rst_base_addr;
	uint32_t ref_clk_rate;
	uint32_t sdc_emmc_clk_rate;
} SdcDev;

/* COMMON */
void hw_sdc_set_reg(struct sdc_dev *dev, uint32_t offset, uint32_t val);
uint32_t hw_sdc_get_reg(struct sdc_dev *dev, uint32_t offset);
int hw_sdc_poll_reg(struct sdc_dev *dev, uint32_t offset, uint32_t exp_data, uint32_t en_bits, uint32_t timeout);
int hw_sdc_set_cmd(struct sdc_dev *dev, uint32_t cmd_arg, uint32_t cmd);
void hw_sdc_reset(struct sdc_dev *dev);

/* SD & MMC */
int hw_sdc_go_idle_state(struct sdc_dev *dev);
int hw_sdc_select_card(struct sdc_dev *dev, uint16_t rca);

/* MMC */
int hw_sdc_mmc_go_pre_idle_state(struct sdc_dev *dev);
void hw_sdc_mmc_boot_initiation(struct sdc_dev *dev);
int hw_sdc_mmc_send_op_cond(struct sdc_dev *dev);
int hw_sdc_mmc_all_send_cid(struct sdc_dev *dev);
int hw_sdc_mmc_set_relative_addr(struct sdc_dev *dev, uint16_t rca);
int hw_sdc_mmc_idle_to_tran(struct sdc_dev *dev, uint16_t rca);
int hw_sdc_mmc_set_partition_config(struct sdc_dev *dev, enum mmc_boot_ack boot_ack,
                                    enum mmc_boot_partition_enable boot_partition_enable,
                                    enum mmc_boot_partition_access boot_partition_access);
int hw_sdc_mmc_set_boot_bus_width(struct sdc_dev *dev, enum mmc_reset_boot_bus_width reset_boot_bus_width,
                                  enum mmc_boot_bus_width boot_bus_width);
int hw_sdc_mmc_read_boot_data(struct sdc_dev *dev, volatile uint32_t *dest, uint32_t size);
void hw_sdc_mmc_boot_operation_init(struct sdc_dev *dev);
int hw_sdc_mmc_boot_operation(struct sdc_dev *dev, uint8_t boot_partition);
int mmc_write(struct sdc_dev *dev, uint32_t blk_base, uint32_t mem_base, uint32_t size, enum mmc_partition part);
int mmc_read(struct sdc_dev *dev, uint32_t blk_base, uint32_t mem_base, uint32_t size, enum mmc_partition part);

#endif /* HW_SDC_H_ */
