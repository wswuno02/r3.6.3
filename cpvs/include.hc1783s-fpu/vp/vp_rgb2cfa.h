#ifndef VPK_rgb2cfa_H_
#define VPK_rgb2cfa_H_

#include "stdint.h"

typedef struct vp_rgb2cfa_resolution {
	uint32_t width;
	uint32_t height;
} VpRgb2cfaResolution;

typedef struct vp_rgb2cfa_ctx {
	VpRgb2cfaResolution rgb2cfa_res;
	uint32_t rgb2cfa_bayer_ini_phase_o;

} VpRgb2cfaCtx;

typedef struct vp_rgb2cfa_attr {
	uint32_t rgb2cfa_width;
	uint32_t rgb2cfa_height;
	uint32_t rgb2cfa_bayer_ini_phase_o;

} VpRgb2cfaAttr;

void hw_vp_rgb2cfa_frame_start();
void hw_vp_rgb2cfa_irq_clear_frame_end();
uint32_t hw_vp_rgb2cfa_status_frame_end();
void hw_vp_rgb2cfa_irq_mask_frame_end(uint32_t irq_mask_frame_end);
void hw_vp_rgb2cfa_resolution(uint32_t width, uint32_t height);
void hw_vp_rgb2cfa_bypass(uint32_t bayer_ini_phase_o);
void hw_vp_rgb2cfa_run(VpRgb2cfaAttr *vprgb2cfaattr);
void hw_vp_rgb2cfa_set_ctx(VpRgb2cfaCtx *vprgb2cfactx, VpRgb2cfaAttr *vprgb2cfaattr);
void hw_vp_rgb2cfa_set_reg(VpRgb2cfaCtx *vprgb2cfactx);
void hw_vp_rgb2cfa_debug_mon_sel(uint32_t debug_mon_sel);

#endif