#include <linux/module.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/err.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/of.h>
#include <linux/io.h>
#include <linux/mm.h>
#include <linux/device.h>
#include <linux/uaccess.h>
#include <linux/rwsem.h>

#define DRV_NAME "hc18xx_pll"
#define PLL_OUT_NUM 3

/* PLL OFFSET */
#define FRAC		0x0C
#define POSTDIV	0x14

static int pll_open(struct inode *inode, struct file *filp);
static int pll_release(struct inode *inode, struct file *filp);
ssize_t pll_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos);
static int pll_probe(struct platform_device *dev);
static int pll_remove(struct platform_device *dev);

static dev_t *gp_pll_devt, g_mpll_devt, g_spll_devt;
static struct cdev *gp_pll_cdev, g_mpll_cdev, g_spll_cdev;
static struct class *gp_pll_class, *gp_mpll_class, *gp_spll_class;

static const char **g_mpll_output_names, **g_spll_output_names;
static u32 *g_mpll_freq, *g_spll_freq;
static unsigned int g_mpll_major, g_spll_major;

void __iomem *mpll_base;

struct pll_drvdata {
	unsigned int major;
	void __iomem *base;
};

static int pll_open(struct inode *inode, struct file *filp)
{
	struct pll_drvdata *p;
	p = kmalloc(sizeof(struct pll_drvdata), GFP_KERNEL);
	//	printk("[K]sizeof pll_drvdata: %d\n", sizeof(struct pll_drvdata));
	if (p == NULL) {
		printk("%s: Not memory\n", __func__);
		return -ENOMEM;
	}
	p->major = imajor(inode);
	filp->private_data = p;
	return 0;
}

static int pll_release(struct inode *inode, struct file *filp)
{
	if (filp->private_data) {
		kfree(filp->private_data);
		filp->private_data = NULL;
	}
	return 0;
}

ssize_t pll_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos)
{
	struct pll_drvdata *p = filp->private_data;
	const char **output_names = NULL;
	u32 *dts_freq = NULL;
	void __iomem *base;
	char str[512];
	int len = 0;
	u32 tmp32, real_freq, p0, p1;
	u64 tmp64;
	if (*f_pos) {
		return 0;
	}
	
	if (p->major == g_mpll_major) {
		output_names = g_mpll_output_names;
		dts_freq = g_mpll_freq;
		base = mpll_base;
	} else if (p->major == g_spll_major) {
		output_names = g_spll_output_names;
		dts_freq = g_spll_freq;
		base = mpll_base + 0x40;
	} else {
		pr_err("Invalid major number!\n");
		return 0;
	}

	tmp32 = readl(base+FRAC);
	tmp64 = (u64)tmp32 * 24;
	real_freq = (tmp64>>24) + (tmp64>>23&1);

	tmp32 = readl(base+POSTDIV); // read postdiv0/postdiv1
	p0 = tmp32 & 0xf; 
	p1 = (tmp32>>16) & 0xf;
	printk("  div0: %d, div1: %d\n", p0, p1);
	p0 = real_freq/(p0+1); 
	p1 = real_freq/(p1+1);
	printk("  f0: %d, f1: %d\n", p0, p1);
	
	len = sprintf(str, "\t\tReal\t\tfrom DTS\n");
	len += sprintf(str+len, "%-9s:\t%4d MHz\t%4d MHz\n", output_names[0], real_freq, dts_freq[0]/1000000);
	len += sprintf(str+len, "%-9s:\t%4d MHz\t%4d MHz\n", output_names[1], p0, dts_freq[1]/1000000);
	len += sprintf(str+len, "%-9s:\t%4d MHz\t%4d MHz\n", output_names[2], p1, dts_freq[2]/1000000);
	//len = sprintf(str, "MPLL:\tVco  %4d MHz\n\tOut1 %4d MHz\n\tOut2 %4d MHz\n", m_freq, mp0, mp1);
	//len += sprintf(str+len, "SPLL:\tVco  %4d MHz\n\tOut1 %4d MHz\n\tOut2 %4d MHz\n", s_freq, sp0, sp1);
	len = (len > count) ? count : (len < 0) ? 0 : len;
	if (copy_to_user(buf, str, strlen(str))) {
		return -EFAULT;
	}
	*f_pos += len;

	return len;
}

static const struct file_operations pll_fops = {
	.owner = THIS_MODULE,
	.open = pll_open,
	.release = pll_release,
	.read = pll_read,
};

static int pll_probe(struct platform_device *dev)
{
	struct device_node *np = dev->dev.of_node;
	int err;
	unsigned int major = 0;
	u32 *freq;
	const char **output_names;
	struct resource *mem;
	int i;

	if (!strcmp(np->name, "mpll")) {
		gp_pll_devt = &g_mpll_devt;
		gp_pll_cdev = &g_mpll_cdev;
	} else if (!strcmp(np->name, "spll")) {
		gp_pll_devt = &g_spll_devt;
		gp_pll_cdev = &g_spll_cdev;
	} else {
		pr_err("Invalid pll node!\n");
		gp_pll_devt = NULL;
		gp_pll_cdev = NULL;
		return PTR_ERR(np->name);
	}

	/* Allocate cdev Number */
	err = alloc_chrdev_region(gp_pll_devt, 0, 1, DRV_NAME);
	if (err) {
		return -EBUSY;
	}

	/* Fill cdev Data Structure */
	cdev_init(gp_pll_cdev, &pll_fops);
	//gp_pll_cdev->owner = THIS_MODULE;
	//gp_pll_cdev->ops = &pll_fops;

	if (cdev_add(gp_pll_cdev, *gp_pll_devt, 1)) {
		unregister_chrdev_region(*gp_pll_devt, 1);
		return -EBUSY;
	}

	major = MAJOR(*gp_pll_devt);
	printk(KERN_ALERT "%s driver(major number %d) installed.\n", DRV_NAME, major);

	/* Generate Class */
	if (!strcmp(np->name, "mpll")) {
		gp_mpll_class = class_create(THIS_MODULE, np->name);
		gp_pll_class = gp_mpll_class;
		printk("  create %s class\n", np->name);
	} else if (!strcmp(np->name, "spll")) {
		gp_spll_class = class_create(THIS_MODULE, np->name);
		gp_pll_class = gp_spll_class;
		printk("  create %s class\n", np->name);
	} else {
		pr_err("Invalid pll node!\n");
		gp_pll_class = NULL;
		return PTR_ERR(np->name);
	}

	if (IS_ERR(gp_pll_class)) {
		cdev_del(gp_pll_cdev);
		unregister_chrdev_region(*gp_pll_devt, 1);
		return PTR_ERR(gp_pll_class);
	}
	/* Create Device Node in Sys File System */
	device_create(gp_pll_class, NULL, *gp_pll_devt, NULL, np->name);

	/* get output clock names and frequency from device tree */
	output_names = kzalloc(PLL_OUT_NUM * sizeof(*output_names), GFP_KERNEL);
	WARN_ON(!output_names);
	freq = kzalloc(PLL_OUT_NUM * sizeof(*freq), GFP_KERNEL);
	WARN_ON(!freq);
	of_property_read_u32_array(np, "clock-frequency", freq, PLL_OUT_NUM);
	for (i = 0; i < PLL_OUT_NUM; i++) {
		of_property_read_string_index(np, "clock-output-names", i, &output_names[i]);
	}
	if (!strcmp(np->name, "mpll")) {
		g_mpll_output_names = output_names;
		g_mpll_major = major;
		g_mpll_freq = freq;
		mem = platform_get_resource(dev, IORESOURCE_MEM, 0);
		mpll_base = devm_ioremap_resource(&dev->dev, mem);
		printk( "  %s: get output frequency\n", np->name);
	} else if (!strcmp(np->name, "spll")) {
		g_spll_output_names = output_names;
		g_spll_major = major;
		g_spll_freq = freq;
		printk( "  %s: get output frequency\n", np->name);
	} else {
		pr_err("Invalid pll node!\n");
		return PTR_ERR(np->name);
	}

	return 0;
}

static int pll_remove(struct platform_device *dev)
{
	struct device_node *np = dev->dev.of_node;
	u32 *freq;

	if (!strcmp(np->name, "mpll")) {
		gp_pll_devt = &g_mpll_devt;
		gp_pll_cdev = &g_mpll_cdev;
		gp_pll_class = gp_mpll_class;
		freq = g_mpll_freq;
	} else if (!strcmp(np->name, "spll")) {
		gp_pll_devt = &g_spll_devt;
		gp_pll_cdev = &g_spll_cdev;
		gp_pll_class = gp_spll_class;
		freq = g_spll_freq;
	} else {
		pr_err("Invalid pll node!\n");
		gp_pll_devt = NULL;
		gp_pll_cdev = NULL;
		gp_pll_class = NULL;
		return PTR_ERR(np->name);
	}

	device_destroy(gp_pll_class, *gp_pll_devt);
	class_destroy(gp_pll_class);
	printk(KERN_ALERT "destroy %s class\n", np->name);

	cdev_del(gp_pll_cdev);
	unregister_chrdev_region(*gp_pll_devt, 1);
	printk(KERN_ALERT "%s %s driver removed.\n", DRV_NAME, np->name);

	kfree(freq);
	printk(KERN_ALERT "%s: free alloc memory\n", np->name);

	return 0;
}



static const struct of_device_id pll_of_ids[] = {
	{.compatible = "augentix,hc18xx-pll", },
	{}
};
MODULE_DEVICE_TABLE(of, pll_of_ids);

static struct platform_driver pll_platform_driver = {
	.probe = pll_probe,
	.remove = pll_remove,
	.driver = {
		.name  = DRV_NAME,
		.owner = THIS_MODULE,
		.of_match_table = pll_of_ids,
	},
};
module_platform_driver(pll_platform_driver)

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Augentix HC18XX Pll Driver");
MODULE_AUTHOR("Augentix Inc.");
