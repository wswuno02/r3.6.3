#ifndef __AGTX_NRS_H
#define __AGTX_NRS_H

/*
 * AUGENTIX INC. - PROPRIETARY
 *
 * nrs-ioctl.h - Nrs ioctl definition
 * Copyright (C) 2019 ShihChieh Lin, Augentix Inc. <shihchieh.lin@augentix.com>
 *
 * NOTICE: The information contained herein is the property of Augentix Inc.
 * Copying and distributing of this file, via any medium,
 * must be licensed by Augentix Inc.
 *
 */

#ifndef __KERNEL__
#include <stdint.h>
#endif /* __KERNEL__ */

#define NRS_IOCTLID 'f'
#define NRS_IOCTLR_READ _IOR(NRS_IOCTLID, 1, uint64_t)

#endif
