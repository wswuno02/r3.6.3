/*
 * AUGENTIX INC. - PROPRIETARY
 *
 * nrs-api.h - Nrs module API
 * Copyright (C) 2019 ShihChieh Lin, Augentix Inc. <shihchieh.lin@augentix.com>
 *
 * NOTICE: The information contained herein is the property of Augentix Inc.
 * Copying and distributing of this file, via any medium,
 * must be licensed by Augentix Inc.
 *
 */

#define NRS_CODE_INVALID 0xffffffffUL

/**
 * CPU speed
 *
 * Identifies CPU frequency 480MHz/528MHz/400MHz
 */
enum nrs_cpu_speed { AGTX_CPU480 = 0, AGTX_CPU528, AGTX_CPU400 };
/**
 * Get CPU speed
 *
 * @return 0: 480MHz, 1: 528MHz, 2: 400MHz, -1: Error
 */
int nrs_get_cpuspeed(void);

/**
 * Check whether H264 HP is supported.
 *
 * H264 BP/MP are always supported.
 *
 * @return 0: false, 1: true
 */
int nrs_support_h264hp(void);

/**
 * Check whether H265 is supported
 *
 * @return 0: false, 1: true
 */
int nrs_support_h265(void);

/**
 * Check whether VENC MP and HP are supported
 *
 * @return 0: BP only, 1: MP/HP supported
 */
int nrs_support_vencmphp(void);

/**
 * Maximum resolution
 *
 * Identifies maximum resolution 10Mpel/ 6.5Mpel/ 3.5Mpel
 */
enum nrs_max_res { AGTX_RES_NOLIMIT = 0, AGTX_RES_10M, AGTX_RES_6P5M, AGTX_RES_3P5M };
/**
 * Check maximum resolution supported
 *
 * @return 0: no limit, 1: 10Mpel, 2: 6.5Mpel, 3: 3.5Mpel
 */
int nrs_get_maxres(void);

/**
 * Maximum bitdepth limitation identifier
 *
 * No limitation/ 14 bits/ 12 bits/ 10 bits
 */
enum nrs_max_bitdepth { AGTX_BITDEPTH_NOLIMIT = 0, AGTX_BITDEPTH_14, AGTX_BITDEPTH_12, AGTX_BITDEPTH_10 };
/**
 * Check maximum bit depth supported
 *
 * @return 0: no limit, 1: 14 bits, 2: 12 bits, 3: 10 bits
 */
int nrs_get_maxbitdepth(void);

/**
 * Maximum encoder pixel rate identifier
 *
 * No limitation / 142.222208 Mpel/s /
 * 101.836800Mpel/s / 56.448000 Mpel/s
 */
enum nrs_max_enc_pr { AGTX_ENC_PR_NOLIMIT = 0, AGTX_ENC_PR_142P2M, AGTX_ENC_PR_101P8M, AGTX_ENC_PR_56P4M };
/**
 * Check maximum encoder pixel rate supported
 *
 * @return 0: no limit, 1: 142.222208 Mpel/s, 2: 101.836800Mpel/s,
 *         3: 56.448000 Mpel/s
 */
int nrs_get_maxencpxlrate(void);

/**
 * Maximum FE pixel rate identifier
 *
 * No limitation / 142.606336 Mpel/s /
 * 100.663296Mpel/s / 58.720256 Mpel/s
 */
enum nrs_max_fe_pr { AGTX_FE_PR_NOLIMIT = 0, AGTX_FE_PR_142P6M, AGTX_FE_PR_100P6M, AGTX_FE_PR_58P7M };
/**
 * Check maximum FE pixel rate supported
 *
 * @return 0: no limit, 1: 142.606336 Mpel/s, 2: 100.663296Mpel/s,
 *         3: 58.720256 Mpel/s
 */
int nrs_get_maxfepxlrate(void);

/**
 * Check whether the product is Augentix turnkey
 *
 * @return 0: SDK, 1: Turnkey
 */
int nrs_is_turnkey(void);

/**
 * Check whether the product supports I2C Slave
 *
 * @return 0: false, 1: true
 */
int nrs_support_i2cs(void);

/**
 * Check whether the product supports MIPI decoder
 *
 * @return 0: false, 1: true
 */
int nrs_support_mipi(void);

/**
 * Check whether the product supports LVDS decoder
 *
 * @return 0: false, 1: true
 */
int nrs_support_lvds(void);

/**
 * Check whether the product supports dual MIPI decoder
 *
 * @return 0: false, 1: true
 */
int nrs_support_dualmipi(void);

/**
 * Check whether the product supports dual LVDS decoder
 *
 * @return 0: false, 1: true
 */
int nrs_support_duallvds(void);

/**
 * Check whether the product supports pad reset de-glitch
 *
 * @return 0: false, 1: true
 */
int nrs_support_rstdeglitch(void);

/**
 * Check whether the product supports GFX polar transform
 *
 * @return 0: false, 1: true
 */
int nrs_support_gfxplrxfm(void);

/**
 * Check whether the product supports GFX Square transform
 *
 * @return 0: false, 1: true
 */
int nrs_support_gfxsqrxfm(void);

/**
 * Check whether the product supports GFX Sphere transform
 *
 * @return 0: false, 1: true
 */
int nrs_support_gfxsphrxfm(void);

/**
 * Check whether the product supports GFX LDC transform
 *
 * @return 0: false, 1: true
 */
int nrs_support_gfxldc(void);

/**
 * Check whether the product supports DMA AES
 *
 * @return 0: false, 1: true
 */
int nrs_support_dmaaes(void);

/**
 * Check whether the product supports BSW AES
 *
 * @return 0: false, 1: true
 */
int nrs_support_bswaes(void);

/**
 * Check whether the product supports HDR
 *
 * @return 0: false, 1: true
 */
int nrs_support_hdr(void);

/**
 * Check whether the product supports top-and-bottom HDR
 *
 * @return 0: false, 1: true
 */
int nrs_support_tnbhdr(void);

/**
 * Check whether the product supports side-by-side HDR
 *
 * @return 0: false, 1: true
 */
int nrs_support_sbshdr(void);

/**
 * Check whether the product supports line-collocated HDR
 *
 * @return 0: false, 1: true
 */
int nrs_support_lineclcthdr(void);

/**
 * Check whether the product supports line-interleaved HDR
 *
 * @return 0: false, 1: true
 */
int nrs_support_lineintlhdr(void);

/**
 * Check whether the product supports pixel-collocated HDR
 *
 * @return 0: false, 1: true
 */
int nrs_support_pxlclcthdr(void);

/**
 * Check whether the product supports pixel-interleaved HDR
 *
 * @return 0: false, 1: true
 */
int nrs_support_pxlintlhdr(void);

/**
 * Reads package code
 *
 * @return: 0xffffffff: Invalid, otherwise: package code
 */
uint32_t nrs_get_wafer_code(void);

/**
 * Reads custumer ID
 *
 * @return: 0xffffffff: Invalid, otherwise: customer ID
 */
uint32_t nrs_get_package_code(void);

/**
 * Reads custumer ID
 *
 * @return: 0xffffffff: Invalid, otherwise: customer ID
 */
uint32_t nrs_get_customer_id(void);
