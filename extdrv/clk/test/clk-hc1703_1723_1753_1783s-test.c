#include <linux/module.h>
#include <linux/err.h>
#include <linux/platform_device.h>

#include <asm/io.h>
#include <linux/slab.h>
#include <linux/clk.h>
#include <linux/of.h>
#include <linux/of_address.h>

#define DRV_NAME "clk_test"

#define CLK_DEBUG

#ifdef CLK_DEBUG
#define DBG(fmt, args...)                                                       \
	do {                                                                    \
		printk("[CLK_TEST] (%d, %s) " fmt, __LINE__, __func__, ##args); \
	} while (0)
#endif

#define MAX_CLK_NUM (53 + 3)
struct clk_test_data {
	void __iomem *base;
	struct device *dev;
	struct clk *clk[MAX_CLK_NUM];
	u32 clock_frequency[MAX_CLK_NUM];
};

static char *hc1703_1723_1753_1783s_clk_name[] = {
	"clk_dramc",        "clk_dramc_hdr",    "clk_axi_dramc", "clk_dma",        "clk_apb_spi0",  "clk_apb_spi1",
	"clk_apb_uart0",    "clk_apb_uart1",    "clk_apb_uart2", "clk_apb_uart3",  "clk_apb_uart4", "clk_apb_uart5",
	"clk_apb_sdc0",     "clk_apb_sdc1",     "clk_apb_emac",  "clk_axi_rom",    "clk_axi_ram",   "clk_ahb_master",
	"clk_ahb_usb",      "clk_ahb_sdc_0",    "clk_ahb_sdc_1", "clk_emac_rgmii", "clk_emac_rmii", "clk_qspi",
	"clk_isp",          "clk_vp",           "clk_sensor",    "clk_senif",      "clk_is",        "clk_disp",
	"clk_sdc_0",        "clk_sdc_1",        "clk_enc",       "clk_efuse",      "clk_eirq",      "clk_is_lp",
	"clk_i2cm0",        "clk_i2cm1",        "clk_spi0",      "clk_spi1",       "clk_timer0",    "clk_timer1",
	"clk_timer2",       "clk_timer3",       "clk_pwm_0",     "clk_pwm_1",      "clk_pwm_2",     "clk_pwm_3",
	"clk_pwm_4",        "clk_pwm_5",        "clk_audio_in",  "clk_audio_out",  "clk_usb_utmi",  "pwm_pll_div",
	"audio_in_pll_div", "audio_out_pll_div"
};

static int clk_test_probe(struct platform_device *pdev)
{
	struct clk_test_data *clk_test = NULL;
	int ret, i;
	unsigned long rate;

	clk_test = devm_kzalloc(&pdev->dev, sizeof(struct clk_test_data), GFP_KERNEL);
	if (!clk_test)
		return -ENOMEM;
	clk_test->dev = &pdev->dev;

	platform_set_drvdata(pdev, clk_test);

	/* Target clock rate */
	of_property_read_u32_array(clk_test->dev->of_node, "clock-frequency", clk_test->clock_frequency, MAX_CLK_NUM);

	for (i = 0; i < MAX_CLK_NUM; i++) {
		clk_test->clk[i] = devm_clk_get(clk_test->dev, hc1703_1723_1753_1783s_clk_name[i]);

		if (IS_ERR(clk_test->clk[i])) {
			DBG("clock not available\n");
		}

		DBG("CLK[%d]: name = %s, clock-frequency = %lu.\n", i, hc1703_1723_1753_1783s_clk_name[i],
		    (unsigned long)clk_test->clock_frequency[i]);

		ret = clk_prepare(clk_test->clk[i]);

		if (ret) {
			dev_err(clk_test->dev, "Fail to prepare clock!\n");
			return ret;
		}

		ret = clk_enable(clk_test->clk[i]);

		if (ret) {
			dev_err(clk_test->dev, "Fail to enable clock!\n");
			return ret;
		}

		DBG("Enable clk finish!\n");
#if 0
		clk_disable(clk_test->clk[i]);

		DBG("Disable clk finish!\n");

		ret = clk_enable(clk_test->clk[i]);

		if (ret) {
			dev_err(clk_test->dev, "Fail to enable clock!\n");
			return ret;
		} else {
			DBG("Re-enable success!\n");
		}

		rate = clk_get_rate(clk_test->clk[i]);

		if (!rate) {
			dev_err(clk_test->dev, "Fail to get clock rate!\n");
		} else {
			DBG("clk[%d], get rate = %lu!\n", i, rate);
		}
#endif
	}

	printk("==== Audio PLL test cases ===\n");
#define AUDIO_IN_CLK 50
#define AUDIO_OUT_CLK 51
#define PWM_PLL 53
#define AUDIO_IN_PLL 54
#define AUDIO_OUT_PLL 55

	rate = clk_get_rate(clk_test->clk[AUDIO_IN_CLK]);
	DBG("AUDIO In clk rate: %lu\n", rate);
	rate = clk_get_rate(clk_test->clk[AUDIO_OUT_CLK]);
	DBG("AUDIO Out clk rate: %lu\n", rate);
	rate = clk_get_rate(clk_test->clk[PWM_PLL]);
	DBG("PWM clk rate: %lu\n", rate);
	rate = clk_get_rate(clk_test->clk[AUDIO_IN_PLL]);
	DBG("AUDIO In PLL rate: %lu\n", rate);
	rate = clk_get_rate(clk_test->clk[AUDIO_OUT_PLL]);
	DBG("AUDIO Out PLL rate: %lu\n", rate);

	clk_disable(clk_test->clk[AUDIO_IN_CLK]);
	clk_disable(clk_test->clk[AUDIO_OUT_CLK]);
	printk("Disabled AUDIO clocks\n");

	clk_set_rate(clk_test->clk[AUDIO_IN_PLL], clk_test->clock_frequency[AUDIO_IN_PLL]);
	clk_set_rate(clk_test->clk[AUDIO_OUT_PLL], clk_test->clock_frequency[AUDIO_OUT_PLL]);

	clk_enable(clk_test->clk[AUDIO_IN_CLK]);
	clk_enable(clk_test->clk[AUDIO_OUT_CLK]);
	printk("Enabled AUDIO clocks\n");

	rate = clk_get_rate(clk_test->clk[AUDIO_IN_CLK]);
	DBG("AUDIO In clk rate: %lu\n", rate);
	rate = clk_get_rate(clk_test->clk[AUDIO_OUT_CLK]);
	DBG("AUDIO Out clk rate: %lu\n", rate);
	rate = clk_get_rate(clk_test->clk[PWM_PLL]);
	DBG("PWM clk rate: %lu\n", rate);
	rate = clk_get_rate(clk_test->clk[AUDIO_IN_PLL]);
	DBG("AUDIO In PLL rate: %lu\n", rate);
	rate = clk_get_rate(clk_test->clk[AUDIO_OUT_PLL]);
	DBG("AUDIO Out PLL rate: %lu\n", rate);

	DBG("CLK test module probed.\n");
	return 0;
}

static int clk_test_remove(struct platform_device *pdev)
{
	struct clk_test_data *clk_test = platform_get_drvdata(pdev);
	devm_kfree(&pdev->dev, clk_test);
	platform_set_drvdata(pdev, NULL);
	return 0;
}

static const struct of_device_id clk_test_dt_ids[] = {
	{ .compatible = "augentix,clk-test" },
	{},
};
MODULE_DEVICE_TABLE(of, clk_test_dt_ids);

static struct platform_driver clk_test_driver = {
	.probe = clk_test_probe,
	.remove = clk_test_remove,
	.driver =
	        {
	                .owner = THIS_MODULE,
	                .name = DRV_NAME,
	                .of_match_table = clk_test_dt_ids,
	        },
};
module_platform_driver(clk_test_driver);

MODULE_AUTHOR("Louis Yang, Augentix <louis.yang@augentix.com>");
MODULE_DESCRIPTION("HC1703_1723_1753_1783S clock driver");
MODULE_LICENSE("GPL");
