/*
 * HC18XX clock controller driver
 *
 * Author: Shih-Chieh Lin
 *
 * This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of the GNU General Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 */

#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/clk.h>
#include <linux/clk-provider.h>
#include <linux/clkdev.h>
#include <linux/delay.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/platform_device.h>
#include <linux/spinlock.h>
#include "clk.h"

//#define DEBUG

#ifdef DEBUG
#define DBG(fmt, args...)                                                       \
	do {                                                                    \
		printk("[CLK_TEST] (%d, %s) " fmt, __LINE__, __func__, ##args); \
	} while (0)
#endif

#define CKEN_FLAG 0x1
#define CKSEL_FLAG 0x2
#define CKDIV_FLAG 0x4

#define DIV_SHIFT 0x98
#define PWM2_SEL_SHIFT 0x90
#define PWM3_SEL_SHIFT 0x94
#define MAX_CLK_NUM 53
#define DISP_CLK_ID 29
#define PWM2_CLK_ID 46
#define PWM3_CLK_ID 47

struct clk_desc {
	const char *name;
	const char *parent;
	u32 cken_bias;
	u8 flag;
};

static struct clk_desc hc1703_1723_1753_1783s_clk_desc[MAX_CLK_NUM] = {
	{
	        .name = "clk_dramc",
	        .parent = "ddr_pll_dclk0",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x0C,
	},
	{
	        .name = "clk_dramc_hdr",
	        .parent = "ddr_pll_dclk0_div2",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x0D,
	},
	{
	        .name = "clk_axi_dramc",
	        .parent = "ddr_pll_dclk4",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x10,
	},
	{
	        .name = "clk_dma",
	        .parent = "ddr_pll_dclk4_div4",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x14,
	},
	{
	        .name = "clk_apb_spi0",
	        .parent = "cpu_pll_dclk0_div4",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x18,
	},
	{
	        .name = "clk_apb_spi1",
	        .parent = "cpu_pll_dclk0_div4",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x19,
	},
	{
	        .name = "clk_apb_uart0",
	        .parent = "cpu_pll_dclk0_div4",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x1C,
	},
	{
	        .name = "clk_apb_uart1",
	        .parent = "cpu_pll_dclk0_div4",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x1D,
	},
	{
	        .name = "clk_apb_uart2",
	        .parent = "cpu_pll_dclk0_div4",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x1E,
	},
	{
	        .name = "clk_apb_uart3",
	        .parent = "cpu_pll_dclk0_div4",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x1F,
	},
	{
	        .name = "clk_apb_uart4",
	        .parent = "cpu_pll_dclk0_div4",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x20,
	},
	{
	        .name = "clk_apb_uart5",
	        .parent = "cpu_pll_dclk0_div4",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x21,
	},
	{
	        .name = "clk_apb_sdc0",
	        .parent = "emac_pll_dclk3",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x24,
	},
	{
	        .name = "clk_apb_sdc1",
	        .parent = "emac_pll_dclk3",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x25,
	},
	{
	        .name = "clk_apb_emac",
	        .parent = "emac_pll_dclk3",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x28,
	},
	{
	        .name = "clk_axi_rom",
	        .parent = "cpu_pll_dclk0_div4",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x2C,
	},
	{
	        .name = "clk_axi_ram",
	        .parent = "cpu_pll_dclk0_div4",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x30,
	},
	{
	        .name = "clk_ahb_master",
	        .parent = "cpu_pll_dclk0_div4",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x34,
	},
	{
	        .name = "clk_ahb_usb",
	        .parent = "cpu_pll_dclk0_div4",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x35,
	},
	{
	        .name = "clk_ahb_sdc_0",
	        .parent = "cpu_pll_dclk0_div4",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x38,
	},
	{
	        .name = "clk_ahb_sdc_1",
	        .parent = "cpu_pll_dclk0_div4",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x39,
	},
	{
	        .name = "clk_emac_rgmii",
	        .parent = "emac_pll_dclk3",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x3C,
	},
	{
	        .name = "clk_emac_rmii",
	        .parent = "emac_pll_dclk3_div5",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x3D,
	},
	{
	        .name = "clk_qspi",
	        .parent = "emac_pll_dclk4",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x40,
	},
	{
	        .name = "clk_isp",
	        .parent = "emac_pll_dclk5",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x44,
	},
	{
	        .name = "clk_vp",
	        .parent = "emac_pll_dclk5",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x45,
	},
	{
	        .name = "clk_sensor",
	        .parent = "sensor_pll_dclk3",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x48,
	},
	{
	        .name = "clk_senif",
	        .parent = "sensor_pll_dclk5",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x49,
	},
	{
	        .name = "clk_is",
	        .parent = "sensor_pll_dclk4",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x4D,
	},
	{
	        .name = "clk_disp",
	        .parent = "sensor_pll_dclk4",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x50,
	},
	{
	        .name = "clk_sdc_0",
	        .parent = "venc_pll_dclk3",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x54,
	},
	{
	        .name = "clk_sdc_1",
	        .parent = "venc_pll_dclk4",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x55,
	},
	{
	        .name = "clk_enc",
	        .parent = "venc_pll_dclk5",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x58,
	},
	{
	        .name = "clk_efuse",
	        .parent = "sys_clk",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x5C,
	},
	{
	        .name = "clk_eirq",
	        .parent = "sys_clk",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x60,
	},
	{
	        .name = "clk_is_lp",
	        .parent = "sys_clk",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x64,
	},
	{
	        .name = "clk_i2cm0",
	        .parent = "sys_clk",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x68,
	},
	{
	        .name = "clk_i2cm1",
	        .parent = "sys_clk",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x69,
	},
	{
	        .name = "clk_spi0",
	        .parent = "sys_clk",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x6C,
	},
	{
	        .name = "clk_spi1",
	        .parent = "sys_clk",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x6D,
	},
	{
	        .name = "clk_timer0",
	        .parent = "sys_clk",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x70,
	},
	{
	        .name = "clk_timer1",
	        .parent = "sys_clk",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x71,
	},
	{
	        .name = "clk_timer2",
	        .parent = "sys_clk",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x72,
	},
	{
	        .name = "clk_timer3",
	        .parent = "sys_clk",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x73,
	},
	{
	        .name = "clk_pwm_0",
	        .parent = "sys_clk",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x74,
	},
	{
	        .name = "clk_pwm_1",
	        .parent = "sys_clk",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x75,
	},
	{
	        .name = "clk_pwm_2",
	        .parent = "audio_pll_dclk3",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x76,
	},
	{
	        .name = "clk_pwm_3",
	        .parent = "audio_pll_dclk3",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x77,
	},
	{
	        .name = "clk_pwm_4",
	        .parent = "sys_clk",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x78,
	},
	{
	        .name = "clk_pwm_5",
	        .parent = "sys_clk",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x79,
	},
	{
	        .name = "clk_audio_in",
	        .parent = "audio_pll_dclk4",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x7C,
	},
	{
	        .name = "clk_audio_out",
	        .parent = "audio_pll_dclk5",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x7D,
	},
	{
	        .name = "clk_usb_utmi",
	        .parent = "usbphy_clk",
	        .flag = CKEN_FLAG,
	        .cken_bias = 0x81,
	},
};

/*
 * hw: clk_hw for mux/rate clock
 * gate: clk_gate for gate-type clock
 */
struct hc1703_1723_1753_1783s_clk_drvdata {
	void __iomem *base;
	struct clk_hw hw;
	spinlock_t lock;

	struct clk **clks;
	struct clk_onecell_data onecell;
	struct clk_desc *descs;

	const char *name;
	u32 disp_div;
	u32 pwm2_sel;
	u32 pwm3_sel;
};

static int __init hc1703_1723_1753_1783s_clocks_init(struct hc1703_1723_1753_1783s_clk_drvdata *clkdata)
{
	struct clk **clks = clkdata->clks;
	struct clk_desc *descs = clkdata->descs;

	struct clk_gate *gate = NULL;

	struct clk_hw *gate_hw;
	int parent_num;
	int i;

	gate = NULL;
	parent_num = 1;

	for (i = 0; i < MAX_CLK_NUM; i++) {
		gate = NULL;

		gate_hw = NULL;

		/* gate clock */
		if (descs[i].flag & CKEN_FLAG) {
			gate = kzalloc(sizeof(*gate), GFP_KERNEL);
			if (!gate)
				return -ENOMEM;

			gate->reg = (clkdata->base + (descs[i].cken_bias & 0xfc));
			gate->bit_idx = ((descs[i].cken_bias & 0x03) << 3);
			gate->lock = &clkdata->lock;

			gate_hw = &gate->hw;
			WARN_ON(!(gate_hw));
		}

		if (i == DISP_CLK_ID) {
			if (clkdata->disp_div == 1) {
				clks[i] = clk_register_gate(NULL, descs[i].name, descs[i].parent, 0, gate->reg,
				                            gate->bit_idx, 0, gate->lock);
			} else if (clkdata->disp_div == 2) {
				clks[i] = clk_register_gate(NULL, descs[i].name, "sensor_pll_dclk4_div2", 0, gate->reg,
				                            gate->bit_idx, 0, gate->lock);
			} else if (clkdata->disp_div == 4) {
				clks[i] = clk_register_gate(NULL, descs[i].name, "sensor_pll_dclk4_div4", 0, gate->reg,
				                            gate->bit_idx, 0, gate->lock);
			}

		} else if (i == PWM2_CLK_ID) {
			if (clkdata->pwm2_sel) {
				clks[i] = clk_register_gate(NULL, descs[i].name, descs[i].parent, 0, gate->reg,
				                            gate->bit_idx, 0, gate->lock);
			} else {
				clks[i] = clk_register_gate(NULL, descs[i].name, "sys_clk", 0, gate->reg, gate->bit_idx,
				                            0, gate->lock);
			}
		} else if (i == PWM3_CLK_ID) {
			if (clkdata->pwm3_sel) {
				clks[i] = clk_register_gate(NULL, descs[i].name, descs[i].parent, 0, gate->reg,
				                            gate->bit_idx, 0, gate->lock);
			} else {
				clks[i] = clk_register_gate(NULL, descs[i].name, "sys_clk", 0, gate->reg, gate->bit_idx,
				                            0, gate->lock);
			}
		} else {
			clks[i] = clk_register_gate(NULL, descs[i].name, descs[i].parent, 0, gate->reg, gate->bit_idx,
			                            0, gate->lock);
		}

		if (IS_ERR(clks[i])) {
			pr_err("Error: Failed to register clk[%d]: %s!\n", i, descs[i].name);
			return -ENODEV;
		}

		clk_register_clkdev(clks[i], descs[i].name, NULL);
	}

	return 0;
}

static void __init hc1703_1723_1753_1783s_clkctrl_init(struct device_node *np)
{
	struct hc1703_1723_1753_1783s_clk_drvdata *clkdata;
	int ret;

	/* clkdata content initialization */
	clkdata = kzalloc(sizeof(*clkdata), GFP_KERNEL);
	if (!clkdata) {
		pr_err("Error: Failed to alloc clkdata!\n");
		return;
	}

	spin_lock_init(&clkdata->lock);

	clkdata->base = of_iomap(np, 0);
	if (!clkdata->base) {
		pr_err("Error: Failed to iomap!\n");
		goto err_clkdata_free;
	}

	clkdata->descs = hc1703_1723_1753_1783s_clk_desc;
	clkdata->name = np->name;
	clkdata->clks = kzalloc(MAX_CLK_NUM * sizeof(*clkdata->clks), GFP_KERNEL);
	if (!clkdata->clks) {
		pr_err("Error: Failed to iomap!\n");
		goto err_iounmap;
	}

	/* set disp divider */
	ret = of_property_read_u32(np, "disp-div", &clkdata->disp_div);
	if (ret) {
		pr_warn("DISP divider not defined!\n");
		clkdata->disp_div = 0;
	}
	writel(clkdata->disp_div, clkdata->base + DIV_SHIFT);

	/* set pwm selector */
	ret = of_property_read_u32(np, "pwm2-sel", &clkdata->pwm2_sel);
	if (ret) {
		pr_warn("PWM2 selector not defined!\n");
		clkdata->pwm2_sel = 0;
	}
	writel(clkdata->pwm2_sel, clkdata->base + PWM2_SEL_SHIFT);

	ret = of_property_read_u32(np, "pwm3-sel", &clkdata->pwm3_sel);
	if (ret) {
		pr_warn("PWM3 selector not defined!\n");
		clkdata->pwm3_sel = 0;
	}
	writel(clkdata->pwm3_sel, clkdata->base + PWM3_SEL_SHIFT);

	ret = hc1703_1723_1753_1783s_clocks_init(clkdata);
	if (ret) {
		pr_err("Error: Failed to initialize clks!\n");
		return;
	}

	clkdata->onecell.clks = clkdata->clks;
	clkdata->onecell.clk_num = MAX_CLK_NUM;

	of_clk_add_provider(np, of_clk_src_onecell_get, &clkdata->onecell);

	printk("CLK: clk add provider finish\n");

	return;

err_iounmap:
	iounmap(clkdata->base);

err_clkdata_free:
	kfree(clkdata);

	return;
}
CLK_OF_DECLARE(hc1703_1723_1753_1783s_clkctrl, "augentix,hc1703_1723_1753_1783s-clkctrl",
               hc1703_1723_1753_1783s_clkctrl_init);

/* HC1703_1723_1753_1783S generic PLL receives one input clock (sys_clk), and outputs several clocks */
static void __init hc1703_1723_1753_1783s_pll_init(struct device_node *np)
{
	void __iomem *base;
	const char *parent, *clk_name;
	const char **output_names;
	u32 *freq;
	struct clk_onecell_data *onecell;
	int i;
	int pll_out_num;

	base = of_iomap(np, 0);
	WARN_ON(!base);

	/* get clk node name from device node */
	clk_name = np->name;
#ifdef DEBUG
	pr_info("HC1703_1723_1753_1783S PLL: %s\n", clk_name);
#endif

	pll_out_num = of_property_count_u32_elems(np, "clock-frequency");

	/* get parent node name from device node */
	parent = of_clk_get_parent_name(np, 0);
	WARN_ON(!parent);

	output_names = kzalloc(pll_out_num * sizeof(*output_names), GFP_KERNEL);
	WARN_ON(!output_names);

	onecell = kzalloc(sizeof(onecell), GFP_KERNEL);
	WARN_ON(!onecell);
	onecell->clk_num = pll_out_num;

	onecell->clks = kzalloc(pll_out_num * sizeof(*onecell->clks), GFP_KERNEL);
	WARN_ON(!onecell->clks);

	freq = kzalloc(pll_out_num * sizeof(*freq), GFP_KERNEL);
	WARN_ON(!freq);

	/* get output clock names and frequency from device tree */
	of_property_read_u32_array(np, "clock-frequency", freq, pll_out_num);
	for (i = 0; i < pll_out_num; i++) {
		of_property_read_string_index(np, "clock-output-names", i, &output_names[i]);
#ifdef DEBUG
		pr_info(" - output clk %2d: %10s,\tfreq: %10d\n", i, output_names[i], freq[i]);
#endif

		/* setup and register output clocks */
		onecell->clks[i] = clk_register_fixed_rate(NULL, output_names[i], parent, 0, freq[i]);
	}

	/* clock lookup registration */
	for (i = 0; i < pll_out_num; i++) {
		WARN_ON(IS_ERR(onecell->clks[i]));
		clk_register_clkdev(onecell->clks[i], output_names[i], NULL);
	}

	of_clk_add_provider(np, of_clk_src_onecell_get, onecell);
}
CLK_OF_DECLARE(hc1703_1723_1753_1783s_pll, "augentix,hc1703_1723_1753_1783s-pll", hc1703_1723_1753_1783s_pll_init);

// Global spinlock for PLL
static DEFINE_SPINLOCK(pll_lock);
#define FPLL_POST_DIV_SEL_OFFS 0x2C

/* hc1703_1723_1753_1783s audio pll receives one input clock (sys_clk), and outputs several clocks
 * It also provides post divider for audio in / audio out */
static void __init hc1703_1723_1753_1783s_audio_pll_init(struct device_node *np)
{
	void __iomem *base;
	const char *clk_name; // name of the clock node in DTS
	const char *parent; // name of the parent clock node in DTS
	const char *output_name; // names of the output clocks in DTS
	uint32_t *freq; // output freq list
	struct clk_onecell_data *onecell;
	int i;
	int pll_out_num;

	base = of_iomap(np, 0);
	WARN_ON(!base);

	/* get clk node name from device node */
	clk_name = np->name;

#ifdef DEBUG
	pr_info("hc1703_1723_1753_1783s AUDIO PLL: %s\n", clk_name);
#endif

	pll_out_num = of_property_count_u32_elems(np, "clock-frequency");

	/* get parent node name from device node */
	parent = of_clk_get_parent_name(np, 0);
	WARN_ON(!parent);

	output_name = kzalloc(sizeof(*output_name), GFP_KERNEL);
	WARN_ON(!output_name);

	onecell = kzalloc(sizeof(onecell), GFP_KERNEL);
	WARN_ON(!onecell);

	onecell->clk_num = pll_out_num;
	onecell->clks = kzalloc(pll_out_num * sizeof(*onecell->clks), GFP_KERNEL);
	WARN_ON(!onecell->clks);

	freq = kzalloc(pll_out_num * sizeof(*freq), GFP_KERNEL);
	WARN_ON(!freq);

	of_property_read_u32_array(np, "clock-frequency", freq, pll_out_num);
	for (i = 0; i < pll_out_num; i++) {
		of_property_read_string_index(np, "clock-output-names", i, &output_name);

		// bit shift = 8 * n, bit width 8
		onecell->clks[i] = clk_register_divider(NULL, output_name, parent, CLK_IGNORE_UNUSED,
		                                        base + FPLL_POST_DIV_SEL_OFFS, 8 * i, 8, CLK_DIVIDER_ONE_BASED,
		                                        &pll_lock);
		clk_register_clkdev(onecell->clks[i], output_name, NULL);
	}

	of_clk_add_provider(np, of_clk_src_onecell_get, onecell);
}
CLK_OF_DECLARE(hc1703_1723_1753_1783s_audio_pll, "augentix,hc1703_1723_1753_1783s-audio-pll",
               hc1703_1723_1753_1783s_audio_pll_init);

MODULE_AUTHOR("Louis Yang, Augentix <louis.yang@augentix.com>");
MODULE_DESCRIPTION("HC1703_1723_1753_1783S clock driver");
MODULE_LICENSE("GPL");
