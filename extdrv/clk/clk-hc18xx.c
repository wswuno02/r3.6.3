/*
 * HC18XX clock controller driver
 *
 * Author: Shih-Chieh Lin
 *
 * This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of the GNU General Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 */

#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/clk.h>
#include <linux/clk-provider.h>
#include <linux/clkdev.h>
#include <linux/delay.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/platform_device.h>
#include <linux/spinlock.h>
#include "clk.h"

static DEFINE_SPINLOCK(swrst_lock);
static __iomem void *sw_rst_base = NULL;
static int hc18xx_set_sw_rst(int index, int level)
{
	unsigned long flag;
	u32 addr_shift = index / 8;
	u32 val_shift = index % 8;
	u8 val;

	spin_lock_irqsave(&swrst_lock, flag);
	val = readb(sw_rst_base + addr_shift);
	if (level == 0)
		val = val & ~(1 << val_shift);
	else /* level == 1 */
		val = val | (1 << val_shift);
	writeb(val, sw_rst_base + addr_shift);
	spin_unlock_irqrestore(&swrst_lock, flag);

	return 0;
}

int hc18xx_disable_sw_rst(int index)
{
	return hc18xx_set_sw_rst(index, 0);
}
EXPORT_SYMBOL(hc18xx_disable_sw_rst);

int hc18xx_enable_sw_rst(int index)
{
	return hc18xx_set_sw_rst(index, 1);
}
EXPORT_SYMBOL(hc18xx_enable_sw_rst);

#define to_clk_gate(_hw) container_of(_hw, struct clk_gate, hw)
#define to_clk_mux(_hw) container_of(_hw, struct clk_mux, hw)
#define to_clk_divider(_hw) container_of(_hw, struct clk_divider, hw)

#define CKEN_OFFSET 0x00
#define CKSEL_OFFSET 0x04
#define CKDIV_OFFSET 0x18
#define SW_RST_OFFSET 0x30

#define CKEN_FLAG 0x1
#define CKSEL_FLAG 0x2
#define CKDIV_FLAG 0x4

/*
 * CKSEL:
 * 0: System clock
 * 1. Main PLL VCO out
 * 2: Main PLL out 1
 * 3: Main PLL out 2
 * 4: Sub PLL VCO out
 * 5: Sub PLL out 1
 * 6: Sub PLL out 2
 * 7: USB PLL
 */
#define MUX_MASK 0x7
#define DIV_REG_WIDTH 3
#define DIV_REG_MASK ((1 << DIV_REG_WIDTH) - 1)
#define DIV_REG_MAX DIV_REG_MASK
#define MAX_CLK_NUM 21
DEFINE_SPINLOCK(clk_lock);

struct clk_desc {
	const char *name;
	u8 cken_bit;
	u8 cksel_shift;
	u8 ckdiv_shift;
	u8 flag;
};

/*
 * Divider interval: in HC1702_1722_1752_1772_1782, two types of HW divider presents:
 * - Linear divider:
 * 0: Divided by 1
 * 1: Divided by 2
 * 2: Divided by 3
 * 3: Divided by 4
 * 4: Divided by 5
 * 5: Divided by 6
 * 6: Divided by 7
 * 7: Divided by 8
 * - Not-so-linear divider:
 * 0: Divided by 1
 * 1: Divided by 2
 * 2: Divided by 4
 * 3: Divided by 6
 * 4: Divided by 8
 * 5: Divided by 10
 * 6: Divided by 12
 * 7: Divided by 14
 */
#define DIV_LNR 0
#define DIV_NSL 1
#define DIV_DUMMY -1 /* register hole in DIV_* registers */

/* g_div_interval array is arranged as in CSR */
static int g_div_interval[] = {
	DIV_LNR, /* DIV_SEL_HBUS */
	DIV_LNR, /* DIV_SEL_CSR */
	DIV_DUMMY, DIV_NSL, /* DIV_SEL_DDRBUS */

	DIV_LNR, /* DIV_SEL_ISP */
	DIV_LNR, /* DIV_SEL_ENC */
	DIV_LNR, /* DIV_SEL_IS */
	DIV_NSL, /* DIV_SEL_PERI */

	DIV_NSL, /* DIV_SEL_EMAC */
	DIV_NSL, /* DIV_SEL_QSPI */
	DIV_NSL, /* DIV_SEL_SDC */
	DIV_NSL, /* DIV_SEL_SENSOR */

	DIV_NSL, /* DIV_SEL_LVDS */
};

static struct clk_desc hc18xx_clk_desc[MAX_CLK_NUM] = {
	{
	        .name = "clk_rom",
	        .flag = CKEN_FLAG,
	        .cken_bit = 0,
	},
	{
	        .name = "clk_ddrbus",
	        .flag = CKEN_FLAG | CKSEL_FLAG | CKDIV_FLAG,
	        .cken_bit = 1,
	        .cksel_shift = 3,
	        .ckdiv_shift = 3,
	},
	{
	        .name = "clk_ddr",
	        .flag = CKEN_FLAG | CKSEL_FLAG,
	        .cken_bit = 2,
	        .cksel_shift = 2,
	},
	{
	        .name = "clk_is",
	        .flag = CKEN_FLAG | CKSEL_FLAG | CKDIV_FLAG,
	        .cken_bit = 3,
	        .cksel_shift = 6,
	        .ckdiv_shift = 6,
	},
	{
	        .name = "clk_isp",
	        .flag = CKEN_FLAG | CKSEL_FLAG | CKDIV_FLAG,
	        .cken_bit = 4,
	        .cksel_shift = 4,
	        .ckdiv_shift = 4,
	},
	{
	        .name = "clk_enc",
	        .flag = CKEN_FLAG | CKSEL_FLAG | CKDIV_FLAG,
	        .cken_bit = 5,
	        .cksel_shift = 5,
	        .ckdiv_shift = 5,
	},
	{
	        .name = "clk_peri",
	        .flag = CKEN_FLAG | CKSEL_FLAG | CKDIV_FLAG,
	        .cken_bit = 6,
	        .cksel_shift = 7,
	        .ckdiv_shift = 7,
	},
	{
	        .name = "clk_usb",
	        .flag = CKEN_FLAG,
	        .cken_bit = 7,
	},
	{
	        .name = "clk_lvds",
	        .flag = CKEN_FLAG | CKSEL_FLAG | CKDIV_FLAG,
	        .cken_bit = 8,
	        .cksel_shift = 12,
	        .ckdiv_shift = 12,
	},
	{
	        .name = "clk_qspi",
	        .flag = CKEN_FLAG | CKSEL_FLAG | CKDIV_FLAG,
	        .cken_bit = 9,
	        .cksel_shift = 9,
	        .ckdiv_shift = 9,
	},
	{
	        .name = "clk_emac",
	        .flag = CKEN_FLAG | CKSEL_FLAG | CKDIV_FLAG,
	        .cken_bit = 10,
	        .cksel_shift = 8,
	        .ckdiv_shift = 8,
	},
	{
	        .name = "clk_sdc",
	        .flag = CKEN_FLAG | CKSEL_FLAG | CKDIV_FLAG,
	        .cken_bit = 11,
	        .cksel_shift = 10,
	        .ckdiv_shift = 10,
	},
	{
	        .name = "clk_sensor",
	        .flag = CKEN_FLAG | CKSEL_FLAG | CKDIV_FLAG,
	        .cken_bit = 12,
	        .cksel_shift = 11,
	        .ckdiv_shift = 11,
	},
	{
	        .name = "clk_dma",
	        .flag = CKEN_FLAG,
	        .cken_bit = 13,
	},
	{
	        .name = "clk_audio_in",
	        .flag = CKEN_FLAG,
	        .cken_bit = 14,
	},
	{
	        .name = "clk_audio_out",
	        .flag = CKEN_FLAG,
	        .cken_bit = 15,
	},
	{
	        .name = "clk_sub_lvds",
	        .flag = CKEN_FLAG,
	        .cken_bit = 16,
	},
	{
	        .name = "clk_dramc",
	        .flag = CKEN_FLAG,
	        .cken_bit = 17,
	},
	{
	        .name = "clk_ram",
	        .flag = CKEN_FLAG,
	        .cken_bit = 18,
	},
	{
	        .name = "clk_cpu",
	        .flag = CKSEL_FLAG | CKDIV_FLAG,
	        .cksel_shift = 0,
	        .ckdiv_shift = 0,
	},
	{
	        .name = "clk_csr",
	        .flag = CKSEL_FLAG | CKDIV_FLAG,
	        .cksel_shift = 1,
	        .ckdiv_shift = 1,
	},
};

/*
 * hw: clk_hw for mux/rate clock
 * gate: clk_gate for gate-type clock
 */
struct hc18xx_clk_drvdata {
	void __iomem *base;
	void __iomem *cken_base;
	void __iomem *cksel_base;
	void __iomem *ckdiv_base;

	struct clk **clks;
	struct clk_onecell_data onecell;
	struct clk_desc *descs;

	const char *name;
	const char **parents;
	u32 parent_num;
};

static u8 hc18xx_clk_get_parent(struct clk_hw *hw)
{
	struct clk_mux *mux = to_clk_mux(hw);
	u8 val;

	val = readb(mux->reg + mux->shift) & mux->mask;
	return val;
}

static int hc18xx_clk_set_parent(struct clk_hw *hw, u8 index)
{
	struct clk_mux *mux = to_clk_mux(hw);

	writeb(index, mux->reg + mux->shift);
	return 0;
}

static u32 reg_to_div(u8 reg, int div_type)
{
	u32 div;
	switch (div_type) {
	case DIV_LNR:
		div = reg + 1;
		break;
	case DIV_NSL:
		div = reg ? (reg << 1) : 1;
		break;
	default:
		pr_err("Invalid DIV_SEL CSR!\n");
		return -EINVAL;
	}

	return div;
}

static u8 div_to_reg(u32 div, int div_type)
{
	u8 reg = 0;
	switch (div_type) {
	case DIV_LNR:
		reg = min((int)(div - 1), DIV_REG_MAX);
		break;
	case DIV_NSL:
		reg = (div == 1) ? 0 : min((int)((div + 1) >> 1), DIV_REG_MAX);
		break;
	default:
		pr_err("Invalid DIV_SEL CSR!\n");
		return -EINVAL;
	}

	return reg;
}

/* querying HW */
static unsigned long hc18xx_clk_recalc_rate(struct clk_hw *hw, unsigned long parent_rate)
{
	struct clk_divider *divider = to_clk_divider(hw);
	u8 reg = readb(divider->reg + divider->shift) & DIV_REG_MASK;
	u32 div = reg_to_div(reg, g_div_interval[divider->shift]);
	unsigned long rate = DIV_ROUND_UP(parent_rate, div);

#ifdef DEBUG
	pr_info("%s:\n"
	        "\tCLK: %s\n"
	        "\tparent rate     = %lu\n"
	        "\trate            = %lu\n"
	        "\trounded divisor = %d\n"
	        "\trounded div_sel = %d\n",
	        __func__, __clk_get_name(hw->clk), parent_rate, rate, div, reg);
#endif

	return rate;
}

/* Currently the parent rate is not modified by the function */
static long hc18xx_clk_round_rate(struct clk_hw *hw, unsigned long rate, unsigned long *parent_rate)
{
	u32 div = DIV_ROUND_UP(*parent_rate, rate);

#ifdef DEBUG
	struct clk_divider *divider = to_clk_divider(hw);
	u8 reg = div_to_reg(div, g_div_interval[divider->shift]);

	pr_info("%s:\n"
	        "\tCLK: %s\n"
	        "\tparent rate           = %lu\n"
	        "\ttarget_rate           = %lu\n"
	        "\trounded rate (calc)   = %lu\n"
	        "\trounded rate (actual) = %lu\n"
	        "\trounded divisor = %d\n"
	        "\trounded div_sel = %d\n",
	        __func__, __clk_get_name(hw->clk), *parent_rate, rate, (*parent_rate / div),
	        (*parent_rate / reg_to_div(reg, g_div_interval[divider->shift])),
	        reg_to_div(reg, g_div_interval[divider->shift]), reg);
#endif
	return (*parent_rate / div);
}

static int hc18xx_clk_set_rate(struct clk_hw *hw, unsigned long rate, unsigned long parent_rate)
{
	struct clk_divider *divider = to_clk_divider(hw);
	u32 div = DIV_ROUND_UP(parent_rate, rate);
	u8 reg = div_to_reg(div, g_div_interval[divider->shift]);

#ifdef DEBUG
	pr_info("%s:\n"
	        "\tCLK: %s\n"
	        "\tparent rate             = %lu\n"
	        "\ttarget_rate             = %lu\n"
	        "\trounded rate (calc)     = %lu\n"
	        "\trounded rate (actual)   = %lu\n"
	        "\trounded divisor = %d\n"
	        "\trounded div_sel = %d\n",
	        __func__, __clk_get_name(hw->clk), parent_rate, rate, (parent_rate / div),
	        (parent_rate / reg_to_div(reg, g_div_interval[divider->shift])), div, reg);
#endif
	writeb(reg, divider->reg + divider->shift);
	return 0;
}

static struct clk_ops ckdiv_ops = {
	.recalc_rate = hc18xx_clk_recalc_rate,
	.set_rate = hc18xx_clk_set_rate,
	.round_rate = hc18xx_clk_round_rate,
};

static struct clk_ops cksel_ops = {
	.get_parent = hc18xx_clk_get_parent,
	.set_parent = hc18xx_clk_set_parent,
};

int hc18xx_set_clk_parent_by_name(struct clk *clk, const char *parent_name)
{
	struct clk *parent_clk = NULL;
	const char *name;
	u8 i, max_num;

	if (!clk)
		return -EINVAL;

	max_num = __clk_get_num_parents(clk);
	for (i = 0; i < max_num; i++) {
		parent_clk = clk_get_parent_by_index(clk, i);
		name = __clk_get_name(parent_clk);

		if (!strcmp(parent_name, name))
			return clk_set_parent(clk, parent_clk);
	}
	return -EINVAL;
}
EXPORT_SYMBOL(hc18xx_set_clk_parent_by_name);

static int __init hc18xx_clocks_init(struct hc18xx_clk_drvdata *clkdata)
{
	struct clk **clks = clkdata->clks;
	struct clk_desc *descs = clkdata->descs;

	struct clk_gate *gate = NULL;
	struct clk_mux *mux = NULL;
	struct clk_divider *div = NULL;

	struct clk_hw *gate_hw, *mux_hw, *div_hw;
	struct clk_ops *mux_ops, *div_ops;
	const struct clk_ops *gate_ops;
	int parent_num;
	int i;

	for (i = 0; i < MAX_CLK_NUM; i++) {
		gate = NULL;
		mux = NULL;
		div = NULL;

		gate_hw = NULL;
		mux_hw = NULL;
		div_hw = NULL;

		gate_ops = NULL;
		mux_ops = NULL;
		div_ops = NULL;

		parent_num = 1;

		/* Check if need to create a gate/mux/divider clock */
		/* mux clock */
		if (descs[i].flag & CKSEL_FLAG) {
			mux = kzalloc(sizeof(*mux), GFP_KERNEL);
			WARN_ON(!mux);

			mux->reg = clkdata->cksel_base;
			mux->shift = descs[i].cksel_shift;
			mux->mask = MUX_MASK;
			mux->lock = &clk_lock;

			mux_hw = &mux->hw;
			mux_ops = &cksel_ops;
			WARN_ON(!(mux_hw && mux_ops));

			parent_num = clkdata->parent_num;
		}
		/* divider clock */
		if (descs[i].flag & CKDIV_FLAG) {
			div = kzalloc(sizeof(*div), GFP_KERNEL);
			WARN_ON(!div);

			div->reg = clkdata->ckdiv_base;
			div->shift = descs[i].ckdiv_shift;
			div->width = DIV_REG_WIDTH;
			div->lock = &clk_lock;

			div_hw = &div->hw;
			div_ops = &ckdiv_ops;
		}
		/* gate clock */
		if (descs[i].flag & CKEN_FLAG) {
			gate = kzalloc(sizeof(*gate), GFP_KERNEL);
			if (!gate)
				return -ENOMEM;

			gate->reg = clkdata->cken_base;
			gate->bit_idx = descs[i].cken_bit;
			gate->lock = &clk_lock;

			gate_hw = &gate->hw;
			gate_ops = &clk_gate_ops;
			WARN_ON(!(gate_hw && gate_ops));
		}

		clks[i] = clk_register_composite(NULL, descs[i].name, clkdata->parents, parent_num, mux_hw, mux_ops,
		                                 div_hw, div_ops, gate_hw, gate_ops, 0);
		WARN_ON(!clks[i]);
		clk_register_clkdev(clks[i], descs[i].name, NULL);
	}

	return 0;
}

static void __init hc18xx_clkctrl_init(struct device_node *np)
{
	struct hc18xx_clk_drvdata *clkdata;
	int i, ret;

	/* clkdata content initialization */
	clkdata = kzalloc(sizeof(*clkdata), GFP_KERNEL);
	WARN_ON(!clkdata);

	clkdata->base = of_iomap(np, 0);
	WARN_ON(!clkdata->base);
	clkdata->cken_base = clkdata->base + CKEN_OFFSET;
	clkdata->cksel_base = clkdata->base + CKSEL_OFFSET;
	clkdata->ckdiv_base = clkdata->base + CKDIV_OFFSET;
	sw_rst_base = clkdata->base + SW_RST_OFFSET;

	clkdata->descs = hc18xx_clk_desc;
	clkdata->name = np->name;
	clkdata->clks = kzalloc(MAX_CLK_NUM * sizeof(*clkdata->clks), GFP_KERNEL);
	WARN_ON(!clkdata->clks);

	/* get parent count and names from device node*/
	clkdata->parent_num = of_clk_get_parent_count(np);
#ifdef DEBUG
	pr_info("clk controller %s: find %d clock sources\n", clkdata->name, clkdata->parent_num);
#endif
	clkdata->parents = kzalloc(clkdata->parent_num * sizeof(*clkdata->parents), GFP_KERNEL);
	for (i = 0; i < clkdata->parent_num; i++) {
		clkdata->parents[i] = of_clk_get_parent_name(np, i);
#ifdef DEBUG
		pr_info(" - clksrc %d: %s\n", i, clkdata->parents[i]);
#endif
	}

	ret = hc18xx_clocks_init(clkdata);
	if (ret) {
		pr_info("Error: Failed to initialize clks!\n");
		return;
	}

	clkdata->onecell.clks = clkdata->clks;
	clkdata->onecell.clk_num = MAX_CLK_NUM;

	of_clk_add_provider(np, of_clk_src_onecell_get, &clkdata->onecell);
	return;
}
CLK_OF_DECLARE(hc18xx_clkctrl, "augentix,hc18xx-clkctrl", hc18xx_clkctrl_init);

#define PLL_OUT_NUM 3
#define PLL_DIV_REF_MASK 0x10
#define PLL_DIV_REF_BIT
#define PLL_DIV_REF_SHIFT
#define PLL_DIV_OUT 0x14

enum { pll_vco, pll_out1, pll_out2 };

/* HC18XX PLL receives one input clock (sys_clk), and outputs 3 clocks
 * Inside PLL, VCO receives the input clock and generates
 *
 *                                ___________[PLL_VCO]  --->
 *          _______              |   _____
 * [OSC]___|DIV VCO|__[PLL_VCO]__|__|DIV 1|__[PLL_OUT1] --->
 *         |_1-63__|             |  |1-16_|
 *                               |   _____
 *                               |__|DIV 2|__[PLL_OUT2] --->
 *                                  |1-16_|
 */
static void __init hc18xx_pll_init(struct device_node *np)
{
	void __iomem *base;
	const char *parent, *clk_name;
	const char **output_names;
	u32 *freq;
	struct clk_onecell_data *onecell;
	int i;

	base = of_iomap(np, 0);
	WARN_ON(!base);

	/* get clk node name from device node */
	clk_name = np->name;
#ifdef DEBUG
	pr_info("HC1703_1723_1753_1783S PLL: %s\n", clk_name);
#endif

	/* get parent node name from device node */
	parent = of_clk_get_parent_name(np, 0);
	WARN_ON(!parent);

	output_names = kzalloc(PLL_OUT_NUM * sizeof(*output_names), GFP_KERNEL);
	WARN_ON(!output_names);

	onecell = kzalloc(sizeof(onecell), GFP_KERNEL);
	WARN_ON(!onecell);
	onecell->clk_num = PLL_OUT_NUM;

	onecell->clks = kzalloc(PLL_OUT_NUM * sizeof(*onecell->clks), GFP_KERNEL);
	WARN_ON(!onecell->clks);

	freq = kzalloc(PLL_OUT_NUM * sizeof(*freq), GFP_KERNEL);
	WARN_ON(!freq);

	/* get output clock names and frequency from device tree */
	of_property_read_u32_array(np, "clock-frequency", freq, PLL_OUT_NUM);
	for (i = 0; i < PLL_OUT_NUM; i++) {
		of_property_read_string_index(np, "clock-output-names", i, &output_names[i]);
#ifdef DEBUG
		pr_info(" - output clk %2d: %10s,\tfreq: %10d\n", i, output_names[i], freq[i]);
#endif
	}

	/* setup and register output clocks */
	onecell->clks[pll_vco] = clk_register_fixed_rate(NULL, output_names[pll_vco], parent, 0, freq[pll_vco]);
	/* out1 and out2 receive PLL_VCO and divide it */
	onecell->clks[pll_out1] =
	        clk_register_fixed_rate(NULL, output_names[pll_out1], output_names[pll_vco], 0, freq[pll_out1]);
	onecell->clks[pll_out2] =
	        clk_register_fixed_rate(NULL, output_names[pll_out2], output_names[pll_vco], 0, freq[pll_out2]);

	/* clock lookup registration */
	for (i = 0; i < PLL_OUT_NUM; i++) {
		WARN_ON(IS_ERR(onecell->clks[i]));
		clk_register_clkdev(onecell->clks[i], output_names[i], NULL);
	}

	of_clk_add_provider(np, of_clk_src_onecell_get, onecell);
}
CLK_OF_DECLARE(hc18xx_pll, "augentix,hc18xx-pll", hc18xx_pll_init);

MODULE_AUTHOR("Shih-Chieh Lin, Augentix <shihchieh.lin@augentix.com>");
MODULE_DESCRIPTION("HC18XX clock driver");
MODULE_LICENSE("GPL");
