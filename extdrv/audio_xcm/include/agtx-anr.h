#ifndef AGTX_ANR_H_
#define AGTX_ANR_H_

#include <linux/types.h>
#include "pcm_interfaces.h"

struct agtx_anr_info {
	int anr_en;
	int anr_init;
	uint32_t sample_rate;
	uint32_t num_samples; /* IN  >= window_size */
	int16_t *buf; /* IN/OUT  buf[num_samples] */
	int16_t *buf_pre; /* IN/OUT  buf_pre[window_size] */
	int16_t *mag_pre; /* IN/OUT  k_mag[window_size] */
	int16_t *mag_wei_pre; /* IN/OUT  k_mag[window_size] */
	int window_sum_wei;
	int window_sum;
	int gain_last;
	uint16_t window_size; /* IN  33, 65, 129, default: 129 */
	struct anr_params params;
};

int anr_init(struct agtx_anr_info *anr_info);
int anr(struct agtx_anr_info *anr_info);

#endif /* AGTX_ANR_H_ */
