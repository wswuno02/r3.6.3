#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/of.h>
#include <linux/gpio.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/pcm_params.h>
#include <sound/soc.h>

#define DRIVER_NAME "agtx-adcdac"

static const struct snd_soc_dapm_widget agtx_adcdac_dapm_widgets[] = {
	SND_SOC_DAPM_MIC("AMIC", NULL),
	SND_SOC_DAPM_INPUT("LINE1"),
	SND_SOC_DAPM_ADC("ADC", "Capture", SND_SOC_NOPM, 0, 0),
	SND_SOC_DAPM_DAC("DAC", "Playback", SND_SOC_NOPM, 0, 0),
	SND_SOC_DAPM_OUTPUT("LINE2"),
	SND_SOC_DAPM_SPK("SPK", NULL),
};

static const struct snd_soc_dapm_route agtx_audio_map[] = { { "LINE1", NULL, "AMIC" },
	                                                    { "ADC", NULL, "LINE1" },
	                                                    { "DAC", NULL, "LINE2" },
	                                                    { "LINE2", NULL, "SPK" } };

static struct snd_soc_dai_link agtx_dais[] = {
	{
	        .name = DRIVER_NAME,
	        .stream_name = "Capture",
	        .codec_name = "snd-soc-dummy",
	        .codec_dai_name = "snd-soc-dummy-dai",
	        .cpu_dai_name = "80100000.audio_pcm",
	        .platform_name = "80100000.audio_pcm",
	        .dai_fmt = SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_NB_NF,
	},
};

static struct snd_soc_card agtx_card = {
	.name = DRIVER_NAME,
	.owner = THIS_MODULE,
	.dai_link = agtx_dais,
	.num_links = ARRAY_SIZE(agtx_dais),
	.dapm_widgets = agtx_adcdac_dapm_widgets,
	.num_dapm_widgets = ARRAY_SIZE(agtx_adcdac_dapm_widgets),
	.dapm_routes = agtx_audio_map,
	.num_dapm_routes = ARRAY_SIZE(agtx_audio_map),
};

static int audio_machine_probe(struct platform_device *pdev)
{
	int ret;
	struct snd_soc_card *card = &agtx_card;
	card->dev = &pdev->dev;

	/* FIXME: read route from device tree */

	ret = devm_snd_soc_register_card(&pdev->dev, &agtx_card);

	if (ret) {
		dev_err(&pdev->dev, "snd_soc_register_card failed (error: %d)\n", ret);
		return -EINVAL;
	}

	pr_info("ASoC: %s audio probed.\n", DRIVER_NAME);

	return 0;
}

static int audio_machine_remove(struct platform_device *pdev)
{
	/* */

	return 0;
}


static const struct of_device_id audio_machine_of_ids[] = {
	{ .compatible = "augentix,audio_machine" },
	{},
};
MODULE_DEVICE_TABLE(of, audio_machine_of_ids);

static struct platform_driver audio_machine_driver = {
	.probe = audio_machine_probe,
	.remove = audio_machine_remove,
	.driver = {
		.name = DRIVER_NAME,
		.owner = THIS_MODULE,
		.of_match_table = audio_machine_of_ids,
	},
};
module_platform_driver(audio_machine_driver);

MODULE_DESCRIPTION("Augentix Audio Machine");
MODULE_AUTHOR("Henry Liu <henry.liu@augentix.com>");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:agtx-adcdac");
