#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/of.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/pcm_params.h>
#include <sound/soc.h>

#define DRIVER_NAME "HC18XX-RT5660"
#define SYSCLK_RATE 24576000
#define MCLK_RATE   12000000

static const struct snd_soc_dapm_widget hc18xx_rt5660_dapm_widgets[] = {
SND_SOC_DAPM_MIC("AMIC1", NULL),
SND_SOC_DAPM_MIC("AMIC2", NULL),
SND_SOC_DAPM_MIC("AMIC3", NULL),
SND_SOC_DAPM_SPK("SPK", NULL),
SND_SOC_DAPM_HP("HPOL", NULL),
SND_SOC_DAPM_HP("HPOR", NULL),
};

static const struct snd_soc_dapm_route hc18xx_audio_map[] = {
	{"SPK", NULL, "SPO"},
	{"HPOL", NULL, "LOUTL"},
	{"HPOR", NULL, "LOUTL"},
	{"AMIC1", NULL, "MICBIAS1"},
	{"IN1P", NULL, "AMIC1"},
	{"IN1N", NULL, "AMIC1"},
	{"AMIC2", NULL, "MICBIAS2"},
	{"IN2P", NULL, "AMIC2"},
	{"AMIC3", NULL, "MICBIAS2"},
	{"IN3P", NULL, "AMIC3"},
	{"IN3N", NULL, "AMIC3"},
};

static int hc18xx_rt5660_init(struct snd_soc_pcm_runtime *rtd)
{
	int ret = 0;
	struct snd_soc_dapm_context *dapm = &rtd->card->dapm;

	pr_debug("fn: %s\n", __func__);

	snd_soc_dapm_enable_pin(dapm, "AMIC1");
	snd_soc_dapm_enable_pin(dapm, "AMIC2");
	snd_soc_dapm_enable_pin(dapm, "SPK");
	snd_soc_dapm_enable_pin(dapm, "HPOL");
	snd_soc_dapm_enable_pin(dapm, "HPOR");
	snd_soc_dapm_sync(dapm);
	
	return ret;
}

static int hc18xx_rt5660_hw_params(struct snd_pcm_substream *substream,
				  struct snd_pcm_hw_params *params)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *codec_dai = rtd->codec_dai;
	int ret;

	pr_info("fn: %s\n", __func__);

	ret = snd_soc_dai_set_sysclk(codec_dai, 1, SYSCLK_RATE, SND_SOC_CLOCK_IN); // 1: RT5660_SCLK_S_PLL1
	if (ret < 0) {
		pr_info("[%s] set sysclk failed\n", DRIVER_NAME);
		return ret;
	}
	ret = snd_soc_dai_set_pll(codec_dai, 0, 0, MCLK_RATE, SYSCLK_RATE); // source 0: RT5660_PLL1_S_MCLK
	if (ret < 0) {
		pr_info("[%s] set pll failed\n", DRIVER_NAME);
		return ret;
	}

	return 0;
}

static struct snd_soc_ops hc18xx_rt5660_ops = {
	.hw_params = hc18xx_rt5660_hw_params,
};

static struct snd_soc_dai_link hc18xx_dais[] = {
	{
		.name           = DRIVER_NAME,
		.stream_name    = DRIVER_NAME,
		.codec_dai_name = "rt5660-aif1",
		.init           = hc18xx_rt5660_init,
		.dai_fmt        = SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_NB_NF,
		.ops            = &hc18xx_rt5660_ops,
	},
	{
		.name           = "HC18XX-ENC",
		.stream_name    = "HC18XX-ENC",
		.codec_name     = "snd-soc-dummy",
		.codec_dai_name = "snd-soc-dummy-dai",
		.dai_fmt        = SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_NB_NF,
	},
	{
		.name           = "HC18XX-DEC",
		.stream_name    = "HC18XX-DEC",
		.codec_name     = "snd-soc-dummy",
		.codec_dai_name = "snd-soc-dummy-dai",
		.dai_fmt        = SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_NB_NF,
	},
};

static struct snd_soc_card hc18xx = {
	.name = "hc18xx-rt5660",
	.owner = THIS_MODULE,
	.dai_link = hc18xx_dais,
	.num_links = ARRAY_SIZE(hc18xx_dais),

	.dapm_widgets = hc18xx_rt5660_dapm_widgets,
	.num_dapm_widgets = ARRAY_SIZE(hc18xx_rt5660_dapm_widgets),
	.dapm_routes = hc18xx_audio_map,
	.num_dapm_routes = ARRAY_SIZE(hc18xx_audio_map),
};

static int hc18xx_rt5660_probe(struct platform_device *pdev)
{
	struct device_node *np;
	struct device_node *codec_np, **cpu_np;
	struct snd_soc_card *card;
	struct device *dev;
	uint32_t i2s_clk_src;
	int ret;
	int i;

	pr_debug("fn: %s\n", __func__);

	np = pdev->dev.of_node;
	dev = &pdev->dev;
	card = &hc18xx;
	card->dev = dev;

	if (!np) {
		dev_err(dev,"cannot find the device.");
		return -ENODEV;
	}

	if (of_property_read_u32(np, "augentix,i2s-clk-src", &i2s_clk_src)) {
		dev_err(dev, "cannot get i2s clock source (default: codec as master).");
		for (i = 0; i < ARRAY_SIZE(hc18xx_dais); i++) {
			hc18xx_dais[i].dai_fmt |= SND_SOC_DAIFMT_CBM_CFM;
		}
	} else {
		for (i = 0; i < ARRAY_SIZE(hc18xx_dais); i++) {
			hc18xx_dais[i].dai_fmt |= i2s_clk_src;
		}
	}

	ret = snd_soc_of_parse_card_name(card, "augentix,audio-model");
	if (ret) {
		dev_err(dev, "cannot get name of the sound card");
		return ret;
	}

	/* request device node of codec device and CPU DAI */
	codec_np = of_parse_phandle(np, "augentix,audio-codec", 0);
	if (!codec_np) {
		dev_err(dev, "cannot find codec node info");
		goto codec_np_err;
	}
	hc18xx_dais[0].codec_of_node = codec_np;
	of_node_put(codec_np);

	cpu_np = devm_kzalloc(dev, ARRAY_SIZE(hc18xx_dais) *
		       sizeof(struct device_node**), GFP_KERNEL);

	for (i = 0; i < ARRAY_SIZE(hc18xx_dais); i++) {
		cpu_np[i] = of_parse_phandle(np, "augentix,audio-component", i);
		if (!cpu_np[i]) {
			dev_err(dev, "cannot find CPU-side DAI %d info", i);
			ret = i;
			goto cpu_np_err;
		}
		hc18xx_dais[i].cpu_of_node = cpu_np[i];
	}
	hc18xx_dais[0].platform_of_node = cpu_np[0];

	card->dev = dev;
	platform_set_drvdata(pdev, card);

	ret = devm_snd_soc_register_card(dev, card);
	if (ret)
	{
		dev_err(dev, "snd_soc_register_card failed (error: %d)\n",
			ret);
		goto cpu_np_err;
	}

	pr_info("ASoC: %s audio probed.\n", DRIVER_NAME);
	return 0;

	/* release the device node */
cpu_np_err:
	for (i = 0; i <= ret; i++) {
		of_node_put(cpu_np[i]);
	}
codec_np_err:
	of_node_put(codec_np);
	return -EINVAL;
}

static int hc18xx_rt5660_remove(struct platform_device *pdev)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(hc18xx_dais); i++) {
		of_node_put((struct device_node *)hc18xx_dais[i].cpu_of_node);
		if (strncmp(hc18xx_dais[i].codec_dai_name, "snd-soc-dummy-dai",
					  strlen(hc18xx_dais[i].codec_dai_name)) != 0) {
			of_node_put((struct device_node *)hc18xx_dais[i].codec_of_node);
		}
	}
	return 0;
}

static const struct of_device_id hc18xx_rt5660_of_ids[] = {
	{.compatible = "augentix,hc18xx-rt5660"},
	{}
};
MODULE_DEVICE_TABLE(of, hc18xx_rt5660_of_ids);

static struct platform_driver hc18xx_rt5660_driver = {
	.probe = hc18xx_rt5660_probe,
	.remove = hc18xx_rt5660_remove,
	.driver = {
		.name = "hc18xx-rt5660",
		.owner = THIS_MODULE,
		.of_match_table = hc18xx_rt5660_of_ids,
	},
};
module_platform_driver(hc18xx_rt5660_driver);

MODULE_DESCRIPTION("Augentix HC18xx-RT5660 Audio");
MODULE_AUTHOR("Henry Liu <henry.liu@augentix.com>");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:hc18xx-rt5660");
