#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/of.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/pcm_params.h>
#include <sound/soc.h>

#if 0
static const struct snd_soc_dapm_widget hc18xx_codec_dapm_widgets[] = {
SND_SOC_DAPM_MIC("Mic 0", NULL),
SND_SOC_DAPM_MIC("Mic 1", NULL),
SND_SOC_DAPM_MIC("Mic 2", NULL),
};

static const struct snd_soc_dapm_route hc18xx_audio_map[] = {
	{"MICIN_CH0", NULL, "Mic 0"},
	{"MICIN_CH1", NULL, "Mic 1"},
	{"MICIN_CH2", NULL, "Mic 2"},
};
#endif

static int hc18xx_adc_startup(struct snd_pcm_substream *substream)
{
#if 0
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dapm_context *dapm = &rtd->card->dapm;
#endif
	pr_debug("fn: %s\n", __func__);
#if 0
	snd_soc_dapm_enable_pin(dapm, "Mic 0");
	snd_soc_dapm_enable_pin(dapm, "Mic 1");
	snd_soc_dapm_enable_pin(dapm, "Mic 2");
	snd_soc_dapm_sync(dapm);
#endif
	return 0;
}

static struct snd_soc_ops hc18xx_adc_ops = {
	.startup   = hc18xx_adc_startup,
};


static struct snd_soc_dai_link hc18xx_dais[] = {
	{
		.name           = "HC18XX-ADC",
		.stream_name    = "HC18XX-ADC",
		.codec_dai_name = "hc18xx-adc-dai",
		.dai_fmt        = SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_NB_NF |
			              SND_SOC_DAIFMT_CBS_CFS,
		.ops            = &hc18xx_adc_ops,
	},
	{
		.name           = "HC18XX-ENC",
		.stream_name    = "HC18XX-ENC",
		.codec_name     = "snd-soc-dummy",
		.codec_dai_name = "snd-soc-dummy-dai",
		.dai_fmt        = SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_NB_NF |
			              SND_SOC_DAIFMT_CBS_CFS,
	},
};

static struct snd_soc_card hc18xx = {
	.name = "hc18xx-adc",
	.owner = THIS_MODULE,
	.dai_link = hc18xx_dais,
	.num_links = ARRAY_SIZE(hc18xx_dais),
#if 0
	.dapm_widgets = hc18xx_codec_dapm_widgets,
	.num_dapm_widgets = ARRAY_SIZE(hc18xx_codec_dapm_widgets),
	.dapm_routes = hc18xx_audio_map,
	.num_dapm_routes = ARRAY_SIZE(hc18xx_audio_map),
#endif
};

static int hc18xx_audio_probe(struct platform_device *pdev)
{
	struct device_node *np;
	struct device_node *codec_np, **cpu_np;
	struct snd_soc_card *card;
	struct device *dev;
	int ret;
	int i;

	pr_debug("fn: %s\n", __func__);

	np = pdev->dev.of_node;
	dev = &pdev->dev;
	card = &hc18xx;
	card->dev = dev;

	if (!np) {
		dev_err(dev,"cannot find the device.");
		return -ENODEV;
	}

	/* request device node of codec device and CPU DAI */
	codec_np = of_parse_phandle(np, "augentix,audio-codec", 0);
	if (!codec_np) {
		dev_err(dev, "cannot find codec node info");
		goto codec_np_err;
	}
	hc18xx_dais[0].codec_of_node = codec_np;
	of_node_put(codec_np);

	cpu_np = devm_kzalloc(dev, ARRAY_SIZE(hc18xx_dais) *
		       sizeof(struct device_node**), GFP_KERNEL);

	for (i = 0; i < ARRAY_SIZE(hc18xx_dais); i++) {
		cpu_np[i] = of_parse_phandle(np, "augentix,audio-component", i);
		if (!cpu_np[i]) {
			dev_err(dev, "cannot find CPU-side DAI %d info", i);
			ret = i;
			goto cpu_np_err;
		}
		hc18xx_dais[i].cpu_of_node = cpu_np[i];
	}
	hc18xx_dais[0].platform_of_node = cpu_np[0];

	card->dev = dev;
	platform_set_drvdata(pdev, card);

	ret = devm_snd_soc_register_card(dev, card);
	if (ret)
	{
		dev_err(dev, "snd_soc_register_card failed (error: %d)\n",
			ret);
		goto cpu_np_err;
	}

	pr_info("ASoC: HC18XX audio probed.\n");
	return 0;

	/* release the device node */
cpu_np_err:
	for (i = 0; i <= ret; i++) {
		of_node_put(cpu_np[i]);
	}
codec_np_err:
	of_node_put(codec_np);
	return -EINVAL;
}

static int hc18xx_audio_remove(struct platform_device *pdev)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(hc18xx_dais); i++) {
		of_node_put((struct device_node *)hc18xx_dais[i].cpu_of_node);
		if (strncmp(hc18xx_dais[i].codec_dai_name, "snd-soc-dummy-dai",
					  strlen(hc18xx_dais[i].codec_dai_name)) != 0) {
			of_node_put((struct device_node *)hc18xx_dais[i].codec_of_node);
		}
	}
	return 0;
}

static const struct of_device_id hc18xx_audio_of_ids[] = {
	{.compatible = "augentix,hc18xx-adc"},
	{}
};
MODULE_DEVICE_TABLE(of, hc18xx_audio_of_ids);

static struct platform_driver hc18xx_audio_driver = {
	.probe = hc18xx_audio_probe,
	.remove = hc18xx_audio_remove,
	.driver = {
		.name = "hc18xx-adc",
		.owner = THIS_MODULE,
		.of_match_table = hc18xx_audio_of_ids,
	},
};
module_platform_driver(hc18xx_audio_driver);

MODULE_DESCRIPTION("Augentix HC18xx-ADC Audio");
MODULE_AUTHOR("Henry Liu <henry.liu@augentix.com>");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:hc18xx-adc");
