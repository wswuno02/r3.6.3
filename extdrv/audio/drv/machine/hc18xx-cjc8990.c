#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/of.h>
#include <linux/gpio.h>

#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/pcm_params.h>
#include <sound/soc.h>

#define DRIVER_NAME "HC18XX-CJC8990"

struct mach_priv {
	int spk_amp_pol;
	int spk_amp_gpio;
};

static int spk_amp_event(struct snd_soc_dapm_widget *w,
				struct snd_kcontrol *k, int event)
{
	struct mach_priv *drvdata = snd_soc_card_get_drvdata(w->dapm->card);
	int on = !SND_SOC_DAPM_EVENT_OFF(event);

	pr_debug("%s: %s\n", __func__, on == 0 ? "OFF" : "ON");

	if (drvdata->spk_amp_pol >= 0 && drvdata->spk_amp_gpio >= 0) {
		gpio_set_value(drvdata->spk_amp_gpio, on ^ drvdata->spk_amp_pol);
	}

	return 0;
}

static const struct snd_soc_dapm_widget hc18xx_cjc8990_dapm_widgets[] = {
SND_SOC_DAPM_SPK("SPK", spk_amp_event),
SND_SOC_DAPM_MIC("MIC", NULL),
};

static const struct snd_soc_dapm_route hc18xx_audio_map[] = {
	{"SPK", NULL, "OUT"},
	{"MICIN", NULL, "MIC"},
};

static int hc18xx_cjc8990_init(struct snd_soc_pcm_runtime *rtd)
{
	struct snd_soc_dapm_context *dapm = &rtd->card->dapm;

	pr_info("%s\n", __func__);

	snd_soc_dapm_enable_pin(dapm, "SPK");
	snd_soc_dapm_sync(dapm);

	return 0;
}

static struct snd_soc_dai_link hc18xx_dais[] = {
	{
		.name           = DRIVER_NAME,
		.stream_name    = DRIVER_NAME,
		.codec_dai_name = "cjc8990-dai",
		.init           = hc18xx_cjc8990_init,
		.dai_fmt        = SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_NB_NF,
	},
	{
		.name           = "HC18XX-ENC",
		.stream_name    = "HC18XX-ENC",
		.codec_name     = "snd-soc-dummy",
		.codec_dai_name = "snd-soc-dummy-dai",
		.dai_fmt        = SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_NB_NF,
	},
	{
		.name           = "HC18XX-DEC",
		.stream_name    = "HC18XX-DEC",
		.codec_name     = "snd-soc-dummy",
		.codec_dai_name = "snd-soc-dummy-dai",
		.dai_fmt        = SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_NB_NF,
	},
};

static struct snd_soc_card hc18xx = {
	.name = "hc18xx-cjc8990",
	.owner = THIS_MODULE,
	.dai_link = hc18xx_dais,
	.num_links = ARRAY_SIZE(hc18xx_dais),
	.dapm_widgets = hc18xx_cjc8990_dapm_widgets,
	.num_dapm_widgets = ARRAY_SIZE(hc18xx_cjc8990_dapm_widgets),
	.dapm_routes = hc18xx_audio_map,
	.num_dapm_routes = ARRAY_SIZE(hc18xx_audio_map),
};

static int hc18xx_cjc8990_probe(struct platform_device *pdev)
{
	struct mach_priv *priv;
	struct device_node *np;
	struct device_node *codec_np, **cpu_np;
	struct snd_soc_card *card;
	struct device *dev;
	uint32_t i2s_clk_src;
	int ret;
	int i;

	pr_debug("fn: %s\n", __func__);

	np = pdev->dev.of_node;
	dev = &pdev->dev;
	card = &hc18xx;
	card->dev = dev;

	if (!np) {
		dev_err(dev,"cannot find the device.");
		return -ENODEV;
	}

	if (of_property_read_u32(np, "augentix,i2s-clk-src", &i2s_clk_src)) {
		dev_err(dev, "cannot get i2s clock source (default: codec as master).");
		for (i = 0; i < ARRAY_SIZE(hc18xx_dais); i++) {
			hc18xx_dais[i].dai_fmt |= SND_SOC_DAIFMT_CBM_CFM;
		}
	} else {
		for (i = 0; i < ARRAY_SIZE(hc18xx_dais); i++) {
			hc18xx_dais[i].dai_fmt |= i2s_clk_src;
		}
	}

	ret = snd_soc_of_parse_card_name(card, "augentix,audio-model");
	if (ret) {
		dev_err(dev, "cannot get name of the sound card");
		return ret;
	}

	/* request device node of codec device and CPU DAI */
	codec_np = of_parse_phandle(np, "augentix,audio-codec", 0);
	if (!codec_np) {
		dev_err(dev, "cannot find codec node info");
		goto codec_np_err;
	}
	hc18xx_dais[0].codec_of_node = codec_np;
	of_node_put(codec_np);

	cpu_np = devm_kzalloc(dev, ARRAY_SIZE(hc18xx_dais) *
		       sizeof(struct device_node**), GFP_KERNEL);

	for (i = 0; i < ARRAY_SIZE(hc18xx_dais); i++) {
		cpu_np[i] = of_parse_phandle(np, "augentix,audio-component", i);
		if (!cpu_np[i]) {
			dev_err(dev, "cannot find CPU-side DAI %d info", i);
			ret = i;
			goto cpu_np_err;
		}
		hc18xx_dais[i].cpu_of_node = cpu_np[i];
	}
	hc18xx_dais[0].platform_of_node = cpu_np[0];

	card->dev = dev;
	platform_set_drvdata(pdev, card);

	/* Private data allocate */
	priv = devm_kzalloc(&pdev->dev, sizeof(*priv), GFP_KERNEL);
	if (!priv)
		return -ENOMEM;

	if (of_property_read_u32_index(np, "augentix,spk-amp", 0, &priv->spk_amp_pol) == 0) {
		if (of_property_read_u32_index(np, "augentix,spk-amp", 1, &priv->spk_amp_gpio) == 0) {
			gpio_direction_output(priv->spk_amp_gpio, 1);
			gpio_set_value(priv->spk_amp_gpio, priv->spk_amp_pol);
		} else {
			priv->spk_amp_gpio = -1;
		}
	} else {
		priv->spk_amp_pol = -1;
		priv->spk_amp_gpio = -1;
	}

	snd_soc_card_set_drvdata(card, priv);

	ret = devm_snd_soc_register_card(dev, card);
	if (ret)
	{
		dev_err(dev, "snd_soc_register_card failed (error: %d)\n",
			ret);
		goto cpu_np_err;
	}

	pr_info("ASoC: %s audio probed.\n", DRIVER_NAME);
	return 0;

	/* release the device node */
cpu_np_err:
	for (i = 0; i <= ret; i++) {
		of_node_put(cpu_np[i]);
	}
codec_np_err:
	of_node_put(codec_np);
	return -EINVAL;
}

static int hc18xx_cjc8990_remove(struct platform_device *pdev)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(hc18xx_dais); i++) {
		of_node_put((struct device_node *)hc18xx_dais[i].cpu_of_node);
		if (strncmp(hc18xx_dais[i].codec_dai_name, "snd-soc-dummy-dai",
					  strlen(hc18xx_dais[i].codec_dai_name)) != 0) {
			of_node_put((struct device_node *)hc18xx_dais[i].codec_of_node);
		}
	}
	return 0;
}

static const struct of_device_id hc18xx_cjc8990_of_ids[] = {
	{.compatible = "augentix,hc18xx-cjc8990"},
	{}
};
MODULE_DEVICE_TABLE(of, hc18xx_cjc8990_of_ids);

static struct platform_driver hc18xx_cjc8990_driver = {
	.probe = hc18xx_cjc8990_probe,
	.remove = hc18xx_cjc8990_remove,
	.driver = {
		.name = "hc18xx-cjc8990",
		.owner = THIS_MODULE,
		.of_match_table = hc18xx_cjc8990_of_ids,
	},
};
module_platform_driver(hc18xx_cjc8990_driver);

MODULE_DESCRIPTION("Augentix HC18xx-CJC8990 Audio");
MODULE_AUTHOR("Henry Liu <henry.liu@augentix.com>");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:hc18xx-cjc8990");
