#ifndef __CJC8990_H_
#define __CJC8990_H_

/********** Configure Start **********/

/* Supported MCLK:
 * 11289600, 12288000, 12000000 */
/* 22579200, 24576000, 24000000 */
#define CJC8990_MCLK_FREQUENCY 12000000

/********** Configure End ***********/

#define CJC8990_CLOCKING_MODE_USB    1
#define CJC8990_CLOCKING_MODE_NORMAL 0
#define CJC8990_SR_NOT_SUPPORT       0xFF

#if CJC8990_MCLK_FREQUENCY == 12000000 || CJC8990_MCLK_FREQUENCY == 24000000
#define CJC8990_R8_USB        CJC8990_CLOCKING_MODE_USB
#define CJC8990_R8_SR_8000    0x06
#define CJC8990_R8_SR_11025   0x19
#define CJC8990_R8_SR_16000   0x0A
#define CJC8990_R8_SR_22050   0x1B
#define CJC8990_R8_SR_32000   0x0C
#define CJC8990_R8_SR_44100   0x11
#define CJC8990_R8_SR_48000   0x00
#define CJC8990_R8_BCM_L      1
#elif CJC8990_MCLK_FREQUENCY == 11289600 || CJC8990_MCLK_FREQUENCY == 22579200
#define CJC8990_R8_USB        CJC8990_CLOCKING_MODE_NORMAL
#define CJC8990_R8_SR_8000    0x16
#define CJC8990_R8_SR_11025   0x18
#define CJC8990_R8_SR_16000   CJC8990_SR_NOT_SUPPORT
#define CJC8990_R8_SR_22050   0x1A
#define CJC8990_R8_SR_32000   CJC8990_SR_NOT_SUPPORT
#define CJC8990_R8_SR_44100   0x10
#define CJC8990_R8_SR_48000   CJC8990_SR_NOT_SUPPORT
#define CJC8990_R8_BCM_L      0
#elif CJC8990_MCLK_FREQUENCY == 12288000 || CJC8990_MCLK_FREQUENCY == 24576000
#define CJC8990_R8_USB        CJC8990_CLOCKING_MODE_NORMAL
#define CJC8990_R8_SR_8000    0x06
#define CJC8990_R8_SR_11025   CJC8990_SR_NOT_SUPPORT
#define CJC8990_R8_SR_16000   0x0A
#define CJC8990_R8_SR_22050   CJC8990_SR_NOT_SUPPORT
#define CJC8990_R8_SR_32000   0x0C
#define CJC8990_R8_SR_44100   CJC8990_SR_NOT_SUPPORT
#define CJC8990_R8_SR_48000   0x00
#define CJC8990_R8_BCM_L      0
#else
#error "CJC8990_MCLK_FREQUENCY not support"
#endif

#if CJC8990_MCLK_FREQUENCY > 20000000
#define CJC8990_R8_CLKDIV2    1
#else
#define CJC8990_R8_CLKDIV2    0
#endif

#define CJC8990_Write_Address                      0x36
#define CJC8990_Read_Address                       0x37

/* register addr */

#define CJC8990_REG_NUM                            0x100  // Total number of registers

#define CJC8990_R0_LEFT_INPUT_VOLUME               0x01   // Audio input left channel volume; R/W
#define CJC8990_R2_LOUT1_VOLUME                    0x05   // Audio output letf channel1 volume; R/W
#define CJC8990_R3_ROUT1_VOLUME                    0x07   // Audio output right channel1 volume; R/W
#define CJC8990_R5_ADC_DAC_CONTROL                 0x0A   // ADC and DAC CONTROL; R/W
#define CJC8990_R7_AUDIO_INTERFACE                 0x0E   // Digital Audio interface format; R/W
#define CJC8990_R8_SAMPLE_RATE                     0x10   // Clock and Sample rate control; R/W
#define CJC8990_R10_LEFT_DAC_VOLUME                0x15   // Left channel DAC digital volume; R/W
#define CJC8990_R11_RIGHT_DAC_VOLUME               0x17   // Right channel DAC digital volume; R/W
#define CJC8990_R12_BASS_CONTROL                   0x18   // BASS control; R/W
#define CJC8990_R13_TREBLE_CONTROL                 0x1A   // Treble control; R/W
#define CJC8990_R15_RESET                          0x1E   // RESET  ; R/W
#define CJC8990_R16_3D_CONTROL                     0x20   // 3D control; R/W
#define CJC8990_R17_ALC1_CONTROL                   0x22   // ALC1; R/W
#define CJC8990_R18_ALC2_CONTROL                   0x24   // ALC2; R/W
#define CJC8990_R19_ALC3_CONTROL                   0x26   // ALC3; R/W
#define CJC8990_R20_NOISE_GATE_CONTROL             0x28   // NOISE_GATE_CONTROL; R/W
#define CJC8990_R21_LEFT_ADC_VOLUME                0x2B   // Left ADC digital volume; R/W
#define CJC8990_R22_RIGHT_ADC_VOLUME               0x2D   // Right ADC digital volume; R/W
#define CJC8990_R23_ADDITIONAL_CONTROL1            0x2E   // Additional control; R/W
#define CJC8990_R24_ADDITIONAL_CONTROL2            0x30   // Additional control; R/W
#define CJC8990_R27_ADDITIONAL_CONTROL3            0x36   // Additional control; R/W
#define CJC8990_R31_ADC_INPUT_MODE                 0x3E   // ADC input mode; R/W
#define CJC8990_R32_ADCL_SIGNAL_PATH               0x40   // ADC signal path; R/W
#define CJC8990_R33_MIC                            0x42   // MIC
#define CJC8990_R34_AUX                            0x44   // AUX; R/W
#define CJC8990_R35_LEFT_OUT_MIX2_L                0x46   // Left out Mix (2); R/W
#define CJC8990_R35_LEFT_OUT_MIX2_H                0X47   //
#define CJC8990_R36_RIGHT_OUT_MIX1_L               0x48   // Right out Mix (1; R/W
#define CJC8990_R36_RIGHT_OUT_MIX1_H               0x49   // Left DAC to Right mixer enable; R/W
#define CJC8990_R37_ADC_PDN                        0x4A   // Adc_pdn se; R/W
#define CJC8990_R43_LOW_POWER_PLAYBACK             0x86   // Low power playback; R/W
#define CJC8990_R25_PWR_MGMT1_L                    0x32   // Power management1 and VMIDSEL; R/W
#define CJC8990_R25_PWR_MGMT1_H                    0x33   // Power management1 and VMIDSEL; R/W
#define CJC8990_R26_PWR_MGMT2_L                    0x34   // Power management2 and DAC left Power down; R/W
#define CJC8990_R26_PWR_MGMT2_H                    0x35   // Power management2 and DAC right Power up; R/W

/* Register map */
#define CJC8990_R0_LEFT_INPUT_VOLUME_DEF 0x17
#define CJC8990_R2_LOUTVOL_OFFSET    0
#define CJC8990_R2_LOUTVOL_MASK      0x7F
#define CJC8990_R2_LOUTVOL_DEF       0x79
#define CJC8990_R2_LOZC_OFFSET       7
#define CJC8990_R2_LOZC_MASK         0x1
#define CJC8990_R2_LOZC_DEF          0x0
#define CJC8990_R2_LOUT1_VOLUME_DEF  ((CJC8990_R2_LOZC_DEF << CJC8990_R2_LOZC_OFFSET) |\
		                      (CJC8990_R2_LOUTVOL_DEF << CJC8990_R2_LOUTVOL_OFFSET))
#define CJC8990_R3_ROUTVOL_OFFSET    0
#define CJC8990_R3_ROUTVOL_MASK      0x7F
#define CJC8990_R3_ROUTVOL_DEF       0x79
#define CJC8990_R3_ROZC_OFFSET       7
#define CJC8990_R3_ROZC_MASK         0x1
#define CJC8990_R3_ROZC_DEF          0x0
#define CJC8990_R3_ROUT1_VOLUME_DEF  ((CJC8990_R3_ROZC_DEF << CJC8990_R3_ROZC_OFFSET) |\
		                      (CJC8990_R3_ROUTVOL_DEF << CJC8990_R3_ROUTVOL_OFFSET))
#define CJC8990_R8_USB_OFFSET        0
#define CJC8990_R8_USB_MASK          0x1
#define CJC8990_R8_SR_OFFSET         1
#define CJC8990_R8_SR_MASK           0x1F
#define CJC8990_R8_CLKDIV2_OFFSET    6
#define CJC8990_R8_CLKDIV2_MASK      0x1
#define CJC8990_R8_BCM_L_OFFSET      7
#define CJC8990_R8_BCM_L_MASK        0x1
#define CJC8990_R10_LEFT_DAC_VOLUME_DEF  0xFF
#define CJC8990_R11_RIGHT_DAC_VOLUME_DEF 0xFF
#define CJC8990_R21_LEFT_ADC_VOLUME_DEF  0xC3


#endif /* __CJC8990_H_ */
