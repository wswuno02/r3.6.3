#include <linux/module.h>
#include <linux/init.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/gpio.h>
#include <sound/pcm.h>
#include <sound/pcm_params.h>
#include <sound/soc.h>
#include <sound/soc-dapm.h>
#include <sound/initval.h>
#include <sound/tlv.h>
#include <linux/ioctl.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/mutex.h>
#include <linux/firmware.h>
#include <linux/vmalloc.h>
#include "cjc8990.h"

#define AO_VOL_OFFSET 63

struct cjc8990_priv {
	struct snd_soc_codec codec;
	struct i2c_client *i2c;
	int inputl_analog_gain;
	int outputl_analog_gain;
	int outputr_analog_gain;
	int inputl_digital_gain;
	int outputl_digital_gain;
	int outputr_digital_gain;
	int mic_boost;
};

//*************************************************************************
// CJC8990 Wirte register operation
//
#define CJC8990_WriteReg(a, d) cjc8990_write_reg(codec, a, d)
static int cjc8990_write_reg(struct snd_soc_codec *codec, unsigned char reg_addr, unsigned char reg_data)
{
	struct cjc8990_priv *cjc8990 = snd_soc_codec_get_drvdata(codec);
	int ret = -1;
	unsigned char tx[2];
	size_t len = 2;

	tx[0] = reg_addr;
	tx[1] = reg_data;

	pr_debug("[CJC8990] %s: i2c send (%x, %x)\n", __func__, reg_addr, reg_data);
	ret = i2c_master_send(cjc8990->i2c, tx, len);

	if (ret < 0) {
		pr_err("[CJC8990] %s: I2C write (0x%X, 0x%X) failed (%d)\n", __func__, reg_addr, reg_data, ret);
	}

	return ret;
}

//*************************************************************************
// CJC8990 Read register operation
//
#if 0
static unsigned char CJC8990_ReadReg(unsigned char Reg_Addr)
{
	return 0;
}
#endif

//*************************************************************************
// CJC8990 Register Initial Value
//
struct CJC8990_reg{
	unsigned char reg_index;
	unsigned char reg_value;
};

static struct CJC8990_reg cjc8990_usb_mode_reg[] = {
	{CJC8990_R0_LEFT_INPUT_VOLUME,       CJC8990_R0_LEFT_INPUT_VOLUME_DEF},   // Audio input left channel volume; R/W
	{CJC8990_R2_LOUT1_VOLUME,            CJC8990_R2_LOUT1_VOLUME_DEF},   // Audio output letf channel1 volume; R/W
	{CJC8990_R3_ROUT1_VOLUME,            CJC8990_R3_ROUT1_VOLUME_DEF},   // Audio output right channel1 volume; R/W
	{CJC8990_R5_ADC_DAC_CONTROL,         0x00},   // ADC and DAC CONTROL; R/W
	{CJC8990_R7_AUDIO_INTERFACE,         0x42},   // Digital Audio interface format; R/W
//	{CJC8990_R8_SAMPLE_RATE,             0x4D},   // Clock and Sample rate control; R/W
	{CJC8990_R10_LEFT_DAC_VOLUME,        CJC8990_R10_LEFT_DAC_VOLUME_DEF},   // Left channel DAC digital volume; R/W
	{CJC8990_R11_RIGHT_DAC_VOLUME,       CJC8990_R11_RIGHT_DAC_VOLUME_DEF},   // right channel DAC digital volume; R/W
	{CJC8990_R12_BASS_CONTROL,           0x0f},   // BASS control; R/W
	{CJC8990_R13_TREBLE_CONTROL,         0x0f},   // Treble control; R/W
	{CJC8990_R16_3D_CONTROL,             0x00},   // 3D control; R/W
	{CJC8990_R17_ALC1_CONTROL,           0x7b},   // ALC1 control; R/W
	{CJC8990_R18_ALC2_CONTROL,           0x00},   // ACL2 control; R/W
	{CJC8990_R19_ALC3_CONTROL,           0x32},   // ALC3 control; R/W
	{CJC8990_R20_NOISE_GATE_CONTROL,     0x00},   // noise gate; R/W
	{CJC8990_R21_LEFT_ADC_VOLUME,        CJC8990_R21_LEFT_ADC_VOLUME_DEF},   // Left ADC digital volume; R/W
	{CJC8990_R23_ADDITIONAL_CONTROL1,    0x00},   // Additional control 1; R/W
	{CJC8990_R24_ADDITIONAL_CONTROL2,    0x04},   // Additional control 2; R/W
	{CJC8990_R27_ADDITIONAL_CONTROL3,    0x00},   // Additional control 3; R/W
	{CJC8990_R31_ADC_INPUT_MODE,         0x00},   // ADC input mode; R/W
	{CJC8990_R32_ADCL_SIGNAL_PATH,       0x00},   // ADC signal path control; R/W
	{CJC8990_R33_MIC,                    0x0a},   // mic R/W
	{CJC8990_R34_AUX,                    0x0a},   // aux R/W
	{CJC8990_R35_LEFT_OUT_MIX2_H,        0x00},   // Left out Mix (2) ; R/W
	{CJC8990_R36_RIGHT_OUT_MIX1_L,       0x80},   // Right out Mix (1; R/W
	{CJC8990_R37_ADC_PDN,                0x00},   // Adc_pdn sel; R/W
	{CJC8990_R43_LOW_POWER_PLAYBACK,     0x00},   // Low power playback; R/W
	{CJC8990_R25_PWR_MGMT1_H,            0xC0},   // Power management1 and VMIDSEL; R/W
	{CJC8990_R26_PWR_MGMT2_L,            0x00},   // Power management2 and DAC left power down; R/W;
};

static int cjc8990_get_mic_boost(struct snd_kcontrol *kcontrol,
		struct snd_ctl_elem_value *ucontrol) {
	struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);
	struct cjc8990_priv *drvdata = snd_soc_codec_get_drvdata(codec);

	ucontrol->value.integer.value[0] = drvdata->mic_boost;

	return 0;
};

static int cjc8990_set_mic_boost(struct snd_kcontrol *kcontrol,
		struct snd_ctl_elem_value *ucontrol) {
	struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);
	struct cjc8990_priv *drvdata = snd_soc_codec_get_drvdata(codec);
	int ret = 0;

	drvdata->mic_boost = ucontrol->value.integer.value[0];
	ret = CJC8990_WriteReg(CJC8990_R32_ADCL_SIGNAL_PATH, drvdata->mic_boost << 4);

	return ret;
};

static int cjc8990_get_volume(struct snd_kcontrol *kcontrol,
		struct snd_ctl_elem_value *ucontrol) {
	struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);
	struct cjc8990_priv *drvdata = snd_soc_codec_get_drvdata(codec);
	struct soc_mixer_control *mc =
		(struct soc_mixer_control *)kcontrol->private_value;
	unsigned int reg = mc->reg;

	switch(reg) {
	case CJC8990_R0_LEFT_INPUT_VOLUME:
		ucontrol->value.integer.value[0] = drvdata->inputl_analog_gain;
		break;
	case CJC8990_R2_LOUT1_VOLUME:
		ucontrol->value.integer.value[0] = drvdata->outputl_analog_gain;
		if (snd_soc_volsw_is_stereo(mc)) {
			ucontrol->value.integer.value[1] = drvdata->outputr_analog_gain;
		}
		break;
	case CJC8990_R21_LEFT_ADC_VOLUME:
		ucontrol->value.integer.value[0] = drvdata->inputl_digital_gain;
		break;
	case CJC8990_R10_LEFT_DAC_VOLUME:
		ucontrol->value.integer.value[0] = drvdata->outputl_digital_gain;
		if (snd_soc_volsw_is_stereo(mc)) {
			ucontrol->value.integer.value[1] = drvdata->outputr_digital_gain;
		}
		break;
	default:
		break;
	}

	return 0;
}

static int cjc8990_set_volume(struct snd_kcontrol *kcontrol,
		struct snd_ctl_elem_value *ucontrol) {
	struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);
	struct cjc8990_priv *drvdata = snd_soc_codec_get_drvdata(codec);
	struct soc_mixer_control *mc =
		(struct soc_mixer_control *)kcontrol->private_value;
	unsigned int reg = mc->reg;
	unsigned int rreg = mc->rreg;
	int ret = 0;

	switch(reg) {
	case CJC8990_R0_LEFT_INPUT_VOLUME:
		drvdata->inputl_analog_gain = ucontrol->value.integer.value[0];
		ret = CJC8990_WriteReg(reg, drvdata->inputl_analog_gain);
		break;
	case CJC8990_R2_LOUT1_VOLUME:
		drvdata->outputl_analog_gain = ucontrol->value.integer.value[0];
		ret = CJC8990_WriteReg(reg, drvdata->outputl_analog_gain + AO_VOL_OFFSET);
		if (snd_soc_volsw_is_stereo(mc)) {
			drvdata->outputr_analog_gain = ucontrol->value.integer.value[1];
			ret = CJC8990_WriteReg(rreg, drvdata->outputr_analog_gain + AO_VOL_OFFSET);
		}
		break;
	case CJC8990_R21_LEFT_ADC_VOLUME:
		drvdata->inputl_digital_gain = ucontrol->value.integer.value[0];
		ret = CJC8990_WriteReg(reg, drvdata->inputl_digital_gain);
		break;
	case CJC8990_R10_LEFT_DAC_VOLUME:
		drvdata->outputl_digital_gain = ucontrol->value.integer.value[0];
		ret = CJC8990_WriteReg(reg, drvdata->outputl_digital_gain);
		if (snd_soc_volsw_is_stereo(mc)) {
			drvdata->outputr_digital_gain = ucontrol->value.integer.value[1];
			ret = CJC8990_WriteReg(rreg, drvdata->outputr_digital_gain);
		}
		break;
	default:
		break;
	}

	return ret;
}

static const DECLARE_TLV_DB_SCALE(in_analog_tlv, -1725, 75, 0);
static const DECLARE_TLV_DB_SCALE(out_analog_tlv, -5800, 100, 1);
static const DECLARE_TLV_DB_SCALE(in_digital_tlv, -9750, 50, 1);
static const DECLARE_TLV_DB_SCALE(out_digital_tlv, -12750, 50, 1);
static const char *cjc8990_mic_boost_texts[] = {"off", "13dB", "20dB", "29dB"};

static const struct soc_enum cjc8990_enum[] = {
	SOC_ENUM_SINGLE_EXT(ARRAY_SIZE(cjc8990_mic_boost_texts), cjc8990_mic_boost_texts),
};

static const struct snd_kcontrol_new cjc8990_snd_controls[] = {
SOC_DOUBLE_R_EXT_TLV("Digital Output Playback Volume", CJC8990_R10_LEFT_DAC_VOLUME, CJC8990_R11_RIGHT_DAC_VOLUME, 0, 255, 0, cjc8990_get_volume, cjc8990_set_volume, out_digital_tlv),
SOC_DOUBLE_R_EXT_TLV("Analog Output Playback Volume", CJC8990_R2_LOUT1_VOLUME, CJC8990_R3_ROUT1_VOLUME, \
		0, 64, 0, cjc8990_get_volume, cjc8990_set_volume, out_analog_tlv),
SOC_SINGLE_EXT_TLV("Digital Input Capture Volume", CJC8990_R21_LEFT_ADC_VOLUME, 0, 255, 0, cjc8990_get_volume, cjc8990_set_volume, in_digital_tlv),
SOC_SINGLE_EXT_TLV("Analog Input Capture Volume", CJC8990_R0_LEFT_INPUT_VOLUME, \
		0, 63, 0, cjc8990_get_volume, cjc8990_set_volume, in_analog_tlv),
SOC_ENUM_EXT("MIC Boost Capture Switch", cjc8990_enum[0], cjc8990_get_mic_boost, cjc8990_set_mic_boost),
};

static const struct snd_soc_dapm_widget cjc8990_dapm_widgets[] = {
	SND_SOC_DAPM_AIF_IN("AIFRX", "AIF Playback", 0, SND_SOC_NOPM, 0, 0),
	SND_SOC_DAPM_AIF_OUT("AIFTX", "AIF Capture", 0, SND_SOC_NOPM, 0, 0),

	/* Input */
	SND_SOC_DAPM_INPUT("MICIN"),
	SND_SOC_DAPM_ADC("ADC", NULL, SND_SOC_NOPM, 0, 0),

	/* Output */
	SND_SOC_DAPM_OUTPUT("OUT"),
	SND_SOC_DAPM_DAC("DAC", NULL, SND_SOC_NOPM, 0, 0),
};

static const struct snd_soc_dapm_route cjc8990_intercon[] = {
	{"AIFTX", NULL, "ADC"},
	{"DAC", NULL, "AIFRX"},
	{"ADC", NULL, "MICIN"},
	{"OUT", NULL, "DAC"},
};

static int cjc8990_hw_params(struct snd_pcm_substream *substream,
		struct snd_pcm_hw_params *params,
		struct snd_soc_dai *dai)
{
	struct snd_soc_codec *codec = dai->codec;
	struct cjc8990_priv *drvdata = snd_soc_codec_get_drvdata(codec);
	unsigned int rate;

	rate = params_rate(params);
	pr_debug("[CJC8990] %s: rate[%d]\n", __func__, rate);

#define R8_DATA(sr) (((CJC8990_R8_USB & CJC8990_R8_USB_MASK) << CJC8990_R8_USB_OFFSET) \
		| ((sr & CJC8990_R8_SR_MASK) << CJC8990_R8_SR_OFFSET) \
		| ((CJC8990_R8_CLKDIV2 & CJC8990_R8_CLKDIV2_MASK) << CJC8990_R8_CLKDIV2_OFFSET) \
		| ((CJC8990_R8_BCM_L & CJC8990_R8_BCM_L_MASK) << CJC8990_R8_BCM_L_OFFSET))
	switch (rate) {
	case 8000:
		CJC8990_WriteReg(CJC8990_R8_SAMPLE_RATE, R8_DATA(CJC8990_R8_SR_8000));
		break;
	case 11025:
		if (CJC8990_R8_SR_11025 > CJC8990_R8_SR_MASK) {
			return -EINVAL;
		}
		CJC8990_WriteReg(CJC8990_R8_SAMPLE_RATE, R8_DATA(CJC8990_R8_SR_11025));
		break;
	case 16000:
		if (CJC8990_R8_SR_16000 > CJC8990_R8_SR_MASK) {
			return -EINVAL;
		}
		CJC8990_WriteReg(CJC8990_R8_SAMPLE_RATE, R8_DATA(CJC8990_R8_SR_16000));
		break;
	case 22050:
		if (CJC8990_R8_SR_22050 > CJC8990_R8_SR_MASK) {
			return -EINVAL;
		}
		CJC8990_WriteReg(CJC8990_R8_SAMPLE_RATE, R8_DATA(CJC8990_R8_SR_22050));
		break;
	case 32000:
		if (CJC8990_R8_SR_32000 > CJC8990_R8_SR_MASK) {
			return -EINVAL;
		}
		CJC8990_WriteReg(CJC8990_R8_SAMPLE_RATE, R8_DATA(CJC8990_R8_SR_32000));
		break;
	case 44100:
		if (CJC8990_R8_SR_44100 > CJC8990_R8_SR_MASK) {
			return -EINVAL;
		}
		CJC8990_WriteReg(CJC8990_R8_SAMPLE_RATE, R8_DATA(CJC8990_R8_SR_44100));
		break;
	case 48000:
		if (CJC8990_R8_SR_48000 > CJC8990_R8_SR_MASK) {
			return -EINVAL;
		}
		CJC8990_WriteReg(CJC8990_R8_SAMPLE_RATE, R8_DATA(CJC8990_R8_SR_48000));
		break;
	default:
		break;
	}

	/* Workaround for Digital volume */
	CJC8990_WriteReg(CJC8990_R10_LEFT_DAC_VOLUME, drvdata->outputl_digital_gain);
	CJC8990_WriteReg(CJC8990_R11_RIGHT_DAC_VOLUME, drvdata->outputr_digital_gain);

	return 0;
}

static int cjc8990_startup(struct snd_pcm_substream *substream, struct snd_soc_dai *codec_dai)
{
	int ret = 0;
	struct snd_soc_codec *codec = codec_dai->codec;

	pr_debug("[CJC8990] %s\n", __func__);

	switch (substream->stream) {
	case SNDRV_PCM_STREAM_CAPTURE:
		ret = CJC8990_WriteReg(CJC8990_R25_PWR_MGMT1_H, 0xE8);
		break;
	case SNDRV_PCM_STREAM_PLAYBACK:
		ret = CJC8990_WriteReg(CJC8990_R26_PWR_MGMT2_H, 0xE0);
		break;
	default:
		break;
	}

	return ret;
}
static void cjc8990_shutdown(struct snd_pcm_substream *substream, struct snd_soc_dai *codec_dai)
{
	struct snd_soc_codec *codec = codec_dai->codec;

	pr_debug("[CJC8990] %s\n", __func__);

	switch (substream->stream) {
	case SNDRV_PCM_STREAM_CAPTURE:
		CJC8990_WriteReg(CJC8990_R25_PWR_MGMT1_H, 0xC0);
		break;
	case SNDRV_PCM_STREAM_PLAYBACK:
		CJC8990_WriteReg(CJC8990_R26_PWR_MGMT2_L, 0x00);
		break;
	default:
		break;
	}
}

static struct snd_soc_dai_ops cjc8990_dai_ops = {
	.hw_params = cjc8990_hw_params,
	.startup = cjc8990_startup,
	.shutdown = cjc8990_shutdown,
};

struct snd_soc_dai_driver cjc8990_dai[] = {
	{
		.name = "cjc8990-dai",
		.playback = {
		       .stream_name = "Playback",
		       .channels_min = 1,
		       .channels_max = 2,
		       .rates = SNDRV_PCM_RATE_8000_48000,
		       .formats = SNDRV_PCM_FMTBIT_S16_LE |
			          SNDRV_PCM_FMTBIT_S24_LE |
			          SNDRV_PCM_FMTBIT_S32_LE,
		},
		.capture = {
		       .stream_name = "Capture",
		       .channels_min = 1,
		       .channels_max = 1,
		       .rates = SNDRV_PCM_RATE_8000_48000,
		       .formats = SNDRV_PCM_FMTBIT_S16_LE |
			          SNDRV_PCM_FMTBIT_S24_LE |
			          SNDRV_PCM_FMTBIT_S32_LE,
		},
		.ops = &cjc8990_dai_ops,
	},
};

static void cjc8990_init_reg(struct snd_soc_codec *codec)
{
	int i = 0;

	pr_debug("[CJC8990] %s\n", __func__);

	for (i = 0; i < ARRAY_SIZE(cjc8990_usb_mode_reg); i++) {
		CJC8990_WriteReg(cjc8990_usb_mode_reg[i].reg_index, cjc8990_usb_mode_reg[i].reg_value);
	}
}

static int cjc8990_probe(struct snd_soc_codec *codec)
{
//	struct cjc8990_priv *cjc8990 = snd_soc_codec_get_drvdata(codec);

	pr_debug("[CJC8990] %s\n", __func__);

	cjc8990_init_reg(codec);

	return 0;
}

static int cjc8990_remove(struct snd_soc_codec *codec)
{
	return 0;
}

struct snd_soc_codec_driver cjc8990_driver = {
	.probe = cjc8990_probe,
	.remove = cjc8990_remove,
	.controls = cjc8990_snd_controls,
	.num_controls = ARRAY_SIZE(cjc8990_snd_controls),
	.dapm_widgets = cjc8990_dapm_widgets,
	.num_dapm_widgets = ARRAY_SIZE(cjc8990_dapm_widgets),
	.dapm_routes = cjc8990_intercon,
	.num_dapm_routes = ARRAY_SIZE(cjc8990_intercon),
};

static struct of_device_id cjc8990_i2c_dt_ids[] = {
	{ .compatible = "cjc8990"},
	{ }
};
MODULE_DEVICE_TABLE(of, cjc8990_i2c_dt_ids);

static int cjc8990_i2c_probe(struct i2c_client *i2c,
                            const struct i2c_device_id *id)
{
	struct cjc8990_priv *cjc8990;
	int ret=0;

	pr_debug("[CJC8990] %s: I2C probe.\n", __func__);

	cjc8990 = devm_kzalloc(&i2c->dev, sizeof(struct cjc8990_priv), GFP_KERNEL);
	if (cjc8990 == NULL) {
		return -ENOMEM;
	}

	i2c_set_clientdata(i2c, cjc8990);
	cjc8990->i2c = i2c;
	cjc8990->inputl_analog_gain = CJC8990_R0_LEFT_INPUT_VOLUME_DEF;
	cjc8990->outputl_analog_gain = CJC8990_R2_LOUTVOL_DEF - AO_VOL_OFFSET;
	cjc8990->outputr_analog_gain = CJC8990_R3_ROUTVOL_DEF - AO_VOL_OFFSET;
	cjc8990->inputl_digital_gain = CJC8990_R21_LEFT_ADC_VOLUME_DEF;
	cjc8990->outputl_digital_gain = CJC8990_R10_LEFT_DAC_VOLUME_DEF;
	cjc8990->outputr_digital_gain = CJC8990_R11_RIGHT_DAC_VOLUME_DEF;
	cjc8990->mic_boost = 0;

	ret = snd_soc_register_codec(&i2c->dev,
			&cjc8990_driver, &cjc8990_dai[0], ARRAY_SIZE(cjc8990_dai));
	if (ret < 0){
		devm_kfree(&i2c->dev, cjc8990);
		pr_debug("\t[CJC8990] %s: Register codec error(%d).\n", __func__, ret);
	}
	return ret;
}

static int cjc8990_i2c_remove(struct i2c_client *client)
{
	snd_soc_unregister_codec(&client->dev);

	return 0;
}

static const struct i2c_device_id cjc8990_i2c_id[] = {
	{ "cjc8990", 0 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, cjc8990_i2c_id);

static struct i2c_driver cjc8990_i2c_driver = {
	.driver = {
		.name = "cjc8990",
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(cjc8990_i2c_dt_ids),
	},
	.probe = cjc8990_i2c_probe,
	.remove = cjc8990_i2c_remove,
	.id_table = cjc8990_i2c_id,
};

static int __init cjc8990_modinit(void)
{
	return i2c_add_driver(&cjc8990_i2c_driver);
}
module_init(cjc8990_modinit);

static void __exit cjc8990_exit(void)
{
	i2c_del_driver(&cjc8990_i2c_driver);
}
module_exit(cjc8990_exit);

MODULE_DESCRIPTION("ASoC cjc8990 codec driver");
MODULE_LICENSE("GPL v2");
