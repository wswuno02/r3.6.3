/*
 * ak4637.c  --  audio driver for AK4637
 *
 * Copyright (C) 2015 Asahi Kasei Microdevices Corporation
 *  Author                Date        Revision
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *                      15/07/02	    1.0
 *                      16/01/22        1.1  kernel 3.18.25
 *                      16/05/25        1.2
 *                      16/09/26        1.3
 *                      18/03/13        1.4
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 *
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/gpio.h>
#include <sound/pcm.h>
#include <sound/pcm_params.h>
#include <sound/soc.h>
#include <sound/soc-dapm.h>
#include <sound/initval.h>
#include <sound/tlv.h>
#include <linux/ioctl.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

#include <linux/of_gpio.h> // '16/01/22
#include <linux/regmap.h>  // '16/01/22

#include <linux/mutex.h>
#include <linux/firmware.h>
#include <linux/vmalloc.h>

#include "ak4637.h"

//#define AK4637_DEBUG			//used at debug mode
//#define AK4637_CONTIF_DEBUG		//used at debug mode

//#define AK4637_PDN_GPIO

#ifdef AK4637_DEBUG
#define akdbgprt printk
#else
#define akdbgprt(format, arg...) do {} while (0)
#endif

/* AK4637 Codec Private Data */
struct ak4637_priv {
	struct snd_soc_codec codec;
	struct i2c_client *i2c;
	struct regmap *regmap;
	int fs2;
	int mGain;
	int rclk;	//Master Clock
	int pdn_gpio;
	int Hpf2FirmMode;
	int LpfFirmMode;
	int Eq1FirmMode;
	int Eq2FirmMode;
	int Eq3FirmMode;
	int Eq4FirmMode;
	int Eq5FirmMode;
	int dvtm;
	int dvtmMode;
	
};

//static struct snd_soc_codec *ak4637_codec;	//16/05/25
//static struct ak4637_priv *ak4637_data;

/* ak4637 register cache & default register settings */
static const struct reg_default ak4637_reg[] = { // '16/01/22
  { 0x0, 0x00 },   /*  AK4637_00_POWER_MANAGEMENT1           */
  { 0x1, 0x00 },   /*  AK4637_01_POWER_MANAGEMENT2           */
  { 0x2, 0x06 },   /*  AK4637_02_SIGNAL_SELECT1              */
  { 0x3, 0x00 },   /*  AK4637_03_SIGNAL_SELECT2              */
  { 0x4, 0x40 },   /*  AK4637_04_SIGNAL_SELECT3              */
  { 0x5, 0x50 },   /*  AK4637_05_MODE_CONTROL1               */
  { 0x6, 0x0B },   /*  AK4637_06_MODE_CONTROL2               */
  { 0x7, 0x02 },   /*  AK4637_07_MODE_CONTROL3               */
  { 0x8, 0x00 },   /*  AK4637_08_DIGITAL_MIC                 */
  { 0x9, 0x00 },   /*  AK4637_09_TIMER_SELECT                */
  { 0xA, 0x60 },   /*  AK4637_0A_ALC_TIMER_SELECT            */
  { 0xB, 0x00 },   /*  AK4637_0B_ALC_MODE_CONTROL1           */
  { 0xC, 0xE1 },   /*  AK4637_0C_ALC_MODE_CONTROL2           */
  { 0xD, 0xE1 },   /*  AK4637_0D_INPUT_VOLUME_CONTROL        */
  { 0xE, 0x00 },   /*  AK4637_0E_ALC_VOLUME                  */
  { 0xF, 0x00 },   /*  AK4637_0F_BEEP_CONTROL                */
  { 0x10, 0x18 },   /*  AK4637_10_DIGITAL_VOLUME_CONTROL      */
  { 0x11, 0x00 },   /*  AK4637_11_EQ_COMMON_GAIN_SELECT       */
  { 0x12, 0x00 },   /*  AK4637_12_EQ2_COMMON_GAIN_SETTING     */
  { 0x13, 0x00 },   /*  AK4637_13_EQ3_COMMON_GAIN_SETTING     */
  { 0x14, 0x00 },   /*  AK4637_14_EQ4_COMMON_GAIN_SETTING     */
  { 0x15, 0x00 },   /*  AK4637_15_EQ5_COMMON_GAIN_SETTING     */
  { 0x16, 0x01 },   /*  AK4637_16_DIGITAL_FILTER_SELECT1      */
  { 0x17, 0x00 },   /*  AK4637_17_DIGITAL_FILTER_SELECT2      */
  { 0x18, 0x03 },   /*  AK4637_18_DIGITAL_FILTER_MODE         */
  { 0x19, 0xB0 },   /*  AK4637_19_HPF2_COEFFICIENT0           */
  { 0x1A, 0x1F },   /*  AK4637_1A_HPF2_COEFFICIENT1           */
  { 0x1B, 0x9F },   /*  AK4637_1B_HPF2_COEFFICIENT2           */
  { 0x1C, 0x20 },   /*  AK4637_1C_HPF2_COEFFICIENT3           */
  { 0x1D, 0xB6 },   /*  AK4637_1D_LPF_COEFFICIENT0       change default fc 8kHz */
  { 0x1E, 0x0B },   /*  AK4637_1E_LPF_COEFFICIENT1       change default         */
  { 0x1F, 0x6D },   /*  AK4637_1F_LPF_COEFFICIENT2       change default         */
  { 0x20, 0x37 },   /*  AK4637_20_LPF_COEFFICIENT3       change default         */
  { 0x21, 0x00 },   /*  AK4637_21_DIGITAL_FILTER_SELECT3      */
  { 0x22, 0x28 },   /*  AK4637_22_E1_COEFFICIENT0        change default fc150Hz */
  { 0x23, 0x00 },   /*  AK4637_23_E1_COEFFICIENT1        change default         */
  { 0x24, 0x5E },   /*  AK4637_24_E1_COEFFICIENT2        change default         */
  { 0x25, 0x3F },   /*  AK4637_25_E1_COEFFICIENT3        change default         */
  { 0x26, 0x9F },   /*  AK4637_26_E1_COEFFICIENT4        change default         */
  { 0x27, 0xE0 },   /*  AK4637_27_E1_COEFFICIENT5        change default         */
  { 0x28, 0x0B },   /*  AK4637_28_E2_COEFFICIENT0        change default fc200Hz */
  { 0x29, 0x02 },   /*  AK4637_29_E2_COEFFICIENT1        change default         */
  { 0x2A, 0x58 },   /*  AK4637_2A_E2_COEFFICIENT2        change default         */
  { 0x2B, 0x3E },   /*  AK4637_2B_E2_COEFFICIENT3        change default         */
  { 0x2C, 0xA2 },   /*  AK4637_2C_E2_COEFFICIENT4        change default         */
  { 0x2D, 0xE1 },   /*  AK4637_2D_E2_COEFFICIENT5        change default         */
  { 0x2E, 0x60 },   /*  AK4637_2E_E3_COEFFICIENT0        change default fc1kHz  */
  { 0x2F, 0x08 },   /*  AK4637_2F_E3_COEFFICIENT1        change default         */
  { 0x30, 0x12 },   /*  AK4637_30_E3_COEFFICIENT2        change default         */
  { 0x31, 0x38 },   /*  AK4637_31_E3_COEFFICIENT3        change default         */
  { 0x32, 0x72 },   /*  AK4637_32_E3_COEFFICIENT4        change default         */
  { 0x33, 0xE7 },   /*  AK4637_33_E3_COEFFICIENT5        change default         */
  { 0x34, 0xC8 },   /*  AK4637_34_E4_COEFFICIENT0        change default fc4kHz  */
  { 0x35, 0x17 },   /*  AK4637_35_E4_COEFFICIENT1        change default         */
  { 0x36, 0x1F },   /*  AK4637_36_E4_COEFFICIENT2        change default         */
  { 0x37, 0x25 },   /*  AK4637_37_E4_COEFFICIENT3        change default         */
  { 0x38, 0x23 },   /*  AK4637_38_E4_COEFFICIENT4        change default         */
  { 0x39, 0xF5 },   /*  AK4637_39_E4_COEFFICIENT5        change default         */
  { 0x3A, 0x43 },   /*  AK4637_3A_E5_COEFFICIENT0        change default fc12kHz */
  { 0x3B, 0x1F },   /*  AK4637_3B_E5_COEFFICIENT1        change default         */
  { 0x3C, 0x00 },   /*  AK4637_3C_E5_COEFFICIENT2        change default         */
  { 0x3D, 0x00 },   /*  AK4637_3D_E5_COEFFICIENT3        change default         */
  { 0x3E, 0xCA },   /*  AK4637_3E_E5_COEFFICIENT4        change default         */
  { 0x3F, 0xFB },   /*  AK4637_3F_E5_COEFFICIENT5        change default         */
};


/* Mic Gain Control */
static const char *mic_gain_table[]  =
{
	"0","+3","+6","+9","+12","+15","+18","+21","+24","+27","+30",
};

static const struct soc_enum ak4637_micgain_enum[] = {
    SOC_ENUM_SINGLE_EXT(ARRAY_SIZE(mic_gain_table), mic_gain_table), 
};
/* Input Digital volume control:
 * from -54.375 to 36 dB in 0.375 dB steps (mute instead of -54.375 dB) */
static DECLARE_TLV_DB_SCALE(ivol_tlv, -5437, 37, 0);

/* Speaker output volume control: from 6.4 to 14.9 dB */
static DECLARE_TLV_DB_MINMAX(spkout_tlv, 640, 1490);

/* Output Digital volume control1:
 * from -90.0 to 12.0 dB in 0.5 dB steps (mute instead of -90.0 dB) */
static DECLARE_TLV_DB_SCALE(dvol_tlv, -9000, 50, 0);

/* Beep volume control:
 * from -42 to 0 dB (quantity of each step is various) */
static DECLARE_TLV_DB_MINMAX(beep_tlv, -4200, 0);

/* Programmable Filter Output volume control:
 * from -18 to 0 dB in 6 dB steps */
static DECLARE_TLV_DB_SCALE(pfvol_tlv, -1800, 600, 0);

/* EQx Gain control: (EQ2, EQ3, EQ4, EQ5)
 * from xx(too small value) to 0 dB (quantity of each step is various) */
static DECLARE_TLV_DB_MINMAX(eq2_tlv, -10000, 0);
static DECLARE_TLV_DB_MINMAX(eq3_tlv, -10000, 0);
static DECLARE_TLV_DB_MINMAX(eq4_tlv, -10000, 0);
static DECLARE_TLV_DB_MINMAX(eq5_tlv, -10000, 0);


static const char *ak4637_lvcm_select_texts[] =
		{"0dB/2.8-3.6V", "+2dB/3.0-3.6V", "+2dB/2.8-3.6V", "+4dB/3.0-3.6V"};

static const struct soc_enum ak4637_lvcm_enum[] = 
{
	SOC_ENUM_SINGLE(AK4637_04_SIGNAL_SELECT3, 6,
			ARRAY_SIZE(ak4637_lvcm_select_texts), ak4637_lvcm_select_texts),
};

static const char *ak4637_dclke_select_texts[] =
		{"Low", "64fs"};
static const char *ak4637_dclkp_select_texts[] =
		{"Low", "High"};
static const char *ak4637_bcko_select_texts[] =
		{"16fs", "32fs", "64fs"};
static const char *ak4637_ckoff_select_texts[] =
		{"Output", "Stop"};
static const char *ak4637_rgain_select_texts[] =
		{"0.00424dB_1/fs", "0.00212dB_1/fs", "0.00106dB_1/fs", "0.00106dB_2/fs",
		 "0.00106dB_4/fs", "0.00106dB_8/fs", "0.00106dB_16/fs", "0.00106dB_32/fs"};
static const char *ak4637_hpf1_fc_select_texts[] =
		{"3.7Hz", "14.8Hz", "118.4Hz", "236.8Hz"};
static const char *ak4637_mpwr_outvol_select_texts[] =
		{"2.4V", "2.0V"};
static const char *ak4637_adc_initcycle_set_texts[] =
		{"1059/fs", "267/fs", "531/fs", "135/fs"};
static const char *ak4637_fratt_select_texts[] =
		{"4/fs", "16/fs"};
static const char *ak4637_dvtm_texts[] =
		{"816/fs", "204/fs"};
static const char *ak4637_ivtm_select_texts[] =
		{"236/fs", "944/fs"};
static const char *ak4637_alceqfc_select_texts[] =
		{"8kHz_12kHz", "12kHz_24kHz", "24kHz_48kHz"};
static const char *ak4637_alcwtm_select_texts[] =
		{"128/fs", "256/fs", "512/fs", "1024/fs"};
static const char *ak4637_alcrfst_select_texts[] =
		{"0.0032dB", "0.0042dB", "0.0064dB", "0.0127dB"};
static const char *ak4637_alclmth_set_texts[] = {
	"-2.5_-4.1dBFS",
	"-2.5_-3.3dBFS",
	"-4.1_-6.0dBFS",
	"-4.1_-5.0dBFS",
	"-6.0_-8.5dBFS",
	"-6.0_-7.2dBFS",
	"-8.5_-12.0dBFS",
	"-8.5_-10.1dBFS",
};
static const unsigned int ak4637_alclmth_set_values[] = {
	0x00,
	0x01,
	0x02,
	0x03,
	0x40,
	0x41,
	0x42,
	0x43,
};
static const char *ak4637_beepvcom_select_texts[] =
		{"1.15V", "1.65V"};

static const char *ak4637_eq_transition_texts[] =
		{"256/fs", "2048/fs", "8192/fs", "16384/fs"};


static const struct soc_enum ak4637_bitset_enum[] = 
{
	SOC_ENUM_SINGLE(AK4637_08_DIGITAL_MIC, 3,
			ARRAY_SIZE(ak4637_dclke_select_texts), ak4637_dclke_select_texts), // 0
	SOC_ENUM_SINGLE(AK4637_08_DIGITAL_MIC, 1,
			ARRAY_SIZE(ak4637_dclkp_select_texts), ak4637_dclkp_select_texts),
	SOC_ENUM_SINGLE(AK4637_05_MODE_CONTROL1, 0,
			ARRAY_SIZE(ak4637_bcko_select_texts), ak4637_bcko_select_texts),
	SOC_ENUM_SINGLE(AK4637_05_MODE_CONTROL1, 2,
			ARRAY_SIZE(ak4637_ckoff_select_texts), ak4637_ckoff_select_texts),
	SOC_ENUM_SINGLE(AK4637_0B_ALC_MODE_CONTROL1, 2,
			ARRAY_SIZE(ak4637_rgain_select_texts), ak4637_rgain_select_texts),
	SOC_ENUM_SINGLE(AK4637_16_DIGITAL_FILTER_SELECT1, 1,
			ARRAY_SIZE(ak4637_hpf1_fc_select_texts), ak4637_hpf1_fc_select_texts), // 5
	SOC_ENUM_SINGLE(AK4637_03_SIGNAL_SELECT2, 4,
			ARRAY_SIZE(ak4637_mpwr_outvol_select_texts), ak4637_mpwr_outvol_select_texts),
	SOC_ENUM_SINGLE(AK4637_09_TIMER_SELECT, 6,
			ARRAY_SIZE(ak4637_adc_initcycle_set_texts), ak4637_adc_initcycle_set_texts),
	SOC_ENUM_SINGLE(AK4637_09_TIMER_SELECT, 5,
			ARRAY_SIZE(ak4637_fratt_select_texts), ak4637_fratt_select_texts),
	SOC_ENUM_SINGLE_EXT(ARRAY_SIZE(ak4637_dvtm_texts), ak4637_dvtm_texts),
	SOC_ENUM_SINGLE(AK4637_0A_ALC_TIMER_SELECT, 6,
			ARRAY_SIZE(ak4637_ivtm_select_texts), ak4637_ivtm_select_texts), // 10
	SOC_ENUM_SINGLE(AK4637_0A_ALC_TIMER_SELECT, 4,
			ARRAY_SIZE(ak4637_alceqfc_select_texts), ak4637_alceqfc_select_texts),
	SOC_ENUM_SINGLE(AK4637_0A_ALC_TIMER_SELECT, 2,
			ARRAY_SIZE(ak4637_alcwtm_select_texts), ak4637_alcwtm_select_texts),
	SOC_ENUM_SINGLE(AK4637_0A_ALC_TIMER_SELECT, 0,
			ARRAY_SIZE(ak4637_alcrfst_select_texts), ak4637_alcrfst_select_texts),
	SOC_VALUE_ENUM_SINGLE(AK4637_0B_ALC_MODE_CONTROL1, 0, 0x43,
					ARRAY_SIZE(ak4637_alclmth_set_texts), ak4637_alclmth_set_texts, ak4637_alclmth_set_values),
	SOC_ENUM_SINGLE(AK4637_0F_BEEP_CONTROL, 6,
			ARRAY_SIZE(ak4637_beepvcom_select_texts), ak4637_beepvcom_select_texts), // 15
	SOC_ENUM_SINGLE(AK4637_12_EQ2_COMMON_GAIN_SETTING, 0,
			ARRAY_SIZE(ak4637_eq_transition_texts), ak4637_eq_transition_texts),
	SOC_ENUM_SINGLE(AK4637_13_EQ3_COMMON_GAIN_SETTING, 0,
			ARRAY_SIZE(ak4637_eq_transition_texts), ak4637_eq_transition_texts),
	SOC_ENUM_SINGLE(AK4637_14_EQ4_COMMON_GAIN_SETTING, 0,
			ARRAY_SIZE(ak4637_eq_transition_texts), ak4637_eq_transition_texts),
	SOC_ENUM_SINGLE(AK4637_15_EQ5_COMMON_GAIN_SETTING, 0,
			ARRAY_SIZE(ak4637_eq_transition_texts), ak4637_eq_transition_texts),

};

static const char *stereo_on_select[]  =
{
	"Off", "On",
};

static const struct soc_enum ak4637_stereo_enum[] = {
    SOC_ENUM_SINGLE_EXT(ARRAY_SIZE(stereo_on_select), stereo_on_select), 
};

static int get_micgain(
struct snd_kcontrol       *kcontrol,
struct snd_ctl_elem_value  *ucontrol)
{
    struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);	//16/05/25
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);

    ucontrol->value.enumerated.item[0] = ak4637->mGain;

    return 0;
}

static int set_micgain(
struct snd_kcontrol       *kcontrol,
struct snd_ctl_elem_value  *ucontrol)
{
    struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);	//16/05/25
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
	int currMode = ucontrol->value.enumerated.item[0];

	if (currMode < ARRAY_SIZE(mic_gain_table)){
		if ( ak4637->mGain != currMode ) {
			ak4637->mGain = currMode;
			if ( ak4637->mGain >= 8 ) {
				snd_soc_update_bits(codec, AK4637_02_SIGNAL_SELECT1, 0x47, ((ak4637->mGain-8)|0x40));
			}
			else {
				snd_soc_update_bits(codec, AK4637_02_SIGNAL_SELECT1, 0x47, (ak4637->mGain));
			}
		}
	}
	else {
		akdbgprt(" [AK4637] %s Invalid Value selected!\n",__FUNCTION__);
	}
    return 0;
}

static const struct soc_enum ak4637_firmware_enum[] = 
{
    SOC_ENUM_SINGLE_EXT(ARRAY_SIZE(ak4637_firmware_texts), ak4637_firmware_texts),
};

static int ak4637_firmware_write_reg(struct snd_soc_codec *codec, u16 mode, u16 cmd);
static int ak4637_write_cache_reg(struct snd_soc_codec *codec, u16  regs, u16  rege);

static int get_hpf2_write_reg(
struct snd_kcontrol       *kcontrol,
struct snd_ctl_elem_value  *ucontrol)
{
	struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);	//16/05/25
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
  
   /* Get the current output routing */
    ucontrol->value.enumerated.item[0] = ak4637->Hpf2FirmMode;	

    return 0;
}

static int set_hpf2_write_reg(
struct snd_kcontrol       *kcontrol,
struct snd_ctl_elem_value  *ucontrol)
{
    struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);	//16/05/25
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
    int    currMode = ucontrol->value.enumerated.item[0];
	int    ret;
	
	if (currMode < ARRAY_SIZE(ak4637_firmware_texts)){
		if ( ak4637->Hpf2FirmMode != currMode ) {
			akdbgprt("\t%s HPF2 Firmware mode =%d\n",__FUNCTION__, currMode);

			ret = ak4637_firmware_write_reg(codec, FIRMTYPE_HPF2, currMode); 
			if ( ret != 0 ) return(-1);

			ak4637->Hpf2FirmMode = currMode;
		}
	}
	else {
		akdbgprt(" [AK4637] %s Invalid Value selected!\n",__FUNCTION__);
	}
	return(0);
}


static int get_lpf_write_reg(
struct snd_kcontrol       *kcontrol,
struct snd_ctl_elem_value  *ucontrol)
{
	struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);	//16/05/25
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
  
   /* Get the current output routing */
    ucontrol->value.enumerated.item[0] = ak4637->LpfFirmMode;	

    return 0;
}

static int set_lpf_write_reg(
struct snd_kcontrol       *kcontrol,
struct snd_ctl_elem_value  *ucontrol)
{
    struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);	//16/05/25
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
    int    currMode = ucontrol->value.enumerated.item[0];
	int    ret;
	
	if (currMode < ARRAY_SIZE(ak4637_firmware_texts)){
		if ( ak4637->LpfFirmMode != currMode ) {
			akdbgprt("\t%s LPF Firmware mode =%d\n",__FUNCTION__, currMode);

			ret = ak4637_firmware_write_reg(codec, FIRMTYPE_LPF, currMode); 
			if ( ret != 0 ) return(-1);

			ak4637->LpfFirmMode = currMode;
			}
	}
	else {
		akdbgprt(" [AK4637] %s Invalid Value selected!\n",__FUNCTION__);
	}
	return(0);
}


static int get_eq1_write_reg(
struct snd_kcontrol       *kcontrol,
struct snd_ctl_elem_value  *ucontrol)
{
	struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);	//16/05/25
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
  
   /* Get the current output routing */
    ucontrol->value.enumerated.item[0] = ak4637->Eq1FirmMode;	

    return 0;
}

static int set_eq1_write_reg(
struct snd_kcontrol       *kcontrol,
struct snd_ctl_elem_value  *ucontrol)
{
    struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);	//16/05/25
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
    int    currMode = ucontrol->value.enumerated.item[0];
	int    ret;
	
	if (currMode < ARRAY_SIZE(ak4637_firmware_texts)){
		if ( ak4637->Eq1FirmMode != currMode ) {
			akdbgprt("\t%s EQ1 Firmware mode =%d\n",__FUNCTION__, currMode);

			ret = ak4637_firmware_write_reg(codec, FIRMTYPE_EQ1, currMode); 
			if ( ret != 0 ) return(-1);

			ak4637->Eq1FirmMode = currMode;
			}
	}
	else {
		akdbgprt(" [AK4637] %s Invalid Value selected!\n",__FUNCTION__);
	}
	return(0);
}


static int get_eq2_write_reg(
struct snd_kcontrol       *kcontrol,
struct snd_ctl_elem_value  *ucontrol)
{
	struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);	//16/05/25
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
  
   /* Get the current output routing */
    ucontrol->value.enumerated.item[0] = ak4637->Eq2FirmMode;	

    return 0;
}

static int set_eq2_write_reg(
struct snd_kcontrol       *kcontrol,
struct snd_ctl_elem_value  *ucontrol)
{
    struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);	//16/05/25
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
    int    currMode = ucontrol->value.enumerated.item[0];
	int    ret;
	
	if (currMode < ARRAY_SIZE(ak4637_firmware_texts)){
		if ( ak4637->Eq2FirmMode != currMode ) {
			akdbgprt("\t%s EQ2 Firmware mode =%d\n",__FUNCTION__, currMode);

			ret = ak4637_firmware_write_reg(codec, FIRMTYPE_EQ2, currMode); 
			if ( ret != 0 ) return(-1);

			ak4637->Eq2FirmMode = currMode;
			}
	}
	else {
		akdbgprt(" [AK4637] %s Invalid Value selected!\n",__FUNCTION__);
	}
	return(0);
}


static int get_eq3_write_reg(
struct snd_kcontrol       *kcontrol,
struct snd_ctl_elem_value  *ucontrol)
{
	struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);	//16/05/25
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
  
   /* Get the current output routing */
    ucontrol->value.enumerated.item[0] = ak4637->Eq3FirmMode;	

    return 0;
}

static int set_eq3_write_reg(
struct snd_kcontrol       *kcontrol,
struct snd_ctl_elem_value  *ucontrol)
{
    struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);	//16/05/25
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
    int    currMode = ucontrol->value.enumerated.item[0];
	int    ret;
	
	if (currMode < ARRAY_SIZE(ak4637_firmware_texts)){
		if ( ak4637->Eq3FirmMode != currMode ) {
			akdbgprt("\t%s EQ3 Firmware mode =%d\n",__FUNCTION__, currMode);

			ret = ak4637_firmware_write_reg(codec, FIRMTYPE_EQ3, currMode); 
			if ( ret != 0 ) return(-1);

			ak4637->Eq3FirmMode = currMode;
			}
	}
	else {
		akdbgprt(" [AK4637] %s Invalid Value selected!\n",__FUNCTION__);
	}
	return(0);
}


static int get_eq4_write_reg(
struct snd_kcontrol       *kcontrol,
struct snd_ctl_elem_value  *ucontrol)
{
	struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);	//16/05/25
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
  
   /* Get the current output routing */
    ucontrol->value.enumerated.item[0] = ak4637->Eq4FirmMode;	

    return 0;
}

static int set_eq4_write_reg(
struct snd_kcontrol       *kcontrol,
struct snd_ctl_elem_value  *ucontrol)
{
    struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);	//16/05/25
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
    int    currMode = ucontrol->value.enumerated.item[0];
	int    ret;
	
	if (currMode < ARRAY_SIZE(ak4637_firmware_texts)){
		if ( ak4637->Eq4FirmMode != currMode ) {
			akdbgprt("\t%s EQ4 Firmware mode =%d\n",__FUNCTION__, currMode);

			ret = ak4637_firmware_write_reg(codec, FIRMTYPE_EQ4, currMode); 
			if ( ret != 0 ) return(-1);

			ak4637->Eq4FirmMode = currMode;
			}
	}
	else {
		akdbgprt(" [AK4637] %s Invalid Value selected!\n",__FUNCTION__);
	}
	return(0);
}


static int get_eq5_write_reg(
struct snd_kcontrol       *kcontrol,
struct snd_ctl_elem_value  *ucontrol)
{
	struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);	//16/05/25
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
  
   /* Get the current output routing */
    ucontrol->value.enumerated.item[0] = ak4637->Eq5FirmMode;	

    return 0;
}

static int set_eq5_write_reg(
struct snd_kcontrol       *kcontrol,
struct snd_ctl_elem_value  *ucontrol)
{
    struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);	//16/05/25
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
    int    currMode = ucontrol->value.enumerated.item[0];
	int    ret;

	if (currMode < ARRAY_SIZE(ak4637_firmware_texts)){
		if ( ak4637->Eq5FirmMode != currMode ) {
			akdbgprt("\t%s EQ5 Firmware mode =%d\n",__FUNCTION__, currMode);

			ret = ak4637_firmware_write_reg(codec, FIRMTYPE_EQ5, currMode); 
			if ( ret != 0 ) return(-1);

			ak4637->Eq5FirmMode = currMode;
		}
	}
	else {
		akdbgprt(" [AK4637] %s Invalid Value selected!\n",__FUNCTION__);
	}
	return(0);
}

static int get_dvtm(
struct snd_kcontrol       *kcontrol,
struct snd_ctl_elem_value  *ucontrol)
{
	struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);	//16/05/25
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
  
   /* Get the current output routing */
    ucontrol->value.enumerated.item[0] = ak4637->dvtmMode;	

    return 0;
}

static int set_dvtm(
struct snd_kcontrol       *kcontrol,
struct snd_ctl_elem_value  *ucontrol)
{
    struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);	//16/05/25
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
    int    currMode = ucontrol->value.enumerated.item[0];

	if (currMode < ARRAY_SIZE(ak4637_dvtm_texts)){
		if ( ak4637->dvtmMode != currMode ) {
			ak4637->dvtmMode = currMode;
			if ( ak4637->dvtmMode == 0 ){
				ak4637->dvtm = 816000;
			}
			else{
				ak4637->dvtm = 204000;
			}
			snd_soc_update_bits(codec, AK4637_09_TIMER_SELECT, 0x01, ak4637->dvtmMode);
			akdbgprt("\t%s DVTM = %d\n",__FUNCTION__, ak4637->dvtm);
		}
	}
	else {
		akdbgprt(" [AK4637] %s Invalid Value selected!\n",__FUNCTION__);
	}
	return(0);
}

#ifdef AK4637_DEBUG

static const char *test_reg_select[]   = 
{
    "read AK4637 Reg 00:21",
    "read AK4637 Reg 22:3F",
};

static const struct soc_enum ak4637_enum[] = 
{
    SOC_ENUM_SINGLE_EXT(ARRAY_SIZE(test_reg_select), test_reg_select),
};

static int nTestRegNo = 0;

static int get_test_reg(
struct snd_kcontrol       *kcontrol,
struct snd_ctl_elem_value  *ucontrol)
{
    /* Get the current output routing */
    ucontrol->value.enumerated.item[0] = nTestRegNo;

    return 0;

}

static int set_test_reg(
struct snd_kcontrol       *kcontrol,
struct snd_ctl_elem_value  *ucontrol)
{
    struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);	//16/05/25
    u32    currMode = ucontrol->value.enumerated.item[0];
	int    i, value;
	int	   regs, rege;

	nTestRegNo = currMode;

	switch(nTestRegNo) {
		case 1:
			regs = 0x22;
			rege = 0x3F;
			break;
		default:
			regs = 0x00;
			rege = 0x21;
			break;
	}

	for ( i = regs ; i <= rege ; i++ ){
		value = snd_soc_read(codec, i);
		printk("***AK4637 Addr,Reg=(%x, %x)\n", i, value);
	}

	return(0);

}
#endif

static const struct snd_kcontrol_new ak4637_snd_controls[] = {
	SOC_ENUM_EXT("Mic Gain Control", ak4637_micgain_enum[0], get_micgain, set_micgain),
	SOC_SINGLE_TLV("Digital Input Volume",
			AK4637_0D_INPUT_VOLUME_CONTROL, 0, 0xF1, 0, ivol_tlv),
	SOC_SINGLE_TLV("Speaker Output Volume",
			AK4637_03_SIGNAL_SELECT2, 6, 0x03, 0, spkout_tlv),
	SOC_ENUM("Monaural Lineout Volume", ak4637_lvcm_enum[0]),
	SOC_SINGLE_TLV("Digital Output Volume",
			AK4637_10_DIGITAL_VOLUME_CONTROL, 0, 0xCC, 1, dvol_tlv),
	SOC_SINGLE_TLV("Beep Volume Control",
			AK4637_0F_BEEP_CONTROL, 0, 0x09, 1, beep_tlv),
	SOC_SINGLE_TLV("Programmable Filter Output Volume",
			AK4637_18_DIGITAL_FILTER_MODE, 4, 0x03, 1,  pfvol_tlv),

	SOC_ENUM("DMCLK pin Output Clock", ak4637_bitset_enum[0]),
	SOC_ENUM("Digital MIC Data Select", ak4637_bitset_enum[1]),
	SOC_ENUM("BICK Output Frequency in Master Mode", ak4637_bitset_enum[2]),
	SOC_ENUM("FCK BICK Output in Master Mode", ak4637_bitset_enum[3]),

    SOC_ENUM("High Path Filter 1 Fc", ak4637_bitset_enum[5]),
   	SOC_SINGLE("High Path Filter 1", AK4637_16_DIGITAL_FILTER_SELECT1, 0, 1, 0),
	SOC_SINGLE("High Path Filter 2", AK4637_17_DIGITAL_FILTER_SELECT2, 0, 1, 0),
    SOC_SINGLE("Low Path Filter", AK4637_17_DIGITAL_FILTER_SELECT2, 1, 1, 0),
    
    SOC_SINGLE("5 Band Equalizer 1", AK4637_21_DIGITAL_FILTER_SELECT3, 0, 1, 0),
	SOC_SINGLE("5 Band Equalizer 2", AK4637_21_DIGITAL_FILTER_SELECT3, 1, 1, 0),
	SOC_SINGLE("5 Band Equalizer 3", AK4637_21_DIGITAL_FILTER_SELECT3, 2, 1, 0),
	SOC_SINGLE("5 Band Equalizer 4", AK4637_21_DIGITAL_FILTER_SELECT3, 3, 1, 0),
	SOC_SINGLE("5 Band Equalizer 5", AK4637_21_DIGITAL_FILTER_SELECT3, 4, 1, 0),
	
	SOC_SINGLE("5 Band Equalizer 2 Gain Enable", AK4637_11_EQ_COMMON_GAIN_SELECT, 1, 1, 0),
	SOC_SINGLE("5 Band Equalizer 3 Gain Enable", AK4637_11_EQ_COMMON_GAIN_SELECT, 2, 1, 0),
	SOC_SINGLE("5 Band Equalizer 4 Gain Enable", AK4637_11_EQ_COMMON_GAIN_SELECT, 3, 1, 0),
	SOC_SINGLE("5 Band Equalizer 5 Gain Enable", AK4637_11_EQ_COMMON_GAIN_SELECT, 4, 1, 0),
	
	SOC_ENUM("5 Band Equalizer 2 Transition Time", ak4637_bitset_enum[16]),
	SOC_ENUM("5 Band Equalizer 3 Transition Time", ak4637_bitset_enum[17]),
	SOC_ENUM("5 Band Equalizer 4 Transition Time", ak4637_bitset_enum[18]),
	SOC_ENUM("5 Band Equalizer 5 Transition Time", ak4637_bitset_enum[19]),

	SOC_SINGLE_TLV("5 Band Equalizer 2 Gain Control",
			AK4637_12_EQ2_COMMON_GAIN_SETTING, 2, 0x3F, 0, eq2_tlv),
	SOC_SINGLE_TLV("5 Band Equalizer 3 Gain Control",
			AK4637_13_EQ3_COMMON_GAIN_SETTING, 2, 0x3F, 0, eq3_tlv),
	SOC_SINGLE_TLV("5 Band Equalizer 4 Gain Control",
			AK4637_14_EQ4_COMMON_GAIN_SETTING, 2, 0x3F, 0, eq4_tlv),
	SOC_SINGLE_TLV("5 Band Equalizer 5 Gain Control",
			AK4637_15_EQ5_COMMON_GAIN_SETTING, 2, 0x3F, 0, eq5_tlv),

	SOC_ENUM("ALC Recovery Gain Step", ak4637_bitset_enum[4]),
	SOC_ENUM("ALC Fast Recovery Attenuation Amount", ak4637_bitset_enum[8]),
	SOC_SINGLE("ALC Fast Recovery Enable", AK4637_09_TIMER_SELECT, 4, 1, 1),
	SOC_ENUM("ALC Equalizer Frequency Setting", ak4637_bitset_enum[11]),
	SOC_ENUM("ALC Recovery Waiting Period", ak4637_bitset_enum[12]),
	SOC_ENUM("ALC Fast Recovery Speed", ak4637_bitset_enum[13]),

	SOC_SINGLE("ALC Equalizer Enable", AK4637_0B_ALC_MODE_CONTROL1, 7, 1, 1),
	SOC_SINGLE("ALC for Recording", AK4637_0B_ALC_MODE_CONTROL1, 5, 1, 0),
	SOC_ENUM("ALC Limiter Detection Level", ak4637_bitset_enum[14]),  // '16/01/22
	SOC_SINGLE_TLV("ALC Recovery Reference Value",
			AK4637_0C_ALC_MODE_CONTROL2, 0, 0xF1, 0, ivol_tlv),

	SOC_ENUM("MPWR pin Output Voltage", ak4637_bitset_enum[6]),
	SOC_SINGLE("Thermal Shutdown Auto Power Up", AK4637_07_MODE_CONTROL3, 7, 1, 1),

	SOC_ENUM("ADC Initialization Cycle", ak4637_bitset_enum[7]),
	
	SOC_ENUM_EXT("Output Digital Volume Transition Time", ak4637_bitset_enum[9], get_dvtm, set_dvtm),
	SOC_ENUM("Input Digital Volume Transition Time", ak4637_bitset_enum[10]),
	SOC_ENUM("BEEP Input Amp Vcom Setting", ak4637_bitset_enum[15]),

	SOC_ENUM_EXT("High Pass Filter 2 Firmware", ak4637_firmware_enum[0], get_hpf2_write_reg, set_hpf2_write_reg),
	SOC_ENUM_EXT("Low Pass Filter Firmware", ak4637_firmware_enum[0], get_lpf_write_reg, set_lpf_write_reg),
	SOC_ENUM_EXT("5 Band Equalizer 1 Firmware", ak4637_firmware_enum[0], get_eq1_write_reg, set_eq1_write_reg),
	SOC_ENUM_EXT("5 Band Equalizer 2 Firmware", ak4637_firmware_enum[0], get_eq2_write_reg, set_eq2_write_reg),
	SOC_ENUM_EXT("5 Band Equalizer 3 Firmware", ak4637_firmware_enum[0], get_eq3_write_reg, set_eq3_write_reg),
	SOC_ENUM_EXT("5 Band Equalizer 4 Firmware", ak4637_firmware_enum[0], get_eq4_write_reg, set_eq4_write_reg),
	SOC_ENUM_EXT("5 Band Equalizer 5 Firmware", ak4637_firmware_enum[0], get_eq5_write_reg, set_eq5_write_reg),

#ifdef AK4637_DEBUG
	SOC_ENUM_EXT("Reg Read", ak4637_enum[0], get_test_reg, set_test_reg),
#endif

};


static const char *ak4637_adcsw_select_texts[] =
		{"Off", "On"};

// '16/01/22
static SOC_ENUM_SINGLE_VIRT_DECL(ak4637_adcsw_mux_enum, ak4637_adcsw_select_texts);

static const struct snd_kcontrol_new ak4637_adcsw_mux_control =
	SOC_DAPM_ENUM("ADC Switch", ak4637_adcsw_mux_enum);
//

static const char *ak4637_dmicsw_select_texts[] =
		{"Off", "On"};

// '16/01/22
static SOC_ENUM_SINGLE_VIRT_DECL(ak4637_dmicsw_mux_enum, ak4637_dmicsw_select_texts);

static const struct snd_kcontrol_new ak4637_dmicsw_mux_control =
	SOC_DAPM_ENUM("DMIC Switch", ak4637_dmicsw_mux_enum);
//

static const char *ak4637_ain_select_texts[] =
		{"AIN", "Mic Bias"};

// '16/01/22
static SOC_ENUM_SINGLE_VIRT_DECL(ak4637_ain_mux_enum, ak4637_ain_select_texts);

static const struct snd_kcontrol_new ak4637_ain_mux_control =
	SOC_DAPM_ENUM("AIN Switch", ak4637_ain_mux_enum);
//

static const char *ak4637_spklo_select_texts[] =
		{"Speaker", "Line"};

static const struct soc_enum ak4637_spklo_mux_enum =
	SOC_ENUM_SINGLE(AK4637_00_POWER_MANAGEMENT1, 3,
			ARRAY_SIZE(ak4637_spklo_select_texts), ak4637_spklo_select_texts);

static const struct snd_kcontrol_new ak4637_spklo_mux_control =
	SOC_DAPM_ENUM("SPKLO Select", ak4637_spklo_mux_enum);

static const char *ak4637_adcpf_select_texts[] =
		{"SDTI", "ADC"};

static const struct soc_enum ak4637_adcpf_mux_enum =
	SOC_ENUM_SINGLE(AK4637_18_DIGITAL_FILTER_MODE, 1,
			ARRAY_SIZE(ak4637_adcpf_select_texts), ak4637_adcpf_select_texts);

static const struct snd_kcontrol_new ak4637_adcpf_mux_control =
	SOC_DAPM_ENUM("ADCPF Select", ak4637_adcpf_mux_enum);


static const char *ak4637_mdif_select_texts[] =
		{"Single", "Differential"};

static const struct soc_enum ak4637_mdif_mux_enum =
	SOC_ENUM_SINGLE(AK4637_03_SIGNAL_SELECT2, 0,
			ARRAY_SIZE(ak4637_mdif_select_texts), ak4637_mdif_select_texts);

static const struct snd_kcontrol_new ak4637_mdif_mux_control =
	SOC_DAPM_ENUM("MDIF Select", ak4637_mdif_mux_enum);


static const char *ak4637_pfsdo_select_texts[] =
		{"ADC", "PFIL"};

static const struct soc_enum ak4637_pfsdo_mux_enum =
	SOC_ENUM_SINGLE(AK4637_18_DIGITAL_FILTER_MODE, 0,
			ARRAY_SIZE(ak4637_pfsdo_select_texts), ak4637_pfsdo_select_texts);

static const struct snd_kcontrol_new ak4637_pfsdo_mux_control =
	SOC_DAPM_ENUM("PFSDO Select", ak4637_pfsdo_mux_enum);


static const char *ak4637_pfdac_select_texts[] =
		{"SDTI", "PFVOL", "(SDTI+PFVOL)/2"};

static const struct soc_enum ak4637_pfdac_mux_enum =
	SOC_ENUM_SINGLE(AK4637_18_DIGITAL_FILTER_MODE, 2,
			ARRAY_SIZE(ak4637_pfdac_select_texts), ak4637_pfdac_select_texts);

static const struct snd_kcontrol_new ak4637_pfdac_mux_control =
	SOC_DAPM_ENUM("PFDAC Select", ak4637_pfdac_mux_enum);


static const char *ak4637_mic_select_texts[] =
		{"AMIC", "DMIC"};

static const struct soc_enum ak4637_mic_mux_enum =
	SOC_ENUM_SINGLE(AK4637_08_DIGITAL_MIC, 0,
			ARRAY_SIZE(ak4637_mic_select_texts), ak4637_mic_select_texts);

static const struct snd_kcontrol_new ak4637_mic_mux_control =
	SOC_DAPM_ENUM("MIC Select", ak4637_mic_mux_enum);

static const struct snd_kcontrol_new ak4637_dacsl_mixer_controls[] = {
	SOC_DAPM_SINGLE("DACS", AK4637_02_SIGNAL_SELECT1, 5, 1, 0), 
	SOC_DAPM_SINGLE("DACL", AK4637_04_SIGNAL_SELECT3, 5, 1, 0), 
	SOC_DAPM_SINGLE("BEEPS", AK4637_0F_BEEP_CONTROL, 5, 1, 0), 
};

static int ak4637_spklo_event(struct snd_soc_dapm_widget *w,
		struct snd_kcontrol *kcontrol, int event) //CONFIG_LINF
{
	struct snd_soc_codec *codec = w->codec;
	u32 reg, nLOSEL;
	
	akdbgprt("\t[AK4637] %s(%d)\n",__FUNCTION__,__LINE__);
	
	reg = snd_soc_read(codec, AK4637_00_POWER_MANAGEMENT1);  // '16/01/22
	nLOSEL = (0x08 & reg); 

	switch (event) {
		case SND_SOC_DAPM_PRE_PMU:	/* before widget power up */
			break;
		case SND_SOC_DAPM_POST_PMU:	/* after widget power up */
			if ( nLOSEL ) {
				akdbgprt("\t[AK4637] %s wait=300msec\n",__FUNCTION__);
				mdelay(300);
			}
			else {
				akdbgprt("\t[AK4637] %s wait=1msec\n",__FUNCTION__);
				mdelay(1);
			}
			snd_soc_update_bits(codec, AK4637_02_SIGNAL_SELECT1, 0x80,0x80);
	
			break;
		case SND_SOC_DAPM_PRE_PMD:	/* before widget power down */
			snd_soc_update_bits(codec, AK4637_02_SIGNAL_SELECT1, 0x80,0x00);
			mdelay(1);
			break;
		case SND_SOC_DAPM_POST_PMD:	/* after widget power down */
			if ( nLOSEL ) {
				akdbgprt("\t[AK4637] %s wait=300msec\n",__FUNCTION__);
				mdelay(300);
			}
			break;
	}

	return 0;
}


static const struct snd_soc_dapm_widget ak4637_dapm_widgets[] = {

// Analog Output
	SND_SOC_DAPM_OUTPUT("SPKLO"),

	SND_SOC_DAPM_PGA("SPK Amp", SND_SOC_NOPM, 0, 0, NULL, 0),
	SND_SOC_DAPM_PGA("Line Amp", SND_SOC_NOPM, 0, 0, NULL, 0),
	SND_SOC_DAPM_PGA("BEEP Amp", AK4637_00_POWER_MANAGEMENT1, 5, 0, NULL, 0),

	SND_SOC_DAPM_MIXER_E("SPKLO Mixer", AK4637_01_POWER_MANAGEMENT2, 1, 0, 
			&ak4637_dacsl_mixer_controls[0], ARRAY_SIZE(ak4637_dacsl_mixer_controls),
			ak4637_spklo_event, (SND_SOC_DAPM_POST_PMU |SND_SOC_DAPM_PRE_PMD 
                            |SND_SOC_DAPM_PRE_PMU |SND_SOC_DAPM_POST_PMD)),

	SND_SOC_DAPM_MUX("SPKLO MUX", SND_SOC_NOPM, 0, 0, &ak4637_spklo_mux_control),

// Analog Input
	SND_SOC_DAPM_INPUT("AIN"),
	SND_SOC_DAPM_INPUT("BEEPIN"),
	SND_SOC_DAPM_INPUT("INP_N"),


#if (defined(PLL_16BICK_MODE) | defined(PLL_32BICK_MODE) | defined(PLL_64BICK_MODE) | defined(PLL_MCLK_MODE))
	SND_SOC_DAPM_SUPPLY("PMPLL", AK4637_01_POWER_MANAGEMENT2, 2, 0, NULL, 0),
#endif

	SND_SOC_DAPM_ADC("PFIL", NULL, AK4637_00_POWER_MANAGEMENT1, 7, 0),
	SND_SOC_DAPM_PGA("SDTI+PFIL", SND_SOC_NOPM, 0, 0, NULL, 0),

	SND_SOC_DAPM_AIF_OUT("SDTO", "Capture", 0, SND_SOC_NOPM, 0, 0),
	SND_SOC_DAPM_AIF_IN("SDTI", "Playback", 0, SND_SOC_NOPM, 0, 0),

// PFIL
	SND_SOC_DAPM_MUX("PFDAC MUX", SND_SOC_NOPM, 0, 0, &ak4637_pfdac_mux_control),
	SND_SOC_DAPM_MUX("PFSDO MUX", SND_SOC_NOPM, 0, 0, &ak4637_pfsdo_mux_control),
	SND_SOC_DAPM_MUX("PFIL MUX", SND_SOC_NOPM, 0, 0, &ak4637_adcpf_mux_control),

// ADC, DMIC, etc...
	SND_SOC_DAPM_ADC("DMIC", NULL, AK4637_08_DIGITAL_MIC, 4, 0),
	SND_SOC_DAPM_MUX("DMIC SW", SND_SOC_NOPM, 0, 0, &ak4637_dmicsw_mux_control),
	
	SND_SOC_DAPM_ADC("ADC", NULL, AK4637_00_POWER_MANAGEMENT1, 0, 0),
	SND_SOC_DAPM_MUX("ADC SW", SND_SOC_NOPM, 0, 0, &ak4637_adcsw_mux_control),
	SND_SOC_DAPM_DAC("DAC", NULL, AK4637_00_POWER_MANAGEMENT1, 2, 0),

	SND_SOC_DAPM_MUX("AIN MUX", SND_SOC_NOPM, 0, 0, &ak4637_mdif_mux_control),

// Analog Input Pin Select, MIC Bias
	SND_SOC_DAPM_MICBIAS("Mic Bias", AK4637_02_SIGNAL_SELECT1, 3, 0),
	SND_SOC_DAPM_MUX("AIN Mic Bias MUX", SND_SOC_NOPM, 0, 0, &ak4637_ain_mux_control),

// Digital Mic
	SND_SOC_DAPM_INPUT("DMICIN"),
	SND_SOC_DAPM_MUX("MIC MUX", SND_SOC_NOPM, 0, 0, &ak4637_mic_mux_control),

};

static const struct snd_soc_dapm_route ak4637_intercon[] = {

#if (defined(PLL_16BICK_MODE) | defined(PLL_32BICK_MODE) | defined(PLL_64BICK_MODE) | defined(PLL_MCLK_MODE))
	{"ADC", NULL, "PMPLL"},
	{"DAC", NULL, "PMPLL"},
	{"DMIC", NULL, "PMPLL"},
#endif

	{"AIN MUX", "Single", "AIN"},
	{"AIN MUX", "Differential", "INP_N"},

	{"Mic Bias", "AIN MUX", "AIN MUX"},
	{"AIN Mic Bias MUX", "AIN", "AIN MUX"},
	{"AIN Mic Bias MUX", "Mic Bias", "Mic Bias"},

	{"ADC", NULL, "AIN Mic Bias MUX"},
	{"ADC SW", "On", "ADC"},
	{"MIC MUX", "AMIC", "ADC SW"},

	{"DMIC", NULL, "DMICIN"},
	{"DMIC SW", "On", "DMIC"},
	{"MIC MUX", "DMIC", "DMIC SW"},

	{"PFIL MUX", "SDTI", "SDTI"},
	{"PFIL MUX", "ADC", "MIC MUX"},
	{"PFIL", NULL, "PFIL MUX"},

	{"PFSDO MUX", "ADC", "MIC MUX"},
	{"PFSDO MUX", "PFIL", "PFIL"},
	{"SDTO", NULL, "PFSDO MUX"},

	{"SDTI+PFIL", NULL, "SDTI"},
	{"SDTI+PFIL", NULL, "PFIL"},

	{"PFDAC MUX", "SDTI", "SDTI"},
	{"PFDAC MUX", "PFVOL", "PFIL"},
	{"PFDAC MUX", "(SDTI+PFVOL)/2", "SDTI+PFIL"},
	{"DAC", NULL, "PFDAC MUX"},

	{"BEEP Amp", NULL, "BEEPIN"},
	{"SPKLO Mixer", "BEEPS", "BEEP Amp"},
	{"SPKLO Mixer", "DACL", "DAC"},
	{"SPKLO Mixer", "DACS", "DAC"},

	{"SPK Amp", NULL, "SPKLO Mixer"},
	{"Line Amp", NULL, "SPKLO Mixer"},
	{"SPKLO MUX", "Speaker", "SPK Amp"},
	{"SPKLO MUX", "Line", "Line Amp"},
	{"SPKLO", NULL, "SPKLO MUX"},

};

#if (!defined(PLL_16BICK_MODE) & !defined(PLL_32BICK_MODE) & !defined(PLL_64BICK_MODE) & !defined(PLL_MCLK_MODE))
static int ak4637_set_mcki(struct snd_soc_codec *codec, int fs, int rclk)
{

	u8 mode;
	int mcki_rate;

	akdbgprt("\t[AK4637] %s fs=%d rclk=%d\n",__FUNCTION__, fs, rclk);

	if ((fs != 0)&&(rclk != 0)) {
		mcki_rate = rclk/fs;

		mode = snd_soc_read(codec, AK4637_06_MODE_CONTROL2);
		mode &= ~AK4637_CM;
		switch (mcki_rate) {
			case 256:
				mode |= AK4637_CM_0;
				break;
			case 384:
				mode |= AK4637_CM_1;
				break;
			case 512:
				mode |= AK4637_CM_2;
				break;
			case 1024:
				mode |= AK4637_CM_3;
				break;
			default:
				return(-1);
		}
		snd_soc_write(codec, AK4637_06_MODE_CONTROL2, mode);
	}
	return(0);

}
#endif

static int ak4637_hw_params(struct snd_pcm_substream *substream,
		struct snd_pcm_hw_params *params,
		struct snd_soc_dai *dai)
{
	struct snd_soc_codec *codec = dai->codec;
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
 
	u8 	fs;

	akdbgprt("\t[AK4637] %s(%d)\n",__FUNCTION__,__LINE__);

	fs = snd_soc_read(codec, AK4637_06_MODE_CONTROL2);
	fs &= ~AK4637_FS;

	ak4637->fs2 = params_rate(params);

	switch (ak4637->fs2) {
	case 8000:
		fs |= AK4637_FS_8KHZ;
		break;
	case 11025:
		fs |= AK4637_FS_11_025KHZ;
		break;
	case 12000:
		fs |= AK4637_FS_12KHZ;
		break;
	case 16000:
		fs |= AK4637_FS_16KHZ;
		break;
	case 22050:
		fs |= AK4637_FS_22_05KHZ;
		break;
	case 32000:
		fs |= AK4637_FS_32KHZ;
		break;
	case 44100:
		fs |= AK4637_FS_44_1KHZ;
		break;
	case 48000:
		fs |= AK4637_FS_48KHZ;
		break;
	default:
		return -EINVAL;
	}

	snd_soc_write(codec, AK4637_06_MODE_CONTROL2, fs);

	ak4637->fs2 = params_rate(params);

#if (!defined(PLL_16BICK_MODE) & !defined(PLL_32BICK_MODE) & !defined(PLL_64BICK_MODE) & !defined(PLL_MCLK_MODE))
	ak4637_set_mcki(codec, ak4637->fs2, ak4637->rclk);
#endif

	return 0;
}

static int ak4637_set_dai_sysclk(struct snd_soc_dai *dai, int clk_id,
		unsigned int freq, int dir)
{
	struct snd_soc_codec *codec = dai->codec;
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
	u8 pll;

	akdbgprt("\t[AK4637] %s(%d)\n",__FUNCTION__,__LINE__);

	pll = snd_soc_read(codec, AK4637_05_MODE_CONTROL1);
	pll &= ~AK4637_PLL;

#ifdef PLL_16BICK_MODE
	pll |= AK4637_PLL_BICK16;
#else
#ifdef PLL_32BICK_MODE
	pll |= AK4637_PLL_BICK32;
#else
#ifdef PLL_64BICK_MODE
	pll |= AK4637_PLL_BICK64;
#else
#ifdef PLL_MCLK_MODE
	pll |= AK4637_PLL_MCLK_FREQ;	//user select
#else
	pll |= AK4637_EXT_SLAVE;
#endif
#endif
#endif
#endif

	snd_soc_write(codec, AK4637_05_MODE_CONTROL1, pll);

	ak4637->rclk = freq;

#if (!defined(PLL_16BICK_MODE) & !defined(PLL_32BICK_MODE) & !defined(PLL_64BICK_MODE) & !defined(PLL_MCLK_MODE))
	ak4637_set_mcki(codec, ak4637->fs2, ak4637->rclk);
#endif

	return 0;
}

static int ak4637_set_dai_fmt(struct snd_soc_dai *dai, unsigned int fmt)
{

	struct snd_soc_codec *codec = dai->codec;
	u8 mode;
	u8 format, format2;

	akdbgprt("\t[AK4637] %s(%d)\n",__FUNCTION__,__LINE__);

	/* set master/slave audio interface */
	mode = snd_soc_read(codec, AK4637_01_POWER_MANAGEMENT2);
	format2 = snd_soc_read(codec, AK4637_05_MODE_CONTROL1);
	format = snd_soc_read(codec, AK4637_07_MODE_CONTROL3);

	format &= ~AK4637_DIF;

    switch (fmt & SND_SOC_DAIFMT_MASTER_MASK) {
        case SND_SOC_DAIFMT_CBS_CFS:
 			akdbgprt("\t[AK4637] %s(Slave)\n",__FUNCTION__);
            mode &= ~AK4637_M_S;
            break;
        case SND_SOC_DAIFMT_CBM_CFM:
			akdbgprt("\t[AK4637] %s(Master)\n",__FUNCTION__);
            mode |= AK4637_M_S;
            format &= ~AK4637_CKOFF;
            break;
        case SND_SOC_DAIFMT_CBS_CFM:
        case SND_SOC_DAIFMT_CBM_CFS:
        default:
            dev_err(codec->dev, "Clock mode unsupported");
           return -EINVAL;
    	}

	switch (fmt & SND_SOC_DAIFMT_FORMAT_MASK) {
		case SND_SOC_DAIFMT_I2S:
			format |= AK4637_DIF_I2S_MODE;
			break;
		case SND_SOC_DAIFMT_LEFT_J:
			format |= AK4637_DIF_MSB_MODE;
			break;
		case SND_SOC_DAIFMT_DSP_A:
			format |= AK4637_DIF_PCM_SHORT_MODE;
			break;
		default:
			return -EINVAL;
	}

	/* set mode and format */
	snd_soc_write(codec, AK4637_01_POWER_MANAGEMENT2, mode);
	snd_soc_write(codec, AK4637_05_MODE_CONTROL1, format2);
	snd_soc_write(codec, AK4637_07_MODE_CONTROL3, format);

	return 0;
}

static bool ak4637_volatile(struct device *dev, unsigned int reg) // '16/01/22
{
	int	ret;

	switch (reg) {
//		case :
//			ret = 1;
		default:
			ret = 0;
			break;
	}
	return(ret);
}

static bool ak4637_writeable(struct device *dev, unsigned int reg) // '16/01/22
{
	bool ret;

	if (  reg <= AK4637_MAX_REGISTERS  ) {
		ret = 1;
	}
	else ret = 0;

	return ret;
}

#ifdef AK4637_CONTIF_DEBUG

unsigned int ak4637_i2c_read(struct snd_soc_codec *codec, unsigned int reg)	//16/05/25
{
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
	int ret = -1;
	unsigned char tx[1], rx[1];

	struct i2c_msg xfer[2];
	struct i2c_client *client = ak4637->i2c;

	tx[0] = reg;
	rx[0] = 0;

	/* Write register */
	xfer[0].addr = client->addr;
	xfer[0].flags = 0;
	xfer[0].len = 1;
	xfer[0].buf = tx;

	/* Read data */
	xfer[1].addr = client->addr;
	xfer[1].flags = I2C_M_RD;
	xfer[1].len = 1;
	xfer[1].buf = rx;

	ret = i2c_transfer(client->adapter, xfer, 2);

	if( ret != 2 ){
		akdbgprt("\t[ak4637] %s error ret = %d \n", __FUNCTION__, ret );
	}

	return (unsigned int)rx[0];

}

static int ak4637_i2c_write(struct snd_soc_codec *codec, unsigned int reg,
	unsigned int value)	//16/05/25
{
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
	int ret = -1;
	unsigned char tx[2];
	size_t	len = 2;

	tx[0] = reg;
	tx[1] = value;

	akdbgprt("\t[ak4637] %s: (addr,data)=(%x, %x)\n",__FUNCTION__, reg, value);

	ret = i2c_master_send( ak4637->i2c, tx, len );
	
	if( ret != len ){
		return -EIO;
	}	
	return 0;
}
#endif

static int ak4637_firmware_write_reg(struct snd_soc_codec *codec, u16 mode, u16 cmd)
{
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
	int ret = 0;

	u8 StartReg;
	u8 StopReg;

	int nNumMode, nFirmLen;
	int i;
	const struct firmware *fw;
	u8  *fwdn;
	char szFileName[32];
	
	akdbgprt("[AK4637] %s mode=%d, cmd=%d\n",__FUNCTION__, mode, cmd);


	nNumMode = sizeof(ak4637_firmware_texts) / sizeof(ak4637_firmware_texts[0]);

	if ( cmd >= nNumMode ) {
		pr_err("%s: invalid command %d\n", __func__, cmd);
		return( -EINVAL);
	}

	switch(mode) {
		case FIRMTYPE_HPF2:
			StartReg =AK4637_19_HPF2_COEFFICIENT0;
			StopReg = AK4637_1C_HPF2_COEFFICIENT3;
			sprintf(szFileName, "ak4637_hpf2_%s.bin", ak4637_firmware_texts[cmd]);
			break;
		case FIRMTYPE_LPF:
			StartReg =AK4637_1D_LPF_COEFFICIENT0;
			StopReg = AK4637_20_LPF_COEFFICIENT3;
			sprintf(szFileName, "ak4637_lpf_%s.bin", ak4637_firmware_texts[cmd]);
			break;
		case FIRMTYPE_EQ1:
			StartReg =AK4637_22_E1_COEFFICIENT0;
			StopReg = AK4637_27_E1_COEFFICIENT5;
			sprintf(szFileName, "ak4637_eq1_%s.bin", ak4637_firmware_texts[cmd]);
			break;
		case FIRMTYPE_EQ2:
			StartReg =AK4637_28_E2_COEFFICIENT0;
			StopReg = AK4637_2D_E2_COEFFICIENT5;
			sprintf(szFileName, "ak4637_eq2_%s.bin", ak4637_firmware_texts[cmd]);
			break;
		case FIRMTYPE_EQ3:
			StartReg =AK4637_2E_E3_COEFFICIENT0;
			StopReg = AK4637_33_E3_COEFFICIENT5;
			sprintf(szFileName, "ak4637_eq3_%s.bin", ak4637_firmware_texts[cmd]);
			break;
		case FIRMTYPE_EQ4:
			StartReg =AK4637_34_E4_COEFFICIENT0;
			StopReg = AK4637_39_E4_COEFFICIENT5;
			sprintf(szFileName, "ak4637_eq4_%s.bin", ak4637_firmware_texts[cmd]);
			break;
		case FIRMTYPE_EQ5:
			StartReg =AK4637_3A_E5_COEFFICIENT0;
			StopReg = AK4637_3F_E5_COEFFICIENT5;
			sprintf(szFileName, "ak4637_eq5_%s.bin", ak4637_firmware_texts[cmd]);
			break;
		default:
			return( -EINVAL);
	}



	nFirmLen = (int)StopReg - (int)StartReg + 1;

	if ( cmd == 0 ) {
		for (i =0; i < nFirmLen ; i ++) {
			snd_soc_write(codec, StartReg + i, ak4637_reg[StartReg + i].def);  // '16/01/22
		}
	}
	else {
		akdbgprt("[AK4637] %s file=%s\n",__FUNCTION__, szFileName);
		ret = request_firmware(&fw, szFileName, &(ak4637->i2c->dev));

		akdbgprt("[AK4637] %s request\n",__FUNCTION__);

		if (ret) {
			akdbgprt("[AK4637] %s could not load firmware=%d\n", szFileName, ret);
			return -EINVAL;
		}

		akdbgprt("[AK4637] %s name=%s size=%d\n",__FUNCTION__, szFileName, (int)fw->size);
		if ( fw->size != nFirmLen ) {
			akdbgprt("[AK4637] %s Firmware Size Error : %d\n",__FUNCTION__, (int)fw->size);
			return -ENOMEM;
		}

		fwdn = kmalloc((unsigned long)fw->size, GFP_KERNEL);
		if (fwdn == NULL) {
			printk(KERN_ERR "failed to buffer vmalloc: %d\n", (int)fw->size);
			return -ENOMEM;
		}

		memcpy((void *)fwdn, fw->data, fw->size);
			
		for (i =0; i < nFirmLen ; i ++) {
			snd_soc_write(codec, StartReg + i, fwdn[i]);
		}
		kfree(fwdn);
	}
	return ret;
}

// * for AK4637
static int ak4637_trigger(struct snd_pcm_substream *substream, int cmd, struct snd_soc_dai *codec_dai)
{
	int 	ret = 0;
 //   struct snd_soc_codec *codec = codec_dai->codec;

	akdbgprt("\t[AK4637] %s(%d)\n",__FUNCTION__,__LINE__);

	return ret;
}


static int ak4637_set_bias_level(struct snd_soc_codec *codec,
		enum snd_soc_bias_level level)
{
	u8 reg;

	akdbgprt("\t[AK4637] %s(%d)\n",__FUNCTION__,__LINE__);

	switch (level) {
	case SND_SOC_BIAS_ON:
	case SND_SOC_BIAS_PREPARE:
	case SND_SOC_BIAS_STANDBY:
		reg = snd_soc_read(codec, AK4637_00_POWER_MANAGEMENT1);	// * for AK4637
		snd_soc_write(codec, AK4637_00_POWER_MANAGEMENT1,			// * for AK4637
				reg | AK4637_PMVCM);
		break;
	case SND_SOC_BIAS_OFF:
		snd_soc_write(codec, AK4637_00_POWER_MANAGEMENT1, 0x00);	// * for AK4637
		break;
	}
	codec->dapm.bias_level = level;
	return 0;
}

static int ak4637_set_dai_mute(struct snd_soc_dai *dai, int mute) {
    struct snd_soc_codec *codec = dai->codec;
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);	
	int ret = 0;
	int ndt;
		
	akdbgprt("\t[AK4637] %s mute[%s]\n",__FUNCTION__, mute ? "ON":"OFF");
	
	if (mute) {
		//SMUTE: 1 , MUTE
		ret = snd_soc_update_bits(codec, AK4637_07_MODE_CONTROL3, 0x20, 0x20); 
		ndt = ak4637->dvtm / ak4637->fs2;
		mdelay(ndt);
	}
	else
		// SMUTE:  0  ,NORMAL operation
		ret =snd_soc_update_bits(codec, AK4637_07_MODE_CONTROL3, 0x20, 0x00); 

	return ret;
        
}

#define AK4637_RATES		(SNDRV_PCM_RATE_8000 | SNDRV_PCM_RATE_11025 |\
				SNDRV_PCM_RATE_16000 | SNDRV_PCM_RATE_22050 |\
				SNDRV_PCM_RATE_32000 | SNDRV_PCM_RATE_44100 |\
				SNDRV_PCM_RATE_48000)

#define AK4637_FORMATS		SNDRV_PCM_FMTBIT_S16_LE | SNDRV_PCM_FMTBIT_S24_LE


static struct snd_soc_dai_ops ak4637_dai_ops = {
	.hw_params	= ak4637_hw_params,
	.set_sysclk	= ak4637_set_dai_sysclk,
	.set_fmt	= ak4637_set_dai_fmt,
	.trigger = ak4637_trigger,
	.digital_mute = ak4637_set_dai_mute,
};

struct snd_soc_dai_driver ak4637_dai[] = {   
	{										 
		.name = "ak4637-aif",
		.playback = {
		       .stream_name = "Playback",
		       .channels_min = 1,
		       .channels_max = 1, // Monaural
		       .rates = AK4637_RATES,
		       .formats = AK4637_FORMATS,
		},
		.capture = {
		       .stream_name = "Capture",
		       .channels_min = 1,
		       .channels_max = 1, // Monaural
		       .rates = AK4637_RATES,
		       .formats = AK4637_FORMATS,
		},
		.ops = &ak4637_dai_ops,
	},										 
};

static int ak4637_write_cache_reg(
struct snd_soc_codec *codec,
u16  regs,
u16  rege)
{
	u16	reg; 
	u32 cache_data;

	reg = regs;
	do {
//'16/01/22
//		cache_data = ak4637_read_reg_cache(codec, reg);
		cache_data = ak4637_reg[reg].def;
//
		snd_soc_write(codec, (unsigned int)reg, (unsigned int)cache_data);
		reg ++;
	} while (reg <= rege);

	return(0);
}

static int ak4637_init_reg(struct snd_soc_codec *codec)
{
#ifdef AK4637_PDN_GPIO
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
#endif
	akdbgprt("\t[AK4637 bias] %s(%d)\n",__FUNCTION__,__LINE__);

#ifdef AK4637_PDN_GPIO
	gpio_set_value(ak4637->pdn_gpio, 0);	
	msleep(1);
	gpio_set_value(ak4637->pdn_gpio, 1);	
	msleep(1);
#endif

	snd_soc_write(codec, 0x0, 0x0);   // Dummy Commmand

//	ak4637_set_bias_level(codec, SND_SOC_BIAS_STANDBY);//comment out 16/05/25

	snd_soc_write(codec, AK4637_0D_INPUT_VOLUME_CONTROL, 0x91);	//IVL=91H(0dB)

	ak4637_write_cache_reg(codec, AK4637_0A_ALC_TIMER_SELECT, AK4637_0C_ALC_MODE_CONTROL2); //ALC
	ak4637_write_cache_reg(codec, AK4637_19_HPF2_COEFFICIENT0, AK4637_20_LPF_COEFFICIENT3);	//Digital Filter
	ak4637_write_cache_reg(codec, AK4637_22_E1_COEFFICIENT0, AK4637_3F_E5_COEFFICIENT5);	//Digital Filter
	
	return(0);

}

#ifdef AK4637_PDN_GPIO
static int ak4637_parse_dt(struct ak4637_priv *ak4637)
{
	struct device *dev;
	struct device_node *np;

	dev = &(ak4637->i2c->dev);

	np = dev->of_node;

	if (!np)
		return -1;

	printk("Read PDN pin from device tree\n");

	ak4637->pdn_gpio = of_get_named_gpio(np, "ak4637,pdn-gpio", 0);
	if (ak4637->pdn_gpio < 0) {
		ak4637->pdn_gpio = -1;
		return -1;
	}

	if( !gpio_is_valid(ak4637->pdn_gpio) ) {
		printk(KERN_ERR "ak4637 pdn pin(%u) is invalid\n", ak4637->pdn_gpio);
		return -1;
	}

	return 0;
}
#endif

static int ak4637_probe(struct snd_soc_codec *codec)
{
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
	int ret = 0;

	akdbgprt("\t[AK4637] %s(%d)\n",__FUNCTION__,__LINE__);


#ifdef AK4637_PDN_GPIO	
	ret = ak4637_parse_dt(ak4637);
	if ( ret < 0 ) ak4637->pdn_gpio = -1;
	ret = gpio_request(ak4637->pdn_gpio, "ak4637 pdn");
	akdbgprt("\t[AK4637] %s : gpio_request ret = %d\n",__FUNCTION__, ret);
	gpio_direction_output(ak4637->pdn_gpio, 0);
#endif

	akdbgprt("\t[AK4637] %s(%d) ak4637=%p\n",__FUNCTION__,__LINE__, ak4637);

	ak4637_init_reg(codec);

	akdbgprt("\t[AK4637 Effect] %s(%d)\n",__FUNCTION__,__LINE__);

	ak4637->fs2 = 48000;
	ak4637->mGain = 6;
	ak4637->rclk = 0;
	ak4637->dvtm = 816000;

    return ret;

}

static int ak4637_remove(struct snd_soc_codec *codec)
{
#ifdef AK4637_PDN_GPIO
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
#endif
	akdbgprt("\t[AK4637] %s(%d)\n",__FUNCTION__,__LINE__);

	ak4637_set_bias_level(codec, SND_SOC_BIAS_OFF);
#ifdef AK4637_PDN_GPIO
	gpio_set_value(ak4637->pdn_gpio, 0);
	msleep(1);
	gpio_free(ak4637->pdn_gpio);
	msleep(1);
#endif

	return 0;
}

static int ak4637_suspend(struct snd_soc_codec *codec)  // '16/01/22
{
#ifdef AK4637_PDN_GPIO
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);
#endif
	ak4637_set_bias_level(codec, SND_SOC_BIAS_OFF);
#ifdef AK4637_PDN_GPIO
	gpio_set_value(ak4637->pdn_gpio, 0);
	msleep(1);
#endif

	return 0;
}

static int ak4637_resume(struct snd_soc_codec *codec)
{
	struct ak4637_priv *ak4637 = snd_soc_codec_get_drvdata(codec);	
	int i;

	regcache_cache_only(ak4637->regmap, true);
	for ( i = 0 ; i < ARRAY_SIZE(ak4637_reg) ; i++ ) {
		regmap_write(ak4637->regmap, ak4637_reg[i].reg, ak4637_reg[i].def);
	}
	regcache_cache_only(ak4637->regmap, false);

	ak4637_init_reg(codec);

	return 0;
}


struct snd_soc_codec_driver soc_codec_dev_ak4637 = {
	.probe = ak4637_probe,
	.remove = ak4637_remove,
	.suspend =	ak4637_suspend,
	.resume =	ak4637_resume,


#ifdef AK4637_CONTIF_DEBUG	//16/05/25
	.write = ak4637_i2c_write,
	.read = ak4637_i2c_read,
#endif


	.idle_bias_off = true,   // 16/01/22
	.set_bias_level = ak4637_set_bias_level,

	.controls = ak4637_snd_controls,
	.num_controls = ARRAY_SIZE(ak4637_snd_controls),
	.dapm_widgets = ak4637_dapm_widgets,
	.num_dapm_widgets = ARRAY_SIZE(ak4637_dapm_widgets),
	.dapm_routes = ak4637_intercon,
	.num_dapm_routes = ARRAY_SIZE(ak4637_intercon),
};
EXPORT_SYMBOL_GPL(soc_codec_dev_ak4637);

static const struct regmap_config ak4637_regmap = { // '16/01/22
	.reg_bits = 8,
	.val_bits = 8,

	.max_register = AK4637_MAX_REGISTERS,
	.volatile_reg = ak4637_volatile,
	.writeable_reg = ak4637_writeable,

	.reg_defaults = ak4637_reg,
	.num_reg_defaults = ARRAY_SIZE(ak4637_reg),
	.cache_type = REGCACHE_RBTREE,
};

static struct of_device_id ak4637_i2c_dt_ids[] = {
	{ .compatible = "akm,ak4637"},
    { }
};
MODULE_DEVICE_TABLE(of, ak4637_i2c_dt_ids);

static int ak4637_i2c_probe(struct i2c_client *i2c,
                            const struct i2c_device_id *id)
{
	struct ak4637_priv *ak4637;
//	struct snd_soc_codec *codec;
	int ret=0;
	
	akdbgprt("\t[AK4637] %s(%d)\n",__FUNCTION__,__LINE__);

	ak4637 = devm_kzalloc(&i2c->dev, sizeof(struct ak4637_priv), GFP_KERNEL);
	if (ak4637 == NULL) return -ENOMEM;

	ak4637->regmap = devm_regmap_init_i2c(i2c, &ak4637_regmap); // '16/01/22
	if (IS_ERR(ak4637->regmap)) {
		devm_kfree(&i2c->dev, ak4637);
		return PTR_ERR(ak4637->regmap);
	}

	i2c_set_clientdata(i2c, ak4637);
	ak4637->i2c = i2c;
	
	ret = snd_soc_register_codec(&i2c->dev,
			&soc_codec_dev_ak4637, &ak4637_dai[0], ARRAY_SIZE(ak4637_dai));
	if (ret < 0){
		devm_kfree(&i2c->dev, ak4637);
		akdbgprt("\t[AK4637 Error!] %s(%d)\n",__FUNCTION__,__LINE__);
	}
	return ret;
}

static int ak4637_i2c_remove(struct i2c_client *client)  // '16/01/22
{
	snd_soc_unregister_codec(&client->dev);

	return 0;
}

static const struct i2c_device_id ak4637_i2c_id[] = {
	{ "ak4637", 0 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, ak4637_i2c_id);

static struct i2c_driver ak4637_i2c_driver = {
	.driver = {
		.name = "ak4637",
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(ak4637_i2c_dt_ids),
	},
	.probe = ak4637_i2c_probe,
	.remove = ak4637_i2c_remove,  // '16/01/22
	.id_table = ak4637_i2c_id,
};

static int __init ak4637_modinit(void)
{

	akdbgprt("\t[AK4637] %s(%d)\n", __FUNCTION__,__LINE__);

	return i2c_add_driver(&ak4637_i2c_driver);
}

module_init(ak4637_modinit);

static void __exit ak4637_exit(void)
{
	i2c_del_driver(&ak4637_i2c_driver);
}
module_exit(ak4637_exit);

MODULE_DESCRIPTION("ASoC ak4637 codec driver");
MODULE_LICENSE("GPL v2");
