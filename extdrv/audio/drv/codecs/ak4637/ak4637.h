/*
 * ak4637.h  --  audio driver for ak4637
 *
 * Copyright (C) 2015 Asahi Kasei Microdevices Corporation
 *  Author                Date        Revision
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *                      15/07/02    1.0
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 *
 */

#ifndef _AK4637_H
#define _AK4637_H

#include <mach/config.h>

#define AK4637_PCM_BCKP // BICK Pos edge setting for PCM format

//#define PLL_16BICK_MODE
//#define PLL_32BICK_MODE
//#define PLL_64BICK_MODE
//#define PLL_MCLK_MODE

#ifdef CONFIG_HC1892_MS700_2
#define PLL_32BICK_MODE
#else
#define PLL_MCLK_MODE
#endif

#define  AK4637_00_POWER_MANAGEMENT1         0x00
#define  AK4637_01_POWER_MANAGEMENT2         0x01
#define  AK4637_02_SIGNAL_SELECT1            0x02
#define  AK4637_03_SIGNAL_SELECT2            0x03
#define  AK4637_04_SIGNAL_SELECT3            0x04
#define  AK4637_05_MODE_CONTROL1             0x05
#define  AK4637_06_MODE_CONTROL2             0x06
#define  AK4637_07_MODE_CONTROL3             0x07
#define  AK4637_08_DIGITAL_MIC               0x08
#define  AK4637_09_TIMER_SELECT              0x09
#define  AK4637_0A_ALC_TIMER_SELECT          0x0A
#define  AK4637_0B_ALC_MODE_CONTROL1         0x0B
#define  AK4637_0C_ALC_MODE_CONTROL2         0x0C
#define  AK4637_0D_INPUT_VOLUME_CONTROL      0x0D
#define  AK4637_0E_ALC_VOLUME                0x0E
#define  AK4637_0F_BEEP_CONTROL              0x0F
#define  AK4637_10_DIGITAL_VOLUME_CONTROL    0x10
#define  AK4637_11_EQ_COMMON_GAIN_SELECT     0x11
#define  AK4637_12_EQ2_COMMON_GAIN_SETTING   0x12
#define  AK4637_13_EQ3_COMMON_GAIN_SETTING   0x13
#define  AK4637_14_EQ4_COMMON_GAIN_SETTING   0x14
#define  AK4637_15_EQ5_COMMON_GAIN_SETTING   0x15
#define  AK4637_16_DIGITAL_FILTER_SELECT1    0x16
#define  AK4637_17_DIGITAL_FILTER_SELECT2    0x17
#define  AK4637_18_DIGITAL_FILTER_MODE       0x18
#define  AK4637_19_HPF2_COEFFICIENT0         0x19
#define  AK4637_1A_HPF2_COEFFICIENT1         0x1A
#define  AK4637_1B_HPF2_COEFFICIENT2         0x1B
#define  AK4637_1C_HPF2_COEFFICIENT3         0x1C
#define  AK4637_1D_LPF_COEFFICIENT0          0x1D
#define  AK4637_1E_LPF_COEFFICIENT1          0x1E
#define  AK4637_1F_LPF_COEFFICIENT2          0x1F
#define  AK4637_20_LPF_COEFFICIENT3          0x20
#define  AK4637_21_DIGITAL_FILTER_SELECT3    0x21
#define  AK4637_22_E1_COEFFICIENT0           0x22
#define  AK4637_23_E1_COEFFICIENT1           0x23
#define  AK4637_24_E1_COEFFICIENT2           0x24
#define  AK4637_25_E1_COEFFICIENT3           0x25
#define  AK4637_26_E1_COEFFICIENT4           0x26
#define  AK4637_27_E1_COEFFICIENT5           0x27
#define  AK4637_28_E2_COEFFICIENT0           0x28
#define  AK4637_29_E2_COEFFICIENT1           0x29
#define  AK4637_2A_E2_COEFFICIENT2           0x2A
#define  AK4637_2B_E2_COEFFICIENT3           0x2B
#define  AK4637_2C_E2_COEFFICIENT4           0x2C
#define  AK4637_2D_E2_COEFFICIENT5           0x2D
#define  AK4637_2E_E3_COEFFICIENT0           0x2E
#define  AK4637_2F_E3_COEFFICIENT1           0x2F
#define  AK4637_30_E3_COEFFICIENT2           0x30
#define  AK4637_31_E3_COEFFICIENT3           0x31
#define  AK4637_32_E3_COEFFICIENT4           0x32
#define  AK4637_33_E3_COEFFICIENT5           0x33
#define  AK4637_34_E4_COEFFICIENT0           0x34
#define  AK4637_35_E4_COEFFICIENT1           0x35
#define  AK4637_36_E4_COEFFICIENT2           0x36
#define  AK4637_37_E4_COEFFICIENT3           0x37
#define  AK4637_38_E4_COEFFICIENT4           0x38
#define  AK4637_39_E4_COEFFICIENT5           0x39
#define  AK4637_3A_E5_COEFFICIENT0           0x3A
#define  AK4637_3B_E5_COEFFICIENT1           0x3B
#define  AK4637_3C_E5_COEFFICIENT2           0x3C
#define  AK4637_3D_E5_COEFFICIENT3           0x3D
#define  AK4637_3E_E5_COEFFICIENT4           0x3E
#define  AK4637_3F_E5_COEFFICIENT5           0x3F

#define AK4637_MAX_REGISTERS	(AK4637_3F_E5_COEFFICIENT5 + 1)

/* Bitfield Definitions */

/* AK4637_00_POWER_MANAGEMENT1 (0x00) Fields */
#define AK4637_PMVCM				0x40

/* AK4637_01_POWER_MANAGEMENT2 (0x01) Fields */
#define AK4637_M_S					0x08

/* AK4637_07_MODE_CONTROL1 (0x07) Fields */
#define AK4637_DIF					0x0F

#define AK4637_DIF_MSB_LSB_MODE     (1 << 0)
#define AK4637_DIF_MSB_MODE         (2 << 0)
#define AK4637_DIF_I2S_MODE         (3 << 0)


#ifndef AK4637_PCM_BCKP
#define AK4637_DIF_16DSP_MODE		(4 << 0)
#define AK4637_DIF_PCM_SHORT_MODE	(C << 0)
#else
#define AK4637_DIF_16DSP_MODE		(0 << 0)
#define AK4637_DIF_PCM_SHORT_MODE	(8 << 0)

#define AK4637_CKOFF				0x04

/* PLL bits */
#define AK4637_PLL					0xF0
#define AK4637_EXT_SLAVE			(0 << 4)
#define AK4637_PLL_BICK16			(1 << 4)
#define AK4637_PLL_BICK32			(2 << 4)
#define AK4637_PLL_BICK64			(3 << 4)
#define AK4637_PLL_11_2896MHZ		(4 << 4)
#define AK4637_PLL_12MHZ			(6 << 4)
#define AK4637_PLL_12_288MHZ		(5 << 4)
#define AK4637_PLL_13_5MHZ			(12 << 4)
#define AK4637_PLL_24MHZ			(7 << 4)
#define AK4637_PLL_27MHZ			(13 << 4)

#define AK4637_PLL_MCLK_FREQ    AK4637_PLL_12MHZ   //MCLK Frequency Select

/*FS bits*/
/* AK4637_06_MODE_CONTROL2 (0x06) Fields */
#ifdef PLL_16BICK_MODE // PLLREFCK input : BICK pin
#define AK4637_FS				0x0C
#define AK4637_FS_8KHZ			(0x00 << 0)
#define AK4637_FS_11_025KHZ		(0x00 << 0)
#define AK4637_FS_12KHZ			(0x00 << 0)
#define AK4637_FS_16KHZ			(0x04 << 0)
#define AK4637_FS_22_05KHZ		(0x04 << 0)
#define AK4637_FS_24KHZ			(0x04 << 0)
#define AK4637_FS_32KHZ			(0x08 << 0)
#define AK4637_FS_44_1KHZ		(0x08 << 0)
#define AK4637_FS_48KHZ			(0x08 << 0)
#else
#ifdef PLL_32BICK_MODE // PLLREFCK input : BICK pin
#define AK4637_FS				0x0C
#define AK4637_FS_8KHZ			(0x00 << 0)
#define AK4637_FS_11_025KHZ		(0x00 << 0)
#define AK4637_FS_12KHZ			(0x00 << 0)
#define AK4637_FS_16KHZ			(0x04 << 0)
#define AK4637_FS_22_05KHZ		(0x04 << 0)
#define AK4637_FS_24KHZ			(0x04 << 0)
#define AK4637_FS_32KHZ			(0x08 << 0)
#define AK4637_FS_44_1KHZ		(0x08 << 0)
#define AK4637_FS_48KHZ			(0x08 << 0)
#else
#ifdef PLL_64BICK_MODE // PLLREFCK input : BICK pin
#define AK4637_FS				0x0C
#define AK4637_FS_8KHZ			(0x00 << 0)
#define AK4637_FS_11_025KHZ		(0x00 << 0)
#define AK4637_FS_12KHZ			(0x00 << 0)
#define AK4637_FS_16KHZ			(0x04 << 0)
#define AK4637_FS_22_05KHZ		(0x04 << 0)
#define AK4637_FS_24KHZ			(0x04 << 0)
#define AK4637_FS_32KHZ			(0x08 << 0)
#define AK4637_FS_44_1KHZ		(0x08 << 0)
#define AK4637_FS_48KHZ			(0x08 << 0)
#else
#ifdef PLL_MCLK_MODE // PLLREFCK input : MCKI pin 
#define AK4637_FS				0x0F
#define AK4637_FS_8KHZ			(0x01 << 0)
#define AK4637_FS_11_025KHZ		(0x02 << 0)
#define AK4637_FS_12KHZ			(0x03 << 0)
#define AK4637_FS_16KHZ			(0x05 << 0)
#define AK4637_FS_22_05KHZ		(0x06 << 0)
#define AK4637_FS_24KHZ			(0x07 << 0)
#define AK4637_FS_32KHZ			(0x09 << 0)
#define AK4637_FS_44_1KHZ		(0x0A << 0)
#define AK4637_FS_48KHZ			(0x0B << 0)
#else
#define AK4637_FS				0x0C
#define AK4637_FS_8KHZ			(0x00 << 0)
#define AK4637_FS_11_025KHZ		(0x00 << 0)
#define AK4637_FS_12KHZ			(0x00 << 0)
#define AK4637_FS_16KHZ			(0x04 << 0)
#define AK4637_FS_22_05KHZ		(0x04 << 0)
#define AK4637_FS_24KHZ			(0x04 << 0)
#define AK4637_FS_32KHZ			(0x08 << 0)
#define AK4637_FS_44_1KHZ		(0x08 << 0)
#define AK4637_FS_48KHZ			(0x08 << 0)
#endif	// PLL_MCLK_MODE
#endif	// PLL_64BICK_MODE
#endif	// PLL_32BICK_MODE
#endif  // PLL_16BICK_MODE
#endif

#define AK4637_CM		(0x03 << 6)
#define AK4637_CM_0		(0 << 6)
#define AK4637_CM_1		(1 << 6)
#define AK4637_CM_2		(2 << 6)
#define AK4637_CM_3		(3 << 6)

#define FIRMTYPE_HPF2	0
#define FIRMTYPE_LPF	1
#define FIRMTYPE_EQ1 	2
#define FIRMTYPE_EQ2 	3
#define FIRMTYPE_EQ3 	4
#define FIRMTYPE_EQ4 	5
#define FIRMTYPE_EQ5 	6

static const char *ak4637_firmware_texts[] =
{
    "default",
    "data1",
    "data2",
    "data3",
    "data4",
    "data5",
};

#endif
