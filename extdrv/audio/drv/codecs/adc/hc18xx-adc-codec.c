#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/pm.h>
#include <linux/io.h>
#include <linux/slab.h>
#include <linux/regmap.h>
#include <linux/regulator/consumer.h>
#include <linux/spi/spi.h>
#include <linux/of_device.h>
#include <misc/hc-adc_ctrl.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/pcm_params.h>
#include <sound/soc.h>
#include <sound/initval.h>
#include <sound/tlv.h>

#include "hc18xx-adc-codec.h"

/* adc private data */
struct hc18xx_adc_drvdata {
	struct regmap *regmap;
	u32 __iomem *adc_base;
};
/*
static int hc18xx_adc_read_reg(void *context, unsigned int reg,
				 unsigned int *value)
{
	int ret = 0;
	return ret;
}

static int hc18xx_adc_write_reg(void *context, unsigned int reg,
				 unsigned int value)
{
	int ret = 0;
	pr_info("hc18xx_adc_write_reg: reg = 0x%X, value = 0x%X", reg, value);
	return ret;
}*/

/*
static int hc18xx_adc_control_get(struct snd_kcontrol *kcontrol,
		struct snd_ctl_elem_value *ucontrol) {
	pr_info("fn: %s\n", __func__);
	return 0;
}

static int hc18xx_adc_control_put(struct snd_kcontrol *kcontrol,
		struct snd_ctl_elem_value *ucontrol) {
	pr_info("fn: %s\n", __func__);
	return 0;
}
*/
static void hc18xx_adc_write_relaxed(u32 __iomem *base,
		unsigned int shift, unsigned int mask, unsigned int value) {
	unsigned int val;

	val = readl_relaxed(base);
	val &= ~(mask << shift);
	val |= (value << shift);

	writel_relaxed(val, base);
}

#define adc_write_reg(x,y) hc18xx_adc_write_relaxed( \
		                      base + x##_OFFSET, \
		                      x##_SHIFT, \
		                      x##_MASK, \
		                      y)

static unsigned int hc18xx_adc_read_reg(struct snd_soc_codec *codec,
		unsigned int reg) {
	struct hc18xx_adc_drvdata *drvdata = snd_soc_codec_get_drvdata(codec);
	unsigned int ret;

	ret = readl_relaxed(drvdata->adc_base + reg);
	return ret;
}

static int hc18xx_adc_write_reg(struct snd_soc_codec *codec,
		unsigned int reg, unsigned int value) {
	struct hc18xx_adc_drvdata *drvdata = snd_soc_codec_get_drvdata(codec);

	writel_relaxed(value, (void *)(drvdata->adc_base + reg));
	return 0;
}
/*
static const struct regmap_config hc18xx_adc_regmap = {
	.reg_read = hc18xx_adc_read_reg,
	.reg_write = hc18xx_adc_write_reg,
};
*/

#if 0
static const struct snd_kcontrol_new hc18xx_adc_snd_controls[] = {
SOC_SINGLE("Ch0 Capture Switch", HC18XX_ADC_TYPE, HC18XX_ADC_TYPE_CH0, 1, 0),
SOC_SINGLE("Ch1 Capture Switch", HC18XX_ADC_TYPE, HC18XX_ADC_TYPE_CH1, 1, 0),
SOC_SINGLE("Ch2 Capture Switch", HC18XX_ADC_TYPE, HC18XX_ADC_TYPE_CH2, 1, 0),
};
#endif

static const struct snd_soc_dapm_widget hc18xx_adc_dapm_widgets[] = {
SND_SOC_DAPM_ADC("ADC", "Capture", SND_SOC_NOPM, 0, 0),
#if 0
/*SND_SOC_DAPM_MIXER("ADC_CHAN_CTRL", SND_SOC_NOPM, 0, 0,
			&hc18xx_adc_adc_ch_controls[0],
			ARRAY_SIZE(hc18xx_adc_adc_ch_controls)),*/
SND_SOC_DAPM_INPUT("MICIN_CH0"),
SND_SOC_DAPM_INPUT("MICIN_CH1"),
SND_SOC_DAPM_INPUT("MICIN_CH2"),
#endif
};

static const struct snd_soc_dapm_route hc18xx_adc_route[] = {
#if 0
	{"ADC", NULL, "MICIN_CH0"},
	{"ADC", NULL, "MICIN_CH1"},
	{"ADC", NULL, "MICIN_CH2"},
/*	{"ADC", NULL, "ADC_CHAN_CTRL"},*/

	/* Mixer */
/*	{"ADC_CHAN_CTRL", "Ch0 Enable", "MICIN_CH0"},
	{"ADC_CHAN_CTRL", "Ch1 Enable", "MICIN_CH1"},
	{"ADC_CHAN_CTRL", "Ch2 Enable", "MICIN_CH2"},*/
#endif
};

static int hc18xx_adc_hw_params(struct snd_pcm_substream *substream,
			    struct snd_pcm_hw_params *params,
			    struct snd_soc_dai *dai)
{
	struct snd_soc_codec *codec = dai->codec;
	struct hc18xx_adc_drvdata *drvdata = snd_soc_codec_get_drvdata(codec);
	u32 __iomem *base = drvdata->adc_base;
	unsigned int rate;

	rate = params_rate(params);

	switch (rate) {
	case 8000:
		adc_write_reg(ADC_CLK_0_CYCLE, 15);
		adc_write_reg(ADC_CLK_1_CYCLE, 15);
		adc_write_reg(ADC_LENGTH, 25);
		adc_write_reg(ADC_CH_NUM, 4);
		break;
	case 11025:
		adc_write_reg(ADC_CLK_0_CYCLE, 16);
		adc_write_reg(ADC_CLK_1_CYCLE, 16);
		adc_write_reg(ADC_LENGTH, 17);
		adc_write_reg(ADC_CH_NUM, 4);
		break;
	case 16000:
		adc_write_reg(ADC_CLK_0_CYCLE, 15);
		adc_write_reg(ADC_CLK_1_CYCLE, 15);
		adc_write_reg(ADC_LENGTH, 25);
		adc_write_reg(ADC_CH_NUM, 2);
		break;
	case 22050:
		adc_write_reg(ADC_CLK_0_CYCLE, 16);
		adc_write_reg(ADC_CLK_1_CYCLE, 16);
		adc_write_reg(ADC_LENGTH, 17);
		adc_write_reg(ADC_CH_NUM, 2);
		break;
	case 32000:
		adc_write_reg(ADC_CLK_0_CYCLE, 8);
		adc_write_reg(ADC_CLK_1_CYCLE, 7);
		adc_write_reg(ADC_LENGTH, 25);
		adc_write_reg(ADC_CH_NUM, 2);
		break;
	case 44100:
		adc_write_reg(ADC_CLK_0_CYCLE, 8);
		adc_write_reg(ADC_CLK_1_CYCLE, 8);
		adc_write_reg(ADC_LENGTH, 17);
		adc_write_reg(ADC_CH_NUM, 2);
		break;
	case 48000:
		adc_write_reg(ADC_CLK_0_CYCLE, 5);
		adc_write_reg(ADC_CLK_1_CYCLE, 5);
		adc_write_reg(ADC_LENGTH, 25);
		adc_write_reg(ADC_CH_NUM, 2);
		break;
	default:
		break;
	}

	return 0;
}

static int hc18xx_adc_set_dai_fmt(struct snd_soc_dai *codec_dai,
		unsigned int fmt)
{
/*	struct snd_soc_codec *codec = codec_dai->codec; */

	/* interface format */
/*	switch (fmt & SND_SOC_DAIFMT_FORMAT_MASK) {
	case SND_SOC_DAIFMT_RIGHT_J:
		break;
	case SND_SOC_DAIFMT_LEFT_J:
		break;
	case SND_SOC_DAIFMT_DSP_A:
		break;
	case SND_SOC_DAIFMT_DSP_B:
		break;
	default:
		return -EINVAL;
	}
*/
	/* clock inversion */
/*	switch (fmt & SND_SOC_DAIFMT_INV_MASK) {
	case SND_SOC_DAIFMT_NB_NF:
		break;
	case SND_SOC_DAIFMT_IB_IF:
		break;
	case SND_SOC_DAIFMT_IB_NF:
		break;
	case SND_SOC_DAIFMT_NB_IF:
		break;
	default:
		return -EINVAL;
	}
*/
	return 0;
}

static int hc18xx_adc_startup(struct snd_pcm_substream *substream,
	struct snd_soc_dai *dai)
{
//	struct wm8731_priv *wm8731 = snd_soc_codec_get_drvdata(dai->codec);

	return 0;
}

static const struct snd_soc_dai_ops hc18xx_adc_dai_ops = {
	.startup	= hc18xx_adc_startup,
	.hw_params	= hc18xx_adc_hw_params,
	.set_fmt	= hc18xx_adc_set_dai_fmt,
};

static struct snd_soc_dai_driver hc18xx_adc_dai = {
	.name = "hc18xx-adc-dai",
	.capture = {
		.stream_name = "Capture",
		.channels_min = 1,
		.channels_max = 3,
		.rates = SNDRV_PCM_RATE_8000_48000,
		.formats = SNDRV_PCM_FMTBIT_S16_LE |
			       SNDRV_PCM_FMTBIT_S16_BE,},
	.ops = &hc18xx_adc_dai_ops,
};

static int hc18xx_adc_probe(struct snd_soc_codec *codec)
{
	pr_info("ASoC: hc18xx adc probing...\n");

	/* Latch the update bits */
//	snd_soc_update_bits(codec, WM8731_LOUT1V, 0x100, 0);

	return 0;
}

/* power down chip */
static int hc18xx_adc_remove(struct snd_soc_codec *codec)
{
//	struct wm8731_priv *wm8731 = snd_soc_codec_get_drvdata(codec);

	return 0;
}

static struct snd_soc_codec_driver hc18xx_adc_driver = {
	.probe =    hc18xx_adc_probe,
	.remove =   hc18xx_adc_remove,
	.dapm_widgets = hc18xx_adc_dapm_widgets,
	.num_dapm_widgets = ARRAY_SIZE(hc18xx_adc_dapm_widgets),
	.dapm_routes = hc18xx_adc_route,
	.num_dapm_routes = ARRAY_SIZE(hc18xx_adc_route),
#if 0
	.controls = hc18xx_adc_snd_controls,
	.num_controls = ARRAY_SIZE(hc18xx_adc_snd_controls),
#endif
	.read = hc18xx_adc_read_reg,
	.write = hc18xx_adc_write_reg,
};

static void hc18xx_adc_hw_init(struct hc18xx_adc_drvdata *drvdata)
{
#if 0
	u32 __iomem *base = drvdata->adccfg_base;

	adc_write_reg(ADCCFG_EN_LDO, 1);
	mdelay(1);
	adc_write_reg(ADCCFG_EN_ADC, 1);
	adc_write_reg(ADCCFG_RESET_ADC, 1);
	adc_write_reg(ADCCFG_RESET_CAL, 1);
	mdelay(1);
	adc_write_reg(ADCCFG_RESET_ADC, 0);
	adc_write_reg(ADCCFG_RESET_CAL, 0);
	adc_write_reg(ADCCFG_BYPASS_CAL, 1);
#else
	hc18xx_adc_hw_initial();
#endif
	/* Init ADC hardware & Setup channel information */

}

static int hc18xx_adc_driver_probe(struct platform_device *pdev)
{
	struct hc18xx_adc_drvdata *drvdata;
	struct resource *res;
	struct device *dev;
	int ret, ch_num;
	unsigned int val;

	pr_debug("fn: %s\n", __func__);
	pr_info("ASoC: HC18XX adc i/f is found. Probing...\n");

	dev = &pdev->dev;
	drvdata = devm_kzalloc(dev, sizeof(struct hc18xx_adc_drvdata), GFP_KERNEL);

	if (!drvdata) {
		return -ENOMEM;
	}
	dev_set_drvdata(dev, drvdata);
	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res)
	{
		dev_err(dev, "cannot find the adc device in resource");
		return -EINVAL;
	}
	drvdata->adc_base = devm_ioremap_nocache(dev, res->start, resource_size(res));
	if (IS_ERR(drvdata->adc_base))
	{
		dev_err(dev, "cannot find adcctrl register addr\n");
		return -EINVAL;
	}

#if 0
	res = platform_get_resource(pdev, IORESOURCE_MEM, 1);
	if (!res)
	{
		dev_err(dev, "cannot find the adc device in resource");
		return -EINVAL;
	}
	drvdata->adccfg_base = devm_ioremap_resource(dev, res);
	if (IS_ERR(drvdata->adccfg_base))
	{
		dev_err(dev, "cannot find adccfg register addr\n");
		return -EINVAL;
	}
#endif
	/*
	drvdata->regmap = devm_regmap_init(dev, NULL, dev, &hc18xx_adc_regmap);
	if (IS_ERR(drvdata->regmap)) {
		ret = PTR_ERR(drvdata->regmap);
		pr_info("ASoC: HC18XX adc failed to allocate regmap.\n");
		return ret;
	}*/

	hc18xx_adc_hw_init(drvdata);

	/* read device tree to determine maximum channel number */
	/* [TBD] Get ADC channels setting from ADC ctrl instead read register */
	val = readl_relaxed(drvdata->adc_base + HC18XX_ADC_TYPE);
	ch_num = (val & 0x1) + ((val >> 8) & 0x1) + ((val >> 16) & 0x1);
	if (ch_num == 0) {
		dev_err(dev, "no ADC channels for audio\n");
		return -EINVAL;
	} else {
		hc18xx_adc_dai.capture.channels_min = ch_num;
		hc18xx_adc_dai.capture.channels_max = ch_num;
		pr_info("ASoC: HC18XX adc channel number = %d\n", ch_num);
	}

	ret = snd_soc_register_codec(dev,
			&hc18xx_adc_driver, &hc18xx_adc_dai, 1);
	if (ret != 0) {
		pr_info("ASoC: HC18XX adc register failed.\n");
		return ret;
	}

	pr_info("ASoC: HC18XX adc i/f probed.\n");

	return 0;
}

static int hc18xx_adc_driver_remove(struct platform_device *pdev)
{
	pr_debug("fn: %s\n", __func__);
	snd_soc_unregister_codec(&pdev->dev);
	return 0;
}

static const struct of_device_id hc18xx_adc_of_ids[] = {
	{ .compatible = "augentix,adc", },
	{ }
};

MODULE_DEVICE_TABLE(of, hc18xx_adc_of_ids);

static struct platform_driver hc18xx_adc_platform_driver = {
	.probe = hc18xx_adc_driver_probe,
	.remove = hc18xx_adc_driver_remove,
	.driver = {
		.name   = "adc",
		.owner  = THIS_MODULE,
		.of_match_table = hc18xx_adc_of_ids,
	},
};
module_platform_driver(hc18xx_adc_platform_driver);

MODULE_DESCRIPTION("ASoC HC18XX ADC codec driver");
MODULE_AUTHOR("Henry Liu<henry.liu@augentix.com");
MODULE_LICENSE("GPL");
