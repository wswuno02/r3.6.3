#ifndef HC18XX_PCM_H
#define HC18XX_PCM_H

#include <linux/types.h>
#include <linux/clk.h>
#include <sound/soc.h>
#include <asm/io.h>
#include <pcm_interfaces.h>
#include "hc18xx-reg.h"
#include "agtx-anr.h"

#define PERIOD_BYTE_MAX 32768
#define PERIOD_BYTE_MIN 32

#define PERIOD_CNT_MAX  4
#define PERIOD_CNT_MIN  2
#define BUFFER_BYTE_MAX (PERIOD_BYTE_MAX * PERIOD_CNT_MAX)

#define RT_IDLE          (1 << 0)
#define RT_RUNNING       (1 << 1)
#define RT_WAIT_WR_END   (1 << 2)
#define RT_WAIT_BUF_FULL (1 << 3)
#define RT_ERROR         (1 << 7)

/* PCM runtime data */
struct hc18xx_runtime_data {
	struct hc_audio_info *info;
	struct snd_pcm_substream *substream;

	u32 __iomem *reg;
	u32 __iomem *i2s_reg;

	struct hc18xx_i2s *i2s;
	struct hc18xx_dsp *codec;
	struct hc18xx_adowr *adowr;
	struct hc18xx_runtime_data *shadow;

	//struct snd_dmaengine_dai_dma_data *dai_dma_data;
	struct hc18xx_dma_desc dma_desc[PERIOD_CNT_MAX];

	uint32_t rate;

	/* DMA parameters received from input parameters */
	dma_addr_t dma_hw_base; /* starting PA of the DMA buffer */
	int dma_period; /* number of buffer periods */
	size_t dma_size; /* total buffer size */
	size_t dma_periodsz; /* period size */

	spinlock_t lock;
	struct clk *clk;
	int curr_period; /* current DMA buffer ID */
	int violation_en; /* enable hw dram violation detection */
	int state; /* current driver state */
};

/* device and CSR information */
struct hc_audio_info {
	/* Quick references */
	struct snd_card *card;
	struct device *dev;
	int pdev_id;

	/* Register map */
	u32 __iomem *adoin_base;
	u32 __iomem *adoout_base;
	u32 adoin_phys;
	u32 adoout_phys;

	/* IRQ ID */
	int adoin_irq;
	int adoout_irq;

	/* Runtime data */
	struct hc18xx_runtime_data *adoin_rtd;
	struct hc18xx_runtime_data *adoout_rtd;

	/* Register info */
	struct hc18xx_i2s i2s_in;
	struct hc18xx_i2s i2s_out;
	struct hc18xx_dsp enc;
	struct hc18xx_dsp dec;
	struct hc18xx_adowr adow;
	struct hc18xx_adowr ador;

	/* Feature control */
	struct agtx_anr_info anr_info;
};

#endif /* HC18XX_PCM_H_ */
