#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/pm.h>
#include <linux/io.h>
#include <linux/slab.h>
#include <linux/regmap.h>
#include <linux/regulator/consumer.h>
#include <linux/of_device.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/pcm_params.h>
#include <sound/dmaengine_pcm.h>
#include <sound/soc.h>
#include <sound/initval.h>
#include <sound/tlv.h>

#include "hc18xx-dec.h"

/* dec private data */
struct hc18xx_dec_drvdata {
	struct regmap *regmap;
	u32 __iomem *base;
	int gain;
};

static void hc18xx_dec_write_relaxed(u32 __iomem *base,
		unsigned int shift, unsigned int mask, unsigned int value) {
	unsigned int val;

	val = readl_relaxed(base);
	val &= ~(mask << shift);
	val |= (value << shift);

	writel_relaxed(val, base);
}

static int hc18xx_db_table[HC18XX_DEC_GAIN_MAX + 1] =
	             { 0x00,
	               0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
	               0x02, 0x02, 0x02, 0x02, 0x03, 0x03,
	               0x04, 0x04, 0x05, 0x05, 0x06, 0x07,
	               0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0E,
	               0x10, 0x11, 0x14, 0x16, 0x19, 0x1C,
	               0x20, 0x23, 0x28, 0x2D, 0x32, 0x38,
	               0x40, 0x47, 0x50, 0x59, 0x64, 0x71,
	               0x7F, 0x8E, 0xA0, 0xB3, 0xC9, 0xE4,
	               0xFD, 0x11C, 0x13F, 0x166, 0x191, 0x1C4,
	               0x1F9, 0x237, 0x27C, 0x2CA, 0x321, 0x383,
	               0x3F1, 0x46C, 0x4F6, 0x594, 0x640, 0x703,
	               0x7DE, 0x8D4, 0x9E7, 0xB1D, 0xC78, 0xDFD,
	               0xFB3 };

static int hc18xx_dec_read_reg(struct snd_kcontrol *kcontrol,
		struct snd_ctl_elem_value *ucontrol) {
	struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);
	struct hc18xx_dec_drvdata *drvdata = snd_soc_codec_get_drvdata(codec);

	ucontrol->value.integer.value[0] = drvdata->gain;
	return 0;
}

static int hc18xx_dec_write_reg(struct snd_kcontrol *kcontrol,
		struct snd_ctl_elem_value *ucontrol) {
	struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);
	struct hc18xx_dec_drvdata *drvdata = snd_soc_codec_get_drvdata(codec);
	int gain = ucontrol->value.integer.value[0];

	if (gain > HC18XX_DEC_GAIN_MAX) {
		return -EINVAL;
	}

	hc18xx_dec_write_relaxed(drvdata->base + HC18XX_DEC_GAIN,
				HC18XX_DEC_GAIN_OFFSET, HC18XX_DEC_GAIN_MASK, hc18xx_db_table[gain]);
	drvdata->gain = gain;

	return 0;
}

static const DECLARE_TLV_DB_SCALE(out_tlv, -2500, 100, 1);

static const struct snd_kcontrol_new hc18xx_dec_snd_controls[] = {
SOC_SINGLE_EXT_TLV("Gain(Out) Playback Volume", HC18XX_DEC_GAIN, HC18XX_DEC_GAIN_OFFSET,
			HC18XX_DEC_GAIN_MAX, 0, hc18xx_dec_read_reg, hc18xx_dec_write_reg, out_tlv),
};

static const struct snd_soc_dapm_widget hc18xx_dec_dapm_widgets[] = {
};

static const struct snd_soc_dapm_route hc18xx_dec_route[] = {
};

static int hc18xx_dec_hw_params(struct snd_pcm_substream *substream,
			    struct snd_pcm_hw_params *params,
			    struct snd_soc_dai *dai)
{
	return 0;
}

static int hc18xx_dec_startup(struct snd_pcm_substream *substream,
	struct snd_soc_dai *dai)
{

	return 0;
}

static const struct snd_soc_dai_ops hc18xx_dec_dai_ops = {
	.startup   = hc18xx_dec_startup,
	.hw_params = hc18xx_dec_hw_params,
};

static struct snd_soc_dai_driver hc18xx_dec_dai = {
	.name = "hc18xx-dec-dai",
	.playback = {
		.stream_name = "Playback",
		.channels_min = 1,
		.channels_max = 3,
		.rates = SNDRV_PCM_RATE_8000_96000,
		.formats = SNDRV_PCM_FMTBIT_S16_LE |
			       SNDRV_PCM_FMTBIT_S16_BE,},
	.capture = {
		.stream_name = "Capture",
		.channels_min = 1,
		.channels_max = 3,
		.rates = SNDRV_PCM_RATE_8000_96000,
		.formats = SNDRV_PCM_FMTBIT_S16_LE |
			       SNDRV_PCM_FMTBIT_S16_BE,},
	.ops = &hc18xx_dec_dai_ops,
};

static int hc18xx_dec_probe(struct snd_soc_component *codec)
{
	pr_info("ASoC: hc18xx codec probing...");

	return 0;
}

static void hc18xx_dec_remove(struct snd_soc_component *component)
{
}

static struct snd_soc_component_driver hc18xx_dec_driver = {
	.name = "hc18xx-dec",
	.probe =    hc18xx_dec_probe,
	.remove =   hc18xx_dec_remove,
	.dapm_widgets = hc18xx_dec_dapm_widgets,
	.num_dapm_widgets = ARRAY_SIZE(hc18xx_dec_dapm_widgets),
	.dapm_routes = hc18xx_dec_route,
	.num_dapm_routes = ARRAY_SIZE(hc18xx_dec_route),
	.controls = hc18xx_dec_snd_controls,
	.num_controls = ARRAY_SIZE(hc18xx_dec_snd_controls),
};

static int hc18xx_dec_driver_probe(struct platform_device *pdev)
{
	struct hc18xx_dec_drvdata *drvdata;
	struct resource *res;
	struct device *dev;
	int ret;

	pr_debug("fn: %s\n", __func__);
	pr_info("ASoC: HC18XX dec i/f is found. Probing...\n");

	dev = &pdev->dev;
	drvdata = devm_kzalloc(dev, sizeof(struct hc18xx_dec_drvdata), GFP_KERNEL);

	if (!drvdata) {
		return -ENOMEM;
	}
	dev_set_drvdata(dev, drvdata);
	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res)
	{
		dev_err(dev, "cannot find the encoder in resource");
		return -EINVAL;
	}
	drvdata->base = devm_ioremap_nocache(dev, res->start, resource_size(res));
	if (IS_ERR(drvdata->base))
	{
		dev_err(dev, "cannot map encoder register address\n");
		return -EINVAL;
	}

	drvdata->gain = 25;

	ret = devm_snd_soc_register_component(dev,
			&hc18xx_dec_driver, &hc18xx_dec_dai, 1);
	if (ret != 0) {
		pr_info("ASoC: HC18XX dec component register failed.\n");
		return ret;
	}

	pr_info("ASoC: HC18XX dec i/f probed.\n");

	return 0;
}

static int hc18xx_dec_driver_remove(struct platform_device *pdev)
{
	pr_debug("fn: %s\n", __func__);
	return 0;
}

static const struct of_device_id hc18xx_dec_of_ids[] = {
	{ .compatible = "augentix,hc18xx-adec", },
	{ }
};

MODULE_DEVICE_TABLE(of, hc18xx_dec_of_ids);

static struct platform_driver hc18xx_dec_platform_driver = {
	.probe = hc18xx_dec_driver_probe,
	.remove = hc18xx_dec_driver_remove,
	.driver = {
		.name   = "hc18xx-adec",
		.owner  = THIS_MODULE,
		.of_match_table = hc18xx_dec_of_ids,
	},
};
module_platform_driver(hc18xx_dec_platform_driver);

MODULE_DESCRIPTION("ASoC hc18xx-dec driver");
MODULE_AUTHOR("Henry Liu<henry.liu@augentix.com");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:hc18xx-adec");
