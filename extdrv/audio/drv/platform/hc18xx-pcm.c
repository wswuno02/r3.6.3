#include <linux/init.h>
#include <linux/module.h>
#include <linux/clk.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/dma-mapping.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/delay.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/pcm_params.h>
#include <sound/soc.h>
#include <misc/hc-adc_ctrl.h>
#include <mach/hc18xx.h>
#include <mach/config.h>
#include "hc18xx-reg.h"
#include "hc18xx-pcm.h"
#include <sound/hwdep.h>
#include "agtx-anr.h"

#define __PREG(x) virt_to_phys(x)

struct hc_audio_info *hc_info;

static void hc18xx_pcm_stop_i2s(struct hc18xx_runtime_data *hc_rtd) {
	unsigned long flags;
	if (!((hc_rtd->shadow->i2s->ext_sck == 1 && hc_rtd->i2s->master_mode == 1)
		&& (hc_rtd->shadow->state != RT_IDLE))) {
		pr_info("%s: i2s_stop(%s)\n", __func__,
				hc_rtd->substream->stream == SNDRV_PCM_STREAM_CAPTURE ? "IN" : "OUT");
		hc_stop_i2s(hc_rtd->reg);
	} else {
		spin_lock_irqsave(&hc_rtd->lock, flags);
		hc_rtd->state = RT_IDLE;
		spin_unlock_irqrestore(&hc_rtd->lock, flags);
		hc_mask_irq(hc_rtd->reg,
			IRQ_WR_BW_INSUF | IRQ_WR_ACCESS_VIOL | IRQ_I2S_ERROR);
	}
}

static void hc18xx_pcm_start_i2s(struct hc18xx_runtime_data *hc_rtd) {
	pr_info("%s: i2s_start(%s)\n", __func__,
			hc_rtd->substream->stream == SNDRV_PCM_STREAM_CAPTURE ? "IN" : "OUT");
	hc_start_i2s(hc_rtd->reg);

	if (hc_rtd->i2s->ext_sck == 1 && hc_rtd->shadow->i2s->master_mode == 1
			&& hc_rtd->shadow->state == RT_IDLE) {
		pr_info("%s: i2s_start(%s)\n", __func__,
				hc_rtd->substream->stream == SNDRV_PCM_STREAM_CAPTURE ? "IN" : "OUT");
		hc_start_i2s(hc_rtd->i2s_reg);
	}
}

static irqreturn_t audio_irq_handler(void *dev_id, int stream)
{
	struct hc18xx_runtime_data *rtd = (struct hc18xx_runtime_data *)dev_id;
	u32 irq = hc_read_irq_sta(rtd->reg);

	if (!irq)
		return IRQ_NONE;

	if (irq & IRQ_I2S_REAL_STOP)
	{
		hc_clear_irq(rtd->reg, IRQ_I2S_REAL_STOP);
		pr_debug(" - received IRQ_I2S_REAL_STOP\n");
		if (rtd->state == RT_RUNNING) {
			hc_trigger_end_request(rtd->reg);
			pr_debug("Issue end request\n");
			rtd->state = RT_WAIT_WR_END | RT_WAIT_BUF_FULL;
		} else {
			BUG_ON(0);
		}
	}
	if (irq & IRQ_WR_BUFFER_FULL)
	{
		hc_clear_irq(rtd->reg, IRQ_WR_BUFFER_FULL);
		pr_debug(" - received IRQ_BUFFER_FULL\n");

		/* Update ANR buffer address */
		rtd->info->anr_info.buf = (int16_t *)rtd->dma_desc[rtd->curr_period].virt;
		/* setup next DMA */
		rtd->curr_period = (rtd->curr_period == (rtd->dma_period - 1))?
			0 : (rtd->curr_period + 1);
		hc_set_dma_period(rtd->reg, &rtd->dma_desc[rtd->curr_period], rtd->violation_en);

		if (stream == SNDRV_PCM_STREAM_CAPTURE) {
			hc_trigger_buf_switch(rtd->reg);
			pr_debug("Issue buffer switch\n");
			if (rtd->state != RT_IDLE) {
				if (rtd->info->anr_info.anr_init == 0) {
					anr_init(&rtd->info->anr_info);
				} else {
					anr(&rtd->info->anr_info);
					snd_pcm_period_elapsed(rtd->substream);
				}
			} else {
				pr_debug("No snd_pcm_period_elapsed\n");
			}
		} else /* Playback */ {
			if ((rtd->state & RT_WAIT_BUF_FULL) == 0) {
				if (rtd->state != RT_IDLE) {
					snd_pcm_period_elapsed(rtd->substream);
				} else {
					pr_debug("No snd_pcm_period_elapsed\n");
				}
			}
			if (rtd->state == (RT_WAIT_WR_END | RT_WAIT_BUF_FULL)) {
				/* waiting for both IRQ_BUFFER_FULL and IRQ_READ_END */
				/* received IRQ_BUFFER_FULL first */
				rtd->state = RT_WAIT_WR_END;
			} else if (rtd->state == RT_WAIT_BUF_FULL) {
				rtd->state = RT_IDLE;
				pr_info("Audio output complete(BUF_FULL)\n");
			} else {
				/* general buffer_full handling */
				hc_trigger_buf_switch(rtd->reg);
				pr_debug("Issue buffer switch\n");
			}
		}
	}
	if (irq & IRQ_WR_END) {
		hc_clear_irq(rtd->reg, IRQ_WR_END);
		pr_debug(" - received IRQ_READ_END\n");

		if (stream == SNDRV_PCM_STREAM_CAPTURE) {
			rtd->info->anr_info.anr_init = 0;
			rtd->state = RT_IDLE;
			hc_mask_irq(rtd->reg,
				IRQ_WR_BW_INSUF | IRQ_WR_ACCESS_VIOL | IRQ_I2S_ERROR);
			pr_info("Audio input complete\n");
		} else /* Playback */ {
			if (rtd->state == (RT_WAIT_WR_END | RT_WAIT_BUF_FULL)) {
				/* waiting for both IRQ_BUFFER_FULL and
				 * IRQ_READ_END, and received IRQ_READ_END
				 * first */
				rtd->state = RT_WAIT_BUF_FULL;
			} else if (rtd->state == RT_WAIT_WR_END) {
				rtd->state = RT_IDLE;
				pr_info("Audio output complete(R_END)\n");
			}
		}
	}
	if (irq & IRQ_ADC_REAL_STOP) {
		hc_clear_irq(rtd->reg, IRQ_ADC_REAL_STOP);
		pr_debug(" - received IRQ_ADC_REAL_STOP\n");
		rtd->state = RT_WAIT_WR_END;
		hc_trigger_end_request(rtd->reg);
		pr_debug("Issue end request\n");
	}

	if (irq & IRQ_ADC_NO_ACK) {
		hc_clear_irq(rtd->reg, IRQ_ADC_NO_ACK);
		pr_debug(" - received IRQ_ADC_NO_ACK\n");
	}
	if (irq & IRQ_ADC_NO_EOC) {
		hc_clear_irq(rtd->reg, IRQ_ADC_NO_EOC);
		pr_debug(" - received IRQ_ADC_NO_EOC\n");
	}
	if (irq & IRQ_WR_BW_INSUF) {
		hc_clear_irq(rtd->reg, IRQ_WR_BW_INSUF);
		pr_debug(" - received IRQ_WR_BW_INSUF\n");
		//rtd->state = RT_ERROR;
	}
	if (irq & IRQ_I2S_ERROR) {
		hc_clear_irq(rtd->reg, IRQ_I2S_ERROR);
		pr_debug(" - received IRQ_I2S_ERROR\n");
		//rtd->state = RT_ERROR;
	}
	if (irq & IRQ_WR_ACCESS_VIOL) {
		hc_clear_irq(rtd->reg, IRQ_WR_ACCESS_VIOL);
		pr_debug(" - received IRQ_WR_ACCESS_VIOL\n");
		//rtd->state = RT_ERROR;
	}

	if (irq & ~ADOIN_IRQMASK) {
		return IRQ_NONE;
	}
	return IRQ_HANDLED;
}

static irqreturn_t adoin_irq_handler(int irq_id, void *dev_id)
{
	pr_debug("fn: %s\n", __func__);
	return audio_irq_handler(dev_id, SNDRV_PCM_STREAM_CAPTURE);
}

static irqreturn_t adoout_irq_handler(int irq_id, void *dev_id)
{
	pr_debug("fn: %s\n", __func__);
	return  audio_irq_handler(dev_id, SNDRV_PCM_STREAM_PLAYBACK);
}

static struct snd_pcm_hardware hc18xx_pcm_hw = {
	.info = SNDRV_PCM_INFO_MMAP | SNDRV_PCM_INFO_MMAP_VALID | SNDRV_PCM_INFO_INTERLEAVED |
	        SNDRV_PCM_INFO_BLOCK_TRANSFER | SNDRV_PCM_INFO_DOUBLE | SNDRV_PCM_INFO_BATCH,
	.formats = SNDRV_PCM_FMTBIT_S16_LE,
	.buffer_bytes_max = BUFFER_BYTE_MAX,
	.period_bytes_min = PERIOD_BYTE_MIN,
	.period_bytes_max = PERIOD_BYTE_MAX,
	.periods_min = PERIOD_CNT_MIN,
	.periods_max = PERIOD_CNT_MAX,
};

static int hc18xx_hwdep_open(struct snd_hwdep * hw, struct file *file)
{
	pr_debug("fn: %s\n", __func__);

	return 0;
}

static int hc18xx_hwdep_ioctl(struct snd_hwdep * hw, struct file *file, unsigned int cmd, unsigned long arg)
{
	uint32_t value;
	struct anr_params *params = (struct anr_params *)arg;

	pr_debug("fn: %s\n", __func__);

	switch (cmd) {
	case HC_IOCTL_SET_DEC_MODE:
		get_user(value, (int __user *)arg);
		hc_info->dec.dsp_mode = value;
		pr_info("%s: hc_info->dec.dsp_mode: %u\n", __func__, hc_info->dec.dsp_mode);
		hc_set_codec_mode(hc_info->adoout_base, hc_info->dec.dsp_mode);
		return 0;

	case HC_IOCTL_GET_DEC_MODE:
		pr_info("%s: hc_info->dec.dsp_mode = %u\n", __func__, hc_info->dec.dsp_mode);
		put_user(hc_info->dec.dsp_mode, (int __user *)arg);
		return 0;

	case HC_IOCTL_SET_ENC_MODE:
		get_user(value, (int __user *)arg);
		hc_info->enc.dsp_mode = value;
		pr_info("%s: hc_info->enc.dsp_mode: %u\n", __func__, hc_info->enc.dsp_mode);
		hc_set_codec_mode(hc_info->adoin_base, hc_info->enc.dsp_mode);
		return 0;

	case HC_IOCTL_GET_ENC_MODE:
		pr_info("%s: hc_info->enc.dsp_mode: %u\n", __func__, hc_info->enc.dsp_mode);
		put_user(hc_info->dec.dsp_mode, (int __user *)arg);
		return 0;

	case  HC_IOCTL_VIOLATION_DIS:
		pr_info("%s: violation_en = %d\n", __func__, hc_info->adoin_rtd->violation_en);
		hc_info->adoin_rtd->violation_en = 0;
		hc_info->adoout_rtd->violation_en = 0;
		return 0;

	case  HC_IOCTL_VIOLATION_EN:
		pr_info("%s: violation_en = %d\n", __func__, hc_info->adoin_rtd->violation_en);
		hc_info->adoin_rtd->violation_en = 1;
		hc_info->adoout_rtd->violation_en = 1;
		return 0;

	case AGTX_AUDIO_IOCTL_SET_ANR_ENABLE:
		get_user(hc_info->anr_info.anr_en, (int __user *)arg);
		pr_info("%s anr_en = %d\n", __func__, hc_info->anr_info.anr_en);
		return 0;

	case AGTX_AUDIO_IOCTL_GET_ANR_ENABLE:
		put_user(hc_info->anr_info.anr_en, (int __user *)arg);
		return 0;

	case AGTX_AUDIO_IOCTL_SET_ANR_PARAMS:
		if (params->window_size_opt > 2 || params->window_size_opt < 0 ||
		    (params->auto_noise_mag != true && params->auto_noise_mag != false) || params->noise_mag_thd < 0 ||
		    params->noise_mag_thd > 1023 || params->large_mag_gain < 1 || params->large_mag_gain > 7 ||
		    (params->noise_level_rto != 1 && params->noise_level_rto != 2 && params->noise_level_rto != 4 &&
		     params->noise_level_rto != 8) ||
		    params->global_min_avg_iir_wei < 0 || params->global_min_avg_iir_wei > 16 ||
		    params->global_avg_to_nthd_rate < 0 || params->global_avg_to_nthd_rate > 31 ||
		    params->noise_mag_auto_thd_base < 0 || params->noise_mag_auto_thd_base > 2047 ||
		    params->nr_min_level < 0 || params->nr_min_level > 256 || params->nr_max_level < 0 ||
		    params->nr_max_level > 256 || (params->gaindecade_en != true && params->gaindecade_en != false) ||
		    params->gain_decrease < 20 || params->gain_decrease > 2000 || params->gain_increase < 20 ||
		    params->gain_increase > 200) {
			return -EINVAL;
		}
		if (copy_from_user(&(hc_info->anr_info.params), (void __user *)arg, sizeof(struct anr_params))) {
			return -EFAULT;
		}
		hc_info->anr_info.window_size = (32 << hc_info->anr_info.params.window_size_opt) + 1;
		pr_info("%s anr_params:\n", __func__);
		pr_info("\twindow_size_opt = %d\n", hc_info->anr_info.params.window_size_opt);
		pr_info("\tauto_noise_mag = %d\n", hc_info->anr_info.params.auto_noise_mag);
		pr_info("\tnoise_mag_thd = %d\n", hc_info->anr_info.params.noise_mag_thd);
		pr_info("\tlarge_mag_gain = %d\n", hc_info->anr_info.params.large_mag_gain);
		pr_info("\tnoise_level_rto = %d\n", hc_info->anr_info.params.noise_level_rto);
		pr_info("\tglobal_min_avg_iir_wei = %d\n", hc_info->anr_info.params.global_min_avg_iir_wei);
		pr_info("\tglobal_avg_to_nthd_rate = %d\n", hc_info->anr_info.params.global_avg_to_nthd_rate);
		pr_info("\tnoise_mag_auto_thd_base = %d\n", hc_info->anr_info.params.noise_mag_auto_thd_base);
		pr_info("\tnr_min_level = %d\n", hc_info->anr_info.params.nr_min_level);
		pr_info("\tnr_max_level = %d\n", hc_info->anr_info.params.nr_max_level);
		pr_info("\tgaindecade_en = %d\n", hc_info->anr_info.params.gaindecade_en);
		pr_info("\tgain_decrease = %d\n", hc_info->anr_info.params.gain_decrease);
		pr_info("\tgain_increase = %d\n", hc_info->anr_info.params.gain_increase);
		return 0;

	case AGTX_AUDIO_IOCTL_GET_ANR_PARAMS:
		if (copy_to_user((void __user *)arg, &hc_info->anr_info.params, sizeof(struct anr_params))) {
			return -EFAULT;
		}
		return 0;
	}

	return -ENOIOCTLCMD;
}

static int hc18xx_create_hwdep(struct snd_card *card)
{
	struct snd_hwdep *hwdep;

	hwdep = devm_kzalloc(card->dev, sizeof(struct snd_hwdep), GFP_KERNEL);
	if (hwdep == NULL) {
		dev_err(card->dev, "hc18xx_create_hwdep: cannot allocate hwdep\n");
		return -ENOMEM;
	}

	if (snd_hwdep_new(card, "hc_alsa_hwdep", 0, &hwdep) < 0) {
		pr_err("failure to create hc_alsa_hwdep");
	}

	sprintf(hwdep->name, "hc_alsa_hwdep %d", 0);

	hwdep->iface = 0;//SNDRV_HWDEP_IFACE_WMT;
	hwdep->ops.open = hc18xx_hwdep_open;
	hwdep->ops.ioctl = hc18xx_hwdep_ioctl;

	return 0;
}

/* Allocate substream private data and runtime hardware parameters */
static int hc18xx_pcm_open(struct snd_pcm_substream *substream)
{
	struct snd_pcm_runtime *runtime = substream->runtime;
	struct hc18xx_runtime_data *hc_rtd;
	struct hc_audio_info *info = hc_info;
	int cnt;

	pr_info("fn: %s\n", __func__);

	/* substream->runtime points to a snd_pcm_runtime,
	 * while substream->private_data points to a snd_soc_pcm_runtime */
	runtime->hw = hc18xx_pcm_hw;

	snd_soc_set_runtime_hwparams(substream, &hc18xx_pcm_hw);

	/* update sck/ws intra/inter */
	info->i2s_in.ext_sck = hc_get_i2s_sck_inter(info->adoin_base);
	info->i2s_in.ext_ws = hc_get_i2s_ws_inter(info->adoin_base);
	info->i2s_out.ext_sck = hc_get_i2s_sck_inter(info->adoout_base);
	info->i2s_out.ext_ws = hc_get_i2s_ws_inter(info->adoout_base);

	/* initialize runtime data */
	if (substream->stream == SNDRV_PCM_STREAM_CAPTURE)
	{
		hc_rtd = info->adoin_rtd;
		if (hc_rtd->i2s->ext_sck == 1 && hc_rtd->i2s->ext_ws == 1 &&
				info->i2s_out.master_mode == 1) {
			hc_rtd->i2s_reg  = info->adoout_base;
		} else {
			hc_rtd->i2s_reg  = info->adoin_base;
		}
	}
	else
	{
		hc_rtd = info->adoout_rtd;
		if (hc_rtd->i2s->ext_sck == 1 && hc_rtd->i2s->ext_ws == 1 &&
				info->i2s_in.master_mode == 1) {
			hc_rtd->i2s_reg  = info->adoin_base;
		} else {
			hc_rtd->i2s_reg  = info->adoout_base;
		}
	}
	hc_rtd->info = info;
	hc_rtd->substream = substream;

	for (cnt = 0; cnt < PERIOD_CNT_MAX; cnt++)
		hc_rtd->dma_desc[cnt].id = cnt;

	spin_lock_init(&hc_rtd->lock);
	runtime->private_data = hc_rtd;

	runtime->byte_align = 8;

	return 0;
}

static int hc18xx_pcm_close(struct snd_pcm_substream *substream)
{
	pr_debug("fn: %s\n", __func__);

	return 0;
}

static int hc18xx_pcm_hw_params(struct snd_pcm_substream *substream,
			 struct snd_pcm_hw_params *params)
{
	struct snd_pcm_runtime *runtime = substream->runtime;
	struct hc18xx_runtime_data *hc_rtd = runtime->private_data;
	struct hc18xx_dsp *codec = hc_rtd->codec;
	struct hc18xx_dma_desc *dma_desc;
	dma_addr_t dma_buff_phys;
	size_t total_size;
	size_t period_size;
	int period_count;
	unsigned int channels;
	unsigned int rate;
	int i;

	pr_info("fn: %s\n", __func__);

	/*
	 * hc_rtd->params contains DMA info such as max burst, addr_width.
	 * Get total buffer size, and byte count for each period from
	 * input params.
	 */
	rate = params_rate(params);
	if (codec->dsp_mode) {
		pr_info("codec->dsp_mode = %u\n", codec->dsp_mode);
		if (rate != 8000) {
			pr_err("%s: G.711, G.726 only support 8KHz sampling rate (current %d).\n", __func__, rate);
			return -EINVAL;
		}
	}
	total_size = params_buffer_bytes(params);
	period_size = params_period_bytes(params);
	period_count = params_periods(params);
	channels = params_channels(params);
	pr_info("hw_params:\n"
		 "\tTotal size = 0x%08x\n"
		 "\tPeriod size = 0x%08x\n"
		 "\tPeriod count = %d\n"
		 "\tchannels = %d\n"
		 "\trate = %d\n",
		 total_size, period_size, period_count, channels, rate);

	if (substream->stream == SNDRV_PCM_STREAM_PLAYBACK) {
		if (channels == 1) {
			hc_rtd->codec->ch_mode = PASS_DUPL;
		} else {
			hc_rtd->codec->ch_mode = PASS_ALL;
		}
	} else {
		if (hc_get_audio_src(hc_rtd->reg) == SRC_I2S) {
			if (channels == 1) {
				hc_rtd->codec->ch_mode = PASS_LEFT;
			} else {
				hc_rtd->codec->ch_mode = PASS_ALL;
			}
		} else {
			hc_rtd->codec->ch_mode = PASS_ALL;
		}
	}

	/* 4-pin, 5-pin mode could only support the same sampling rate
	 * at the same time. */
	if (hc_rtd->i2s->ext_sck == 1) {
		if (hc_rtd->shadow->state != RT_IDLE && hc_rtd->shadow->rate != rate) {
			pr_err("%s: Set rate %d failed, Running in rate %d.\n",
				__func__, rate, hc_rtd->rate);
			return -EBUSY;
		} else {
			hc_rtd->shadow->rate = rate;
		}
	} else {
		if (hc_rtd->state != RT_IDLE && hc_rtd->rate != rate) {
			pr_err("%s: Set rate %d failed, Running in rate %d.\n",
				__func__, rate, hc_rtd->rate);
			return -EBUSY;
		} else {
			hc_rtd->rate = rate;
		}
	}

	/* Set sampling rate.
	 * This is only effect in master mode */
	switch (rate) {
		case 8000:
			hc_set_i2s_msck_div(hc_rtd->i2s_reg, 10, 10);
			hc_set_i2s_ws_sd_len(hc_rtd->i2s_reg, 75);
			break;
		case 11025:
			hc_set_i2s_msck_div(hc_rtd->i2s_reg, 8, 8);
			hc_set_i2s_ws_sd_len(hc_rtd->i2s_reg, 68);
			break;
		case 16000:
			hc_set_i2s_msck_div(hc_rtd->i2s_reg, 5, 5);
			hc_set_i2s_ws_sd_len(hc_rtd->i2s_reg, 75);
			break;
		case 22050:
			hc_set_i2s_msck_div(hc_rtd->i2s_reg, 8, 8);
			hc_set_i2s_ws_sd_len(hc_rtd->i2s_reg, 34);
			break;
		case 32000:
			hc_set_i2s_msck_div(hc_rtd->i2s_reg, 7, 8);
			hc_set_i2s_ws_sd_len(hc_rtd->i2s_reg, 25);
			break;
		case 44100:
			hc_set_i2s_msck_div(hc_rtd->i2s_reg, 4, 4);
			hc_set_i2s_ws_sd_len(hc_rtd->i2s_reg, 34);
			break;
		case 48000:
			hc_set_i2s_msck_div(hc_rtd->i2s_reg, 5, 5);
			hc_set_i2s_ws_sd_len(hc_rtd->i2s_reg, 25);
			break;
		case 64000:
			hc_set_i2s_msck_div(hc_rtd->i2s_reg, 2, 2);
			hc_set_i2s_ws_sd_len(hc_rtd->i2s_reg, 47);
			break;
		case 88200:
			hc_set_i2s_msck_div(hc_rtd->i2s_reg, 2, 2);
			hc_set_i2s_ws_sd_len(hc_rtd->i2s_reg, 34);
			break;
		case 96000:
			hc_set_i2s_msck_div(hc_rtd->i2s_reg, 2, 3);
			hc_set_i2s_ws_sd_len(hc_rtd->i2s_reg, 25);
			break;
		default:
			pr_info("%s: Unexpected sampling rate %d\n", __func__,
					params_rate(params));
			return -EINVAL;
			break;
	}

	/*
	 * Since substream->dma_buffer->area/addr is set,
	 * these 2 other DMA info structs are to be filled:
	 * 1. substream->runtime->dma_buffer_p->area/addr
	 * 2. substream->runtime->dma_area/dma_addr
	 * It is advised not to use substream->dma_buffer directly.
	 */
	snd_pcm_set_runtime_buffer(substream, &substream->dma_buffer);
	runtime->dma_bytes = total_size;

	dma_buff_phys = runtime->dma_addr;
	pr_info("DMA buffer address = 0x%08x, total size = 0x%08x\n",
		 dma_buff_phys, total_size);

	hc_rtd->dma_hw_base = dma_buff_phys;
	hc_rtd->dma_period = period_count;
	hc_rtd->dma_periodsz = period_size;

	dma_desc = hc_rtd->dma_desc;
	pr_info("  DMA descriptors:\n");
	for (i = 0; i < hc_rtd->dma_period; i++) {
		dma_desc[i].phys = hc_rtd->dma_hw_base + (i * hc_rtd->dma_periodsz);
		dma_desc[i].virt = (u32)runtime->dma_area + (i * hc_rtd->dma_periodsz);
		dma_desc[i].byte = hc_rtd->dma_periodsz;
		pr_info("\t%2d:\tlinear_addr=0x%08x\taccess_length=0x%08x\n",
			i,
			PHYS_TO_LINEAR(dma_desc[i].phys),
			BYTE_TO_AXI(dma_desc[i].byte)   );
	}

	/* ANR */
	if (substream->stream == SNDRV_PCM_STREAM_CAPTURE) {
		struct agtx_anr_info *anr_info = &(hc_rtd->info->anr_info);
		anr_info->buf = (int16_t *)dma_desc[0].virt;
		anr_info->sample_rate = rate;
		anr_info->num_samples = hc_rtd->dma_periodsz / 2;
	}

	return 0;
}

static int hc18xx_pcm_hw_free(struct snd_pcm_substream *substream)
{
	pr_debug("fn: %s\n", __func__);

	/* ANR */
	if (substream->stream == SNDRV_PCM_STREAM_CAPTURE) {
		struct snd_pcm_runtime *runtime = substream->runtime;
		struct hc18xx_runtime_data *hc_rtd = runtime->private_data;
		struct agtx_anr_info *anr_info = &(hc_rtd->info->anr_info);

		anr_info->buf = NULL;
	}
	snd_pcm_set_runtime_buffer(substream, NULL);
	return 0;
}

static int hc18xx_pcm_prepare(struct snd_pcm_substream *substream)
{
	struct snd_pcm_runtime *runtime = substream->runtime;
	struct hc18xx_runtime_data *hc_rtd = runtime->private_data;

	pr_info("fn: %s %s state = %d\n", __func__,
			substream->stream == SNDRV_PCM_STREAM_CAPTURE ? "IN" : "OUT", hc_rtd->state);

	/* codec clear */
	hc_rtd->curr_period = 0;

	/* DMA register setup */
	hc_set_dma_period(hc_rtd->reg, &hc_rtd->dma_desc[0], hc_rtd->violation_en);

	/* Enable IRQ */
	hc_unmask_irq(hc_rtd->reg,
		IRQ_WR_BW_INSUF | IRQ_WR_ACCESS_VIOL | IRQ_I2S_ERROR);

	return 0;
}

/* start/stop DMA and ENC */
static int hc18xx_pcm_trigger(struct snd_pcm_substream *substream, int cmd)
{
	struct snd_pcm_runtime *runtime = substream->runtime;
	struct hc18xx_runtime_data *hc_rtd = runtime->private_data;
	unsigned long flags;

	pr_info("fn: %s %s cmd %d\n", __func__,
			substream->stream == SNDRV_PCM_STREAM_CAPTURE ? "IN" : "OUT", cmd);

	switch (cmd) {
	case SNDRV_PCM_TRIGGER_START:
#ifdef CONFIG_AUDIO_DMIC_READY_DELAY
		if (substream->stream == SNDRV_PCM_STREAM_CAPTURE) {
			msleep(CONFIG_AUDIO_DMIC_READY_DELAY);
		}
#endif

		if (hc_rtd->state != RT_RUNNING) {
			hc_clear_codec(hc_rtd->reg);
		}
		hc_trigger_audio_wr(hc_rtd->reg);

		spin_lock_irqsave(&hc_rtd->lock, flags);
		hc_rtd->state = RT_RUNNING;
		spin_unlock_irqrestore(&hc_rtd->lock, flags);
		if (substream->stream == SNDRV_PCM_STREAM_CAPTURE
			&& hc_get_audio_src(hc_rtd->reg) == SRC_ADC) {
			hc_start_adc(hc_rtd->reg);
		} else {
			hc_set_codec_ch(hc_rtd->reg, hc_rtd->codec->ch_mode);
			hc_set_i2s_mute(hc_rtd->reg, hc_rtd->i2s->mute);
			hc18xx_pcm_start_i2s(hc_rtd);
		}
		break;
	case SNDRV_PCM_TRIGGER_STOP:
		if (substream->stream == SNDRV_PCM_STREAM_CAPTURE) {
			if ( hc_get_audio_src(hc_rtd->reg) == SRC_ADC) {
				hc_stop_adc(hc_rtd->reg);
			} else { /* SRC == I2S */
				hc18xx_pcm_stop_i2s(hc_rtd);
			}
		} else {
			hc18xx_pcm_stop_i2s(hc_rtd);
			hc_set_codec_ch(hc_rtd->reg, PASS_NONE);
		}
		break;
	case SNDRV_PCM_TRIGGER_SUSPEND:
	case SNDRV_PCM_TRIGGER_RESUME:
	case SNDRV_PCM_TRIGGER_PAUSE_RELEASE:
	case SNDRV_PCM_TRIGGER_PAUSE_PUSH:
	default:
		return -EINVAL;
	}

	return 0;
}

static snd_pcm_uframes_t hc18xx_pcm_pointer(struct snd_pcm_substream *substream)
{
	struct snd_pcm_runtime *runtime = substream->runtime;
	struct hc18xx_runtime_data *hc_rtd = runtime->private_data;
	size_t pointer = (hc_rtd->curr_period * hc_rtd->dma_periodsz);

	pr_debug("fn: %s\n", __func__);

	return bytes_to_frames(runtime, pointer);
}

static struct snd_pcm_ops hc18xx_pcm_ops = {
	.open = hc18xx_pcm_open,
	.close = hc18xx_pcm_close,
	.ioctl = snd_pcm_lib_ioctl,
	.prepare = hc18xx_pcm_prepare,
	.trigger = hc18xx_pcm_trigger,
	.pointer = hc18xx_pcm_pointer,
	.hw_params = hc18xx_pcm_hw_params,
	.hw_free = hc18xx_pcm_hw_free,
};

static int hc18xx_pcm_new(struct snd_soc_pcm_runtime *rtd)
{
	struct snd_card *card = rtd->card->snd_card;
	struct snd_pcm *pcm = rtd->pcm;
	int ret;
	size_t size = hc18xx_pcm_hw.buffer_bytes_max;

	pr_debug("fn: %s\n", __func__);

	hc18xx_create_hwdep(card);

	ret = dma_coerce_mask_and_coherent(card->dev, DMA_BIT_MASK(28));

	/* Allocate DMA buffer here */
	return snd_pcm_lib_preallocate_pages_for_all(pcm, SNDRV_DMA_TYPE_DEV,
	                                             card->dev, size, size);
}

static void hc18xx_pcm_free(struct snd_pcm *pcm)
{
	pr_debug("fn: %s\n", __func__);

	snd_pcm_lib_preallocate_free_for_all(pcm);
}

static struct snd_soc_platform_driver hc18xx_pcm_dma_platform = {
	.ops		= &hc18xx_pcm_ops,
	.pcm_new	= hc18xx_pcm_new,
	.pcm_free	= hc18xx_pcm_free,
};


/* HC18xx DAI (I2S) driver */
static int hc18xx_dai_startup(struct snd_pcm_substream *substream,
			      struct snd_soc_dai *dai)
{
	struct snd_pcm_runtime *runtime = substream->runtime;
	int ret;

	ret = snd_pcm_hw_constraint_step(runtime, 0,
			SNDRV_PCM_HW_PARAM_PERIOD_BYTES, 4);
	if (ret)
		return ret;

	ret = snd_pcm_hw_constraint_step(runtime, 0,
			SNDRV_PCM_HW_PARAM_BUFFER_BYTES, 16);
	if (ret)
		return ret;
	
	pr_debug("fn: %s\n", __func__);
	return 0;
}

static void hc18xx_dai_shutdown(struct snd_pcm_substream *substream,
			      struct snd_soc_dai *dai)
{
	pr_debug("fn: %s\n", __func__);
}

static int hc18xx_dai_hw_params(struct snd_pcm_substream *substream,
				struct snd_pcm_hw_params *params,
				struct snd_soc_dai *dai)
{
	pr_debug("fn: %s\n", __func__);
	return 0;
}

static int hc18xx_dai_hw_free(struct snd_pcm_substream *substream,
				struct snd_soc_dai *dai)
{
	pr_debug("fn: %s\n", __func__);
	return 0;
}

static int hc18xx_dai_set_fmt(struct snd_soc_dai *dai,
			      unsigned int fmt)
{
	struct hc_audio_info *info = snd_soc_dai_get_drvdata(dai);
	struct device *dev = info->dev;
	pr_info("fn: %s\n", __func__);

	/* Leave the switch options here for further modification */
	switch (fmt & SND_SOC_DAIFMT_INV_MASK) {
	case SND_SOC_DAIFMT_NB_NF:
		break;
	default:
		dev_err(dev, "%s: Unexpected DAI invert mode\n", __func__);
		return -EINVAL;
	}

	switch (fmt & SND_SOC_DAIFMT_FORMAT_MASK) {
	case SND_SOC_DAIFMT_I2S:
		break;
	default:
		dev_err(dev, "%s: Unexpected DAI format\n", __func__);
		return -EINVAL;
	}

	switch (fmt & SND_SOC_DAIFMT_MASTER_MASK) {
	case SND_SOC_DAIFMT_CBS_CFS: /* Codec slave */
		hc_set_i2s_master_mode(info->adoin_base, I2S_MASTER);
		hc_set_i2s_master_mode(info->adoout_base, I2S_MASTER);
		info->i2s_in.master_mode = 1;
		info->i2s_out.master_mode = 1;
		break;
	case SND_SOC_DAIFMT_CBM_CFM: /* Codec master */
		hc_set_i2s_master_mode(info->adoin_base, I2S_SLAVE);
		hc_set_i2s_master_mode(info->adoout_base, I2S_SLAVE);
		info->i2s_in.master_mode = 0;
		info->i2s_out.master_mode = 0;
		break;
	default:
		dev_err(dev, "%s: Unexpected DAI clk & FRM master/slave mode\n",
			 __func__);
		return -EINVAL;
	}

	return 0;
}

static int hc18xx_dai_trigger(struct snd_pcm_substream *substream, int cmd,
			      struct snd_soc_dai *dai)
{
	pr_debug("fn: %s\n", __func__);
	return 0;
}


static int hc18xx_dai_probe(struct snd_soc_dai *dai)
{
	pr_debug("fn: %s\n", __func__);
	snd_soc_dai_set_drvdata(dai, hc_info);
	return 0;
}

static int hc18xx_dai_remove(struct snd_soc_dai *dai)
{
	snd_soc_dai_set_drvdata(dai, NULL);
	return 0;
}

static const struct snd_soc_dai_ops hc18xx_dai_ops ={
	.startup	= hc18xx_dai_startup,
	.shutdown	= hc18xx_dai_shutdown,
	.hw_params	= hc18xx_dai_hw_params,
	.hw_free	= hc18xx_dai_hw_free,
	.trigger	= hc18xx_dai_trigger, /* when is this function called? */
	.set_fmt	= hc18xx_dai_set_fmt,
};

struct snd_soc_dai_driver hc18xx_dai_driver = {
	.name = "hc18xx-dai",
	.probe = hc18xx_dai_probe,
	.remove = hc18xx_dai_remove,
	.playback = {
		.channels_min = 1,
		.channels_max = 2,
		.rates = SNDRV_PCM_RATE_8000_96000,
		.formats = SNDRV_PCM_FMTBIT_S16_LE,
	},
	.capture = {
		.channels_min = 1,
		.channels_max = 2,
		.rates = SNDRV_PCM_RATE_8000_96000,
		.formats = SNDRV_PCM_FMTBIT_S16_LE,
	},
	.ops = &hc18xx_dai_ops,
};

static int hc18xx_dai_read_adoin_src(struct snd_kcontrol *kcontrol,
		struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);
	struct hc_audio_info *drvdata = snd_soc_codec_get_drvdata(codec);

	ucontrol->value.integer.value[0] = hc_get_audio_src(drvdata->adoin_base);
	return 0;
}

static int hc18xx_dai_write_adoin_src(struct snd_kcontrol *kcontrol,
		struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);
	struct hc_audio_info *drvdata = snd_soc_codec_get_drvdata(codec);
	int val = ucontrol->value.integer.value[0];

	hc_set_audio_src(drvdata->adoin_base, val);
	return 0;
}

static int hc18xx_dai_read_adoin_clk_src(struct snd_kcontrol *kcontrol,
		struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);
	struct hc_audio_info *drvdata = snd_soc_codec_get_drvdata(codec);

	ucontrol->value.integer.value[0] =
		hc_get_i2s_sck_inter(drvdata->adoin_base)
		| (hc_get_i2s_ws_inter(drvdata->adoin_base) << 1);
	return 0;
}

static int hc18xx_dai_write_adoin_clk_src(struct snd_kcontrol *kcontrol,
		struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);
	struct hc_audio_info *drvdata = snd_soc_codec_get_drvdata(codec);
	int val = ucontrol->value.integer.value[0];

	hc_set_i2s_sck_inter(drvdata->adoin_base, val & 1);
	hc_set_i2s_ws_inter(drvdata->adoin_base, (val >> 1) & 1);
	return 0;
}

static int hc18xx_dai_read_adoout_clk_src(struct snd_kcontrol *kcontrol,
		struct snd_ctl_elem_value *ucontrol) 
{
	struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);
	struct hc_audio_info *drvdata = snd_soc_codec_get_drvdata(codec);

	ucontrol->value.integer.value[0] =
		hc_get_i2s_sck_inter(drvdata->adoout_base)
		| (hc_get_i2s_ws_inter(drvdata->adoout_base) << 1);
	return 0;
}

static int hc18xx_dai_write_adoout_clk_src(struct snd_kcontrol *kcontrol,
		struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_codec *codec = snd_soc_kcontrol_codec(kcontrol);
	struct hc_audio_info *drvdata = snd_soc_codec_get_drvdata(codec);
	int val = ucontrol->value.integer.value[0];

	hc_set_i2s_sck_inter(drvdata->adoout_base, val & 1);
	hc_set_i2s_ws_inter(drvdata->adoout_base, (val >> 1) & 1);
	return 0;
}

static const char *in_src_texts[] =
	{ "None", "I2S", "ADC" };
static const char *intra_inter_texts[] =
	{ "SCK_WS_INTRA", "SCK_INTER", "WS_INTER", "SCK_WS_INTER" };

static const struct soc_enum hc18xx_dai_enums[] = {
	SOC_ENUM_SINGLE_EXT(ARRAY_SIZE(in_src_texts), in_src_texts),
	SOC_ENUM_SINGLE_EXT(ARRAY_SIZE(intra_inter_texts), intra_inter_texts),
};

static const struct snd_kcontrol_new hc18xx_dai_snd_controls[] = {
SOC_ENUM_EXT("IN_SRC Capture Switch", hc18xx_dai_enums[0], hc18xx_dai_read_adoin_src, hc18xx_dai_write_adoin_src),
SOC_ENUM_EXT("IN_CLK_SRC Capture Switch", hc18xx_dai_enums[1], hc18xx_dai_read_adoin_clk_src, hc18xx_dai_write_adoin_clk_src),
SOC_ENUM_EXT("OUT_CLK_SRC Playback Switch", hc18xx_dai_enums[1], hc18xx_dai_read_adoout_clk_src, hc18xx_dai_write_adoout_clk_src),
};

static const struct snd_soc_component_driver hc18xx_dai_component = {
	.name = "hc18xx-dai",
	.controls = hc18xx_dai_snd_controls,
	.num_controls = ARRAY_SIZE(hc18xx_dai_snd_controls),
};

/* Below is the Linux platform driver part, which is
 * necessary for registering ASoC platform driver */
static int hc18xx_hw_init(struct hc_audio_info *info)
{
	int i;
	struct hc18xx_adowr *adowr;
	struct hc18xx_i2s *i2s;
	struct hc18xx_dsp *codec;
	void __iomem *base;
	u32 irqmask;

	if (IS_ERR(info->adoin_base))
		return PTR_ERR(info->adoin_base);
	if (IS_ERR(info->adoout_base))
		return PTR_ERR(info->adoout_base);

	/* Initialize structs */
	for (i = 0; i < 2; i++)
	{
		adowr = (i == 0)? &info->adow:
			&info->ador;
		/* ADOW/ADOR FIFO settings */
		adowr->burst_len = 8;
		adowr->target_fifo_lvl = 0x10;
		adowr->full_fifo_lvl = 0x20;

		i2s = (i == 0)? &info->i2s_in:
			&info->i2s_out;
		i2s->ws_tick = 0x96;
		i2s->sd_tick = 0x96;
		i2s->sd_fmt = 0;
		i2s->sckl_cycle = 5;
		i2s->sckh_cycle = 5;
		i2s->ext_sck = I2S_INTRA;
		i2s->ext_ws = I2S_INTRA;
		i2s->sck_deglitch_th = 0;
		i2s->ws_deglitch_th = 0;
		i2s->sd_deglitch_th = 0;
		i2s->sck_deglitch_en = 0;
		i2s->ws_deglitch_en = 0;
		i2s->sd_deglitch_en = 0;
		i2s->sck_deglitch_bp = 1;
		i2s->ws_deglitch_bp = 1;
		i2s->sd_deglitch_bp = 1;
		i2s->mute = 0;

		codec = (i == 0)?  &info->enc:
			&info->dec;
		codec->ch_mode = 0;
		codec->dsp_mode = CODEC_RAW;
		codec->pre_shift = 0;
		codec->post_shift = 0;
		codec->gain = 0x10;
		codec->endian = 1;

		/* select default audio source */
		hc_set_audio_src(info->adoin_base, SRC_I2S);
		base = (i == 0)? info->adoin_base:
			info->adoout_base;

		irqmask = hc_read_irq_msk(base);
		irqmask |= (i == 0)? ADOIN_IRQMASK : ADOOUT_IRQMASK;
		hc_mask_irq(base, irqmask);

		hc_set_darb_attr(base);
		hc_set_fifo_attr(base, adowr);

		hc_clear_codec(base);
		hc_set_codec_ch(base, codec->ch_mode);
		hc_set_codec_mode(base, codec->dsp_mode);
		hc_adjust_sample(base, codec);

		hc_set_i2s_data_fmt(base, i2s->sd_fmt);
		hc_set_i2s_sck_inter(base, i2s->ext_sck);
		hc_set_i2s_ws_inter(base, i2s->ext_ws);
		hc_set_i2s_mute(base, i2s->mute);
		hc_set_i2s_mux(base, i2s);
		hc_set_i2s_deglitch(base, i2s);

		hc_clear_irq(base, irqmask);
	}

	return 0;
}

static int hc18xx_sw_init(struct hc_audio_info *info)
{
	/* update sck/ws intra/inter */
	info->i2s_in.ext_sck = hc_get_i2s_sck_inter(info->adoin_base);
	info->i2s_in.ext_ws = hc_get_i2s_ws_inter(info->adoin_base);
	info->i2s_out.ext_sck = hc_get_i2s_sck_inter(info->adoout_base);
	info->i2s_out.ext_ws = hc_get_i2s_ws_inter(info->adoout_base);

	/* initialize runtime data */
	info->adoin_rtd->shadow = info->adoout_rtd;
	info->adoin_rtd->i2s = &info->i2s_in;
	info->adoin_rtd->adowr = &info->ador;
	info->adoin_rtd->codec = &info->enc;
	info->adoin_rtd->reg = info->adoin_base;
	info->adoin_rtd->state = RT_IDLE;
	info->adoin_rtd->violation_en = 0;

	info->adoout_rtd->shadow    = info->adoin_rtd;
	info->adoout_rtd->i2s       = &info->i2s_out;
	info->adoout_rtd->adowr     = &info->adow;
	info->adoout_rtd->codec     = &info->dec;
	info->adoout_rtd->reg       = info->adoout_base;
	info->adoout_rtd->state     = RT_IDLE;
	info->adoout_rtd->violation_en = 0;

	/* ANR init */
#define ANR_WINDOW_SIZE_MAX 129
	info->anr_info.anr_en = 1;
	info->anr_info.anr_init = 0;
	info->anr_info.buf = NULL;
	info->anr_info.num_samples = 0;
	info->anr_info.window_size = ANR_WINDOW_SIZE_MAX;
	info->anr_info.buf_pre = devm_kmalloc(info->dev, sizeof(int16_t) * ANR_WINDOW_SIZE_MAX, GFP_ATOMIC);
	info->anr_info.mag_pre = devm_kmalloc(info->dev, sizeof(int16_t) * ANR_WINDOW_SIZE_MAX, GFP_ATOMIC);
	info->anr_info.mag_wei_pre = devm_kmalloc(info->dev, sizeof(int16_t) * ANR_WINDOW_SIZE_MAX, GFP_ATOMIC);
	info->anr_info.window_sum_wei = 0;
	info->anr_info.window_sum = 0;
	info->anr_info.gain_last = 0;
	info->anr_info.params.window_size_opt = 2;
	info->anr_info.params.auto_noise_mag = false;
	info->anr_info.params.noise_mag_thd = 600;
	info->anr_info.params.large_mag_gain = 1;
	info->anr_info.params.noise_level_rto = 4;
	info->anr_info.params.global_min_avg_iir_wei = 15;
	info->anr_info.params.global_avg_to_nthd_rate = 9;
	info->anr_info.params.noise_mag_auto_thd_base = 790;
	info->anr_info.params.nr_min_level = 50;
	info->anr_info.params.nr_max_level = 230;
	info->anr_info.params.gaindecade_en = true;
	info->anr_info.params.gain_decrease = 500;
	info->anr_info.params.gain_increase = 30;

	/* Enable IRQ */
	hc_unmask_irq(info->adoin_base,
		IRQ_WR_BUFFER_FULL | IRQ_WR_END | IRQ_I2S_REAL_STOP |
		IRQ_ADC_REAL_STOP | IRQ_ADC_NO_ACK | IRQ_ADC_NO_EOC);
	hc_unmask_irq(info->adoout_base,
		IRQ_WR_BUFFER_FULL | IRQ_WR_END | IRQ_I2S_REAL_STOP |
		IRQ_ADC_REAL_STOP | IRQ_ADC_NO_ACK | IRQ_ADC_NO_EOC);

	/* software reset */
	hc18xx_adoin_sw_rst();
	hc18xx_sw_rst(SW_RST_AUDIO_OUT);

	/* start clock */
	clk_prepare_enable(info->adoin_rtd->clk);
	clk_prepare_enable(info->adoout_rtd->clk);
	adc_clk_start();

	return 0;
}

static int hc18xx_pcm_platform_probe(struct platform_device * pdev)
{
	struct device_node *np;
	struct resource *res;
	struct device *dev;
	int ret;

	pr_info("ASoC: HC18xx pcm i/f is found. Probing...\n");
	pr_debug("fn: %s\n", __func__);

	np = pdev->dev.of_node;
	dev = &pdev->dev;

	if (!np) {
		dev_err(dev, "cannot find the device.");
		return -ENODEV;
	}

	/* fill in HW info to hc_info */
	hc_info = devm_kzalloc(dev, sizeof(*hc_info), GFP_KERNEL);
	if (!hc_info) {
		dev_err(dev, "Failed to allocate memory for hc_audio_info\n");
		return -ENOMEM;
	}

	hc_info->dev = dev;
	hc_info->pdev_id = pdev->id;

	hc_info->adoin_rtd = devm_kzalloc(dev, sizeof(struct hc18xx_runtime_data), GFP_KERNEL);
	if (!hc_info->adoin_rtd) {
		dev_err(dev, "Failed to allocate memory for adoin_rtd\n");
		return -ENOMEM;
	}

	hc_info->adoin_rtd->clk = devm_clk_get(dev, "adoin_clk");
	if(IS_ERR(hc_info->adoin_rtd->clk)) {
		dev_err(dev, "cannot get audio-in clock source");
		return -ENODEV;
	}

	hc_info->adoout_rtd = devm_kzalloc(dev, sizeof(struct hc18xx_runtime_data), GFP_KERNEL);
	if (!hc_info->adoout_rtd) {
		dev_err(dev, "Failed to allocate memory for adoout_rtd\n");
		return -ENOMEM;
	}

	hc_info->adoout_rtd->clk = devm_clk_get(dev, "adoout_clk");
	if(IS_ERR(hc_info->adoout_rtd->clk)) {
		dev_err(dev, "cannot get audio-out clock source");
		return -ENODEV;
	}

	/* get CSR address from device tree*/
	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res)
	{
		dev_err(dev, "cannot find the audio device in resource");
		return -EINVAL;
	}
	hc_info->adoin_base = devm_ioremap_resource(dev, res);
	if (IS_ERR(hc_info->adoin_base))
	{
		dev_err(dev, "cannot find audio register addr\n");
		return -EINVAL;
	}
	hc_info->adoin_phys = res->start;

	res = platform_get_resource(pdev, IORESOURCE_MEM, 1);
	if (!res)
	{
		dev_err(dev, "cannot find the audio in resource\n");
		return -EINVAL;
	}
	hc_info->adoout_base = devm_ioremap_resource(dev, res);
	if (IS_ERR(hc_info->adoout_base))
	{
		dev_err(dev, "cannot find audio register addr\n");
		return -EINVAL;
	}
	hc_info->adoout_phys = res->start;

	/* IRQ is platform-specific, so we put them here */
	hc_info->adoin_irq = platform_get_irq(pdev, 0);
	if (hc_info->adoin_irq < 0)
	{
		dev_err(dev, "cannot find audio-in IRQ\n");
		return hc_info->adoin_irq;
	}
	hc_info->adoout_irq = platform_get_irq(pdev, 1);
	if (hc_info->adoout_irq < 0)
	{
		dev_err(dev, "cannot find audio-out IRQ\n");
		return hc_info->adoout_irq;
	}

	ret = devm_request_irq(dev,
			       hc_info->adoin_irq,
			       adoin_irq_handler,
			       IRQF_SHARED, "adoin", hc_info->adoin_rtd);
	if (ret < 0)
	{
		pr_info("fail to claim adoin IRQ, ret = %d\n", ret);
	}

	ret = devm_request_irq(dev,
			       hc_info->adoout_irq,
			       adoout_irq_handler,
			       0, "adoout", hc_info->adoout_rtd);
	if (ret < 0)
	{
		pr_info("fail to claim adoout IRQ, ret = %d\n", ret);
	}

	pr_info("hc18xx-pcm: adoin hwirq = %d, adoout hwirq = %d\n",
		hc_info->adoin_irq, hc_info->adoout_irq);

	ret = hc18xx_hw_init(hc_info);
	if (ret)
	{
		dev_err(dev, "cannot initialize register setting\n");
		return -EINVAL;
	}
	ret = hc18xx_sw_init(hc_info);
	if (ret)
	{
		dev_err(dev, "cannot initialize software setting\n");
		return -EINVAL;
	}

	/* ALSA Platform driver registration */
	ret = devm_snd_soc_register_platform(dev,
					   &hc18xx_pcm_dma_platform);
	if (ret)
	{
		dev_err(dev,
			"Failed to register ALSA platform driver!\n");
		return ret;
	}

	/* ALSA DAI driver registration */
	ret = devm_snd_soc_register_component(dev,
					      &hc18xx_dai_component,
					      &hc18xx_dai_driver, 1);
	if (ret)
	{
		dev_err(dev, "Failed to register ALSA DAI driver!\n");
		return ret;
	}

	platform_set_drvdata(pdev, hc_info);

	pr_info("ASoC: HC18xx pcm i/f probed.\n");
	return 0;
}

static int hc18xx_pcm_platform_remove(struct platform_device *pdev)
{
	pr_info("fn: %s\n", __func__);

	clk_disable_unprepare(hc_info->adoin_rtd->clk);
	clk_disable_unprepare(hc_info->adoout_rtd->clk);
	adc_clk_stop();

	hc_mask_irq(hc_info->adoin_base,
		IRQ_WR_BUFFER_FULL | IRQ_WR_END | IRQ_I2S_REAL_STOP |
		IRQ_WR_BW_INSUF | IRQ_WR_ACCESS_VIOL | IRQ_I2S_ERROR);

	hc_mask_irq(hc_info->adoout_base,
		IRQ_WR_BUFFER_FULL | IRQ_WR_END | IRQ_I2S_REAL_STOP |
		IRQ_WR_BW_INSUF | IRQ_WR_ACCESS_VIOL | IRQ_I2S_ERROR);

	return 0;
}

static const struct of_device_id hc18xx_pcm_of_ids[] =
{
	{ .compatible = "augentix,hc18xx-pcm", },
	{},
};
MODULE_DEVICE_TABLE(of, hc18xx_pcm_of_ids);

static struct platform_driver hc18xx_pcm_driver = {
	.probe = hc18xx_pcm_platform_probe,
	.remove = hc18xx_pcm_platform_remove,
	.driver = {
		.name = "hc18xx-pcm",
		.owner = THIS_MODULE,
		.of_match_table = hc18xx_pcm_of_ids,
	},
};
module_platform_driver(hc18xx_pcm_driver);

MODULE_AUTHOR("Henry Liu<henry.liu@augentix.com>");
MODULE_DESCRIPTION("Augentix HC18xx ASoC PCM Driver");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:hc-pcm");
