#ifndef HC18XX_REG_H
#define HC18XX_REG_H

#include <linux/bitops.h>
#include "audio-xcm.h"

/* Audio registers */
#define ADOIN_PHYS  0x80540000
#define ADOOUT_PHYS 0x80550000

/* Audio in / audio out offset */
#define ADOIN_OFFS  0x00000000
#define ADOOUT_OFFS 0x00010000

/* audio engine bank offsets */
#define AIO_OFFS    0x0000
#define AWR_OFFS    0x1000
#define ACODEC_OFFS 0x2000
#define I2S_OFFS    0x3000
#define ADC_OFFS    0x4000

#define TRIGGER     BIT(0)
#define BUF_SWITCH  BIT(8)
/* write_req_end must be triggered together with buffer_switch */
#define REQUEST_END (BIT(8) | BIT(9))

#define CLEAR       BIT(0)

#define I2S_START   BIT(0)
#define I2S_STOP    BIT(1)

#define I2S_SLAVE   0
#define I2S_MASTER  1

#define I2S_INTRA   0
#define I2S_INTER   1

#define ADC_CLK_START   BIT(0)
#define ADC_CLK_STOP    BIT(1)
#define ADC_AUDIO_START BIT(2)
#define ADC_AUDIO_STOP  BIT(3)

/* IRQ identifier bits */
#define IRQ_WR_BW_INSUF     BIT(0)
#define IRQ_WR_BUFFER_FULL  BIT(2)
#define IRQ_WR_ACCESS_VIOL  BIT(3)
#define IRQ_WR_END          BIT(4)
#define IRQ_I2S_REAL_STOP   BIT(5)
#define IRQ_I2S_ERROR       BIT(6)
#define IRQ_ADC_REAL_STOP   BIT(7)
#define IRQ_ADC_NO_ACK      BIT(8)
#define IRQ_ADC_NO_EOC      BIT(9)

/* CSR offsets */
#define AIO_IRQCLR	(0x0004 >> 2)
#define AIO_IRQSTA	(0x0008 >> 2)
#define AIO_IRQMSK	(0x000C >> 2)

#define AI_SRCSEL	(0x0010 >> 2)

#define AWR_TRIG	(0x1000 >> 2)
#define AWR_DARB00	(0x1010 >> 2)
#define AWR_DARB01	(0x101C >> 2)
#define AWR_FIFO	(0x1020 >> 2)
#define AWR_AXISZ	(0x1024 >> 2)
#define AWR_STARTADDR	(0x1028 >> 2)
#define AWR_ENDADDR	(0x102C >> 2)
#define AWR_ADDR	(0x1038 >> 2)

#define CDC_CLR		(0x2000 >> 2)
#define CDC_MODE	(0x2010 >> 2)
#define CDC_PACKER	(0x2014 >> 2)
#define CDC_PRESH	(0x2018 >> 2)
#define CDC_POSTSH	(0x201C >> 2)
#define CDC_GAIN	(0x2020 >> 2)

#define I2S_TRIG	(0x3000 >> 2)
#define I2S_CHMODE	(0x3010 >> 2)
#define I2S_MSCK	(0x3014 >> 2)
#define I2S_SMPLEN	(0x3018 >> 2)
#define I2S_DGLHEN	(0x301C >> 2)
#define I2S_DGLHBP	(0x3020 >> 2)
#define I2S_DGLHTH	(0x3024 >> 2)
#define I2S_MUTE	(0x3028 >> 2)

#define ADC_CTRL    (0x4000 >> 2)

/* IRQ bits */
#define ADOIN_IRQMASK  0x3FD
#define ADOOUT_IRQMASK 0x7D

/* Audio source */
#define SRC_NONE 0
#define SRC_I2S  1
#define SRC_ADC  2

/* Codec channel */
#define PASS_ALL	0
#define PASS_NONE	1
#define PASS_DUPL	2 /* for audio-out only */
#define PASS_LEFT	2 /* for audio-in only */
#define PASS_RIGHT	3 /* for audio-in only */

/* Codec mode */
#define CODEC_RAW       0
#define CODEC_ALAW      1
#define CODEC_MULAW     2
#define CODEC_G726_4_LE 3
#define CODEC_G726_4_BE 4
#define CODEC_G726_2_LE 5
#define CODEC_G726_2_BE 6

/* dram */
#define AUDIO_ACCESS_END_SEL 1
#define STARTADDR_DEF 0
#define ENDADDR_DEF   0xFFFFFFF

/* transfer physical address to linear address */
#define PHYS_TO_LINEAR(addr) ((addr) >> 3)
#define BYTE_TO_AXI(byte) (byte >> 3)

#define PHYS_TO_VIOLATION_ADDR(addr, col_addr_type) \
	(((addr) >> (9 + col_addr_type) << 9) \
	+ (((addr) << (23 - col_addr_type) >> (23 - col_addr_type)) >> 3))

/* this is the struct for filling DMA-related registers */
struct hc18xx_dma_desc {
	int id; /* buffer index */
	u32 phys;
	u32 virt;
	u32 byte;
};

struct hc18xx_adowr {
/* ADOW/ADOR FIFO settings */
	u8 burst_len;
	u8 target_fifo_lvl;
	u8 full_fifo_lvl;
};

struct hc18xx_dsp {
	int ch_mode;
	int dsp_mode;
	u16 pre_shift;
	u16 post_shift;
	u16 gain;
	u8  endian;
};

struct hc18xx_i2s {
	u16 ws_tick;
	u16 sd_tick;
	u8  sd_fmt;
	u8  ext_sck;
	u8  ext_ws;
	u8  master_mode;
	u8  sckl_cycle;
	u8  sckh_cycle;
	/* deglitch */
	u8  sck_deglitch_th;
	u8  ws_deglitch_th;
	u8  sd_deglitch_th;
	u8  sck_deglitch_en:1;
	u8  ws_deglitch_en:1;
	u8  sd_deglitch_en:1;
	u8  sck_deglitch_bp:1;
	u8  ws_deglitch_bp:1;
	u8  sd_deglitch_bp:1;
	u8  mute:1;
};

/* register read/write */

static inline void hc_write32(u32 __iomem *addr, u32 val)
{
	writel_relaxed(val, addr);
}

static inline u32 hc_read32(u32 __iomem *addr)
{
	return readl_relaxed(addr);
}

static inline void hc_write16(u16 __iomem *addr, u16 val)
{
	writew_relaxed(val, addr);
}

static inline u16 hc_read16(u16 __iomem *addr)
{
	return readw_relaxed(addr);
}

static inline void hc_write8(u8 __iomem *addr, u8 val)
{
	writeb_relaxed(val, addr);
}

static inline u8 hc_read8(u8 __iomem *addr)
{
	return readb_relaxed(addr);
}

/* functional operations */

u32 hc_read_irq_sta(u32 __iomem *base);
u32 hc_read_irq_msk(u32 __iomem *base);
void hc_clear_irq(u32 __iomem *base, u32 bits);
void hc_mask_irq(u32 __iomem *base, u32 bits);
void hc_unmask_irq(u32 __iomem *base, u32 bits);
void hc_set_audio_src(u32 __iomem *base, int src_type);
int hc_get_audio_src(u32 __iomem *base);

void hc_trigger_audio_wr(u32 __iomem *base);
void hc_trigger_buf_switch(u32 __iomem *base);
void hc_trigger_end_request(u32 __iomem *base);

void hc_set_target_fifo_lvl(u32 __iomem *base, struct hc18xx_adowr *adowr);
void hc_set_fifo_full_lvl(u32 __iomem *base, struct hc18xx_adowr *adowr);
void hc_set_burst_len(u32 __iomem *base, struct hc18xx_adowr *adowr);
void hc_set_fifo_attr(u32 __iomem *base, struct hc18xx_adowr *adowr);
void hc_set_darb_attr(u32 __iomem *base);

void hc_set_dma_period(u32 __iomem *base, struct hc18xx_dma_desc *dma, int violation_en);

void hc_clear_codec(u32 __iomem *base);
void hc_set_codec_ch(u32 __iomem *base, int ch_mode);
void hc_set_codec_mode(u32 __iomem *base, int codec_mode);
void hc_adjust_sample(u32 __iomem *base, struct hc18xx_dsp *dsp);

void hc_start_i2s(u32 __iomem *base);
void hc_stop_i2s(u32 __iomem *base);

void hc_start_adc_clk(u32 __iomem *base);
void hc_stop_adc_clk(u32 __iomem *base);
void hc_start_adc(u32 __iomem *base);
void hc_stop_adc(u32 __iomem *base);

/* I2S hardware */
void hc_set_i2s_data_fmt(u32 __iomem *base, int fmt);
void hc_set_i2s_master_mode(u32 __iomem *base, int ms_mode);
void hc_set_i2s_sck_inter(u32 __iomem *base, int inter);
int hc_get_i2s_sck_inter(u32 __iomem *base);
void hc_set_i2s_ws_inter(u32 __iomem *base, int inter);
int hc_get_i2s_ws_inter(u32 __iomem *base);
void hc_set_i2s_msck_div(u32 __iomem *base, int divisor_0, int divisor_1);
void hc_set_i2s_ws_sd_len(u32 __iomem *base, int length);
void hc_set_i2s_mute(u32 __iomem *base, int mute);
void hc_set_i2s_mux(u32 __iomem *base, struct hc18xx_i2s *i2s);
void hc_set_i2s_deglitch(u32 __iomem *base, struct hc18xx_i2s *i2s);

#endif /* HC18XX_REG_H_ */
