/*
 * Augentix HC18xx Audio Engine Hardware Operations
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <asm/io.h>
#include "hc18xx-reg.h"

/* functional operations */
void hc_clear_irq(u32 __iomem *base, u32 bits)
{
	hc_write32((base + AIO_IRQCLR), bits);
}
EXPORT_SYMBOL(hc_clear_irq);

void hc_mask_irq(u32 __iomem *base, u32 bits)
{
	u32 mask = hc_read32(base + AIO_IRQMSK);
	mask |= bits;
	hc_write32((base + AIO_IRQMSK), mask);
}
EXPORT_SYMBOL(hc_mask_irq);

void hc_unmask_irq(u32 __iomem *base, u32 bits)
{
	u32 mask = hc_read32(base + AIO_IRQMSK);
	mask &= ~bits;
	hc_write32((base + AIO_IRQMSK), mask);
}
EXPORT_SYMBOL(hc_unmask_irq);

u32 hc_read_irq_msk(u32 __iomem *base)
{
	return hc_read32(base + AIO_IRQMSK);
}
EXPORT_SYMBOL(hc_read_irq_msk);

/* read (mask & sta) */
u32 hc_read_irq_sta(u32 __iomem *base)
{
	return (hc_read32(base + AIO_IRQSTA) & (~hc_read_irq_msk(base)));
}
EXPORT_SYMBOL(hc_read_irq_sta);

void hc_set_audio_src(u32 __iomem *base, int src_type)
{
	u8 *reg_b_ptr = (u8 *)(base + AI_SRCSEL) + 1;
	hc_write8(reg_b_ptr , src_type);
}
EXPORT_SYMBOL(hc_set_audio_src);

int hc_get_audio_src(u32 __iomem *base)
{
	u8 *reg_b_ptr = (u8 *)(base + AI_SRCSEL) + 1;
	return hc_read8(reg_b_ptr);
}
EXPORT_SYMBOL(hc_get_audio_src);

void hc_trigger_audio_wr(u32 __iomem *base)
{
	hc_write32((base + AWR_TRIG), TRIGGER);
}
EXPORT_SYMBOL(hc_trigger_audio_wr);

void hc_trigger_buf_switch(u32 __iomem *base)
{
	hc_write32((base + AWR_TRIG), BUF_SWITCH);
}
EXPORT_SYMBOL(hc_trigger_buf_switch);

void hc_trigger_end_request(u32 __iomem *base)
{
	hc_write32((base + AWR_TRIG), REQUEST_END | BUF_SWITCH);
}
EXPORT_SYMBOL(hc_trigger_end_request);

void hc_set_darb_attr(u32 __iomem *base)
{
	u8 *reg_b_ptr;
	u8 col_addr_type;
	u8 bank_addr_type;

	xcm_get_darb_attr(&col_addr_type, &bank_addr_type);

	/* COL_ADDR_TYPE */
	reg_b_ptr = ((u8 *)(base + AWR_DARB00)) + 1 ;
	hc_write8(reg_b_ptr, col_addr_type);

	/* BANK_ADDR_TYPE */
	reg_b_ptr = ((u8 *)(base + AWR_DARB01)) + 2 ;
	hc_write8(reg_b_ptr, bank_addr_type);

	/* ACCESS_END_SEL */
	reg_b_ptr = ((u8 *)(base + AWR_DARB01)) + 1 ;
	hc_write8(reg_b_ptr, AUDIO_ACCESS_END_SEL);
}
EXPORT_SYMBOL(hc_set_darb_attr);

void hc_set_target_fifo_lvl(u32 __iomem *base, struct hc18xx_adowr *adowr)
{
	u8 *reg_b_ptr = (u8 *)(base + AWR_FIFO);
	/* TARGET_FIFO_LEVEL */
	hc_write8(reg_b_ptr, adowr->target_fifo_lvl);
}
EXPORT_SYMBOL(hc_set_target_fifo_lvl);

void hc_set_fifo_full_lvl(u32 __iomem *base, struct hc18xx_adowr *adowr)
{
	u8 *reg_b_ptr = (u8 *)(base + AWR_FIFO) + 2;
	/* FULL_FIFO_LEVEL */
	hc_write8(reg_b_ptr, adowr->full_fifo_lvl);
}
EXPORT_SYMBOL(hc_set_fifo_full_lvl);

void hc_set_burst_len(u32 __iomem *base, struct hc18xx_adowr *adowr)
{
	u8 *reg_b_ptr;

	/* TARGET_BURST_LEN */
	reg_b_ptr = (u8 *)(base + AWR_DARB01);
	hc_write8(reg_b_ptr, adowr->burst_len);
}
EXPORT_SYMBOL(hc_set_burst_len);

void hc_set_fifo_attr(u32 __iomem *base, struct hc18xx_adowr *adowr)
{
	hc_set_target_fifo_lvl(base, adowr);
	hc_set_fifo_full_lvl(base, adowr);
	hc_set_burst_len(base, adowr);
}
EXPORT_SYMBOL(hc_set_fifo_attr);

void hc_set_dma_period(u32 __iomem *base, struct hc18xx_dma_desc *dma, int violation_en)
{
	u16 *reg_s_ptr;
	u32 laddr;
	u16 len;

	/* INI_ADDR_LINEAR */
	laddr = PHYS_TO_LINEAR(dma->phys);
	hc_write32(base + AWR_ADDR, laddr);

	/* ACCESS_LENGTH */
	reg_s_ptr = (u16 *)(base + AWR_AXISZ);
	len = (u16)BYTE_TO_AXI(dma->byte); /* byte to AXI-word(8-byte) length */
	hc_write16(reg_s_ptr, len);

	/* VIOLATION */
	if (1 == violation_en) {
		u8 col_addr_type;
		u8 bank_addr_type;

		xcm_get_darb_attr(&col_addr_type, &bank_addr_type);
		hc_write32(base + AWR_STARTADDR, PHYS_TO_VIOLATION_ADDR(dma->phys, col_addr_type));
		hc_write32(base + AWR_ENDADDR, PHYS_TO_VIOLATION_ADDR(dma->phys + dma->byte, col_addr_type));
	} else {
		hc_write32(base + AWR_STARTADDR, STARTADDR_DEF);
		hc_write32(base + AWR_ENDADDR, ENDADDR_DEF);
	}

}
EXPORT_SYMBOL(hc_set_dma_period);

void hc_clear_codec(u32 __iomem *base)
{
	hc_write32(base + CDC_CLR, CLEAR);
}
EXPORT_SYMBOL(hc_clear_codec);

void hc_set_codec_ch(u32 __iomem *base, int ch_mode)
{
	u8 *reg_b_ptr = (u8 *)(base + CDC_MODE);
	hc_write8(reg_b_ptr, ch_mode);
}
EXPORT_SYMBOL(hc_set_codec_ch);

void hc_set_codec_mode(u32 __iomem *base, int codec_mode)
{
	u8 *mode_ptr;
	u8 *packer_ptr;

	mode_ptr = (u8 *)(base + CDC_MODE);
	packer_ptr = (u8 *)(base + CDC_PACKER);

	switch (codec_mode)
	{
	case CODEC_RAW:
		hc_write8(mode_ptr + 1, 0);
		hc_write8(packer_ptr, 0);
		break;
	case CODEC_ALAW:
		hc_write8(mode_ptr + 1, 1);
		hc_write8(mode_ptr + 2, 0);
		hc_write8(packer_ptr, 1);
		break;
	case CODEC_MULAW:
		hc_write8(mode_ptr + 1, 1);
		hc_write8(mode_ptr + 2, 1);
		hc_write8(packer_ptr, 1);
		break;
	case CODEC_G726_2_LE:
		hc_write8(mode_ptr + 1, 2);
		hc_write8(mode_ptr + 3, 0);
		hc_write8(packer_ptr, 3);
		hc_write8(packer_ptr + 1, 1);
		break;
	case CODEC_G726_2_BE:
		hc_write8(mode_ptr + 1, 2);
		hc_write8(mode_ptr + 3, 0);
		hc_write8(packer_ptr, 3);
		hc_write8(packer_ptr + 1, 0);
		break;
	case CODEC_G726_4_LE:
		hc_write8(mode_ptr + 1, 2);
		hc_write8(mode_ptr + 3, 1);
		hc_write8(packer_ptr, 2);
		hc_write8(packer_ptr + 1, 1);
		break;
	case CODEC_G726_4_BE:
		hc_write8(mode_ptr + 1, 2);
		hc_write8(mode_ptr + 3, 1);
		hc_write8(packer_ptr, 2);
		hc_write8(packer_ptr + 1, 0);
		break;
	default:
		break;
	}
}
EXPORT_SYMBOL(hc_set_codec_mode);

void hc_adjust_sample(u32 __iomem *base, struct hc18xx_dsp *dsp)
{
	u16 *reg_s_ptr;

	reg_s_ptr = (u16 *)(base + CDC_PRESH);
	hc_write16(reg_s_ptr, dsp->pre_shift);

	reg_s_ptr = (u16 *)(base + CDC_POSTSH);
	hc_write16(reg_s_ptr, dsp->post_shift);

	reg_s_ptr = (u16 *)(base + CDC_GAIN);
	hc_write16(reg_s_ptr, dsp->gain);
}
EXPORT_SYMBOL(hc_adjust_sample);

void hc_start_i2s(u32 __iomem *base)
{
	hc_write32(base + I2S_TRIG, I2S_START);
}
EXPORT_SYMBOL(hc_start_i2s);

void hc_stop_i2s(u32 __iomem *base)
{
	hc_write32(base + I2S_TRIG, I2S_STOP);
}
EXPORT_SYMBOL(hc_stop_i2s);

void hc_start_adc_clk(u32 __iomem *base)
{
	hc_write8((u8 *)(base + ADC_CTRL), ADC_CLK_START);
}
EXPORT_SYMBOL(hc_start_adc_clk);

void hc_stop_adc_clk(u32 __iomem *base)
{
	hc_write8((u8 *)(base + ADC_CTRL), ADC_CLK_STOP);
}
EXPORT_SYMBOL(hc_stop_adc_clk);

void hc_start_adc(u32 __iomem *base)
{
	hc_write8((u8 *)(base + ADC_CTRL), ADC_AUDIO_START);
}
EXPORT_SYMBOL(hc_start_adc);

void hc_stop_adc(u32 __iomem *base)
{
	hc_write8((u8 *)(base + ADC_CTRL), ADC_AUDIO_STOP);
}
EXPORT_SYMBOL(hc_stop_adc);

void hc_set_i2s_data_fmt(u32 __iomem *base, int fmt)
{
	hc_write8((u8 *)(base + I2S_CHMODE), fmt);
}
EXPORT_SYMBOL(hc_set_i2s_data_fmt);

void hc_set_i2s_master_mode(u32 __iomem *base, int ms_mode)
{
	u8 *mode = (u8 *)(base + I2S_CHMODE) + 1;
	hc_write8(mode , ms_mode);
}
EXPORT_SYMBOL(hc_set_i2s_master_mode);

void hc_set_i2s_sck_inter(u32 __iomem *base, int inter)
{
	hc_write8((u8 *)(base + I2S_CHMODE) + 2, inter);
}
EXPORT_SYMBOL(hc_set_i2s_sck_inter);

int hc_get_i2s_sck_inter(u32 __iomem *base)
{
	return hc_read8((u8 *)(base + I2S_CHMODE) + 2);
}
EXPORT_SYMBOL(hc_get_i2s_sck_inter);

void hc_set_i2s_ws_inter(u32 __iomem *base, int inter)
{
	hc_write8((u8 *)(base + I2S_CHMODE) + 3, inter);
}
EXPORT_SYMBOL(hc_set_i2s_ws_inter);

int hc_get_i2s_ws_inter(u32 __iomem *base)
{
	return hc_read8((u8 *)(base + I2S_CHMODE) + 3);
}
EXPORT_SYMBOL(hc_get_i2s_ws_inter);

void hc_set_i2s_msck_div(u32 __iomem *base, int divisor_0, int divisor_1)
{
	hc_write8((u8 *)(base + I2S_MSCK), divisor_0);
	hc_write8((u8 *)(base + I2S_MSCK) + 2, divisor_1);
}
EXPORT_SYMBOL(hc_set_i2s_msck_div);

void hc_set_i2s_ws_sd_len(u32 __iomem *base, int length)
{
	hc_write8((u8 *)(base + I2S_SMPLEN), length);
	hc_write8((u8 *)(base + I2S_SMPLEN) + 2, length);
}
EXPORT_SYMBOL(hc_set_i2s_ws_sd_len);

void hc_set_i2s_mute(u32 __iomem *base, int mute)
{
	hc_write8((u8 *)(base + I2S_MUTE) + 1, mute);
}
EXPORT_SYMBOL(hc_set_i2s_mute);

void hc_set_i2s_mux(u32 __iomem *base, struct hc18xx_i2s *i2s)
{
	u8 *ext;

	/* USE_SCK_INTER */
	ext = (u8 *)(base + I2S_CHMODE) + 2;
	hc_write8(ext , i2s->ext_sck);

	/* USE_WS_INTER */
	hc_write8(ext + 1 , i2s->ext_ws);
}
EXPORT_SYMBOL(hc_set_i2s_mux);

void hc_set_i2s_deglitch(u32 __iomem *base, struct hc18xx_i2s *i2s)
{
	u8 *reg_b_ptr;

	reg_b_ptr = (u8 *)(base + I2S_DGLHEN);
	hc_write8(reg_b_ptr    , i2s->sck_deglitch_en);
	hc_write8(reg_b_ptr + 1, i2s->ws_deglitch_en);
	hc_write8(reg_b_ptr + 2, i2s->sd_deglitch_en);

	reg_b_ptr = (u8 *)(base + I2S_DGLHBP);
	hc_write8(reg_b_ptr    , i2s->sck_deglitch_bp);
	hc_write8(reg_b_ptr + 1, i2s->ws_deglitch_bp);
	hc_write8(reg_b_ptr + 2, i2s->sd_deglitch_bp);

	reg_b_ptr = (u8 *)(base + I2S_DGLHTH);
	hc_write8(reg_b_ptr    , i2s->sck_deglitch_th);
	hc_write8(reg_b_ptr + 1, i2s->ws_deglitch_th);
	hc_write8(reg_b_ptr + 2, i2s->sd_deglitch_th);
}
EXPORT_SYMBOL(hc_set_i2s_deglitch);

MODULE_AUTHOR("Henry Liu<henry.liu@augentix.com>");
MODULE_DESCRIPTION("Augentix HC18XX ASoC REG Library");
MODULE_LICENSE("GPL");
