#ifndef __HC18XX_MTD_BOOTCONF_H__
#define __HC18XX_MTD_BOOTCONF_H__
#include <linux/ioctl.h>


#define MAX_PART_NUM 20

struct partition_info_t{
	   unsigned int flash_start;         //The start address of this partition.
	   unsigned int flash_size;          //The size of this partition.
	   unsigned int mem_start;           //It's the start address that Image is loaded to DRAM.
	   unsigned int image_size;          //image_page_num = ( (image_size+1) / page_size) , page_size = 2048 bytes
	   unsigned int image_csum;          //image_checksum
	   signed char  label[16];
};

struct partition_table_t{
	   unsigned int  part_num;           //total partition numbers.
	   unsigned int  boot_part;          //which partition is used to boot system.
	   struct partition_info_t pInfo[MAX_PART_NUM];  //record the information for each partition.
};

struct system_info_t{
	   char serial_number[32];
	   unsigned char rsv[64];
};

struct net_info_t{
	   char mac_address[18];
	   unsigned char rsv[52];
};

struct boot_configs_t{
	   unsigned char   signiture[16];                  //16 #BOOTCONFIGS\0\0\0\0
	   struct partition_table_t part_tb;               //648
	   struct system_info_t sys;                       //96
	   struct net_info_t net;                          //70
//	   unsigned char   rsv[190];                       //190
	   unsigned char   rsv[110];                       //190
	   unsigned int self_checksum;                     //4
};

#define NAND_PG_SZ 2048
#define BOOT_CONFIGS_END_OFFSET 4096

#define IOC_SET_MAGIC 'w'
#define IOCTL_SET_NET_INFO     _IOW(IOC_SET_MAGIC, 0, struct net_info_t)
#define IOCTL_SET_SYS_INFO     _IOW(IOC_SET_MAGIC, 1, struct system_info_t)
#define IOCTL_SET_BCONF        _IOW(IOC_SET_MAGIC, 2, struct boot_configs_t)

#define IOC_GET_MAGIC 'r'
#define IOCTL_GET_NET_INFO     _IOR(IOC_GET_MAGIC, 0, struct net_info_t)
#define IOCTL_GET_SYS_INFO     _IOR(IOC_GET_MAGIC, 1, struct system_info_t)
#define IOCTL_GET_BCONF        _IOR(IOC_GET_MAGIC, 2, struct boot_configs_t)


#endif
