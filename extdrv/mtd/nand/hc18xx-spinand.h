#ifndef __HC18XX_MTD_SPI_NAND_H__
#define __HC18XX_MTD_SPI_NAND_H__

#include <linux/spi/hc18xx-spi-comm.h>

//#define CONFIG_MTD_SPINAND_ONDIEECC

#define BUFSIZE 2112
#define CACHE_BUF 2112

/* cmd */
#define CMD_READ                            0x13
//#define CMD_READ_RDM                      0x03
#define CMD_READ_QRDM                       0x6B
#define CMD_READ_ID                         0x9F
#define CMD_READ_REG                        0x0F
#define CMD_WRITE_REG                       0x1F
#define CMD_WR_ENABLE                       0x06
//#define CMD_WR_DISABLE                    0x04
//#define CMD_PROG_PAGE                     0x84
//#define CMD_PROG_PAGE_CLRCACHE            0x02
#define CMD_PROG_PAGE_CLRCACHE_0x32         0x32
#define CMD_PROG_PAGE_CLRCACHE_0x34         0x34
#define CMD_PROG_PAGE_EXC                   0x10
#define CMD_ERASE_BLK                       0xD8
#define CMD_RESET                           0xFF

/* feature/ status reg */
#define REG_BLOCK_LOCK                      0xA0
#define REG_OTP                             0xB0
#define REG_STATUS                          0xC0/* timing */

/* status */
#define STATUS_OIP_MASK                     0x01
#define STATUS_READY                       (0 << 0)
#define STATUS_BUSY                        (1 << 0)

#define STATUS_E_FAIL_MASK                  0x04
#define STATUS_E_FAIL                      (1 << 2)

#define STATUS_P_FAIL_MASK                  0x08
#define STATUS_P_FAIL                      (1 << 3)

#define STATUS_ECC_MASK                     0x30
#define STATUS_ECC_1BIT_CORRECTED          (1 << 4)
#define STATUS_ECC_ERROR                   (2 << 4)
#define STATUS_ECC_RESERVED                (3 << 4)

/*ECC enable defines*/
#define OTP_ECC_MASK                        0x10
#define OTP_ECC_OFF                         0
#define OTP_ECC_ON                          1

/*block lock*/
#define BL_ALL_UNLOCKED                     0

struct spinand_info {
	struct nand_ecclayout *ecclayout;
	struct spi_device *spi;
	void *priv;
};

struct spinand_state {
	uint32_t	col;
	uint32_t	row;
	int		buf_ptr;
	u8		*buf;
};

//spi command structure
struct spinand_cmd {
	u8   cmd;
	u8   bDFS_32; /* 32 bits per word */
	u32  n_addr;  /* Number of address */
	u8   addr[4]; /* Reg Offset */
	u32  n_dummy; /* Dummy use */
	u32  n_tx;    /* Number of tx bytes */
	u8  *tx_buf;  /* Tx buf */
	u32  n_rx;    /* Number of rx bytes */
	u8  *rx_buf;  /* Rx buf */
};

//Qual spi command structure
struct qspinand_cmd {
	u8   cmd;
	u8   bDFS_32; /* 32 bits per word */
	u8   n_addr;  /* Number of address */
	u8   addr[4]; /* Reg Offset */
	u8   n_dummy; /* Dummy use */
	u32  n_tx;    /* Number of tx bytes */
	u8  *tx_buf;  /* Tx buf */
	u32  n_rx;    /* Number of rx bytes */
	u8  *rx_buf;  /* Rx buf */
};

#endif
