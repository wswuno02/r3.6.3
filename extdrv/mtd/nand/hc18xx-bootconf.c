#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/mtd/mtd.h>
#include <linux/err.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/version.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <asm/uaccess.h>
#include <linux/sched.h>
#include <linux/slab.h>

#include "hc18xx-bootconf.h"

static struct mtd_info *mtd_info = NULL;
static dev_t first;       // global variable for the first device number
static struct cdev c_dev; // global variable for the character device structure
static struct class *cl;  // global variable for the device class

static void erase_partition(struct mtd_info *mtd_info);
static void erase_sector(struct mtd_info *mtd_info, unsigned int start, unsigned int len);
static void erase_callback (struct erase_info *instr);

/*
 * calcChecksum - calculate checksum
 *
 * calcChecksum is the function of calculate checksum for check partition
 * information table correct or not.
 */
static uint32_t calcChecksum ( uint32_t *data, uint32_t count )
{
	uint32_t sum = 0;
	uint32_t inverse = 0xB139D2E5;
	int i;

	for (i=0;i<count;i++) {
    	sum = ( sum >> 1 ) | ( sum << 31 );
    	sum = sum ^ data[i];
    	sum = sum ^ inverse;
	}

	return sum;
}

static int get_bootconfigs(struct boot_configs_t* ptr)
{
	size_t rlen;
	uint32_t off = 0;
	uint32_t blk_size = (uint32_t)mtd_info->erasesize;
    uint32_t part_size = (uint32_t)mtd_info->size;

	do {

		mtd_info->_read(mtd_info, off, sizeof(struct boot_configs_t),&rlen, (u_char*)ptr);

		if ( ptr->signiture[0] ==  '#'  && ptr->signiture[1] ==  'B'
		  && ptr->signiture[2] ==  'O'  && ptr->signiture[3] ==  'O'
		  && ptr->signiture[4] ==  'T'  && ptr->signiture[5] ==  'C'
		  && ptr->signiture[6] ==  'O'  && ptr->signiture[7] ==  'N'
		  && ptr->signiture[8] ==  'F'  && ptr->signiture[9] ==  'I'
		  && ptr->signiture[10] == 'G'  && ptr->signiture[11] == 'S'
		  && ptr->signiture[12] == '\0' && ptr->signiture[13] == '\0'
		  && ptr->signiture[14] == '\0' && ptr->signiture[15] == '\0' ) {

		  if (ptr->self_checksum == calcChecksum ( (uint32_t*)ptr, 255 )) {
		  	  return 0;
		  }
	    }

		off += blk_size;
	} while(off < part_size);

	pr_err("No Found Bootconfigs\n");
	return -1;
}

static int set_bootconfigs(struct file *fp,struct boot_configs_t* ptr)
{
	size_t rlen;
	uint32_t off = 0;
	uint32_t blk_size = (uint32_t)mtd_info->erasesize;
    //uint32_t part_size = (uint32_t)mtd_info->size;
	u_char *w_buf = fp->private_data;

	ptr->self_checksum = calcChecksum ( (uint32_t*)ptr, 255 );

	memcpy(w_buf,ptr,sizeof(struct boot_configs_t));

	do {
		mtd_info->_write(mtd_info, off, NAND_PG_SZ, &rlen, (u_char*)w_buf);
		off += blk_size;
	} while(off < BOOT_CONFIGS_END_OFFSET);

	return 0;
}


static void erase_partition(struct mtd_info *mtd_info) {
    unsigned int start;
	uint32_t blk_size = (uint32_t)mtd_info->erasesize;
    //uint32_t part_size = (uint32_t)mtd_info->size;

    for(start = 0; start < BOOT_CONFIGS_END_OFFSET; start += blk_size)
        erase_sector(mtd_info, start, blk_size);
}

static void erase_sector(struct mtd_info *mtd_info, unsigned int start, unsigned int len)
{
    int ret;
    struct erase_info ei = {0};
    wait_queue_head_t waitq;
    DECLARE_WAITQUEUE(wait, current);

    init_waitqueue_head(&waitq);
    ei.addr = start;
    ei.len = mtd_info->erasesize;
    ei.mtd = mtd_info;
    ei.callback = erase_callback;
    ei.priv = (unsigned long)&waitq;
    ret = mtd_info->_erase(mtd_info, &ei);
    if (!ret) {
        set_current_state(TASK_UNINTERRUPTIBLE);
        add_wait_queue(&waitq, &wait);
        if (ei.state != MTD_ERASE_DONE && ei.state != MTD_ERASE_FAILED)
            schedule();
        remove_wait_queue(&waitq, &wait);
        set_current_state(TASK_RUNNING);

        ret = (ei.state == MTD_ERASE_FAILED)?-EIO:0;
    }
}

static void erase_callback (struct erase_info *instr) {
    wake_up((wait_queue_head_t *)instr->priv);
}

static int bconf_open(struct inode *inod, struct file *fp)
{
	int i;
	u_char *w_buf = NULL;

	w_buf = kmalloc(NAND_PG_SZ, GFP_KERNEL);
	if (w_buf == NULL) {
		return -ENOMEM;
	}
	fp->private_data = w_buf;

	for (i=0; i<MAX_PART_NUM; i++) {

		  mtd_info = get_mtd_device(NULL, i);
		  if (IS_ERR(mtd_info)) {
			  pr_debug("No device for num %d\n", i);
			  continue;
		  }

		  if (mtd_info->type == MTD_ABSENT) {
			  put_mtd_device(mtd_info);
			  continue;
		  }

		  if (strcmp(mtd_info->name, "hc18xx-ptable")) {
			  put_mtd_device(mtd_info);
			  continue;
		  }

		  pr_debug("mtd%d: %8.8llx %8.8x \"%s\"\n",
						 mtd_info->index, (unsigned long long)mtd_info->size,
						 mtd_info->erasesize, mtd_info->name);

		  return 0;
	}

	mtd_info = NULL;
	return 0;
}

static int bconf_close(struct inode *inod, struct file *fp)
{
	u_char *w_buf = fp->private_data;

	if (mtd_info) {
		put_mtd_device(mtd_info);
	}

	if (w_buf) {
	   kfree(w_buf);
	}

	return 0;
}

static ssize_t bconf_read(struct file *fp, char __user *buf, size_t len, loff_t *off)
{
  	return 0;
}

static ssize_t bconf_write(struct file *fp, const char __user *buf, size_t len, loff_t *off)
{
    return len;
}

static long bconf_ioctl(struct file *fp, unsigned int cmd, unsigned long arg)
{
	int ret = 0;
	struct boot_configs_t* bconf = (struct boot_configs_t*)fp->private_data;

	if (get_bootconfigs(bconf)) {
	   return -ENOENT;
	}

	if (_IOC_DIR(cmd) == _IOC_READ) {

		switch (cmd) {
				case IOCTL_GET_NET_INFO:
					 if (copy_to_user((int __user *)arg, &bconf->net, sizeof(struct net_info_t))) {
						  return -EFAULT;
					 }
					 break;
				case IOCTL_GET_SYS_INFO:
					 if (copy_to_user((int __user *)arg, &bconf->sys, sizeof(struct system_info_t))) {
						  return -EFAULT;
					 }
					 break;
				case IOCTL_GET_BCONF:
					 if (copy_to_user((int __user *)arg, bconf, sizeof(struct boot_configs_t))) {
						  return -EFAULT;
					 }
					 break;
	            default:
	                 return -ENOTTY;
		}

	} else if (_IOC_DIR(cmd) == _IOC_WRITE) {

		switch (cmd) {
				case IOCTL_SET_NET_INFO:
					 if (copy_from_user(&bconf->net, (void __user *)arg, sizeof(struct net_info_t))) {
						  return -EFAULT;
					 }
					 break;
				case IOCTL_SET_SYS_INFO:
					 if (copy_from_user(&bconf->sys, (void __user *)arg, sizeof(struct system_info_t))) {
						  return -EFAULT;
					 }
					 break;
				case IOCTL_SET_BCONF:
					 if (copy_from_user(bconf, (void __user *)arg, sizeof(struct boot_configs_t))) {
						  return -EFAULT;
					 }
					 break;
				default:
					 return -ENOTTY;
		}

		erase_partition(mtd_info);
		set_bootconfigs(fp,bconf);

	} else {
		pr_err("No such command\n");
		return -ENOTTY;
	}

	return ret;

}

static struct file_operations bconf_fops =
{
  .owner = THIS_MODULE,
  .open = bconf_open,
  .release = bconf_close,
  .read = bconf_read,
  .write = bconf_write,
  .unlocked_ioctl = bconf_ioctl,
};

static int __init hc18xx_bootconf_init(void)
{
	printk(KERN_INFO "hc18xx_bootconf_init");

	if (alloc_chrdev_region(&first, 0, 1, "bootconf") < 0) {
	  	return -1;
	}

	if ((cl = class_create(THIS_MODULE, "bootconf")) == NULL) {
	  	unregister_chrdev_region(first, 1);
	  	return -1;
	}

	if (device_create(cl, NULL, first, NULL, "bootconf") == NULL) {
	  	class_destroy(cl);
	  	unregister_chrdev_region(first, 1);
	  	return -1;
	}

	cdev_init(&c_dev, &bconf_fops);
	if (cdev_add(&c_dev, first, 1) == -1) {
		device_destroy(cl, first);
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
		return -1;
	}
	return 0;
}

static void __exit hc18xx_bootconf_exit(void)
{
	cdev_del(&c_dev);
	device_destroy(cl, first);
	class_destroy(cl);
	unregister_chrdev_region(first, 1);
	printk(KERN_INFO "hc18xx_bootconf_exit");
}

module_init(hc18xx_bootconf_init);
module_exit(hc18xx_bootconf_exit);
MODULE_DESCRIPTION(" module");
MODULE_AUTHOR("Nick Lin<Nick.lin@augentix.com>");
MODULE_LICENSE("GPL");
