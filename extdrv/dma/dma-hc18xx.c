/*
 * AUGENTIX INC. - OPEN-SOURCE UNDER GPL LICENSE
 *
 * dma-hc18xx.c - Augentix DMA engine
 * Copyright (C) 2018, 2019, Augentix Inc. <shihchieh.lin@augentix.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation, incorporated herein by reference.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <linux/err.h>
#include <linux/init.h>
#include <linux/types.h>
#include <linux/mm.h>
#include <linux/interrupt.h>
#include <linux/spinlock.h>
#include <linux/device.h>
#include <linux/dma-mapping.h>
#include <linux/slab.h>
#include <linux/platform_device.h>
#include <linux/clk.h>
#include <linux/dmaengine.h>
#include <linux/module.h>
#include <linux/of_device.h>
#include <linux/of_dma.h>
#include <asm/irq.h>

#include <linux/crypto.h>
#include <crypto/algapi.h>
#include <crypto/aes.h>
#include <crypto/ctr.h>

#include <linux/kthread.h>

#include "dmaengine.h"
#include "dma-hc18xx.h"
#include <mach/hc18xx.h>
#include <linux/clk.h>
#include <linux/io.h>
#include <linux/delay.h>

#define EN_DBG_AES 0
struct dma_chan *memcpy_chan;

noinline void augentix_dma_stub(void)
{
	asm volatile("stub_start:\n"
	             "nop\n");
}
EXPORT_SYMBOL(augentix_dma_stub);

static void hc18xx_aes_complete(struct hc18xx_dma_engine *dev, int err);

static int setup_xfer_desc_next(struct hc18xx_dma_engine *pdma, struct hc18xx_dma_desc *d);

static bool xfer_done(struct hc18xx_dma_desc *d);

static int enable_dma_access(struct hc18xx_dma_engine *pdma);

static void *g_dma_engine = NULL;
static uint8_t g_is_qfn = 0;

static struct hc18xx_dma_channel *to_hc18xx_dma_chan(struct dma_chan *chan)
{
	return container_of(chan, struct hc18xx_dma_channel, chan);
}

static void hc18xx_dma_writel(struct hc18xx_dma_engine *pdma, unsigned val, unsigned offset)
{
	__raw_writel(val, pdma->base + offset);
}

static unsigned hc18xx_dma_readl(struct hc18xx_dma_engine *pdma, unsigned offset)
{
	return __raw_readl(pdma->base + offset);
}

static const struct of_device_id hc18xx_dma_of_dev_id[] = { {
	                                                            .compatible = "augentix,dma",
	                                                    },
	                                                    {} };
MODULE_DEVICE_TABLE(of, hc18xx_dma_of_dev_id);

static int set_aes_endian(struct hc18xx_dma_engine *pdma, u32 edian)
{
	u32 val = 0;

	val = (hc18xx_dma_readl(pdma, AES16_ENDIN_BLOCKM) & (~(0x00000100)));
	hc18xx_dma_writel(pdma, (val | edian), AES16_ENDIN_BLOCKM);

	return 0;
}

static int set_aes_blk_mode(struct hc18xx_dma_engine *pdma, u32 mode)
{
	u32 val = 0;

	val = (hc18xx_dma_readl(pdma, AES16_ENDIN_BLOCKM) & (~(0x00010000)));
	hc18xx_dma_writel(pdma, (val | mode), AES16_ENDIN_BLOCKM);

	return 0;
}

static int set_aes_init_vector(struct hc18xx_dma_engine *pdma, u32 *p_iv)
{
	if (pdma->is_ablk_mode) {
		hc18xx_dma_writel(pdma, BigtoLittle32(*((p_iv) + 3)), AES17_INIT_VEC_0);
		hc18xx_dma_writel(pdma, BigtoLittle32(*((p_iv) + 2)), AES18_INIT_VEC_1);
		hc18xx_dma_writel(pdma, BigtoLittle32(*((p_iv) + 1)), AES19_INIT_VEC_2);
		hc18xx_dma_writel(pdma, BigtoLittle32(*((p_iv) + 0)), AES20_INIT_VEC_3);
	} else {
		hc18xx_dma_writel(pdma, *((p_iv) + 3), AES17_INIT_VEC_0);
		hc18xx_dma_writel(pdma, *((p_iv) + 2), AES18_INIT_VEC_1);
		hc18xx_dma_writel(pdma, *((p_iv) + 1), AES19_INIT_VEC_2);
		hc18xx_dma_writel(pdma, *((p_iv) + 0), AES20_INIT_VEC_3);
	}

	return 0;
}

static int set_aes_key(struct hc18xx_dma_engine *pdma, u32 *p_key)
{
	if (pdma->is_ablk_mode) {
		hc18xx_dma_writel(pdma, BigtoLittle32(*((p_key) + 7)), AES04_KEY_0);
		hc18xx_dma_writel(pdma, BigtoLittle32(*((p_key) + 6)), AES05_KEY_1);
		hc18xx_dma_writel(pdma, BigtoLittle32(*((p_key) + 5)), AES06_KEY_2);
		hc18xx_dma_writel(pdma, BigtoLittle32(*((p_key) + 4)), AES07_KEY_3);
		hc18xx_dma_writel(pdma, BigtoLittle32(*((p_key) + 3)), AES08_KEY_4);
		hc18xx_dma_writel(pdma, BigtoLittle32(*((p_key) + 2)), AES09_KEY_5);
		hc18xx_dma_writel(pdma, BigtoLittle32(*((p_key) + 1)), AES10_KEY_6);
		hc18xx_dma_writel(pdma, BigtoLittle32(*((p_key) + 0)), AES11_KEY_7);
	} else {
		hc18xx_dma_writel(pdma, *((p_key) + 7), AES04_KEY_0);
		hc18xx_dma_writel(pdma, *((p_key) + 6), AES05_KEY_1);
		hc18xx_dma_writel(pdma, *((p_key) + 5), AES06_KEY_2);
		hc18xx_dma_writel(pdma, *((p_key) + 4), AES07_KEY_3);
		hc18xx_dma_writel(pdma, *((p_key) + 3), AES08_KEY_4);
		hc18xx_dma_writel(pdma, *((p_key) + 2), AES09_KEY_5);
		hc18xx_dma_writel(pdma, *((p_key) + 1), AES10_KEY_6);
		hc18xx_dma_writel(pdma, *((p_key) + 0), AES11_KEY_7);
	}

	return 0;
}

static int set_channel_operation_mode(struct hc18xx_dma_engine *pdma, int ch_num, unsigned int oper_mode)
{
	u32 val = 0;

	val = (hc18xx_dma_readl(pdma, DMA_CH_CTRL1(ch_num)) & (~(0x00000003))) | (oper_mode);
	hc18xx_dma_writel(pdma, val, DMA_CH_CTRL1(ch_num));

	return 0;
}

static int set_channel_access_mode(struct hc18xx_dma_engine *pdma, int ch_num, unsigned int access_mode)
{
	u32 val = 0;

	val = (hc18xx_dma_readl(pdma, DMA_CH_CTRL1(ch_num)) & (~(0x00000300))) | (access_mode);
	hc18xx_dma_writel(pdma, val, DMA_CH_CTRL1(ch_num));

	return 0;
}

static int set_bank_interleave_type(struct hc18xx_dma_engine *pdma, int ch_num, unsigned int type)
{
	u32 val = 0;
	u32 clr_mask = 0;
	u32 shift = 0;

	if (ch_num < 4) {
		shift = 8 * ch_num;
		clr_mask = ~(0x3 << shift);
		val = (hc18xx_dma_readl(pdma, DMA46_BANK_INTER_TP_0) & (clr_mask)) | (type << shift);
		hc18xx_dma_writel(pdma, val, DMA46_BANK_INTER_TP_0);
	} else {
		shift = 8 * (ch_num - 4);
		clr_mask = ~(0x3 << shift);
		val = (hc18xx_dma_readl(pdma, DMA47_BANK_INTER_TP_1) & (clr_mask)) | (type << shift);
		hc18xx_dma_writel(pdma, val, DMA47_BANK_INTER_TP_1);
	}
	return 0;
}

static void dma_slave_config(struct hc18xx_dma_engine *pdma, struct dma_args *args)
{
	int i = 0;

	if (args->oper_mode == DMA_NORMAL) {
		for (i = 0; i < MAX_DMA_ACCESS_CNT; i++) {
			set_channel_operation_mode(pdma, i, AES_MODE_BYPASS);
		}
	} else {
		for (i = 0; i < MAX_DMA_ACCESS_CNT; i++) {
			set_channel_operation_mode(pdma, i, AES_MODE_NORMAL);
		}

		if (args->oper_mode == DMA_AES_CBC_BE || args->oper_mode == DMA_AES_CBC_LE) {
			set_aes_blk_mode(pdma, AES_CBC_MODE);
			set_aes_key(pdma, args->aes_key);
			set_aes_init_vector(pdma, args->aes_init_vet);
			if (args->oper_mode == DMA_AES_CBC_BE) {
				set_aes_endian(pdma, AES_BIG_ENDIAN);
			} else {
				set_aes_endian(pdma, AES_LIT_ENDIAN);
			}
		} else if (args->oper_mode == DMA_AES_ECB_BE || args->oper_mode == DMA_AES_ECB_LE) {
			set_aes_blk_mode(pdma, AES_ECB_MODE);
			set_aes_key(pdma, args->aes_key);
			if (args->oper_mode == DMA_AES_ECB_BE) {
				set_aes_endian(pdma, AES_BIG_ENDIAN);
			} else {
				set_aes_endian(pdma, AES_LIT_ENDIAN);
			}
		}
	}
}

static void set_addr_type(struct hc18xx_dma_engine *pdma)
{
	if (g_is_qfn) {
		pdma->bank_addr_type = BANK_ADDR_TYPE_4;
		pdma->bank_intertleave = BANK_INTERLEAVE_4;
		pdma->col_addr_type = COL_ADDR_TYPE_2048;
	} else {
		pdma->bank_addr_type = BANK_ADDR_TYPE_8;
		pdma->bank_intertleave = BANK_INTERLEAVE_8;
		pdma->col_addr_type = COL_ADDR_TYPE_2048;
	}
}

static void hc18xx_dma_init(struct hc18xx_dma_engine *pdma)
{
	u32 val = 0;
	u32 i = 0;

	hc18xx_sw_rst(SW_RST_DMA);
	set_addr_type(pdma);
	/*enable all irq*/
	val = hc18xx_dma_readl(pdma, DMA03_IRQ_MASK) & (~IRQ_MASK_ACCESS_END);
	hc18xx_dma_writel(pdma, val, DMA03_IRQ_MASK);

	/* bank_addr_type,target_burst_len_r,target_burst_len_w */
	val = (pdma->bank_addr_type << 16) | TARGET_BURST_LEN_R(16) | TARGET_BURST_LEN_W(16) | ACCESS_END_SEL_1;
	hc18xx_dma_writel(pdma, val, DMA07_CTRL);

	/* col_addr_type */
	val = hc18xx_dma_readl(pdma, DMA04_CTRL) | (pdma->col_addr_type << 8) | TOTAL_ACCESS_CNT(1);
	hc18xx_dma_writel(pdma, val, DMA04_CTRL);

	/*fifo level*/
	val = TARGET_FIFO_LEVEL_R(16) | TARGET_FIFO_LEVEL_W(16) | TARGET_FULL_LEVEL_R(32) | TARGET_FULL_LEVEL_W(32);
	hc18xx_dma_writel(pdma, val, DMA08_CTRL);

	for (i = 0; i < MAX_DMA_ACCESS_CNT; i++) {
		set_channel_operation_mode(pdma, i, AES_MODE_BYPASS);
		set_channel_access_mode(pdma, i, AES_ACCESS_LINEAR);
		set_bank_interleave_type(pdma, i, pdma->bank_intertleave);
	}
}

static void dma_irq_handle_channel(struct hc18xx_dma_channel *hc18xx_dmac)
{
	struct hc18xx_dma_engine *hc18xx_dma = hc18xx_dmac->dma;
	struct hc18xx_dma_desc *desc;
	unsigned long flags;

	spin_lock_irqsave(&hc18xx_dma->lock, flags);

	if (list_empty(&hc18xx_dmac->ld_active)) {
		spin_unlock_irqrestore(&hc18xx_dma->lock, flags);
		goto out;
	}

	desc = list_first_entry(&hc18xx_dmac->ld_active, struct hc18xx_dma_desc, node);

	spin_unlock_irqrestore(&hc18xx_dma->lock, flags);

	if (!xfer_done(desc)) {
		setup_xfer_desc_next(hc18xx_dma, desc);
		enable_dma_access(hc18xx_dma);
		return;
	}

out:
	/* Tasklet irq */
	tasklet_schedule(&hc18xx_dmac->dma_tasklet);
}

static irqreturn_t dma_irq_handler(int irq, void *dev_id)
{
	struct hc18xx_dma_engine *hc18xx_dma = dev_id;
	unsigned int status;

	status = hc18xx_dma_readl(hc18xx_dma, DMA02_IRQ_STATUS) & ~(hc18xx_dma_readl(hc18xx_dma, DMA03_IRQ_MASK));
	//pr_err("%s called, status=0x%08x\n", __func__, status);
	if ((status & STATUS_ACCESS_END)) {
		hc18xx_dma_writel(hc18xx_dma, STATUS_ACCESS_END, DMA01_IRQ_CLEAR);
		dma_irq_handle_channel(&hc18xx_dma->channel[0]);
	}
	if (status & STATUS_BW_INSUFFICIENT_R) {
		hc18xx_dma_writel(hc18xx_dma, STATUS_BW_INSUFFICIENT_R, DMA01_IRQ_CLEAR);
	}
	if (status & STATUS_BW_INSUFFICIENT_W) {
		hc18xx_dma_writel(hc18xx_dma, STATUS_BW_INSUFFICIENT_W, DMA01_IRQ_CLEAR);
	}
	if (status & STATUS_ACCESS_VIOLATION_R) {
		hc18xx_dma_writel(hc18xx_dma, STATUS_ACCESS_VIOLATION_R, DMA01_IRQ_CLEAR);
	}
	if (status & STATUS_ACCESS_VIOLATION_W) {
		hc18xx_dma_writel(hc18xx_dma, STATUS_ACCESS_VIOLATION_W, DMA01_IRQ_CLEAR);
	}

	return IRQ_HANDLED;
}

static u32 pa_to_viol_addr(struct hc18xx_dma_engine *pdma, u32 pa, enum dma_addr_type type)
{
	u32 carry;
	u32 row;
	u32 bank;
	u32 col;
	u32 row_bank;
	u32 col_mask;
	u32 col_width[4] = { 9, 10, 11, 12 };

	if ((type == END) && (pa & 0x7)) {
		carry = 1;
	} else {
		carry = 0;
	}

	col_mask = 0xffffffff >> (32 - col_width[pdma->col_addr_type]);
	col = (pa & col_mask) >> 3;
	row_bank = pa >> col_width[pdma->col_addr_type];

	if (pdma->bank_addr_type == 0) {
		bank = row_bank & 0x3;
		row = row_bank >> 2;
	} else {
		bank = row_bank & 0x7;
		row = row_bank >> 3;
	}

	return ((row << 12) + (bank << 9) + col + carry);
}

static int setup_xfer_desc(struct hc18xx_dma_engine *pdma, dma_addr_t src, dma_addr_t dest, size_t len)
{
	unsigned int val = 0;
	unsigned int src_offset = 0;
	unsigned int dest_offset = 0;
	dma_addr_t xfer_src;
	dma_addr_t xfer_dest;
	size_t cur_len = 0;
	size_t xfer_len = 0;
	int as_no = 0; //need to set total access count

	cur_len = len;
	xfer_src = src;
	xfer_dest = dest;

	clk_disable(pdma->dma_clk);

	do {
		xfer_len = (cur_len < MAX_XFER_SIZE_PER_CH) ? cur_len : MAX_XFER_SIZE_PER_CH;

		src_offset = xfer_src - ((xfer_src >> 3) << 3);
		dest_offset = xfer_dest - ((xfer_dest >> 3) << 3);

		//	pr_debug("%s as_no: %d xfer_dest=0x%08llx xfer_src=0x%08llx xfer_len=%zu\n", __func__, as_no,
		//	         (unsigned long long)xfer_dest, (unsigned long long)xfer_src, xfer_len);

		val = (hc18xx_dma_readl(pdma, DMA_CH_CTRL1(as_no)) & (~(0x07070000))) | (src_offset << 16) |
		      (dest_offset << 24);
		hc18xx_dma_writel(pdma, val, DMA_CH_CTRL1(as_no));
		hc18xx_dma_writel(pdma, (xfer_src >> 3), DMA_INI_ADDR_LINEAR_R(as_no));
		hc18xx_dma_writel(pdma, (xfer_dest >> 3), DMA_INI_ADDR_LINEAR_W(as_no));
		hc18xx_dma_writel(pdma, xfer_len, DMA_CH_CTRL0(as_no));

		xfer_src = xfer_src + xfer_len;
		xfer_dest = xfer_dest + xfer_len;
		cur_len = cur_len - xfer_len;
		as_no++;
	} while ((cur_len > 0) && (as_no < MAX_DMA_ACCESS_CNT));

	hc18xx_dma_writel(pdma, pa_to_viol_addr(pdma, (u32)src, (enum dma_addr_type)START),
	                  DMA09_STR_ADDR_R); // start address for read
	hc18xx_dma_writel(pdma, pa_to_viol_addr(pdma, (u32)(src + len), (enum dma_addr_type)END),
	                  DMA10_END_ADDR_R); // end address for read
	hc18xx_dma_writel(pdma, pa_to_viol_addr(pdma, (u32)dest, (enum dma_addr_type)START),
	                  DMA11_STR_ADDR_W); // start address for write
	hc18xx_dma_writel(pdma, pa_to_viol_addr(pdma, (u32)(dest + len), (enum dma_addr_type)END),
	                  DMA12_END_ADDR_W); // end address for write

	val = hc18xx_dma_readl(pdma, DMA04_CTRL) & (~TOTAL_ACCESS_CNT(15));
	hc18xx_dma_writel(pdma, val | TOTAL_ACCESS_CNT(as_no), DMA04_CTRL);

	// Zero the unused channel registers
	for (; as_no < MAX_DMA_ACCESS_CNT; ++as_no) {
		hc18xx_dma_writel(pdma, 0, DMA_CH_CTRL0(as_no));
		val = hc18xx_dma_readl(pdma, DMA_CH_CTRL1(as_no) & (~(0x07070000)));
		hc18xx_dma_writel(pdma, val, DMA_CH_CTRL1(as_no));
		hc18xx_dma_writel(pdma, 0, DMA_INI_ADDR_LINEAR_R(as_no));
		hc18xx_dma_writel(pdma, 0, DMA_INI_ADDR_LINEAR_W(as_no));
	}

	return 0;
}

static int setup_xfer_desc_octa(struct hc18xx_dma_engine *pdma, unsigned long **dma_src_pg_list,
                                unsigned long **dma_dest_pg_list, size_t len)
{
	unsigned int val = 0;
	unsigned int src_offset = 0;
	unsigned int dest_offset = 0;
	dma_addr_t xfer_src;
	dma_addr_t xfer_dest;
	int as_no = 0;

	clk_disable(pdma->dma_clk);

	for (as_no = 0; as_no < MAX_DMA_ACCESS_CNT; as_no++) {
		xfer_src = (dma_addr_t)dma_src_pg_list[as_no];
		xfer_dest = (dma_addr_t)dma_dest_pg_list[as_no];
		if ((xfer_src == 0) || (xfer_dest == 0)) {
			break;
		}

		src_offset = xfer_src - ((xfer_src >> 3) << 3);
		dest_offset = xfer_dest - ((xfer_dest >> 3) << 3);

		val = (hc18xx_dma_readl(pdma, DMA_CH_CTRL1(as_no)) & (~(0x03030000))) | (src_offset << 16) |
		      (dest_offset << 24);
		hc18xx_dma_writel(pdma, val, DMA_CH_CTRL1(as_no));
		hc18xx_dma_writel(pdma, (xfer_src >> 3), DMA_INI_ADDR_LINEAR_R(as_no));
		hc18xx_dma_writel(pdma, (xfer_dest >> 3), DMA_INI_ADDR_LINEAR_W(as_no));
		hc18xx_dma_writel(pdma, len, DMA_CH_CTRL0(as_no));
	}

	hc18xx_dma_writel(pdma, pa_to_viol_addr(pdma, (u32)dma_src_pg_list[0], (enum dma_addr_type)START),
	                  DMA09_STR_ADDR_R); // start address for read
	hc18xx_dma_writel(pdma, pa_to_viol_addr(pdma, (u32)dma_src_pg_list[as_no - 1] + len, (enum dma_addr_type)END),
	                  DMA10_END_ADDR_R); // end address for read
	hc18xx_dma_writel(pdma, pa_to_viol_addr(pdma, (u32)dma_dest_pg_list[0], (enum dma_addr_type)START),
	                  DMA11_STR_ADDR_W); // start address for write
	hc18xx_dma_writel(pdma, pa_to_viol_addr(pdma, (u32)dma_dest_pg_list[as_no - 1] + len, (enum dma_addr_type)END),
	                  DMA12_END_ADDR_W); // end address for write

	val = hc18xx_dma_readl(pdma, DMA04_CTRL) & (~TOTAL_ACCESS_CNT(15));
	hc18xx_dma_writel(pdma, val | TOTAL_ACCESS_CNT(as_no), DMA04_CTRL);

	return 0;
}

#ifdef US_EN
static int setup_xfer_desc_for_us(struct hc18xx_dma_engine *pdma, unsigned long **dma_src_pg_list,
                                  unsigned long **dma_dest_pg_list, size_t npages)
{
	unsigned int val = 0;
	size_t xfer_len = 4096;
	unsigned int src_offset = 0;
	unsigned int dest_offset = 0;
	dma_addr_t xfer_src;
	dma_addr_t xfer_dest;
	int as_no = 0;
	do {
		xfer_src = (dma_addr_t)dma_src_pg_list[as_no];
		xfer_dest = (dma_addr_t)dma_dest_pg_list[as_no];

		src_offset = xfer_src - ((xfer_src >> 3) << 3);
		dest_offset = xfer_dest - ((xfer_dest >> 3) << 3);

		val = (hc18xx_dma_readl(pdma, DMA_CH_CTRL1(as_no)) & (~(0x03030000))) | (src_offset << 16) |
		      (dest_offset << 24);
		hc18xx_dma_writel(pdma, val, DMA_CH_CTRL1(as_no));
		hc18xx_dma_writel(pdma, (xfer_src >> 3), DMA_INI_ADDR_LINEAR_R(as_no));
		hc18xx_dma_writel(pdma, (xfer_dest >> 3), DMA_INI_ADDR_LINEAR_W(as_no));
		hc18xx_dma_writel(pdma, xfer_len, DMA_CH_CTRL0(as_no));
		as_no++;
	} while ((as_no < npages));

	hc18xx_dma_writel(pdma, pa_to_viol_addr(pdma, (u32)dma_src_pg_list[0], (enum dma_addr_type)START),
	                  DMA09_STR_ADDR_R); // start address for read
	hc18xx_dma_writel(pdma, pa_to_viol_addr(pdma, (u32)dma_src_pg_list[as_no] + xfer_len, (enum dma_addr_type)END),
	                  DMA10_END_ADDR_R); // end address for read
	hc18xx_dma_writel(pdma, pa_to_viol_addr(pdma, (u32)dma_dest_pg_list[0], (enum dma_addr_type)START),
	                  DMA11_STR_ADDR_W); // start address for write
	hc18xx_dma_writel(pdma, pa_to_viol_addr(pdma, (u32)dma_dest_pg_list[as_no] + xfer_len, (enum dma_addr_type)END),
	                  DMA12_END_ADDR_W); // end address for write

	val = hc18xx_dma_readl(pdma, DMA04_CTRL) & (~TOTAL_ACCESS_CNT(15));
	hc18xx_dma_writel(pdma, val | TOTAL_ACCESS_CNT(as_no), DMA04_CTRL);

	return 0;
}
#endif

static bool xfer_done(struct hc18xx_dma_desc *d)
{
	return (d->len_off == d->len);
}

static int setup_xfer_desc_next(struct hc18xx_dma_engine *pdma, struct hc18xx_dma_desc *d)
{
#define MAX_XFER_SIZE (MAX_XFER_SIZE_PER_CH * MAX_DMA_ACCESS_CNT)

	size_t xfer_len = 0;
	size_t cur_len = 0;
	size_t start_src;
	size_t start_dest;
	size_t remainder_len;

	cur_len = d->len - d->len_off;

	if (xfer_done(d)) {
		return 0;
	}

	/*
	   * Apply #20386 SW workaround
	   * Originally, xfer_len = (cur_len < MAX_XFER_SIZE)? cur_len:MAX_XFER_SIZE;
	   * We isolate non-full data section out here
	   */
	xfer_len = cur_len;
	if (xfer_len > MAX_XFER_SIZE)
		xfer_len = MAX_XFER_SIZE;
	else {
		remainder_len = xfer_len & (MAX_XFER_SIZE_PER_CH - 1);
		if ((remainder_len != 0) && (xfer_len > (MAX_XFER_SIZE_PER_CH * 2))) {
			xfer_len -= remainder_len;
		}
	}

	start_src = d->src + d->len_off;
	start_dest = d->dest + d->len_off;

	setup_xfer_desc(pdma, start_src, start_dest, xfer_len);

	d->len_off += xfer_len;

	return 0;
}

static int enable_dma_access(struct hc18xx_dma_engine *pdma)
{
	hc18xx_sw_rst(SW_RST_DMA);
	clk_enable(pdma->dma_clk);
	augentix_dma_stub();
	hc18xx_dma_writel(pdma, 0x00000001, DMA00_ACCESS_START);
	return 0;
}

static int hc18xx_dma_xfer_desc(struct hc18xx_dma_desc *d)
{
	struct hc18xx_dma_channel *hc18xx_dmac = to_hc18xx_dma_chan(d->desc.chan);
	struct hc18xx_dma_engine *hc18xx_dma = hc18xx_dmac->dma;

	switch (d->type) {
	case HC18XX_DMA_DESC_MEMCPY:
		if (!(0xC0000000 & d->src)) {
			setup_xfer_desc_next(hc18xx_dma, d);
		} else {
#ifdef US_EN
			int npages = 1 + (d->len - 1) / PAGE_SIZE;
			setup_xfer_desc_for_us(hc18xx_dma, (unsigned long **)d->src, (unsigned long **)d->dest, npages);
#endif
			setup_xfer_desc_octa(hc18xx_dma, (unsigned long **)d->src, (unsigned long **)d->dest, d->len);
		}

		pr_debug("%s channel: %d dest=0x%08llx src=0x%08llx dma_length=%zu\n", __func__, hc18xx_dmac->channel,
		         (unsigned long long)d->dest, (unsigned long long)d->src, d->len);

		break;
	default:
		return -EINVAL;
	}

	enable_dma_access(hc18xx_dma);
	return 0;
}

static enum dma_status hc18xx_dma_tx_status(struct dma_chan *chan, dma_cookie_t cookie, struct dma_tx_state *txstate)
{
	return dma_cookie_status(chan, cookie, txstate);
}

static dma_cookie_t hc18xx_dma_tx_submit(struct dma_async_tx_descriptor *tx)
{
	struct hc18xx_dma_channel *hc18xx_dmac = to_hc18xx_dma_chan(tx->chan);
	struct hc18xx_dma_engine *hc18xx_dma = hc18xx_dmac->dma;
	dma_cookie_t cookie;
	unsigned long flags;
	spin_lock_irqsave(&hc18xx_dma->lock, flags);

	list_move_tail(hc18xx_dmac->ld_free.next, &hc18xx_dmac->ld_queue);
	cookie = dma_cookie_assign(tx);

	spin_unlock_irqrestore(&hc18xx_dma->lock, flags);

	return cookie;
}

static int hc18xx_dma_alloc_chan_resources(struct dma_chan *chan)
{
	struct hc18xx_dma_channel *hc18xx_dmac = to_hc18xx_dma_chan(chan);
	//struct hc18xx_dma_engine *hc18xx_dma = hc18xx_dmac->dma;
	//struct imx_dma_data *data = chan->private;

	//if (data != NULL)
	//	imxdmac->dma_request = data->dma_request;

	while (hc18xx_dmac->descs_allocated < HC18XX_DMA_MAX_CHAN_DESCRIPTORS) {
		struct hc18xx_dma_desc *desc;

		desc = kzalloc(sizeof(*desc), GFP_KERNEL);
		if (!desc) {
			pr_err("%s: failure to allocate hc18xx_dma_desc.\n", __func__);
			break;
		}

		__memzero(&desc->desc, sizeof(struct dma_async_tx_descriptor));

		dma_async_tx_descriptor_init(&desc->desc, chan);
		desc->desc.tx_submit = hc18xx_dma_tx_submit;
		/* txd.flags will be overwritten in prep funcs */
		desc->desc.flags = DMA_CTRL_ACK;
		desc->status = DMA_COMPLETE;

		list_add_tail(&desc->node, &hc18xx_dmac->ld_free);
		hc18xx_dmac->descs_allocated++;
	}

	if (!hc18xx_dmac->descs_allocated) {
		pr_err("%s: No any descs.\n", __func__);
		return -ENOMEM;
	}

	return hc18xx_dmac->descs_allocated;
}

static void hc18xx_dma_free_chan_resources(struct dma_chan *chan)
{
	struct hc18xx_dma_channel *hc18xx_dmac = to_hc18xx_dma_chan(chan);
	struct hc18xx_dma_engine *hc18xx_dma = hc18xx_dmac->dma;
	struct hc18xx_dma_desc *desc, *_desc;
	unsigned long flags;

	spin_lock_irqsave(&hc18xx_dma->lock, flags);

	//imxdma_disable_hw(imxdmac);
	list_splice_tail_init(&hc18xx_dmac->ld_active, &hc18xx_dmac->ld_free);
	list_splice_tail_init(&hc18xx_dmac->ld_queue, &hc18xx_dmac->ld_free);

	spin_unlock_irqrestore(&hc18xx_dma->lock, flags);

	list_for_each_entry_safe (desc, _desc, &hc18xx_dmac->ld_free, node) {
		kfree(desc);
		hc18xx_dmac->descs_allocated--;
	}
	INIT_LIST_HEAD(&hc18xx_dmac->ld_free);
}

static struct dma_async_tx_descriptor *hc18xx_dma_prep_dma_memcpy(struct dma_chan *chan, dma_addr_t dest,
                                                                  dma_addr_t src, size_t len, unsigned long flags)
{
	struct hc18xx_dma_channel *hc18xx_dmac = to_hc18xx_dma_chan(chan);
	struct hc18xx_dma_desc *desc;

	if (list_empty(&hc18xx_dmac->ld_free))
		return NULL;

	desc = list_first_entry(&hc18xx_dmac->ld_free, struct hc18xx_dma_desc, node);

	desc->type = HC18XX_DMA_DESC_MEMCPY;
	desc->src = src;
	desc->dest = dest;
	desc->len = len;
	desc->len_off = 0;
	desc->direction = DMA_MEM_TO_MEM;
	desc->desc.callback = NULL;
	desc->desc.callback_param = NULL;

	return &desc->desc;
}

static int hc18xx_dma_control(struct dma_chan *chan, enum dma_ctrl_cmd cmd, unsigned long arg)
{
	struct hc18xx_dma_channel *hc18xx_dmac = to_hc18xx_dma_chan(chan);
	struct hc18xx_dma_engine *hc18xx_dma = hc18xx_dmac->dma;
	struct dma_args *dma_args = (struct dma_args *)arg;
	unsigned long flags;

	switch (cmd) {
	case DMA_SLAVE_CONFIG:
		dma_slave_config(hc18xx_dma, dma_args);
		return 0;
	case DMA_TERMINATE_ALL:
		spin_lock_irqsave(&hc18xx_dma->lock, flags);

		list_splice_tail_init(&hc18xx_dmac->ld_active, &hc18xx_dmac->ld_free);
		list_splice_tail_init(&hc18xx_dmac->ld_queue, &hc18xx_dmac->ld_free);

		spin_unlock_irqrestore(&hc18xx_dma->lock, flags);
		return 0;
	default:
		return -ENOSYS;
	}
	return -EINVAL;
}

static void hc18xx_dma_issue_pending(struct dma_chan *chan)
{
	struct hc18xx_dma_channel *hc18xx_dmac = to_hc18xx_dma_chan(chan);
	struct hc18xx_dma_engine *hc18xx_dma = hc18xx_dmac->dma;
	struct hc18xx_dma_desc *desc;
	unsigned long flags;

	spin_lock_irqsave(&hc18xx_dma->lock, flags);

	if (list_empty(&hc18xx_dmac->ld_active) && !list_empty(&hc18xx_dmac->ld_queue)) {
		desc = list_first_entry(&hc18xx_dmac->ld_queue, struct hc18xx_dma_desc, node);
		//pr_info("desc: len=%d",desc->len);
		if (hc18xx_dma_xfer_desc(desc) < 0) {
			pr_warn("%s: channel: %d couldn't issue DMA xfer\n", __func__, hc18xx_dmac->channel);
		} else {
			list_move_tail(hc18xx_dmac->ld_queue.next, &hc18xx_dmac->ld_active);
		}
	}

	spin_unlock_irqrestore(&hc18xx_dma->lock, flags);
}

static void hc18xx_dma_tasklet(unsigned long data)
{
	struct hc18xx_dma_channel *hc18xx_dmac = (void *)data;
	struct hc18xx_dma_engine *hc18xx_dma = hc18xx_dmac->dma;
	struct hc18xx_dma_desc *desc;
	unsigned long flags;

	spin_lock_irqsave(&hc18xx_dma->lock, flags);

	if (list_empty(&hc18xx_dmac->ld_active)) {
		/* Someone might have called terminate all */
		spin_unlock_irqrestore(&hc18xx_dma->lock, flags);
		return;
	}

	desc = list_first_entry(&hc18xx_dmac->ld_active, struct hc18xx_dma_desc, node);

	dma_cookie_complete(&desc->desc);

	list_move_tail(hc18xx_dmac->ld_active.next, &hc18xx_dmac->ld_free);

	if (!list_empty(&hc18xx_dmac->ld_queue)) {
		desc = list_first_entry(&hc18xx_dmac->ld_queue, struct hc18xx_dma_desc, node);
		list_move_tail(hc18xx_dmac->ld_queue.next, &hc18xx_dmac->ld_active);
		if (hc18xx_dma_xfer_desc(desc) < 0)
			pr_warn("%s: channel: %d couldn't xfer desc\n", __func__, hc18xx_dmac->channel);
	}

	spin_unlock_irqrestore(&hc18xx_dma->lock, flags);

	if (desc->desc.callback)
		desc->desc.callback(desc->desc.callback_param);
#ifdef HC18XX_CRYPTO_ALG_TYPE_ABLKCIPHER
	hc18xx_aes_complete(hc18xx_dma, 0);
#endif
}

struct dma_chan *__augentix_get_dma_chan(void)
{
	return memcpy_chan;
}
EXPORT_SYMBOL(__augentix_get_dma_chan);

struct dma_chan *__augentix_dma_request_channel(unsigned int flags)
{
	dma_cap_mask_t mask;

	dma_cap_zero(mask);
	dma_cap_set(flags, mask);

	return dma_request_channel(mask, NULL, NULL);
}
EXPORT_SYMBOL(__augentix_dma_request_channel);

void __augentix_dma_release_channel(struct dma_chan *chan)
{
	dma_release_channel(chan);
}
EXPORT_SYMBOL(__augentix_dma_release_channel);

static struct dma_async_tx_descriptor *__hc18xx_device_prep_dma_memcpy(struct dma_chan *chan, dma_addr_t dest,
                                                                       dma_addr_t src, size_t len, unsigned int flags)
{
	return chan->device->device_prep_dma_memcpy(chan, dest, src, len, flags);
}

static void __hc18xx_dma_start_transfer(struct dma_chan *chan, struct completion *cmp, dma_cookie_t cookie, int wait)
{
	enum dma_status status;
	u32 timeout = 0;

	if (wait < 3000) {
		timeout = msecs_to_jiffies(3000);
	} else {
		timeout = msecs_to_jiffies(wait);
	}

	init_completion(cmp);
	dma_async_issue_pending(chan);

	if (wait) {
		pr_debug("waiting for dma to complete...\n");

		timeout = wait_for_completion_timeout(cmp, timeout);
		status = dma_async_is_tx_complete(chan, cookie, NULL, NULL);

		if (timeout == 0) {
			pr_err("dma xfer timedout\n");
		} else if (status != DMA_COMPLETE) {
			pr_err("dma returned completion callback status of: %s\n",
			       status == DMA_ERROR ? "error" : "in progress");
		}
	}
}

unsigned int __augentix_dma_config(struct dma_chan *chan, u32 op_mode, u32 *key, u32 *iv)
{
	struct dma_args dma_args;

	dma_args.oper_mode = op_mode;
	if ((op_mode == DMA_AES_CBC_BE) || (op_mode == DMA_AES_CBC_LE)) {
#ifdef HC18XX_CRYPTO_ALG_TYPE_ABLKCIPHER
		dma_args.aes_init_vet = iv;
		dma_args.aes_key = key;
#else
		memcpy(dma_args.aes_init_vet, iv, LEN_AES_INIT_VT);
		memcpy(dma_args.aes_key, key, LEN_AES_KEY);
#endif
	} else if ((op_mode == DMA_AES_ECB_BE) || (op_mode == DMA_AES_ECB_LE)) {
#ifdef HC18XX_CRYPTO_ALG_TYPE_ABLKCIPHER
		dma_args.aes_key = key;
#else
		memcpy(dma_args.aes_key, key, LEN_AES_KEY);
#endif
	}

	dmaengine_device_control(chan, DMA_SLAVE_CONFIG, (unsigned long)&dma_args);

	return 0;
}
EXPORT_SYMBOL(__augentix_dma_config);

void __hc18xx_dma_sync_callback(void *completion)
{
	complete(completion);
}

unsigned int __augentix_dma_copy(struct dma_chan *chan, void *dest, void *src, size_t len, int timeout)
{
	u32 ret = 0;
	struct dma_async_tx_descriptor *memcpy_chan_desc;
	dma_cookie_t memcpy_cookie;
	struct completion memcpy_cmp;

	memcpy_chan_desc = __hc18xx_device_prep_dma_memcpy(chan, (dma_addr_t)dest, (dma_addr_t)src, len,
	                                                   DMA_CTRL_ACK | DMA_PREP_INTERRUPT);
	if (!memcpy_chan_desc) {
		//pr_err("failure to prep_dma_memcpy\n");
		memcpy_cookie = -EBUSY;
		ret = -1;
		goto end;
	} else {
		memcpy_chan_desc->callback = __hc18xx_dma_sync_callback;
		memcpy_chan_desc->callback_param = &memcpy_cmp;

		memcpy_cookie = dmaengine_submit(memcpy_chan_desc);
	}
	if (dma_submit_error(memcpy_cookie)) {
		pr_err("memcpy_cookie error\n");
	}
	__hc18xx_dma_start_transfer(chan, &memcpy_cmp, memcpy_cookie, timeout);

end:
	return ret;
}
EXPORT_SYMBOL(__augentix_dma_copy);

#ifdef HC18XX_CRYPTO_ALG_TYPE_ABLKCIPHER

u32 do_aes_ablkcipher(struct hc18xx_aes_ctx *op)
{
	__augentix_dma_config(memcpy_chan, op->mode, op->aes_key, op->aes_iv);

	__augentix_dma_copy(memcpy_chan, (void *)op->dst, (void *)op->src, op->len, 1);

	return op->len;
}

static int hc18xx_set_in(struct hc18xx_dma_engine *dev, struct scatterlist *sg)
{
	int err;
#if 1
	if (!IS_ALIGNED(sg_dma_len(sg), AES_BLOCK_SIZE)) {
		err = -EINVAL;
		goto exit;
	}
	if (!sg_dma_len(sg)) {
		err = -EINVAL;
		goto exit;
	}
#endif
	err = dma_map_sg(dev->dev, sg, 1, DMA_TO_DEVICE);
	if (!err) {
		err = -ENOMEM;
		goto exit;
	}

	dev->sg_src = sg;
	err = 0;

exit:
	return err;
}

static int hc18xx_set_out(struct hc18xx_dma_engine *dev, struct scatterlist *sg)
{
	int err;
#if 1
	if (!IS_ALIGNED(sg_dma_len(sg), AES_BLOCK_SIZE)) {
		err = -EINVAL;
		goto exit;
	}
	if (!sg_dma_len(sg)) {
		err = -EINVAL;
		goto exit;
	}
#endif
	err = dma_map_sg(dev->dev, sg, 1, DMA_FROM_DEVICE);
	if (!err) {
		err = -ENOMEM;
		goto exit;
	}

	dev->sg_dst = sg;
	err = 0;

exit:
	return err;
}

static void hc18xx_unset_out(struct hc18xx_dma_engine *dev)
{
	dma_unmap_sg(dev->dev, dev->sg_dst, 1, DMA_FROM_DEVICE);
}

static void hc18xx_unset_in(struct hc18xx_dma_engine *dev)
{
	dma_unmap_sg(dev->dev, dev->sg_src, 1, DMA_TO_DEVICE);
}

static void hc18xx_aes_complete(struct hc18xx_dma_engine *dev, int err)
{
	/* holding a lock outside */
	if (dev->is_ablk_mode) {
		hc18xx_unset_in(dev);
		hc18xx_unset_out(dev);

		dev->req->base.complete(&dev->req->base, err);
		dev->is_ablk_mode = 0;
	}
	//dev->busy = false;
}
#if 0
static void hc18xx_aes_tasklet_cb(unsigned long data)
{
	struct hc18xx_dma_engine *dev = (struct hc18xx_dma_engine *)data;
	struct crypto_async_request *async_req, *backlog;
	struct hc18xx_aes_ctx* ctx;
	struct hc18xx_aes_reqctx *reqctx;
	unsigned long flags;
	u8* p_dst;
	int i;
	spin_lock_irqsave(&dev->lock, flags);
	backlog   = crypto_get_backlog(&dev->queue);
	async_req = crypto_dequeue_request(&dev->queue);

	if (!async_req) {
		dev->busy = false;
		spin_unlock_irqrestore(&dev->lock, flags);
		return;
	}
	spin_unlock_irqrestore(&dev->lock, flags);

	if (backlog)
		backlog->complete(backlog, -EINPROGRESS);

	dev->req = ablkcipher_request_cast(async_req);
	ctx = crypto_tfm_ctx(dev->req->base.tfm);
	reqctx   = ablkcipher_request_ctx(dev->req);

#if 1
	//spin_lock_irqsave(&dev->lock, flags);

	hc18xx_set_in(dev,dev->req->src);
	hc18xx_set_out(dev,dev->req->dst);

	ctx->src = sg_dma_address(dev->sg_src);
	ctx->dst = sg_dma_address(dev->sg_dst);
	ctx->len = sg_dma_len(dev->sg_src);
	ctx->mode = reqctx->mode;

    p_dst = phys_to_virt((unsigned long) ctx->src);

	for (i=0; i<8; i++) {
		pr_info("%x %x %x %x %x %x %x %x\n",p_dst[0],p_dst[1],p_dst[2],p_dst[3],p_dst[4],p_dst[5],p_dst[6],p_dst[7]);
		p_dst	= p_dst + (8);
	}
	ctx->aes_iv = dev->req->info;
	do_aes_ablkcipher(ctx);

	//spin_unlock_irqrestore(&dev->lock, flags);


	//hc18xx_unset_in(dev);
	//hc18xx_unset_out(dev);
#endif
	//hc18xx_aes_complete(dev, 0);
}

static int hc18xx_aes_handle_req(struct hc18xx_dma_engine *dev,
			      struct ablkcipher_request *req)
{
	unsigned long flags;
	int err;

	spin_lock_irqsave(&dev->lock, flags);
	err = ablkcipher_enqueue_request(&dev->queue, req);
	if (dev->busy) {
		spin_unlock_irqrestore(&dev->lock, flags);
		goto exit;
	}
	dev->busy = true;
	spin_unlock_irqrestore(&dev->lock, flags);
#if 1
 	//wake_up_process(dev->queue_th);
 	hc18xx_aes_tasklet_cb((unsigned long)dev);
	//hc18xx_aes_complete(dev, 0);
#endif
 exit:
	return err;
}
#endif

static int hc18xx_handle_req(struct hc18xx_aes_ctx *ctx, u32 mode)
{
	struct hc18xx_dma_engine *dev = ctx->dev;

	hc18xx_set_in(dev, dev->req->src);
	hc18xx_set_out(dev, dev->req->dst);

	ctx->src = (void *)sg_dma_address(dev->sg_src);
	ctx->dst = (void *)sg_dma_address(dev->sg_dst);
	ctx->len = sg_dma_len(dev->sg_src);
	ctx->mode = mode;

	ctx->aes_iv = dev->req->info;
	do_aes_ablkcipher(ctx);

	return 0;
}

static int hc18xx_aes_cbc_encrypt(struct ablkcipher_request *req)
{
	struct crypto_ablkcipher *tfm = crypto_ablkcipher_reqtfm(req);
	struct hc18xx_aes_ctx *ctx = crypto_ablkcipher_ctx(tfm);
	//struct hc18xx_aes_reqctx	*reqctx = ablkcipher_request_ctx(req);
	struct hc18xx_dma_engine *dev = ctx->dev;

//pr_info("crypto_ablkcipher: %s\n",__func__);
#if 0
	if (!IS_ALIGNED(req->nbytes, AES_BLOCK_SIZE)) {
		pr_err("request size is not exact amount of AES blocks\n");
		return -EINVAL;
	}
	reqctx->mode = DMA_AES_CBC_BE;
#endif

	dev->req = req;
	dev->is_ablk_mode = 1;

	return hc18xx_handle_req(ctx, DMA_AES_CBC_BE); //hc18xx_aes_handle_req(dev, req);
}

static int hc18xx_aes_cbc_dencrypt(struct ablkcipher_request *req)
{
	pr_info("crypto_ablkcipher: %s [Not Support!!]\n", __func__);

	return 0;
}

static int hc18xx_aes_ecb_encrypt(struct ablkcipher_request *req)
{
	struct crypto_ablkcipher *tfm = crypto_ablkcipher_reqtfm(req);
	struct hc18xx_aes_ctx *ctx = crypto_ablkcipher_ctx(tfm);
	//struct hc18xx_aes_reqctx	*reqctx = ablkcipher_request_ctx(req);
	struct hc18xx_dma_engine *dev = ctx->dev;

//pr_info("crypto_ablkcipher: %s\n",__func__);
#if 0
	if (!IS_ALIGNED(req->nbytes, AES_BLOCK_SIZE)) {
		pr_err("request size is not exact amount of AES blocks\n");
		return -EINVAL;
	}
	reqctx->mode = DMA_AES_ECB_BE;
#endif
	dev->req = req;
	dev->is_ablk_mode = 1;

	return hc18xx_handle_req(ctx, DMA_AES_ECB_BE); //hc18xx_aes_handle_req(dev, req);
}

static int hc18xx_aes_ecb_dencrypt(struct ablkcipher_request *req)
{
	pr_info("crypto_ablkcipher: %s\n", __func__);

	return 0;
}

#else

static int __aes_zero_padding(u8 *buf, u32 xfer_len, u32 encrypt_len)
{
	int pad_num = 0;
	if (encrypt_len > xfer_len) {
		pad_num = (encrypt_len - xfer_len);
		memset(buf + xfer_len, 0, pad_num);
	}

	return pad_num;
}

u32 do_aes_blkcipher(struct hc18xx_aes_ctx *op)
{
	//static struct dma_chan* memcpy_chan;
	u32 encrypt_len = (((op->len + 15) >> 4) << 4);
	dma_addr_t dma_dst, dma_src;
#if 0
	memcpy_chan = __augentix_dma_request_channel(DMA_MEMCPY);
	if (!memcpy_chan) {
		pr_err("failure to request dma channel\n");
		return -1;
	}
#endif
	//__aes_zero_padding(op->src,op->len,encrypt_len);

	__augentix_dma_config(memcpy_chan, op->mode, op->aes_key, op->aes_iv);

	dma_src = dma_map_single(memcpy_chan->device->dev, op->src, encrypt_len, DMA_TO_DEVICE);
	dma_dst = dma_map_single(memcpy_chan->device->dev, op->dst, encrypt_len, DMA_FROM_DEVICE);

	pr_info("dma_src=%x, dma_dst=%x\n", dma_src, dma_dst);
	__augentix_dma_copy(memcpy_chan, (void *)dma_dst, (void *)dma_src, encrypt_len, 1);

	dma_unmap_single(memcpy_chan->device->dev, dma_src, encrypt_len, DMA_TO_DEVICE);
	dma_unmap_single(memcpy_chan->device->dev, dma_dst, encrypt_len, DMA_FROM_DEVICE);

	//	__augentix_dma_release_channel(memcpy_chan);

	return op->len;
}

static int hc18xx_aes_cbc_encrypt(struct blkcipher_desc *desc, struct scatterlist *dst, struct scatterlist *src,
                                  unsigned int nbytes)
{
	struct hc18xx_aes_ctx *op = crypto_blkcipher_ctx(desc->tfm);
	struct blkcipher_walk walk;
	int err;
	//u8* p_dest;

	//pr_info("%s\n",__func__);

	blkcipher_walk_init(&walk, dst, src, nbytes);
	err = blkcipher_walk_virt(desc, &walk);
	op->aes_iv = walk.iv;

	while ((nbytes = walk.nbytes)) {
		op->src = walk.src.virt.addr, op->dst = walk.dst.virt.addr;
		op->mode = DMA_AES_CBC_BE;
		op->len = nbytes;
		do_aes_blkcipher(op);
		nbytes = 0;
		err = blkcipher_walk_done(desc, &walk, nbytes);
	}

	pr_info("%s\n", __func__);

#if 0
	p_dest = op->src;

	for (i=0; i<8; i++) {
			pr_info("%x %x %x %x %x %x %x %x\n",p_dest[0],p_dest[1],p_dest[2],p_dest[3],p_dest[4],p_dest[5],p_dest[6],p_dest[7]);
			p_dest	= p_dest + (8);
	}


	pr_info("========================\n");
	p_dest = op->dst;

	for (i=0; i<8; i++) {
			pr_info("%x %x %x %x %x %x %x %x\n",p_dest[0],p_dest[1],p_dest[2],p_dest[3],p_dest[4],p_dest[5],p_dest[6],p_dest[7]);
			p_dest	= p_dest + (8);
	}
#endif
	return err;
}

static int hc18xx_aes_cbc_decrypt(struct blkcipher_desc *desc, struct scatterlist *dst, struct scatterlist *src,
                                  unsigned int nbytes)
{
	pr_err("Not Support aes-cbc decrypt\n");
	return 0;
}

static int hc18xx_aes_ecb_encrypt(struct blkcipher_desc *desc, struct scatterlist *dst, struct scatterlist *src,
                                  unsigned int nbytes)
{
	struct hc18xx_aes_ctx *op = crypto_blkcipher_ctx(desc->tfm);
	struct blkcipher_walk walk;
	int err;
	//u8* p_dest;

	pr_info("%s, nbytes=%u\n", __func__, nbytes);

	blkcipher_walk_init(&walk, dst, src, nbytes);
	err = blkcipher_walk_virt(desc, &walk);

	while ((nbytes = walk.nbytes)) {
		op->src = walk.src.virt.addr, op->dst = walk.dst.virt.addr;
		op->mode = DMA_AES_ECB_BE;
		op->len = nbytes;
		do_aes_blkcipher(op);
		nbytes = 0;
		err = blkcipher_walk_done(desc, &walk, nbytes);
	}

#if 0
	p_dest = op->src;

	for (i=0; i<8; i++) {
			pr_info("%x %x %x %x %x %x %x %x\n",p_dest[0],p_dest[1],p_dest[2],p_dest[3],p_dest[4],p_dest[5],p_dest[6],p_dest[7]);
			p_dest	= p_dest + (8);
	}


	pr_info("========================\n");
	p_dest = op->dst;

	for (i=0; i<8; i++) {
			pr_info("%x %x %x %x %x %x %x %x\n",p_dest[0],p_dest[1],p_dest[2],p_dest[3],p_dest[4],p_dest[5],p_dest[6],p_dest[7]);
			p_dest	= p_dest + (8);
	}
#endif
	return err;
}

static int hc18xx_aes_ecb_decrypt(struct blkcipher_desc *desc, struct scatterlist *dst, struct scatterlist *src,
                                  unsigned int nbytes)
{
	pr_err("Not Support aes-ecb decrypt\n");
	return 0;
}

#endif
static int hc18xx_aes_cra_init(struct crypto_tfm *tfm)
{
	struct hc18xx_aes_ctx *ctx = crypto_tfm_ctx(tfm);
	//pr_info("crypto_ablkcipher: %s\n",__func__);
	ctx->dev = g_dma_engine;
#ifdef HC18XX_CRYPTO_ALG_TYPE_ABLKCIPHER
	tfm->crt_ablkcipher.reqsize = sizeof(struct hc18xx_aes_ctx);
#endif

	return 0;
}
#ifndef HC18XX_CRYPTO_ALG_TYPE_ABLKCIPHER
static void hc18xx_aes_cra_exit(struct crypto_tfm *tfm)
{
	//struct hc18xx_aes_ctx  *ctx = crypto_tfm_ctx(tfm);
	pr_info("%s\n", __func__);
}
#endif

#ifdef HC18XX_CRYPTO_ALG_TYPE_ABLKCIPHER
static int hc18xx_aes_setkey(struct crypto_ablkcipher *tfm, const u8 *key, unsigned int keylen)
{
	struct hc18xx_aes_ctx *ctx = crypto_ablkcipher_ctx(tfm);

	if (keylen != AES_KEYSIZE_256)
		return -EINVAL;

	pr_debug("enter, keylen: %d\n", keylen);

	memcpy(ctx->aes_key, key, keylen);
	ctx->keylen = keylen;

	return 0;
}
#if 0
static int hc18xx_aes_setkey(struct crypto_ablkcipher *cipher, const u8 *key,
		unsigned int len)
{
	struct crypto_tfm *tfm = crypto_ablkcipher_tfm(cipher);
	struct hc18xx_aes_ctx *ctx = crypto_tfm_ctx(tfm);
	pr_info("crypto_ablkcipher: %s\n",__func__);
	u8* p_dst;
	int i;
	switch (len) {
	case AES_KEYSIZE_256:
		break;
	default:
		crypto_ablkcipher_set_flags(cipher, CRYPTO_TFM_RES_BAD_KEY_LEN);
		return -EINVAL;
	}

	ctx->keylen = len;
	memcpy(ctx->aes_key, key, len);
#if 0
	p_dst = ctx->aes_key;
	for (i=0; i<4; i++) {
		pr_info("%x %x %x %x %x %x %x %x\n",p_dst[0],p_dst[1],p_dst[2],p_dst[3],p_dst[4],p_dst[5],p_dst[6],p_dst[7]);
		p_dst	= p_dst + (8);
	}
#endif
	return 0;
}
#endif
static struct crypto_alg cbc_alg = {
	.cra_name = "cbc(aes)",
	.cra_driver_name = "augentix-cbc",
	.cra_priority = 300,
	.cra_flags = CRYPTO_ALG_TYPE_ABLKCIPHER | CRYPTO_ALG_KERN_DRIVER_ONLY | CRYPTO_ALG_ASYNC,
	.cra_blocksize = AES_BLOCK_SIZE,
	.cra_ctxsize = sizeof(struct hc18xx_aes_ctx),
	.cra_alignmask = 0,
	.cra_type = &crypto_ablkcipher_type,
	.cra_module = THIS_MODULE,
	.cra_init = hc18xx_aes_cra_init,
	.cra_u =
	        {
	                .ablkcipher =
	                        {
	                                .ivsize = AES_BLOCK_SIZE,
	                                .min_keysize = AES_MIN_KEY_SIZE,
	                                .max_keysize = AES_MAX_KEY_SIZE,
	                                .setkey = hc18xx_aes_setkey,
	                                .encrypt = hc18xx_aes_cbc_encrypt,
	                                .decrypt = hc18xx_aes_cbc_dencrypt,
	                        },
	        },
};

static struct crypto_alg ecb_alg = {
	.cra_name = "ecb(aes)",
	.cra_driver_name = "augentix-ecb",
	.cra_priority = 300,
	.cra_flags = CRYPTO_ALG_TYPE_ABLKCIPHER | CRYPTO_ALG_KERN_DRIVER_ONLY | CRYPTO_ALG_ASYNC,
	.cra_blocksize = AES_BLOCK_SIZE,
	.cra_ctxsize = sizeof(struct hc18xx_aes_ctx),
	.cra_alignmask = 0,
	.cra_type = &crypto_ablkcipher_type,
	.cra_module = THIS_MODULE,
	.cra_init = hc18xx_aes_cra_init,
	.cra_u =
	        {
	                .ablkcipher =
	                        {
	                                //.ivsize		=	AES_BLOCK_SIZE,
	                                .min_keysize = AES_MIN_KEY_SIZE,
	                                .max_keysize = AES_MAX_KEY_SIZE,
	                                .setkey = hc18xx_aes_setkey,
	                                .encrypt = hc18xx_aes_ecb_encrypt,
	                                .decrypt = hc18xx_aes_ecb_dencrypt,
	                        },
	        },
};
#if 0
static int hc18xx_aes_rqueue_thread(void *data)
{
	struct hc18xx_dma_engine *dev = (struct hc18xx_dma_engine *)data;
	struct crypto_async_request *async_req, *backlog;
	struct hc18xx_aes_ctx* ctx;
	struct hc18xx_aes_reqctx *reqctx;
	unsigned long flags;

	do {
		__set_current_state(TASK_INTERRUPTIBLE);

		spin_lock_irqsave(&dev->lock, flags);
		backlog   = crypto_get_backlog(&dev->queue);
		async_req = crypto_dequeue_request(&dev->queue);
		if (!async_req) {
			dev->busy = false;
			spin_unlock_irqrestore(&dev->lock, flags);
			goto next;
		}
		spin_unlock_irqrestore(&dev->lock, flags);

		if (backlog) {
			backlog->complete(backlog, -EINPROGRESS);
		}

		dev->req = ablkcipher_request_cast(async_req);
		ctx = crypto_tfm_ctx(dev->req->base.tfm);
		reqctx = ablkcipher_request_ctx(dev->req);

		hc18xx_set_in(dev,dev->req->src);
		hc18xx_set_out(dev,dev->req->dst);

		ctx->src = sg_dma_address(dev->req->src);
		ctx->dst = sg_dma_address(dev->req->dst);
		ctx->len = sg_dma_len(dev->req->src);
		ctx->mode = reqctx->mode;
		ctx->aes_iv = dev->req->info;
		//pr_info("ctx->len=%d\n",ctx->len);
		do_aes_ablkcipher(ctx);

		hc18xx_unset_in(dev);
		hc18xx_unset_out(dev);

next:
		schedule();
	} while (!kthread_should_stop());
	return 0;
}
#endif
#else
static int hc18xx_aes_setkey(struct crypto_tfm *tfm, const u8 *key, u32 len)
{
	struct hc18xx_aes_ctx *op = crypto_tfm_ctx(tfm);
	u32 ret = 0, i;
	u8 *p_dst;
	//pr_info("%s\n",__func__);

	if (len == AES_KEYSIZE_256) {
		op->keylen = len;
		memcpy(op->aes_key, key, len);
#if 0
		p_dst = op->aes_key;
		for (i=0; i<4; i++) {
			pr_info("%x %x %x %x %x %x %x %x\n",p_dst[0],p_dst[1],p_dst[2],p_dst[3],p_dst[4],p_dst[5],p_dst[6],p_dst[7]);
			p_dst	= p_dst + (8);
		}
#endif
	} else {
		//pr_err("%s Only support AES-256\n",__func__);
	}

	return ret;
}

static struct crypto_alg cbc_alg = { .cra_name = "cbc(aes)",
	                             .cra_driver_name = "augentix-cbc",
	                             .cra_priority = 300,
	                             .cra_flags = CRYPTO_ALG_TYPE_BLKCIPHER |
	                                          CRYPTO_ALG_KERN_DRIVER_ONLY, // | CRYPTO_ALG_NEED_FALLBACK,
	                             .cra_init = hc18xx_aes_cra_init,
	                             .cra_exit = hc18xx_aes_cra_exit,
	                             .cra_blocksize = AES_BLOCK_SIZE,
	                             .cra_ctxsize = sizeof(struct hc18xx_aes_ctx),
	                             .cra_alignmask = 15,
	                             .cra_type = &crypto_blkcipher_type,
	                             .cra_module = THIS_MODULE,
	                             .cra_u = { .blkcipher = {
	                                                .min_keysize = AES_MIN_KEY_SIZE,
	                                                .max_keysize = AES_MAX_KEY_SIZE,
	                                                .setkey = hc18xx_aes_setkey,
	                                                .encrypt = hc18xx_aes_cbc_encrypt,
	                                                .decrypt = hc18xx_aes_cbc_decrypt,
	                                                .ivsize = AES_BLOCK_SIZE,
	                                        } } };

static struct crypto_alg ecb_alg = { .cra_name = "ecb(aes)",
	                             .cra_driver_name = "augentix-ecb",
	                             .cra_priority = 300,
	                             .cra_flags = CRYPTO_ALG_TYPE_BLKCIPHER |
	                                          CRYPTO_ALG_KERN_DRIVER_ONLY, // | CRYPTO_ALG_NEED_FALLBACK,
	                             .cra_init = hc18xx_aes_cra_init,
	                             .cra_exit = hc18xx_aes_cra_exit,
	                             .cra_blocksize = AES_BLOCK_SIZE,
	                             .cra_ctxsize = sizeof(struct hc18xx_aes_ctx),
	                             .cra_alignmask = 15,
	                             .cra_type = &crypto_blkcipher_type,
	                             .cra_module = THIS_MODULE,
	                             .cra_u = { .blkcipher = {
	                                                .min_keysize = AES_MIN_KEY_SIZE,
	                                                .max_keysize = AES_MAX_KEY_SIZE,
	                                                .setkey = hc18xx_aes_setkey,
	                                                .encrypt = hc18xx_aes_ecb_encrypt,
	                                                .decrypt = hc18xx_aes_ecb_decrypt,
	                                                //.ivsize			=	AES_BLOCK_SIZE,
	                                        } } };
#endif

static int __init hc18xx_dma_probe(struct platform_device *pdev)
{
	struct hc18xx_dma_engine *hc18xx_dma;
	struct resource *res;
	int ret, i;
	int err = -ENODEV;
	int irq;

#ifdef CONFIG_PLAT_HC18XX
	/* peek NRS code */
	phys_addr_t nrs_paddr = 0x80040400;
	size_t nrs_size = 0x40;
	void __iomem *nrs_base = devm_ioremap(&pdev->dev, nrs_paddr, nrs_size);
	uint32_t nrs_code;
	uint32_t package_code;

	if (nrs_base == NULL) {
		BUG();
	} else {
		writel(0x19811030, nrs_base + 0x0C);
		writel(0x1, nrs_base + 0x20);
		writeb(0x1, nrs_base + 0x12);

		msleep(10);
		ret = readl(nrs_base + 0x24);
		writel(0x20190229, nrs_base + 0x0C);

		if (ret != 0x0101) {
			BUG();
		}
	}

	nrs_code = readl(nrs_base + 0x0);
	package_code = (nrs_code >> 20) & 0xF;
	if (package_code == 0x8 || package_code == 0x9) {
		g_is_qfn = 1;
	} else {
		g_is_qfn = 0;
	}
#endif

	/* alloc struct hc18xx_dma_engine */
	hc18xx_dma = devm_kzalloc(&pdev->dev, sizeof(*hc18xx_dma), GFP_KERNEL);
	if (!hc18xx_dma) {
		pr_err("%s: Can't alloc dma engine.\n", __func__);
		return -ENOMEM;
	}

	g_dma_engine = hc18xx_dma;

	hc18xx_dma->dev = &pdev->dev;

	/* get memory resource and do ioremap*/
	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	hc18xx_dma->base = devm_ioremap_resource(&pdev->dev, res);
	if (IS_ERR(hc18xx_dma->base)) {
		pr_err("%s: failure to devm_ioremap_resource.\n", __func__);
		return PTR_ERR(hc18xx_dma->base);
	}

	/* get irq number and register irq handler */
	irq = platform_get_irq(pdev, 0);
	ret = devm_request_irq(&pdev->dev, irq, dma_irq_handler, 0, "DMA", hc18xx_dma);
	if (ret) {
		pr_err("%s: failure to register irq handler for dma.\n", __func__);
		return ret;
	}

	/* get dma clk and enable it */
	hc18xx_dma->dma_clk = devm_clk_get(&pdev->dev, "dma");
	if (IS_ERR(hc18xx_dma->dma_clk)) {
		pr_err("%s: failure to enable dma-clk.\n", __func__);
		return PTR_ERR(hc18xx_dma->dma_clk);
	}

	clk_prepare_enable(hc18xx_dma->dma_clk);
#ifdef HC18XX_CRYPTO_ALG_TYPE_ABLKCIPHER
	/*
	hc18xx_dma->queue_th = kthread_run(hc18xx_aes_rqueue_thread, hc18xx_dma, "hc18xx_crypto");
	if (IS_ERR(hc18xx_dma->queue_th)) {
		ret = PTR_ERR(hc18xx_dma->queue_th);
		goto e_thread;
	}
*/
	crypto_init_queue(&hc18xx_dma->queue, 1);
#endif
	/* register aes(cbc and ecb) algorithm */
	err = crypto_register_alg(&cbc_alg);
	if (err) {
		pr_err("%s: failure to reg cbc.\n", __func__);
		goto e_cbc;
	}

	err = crypto_register_alg(&ecb_alg);
	if (err) {
		pr_err("%s: failure to reg ecb.\n", __func__);
		goto e_ecb;
	}
	/* init lower level dma settings */
	hc18xx_dma_init(hc18xx_dma);

	INIT_LIST_HEAD(&hc18xx_dma->dma_device.channels);

	/* support DMA_MEMCPY */
	dma_cap_set(DMA_MEMCPY, hc18xx_dma->dma_device.cap_mask);

	spin_lock_init(&hc18xx_dma->lock);

	/* init channel parameters */
	for (i = 0; i < HC18XX_DMA_MAX_CHANNELS; i++) {
		struct hc18xx_dma_channel *hc18xx_dmac = &hc18xx_dma->channel[i];
		hc18xx_dmac->dma = hc18xx_dma;

		INIT_LIST_HEAD(&hc18xx_dmac->ld_queue);
		INIT_LIST_HEAD(&hc18xx_dmac->ld_free);
		INIT_LIST_HEAD(&hc18xx_dmac->ld_active);

		tasklet_init(&hc18xx_dmac->dma_tasklet, hc18xx_dma_tasklet, (unsigned long)hc18xx_dmac);
		hc18xx_dmac->chan.device = &hc18xx_dma->dma_device;
		dma_cookie_init(&hc18xx_dmac->chan);
		hc18xx_dmac->channel = i;

		/* add the channel to the DMAC list */
		list_add_tail(&hc18xx_dmac->chan.device_node, &hc18xx_dma->dma_device.channels);
	}

	hc18xx_dma->dma_device.dev = &pdev->dev;

	hc18xx_dma->dma_device.device_alloc_chan_resources = hc18xx_dma_alloc_chan_resources;
	hc18xx_dma->dma_device.device_free_chan_resources = hc18xx_dma_free_chan_resources;
	hc18xx_dma->dma_device.device_tx_status = hc18xx_dma_tx_status;
	hc18xx_dma->dma_device.device_prep_dma_memcpy = hc18xx_dma_prep_dma_memcpy;
	hc18xx_dma->dma_device.device_control = hc18xx_dma_control;
	hc18xx_dma->dma_device.device_issue_pending = hc18xx_dma_issue_pending;

	platform_set_drvdata(pdev, hc18xx_dma);

	hc18xx_dma->dma_device.copy_align = 2; /* 2^2 = 4 bytes alignment */
	//hc18xx_dma->dma_device.dev->dma_parms = &hc18xx_dma->dma_parms;
	//dma_set_max_seg_size(hc18xx_dma->dma_device.dev, 0xffffff);

	ret = dma_async_device_register(&hc18xx_dma->dma_device);
	if (ret) {
		pr_err("%s: failure to reg dma.\n", __func__);
		goto e_dma;
	}

	memcpy_chan = __augentix_dma_request_channel(DMA_MEMCPY);
	if (!memcpy_chan) {
		pr_err("failure to request dma channel\n");
		return -1;
	}

	return 0;

e_dma:
	crypto_unregister_alg(&ecb_alg);
e_ecb:
	crypto_unregister_alg(&cbc_alg);
e_cbc:
	//kthread_stop(hc18xx_dma->queue_th);
	return ret;
}

static int hc18xx_dma_remove(struct platform_device *pdev)
{
	struct hc18xx_dma_engine *hc18xx_dma = platform_get_drvdata(pdev);

	//kthread_stop(hc18xx_dma->queue_th);

	crypto_unregister_alg(&cbc_alg);
	crypto_unregister_alg(&ecb_alg);

	__augentix_dma_release_channel(memcpy_chan);

	dma_async_device_unregister(&hc18xx_dma->dma_device);

	clk_unprepare(hc18xx_dma->dma_clk);

	return 0;
}

static struct platform_driver hc18xx_dma_driver = {
	.driver =
	        {
	                .name = "hc18xx-dma",
	                .owner = THIS_MODULE,
	                .of_match_table = hc18xx_dma_of_dev_id,
	        },
	.remove = hc18xx_dma_remove,
};

static int __init hc18xx_dma_module_init(void)
{
	return platform_driver_probe(&hc18xx_dma_driver, hc18xx_dma_probe);
}
subsys_initcall(hc18xx_dma_module_init);

MODULE_AUTHOR("Nick Lin <nick.lin@augentix.com>");
MODULE_DESCRIPTION("hc18xx dma/aes driver");
MODULE_LICENSE("GPL");
