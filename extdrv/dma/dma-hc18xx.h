#ifndef _DMA_AES_HC18XX_H_
#define _DMA_AES_HC18XX_H_

/* ACCESS VIOLATION */
#define STR_ADDR_DEF 0x0
#define END_ADDR_DEF 0xFFFFFFF
#define HC18XX_CRYPTO_ALG_TYPE_ABLKCIPHER 1

#define HC18XX_DMA_MAX_CHAN_DESCRIPTORS 1
#define HC18XX_DMA_MAX_CHANNELS 1
#define MAX_DMA_ACCESS_CNT 8
#define MAX_XFER_SIZE_PER_CH 32768

/* DMA Offset */
#define DMA00_ACCESS_START 0x0000
#define DMA01_IRQ_CLEAR 0x0004
#define DMA02_IRQ_STATUS 0x0008
#define DMA03_IRQ_MASK 0x000C
#define DMA04_CTRL 0x0010
#define DMA07_CTRL 0x001C
#define DMA08_CTRL 0x0020
#define DMA09_STR_ADDR_R 0x0024
#define DMA10_END_ADDR_R 0x0028
#define DMA11_STR_ADDR_W 0x002C
#define DMA12_END_ADDR_W 0x0030
#define DMA13_EFUSE_CTRL 0x0034
#define CH_CTRL0_BASE 0x0038
#define CH_CTRL1_BASE 0x0044
#define DMA_CH_CTRL0(n) (CH_CTRL0_BASE + (n * 0x10))
#define DMA_CH_CTRL1(n) (CH_CTRL1_BASE + (n * 0x10))
#define DMA46_BANK_INTER_TP_0 0x00B8
#define DMA47_BANK_INTER_TP_1 0x00BC
#define DMA48_ADDR_BANK_OF_R0 0x00C0
#define DMA49_ADDR_BANK_OF_R1 0x00C4
#define INI_ADDR_LINEAR_W_BASE 0x00C8
#define DMA_INI_ADDR_LINEAR_W(n) (INI_ADDR_LINEAR_W_BASE + (n * 0x4))
#define INI_ADDR_LINEAR_R_BASE 0x00E8
#define DMA_INI_ADDR_LINEAR_R(n) (INI_ADDR_LINEAR_R_BASE + (n * 0x4))

/* IRQ */
#define IRQ_MASK_ACCESS_END (1 << 0)
#define IRQ_MASK_BW_INSUFFICIENT_R (1 << 2)
#define IRQ_MASK_ACCESS_VIOLATION_R (1 << 3)
#define IRQ_MASK_BW_INSUFFICIENT_W (1 << 5)
#define IRQ_MASK_ACCESS_VIOLATION_W (1 << 6)
#define IRQ_MASK_ALL                                                                                                   \
	(IRQ_MASK_ACCESS_END | IRQ_MASK_BW_INSUFFICIENT_R | IRQ_MASK_ACCESS_VIOLATION_R | IRQ_MASK_BW_INSUFFICIENT_W | \
	 IRQ_MASK_ACCESS_VIOLATION_W)

/* DMA02 */
#define STATUS_ACCESS_END IRQ_MASK_ACCESS_END
#define STATUS_BW_INSUFFICIENT_R IRQ_MASK_BW_INSUFFICIENT_R
#define STATUS_ACCESS_VIOLATION_R IRQ_MASK_ACCESS_VIOLATION_R
#define STATUS_BW_INSUFFICIENT_W IRQ_MASK_BW_INSUFFICIENT_W
#define STATUS_ACCESS_VIOLATION_W IRQ_MASK_ACCESS_VIOLATION_W

/* DMA04 */
#define COL_ADDR_TYPE_512 0
#define COL_ADDR_TYPE_1024 1
#define COL_ADDR_TYPE_2048 2
#define COL_ADDR_TYPE_4096 3
#define TOTAL_ACCESS_CNT(n) (n << 16)

/* DMA07 */
#define TARGET_BURST_LEN_R(n) (n << 0)
#define TARGET_BURST_LEN_W(n) (n << 8)
#define BANK_ADDR_TYPE_4 0
#define BANK_ADDR_TYPE_8 1
#define ACCESS_END_SEL_0 (0 << 24)
#define ACCESS_END_SEL_1 (1 << 24)

/* DMA08 */
#define TARGET_FIFO_LEVEL_R(n) (n << 0)
#define TARGET_FIFO_LEVEL_W(n) (n << 8)
#define TARGET_FULL_LEVEL_R(n) (n << 16)
#define TARGET_FULL_LEVEL_W(n) (n << 24)

#define BANK_INTERLEAVE_0 0
#define BANK_INTERLEAVE_2 1
#define BANK_INTERLEAVE_4 2
#define BANK_INTERLEAVE_8 3

/* AES Offser */
#define AES04_KEY_0 0x0110
#define AES05_KEY_1 0x0114
#define AES06_KEY_2 0x0118
#define AES07_KEY_3 0x011C
#define AES08_KEY_4 0x0120
#define AES09_KEY_5 0x0124
#define AES10_KEY_6 0x0128
#define AES11_KEY_7 0x012C
#define AES16_ENDIN_BLOCKM 0x0140
#define AES17_INIT_VEC_0 0x0144
#define AES18_INIT_VEC_1 0x0148
#define AES19_INIT_VEC_2 0x014C
#define AES20_INIT_VEC_3 0x0150

#define AES_MODE_NORMAL (0 << 0)
#define AES_MODE_BYPASS (1 << 0)
#define AES_MODE_DISABLE (2 << 0)

#define AES_ACCESS_LINEAR (0 << 8)
#define AES_ACCESS_YORC (1 << 8)
#define AES_ACCESS_CB (2 << 8)
#define AES_ACCESS_CR (3 << 8)

#define AES_BIG_ENDIAN (0x0 << 8)
#define AES_LIT_ENDIAN (0x1 << 8)
#define AES_ECB_MODE (0x0 << 16)
#define AES_CBC_MODE (0x1 << 16)

#define BigtoLittle32(A)                                                                               \
	((((u32)(A)&0xff000000) >> 24) | (((u32)(A)&0x00ff0000) >> 8) | (((u32)(A)&0x0000ff00) << 8) | \
	 (((u32)(A)&0x000000ff) << 24))

typedef enum
{
	DMA_NORMAL,
	DMA_AES_CBC_BE,
	DMA_AES_CBC_LE,
	DMA_AES_ECB_BE,
	DMA_AES_ECB_LE,
	DMA_MAX,
} dma_op_mode;

#define LEN_AES_KEY 32
#define LEN_AES_INIT_VT 16

struct dma_args {
	u32 oper_mode;
#ifdef HC18XX_CRYPTO_ALG_TYPE_ABLKCIPHER
	u32 *aes_key;
	u32 *aes_init_vet;
#else
	unsigned char aes_key[LEN_AES_KEY];
	unsigned char aes_init_vet[LEN_AES_INIT_VT];
#endif
};

enum hc18xx_dma_prep_type
{
	HC18XX_DMA_DESC_MEMCPY = 0,
};

struct hc18xx_dma_desc {
	struct list_head node;
	struct dma_async_tx_descriptor desc;
	enum dma_status status;
	dma_addr_t src;
	dma_addr_t dest;
	size_t len;
	size_t len_off;
	enum dma_transfer_direction direction;
	enum hc18xx_dma_prep_type type;
};

struct hc18xx_dma_channel {
	struct hc18xx_dma_engine *dma;
	unsigned int channel;
	struct tasklet_struct dma_tasklet;
	struct list_head ld_free;
	struct list_head ld_queue;
	struct list_head ld_active;
	int descs_allocated;
	struct dma_chan chan;
	struct dma_async_tx_descriptor desc;
	enum dma_status status;
};

struct hc18xx_dma_args {
	u32 oper_mode;
	u32 edian;
	u32 aes_blk_mode;
	u32 aes_init_vet[4];
	u32 aes_key[8];
};

enum engine_status
{
	ENGINE_IDLE,
	ENGINE_BUSY,
	ENGINE_W_DEQUEUE,
};

enum dma_addr_type {
	START,
	END,
};

struct hc18xx_dma_engine {
	struct device *dev;
	struct dma_device dma_device;
	void __iomem *base;
	struct clk *dma_clk;
	spinlock_t lock;
	struct hc18xx_dma_channel channel[HC18XX_DMA_MAX_CHANNELS];
	struct hc18xx_dma_args dma_args;

	struct ablkcipher_request *req;
	struct scatterlist *sg_src;
	struct scatterlist *sg_dst;
	struct tasklet_struct tasklet;
	struct task_struct *queue_th;
	enum engine_status eng_st;
	struct crypto_queue queue;
	u8 bank_addr_type;
	u8 col_addr_type;
	u32 bank_intertleave;
	u8 busy;
	u8 is_ablk_mode;
};

struct hc18xx_aes_ctx {
	struct hc18xx_dma_engine *dev;
	void *src;
	void *dst;
	u32 mode;
	u32 len;
	u32 keylen;
	u32 aes_key[8];
	u32 *aes_iv;
};

struct hc18xx_aes_reqctx {
	unsigned long mode;
};

#endif
