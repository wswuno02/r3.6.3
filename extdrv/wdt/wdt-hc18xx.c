#include <linux/clk.h>
#include <linux/err.h>
#include <linux/io.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/watchdog.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/interrupt.h>
#include <linux/of_irq.h>
#include <mach/hc18xx.h>

/* Region WDT */
#define WDT_TRI_WDT 0x00
#define WDT_CLR_IRQ 0x04
#define WDT_STA_WDT 0x08
#define WDT_LCK_STA 0x0C
#define WDT_ULK_REG 0x10
#define WDT_DIS_WDT 0x14
#define WDT_CNT_INI 0x18
#define WDT_CNT_S1X 0x1C

/* Control register */
#define WDT_PRE_SC0_VAL 0xF
#define WDT_PRE_SC1_VAL 0xF
#define WDT_SET_CNT_ST1_MAX 0xFFFF0000
#define WDT_SET_CNT_ST1_MIN 0xFFFF
#define WDT_REG_CLR 0x0
#define WDT_PRE_SC0_ADDR 16
#define WDT_PRE_SC1_ADDR 24

#define DEFAULT_TIMEOUT	30
#define MAX_TIMEOUT	90

static unsigned int timeout = DEFAULT_TIMEOUT;
module_param(timeout, uint, 0);
MODULE_PARM_DESC(timeout,
	"Watchdog timeout in seconds. (>=0, default="
	__MODULE_STRING(DEFAULT_TIMEOUT) ")");

static bool nowayout = WATCHDOG_NOWAYOUT;
module_param(nowayout, bool, 0);
MODULE_PARM_DESC(nowayout,
	"Watchdog cannot be stopped once started(default="
	__MODULE_STRING(WATCHDOG_NOWAYOUT) ")");

/* hc wdt device data */
struct hc_wdt_pdata{
	struct watchdog_device	wdd;
	int			irq;
	//spinlock_t		lock;
	void __iomem		*reg_base;
	struct clk		*clk;
	int			hz;
};

static const struct watchdog_info hc_wdt_info = {
	.options	=	WDIOF_KEEPALIVEPING |
				WDIOF_SETTIMEOUT |
				WDIOF_MAGICCLOSE,
	.identity	=	"Augentix Watchdog",
};

int hc_wdt_exp2(int exp)
{
	if(exp == 0)	return 1;
	else	return 2 * hc_wdt_exp2(exp - 1);
}

void hc_wdt_unlock_reg(struct watchdog_device *wdd)
{
	struct hc_wdt_pdata *hc_wdt = watchdog_get_drvdata(wdd);
	void __iomem *ker_off;

	ker_off = hc_wdt->reg_base;
	writeb_relaxed(0x54, ker_off + WDT_ULK_REG);
	writeb_relaxed(0x6F, ker_off + WDT_ULK_REG);
	writeb_relaxed(0x6B, ker_off + WDT_ULK_REG);
	writeb_relaxed(0x79, ker_off + WDT_ULK_REG);
	writeb_relaxed(0x6F, ker_off + WDT_ULK_REG);

}

static irqreturn_t hc_wdt_irq_handler(int irq, void *dev_id)
{
	struct hc_wdt_pdata *hc_wdt = (struct hc_wdt_pdata *)dev_id;
	void __iomem *ker_off;

	pr_info("[WDT] Watchdog timed out.");

	ker_off = hc_wdt->reg_base;
	writeb_relaxed(0x1, ker_off + WDT_CLR_IRQ);

	return IRQ_HANDLED;
}

static int hc_wdt_ping(struct watchdog_device *wdd)
{
	struct hc_wdt_pdata *hc_wdt = watchdog_get_drvdata(wdd);
	void __iomem *ker_off;

	ker_off = hc_wdt->reg_base;
	writeb_relaxed(0x1, ker_off + WDT_TRI_WDT);

	return 0;
}

static int hc_wdt_start(struct watchdog_device *wdd)
{
	struct hc_wdt_pdata *hc_wdt = watchdog_get_drvdata(wdd);
	void __iomem *ker_off;
	uint32_t t_reg;

	ker_off = hc_wdt->reg_base;
	t_reg = *(volatile uint32_t *) (ker_off + WDT_LCK_STA);
	if((t_reg & 0x1) == 1){
		hc_wdt_unlock_reg(wdd);
	}

	t_reg = *(volatile uint32_t *) (ker_off + WDT_DIS_WDT);
	if((t_reg & 0x1) != 0x0){
		t_reg = t_reg & 0x0;
		writeb_relaxed(t_reg, ker_off + WDT_DIS_WDT);
	}

	writeb_relaxed(0xFF, ker_off + WDT_ULK_REG);	//Re-lock reg

	return 0;
}

static int hc_wdt_stop(struct watchdog_device *wdd)
{
	struct hc_wdt_pdata *hc_wdt = watchdog_get_drvdata(wdd);
	void __iomem *ker_off;
	uint32_t t_reg;

	ker_off = hc_wdt->reg_base;
	t_reg = *(volatile uint32_t *) (ker_off + WDT_LCK_STA);
	if((t_reg & 0x1) == 1){
		hc_wdt_unlock_reg(wdd);
	}

	t_reg = *(volatile uint32_t *) (ker_off + WDT_DIS_WDT);
	if((t_reg & 0x1) != 0x1){
		t_reg = t_reg | 0x1;
		writeb_relaxed(t_reg, ker_off + WDT_DIS_WDT);
	}

	writeb_relaxed(0xFF, ker_off + WDT_ULK_REG);	//Re-lock reg

	return 0;
}

static int hc_wdt_convert_cnt(struct watchdog_device *wdd, uint32_t prescaler, unsigned int psca_bitaddr, unsigned int timeout)
{
	struct hc_wdt_pdata *hc_wdt = watchdog_get_drvdata(wdd);
	void __iomem *ker_off;
	uint32_t t_reg;
	uint32_t p_sca_tmp;
	uint32_t cnt_tmp;
	uint32_t calc_res1=0,calc_res2=0;
	int wdt_hz;

	/*
	 * Convert: timeout -> interval(stage1)
	 * hc_wdt_set_timeout() set the stage1 timeout only(both min and max),
	 * so it will ignore the invalid tick.
	 * The initial stage and the stage2 timeout will set the default value
	 * in the device tree. It means that the timeout values will decided
	 * in the build time.
	 *
	 * Default value(defined in the device tree):
	 * @example: stage1 valid timeout
	 * stage1 count max: 30 sec
	 * stage1 count min: 20 sec
	 *                    20         <--
	 * |---------|-------------------|
	 * 0         10                  30
	 * it means: 10 < valid tick < 30
	 * User can set the same value to avoid invalid tick.
	 *
	 * interval(stage1) = count_stage1_max / rate(stage1)
	 * rate(stage1) = 24MHz / ((2 ^ prescaler_1) + 1)
	 * prescaler_0 & prescaler_1 -> Fh (for linux wdt framework timeout val
	 * ue setting in seconds)
	 */

	/*
	 * Get PRESCALER for calculate rate decision
	 */
	ker_off = hc_wdt->reg_base;
	t_reg = *(volatile uint32_t *) (ker_off + WDT_DIS_WDT);
	t_reg = t_reg & (prescaler << psca_bitaddr);
	p_sca_tmp = t_reg >> psca_bitaddr;
	//pr_info("[wdt](wdt_convert_cnt)prescaler: %x\n", p_sca_tmp);

	/*
	 * Convert timeout value to cycle count(for WDT register)
	 */
	wdt_hz = hc_wdt->hz;
	calc_res1 = hc_wdt_exp2(p_sca_tmp) + 1;
	calc_res2 = wdt_hz / calc_res1;
	cnt_tmp = timeout * calc_res2;
	//pr_info("[wdt](wdt_convert_cnt)cnt: %x\n", cnt_tmp);

	return cnt_tmp;
}

static int hc_wdt_set_timeout(struct watchdog_device *wdd, unsigned int timeout)
{
	struct hc_wdt_pdata *hc_wdt = watchdog_get_drvdata(wdd);
	void __iomem *ker_off;
	uint32_t t_reg;
	uint32_t cnt_st1_max_tmp;
	uint32_t cnt_st1_min_tmp;

	if(timeout < 0 || timeout > MAX_TIMEOUT) {
		pr_warn("timeout value must be 0<=x<=%d\n", MAX_TIMEOUT);
		return -EINVAL;
	}

	ker_off = hc_wdt->reg_base;
	t_reg = *(volatile uint32_t *) (ker_off + WDT_LCK_STA);
	if((t_reg & 0x1) == 1){
		hc_wdt_unlock_reg(wdd);
	}

	cnt_st1_max_tmp = (uint32_t)hc_wdt_convert_cnt(wdd, WDT_PRE_SC1_VAL, WDT_PRE_SC1_ADDR, timeout);

	/* Set WDT register - COUNT_STAGE1_MAX */
	t_reg = *(volatile uint32_t *) (ker_off + WDT_CNT_S1X);
	t_reg = t_reg & WDT_SET_CNT_ST1_MAX;
	t_reg = t_reg | cnt_st1_max_tmp;
	// pr_info("[wdt](wdt_set_timeout)after CNT_S1X: %x\n", t_reg);
	writel_relaxed(t_reg, ker_off + WDT_CNT_S1X);

	/* Set WDT register - COUNT_STAGE1_MIN */
	t_reg = *(volatile uint32_t *) (ker_off + WDT_CNT_INI);
	t_reg = t_reg & WDT_SET_CNT_ST1_MIN;
	cnt_st1_min_tmp = cnt_st1_max_tmp << 16;
	t_reg = t_reg | cnt_st1_min_tmp;
	// pr_info("[wdt](wdt_set_timeout)after CNT_INI: %x\n", t_reg);
	writel_relaxed(t_reg, ker_off + WDT_CNT_INI);

	writeb_relaxed(0x1, ker_off + WDT_DIS_WDT);	//dis wdt
	delay_n(50);
	writeb_relaxed(0x0, ker_off + WDT_DIS_WDT);	//en wdt
	writeb_relaxed(0x54, ker_off + WDT_ULK_REG);	//Re-lock reg

	return 0;
}

static long hc_wdt_ioctl(struct watchdog_device *wdd, unsigned int cmd, unsigned long arg)
{
	void __user *argp = (void __user *)arg;
	int __user *p = argp;
	int new_value;

	switch (cmd) {
	case WDIOC_KEEPALIVE:
		hc_wdt_ping(wdd);
		return 0;
	case WDIOC_SETTIMEOUT:
		if(get_user(new_value, p))
			return -EFAULT;
		hc_wdt_set_timeout(wdd, new_value);
		return 0;
	default:
		return -ENOTTY;
	}
}

static const struct watchdog_ops hc_wdt_ops = {
	.owner		= THIS_MODULE,
	.start		= hc_wdt_start,
	.stop		= hc_wdt_stop,
	.ping		= hc_wdt_ping,
	.ioctl		= hc_wdt_ioctl,
	.set_timeout	= hc_wdt_set_timeout,
};

static int hc_wdt_setreg_forssys(struct watchdog_device *wdd, struct platform_device *pdev)
{
	struct hc_wdt_pdata *hc_wdt = watchdog_get_drvdata(wdd);
	void __iomem *ker_off;
	uint32_t t_reg;
	uint32_t psca0, psca1;
	uint32_t cnt_init, cnt_st1_min, cnt_st1_max, cnt_st2;
	uint32_t dt_init, dt_st1_min, dt_st1_max, dt_st2;
	struct device_node *np = pdev->dev.of_node;

	/*
	 *
	 * Setting the WDT register is suitable for the linux subsystem
	 *
	 */

	/*
	 * Check the UNLOCK status
	 */
	ker_off = hc_wdt->reg_base;
	t_reg = *(volatile uint32_t *) (ker_off + WDT_LCK_STA);
	if((t_reg & 0x1) == 1){
		hc_wdt_unlock_reg(wdd);
	}

	/*
	 * Set PRESCALER_0 & PRESCALER_1 to Fh
	 */
	t_reg = *(volatile uint32_t *) (ker_off + WDT_DIS_WDT);
	psca0 = WDT_PRE_SC0_VAL << 16;
	t_reg = t_reg | psca0;
	writel_relaxed(t_reg, ker_off + WDT_DIS_WDT);

	t_reg = *(volatile uint32_t *) (ker_off + WDT_DIS_WDT);
	psca1 = WDT_PRE_SC1_VAL << 24;
	t_reg = t_reg | psca1;
	writel_relaxed(t_reg, ker_off + WDT_DIS_WDT);

	t_reg = *(volatile uint32_t *) (ker_off + WDT_DIS_WDT);

	/*
	 * Set timeout values by device tree
	 */
	if(of_property_read_u32(np, "count-init", &dt_init)){	//init stage
		dev_err(&pdev->dev, "invalid cycle count\n");
		return -ERANGE;
	}
	cnt_init = (uint32_t)hc_wdt_convert_cnt(wdd, WDT_PRE_SC0_VAL, WDT_PRE_SC0_ADDR, dt_init);

	if(of_property_read_u32(np, "count-st1-max", &dt_st1_max)){	//stage1 max
		dev_err(&pdev->dev, "invalid cycle count\n");
		return -ERANGE;
	}
	cnt_st1_max = (uint32_t)hc_wdt_convert_cnt(wdd, WDT_PRE_SC1_VAL, WDT_PRE_SC1_ADDR, dt_st1_max);


	if(of_property_read_u32(np, "count-st1-min", &dt_st1_min)){	//stage1 min
		//if user didn't get count-st1-min from device tree,
		//it will same as count-st1-max.(To avoid user confused)
		dev_err(&pdev->dev, "DT st1_min ref st1_max\n");
		dt_st1_min = dt_st1_max;
	}
	cnt_st1_min = (uint32_t)hc_wdt_convert_cnt(wdd, WDT_PRE_SC1_VAL, WDT_PRE_SC1_ADDR, dt_st1_min);


	if(of_property_read_u32(np, "count-st2", &dt_st2)){	//stage2
		dev_err(&pdev->dev, "invalid cycle count\n");
		return -ERANGE;
	}
	cnt_st2 = (uint32_t)hc_wdt_convert_cnt(wdd, WDT_PRE_SC1_VAL, WDT_PRE_SC1_ADDR, dt_st2);

	t_reg = *(volatile uint32_t *) (ker_off + WDT_CNT_INI);
	t_reg = t_reg & WDT_REG_CLR;
	t_reg = t_reg | (cnt_init + (cnt_st1_min << 16));
	writel_relaxed(t_reg, ker_off + WDT_CNT_INI);

	t_reg = *(volatile uint32_t *) (ker_off + WDT_CNT_S1X);
	t_reg = t_reg & WDT_REG_CLR;
	t_reg = t_reg | (cnt_st1_max + (cnt_st2 << 16));
	writel_relaxed(t_reg, ker_off + WDT_CNT_S1X);

	writeb_relaxed(0x54, ker_off + WDT_ULK_REG);	//Re-lock reg

	return 0;
}

static int hc_wdt_probe(struct platform_device *pdev)
{
	int ret = 0;
	struct device *dev = &pdev->dev;
	struct resource *wdt_mem;
	struct watchdog_device *wdd;
	struct hc_wdt_pdata *hc_wdt;

	pr_info("Augentix WDT driver probed.\n");

	hc_wdt = devm_kzalloc(dev, sizeof(*hc_wdt), GFP_KERNEL);
	if(!hc_wdt)
		return -ENOMEM;

	hc_wdt->clk = devm_clk_get(dev, "sys_clk");
	if(WARN_ON(IS_ERR(hc_wdt->clk))){
		dev_err(dev, "failed to find watchdog clock source\n");
		return PTR_ERR(hc_wdt->clk);
	}

	ret = clk_prepare_enable(hc_wdt->clk);
	if(ret < 0){
		dev_err(dev, "failed to enable clock\n");
		return ret;
	}

	hc_wdt->hz = clk_get_rate(hc_wdt->clk);
	platform_set_drvdata(pdev, hc_wdt);

	wdd			= &hc_wdt->wdd;
	wdd->info		= &hc_wdt_info;
	wdd->ops		= &hc_wdt_ops;
	//wdd->min_timeout	= 1;
	wdd->max_timeout	= MAX_TIMEOUT;
	wdd->timeout		= DEFAULT_TIMEOUT;

	watchdog_init_timeout(wdd, timeout, dev);
	watchdog_set_drvdata(wdd, hc_wdt);
	watchdog_set_nowayout(wdd, nowayout);

	wdt_mem = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	hc_wdt->reg_base = devm_ioremap_resource(dev, wdt_mem);
	if(IS_ERR(hc_wdt->reg_base)){
		return PTR_ERR(hc_wdt->reg_base);
	}

	hc_wdt->irq = irq_of_parse_and_map(pdev->dev.of_node, 0);

	ret = devm_request_irq(&pdev->dev, hc_wdt->irq, hc_wdt_irq_handler, IRQF_SHARED, pdev->name, hc_wdt);
	if(ret != 0){
		dev_err(dev, "failed to install irq(%d)", ret);
		return -EINVAL;
	}

	ret = watchdog_register_device(wdd);
	if(ret < 0){
		dev_err(dev, "cannot register watchdog(%d)\n", ret);
		return -EINVAL;
	}

	hc_wdt_setreg_forssys(wdd, pdev);

	return ret;
}

static int hc_wdt_remove(struct platform_device *pdev)
{
	struct hc_wdt_pdata *hc_wdt = platform_get_drvdata(pdev);

	watchdog_unregister_device(&hc_wdt->wdd);
	clk_disable_unprepare(hc_wdt->clk);

	return 0;
}

static const struct of_device_id hc_wdt_of_match[] = {
	{ .compatible = "augentix,wdt" },
	{},
};

MODULE_DEVICE_TABLE(of, hc_wdt_of_match);

static struct platform_driver hc_wdt_driver = {
	.driver = {
		.name = "hc_wdt",
		.owner = THIS_MODULE,
		.of_match_table = hc_wdt_of_match,
	},
	.probe = hc_wdt_probe,
	.remove = hc_wdt_remove,
};

module_platform_driver(hc_wdt_driver);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Augentix Inc.");

