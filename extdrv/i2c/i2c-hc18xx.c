/*
 * I2C bus driver for HC18xx I2C controller
 *
 * Copyright (C) 2015 - 2020 Augentix Inc.
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any
 * later version.
 */

#include <linux/clk.h>
#include <linux/delay.h>
#include <linux/i2c.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/clk.h>
#include <linux/err.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/interrupt.h>
#include <linux/semaphore.h>
#ifdef CONFIG_PLAT_HC18XX
#include <mach/hc18xx.h>
#else
#include <linux/reset.h>
#endif

/* Region offsets for I2C controller. */
#define HC18XX_I2C_TRIG_OFFSET 0x00 /* Trigger I2C transfer, RO */
#define HC18XX_I2C_ICLR_OFFSET 0x04 /* Clear irq state, RO */
#define HC18XX_I2C_ISR_OFFSET 0x08 /* Irq status, RO */
#define HC18XX_I2C_IER_OFFSET 0x0C /* Enable irq, RW */
#define HC18XX_I2C_SR_OFFSET 0x10 /* Status register, RO */
#define HC18XX_I2C_TRANS_SR_OFFSET 0x14 /* Transaction status, RO */
#define HC18XX_I2C_CLK_OFFSET 0x18 /* Clock cycle, RW */
#define HC18XX_I2C_AUTOREAD_OFFSET 0x1C /* Control register, RW */
#define HC18XX_I2C_ADDR_OFFSET 0x20 /* I2C address register, RW */
#define HC18XX_I2C_BYTES_OFFSET 0x24 /* Read or write data length, RW */
#define HC18XX_I2C_WR0_OFFSET 0x28 /* Register for write data, RW */
#define HC18XX_I2C_WR1_OFFSET 0x2C /* Register for write data, RW */
#define HC18XX_I2C_WR2_OFFSET 0x30 /* Register for write data, RW */
#define HC18XX_I2C_WR3_OFFSET 0x34 /* Register for write data, RW */
#define HC18XX_I2C_WR4_OFFSET 0x38 /* Register for write data, RW */
#define HC18XX_I2C_WR5_OFFSET 0x3C /* Register for write data, RW */
#define HC18XX_I2C_WR6_OFFSET 0x40 /* Register for write data, RW */
#define HC18XX_I2C_WR7_OFFSET 0x44 /* Register for write data, RW */
#define HC18XX_I2C_RD0_OFFSET 0x48 /* Register for read data, RW */
#define HC18XX_I2C_RD1_OFFSET 0x4C /* Register for read data, RW */

/* Control register signal difinition */
#define HC18XX_I2C_TRIG_SET 0x01 /* Start transmit */
#define HC18XX_I2C_ICLR_SET 0x01 /* Clear irq status */
#define HC18XX_I2C_ISR_DONE 0x01 /* I2C interrupt*/
#define HC18XX_I2C_IER_DISABLE 0x01 /* Disable interrupt */
#define HC18XX_I2C_IER_ENABLE 0x00 /* Enable interrupt */
#define HC18XX_I2C_SR_BUSY 0x01 /* Bus busy */
#define HC18XX_I2C_ADDR_WRITE 0x00 /* I2C read */
#define HC18XX_I2C_ADDR_READ BIT(8) /* I2C write */
#define HC18XX_I2C_SR_ERRADDR BIT(16) /* Error address */
#define HC18XX_I2C_SR_ERRDATA BIT(17) /* Error data */
#define HC18XX_I2C_CLK_DEFAULT ((120 << 16) | 0x00) /* Frquency 100Khz */
#define HC18XX_I2C_AUTOREAD_ON ((10 << 16) | 0x01) /* Enable autogenread */
#define HC18XX_I2C_AUTOREAD_OFF ((10 << 16) | 0x00) /* Disable autogenread */

/* Define masks */
#define HC18XX_I2C_WRLEN_MASK 0xff
#define HC18XX_I2C_WRNUM_MASK 0xff00
#define HC18XX_I2C_RDLEN_MASK 0xff0000
#define HC18XX_I2C_CLK_MASK 0xff0000

/* Shift macros */
#define HC18XX_I2C_DELAY_CYCLE_SHIFT 0x08
#define HC18XX_I2C_RDLEN_SHIFT 0x10
#define HC18XX_I2C_CLK_SHIFT 0x10

/* Software definition */
#define HC18XX_I2C_DEFAULT_DELAY_CYCLE 0x04
#define HC18XX_I2C_DEFAULT_RATE (100 * 1000)
#define HC18XX_I2C_MAX_RATE (400 * 1000)
#define HC18XX_I2C_MAX_WR_LEN 0x20
#define HC18XX_I2C_MAX_RD_LEN 0x08

#define HC18XX_I2C_TIMEOUT msecs_to_jiffies(1000)

/* Register read/write macro */
#define hc18xx_readreg(offset) readl_relaxed(id->membase + offset)
#define hc18xx_writereg(val, offset) writel_relaxed(val, id->membase + offset)
#define i2c_write_clkreg(val)                                                                                          \
	writel_relaxed((val << HC18XX_I2C_CLK_SHIFT) & HC18XX_I2C_CLK_MASK, id->membase + HC18XX_I2C_CLK_OFFSET)

/* Driver name */
#define DRIVER_NAME "hc18xx_i2c"

/* Driver private data */
struct hc18xx_i2c {
	void __iomem *membase;
	struct i2c_adapter adap;
	struct i2c_msg *p_msg;
	u32 err_status;
	struct completion xfer_done;
	struct semaphore sem;
	int irq;
	struct clk *sclk;
#ifdef CONFIG_PLAT_HC18XX
	int sw_rst;
#else
	struct reset_control *rst;
#endif
	u32 sclk_rate;
	u32 i2c_rate;
	u32 delay_cycle;
};

static int hc18xx_i2c_setclk(u32 rate, struct hc18xx_i2c *id)
{
	u32 sclk_rate = id->sclk_rate;
	u32 cycle = 0;
	/* calculate clock division */
	cycle = ((sclk_rate + rate) / (rate << 1)) - 1;
	/* write div to i2c */
	hc18xx_writereg((cycle << HC18XX_I2C_CLK_SHIFT) | (id->delay_cycle << HC18XX_I2C_DELAY_CYCLE_SHIFT),
	                HC18XX_I2C_CLK_OFFSET);
	return 0;
}

static void hc18xx_i2c_reset(struct hc18xx_i2c *id)
{
	/* setting clock division */
	hc18xx_i2c_setclk(id->i2c_rate, id);
	/* clear irq bit */
	hc18xx_writereg(HC18XX_I2C_IER_DISABLE, HC18XX_I2C_IER_OFFSET);
	hc18xx_writereg(HC18XX_I2C_ICLR_SET, HC18XX_I2C_ICLR_OFFSET);
	/* diable autogenread */
	hc18xx_writereg(HC18XX_I2C_AUTOREAD_OFF, HC18XX_I2C_AUTOREAD_OFFSET);
	/* set slave addr to 0 */
	hc18xx_writereg(0x00, HC18XX_I2C_ADDR_OFFSET);
	/* default configuration */
	hc18xx_writereg(0x20002, HC18XX_I2C_BYTES_OFFSET);
	hc18xx_writereg(0x00, HC18XX_I2C_WR0_OFFSET);
	hc18xx_writereg(0x00, HC18XX_I2C_WR1_OFFSET);
	hc18xx_writereg(0x00, HC18XX_I2C_WR2_OFFSET);
	hc18xx_writereg(0x00, HC18XX_I2C_WR3_OFFSET);
	hc18xx_writereg(0x00, HC18XX_I2C_WR4_OFFSET);
	hc18xx_writereg(0x00, HC18XX_I2C_WR5_OFFSET);
	hc18xx_writereg(0x00, HC18XX_I2C_WR6_OFFSET);
	hc18xx_writereg(0x00, HC18XX_I2C_WR7_OFFSET);
	hc18xx_writereg(0x00, HC18XX_I2C_RD0_OFFSET);
	hc18xx_writereg(0x00, HC18XX_I2C_RD1_OFFSET);
	/* interrupt enable */
	hc18xx_writereg(HC18XX_I2C_IER_ENABLE, HC18XX_I2C_IER_OFFSET);
}

static irqreturn_t hc18xx_i2c_isr(int irq, void *dev_id)
{
	struct hc18xx_i2c *id = (struct hc18xx_i2c *)dev_id;
	u32 reg;
	reg = hc18xx_readreg(HC18XX_I2C_ISR_OFFSET);

	if (reg != HC18XX_I2C_ISR_DONE) {
		return IRQ_NONE;
	}

	/* clear interrupt status */
	hc18xx_writereg(HC18XX_I2C_ICLR_SET, HC18XX_I2C_ICLR_OFFSET);
	/* read error status */
	id->err_status = hc18xx_readreg(HC18XX_I2C_SR_OFFSET);
	/* print isr info */
	pr_debug("hc18xx_i2c_isr: isr status %x, err status %x\n", reg, id->err_status);
	/* wake up xfer_done */
	complete(&id->xfer_done);
	return IRQ_HANDLED;
}

static int hc18xx_i2c_msend(struct hc18xx_i2c *id)
{
	struct i2c_msg *msg = id->p_msg;
	int count = 0;
	int ret = 0;
	int it = 0;
	u32 reg = 0;
	u32 buf = 0;
	u8 *data_ptr = msg->buf;
	u8 len = msg->len;

	if (msg->len > HC18XX_I2C_MAX_WR_LEN) {
		return -EINVAL;
	}

	/* set addr and read flags */
	hc18xx_writereg(HC18XX_I2C_ADDR_WRITE | (msg->addr & 0x7f), HC18XX_I2C_ADDR_OFFSET);

	if (msg->flags & I2C_M_NOSTART) {
		--len;
		reg = ((len / msg->buf[0] - 1) << 8) | (msg->buf[0] - 1);
		++data_ptr;
	} else {
		reg = len - 1;
	}

	hc18xx_writereg(reg, HC18XX_I2C_BYTES_OFFSET);

	/* fill data to register */
	while (it < len) {
		reg = 0;

		for (count = it; count < len; ++count) {
			buf = *(data_ptr + count);
			reg += (buf << ((count - it) << 3));

			if ((count - it) >= 3) {
				break;
			}
		}

		pr_debug("wd_data = %x\n", reg);
		hc18xx_writereg(reg, HC18XX_I2C_WR0_OFFSET + it);
		it += 4;
	}

	/* clear interrupt status and trigger once */
	hc18xx_writereg(HC18XX_I2C_TRIG_SET, HC18XX_I2C_TRIG_OFFSET);

	/* wait for work completion */
	ret = wait_for_completion_timeout(&id->xfer_done, id->adap.timeout);

	if (ret == 0) {
		hc18xx_i2c_reset(id);
		dev_err(id->adap.dev.parent, "timeout waiting on completion\n");
		return -ETIMEDOUT;
	}

	return 0;
}

static int hc18xx_i2c_mrecv(struct hc18xx_i2c *id)
{
	struct i2c_msg *msg = id->p_msg;
	int count = 0;
	int ret = 0;
	int len = 0;
	int it = 0;
	u32 reg = 0;

	if (msg->len > HC18XX_I2C_MAX_RD_LEN) {
		return -EINVAL;
	}

	/* set addr and read flags */
	hc18xx_writereg(HC18XX_I2C_ADDR_READ | (msg->addr & 0x7f), HC18XX_I2C_ADDR_OFFSET);

	/* set read length */
	hc18xx_writereg((msg->len - 1) << HC18XX_I2C_RDLEN_SHIFT, HC18XX_I2C_BYTES_OFFSET);

	/* clear interrupt status and trigger once */
	hc18xx_writereg(HC18XX_I2C_TRIG_SET, HC18XX_I2C_TRIG_OFFSET);

	/* wait for work completion */
	ret = wait_for_completion_timeout(&id->xfer_done, id->adap.timeout);

	if (ret == 0) {
		hc18xx_i2c_reset(id);
		dev_err(id->adap.dev.parent, "timeout waiting on completion\n");
		return -ETIMEDOUT;
	}

	/* error */
	if (id->err_status) {
		return 0;
	}

	/* fill data to register */
	len = msg->len;

	while (len > 0) {
		reg = hc18xx_readreg(HC18XX_I2C_RD0_OFFSET + (it << 2));
		pr_debug("rd_data = %x\n", reg);

		for (count = 0; count < len; ++count) {
			*(msg->buf + count + (it << 2)) = reg & 0xff;
			reg = reg >> 8;

			if (count >= 3) {
				break;
			}
		}

		len -= 4;
		++it;
	}

	return 0;
}

static int hc18xx_i2c_process_msg(struct hc18xx_i2c *id, struct i2c_msg *msg)
{
	int ret = 0;
	id->p_msg = msg;
	id->err_status = 0;
	/* init waitqueue tasks */
	reinit_completion(&id->xfer_done);

	/* wrong input messages */
	if (msg->flags & I2C_M_TEN) {
		return -EINVAL;
	}

	/* read or write i2c */
	if (msg->flags & I2C_M_RD) {
		ret = hc18xx_i2c_mrecv(id);
	} else {
		ret = hc18xx_i2c_msend(id);
	}

	return ret;
}

static int hc18xx_i2c_master_xfer(struct i2c_adapter *adap, struct i2c_msg *msgs, int num)
{
	int ret, count;
	struct hc18xx_i2c *id = adap->algo_data;

	/* pr_info("hc18xx_i2c_master_xfer, num msgs %d addr %x len %x flags %x.\n",
	    num, msgs->addr, msgs->len, msgs->flags); */

	/* check if bus is not busy */
	if (down_interruptible(&id->sem)) {
		return -ERESTARTSYS;
	}

	if (hc18xx_readreg(HC18XX_I2C_SR_OFFSET) & HC18XX_I2C_SR_BUSY) {
		up(&id->sem);
		return -EAGAIN;
	}

	for (count = 0; count < num; ++count, ++msgs) {
		/* if timeout or invalid input */
		ret = hc18xx_i2c_process_msg(id, msgs);

		if (ret) {
			goto error;
		}

		/* check error status */
		if (id->err_status) {
			if (id->err_status & HC18XX_I2C_SR_ERRADDR) {
				ret = -ENXIO;
				goto error;
			}

			ret = -EIO;
			goto error;
		}
	}

	/* release lock */
	up(&id->sem);
	return num;
error:
	up(&id->sem);
	return ret;
}

static u32 hc18xx_i2c_func(struct i2c_adapter *adap)
{
	return I2C_FUNC_I2C;
}

static const struct i2c_algorithm hc18xx_i2c_algo = {
	.master_xfer = hc18xx_i2c_master_xfer,
	.functionality = hc18xx_i2c_func,
};

static int hc18xx_i2c_probe(struct platform_device *pdev)
{
	struct resource *r_mem;
	struct hc18xx_i2c *id;
#ifdef CONFIG_PLAT_HC18XX
#else
	const char *clk_name;
	const char *rst_names;
#endif
	int ret;
	/* allocate memory */
	id = devm_kzalloc(&pdev->dev, sizeof(struct hc18xx_i2c), GFP_KERNEL);

	if (id == NULL) {
		return -ENOMEM;
	}

	/* set platform data */
	platform_set_drvdata(pdev, id);
	/* get memory region */
	r_mem = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	id->membase = devm_ioremap_resource(&pdev->dev, r_mem);

	if (IS_ERR(id->membase)) {
		return PTR_ERR(id->membase);
	}

	/* init semaphor */
	sema_init(&id->sem, 1);

	/* get source clock and read clock rate */
#ifdef CONFIG_PLAT_HC18XX
	id->sclk = devm_clk_get(&pdev->dev, "sys_clk");
#else
	ret = of_property_read_string(pdev->dev.of_node, "clock-names", &clk_name);
	if (ret < 0) {
		dev_err(&pdev->dev, "clock name not found.\n");
		return ret;
	}

	id->sclk = devm_clk_get(&pdev->dev, clk_name);
#endif
	if (IS_ERR(id->sclk)) {
		dev_err(&pdev->dev, "input clock not found.\n");
		return PTR_ERR(id->sclk);
	}

	id->sclk_rate = clk_get_rate(id->sclk);

	/* prepare clock (empty function - fixed clock)*/
	ret = clk_prepare_enable(id->sclk);
	if (ret) {
		dev_err(&pdev->dev, "unable to enable clock.\n");
		return ret;
	}

	/* read irq from device tree */
	id->irq = platform_get_irq(pdev, 0);
	ret = devm_request_irq(&pdev->dev, id->irq, hc18xx_i2c_isr, IRQF_SHARED, pdev->name, id);

	if (ret < 0) {
		dev_err(&pdev->dev, "request irq fail %d\n", id->irq);
		return -EINVAL;
	}

#ifdef CONFIG_PLAT_HC18XX
	/* read software reset */
	if (of_property_read_u32(pdev->dev.of_node, "software-reset", &id->sw_rst)) {
		dev_err(&pdev->dev, "request software-reset fail %d\n", id->sw_rst);
		return -EINVAL;
	}
#else
	if (of_property_read_string(pdev->dev.of_node, "reset-names", &rst_names)) {
		dev_err(&pdev->dev, "request reset-names fail\n");
		return -EINVAL;
	}
	id->rst = devm_reset_control_get(&pdev->dev, rst_names);
#endif

	/* setting i2c_adapter structure definition */
	id->adap.dev.of_node = pdev->dev.of_node;
	id->adap.algo = &hc18xx_i2c_algo;
	id->adap.timeout = HC18XX_I2C_TIMEOUT;
	id->adap.retries = 3;
	id->adap.algo_data = id;
	id->adap.dev.parent = &pdev->dev;
	init_completion(&id->xfer_done);
	snprintf(id->adap.name, sizeof(id->adap.name), "HC18xx I2C at %08lx", (unsigned long)r_mem->start);

	/* set clock divsion */
	if (of_property_read_u32(pdev->dev.of_node, "clock-frequency", &id->i2c_rate)) {
		id->i2c_rate = HC18XX_I2C_DEFAULT_RATE;
	}

	/* read clock delay */
	if (of_property_read_u32(pdev->dev.of_node, "clock-delay", &id->delay_cycle)) {
		id->delay_cycle = HC18XX_I2C_DEFAULT_DELAY_CYCLE;
	}

	/* software reset */
#ifdef CONFIG_PLAT_HC18XX
	hc18xx_sw_rst(id->sw_rst);
#else
	reset_control_reset(id->rst);
#endif

	/* register reset */
	hc18xx_i2c_reset(id);

	/* register adapter */
	ret = i2c_add_adapter(&id->adap);

	if (ret < 0) {
		dev_err(&pdev->dev, "register adaptor failed: %d\n", ret);
		return ret;
	}

	pr_info("hc18xx-i2c: probing %s, <0x%x 0x%x>, irq %d.\n", pdev->name, r_mem->start, r_mem->end - r_mem->start,
	        id->irq);
	return 0;
}

static int hc18xx_i2c_remove(struct platform_device *pdev)
{
	struct hc18xx_i2c *id = platform_get_drvdata(pdev);
	i2c_del_adapter(&id->adap);
	platform_set_drvdata(pdev, NULL);
	/* unprepare close (empty function - fixed clock) */
	clk_disable_unprepare(id->sclk);
	pr_info("hc18xx-i2c: %s removed.\n", pdev->name);
	return 0;
}

static const struct of_device_id hc18xx_i2c_of_match[] = {
	{
	        .compatible = "augentix,i2c",
	},
	{},
};
MODULE_DEVICE_TABLE(of, hc18xx_i2c_of_match);

static struct platform_driver hc18xx_i2c_drv = {
	.driver =
	        {
	                .name = DRIVER_NAME,
	                .of_match_table = hc18xx_i2c_of_match,
	        },
	.probe = hc18xx_i2c_probe,
	.remove = hc18xx_i2c_remove,
};

module_platform_driver(hc18xx_i2c_drv);

MODULE_AUTHOR("Rowan Lin <rowan.lin@augentix.com");
MODULE_DESCRIPTION("HC18xx I2C bus driver");
MODULE_LICENSE("GPL");
