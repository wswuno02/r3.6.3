***********************************
Augentix EMAC driver settings     *
***********************************
1. codebase path 
   hc18xx_bsp/linux-linaro/drivers/net/ethernet/augentix

2. Check menuconfig->Device Drivers->Network device support->Ethernet driver support
   ->Augentix Device-> set "M"

3. To return linux-linaro folder, command "make modules". It will generate "synopGMAC_ether.ko"

4. In hc18xx_bsp/linux-linaro/drivers/net/ethernet/augentix, copy "synopGMAC_ether.ko" and 
   "emac_rmii_init.sh" to hc18xx_bsp/rootfs/root/driver. To return roofts and command "make".

5. To Load firmware to EVB board and boot-up linux system.

6. To enter /root/driver. command "sh emac_rmii_init.sh" -> "insmod synopGMAC_ether.ko"->
   "ifconfig eth0 192.168.1.10 up". You can use ping, ftpget and ftpput to test connecting.