/* ===================================================================================
 * Copyright (c) <2009> Synopsys, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software annotated with this license and associated documentation files
 * (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * =================================================================================== */


/**\file
 * This file encapsulates all the PCI dependent initialization and resource allocation
 * on Linux
 */
#include "synopGMAC_platform_dev.h"

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/of.h>
#include <linux/of_irq.h>
#include <linux/of_net.h>
#include <linux/dma-mapping.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/platform_device.h>
#include <linux/clk.h>
#include <linux/gpio.h>

#include "synopGMAC_Host.h"
#include "synopGMAC_network_interface.h"

#define DRIVER_NAME "synopGMAC"

#define GPIO_RST 9
#define DEFAULT_FREQ 125000000
#define EMACC_MODE_OFFS 0
#define EMACCLK_SEL_OFFS 0x00
#define EMACCLK_EN_OFFS  0x04
#define EMACCLK_DIV_OFFS 0x08

enum emac_mode {
	MII   = 0,
	RGMII = 1,
	RMII  = 4,
	SMII  = 7
};

static struct mode_param {
	const char *name;
	enum emac_mode md;
} emac_params[] = {
	{ "mii", MII },
	{ "rgmii", RGMII },
	{ "rmii", RMII },
	{ "smii", SMII}
};

/* HC18xx driver data */
struct hc18xx_emac_drvdata {
	void __iomem      *emacc_base;
	void __iomem      *emacclk_base;
	struct clk        *emac_clk;
	struct device     *dev;
};

static struct hc18xx_emac_drvdata *hcemac;

extern int hc18xx_set_clk_parent_by_name(struct clk *clk, const char *parent_name);

/* Device-tree init of hc18xx ethernet driver */
static int hc18xx_emac_dt_init(struct platform_device *pdev)
{
	struct resource *r_emacc, *r_emacclk;
	const char *mode_str;
	int rc = 0, i;
	u32 clock_frequency, mode;

	hcemac = devm_kzalloc(&pdev->dev, sizeof(struct hc18xx_emac_drvdata), GFP_KERNEL);
	if (IS_ERR(hcemac))
	{
		dev_err(&pdev->dev, "fail to allocate memory for hc18xx emac driver data\n");
		return -ENOMEM;
	}
	hcemac->dev = &pdev->dev;

	r_emacc = platform_get_resource(pdev, IORESOURCE_MEM, 1);
	if (!r_emacc) {
		dev_err(&pdev->dev, "no IO resource defined for EMACC\n");
		return -ENXIO;
	}
	hcemac->emacc_base = devm_ioremap_resource(&pdev->dev, r_emacc);
	if (IS_ERR(hcemac->emacc_base))
	{
		rc = PTR_ERR(hcemac->emacc_base);
		return rc;
	}

	r_emacclk = platform_get_resource(pdev, IORESOURCE_MEM, 2);
	if (!r_emacclk) {
		dev_err(&pdev->dev, "no IO resource defined for EMACC\n");
		return -ENXIO;
	}
	hcemac->emacclk_base = devm_ioremap_resource(&pdev->dev, r_emacclk);
	if (IS_ERR(hcemac->emacclk_base))
	{
		rc = PTR_ERR(hcemac->emacclk_base);
		return rc;
	}
	pr_info("Mapped resource: addr = 0x%08x - 0x%08x\n",
			r_emacclk->start, r_emacclk->end);

	/* GPIO reset assertion */
	rc = gpio_is_valid(GPIO_RST);
	if (!rc)
	{
		dev_err(&pdev->dev, "reset pin GPIO %d is invalid\n", GPIO_RST);
		return -EINVAL;
	}
	rc = gpio_request(GPIO_RST, "gmac_rst");
	if (rc)
	{
		dev_err(&pdev->dev, "fail to reset pin GPIO %d\n", GPIO_RST);
		return rc;
	}
	gpio_direction_output(GPIO_RST, 1);
	gpio_set_value(GPIO_RST, 0);
	gpio_set_value(GPIO_RST, 1);

	/* clock setup */
	hcemac->emac_clk = devm_clk_get(&pdev->dev, NULL);
	if (IS_ERR(hcemac->emac_clk))
	{
		dev_err(&pdev->dev, "fail to get clock resource\n");
		return -ENODEV;
	}
	clk_prepare_enable(hcemac->emac_clk);

	hc18xx_set_clk_parent_by_name(hcemac->emac_clk, "spll_out1");
	if (of_property_read_u32(pdev->dev.of_node, "clock-frequency", &clock_frequency))
		clock_frequency = DEFAULT_FREQ;
	clk_set_rate(hcemac->emac_clk, clock_frequency);

	/* EMAC mode selection; Use RMII as default mode */
	mode = MII;
	rc = of_property_read_string(pdev->dev.of_node, "emac-mode", &mode_str);
	if (!rc)
	{
		for (i = 0; i < ARRAY_SIZE(emac_params); i++)
		{
			if (!strcmp(emac_params[i].name, mode_str))
			{
				mode = emac_params[i].md;
				dev_info(&pdev->dev, "EMAC mode set to \"%s\"\n",
						emac_params[i].name);
				break;
			}
		}
	}
	writeb_relaxed(mode, hcemac->emacc_base + EMACC_MODE_OFFS);

	/* EMAC tx/rx clock settings */
	switch (mode)
	{
	case MII:
		break;

	case RGMII:
		writel_relaxed(0x00000003,
			       hcemac->emacclk_base + EMACCLK_SEL_OFFS);
		writel_relaxed(0x01010101,
			       hcemac->emacclk_base + EMACCLK_EN_OFFS);
		break;

	case RMII:
		writel_relaxed(0x00000101,
			       hcemac->emacclk_base + EMACCLK_SEL_OFFS);
		writel_relaxed(0x01010100,
			       hcemac->emacclk_base + EMACCLK_EN_OFFS);
		writeb_relaxed(0x09,
			       hcemac->emacclk_base + EMACCLK_DIV_OFFS);
		break;
	case SMII:	
		break;
	default:
		dev_err(&pdev->dev, "Invalide EMAC mode\n");
		return -EINVAL;
		break;
	}

#if EMAC_DBGMSG_INIT
	// 0x80010054
	TR0("addr=%X, %08X\n", (u32)(r_emacclk->start+EMACCLK_SEL_OFFS), 
			readl_relaxed((void *)hcemac->emacclk_base + EMACCLK_SEL_OFFS));
	TR0("addr=%X, %08X\n", (u32)(r_emacclk->start+EMACCLK_EN_OFFS), 
			readl_relaxed((void *)hcemac->emacclk_base + EMACCLK_EN_OFFS));
	TR0("addr=%X, %08X\n", (u32)(r_emacclk->start+EMACCLK_DIV_OFFS), 
			readl_relaxed((void *)hcemac->emacclk_base + EMACCLK_DIV_OFFS));
	// 0x805E0000
	TR0("addr=%X, %08X\n", (u32)(r_emacc->start+EMACC_MODE_OFFS), 
			readl_relaxed((void *)hcemac->emacc_base + EMACC_MODE_OFFS));	
#endif
	return 0;
}

/**
 * probe function of Linux platform driver.
 *     - Ioremap the memory
 *    - lock the memory for the device
 * \return Returns 0 on success and Error code on failure.
 */
static int synopGMAC_probe(struct platform_device *pdev)
{
	struct resource *r_mem = NULL;
	struct resource *r_irq = NULL;
	struct net_device *ndev;
	synopGMACNetworkAdapter *adap;
	synopGMACdevice         *gmacdev;
	const void *mac_address;
	const char *mode_str;
	u32 clock_frequency;
	u32 rc = 0;

	/* check hw resources */
	r_mem = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	r_irq = platform_get_resource(pdev, IORESOURCE_IRQ, 0);

	if (!r_mem || !r_irq) {
		dev_err(&pdev->dev, "no IO resource defined\n");
		return -ENXIO;
	}

	/* allocate memory */
	ndev = alloc_etherdev(sizeof(*adap));

	if (!ndev) {
		dev_err(&pdev->dev, "ethernet allocation failed\n");
		return -ESYNOPGMACNOMEM;
	}

	SET_NETDEV_DEV(ndev, &pdev->dev);

	adap = netdev_priv(ndev);
	adap->synopGMACpltdev = pdev;
	adap->synopGMACnetdev = ndev;

	/* ioremap mem region */
	adap->base = devm_ioremap_resource(&pdev->dev, r_mem);

	if (IS_ERR(adap->base)) {
		rc = PTR_ERR(adap->base);
		goto err_out_free_netdev;
	}

	if (devm_request_mem_region(
	            &pdev->dev,
	            r_mem->start,
	            resource_size(r_mem),
	            pdev->name)) {
		rc = -EBUSY;
		dev_err(&pdev->dev, "fail to request mem region\n");
		goto err_out_free_netdev;
	}

	/* fill netdev some private data */
	ndev->irq = irq_of_parse_and_map(pdev->dev.of_node, 0);
	ndev->base_addr = r_mem->start;

	rc = hc18xx_emac_dt_init(pdev);
	if (rc)
	{
		dev_err(&pdev->dev, "fail to initialize hc18xx ethernet driver\n");
		goto err_out_free_netdev;
	}

	// dbg message
	//ndev->features |= NETIF_F_HW_CSUM;
	if (0) {
		//ndev->features |= NETIF_F_HW_CSUM;
		ndev->features |= NETIF_F_SG | NETIF_F_GSO | NETIF_F_HW_CSUM; // upgrade speed max
		//ndev->features |= NETIF_F_SG | NETIF_F_GSO | NETIF_F_HW_CSUM | NETIF_F_FRAGLIST;
		//ndev->features |= NETIF_F_SG | NETIF_F_GSO | NETIF_F_HW_CSUM | NETIF_F_HIGHDMA ;
		//ndev->features |= NETIF_F_HW_CSUM | NETIF_F_SOFT_FEATURES | NETIF_F_SG;
		//ndev->features |= NETIF_F_GSO_SOFTWARE | NETIF_F_SG;
		//ndev->wanted_features |= NETIF_F_ALL_CSUM;
		TR0("1. features: %d, %d, %d, %d\n",  (int)NETIF_F_IP_CSUM,  (int)NETIF_F_IPV6_CSUM, (int)NETIF_F_HW_CSUM, (int)ndev->features);
		TR0("hw_features: %d\n", (int)ndev->hw_features);
		TR0("wanted_features: %d\n", (int)ndev->wanted_features);
		TR0("vlan_features: %d\n", (int)ndev->vlan_features);
		TR0("hw_enc_features: %d\n", (int)ndev->hw_enc_features);
		TR0("mpls_features: %d\n", (int)ndev->mpls_features);
	}

	/* set driver data */
	platform_set_drvdata(pdev, ndev);

	//TR0("phyaddr = %08x, remapaddr = %08x, irq = %d\n",
	//    r_mem->start, (u32)adap->base, ndev->irq);

	/* init network interface */
	rc = synopGMAC_init_network_interface(ndev);
	gmacdev = adap->synopGMACdev;

	// sw_reset
	if (gmacdev->Protocol == GMAC_RGMII || gmacdev->Protocol == GMAC_RMII) {
		void __iomem *base, *emac_config;
		//synopGMACNetworkAdapter *adapter = netdev_priv(ndev);
		base = devm_ioremap(&pdev->dev, 0x80010000, 0x100);
		emac_config = devm_ioremap(&pdev->dev, 0x805E0000, 0x100);
		plat_delay(1000);
		// set rgmii
		//synopGMACSetBits((u32*)emac_config, 0x00000000, 0x00000001);
		//TR0("0x80010040:%08X \n", synopGMACReadReg((u32*)emac_config, 0x00000000));
		//plat_delay(10000);
		
		// set SW_RST_EMAC = 1
		synopGMACSetBits((u32*)base, 0x00000040, 0x00000100);
		//TR0("0x80010040:%08X \n", synopGMACReadReg((u32*)base, 0x00000040));
		plat_delay(1000);

		//set EMAC_TXI_CKEN = 0
		synopGMACWriteReg((u32*)base, 0x00000058, 0x00000000);
		//TR0("0x80010058:%08X \n", synopGMACReadReg((u32*)base, 0x00000050));

		//set CKEN_EMAC = 0
		synopGMACClearBits((u32*)base, 0x00000010, 0x00000400);
		//TR0("0x80010010:%08X \n", synopGMACReadReg((u32*)base, 0x00000010));
		plat_delay(1000);

		//set SW_RST_EMAC = 0
		synopGMACClearBits((u32*)base, 0x00000040, 0x00000100);
		//TR0("0x80010040:%08X \n", synopGMACReadReg((u32*)base, 0x00000040));		
		//synopGMAC_reset(adapter->synopGMACdev);
		plat_delay(1000);

		//set CKEN_EMAC = 1
		synopGMACSetBits((u32*)base, 0x00000010, 0x00000400);
		//TR0("0x80010010:%08X \n", synopGMACReadReg((u32*)base, 0x00000010));
		//set EMAC_TXI_CKEN = 1
		synopGMACWriteReg((u32*)base, 0x00000058, 0x01010000);
		//TR0("0x80010058:%08X \n", synopGMACReadReg((u32*)base, 0x00000058));
		plat_delay(1000);
		TR0("SW_RST_EMAC\n");
		
	}

	if (rc) {
		dev_err(&pdev->dev, "fail to register network device\n");
		goto err_out_free_netdev;
	}

	/* mac address */
	mac_address = of_get_mac_address(pdev->dev.of_node);

	if (mac_address) {
		synopGMAC_set_mac_addr(adap->synopGMACdev, GmacAddr0High, GmacAddr0Low, 
				               (void *)mac_address);
		ether_addr_copy(ndev->dev_addr, mac_address);
		TR0("mac address = %02X:%02X:%02X:%02X:%02X:%02X\n", 
				*(ndev->dev_addr + 0),
				*(ndev->dev_addr + 1),
				*(ndev->dev_addr + 2),
				*(ndev->dev_addr + 3),
				*(ndev->dev_addr + 4),
				*(ndev->dev_addr + 5));
	}

	if (of_property_read_u32(pdev->dev.of_node, "clock-frequency", &clock_frequency))
		TR0("clock-frequency is null\n");
	else {
		if (clock_frequency == 125000000) 
			gmacdev->Speed = SPEED1000;
		else if (clock_frequency == 25000000) 
			gmacdev->Speed = SPEED100;
		else if (clock_frequency == 2500000) 
			gmacdev->Speed = SPEED10;
		else 
			TR0("clock is not 125/25/2.5MHz, clock=%d\n", clock_frequency);

		//TR0("clock=%d, speed=%d\n", clock_frequency, gmacdev->Speed);
	}	

	if (of_property_read_string(pdev->dev.of_node, "emac-mode", &mode_str))
		TR0("emac-mode is null\n");
	else {
		if (!strcmp("mii", mode_str))
			gmacdev->Protocol = GMAC_MII;
		else if (!strcmp("rmii", mode_str))
			gmacdev->Protocol = GMAC_RMII;
		else if (!strcmp("rgmii", mode_str))
			gmacdev->Protocol = GMAC_RGMII;
		else if (!strcmp("smii", mode_str))
			gmacdev->Protocol = GMAC_SMII;
		else 
			TR0("emac-mode is not supported %s\n", mode_str);
		//TR0("emac-mode=%s, protocol=%d\n", mode_str, gmacdev->Protocol);
	}

	gmacdev->div_base = devm_ioremap(&pdev->dev, EMAC_DIV_BASE, 0x10);

	return 0;

err_out_free_netdev:
	free_netdev(ndev);

	return rc;
}


/**
 * remove function of Linux platform driver.
 *
 *     - Releases the memory allocated by probe function
 *
 */
static int synopGMAC_remove(struct platform_device *pdev)
{
	struct net_device *ndev = platform_get_drvdata(pdev);
	synopGMAC_exit_network_interface(ndev);
	free_netdev(ndev);
	platform_set_drvdata(pdev, NULL);
	TR0("removed\n");
	return 0;
}


static struct of_device_id synopGMAC_of_match[] = {
	{ .compatible = "augentix,ethernet-MAC", },
	{ /* End of table */ }
};
MODULE_DEVICE_TABLE(of, synopGMAC_of_match);

static struct platform_driver synopGMAC_platform_driver = {
	.probe = synopGMAC_probe,
	.remove = synopGMAC_remove,
	.driver = {
		.name = DRIVER_NAME,
		.of_match_table = synopGMAC_of_match,
	},
};

module_platform_driver(synopGMAC_platform_driver);

MODULE_AUTHOR("Augenetix Inc,Rowan Lin");
MODULE_DESCRIPTION("SYNOPSYS GMAC PlATFORM DRIVER");
MODULE_LICENSE("GPL");

