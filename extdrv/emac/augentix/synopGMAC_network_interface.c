/* =======================================================================================
 * Copyright (c) <2009> Synopsys, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software annotated with this license and associated documentation files
 * (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * ==================================================================================== */


/** \file
 * This is the network dependent layer to handle network related functionality.
 * This file is tightly coupled to neworking frame work of linux 2.6.xx kernel.
 * The functionality carried out in this file should be treated as an example only
 * if the underlying operating system is not Linux.
 *
 * \note Many of the functions other than the device specific functions
 *  changes for operating system other than Linux 2.6.xx
 * \internal
 *-----------------------------REVISION HISTORY-----------------------------------
 * Synopsys            01/Aug/2007                Created
 * Augenetix           05/Nov/2015                Revision
 */


#include "synopGMAC_network_interface.h"

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/dma-mapping.h>
#include <linux/init.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>

#include <linux/socket.h>

#include "synopGMAC_Host.h"
#include "synopGMAC_plat.h"
#include "synopGMAC_Dev.h"

#define IOCTL_READ_REGISTER  SIOCDEVPRIVATE + 1
#define IOCTL_WRITE_REGISTER SIOCDEVPRIVATE + 2
#define IOCTL_READ_IPSTRUCT  SIOCDEVPRIVATE + 3
#define IOCTL_READ_RXDESC    SIOCDEVPRIVATE + 4
#define IOCTL_READ_TXDESC    SIOCDEVPRIVATE + 5
#define IOCTL_POWER_DOWN     SIOCDEVPRIVATE + 6

// static struct timer_list synopGMAC_cable_unplug_timer;
// This global variable is used to indicate the ISR whether the interrupts occured in the
// process of powering down the mac or not
static u32 GMAC_Power_down;

/* Sample Wake-up frame filter configurations
 * The synopGMAC_wakeup_filter_config3[] is a sample configuration for wake up filter.
 * Filter1 is used here
 * Filter1 offset is programmed to 50 (0x32)
 * Filter1 mask is set to 0x000000FF, indicating First 8 bytes are used by the filter
 * Filter1 CRC= 0x7EED this is the CRC computed on data 0x55 0x55 0x55 0x55 0x55 0x55
 * 0x55 0x55
 *
 * Refer accompanied software DWC_gmac_crc_example.c for CRC16 generation and how to use
 * the same
*/

u32 synopGMAC_wakeup_filter_config3[] = {
	0x00000000, // For Filter0 CRC is not computed may be it is 0x0000
	0x000000FF, // For Filter1 CRC is computed on 0,1,2,3,4,5,6,7 bytes from offset
	0x00000000, // For Filter2 CRC is not computed may be it is 0x0000
	0x00000000, // For Filter3 CRC is not computed may be it is 0x0000
	0x00000100, // Filter 0,2,3 are disabled, Filter 1 is enabled and filtering applies
	            // to only unicast packets
	0x00003200, // Filter 0,2,3 (no significance), filter 1 offset is 50 bytes from start
	            // of Destination MAC address
	0x7eED0000, // No significance of CRC for Filter0, Filter1 CRC is 0x7EED,
	0x00000000  // No significance of CRC for Filter2 and Filter3
};

//extern int hc_trigger_gpio(int pin_id, int level);

static void synopGMAC_linux_powerdown_mac(synopGMACdevice *gmacdev)
{
	TR0("Put the GMAC to power down mode..\n");

	// Disable the Dma engines in tx path
	// Let ISR know that Mac is going to be in the power down mode
	GMAC_Power_down = 1;
	synopGMAC_disable_dma_tx(gmacdev);
	plat_delay(10000);   //allow any pending transmission to complete

	// Disable the Mac for both tx and rx
	synopGMAC_tx_disable(gmacdev);
	synopGMAC_rx_disable(gmacdev);
	plat_delay(10000);   //Allow any pending buffer to be read by host

	// Disable the Dma in rx path
	synopGMAC_disable_dma_rx(gmacdev);

	// Enable the power down mode
	// synopGMAC_pmt_unicast_enable(gmacdev);

	// Prepare the gmac for magic packet reception and wake up frame reception
	synopGMAC_magic_packet_enable(gmacdev);
	synopGMAC_write_wakeup_frame_register(gmacdev, synopGMAC_wakeup_filter_config3);
	synopGMAC_wakeup_frame_enable(gmacdev);

	// Gate the application and transmit clock inputs to the code. This is not done
	// in this driver :).
	// Enable the Mac for reception
	synopGMAC_rx_enable(gmacdev);

	// Enable the assertion of PMT interrupt
	synopGMAC_pmt_int_enable(gmacdev);

	// Enter the power down mode
	synopGMAC_power_down_enable(gmacdev);
	return;
}

static void synopGMAC_linux_powerup_mac(synopGMACdevice *gmacdev)
{
	GMAC_Power_down = 0;    // Let ISR know that MAC is out of power down now

	if (synopGMAC_is_magic_packet_received(gmacdev)) {
		TR("GMAC wokeup due to Magic Pkt Received\n");
	}

	if (synopGMAC_is_wakeup_frame_received(gmacdev)) {
		TR("GMAC wokeup due to Wakeup Frame Received\n");
	}

	// Disable the assertion of PMT interrupt
	synopGMAC_pmt_int_disable(gmacdev);
	// Enable the mac and Dma rx and tx paths
	synopGMAC_rx_enable(gmacdev);
	synopGMAC_enable_dma_rx(gmacdev);

	synopGMAC_tx_enable(gmacdev);
	synopGMAC_enable_dma_tx(gmacdev);
	return;
}

/**
 * This sets up the transmit Descriptor queue in ring or chain mode.
 * This function is tightly coupled to the platform and operating system
 * Device is interested only after the descriptors are setup. Therefore this function
 * is not included in the device driver API. This function should be treated as an
 * example code to design the descriptor structures for ring mode or chain mode.
 * This function depends on the pltdev structure for allocation consistent dma-able
 * memory in case of linux.
 * This limitation is due to the fact that linux uses pci structure to allocate a dmable
 * memory
 *    - Allocates the memory for the descriptors.
 *    - Initialize the Busy and Next descriptors indices to 0.
 *    - Initialize the Busy and Next descriptors to first descriptor address.
 *     - Initialize the last descriptor with the endof ring in case of ring mode.
 *    - Initialize the descriptors in chain mode.
 * @param[in] pointer to synopGMACdevice.
 * @param[in] pointer to platform_device structure.
 * @param[in] number of descriptor expected in tx descriptor queue.
 * @param[in] whether descriptors to be created in RING mode or CHAIN mode.
 * \return 0 upon success. Error code upon failure.
 * \note This function fails if allocation fails for required number of descriptors in
 * Ring mode, but in chain mode
 * function returns -ESYNOPGMACNOMEM in the process of descriptor chain creation. once
 * returned from this function
 * user should for gmacdev->TxDescCount to see how many descriptors are there in the
 * chain. Should continue further
 * only if the number of descriptors in the chain meets the requirements
 */

s32 synopGMAC_setup_tx_desc_queue(synopGMACdevice *gmacdev,
                                  struct platform_device *pltdev, u32 no_of_desc,
								  u32 desc_mode)
{
	s32 i;

	DmaDesc *first_desc = NULL;
	DmaDesc *second_desc = NULL;
	dma_addr_t dma_addr;
	gmacdev->TxDescCount = 0;

	if (desc_mode == RINGMODE) {
		TR("Total size of memory required for Tx Descriptors in Ring Mode = 0x%08x\n",
		   ((sizeof(DmaDesc) * no_of_desc)));
		first_desc = plat_alloc_consistent_dmaable_memory(pltdev,
		             sizeof(DmaDesc) * no_of_desc, &dma_addr);

		if (first_desc == NULL) {
			TR("Error in Tx Descriptors memory allocation\n");
			return -ESYNOPGMACNOMEM;
		}

		gmacdev->TxDescCount = no_of_desc;
		gmacdev->TxDesc      = first_desc;
		gmacdev->TxDescDma   = dma_addr;

		for (i = 0; i < gmacdev -> TxDescCount; i++) {
			synopGMAC_tx_desc_init_ring(gmacdev->TxDesc + i,
			                            i == (gmacdev->TxDescCount - 1));
			TR("%02d %08x %08x\n", i, (u32)(gmacdev->TxDesc + i),
			   (u32)((DmaDesc *)gmacdev->TxDescDma + i));
		}
	} else {
		//Allocate the head descriptor
		first_desc = plat_alloc_consistent_dmaable_memory(pltdev, sizeof(DmaDesc),
		             &dma_addr);

		if (first_desc == NULL) {
			TR("Error in Tx Descriptor Memory allocation in Ring mode\n");
			return -ESYNOPGMACNOMEM;
		}

		gmacdev->TxDesc       = first_desc;
		gmacdev->TxDescDma    = dma_addr;

		TR("Tx===================================================================Tx\n");
		first_desc->buffer2   = gmacdev->TxDescDma;
		first_desc->data2     = (u32)gmacdev->TxDesc;

		gmacdev->TxDescCount = 1;

		for (i = 0; i < (no_of_desc - 1); i++) {
			second_desc = plat_alloc_consistent_dmaable_memory(pltdev, sizeof(DmaDesc),
			              &dma_addr);

			if (second_desc == NULL) {
				TR("Error in Tx Descriptor Memory allocation in Chain mode\n");
				return -ESYNOPGMACNOMEM;
			}

			first_desc->buffer2  = dma_addr;
			first_desc->data2    = (u32)second_desc;

			second_desc->buffer2 = gmacdev->TxDescDma;
			second_desc->data2   = (u32)gmacdev->TxDesc;

			synopGMAC_tx_desc_init_chain(first_desc);
			TR("%02d %08x %08x %08x %08x %08x %08x %08x \n", gmacdev->TxDescCount,
			   (u32)first_desc, first_desc->status,
			   first_desc->length, first_desc->buffer1, first_desc->buffer2,
			   first_desc->data1,
			   first_desc->data2);
			gmacdev->TxDescCount += 1;
			first_desc = second_desc;
		}

		synopGMAC_tx_desc_init_chain(first_desc);
		TR("%02d %08x %08x %08x %08x %08x %08x %08x \n", gmacdev->TxDescCount,
		   (u32)first_desc, first_desc->status,
		   first_desc->length, first_desc->buffer1, first_desc->buffer2,
		   first_desc->data1,
		   first_desc->data2);
		TR("Tx===================================================================Tx\n");
	}

	gmacdev->TxNext = 0;
	gmacdev->TxBusy = 0;
	gmacdev->TxNextDesc = gmacdev->TxDesc;
	gmacdev->TxBusyDesc = gmacdev->TxDesc;
	gmacdev->BusyTxDesc  = 0;

	return -ESYNOPGMACNOERR;
}


/**
 * This sets up the receive Descriptor queue in ring or chain mode.
 * This function is tightly coupled to the platform and operating system
 * Device is interested only after the descriptors are setup. Therefore this function
 * is not included in the device driver API. This function should be treated as an
 * example code to design the descriptor structures in ring mode or chain mode.
 * This function depends on the pltdev structure for allocation of consistent
 * dma-able memory in case of linux.
 * This limitation is due to the fact that linux uses pci structure to allocate a
 * dmable memory
 *    - Allocates the memory for the descriptors.
 *    - Initialize the Busy and Next descriptors indices to 0.
 *    - Initialize the Busy and Next descriptors to first descriptor address.
 *    - Initialize the last descriptor with the endof ring in case of ring mode.
 *    - Initialize the descriptors in chain mode.
 * @param[in] pointer to synopGMACdevice.
 * @param[in] pointer to pci_device structure.
 * @param[in] number of descriptor expected in rx descriptor queue.
 * @param[in] whether descriptors to be created in RING mode or CHAIN mode.
 * \return 0 upon success. Error code upon failure.
 * \note This function fails if allocation fails for required number of descriptors
 * in Ring mode, but in chain mode
 * function returns -ESYNOPGMACNOMEM in the process of descriptor chain creation.
 * once returned from this function
 * user should for gmacdev->RxDescCount to see how many descriptors are there in i
 * the chain. Should continue further
 * only if the number of descriptors in the chain meets the requirements
 */
s32 synopGMAC_setup_rx_desc_queue(synopGMACdevice *gmacdev,
                                  struct platform_device *pltdev, u32 no_of_desc,
								  u32 desc_mode)
{
	s32 i;
	DmaDesc *first_desc = NULL;
	DmaDesc *second_desc = NULL;
	dma_addr_t dma_addr;
	gmacdev->RxDescCount = 0;

	if (desc_mode == RINGMODE) {
		TR("total size of memory required for Rx Descriptors in Ring Mode = 0x%08x\n",
		   ((sizeof(DmaDesc) * no_of_desc)));
		first_desc = plat_alloc_consistent_dmaable_memory(pltdev,
		             sizeof(DmaDesc) * no_of_desc, &dma_addr);

		if (first_desc == NULL) {
			TR("Error in Rx Descriptor Memory allocation in Ring mode\n");
			return -ESYNOPGMACNOMEM;
		}

		gmacdev->RxDescCount = no_of_desc;
		gmacdev->RxDesc      = first_desc;
		gmacdev->RxDescDma   = dma_addr;

		for (i = 0; i < gmacdev -> RxDescCount; i++) {
			synopGMAC_rx_desc_init_ring(gmacdev->RxDesc + i,
			                            i == (gmacdev->RxDescCount - 1));
			TR("%02d %08x %08x\n", i, (u32)(gmacdev->RxDesc + i),
			   (u32)((DmaDesc *)gmacdev->RxDescDma + i));
		}
	} else {
		//Allocate the head descriptor
		first_desc = plat_alloc_consistent_dmaable_memory(pltdev, sizeof(DmaDesc),
		             &dma_addr);

		if (first_desc == NULL) {
			TR("Error in Rx Descriptor Memory allocation in Ring mode\n");
			return -ESYNOPGMACNOMEM;
		}

		gmacdev->RxDesc       = first_desc;
		gmacdev->RxDescDma    = dma_addr;

		TR("Rx===================================================================Rx\n");
		first_desc->buffer2   = gmacdev->RxDescDma;
		first_desc->data2     = (u32) gmacdev->RxDesc;

		gmacdev->RxDescCount = 1;

		for (i = 0; i < (no_of_desc - 1); i++) {
			second_desc = plat_alloc_consistent_dmaable_memory(pltdev, sizeof(DmaDesc),
			              &dma_addr);

			if (second_desc == NULL) {
				TR("Error in Rx Descriptor Memory allocation in Chain mode\n");
				return -ESYNOPGMACNOMEM;
			}

			first_desc->buffer2  = dma_addr;
			first_desc->data2    = (u32)second_desc;

			second_desc->buffer2 = gmacdev->RxDescDma;
			second_desc->data2   = (u32)gmacdev->RxDesc;

			synopGMAC_rx_desc_init_chain(first_desc);
			TR("%02d  %08x %08x %08x %08x %08x %08x %08x \n", gmacdev->RxDescCount,
			   (u32)first_desc,
			   first_desc->status, first_desc->length, first_desc->buffer1,
			   first_desc->buffer2, first_desc->data1, first_desc->data2);
			gmacdev->RxDescCount += 1;
			first_desc = second_desc;
		}

		synopGMAC_rx_desc_init_chain(first_desc);
		TR("%02d  %08x %08x %08x %08x %08x %08x %08x \n", gmacdev->RxDescCount,
		   (u32)first_desc,
		   first_desc->status, first_desc->length, first_desc->buffer1,
		   first_desc->buffer2, first_desc->data1, first_desc->data2);
		TR("Rx===================================================================Rx\n");
	}

	gmacdev->RxNext = 0;
	gmacdev->RxBusy = 0;
	gmacdev->RxNextDesc = gmacdev->RxDesc;
	gmacdev->RxBusyDesc = gmacdev->RxDesc;

	gmacdev->BusyRxDesc = 0;

	return -ESYNOPGMACNOERR;
}

/**
 * This gives up the receive Descriptor queue in ring or chain mode.
 * This function is tightly coupled to the platform and operating system
 * Once device's Dma is stopped the memory descriptor memory and the buffer memory
 * deallocation,
 * is completely handled by the operating system, this call is kept outside the device
 * driver Api.
 * This function should be treated as an example code to de-allocate the descriptor
 * structures in ring mode or chain mode
 * and network buffer deallocation.
 * This function depends on the pltdev structure for dma-able memory deallocation for
 * both descriptor memory and the
 * network buffer memory under linux.
 * The responsibility of this function is to
 *    - Free the network buffer memory if any.
 *    - Fee the memory allocated for the descriptors.
 * @param[in] pointer to synopGMACdevice.
 * @param[in] pointer to pci_device structure.
 * @param[in] number of descriptor expected in rx descriptor queue.
 * @param[in] whether descriptors to be created in RING mode or CHAIN mode.
 * \return 0 upon success. Error code upon failure.
 * \note No referece should be made to descriptors once this function is called. This
 * function is invoked when the device is closed.
 */
void synopGMAC_giveup_rx_desc_queue(synopGMACdevice *gmacdev,
                                    struct platform_device *pltdev, u32 desc_mode)
{
	s32 i;
	DmaDesc *first_desc = NULL;
	dma_addr_t first_desc_dma_addr;
	u32 status;
	dma_addr_t dma_addr1;
	dma_addr_t dma_addr2;
	u32 length1;
	u32 length2;
	u32 data1;
	u32 data2;

	if (desc_mode == RINGMODE) {
		for (i = 0; i < gmacdev -> RxDescCount; i++) {
			synopGMAC_get_desc_data(gmacdev->RxDesc + i, &status, &dma_addr1, &length1,
			                        &data1, &dma_addr2, &length2, &data2);

			if ((length1 != 0) && (data1 != 0)) {
				dma_unmap_single(&pltdev->dev, dma_addr1, 0, DMA_FROM_DEVICE);
				dev_kfree_skb((struct sk_buff *)data1);    // free buffer1
				TR("(Ring mode) rx buffer1 %08x of size %d from %d rx descriptor is given back\n",
				   data1, length1, i);
			}

			if ((length2 != 0) && (data2 != 0)) {
				dma_unmap_single(&pltdev->dev, dma_addr2, 0, DMA_FROM_DEVICE);
				dev_kfree_skb((struct sk_buff *) data2);    //free buffer2
				TR("(Ring mode) rx buffer2 %08x of size %d from %d rx descriptor is given back\n",
				   data2, length2, i);
			}
		}

		plat_free_consistent_dmaable_memory(pltdev,
		                                    sizeof(DmaDesc) * gmacdev->RxDescCount,
		                                    gmacdev->RxDesc, gmacdev->RxDescDma);
		TR("Memory allocated %08x  for Rx Desriptors (ring) is given back\n",
		   (u32)gmacdev->RxDesc);
	} else {
		TR("rx-------------------------------------------------------------------rx\n");
		first_desc          = gmacdev->RxDesc;
		first_desc_dma_addr = gmacdev->RxDescDma;

		for (i = 0; i < gmacdev -> RxDescCount; i++) {
			synopGMAC_get_desc_data(first_desc, &status, &dma_addr1, &length1, &data1,
			                        &dma_addr2, &length2, &data2);
			TR("%02d %08x %08x %08x %08x %08x %08x %08x\n", i, (u32)first_desc,
			   first_desc->status, first_desc->length,
			   first_desc->buffer1, first_desc->buffer2, first_desc->data1,
			   first_desc->data2);

			if ((length1 != 0) && (data1 != 0)) {
				dma_unmap_single(&pltdev->dev, dma_addr1, length1, DMA_FROM_DEVICE);
				dev_kfree_skb((struct sk_buff *) data1);    // free buffer1
				TR("(Chain mode) rx buffer1 %08x of size %d from %d rx descriptor is given back\n",
				   data1, length1, i);
			}

			plat_free_consistent_dmaable_memory(pltdev, sizeof(DmaDesc), first_desc,
			                                    first_desc_dma_addr); //free descriptors
			TR("Memory allocated %08x for Rx Descriptor (chain) at  %d is given back\n",
			   data2, i);

			first_desc = (DmaDesc *)data2;
			first_desc_dma_addr = dma_addr2;
		}

		TR("rx-------------------------------------------------------------------rx\n");
	}

	gmacdev->RxDesc    = NULL;
	gmacdev->RxDescDma = 0;
	return;
}

/**
 * This gives up the transmit Descriptor queue in ring or chain mode.
 * This function is tightly coupled to the platform and operating system
 * Once device's Dma is stopped the memory descriptor memory and the buffer memory
 * deallocation,
 * is completely handled by the operating system, this call is kept outside the device
 * driver Api.
 * This function should be treated as an example code to de-allocate the descriptor structures in ring mode or chain mode
 * and network buffer deallocation.
 * This function depends on the pltdev structure for dma-able memory deallocation for both descriptor memory and the
 * network buffer memory under linux.
 * The responsibility of this function is to
 *     - Free the network buffer memory if any.
 *    - Fee the memory allocated for the descriptors.
 * @param[in] pointer to synopGMACdevice.
 * @param[in] pointer to pci_device structure.
 * @param[in] number of descriptor expected in tx descriptor queue.
 * @param[in] whether descriptors to be created in RING mode or CHAIN mode.
 * \return 0 upon success. Error code upon failure.
 * \note No reference should be made to descriptors once this function is called. This function is invoked when the device is closed.
 */
void synopGMAC_giveup_tx_desc_queue(synopGMACdevice *gmacdev,
                                    struct platform_device *pltdev, u32 desc_mode)
{
	s32 i;

	DmaDesc *first_desc = NULL;
	dma_addr_t first_desc_dma_addr;
	u32 status;
	dma_addr_t dma_addr1;
	dma_addr_t dma_addr2;
	u32 length1;
	u32 length2;
	u32 data1;
	u32 data2;

	if (desc_mode == RINGMODE) {
		for (i = 0; i < gmacdev -> TxDescCount; i++) {
			synopGMAC_get_desc_data(gmacdev->TxDesc + i, &status, &dma_addr1, &length1,
			                        &data1, &dma_addr2, &length2, &data2);

			if ((length1 != 0) && (data1 != 0)) {
				dma_unmap_single(&pltdev->dev, dma_addr1, length1, DMA_TO_DEVICE);
				dev_kfree_skb((struct sk_buff *) data1);    // free buffer1
				TR("(Ring mode) tx buffer1 %08x of size %d from %d rx descriptor is given back\n",
				   data1, length1, i);
			}

			if ((length2 != 0) && (data2 != 0)) {
				dma_unmap_single(&pltdev->dev, dma_addr2, length2, DMA_TO_DEVICE);
				dev_kfree_skb((struct sk_buff *) data2);    //free buffer2
				TR("(Ring mode) tx buffer2 %08x of size %d from %d rx descriptor is given back\n",
				   data2, length2, i);
			}
		}

		plat_free_consistent_dmaable_memory(pltdev,
		                                    sizeof(DmaDesc) * gmacdev->TxDescCount, gmacdev->TxDesc, gmacdev->TxDescDma);
		TR("Memory allocated %08x for Tx Desriptors (ring) is given back\n",
		   (u32)gmacdev->TxDesc);
	} else {
		TR("tx-------------------------------------------------------------------tx\n");
		first_desc          = gmacdev->TxDesc;
		first_desc_dma_addr = gmacdev->TxDescDma;

		for (i = 0; i < gmacdev -> TxDescCount; i++) {
			synopGMAC_get_desc_data(first_desc, &status, &dma_addr1, &length1, &data1,
			                        &dma_addr2, &length2, &data2);
			TR("%02d %08x %08x %08x %08x %08x %08x %08x\n", i, (u32)first_desc,
			   first_desc->status, first_desc->length,
			   first_desc->buffer1, first_desc->buffer2, first_desc->data1, first_desc->data2);

			if ((length1 != 0) && (data1 != 0)) {
				dma_unmap_single(&pltdev->dev, dma_addr1, length1, DMA_TO_DEVICE);
				dev_kfree_skb((struct sk_buff *) data1);    // free buffer1
				TR("(Chain mode) tx buffer1 %08x of size %d from %d rx descriptor is given back\n",
				   data1, length1, i);
			}

			plat_free_consistent_dmaable_memory(pltdev, sizeof(DmaDesc), first_desc,
			                                    first_desc_dma_addr); //free descriptors
			TR("Memory allocated %08x for Tx Descriptor (chain) at  %d is given back\n",
			   data2, i);

			first_desc = (DmaDesc *)data2;
			first_desc_dma_addr = dma_addr2;
		}

		TR("tx-------------------------------------------------------------------tx\n");
	}

	gmacdev->TxDesc    = NULL;
	gmacdev->TxDescDma = 0;
	return;
}

/**
 * Function to handle housekeeping after a packet is transmitted over the wire.
 * After the transmission of a packet DMA generates corresponding interrupt
 * (if it is enabled). It takes care of returning the sk_buff to the linux
 * kernel, updating the networking statistics and tracking the descriptors.
 * @param[in] pointer to net_device structure.
 * \return void.
 * \note This function runs in interrupt context
 */
void synop_handle_transmit_over(struct net_device *netdev)
{
	synopGMACNetworkAdapter *adapter;
	synopGMACdevice *gmacdev;
	struct platform_device *pltdev;
	s32 desc_index;
	u32 data1, data2;
	u32 status;
	u32 length1, length2;
	u32 dma_addr1, dma_addr2;

	/* Extract private data */
	adapter = netdev_priv(netdev);

	if (adapter == NULL) {
		TR("Unknown Device\n");
		return;
	}

	gmacdev = adapter->synopGMACdev;

	if (gmacdev == NULL) {
		TR("GMAC device structure is missing\n");
		return;
	}

	pltdev = adapter->synopGMACpltdev;

	if (pltdev == NULL) {
		TR("Platform device structure is missing\n");
		return;
	}

	/* Handle the transmit Descriptors */
	do {

		desc_index = synopGMAC_get_tx_qptr(gmacdev, &status, &dma_addr1, &length1,
		                                   &data1, &dma_addr2, &length2, &data2);

		//TR0("desc_index, dma_addr1, data1: %08x, %08x, %08x, %08x, %08x\n", desc_index, dma_addr1, data1, status, length1);

		if (desc_index >= 0 && data1 != 0) {
			//TR("Finished Transmit at Tx Descriptor %d for skb 0x%08x and buffer = %08x whose status is %08x \n",
			//    desc_index,data1,dma_addr1,status);

			dma_unmap_single(&pltdev->dev, dma_addr1, 0, DMA_TO_DEVICE);
			dev_kfree_skb_irq((struct sk_buff *)data1);

			if (synopGMAC_is_desc_valid(status)) {
				adapter->synopGMACNetStats.tx_bytes += length1;
				adapter->synopGMACNetStats.tx_packets++;
			} else {
				TR("Error in Status %08x\n", status);
				adapter->synopGMACNetStats.tx_errors++;
				adapter->synopGMACNetStats.tx_aborted_errors += synopGMAC_is_tx_aborted(status);
				adapter->synopGMACNetStats.tx_carrier_errors += synopGMAC_is_tx_carrier_error(
				            status);
			}
		}

		adapter->synopGMACNetStats.collisions += synopGMAC_get_tx_collision_count(
		            status);

	} while (desc_index >= 0);

	/* Wake up transmit queue */
	netif_wake_queue(netdev);
	//hc_trigger_gpio(48, 0);
}


/**
 * Function to Receive a packet from the interface.
 * After Receiving a packet, DMA transfers the received packet to the system memory
 * and generates corresponding interrupt (if it is enabled). This function prepares
 * the sk_buff for received packet after removing the ethernet CRC, and hands it over
 * to linux networking stack.
 *     - Updataes the networking interface statistics
 *    - Keeps track of the rx descriptors
 * @param[in] pointer to net_device structure.
 * \return void.
 * \note This function runs in interrupt context.
 */

void synop_handle_received_data(struct net_device *netdev)
{
	synopGMACNetworkAdapter *adapter;
	synopGMACdevice *gmacdev;
	struct platform_device *pltdev;
	s32 desc_index;

	u32 data1;
	u32 data2;
	u32 len;
	u32 status;
	u32 dma_addr1;
	u32 dma_addr2;

	struct sk_buff *skb; //This is the pointer to hold the received data

	//TR("%s\n", __FUNCTION__);

	adapter = netdev_priv(netdev);

	if (adapter == NULL) {
		TR("Unknown Device\n");
		return;
	}

	gmacdev = adapter->synopGMACdev;

	if (gmacdev == NULL) {
		TR("GMAC device structure is missing\n");
		return;
	}

	pltdev = adapter->synopGMACpltdev;

	if (pltdev == NULL) {
		TR("GMAC platform device structure is missing\n");
		return;
	}

	/* Handle the Receive Descriptors */
	do {
		desc_index = synopGMAC_get_rx_qptr(gmacdev, &status, &dma_addr1, NULL, &data1,
		                                   &dma_addr2, NULL, &data2);

		if (desc_index >= 0 && data1 != 0) {
			//TR("Received Data at Rx Descriptor %d for skb 0x%08x whose status is %08x\n", desc_index,data1,status);

			/*At first step unmapped the dma address*/
			dma_unmap_single(&pltdev->dev, dma_addr1, RX_BUF_SIZE, DMA_FROM_DEVICE);
			skb = (struct sk_buff *)data1;

			if (1) { // (synopGMAC_is_rx_desc_valid(status))
				len =  synopGMAC_get_rx_desc_frame_length(status) - 4; //Not interested in Ethernet CRC bytes
				//TR("CRC value is %08X, %04X\n", (u32)*((u8*)(skb->data + len)), len);

				skb_put(skb, len);
				skb->dev = netdev;
				skb->protocol = eth_type_trans(skb, netdev);
				netif_rx(skb);

				netdev->last_rx = jiffies;
				adapter->synopGMACNetStats.rx_packets++;
				adapter->synopGMACNetStats.rx_bytes += len;
			} else {
				/* Now the present skb should be set free */
				dev_kfree_skb_irq(skb);
				TR0("Rx descriptor is not valid: %08x\n", status);
				adapter->synopGMACNetStats.rx_errors++;
				adapter->synopGMACNetStats.collisions       += synopGMAC_is_rx_frame_collision(
				            status);
				adapter->synopGMACNetStats.rx_crc_errors    += synopGMAC_is_rx_crc(status);
				adapter->synopGMACNetStats.rx_frame_errors  +=
				    synopGMAC_is_frame_dribbling_errors(status);
				adapter->synopGMACNetStats.rx_length_errors +=
				    synopGMAC_is_rx_frame_length_errors(status);
			}

			/* Now lets allocate the skb for the emptied descriptor */
			skb = dev_alloc_skb(RX_BUF_SIZE);

			if (skb == NULL) {
				TR("SKB memory allocation failed\n");
				adapter->synopGMACNetStats.rx_dropped++;
			}

			dma_addr1 = dma_map_single(&pltdev->dev, skb->data, RX_BUF_SIZE,
			                           DMA_FROM_DEVICE);
			desc_index = synopGMAC_set_rx_qptr(gmacdev, dma_addr1, RX_BUF_SIZE, (u32)skb, 0,
			                                   0, 0);

			if (desc_index < 0) {
				TR("Cannot set Rx Descriptor for skb %08x\n", (u32)skb);
				dev_kfree_skb_irq(skb);
			}
		}
	} while (desc_index >= 0);
}

/**
 * Interrupt service routing.
 * This is the function registered as ISR for device interrupts.
 * @param[in] interrupt number.
 * @param[in] void pointer to device unique structure (Required for shared interrupts in Linux).
 * @param[in] pointer to pt_regs (not used).
 * \return Returns IRQ_NONE if not device interrupts IRQ_HANDLED for device interrupts.
 * \note This function runs in interrupt context
 *
 */
static irqreturn_t synopGMAC_intr_handler(s32 intr_num, void *dev_id)
{
	/*Kernels passes the netdev structure in the dev_id. So grab it*/
	struct net_device *netdev;
	synopGMACNetworkAdapter *adapter;
	synopGMACdevice *gmacdev;
	struct platform_device *pltdev;
	u32 interrupt, dma_status_reg, phy_status;
	s32 status;
	u32 dma_addr;

	//TR0("%s called \n",__FUNCTION__);
	netdev = (struct net_device *)dev_id;

	if (netdev == NULL) {
		TR("Unknown Device\n");
		return -1;
	}

	adapter = netdev_priv(netdev);

	if (adapter == NULL) {
		TR("Adapter Structure Missing\n");
		return -1;
	}

	gmacdev = adapter->synopGMACdev;

	if (gmacdev == NULL) {
		TR("GMAC device structure Missing\n");
		return -1;
	}

	pltdev = adapter->synopGMACpltdev;

	/*Read the Dma interrupt status to know whether the interrupt got generated by our device or not*/
	dma_status_reg = synopGMACReadReg((u32 *)gmacdev->DmaBase, DmaStatus);

	if (dma_status_reg == 0) {
		return IRQ_NONE;
	}

	synopGMAC_disable_interrupt_all(gmacdev);

	//TR("%s:: Dma Status Reg: 0x%08x\n",__FUNCTION__,dma_status_reg);

	if (dma_status_reg & GmacPmtIntr) {
		TR("%s:: Interrupt due to PMT module\n", __FUNCTION__);
		synopGMAC_linux_powerup_mac(gmacdev);
	}

	if (dma_status_reg & GmacMmcIntr) {
		TR("%s:: Interrupt due to MMC module\n", __FUNCTION__);
		TR("%s:: synopGMAC_rx_int_status = %08x\n", __FUNCTION__,
		   synopGMAC_read_mmc_rx_int_status(gmacdev));
		TR("%s:: synopGMAC_tx_int_status = %08x\n", __FUNCTION__,
		   synopGMAC_read_mmc_tx_int_status(gmacdev));
	}

	if (dma_status_reg & GmacLineIntfIntr) {
		phy_status = synopGMACReadReg((u32 *)gmacdev->MacBase, GmacInterruptStatus);
		TR("%s:: Interrupt due to GMAC LINE module, interrupts %04x\n", __FUNCTION__,
		   phy_status);
		phy_status = synopGMACReadReg((u32 *)gmacdev->MacBase, 0xD8);

		if (phy_status & 0x08) {
			if (gmacdev->Protocol == GMAC_RGMII)
				TR0("%s:: RGMII status %04x, phy link-up\n", __FUNCTION__, phy_status);
			gmacdev->LinkState = LINKUP;

			/* detect link speed link type */
			if ((phy_status & 0x6) == 0x4) {
				gmacdev->Speed = SPEED1000;
			} else if ((phy_status & 0x6) == 0x2) {
				gmacdev->Speed = SPEED100;
			} else {
				gmacdev->Speed = SPEED10;
			}

			if (gmacdev->Speed == SPEED1000) {
				synopGMAC_select_gmii(gmacdev);
			} else {
				synopGMAC_select_mii(gmacdev);
			}

			/* duplex */
			if (phy_status & 0x1) {
				gmacdev->DuplexMode = FULLDUPLEX;
				synopGMAC_set_full_duplex(gmacdev);
			} else {
				gmacdev->DuplexMode = HALFDUPLEX;
				synopGMAC_set_half_duplex(gmacdev);
				TR0("%s:: Cannot support half duplex mode\n", __FUNCTION__);
			}

			if (gmacdev->Protocol == GMAC_RGMII) {
				if (gmacdev->Speed == SPEED1000) {
					writeb_relaxed(0x01, gmacdev->div_base);
					TR0("Change speed to 1000M.\n");
				}
				else if (gmacdev->Speed == SPEED100) {
					writeb_relaxed(0x05, gmacdev->div_base);
					synopGMACSetBits((u32 *)gmacdev->MacBase, GmacConfig, GmacFESpeed100);
					TR0("Change speed to 100M.\n");
				}
				else if (gmacdev->Speed == SPEED10) {
					synopGMACClearBits((u32 *)gmacdev->MacBase, GmacConfig, GmacFESpeed100);
					TR0("Change speed to 10M.\n");
				}
				else
					TR0("Error, change speed is wrong.\n");

			}




		} else {
			TR("%s:: RGMII status %04x, phy link-down\n", __FUNCTION__, phy_status);
			gmacdev->LinkState = LINKDOWN;
		}
	}

	/*Now lets handle the DMA interrupts*/
	interrupt = synopGMAC_get_interrupt_type(gmacdev);
	TR("%s:: Interrupts to be handled: 0x%08x\n", __FUNCTION__, interrupt);

	if (interrupt & synopGMACDmaError) {
		// after soft reset, configure the MAC address to default value
		// u8 mac_addr0[6] = DEFAULT_MAC_ADDRESS;
		TR0("%s:: Fatal Bus Error Inetrrupt Seen\n", __FUNCTION__);
		synopGMAC_disable_dma_tx(gmacdev);
		synopGMAC_disable_dma_rx(gmacdev);

		/* set descriptor ownership */
		synopGMAC_take_desc_ownership_tx(gmacdev);
		synopGMAC_take_desc_ownership_rx(gmacdev);

		/* init rx desc queue */
		synopGMAC_init_tx_rx_desc_queue(gmacdev);

		// reset the DMA engine and the GMAC ip
		synopGMAC_reset(gmacdev);

		// synopGMAC_set_mac_addr(gmacdev, GmacAddr0High, GmacAddr0Low, mac_addr0);
		synopGMAC_dma_bus_mode_init(gmacdev,
		                            DmaFixedBurstEnable | DmaBurstLength8 | DmaDescriptorSkip2);
		synopGMAC_dma_control_init(gmacdev, DmaStoreAndForward);
		synopGMAC_init_rx_desc_base(gmacdev);
		synopGMAC_init_tx_desc_base(gmacdev);
		synopGMAC_mac_init(gmacdev);
		synopGMAC_enable_dma_rx(gmacdev);
		synopGMAC_enable_dma_tx(gmacdev);
	}

	if (interrupt & synopGMACDmaRxNormal) {
		//TR("%s:: Rx Normal \n", __FUNCTION__);
		synop_handle_received_data(netdev);
	}

	if (interrupt & synopGMACDmaRxAbnormal) {
		TR("%s:: Abnormal Rx Interrupt Seen\n", __FUNCTION__);
#if 1

		if (GMAC_Power_down == 0) {
			// If Mac is not in powerdown
			adapter->synopGMACNetStats.rx_over_errors++;
			/*Now Descriptors have been created in synop_handle_received_data(). Just issue a poll demand to resume DMA operation*/
			synopGMAC_resume_dma_rx(gmacdev);//To handle GBPS with 12 descriptors
		}

#endif
	}

	if (interrupt & synopGMACDmaRxStopped) {
		TR("%s:: Receiver stopped seeing Rx interrupts\n",
		   __FUNCTION__); //Receiver gone in to stopped state
#if 1

		if (GMAC_Power_down == 0) {
			// If Mac is not in powerdown
			adapter->synopGMACNetStats.rx_over_errors++;

			do {
				struct sk_buff *skb = dev_alloc_skb(RX_BUF_SIZE);

				if (skb == NULL) {
					TR("%s:: ERROR in skb buffer allocation Better Luck Next time\n", __FUNCTION__);
					break;
				}

				dma_addr = dma_map_single(&pltdev->dev, skb->data, RX_BUF_SIZE,
				                          DMA_FROM_DEVICE);
				status = synopGMAC_set_rx_qptr(gmacdev, dma_addr, RX_BUF_SIZE, (u32)skb, 0, 0,
				                               0);
				TR("%s:: Set Rx Descriptor no %08x for skb %08x \n", __FUNCTION__, status,
				   (u32)skb);

				if (status < 0) {
					dev_kfree_skb_irq(
					    skb);    //changed from dev_free_skb. If problem check this again
				}

			} while (status >= 0);

			synopGMAC_enable_dma_rx(gmacdev);
		}

#endif
	}

	if (interrupt & synopGMACDmaTxNormal) {
		//xmit function has done its job
		//TR("%s:: Finished Normal Transmission \n",__FUNCTION__);
		synop_handle_transmit_over(netdev);
	}

	if (interrupt & synopGMACDmaTxAbnormal) {
		TR("%s:: Abnormal Tx Interrupt Seen\n", __FUNCTION__);
#if 1

		if (GMAC_Power_down == 0) {
			// If Mac is not in powerdown
			synop_handle_transmit_over(netdev);
		}

#endif
	}

	if (interrupt & synopGMACDmaTxStopped) {
		TR("%s:: Transmitter stopped sending the packets\n", __FUNCTION__);
#if 1

		if (GMAC_Power_down == 0) {
			// If Mac is not in powerdown
			synopGMAC_disable_dma_tx(gmacdev);
			synopGMAC_take_desc_ownership_tx(gmacdev);

			synopGMAC_enable_dma_tx(gmacdev);
			netif_wake_queue(netdev);
			TR("%s:: Transmission Resumed\n", __FUNCTION__);
		}

#endif
	}

	/* Enable the interrrupt before returning from ISR*/
	synopGMAC_enable_interrupt(gmacdev, DmaIntEnable);
	return IRQ_HANDLED;
}



/**
 * Function used when the interface is opened for use.
 * We register synopGMAC_linux_open function to linux open(). Basically this
 * function prepares the the device for operation . This function is called whenever ifconfig (in Linux)
 * activates the device (for example "ifconfig eth0 up"). This function registers
 * system resources needed
 *    - Attaches device to device specific structure
 *    - Programs the MDC clock for PHY configuration
 *    - Check and initialize the PHY interface
 *    - ISR registration
 *    - Setup and initialize Tx and Rx descriptors
 *    - Initialize MAC and DMA
 *    - Allocate Memory for RX descriptors (The should be DMAable)
 *    - Configure and Enable Interrupts
 *    - Enable Tx and Rx
 *    - start the Linux network queue interface
 * @param[in] pointer to net_device structure.
 * \return Returns 0 on success and error status upon failure.
 * \callgraph
 */

/* porting status: OK */
s32 synopGMAC_linux_open(struct net_device *netdev)
{
	s32 status = 0;
	s32 retval = 0;
	s32 ijk;
	u16 data;
	u32 dma_addr;
	struct sk_buff *skb;
	synopGMACNetworkAdapter *adapter;
	synopGMACdevice *gmacdev;
	struct platform_device *pltdev;

	TR0("%s called \n", __FUNCTION__);
	/* extract private data */
	adapter = netdev_priv(netdev);

	if (adapter == NULL) {
		return -ENODEV;
	}

	gmacdev = adapter->synopGMACdev;

	if (gmacdev == NULL) {
		return -ENODEV;
	}

	pltdev = adapter->synopGMACpltdev;

	if (pltdev  == NULL) {
		return -ENODEV;
	}

	TR0("PHY base =  %d\n", gmacdev->PhyBase);
	/* Lets read the version of ip in to device structure */
	synopGMAC_read_version(gmacdev);

	/* Now set the broadcast address */
	for (ijk = 0; ijk < 6; ijk++) {
		netdev->broadcast[ijk] = 0xff;
		TR("netdev->dev_addr[%d] = %02x and netdev->broadcast[%d] = %02x\n",
		   ijk, netdev->dev_addr[ijk], ijk, netdev->broadcast[ijk]);
	}

	/* Request for an shared interrupt. Instead of using netdev->irq */
	if (devm_request_irq(&pltdev->dev, netdev->irq, &synopGMAC_intr_handler,
	                     IRQF_SHARED | IRQF_DISABLED, netdev->name, netdev)) {
		TR0("Error in request_irq\n");
		return -ESYNOPGMACBUSY;
	}

	TR("%s owns a shared interrupt on line %d\n", netdev->name, netdev->irq);

	/* Set up the tx and rx descriptor queue/ring */
	synopGMAC_setup_tx_desc_queue(gmacdev, pltdev, TRANSMIT_DESC_SIZE, RINGMODE);
	synopGMAC_init_tx_desc_base(gmacdev);

	/* Rx desc */
	synopGMAC_setup_rx_desc_queue(gmacdev, pltdev, RECEIVE_DESC_SIZE, RINGMODE);
	synopGMAC_init_rx_desc_base(gmacdev);

	/* Allocate dmable space to rx descriptors */
	do {
		skb = dev_alloc_skb(RX_BUF_SIZE);

		if (skb == NULL) {
			TR0("ERROR in skb buffer allocation\n");
			break;
		}

		/* dma memory map */
		dma_addr = dma_map_single(&pltdev->dev, skb->data, RX_BUF_SIZE,
		                          DMA_FROM_DEVICE);
		status = synopGMAC_set_rx_qptr(gmacdev, dma_addr, RX_BUF_SIZE, (u32)skb, 0, 0,
		                               0);

		if (status < 0) {
			pr_info("Reach the end of the ring\n");
			dev_kfree_skb(skb);
		}
	} while (status >= 0);

# if 0
	//synopGMAC_write_phy_reg((u32 *)gmacdev->MacBase, gmacdev->PhyBase, PHY_CONTROL_REG, Mii_Loopback |
	//    Mii_Speed_1000 | Mii_Duplex);
	//synopGMAC_write_phy_reg((u32 *)gmacdev->MacBase, gmacdev->PhyBase, PHY_1000BT_CTRL_REG, 0x1200);
	//gmacdev->Speed = SPEED1000;
	//status = synopGMAC_read_phy_reg((u32 *)gmacdev->MacBase, gmacdev->PhyBase, PHY_1000BT_CTRL_REG, &data);
	//TR("Setting PHY 1000BT control register: %02X.\n", data);
# else

	// loopback
	if (0) {
		synopGMAC_write_phy_reg((u32 *)gmacdev->MacBase, gmacdev->PhyBase,
		                        PHY_CONTROL_REG, Mii_Loopback |
		                        Mii_Speed_100 | Mii_Duplex);
	} else {

#if 1
	if (/*gmacdev->Speed == SPEED100 && */gmacdev->Protocol == GMAC_RMII)
		synopGMAC_write_phy_reg((u32 *)gmacdev->MacBase, gmacdev->PhyBase, PHY_CONTROL_REG, Mii_Speed_100 | Mii_Duplex);
#endif
/*
#if (PHE_SEL_SPEED == PHY_SPEED_100M)
		synopGMAC_write_phy_reg((u32 *)gmacdev->MacBase, gmacdev->PhyBase, PHY_CONTROL_REG, Mii_Speed_100 | Mii_Duplex);
#endif
*/
	}

# endif

	TR("macbase: %02X. phybase: %02X.\n", gmacdev->MacBase, gmacdev->PhyBase);
	status = synopGMAC_read_phy_reg((u32 *)gmacdev->MacBase, gmacdev->PhyBase,
	                                PHY_CONTROL_REG, &data);
	TR("Setting PHY control register: %02X.\n", data);


	/* pbl32 incr with rxthreshold 128 */
	synopGMAC_dma_bus_mode_init(gmacdev,
			DMARebuildINCRxBurstEn | DmaAddrAlignBeatsEn | DmaRxBustLength8 | DmaFixedBurstEnable | DmaBurstLength16 | DmaDescriptorSkip2 );
			//DmaBurstLength32 | DmaRxBustLength32 | DmaDescriptorSkip2);

	/* control init */
	synopGMAC_dma_control_init(gmacdev, DmaStoreAndForward | DmaRxThreshCtrl128 |
	                           DmaFwdErrorFrames | DmaFwdUnderSzFrames);

	/* Check for Phy initialization */
	synopGMAC_set_mdc_clk_div(gmacdev, GmiiCsrClk1);
	gmacdev->ClockDivMdc = synopGMAC_get_mdc_clk_div(gmacdev);

	/* Initialize the mac interface */
	synopGMAC_mac_init(gmacdev);
	synopGMAC_pause_control(gmacdev);

	/* clear interrupts */
	synopGMAC_clear_interrupt(gmacdev);

	/* Disable the interrupts generated by MMC and IPC counters.
	 * If these are not disabled ISR should be modified accordingly to
	 * handle these interrupts. */
	synopGMAC_disable_mmc_tx_interrupt(gmacdev,     0xFFFFFFFF);
	synopGMAC_disable_mmc_rx_interrupt(gmacdev,     0xFFFFFFFF);
	synopGMAC_disable_mmc_ipc_rx_interrupt(gmacdev, 0xFFFFFFFF);

	/* Enable interrupts, dma_tx, dma_rx */
	TR("Enable DMA\n");
	synopGMAC_enable_interrupt(gmacdev, DmaIntEnable);
	synopGMAC_enable_dma_rx(gmacdev);
	synopGMAC_enable_dma_tx(gmacdev);

	/* Allow upper layer to call */
	netif_start_queue(netdev);

	return retval;
}

/**
 * Function used when the interface is closed.
 *
 * This function is registered to linux stop() function. This function is
 * called whenever ifconfig (in Linux) closes the device (for example "ifconfig eth0 down").
 * This releases all the system resources allocated during open call.
 * system resources int needs
 *     - Disable the device interrupts
 *     - Stop the receiver and get back all the rx descriptors from the DMA
 *     - Stop the transmitter and get back all the tx descriptors from the DMA
 *     - Stop the Linux network queue interface
 *    - Free the irq (ISR registered is removed from the kernel)
 *     - Release the TX and RX descripor memory
 *    - De-initialize one second timer rgistered for cable plug/unplug tracking
 * @param[in] pointer to net_device structure.
 * \return Returns 0 on success and error status upon failure.
 * \callgraph
 */

/* porting status: OK */
s32 synopGMAC_linux_close(struct net_device *netdev)
{

	synopGMACNetworkAdapter *adapter;
	synopGMACdevice *gmacdev;
	struct platform_device *pltdev;
	TR0("%s\n", __FUNCTION__);

	/* Extract private data */
	adapter = netdev_priv(netdev);

	if (adapter == NULL) {
		TR0("OOPS adapter is null\n");
		return -1;
	}

	gmacdev = (synopGMACdevice *)adapter->synopGMACdev;

	if (gmacdev == NULL) {
		TR0("OOPS gmacdev is null\n");
		return -1;
	}

	pltdev = (struct platform_device *)adapter->synopGMACpltdev;

	if (pltdev == NULL) {
		TR("OOPS pltdev is null\n");
		return -1;
	}

	/*Disable all the interrupts*/
	synopGMAC_disable_interrupt_all(gmacdev);
	TR("the synopGMAC interrupt has been disabled\n");

	/*Disable the reception*/
	synopGMAC_disable_dma_rx(gmacdev);
	synopGMAC_take_desc_ownership_rx(gmacdev);
	TR("the synopGMAC Reception has been disabled\n");

	/*Disable the transmission*/
	synopGMAC_disable_dma_tx(gmacdev);
	synopGMAC_take_desc_ownership_tx(gmacdev);

	/* Tell upper layer to stop calling hard_xmit */
	TR("the synopGMAC Transmission has been disabled\n");
	netif_stop_queue(netdev);

	/*Free the Rx Descriptor contents*/
	TR("Now calling synopGMAC_giveup_rx_desc_queue \n");
	synopGMAC_giveup_rx_desc_queue(gmacdev, pltdev, RINGMODE);

	TR("Now calling synopGMAC_giveup_tx_desc_queue \n");
	synopGMAC_giveup_tx_desc_queue(gmacdev, pltdev, RINGMODE);

	//TR("Freeing the cable unplug timer\n");
	//del_timer(&synopGMAC_cable_unplug_timer);

	/* Release allocated memory */
	if (gmacdev) {
		plat_free_memory(gmacdev);
	}

	return -ESYNOPGMACNOERR;
}

/**
 * Function to transmit a given packet on the wire.
 * Whenever Linux Kernel has a packet ready to be transmitted, this function is called.
 * The function prepares a packet and prepares the descriptor and
 * enables/resumes the transmission.
 * @param[in] pointer to sk_buff structure.
 * @param[in] pointer to net_device structure.
 * \return Returns 0 on success and Error code on failure.
 * \note structure sk_buff is used to hold packet in Linux networking stacks.
 */
/* porting status: OK */
s32 synopGMAC_linux_xmit_frames(struct sk_buff *skb, struct net_device *netdev)
{
	s32 status = 0;
	u32 offload_needed = 0x0;
	u32 dma_addr;
	synopGMACNetworkAdapter *adapter;
	synopGMACdevice *gmacdev;
	struct platform_device *pltdev;

	//TR0("%s called \n",__FUNCTION__);
	if (skb == NULL) {
		TR0("skb is NULL What happened to Linux Kernel? \n ");
		return -1;
	}

	/* Extract private data */
	adapter = netdev_priv(netdev);

	if (adapter == NULL) {
		return -1;
	}

	gmacdev = adapter->synopGMACdev;

	if (gmacdev == NULL) {
		return -1;
	}

	pltdev = adapter->synopGMACpltdev;

	//hc_trigger_gpio(48, 1);
	/* Stop the network queue */
	netif_stop_queue(netdev);

	/* Hardware checksum */
	if (skb->ip_summed == CHECKSUM_COMPLETE || skb->ip_summed == CHECKSUM_PARTIAL) {
		/* In Linux networking, if kernel indicates skb->ip_summed = CHECKSUM_HW,
		 * then only checksum offloading should be performed
		 * Make sure that the OS on which this code runs have
		 * proper support to enable offloading. */
		offload_needed = 0x00000001;
		//TR0("%s Perform hardware checksum offloading\n", __FUNCTION__);
	}

	// dbg message
	if (0) {
		TR0("ip_summed: %04x, %04x\n", skb->ip_summed, CHECKSUM_COMPLETE);
		TR0("dma_map, skb->len: %04x, %04x, %04x, %04x\n", skb->len, skb->data_len,
		    skb->mac_len, skb->hdr_len);
		//offload_needed = 0x00000001;
		//skb->len += 4;
	}

	/* Now we have skb ready and OS invoked this function. Lets make our DMA know about this */
	dma_addr = dma_map_single(&pltdev->dev, skb->data, skb->len, DMA_TO_DEVICE);
	status = synopGMAC_set_tx_qptr(gmacdev, dma_addr, skb->len, (u32)skb, 0, 0, 0,
	                               offload_needed);

	if (status < 0) {
		TR0("%s No More Free Tx Descriptors\n", __FUNCTION__);
		// dev_kfree_skb (skb); //with this, system used to freeze.. ??
		return -EBUSY;
	}

	/* Now force the DMA to start transmission */
	synopGMAC_resume_dma_tx(gmacdev);
	netdev->trans_start = jiffies;

	/* Now start the netdev queue */
	netif_wake_queue(netdev);

	return -ESYNOPGMACNOERR;
}

/**
 * Function provides the network interface statistics.
 * Function is registered to linux get_stats() function. This function is
 * called whenever ifconfig (in Linux) asks for networkig statistics
 * (for example "ifconfig eth0").
 * @param[in] pointer to net_device structure.
 * \return Returns pointer to net_device_stats structure.
 * \callgraph
 */
struct net_device_stats *synopGMAC_linux_get_stats(struct net_device *netdev)
{
	TR("%s called \n", __FUNCTION__);
	return (&(((synopGMACNetworkAdapter *)netdev_priv(netdev))->synopGMACNetStats));
}

/**
 * Function to set multicast and promiscous mode.
 * @param[in] pointer to net_device structure.
 * \return returns void.
 */
void synopGMAC_linux_set_multicast_list(struct net_device *netdev)
{
	TR("%s called \n", __FUNCTION__);
	//todo Function not yet implemented.
	return;
}

/**
 * Function to set ethernet address of the NIC.
 * @param[in] pointer to net_device structure.
 * @param[in] pointer to an address structure.
 * \return Returns 0 on success Errorcode on failure.
 */
/* porting status: OK */
s32 synopGMAC_linux_set_mac_address(struct net_device *netdev, void *macaddr)
{
	synopGMACNetworkAdapter *adapter = NULL;
	synopGMACdevice *gmacdev = NULL;
	struct sockaddr *addr = macaddr;

	/* extract private data */
	adapter = netdev_priv(netdev);

	if (adapter == NULL) {
		return -1;
	}

	gmacdev = adapter->synopGMACdev;

	if (gmacdev == NULL) {
		return -1;
	}

	if (!is_valid_ether_addr(addr->sa_data)) {
		TR0("invalid mac address !\n");
		return -EADDRNOTAVAIL;
	}

	/* set mac address */
	synopGMAC_set_mac_addr(gmacdev, GmacAddr0High, GmacAddr0Low, addr->sa_data);
	synopGMAC_get_mac_addr(gmacdev, GmacAddr0High, GmacAddr0Low, netdev->dev_addr);

	TR("%s called \n", __FUNCTION__);
	return 0;
}

/**
 * Function to change the Maximum Transfer Unit.
 * @param[in] pointer to net_device structure.
 * @param[in] New value for maximum frame size.
 * \return Returns 0 on success Errorcode on failure.
 */
/* porting status: OK */
s32 synopGMAC_linux_change_mtu(struct net_device *netdev, s32 newmtu)
{
	TR("%s called, mtu:%d \n", __FUNCTION__, newmtu);

	netdev->mtu = newmtu;


	return 0;
}

/**
 * IOCTL interface.
 * This function is mainly for debugging purpose.
 * This provides hooks for Register read write, Retrieve descriptor status
 * and Retreiving Device structure information.
 * @param[in] pointer to net_device structure.
 * @param[in] pointer to ifreq structure.
 * @param[in] ioctl command.
 * \return Returns 0 on success Error code on failure.
 */
s32 synopGMAC_linux_do_ioctl(struct net_device *netdev, struct ifreq *ifr,
                             s32 cmd)
{

	s32 retval = 0;
	u16 temp_data = 0;
	synopGMACNetworkAdapter *adapter = NULL;
	synopGMACdevice *gmacdev = NULL;

	struct ifr_data_struct {
		u32 unit;
		u32 addr;
		u32 data;
	} *req;

	TR0("%s called, cmd=%04X \n", __FUNCTION__, cmd);

	/* extract private data */
	if (netdev == NULL) {
		return -1;
	}

	if (ifr == NULL) {
		return -1;
	}

	req = (struct ifr_data_struct *)ifr->ifr_data;

	adapter = netdev_priv(netdev);

	if (adapter == NULL) {
		return -1;
	}

	gmacdev = adapter->synopGMACdev;

	if (gmacdev == NULL) {
		return -1;
	}

	/* parse cmd */
	switch (cmd) {
	case IOCTL_READ_REGISTER:     // IOCTL for reading IP registers : Read Registers
		if (req->unit == 0) {       // Read Mac Register
			req->data = synopGMACReadReg((u32 *)gmacdev->MacBase, req->addr);
		} else if (req->unit == 1) { // Read DMA Register
			req->data = synopGMACReadReg((u32 *)gmacdev->DmaBase, req->addr);
		} else if (req->unit == 2) {
			// Read Phy Register
			retval = synopGMAC_read_phy_reg((u32 *)gmacdev->MacBase, gmacdev->PhyBase,
			                                req->addr, &temp_data);
			req->data = (u32)temp_data;

			if (retval != -ESYNOPGMACNOERR) {
				TR("ERROR in Phy read\n");
			}
		}

		break;

	case IOCTL_WRITE_REGISTER:    // IOCTL for reading IP registers : Read Registers
		if (req->unit == 0) {       // Write Mac Register
			synopGMACWriteReg((u32 *)gmacdev->MacBase, req->addr, req->data);
		} else if (req->unit == 1) { // Write DMA Register
			synopGMACWriteReg((u32 *)gmacdev->DmaBase, req->addr, req->data);
		} else if (req->unit == 2) {
			// Write Phy Register
			retval = synopGMAC_write_phy_reg((u32 *)gmacdev->MacBase, gmacdev->PhyBase,
			                                 req->addr, req->data);

			if (retval != -ESYNOPGMACNOERR) {
				TR("ERROR in Phy read\n");
			}
		}

		break;

	case IOCTL_READ_IPSTRUCT:  //IOCTL for reading GMAC DEVICE IP private structure
		if (req->unit == 0) {
			gmacdev = adapter->synopGMACdev;

			if (gmacdev == NULL) {
				return -1;
			}

			memcpy((synopGMACdevice *)req->addr, gmacdev, sizeof(synopGMACdevice));
		} else {
			return -1;
		}

		break;

	case IOCTL_READ_RXDESC:
		if (req->unit == 0) {
			gmacdev = adapter->synopGMACdev;

			if (gmacdev == NULL) {
				return -1;
			}

			memcpy((DmaDesc *)req->addr, gmacdev->RxDesc + ((DmaDesc *)(req->addr))->data1,
			       sizeof(DmaDesc));
		} else {
			return -1;
		}

		break;

	case IOCTL_READ_TXDESC:
		if (req->unit == 0) {
			gmacdev = adapter->synopGMACdev;

			if (gmacdev == NULL) {
				return -1;
			}

			memcpy((DmaDesc *)req->addr, gmacdev->TxDesc + ((DmaDesc *)(req->addr))->data1,
			       sizeof(DmaDesc));
		} else {
			return -1;
		}

		break;

	case IOCTL_POWER_DOWN:
		if (req->unit == 1) {
			//power down the mac
			TR("============I will Power down the MAC now =============\n");
			// If it is already in power down don't power down again
			retval = 0;

			if (((synopGMACReadReg((u32 *)gmacdev->MacBase,
			                       GmacPmtCtrlStatus)) & GmacPmtPowerDown) != GmacPmtPowerDown) {
				synopGMAC_linux_powerdown_mac(gmacdev);
				retval = 0;
			}
		}

		if (req->unit == 2) {
			//Disable the power down  and wake up the Mac locally
			TR("============I will Power up the MAC now =============\n");
			//If already powered down then only try to wake up
			retval = -1;

			if (((synopGMACReadReg((u32 *)gmacdev->MacBase,
			                       GmacPmtCtrlStatus)) & GmacPmtPowerDown) == GmacPmtPowerDown) {
				synopGMAC_power_down_disable(gmacdev);
				synopGMAC_linux_powerup_mac(gmacdev);
				retval = 0;
			}
		}

		break;

	default:
		retval = -1;
	}

	return retval;
}

/**
 * Function to handle a Tx Hang.
 * This is a software hook (Linux) to handle transmitter hang if any.
 * We get transmitter hang in the device interrupt status, and is handled
 * in ISR. This function is here as a place holder.
 * @param[in] pointer to net_device structure
 * \return void.
 */
void synopGMAC_linux_tx_timeout(struct net_device *netdev)
{
	TR("%s called \n", __FUNCTION__);
	//todo Function not yet implemented
	return;
}

/* netdev operation structure */
static struct net_device_ops synopGMAC_netdev_ops = {
	.ndo_open = synopGMAC_linux_open,
	.ndo_stop = synopGMAC_linux_close,
	.ndo_start_xmit = synopGMAC_linux_xmit_frames,
	.ndo_set_mac_address = synopGMAC_linux_set_mac_address,
	.ndo_do_ioctl = synopGMAC_linux_do_ioctl,
	.ndo_change_mtu = synopGMAC_linux_change_mtu,
	.ndo_tx_timeout = synopGMAC_linux_tx_timeout,
	.ndo_get_stats = synopGMAC_linux_get_stats,
};

/**
 * Function to initialize the Linux network interface.
 * Linux dependent Network interface is setup here. This provides
 * an example to handle the network dependent functionality.
 * \return Returns 0 on success and Error code on failure.
 */
s32 synopGMAC_init_network_interface(struct net_device *netdev)
{
	s32 err;
	u8 mac_addr0[6] = DEFAULT_MAC_ADDRESS;
	synopGMACNetworkAdapter *adapter = netdev_priv(netdev);
	u32 phy_base;

	/* Allocate Memory for the the GMACip structure */
	adapter->synopGMACdev = (synopGMACdevice *)plat_alloc_memory(sizeof(
	                            synopGMACdevice));

	if (!adapter->synopGMACdev) {
		TR0("Error in Memory Allocataion \n");
		return -ESYNOPGMACNOMEM;
	}

	phy_base = PHY0;
	if (of_property_read_u32(adapter->synopGMACpltdev->dev.of_node, "phy-base", &phy_base))
		TR0("phy-base is null\n");
	TR0("phy-base=%d\n", phy_base);

	/* Attach the device to MAC struct This will configure all the required base addresses
	   such as Mac base, configuration base, phy base address(out of 32 possible phys ) */
	synopGMAC_attach(adapter->synopGMACdev, (u32)adapter->base + MACBASE,
	                 (u32)adapter->base + DMABASE, phy_base/*DEFAULT_PHY_BASE*/);

	synopGMAC_reset(adapter->synopGMACdev);

	/* Fill network device data */
	netdev->netdev_ops = &synopGMAC_netdev_ops;
	netdev->watchdog_timeo = 5 * HZ;
	synopGMAC_set_mac_addr(adapter->synopGMACdev, GmacAddr0High, GmacAddr0Low, mac_addr0);
	ether_addr_copy(netdev->dev_addr, mac_addr0);

	/* Now start the network interface */
	if ((err = register_netdev(netdev)) != 0) {
		TR0("Error in Registering netdevice\n");
		return err;
	}

	return 0;
}


/**
 * Function to initialize the Linux network interface.
 * Linux dependent Network interface is setup here. This provides
 * an example to handle the network dependent functionality.
 * \return Returns 0 on success and Error code on failure.
 */
void synopGMAC_exit_network_interface(struct net_device *netdev)
{
	synopGMACNetworkAdapter *adapter = netdev_priv(netdev);
	plat_free_memory(adapter->synopGMACdev);
	unregister_netdev(netdev);
	TR0("Unregister network device\n");
}

