#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/platform_device.h>
#include <linux/stmmac.h>
#include <linux/phy.h>
#include <linux/mfd/syscon.h>
#include <linux/regmap.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/gpio.h>
#include <linux/clk.h>
#include <linux/of.h>
#include <linux/of_net.h>

#define GPIO_RST 9
#define DEFAULT_FREQ 100000000
#define DWMAC_100MHZ_FREQ 100000000
#define DWMAC_25MHZ_FREQ   25000000
#define EMACC_MODE_OFFS 0
#define EMACCLK_SEL_OFFS 0x00
#define EMACCLK_EN_OFFS  0x04
#define EMACCLK_DIV_OFFS 0x08

enum emac_mode {
  MII   = 0,
  RGMII = 1,
  RMII  = 4,
  SMII  = 7
};

static struct mode_param
{
  const char *name;
  enum emac_mode md;
} emac_params[] = {
  { "mii", MII },
  { "rgmii", RGMII },
  { "rmii", RMII },
  { "smii", SMII}
};

struct augentix_dwmac
{
  int interface;
  void __iomem *gmac_base; //phy:0x88300000 -->gmac and dma control
  void __iomem *emac_base; //phy:0x805E0000 -->phy and gmac clock(internal) control
  void __iomem *emacclk_base; //phy:0x80010054 --> clock (input pad ) control
  struct clk *emac_clk;
  struct device *dev;
};

extern int hc18xx_set_clk_parent_by_name(struct clk *clk, const char *parent_name);

static void augentix_fix_speed(void *priv, u32 speed)
{
  struct augentix_dwmac *dwmac = priv;
  ulong rate = DWMAC_25MHZ_FREQ;

  /* only GMII mode requires us to reconfigure the clock lines */
  if (dwmac->interface != PHY_INTERFACE_MODE_RGMII)
	  return;

  if (speed == 1000) {
	  rate = DWMAC_100MHZ_FREQ;
  } else {
	  rate = DWMAC_25MHZ_FREQ;
  }
  pr_debug("augentix_fix_speed: set freq rate: %lu",rate);
  clk_set_rate(dwmac->emac_clk, rate);
}

static void *augentix_dwmac_setup(struct platform_device *pdev)
{
  struct augentix_dwmac *dwmac;
  struct resource *res;

  //alloc priv data
  dwmac = devm_kzalloc(&pdev->dev, sizeof(*dwmac), GFP_KERNEL);
  if (!dwmac)
  {
    printk("ERROR: can't alloc augentix_dwmac struct\n");
    return NULL;
  }

  dwmac->dev = &pdev->dev;

  //iomap emac base (phy and gmac clock(internal) control}
  res = platform_get_resource(pdev, IORESOURCE_MEM, 1);
  if (!res)
  {
    dev_err(&pdev->dev, "can't get emac base\n");
    return NULL;
  }
  dwmac->emac_base = devm_ioremap_resource(&pdev->dev, res);
  if(IS_ERR(dwmac->emac_base))
  {
    dev_err(&pdev->dev, "can't map emac base\n");
    return NULL;
  }

  //iomap emacclk__base (clock (input pad ) control)
  res = platform_get_resource(pdev, IORESOURCE_MEM, 2);
  if (!res)
  {
    dev_err(&pdev->dev, "can't get emacclk base\n");
    return NULL;
  }
  dwmac->emacclk_base = devm_ioremap_resource(&pdev->dev, res);
  if(IS_ERR(dwmac->emacclk_base))
  {
    dev_err(&pdev->dev, "can't map emacclk base\n");
    return NULL;
  }

  return dwmac;
}

static int augentix_dwmac_init(struct platform_device *pdev, void *priv)
{
  struct augentix_dwmac *dwmac = priv;
  const char *mode_str;
  u32 rc, mode, i, clock_frequency;

  /* clock setup */
  dwmac->emac_clk = devm_clk_get(&pdev->dev, NULL);
  if (IS_ERR(dwmac->emac_clk))
  {
    dev_err(&pdev->dev, "fail to get clock resource\n");
    return -ENODEV;
  }
  clk_prepare_enable(dwmac->emac_clk);

  hc18xx_set_clk_parent_by_name(dwmac->emac_clk, "spll_out2");
  if (of_property_read_u32(pdev->dev.of_node, "clock-frequency", &clock_frequency))
  {
    clock_frequency = DEFAULT_FREQ;
  }
  clk_set_rate(dwmac->emac_clk, clock_frequency);

  /* EMAC mode selection; Use RMII as default mode */
  mode = MII;
  rc = of_property_read_string(pdev->dev.of_node, "phy-mode", &mode_str);
  if (!rc)
  {
    for (i = 0; i < ARRAY_SIZE(emac_params); i++)
    {
      if (!strcmp(emac_params[i].name, mode_str))
      {
        mode = emac_params[i].md;
        dev_info(&pdev->dev, "EMAC mode set to \"%s\"\n",
            emac_params[i].name);
        break;
      }
    }
  }
  writeb_relaxed(mode, dwmac->emac_base + EMACC_MODE_OFFS);

  /* EMAC tx/rx clock settings */
  switch (mode)
  {
  case MII:
    break;

  case RGMII:
  	dwmac->interface = PHY_INTERFACE_MODE_RGMII;
    writel_relaxed(0x00000003, dwmac->emacclk_base + EMACCLK_SEL_OFFS);
    writel_relaxed(0x01010101, dwmac->emacclk_base + EMACCLK_EN_OFFS);
    break;

  case RMII:
    dwmac->interface = PHY_INTERFACE_MODE_RMII;
    writel_relaxed(0x00000101, dwmac->emacclk_base + EMACCLK_SEL_OFFS);
    writel_relaxed(0x01010100, dwmac->emacclk_base + EMACCLK_EN_OFFS);
    writeb_relaxed(0x09, dwmac->emacclk_base + EMACCLK_DIV_OFFS);
    break;

  case SMII:
    break;

  default:
    dev_err(&pdev->dev, "Invalide EMAC mode\n");
    return -EINVAL;
    break;
  }

  return 0;
}

static void augentix_dwmac_exit(struct platform_device *pdev, void *priv)
{

}

const struct stmmac_of_data augentix_dwmac_data = {
  .has_gmac = 1,
  .fix_mac_speed = augentix_fix_speed,
  .setup = augentix_dwmac_setup,
  .init = augentix_dwmac_init,
  .exit = augentix_dwmac_exit,
};

