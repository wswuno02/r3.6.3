#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/irqreturn.h>
#include <linux/clocksource.h>
#include <linux/clockchips.h>
#include <linux/sched_clock.h>
#include <linux/clk.h>
#include <linux/delay.h>
#include <linux/err.h>
#include <linux/of.h>
#include <linux/of_irq.h>
#include <linux/of_address.h>

#include <asm/cputype.h>

#define TIMER_OFFSET 0x00070000
#define MAX_TIMER_CNT 0xffffffff

#define TIMER_CLKSRC_TRIG       0x00
#define TIMER_CLKSRC_IRQ_MASK   0x0C
#define TIMER_CLKSRC_STATUS     0x10
#define TIMER_CLKSRC_COUNT_VAL  0x14
#define TIMER_CLKSRC_MODE       0x18
#define TIMER_CLKSRC_COUNT_LOAD 0x1C

#define TIMER_CLKEVT_TRIG       0x20
#define TIMER_CLKEVT_IRQ_CLR    0x24
#define TIMER_CLKEVT_IRQ_STA    0x28
#define TIMER_CLKEVT_IRQ_MASK   0x2C
#define TIMER_CLKEVT_STATUS     0x30
#define TIMER_CLKEVT_COUNT_VAL  0x34
#define TIMER_CLKEVT_MODE       0x38
#define TIMER_CLKEVT_COUNT_LOAD 0x3C

static void __iomem * systimer_base;
static u32 ticks_per_jiffy;

static void systimer_clkevt_timer_setup(u32 count_load)
{
	writel_relaxed(count_load, systimer_base + TIMER_CLKEVT_COUNT_LOAD);
};

static void systimer_clkevt_timer_start(int periodic)
{
	if(readl_relaxed(systimer_base + TIMER_CLKEVT_STATUS) != 0)
		return;
	writel_relaxed(periodic, systimer_base + TIMER_CLKEVT_MODE);
	writel_relaxed(0, systimer_base + TIMER_CLKEVT_IRQ_MASK);
	writel_relaxed(1, systimer_base + TIMER_CLKEVT_TRIG);
};

static void systimer_clkevt_timer_stop(void)
{
	if(readl_relaxed(systimer_base + TIMER_CLKEVT_STATUS) == 0)
		return;
	writel_relaxed(1, systimer_base + TIMER_CLKEVT_IRQ_MASK);
	writel_relaxed(1, systimer_base + TIMER_CLKEVT_TRIG);
	while(readl_relaxed(systimer_base + TIMER_CLKEVT_STATUS) != 0) {;}
};

static void systimer_clkevt_set_mode(enum clock_event_mode mode,
				   struct clock_event_device *clk)
{
	switch (mode) {
	case CLOCK_EVT_MODE_PERIODIC:
		systimer_clkevt_timer_stop();
		systimer_clkevt_timer_setup(ticks_per_jiffy);
		systimer_clkevt_timer_start(1);
		break;
	case CLOCK_EVT_MODE_ONESHOT:
		systimer_clkevt_timer_stop();
		systimer_clkevt_timer_setup(ticks_per_jiffy);
		systimer_clkevt_timer_start(0);
		break;
	case CLOCK_EVT_MODE_UNUSED:
	case CLOCK_EVT_MODE_SHUTDOWN:
	default:
		systimer_clkevt_timer_stop();
		break;
	}
}

static int systimer_clkevt_next_event(unsigned long evt,
				      struct clock_event_device *unused)
{
	systimer_clkevt_timer_stop();
	systimer_clkevt_timer_setup(ticks_per_jiffy);
	systimer_clkevt_timer_start(0);
	return 0;
};

static struct clock_event_device systimer_clkevt = {
	.name = "systimer_clkevt",
	.rating = 300,
	.features = CLOCK_EVT_FEAT_PERIODIC | CLOCK_EVT_FEAT_ONESHOT,
	.set_mode = systimer_clkevt_set_mode,
	.set_next_event = systimer_clkevt_next_event,
};

static irqreturn_t systimer_clkevt_interrupt(int irq, void *dev_id)
{
	struct clock_event_device *evt = (struct clock_event_device *)dev_id;

	if(readl_relaxed(systimer_base + TIMER_CLKEVT_IRQ_STA) == 0)
		return IRQ_NONE;

	writel_relaxed(1, systimer_base + TIMER_CLKEVT_IRQ_CLR);
	evt->event_handler(evt);

	return IRQ_HANDLED;
}

static struct irqaction systimer_irq = {
	.name = "systimer-irqact",
	.flags = IRQF_TIMER | IRQF_IRQPOLL,
	.handler = systimer_clkevt_interrupt,
	.dev_id = &systimer_clkevt,
};

static u64 notrace systimer_sched_clock_read(void)
{
	return ~(readl_relaxed(systimer_base + TIMER_CLKSRC_COUNT_VAL));
}

u64 hc18xx_get_clocksource(void){
	return systimer_sched_clock_read();
}
EXPORT_SYMBOL(hc18xx_get_clocksource);

static void __init systimer_clksrc_init(void)
{
/* Stop the clock source */
	if(readl_relaxed(systimer_base + TIMER_CLKSRC_STATUS) != 0)
	{
		writel_relaxed(1, systimer_base + TIMER_CLKSRC_TRIG);
		while(readl_relaxed(systimer_base + TIMER_CLKSRC_STATUS) != 0) {;}
	}

	writel_relaxed(1, systimer_base + TIMER_CLKSRC_MODE);
	writel_relaxed(1, systimer_base + TIMER_CLKSRC_IRQ_MASK);
	writel_relaxed(MAX_TIMER_CNT, systimer_base + TIMER_CLKSRC_COUNT_LOAD);
	writel_relaxed(1, systimer_base + TIMER_CLKSRC_TRIG);
}

static void __init hc18xx_systimer_of_register(struct device_node *np)
{
	struct clk * clk;
	unsigned long rate = 0;
	int err = 0;
	int irq;

	systimer_base = of_iomap(np, 0);
	if (!systimer_base) {
		pr_err("hc18xx-systimer: invalid base address\n");
		return;
	}

	irq = irq_of_parse_and_map(np, 0);
	if (!irq) {
		pr_err("hc18xx-systimer: unable to parse irq\n");
		return;
	}

	clk = of_clk_get(np, 0);
	if (!IS_ERR(clk))
	{
		err = clk_prepare_enable(clk);
		if (err)
			pr_warn("hc18xx-systimer: sys_clk not found\n");
	}
	else
	{
		pr_warn("hc18xx-systimer: sys_clk not found\n");
		err = -EINVAL;
	}
	rate = clk_get_rate(clk);

	/* Immediately configure the timer on the boot CPU */
	systimer_clksrc_init();
	sched_clock_register(systimer_sched_clock_read, 32, rate);
	clocksource_mmio_init(systimer_base + TIMER_CLKSRC_COUNT_VAL,
			      np->name, rate, 300, 32,
			      clocksource_mmio_readl_down);

	ticks_per_jiffy = DIV_ROUND_UP(rate, HZ);
	systimer_clkevt_timer_stop();
	systimer_clkevt.cpumask = cpu_possible_mask;
	systimer_clkevt.irq = irq;
	clockevents_config_and_register(&systimer_clkevt, rate, 1, 0xffffffff);

	err = setup_irq(irq, &systimer_irq);
	if(err)
		pr_warn("Failed to setup irq %d\n", irq);

#ifdef  DEBUG
	printk("======== systimer info ========\n");
	printk("  sys_clk_rate = %ld\n", rate);
	printk("  HZ = %d\n", HZ);
	printk("  delta for timer = %d\n", ticks_per_jiffy);
	printk("  IRQ number = %d\n", irq);
	printk("  base = %p\n", systimer_base);
	printk("======== systimer info ========\n");
#endif
	return;
}

CLOCKSOURCE_OF_DECLARE(hc18xx_systimer, "augentix,hc18xx-systimer",
			hc18xx_systimer_of_register);
