#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/irqreturn.h>
#include <linux/clocksource.h>
#include <linux/clockchips.h>
#include <linux/sched_clock.h>
#include <linux/clk.h>
#include <linux/delay.h>
#include <linux/err.h>
#include <linux/of.h>
#include <linux/of_irq.h>
#include <linux/of_address.h>

#include <asm/cputype.h>

#define TIMER_OFFSET 0x80050040
#define MAX_TIMER_CNT 0xffffffff

#define TIMER_CLK_TRIG 0x00
#define TIMER_CLK_IRQ_CLR 0x04
#define TIMER_CLK_IRQ_STA 0x08
#define TIMER_CLK_IRQ_MASK 0x0C
#define TIMER_CLK_STATUS 0x10
#define TIMER_CLK_COUNT_VAL 0x14
#define TIMER_CLK_MODE 0x18
#define TIMER_CLK_COUNT_LOAD 0x1C

#define TIMER3_CLK_TRIG 0x20
#define TIMER3_CLK_IRQ_CLR 0x24
#define TIMER3_CLK_IRQ_STA 0x28
#define TIMER3_CLK_IRQ_MASK 0x2C
#define TIMER3_CLK_STATUS 0x30
#define TIMER3_CLK_COUNT_VAL 0x34
#define TIMER3_CLK_MODE 0x38
#define TIMER3_CLK_COUNT_LOAD 0x3C

enum { TIMER0_HANDLE = 1,
       TIMER1_HANDLE = 2,
       TIMER2_HANDLE = 4,
       TIMER3_HANDLE = 8,
};

static void __iomem *systimer_base;

//==========================================================
static int agtx_timer_used = 0;
int agtx_timer_request(int timer_index) //Timer2-> input 2   Timer3->input 3
{
	int handle;

	if (timer_index == 2)
		handle = TIMER2_HANDLE;
	else if (timer_index == 3)
		handle = TIMER3_HANDLE;
	else { //only support timer2 and timer3
		return -EINVAL;
	}

	if ((agtx_timer_used & handle) != 0) {
		return -EBUSY;
	} else {
		agtx_timer_used |= handle;
		return handle;
	}
}
EXPORT_SYMBOL(agtx_timer_request);

void agtx_timer_free(int handle)
{
	if (handle == TIMER2_HANDLE) //timer2
		agtx_timer_used &= 11;
	else if (handle == TIMER3_HANDLE)
		agtx_timer_used &= 7;
}
EXPORT_SYMBOL(agtx_timer_free);
//==========================================================
int agtx_timer_set_period(int handle, unsigned int count_value)
{
	/*
	Let prescaler = 63
	Counting_time(s) = (count_value + 1) (prescaler + 1) /24M  
	                 = (count_value + 1)/375000
	Range: 11453s > period(sec) > 2.66*10 us
	*/
	const int mode = 1; //Periodic mode
	const int prescaler = 63;

	if (count_value > 3750000 || count_value < 38) //10s ~ 0.1013ms
		return -EINVAL;

	if (handle == TIMER2_HANDLE) { //timer2
		writel_relaxed(count_value - 1, systimer_base + TIMER_CLK_COUNT_LOAD);
		writel_relaxed((prescaler << 16) | (mode), systimer_base + TIMER_CLK_MODE);
	} else if (handle == TIMER3_HANDLE) { //timer3
		writel_relaxed(count_value - 1, systimer_base + TIMER3_CLK_COUNT_LOAD);
		writel_relaxed((prescaler << 16) | (mode), systimer_base + TIMER3_CLK_MODE);
	}

	return 0;
}
EXPORT_SYMBOL(agtx_timer_set_period);

//==========================================================

void agtx_timer_start(int handle) //start timer
{
	if (handle == TIMER2_HANDLE) { //timer2
		if (readl_relaxed(systimer_base + TIMER_CLK_STATUS) != 0) //if counting
			return;
		writel_relaxed(0, systimer_base + TIMER_CLK_IRQ_MASK);
		writel_relaxed(1, systimer_base + TIMER_CLK_TRIG);
	} else if (handle == TIMER3_HANDLE) { //timer3
		if (readl_relaxed(systimer_base + TIMER3_CLK_STATUS) != 0) //if counting
			return;
		writel_relaxed(0, systimer_base + TIMER3_CLK_IRQ_MASK);
		writel_relaxed(1, systimer_base + TIMER3_CLK_TRIG);
	}
}

EXPORT_SYMBOL(agtx_timer_start);
//==========================================================

void agtx_timer_stop(int handle) //stop timer
{
	if (handle == TIMER2_HANDLE) { //timer2
		if (readl_relaxed(systimer_base + TIMER_CLK_STATUS) == 0) //not counting
			return;
		writel_relaxed(1, systimer_base + TIMER_CLK_IRQ_MASK);
		writel_relaxed(1, systimer_base + TIMER_CLK_TRIG);
		while (readl_relaxed(systimer_base + TIMER_CLK_STATUS) != 0) {
			;
		}
	} else if (handle == TIMER3_HANDLE) {
		if (readl_relaxed(systimer_base + TIMER3_CLK_STATUS) == 0) //not counting
			return;
		writel_relaxed(1, systimer_base + TIMER3_CLK_IRQ_MASK);
		writel_relaxed(1, systimer_base + TIMER3_CLK_TRIG);
		while (readl_relaxed(systimer_base + TIMER3_CLK_STATUS) != 0) {
			;
		}
	}
}

EXPORT_SYMBOL(agtx_timer_stop);
//==========================================================

irqreturn_t agtx_timer_clear_IRQ(int handle)
{
	if (handle == TIMER2_HANDLE) { //timer2
		if (readl_relaxed(systimer_base + TIMER_CLK_IRQ_STA) == 0)
			return IRQ_NONE;

		writel_relaxed(1, systimer_base + TIMER_CLK_IRQ_CLR);
	} else if (handle == TIMER3_HANDLE) {
		if (readl_relaxed(systimer_base + TIMER3_CLK_IRQ_STA) == 0)
			return IRQ_NONE;

		writel_relaxed(1, systimer_base + TIMER3_CLK_IRQ_CLR);
	}
	return IRQ_HANDLED;
}
EXPORT_SYMBOL(agtx_timer_clear_IRQ);

//==========================================================

static void __init augentix_timer_of_register(struct device_node *np)
{
	systimer_base = of_iomap(np, 0);
	if (!systimer_base) {
		pr_err("augentix_timer: invalid base address\n");
		return;
	}

	return;
}

CLOCKSOURCE_OF_DECLARE(augentix_timer, "augentix,augentix-timer", augentix_timer_of_register);
