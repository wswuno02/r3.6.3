/*
 * Augentix Environment detection ADC driver
 * Copyright (c) Augentix Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/interrupt.h>

#include <linux/platform_device.h>

#include <linux/iio/iio.h>
#include <linux/iio/sysfs.h>
#include <linux/iio/events.h>
#include <linux/iio/buffer.h>
#include <linux/delay.h>

#include <misc/hc-adc_ctrl.h>
#include "hc-adc_env.h"
#include "csrif_adc_env.h"

/* definition for ADC_ENV IRQ or Polling mode*/
#define ADC_ENV_MODE_INTR

#define DRIVER_NAME "hc18xx-adc_env"

#define IRQ_RELATED 1
#define IRQ_UNRELATED 0

#define HC18XX_ADC_ENV_TIMEOUT (msecs_to_jiffies(100))
/*
 * adc_env_iio_channels - Description of available channels
 * dynamically devm_kzalloced during driver probe
 */
struct iio_chan_spec *adc_env_iio_channels;

/**
 * set_adc_env_query() - set adoin.adc.env_query_[0-2]
 * @chan:	the channel whose data is to be read
 *		in iio_chan_spec.
 */
static void set_adc_env_query(struct hc_adc_env_info *info, struct iio_chan_spec const *chan)
{
	switch (chan->channel) {
	case 0:
		writeCsr(info->adoin_csrif_base, ADOIN, ADC, ENV_QUERY_0, 1);
		break;
	case 1:
		writeCsr(info->adoin_csrif_base, ADOIN, ADC, ENV_QUERY_1, 1);
		break;
	case 2:
		writeCsr(info->adoin_csrif_base, ADOIN, ADC, ENV_QUERY_2, 1);
		break;
	default:
		printk("ADC env channel %d num error\n", chan->channel);
	}
}

/**
 * read_adc_env_data() - read adoin.adc.env_data_[0-2]
 * @chan:	the channel whose data is to be read
 *		in iio_chan_spec.
 */
static void read_adc_env_data(struct hc_adc_env_info *info, const int chan, int *val)
{
	//struct hc_adc_env_info *info = iio_priv(indio_dev);

	switch (chan) {
	case 0:
		readCsr(info->adoin_csrif_base, ADOIN, ADC, ENV_DATA_0, val);
		break;
	case 1:
		readCsr(info->adoin_csrif_base, ADOIN, ADC, ENV_DATA_1, val);
		break;
	case 2:
		readCsr(info->adoin_csrif_base, ADOIN, ADC, ENV_DATA_2, val);
		break;
	default:
		printk("%s: ADC env channel %d num error\n", __func__, chan);
	}
}

/**
 * clear_adc_env_irq() - read adoin.adc.env_data_[0-2]
 * @chan:	the channel whose data is to be read
 * @endisable:	0: don't mask irq, 1: mask irq
 */
static void adc_irq_mask_endisable(struct hc_adc_env_info *info, const int chan, const unsigned int endisable)
{
	switch (chan) {
	case 0:
		writeCsr(info->adoin_csrif_base, ADOIN, ADOIN, ADC_IRQ_MASK_ENV_DE_0, endisable);
		break;
	case 1:
		writeCsr(info->adoin_csrif_base, ADOIN, ADOIN, ADC_IRQ_MASK_ENV_DE_1, endisable);
		break;
	case 2:
		writeCsr(info->adoin_csrif_base, ADOIN, ADOIN, ADC_IRQ_MASK_ENV_DE_2, endisable);
		break;
	default:
		printk("%s: ADC env channel %d num error\n", __func__, chan);
	}
}

/**
 * adccfg_env_status_read() - read channel status (done or not)
 * @chan:	the channel whose data is to be read
 * @env_de:	Used to pass the env_de register value for the specified channel
 */
static void adccfg_env_status_read(struct hc_adc_env_info *info, const int chan, unsigned int *env_de)
{
	switch (chan) {
	case 0:
		readCsr(info->adoin_csrif_base, ADOIN, ADOIN, ADC_STATUS_ENV_DE_0, env_de);
		break;
	case 1:
		readCsr(info->adoin_csrif_base, ADOIN, ADOIN, ADC_STATUS_ENV_DE_1, env_de);
		break;
	case 2:
		readCsr(info->adoin_csrif_base, ADOIN, ADOIN, ADC_STATUS_ENV_DE_2, env_de);
		break;
	default:
		printk("%s: ADC env channel %d num error\n", __func__, chan);
	}
}

/**
 * clear_adc_env_irq_and_wait() - clear irq status for the specified channel
 * wait after clear is currently disabled (not in designer's provided flow)
 * @chan:	the channel whose data is to be read
 */
static void clear_adc_env_irq_and_wait(struct hc_adc_env_info *info, int chan)
{
	switch (chan) {
	case 0:
		writeCsr(info->adoin_csrif_base, ADOIN, ADOIN, ADC_IRQ_CLEAR_ENV_DE_0, 1);
		break;
	case 1:
		writeCsr(info->adoin_csrif_base, ADOIN, ADOIN, ADC_IRQ_CLEAR_ENV_DE_1, 1);
		break;
	case 2:
		writeCsr(info->adoin_csrif_base, ADOIN, ADOIN, ADC_IRQ_CLEAR_ENV_DE_2, 1);
		break;
	default:
		printk("%s: ADC env channel %d num error\n", __func__, chan);
	}
}

/**
 * adccfg_irq_relation_chk() - clear irq status for the specified channel
 * wait after clear is currently disabled (not in designer's provided flow)
 * @chan:	the channel whose data is to be read
 */
static int adccfg_irq_relation_chk(struct hc_adc_env_info *info, unsigned int *adc_env_stat)
{
	int idx = 0;
	adccfg_env_status_read(info, 0, &adc_env_stat[0]);
	adccfg_env_status_read(info, 1, &adc_env_stat[1]);
	adccfg_env_status_read(info, 2, &adc_env_stat[2]);

	pr_debug("env_stat[0] = %d, env_stat[1] = %d, env_stat[2] = %d\n", adc_env_stat[0], adc_env_stat[1],
	         adc_env_stat[2]);

	for (idx = 0; idx < 3; idx++) {
		if ((info->adc_rtn.current_chan != idx) && (adc_env_stat[idx] == 1)) {
			printk("err! %d \n", idx);
			printk("env_stat[0] = %d, env_stat[1] = %d, env_stat[2] = %d\n", adc_env_stat[0],
			       adc_env_stat[1], adc_env_stat[2]);
		}
	}
	if (adc_env_stat[0] || adc_env_stat[1] || adc_env_stat[2])
		return IRQ_RELATED;

	return IRQ_UNRELATED;
}

static irqreturn_t adccfg_irq_handler(int irq_id, void *dev_id)
{
	struct hc_adc_env_info *info = (struct hc_adc_env_info *)dev_id;
	unsigned int adc_env_stat[3];

	pr_debug("fn: %s\n", __func__);

	//adc_irq_dbg(info);
	if (adccfg_irq_relation_chk(info, adc_env_stat) == IRQ_UNRELATED) {
		pr_debug("Unrelated irq, leave without handling.\n");
		return IRQ_NONE;
	} else {
		//tasklet_schedule(&info->adc_rtn.tasklet);
		//printk("Related irq\n");
	}

	/*
	 * Below code assumes irq is related to adc environment detection
	 * (and not audio related)
	 */

	/* Clear irq */
	clear_adc_env_irq_and_wait(info, info->adc_rtn.current_chan);

	pr_debug("val =%u\n", info->adc_rtn.adc_irq_val);

	complete(&info->adc_rtn.adc_env_comp);

	return IRQ_HANDLED;
}

/**
 * iio_adc_env_detect() - environment detection flow
 * @indio_dev:	the struct iio_dev associated with this device instance
 * @chan:	the channel whose data is to be read
 * @val:	first element of returned value (typically INT)
 */
static void iio_adc_env_detect(struct iio_dev *indio_dev, struct iio_chan_spec const *chan, int *val)
{
	struct hc_adc_env_info *info = iio_priv(indio_dev);
	int ret;

	adc_clk_start();

	info->adc_rtn.current_chan = chan->channel;
	set_adc_env_query(info, chan); // Set adoin.adc.env_query_[0-2]

#ifdef ADC_ENV_MODE_INTR
	ret = wait_for_completion_timeout(&info->adc_rtn.adc_env_comp, HC18XX_ADC_ENV_TIMEOUT);
	if (ret == 0) {
		printk("%s timeout waiting on completion\n", __func__);
		//TODO: deal with timeout
	} else {
		//printk("No timeout!\n");
	}
#else //ADC_ENV_POLL
	while (1) {
		adccfg_env_status_read(info, chan->channel, &env_de);
		if (env_de == 1) {
			break;
		}
	}
	clear_adc_env_irq_and_wait(info, info->adc_rtn.current_chan);
	adccfg_irq_relation_chk(info, adc_env_stat);
	printk("[0]:%d [1]:%d [2]:%d\n", adc_env_stat[0], adc_env_stat[1], adc_env_stat[2]);
#endif /* ADC_ENV_MODE_INTR */

	read_adc_env_data(info, info->adc_rtn.current_chan, &info->adc_rtn.adc_irq_val);

	/* Temp workaround for Redmine #8288 */
	usleep_range(94, 120);

	adc_clk_stop();
}

/**
 * iio_adc_read_raw() - data read function.
 * @indio_dev:	the struct iio_dev associated with this device instance
 * @chan:	the channel whose data is to be read
 * @val:	first element of returned value (typically INT)
 * @mask:	what we actually want to read as per the info_mask_*
 *		in iio_chan_spec.
 */
static int iio_adc_read_raw(struct iio_dev *indio_dev, struct iio_chan_spec const *chan, int *val, int *val2, long mask)
{
	struct hc_adc_env_info *info = iio_priv(indio_dev);
	int ret = -EINVAL;

	pr_debug("In read raw!\n");
	mutex_lock(&info->lock);
	reinit_completion(&info->adc_rtn.adc_env_comp);

	switch (mask) {
	case IIO_CHAN_INFO_RAW: /* channel value read */
		switch (chan->type) {
		case IIO_VOLTAGE:
			pr_debug("channel %d\n", chan->channel);
			iio_adc_env_detect(indio_dev, chan, val);
			pr_debug("Done!\n");
			*val = info->adc_rtn.adc_irq_val;

			ret = IIO_VAL_INT;
			break;
		default:
			printk("%s: Unexpected chan_type %d\n", __func__, chan->type);
			break;
		}
		break;
	default:
		printk("%s: Unexpected mask %ld\n", __func__, mask);
		break;
	}
	mutex_unlock(&info->lock);
	return ret;
}

/*
 * Device type specific information.
 */
static const struct iio_info iio_adc_info = {
	.driver_module = THIS_MODULE,
	.read_raw = &iio_adc_read_raw,
};

/**
 * iio_adc_init_device() - device instance specific init
 * @indio_dev: the iio device structure
 */
static int iio_adc_init_device(struct iio_dev *indio_dev)
{
	struct hc_adc_env_info *info = iio_priv(indio_dev);
	pr_debug("%s\n", __func__);

	//TODO: Change to other value
	init_completion(&info->adc_rtn.adc_env_comp);

	return 0;
}

static int access_dt(struct platform_device *pdev, struct iio_dev *indio_dev)
{
	/* Retrieve channel type from dts, and save to local struct */
	/* device node path - check it from /proc/device-tree/ */
	struct hc_adc_env_info *info = iio_priv(indio_dev);
	char *name = "adc_ctrl";
	struct device_node *dt_node;
	const u8 *prop = NULL;
	int ret;
	unsigned int out_values[3], idx, array_idx;
	struct iio_chan_spec *channels;

	dt_node = of_find_node_by_name(NULL, name);
	if (!dt_node) {
		printk("Failed to find node by name: %s.\n", name);
	} else {
		pr_debug("Found the node for %s.\n", name);
		prop = of_get_property(dt_node, "channels", &ret);
		if (!prop) {
			printk("channels not found!\n");
			return -ENOENT;
		} else {
			pr_debug("reg found\n");
			of_property_read_u32_array(dt_node, "channels", out_values, 3);
		}
	}

	pr_debug("channel type: 0x%x 0x%x 0x%x\n", out_values[0], out_values[1], out_values[2]);

	/* Calculate the number of channels */
	for (idx = 0, indio_dev->channels = 0; idx < 3; idx++) {
		info->chan_type[idx] = (unsigned short)out_values[idx];
		if (info->chan_type[idx] == ADC_CH_ENV_DETECT) {
			indio_dev->num_channels++;
		}
	}
	pr_debug("Num of channels: %u\n", indio_dev->num_channels);

	/* Channel array setup */
	channels = devm_kzalloc(&indio_dev->dev, indio_dev->num_channels * sizeof(struct iio_chan_spec), GFP_KERNEL);
	if (!channels) {
		dev_err(&pdev->dev, "failed to malloc channels\n");
		return -ENOMEM;
	}

	/* Channel index modification */
	for (array_idx = 0, idx = 0; idx < 3 && array_idx < indio_dev->num_channels; idx++) {
		if (info->chan_type[idx] == ADC_CH_ENV_DETECT) {
			pr_debug("array_idx %u, chan_idx %u\n", array_idx, idx);
			channels[array_idx].channel = idx;
			channels[array_idx].type = IIO_VOLTAGE;
			channels[array_idx].indexed = 1;
			channels[array_idx].info_mask_separate = BIT(IIO_CHAN_INFO_RAW);

			array_idx++;
		}
	}

	indio_dev->channels = channels;

	return 0;
}

/**
 * adc_sw_init() - adc sw related init
 * @indio_dev: the iio device structure
 *
 */
static int adc_sw_init(struct platform_device *pdev, struct iio_dev *indio_dev)
{
	struct hc_adc_env_info *info = iio_priv(indio_dev);
	int chan_idx = 0, ret;
	pr_debug("%s\n", __func__);

	ret = access_dt(pdev, indio_dev);
	if (ret) {
		dev_err(&pdev->dev, "%s err!\n", __func__);
		return ret;
	}

	/* Enable IRQ (Only if channel type is correct) */
	for (chan_idx = 0; chan_idx < MAX_CHAN; chan_idx++) {
#ifdef ADC_ENV_MODE_INTR
		if (info->chan_type[chan_idx] == ADC_CH_ENV_DETECT) {
			adc_irq_mask_endisable(info, chan_idx, 0); // Don't mask channel's irq
		} else
			adc_irq_mask_endisable(info, chan_idx, 1); // Mask channel's irq
#else //ADC_ENV_POLL
		adc_irq_mask_endisable(info, chan_idx, 1); // Mask channel's irq
#endif
	}

	/* prepare clock */
	clk_prepare(info->adoin_clk);
	clk_enable(info->adoin_clk);

	hc18xx_adc_hw_initial();
	return 0;
}

/**
 * iio_adc_probe() - device instance probe
 */
static int iio_adc_probe(struct platform_device *pdev)
{
	int ret;
	struct iio_dev *indio_dev;
	struct hc_adc_env_info *info;
	struct resource *res;
	int adoin_irq;

	pr_debug("%s\n", __func__);

	/*
	 * Allocate an IIO device. It also has a region accessed by iio_priv()
	 * for chip specific state information.
	 */
	indio_dev = devm_iio_device_alloc(&pdev->dev, sizeof(struct hc_adc_env_info));

	if (indio_dev == NULL) {
		dev_err(&pdev->dev, "failed to allocate iio device\n");
		return -ENOMEM;
	}

	info = iio_priv(indio_dev);

	mutex_init(&info->lock);

	iio_adc_init_device(indio_dev);

	indio_dev->dev.parent = &pdev->dev;

	/* Set the device name */
	indio_dev->name = DRIVER_NAME;

	indio_dev->info = &iio_adc_info;

	/* Specify that device provides sysfs type interfaces */
	indio_dev->modes = INDIO_DIRECT_MODE; // Software trigger mode

	/* Retrieve register offsets from device tree */
	/* ADOIN */
	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res) {
		dev_err(&pdev->dev, "cannot find the ADOIN in resource");
		return -EINVAL;
	}

	info->adoin_ioremap_base = devm_ioremap_nocache(&pdev->dev, res->start, resource_size(res));
	if (IS_ERR(info->adoin_ioremap_base)) {
		dev_err(&pdev->dev, "cannot find ADOIN register addr\n");
		return -EINVAL;
	} else {
		info->adoin_csrif_base = (void __iomem *)CSR_BASE(info->adoin_ioremap_base, res->start);
	}

	/* ADCCFG */
	res = platform_get_resource(pdev, IORESOURCE_MEM, 1);
	if (!res) {
		dev_err(&pdev->dev, "cannot find the ADCCFG in resource");
		return -EINVAL;
	}

	info->adccfg_ioremap_base = devm_ioremap_nocache(&pdev->dev, res->start, resource_size(res));
	if (IS_ERR(info->adccfg_ioremap_base)) {
		dev_err(&pdev->dev, "cannot find ADCCFG register addr\n");
		return -EINVAL;
	} else {
		info->adccfg_csrif_base = (void __iomem *)CSR_BASE(info->adccfg_ioremap_base, res->start);
	}

	/* Retrieve IRQ num */
	adoin_irq = platform_get_irq(pdev, 0);
	if (adoin_irq < 0) {
		dev_err(&pdev->dev, "cannot find audio-in IRQ\n");
		return adoin_irq;
	}

	ret = devm_request_irq(&pdev->dev, adoin_irq, adccfg_irq_handler, IRQF_SHARED, dev_name(&pdev->dev), info);
	if (ret < 0) {
		printk("fail to claim adoin IRQ, ret = %d\n", ret);
	} else {
		pr_debug("irq %d\n", adoin_irq);
	}

	/* ADOIN clk */
	info->adoin_clk = devm_clk_get(&pdev->dev, "adoin_clk");
	if (IS_ERR(info->adoin_clk)) {
		dev_err(&pdev->dev, "cannot get audio-in clock source");
		return -ENODEV;
	}

	ret = adc_sw_init(pdev, indio_dev);
	if (ret) {
		dev_err(&pdev->dev, "cannot initialize software setting\n");
		return -EINVAL;
	}

	ret = devm_iio_device_register(&pdev->dev, indio_dev);
	if (ret < 0) {
		printk("devm register err!\n");
		goto error_ret;
	}

	/* Make the iio_dev struct available to remove function */
	platform_set_drvdata(pdev, indio_dev);

	return 0;

error_ret:
	return ret;
}

/**
 * iio_dummy_remove() - device instance removal function
 */
static int iio_adc_remove(struct platform_device *pdev)
{
	struct iio_dev *indio_dev = platform_get_drvdata(pdev);
	struct hc_adc_env_info *hc_adc_info = iio_priv(indio_dev);

	if (indio_dev == NULL) {
		printk("indio_dev == NULL\n");
		return 0;
	}

	if (hc_adc_info == NULL) {
		printk("hc_adc_info == NULL\n");
		return 0;
	}

	/* unprepare & disable adoin_clk */
	clk_disable(hc_adc_info->adoin_clk);
	clk_unprepare(hc_adc_info->adoin_clk);

	return 0;
}

static const struct of_device_id hc18xx_adc_env_of_ids[] = { {.compatible = "augentix,hc18xx-adc_env" }, {} };
MODULE_DEVICE_TABLE(of, hc18xx_adc_env_of_ids);

static struct platform_driver hc18xx_adc_env_driver = {
	.probe = iio_adc_probe,
	.remove = iio_adc_remove,
	.driver =
	        {
	                .name = DRIVER_NAME,
	                .owner = THIS_MODULE,
	                .of_match_table = hc18xx_adc_env_of_ids,
	        },
};
module_platform_driver(hc18xx_adc_env_driver);

MODULE_AUTHOR("Tony Kao <tony.kao@augentix.com>");
MODULE_DESCRIPTION("Augentix ADC ENV driver");
MODULE_LICENSE("GPL v2");
