/**
 * Augentix Environment detection ADC driver header
 * Copyright (c) Augentix Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 *
 */

#ifndef HC_ADC_ENV_H_
#define HC_ADC_ENV_H_
#include <linux/kernel.h>
#include <linux/clk.h>
#include <linux/completion.h>

#define ENABLE 1
#define DISABLE 0

struct hc_adc_rtn {
	int adc_irq_val;
	int current_chan;
	struct completion adc_env_comp;
	struct tasklet_struct tasklet;
};

/**
 * enum iio_simple_dummy_scan_elements - scan index enum
 * @voltage0:		the single ended voltage channel 0
 * @voltage1:		the single ended voltage channel 1
 * @voltage2:		the single ended voltage channel 2
 *
 * Enum provides convenient numbering for the scan index.
 */
enum iio_simple_dummy_scan_elements {
	voltage0 = 0,
	voltage1,
	voltage2,
	MAX_CHAN,
};

/**
 * struct iio_dummy_state - device instance specific state.
 * @single_ended_adc_val:	cache for single ended adc value
 * @lock:			lock to ensure state is consistent
 * @event_irq:			irq number for event line (faked)
 * @event_val:			cache for event theshold value
 * @event_en:			cache of whether event is enabled
 */
/* device and CSR information */
struct hc_adc_env_info {
	void __iomem *adoin_ioremap_base;
	void __iomem *adccfg_ioremap_base;
	/* 
	 * ioremapped physical base addr can't be used
	 * directly via readCsr() & writeCsr()
	 * -> use translated addr using CSR_BASE( ) instead
	 */
	void __iomem *adoin_csrif_base;
	void __iomem *adccfg_csrif_base;
	struct clk *adoin_clk;

	struct hc_adc_rtn adc_rtn;
	struct mutex lock;
	unsigned short chan_type[MAX_CHAN];
};

#endif /* HC_ADC_ENV_H_ */
