#include <linux/clk.h>
#include <linux/err.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/spi/spi.h>
#include <linux/scatterlist.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/of_platform.h>

#include "qspi-hc18xx.h"

#define DRIVER_NAME "auge_spi"

struct auge_spi_ex_s {
	struct auge_spi_s  auge_spi;
	struct clk     *clk;
};

static irqreturn_t auge_spi_irq(int irq, void *dev_id)
{

	struct spi_master *master = dev_id;
	struct auge_spi_s *auge_spi = spi_master_get_devdata(master);
	u32 irq_status = auge_readl(auge_spi, AUGE_SPI_ISR) & 0x3f;

	if (!irq_status)
		return IRQ_NONE;

	if (!master->cur_msg) {
		spi_mask_intr(auge_spi, SPI_INT_TXEI);
		return IRQ_HANDLED;
	}

	return auge_spi->transfer_handler(auge_spi);

}

/* Restart the controller, disable all interrupts, clean rx fifo */
static void spi_hw_init(struct device *dev, struct auge_spi_s *auge_spi)
{
	spi_reset_chip(auge_spi);

	auge_spi->fifo_len = HW_SSI_FIFO_DEPTH;
}


/* This may be called twice for each spi dev */
static int auge_spi_setup(struct spi_device *spi)
{
	struct auge_spi_chip *chip_info = NULL;
	struct chip_data *chip;

	/* Only alloc on first setup */
	chip = spi_get_ctldata(spi);
	if (!chip) {
		chip = kzalloc(sizeof(struct chip_data), GFP_KERNEL);
		if (!chip){
			printk("alloc chip data failure\n");
			return -ENOMEM;
		}
		spi_set_ctldata(spi, chip);
	}

	/*
	 * Protocol drivers may change the chip settings, so...
	 * if chip_info exists, use it
	 */
	chip_info = spi->controller_data;

	/* chip_info doesn't always exist */
	if (chip_info) {
		if (chip_info->cs_control)
			chip->cs_control = chip_info->cs_control;

		chip->poll_mode = chip_info->poll_mode;
	}

	chip->poll_mode = 1;
	chip->tmode = SPI_TMOD_TO;   //need to set
	chip->rx_sample_dly = 1;
	chip->cr1 = 0;               //for SPI_TMOD_RO and SPI_TMOD_EPROMREAD
	chip->spi_cr0 = 0;           //for qual spi
	chip->clk_div = 0;
	chip->speed_hz = 0;
	chip->enable_ssi_controller = 0;
	chip->cs_control = NULL;

	printk("run auge_spi_setup done\n");
	return 0;
}

static void auge_spi_cleanup(struct spi_device *spi)
{
	struct chip_data *chip = spi_get_ctldata(spi);

	kfree(chip);
	spi_set_ctldata(spi, NULL);
}
/*
static u32 bigTolittle(u32* val){
	u8* ptr = (u8*)val;
	u32 tmp =0;
	tmp =  (*(ptr+3) <<0 )|(*(ptr+2) <<8 )| (*(ptr+1) <<16 )| ((*ptr) << 24);
	return tmp;
}*/

static void auge_writer(struct auge_spi_s *auge_spi)
{
	u32 txw = 0;

	while ((auge_spi->tx_end - auge_spi->tx) > 0 && auge_spi->fifo_entry < HW_SSI_FIFO_DEPTH) {

		if (auge_spi->n_bytes == 1) {
			txw = *(u8 *)(auge_spi->tx);
			auge_write_io_reg(auge_spi, AUGE_SPI_DR, txw);
		//	printk("0 >0x%X\n",txw);
		} else if (auge_spi->n_bytes == 2) {

			txw = *(u16 *)(auge_spi->tx);
			auge_write_io_reg(auge_spi, AUGE_SPI_DR, txw);
		//	printk("1 >0x%X\n",txw);
		} else {
			txw = *(u32 *)(auge_spi->tx);
		//	auge_write_io_reg(auge_spi, AUGE_SPI_DR, bigTolittle(&txw));
			auge_write_io_reg(auge_spi, AUGE_SPI_RDR, txw);
		//	printk("2 >0x%X\n",txw);
		}

		auge_spi->tx += auge_spi->n_bytes;
		auge_spi->fifo_entry += 1;
	}
}

static void auge_reader(struct auge_spi_s *auge_spi)
{
	u32 rxw;
	while ((auge_spi->rx_end - auge_spi->rx) > 0) {
		if (auge_spi->n_bytes == 1){
		   rxw = auge_read_io_reg(auge_spi, AUGE_SPI_DR);
		   *(u8 *)(auge_spi->rx) = rxw;
		}
		else if (auge_spi->n_bytes == 2){
			/*
		   rxw = auge_read_io_reg(auge_spi, AUGE_SPI_DR);
		   *(u16 *)(auge_spi->rx) = rxw;*/
		}
		else if (auge_spi->n_bytes == 4){
		   //rxw = auge_read_io_reg(auge_spi, AUGE_SPI_DR);
		   rxw = auge_read_io_reg(auge_spi, AUGE_SPI_RDR);
		   *(u32 *)(auge_spi->rx) = rxw;//bigTolittle(&rxw);
		}
		auge_spi->rx += auge_spi->n_bytes;
	}
}

static int auge_conti_read(struct auge_spi_s *auge_spi)
{
	int32_t  timeout    = 255;
	uint32_t qspi_edr   = (uint32_t)(auge_spi->regs + AUGE_SPI_RDR);
	uint32_t qspi_rxflr = (uint32_t)(auge_spi->regs + AUGE_SPI_RXFLR);
	uint32_t qspi_ser   = (uint32_t)(auge_spi->regs + AUGE_SPI_SER);
	int32_t  word_count = auge_spi->len / auge_spi->n_bytes;

	uint32_t *pBuf;
	uint32_t  pEnd;
	uint32_t  left_word_count;
	uint32_t  rx_cnt;
	uint32_t  loop = 0;
	int ret = 0;

	if (word_count > 16) {
		/* Assumption: n_byte = 4 */
		BUG_ON(auge_spi->n_bytes != 4);

		left_word_count = word_count & 0x7;

		pBuf = (uint32_t *)auge_spi->rx;
		pEnd = (uint32_t)(pBuf + (word_count - left_word_count));

		asm volatile (
			"mov r1, %[pBuf]\n"		/* r1 holds address to destination memory */
			"preload_dcache:"
			"ldm r1!, {r2-r5}\n"		/* preload memory to d-cache */
			"ldm r1!, {r2-r5}\n"
			"cmp r1, %[pEnd]\n"		/* check if all memory loaded */
			"bne preload_dcache\n"
			"dummy_run_to_preload_icache:"
			"movs r1, #0\n"			/* r1 holds rxflr */
			"read_start:"
			"movs r1, r1, LSL #29\n"	/* shift rxflr[3] to C bit and rlxlr[2] to N bit */
			"ldmcs %[qspi_edr], {r2-r5}\n"	/* load 4 words if rxflr[3] == 1 */
			"stmcs %[pBuf]!, {r2-r5}\n"	/* store 4 words if rxflr[3] == 1 */
			"ldmcs %[qspi_edr], {r2-r5}\n"	/* load 4 words if rxflr[3] == 1 */
			"stmcs %[pBuf]!, {r2-r5}\n"	/* store 4 words if rxflr[3] == 1 */
			"ldmmi %[qspi_edr], {r2-r5}\n"	/* load 4 words if rxflr[2] == 1 */
			"stmmi %[pBuf]!, {r2-r5}\n"	/* store 4 words if rxflr[2] == 1 */
			"movs r1, r1, LSL #2\n"		/* shift rxflr[1] to C bit and rlxlr[0] to N bit */
			"ldmcs %[qspi_edr], {r2-r3}\n"	/* load 2 words if rxflr[1] == 1 */
			"stmcs %[pBuf]!, {r2-r3}\n"	/* store 2 words if rxflr[1] == 1 */
			"ldmmi %[qspi_edr], {r2}\n"	/* load 1 words if rxflr[0] == 1 */
			"stmmi %[pBuf]!, {r2}\n"	/* store 1 words if rxflr[0] == 1 */
			"check_finish:"
			"cmp %[pBuf], %[pEnd]\n"	/* check if all data copied to memory */
			"beq read_end\n"
			"read_rxflr:"
			"ldr r1, [%[qspi_rxflr]]\n"	/* read rxflr to r1 */
			"trigger_qspi:"
			"cmp %[loop], #0\n"		/* check if first time, send */
			"bne check_timeout\n"
			"mov r2, #1\n"
			"str r2, [%[qspi_ser]]\n"
			"check_timeout:"
			"add %[loop], %[loop], #1\n"	/* check if time out */
			"cmp %[loop], #0x10000\n"
			"bcs read_end\n"
			"b read_start\n"
			"read_end:"
			: [pBuf] "+r" (pBuf),
			  [loop] "+r" (loop)
			: [pEnd] "r" (pEnd),
			  [qspi_edr] "r" (qspi_edr),
			  [qspi_rxflr] "r" (qspi_rxflr),
			  [qspi_ser] "r" (qspi_ser)
			: "cc", "memory", "r1", "r2", "r3", "r4", "r5"
		);

		if (loop == 65537) {
			ret = -1;
		}

		if (left_word_count) {
			auge_spi->rx = (void *)pBuf;

			do {
				rx_cnt = auge_readl(auge_spi, AUGE_SPI_RXFLR);
				if (rx_cnt == left_word_count) {
					break;
				}
			} while (--timeout > 0);

			auge_reader(auge_spi);
		}
	} else {
		auge_writel(auge_spi, AUGE_SPI_SER, 1);

		do {
			rx_cnt = auge_readl(auge_spi, AUGE_SPI_RXFLR);
			if (rx_cnt == word_count) {
				break;
			}
		} while (--timeout > 0);

		auge_reader(auge_spi);
	}

	return ret;
}

static int auge_conti_write(struct auge_spi_s *auge_spi)
{
	uint32_t  qspi_edr   = (uint32_t)(auge_spi->regs + AUGE_SPI_RDR);
	uint32_t  qspi_txflr = (uint32_t)(auge_spi->regs + AUGE_SPI_TXFLR);
	uint32_t  qspi_ser   = (uint32_t)(auge_spi->regs + AUGE_SPI_SER);
	int32_t   word_count = ((uint32_t)auge_spi->tx_end - (uint32_t)auge_spi->tx) / auge_spi->n_bytes;
	uint32_t *pData;
	uint32_t  pEnd;
	uint32_t  is_trigger = 0;

	BUG_ON(word_count < 0);

	if (word_count > 0) {
		BUG_ON(auge_spi->n_bytes != 4);

		pData = (uint32_t *)auge_spi->tx;
		pEnd  = (uint32_t)auge_spi->tx_end;

		asm volatile (
			"mov r1, %[pData]\n"		/* r1 holds address to source memory */
			"preload_w_dcache:"
			"ldm r1!, {r2-r5}\n"		/* preload memory to d-cache */
			"ldm r1!, {r2-r5}\n"		/* preload memory to d-cache */
			"cmp r1, %[pEnd]\n"		/* check if all memory loaded */
			"bcc preload_w_dcache\n"
			"dummy_round_w:"
			"movs r1, #0\n"			/* r1 holds 0 for pre-load instruction */
			"write_start:"
			"movs r1, r1, LSL #29\n"	/* shift elem_count[3] to C bit and elem_count[2] to N bit */
			"ldmcs %[pData]!, {r2-r5}\n"	/* load 4 words if elem_count[3] == 1 */
			"stmcs %[qspi_edr], {r2-r5}\n"	/* store 4 words if elem_count[3] == 1 */
			"ldmcs %[pData]!, {r2-r5}\n"	/* load 4 words if elem_count[3] == 1 */
			"stmcs %[qspi_edr], {r2-r5}\n"	/* store 4 words if elem_count[3] == 1 */
			"ldmmi %[pData]!, {r2-r5}\n"	/* load 4 words if elem_count[2] == 1 */
			"stmmi %[qspi_edr], {r2-r5}\n"	/* store 4 words if elem_count[2] == 1 */
			"movs r1, r1, LSL #2\n"		/* shift elem_count[1] to C bit and elem_count[0] to N bit */
			"ldmcs %[pData]!, {r2-r3}\n"	/* load 2 words if elem_count[1] == 1 */
			"stmcs %[qspi_edr], {r2-r3}\n"	/* store 2 words if elem_count[1] == 1 */
			"ldmmi %[pData]!, {r2}\n"	/* load 1 words if elem_count[0] == 1 */
			"stmmi %[qspi_edr], {r2}\n"	/* store 1 words if elem_count[0] == 1 */
			"check_w_finish:"
			"cmp %[pData], %[pEnd]\n"	/* check if all data copied to FIFO */
			"beq write_end\n"
			"update_count:"
			"ldr r1, [%[qspi_txflr]]\n"	/* read txflr to r1 */
			"mov r2, #16\n"			/* r2 holds 16 */
			"sub r1, r2, r1\n"		/* elem_count(r1) = 16 - txflr */
			"sub r2, %[pEnd], %[pData]\n"	/* r2 = pEnd - pData */
			"mov r2, r2, LSR #2\n"		/* left_word_count(r2) = r2 >> 2 */
			"cmp r1, r2\n"			/* compare elem_count and left_word_count */
			"movgt r1, r2 \n"		/* Select min of elem_count and left_word_count */
			"trigger_w_qspi:"
			"cmp %[is_trigger], #0\n"		/* check if first time */
			"bne write_start\n"
			"mov %[is_trigger], #1\n"		/* set first time flag */
			"mov r2, #1\n"				/* set first time flag */
			"str r2, [%[qspi_ser]]\n"		/* trigger QSPI HW */
			"b write_start\n"
			"write_end:"
			: [pData] "+r" (pData),
			  [is_trigger] "+r" (is_trigger)
			: [pEnd] "r" (pEnd),
			  [qspi_edr] "r" (qspi_edr),
			  [qspi_txflr] "r" (qspi_txflr),
			  [qspi_ser] "r" (qspi_ser)
			: "cc", "memory", "r1", "r2", "r3", "r4", "r5"
		);

	} else if (word_count == 0) {
		auge_writel(auge_spi, AUGE_SPI_SER, 1);
	}

	return 0;
}

#if 0
static void wait_tx_done(struct auge_spi_s *auge_spi)
{
	u32 status;
	u32 umask;
	u32 expect;

	/* Wait for TX FIFO empty and BUSY flag cleared */
	umask = SR_TF_EMPT | SR_BUSY; // check TFE, BUSY
	expect = SR_TF_EMPT; // expect TFE = 1, BUSY = 0

	do {
		status = auge_readl(auge_spi, AUGE_SPI_SR);
		if ((status & umask) == expect) {
			break;
		}
	//	cpu_relax();
	} while(1);
}

static void wait_rx_done(struct auge_spi_s *auge_spi)
{
	  u32 rx_cnt;
	  u32 timeout = 256;
	  u32 rx_byte_count = 0;

	  if((auge_spi->rx_end - auge_spi->rx) > 0 ){
	  	rx_byte_count = (auge_spi->len/auge_spi->n_bytes);
	    do{
	      rx_cnt = auge_readl (auge_spi,AUGE_SPI_RXFLR);
	      if (rx_cnt == rx_byte_count)
	      {
	        break;
	      }
	    } while ( --timeout > 0 );
	  }
}

/* Must be called inside pump_transfers() */
static int poll_transfer(struct auge_spi_s *auge_spi,struct chip_data *chip, struct spi_transfer *transfer)
{
	auge_writer(auge_spi);
	if( transfer->tx_buf && !transfer->cs_change ){
		//printk("go poll xfer\n");
		auge_writel(auge_spi, AUGE_SPI_SER, 1);
		wait_tx_done(auge_spi);
		//printk("go poll xfer Done\n");
		chip->enable_ssi_controller = 0;
	}
	wait_rx_done(auge_spi);
	auge_reader(auge_spi);

	return 0;
}
#endif

static int poll_transfer_read(struct auge_spi_s *auge_spi,struct chip_data *chip, struct spi_transfer *transfer)
{
	int ret = 0;

//	auge_writel(auge_spi, AUGE_SPI_SER, 1);

	ret = auge_conti_read(auge_spi);

	return ret;
}

static int poll_transfer_write(struct auge_spi_s *auge_spi,struct chip_data *chip, struct spi_transfer *transfer)
{
	int ret = 0;

//	auge_writel(auge_spi, AUGE_SPI_SER, 1);

	ret = auge_conti_write(auge_spi);

	return ret;
}

static int setup_ssi_controller(struct auge_spi_s *auge_spi,
				struct chip_data *chip, struct spi_transfer *transfer)
{
	u16 clk_div;
	u32 spi_frf;
	u32 cr0;
	u32 dfs;

//	if (!chip->enable_ssi_controller) {
		dfs = chip->bDFS_32 ? 32 : 8;

	//	chip->enable_ssi_controller = 1;

		auge_writel(auge_spi, AUGE_SPI_SER, 0);

		spi_enable_chip(auge_spi, 0); //SSIENR = 0

		//BAUDR
		if (transfer->speed_hz != chip->speed_hz) {
			/* clk_div doesn't support odd number */
			clk_div = auge_spi->max_freq / transfer->speed_hz;
			clk_div = (clk_div + 1) & 0xfffe;

			chip->speed_hz = transfer->speed_hz;
			chip->clk_div = clk_div;

			spi_set_clk(auge_spi, chip->clk_div);
		}

		if (chip->bQualSPI) {
			spi_frf = SPI_SPI_FRF_QSPI;
		} else {
			spi_frf = SPI_SPI_FRF_SSPI;
		}

		//CTRL0
		cr0 = (auge_spi->type << SPI_FRF_OFFSET) /* SPI/SSP/MicroWire */
		    | (chip->tmode << SPI_TMOD_OFFSET)   /* TR/RO/TO/EEROM */
		    | ((dfs - 1) << SPI_DFS_32_OFFSET)   /* DFS_32 */
		    | (spi_frf << SPI_SPI_FRF_OFFSET);   /* spi_frf */

	//	printk("cr0 = 0x%X\n",cr0);
		auge_writel(auge_spi, AUGE_SPI_CTRL0, cr0);

		//CTRL1
		if(chip->tmode == SPI_TMOD_TO) {
			auge_writel(auge_spi, AUGE_SPI_CTRL1, 0);
		} else {
			auge_writel(auge_spi, AUGE_SPI_CTRL1, chip->cr1);
		}

		//SPI CTRL0
	//	printk("chip->spi_cr0 = 0x%X\n",chip->spi_cr0);
		auge_writel(auge_spi, AUGE_SPI_SPICTRLR0, chip->spi_cr0);

		//RX_SAMPLE_DELAY
		auge_writel(auge_spi, AUGE_SPI_RXSAMDLY, chip->rx_sample_dly);

		/* For poll mode just disable all interrupts */
		spi_mask_intr(auge_spi, 0xff);

		/* software reset */
		hc18xx_sw_rst(SW_RST_QSPI);

		spi_enable_chip(auge_spi, 1); //SSIENR = 1
//	}

	return 0;
}

static int auge_spi_transfer_one(struct spi_master *master,
		struct spi_device *spi, struct spi_transfer *transfer)
{
	struct auge_spi_s *auge_spi = spi_master_get_devdata(master);
	struct chip_data *chip = spi_get_ctldata(spi);

	auge_spi->dma_mapped = 0;

	if (transfer->tx_buf && transfer->rx_buf) {
		printk("Don't support tx_buf and rx_buf at the same transfer\n");
		return -EINVAL;
	}

	if (transfer->tx_buf) {
		auge_spi->tx     = (void *)transfer->tx_buf;     //in bytes
		auge_spi->tx_end = auge_spi->tx + transfer->len; //in bytes
		auge_spi->rx     = (void *)0;                    //in bytes
		auge_spi->rx_end = auge_spi->rx;                 //in bytes
	} else if (transfer->rx_buf) {
		auge_spi->tx     = (void *)0;                    //in bytes
		auge_spi->tx_end = auge_spi->tx;                 //in bytes
		auge_spi->rx     = transfer->rx_buf;             //in bytes
		auge_spi->rx_end = auge_spi->rx + transfer->len; //in bytes
	} else {
		printk("Don't support tx_buf=NULL and rx_buf=NULL\n");
		return -EINVAL;
	}

	auge_spi->len = transfer->len;

	if (!chip->enable_ssi_controller) {
	//	printk("chip->enable_ssi_controller=%d\n",chip->enable_ssi_controller);
		setup_ssi_controller(auge_spi,chip,transfer);
		chip->enable_ssi_controller = 1;

		auge_spi->fifo_entry = 0;
	}

	if (transfer->bits_per_word == 8) {
		auge_spi->n_bytes = 1;
	} else if (transfer->bits_per_word == 16) {
		auge_spi->n_bytes = 2;
	} else if (transfer->bits_per_word == 32) {
		auge_spi->n_bytes = 4;
	}

	if (transfer->tx_buf) {
		auge_writer(auge_spi);
	}

#if 0
	/*
	 * Interrupt mode
	 * we only need set the TXEI IRQ, as TX/RX always happen syncronizely
	 */
	if (!chip->poll_mode) {
		txlevel = min_t(u16, auge_spi->fifo_len / 2, auge_spi->len / auge_spi->n_bytes);
		auge_writel(auge_spi, AUGE_SPI_TXFLTR, txlevel);

		/* Set the interrupt mask */
		imask |= SPI_INT_TXEI | SPI_INT_TXOI |
			 SPI_INT_RXUI | SPI_INT_RXOI;
		spi_umask_intr(auge_spi, imask);

		auge_spi->transfer_handler = NULL;//interrupt_transfer;
	}
#endif

	if (chip->poll_mode) {
		int ret = 0;

		if (!transfer->cs_change) {
			unsigned long flag;

			spin_lock_irqsave(&auge_spi->lock, flag);

			if (transfer->rx_buf) {
			//	printk("rx_buf transfer->len = %d\n", transfer->len);
				ret = poll_transfer_read(auge_spi, chip, transfer);
			} else {
			//	printk("tx_buf transfer->len = %d\n", transfer->len);
				ret = poll_transfer_write(auge_spi, chip, transfer);
			}

			spin_unlock_irqrestore(&auge_spi->lock, flag);

			chip->enable_ssi_controller = 0;
		}

		transfer->cs_change = 0;

		return ret;
	}

	return 1;
}

int auge_spi_add_host(struct device *dev, struct auge_spi_s *auge_spi)
{
	struct spi_master *master;
	int ret;

	BUG_ON(auge_spi == NULL);

	master = spi_alloc_master(dev, 0);
	if (!master)
		return -ENOMEM;

	auge_spi->master = master;
	auge_spi->type = SSI_MOTO_SPI;
	auge_spi->dma_inited = 0;
	auge_spi->dma_addr = (dma_addr_t)(auge_spi->paddr + AUGE_SPI_DR);
	snprintf(auge_spi->name, sizeof(auge_spi->name), "auge_spi%d", auge_spi->bus_num);

	ret = request_irq(auge_spi->irq, auge_spi_irq, IRQF_SHARED, auge_spi->name, master);
	if (ret < 0) {
		dev_err(dev, "can not get IRQ\n");
		goto err_free_master;
	}

	master->mode_bits = SPI_CPOL | SPI_CPHA | SPI_TX_QUAD | SPI_RX_QUAD;
	master->bits_per_word_mask = SPI_BPW_MASK(8) | SPI_BPW_MASK(16) | SPI_BPW_MASK(32);
	master->bus_num = auge_spi->bus_num;
	master->num_chipselect = auge_spi->num_cs;
	master->setup = auge_spi_setup;
	master->cleanup = auge_spi_cleanup;
	//master->set_cs = dw_spi_set_cs;
	master->transfer_one = auge_spi_transfer_one;
	//master->handle_err = dw_spi_handle_err;
	master->max_speed_hz = auge_spi->max_freq;
	master->dev.of_node = dev->of_node;

	/* Basic HW init */
	spi_hw_init(dev, auge_spi);

	spi_master_set_devdata(master, auge_spi);
	ret = devm_spi_register_master(dev, master);
	if (ret) {
		dev_err(&master->dev, "problem registering spi master\n");
		goto err_free_irq;
	}

	return 0;

err_free_irq:
	spi_enable_chip(auge_spi, 0);
	free_irq(auge_spi->irq, master);
err_free_master:
	spi_master_put(master);
	return ret;
}

void auge_spi_remove_host(struct auge_spi_s *auge_spi)
{
	spi_shutdown_chip(auge_spi);
	free_irq(auge_spi->irq, auge_spi->master);
}

static int auge_spi_probe(struct platform_device *pdev)
{
	struct auge_spi_ex_s *auge_spi_ex;
	struct auge_spi_s *auge_spi;
	struct resource *mem;
	int ret;
	int num_cs;


	auge_spi_ex = devm_kzalloc(&pdev->dev, sizeof(struct auge_spi_ex_s),
			GFP_KERNEL);
	if (!auge_spi_ex)
		return -ENOMEM;

	auge_spi = &auge_spi_ex->auge_spi;

	/* Get basic io resource and map it */
	mem = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!mem) {
		dev_err(&pdev->dev, "no mem resource?\n");
		return -EINVAL;
	}

	auge_spi->regs = devm_ioremap_resource(&pdev->dev, mem);
	if (IS_ERR(auge_spi->regs)) {
		dev_err(&pdev->dev, "SPI region map failed\n");
		return PTR_ERR(auge_spi->regs);
	}

	/* For poll mode just disable all interrupts */
	spi_mask_intr(auge_spi, 0xff);

	auge_spi->irq = platform_get_irq(pdev, 0);
	if (auge_spi->irq < 0) {
		dev_err(&pdev->dev, "no irq resource?\n");
		return auge_spi->irq; /* -ENXIO */
	}
    printk("auge->irq = %d\n",auge_spi->irq);
#if 0
	auge_spi_ex->clk = devm_clk_get(&pdev->dev, NULL);
	if (IS_ERR(auge_spi_ex->clk))
		return PTR_ERR(auge_spi_ex->clk);
	ret = clk_prepare_enable(auge_spi_ex->clk);
	if (ret)
		return ret;
#endif

	auge_spi->bus_num = pdev->id;

	/* TODO: get clock setting from device tree and set clock with clock
	 * driver's API */
	auge_spi->max_freq = 133000000; //clk_get_rate(auge_spi_ex->clk);

	//device_property_read_u32(&pdev->dev, "reg-io-width", &dws->reg_io_width);
	num_cs = 1;
	//device_property_read_u32(&pdev->dev, "num-cs", &num_cs);
	auge_spi->num_cs = num_cs;

	spin_lock_init(&auge_spi->lock);

	ret = auge_spi_add_host(&pdev->dev, auge_spi);
	if (ret)
		goto out;

	printk("auge_spi_probe irq = %d\n",auge_spi_ex->auge_spi.irq);
	platform_set_drvdata(pdev, auge_spi_ex);
	return 0;

out:
	//clk_disable_unprepare(auge_spi_ex->clk);
	return ret;
}

static int auge_spi_remove(struct platform_device *pdev)
{
	struct auge_spi_ex_s *auge_spi_ex = platform_get_drvdata(pdev);

	//clk_disable_unprepare(auge_spi_ex->clk);
	auge_spi_remove_host(&auge_spi_ex->auge_spi);

	return 0;
}

static const struct of_device_id auge_spi_of_match[] = {
	{ .compatible = "augentix,qspi", },
	{ /* end of table */}
};
MODULE_DEVICE_TABLE(of, auge_spi_of_match);

static struct platform_driver auge_spi_driver = {
	.probe		= auge_spi_probe,
	.remove		= auge_spi_remove,
	.driver		= {
		.name	= DRIVER_NAME,
		.of_match_table = auge_spi_of_match,
	},
};
module_platform_driver(auge_spi_driver);

MODULE_AUTHOR("Nick Lin <nick.lin@augentix.com>");
MODULE_DESCRIPTION("Auge SPI driver");
MODULE_LICENSE("GPL v2");
