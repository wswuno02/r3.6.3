#include <linux/clk.h>
#include <linux/err.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/spi/spi.h>
#include <linux/scatterlist.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/of_platform.h>
#include <linux/dma-mapping.h>
#include <linux/delay.h>

#include "qspi-hc21xx.h"

#define DRIVER_NAME "auge_spi"

static struct auge_spi_s * g_auge_spi = NULL;
int auge_qspi_get_irq_status(struct spi_master *master);

static irqreturn_t auge_spi_irq(int irq, void *dev_id)
{
	struct spi_master *master = dev_id;
	struct auge_spi_s *auge_spi = spi_master_get_devdata(master);
	u32 irq_status = auge_readl(auge_spi, AGTX_SPI_WORD3) & 0x5555;

	if (!irq_status)
		return IRQ_NONE;

	if (irq_status & (1 << SPI_STATUS_DONE_OFFSET)) {
		complete(&auge_spi->xfer_completion);
		auge_writel(auge_spi, AGTX_SPI_WORD2, (1 << SPI_STATUS_DONE_OFFSET)); //irq clear
		return IRQ_HANDLED;
	}

	if (!master->cur_msg) {
		spi_mask_intr(auge_spi, 0x5555);
		return IRQ_HANDLED;
	}

	return IRQ_NONE;
}

/* Restart the controller, disable all interrupts, clean rx fifo */
static void spi_hw_init(struct device *dev, struct auge_spi_s *auge_spi)
{
	u32 qw10, val;
	spi_reset_chip(auge_spi);

	/* QSPI10 */
	qw10 = auge_readl(auge_spi, AGTX_SPI_WORD10);
	qw10 = (qw10 & 0xFFFFFF00) | (auge_spi->clk_div << SPI_SCK_DV_OFFSET);
	auge_writel(auge_spi, AGTX_SPI_WORD10, qw10);

	/* DARB QSPIR*/
	val = __raw_readl(auge_spi->regs_qspir_darb + 0x58) & ~0x03;
	val |= 0x01;
	__raw_writel(val, auge_spi->regs_qspir_darb + 0x58);
	/* DARB QSPIW*/
	val = __raw_readl(auge_spi->regs_qspir_darb + 0x38) & ~0x03;
	val |= 0x01;
	__raw_writel(val, auge_spi->regs_qspir_darb + 0x38);

	/* QSPIR TARGET_FIFO_LEVEL */
	val = __raw_readl(auge_spi->regs_qspir + 0x20) & ~0x07f;
	val |= 48;
	__raw_writel(val, auge_spi->regs_qspir + 0x20);
}

/* This may be called twice for each spi dev */
static int auge_spi_setup(struct spi_device *spi)
{
	struct chip_data *chip;

	/* Only alloc on first setup */
	chip = spi_get_ctldata(spi);
	if (!chip) {
		chip = kzalloc(sizeof(struct chip_data), GFP_KERNEL);
		if (!chip) {
			printk("alloc chip data failure\n");
			return -ENOMEM;
		}
		spi_set_ctldata(spi, chip);
	}

	chip->poll_mode = 0;
	chip->data_frame_size = QSPI_DFS__32_BIT;
	chip->dma_mode = 0;

	printk("run auge_spi_setup done\n");
	return 0;
}

static void auge_spi_cleanup(struct spi_device *spi)
{
	struct chip_data *chip = spi_get_ctldata(spi);

	kfree(chip);
	spi_set_ctldata(spi, NULL);
}

static void auge_writer_fifo(struct auge_spi_s *auge_spi, struct spi_transfer *transfer)
{
	int n_bytes = transfer->bits_per_word / 8;
	int write_len = 0;
	void *buf = transfer->tx_buf;
	u32 txw;

	while (transfer->len > write_len) {
		txw = 0;
		memcpy(&txw, buf, n_bytes);
		auge_write_io_reg(auge_spi, AGTX_SPI_WORD11, txw);
		buf += n_bytes;
		write_len += n_bytes;
	}
}

static void auge_read_fifo(struct auge_spi_s *auge_spi, struct spi_transfer *transfer)
{
	int read_len = transfer->len;
	void *buf = transfer->rx_buf;
	u32 rxw;

	while (read_len) {
		rxw = auge_read_io_reg(auge_spi, AGTX_SPI_WORD12);
		if (read_len < 4) {
			*(u8 *)buf = rxw & 0xff;
			buf++;
			read_len--;
		} else {
			*(u32 *)buf = rxw;
			buf += 4;
			read_len -= 4;
		}
	}
}

static int wait_done(struct auge_spi_s *auge_spi, struct chip_data *chip)
{
	u32 status;
	u32 expect;
	int ms;

	if (chip->poll_mode == 1) {
		auge_writel(auge_spi, AGTX_SPI_WORD0, 1); //Action start

		/* Wait for TX FIFO empty and DONE flag raised */
		expect = (1 << SPI_STATUS_DONE_OFFSET); // expect DONE = 1

		do {
			status = auge_readl(auge_spi, AGTX_SPI_WORD3);
			if (status & (1 << SPI_STATUS_ERROR_OFFSET)) {
				printk("\n\nQSPI error!\n\n");
				auge_qspi_get_irq_status(auge_spi->master);
				auge_writel(auge_spi, AGTX_SPI_WORD2, 0x00005555); //irq clear
				return -EIO;
			}
			if (status & expect) {
				break;
			}
		} while (1);
		auge_writel(auge_spi, AGTX_SPI_WORD2, 0x00005555); //irq clear
	} else {
		reinit_completion(&auge_spi->xfer_completion);
		auge_writel(auge_spi, AGTX_SPI_WORD0, 1); //Action start
		ms = wait_for_completion_timeout(&auge_spi->xfer_completion, msecs_to_jiffies(1000));
		status = auge_readl(auge_spi, AGTX_SPI_WORD3);

		if (status & (1 << SPI_STATUS_ERROR_OFFSET)) {
			printk("\n\nQSPI error!\n\n");
			auge_qspi_get_irq_status(auge_spi->master);
			auge_writel(auge_spi, AGTX_SPI_WORD2, 0x00005555); //irq clear
			return -EIO;
		}
		if (readl_relaxed(auge_spi->regs_qspiw + 8) & 0x100) {
			printk("\n\nQSPI Read BW Insufficient\n\n");
			auge_qspi_get_irq_status(auge_spi->master);
			return -EIO;
		}
		if (readl_relaxed(auge_spi->regs_qspir + 8) & 0x100) {
			printk("\n\nQSPI Write BW Insufficient\n\n");
			auge_qspi_get_irq_status(auge_spi->master);
			return -EIO;
		}
		if (ms == 0) {
			printk("\n\nQSPI time out\n\n");
			auge_qspi_get_irq_status(auge_spi->master);
			return -ETIMEDOUT;
		}
	}

	return 0;
}

static int transfer_write(struct auge_spi_s *auge_spi, struct chip_data *chip, struct spi_transfer *transfer)
{
	u32 len = transfer->len;
	dma_addr_t dma_dest;
	void __iomem *reg;
	u32 flush_len;
	int ret;
	u32 val;

	if (chip->dma_mode == 0) {
		ret = wait_done(auge_spi, chip);
	} else {
		dma_dest = dma_map_single(auge_spi->dev, transfer->tx_buf, len, DMA_TO_DEVICE);

		reg = auge_spi->regs_qspir;

		writel_relaxed(0x101, reg + LR032_01); //IRQ_CLEAR_FRAME_END, IRQ_CLEAR_BW_INSUFFICIENT
		writel_relaxed(len >> 2, reg + LR032_11); //PIXEL_FLUSH_LEN
		flush_len = len >> 3;
		if (len % 8)
			flush_len++;
		writel_relaxed(flush_len, reg + LR032_12); //FIFO_FLUSH_LEN
		writel_relaxed(dma_dest >> 3, reg + LR032_40); //INI_ADDR_LINEAR

		auge_writel(auge_spi, 0x168, 2); //QSPI.QSPI.debug_mon_sel  = 2

		val = readl_relaxed(auge_spi->regs_pc_syscfg_dbg_mon) & ~0x1f;
		val |= 7;
		writel_relaxed(val, auge_spi->regs_pc_syscfg_dbg_mon); //PC.SYSCFG.debug_mon_sel = 7

		val = readl_relaxed(auge_spi->regs_qspir + 0x10) & ~0x1000000;
		val |= 1 << 24;
		writel_relaxed(val, auge_spi->regs_qspir + 0x10); //QSPIR.QSPIR.debug_mon_sel = 1

		writel_relaxed(1, reg + LR032_00); //FRAME_START

		while ((readl_relaxed(auge_spi->regs_pc_syscfg_dbg_mon + 4) & 0x3f) != (flush_len - 1)) {
		}

		ret = wait_done(auge_spi, chip);
		dma_unmap_single(auge_spi->dev, dma_dest, len, DMA_TO_DEVICE);
	}

	return ret;
}

static int transfer_read(struct auge_spi_s *auge_spi, struct chip_data *chip, struct spi_transfer *transfer)
{
	u32 len = transfer->len;
	dma_addr_t dma_dest;
	void __iomem *reg;
	int ret;

	if (chip->dma_mode == 0) {
		ret = wait_done(auge_spi, chip);
		auge_read_fifo(auge_spi, transfer);
	} else {
		dma_dest = dma_map_single(auge_spi->dev, transfer->rx_buf, len, DMA_FROM_DEVICE);
		reg = auge_spi->regs_qspiw;

		writel_relaxed(0x101, reg + LR032_01); //IRQ_CLEAR_FRAME_END, IRQ_CLEAR_BW_INSUFFICIENT
		writel_relaxed(len >> 2, reg + LR032_11); //PIXEL_FLUSH_LEN
		writel_relaxed(dma_dest >> 3, reg + LR032_40); //INI_ADDR_LINEAR

		writel_relaxed(1, reg + LR032_00); //FRAME_START

		ret = wait_done(auge_spi, chip);
		dma_unmap_single(auge_spi->dev, dma_dest, len, DMA_FROM_DEVICE);
	}

	return ret;
}

static int setup_ssi_controller(struct auge_spi_s *auge_spi, struct chip_data *chip)
{
	u32 qw08, qw09, qw14, qw08_len;

	/* QSPI14 - DMA mode selection */
	qw14 = auge_readl(auge_spi, AGTX_SPI_WORD14);
	if (chip->dma_mode == 1) {
		qw14 |= QSPI_DMA_M__EN;
	} else {
		qw14 &= ~QSPI_DMA_M__EN;
	}
	auge_writel(auge_spi, AGTX_SPI_WORD14, qw14);

	/* QSPI08 */
	qw08_len = (1 << 10) | (0 << 12) | (0 << 14);
	qw08 = (chip->tmode << SPI_TRANSFER_MODE_OFFSET) | (chip->frame_format << SPI_FRAME_FORMAT_OFFSET) |
	       (chip->data_frame_size << SPI_DATA_FRAME_SIZE_OFFSET) | qw08_len | (chip->inst_set);
	auge_writel(auge_spi, AGTX_SPI_WORD8, qw08);

	/* QSPI09 - Number of Data Frames */
	if (chip->tmode == QSPI_TMOD__READ) {
		qw09 = (chip->ndf << SPI_NDF_OFFSET);
	} else {
		qw09 = (chip->ndfw << SPI_NDFW_OFFSET);
	}
	auge_writel(auge_spi, AGTX_SPI_WORD9, qw09);

	return 0;
}

static int augespi_transfer_one_message(struct spi_master *master, struct spi_message *msg)
{
	struct auge_spi_s *auge_spi = spi_master_get_devdata(master);
	struct chip_data *chip = spi_get_ctldata(msg->spi);
	struct spi_transfer *transfer;
	int ret = 0;

	spin_lock(&auge_spi->lock);
	setup_ssi_controller(auge_spi, chip);

	list_for_each_entry (transfer, &msg->transfers, transfer_list) {
		if (transfer->tx_buf || transfer->rx_buf) {
			if (transfer->tx_buf && (transfer->cs_change || chip->dma_mode == 0)) {
				auge_writer_fifo(auge_spi, transfer);
			}

			if (!transfer->cs_change) {
				unsigned long flag;

				if (transfer->rx_buf) {
					ret = transfer_read(auge_spi, chip, transfer);
				} else {
					ret = transfer_write(auge_spi, chip, transfer);
				}
			}
		} else {
			if (transfer->len)
				dev_err(&msg->spi->dev, "Bufferless transfer has length %u\n", transfer->len);
		}

		if (transfer->delay_usecs)
			udelay(transfer->delay_usecs);
	}
	spin_unlock(&auge_spi->lock);

	msg->status = ret;
	spi_finalize_current_message(master);

	return ret;
}

int auge_spi_add_host(struct device *dev, struct auge_spi_s *auge_spi)
{
	struct spi_master *master;
	int ret;

	BUG_ON(auge_spi == NULL);

	master = spi_alloc_master(dev, 0);
	if (!master)
		return -ENOMEM;

	auge_spi->master = master;
	auge_spi->dma_inited = 0;
	auge_spi->dma_addr = (dma_addr_t)(auge_spi->paddr + AGTX_SPI_WORD11); //WR_DATA
	auge_spi->max_freq = QSPI_REF_CLK_187500KHZ;
	auge_spi->freq = QSPI_IF_CLK_46875KHZ;
	auge_spi->clk_div = auge_spi->max_freq / auge_spi->freq;
	snprintf(auge_spi->name, sizeof(auge_spi->name), "auge_spi%d", auge_spi->bus_num);

	ret = request_irq(auge_spi->irq, auge_spi_irq, IRQF_SHARED, auge_spi->name, master);
	if (ret < 0) {
		dev_err(dev, "can not get IRQ\n");
		goto err_free_master;
	}

	master->mode_bits = SPI_CPOL | SPI_CPHA | SPI_TX_QUAD | SPI_RX_QUAD | SPI_RX_DUAL;
	master->bits_per_word_mask = SPI_BPW_MASK(8) | SPI_BPW_MASK(16) | SPI_BPW_MASK(32);
	master->bus_num = auge_spi->bus_num;
	master->num_chipselect = auge_spi->num_cs;
	master->setup = auge_spi_setup;
	master->cleanup = auge_spi_cleanup;
	master->transfer_one_message = augespi_transfer_one_message;
	master->max_speed_hz = auge_spi->max_freq;
	master->dev.of_node = dev->of_node;

	/* Basic HW init */
	spi_hw_init(dev, auge_spi);

	spi_master_set_devdata(master, auge_spi);
	ret = devm_spi_register_master(dev, master);
	if (ret) {
		dev_err(&master->dev, "problem registering spi master\n");
		goto err_free_irq;
	}

	return 0;

err_free_irq:
	spi_enable_chip(auge_spi, 0);
	free_irq(auge_spi->irq, master);
err_free_master:
	spi_master_put(master);
	return ret;
}

void auge_spi_remove_host(struct auge_spi_s *auge_spi)
{
	spi_shutdown_chip(auge_spi);
	free_irq(auge_spi->irq, auge_spi->master);
}

/* For NOR flash, we will enter 4-byte address mode when the flash size is
   bigger than 16MB. And we need send "E9h" CMD to exit 4-byte address mode
   when the system reboot with the instruction "reboot -f". The "E9h" CMD
   does not affect the NAND flash and the non-support 4-byte address mode's
   NOR flash.*/
void auge_nor_exit_4byte(void)
{
	auge_writel(g_auge_spi, AGTX_SPI_WORD8, 0x000205f1);
	auge_writel(g_auge_spi, AGTX_SPI_WORD9, 0);
	auge_writel(g_auge_spi, AGTX_SPI_WORD11, 0xe9);
	auge_writel(g_auge_spi, AGTX_SPI_WORD0, 0x01);
}
EXPORT_SYMBOL(auge_nor_exit_4byte);

static int auge_spi_probe(struct platform_device *pdev)
{
	struct auge_spi_s *auge_spi;
	struct resource *mem;
	int ret;
	int num_cs;

	auge_spi = devm_kzalloc(&pdev->dev, sizeof(struct auge_spi_s), GFP_KERNEL);
	if (!auge_spi)
		return -ENOMEM;

	auge_spi->dev = &pdev->dev;

	/* Get basic io resource and map it */
	mem = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!mem) {
		dev_err(&pdev->dev, "no mem resource?\n");
		return -EINVAL;
	}

	auge_spi->regs_qspi = devm_ioremap_resource(&pdev->dev, mem);
	if (IS_ERR(auge_spi->regs_qspi)) {
		dev_err(&pdev->dev, "QSPI region map failed\n");
		return PTR_ERR(auge_spi->regs_qspi);
	}

	mem = platform_get_resource(pdev, IORESOURCE_MEM, 1);
	if (!mem) {
		dev_err(&pdev->dev, "no mem resource?\n");
		return -EINVAL;
	}
	auge_spi->regs_qspir = devm_ioremap_resource(&pdev->dev, mem);
	if (IS_ERR(auge_spi->regs_qspir)) {
		dev_err(&pdev->dev, "QSPIR region map failed\n");
		return PTR_ERR(auge_spi->regs_qspir);
	}

	mem = platform_get_resource(pdev, IORESOURCE_MEM, 2);
	if (!mem) {
		dev_err(&pdev->dev, "no mem resource?\n");
		return -EINVAL;
	}
	auge_spi->regs_qspiw = devm_ioremap_resource(&pdev->dev, mem);
	if (IS_ERR(auge_spi->regs_qspiw)) {
		dev_err(&pdev->dev, "QSPIR region map failed\n");
		return PTR_ERR(auge_spi->regs_qspiw);
	}

	mem = platform_get_resource(pdev, IORESOURCE_MEM, 3);
	if (!mem) {
		dev_err(&pdev->dev, "no mem resource?\n");
		return -EINVAL;
	}
	auge_spi->regs_qspir_darb = devm_ioremap_resource(&pdev->dev, mem);
	if (IS_ERR(auge_spi->regs_qspir_darb)) {
		dev_err(&pdev->dev, "QSPIR_DARB region map failed\n");
		return PTR_ERR(auge_spi->regs_qspir_darb);
	}

	mem = platform_get_resource(pdev, IORESOURCE_MEM, 4);
	if (!mem) {
		dev_err(&pdev->dev, "no mem resource?\n");
		return -EINVAL;
	}
	auge_spi->regs_pc_syscfg_dbg_mon = devm_ioremap_resource(&pdev->dev, mem);
	if (IS_ERR(auge_spi->regs_pc_syscfg_dbg_mon)) {
		dev_err(&pdev->dev, "regs_pc_syscfg_dbg_mon region map failed\n");
		return PTR_ERR(auge_spi->regs_pc_syscfg_dbg_mon);
	}

	/* For poll mode just disable all interrupts */
	//spi_mask_intr(auge_spi, 0xff);

	auge_spi->irq = platform_get_irq(pdev, 0);
	if (auge_spi->irq < 0) {
		dev_err(&pdev->dev, "no irq resource?\n");
		return auge_spi->irq; /* -ENXIO */
	}
	printk("auge->irq = %d\n", auge_spi->irq);

	auge_spi->bus_num = pdev->id;

	num_cs = 1;
	auge_spi->num_cs = num_cs;

	spin_lock_init(&auge_spi->lock);
	init_completion(&auge_spi->xfer_completion);

	// IOSEL: QSPI
	//__raw_writel(0x00000001, 0x80001598);
	//__raw_writel(0x00000001, 0x8000159C);
	//__raw_writel(0x00000001, 0x800015A0);
	//__raw_writel(0x00000001, 0x800015A4);
	//__raw_writel(0x00000001, 0x800015A8);
	//__raw_writel(0x00000001, 0x800015AC);
	// QSPI_D2, QSPI_D3: ST enable, pull-up enable
	//__raw_writel(0x00080001, 0x8000145C);
	//__raw_writel(0x00080001, 0x80001460);

	ret = auge_spi_add_host(&pdev->dev, auge_spi);
	if (ret)
		goto out;

	g_auge_spi = auge_spi;
	if (g_auge_spi == NULL) {
		pr_err("%s: [Error] g_auge_spi no assigned!\n", __func__);
		return -ENODEV;
	}

	printk("auge_spi_probe irq = %d\n", auge_spi->irq);
	platform_set_drvdata(pdev, g_auge_spi);

	return 0;

out:
	return ret;
}

int auge_qspi_get_irq_status(struct spi_master *master)
{
	struct auge_spi_s *auge_spi = spi_master_get_devdata(master);
	u32 irq_status = auge_readl(auge_spi, AGTX_SPI_WORD3) & 0x5555;
	printk("IRQ status:\nQSPI 03:0x%08x\n", irq_status);
	irq_status = auge_readl(auge_spi, 0x14);
	printk("QSPI 05:0x%08x\n", irq_status);
	irq_status = auge_readl(auge_spi, 0x18);
	printk("QSPI 06:0x%08x\n", irq_status);
	irq_status = auge_readl(auge_spi, 0x1c);
	printk("QSPI 07:0x%08x\n", irq_status);
	irq_status = auge_readl(auge_spi, 0x20);
	printk("QSPI 08:0x%08x\n", irq_status);
	irq_status = auge_readl(auge_spi, 0x24);
	printk("QSPI 09:0x%08x\n", irq_status);
	irq_status = auge_readl(auge_spi, 0x28);
	printk("QSPI 10:0x%08x\n", irq_status);
	irq_status = auge_readl(auge_spi, 0x2c);
	printk("QSPI 11:0x%08x\n", irq_status);
	irq_status = auge_readl(auge_spi, 0x30);
	printk("QSPI 12:0x%08x\n", irq_status);
	irq_status = auge_readl(auge_spi, 0x34);
	printk("QSPI 13:0x%08x\n", irq_status);
	irq_status = auge_readl(auge_spi, 0x38);
	printk("QSPI 14:0x%08x\n", irq_status);

	irq_status = readl_relaxed(auge_spi->regs_qspir + 8);
	printk("QSPIR 02:0x%08x\n", irq_status);
	irq_status = readl_relaxed(auge_spi->regs_qspir + 0x10);
	printk("QSPIR 04:0x%08x\n", irq_status);
	irq_status = readl_relaxed(auge_spi->regs_qspir + 0x18);
	printk("QSPIR 06:0x%08x\n", irq_status);
	irq_status = readl_relaxed(auge_spi->regs_qspir + 0x1c);
	printk("QSPIR 07:0x%08x\n", irq_status);
	irq_status = readl_relaxed(auge_spi->regs_qspir + 0x20);
	printk("QSPIR 08:0x%08x\n", irq_status);
	irq_status = readl_relaxed(auge_spi->regs_qspir + 0x24);
	printk("QSPIR 09:0x%08x\n", irq_status);
	irq_status = readl_relaxed(auge_spi->regs_qspir + 0x28);
	printk("QSPIR 10:0x%08x\n", irq_status);
	irq_status = readl_relaxed(auge_spi->regs_qspir + 0x2c);
	printk("QSPIR 11:0x%08x\n", irq_status);
	irq_status = readl_relaxed(auge_spi->regs_qspir + 0x30);
	printk("QSPIR 12:0x%08x\n", irq_status);
	irq_status = readl_relaxed(auge_spi->regs_qspir + 0x38);
	printk("QSPIR 14:0x%08x\n", irq_status);
	irq_status = readl_relaxed(auge_spi->regs_qspir + 0xA0);
	printk("QSPIR 40:0x%08x\n", irq_status);

	irq_status = readl_relaxed(auge_spi->regs_qspiw + 8);
	printk("QSPIW 02:0x%08x\n", irq_status);
	irq_status = readl_relaxed(auge_spi->regs_qspiw + 0x2c);
	printk("QSPIW 11:0x%08x\n", irq_status);
	irq_status = readl_relaxed(auge_spi->regs_qspiw + 0xA0);
	printk("QSPIW 40:0x%08x\n", irq_status);

	irq_status = readl_relaxed(auge_spi->regs_qspir + 0x10) & ~0xffffff;
	irq_status |= 1 << 24;
	writel_relaxed(irq_status, auge_spi->regs_qspir + 0x10);

	irq_status = readl_relaxed(auge_spi->regs_pc_syscfg_dbg_mon) & ~0x1f;
	irq_status |= 7;
	writel_relaxed(irq_status, auge_spi->regs_pc_syscfg_dbg_mon);

	auge_writel(auge_spi, 0x168, 1);
	irq_status = readl_relaxed(auge_spi->regs_pc_syscfg_dbg_mon + 4);
	printk("QSPI.QSPI.debug_mon_sel = QSPI Engine, PC.SYSCFG.debug_mon = 0x%08x\n", irq_status);

	auge_writel(auge_spi, 0x168, 2);
	irq_status = readl_relaxed(auge_spi->regs_pc_syscfg_dbg_mon + 4);
	printk("QSPI.QSPI.debug_mon_sel = DRAM Read Agent, PC.SYSCFG.debug_mon = 0x%08x\n", irq_status);

	return irq_status;
}

static int auge_spi_remove(struct platform_device *pdev)
{
	struct auge_spi_s *auge_spi = platform_get_drvdata(pdev);

	auge_spi_remove_host(auge_spi);

	return 0;
}

static const struct of_device_id auge_spi_of_match[] = {
	{ .compatible = "augentix,qspi", },
	{ /* end of table */}
};
MODULE_DEVICE_TABLE(of, auge_spi_of_match);

static struct platform_driver auge_spi_driver = {
	.probe		= auge_spi_probe,
	.remove		= auge_spi_remove,
	.driver		= {
		.name	= DRIVER_NAME,
		.of_match_table = auge_spi_of_match,
	},
};
module_platform_driver(auge_spi_driver);

MODULE_AUTHOR("Nick Lin <nick.lin@augentix.com>");
MODULE_DESCRIPTION("Auge SPI driver");
MODULE_LICENSE("GPL v2");
