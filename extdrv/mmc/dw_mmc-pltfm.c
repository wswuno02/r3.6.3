/*
 * Synopsys DesignWare Multimedia Card Interface driver
 *
 * Copyright (C) 2009 NXP Semiconductors
 * Copyright (C) 2009, 2010 Imagination Technologies Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <linux/err.h>
#include <linux/interrupt.h>
#include <linux/module.h>
#include <linux/io.h>
#include <linux/irq.h>

#ifndef CONFIG_PLAT_HC18XX
#include <linux/pinctrl/consumer.h>
#endif

#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/mmc/host.h>
#include <linux/mmc/mmc.h>
#include <linux/mmc/dw_mmc.h>
#include <linux/of.h>
#include <linux/clk.h>

#include "dw_mmc.h"
#include "dw_mmc-pltfm.h"

void __iomem *ioreg;

int dw_mci_init(struct dw_mci *host)
{
	//printk("dw_mci_init...for IOMUX init\n ");
	host->pdata->quirks |= DW_MCI_QUIRK_BROKEN_DTO;
	return 0;
}

static void dw_mci_pltfm_prepare_command(struct dw_mci *host, u32 *cmdr)
{
	*cmdr |= SDMMC_CMD_USE_HOLD_REG;
}

static const struct dw_mci_drv_data socfpga_drv_data = {
	.prepare_command	= dw_mci_pltfm_prepare_command,
};

static const struct dw_mci_drv_data pistachio_drv_data = {
	.prepare_command	= dw_mci_pltfm_prepare_command,
};

static const struct dw_mci_drv_data auge_sdc_drv_data = {
    .init               = dw_mci_init,
	.prepare_command	= dw_mci_pltfm_prepare_command,
};

/* Set SDC HW phase and delay */
int hc18xx_dw_pci_init_dt(struct platform_device *pdev)
{
	void __iomem *base;
	struct resource *regs;

	regs = platform_get_resource(pdev, IORESOURCE_MEM, 1);
	base = devm_ioremap_resource(&pdev->dev, regs);
	if (IS_ERR(base))
		return PTR_ERR(base);

	writel_relaxed(0x00010001, base);
	return 0;
}

int dw_mci_pltfm_register(struct platform_device *pdev,
			  const struct dw_mci_drv_data *drv_data)
{
	struct dw_mci *host;
	struct resource	*regs;
	int ret;

	host = devm_kzalloc(&pdev->dev, sizeof(struct dw_mci), GFP_KERNEL);
	if (!host)
		return -ENOMEM;

	host->irq = platform_get_irq(pdev, 0);
	if (host->irq < 0)
		return host->irq;


	host->drv_data = drv_data;
	host->dev = &pdev->dev;
	host->irq_flags = 0;
	host->pdata = pdev->dev.platform_data;

	regs = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	/* Get registers' physical base address */
	host->phy_regs = regs->start;
	//printk("host->phy_regs...%11x\n ",host->phy_regs);
	host->regs = devm_ioremap_resource(&pdev->dev, regs);
	if (IS_ERR(host->regs))
		return PTR_ERR(host->regs);

	ioreg = devm_ioremap(&pdev->dev, 0x80001000, 0x1000);
	if (IS_ERR(ioreg))
		return PTR_ERR(ioreg);

	ret = hc18xx_dw_pci_init_dt(pdev);
	if (ret)
	{
		dev_err(&pdev->dev, "Failed to initialize hc18xx SDC node\n");
		return ret;
	}

	platform_set_drvdata(pdev, host);

#ifndef CONFIG_PLAT_HC18XX
	host->p = devm_pinctrl_get(host->dev);
	if (!host->p)
		dev_err(host->dev, "Fail to get pinctrl\n");

	if (strstr(pdev->name, "818a0000") != NULL) {
		host->dev->init_name = "SDC0";
		host->stage3v3 = pinctrl_lookup_state(host->p, "default");
		if (IS_ERR(host->stage3v3))
			dev_err(host->dev, "Fail to find default pinctrl setting\n");

		host->stage1v8 = pinctrl_lookup_state(host->p, "sd_poc_1v8");
		if (IS_ERR(host->stage1v8))
			dev_err(host->dev, "Fail to find 1.8V pinctrl setting\n");

		host->volt_a_dis = pinctrl_lookup_state(host->p, "volt_a_dis");
		if (IS_ERR(host->volt_a_dis))
			dev_err(host->dev, "Fail to find disable VOLT_A pinctrl setting\n");

		host->volt_a_en = pinctrl_lookup_state(host->p, "volt_a_en");
		if (IS_ERR(host->volt_a_en))
			dev_err(host->dev, "Fail to find enable VOLT_A pinctrl setting\n");
	} else
		host->dev->init_name = "SDC1";
#endif

	return dw_mci_probe(host);
}
EXPORT_SYMBOL_GPL(dw_mci_pltfm_register);

#ifndef CONFIG_PLAT_HC18XX
int dw_mci_augentix_voltage_select(struct dw_mci *host, const int volt_sel)
{
	int err;

	if (volt_sel == 1)
		err = pinctrl_select_state(host->p, host->stage1v8);
	else if (volt_sel == 0)
		err = pinctrl_select_state(host->p, host->stage3v3);
	else {
		dev_err(host->dev, "Invalid argument to set VOLT_REG\n");
		err = -EINVAL;
	}
	
	return err;
}

// int dw_mci_augentix_voltage_enable(struct dw_mci *host, const int en)
// {
// 	if (en == 1)
// 		return pinctrl_select_state(host->p, host->volt_a_en);
// 	else
// 		return pinctrl_select_state(host->p, host->volt_a_dis);
// }
#endif

#ifdef CONFIG_PM_SLEEP
/*
 * TODO: we should probably disable the clock to the card in the suspend path.
 */
static int dw_mci_pltfm_suspend(struct device *dev)
{
	struct dw_mci *host = dev_get_drvdata(dev);

	return dw_mci_suspend(host);
}

static int dw_mci_pltfm_resume(struct device *dev)
{
	struct dw_mci *host = dev_get_drvdata(dev);

	return dw_mci_resume(host);
}
#endif /* CONFIG_PM_SLEEP */

SIMPLE_DEV_PM_OPS(dw_mci_pltfm_pmops, dw_mci_pltfm_suspend, dw_mci_pltfm_resume);
EXPORT_SYMBOL_GPL(dw_mci_pltfm_pmops);


static const struct of_device_id dw_mci_pltfm_match[] = {
	{ .compatible = "snps,dw-mshc", },
	{ .compatible = "altr,socfpga-dw-mshc",
		.data = &socfpga_drv_data },
	{ .compatible = "img,pistachio-dw-mshc",
		.data = &pistachio_drv_data },
	{ .compatible = "augentix,sdc",
		.data = &auge_sdc_drv_data },
	{},
};
MODULE_DEVICE_TABLE(of, dw_mci_pltfm_match);

static int dw_mci_pltfm_probe(struct platform_device *pdev)
{
	const struct dw_mci_drv_data *drv_data = NULL;
	const struct of_device_id *match;

	if (pdev->dev.of_node) {
		match = of_match_node(dw_mci_pltfm_match, pdev->dev.of_node);
		drv_data = match->data;
	}

	return dw_mci_pltfm_register(pdev, drv_data);
}

int dw_mci_pltfm_remove(struct platform_device *pdev)
{
	struct dw_mci *host = platform_get_drvdata(pdev);

	dw_mci_remove(host);
	return 0;
}
EXPORT_SYMBOL_GPL(dw_mci_pltfm_remove);

static struct platform_driver dw_mci_pltfm_driver = {
	.probe		= dw_mci_pltfm_probe,
	.remove		= dw_mci_pltfm_remove,
	.driver		= {
		.name		= "dw_mmc",
		.of_match_table	= dw_mci_pltfm_match,
		.pm		= &dw_mci_pltfm_pmops,
	},
};

module_platform_driver(dw_mci_pltfm_driver);

MODULE_DESCRIPTION("DW Multimedia Card Interface driver");
MODULE_AUTHOR("NXP Semiconductor VietNam");
MODULE_AUTHOR("Imagination Technologies Ltd");
MODULE_LICENSE("GPL v2");
