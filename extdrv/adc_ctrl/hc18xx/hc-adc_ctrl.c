/*
 * Augentix HC18xx Audio Engine Hardware Operations
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <asm/io.h>
#include <mach/hc18xx.h>
#include <misc/hc-adc_ctrl.h>
#include <misc/csrif_adc_ctrl.h>

static struct adc_ctrl adc_ctrl_info;
static int stat = 0;

static DEFINE_SPINLOCK(adc_ctrl_lock);
static unsigned long adc_ctrl_spinlock_flags;

void hc18xx_adoin_sw_rst(void)
{
	pr_debug("%s\n", __func__);

	spin_lock_irqsave(&adc_ctrl_lock, adc_ctrl_spinlock_flags);

	if (stat == 0)
		hc18xx_sw_rst(SW_RST_AUDIO_IN);

	spin_unlock_irqrestore(&adc_ctrl_lock, adc_ctrl_spinlock_flags);
}
EXPORT_SYMBOL(hc18xx_adoin_sw_rst);

void adc_clk_start(void)
{
	pr_debug("%s: stat = %d\n", __func__, stat);

	spin_lock_irqsave(&adc_ctrl_lock, adc_ctrl_spinlock_flags);

	if (stat == 0) {
		pr_debug("%s: Start ADC clock\n", __func__);
		writeCsr(adc_ctrl_info.adoin_csrif_base, ADOIN, ADC, ADC_CLK_START, 1);
	}
	stat++;

	spin_unlock_irqrestore(&adc_ctrl_lock, adc_ctrl_spinlock_flags);
}
EXPORT_SYMBOL(adc_clk_start);

void adc_clk_stop(void)
{
	pr_debug("%s, stat = %d\n", __func__, stat);

	spin_lock_irqsave(&adc_ctrl_lock, adc_ctrl_spinlock_flags);

	stat--;
	if (stat == 0) {
		pr_debug("%s: Stop ADC clock\n", __func__);
		writeCsr(adc_ctrl_info.adoin_csrif_base, ADOIN, ADC, ADC_CLK_STOP, 1);
	}

	spin_unlock_irqrestore(&adc_ctrl_lock, adc_ctrl_spinlock_flags);
}
EXPORT_SYMBOL(adc_clk_stop);

static void adc_chan_type_set(const int chan, const unsigned int type)
{
	switch (chan) {
	case 0:
		writeCsr(adc_ctrl_info.adoin_csrif_base, ADOIN, ADC, CH_0_TYPE, type);
		break;
	case 1:
		writeCsr(adc_ctrl_info.adoin_csrif_base, ADOIN, ADC, CH_1_TYPE, type);
		break;
	case 2:
		writeCsr(adc_ctrl_info.adoin_csrif_base, ADOIN, ADC, CH_2_TYPE, type);
		break;
	default:
		printk("%s: ADC env channel %d num error\n", __func__, chan);
	}
}

static void hc18xx_adc_off_cali(void)
{
	unsigned int reg_val = 0;

#if 1
	/* Digital offset calibration flow */
	writeCsr(adc_ctrl_info.adccfg_csrif_base, ADCCFG, ADCCFG, SEL_DIFF, 0); // Single ended mode
	writeCsr(adc_ctrl_info.adccfg_csrif_base, ADCCFG, ADCCFG, BYPASS_CAL, 0);

	writeCsr(adc_ctrl_info.adccfg_csrif_base, ADCCFG, ADCCFG, RESET_CAL, 1);
	mdelay(1);
	writeCsr(adc_ctrl_info.adccfg_csrif_base, ADCCFG, ADCCFG, RESET_CAL, 0);

	writeCsr(adc_ctrl_info.adccfg_csrif_base, ADCCFG, ADCCFG, START_CAL, 1);
	mdelay(1);
	writeCsr(adc_ctrl_info.adccfg_csrif_base, ADCCFG, ADCCFG, START_CAL, 0);

	/* Wait for calibration done */
	do {
		readCsr(adc_ctrl_info.adccfg_csrif_base, ADCCFG, ADCCFG, CAL_ON, &reg_val);
		pr_debug("Read cal on val!\n");
	} while (reg_val == 1);
#if 0 // For debug use (Calibration results check)
	readCsr(adc_ctrl_info.adccfg_csrif_base, ADCCFG, ADCCFG, BVOS_OUT, &reg_val);
	pr_debug("BVOS_OUT: 0x%x\n", reg_val);
#endif
#else
	/* Bypass calibration */
	writeCsr(adc_ctrl_info.adccfg_csrif_base, ADCCFG, ADCCFG, BYPASS_CAL, 1);
#endif
}

void hc18xx_adc_hw_initial(void)
{
	static int init_stat = 0;

	adc_clk_start();
	spin_lock_irqsave(&adc_ctrl_lock, adc_ctrl_spinlock_flags);

	if (init_stat == 0) {
		pr_debug("%s\n", __func__);
		/* Seems to be calibration flow */
		writeCsr(adc_ctrl_info.adccfg_csrif_base, ADCCFG, ADCCFG, EN_LDO, 1);
		mdelay(1);
		writeCsr(adc_ctrl_info.adccfg_csrif_base, ADCCFG, ADCCFG, EN_ADC, 1);
		writeCsr(adc_ctrl_info.adccfg_csrif_base, ADCCFG, ADCCFG, RESET_ADC, 1);
		writeCsr(adc_ctrl_info.adccfg_csrif_base, ADCCFG, ADCCFG, RESET_CAL, 1);
		mdelay(1);
		writeCsr(adc_ctrl_info.adccfg_csrif_base, ADCCFG, ADCCFG, RESET_ADC, 0);
		writeCsr(adc_ctrl_info.adccfg_csrif_base, ADCCFG, ADCCFG, RESET_CAL, 0);

		hc18xx_adc_off_cali(); // Digital offset calibration

		init_stat = 1;
	} else {
		pr_debug("hw_init already init, bypass this time!\n");
	}
	spin_unlock_irqrestore(&adc_ctrl_lock, adc_ctrl_spinlock_flags);
	adc_clk_stop();
}
EXPORT_SYMBOL(hc18xx_adc_hw_initial);

static int set_adc_chan(struct platform_device *pdev)
{
	struct device_node *dt_node;
	char *name = "adc_ctrl";
	const void *prop = NULL;
	unsigned int idx, out_values[3];

	/* Retrieve & set channel type */
	dt_node = of_find_node_by_name(NULL, name);
	if (!dt_node) {
		printk("Failed to find node by name: %s.\n", name);
		return -ENOENT;
	} else {
		pr_debug("Found the node for %s.\n", name);
	}

	prop = of_get_property(dt_node, "channels", NULL);
	if (!prop) {
		printk("channels not found!\n");
		return -ENOENT;
	} else {
		pr_debug("reg found\n");

		if (of_property_read_u32_array(dt_node, "channels", out_values, 3)) {
			dev_err(&pdev->dev, "channels prop read err!\n");
		}

		/* channel type register set */
		for (idx = 0; idx < 3; idx++) {
			adc_chan_type_set(idx, out_values[idx]);
		}

		pr_debug("channel type: 0x%x 0x%x 0x%x\n", out_values[0], out_values[1], out_values[2]);
	}

	return 0;
}

static int set_regs_ioremap(struct platform_device *pdev)
{
	struct resource *res;

	/* Retrieve register offsets from device tree */
	/* ADOIN */
	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res) {
		dev_err(&pdev->dev, "cannot find the ADOIN in resource");
		return -EINVAL;
	}

	adc_ctrl_info.adoin_ioremap_base = devm_ioremap_nocache(&pdev->dev, res->start, resource_size(res));
	if (IS_ERR(adc_ctrl_info.adoin_ioremap_base)) {
		dev_err(&pdev->dev, "cannot find ADOIN register addr\n");
		return -EINVAL;
	} else {
		adc_ctrl_info.adoin_csrif_base = (void __iomem *)CSR_BASE(adc_ctrl_info.adoin_ioremap_base, res->start);
	}

	/* ADCCFG */
	res = platform_get_resource(pdev, IORESOURCE_MEM, 1);
	if (!res) {
		dev_err(&pdev->dev, "cannot find the ADCCFG in resource");
		return -EINVAL;
	}

	adc_ctrl_info.adccfg_ioremap_base = devm_ioremap_nocache(&pdev->dev, res->start, resource_size(res));
	if (IS_ERR(adc_ctrl_info.adccfg_ioremap_base)) {
		dev_err(&pdev->dev, "cannot find ADCCFG register addr\n");
		return -EINVAL;
	} else {
		adc_ctrl_info.adccfg_csrif_base =
		        (void __iomem *)CSR_BASE(adc_ctrl_info.adccfg_ioremap_base, res->start);
	}

	return 0;
}

static int hc_adc_ctrl_probe(struct platform_device *pdev)
{
	int ret = 0;

	pr_debug("%s\n", __func__);
	/* Retrieve register offsets from device tree */
	ret = set_regs_ioremap(pdev);
	if (ret) {
		dev_err(&pdev->dev, "regs ioremap err\n");
		return ret;
	}

	/* Retrive and set adc channel type from device tree */
	ret = set_adc_chan(pdev);
	if (ret) {
		dev_err(&pdev->dev, "set adc channel type err\n");
		return ret;
	}

	/* ADOIN clk */
	adc_ctrl_info.adoin_clk = devm_clk_get(&pdev->dev, "adoin_clk");
	if (IS_ERR(adc_ctrl_info.adoin_clk)) {
		dev_err(&pdev->dev, "cannot get audio-in clock source");
		return -ENODEV;
	}
	clk_prepare(adc_ctrl_info.adoin_clk);
	clk_enable(adc_ctrl_info.adoin_clk);

	return ret;
}

static int hc_adc_ctrl_remove(struct platform_device *pdev)
{
	pr_debug("%s\n", __func__);
	clk_disable(adc_ctrl_info.adoin_clk);
	clk_unprepare(adc_ctrl_info.adoin_clk);
	return 0;
}

static const struct of_device_id hc18xx_adc_ctrl_of_ids[] = { {.compatible = "augentix,hc18xx-adc_ctrl" }, {} };
MODULE_DEVICE_TABLE(of, hc18xx_adc_ctrl_of_ids);

static struct platform_driver hc18xx_adc_ctrl_driver = {
	.probe = hc_adc_ctrl_probe,
	.remove = hc_adc_ctrl_remove,
	.driver =
	        {
	                .name = "hc18xx-adc_ctrl",
	                .owner = THIS_MODULE,
	                .of_match_table = hc18xx_adc_ctrl_of_ids,
	        },
};
module_platform_driver(hc18xx_adc_ctrl_driver);

MODULE_AUTHOR("Tony Kao <tony.kao@augentix.com>");
MODULE_DESCRIPTION("Augentix HC18XX ASoC ADC Ctrl");
MODULE_LICENSE("GPL");
