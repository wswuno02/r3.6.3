#ifndef HC_ADC_CTRL_H_
#define HC_ADC_CTRL_H_

#include <linux/clk.h>
#include <linux/clk-provider.h>

enum ADC_CH_TYPE {
	ADC_CH_ENV_DETECT = 0,
	ADC_CH_ADO,
};

/* device and CSR information */
struct adc_ctrl {
	struct clk *adoin_clk;
	void __iomem *adoin_csrif_base;
	void __iomem *adccfg_csrif_base;

	void __iomem *adoin_ioremap_base;
	void __iomem *adccfg_ioremap_base;
};

void hc18xx_adoin_sw_rst(void);
void adc_clk_start(void);
void adc_clk_stop(void);
void hc18xx_adc_hw_initial(void);

#endif /* HC_ADC_CTRL_H_ */
