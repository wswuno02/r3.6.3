#ifndef AUGE_SPI_HEADER_H
#define AUGE_SPI_HEADER_H
#include <linux/io.h>
#include <linux/spi/hc18xx-spi-comm.h>
/* Register offsets */
#define AUGE_SPI_CTRL0			0x00
#define AUGE_SPI_CTRL1			0x04
#define AUGE_SPI_SSIENR			0x08
#define AUGE_SPI_MWCR			0x0C
#define AUGE_SPI_SER			0x10
#define AUGE_SPI_BAUDR			0x14
#define AUGE_SPI_TXFLTR			0x18
#define AUGE_SPI_RXFLTR			0x1C
#define AUGE_SPI_TXFLR			0x20
#define AUGE_SPI_RXFLR			0x24
#define AUGE_SPI_SR			    0x28
#define AUGE_SPI_IMR			0x2C
#define AUGE_SPI_ISR			0x30
#define AUGE_SPI_RISR			0x34
#define AUGE_SPI_TXOICR			0x38
#define AUGE_SPI_RXOICR			0x3C
#define AUGE_SPI_RXUICR			0x40
#define AUGE_SPI_MSTICR			0x44
#define AUGE_SPI_ICR			0x48
#define AUGE_SPI_DMACR			0x4C
#define AUGE_SPI_DMATDLR		0x50
#define AUGE_SPI_DMARDLR		0x54
#define AUGE_SPI_IDR			0x58
#define AUGE_SPI_VERSION		0x5C
#define AUGE_SPI_DR			    0x60
#define AUGE_SPI_RXSAMDLY		0xF0
#define AUGE_SPI_SPICTRLR0      0xF4


enum auge_ssi_type {
	SSI_MOTO_SPI = 0,
	SSI_TI_SSP,
	SSI_NS_MICROWIRE,
};

/*Hardware Settings*/
#define HW_SSI_FIFO_DEPTH 16


struct auge_spi_s {
	struct spi_master	*master;
	struct spi_device	*cur_dev;
	enum auge_ssi_type	type;
	char			name[16];

	void __iomem		*regs;
	unsigned long		paddr;
	int			irq;
	u32			fifo_len;	/* depth of the FIFO buffer */
	u32			max_freq;	/* max bus freq supported */

	u16			bus_num;
	u16			num_cs;		/* supported slave numbers */

	/* Message Transfer pump */
	struct tasklet_struct	pump_transfers;

	/* Current message transfer state info */
	struct spi_message	*cur_msg;
	struct spi_transfer	*cur_transfer;
	struct chip_data	*cur_chip;
	struct chip_data	*prev_chip;
	size_t			len;
	void			*tx;
	void			*tx_end;
	void			*rx;
	void			*rx_end;
	int			dma_mapped;
	dma_addr_t		rx_dma;
	dma_addr_t		tx_dma;
	size_t			rx_map_len;
	size_t			tx_map_len;
	u8			n_bytes;	/* current is a 1/2 bytes op */
	u8			max_bits_per_word;	/* maxim is 16b */
	u32			dma_width;
	irqreturn_t		(*transfer_handler)(struct auge_spi_s *auge_spi);
	void			(*cs_control)(u32 command);

	/* Dma info */
	int			dma_inited;
	struct dma_chan		*txchan;
	struct scatterlist	tx_sgl;
	struct dma_chan		*rxchan;
	struct scatterlist	rx_sgl;
	int			dma_chan_done;
	struct device		*dma_dev;
	dma_addr_t		dma_addr; /* phy address of the Data register */
	struct dw_spi_dma_ops	*dma_ops;
	void			*dma_priv; /* platform relate info */

	/* Bus interface info */
	void			*priv;
#ifdef CONFIG_DEBUG_FS
	struct dentry *debugfs;
#endif
};

/*
 * Each SPI slave device to work with dw_api controller should
 * has such a structure claiming its working mode (poll or PIO/DMA),
 * which can be save in the "controller_data" member of the
 * struct spi_device.
 */
struct auge_spi_chip {
	u8 poll_mode;	/* 1 for controller polling mode */
	u8 type;	/* SPI/SSP/MicroWire */
	void (*cs_control)(u32 command);
};

#define SPI_DFS_OFFSET			0
#define SPI_FRF_OFFSET			4
#define SPI_SCPH_OFFSET			6
#define SPI_SCOL_OFFSET			7
#define SPI_TMOD_OFFSET			8
#define SPI_DFS_32_OFFSET		16
#define SPI_SPI_FRF_OFFSET      21
#define SPI_MODE_OFFSET			6

//type
#define SPI_FRF_SPI			    0x0
#define SPI_FRF_SSP			    0x1
#define SPI_FRF_MICROWIRE		0x2
#define SPI_FRF_RESV			0x3
//spi_frf
#define SPI_SPI_FRF_SSPI        0x0
#define SPI_SPI_FRF_DSPI        0x1
#define SPI_SPI_FRF_QSPI        0x2

/* Bit fields in SR, 7 bits */
#define SR_MASK				0x7f		/* cover 7 bits */
#define SR_BUSY				(1 << 0)
#define SR_TF_NOT_FULL		(1 << 1)
#define SR_TF_EMPT			(1 << 2)
#define SR_RF_NOT_EMPT		(1 << 3)
#define SR_RF_FULL			(1 << 4)
#define SR_TX_ERR			(1 << 5)
#define SR_DCOL				(1 << 6)

/* Bit fields in ISR, IMR, RISR, 7 bits */
#define SPI_INT_TXEI			(1 << 0)
#define SPI_INT_TXOI			(1 << 1)
#define SPI_INT_RXUI			(1 << 2)
#define SPI_INT_RXOI			(1 << 3)
#define SPI_INT_RXFI			(1 << 4)
#define SPI_INT_MSTI			(1 << 5)

static inline u32 auge_readl(struct auge_spi_s *auge_spi, u32 offset)
{
	return __raw_readl(auge_spi->regs + offset);
}

static inline u16 auge_readw(struct auge_spi_s *auge_spi, u32 offset)
{
	return __raw_readw(auge_spi->regs + offset);
}

static inline void auge_writel(struct auge_spi_s *auge_spi, u32 offset, u32 val)
{
	__raw_writel(val, auge_spi->regs + offset);
}

static inline void auge_writew(struct auge_spi_s *auge_spi, u32 offset, u16 val)
{
	__raw_writew(val, auge_spi->regs + offset);
}

static inline u32 auge_read_io_reg(struct auge_spi_s *auge_spi, u32 offset)
{
	return auge_readl(auge_spi, offset);

}

static inline void auge_write_io_reg(struct auge_spi_s *auge_spi, u32 offset, u32 val)
{
	auge_writel(auge_spi, offset, val);
}

static inline void spi_set_clk(struct auge_spi_s *auge_spi, u32 div)
{
	auge_writel(auge_spi, AUGE_SPI_BAUDR, div);
}

static inline void spi_enable_chip(struct auge_spi_s *auge_spi, int enable)
{
	auge_writel(auge_spi, AUGE_SPI_SSIENR, (enable ? 1 : 0));
}

static inline void spi_shutdown_chip(struct auge_spi_s *auge_spi)
{
	spi_enable_chip(auge_spi, 0);
	spi_set_clk(auge_spi, 0);
}


/* Disable IRQ bits */
static inline void spi_mask_intr(struct auge_spi_s *auge_spi, u32 mask)
{
	u32 new_mask;

	new_mask = auge_readl(auge_spi, AUGE_SPI_IMR) & ~mask;
	auge_writel(auge_spi, AUGE_SPI_IMR, new_mask);
}

/* Enable IRQ bits */
static inline void spi_umask_intr(struct auge_spi_s *auge_spi, u32 mask)
{
	u32 new_mask;

	new_mask = auge_readl(auge_spi, AUGE_SPI_IMR) | mask;
	auge_writel(auge_spi, AUGE_SPI_IMR, new_mask);
}

/*
 * This does disable the SPI controller, interrupts, and re-enable the
 * controller back. Transmit and receive FIFO buffers are cleared when the
 * device is disabled.
 */
static inline void spi_reset_chip(struct auge_spi_s *auge_spi)
{
	spi_enable_chip(auge_spi, 0);
	spi_mask_intr(auge_spi, 0xff);
	spi_enable_chip(auge_spi, 1);
}

#endif
