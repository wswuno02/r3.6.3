#include <linux/module.h>
#include <linux/err.h>
#include <linux/platform_device.h>

#include <asm/io.h>
#include <linux/slab.h>
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/gpio/consumer.h>
#include <linux/gpio.h>

#define DRV_NAME "gpio_test"

#define GPIO_TEST_DEBUG (1)

#ifdef GPIO_TEST_DEBUG
#define DBG(fmt, args...)                                                        \
	do {                                                                     \
		printk("[GPIO_TEST] (%d, %s) " fmt, __LINE__, __func__, ##args); \
	} while (0)
#endif

#define MAX_AIOC_NUM 1
#define MAX_PIOC_NUM 5
#define MAX_PIOC1_NUM 5
#define MAX_PIOC2_NUM 4
#define MAX_MIPI_NUM 5

struct gpio_test_data {
	struct device *dev;
	struct gpio_desc *a_gpio[MAX_AIOC_NUM];
	struct gpio_desc *p0_gpio[MAX_PIOC_NUM];
	struct gpio_desc *p1_gpio[MAX_PIOC1_NUM];
	struct gpio_desc *p2_gpio[MAX_PIOC2_NUM];
	struct gpio_desc *mipitx_gpio[MAX_MIPI_NUM];
};

enum gpio_group {
	AIOC,
	PIOC0,
	PIOC1,
	PIOC2,
	MIPITX,
	GROUP_NUM,
};

struct gpio_group_desc {
	enum gpio_group id;
	const char *name;
	int num;
};

struct gpio_group_desc gpio_map[] = {
	{ AIOC, "aioc", MAX_AIOC_NUM },    { PIOC0, "pioc0", MAX_PIOC_NUM },   { PIOC1, "pioc1", MAX_PIOC1_NUM },
	{ PIOC2, "pioc2", MAX_PIOC2_NUM }, { MIPITX, "mipitx", MAX_MIPI_NUM },
};

static int do_single_unitest_val(int group_type, struct gpio_desc *gd)
{
	int logic_val, raw_val;
	int id = desc_to_gpio(gd);

	logic_val = gpiod_get_value(gd);
	raw_val = gpiod_get_raw_value(gd);
	DBG("Group %s, gpio[%d] %s:\n\tlogical val = %d, raw val = %d\n", gpio_map[group_type].name, id,
	    ((logic_val != raw_val) ? "(ACTIVE_LOW)" : ""), logic_val, raw_val);
	DBG("\tSet logical value to 1\n");
	gpiod_set_value(gd, 1);
	logic_val = gpiod_get_value(gd);
	raw_val = gpiod_get_raw_value(gd);
	DBG("\tlogical val = %d, raw val = %d\n", logic_val, raw_val);
	if (logic_val != 1) {
		DBG("[FAIL] ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n");
		return -1;
	}
	DBG("\tSet logical value to 0\n");
	gpiod_set_value(gd, 0);
	logic_val = gpiod_get_value(gd);
	raw_val = gpiod_get_raw_value(gd);
	DBG("\tlogical val = %d, raw val = %d\n", logic_val, raw_val);
	if (logic_val != 0) {
		DBG("[FAIL] ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n");
		return -1;
	}
	return 0;
}

static int unitest_val(struct gpio_test_data *gpio_test)
{
	int i, k;

	DBG("-----unitest_val-----\n");
	DBG("Initial condition: Output is 0\n");
	DBG("Action:\t1. Verify logical output value 0->1 and 1->0\n\t2. Detect logic inversion case\n");

	for (k = 0; k < GROUP_NUM; k++) {
		switch (k) {
		case AIOC:
			for (i = 0; i < gpio_map[AIOC].num; i++) {
				if (do_single_unitest_val(k, gpio_test->a_gpio[i]))
					return -1;
			}
			break;
		case PIOC0:
			for (i = 0; i < gpio_map[PIOC0].num; i++) {
				if (do_single_unitest_val(k, gpio_test->p0_gpio[i]))
					return -1;
			}
			break;
		case PIOC1:
			for (i = 0; i < gpio_map[PIOC1].num; i++) {
				if (do_single_unitest_val(k, gpio_test->p1_gpio[i]))
					return -1;
			}
			break;
		case PIOC2:
			for (i = 0; i < gpio_map[PIOC2].num; i++) {
				if (do_single_unitest_val(k, gpio_test->p2_gpio[i]))
					return -1;
			}
			break;
		case MIPITX:
			for (i = 0; i < gpio_map[MIPITX].num; i++) {
				if (do_single_unitest_val(k, gpio_test->mipitx_gpio[i]))
					return -1;
			}
			break;
		}
	}

	return 0;
}

static int do_single_unitest_direction(int group_type, struct gpio_desc *gd)
{
	int ret;
	int dir;
	int id = desc_to_gpio(gd);
	int logic_val;

	DBG("Group %s, gpio[%d]\n", gpio_map[group_type].name, id);

	gpiod_set_value(gd, 1);
	DBG("\tSwitch direction to input\n");
	ret = gpiod_direction_input(gd);
	if (ret < 0) {
		DBG("[FAIL] Cannot set input direction , errno=%d\n", ret);
		return -1;
	}

	logic_val = gpiod_get_value(gd);
	DBG("\tInput logic val = %d\n", logic_val);
	if (logic_val == 1) {
		DBG("[WARN] Input value asserted. Is it pulled by circuit?\n");
	}

	DBG("\tSwitch direction to output\n");
	ret = gpiod_direction_output(gd, GPIOF_DIR_OUT);
	if (ret < 0) {
		DBG("[FAIL] Cannot set output direction , errno=%d\n", ret);
		return -1;
	}

	return 0;
}

static int unitest_direction(struct gpio_test_data *gpio_test)
{
	int i, k;

	DBG("-----unitest_direction-----\n");
	DBG("Initial condition: Output is 0\n");
	DBG("Action: Set output logical value to 1, change to input, then read logic_val\n");
	for (k = 0; k < GROUP_NUM; k++) {
		switch (k) {
		case AIOC:
			for (i = 0; i < gpio_map[AIOC].num; i++) {
				if (do_single_unitest_direction(k, gpio_test->a_gpio[i]))
					return -1;
			}
			break;
		case PIOC0:
			for (i = 0; i < gpio_map[PIOC0].num; i++) {
				if (do_single_unitest_direction(k, gpio_test->p0_gpio[i]))
					return -1;
			}
			break;
		case PIOC1:
			for (i = 0; i < gpio_map[PIOC1].num; i++) {
				if (do_single_unitest_direction(k, gpio_test->p1_gpio[i]))
					return -1;
			}
			break;
		case PIOC2:
			for (i = 0; i < gpio_map[PIOC2].num; i++) {
				if (do_single_unitest_direction(k, gpio_test->p2_gpio[i]))
					return -1;
			}
			break;
		}
	}

	return 0;
}

static int gpio_test_probe(struct platform_device *pdev)
{
	struct gpio_test_data *gpio_test = NULL;
	int i, ret;

	gpio_test = devm_kzalloc(&pdev->dev, sizeof(struct gpio_test_data), GFP_KERNEL);
	if (!gpio_test)
		return -ENOMEM;
	gpio_test->dev = &pdev->dev;

	platform_set_drvdata(pdev, gpio_test);

	/* Setup gpio */
	gpio_test->a_gpio[AIOC] = devm_gpiod_get(gpio_test->dev, gpio_map[AIOC].name, GPIOD_OUT_LOW);

	if (IS_ERR(gpio_test->a_gpio[AIOC])) {
		DBG("[AIOC]GPIO driver not available: %s\n", gpio_map[AIOC].name);
	}

	for (i = 0; i < gpio_map[PIOC0].num; i++) {
		gpio_test->p0_gpio[i] = devm_gpiod_get_index(gpio_test->dev, gpio_map[PIOC0].name, i, GPIOD_OUT_LOW);

		if (IS_ERR(gpio_test->p0_gpio[i])) {
			DBG("[PIOC0]GPIO driver not available: %s[%d]\n", gpio_map[PIOC0].name, i);
		}
	}

	for (i = 0; i < gpio_map[PIOC1].num; i++) {
		gpio_test->p1_gpio[i] = devm_gpiod_get_index(gpio_test->dev, gpio_map[PIOC1].name, i, GPIOD_OUT_LOW);

		if (IS_ERR(gpio_test->p1_gpio[i])) {
			DBG("[PIOC1]GPIO driver not available: %s[%d]\n", gpio_map[PIOC1].name, i);
		}
	}

	for (i = 0; i < gpio_map[PIOC2].num; i++) {
		gpio_test->p2_gpio[i] = devm_gpiod_get_index(gpio_test->dev, gpio_map[PIOC2].name, i, GPIOD_OUT_LOW);

		if (IS_ERR(gpio_test->p2_gpio[i])) {
			DBG("[PIOC2]GPIO driver not available: %s[%d]\n", gpio_map[PIOC2].name, i);
		}
	}

	for (i = 0; i < gpio_map[MIPITX].num; i++) {
		gpio_test->mipitx_gpio[i] =
		        devm_gpiod_get_index(gpio_test->dev, gpio_map[MIPITX].name, i, GPIOD_OUT_LOW);

		if (IS_ERR(gpio_test->mipitx_gpio[i])) {
			DBG("[MIPITX]GPIO driver not available: %s[%d]\n", gpio_map[MIPITX].name, i);
		}
	}

	ret = unitest_val(gpio_test);
	if (ret < 0) {
		DBG("unitest_val fail!\n");
	} else {
		DBG("unitest_val pass!\n");
	}

	ret = unitest_direction(gpio_test);
	if (ret < 0) {
		DBG("unitest_direction fail!\n");
	} else {
		DBG("unitest_direction pass!\n");
	}

	DBG("-----GPIO unitest pass!-----\n");

	DBG("GPIO test module probed.\n");
	return 0;
}

static int gpio_test_remove(struct platform_device *pdev)
{
	struct gpio_test_data *gpio_test = platform_get_drvdata(pdev);
	devm_kfree(&pdev->dev, gpio_test);
	platform_set_drvdata(pdev, NULL);
	return 0;
}

static const struct of_device_id gpio_test_dt_ids[] = {
	{ .compatible = "augentix,gpio-test" },
	{},
};
MODULE_DEVICE_TABLE(of, gpio_test_dt_ids);

static struct platform_driver gpio_test_driver = {
	.probe = gpio_test_probe,
	.remove = gpio_test_remove,
	.driver =
	        {
	                .owner = THIS_MODULE,
	                .name = DRV_NAME,
	                .of_match_table = gpio_test_dt_ids,
	        },
};
module_platform_driver(gpio_test_driver);

MODULE_AUTHOR("Louis Yang, Augentix <louis.yang@augentix.com>");
MODULE_DESCRIPTION("HC1703_1723_1753_1783S gpio test driver");
MODULE_LICENSE("GPL");
