#include <linux/module.h>
#include <linux/slab.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/platform_device.h>

#include <linux/spinlock.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/of_address.h>

#include <linux/gpio/driver.h>
#include <linux/gpio.h>

#define DRIVER_NAME "hc1703_1723_1753_1783s_gpio"
#define PIOC0_PIN_OFFSET 0
#define PIOC1_PIN_OFFSET 32
#define PIOC2_PIN_OFFSET 64
#define AIOC_PIN_OFFSET 68
#define MIPI_TX_PIN_OFFSET 81
#define MIPI_RX0_PIN_OFFSET 91
#define MIPI_RX1_PIN_OFFSET 101

#define AIOC_O_OFFSET 0x0
#define AIOC_OE_OFFSET 0x4
#define AIOC_I_OFFSET 0x8
#define PIOC0_O_OFFSET 0xC
#define PIOC0_OE_OFFSET 0x10
#define PIOC0_I_OFFSET 0x14
#define PIOC1_O_OFFSET 0x18
#define PIOC1_OE_OFFSET 0x1C
#define PIOC1_I_OFFSET 0x20
#define PIOC2_O_OFFSET 0x24
#define PIOC2_OE_OFFSET 0x28
#define PIOC2_I_OFFSET 0x30
#define MIPI_TX_O_OFFSET 0x34
#define MIPI_RX0_I_OFFSET 0x38
#define MIPI_RX1_I_OFFSET 0x3C

//#define DEBUG

#ifdef DEBUG
#define DBG(fmt, args...)                                                          \
	do {                                                                       \
		printk("[GPIO_DRIVER] (%d, %s) " fmt, __LINE__, __func__, ##args); \
	} while (0)
#endif

struct hc1703_1723_1753_1783s_gpio_drvdata {
	struct device *dev;
	void __iomem *base;
	struct gpio_chip gpio_chip;
	struct pinctrl_gpio_range grange;
	unsigned int *def_conf;
	spinlock_t lock;
};

enum gpio_bank {
	AIOC_bank,
	PIOC0_bank,
	PIOC1_bank,
	PIOC2_bank,
	MIPI0_TX,
	BANK_NUM,
};

struct gpio_bank_desc {
	enum gpio_bank bank;
	const char *name;
	int num;
};

static struct gpio_bank_desc hc1703_1723_1753_1783s_bank_map[] = {
	{ AIOC_bank, "aioc_gpio", 2 },   { PIOC0_bank, "pioc0_gpio", 2 },     { PIOC1_bank, "pioc1_gpio", 2 },
	{ PIOC2_bank, "pioc2_gpio", 2 }, { MIPI0_TX, "mipi_tx_gpio_o_0", 1 }, { BANK_NUM, "bank_num", 13 },
};

enum gpio_type {
	AIOC,
	PIOC0,
	PIOC1,
	PIOC2,
	MIPI_TX,
	MIPI_RX0,
	MIPI_RX1,
};

struct gpio_desc {
	const char *name;
	enum gpio_type type;
	int pin;
};

static struct gpio_desc hc1703_1723_1753_1783s_gpio_addr[] = {
	{ "PAD_SPI0_SCK", PIOC0, 0 },
	{ "PAD_SPI0_SDI", PIOC0, 1 },
	{ "PAD_SPI0_SDO", PIOC0, 2 },
	{ "PAD_EIRQ0", PIOC0, 3 },
	{ "PAD_SD_CD", PIOC0, 4 },
	{ "PAD_SD_D2", PIOC0, 5 },
	{ "PAD_SD_D3", PIOC0, 6 },
	{ "PAD_SD_CMD", PIOC0, 7 },
	{ "PAD_SD_CK", PIOC0, 8 },
	{ "PAD_SD_D0", PIOC0, 9 },
	{ "PAD_SD_D1", PIOC0, 10 },
	{ "PAD_GPIO3", PIOC0, 11 },
	{ "PAD_PWM1", PIOC0, 12 },
	{ "PAD_PWM4", PIOC0, 13 },
	{ "PAD_I2C1_SCL", PIOC0, 14 },
	{ "PAD_I2C1_SDA", PIOC0, 15 },
	{ "PAD_UART1_TXD", PIOC0, 16 },
	{ "PAD_UART1_RXD", PIOC0, 17 },
	{ "PAD_UART2_TXD", PIOC0, 18 },
	{ "PAD_UART2_RXD", PIOC0, 19 },
	{ "PAD_QSPI_CE_N", PIOC0, 20 },
	{ "PAD_QSPI_D1", PIOC0, 21 },
	{ "PAD_QSPI_D2", PIOC0, 22 },
	{ "PAD_QSPI_D3", PIOC0, 23 },
	{ "PAD_QSPI_CK", PIOC0, 24 },
	{ "PAD_QSPI_D0", PIOC0, 25 },
	{ "PAD_EPHY_RST", PIOC0, 26 },
	{ "PAD_EMAC_TX_CK", PIOC0, 27 },
	{ "PAD_EMAC_TX_CTL", PIOC0, 28 },
	{ "PAD_EMAC_TX_D3", PIOC0, 29 },
	{ "PAD_EMAC_TX_D2", PIOC0, 30 },
	{ "PAD_EMAC_TX_D1", PIOC0, 31 },
	{ "PAD_EMAC_TX_D0", PIOC1, 32 },
	{ "PAD_EMAC_RX_CK", PIOC1, 33 },
	{ "PAD_EMAC_RX_D0", PIOC1, 34 },
	{ "PAD_EMAC_RX_D1", PIOC1, 35 },
	{ "PAD_EMAC_RX_D2", PIOC1, 36 },
	{ "PAD_EMAC_RX_D3", PIOC1, 37 },
	{ "PAD_EMAC_RX_CTL", PIOC1, 38 },
	{ "PAD_EMAC_MDC", PIOC1, 39 },
	{ "PAD_EMAC_MDIO", PIOC1, 40 },
	{ "PAD_SPI1_SDI", PIOC1, 41 },
	{ "PAD_SPI1_SDO", PIOC1, 42 },
	{ "PAD_SPI1_SCK", PIOC1, 43 },
	{ "PAD_GPIO7", PIOC1, 44 },
	{ "PAD_I2C0_SDA", PIOC1, 45 },
	{ "PAD_I2C0_SCL", PIOC1, 46 },
	{ "PAD_SENSOR_CLK", PIOC1, 47 },
	{ "PAD_PWM2", PIOC1, 48 },
	{ "PAD_SENSOR_RSTB", PIOC1, 49 },
	{ "PAD_SENSOR_PWDN", PIOC1, 50 },
	{ "PAD_GPIO4", PIOC1, 51 },
	{ "PAD_GPIO5", PIOC1, 52 },
	{ "PAD_GPIO6", PIOC1, 53 },
	{ "PAD_I2S_TX_CK", PIOC1, 54 },
	{ "PAD_I2S_RX_CK", PIOC1, 55 },
	{ "PAD_I2S_RX_SD", PIOC1, 56 },
	{ "PAD_I2S_TX_SD", PIOC1, 57 },
	{ "PAD_I2S_RX_WS", PIOC1, 58 },
	{ "PAD_I2S_TX_WS", PIOC1, 59 },
	{ "PAD_GPIO2", PIOC1, 60 },
	{ "PAD_UART0_TXD", PIOC1, 61 },
	{ "PAD_UART0_RXD", PIOC1, 62 },
	{ "PAD_EIRQ1", PIOC1, 63 },
	{ "PAD_PWM3", PIOC2, 64 },
	{ "PAD_PWM0", PIOC2, 65 },
	{ "PAD_GPIO0", PIOC2, 66 },
	{ "PAD_GPIO1", PIOC2, 67 },
	{ "PAD_DVP_0", AIOC, 68 },
	{ "PAD_DVP_1", AIOC, 69 },
	{ "PAD_DVP_2", AIOC, 70 },
	{ "PAD_DVP_3", AIOC, 71 },
	{ "PAD_DVP_4", AIOC, 72 },
	{ "PAD_DVP_5", AIOC, 73 },
	{ "PAD_DVP_6", AIOC, 74 },
	{ "PAD_DVP_7", AIOC, 75 },
	{ "PAD_DVP_8", AIOC, 76 },
	{ "PAD_POWER_CTRL_0", AIOC, 77 },
	{ "PAD_POWER_CTRL_1", AIOC, 78 },
	{ "PAD_AO_BUTTON", AIOC, 79 },
	{ "PAD_AO_WAKEUP", AIOC, 80 },
	{ "PAD_MIPI_TX_LN0_N", MIPI_TX, 81 },
	{ "PAD_MIPI_TX_LN0_P", MIPI_TX, 82 },
	{ "PAD_MIPI_TX_LN1_N", MIPI_TX, 83 },
	{ "PAD_MIPI_TX_LN1_P", MIPI_TX, 84 },
	{ "PAD_MIPI_TX_LN2_N", MIPI_TX, 85 },
	{ "PAD_MIPI_TX_LN2_P", MIPI_TX, 86 },
	{ "PAD_MIPI_TX_LN3_N", MIPI_TX, 87 },
	{ "PAD_MIPI_TX_LN3_P", MIPI_TX, 88 },
	{ "PAD_MIPI_TX_LN4_N", MIPI_TX, 89 },
	{ "PAD_MIPI_TX_LN4_P", MIPI_TX, 90 },
	{ "PAD_MIPI_RX0_LN0_N", MIPI_RX0, 91 },
	{ "PAD_MIPI_RX0_LN0_P", MIPI_RX0, 92 },
	{ "PAD_MIPI_RX0_LN1_N", MIPI_RX0, 93 },
	{ "PAD_MIPI_RX0_LN1_P", MIPI_RX0, 94 },
	{ "PAD_MIPI_RX0_LN2_N", MIPI_RX0, 95 },
	{ "PAD_MIPI_RX0_LN2_P", MIPI_RX0, 96 },
	{ "PAD_MIPI_RX0_LN3_N", MIPI_RX0, 97 },
	{ "PAD_MIPI_RX0_LN3_P", MIPI_RX0, 98 },
	{ "PAD_MIPI_RX0_LN4_N", MIPI_RX0, 99 },
	{ "PAD_MIPI_RX0_LN4_P", MIPI_RX0, 100 },
	{ "PAD_MIPI_RX1_LN0_N", MIPI_RX1, 101 },
	{ "PAD_MIPI_RX1_LN0_P", MIPI_RX1, 102 },
	{ "PAD_MIPI_RX1_LN1_N", MIPI_RX1, 103 },
	{ "PAD_MIPI_RX1_LN1_P", MIPI_RX1, 104 },
	{ "PAD_MIPI_RX1_LN2_N", MIPI_RX1, 105 },
	{ "PAD_MIPI_RX1_LN2_P", MIPI_RX1, 106 },
	{ "PAD_MIPI_RX1_LN3_N", MIPI_RX1, 107 },
	{ "PAD_MIPI_RX1_LN3_P", MIPI_RX1, 108 },
	{ "PAD_MIPI_RX1_LN4_N", MIPI_RX1, 109 },
	{ "PAD_MIPI_RX1_LN4_P", MIPI_RX1, 110 },
};

static int offset_to_pintype(unsigned offset)
{
	enum gpio_type type;

	if (offset < 32) {
		type = PIOC0;
	} else if ((offset >= 32) && (offset < 64)) {
		type = PIOC1;
	} else if ((offset >= 64) && (offset < 68)) {
		type = PIOC2;
	} else if ((offset >= 68) && (offset < 81)) {
		type = AIOC;
	} else if ((offset >= 81) && (offset < 91)) {
		type = MIPI_TX;
	} else if ((offset >= 91) && (offset < 101)) {
		type = MIPI_RX0;
	} else if ((offset >= 101) && (offset < 111)) {
		type = MIPI_RX1;
	} else {
		return -EINVAL;
	}

	return type;
}

static int hc1703_1723_1753_1783s_gpio_get_value(struct gpio_chip *gc, unsigned offset)
{
	struct hc1703_1723_1753_1783s_gpio_drvdata *drvdata =
	        container_of(gc, struct hc1703_1723_1753_1783s_gpio_drvdata, gpio_chip);
	void __iomem *reg;
	u32 reg_val, oe_val, type;
	u32 o_off, oe_off, i_off, pin_off;

	type = offset_to_pintype(offset);
	if (type < 0) {
		return -EINVAL;
	}

	switch (type) {
	case AIOC:
		o_off = AIOC_O_OFFSET;
		oe_off = AIOC_OE_OFFSET;
		i_off = AIOC_I_OFFSET;
		pin_off = AIOC_PIN_OFFSET;
		break;

	case PIOC0:
		o_off = PIOC0_O_OFFSET;
		oe_off = PIOC0_OE_OFFSET;
		i_off = PIOC0_I_OFFSET;
		pin_off = PIOC0_PIN_OFFSET;
		break;

	case PIOC1:
		o_off = PIOC1_O_OFFSET;
		oe_off = PIOC1_OE_OFFSET;
		i_off = PIOC1_I_OFFSET;
		pin_off = PIOC1_PIN_OFFSET;
		break;

	case PIOC2:
		o_off = PIOC2_O_OFFSET;
		oe_off = PIOC2_OE_OFFSET;
		i_off = PIOC2_I_OFFSET;
		pin_off = PIOC2_PIN_OFFSET;
		break;

	case MIPI_TX:
		o_off = MIPI_TX_O_OFFSET;
		pin_off = MIPI_TX_PIN_OFFSET;

		reg = drvdata->base + o_off;
		reg_val = readl_relaxed(reg);

		reg_val = (reg_val & (1 << (offset - pin_off)));
		return ((reg_val >> (offset - pin_off)) & 0x01);
		break;

	case MIPI_RX0:
		i_off = MIPI_RX0_I_OFFSET;
		pin_off = MIPI_RX0_PIN_OFFSET;

		reg = drvdata->base + i_off;
		reg_val = (readl_relaxed(reg) & (1 << (offset - pin_off)));

		return ((reg_val >> (offset - pin_off)) & 0x01);
		break;

	case MIPI_RX1:
		i_off = MIPI_RX1_I_OFFSET;
		pin_off = MIPI_RX1_PIN_OFFSET;

		reg = drvdata->base + i_off;
		reg_val = (readl_relaxed(reg) & (1 << (offset - pin_off)));

		return ((reg_val >> (offset - pin_off)) & 0x01);
		break;

	default:
		return -EINVAL;
		break;
	}

	reg = drvdata->base + oe_off;
	oe_val = (readl_relaxed(reg) & (1 << (offset - pin_off)));

	if (oe_val) {
		reg = drvdata->base + o_off;
		reg_val = (readl_relaxed(reg) & (1 << (offset - pin_off)));
		return ((reg_val >> (offset - pin_off)) & 0x01);
	} else {
		reg = drvdata->base + i_off;
		reg_val = (readl_relaxed(reg) & (1 << (offset - pin_off)));

		return ((reg_val >> (offset - pin_off)) & 0x01);
	}
}

static void hc1703_1723_1753_1783s_gpio_set_value(struct gpio_chip *gc, unsigned offset, int val)
{
	struct hc1703_1723_1753_1783s_gpio_drvdata *drvdata =
	        container_of(gc, struct hc1703_1723_1753_1783s_gpio_drvdata, gpio_chip);
	void __iomem *reg;
	unsigned long flags;
	u32 reg_val, type, o_off, pin_off;

	type = offset_to_pintype(offset);
	if (type < 0) {
		return;
	}

	switch (type) {
	case AIOC:
		o_off = AIOC_O_OFFSET;
		pin_off = AIOC_PIN_OFFSET;
		break;

	case PIOC0:
		o_off = PIOC0_O_OFFSET;
		pin_off = PIOC0_PIN_OFFSET;
		break;

	case PIOC1:
		o_off = PIOC1_O_OFFSET;
		pin_off = PIOC1_PIN_OFFSET;
		break;

	case PIOC2:
		o_off = PIOC2_O_OFFSET;
		pin_off = PIOC2_PIN_OFFSET;
		break;

	case MIPI_TX:
		o_off = MIPI_TX_O_OFFSET;
		pin_off = MIPI_TX_PIN_OFFSET;
		break;

	case MIPI_RX0:
		return;
		break;

	case MIPI_RX1:
		return;
		break;

	default:
		return;
		break;
	}

	reg = drvdata->base + o_off;

	spin_lock_irqsave(&drvdata->lock, flags);
	reg_val = readl_relaxed(reg);
	reg_val &= ~(1 << (offset - pin_off));
	if (val) {
		reg_val |= (1 << (offset - pin_off));
	}
	writel(reg_val, reg);
	spin_unlock_irqrestore(&drvdata->lock, flags);
}

static int hc1703_1723_1753_1783s_gpio_set_direction(struct gpio_chip *gc, unsigned offset, u32 oe)
{
	struct hc1703_1723_1753_1783s_gpio_drvdata *drvdata =
	        container_of(gc, struct hc1703_1723_1753_1783s_gpio_drvdata, gpio_chip);
	void __iomem *reg;
	unsigned long flags;
	u32 reg_val, type, oe_off, pin_off;

	type = offset_to_pintype(offset);
	if (type < 0) {
		return -EINVAL;
	}

	switch (type) {
	case AIOC:
		oe_off = AIOC_OE_OFFSET;
		pin_off = AIOC_PIN_OFFSET;
		break;

	case PIOC0:
		oe_off = PIOC0_OE_OFFSET;
		pin_off = PIOC0_PIN_OFFSET;
		break;

	case PIOC1:
		oe_off = PIOC1_OE_OFFSET;
		pin_off = PIOC1_PIN_OFFSET;
		break;

	case PIOC2:
		oe_off = PIOC2_OE_OFFSET;
		pin_off = PIOC2_PIN_OFFSET;
		break;

	/* MIPI couldn't change direction */
	case MIPI_TX:
		return 0;
		break;

	case MIPI_RX0:
		return 0;
		break;

	case MIPI_RX1:
		return 0;
		break;

	default:
		return -EINVAL;
		break;
	}

	reg = drvdata->base + oe_off;
	spin_lock_irqsave(&drvdata->lock, flags);
	reg_val = readl_relaxed(reg);

	reg_val &= ~(1 << (offset - pin_off));
	if (oe) {
		reg_val |= (oe << (offset - pin_off));
	}

	writel(reg_val, reg);
	spin_unlock_irqrestore(&drvdata->lock, flags);

	return 0;
}

static int hc1703_1723_1753_1783s_gpio_direction_input(struct gpio_chip *gc, unsigned offset)
{
	return hc1703_1723_1753_1783s_gpio_set_direction(gc, offset, 0);
}

static int hc1703_1723_1753_1783s_gpio_direction_output(struct gpio_chip *gc, unsigned offset, int value)
{
	hc1703_1723_1753_1783s_gpio_set_value(gc, offset, value);
	return hc1703_1723_1753_1783s_gpio_set_direction(gc, offset, 1);
}

static const struct gpio_chip hc1703_1723_1753_1783s_gpio_chip = {
	.set = hc1703_1723_1753_1783s_gpio_set_value,
	.get = hc1703_1723_1753_1783s_gpio_get_value,
	.direction_input = hc1703_1723_1753_1783s_gpio_direction_input,
	.direction_output = hc1703_1723_1753_1783s_gpio_direction_output,
	.owner = THIS_MODULE,
};

static int hc1703_1723_1753_1783s_gpio_chip_register(struct platform_device *pdev,
                                                     struct hc1703_1723_1753_1783s_gpio_drvdata *drvdata)
{
	struct gpio_chip *gc;
	int ret;
	static int gpio;
	drvdata->gpio_chip = hc1703_1723_1753_1783s_gpio_chip;

	gc = &drvdata->gpio_chip;
	{
		gc->base = gpio;
		gc->ngpio = ARRAY_SIZE(hc1703_1723_1753_1783s_gpio_addr);
		gc->dev = drvdata->dev;
		gc->of_node = gc->dev->of_node;
		gc->label = pdev->name;
	}

	ret = gpiochip_add(gc);
	if (ret) {
		dev_err(&pdev->dev, "Failed to register gpio_chip %s for GPIO controller, error code: %d\n", gc->label,
		        ret);
		return ret;
	}

	return 0;
}

static int hc1703_1723_1753_1783s_gpio_chip_init(struct platform_device *pdev,
                                                 struct hc1703_1723_1753_1783s_gpio_drvdata *drvdata)
{
	struct device *dev = &pdev->dev;
	struct device_node *np = pdev->dev.of_node;
	unsigned int *conf = drvdata->def_conf;
	int length, num, i, k, ret;

	/* parse default setting from dts */
	for (length = 0, i = 0; i < BANK_NUM; i++) {
		num = of_property_count_u32_elems(np, hc1703_1723_1753_1783s_bank_map[i].name);
		if (num < 0) {
			dev_err(dev, "No '%s' property found\n", hc1703_1723_1753_1783s_bank_map[i].name);
			return -EINVAL;
		}
		length += num;
	}

	conf = devm_kzalloc(dev, length * sizeof(*conf), GFP_KERNEL);
	if (!conf)
		return -ENOMEM;

	/* assign default setting to conf array */
	for (length = 0, i = 0; i < BANK_NUM; i++) {
		ret = of_property_read_u32_array(np, hc1703_1723_1753_1783s_bank_map[i].name, conf + length,
		                                 hc1703_1723_1753_1783s_bank_map[i].num);
		if (ret < 0) {
			dev_err(dev, "No '%s' property found\n", hc1703_1723_1753_1783s_bank_map[i].name);
			return -EINVAL;
		}
		length += hc1703_1723_1753_1783s_bank_map[i].num;
	}

	/* skip gpio-input */
	for (i = 0, k = 0; i < hc1703_1723_1753_1783s_bank_map[BANK_NUM].num; i++) {
		if ((i + 1) % 3) {
			writel(conf[k], drvdata->base + i * 4);
			k++;
		}
		if (k == (length - hc1703_1723_1753_1783s_bank_map[MIPI0_TX].num)) {
			writel(conf[k], drvdata->base + MIPI_TX_O_OFFSET);
			break;
		}
	}

	return 0;
}

static int hc1703_1723_1753_1783s_gpio_probe(struct platform_device *pdev)
{
	struct hc1703_1723_1753_1783s_gpio_drvdata *gchip;
	struct resource *res;
	struct device *dev = &pdev->dev;
	int ret;

	dev_info(&pdev->dev, "Initializing HC1703_1723_1753_1783S gpio driver...\n");

	gchip = devm_kzalloc(dev, sizeof(*gchip), GFP_KERNEL);
	if (!gchip)
		return -ENOMEM;
	gchip->dev = &pdev->dev;
	spin_lock_init(&gchip->lock);

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);

	gchip->base = devm_ioremap_resource(&pdev->dev, res);
	if (IS_ERR(gchip->base)) {
		dev_err(dev, "Failed to map I/O address!\n");
	}

	ret = hc1703_1723_1753_1783s_gpio_chip_init(pdev, gchip);
	if (ret) {
		dev_err(dev, "GPIO initialization failed: %d\n", ret);
		return ret;
	}

	ret = hc1703_1723_1753_1783s_gpio_chip_register(pdev, gchip);
	if (ret) {
		dev_err(dev, "GPIO registration failed: %d\n", ret);
		return ret;
	}

	platform_set_drvdata(pdev, gchip);

	dev_info(dev, "hc1703_1723_1753_1783s gpio driver initialized\n");

	return 0;
}

static int hc1703_1723_1753_1783s_gpio_remove(struct platform_device *pdev)
{
	struct hc1703_1723_1753_1783s_gpio_drvdata *dev;

	dev = platform_get_drvdata(pdev);
	gpiochip_remove(&dev->gpio_chip);

	return 0;
}

static const struct of_device_id hc1703_1723_1753_1783s_gpio_of_match[] = {
	{ .compatible = "augentix,gpio" },
	{},
};

MODULE_DEVICE_TABLE(of, hc1703_1723_1753_1783s_gpio_of_match);

static struct platform_driver hc1703_1723_1753_1783s_gpio_driver = {
	.driver = {
		.name = DRIVER_NAME,
		.owner = THIS_MODULE,
		.of_match_table = hc1703_1723_1753_1783s_gpio_of_match,
	},
	.probe = hc1703_1723_1753_1783s_gpio_probe,
	.remove = hc1703_1723_1753_1783s_gpio_remove,
};

module_platform_driver(hc1703_1723_1753_1783s_gpio_driver);

MODULE_AUTHOR("Louis Yang, Augentix <louis.yang@augentix.com>");
MODULE_DESCRIPTION("Augentix HC1703_1723_1753_1783S gpio control driver");
MODULE_LICENSE("GPL v2");
