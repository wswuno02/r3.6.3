/*
 * Device Tree Support for Augentix HC18xx SoCs
 *
 * This file is licensed under the terms of the GNU General Public
 * License version 2.  This program is licensed "as is" without any
 * warranty of any kind, whether express or implied.
 */

#include <linux/init.h>
#include <linux/mm.h>
#include <linux/memblock.h>
#include <linux/clk-provider.h>
#include <linux/clocksource.h>
#include <linux/of_platform.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/setup.h>
#include <mach/hc18xx.h>
#include <linux/delay.h>

unsigned long g_dram_start;
EXPORT_SYMBOL(g_dram_start);

unsigned long g_dram_size;
EXPORT_SYMBOL(g_dram_size);

unsigned long g_vb_size;
EXPORT_SYMBOL(g_vb_size);

extern u64 hc18xx_get_clocksource(void);
extern char __sram_text_start[], __sram_text_end[];

/*
 * Currently we use "augentix,hc18xx" as a common machine identifier
 */
static const char *const hc18xx_board_dt_compat[] = {
	"augentix,hc18xx", NULL,
};

static struct map_desc hc18xx_io_desc[] __initdata = {
	/*
	{
		.virtual = HC18XX_SYSRAM_VA,
		.pfn     = __phys_to_pfn(HC18XX_SYSRAM_PA),
		.length  = SZ_4K,
		.type    = MT_MEMORY_RWX,
	},
	*/
	{
	        .virtual = TO_VA(HC18XX_IOC_PA),
	        .pfn = __phys_to_pfn(HC18XX_IOC_PA),
	        .length = SZ_4K,
	        .type = MT_DEVICE,
	},
	{
	        .virtual = TO_VA(HC18XX_WDT_PA),
	        .pfn = __phys_to_pfn(HC18XX_WDT_PA),
	        .length = SZ_4K,
	        .type = MT_DEVICE,
	},
};

void __init hc18xx_map_io(void)
{
	iotable_init(hc18xx_io_desc, ARRAY_SIZE(hc18xx_io_desc));
}

static void __init hc18xx_reserve(void)
{
	g_dram_start = memblock.memory.regions[0].base;
	g_dram_size = memblock.memory.regions[0].size;

#ifdef CONFIG_DMA_CMA
	g_vb_size =
	        CONFIG_VB_SIZE_MB > CONFIG_CMA_SIZE_MBYTES ? (CONFIG_CMA_SIZE_MBYTES << 20) : (CONFIG_VB_SIZE_MB << 20);
#endif
}

static void __init print_hc18xx_meminfo(void)
{
	printk("HC18xx memory:\n\tDRAM addr:\t0x%08x\n\tDRAM size:\t0x%08x (%u MiB)\n\tCMA size:\t0x%08x (%u MiB)\n\tVB size:\t0x%08x (%u MiB)\n",
	       (unsigned int)g_dram_start, (unsigned int)g_dram_size, (unsigned int)g_dram_size >> 20,
	       (unsigned int)CONFIG_CMA_SIZE_MBYTES << 20, CONFIG_CMA_SIZE_MBYTES, (unsigned int)g_vb_size,
	       (unsigned int)g_vb_size >> 20);
}

#define TLB_MAX_VICTIM 8
static int tlb_victim = 0;

static inline void tlb_page_lockdown(unsigned long lock_addr)
{
	register unsigned long ld_reg; /* lockdown register */
	register unsigned long addr = lock_addr; /* address to be locked down */

	if (tlb_victim >= TLB_MAX_VICTIM)
		return;

	++tlb_victim;
	asm volatile(
	        "mcr	p15, 0, %[addr], c8, c7, 1\n" /* invalidate TLB single entry to ensure that addr is not already in the TLB */
	        "mrc	p15, 0, %[ld_reg], c10, c0, 0\n" /* read the lockdown register */
	        "orr	%[ld_reg], %[ld_reg], #1\n" /* set the preserve bit */
	        "mcr	p15, 0, %[ld_reg], c10, c0, 0\n" /* write to the lockdown register */
	        "ldr	%[addr], [%[addr]]\n" /* read fom the address since TLB will miss */
	        "mrc	p15, 0, %[ld_reg], c10, c0, 0\n" /* read the lockdown register (victim will be incremented) */
	        "bic	%[ld_reg], %[ld_reg], #1\n" /* clear preserve bit */
	        "mcr	p15, 0, %[ld_reg], c10, c0, 0\n" /* write to the lockdown register */
	        : [ld_reg] "=r"(ld_reg), [addr] "+r"(addr)
	        :
	        : "cc");
}

static void __init hc18xx_early_init(void)
{
	/*
	size_t sram_text_sz = __sram_text_end - __sram_text_start;

	memcpy((void *)HC18XX_SRAM_TEXT_VA, __sram_text_start, sram_text_sz);
	*/
	//tlb_page_lockdown(0xffff0000);
	//tlb_page_lockdown(HC18XX_SYSRAM_VA);
}

static void __init hc18xx_machine_init(void)
{
	volatile void __iomem *reg;
	//unsigned long dram_size = get_num_physpages() << PAGE_SHIFT; /* DRAM size in Byte */

	of_platform_populate(NULL, of_default_bus_match_table, NULL, NULL);

	reg = (void *)TO_VA(HC18XX_IOC_PA);
	writeb_relaxed(0, (reg + IOC_UART0_CTS_OFFS));
	writeb_relaxed(0, (reg + IOC_UART0_RTS_OFFS));
	writeb_relaxed(0, (reg + IOC_PWM3_OFFS));
	writeb_relaxed(0, (reg + IOC_PWM6_OFFS));

	writeb_relaxed(0x01, (reg + IOC_PWM3_PAD_OFFS));
	writeb_relaxed(0x01, (reg + IOC_PWM6_PAD_OFFS));

	print_hc18xx_meminfo();
}

static void hc18xx_restart(enum reboot_mode mode, const char *cmd)
{
	volatile void __iomem *reg;
	uint32_t reg_sw_dis;
	uint32_t reg_lck_sta;

	reg = (void *)TO_VA(HC18XX_WDT_PA);

	//Check lock status
	reg_lck_sta = *(volatile uint32_t *)(reg + WDT_LCK_STA_OFFS);
	if ((reg_lck_sta & 0x1) == 1) {
		writeb_relaxed(0x54, (reg + WDT_ULK_REG_OFFS));
		writeb_relaxed(0x6F, (reg + WDT_ULK_REG_OFFS));
		writeb_relaxed(0x6B, (reg + WDT_ULK_REG_OFFS));
		writeb_relaxed(0x79, (reg + WDT_ULK_REG_OFFS));
		writeb_relaxed(0x6F, (reg + WDT_ULK_REG_OFFS));
	}

	//Check watchdog enable/disable, then enable it
	reg_sw_dis = *(volatile uint32_t *)(reg + WDT_DIS_WDT_OFFS);
	if ((reg_sw_dis & 0x1) != 0x0) {
		reg_sw_dis = reg_sw_dis & 0x0;
		writeb_relaxed(reg_sw_dis, (reg + WDT_DIS_WDT_OFFS));
	}
	mdelay(100);
	writeb_relaxed(1, reg);

	//Set watchdog count_stage1 to 0
	writel_relaxed(0, (reg + WDT_CNT_INI_OFFS));
	writel_relaxed(0, (reg + WDT_CNT_S1X_OFFS));

	//Set watchdog disable and then enable
	writeb_relaxed(1, (reg + WDT_DIS_WDT_OFFS));
	mdelay(100);
	writeb_relaxed(0, (reg + WDT_DIS_WDT_OFFS));
}

noinline void hc18xx_stub(void)
{
	asm volatile("stub_start:\n"
	             "nop\n");
}
EXPORT_SYMBOL(hc18xx_stub);

DT_MACHINE_START(HC18XX, "HC18xx SoC").dt_compat = hc18xx_board_dt_compat, .init_early = hc18xx_early_init,
                         .init_machine = hc18xx_machine_init, .map_io = hc18xx_map_io, .reserve = hc18xx_reserve,
                         .restart = hc18xx_restart, MACHINE_END
