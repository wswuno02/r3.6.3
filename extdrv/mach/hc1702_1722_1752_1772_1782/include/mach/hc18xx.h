#ifndef __HC18XX_H
#define __HC18XX_H

/* SRAM */
#define HC18XX_SYSRAM_VA 0xcfff0000
#define HC18XX_SYSRAM_PA 0xffff0000
#define HC18XX_SYSRAM_SZ 0x1000

/* __sram_func attr starting addr */
//#define HC18XX_SRAM_TEXT_VA 0xffff0300
#define HC18XX_SRAM_TEXT_VA HC18XX_SYSRAM_VA

/* Machine static mapping */
#define DRAM_SAFE_OFFS 0x20000000 /* 256MB DRAM capacity margin for vmalloc offset */
#define VA_OFFS (0xC0000000 - 0x80000000) + (DRAM_SAFE_OFFS)
#define TO_VA(pa) (pa + VA_OFFS)

#define DEFAULT_VB_SIZE_MB 64

#define HC18XX_IOC_PA 0x80000000
#define HC18XX_INTC_PA 0x80020000
#define HC18XX_WDT_PA 0x80070100

#define IOC_UART0_RTS_OFFS 0x00000030
#define IOC_UART0_CTS_OFFS 0x00000032
#define IOC_PWM3_OFFS 0x0000003D
#define IOC_PWM6_OFFS 0x0000003C

#define IOC_PWM3_PAD_OFFS 0x00000146
#define IOC_PWM6_PAD_OFFS 0x00000142

#define WDT_LCK_STA_OFFS 0x0000000C
#define WDT_ULK_REG_OFFS 0x00000010
#define WDT_DIS_WDT_OFFS 0x00000014
#define WDT_CNT_INI_OFFS 0x00000018
#define WDT_CNT_S1X_OFFS 0x0000001C

#define SW_RST_DDRBUS     0
#define SW_RST_DDR        1
#define SW_RST_DRAMC      2
#define SW_RST_ISP        3
#define SW_RST_ENC        4
#define SW_RST_IS         5
#define SW_RST_PERI       6
#define SW_RST_USB        7
#define SW_RST_EMAC       8
#define SW_RST_QSPI       9
#define SW_RST_SDC       10
#define SW_RST_LVDS      11
#define SW_RST_CSR       12
#define SW_RST_DMA       13
#define SW_RST_AUDIO_IN  14
#define SW_RST_AUDIO_OUT 15
#define SW_RST_USBBUS    16
#define SW_RST_SUB_LVDS  17
#define SW_RST_I2C0      32
#define SW_RST_I2C1      33
#define SW_RST_SENIF     34
#define SW_RST_SPI0      35
#define SW_RST_SPI1      36
#define SW_RST_SPI2      37
#define SW_RST_UART0     38
#define SW_RST_UART1     39
#define SW_RST_UART2     40
#define SW_RST_PWM       41
#define SW_RST_TIMER     42
#define SW_RST_PERI_QSPI 44
#define SW_RST_PERI_SPI0 45
#define SW_RST_PERI_SPI1 46
#define SW_RST_PERI_SPI2 47
#define SW_RST_SUB_SENIF 48

#ifndef delay_n
extern inline void __delay_n(uint32_t ns)
{
	/* according to RTL simulation, delay in nano second is the loop count divided by 8 when I$ enabled. */
	asm volatile (
		"add r10, %[__delay_ns], #7\n"
		"mov r10, r10, LSR #3\n"
		"delay_loop_%=:\n"
		"subs r10, r10, #1\n"
		"bne delay_loop_%=\n"
		:
		: [__delay_ns] "r" (ns)
		: "r10", "cc"
		);
}
#define delay_n(x) __delay_n(x)
#endif /* delay_n */

extern int hc18xx_enable_sw_rst(int index);
extern int hc18xx_disable_sw_rst(int index);

#define hc18xx_sw_rst(x) \
	do { \
		hc18xx_enable_sw_rst(x); \
		delay_n(1000); \
		hc18xx_disable_sw_rst(x); \
	} while (0)

void hc18xx_stub(void);

#endif
