#include <linux/init.h>
#include <linux/mm.h>
#include <linux/kernel.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>

#define IO_AIOC_PA (0x80001000)
#define AON_WDT_PA (0x80550000)
#define AMC_PA (0x80530000)

/* 4KB memory mapping */
#define IO_AIOC_VA (0xE0800000)
#define AON_WDT_VA (0xE0801000)
#define AMC_VA (0xE0802000)

extern int __init hc1703_1723_1753_1783s_pm_init(void);

unsigned long g_vb_size;
EXPORT_SYMBOL(g_vb_size);

static const char *const hc1703_1723_1753_1783s_dt_match[] = {
	"augentix,hc1703_1723_1753_1783s",
	"augentix,hc1703_1723_1753_1783s-fpga",
	"augentix,hc1703_1723_1753_1783s-evb-v1",
	NULL,
};

extern void auge_nor_exit_4byte(void);
extern void aon_wdt_reboot(volatile void *wdt_base, volatile void *amc_base);
extern void aon_wdt_stop_for_suspend(volatile void *wdt_base, volatile void *amc_base);

static __initdata struct map_desc hc1703_1723_1753_1783s_io_desc[] = {
	{
	        .virtual = IO_AIOC_VA,
	        .pfn = __phys_to_pfn(IO_AIOC_PA),
	        .length = SZ_4K,
	        .type = MT_DEVICE,
	},
	{
	        .virtual = AON_WDT_VA,
	        .pfn = __phys_to_pfn(AON_WDT_PA),
	        .length = SZ_4K,
	        .type = MT_DEVICE,
	},
	{
	        .virtual = AMC_VA,
	        .pfn = __phys_to_pfn(AMC_PA),
	        .length = SZ_4K,
	        .type = MT_DEVICE,
	},
};

void __init hc1703_1723_1753_1783s_map_io(void)
{
	hc1703_1723_1753_1783s_pm_init();
	iotable_init(hc1703_1723_1753_1783s_io_desc, ARRAY_SIZE(hc1703_1723_1753_1783s_io_desc));
}

static void __init hc1703_1723_1753_1783s_reserve(void)
{
#ifdef CONFIG_DMA_CMA
	g_vb_size = CONFIG_VB_SIZE_MB > CONFIG_CMA_SIZE_MBYTES ? (CONFIG_CMA_SIZE_MBYTES << 20) :
	                                                         (CONFIG_VB_SIZE_MB << 20);
#endif
}

static void hc1703_1723_1753_1783s_restart(enum reboot_mode mode, const char *cmd)
{
	volatile void *aon_wdt_base = (volatile void *)AON_WDT_VA;
	volatile void *amc_base = (volatile void *)AMC_VA;
	aon_wdt_reboot(aon_wdt_base, amc_base);
}

void hc1703_1723_1753_1783s_suspend_disable_wdt(void)
{
	volatile void *aon_wdt_base = (volatile void *)AON_WDT_VA;
	volatile void *amc_base = (volatile void *)AMC_VA;
	aon_wdt_stop_for_suspend(aon_wdt_base, amc_base);
}

void hc1703_1723_1753_1783s_amc_pd_off(void)
{
	volatile void *aon_wdt_base = (volatile void *)AON_WDT_VA;
	volatile void *amc_base = (volatile void *)AMC_VA;
	volatile void *addr;
	aon_wdt_stop_for_suspend(aon_wdt_base, amc_base);

	addr = amc_base + 0x68; //PWR_MODE
	writel_relaxed(0x10000, addr);

	addr = amc_base + 0x18; //SLEEP_AON
	writel_relaxed(1, addr);
}
EXPORT_SYMBOL(hc1703_1723_1753_1783s_amc_pd_off);

DT_MACHINE_START(HC1703_1723_1753_1783S_DT, "Augentix HC1703_1723_1753_1783s family").dt_compat =
        hc1703_1723_1753_1783s_dt_match,
                                            .restart = hc1703_1723_1753_1783s_restart,
                                            .reserve = hc1703_1723_1753_1783s_reserve,
                                            .map_io = hc1703_1723_1753_1783s_map_io, MACHINE_END
