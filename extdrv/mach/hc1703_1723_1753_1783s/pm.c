/*  extdrv\mach\hc1703_1723_1753_1783s
 *
 * Copyright 2021 AUGENTIX
 * Dream Yeh <dream.yeh@augentix.com>
 *
 * HC1703_1723_1753_1783s common power management (suspend to ram) support.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
*/

#include <linux/init.h>
#include <linux/suspend.h>
#include <linux/errno.h>
#include <linux/delay.h>
#include <linux/of.h>

#include <linux/io.h>
#include <linux/watchdog.h>
#include <linux/syscore_ops.h>

#include <asm/cacheflush.h>
#include <asm/suspend.h>

#include <asm/irq.h>

//#include <plat/pm.h>
//#include <mach/pm-core.h>

#define DRAM_SAFE_OFFS 0x20000000 /* 256MB DRAM capacity margin for vmalloc offset */
#define VA_OFFS (0xC0000000 - 0x80000000) + (DRAM_SAFE_OFFS)
#define TO_VA(pa) (pa + VA_OFFS)

#define IO_AIOC_PA (0x80001000)
#define AON_WDT_PA (0x80550000)
#define AMC_PA (0x80530000)

//AON Register=================================
#define HC1703_1723_1753_1783S_GSTATUS2 0x8053000C //Status  Bit[18]
#define HC1703_1723_1753_1783S_GSTATUS2_OFFRESET 0x00040000
#define HC1703_1723_1753_1783S_GSTATUS3 0x80550020 //-> do_resume
#define HC1703_1723_1753_1783S_GSTATUS4_lo 0x80530098 //-> sp
#define HC1703_1723_1753_1783S_GSTATUS4_hi 0x8053009c //-> sp

/* for external use */
unsigned long hc1703_1723_1753_1783s_pm_flags;

/* The IRQ ext-int code goes here, it is too small to currently bother
 * with its own file. */

unsigned long hc1703_1723_1753_1783s_irqwake_intmask = 0xffffffffL;
unsigned long hc1703_1723_1753_1783s_irqwake_eintmask = 0xffffffffL;

#if 0
int hc1703_1723_1753_1783s_irqext_wake(struct irq_data *data, unsigned int state)
{
	//	unsigned long bit = 1L << IRQ_EINT_BIT(data->irq);

	//if (!(hc1703_1723_1753_1783s_irqwake_eintallow & bit))
	//	return -ENOENT;

	//	printk(KERN_INFO "wake %s for irq %d\n",
	//	       state ? "enabled" : "disabled", data->irq);

	//	if (!state)
	//		hc1703_1723_1753_1783s_irqwake_eintmask |= bit;
	//	else
	//		hc1703_1723_1753_1783s_irqwake_eintmask &= ~bit;

	return 0;
}
#endif

/* hc1703_1723_1753_1783s_pm_show_resume_irqs
 *
 * print any IRQs asserted at resume time (ie, we woke from)
*/
#if 0
static void __maybe_unused hc1703_1723_1753_1783s_pm_show_resume_irqs(int start, unsigned long which, unsigned long mask)
{
	int i;

	which &= ~mask;

	for (i = 0; i <= 31; i++) {
		if (which & (1L << i)) {
			printk("IRQ %d asserted at resume\n", start + i);
		}
	}
}
#endif
int (*pm_cpu_prep)(void);
int (*pm_cpu_sleep)(unsigned long);

#define any_allowed(mask, allow) (((mask) & (allow)) != (allow))
extern void hc1703_1723_1753_1783s_cpu_resume(void);
//------------------------------
extern void hc1703_1723_1753_1783s_suspend_disable_wdt(void);

int hc1703_1723_1753_1783s_cpu_suspend(unsigned long input)
{
	volatile void *addr;
	volatile void *amc_base = (volatile void *)TO_VA(AMC_PA);
	//volatile void *wdt_aon_base = (volatile void *)TO_VA(AON_WDT_PA);
	//volatile void *io_aioc_base = (volatile void *)TO_VA(IO_AIOC_PA);

	/*Enable AO_BUTTON_IOSEL 04.23 it's default*/
	//addr = io_aioc_base + 0x068;
	//writel_relaxed(1, addr);

	//add boot time for save AON register
	addr = amc_base + 0x38; //PWR_SWITCHING_BLANKING_PERIOD
	writel_relaxed(100, addr);
	addr = amc_base + 0x3c; //PWR_SWITCHING_BLANKING_PERIOD
	writel_relaxed(3, addr);

	//disable watchdog for CPU
	printk("disable watchdog for CPU\n");
	hc1703_1723_1753_1783s_suspend_disable_wdt();

	printk("Ready to sleep.....");
	addr = amc_base + 0x18; //SLEEP_AON
	writel_relaxed(1, addr);

	return 0;
}

int hc1703_1723_1753_1783s_cpu_pm_prepare(void)
{
	volatile void *amc_base = (volatile void *)TO_VA(AMC_PA);
	volatile void *wdt_aon_base = (volatile void *)TO_VA(AON_WDT_PA);

	volatile void *addr;
	int lo, hi;

	/* ensure at least GSTATUS3 has the resume address */
	addr = wdt_aon_base + 0x20;
	writel_relaxed(virt_to_phys(hc1703_1723_1753_1783s_cpu_resume), addr);

	//GSTATUS4 have two register 0x80530098+0x8053009C
	addr = amc_base + 0x98;
	lo = readl_relaxed(addr);

	addr = amc_base + 0x9c;
	hi = readl_relaxed(addr);

	printk("GSTATUS3 = 0x%08x\n", readl_relaxed(addr));
	printk("GSTATUS4 = 0x%08x\n", (lo | (hi << 16)));

	return 0;
}

int hc1703_1723_1753_1783s_get_suspend_status(void)
{
	//HC1703_1723_1753_1783S_GSTATUS2 0x8053000C	//Status  Bit[18]
	int ret;
	volatile void *amc_base = (volatile void *)TO_VA(AMC_PA);
	volatile void *addr;
	addr = amc_base + 0x98; //15:0 RESERVED_0
	ret = readl_relaxed(addr);
	return ret;
}
//------------------------------

/* hc1703_1723_1753_1783s_pm_enter
 *
 * central control for sleep/resume process
*/

static int hc1703_1723_1753_1783s_pm_enter(suspend_state_t state)
{
	int ret;
	/* ensure the debug is initialised (if enabled) */

	//	hc1703_1723_1753_1783s_pm_debug_init();

	printk("%s(%d)\n", __func__, state);

	pm_cpu_sleep = hc1703_1723_1753_1783s_cpu_suspend;
	pm_cpu_prep = hc1703_1723_1753_1783s_cpu_pm_prepare;

	if (pm_cpu_prep == NULL || pm_cpu_sleep == NULL) {
		printk(KERN_ERR "%s: error: no cpu sleep function\n", __func__);
		return -EINVAL;
	}

	/* check if we have anything to wake-up with... bad things seem
	 * to happen if you suspend with no wakeup (system will often
	 * require a full power-cycle)
	*/
	/*
	if (!of_have_populated_dt() &&
	    !any_allowed(hc1703_1723_1753_1783s_irqwake_intmask, hc1703_1723_1753_1783s_irqwake_intallow) &&
	    !any_allowed(hc1703_1723_1753_1783s_irqwake_eintmask, hc1703_1723_1753_1783s_irqwake_eintallow)) {
		printk(KERN_ERR "%s: No wake-up sources!\n", __func__);
		printk(KERN_ERR "%s: Aborting sleep\n", __func__);
		return -EINVAL;
	}
*/
	/* save all necessary core registers not covered by the drivers */

	//if (!of_have_populated_dt()) {
	//hc1703_1723_1753_1783s_pm_save_gpios();
	//hc1703_1723_1753_1783s_pm_saved_gpios();
	//}

	//hc1703_1723_1753_1783s_pm_save_uarts();
	//hc1703_1723_1753_1783s_pm_save_core();

	/* set the irq configuration for wake */

	//hc1703_1723_1753_1783s_pm_configure_extint();

	//printk("sleep: irq wakeup masks: %08lx,%08lx\n", hc1703_1723_1753_1783s_irqwake_intmask, hc1703_1723_1753_1783s_irqwake_eintmask);

	//hc1703_1723_1753_1783s_pm_arch_prepare_irqs();

	/* call cpu specific preparation */
	pm_cpu_prep();

	/* flush cache back to ram */
	flush_cache_all();

	//hc1703_1723_1753_1783s_pm_check_store();

	/* send the cpu to sleep... */
	//hc1703_1723_1753_1783s_pm_arch_stop_clocks();

	/* this will also act as our return point from when
	 * we resume as it saves its own register state and restores it
	 * during the resume.  */
	ret = cpu_suspend(0, pm_cpu_sleep);
	if (ret)
		return ret;

	/* restore the system state */

	//hc1703_1723_1753_1783s_pm_restore_core();
	//hc1703_1723_1753_1783s_pm_restore_uarts();

	if (!of_have_populated_dt()) {
		//hc1703_1723_1753_1783s_pm_restore_gpios();
		//hc1703_1723_1753_1783s_pm_restored_gpios();
	}

	//	hc1703_1723_1753_1783s_pm_debug_init();

	/* check what irq (if any) restored the system */

	//	hc1703_1723_1753_1783s_pm_arch_show_resume_irqs();

	printk("%s: post sleep, preparing to return\n", __func__);

	/* LEDs should now be 1110 */
	//hc1703_1723_1753_1783s_pm_debug_smdkled(1 << 1, 0);

	//hc1703_1723_1753_1783s_pm_check_restore();

	/* ok, let's return from sleep */
	printk("HC1703_1723_1753_1783S PM Resume (post-restore)\n");
	return 0;
}

static int hc1703_1723_1753_1783s_pm_prepare(void)
{
	/* prepare check area if configured */

	//hc1703_1723_1753_1783s_pm_check_prepare();
	return 0;
}

static void hc1703_1723_1753_1783s_pm_finish(void)
{
	//hc1703_1723_1753_1783s_pm_check_cleanup();
}

static const struct platform_suspend_ops hc1703_1723_1753_1783s_pm_ops = {
	.enter = hc1703_1723_1753_1783s_pm_enter,
	.prepare = hc1703_1723_1753_1783s_pm_prepare,
	.finish = hc1703_1723_1753_1783s_pm_finish,
	.valid = suspend_valid_only_mem,
};

/* hc1703_1723_1753_1783s_pm_init
 *
 * Attach the power management functions. This should be called
 * from the board specific initialisation if the board supports
 * it.
*/

int __init hc1703_1723_1753_1783s_pm_init(void)
{
	printk("HC1703_1723_1753_1783s Power Management, hc1703_1723_1753_1783s_pm_init 2021\n");

	suspend_set_ops(&hc1703_1723_1753_1783s_pm_ops);
	return 0;
}

/*-------------------------------------------------------------
  Below code is refer pm-xxxx0.c on mach-xxxx
 *-------------------------------------------------------------*/

void hc1703_1723_1753_1783s_cpu_resume() //temp function
{
}

static void hc1703_1723_1753_1783s_pm_resume(void)
{
	/* unset the return-from-sleep flag, to ensure reset */

	//tmp = __raw_readl(HC1703_1723_1753_1783S_GSTATUS2);
	//tmp &= HC1703_1723_1753_1783S_GSTATUS2_OFFRESET;
	//__raw_writel(tmp, HC1703_1723_1753_1783S_GSTATUS2);
}

struct syscore_ops hc1703_1723_1753_1783s_pm_syscore_ops = {
	.resume = hc1703_1723_1753_1783s_pm_resume,
};

static int hc1703_1723_1753_1783s_pm_add(struct device *dev, struct subsys_interface *sif)
{
	pm_cpu_prep = hc1703_1723_1753_1783s_pm_prepare;
	pm_cpu_sleep =
	        hc1703_1723_1753_1783s_cpu_suspend; //Dream: use hc1703_1723_1753_1783s arch suspend to sleep CPU and DRAM

	return 0;
}

///========================================================
struct bus_type hc1703_1723_1753_1783s_subsys = {
	.name = "hc1703_1723_1753_1783s-core",
	.dev_name = "hc1703_1723_1753_1783s-core",
};
static struct device hc1703_1723_1753_1783s_dev = {
	.bus = &hc1703_1723_1753_1783s_subsys,
};

static int __init hc1703_1723_1753_1783s_core_init(void)
{
	return subsys_system_register(&hc1703_1723_1753_1783s_subsys, NULL);
}
core_initcall(hc1703_1723_1753_1783s_core_init);

int __init hc1703_1723_1753_1783s_init(void)
{
	printk("HC1703_1723_1753_1783s: Initialising architecture\n");

#ifdef CONFIG_PM
	register_syscore_ops(&hc1703_1723_1753_1783s_pm_syscore_ops);
//	register_syscore_ops(&hc1703_1723_1753_1783s_irq_syscore_ops);
#endif

	return device_register(&hc1703_1723_1753_1783s_dev);
}
//========================================
static struct subsys_interface hc1703_1723_1753_1783s_pm_interface = {
	.name = "hc1703_1723_1753_1783s_pm",
	.subsys = &hc1703_1723_1753_1783s_subsys,
	.add_dev = hc1703_1723_1753_1783s_pm_add,
};

/* register ourselves */

static int __init hc1703_1723_1753_1783s_pm_drvinit(void)
{
	return subsys_interface_register(&hc1703_1723_1753_1783s_pm_interface);
}

arch_initcall(hc1703_1723_1753_1783s_pm_drvinit);
