#include <linux/module.h>
#include <linux/init.h>
#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/irqreturn.h>
#include <linux/platform_device.h>
#include <linux/err.h>
#include <linux/of.h>
#include <linux/of_irq.h>
#include <linux/of_address.h>
#include <linux/export.h>
#include <linux/mm.h>

#define DRV_NAME "pwm_drv"

#define PWM_OFFS(x) (x * 0x0010)
#define TRIGGR 0x00
#define IRQCLR 0x02
#define PWMBSY 0x04
#define IRQSTA 0x06
#define IRQMSK 0x08
#define PWMMDE 0x0a
#define PRESCL 0x0c
#define CNTPRD 0x0d
#define CNTHGH 0x0e
#define NUMPRD 0x0f
#define PINMUX 0x60
#define PINMUX_OFFS 0x1

#define PWM_MODULE 0
#define PWM_PIN    6

#define PWM_PERIODIC 0
#define PWM_ONESHOT  1

struct pwm_drvdata {
	int id;
	int                  irq;               /* Driver IRQ number */
	u32                  phy_start;         /* Driver physical memory */
	u32                  phy_size;
	void __iomem        *base;
};

struct pwm_drvdata* g_pwm;

static irqreturn_t pwm_irq_handler(int irq, void *dev_id)
{
	struct pwm_drvdata *pwm = (struct pwm_drvdata *)dev_id;

	/* Only use PWM0 */
	if(readb_relaxed(pwm->base + IRQSTA))
	{
		writeb_relaxed(1, pwm->base + IRQCLR);
		return IRQ_HANDLED;
	}
	return IRQ_NONE;
}

/* Assert PWM module 0 -> PWM pin 6 */
static void pwm0_init(struct pwm_drvdata *pwm, int pin)
{
	writeb_relaxed(1, pwm->base + IRQCLR);
	writeb_relaxed(1, pwm->base + IRQMSK);
	writeb_relaxed(0, pwm->base + PWMMDE);

	writeb_relaxed(3, pwm->base + CNTPRD);
	writeb_relaxed(13, pwm->base + PRESCL);
	//writeb_relaxed(mod, (pwm->base + PINMUX + PINMUX_OFFS * pin);
}

static void pwm0_start(int mode)
{
	struct pwm_drvdata *pwm = g_pwm;
	if(readb_relaxed(pwm->base + PWMBSY) != 0)
		return;
	writeb_relaxed(mode, pwm->base + PWMMDE);
	writeb_relaxed(1, pwm->base + IRQCLR);
	writeb_relaxed(0, pwm->base + IRQMSK);
	writeb_relaxed(1, pwm->base + TRIGGR);
};
EXPORT_SYMBOL_GPL(pwm0_start);

static void pwm0_stop(void)
{
	struct pwm_drvdata *pwm = g_pwm;
	if(readb_relaxed(pwm->base + PWMBSY) == 0)
		return;
	writeb_relaxed(1, pwm->base + TRIGGR);
	writeb_relaxed(1, pwm->base + IRQMSK);
	writeb_relaxed(1, pwm->base + IRQCLR);
};
EXPORT_SYMBOL_GPL(pwm0_stop);

static int pwm_probe(struct platform_device *dev)
{
	struct resource res;
	struct pwm_drvdata *pwm;
	g_pwm = NULL;

	pwm = devm_kzalloc(&dev->dev, sizeof(struct pwm_drvdata), GFP_KERNEL);
	if (pwm == NULL) {
		dev_err(&dev->dev, "Failed to allocate driver data!\n");
		return -ENOMEM;
	}

	if (of_address_to_resource(dev->dev.of_node, 0, &res)) {
		dev_err(&dev->dev, "No resource specified !\n");
		return -ENODEV;
	}

	pwm->irq = irq_of_parse_and_map(dev->dev.of_node, 0);

	if (devm_request_irq(&dev->dev, pwm->irq, pwm_irq_handler, IRQF_SHARED, DRV_NAME, pwm)) {
		dev_err(&dev->dev, "Failed to request irq !\n");
		return -EINVAL;
	}

	/* Read and Register Driver Info */
	pwm->phy_start = res.start;
	pwm->phy_size  = resource_size(&res);
	pwm->base = devm_ioremap(&dev->dev, pwm->phy_start, pwm->phy_size);

	if (IS_ERR(pwm->base)) {
		dev_err(&dev->dev, "Unable to ioremap resource !\n");
		return PTR_ERR(pwm->base);
	}

	pwm0_init(pwm, PWM_PIN);

	g_pwm = pwm;
	platform_set_drvdata(dev, pwm);

	pr_info("Augentix PWM driver probed.\n");
	return 0;
}

static int pwm_remove(struct platform_device *dev)
{
	struct pwm_drvdata *pwm = platform_get_drvdata(dev);
	pwm0_stop();
	platform_set_drvdata(dev, NULL);
	devm_kfree(&dev->dev, pwm);
	g_pwm = NULL;
	return 0;
}

static const struct of_device_id pwm_of_ids[] = {
	{.compatible = "augentix,pwm", },
	{}
};
MODULE_DEVICE_TABLE(of, pwm_of_ids);

static struct platform_driver pwm_platform_driver = {
	.probe  = pwm_probe,
	.remove = pwm_remove,
	.driver = {
		.name  = DRV_NAME,
		.owner = THIS_MODULE,
		.of_match_table = pwm_of_ids,
	},
};
module_platform_driver(pwm_platform_driver);

MODULE_DESCRIPTION("PWM testing module");
MODULE_AUTHOR("ShihChieh Lin<shihchieh.lin@augentix.com>");
MODULE_LICENSE("GPL");
