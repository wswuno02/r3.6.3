#include <linux/clk.h>
#include <linux/err.h>
#include <linux/io.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/pwm.h>
#include <linux/platform_device.h>
#include <linux/slab.h>

/* Region PWM */
#define PWMX_TRIG_OFFSET 0x00
#define PWMX_IRQC_OFFSET 0x02
#define PWMX_BUSY_OFFSET 0x04
#define PWMX_STAT_OFFSET 0x06
#define PWMX_IRQM_OFFSET 0x08
#define PWMX_MODE_OFFSET 0x0A
#define PWMX_PRSC_OFFSET 0x0C
#define PWMX_CNTP_OFFSET 0x0D
#define PWMX_CNTH_OFFSET 0x0E
#define PWMX_NUMP_OFFSET 0x0F
#define PWMO_SEL0_OFFSET 0x60

/* Control register */
#define PWMX_MODE_CONT 0x00
#define PWMX_MODE_FIXC 0x01

#define NUM_PWM_KER 6
#define NUM_PWM_OUT 12
#define HC_PERIOD_BASE 1000000000
#define HC_PRESCALER_BIT 5
#define HC_COUNT_PERIOD_BIT 8

struct hc_pwm_chip{
	struct pwm_chip chip;
	struct device *dev;

	void __iomem *mmio_base;
	struct clk *pwm_clk;
	int pwm_hz;

	int idx;
	int out_ker[6];
};

int hc_gcd(int, int);
int hc_exp2(int);
int hc_pow2(int);

static inline struct hc_pwm_chip *to_hc_pwm_chip(struct pwm_chip *chip)
{
	return container_of(chip, struct hc_pwm_chip, chip);
}

int hc_gcd(int gcd_p, int gcd_d)
{
	int gcd_t = 0;
	while(gcd_d != 0){
		gcd_t = gcd_p % gcd_d;
		gcd_p = gcd_d;
		gcd_d = gcd_t;
	}
	return gcd_p;
}

int hc_exp2(int exp)
{
	if(exp == 0)	return 1;
	else	return 2 * hc_exp2(exp-1);
}

int hc_pow2(int pow)
{
	if(pow == 1)	return 0;
	else	return 1 + hc_pow2(pow >> 1);
}

static int hc_pwm_config(struct pwm_chip *chip, struct pwm_device *pwm,
			 int duty_ns, int period_ns)
{
	struct hc_pwm_chip *hc = to_hc_pwm_chip(chip);

	int cnt_psc = 0;
	int period_freq;
	unsigned long long calc_high;
	unsigned long long calc_period;
	unsigned long long mod_prd;
	int max_freq;
	int min_freq;
	void __iomem *ker_off;
	int cho_ker = -1;

	for(hc->idx = 0;hc->idx < 6;hc->idx++){
		if(hc->out_ker[hc->idx] == pwm->hwpwm){
			cho_ker = hc->idx;
		}
	}

	if(cho_ker == -1){
		pr_info("PWM outputs(%d) does not match the description of device tree.\n", pwm->hwpwm);
		return 0;
	}

	pr_info("Augentix PWM config: Set Kernel(%d)==>\n", cho_ker);
	pr_info("Augentix PWM config: duty_ns(%d), period_ns(%d).\n", duty_ns, period_ns);

	period_freq = hc->pwm_hz;

	/*
	 * HC PWM highest frequency is sys_clk / 2
	 * (ex: sys_clk -> 24MHz, highest frequency -> 12Mhz)
	 */
	max_freq = hc->pwm_hz / 2;
	if((HC_PERIOD_BASE / period_ns) > max_freq){
		pr_info("(Over limit)Augentix PWM provide highest frequency is: (%d)Hz.\n", max_freq);
		return 0;
	}

	/*
	 * HC PWM lowest frequency is sys_clk / 2^prescaler_max / count_period_max
	 * (ex: sys_clk -> 24MHz, max perscaler -> 16, max count_period -> 255,
	 * lowest frequency -> 24000000 / 2^16 / 255 = 1.43Hz)
	 */
	min_freq = hc->pwm_hz / hc_exp2( hc_exp2(HC_PRESCALER_BIT-1) ) / (hc_exp2(HC_COUNT_PERIOD_BIT) -1);
	if((HC_PERIOD_BASE / period_ns) < min_freq){
		pr_info("(Over limit)Augentix PWM provide lowest frequency is: (%d)Hz.\n", min_freq);
		return 0;
	}

	do{
		/*
		 *   period_ns     count_period
		 *  ----------- = --------------
		 *     10^9         frequency
		 *
		 * frequency = 24MHz / (2^prescaler)
		 *
		 * count_period = frequency * period_ns / 10^9
		 */
		calc_period = (unsigned long long)period_freq * period_ns;
		mod_prd = do_div(calc_period, HC_PERIOD_BASE);

		if(calc_period > 255){
			cnt_psc = cnt_psc + 1;
			period_freq = period_freq / 2;
		}
		else{
			if(mod_prd > HC_PERIOD_BASE / 2){
				calc_period = calc_period + 1;
			}

			pr_info("request frquency: %d Hz\n", HC_PERIOD_BASE/period_ns);
			do_div(period_freq, calc_period);
			pr_info("provide frquency: %d Hz\n", period_freq);
		}
	}while(calc_period>255);

	/*
	 *    duty_ns       count_high
	 *  ----------- = --------------
	 *   period_ns     count_period
	 *
	 * count_high = count_period * duty_ns / period_ns
	 */
	calc_high = calc_period * duty_ns;
	do_div(calc_high, period_ns);

	/* Set config to register */
	ker_off = hc->mmio_base + (0x10 * cho_ker);
	writeb_relaxed(cnt_psc, ker_off + PWMX_PRSC_OFFSET);
	writeb_relaxed(calc_period, ker_off + PWMX_CNTP_OFFSET);
	writeb_relaxed(calc_high, ker_off + PWMX_CNTH_OFFSET);

	return 0;
}

static int hc_pwm_enable(struct pwm_chip *chip, struct pwm_device *pwm)
{
	struct hc_pwm_chip *hc = to_hc_pwm_chip(chip);
	void __iomem *ker_off;
	int cho_ker = -1;

	for(hc->idx = 0;hc->idx < 6;hc->idx++){
		if(hc->out_ker[hc->idx] == pwm->hwpwm){
			cho_ker = hc->idx;
		}
	}

	if(cho_ker == -1){
		pr_info("PWM outputs(%d) does not match the description of device tree.\n", pwm->hwpwm);
		return 0;
	}

	pr_info("Augentix PWM kernel(%d) enable.\n", cho_ker);

	ker_off = hc->mmio_base + (0x10 * cho_ker);

	if(readb_relaxed(ker_off + PWMX_BUSY_OFFSET) != 0)
		return 0;
	writeb_relaxed(1, ker_off + PWMX_IRQC_OFFSET);
	writeb_relaxed(1, ker_off + PWMX_TRIG_OFFSET);

	return 0;
}

static void hc_pwm_disable(struct pwm_chip *chip, struct pwm_device *pwm)
{
	struct hc_pwm_chip *hc = to_hc_pwm_chip(chip);
	void __iomem *ker_off;
	int cho_ker = -1;

	for(hc->idx = 0;hc->idx < 6;hc->idx++){
		if(hc->out_ker[hc->idx] == pwm->hwpwm){
			cho_ker = hc->idx;
		}
	}

	if(cho_ker == -1){
		pr_info("PWM outputs(%d) does not match the description of device tree.\n", pwm->hwpwm);
		return;
	}

	pr_info("Augentix PWM kernel(%d) disable.\n", cho_ker);

	ker_off = hc->mmio_base + (0x10 * cho_ker);

	if(readb_relaxed(ker_off + PWMX_BUSY_OFFSET) == 0)
		return;
	writeb_relaxed(1, ker_off + PWMX_IRQC_OFFSET);
	writeb_relaxed(1, ker_off + PWMX_TRIG_OFFSET);
}

static const struct pwm_ops hc_pwm_ops = {
	.config = hc_pwm_config,
	.enable = hc_pwm_enable,
	.disable = hc_pwm_disable,
	.owner = THIS_MODULE,
};

static int hc_pwm_probe(struct platform_device *pdev)
{
	struct hc_pwm_chip *hc;
	struct resource *r;
	struct device_node *np = pdev->dev.of_node;
	int ret;

	hc = devm_kzalloc(&pdev->dev, sizeof(*hc), GFP_KERNEL);
	if(!hc)
		return -ENOMEM;

	hc->dev = &pdev->dev;

	r = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	hc->mmio_base = devm_ioremap_resource(&pdev->dev, r);
	if(IS_ERR(hc->mmio_base))
		return PTR_ERR(hc->mmio_base);

	platform_set_drvdata(pdev, hc);

	hc->chip.dev = &pdev->dev;
	hc->chip.ops = &hc_pwm_ops;
	//hc->chip.base = -1;
	hc->chip.npwm = NUM_PWM_OUT;

	/* get system clock */
	hc->pwm_clk = devm_clk_get(hc->chip.dev, "sys_clk");
	if (IS_ERR(hc->pwm_clk)) {
		dev_err(hc->dev, "Clock unavailable\n");
		return -ENODEV;
	}
	hc->pwm_hz = clk_get_rate(hc->pwm_clk);
	//pr_info("%s(#%d)>> pwm_hz: %d\n", __func__, __LINE__, hc->pwm_hz);

	ret = pwmchip_add(&hc->chip);
	if(ret < 0){
		dev_err(&pdev->dev, "pwmchip_add() failed: %d\n", ret);
		return ret;
	}

	/* get output select from device tree */
	for(hc->idx = 0;hc->idx < NUM_PWM_KER; hc->idx++){
		if(of_property_read_u32_index(np, "out-sel-ker", hc->idx, &hc->out_ker[hc->idx])){
			dev_err(&pdev->dev, "invalid output select number: %d\n", hc->idx);
			return -ERANGE;
		}
		writeb_relaxed(hc->idx, hc->mmio_base + PWMO_SEL0_OFFSET + hc->out_ker[hc->idx]);
		//pr_info("%s(#%d)HC probe get: out_sel.num(%d), and kernel(%d) output to addr(%p).\n",
		//	__func__, __LINE__, hc->out_ker[hc->idx], hc->idx,
		//	hc->mmio_base + PWMO_SEL0_OFFSET + hc->out_ker[hc->idx]);
	}

	pr_info("Augentix PWM driver probed.\n");

	return 0;
}

static int hc_pwm_remove(struct platform_device *pdev)
{
	struct hc_pwm_chip *hc = platform_get_drvdata(pdev);

	pr_info("Augentix PWM driver removed.\n");

	return pwmchip_remove(&hc->chip);
}

static const struct of_device_id hc_pwm_of_match[] = {
	{ .compatible = "augentix,pwm" },
	{},
};

MODULE_DEVICE_TABLE(of, hc_pwm_of_match);

static struct platform_driver hc_pwm_driver = {
	.driver = {
		.name = "hc_pwm",
		.owner = THIS_MODULE,
		.of_match_table = hc_pwm_of_match,
	},
	.probe = hc_pwm_probe,
	.remove = hc_pwm_remove,
};

module_platform_driver(hc_pwm_driver);

MODULE_DESCRIPTION("Augentix HC18xx PWM driver");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Augentix Inc.");

