#include <linux/clk.h>
#include <linux/err.h>
#include <linux/io.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/pwm.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/irqreturn.h>
#include <linux/of_irq.h>
#include <linux/pinctrl/consumer.h>
#include <linux/spinlock.h>

// #define DEBUG
// #define pr_fmt(fmt) "%s(): " fmt, __func__

/* Region PWM */
#define PWMX_TRIG_OFFSET 0x00
#define PWMX_IRQC_OFFSET 0x02
#define PWMX_BUSY_OFFSET 0x04
#define PWMX_STAT_OFFSET 0x06
#define PWMX_IRQM_OFFSET 0x08
#define PWMX_MODE_OFFSET 0x0A
#define PWMX_PRSC_OFFSET 0x0C
#define PWMX_CNTP_OFFSET 0x0D
#define PWMX_CNTH_OFFSET 0x0E
#define PWMX_NUMP_OFFSET 0x0F
#define PWMO_SEL0123_OFFSET 0x60
#define PWMO_SEL45_OFFSET 0x64

/* Control register */
#define PWMX_MODE_CONT 0x00
#define PWMX_MODE_FIXC 0x01

#define NUM_PWM_KER 6
#define NUM_PWM_OUT 6
#define HC_PRESCALER_MIN 0
#define HC_PRESCALER_MAX 24
#define HC_PERIOD_BASE 1000000000
#define HC_COUNT_PERIOD_BIT 8

struct hc_pwm_chip {
	struct pwm_chip chip;
	struct device *dev;
	int irq[NUM_PWM_KER];
	spinlock_t lock;
	struct pinctrl *p;
	struct pinctrl_state *pwm_iomux;

	void __iomem *mmio_base;
	struct clk *pwm_clk[NUM_PWM_KER];
	int pwm_hz[NUM_PWM_KER];

	int idx;
	int out_ker[NUM_PWM_OUT];
};

int hc_gcd(int, int);
int hc_exp2(int);
int hc_pow2(int);

static inline struct hc_pwm_chip *to_hc_pwm_chip(struct pwm_chip *chip)
{
	return container_of(chip, struct hc_pwm_chip, chip);
}

int hc_gcd(int gcd_p, int gcd_d)
{
	int gcd_t = 0;
	while (gcd_d != 0) {
		gcd_t = gcd_p % gcd_d;
		gcd_p = gcd_d;
		gcd_d = gcd_t;
	}
	return gcd_p;
}

int hc_exp2(int exp)
{
	if (exp == 0)
		return 1;
	else
		return 2 * hc_exp2(exp - 1);
}

int hc_pow2(int pow)
{
	if (pow == 1)
		return 0;
	else
		return 1 + hc_pow2(pow >> 1);
}

static irqreturn_t hc_pwm_irq_handler(int irq, void *dev_id)
{
	struct hc_pwm_chip *hc = (struct hc_pwm_chip *)dev_id;
	u32 t_reg;

	for (hc->idx = 0; hc->idx < NUM_PWM_KER; ++hc->idx) {
		t_reg = readl_relaxed(hc->mmio_base + (0x10 * hc->idx) + PWMX_BUSY_OFFSET);
		if ((t_reg >> 16) & 0x1) {
			writel_relaxed((0x1 << 16), hc->mmio_base + (0x10 * hc->idx) + PWMX_TRIG_OFFSET);
			pr_debug("[PWM] Kernel %d IRQ cleared\n", hc->idx);
			return IRQ_HANDLED;
		}
	}
	return IRQ_NONE;
}

static int hc_pwm_config(struct pwm_chip *chip, struct pwm_device *pwm, int duty_ns, int period_ns)
{
	struct hc_pwm_chip *hc = to_hc_pwm_chip(chip);

	int cnt_psc = 0;
	int period_freq;
	unsigned long long calc_high;
	unsigned long long calc_period;
	unsigned long long mod_prd;
	int max_freq;
	int min_freq;
	void __iomem *ker_off;
	u32 t_reg;
	int cho_ker = -1;
	unsigned long flags;

	for (hc->idx = 0; hc->idx < 6; hc->idx++)
		if (hc->out_ker[hc->idx] == pwm->hwpwm)
			cho_ker = hc->idx;

	if (cho_ker == -1) {
		pr_err("PWM outputs %d does not match the description of device tree.\n", pwm->hwpwm);
		return -ENODEV;
	}

	pr_debug("Augentix PWM config: Set Kernel %d==>\n", cho_ker);
	pr_info("Augentix PWM%d config: duty_ns = %d ns, period_ns = %d ns.\n", cho_ker, duty_ns, period_ns);

	period_freq = hc->pwm_hz[cho_ker];

	/*
	 * HC PWM highest frequency is sys_clk / 2
	 * (ex: sys_clk -> 24MHz, highest frequency -> 12Mhz)
	 */
	max_freq = hc->pwm_hz[cho_ker] / 2;
	if ((HC_PERIOD_BASE / period_ns) > max_freq) {
		pr_err("(Over limit) Augentix PWM provide highest frequency is: %d Hz.\n", max_freq);
		return -ERANGE;
	}

	/*
	 * HC PWM lowest frequency is sys_clk / 2^prescaler_max / count_period_max
	 * (ex: sys_clk -> 24MHz, max perscaler -> 16, max count_period -> 255,
	 * lowest frequency -> 24000000 / 2^16 / 255 = 1.43Hz)
	 */
	min_freq = hc->pwm_hz[cho_ker] / hc_exp2(HC_PRESCALER_MAX) / (hc_exp2(HC_COUNT_PERIOD_BIT) - 1);
	if ((HC_PERIOD_BASE / period_ns) < min_freq) {
		pr_err("(Over limit) Augentix PWM provide lowest frequency is:  %d Hz.\n", min_freq);
		return -ERANGE;
	}

	do {
		/*
		 *   period_ns     count_period
		 *  ----------- = --------------
		 *     10^9         frequency
		 *
		 * frequency = sys_clk / (2^prescaler)
		 *
		 * count_period = frequency * period_ns / 10^9
		 */
		calc_period = (unsigned long long)period_freq * period_ns;
		mod_prd = do_div(calc_period, HC_PERIOD_BASE);
		pr_debug("[PWM] calc_period = %lld\n", calc_period);
		pr_debug("[PWM] mod_prd = %lld\n", mod_prd);

		if (calc_period > 255) {
			cnt_psc = cnt_psc + 1;
			period_freq = period_freq / 2;
		} else {
			if (mod_prd > HC_PERIOD_BASE / 2) {
				calc_period = calc_period + 1;
			}

			pr_info("request frquency: %d Hz\n", HC_PERIOD_BASE / period_ns);
			do_div(period_freq, calc_period);
			pr_info("provide frquency: %d Hz\n", period_freq);
		}
		pr_debug("[PWM] cnt_psc = %d\n", cnt_psc);
		pr_debug("[PWM] calc_period = %lld\n", calc_period);
	} while (calc_period > 255);

	/*
	 *    duty_ns       count_high
	 *  ----------- = --------------
	 *   period_ns     count_period
	 *
	 * count_high = count_period * duty_ns / period_ns
	 */
	calc_high = calc_period * duty_ns;
	do_div(calc_high, period_ns);

	/* Set config to register */
	spin_lock_irqsave(&hc->lock, flags);
	ker_off = hc->mmio_base + (0x10 * cho_ker);
	t_reg = (cnt_psc | (calc_period << 8) | (calc_high << 16));
	writel_relaxed(t_reg, ker_off + PWMX_PRSC_OFFSET);
	spin_unlock_irqrestore(&hc->lock, flags);

	pr_debug("[PWM] prescaler = %d\n", cnt_psc);
	pr_debug("[PWM] calc_high = %lld\n", calc_high);
	pr_debug("[PWM] calc_period = %lld\n", calc_period);
	pr_debug("[PWM] duty_cycle = %u%\n", (unsigned int)calc_high * 100 / (unsigned int)calc_period);

	return 0;
}

static int hc_pwm_enable(struct pwm_chip *chip, struct pwm_device *pwm)
{
	struct hc_pwm_chip *hc = to_hc_pwm_chip(chip);
	void __iomem *ker_off;
	int cho_ker = -1;

	for (hc->idx = 0; hc->idx < 6; hc->idx++)
		if (hc->out_ker[hc->idx] == pwm->hwpwm)
			cho_ker = hc->idx;

	if (cho_ker == -1) {
		pr_warn("PWM output %d does not match the description of device tree.\n", pwm->hwpwm);
		return 0;
	}

	pr_info("Augentix PWM kernel %d enable.\n", cho_ker);

	ker_off = hc->mmio_base + (0x10 * cho_ker);

	if ((readl_relaxed(ker_off + PWMX_BUSY_OFFSET) & 0x1) != 0) {
		pr_info("[PWM] PWM%d busy, try again later.\n", cho_ker);
		return -EAGAIN;
	}
	writel_relaxed(0x10001, ker_off + PWMX_TRIG_OFFSET);

	return 0;
}

static void hc_pwm_disable(struct pwm_chip *chip, struct pwm_device *pwm)
{
	struct hc_pwm_chip *hc = to_hc_pwm_chip(chip);
	void __iomem *ker_off;
	int cho_ker = -1;

	for (hc->idx = 0; hc->idx < 6; hc->idx++) {
		if (hc->out_ker[hc->idx] == pwm->hwpwm) {
			cho_ker = hc->idx;
		}
	}

	if (cho_ker == -1) {
		pr_err("PWM output %d does not match the description of device tree.\n", pwm->hwpwm);
		return;
	}

	pr_info("Augentix PWM kernel %d disable.\n", cho_ker);

	ker_off = hc->mmio_base + (0x10 * cho_ker);

	if ((readl_relaxed(ker_off + PWMX_BUSY_OFFSET) & 0x1) == 0) {
		pr_info("[PWM] PWM kernel %d already disabled.\n", cho_ker);
		return;
	}
	writel_relaxed(0x10001, ker_off + PWMX_TRIG_OFFSET);
}

/**
 * struct pwm_ops - PWM controller operations
 * @request: optional hook for requesting a PWM
 * @free: optional hook for freeing a PWM
 * @config: configure duty cycles and period length for this PWM
 * @set_polarity: configure the polarity of this PWM
 * @enable: enable PWM output toggling
 * @disable: disable PWM output toggling
 * @dbg_show: optional routine to show contents in debugfs
 * @owner: helps prevent removal of modules exporting active PWMs
 */
static const struct pwm_ops hc_pwm_ops = {
	.config = hc_pwm_config,
	.enable = hc_pwm_enable,
	.disable = hc_pwm_disable,
	.owner = THIS_MODULE,
};

static int hc_pwm_probe(struct platform_device *pdev)
{
	struct hc_pwm_chip *hc;
	struct resource *r;
	struct device_node *np = pdev->dev.of_node;
	int ret;
	u32 out_sel[2] = { 0x0, 0x0 };

	hc = devm_kzalloc(&pdev->dev, sizeof(*hc), GFP_KERNEL);
	if (!hc)
		return -ENOMEM;

	hc->dev = &pdev->dev;

	r = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	hc->mmio_base = devm_ioremap_resource(&pdev->dev, r);
	if (IS_ERR(hc->mmio_base))
		return PTR_ERR(hc->mmio_base);

	platform_set_drvdata(pdev, hc);

	hc->chip.dev = &pdev->dev;
	hc->chip.ops = &hc_pwm_ops;
	hc->chip.base = -1;
	hc->chip.npwm = NUM_PWM_OUT;

	hc->p = devm_pinctrl_get(&pdev->dev);
	if (!hc->p) {
		dev_err(&pdev->dev, "Fail to get pinctrl settings\n");
		return -ENODEV;
	}
	hc->pwm_iomux = pinctrl_lookup_state(hc->p, "default");
	if (IS_ERR(hc->pwm_iomux)) {
		dev_err(&pdev->dev, "Fail to find default pinctrl setting\n");
		return -ENODEV;
	}

	if (pinctrl_select_state(hc->p, hc->pwm_iomux)) {
		dev_err(&pdev->dev, "Fail to set pinctrl setting\n");
		return -ENODEV;
	}

	/* get system clock and interrupt */
	for (hc->idx = 0; hc->idx < NUM_PWM_KER; hc->idx++) {
		hc->pwm_clk[hc->idx] = of_clk_get(np, hc->idx);
		if (IS_ERR(hc->pwm_clk[hc->idx])) {
			dev_err(hc->dev, "[PWM] Clock %d unavailable\n", hc->idx);
			return -ENODEV;
		}

		hc->pwm_hz[hc->idx] = clk_get_rate(hc->pwm_clk[hc->idx]);
		ret = clk_prepare_enable(hc->pwm_clk[hc->idx]);
		if (ret < 0) {
			dev_err(&pdev->dev, "failed to enable clock for kernel %d\n", hc->idx);
			return ret;
		}
		pr_info("Kernel %d clock freq = %d Hz\n", hc->idx, hc->pwm_hz[hc->idx]);

		hc->irq[hc->idx] = irq_of_parse_and_map(pdev->dev.of_node, hc->idx);
		ret = devm_request_irq(&pdev->dev, hc->irq[hc->idx], hc_pwm_irq_handler, IRQF_SHARED, pdev->name, hc);
		if (ret != 0) {
			dev_err(&pdev->dev, "[PWM] Failed to install irq %d", hc->idx);
			return -EINVAL;
		}
	}

	ret = pwmchip_add(&hc->chip);
	if (ret < 0) {
		dev_err(&pdev->dev, "pwmchip_add() failed: %d\n", ret);
		return ret;
	}

	/* get output select from device tree */
	if (!of_property_read_u32_array(np, "outsel", hc->out_ker, NUM_PWM_KER)) {
		for (hc->idx = 0; hc->idx < NUM_PWM_KER; hc->idx++) {
			pr_info("[PWM] Kernel %d => out-sel = %d\n", hc->idx, hc->out_ker[hc->idx]);
			if (hc->out_ker[hc->idx] < 4)
				out_sel[0] |= (hc->idx << (8 * hc->out_ker[hc->idx]));
			else
				out_sel[1] |= (hc->idx << (8 * (hc->out_ker[hc->idx] - 4)));

			writel_relaxed(0x0, hc->mmio_base + (0x10 * hc->idx) + PWMX_IRQM_OFFSET);
		}
	} else {
		pr_warn("[PWM] Fail to get out-sel from device tree, use default value\n");
		out_sel[0] = 0x03020100;
		out_sel[1] = 0x00000504;
	}

	pr_debug("[PWM] out_sel[0] = 0x%08X, out_sel[1] = 0x%08X\n", out_sel[0], out_sel[1]);
	spin_lock_init(&hc->lock);
	writel_relaxed(out_sel[0], hc->mmio_base + PWMO_SEL0123_OFFSET);
	writel_relaxed(out_sel[1], hc->mmio_base + PWMO_SEL45_OFFSET);

	pr_info("Augentix PWM driver probed.\n");

	return 0;
}

static int hc_pwm_remove(struct platform_device *pdev)
{
	struct hc_pwm_chip *hc = platform_get_drvdata(pdev);

	pr_info("Augentix PWM driver removed.\n");
	for (hc->idx = 0; hc->idx < NUM_PWM_KER; hc->idx++)
		clk_disable_unprepare(hc->pwm_clk[hc->idx]);

	return pwmchip_remove(&hc->chip);
}

static const struct of_device_id hc_pwm_of_match[] = {
	{ .compatible = "augentix,pwm" },
	{},
};

MODULE_DEVICE_TABLE(of, hc_pwm_of_match);

static struct platform_driver hc_pwm_driver = {
	.driver = {
		.name = "hc_pwm",
		.owner = THIS_MODULE,
		.of_match_table = hc_pwm_of_match,
	},
	.probe = hc_pwm_probe,
	.remove = hc_pwm_remove,
};

module_platform_driver(hc_pwm_driver);

MODULE_DESCRIPTION("Augentix PWM driver");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Augentix Inc.");
