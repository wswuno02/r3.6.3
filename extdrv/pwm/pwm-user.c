#include <linux/module.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/err.h>

#define DRV_NAME "pwm_user"

extern void pwm0_start(int mode);
extern void pwm0_stop(void);

static int __init pwmusr_init(void)
{
	pwm0_start(0);
	while(1);
	return 0;
}
module_init(pwmusr_init);

static void __exit pwmusr_exit(void)
{
	pwm0_stop();
}
module_exit(pwmusr_exit);

MODULE_DESCRIPTION("PWM user testing module");
MODULE_AUTHOR("ShihChieh Lin<shihchieh.lin@augentix.com>");
MODULE_LICENSE("GPL");
