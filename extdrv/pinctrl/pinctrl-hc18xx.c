#include <linux/init.h>
#include <linux/module.h>
#include <linux/io.h>
#include <linux/slab.h>
#include <linux/err.h>
#include <linux/list.h>
#include <linux/gpio.h>

#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/of_address.h>

#include <linux/pinctrl/pinctrl.h>
#include <linux/pinctrl/pinconf.h>
#include <linux/pinctrl/pinmux.h>
#include <linux/pinctrl/machine.h>

#include <linux/export.h>
#include <mach/hc18xx.h>
#include "pinctrl-hc18xx.h"

/***************
* PART 1: GPIO *
***************/

/* This is the debug GPIO function */
static void __iomem *reg_base;
int hc_trigger_gpio(int pin_id, int level)
{
	/*OE*/
	u16 gpio_oe = 0x0100;
	u16 gpio_o = (level == 1) ? 0x01 : 0x00;
	void __iomem *gpio = (void *__iomem)(TO_VA(HC18XX_IOC_PA)) + 0x50 + (pin_id << 2);
	writew_relaxed((gpio_oe | gpio_o), gpio);
	pr_debug("Write 0x%08x to address 0x%08x + offset 0x%08x\n", (gpio_oe | gpio_o),
	         (uint32_t)((void *__iomem)(TO_VA(HC18XX_IOC_PA)) + 0x50), (pin_id << 2));
	return 0;
}
EXPORT_SYMBOL_GPL(hc_trigger_gpio);
/*****/

static int hc18xx_gpio_request(struct gpio_chip *chip, unsigned offset)
{
	return pinctrl_request_gpio(chip->base + offset);
}

static void hc18xx_gpio_free(struct gpio_chip *chip, unsigned offset)
{
	pinctrl_free_gpio(chip->base + offset);
}

static void hc18xx_gpio_set(struct gpio_chip *chip, unsigned offset, int value)
{
	struct hc18xx_pctl_drvdata *drvdata = container_of(chip, struct hc18xx_pctl_drvdata, gpio_chip);
	void __iomem *reg = drvdata->base + (offset << 2) + HC18XX_GPO_OFFSET;

	/*dev_info(drvdata->dev, "Pin %d: Writes 0x%02x to %p\n",
	    offset, value, reg);*/
	writeb_relaxed(value, reg);
}

static int hc18xx_gpio_get(struct gpio_chip *chip, unsigned offset)
{
	struct hc18xx_pctl_drvdata *drvdata = container_of(chip, struct hc18xx_pctl_drvdata, gpio_chip);
	void __iomem *reg = drvdata->base + (offset << 2) + HC18XX_GPO_OFFSET;
	int val = readl_relaxed(reg);

	if (val & GPIO_OE_BIT) {
		/*dev_info(drvdata->dev, "GPO %d: Reads 0x%02x from %p\n",
		    offset, val & GPIO_O_BIT, reg);*/
		return (val & GPIO_O_BIT);
	} else {
		reg = drvdata->base + offset + HC18XX_GPI_OFFSET;
		val = readb_relaxed(reg);
		/*dev_info(drvdata->dev, "GPI %d: Reads 0x%02x from %p\n",
		    offset, readb_relaxed(reg), reg);*/
		return val & 0x01;
	}
}

static int hc18xx_gpio_set_direction(struct gpio_chip *chip, unsigned offset, u8 oe)
{
	struct hc18xx_pctl_drvdata *drvdata = container_of(chip, struct hc18xx_pctl_drvdata, gpio_chip);
	void __iomem *reg = drvdata->base + (offset << 2) + HC18XX_GPO_OFFSET;

	/*dev_info(drvdata->dev, "Pin %d: Set OE at %p to 0x%02x\n",
	    offset, reg + 1, oe);*/

	writeb_relaxed(oe, reg + 1);
	return 0;
}

static int hc18xx_gpio_direction_input(struct gpio_chip *chip, unsigned offset)
{
	return hc18xx_gpio_set_direction(chip, offset, 0);
}

static int hc18xx_gpio_direction_output(struct gpio_chip *chip, unsigned offset, int value)
{
	hc18xx_gpio_set(chip, offset, value);
	return hc18xx_gpio_set_direction(chip, offset, 1);
}

static const struct gpio_chip hc18xx_gpio_chip = {
	.request = hc18xx_gpio_request,
	.free = hc18xx_gpio_free,
	.set = hc18xx_gpio_set,
	.get = hc18xx_gpio_get,
	.direction_input = hc18xx_gpio_direction_input,
	.direction_output = hc18xx_gpio_direction_output,
	.owner = THIS_MODULE,
};

static int hc18xx_gpio_chip_register(struct platform_device *pdev, struct hc18xx_pctl_drvdata *drvdata)
{
	struct gpio_chip *gc;
	int ret;
	drvdata->gpio_chip = hc18xx_gpio_chip;

	/* Pin controller node as GPIO controller */
	gc = &drvdata->gpio_chip;
	{
		gc->base = 0;
		gc->ngpio = drvdata->pctldesc->npins;
		gc->dev = drvdata->dev;
		gc->of_node = gc->dev->of_node;
		gc->label = drvdata->pctldesc->name;
	}

	ret = gpiochip_add(gc);
	if (ret) {
		dev_err(&pdev->dev, "Failed to register gpio_chip %s for GPIO controller, error code: %d\n", gc->label,
		        ret);
		return ret;
	}

	return 0;
}

/*****************
* PART 2: Pinmux *
*****************/

static int hc18xx_pinctrl_get_groups_count(struct pinctrl_dev *pctldev)
{
	struct hc18xx_pctl_drvdata *d = pinctrl_dev_get_drvdata(pctldev);

	return d->ngroups;
}

static const char *hc18xx_pinctrl_get_group_name(struct pinctrl_dev *pctldev, unsigned selector)
{
	struct hc18xx_pctl_drvdata *d = pinctrl_dev_get_drvdata(pctldev);

	return d->groups[selector].name;
}

static int hc18xx_pinctrl_get_group_pins(struct pinctrl_dev *pctldev, unsigned selector, const unsigned **pins,
                                         unsigned *num_pins)
{
	struct hc18xx_pctl_drvdata *d = pinctrl_dev_get_drvdata(pctldev);

	*pins = d->groups[selector].pins;
	*num_pins = d->groups[selector].npins;
	return 0;
}

/* Translate value of specific configuration type to config value */
static unsigned long config_xlate(struct pinctrl_dev *pctldev, uint32_t val, const struct pincfg *cfg_param)
{
	return (((val & cfg_param->mask) << cfg_param->type) | (cfg_param->mask << (cfg_param->type - 16)));
}

static int hc18xx_pinctrl_dt_node_to_map(struct pinctrl_dev *pctldev, struct device_node *np, struct pinctrl_map **map,
                                         unsigned *num_maps)
{
	struct pinctrl_map *new_maps;
	struct pinctrl_map *muxgrp_map = NULL, *cfggrp_map = NULL;
	char *group = NULL;
	struct hc18xx_pctl_drvdata *d = pinctrl_dev_get_drvdata(pctldev);
	struct device *dev = d->dev;
	unsigned long *config;
	int i, ret = 0, has_config = false;
	int len;
	uint32_t val;

	if (of_property_read_u32(np, "reg", &val)) {
		dev_err(dev, "Failed to parse dt of %s\n", np->name);
		ret = -EINVAL;
		goto free;
	}

	new_maps = kzalloc(2 * sizeof(*new_maps), GFP_KERNEL);
	if (!new_maps) {
		dev_err(dev, "Failed to allocate map of %s\n", np->name);
		return -ENOMEM;
	}

	config = kzalloc(sizeof(*config), GFP_KERNEL);
	if (!config) {
		dev_err(dev, "Failed to allocate config of %s\n", np->name);
		return -ENOMEM;
	}

	muxgrp_map = &new_maps[0];
	muxgrp_map->type = PIN_MAP_TYPE_MUX_GROUP;

	cfggrp_map = &new_maps[1];
	cfggrp_map->type = PIN_MAP_TYPE_CONFIGS_GROUP;

	len = strlen(np->name) + SUFFIX_LEN;
	group = kzalloc(len, GFP_KERNEL);
	if (!group) {
		dev_err(dev, "Failed to allocate group of %s\n", np->name);
		ret = -ENOMEM;
		goto free;
	}
	snprintf(group, len, "%s.%d", np->name, val);
	muxgrp_map->data.mux.group = group;
	muxgrp_map->data.mux.function = np->name;

	/* combine all configs to a single config value */
	*config = 0;
	for (i = 0; i < ARRAY_SIZE(cfg_params); i++) {
		ret = of_property_read_u32(np, cfg_params[i].of_property, &val);
		if (!ret) {
			has_config = true;
			/* use lower 16-bit as enable flags */
			*config |= config_xlate(pctldev, val, &cfg_params[i]);
		} else if (ret != -EINVAL) {
			dev_err(dev, "Failed to parse pinconf of %s\n", np->name);
			goto free;
		}
	}

	if (has_config) {
		cfggrp_map->data.configs.group_or_pin = group;
		cfggrp_map->data.configs.configs = config;
		cfggrp_map->data.configs.num_configs = 1;
	}

	*map = new_maps;
	*num_maps = has_config ? 2 : 1;

	return 0;

free:
	return ret;
}

static void hc18xx_pinctrl_dt_free_map(struct pinctrl_dev *pctldev, struct pinctrl_map *map, unsigned num_maps)
{
	int i;

	for (i = 0; i < num_maps; i++) {
		switch (map[i].type) {
		case PIN_MAP_TYPE_CONFIGS_PIN:
		case PIN_MAP_TYPE_CONFIGS_GROUP:
			kfree(map[i].data.configs.configs);
			break;
		case PIN_MAP_TYPE_MUX_GROUP:
			kfree(map[i].data.mux.group);
			break;
		default:
			break;
		}
	}
	kfree(map);
	return;
}

static const struct pinctrl_ops hc18xx_pinctrl_ops = {
	.get_groups_count = hc18xx_pinctrl_get_groups_count,
	.get_group_name = hc18xx_pinctrl_get_group_name,
	.get_group_pins = hc18xx_pinctrl_get_group_pins,
	.dt_node_to_map = hc18xx_pinctrl_dt_node_to_map,
	.dt_free_map = hc18xx_pinctrl_dt_free_map,
};

static int hc18xx_get_functions_count(struct pinctrl_dev *pctldev)
{
	struct hc18xx_pctl_drvdata *d = pinctrl_dev_get_drvdata(pctldev);

	return d->nfuncs;
}

static const char *hc18xx_get_fname(struct pinctrl_dev *pctldev, unsigned selector)
{
	struct hc18xx_pctl_drvdata *d = pinctrl_dev_get_drvdata(pctldev);

	return d->functions[selector].name;
}

static int hc18xx_get_groups(struct pinctrl_dev *pctldev, unsigned selector, const char *const **groups,
                             unsigned *const num_groups)
{
	struct hc18xx_pctl_drvdata *d = pinctrl_dev_get_drvdata(pctldev);

	*groups = d->functions[selector].groups;
	*num_groups = d->functions[selector].ngroups;
	return 0;
}

static int hc18xx_set_mux(struct pinctrl_dev *pctldev, unsigned fn_sel, unsigned grp_sel)
{
	struct hc18xx_pctl_drvdata *d = pinctrl_dev_get_drvdata(pctldev);
	struct hc18xx_pin_group *g = &d->groups[grp_sel];
	void __iomem *reg_byte;
	uint8_t muxsel;
	int i;

	for (i = 0; i < g->npins; i++) {
		reg_byte = d->base + g->pins[i];
		muxsel = g->muxsel[i];
		writeb_relaxed(muxsel, reg_byte);
	}

	return 0;
}

static const struct pinmux_ops hc18xx_pinmux_ops = {
	.get_functions_count = hc18xx_get_functions_count,
	.get_function_name = hc18xx_get_fname,
	.get_function_groups = hc18xx_get_groups,
	.set_mux = hc18xx_set_mux,
};

/******************
* PART 3: Pinconf *
******************/

/* register to config */
static unsigned long read_config_from_reg(void __iomem *base)
{
	return readl_relaxed(base) | 0x0000030f;
}

/* register rmw */
static void write_config_to_reg(void __iomem *base, unsigned long config)
{
	uint32_t reg = readl_relaxed(base);
	uint32_t mask = (config & 0xffff) << 16;

	/* register bit[15:0] are preserved for GPIO */
	reg = (reg & 0xffff) | ((uint32_t)config & mask);

	writel_relaxed(reg, base);
}

static int hc18xx_pinconf_rw(struct pinctrl_dev *pctldev, unsigned int pin, unsigned long *config, bool set)
{
	struct hc18xx_pctl_drvdata *d = pinctrl_dev_get_drvdata(pctldev);
	void __iomem *reg = d->base + HC18XX_GPO_OFFSET + (pin << 2);

	if (set)
		write_config_to_reg(reg, *config);
	else
		*config = read_config_from_reg(reg);

	return 0;
}

static int hc18xx_pinconf_get(struct pinctrl_dev *pctldev, unsigned int pin, unsigned long *config)
{
	return hc18xx_pinconf_rw(pctldev, pin, config, false);
}

static int hc18xx_pinconf_set(struct pinctrl_dev *pctldev, unsigned int pin, unsigned long *configs,
                              unsigned num_configs)
{
	int i, ret;

	for (i = 0; i < num_configs; i++) {
		ret = hc18xx_pinconf_rw(pctldev, pin, &configs[i], true);
		if (ret < 0)
			return ret;
	} /* for each config */

	return 0;
}

static int hc18xx_pinconf_group_set(struct pinctrl_dev *pctldev, unsigned group, unsigned long *configs,
                                    unsigned num_configs)
{
	struct hc18xx_pctl_drvdata *d = pinctrl_dev_get_drvdata(pctldev);
	const unsigned int *pins;
	unsigned int i;

	pins = d->groups[group].pins;

	for (i = 0; i < d->groups[group].npins; i++)
		hc18xx_pinconf_set(pctldev, pins[i], configs, num_configs);

	return 0;
}

static int hc18xx_pinconf_group_get(struct pinctrl_dev *pctldev, unsigned int group, unsigned long *config)
{
	struct hc18xx_pctl_drvdata *d = pinctrl_dev_get_drvdata(pctldev);
	const unsigned int *pins;

	pins = d->groups[group].pins;
	hc18xx_pinconf_get(pctldev, pins[0], config);

	return 0;
}

static const struct pinconf_ops hc18xx_pinconf_ops = {
	.pin_config_get = hc18xx_pinconf_get,
	.pin_config_set = hc18xx_pinconf_set,
	.pin_config_group_get = hc18xx_pinconf_group_get,
	.pin_config_group_set = hc18xx_pinconf_group_set,
};

/**************************************
* PART 5: Pinctrl driver registration *
**************************************/

/* pinctrl descriptor; to be registered to pinctrl subsystem */
struct pinctrl_desc hc18xx_pinctrl_desc = {
	.name = DRIVER_NAME,
	.pins = hc18xx_pins,
	.npins = ARRAY_SIZE(hc18xx_pins),
	.owner = THIS_MODULE,
	.pctlops = &hc18xx_pinctrl_ops,
	.pmxops = &hc18xx_pinmux_ops,
	.confops = &hc18xx_pinconf_ops,
};

static int hc18xx_pinctrl_parse_group(struct hc18xx_pctl_drvdata *d, struct device_node *np, int idx,
                                      const char **out_name)
{
	struct hc18xx_pin_group *g = &d->groups[idx];
	struct device *dev = d->dev;
	struct property *prop;
	const char *propname = "augentix,pmx";
	char *group;
	int length;
	uint32_t val, i;
	uint32_t *buf;

	if (g == NULL) {
		dev_err(dev, "Failed to find pin group!\n");
		return -EINVAL;
	}

	dev_dbg(dev, "Parsing pin group %s...\n", np->name);

	length = strlen(np->name) + SUFFIX_LEN;
	group = devm_kzalloc(dev, length, GFP_KERNEL);
	if (!group)
		return -ENOMEM;
	if (of_property_read_u32(np, "reg", &val))
		snprintf(group, length, "%s", np->name);
	else
		snprintf(group, length, "%s.%d", np->name, val);
	g->name = group;

	prop = of_find_property(np, propname, &length);
	if (!prop)
		return -EINVAL;
	g->npins = length / (2 * sizeof(uint32_t));

	g->pins = devm_kzalloc(dev, g->npins * sizeof(*g->pins), GFP_KERNEL);
	if (!g->pins)
		return -ENOMEM;

	g->muxsel = devm_kzalloc(dev, g->npins * sizeof(*g->muxsel), GFP_KERNEL);
	if (!g->muxsel)
		return -ENOMEM;

	buf = devm_kzalloc(dev, 2 * g->npins * sizeof(*buf), GFP_KERNEL);
	if (!buf)
		return -ENOMEM;

	of_property_read_u32_array(np, propname, buf, 2 * g->npins);
	for (i = 0; i < g->npins; i++) {
		g->pins[i] = buf[2 * i];
		g->muxsel[i] = buf[(2 * i) + 1];
	}

	if (out_name)
		*out_name = g->name;

	return 0;
}

static int hc18xx_pinctrl_probe_dt(struct platform_device *pdev, struct hc18xx_pctl_drvdata *d)
{
	struct device_node *np = pdev->dev.of_node;
	struct device_node *child;
	struct device *dev = &pdev->dev;
	struct hc18xx_pmx_func *f;
	const char *gpio_compat = "augentix,gpio";
	const char *fn, *fnull = "";
	int i = 0, idxf, idxg;
	int ret;
	uint32_t val;

	child = of_get_next_child(np, NULL);
	if (!child) {
		dev_err(dev, "no group is defined\n");
		return -ENOENT;
	}

	/* Count total functions and groups */
	fn = fnull;
	for_each_child_of_node (np, child) {
		/* When counting pin group, skip pure GPIO sub-nodes */
		if (of_device_is_compatible(child, gpio_compat))
			continue;
		d->ngroups++; /* Each non-GPIO child node represents a single pin group */

		/* When counting pin function, skip non-pinmux node */
		if (of_property_read_u32(child, "reg", &val))
			continue;
		/* Increase nfuncs by 1 if the name of the child node is
		 * different from the previous one, or it is the first child node */
		if (strcmp(fn, child->name)) {
			fn = child->name;
			d->nfuncs++;
		}
	}

	/* Allocate memory for group and functions */
	d->functions = devm_kzalloc(dev, d->nfuncs * sizeof(*d->functions), GFP_KERNEL);
	if (!d->functions)
		return -ENOMEM;

	d->groups = devm_kzalloc(dev, d->ngroups * sizeof(*d->groups), GFP_KERNEL);
	if (!d->groups)
		return -ENOMEM;

	/* Iterate each note to find matched groups for each function. Groups of the same function are to be adjacent */
	fn = fnull;
	idxf = 0;
	f = &d->functions[0];
	for_each_child_of_node (np, child) {
		/* Skip pure GPIO or pinconf nodes */
		if (of_device_is_compatible(child, gpio_compat))
			continue;
		if (of_property_read_u32(child, "reg", &val))
			continue;
		if (strcmp(fn, child->name)) {
			f = &d->functions[idxf++];
			f->name = fn = child->name;
		}
		f->ngroups++;
	};

	/* Get groups for each function */
	idxf = 0;
	idxg = 0;
	fn = fnull;
	for_each_child_of_node (np, child) {
		/* Skip pure GPIO nodes */
		if (of_device_is_compatible(child, gpio_compat))
			continue;
		/* For non-config nodes */
		if (of_property_read_u32(child, "reg", &val)) {
			ret = hc18xx_pinctrl_parse_group(d, child, idxg++, NULL);
			if (ret)
				return ret;
			continue;
		}

		if (strcmp(fn, child->name)) {
			f = &d->functions[idxf++];
			f->groups = devm_kzalloc(&pdev->dev, f->ngroups * sizeof(*f->groups), GFP_KERNEL);
			if (!f->groups)
				return -ENOMEM;
			fn = child->name;
			i = 0;
		}
		ret = hc18xx_pinctrl_parse_group(d, child, idxg++, &f->groups[i++]);
		if (ret)
			return ret;
	}

	return 0;
}

static int hc18xx_pinctrl_probe(struct platform_device *pdev)
{
	struct hc18xx_pctl_drvdata *hpctl;
	struct resource *res;
	struct device *dev = &pdev->dev;
	int ret;

	dev_info(&pdev->dev, "Initializing HC18xx pin control driver...\n");

	/* Create state holders etc for this driver */
	hpctl = devm_kzalloc(dev, sizeof(*hpctl), GFP_KERNEL);
	if (!hpctl)
		return -ENOMEM;
	hpctl->dev = &pdev->dev;

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);

	/*
	hpctl->base = devm_ioremap_resource(&pdev->dev, res);
	if (IS_ERR(hpctl->base))
	{
	    dev_err(dev, "Failed to map I/O address!\n");
	*/
	hpctl->base = (void *)(TO_VA(HC18XX_IOC_PA));
	if (IS_ERR(hpctl->base)) /* fallback to device tree */
	{
		dev_err(dev, "Failed to apply static IOC mapping; use ioremap instead\n");
		hpctl->base = devm_ioremap_resource(&pdev->dev, res);
		if (IS_ERR(hpctl->base)) /* ioremap failed */
			return PTR_ERR(hpctl->base);
	}

	/* Used only by trigger_gpio */
	reg_base = hpctl->base;
	ret = hc18xx_pinctrl_probe_dt(pdev, hpctl);
	if (ret) {
		dev_err(dev, "Device tree probe failed: %d\n", ret);
		return -EINVAL;
	}
	hpctl->pctldesc = &hc18xx_pinctrl_desc;

	ret = hc18xx_gpio_chip_register(pdev, hpctl);
	if (ret) {
		dev_err(dev, "GPIO registration failed: %d\n", ret);
		return ret;
	}

	hpctl->pctldev = pinctrl_register(&hc18xx_pinctrl_desc, dev, hpctl);
	if (!hpctl->pctldev) {
		dev_err(dev, "Failed to register hc18xx pin control device\n");
		return -ENOMEM;
	}

	/* GPIO range */
	{
		hpctl->grange.id = 0;
		hpctl->grange.name = hpctl->pctldesc->name;
		hpctl->grange.gc = &hpctl->gpio_chip;
		hpctl->grange.base = hpctl->gpio_chip.base;
		hpctl->grange.pin_base = hpctl->gpio_chip.base;
		hpctl->grange.npins = hpctl->gpio_chip.ngpio;
	}
	pinctrl_add_gpio_range(hpctl->pctldev, &hpctl->grange);

	platform_set_drvdata(pdev, hpctl);

	dev_info(dev, "HC18xx pin control driver initialized\n");
	return 0;
}

static struct of_device_id hc18xx_pinctrl_of_match[] = {
	{ .compatible = "augentix,pinctrl" },
	{},
};

static struct platform_driver hc18xx_pinctrl_driver =
{
	.driver = {
		.name = DRIVER_NAME,
		.owner = THIS_MODULE,
		.of_match_table = hc18xx_pinctrl_of_match,
	},
	.probe = hc18xx_pinctrl_probe,
};

static int __init hc18xx_pinctrl_init(void)
{
	return platform_driver_register(&hc18xx_pinctrl_driver);
}
arch_initcall(hc18xx_pinctrl_init);

MODULE_AUTHOR("Shih-Chieh Lin, Augentix <shihchieh.lin@augentix.com>");
MODULE_DESCRIPTION("Augentix HC18XX pin control driver");
MODULE_LICENSE("GPL v2");
