#include <linux/module.h>
#include <linux/err.h>
#include <linux/platform_device.h>

#include <asm/io.h>
#include <linux/slab.h>
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/pinctrl/consumer.h>

#define DRV_NAME "pinctrl_test"

#define PINCTRL_TEST_DEBUG (1)

#ifdef PINCTRL_TEST_DEBUG
#define DBG(fmt, args...)                                                           \
	do {                                                                        \
		printk("[PINCTRL_TEST] (%d, %s) " fmt, __LINE__, __func__, ##args); \
	} while (0)
#endif

struct pinctrl_test_data {
	struct device *dev;
	struct pinctrl *p;
	struct pinctrl_state *stage3v3;
	struct pinctrl_state *stage1v8;
	struct pinctrl_state *test[2];
};

static int pinctrl_test_probe(struct platform_device *pdev)
{
	struct pinctrl_test_data *pinctrl_test = NULL;
	int ret;

	DBG("Initializing HC1703_1723_1753_1783S pinctrl test driver...\n");

	pinctrl_test = devm_kzalloc(&pdev->dev, sizeof(struct pinctrl_test_data), GFP_KERNEL);
	if (!pinctrl_test)
		return -ENOMEM;
	pinctrl_test->dev = &pdev->dev;

	platform_set_drvdata(pdev, pinctrl_test);

	/* Setup pinctrl */
	pinctrl_test->p = devm_pinctrl_get(pinctrl_test->dev);
	if (IS_ERR(pinctrl_test->p)) {
		DBG("Get pinctrl device fail\n");
	}

	pinctrl_test->stage3v3 = pinctrl_lookup_state(pinctrl_test->p, "sd_poc_3v3");
	if (IS_ERR(pinctrl_test->stage3v3)) {
		DBG("PINCTRL driver not available: sd_poc_3v3\n");
	}

	pinctrl_test->stage1v8 = pinctrl_lookup_state(pinctrl_test->p, "sd_poc_1v8");
	if (IS_ERR(pinctrl_test->stage1v8)) {
		DBG("PINCTRL driver not available: sd_poc_1v8\n");
	}

	ret = pinctrl_select_state(pinctrl_test->p, pinctrl_test->stage1v8);
	if (ret < 0) {
		DBG("Set stage1v8 fail!\n");
	}

	pinctrl_test->test[0] = pinctrl_lookup_state(pinctrl_test->p, "test_f0");
	if (IS_ERR(pinctrl_test->stage3v3)) {
		DBG("PINCTRL driver not available: test_f0\n");
	}

	pinctrl_test->test[1] = pinctrl_lookup_state(pinctrl_test->p, "test_f1");
	if (IS_ERR(pinctrl_test->stage3v3)) {
		DBG("PINCTRL driver not available: test_f1\n");
	}

	ret = pinctrl_select_state(pinctrl_test->p, pinctrl_test->test[0]);
	if (ret < 0) {
		DBG("Set test_f0 fail!\n");
	}

	ret = pinctrl_select_state(pinctrl_test->p, pinctrl_test->test[1]);
	if (ret < 0) {
		DBG("Set test_f1 fail!\n");
	}

	DBG("PINCTRL test module probed.\n");
	return 0;
}

static int pinctrl_test_remove(struct platform_device *pdev)
{
	struct pinctrl_test_data *pinctrl_test = platform_get_drvdata(pdev);
	devm_kfree(&pdev->dev, pinctrl_test);
	platform_set_drvdata(pdev, NULL);
	return 0;
}

static const struct of_device_id pinctrl_test_dt_ids[] = {
	{ .compatible = "augentix,pinctrl-test" },
	{},
};
MODULE_DEVICE_TABLE(of, pinctrl_test_dt_ids);

static struct platform_driver pinctrl_test_driver = {
	.probe = pinctrl_test_probe,
	.remove = pinctrl_test_remove,
	.driver =
	        {
	                .owner = THIS_MODULE,
	                .name = DRV_NAME,
	                .of_match_table = pinctrl_test_dt_ids,
	        },
};
module_platform_driver(pinctrl_test_driver);

MODULE_AUTHOR("Louis Yang, Augentix <louis.yang@augentix.com>");
MODULE_DESCRIPTION("PINCTRL test driver");
MODULE_LICENSE("GPL");
