#ifndef PINCTRL_HC18XX_H
#define PINCTRL_HC18XX_H

#include <linux/spinlock.h>
#include <linux/gpio.h>

#define DRIVER_NAME "hc18xx_pinctrl"
#define SUFFIX_LEN 4

#define HC18XX_PINMUX_OFFSET 0x0
#define HC18XX_GPO_OFFSET 0x50
#define HC18XX_GPI_OFFSET 0x190

#define GPIO_O_BIT (1 << 0)
#define GPIO_OE_BIT (1 << 8)
#define GPIO_ST_BIT (1 << 16)
#define GPIO_SL_BIT (1 << 17)
#define GPIO_PUD_BIT (3 << 18)
#define GPIO_DS_BIT (3 << 24)

/* pinctrl device instance */
struct hc18xx_pctl_drvdata {
	struct device *dev;
	struct pinctrl_dev *pctldev;
	struct pinctrl_desc *pctldesc;
	void __iomem *base;

	struct hc18xx_pmx_func *functions;
	int nfuncs;
	struct hc18xx_pin_group *groups;
	int ngroups;

	struct gpio_chip gpio_chip;
	struct pinctrl_gpio_range grange;
};

/*
 * struct hc18xx_pin_group: describes a pin group
 * @name: name of this pin group
 * @pins: an array of discrete physical pins used in this group, taken
 *  from the driver-local pin enumeration space
 * @npins: the number of pins in this group array, i.e. the number of
 *  elements in .pins so we can iterate over that array
 */
struct hc18xx_pin_group {
	const char *name;
	unsigned int *pins;
	unsigned npins;
	uint8_t *muxsel;
};

/*
 * @name: name of the function group
 * @groups: a set of names, each of which refers to a pin group
 * @ngroups: number of the assigned pin groups
 */
struct hc18xx_pmx_func {
	const char *name;
	const char **groups;
	unsigned ngroups;
};

/*
 * pincfg_t: Supported pin configuration types
 * @PINCFG_ST: Schmitt trigger configuration
 * @PINCFG_SL: Slew rate flag configuration
 * @PINCFG_PUD: Pull up/down configuration
 * @PINCFG_DS: Driving strength configuration
 */
enum pincfg_type { PINCFG_ST = 16, PINCFG_SL = 17, PINCFG_PUD = 18, PINCFG_DS = 24 };

struct pincfg {
	const char *of_property;
	uint8_t mask;
	enum pincfg_type type;
};

const struct pincfg cfg_params[] = {
	{ "augentix,ioc-st", 0x1, PINCFG_ST },
	{ "augentix,ioc-sl", 0x1, PINCFG_SL },
	{ "augentix,ioc-pud", 0x3, PINCFG_PUD }, /* 2-bit */
	{ "augentix,ioc-ds", 0x3, PINCFG_DS } /* 2-bit */
};

const struct pinctrl_pin_desc hc18xx_pins[] = {
	PINCTRL_PIN(0, "JTAG_TCK"),     PINCTRL_PIN(1, "JTAG_TMS"),     PINCTRL_PIN(2, "JTAG_TDI"),
	PINCTRL_PIN(3, "JTAG_TDO"),     PINCTRL_PIN(4, "JTAG_TRST_N"),  PINCTRL_PIN(5, "JTAG_MODE"),
	PINCTRL_PIN(6, "PWM0"),         PINCTRL_PIN(7, "UART1_RXD"),    PINCTRL_PIN(8, "UART1_TXD"),
	PINCTRL_PIN(9, "UART1_CTS"),    PINCTRL_PIN(10, "UART1_RTS"),   PINCTRL_PIN(11, "SPI0_CK"),
	PINCTRL_PIN(12, "SPI0_SDO"),    PINCTRL_PIN(13, "SPI0_SDI"),    PINCTRL_PIN(14, "EMAC_CRS"),
	PINCTRL_PIN(15, "EMAC_COL"),    PINCTRL_PIN(16, "EMAC_RX_D1"),  PINCTRL_PIN(17, "EMAC_RX_CK"),
	PINCTRL_PIN(18, "EMAC_RX_D0"),  PINCTRL_PIN(19, "EMAC_RX_D3"),  PINCTRL_PIN(20, "EMAC_RX_D2"),
	PINCTRL_PIN(21, "EMAC_RX_CTL"), PINCTRL_PIN(22, "EMAC_TX_CTL"), PINCTRL_PIN(23, "EMAC_MDC"),
	PINCTRL_PIN(24, "EMAC_TX_D1"),  PINCTRL_PIN(25, "EMAC_TX_CK"),  PINCTRL_PIN(26, "EMAC_TX_D0"),
	PINCTRL_PIN(27, "EMAC_MDIO"),   PINCTRL_PIN(28, "EMAC_TX_D3"),  PINCTRL_PIN(29, "EMAC_TX_D2"),
	PINCTRL_PIN(30, "SD_D0"),       PINCTRL_PIN(31, "SD_D1"),       PINCTRL_PIN(32, "SD_D3"),
	PINCTRL_PIN(33, "SD_D2"),       PINCTRL_PIN(34, "SD_CK"),       PINCTRL_PIN(35, "SD_CMD"),
	PINCTRL_PIN(36, "SD_WP"),       PINCTRL_PIN(37, "SD_CD"),       PINCTRL_PIN(38, "QSPI_SCK"),
	PINCTRL_PIN(39, "QSPI_SIO2"),   PINCTRL_PIN(40, "QSPI_SIO3"),   PINCTRL_PIN(41, "QSPI_SIO1"),
	PINCTRL_PIN(42, "QSPI_SIO0"),   PINCTRL_PIN(43, "QSPI_CE"),     PINCTRL_PIN(44, "SPI2_SDO"),
	PINCTRL_PIN(45, "SPI2_CK"),     PINCTRL_PIN(46, "SPI2_SDI"),    PINCTRL_PIN(47, "UART0_TXD"),
	PINCTRL_PIN(48, "UART0_RTS"),   PINCTRL_PIN(49, "UART0_RXD"),   PINCTRL_PIN(50, "UART0_CTS"),
	PINCTRL_PIN(51, "I2C0_SDA"),    PINCTRL_PIN(52, "I2C0_SCL"),    PINCTRL_PIN(53, "I2S_RX_SD"),
	PINCTRL_PIN(54, "I2S_TX_SD"),   PINCTRL_PIN(55, "I2S_RX_WS"),   PINCTRL_PIN(56, "I2S_TX_CK"),
	PINCTRL_PIN(57, "I2S_TX_WS"),   PINCTRL_PIN(58, "I2S_RX_CK"),   PINCTRL_PIN(59, "WATCHDOG"),
	PINCTRL_PIN(60, "PWM6"),        PINCTRL_PIN(61, "PWM3"),        PINCTRL_PIN(62, "SPI1_SS_N"),
	PINCTRL_PIN(63, "SPI1_SDO"),    PINCTRL_PIN(64, "SPI1_CK"),     PINCTRL_PIN(65, "SPI1_SDI"),
	PINCTRL_PIN(66, "PWM4"),        PINCTRL_PIN(67, "PWM5"),        PINCTRL_PIN(68, "DVP_HSYNC"),
	PINCTRL_PIN(69, "SENSOR_CLK"),  PINCTRL_PIN(70, "DVP_D10"),     PINCTRL_PIN(71, "DVP_D9"),
	PINCTRL_PIN(72, "DVP_D8"),      PINCTRL_PIN(73, "DVP_D11"),
};

#endif
