#include <linux/init.h>
#include <linux/module.h>
#include <linux/io.h>
#include <linux/slab.h>
#include <linux/err.h>
#include <linux/list.h>
#include <linux/spinlock.h>

#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/of_address.h>

#include <linux/pinctrl/machine.h>
#include <linux/pinctrl/pinconf.h>
#include <linux/pinctrl/pinctrl.h>
#include <linux/pinctrl/pinmux.h>
#include <linux/pinctrl/consumer.h>
#include <linux/pinctrl/pinconf-generic.h>

#include "pinctrl-hc1703_1723_1753_1783s.h"

#define DEBUG

#ifdef DEBUG
#define DBG(fmt, args...)                                                             \
	do {                                                                          \
		printk("[PINCTRL_DRIVER] (%d, %s) " fmt, __LINE__, __func__, ##args); \
	} while (0)
#endif

/*****************
* PART 1: Pinmux *
*****************/

static int hc1703_1723_1753_1783s_pinctrl_get_groups_count(struct pinctrl_dev *pctldev)
{
	struct hc1703_1723_1753_1783s_pctl_drvdata *d = pinctrl_dev_get_drvdata(pctldev);
	return d->ngroups;
}

static const char *hc1703_1723_1753_1783s_pinctrl_get_group_name(struct pinctrl_dev *pctldev, unsigned selector)
{
	struct hc1703_1723_1753_1783s_pctl_drvdata *d = pinctrl_dev_get_drvdata(pctldev);
	return d->groups[selector].name;
}

static int hc1703_1723_1753_1783s_pinctrl_get_group_pins(struct pinctrl_dev *pctldev, unsigned selector,
                                                         const unsigned **pins, unsigned *num_pins)
{
	struct hc1703_1723_1753_1783s_pctl_drvdata *d = pinctrl_dev_get_drvdata(pctldev);

	*pins = d->groups[selector].pins;
	*num_pins = d->groups[selector].npins;

	return 0;
}

static int hc1703_1723_1753_1783s_pinctrl_dt_node_to_map(struct pinctrl_dev *pctldev, struct device_node *np_config,
                                                         struct pinctrl_map **map, u32 *num_maps)
{
	return pinconf_generic_dt_node_to_map(pctldev, np_config, map, num_maps, PIN_MAP_TYPE_CONFIGS_PIN);
}

static void hc1703_1723_1753_1783s_pinctrl_dt_free_map(struct pinctrl_dev *pctldev, struct pinctrl_map *map,
                                                       u32 num_maps)
{
	kfree(map);
}

static const struct pinctrl_ops hc1703_1723_1753_1783s_pinctrl_ops = {
	.get_groups_count = hc1703_1723_1753_1783s_pinctrl_get_groups_count,
	.get_group_name = hc1703_1723_1753_1783s_pinctrl_get_group_name,
	.get_group_pins = hc1703_1723_1753_1783s_pinctrl_get_group_pins,
	.dt_node_to_map = hc1703_1723_1753_1783s_pinctrl_dt_node_to_map,
	.dt_free_map = hc1703_1723_1753_1783s_pinctrl_dt_free_map,
};

static int hc1703_1723_1753_1783s_get_functions_count(struct pinctrl_dev *pctldev)
{
	struct hc1703_1723_1753_1783s_pctl_drvdata *d = pinctrl_dev_get_drvdata(pctldev);
	return d->nfuncs;
}

static const char *hc1703_1723_1753_1783s_get_fname(struct pinctrl_dev *pctldev, unsigned selector)
{
	struct hc1703_1723_1753_1783s_pctl_drvdata *d = pinctrl_dev_get_drvdata(pctldev);
	return d->functions[selector].name;
}

static int hc1703_1723_1753_1783s_get_groups(struct pinctrl_dev *pctldev, unsigned selector, const char *const **groups,
                                             unsigned *const num_groups)
{
	struct hc1703_1723_1753_1783s_pctl_drvdata *d = pinctrl_dev_get_drvdata(pctldev);

	*groups = d->functions[selector].groups;
	*num_groups = d->functions[selector].ngroups;
	return 0;
}

static int hc1703_1723_1753_1783s_set_mux(struct pinctrl_dev *pctldev, unsigned fn_sel, unsigned grp_sel)
{
	struct hc1703_1723_1753_1783s_pctl_drvdata *d = pinctrl_dev_get_drvdata(pctldev);
	struct hc1703_1723_1753_1783s_pin_group *g = &d->groups[grp_sel];
	void __iomem *reg_byte;
	uint32_t muxsel;
	int i;

	for (i = 0; i < g->npins; i++) {
		/* If no muxsel, skip the operation */
		if (g->muxsel[i] != -1) {
			reg_byte = d->base + hc1703_1723_1753_1783s_ioc_addr[g->pins[i]].mux_bias;
			muxsel = g->muxsel[i];
			writel(muxsel, reg_byte);
		}
	}

	return 0;
}

static const struct pinmux_ops hc1703_1723_1753_1783s_pinmux_ops = {
	.get_functions_count = hc1703_1723_1753_1783s_get_functions_count,
	.get_function_name = hc1703_1723_1753_1783s_get_fname,
	.get_function_groups = hc1703_1723_1753_1783s_get_groups,
	.set_mux = hc1703_1723_1753_1783s_set_mux,
};

/******************
* PART 2: Pinconf *
******************/

static int hc1703_1723_1753_1783s_pinctrl_set_conf_sel(struct pinctrl_dev *pctldev, u32 pin, u32 sel, u32 arg)
{
	struct hc1703_1723_1753_1783s_pctl_drvdata *d = pinctrl_dev_get_drvdata(pctldev);
	void __iomem *reg_byte;
	u32 reg_val;
	int bit_bias;
	unsigned long flags;

	reg_byte = d->base + hc1703_1723_1753_1783s_ioc_addr[pin].conf_bias;
	spin_lock_irqsave(&d->lock, flags);
	reg_val = readl_relaxed(reg_byte);

	switch (sel) {
	case PINCFG_PU:
		reg_val &= ~(3);
		reg_val |= 1;
		break;
	case PINCFG_PD:
		reg_val &= ~(3);
		reg_val |= 2;
		break;
	case PINCFG_DS:
		reg_val &= ~(hc1703_1723_1753_1783s_ioc_addr[pin].ds_bit << 16);
		reg_val |= (arg << 16);
		break;
	case PINCFG_ST:
		bit_bias = 16 + hc1703_1723_1753_1783s_ioc_addr[pin].ds_bit;
		reg_val &= ~(1 << bit_bias);
		if (arg) {
			reg_val |= (arg << bit_bias);
		}
		break;
	case PINCFG_SR:
		bit_bias = 17 + hc1703_1723_1753_1783s_ioc_addr[pin].ds_bit;
		reg_val &= ~(1 << bit_bias);
		if (arg) {
			reg_val |= (arg << bit_bias);
		}
		break;
	case PINCFG_PS:
		reg_val &= ~(1 << 16);
		if (arg) {
			reg_val |= (arg << 16);
		}
		break;
	default:
		spin_unlock_irqrestore(&d->lock, flags);
		return -EINVAL;
		break;
	}

	writel(reg_val, reg_byte);
	spin_unlock_irqrestore(&d->lock, flags);

	return 0;
}

static int hc1703_1723_1753_1783s_pinconf_set(struct pinctrl_dev *pctldev, unsigned pin, unsigned long *configs,
                                              unsigned num_configs)
{
	u16 param;
	u32 arg;
	int idx, err;

	for (idx = 0; idx < num_configs; idx++) {
		param = pinconf_to_config_param(configs[idx]);
		arg = pinconf_to_config_argument(configs[idx]);
		//DBG("HC1703_1723_1753_1783S PIN#%d [%s] CONFIG PARAM:%d ARG:%d\n", pin, hc1703_1723_1753_1783s_pins[pin].name, param, arg);

		switch (param) {
		case PIN_CONFIG_BIAS_PULL_UP:
			err = hc1703_1723_1753_1783s_pinctrl_set_conf_sel(pctldev, pin, PINCFG_PU, arg);
			if (err)
				return err;
			break;

		case PIN_CONFIG_BIAS_PULL_DOWN:
			err = hc1703_1723_1753_1783s_pinctrl_set_conf_sel(pctldev, pin, PINCFG_PD, arg);
			if (err)
				return err;
			break;
		case PIN_CONFIG_DRIVE_STRENGTH:
			err = hc1703_1723_1753_1783s_pinctrl_set_conf_sel(pctldev, pin, PINCFG_DS, arg);
			if (err)
				return err;
			break;
		case PIN_CONFIG_INPUT_SCHMITT_ENABLE:
			err = hc1703_1723_1753_1783s_pinctrl_set_conf_sel(pctldev, pin, PINCFG_ST, arg);
			if (err)
				return err;
			break;

		case PIN_CONFIG_SLEW_RATE:
			err = hc1703_1723_1753_1783s_pinctrl_set_conf_sel(pctldev, pin, PINCFG_SR, arg);
			if (err)
				return err;
			break;
		case PIN_CONFIG_POWER_SOURCE:
			err = hc1703_1723_1753_1783s_pinctrl_set_conf_sel(pctldev, pin, PINCFG_PS, arg);
			if (err)
				return err;
			break;
		default:
			return -ENOTSUPP;
		}
	}

	return 0;
}

static int hc1703_1723_1753_1783s_pinconf_group_set(struct pinctrl_dev *pctldev, unsigned group, unsigned long *configs,
                                                    unsigned num_configs)
{
	struct hc1703_1723_1753_1783s_pctl_drvdata *d = pinctrl_dev_get_drvdata(pctldev);
	const unsigned int *pins;
	unsigned int i;

	pins = d->groups[group].pins;

	for (i = 0; i < d->groups[group].npins; i++)
		hc1703_1723_1753_1783s_pinconf_set(pctldev, pins[i], configs, num_configs);

	return 0;
}

static const struct pinconf_ops hc1703_1723_1753_1783s_pinconf_ops = {
	.pin_config_set = hc1703_1723_1753_1783s_pinconf_set,
	.pin_config_group_set = hc1703_1723_1753_1783s_pinconf_group_set,
	.is_generic = true,
};

/**************************************
* PART 5: Pinctrl driver registration *
**************************************/

/* pinctrl descriptor; to be registered to pinctrl subsystem */
struct pinctrl_desc hc1703_1723_1753_1783s_pinctrl_desc = {
	.name = DRIVER_NAME,
	.pins = hc1703_1723_1753_1783s_pins,
	.npins = ARRAY_SIZE(hc1703_1723_1753_1783s_pins),
	.owner = THIS_MODULE,
	.pctlops = &hc1703_1723_1753_1783s_pinctrl_ops,
	.pmxops = &hc1703_1723_1753_1783s_pinmux_ops,
	.confops = &hc1703_1723_1753_1783s_pinconf_ops,
};

static int hc1703_1723_1753_1783s_pinctrl_parse_function(struct hc1703_1723_1753_1783s_pctl_drvdata *d,
                                                         struct device_node *np, int idxf, int idxg)
{
	struct hc1703_1723_1753_1783s_pmx_func *f = &d->functions[idxf];
	struct device *dev = d->dev;
	struct device_node *child;
	const char *propname_pins = "augentix,pins";
	const char *propname_pmx = "augentix,pmx";
	char **group;
	int length, ret, n = 0;
	uint32_t i, **buf, **mux_buf;

	if (f == NULL) {
		dev_err(dev, "Failed to find pin function!\n");
		return -EINVAL;
	}

	dev_dbg(dev, "Parsing function: %s...\n", np->name);

	group = devm_kzalloc(dev, f->ngroups * sizeof(*group), GFP_KERNEL);
	if (!group)
		return -ENOMEM;
	buf = devm_kzalloc(dev, f->ngroups * sizeof(*buf), GFP_KERNEL);
	if (!buf)
		return -ENOMEM;
	mux_buf = devm_kzalloc(dev, f->ngroups * sizeof(*mux_buf), GFP_KERNEL);
	if (!mux_buf)
		return -ENOMEM;

	child = of_get_next_child(np, NULL);
	if (!child) {
		dev_err(dev, "no group is defined\n");
		return -ENOENT;
	}

	for_each_child_of_node (np, child) {
		group[n] = devm_kzalloc(dev, GROUP_NAME_LEN, GFP_KERNEL);
		if (!group[n])
			return -ENOMEM;

		memcpy(group[n], child->name, GROUP_NAME_LEN);

		/* assign group name */
		d->groups[idxg].name = group[n];
		f->groups[n] = group[n];

		/* assign pin index and pin mux */
		length = of_property_count_u32_elems(child, propname_pins);
		d->groups[idxg].npins = length;

		d->groups[idxg].pins =
		        devm_kzalloc(dev, d->groups[idxg].npins * sizeof(*d->groups[idxg].pins), GFP_KERNEL);
		if (!d->groups[idxg].pins)
			return -ENOMEM;

		d->groups[idxg].muxsel =
		        devm_kzalloc(dev, d->groups[idxg].npins * sizeof(*d->groups[idxg].muxsel), GFP_KERNEL);
		if (!d->groups[idxg].muxsel)
			return -ENOMEM;

		buf[n] = devm_kzalloc(dev, d->groups[idxg].npins * sizeof(**buf), GFP_KERNEL);
		if (!buf[n])
			return -ENOMEM;
		mux_buf[n] = devm_kzalloc(dev, d->groups[idxg].npins * sizeof(**mux_buf), GFP_KERNEL);
		if (!mux_buf[n])
			return -ENOMEM;

		of_property_read_u32_array(child, propname_pins, buf[n], d->groups[idxg].npins);
		ret = of_property_read_bool(child, propname_pmx);
		if (ret) {
			of_property_read_u32_array(child, propname_pmx, mux_buf[n], d->groups[idxg].npins);
		}

		for (i = 0; i < d->groups[idxg].npins; i++) {
			d->groups[idxg].pins[i] = buf[n][i];
			if (ret) {
				d->groups[idxg].muxsel[i] = mux_buf[n][i];
			} else {
				d->groups[idxg].muxsel[i] = -1;
			}
		}

		idxg++;
		n++;
	};
	return 0;
}

static int hc1703_1723_1753_1783s_pinctrl_ioc_register(struct hc1703_1723_1753_1783s_pctl_drvdata *d)
{
	uint32_t n, npins, muxsel, pud, pcfg;
	void __iomem *reg_byte;

	npins = ARRAY_SIZE(hc1703_1723_1753_1783s_ioc_table);
	/* PIOC pin range: pin0~pin67, AIOC pin range: pin68~pin80*/
	for (n = 0; n < npins; n++) {
		reg_byte = d->base + hc1703_1723_1753_1783s_ioc_addr[n].mux_bias;
		muxsel = hc1703_1723_1753_1783s_ioc_table[n].mux;
		writel(muxsel, reg_byte);

		reg_byte = d->base + hc1703_1723_1753_1783s_ioc_addr[n].conf_bias;
		pud = hc1703_1723_1753_1783s_ioc_table[n].pud;
		pcfg = hc1703_1723_1753_1783s_ioc_table[n].pcfg;
		writel((pud | (pcfg << 16)), reg_byte);
	}

	return 0;
}

static int hc1703_1723_1753_1783s_pinctrl_probe_dt(struct platform_device *pdev,
                                                   struct hc1703_1723_1753_1783s_pctl_drvdata *d)
{
	struct device_node *np = pdev->dev.of_node;
	struct device_node *child;
	struct device_node *g_child;
	struct device *dev = &pdev->dev;
	struct hc1703_1723_1753_1783s_pmx_func *f;
	const char *fn, *fnull = "";
	int idxf, idxg;
	int ret;
	uint32_t val;

	child = of_get_next_child(np, NULL);
	if (!child) {
		dev_err(dev, "no group is defined\n");
		return -ENOENT;
	}

	/* Count total functions and groups */
	fn = fnull;
	for_each_child_of_node (np, child) {
		/* Increase nfuncs by 1 if the name of the child node is
		 * different from the previous one, or it is the first child node */
		if (strcmp(fn, child->name)) {
			fn = child->name;
			d->nfuncs++;
		}

		g_child = of_get_next_child(child, NULL);
		for_each_child_of_node (child, g_child) {
			d->ngroups++;
		};
	}

	/* Allocate memory for group and functions */
	d->functions = devm_kzalloc(dev, d->nfuncs * sizeof(*d->functions), GFP_KERNEL);
	if (!d->functions)
		return -ENOMEM;

	d->groups = devm_kzalloc(dev, d->ngroups * sizeof(*d->groups), GFP_KERNEL);
	if (!d->groups)
		return -ENOMEM;

	/* Iterate each note to find matched groups for each function. Groups of the same function are to be adjacent */
	fn = fnull;
	idxf = 0;
	f = &d->functions[0];
	for_each_child_of_node (np, child) {
		if (strcmp(fn, child->name)) {
			f = &d->functions[idxf++];
			f->name = fn = child->name;
		}
		g_child = of_get_next_child(child, NULL);
		for_each_child_of_node (child, g_child) {
			f->ngroups++;
		};
	};

	/* Get groups for each function */
	idxf = 0;
	idxg = 0;
	fn = fnull;
	for_each_child_of_node (np, child) {
		if (strcmp(fn, child->name)) {
			f = &d->functions[idxf];
			f->groups = devm_kzalloc(&pdev->dev, f->ngroups * sizeof(*f->groups), GFP_KERNEL);
			if (!f->groups)
				return -ENOMEM;
			fn = child->name;
		}

		ret = hc1703_1723_1753_1783s_pinctrl_parse_function(d, child, idxf, idxg);
		if (ret) {
			DBG("hc1703_1723_1753_1783s_pinctrl_parse_function failed\n");
			return ret;
		}

		idxf++;
		idxg += f->ngroups;
	}

	ret = hc1703_1723_1753_1783s_pinctrl_ioc_register(d);
	if (ret) {
		DBG("hc1703_1723_1753_1783s_pinctrl_ioc_register failed\n");
		return ret;
	}

	ret = of_property_read_u32(np, "xin-pcfg", &val);
	if (ret) {
		pr_warn("xin-pcfg not defined!\n");
		val = 0;
	}
	writel(val, d->base + HC1703_1723_1753_1783S_XIN_PCFG_OFFSET);

	ret = of_property_read_u32(np, "resetb-pcfg", &val);
	if (ret) {
		pr_warn("resetb-pcfg not defined!\n");
		val = 0;
	}
	writel(val, d->base + HC1703_1723_1753_1783S_RESETB_OFFSET);

	return 0;
}

static int hc1703_1723_1753_1783s_pinctrl_probe(struct platform_device *pdev)
{
	struct hc1703_1723_1753_1783s_pctl_drvdata *hpctl;
	struct resource *res;
	struct device *dev = &pdev->dev;
	int ret;

	dev_info(&pdev->dev, "Initializing HC1703_1723_1753_1783S pin control driver...\n");

	/* Create state holders etc for this driver */
	hpctl = devm_kzalloc(dev, sizeof(*hpctl), GFP_KERNEL);
	if (!hpctl)
		return -ENOMEM;
	hpctl->dev = &pdev->dev;
	spin_lock_init(&hpctl->lock);

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);

	hpctl->base = devm_ioremap_resource(&pdev->dev, res);
	if (IS_ERR(hpctl->base)) {
		dev_err(dev, "Failed to map I/O address!\n");
	}

	ret = hc1703_1723_1753_1783s_pinctrl_probe_dt(pdev, hpctl);
	if (ret) {
		dev_err(dev, "Device tree probe failed: %d\n", ret);
		return -EINVAL;
	}
	hpctl->pctldesc = &hc1703_1723_1753_1783s_pinctrl_desc;

	hpctl->pctldev = pinctrl_register(&hc1703_1723_1753_1783s_pinctrl_desc, dev, hpctl);
	if (IS_ERR(hpctl->pctldev)) {
		dev_err(dev, "Failed to register hc1703_1723_1753_1783s pin control device\n");
		return PTR_ERR(hpctl->pctldev);
	}

	platform_set_drvdata(pdev, hpctl);

	dev_info(dev, "hc1703_1723_1753_1783s pin control driver initialized\n");
	return 0;
}

static struct of_device_id hc1703_1723_1753_1783s_pinctrl_of_match[] = {
	{ .compatible = "augentix,pinctrl" },
	{},
};

static struct platform_driver hc1703_1723_1753_1783s_pinctrl_driver =
{
	.driver = {
		.name = DRIVER_NAME,
		.owner = THIS_MODULE,
		.of_match_table = hc1703_1723_1753_1783s_pinctrl_of_match,
	},
	.probe = hc1703_1723_1753_1783s_pinctrl_probe,
};

static int __init hc1703_1723_1753_1783s_pinctrl_init(void)
{
	return platform_driver_register(&hc1703_1723_1753_1783s_pinctrl_driver);
}
arch_initcall(hc1703_1723_1753_1783s_pinctrl_init);

MODULE_AUTHOR("Louis Yang, Augentix <louis.yang@augentix.com>");
MODULE_DESCRIPTION("Augentix HC1703_1723_1753_1783S pin control driver");
MODULE_LICENSE("GPL v2");
