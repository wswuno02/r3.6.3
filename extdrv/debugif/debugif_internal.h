/*
 * AUGENTIX INC. - OPEN-SOURCE UNDER GPL LICENSE
 *
 * debugif_internal.h - Tokyo debugging internal interface
 * Copyright 2018 ShihChieh Lin, Augentix Inc. <shihchieh.lin@augentix.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation, incorporated herein by reference.
 *
 * * Brief: Tokyo debugging interface
 * *
 * * Author: ShihChieh Lin
 */
#ifndef DEBUGIF_INTERNAL_H
#define DEBUGIF_INTERNAL_H

#include <linux/io.h>

#endif /* DEBUGIF_INTERNAL_H */
