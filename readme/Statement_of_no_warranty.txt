無擔保聲明
本資訊按“原樣”提供，沒有任何明示或暗示的擔保，包括但不限於完整性、準確性、及時性、適銷性、適用於特定目的或不侵權的保證。在任何情況下，作者或持有人均不承擔任何索賠、損害賠償或其他責任，無論是與本資訊有關，還是與本資訊的使用有關。

Statement of no warranty
The Information is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of completeness, accuracy, timeliness, merchantability, fitness for a particular purpose or non-infringement. In no event shall the authors or holders be liable for any claim, damages or other liability, whether in connection with the Information or the use of the Information.
