/*
 * AUGENTIX INC. - PROPRIETARY
 *
 * mpi_iva.h - MPI for IVA
 * Copyright (C) 2017-2019 Augentix Inc. - All Rights Reserved
 *
 * NOTICE: The information contained herein is the property of Augentix Inc.
 * Copying and distributing of this file, via any medium,
 * must be licensed by Augentix Inc.
 *
 * * Brief: MPI for IVA
 * *
 * * Author: NAME <MAIL@augentix.com>
 */

/**
 * @file mpi_iva.h
 * @brief MPI for IVA OD (object detection)
 */

#ifndef MPI_IVA_H_
#define MPI_IVA_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <sys/time.h>
#include "mpi_index.h"

#define MPI_IVA_MAX_OBJ_NUM 10 /**< Max object number. */

#define MPI_IVA_OD_MAX_QUA 63 /**< Max OD quality index. */
#define MPI_IVA_OD_MIN_QUA 0 /**< Min OD quality index. */
#define MPI_IVA_OD_MAX_TRACK_REFINE 63 /**< Max OD track refinement index. */
#define MPI_IVA_OD_MIN_TRACK_REFINE 0 /**< Min OD track refinement index. */
#define MPI_IVA_OD_MAX_OBJ_SIZE 255 /**< Max OD object size(percentage in a frame size). */
#define MPI_IVA_OD_MIN_OBJ_SIZE 0 /**< Min OD object size(percentage in a frame size). */
#define MPI_IVA_OD_MAX_SEN 255 /**< Max OD sensitivity. */
#define MPI_IVA_OD_MIN_SEN 0 /**< Min OD sensitivity. */
#define MPI_IVA_OD_MAX_LIFE 160 /**< Max OD life of object. */
#define MPI_IVA_OD_ENABLE_STOP_DET 1 /**< Enable OD stop detect of object. */
#define MPI_IVA_OD_DISABLE_STOP_DET 0 /**< Disable OD stop detect of object. */

/**
 * @struct MPI_IVA_OD_PARAM_S
 * @brief Structure for the parameters of object detection.
 * @details
 * @see MPI_IVA_setObjParam()
 * @see MPI_IVA_getObjParam()
 * @property UINT8 MPI_IVA_OD_PARAM_S::od_qual
 * @arg Range [::MPI_IVA_OD_MIN_QUA, ::MPI_IVA_OD_MAX_QUA]
 * @property UINT8 MPI_IVA_OD_PARAM_S::od_track_refine
 * @arg Range [::MPI_IVA_OD_MIN_TRACK_REFINE, ::MPI_IVA_OD_MAX_TRACK_REFINE]
 * @property UINT8 MPI_IVA_OD_PARAM_S::od_size_th
 * @arg Range [::MPI_IVA_OD_MIN_OBJ_SIZE, ::MPI_IVA_OD_MAX_OBJ_SIZE]
 * @property UINT8 MPI_IVA_OD_PARAM_S::od_sen
 * @arg Range [::MPI_IVA_OD_MIN_SEN, ::MPI_IVA_OD_MAX_SEN]
 * @property UINT8 MPI_IVA_OD_PARAM_S::en_stop_det
 * @arg Disable: ::MPI_IVA_OD_DISABLE_STOP_DET
 * @arg Enable : ::MPI_IVA_OD_ENABLE_STOP_DET
 */
typedef struct mpi_iva_od_param {
	UINT8 od_qual; /**< Quality index of OD performance. */
	UINT8 od_track_refine; /**< Tracking refinement. */
	UINT8 od_size_th; /**< Object size threshold. */
	UINT8 od_sen; /**< Object sensitivity. */
	UINT8 en_stop_det; /**< Enable stop object detection */
} MPI_IVA_OD_PARAM_S;

/**
 * @struct MPI_IVA_OBJ_ATTR_S
 * @brief Struct for object attributes.
 * @details
 * @see MPI_IVA_OBJ_LIST_S
 * @property UINT8 MPI_IVA_OBJ_ATTR_S::id
 * @arg Range [0, INT_MAX)
 * @property UINT8 MPI_IVA_OBJ_ATTR_S::life
 * @arg Range [0, ::MPI_IVA_OD_MAX_LIFE]
 */
typedef struct mpi_iva_obj_attr {
	INT32 id; /**< ID of object. */
	INT16 life; /**< Life of object. */
	MPI_RECT_POINT_S rect; /**< Rectangle point of object. */
	MPI_MOTION_VEC_S mv; /**< Motion vector of object. */
} MPI_IVA_OBJ_ATTR_S;

/**
 * @struct MPI_IVA_OBJ_LIST_S
 * @brief Struct for object list.
 * @details
 * @property UINT8 MPI_IVA_OBJ_LIST_S::obj_num
 * @arg Range [0, ::MPI_IVA_MAX_OBJ_NUM]
 */
typedef struct mpi_iva_obj_list {
	UINT32 timestamp; /**< Time stamp. */
	INT32 obj_num; /**< Number of objects. */
	MPI_IVA_OBJ_ATTR_S obj[MPI_IVA_MAX_OBJ_NUM]; /**< Attributes list of the objects. */
} MPI_IVA_OBJ_LIST_S;

/* Interface function prototype */
INT32 MPI_IVA_enableObjDet(MPI_WIN idx);
INT32 MPI_IVA_disableObjDet(MPI_WIN idx);
INT32 MPI_IVA_setObjParam(MPI_WIN idx, const MPI_IVA_OD_PARAM_S *param);
INT32 MPI_IVA_getObjParam(MPI_WIN idx, MPI_IVA_OD_PARAM_S *param);
INT32 MPI_IVA_getObjList(MPI_WIN idx, MPI_IVA_OBJ_LIST_S *list);
INT32 MPI_IVA_getBitStreamObjList(MPI_WIN idx, UINT32 timestamp, MPI_IVA_OBJ_LIST_S *list);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* MPI_IVA_H_ */
