/*
 * AUGENTIX INC. - PROPRIETARY
 *
 * mpi_dev.h - MPI for video device
 * Copyright (C) 2017-2019 Augentix Inc. - All Rights Reserved
 *
 * NOTICE: The information contained herein is the property of Augentix Inc.
 * Copying and distributing of this file, via any medium,
 * must be licensed by Augentix Inc.
 *
 * * Brief: MPI for video device
 * *
 * * Author: NAME <MAIL@augentix.com>
 */

/**
 * @file mpi_dev.h
 * @brief MPI for video device
 */

#ifndef MPI_DEV_H_
#define MPI_DEV_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "mpi_index.h"

#define MPI_VENC_ROI_NUM (8) /**< Number of video encode ROI. */
#define MPI_SPH_VIDEO_SENSOR_NUM (2) /**< Number of spherical video sensor. */
#define MPI_STITCH_SENSOR_NUM (2) /**< Number of STITCH sensor. */
#define MPI_STITCH_TABLE_NUM (3) /**< Number of STITCH table. */
#define MPI_CENTER_OFFSET_MIN (-8191) /**< Center offset minimun */
#define MPI_CENTER_OFFSET_MAX (8192) /**< Center offset maximum */
#define MPI_LDC_RATIO_MIN (0) /**< LDC minimal ratio. */
#define MPI_LDC_RATIO_MAX (32767) /**< LDC maximal ratio. */
#define MPI_CIRCLE_RADIUS_MIN (0) /**< Circle radius minimum */
#define MPI_CIRCLE_RADIUS_MAX (65535) /**< Circle radius maximum */
#define MPI_PANORAMA_STRAIGHTEN_MIN (0) /**< Panorama straighten minimum */
#define MPI_PANORAMA_STRAIGHTEN_MAX (4095) /**< Panorama straighten maximum */

#define MPI_MAX_ISP_MV_HIST_BIN_NUM (32) /**< Maximum number of MV histogram bins */
#define MPI_MAX_ISP_MV_HIST_CFG_NUM (1) /**< Maximum number of MV histogram configurations */
#define MPI_MAX_ISP_VAR_CFG_NUM (1) /**< Maximum number of variance configurations */
#define MPI_MAX_ISP_Y_AVG_CFG_NUM (5) /**< Maximum number of Y average configurations */
#define MPI_MIN_ROI_VAL (0) /**< ROI value minimum */
#define MPI_MAX_ROI_VAL (1024) /**< ROI value maximum */

#define MPI_CHN_RES_ALIGN (8) /**< Alignment of channel resolution. */
#define MPI_CHN_LAYOUT_HOR_ALIGN (16) /**< Horizontal alignment of channel layout. */
#define MPI_CHN_LAYOUT_VER_ALIGN (32) /**< Vertical alignment of channel layout. */

/**
 * @brief marco to transform path index to bitmap.
 * @param[in] i sensor path index.
 */
#define MPI_SENSOR_BMP(i) (0x1 << (i))

/**
 * @brief Enumeration of HDR mode.
 */
typedef enum mpi_hdr_mode {
	MPI_HDR_MODE_NONE = 0, /**< NO HDR mode. */
	MPI_HDR_MODE_FRAME_PARL, /**< Frame parallel HDR mode. */
	MPI_HDR_MODE_FRAME_ITLV, /**< Frame interleave HDR mode. */
	MPI_HDR_MODE_TOP_N_BTM, /**< TOP and bottom HDR mode. */
	MPI_HDR_MODE_SIDE_BY_SIDE, /**< Side by side HDR mode. */
	MPI_HDR_MODE_LINE_COLOC, /**< Line colocated HDR mode. */
	MPI_HDR_MODE_LINE_ITLV, /**< Line interleave HDR mode. */
	MPI_HDR_MODE_PIX_COLOC, /**< Pixel colocated HDR mode. */
	MPI_HDR_MODE_PIX_ITLV, /**< Pixel interleave HDR mode. */
	MPI_HDR_MODE_FRAME_COMB, /**< Frame combined HDR mode. */
	MPI_HDR_MODE_FRAME_ITLV_ASYNC, /**< Frame interleave HDR mode with async method. */
	MPI_HDR_MODE_FRAME_ITLV_SYNC, /**< Frame interleave HDR mode with sync method. */
	MPI_HDR_MODE_NUM,
} MPI_HDR_MODE_E;

/**
 * @brief Enumeration of LDC mode.
 */
typedef enum mpi_ldc_view_type {
	MPI_LDC_VIEW_TYPE_CROP =
	        0, /**< The corrected picture is cropped and the size of the cropped region is equal to that of the source picture. */
	MPI_LDC_VIEW_TYPE_ALL, /**< The maximum rectangle is obtained based on the edges of the corrected picture. */
	MPI_LDC_VIEW_TYPE_NUM,
} MPI_LDC_VIEW_TYPE_E;

/**
 * @brief Struct for input path attributes.
 */
typedef struct mpi_path_attr {
	UINT32 sensor_idx; /**< Sensor index. */
	MPI_SIZE_S res; /**< Input resolution (pixel) of the sensor path. */
} MPI_PATH_ATTR_S;

/**
 * @brief Struct for the attribute of video device.
 * @note
 * @arg Device attibute cannot be modified when device is running.
 * @see MPI_DEV_startDev()
 */
typedef struct mpi_dev_attr {
	UINT8 stitch_en; /**< Enable stitching. 0: disable, 1: enable */
	UINT8 eis_en; /**< Enable EIS. 0: disable, 1: enable */
	MPI_HDR_MODE_E hdr_mode; /**< HDR mode. */
	FLOAT fps; /**< Frame rate per second. Should be a positive number. */
	MPI_BAYER_E bayer; /**< Sensor bayer phase. */
	MPI_PATH_BMP_U path; /**< Input path enable bitmap. */
} MPI_DEV_ATTR_S;

/**
 * @struct MPI_CHN_ATTR_S
 * @brief Struct for video channel attributes.
 * @note
 * @arg Output resolution cannot be modified when channel is running.
 * @arg If `MPI_CHN_ATTR_S::fps` is smaller than `MPI_DEV_ATTR_S::fps`,
 *      frames are dropped based on the output to input ratio. 
 * @arg For instance, if `MPI_DEV_ATTR_S::fps` is 30 and `MPI_CHN_ATTR_S::fps`
 *      is 25, then video channel will drop one frame in every six frames.
 * @see MPI_DEV_addChn()
 * @see MPI_DEV_getChnAttr()
 * @see MPI_DEV_setChnAttr()
 * @property MPI_SIZE_S MPI_CHN_ATTR_S::res
 * @note
 * @arg `res.width` and `res.height` must be divisible by 8.
 */
typedef struct mpi_chn_attr {
	MPI_SIZE_S res; /**< Video channel output resolution (pixel). */
	FLOAT fps; /**< Video channel output frame rate. Should be a positive number. */
} MPI_CHN_ATTR_S;

/**
 * @struct MPI_CHN_STAT_S
 * @brief Struct for channel state.
 * @property UINT32 MPI_CHN_STAT_S::status
 * @arg ::MPI_STATE_NONE: channel does not exists.
 * @arg ::MPI_STATE_STOP: channel is not operating.
 * @arg ::MPI_STATE_RUN: channel is running.
 * @arg ::MPI_STATE_SUSPEND channel is suspended.
 */
typedef struct mpi_chn_stat {
	UINT32 status; /**< Channel status. */
} MPI_CHN_STAT_S;

/**
 * @cond code fragment skipped by Doxygen.
 */

/**
 * @brief Struct for crop information.
 */
typedef struct mpi_crop_info {
	UINT8 en; /**< Crop enable. 0: disable, 1: enable */
	MPI_RECT_S rect; /**< Parameters of cropped image. */
} MPI_CROP_INFO_S;

/**
 * @brief Struct for ROI information.
 */
typedef struct mpi_roi_info {
	UINT32 idx; /**< ROI index. */
	MPI_RECT_S rect; /**< Parameters of ROI region. */
} MPI_ROI_INFO_S;

/**
 * @brief Struct for image mirror and flip.
 */
typedef struct mpi_mirror_flip {
	UINT8 mirr_en; /**< Enable image mirror. */
	UINT8 flip_en; /**< Enable image flipping. */
} MPI_MIRROR_FLIP_S;

/**
 * @brief Struct for the attribute of spherical video.
 */
typedef struct mpi_sph_video_attr {
	UINT8 sph_video_en; /**< Enable bit. */
	INT32 x_cent_offs[MPI_SPH_VIDEO_SENSOR_NUM]; /**< X coordinate of center of circle. */
	INT32 y_cent_offs[MPI_SPH_VIDEO_SENSOR_NUM]; /**< Y coordinate of center of circle. */
	INT32 radius[MPI_SPH_VIDEO_SENSOR_NUM]; /**< Radius of circle. */
	INT32 h_scaling_str[MPI_SPH_VIDEO_SENSOR_NUM]; /**< Horizontal scaling strength. */
	INT32 v_scaling_str[MPI_SPH_VIDEO_SENSOR_NUM]; /**< Vertical scaling strength. */
	INT32 h_shearing[MPI_SPH_VIDEO_SENSOR_NUM]; /**< Horizontal shearing. */
	INT32 v_shearing[MPI_SPH_VIDEO_SENSOR_NUM]; /**< Vertical shearing. */
} MPI_SPH_VIDEO_ATTR_S;

/**
 * @endcond
 */

/**
 * @struct MPI_LDC_ATTR_S
 * @brief Struct for the attribute of LDC(Lens Distortion Correction).
 * @note
 * @arg LDC attribute can be modified when device is running.
 * @arg the parameters is valid when `view_type` is set LDC view
 * @arg The unit of `center_offset` is 1/4 pixel.
 * @see ::MPI_WIN_ATTR_S::view_type
 * @property MPI_SHIFT_S MPI_LDC_ATTR_S::center_offset
 * @arg Range [::MPI_CENTER_OFFSET_MIN, ::MPI_CENTER_OFFSET_MAX]. Default 0.
 */
typedef struct mpi_ldc_attr {
	UINT8 enable; /**< Enable LDC, 0: disable, 1: enable. */
	MPI_LDC_VIEW_TYPE_E
	view_type; /**< LDC mode, only ::MPI_LDC_VIEW_TYPE_CROP is available. See online document for more detail. */
	MPI_SHIFT_S
	center_offset; /**< Horizontal and vertical offset of the distortion center relative to the picture center.
	                * Range [::MPI_CENTER_OFFSET_MIN, ::MPI_CENTER_OFFSET_MAX]. Default 0. */
	INT16 ratio; /**< Distortion ratio. Range [::MPI_LDC_RATIO_MIN, ::MPI_LDC_RATIO_MAX]. Default 0. */
} MPI_LDC_ATTR_S;

/**
 * @struct MPI_PANORAMA_ATTR_S
 * @brief Struct for the attribute of fisheye panorama.
 * @note
 * @arg Panorama attribute can be modified when device is running.
 * @arg The parameters is valid when `view_type` is set Panorama view
 * @arg The unit of `center_offset`, `radius`, `curvature` is 1/4 pixel.
 * @see ::MPI_WIN_ATTR_S::view_type
 */
typedef struct mpi_panorama_attr {
	UINT8 enable; /**< Enable bit. 0: disable, 1: enable */
	MPI_SHIFT_S
	center_offset; /**< Horizontal and vertical offset of the distortion center relative to the picture center.
	                * Range [::MPI_CENTER_OFFSET_MIN, ::MPI_CENTER_OFFSET_MAX]. Default 0. */
	UINT16 radius; /**< Horizontal FOV in image circle. Range [::MPI_CIRCLE_RADIUS_MIN, ::MPI_CIRCLE_RADIUS_MAX]. */
	UINT16 curvature; /**< Virtical FOV in image circle. Range [::MPI_CIRCLE_RADIUS_MIN, ::MPI_CIRCLE_RADIUS_MAX]. */
	UINT16 ldc_ratio; /**< Distortion ratio. Range [::MPI_LDC_RATIO_MIN, ::MPI_LDC_RATIO_MAX]. Default 0. */
	UINT16 straighten; /**< Strength for straighten line. Range [0, 4095]. */
} MPI_PANORAMA_ATTR_S;

/**
 * @struct MPI_PANNING_ATTR_S
 * @brief Struct for the attribute of fisheye panning.
 * @note
 * @arg Panning attribute can be modified when device is running.
 * @arg the parameters is valid when MPI_WIN_ATTR_S::view_type is set Panning view
 * @arg The unit of `center_offset` and `raduis` is 1/4 pixel.
 * @arg It is suggested to be cropped 5% outer area by specifying ROI region in ::MPI_WIN_ATTR_S.
 */
typedef struct mpi_panning_attr {
	UINT8 enable; /**< Enable bit. 0: disable, 1: enable */
	MPI_SHIFT_S
	center_offset; /**< Horizontal and vertical offset of the distortion center relative to the picture center.
	                * Range [::MPI_CENTER_OFFSET_MIN, ::MPI_CENTER_OFFSET_MAX]. Default 0. */
	UINT16 radius; /**< Radius of image circle. Range [::MPI_CIRCLE_RADIUS_MIN, ::MPI_CIRCLE_RADIUS_MAX]. */
	UINT16 hor_strength; /**< Horizontal dewarping strength. Suggested 27. */
	UINT16 ver_strength; /**< Vertical dewarping strength. Suggested 27. */
	UINT16 ldc_ratio; /**< Distortion ratio. Range [::MPI_LDC_RATIO_MIN, ::MPI_LDC_RATIO_MAX]. Default 0. */
} MPI_PANNING_ATTR_S;

/**
 * @struct MPI_SURROUND_ATTR_S
 * @brief Struct for the attribute of fisheye surround.
 * @arg the parameters is valid when ::MPI_WIN_ATTR_S::view_type is set as surround view
 * @arg The unit of `center_offset`, `min_raduis`, and `max_radius` is 1/4 pixel.
 * @arg Note that for buttom-up application such as conference camera. You need to flip the view.
 * @arg `max_radius` should be larger than `min_radius`
 */
typedef struct mpi_surround_attr {
	UINT8 enable; /**< Enable bit. 0: disable, 1: enable */
	MPI_SHIFT_S center_offset; /**< Coordinate shift of circle center. */
	MPI_ROTATE_TYPE_E
	rotate; /**< Rotation of image circle. Decide which direction you want to put in the center of the dewarped image */
	UINT16 min_radius; /**< Inner radius of dewarping area. Range [::MPI_CIRCLE_RADIUS_MIN, ::MPI_CIRCLE_RADIUS_MAX]. */
	UINT16 max_radius; /**< Outer radius of dewarping area. Range [::MPI_CIRCLE_RADIUS_MIN, ::MPI_CIRCLE_RADIUS_MAX]. */
	UINT16 ldc_ratio; /**< Distortion ratio. Range [::MPI_LDC_RATIO_MIN, ::MPI_LDC_RATIO_MAX]. Default 0. */
} MPI_SURROUND_ATTR_S;

/**
 * @struct MPI_STITCH_DIST_S
 * @brief Struct for stitch parmeters at certain distance.
 */
typedef struct mpi_stitch_dist {
	UINT16 dist; /**< Distance in cm. */
	UINT16 ver_disp; /**< Vertical display area. */
	UINT16 straighten; /**< Straighten line level. */
	UINT16 src_zoom; /**< Source image zoom. */
	INT16 theta[MPI_STITCH_SENSOR_NUM]; /**< Rotation angle. */
	UINT16 radius[MPI_STITCH_SENSOR_NUM]; /**< Stitch radius. */
	UINT16 curvature[MPI_STITCH_SENSOR_NUM]; /**< Stitch curvature. */
	UINT16 fov_ratio[MPI_STITCH_SENSOR_NUM]; /**< Horizontal fov ratio. */
	UINT16 ver_scale[MPI_STITCH_SENSOR_NUM]; /**< Vertical scaling. */
	INT16 ver_shift[MPI_STITCH_SENSOR_NUM]; /**< Vertical shift. */
} MPI_STITCH_DIST_S;

/**
 * @struct MPI_STITCH_ATTR_S
 * @brief Struct for the attribute of content adaptive stitching.
 * @details The parameters are generated by the dual-sensor calibration tool.
 * @note
 * @arg User must enable stitching in MPI_DEV_ATTR_S before configuring this attirbute.
 * @arg The parameters is valid when ::MPI_WIN_ATTR_S::view_type is set as stitch view
 */
typedef struct mpi_stitch_attr {
	UINT8 enable; /**< Enable stitch, 0: disable, 1: enable. */
	MPI_POINT_S center[MPI_STITCH_SENSOR_NUM]; /**< Camera optical center */
	INT32 dft_dist; /**< Default distance */
	INT32 table_num; /**< Number of stitch table used */
	MPI_STITCH_DIST_S table[MPI_STITCH_TABLE_NUM]; /**< Stitch tables */
} MPI_STITCH_ATTR_S;

/**
 * @brief Enumeration of window view type.
 */
typedef enum mpi_win_view_type {
	MPI_WIN_VIEW_TYPE_NORMAL = 0, /**< Normal view without GFX. */
	MPI_WIN_VIEW_TYPE_LDC, /**< LDC view. */
	MPI_WIN_VIEW_TYPE_PANORAMA, /**< Panorama view. */
	MPI_WIN_VIEW_TYPE_PANNING, /**< Panning view. */
	MPI_WIN_VIEW_TYPE_SURROUND, /**< Surround view. */
	MPI_WIN_VIEW_TYPE_STITCH, /**< Stitch view. */
	MPI_WIN_VIEW_TYPE_GRAPHICS, /**< Graphics view. */
	MPI_WIN_VIEW_TYPE_NUM,
} MPI_WIN_VIEW_TYPE_E;

/**
 * @struct MPI_WIN_ATTR_S
 * @brief Struct for the video window attributes.
 * @note
 * @arg fps, roi, mirr_en and flip_en can by modified dynamically. Other attributes
 *      cannot be modified when video channel is running.
 * @arg If window attributes are not modified by MPI_DEV_setWindowAttr(),
 * it follows the default value:
 * @code{.c}
 * {
 *     .path.bmp = 0x01,
 *     .fps = 20,
 *     .rotate = MPI_ROTATE_0,
 *     .mirr_en = 0,
 *     .flip_en = 0,
 *     .view_type = MPI_WIN_VIEW_TYPE_NORMAL,
 *     .prio = 0,
 *     .roi = (MPI_RECT_S){ .x = 0, .y = 0, .width = MPI_MAX_ROI_VAL, .height = MPI_MAX_ROI_VAL }
 * }
 * @endcode
 * @see MPI_DEV_setWindowAttr()
 * @see MPI_DEV_setWindowRoi()
 * @property MPI_RECT_S MPI_WIN_ATTR_S::roi
 * @note
 * @arg The value of `x`, `y`, `x + width` and `x + height` must be in range
 *      [::MPI_MIN_ROI_VAL, ::MPI_MAX_ROI_VAL].
 * @property MPI_RECT_S MPI_WIN_ATTR_S::prio
 * @details The window with larger priority would overlay smaller ones
 */
typedef struct mpi_win_attr {
	MPI_PATH_BMP_U path; /**< Input path enable bitmap. */
	FLOAT fps; /**< Video window output frame rate.
	             *  Should be a positive number, then smaller than or equal to channel FPS
	             */
	MPI_ROTATE_TYPE_E rotate; /**< Rotate type. */
	UINT8 mirr_en; /**< Mirror enable bit. 0: disable, 1: enable */
	UINT8 flip_en; /**< Flip enable bit. 0: disable, 1: enable */
	MPI_WIN_VIEW_TYPE_E view_type; /**< Window view type. */
	MPI_RECT_S roi; /**< Display ROI (proportionality) on the coordinate of corrected images. */
	UINT8 prio; /**< Priority of window. */
	MPI_WIN src_id; /**< Should be ::MPI_INVALID_VIDEO_WIN (Internal Use Only). */
	UINT8 const_qual; /**< Should be 1 (Internal Use Only). */
	UINT8 dyn_adj; /**< Should be 0 (Internal Use Only). */
} MPI_WIN_ATTR_S;

/**
 * @struct MPI_CHN_LAYOUT_S
 * @brief Struct for the video channel layout.
 * @details
 * @note
 * @arg Layout cannot be modified when channel is running.
 * @see MPI_DEV_getChnLayout()
 * @see MPI_DEV_setChnLayout()
 * @property MPI_RECT_S MPI_CHN_LAYOUT_S::window[MPI_MAX_VIDEO_WIN_NUM]
 * @note
 * @arg `window[i].x` must be divisible by ::MPI_CHN_LAYOUT_HOR_ALIGN.
 * @arg `window[i].y` must be divisible by ::MPI_CHN_LAYOUT_VER_ALIGN.
 * @arg `window[i].width` and `window[i].height` must be divisible by ::MPI_CHN_RES_ALIGN.
 * @arg All windows must be in range of associated channel.
 */
typedef struct mpi_chn_layout {
	INT32 window_num; /**< Number of active windows in channel. Range [0, ::MPI_MAX_VIDEO_WIN_NUM] */
	MPI_WIN win_id[MPI_MAX_VIDEO_WIN_NUM]; /**< Window index. */
	MPI_RECT_S window[MPI_MAX_VIDEO_WIN_NUM]; /**< Position from the upper left corner, and size (pixels) of each video window. */
} MPI_CHN_LAYOUT_S;

/**
 * @brief Struct for the configuration of ISP MV hist.
 */
typedef struct mpi_isp_mv_hist_cfg {
	MPI_RECT_POINT_S roi; /**< Region of interest .*/
} MPI_ISP_MV_HIST_CFG_S;

/**
 * @brief Struct for the of ISP MV hist.
 */
typedef struct mpi_isp_mv_hist {
	UINT32 bin_x[MPI_MAX_ISP_MV_HIST_BIN_NUM]; /**< The x coordinate of the bin. */
	UINT32 bin_y[MPI_MAX_ISP_MV_HIST_BIN_NUM]; /**< The y coordinate of the bin. */
	UINT32 bin_count[MPI_MAX_ISP_MV_HIST_BIN_NUM]; /**< Count of the bin. */
	UINT32 total_cnt; /**< Number of total bin count. */
} MPI_ISP_MV_HIST_S;

/**
 * @brief Struct for the of ISP variance configuration.
 */
typedef struct mpi_isp_var_cfg {
	MPI_RECT_POINT_S roi; /**< Region of interest. */
} MPI_ISP_VAR_CFG_S;

/**
 * @brief Struct for the of ISP variance.
 */
typedef struct mpi_isp_var {
	UINT32 hor_sum; /**< Sum of horizontal variance. */
	UINT32 ver_sum; /**< Sum of vertical variance. */
} MPI_ISP_VAR_S;

/**
 * @brief Struct for the of ISP variance configuration.
 */
typedef struct mpi_isp_y_avg_cfg {
	MPI_RECT_POINT_S roi; /**< Region of interest. */
	UINT16 diff_thr; /**< Difference threshold. */
} MPI_ISP_Y_AVG_CFG_S;

/**
 * @brief MPI_ISP_Y_AVG is represented by UINT16
 */
typedef UINT16 MPI_ISP_Y_AVG;

/**
* @brief Enumneration of snapshot type.
*/
typedef enum mpi_snapshot_type {
	MPI_SNAPSHOT_Y = 0, /**< Y only */
	MPI_SNAPSHOT_NV12, /**< NV12 snapshot */
	MPI_SNAPSHOT_RGB, /**< RGB snapshot */
	MPI_SNAPSHOT_MAX
} MPI_SNAPSHOT_TYPE;

/**
 * @brief Struct for the video frame information.
 * @details Contains width, height, image type, etc.
 */
typedef struct mpi_video_frame_info {
	void *paddr; /**< Reserved */
	void *uaddr; /**< Address for the video frame */
	void *uaddr_1; /**< Address for the YUV format's chrominance data */
	void *kaddr; /**< Reserved */
	UINT32 width; /**< Width of the video frame. */
	UINT32 height; /**< Height of the video frame. */
	UINT32 depth; /**< Bit-depth of the video frame. */
	UINT32 size; /**< Total size of the video frame. */
	MPI_SNAPSHOT_TYPE type; /**< Snapshot type */
} MPI_VIDEO_FRAME_INFO_S;

/**
 * @cond
 *
 * code fragment skipped by Doxygen.
 */

/**
 * @brief Enumeration of RS cost.
 */
typedef enum mpi_rs_cost {
	MPI_RS_COST_BW = 0,
	MPI_RS_COST_DRATE,
	MPI_RS_COST_DRAM_SIZE,
	MPI_RS_COST_SCALE_UP_X,
	MPI_RS_COST_SCALE_UP_Y,
	MPI_RS_COST_SCALE_UP_ACC,
	MPI_RS_COST_SCALE_DOWN_X,
	MPI_RS_COST_SCALE_DOWN_Y,
	MPI_RS_COST_SCALE_DOWN_ACC,
	MPI_RS_COST_SCALE_REVERSE, /**< Reverse. */
	MPI_RS_COST_NUM
} MPI_RS_COST_E;

/**
 * @brief Struct for the route synthesis user configuration.
 */
typedef struct mpi_rs_user_attr {
	UINT8 enc_bitrate;
	UINT16 audio_sample_rate;
	FLOAT weight[MPI_RS_COST_NUM];
	FLOAT cost_toler[MPI_RS_COST_NUM];
} MPI_RS_USER_ATTR_S;

/**
 * @brief Struct for the route synthesis hw configuration.
 */
typedef struct mpi_rs_hw_attr {
	UINT8 msb_bw;
	UINT8 lsb_bw;
	UINT8 mv_bw;
	UINT8 osd_bw;
	UINT8 audio_bw;
	UINT8 audio_ch;
	UINT8 frc_bw;
	UINT8 dram_bank_num;
	UINT16 dram_col_byte_num;

	FLOAT is_drate_limit;
	FLOAT enc_drate_limit;
	FLOAT isp_limit[MPI_RS_COST_NUM];
} MPI_RS_HW_ATTR_S;

typedef struct mpi_dev_bit_depth_attr {
	UINT8 tfw_bit_depth; //tfw bit depth: 8 or 10
	UINT8 fscw_bit_depth; //fscw bit depth: 8 or 10
	UINT8 nrw_bit_depth; //nrw bit depth: 8 or 10
	UINT8 scw_bit_depth; //scw bit depth: 8 or 10
	UINT8 mv4w_bit_depth; //mv4w bit depth: 32
	UINT8 mv8w_bit_depth; //mv8w bit depth: 32
	UINT8 refw_bit_depth; //refw bit depth: 8
	UINT8 osdr_bit_depth; //osdr bit depth: 8
} MPI_DEV_BIT_DEPTH_ATTR_S;

/**
 * @endcond
 */

/* Interface function prototype */
INT32 MPI_DEV_addPath(MPI_PATH idx, const MPI_PATH_ATTR_S *p_path_attr);
INT32 MPI_DEV_deletePath(MPI_PATH idx);
INT32 MPI_DEV_setPathAttr(MPI_PATH idx, const MPI_PATH_ATTR_S *p_path_attr);
INT32 MPI_DEV_getPathAttr(MPI_PATH idx, MPI_PATH_ATTR_S *p_path_attr);
INT32 MPI_DEV_createDev(MPI_DEV idx, const MPI_DEV_ATTR_S *p_dev_attr);
INT32 MPI_DEV_destroyDev(MPI_DEV idx);
INT32 MPI_DEV_setDevAttr(MPI_DEV idx, const MPI_DEV_ATTR_S *p_dev_attr);
INT32 MPI_DEV_getDevAttr(MPI_DEV idx, MPI_DEV_ATTR_S *p_dev_attr);
INT32 MPI_DEV_startDev(MPI_DEV idx);
INT32 MPI_DEV_stopDev(MPI_DEV idx);
INT32 MPI_DEV_addChn(MPI_CHN idx, const MPI_CHN_ATTR_S *p_chn_attr);
INT32 MPI_DEV_deleteChn(MPI_CHN idx);
INT32 MPI_DEV_setChnAttr(MPI_CHN idx, const MPI_CHN_ATTR_S *p_chn_attr);
INT32 MPI_DEV_getChnAttr(MPI_CHN idx, MPI_CHN_ATTR_S *p_chn_attr);

INT32 MPI_DEV_setChnLayout(MPI_CHN idx, const MPI_CHN_LAYOUT_S *p_chn_layout);
INT32 MPI_DEV_getChnLayout(MPI_CHN idx, MPI_CHN_LAYOUT_S *p_chn_layout);
INT32 MPI_DEV_setWindowAttr(MPI_WIN idx, const MPI_WIN_ATTR_S *p_window_attr);
INT32 MPI_DEV_getWindowAttr(MPI_WIN idx, MPI_WIN_ATTR_S *p_window_attr);
INT32 MPI_DEV_setWindowRoi(MPI_WIN idx, const MPI_RECT_S *p_roi);
INT32 MPI_DEV_getWindowRoi(MPI_WIN idx, MPI_RECT_S *p_roi);

INT32 MPI_DEV_setStitchAttr(MPI_WIN idx, const MPI_STITCH_ATTR_S *p_stitch_attr);
INT32 MPI_DEV_getStitchAttr(MPI_WIN idx, MPI_STITCH_ATTR_S *p_stitch_attr);

INT32 MPI_DEV_setLdcAttr(MPI_WIN idx, const MPI_LDC_ATTR_S *p_ldc_attr);
INT32 MPI_DEV_getLdcAttr(MPI_WIN idx, MPI_LDC_ATTR_S *p_ldc_attr);

INT32 MPI_DEV_setPanoramaAttr(MPI_WIN idx, const MPI_PANORAMA_ATTR_S *p_panorama_attr);
INT32 MPI_DEV_getPanoramaAttr(MPI_WIN idx, MPI_PANORAMA_ATTR_S *p_panorama_attr);

INT32 MPI_DEV_setPanningAttr(MPI_WIN idx, const MPI_PANNING_ATTR_S *p_panning_attr);
INT32 MPI_DEV_getPanningAttr(MPI_WIN idx, MPI_PANNING_ATTR_S *p_panning_attr);

INT32 MPI_DEV_setSurroundAttr(MPI_WIN idx, const MPI_SURROUND_ATTR_S *p_surround_attr);
INT32 MPI_DEV_getSurroundAttr(MPI_WIN idx, MPI_SURROUND_ATTR_S *p_surround_attr);

INT32 MPI_DEV_startChn(MPI_CHN idx);
INT32 MPI_DEV_stopChn(MPI_CHN idx);
INT32 MPI_DEV_startAllChn(MPI_DEV idx);
INT32 MPI_DEV_stopAllChn(MPI_DEV idx);
INT32 MPI_DEV_queryChnState(MPI_CHN idx, MPI_CHN_STAT_S *stat);

INT32 MPI_DEV_addIspMvHistCfg(MPI_WIN idx, const MPI_ISP_MV_HIST_CFG_S *cfg, UINT8 *cfg_idx);
INT32 MPI_DEV_rmIspMvHistCfg(MPI_WIN idx, UINT8 cfg_idx);
INT32 MPI_DEV_getIspMvHistCfg(MPI_WIN idx, UINT8 cfg_idx, MPI_ISP_MV_HIST_CFG_S *cfg);
INT32 MPI_DEV_getIspMvHist(MPI_WIN idx, UINT8 roi_idx, MPI_ISP_MV_HIST_S *stat);
INT32 MPI_DEV_addIspVarCfg(MPI_WIN idx, const MPI_ISP_VAR_CFG_S *cfg, UINT8 *cfg_idx);
INT32 MPI_DEV_rmIspVarCfg(MPI_WIN idx, UINT8 cfg_idx);
INT32 MPI_DEV_getIspVarCfg(MPI_WIN idx, UINT8 cfg_idx, MPI_ISP_VAR_CFG_S *cfg);
INT32 MPI_DEV_getIspVar(MPI_WIN idx, UINT8 cfg_idx, MPI_ISP_VAR_S *stat);
INT32 MPI_DEV_addIspYAvgCfg(MPI_WIN idx, const MPI_ISP_Y_AVG_CFG_S *cfg, UINT8 *cfg_idx);
INT32 MPI_DEV_rmIspYAvgCfg(MPI_WIN idx, UINT8 cfg_idx);
INT32 MPI_DEV_getIspYAvgCfg(MPI_WIN idx, UINT8 cfg_idx, MPI_ISP_Y_AVG_CFG_S *cfg);
INT32 MPI_DEV_getIspYAvg(MPI_WIN idx, UINT8 cfg_idx, MPI_ISP_Y_AVG *y_avg);

INT32 MPI_DEV_getWinFrame(MPI_WIN idx, MPI_VIDEO_FRAME_INFO_S *frame_info, INT32 time_ms);
INT32 MPI_DEV_releaseWinFrame(MPI_WIN idx, MPI_VIDEO_FRAME_INFO_S *frame_info);

INT32 MPI_DEV_waitWin(MPI_WIN idx, UINT32 *timestamp, INT32 timeout);

/* Legacy MPIs */
INT32 MPI_DEV_waitIspStat(MPI_WIN idx);

/**
* @cond
*
* code fragment skipped by Doxygen.
*/

INT32 MPI_DEV_setRsUserAttr(MPI_DEV idx, const MPI_RS_USER_ATTR_S *p_rs_attr);
INT32 MPI_DEV_getRsUserAttr(MPI_DEV idx, MPI_RS_USER_ATTR_S *p_rs_attr);
INT32 MPI_DEV_setRsHwAttr(MPI_DEV idx, const MPI_RS_HW_ATTR_S *p_rs_attr);
INT32 MPI_DEV_getRsHwAttr(MPI_DEV idx, MPI_RS_HW_ATTR_S *p_rs_attr);

INT32 MPI_DEV_setBitDepthAttr(MPI_DEV idx, const MPI_DEV_BIT_DEPTH_ATTR_S *p_bit_depth_attr);
INT32 MPI_DEV_getBitDepthAttr(MPI_DEV idx, MPI_DEV_BIT_DEPTH_ATTR_S *p_bit_depth_attr);

/**
 * @endcond
 */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* !MPI_DEV_H_ */
