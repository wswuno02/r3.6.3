/*
 * AUGENTIX INC. - PROPRIETARY
 *
 * mpi_dip_common.h - MPI for DIP common header
 * Copyright (C) 2017-2019 Augentix Inc. - All Rights Reserved
 *
 * NOTICE: The information contained herein is the property of Augentix Inc.
 * Copying and distributing of this file, via any medium,
 * must be licensed by Augentix Inc.
 *
 * * Brief: MPI for DIP common header
 * *
 * * Author: NAME <MAIL@augentix.com>
 */

#ifndef MPI_DIP_COMMON_H_
#define MPI_DIP_COMMON_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "mpi_dip_types.h"
#warning "[WARNING] 'mpi_dip_common.h' is depreciated. Please use 'mpi_dip_types.h' instead."

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* !MPI_DIP_COMMON_H_ */
