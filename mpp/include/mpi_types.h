/*
 * AUGENTIX INC. - PROPRIETARY
 *
 * mpi_types.h - MPI data types
 * Copyright (C) 2017-2019 Augentix Inc. - All Rights Reserved
 *
 * NOTICE: The information contained herein is the property of Augentix Inc.
 * Copying and distributing of this file, via any medium,
 * must be licensed by Augentix Inc.
 *
 * * Brief: MPI data types
 * *
 * * Author: NAME <MAIL@augentix.com>
 */

#ifndef MPI_TYPES_H_
#define MPI_TYPES_H_

#include "mpi_base_types.h"
#warning "[WARNING] 'mpi_types.h' is depreciated. Please use 'mpi_base_types.h' instead."

#endif /* !MPI_TYPES_H_ */
