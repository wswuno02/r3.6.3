/*
 * AUGENTIX INC. - PROPRIETARY
 *
 * mpi_sys.h - MPI for MPP system
 * Copyright (C) 2017-2019 Augentix Inc. - All Rights Reserved
 *
 * NOTICE: The information contained herein is the property of Augentix Inc.
 * Copying and distributing of this file, via any medium,
 * must be licensed by Augentix Inc.
 *
 * * Brief: MPI for MPP system
 * *
 * * Author: NAME <MAIL@augentix.com>
 */

/**
 * @file mpi_sys.h
 * @brief MPI for MPP system
 */

#ifndef MPI_SYS_H_
#define MPI_SYS_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "mpi_base_types.h"

#define MPI_MAX_POOL_NAME_LEN 16 /**< Maximum string length for pool name. */
#define MPI_MAX_PUB_POOL 16 /**< Maximum number of public pool. */

/**
 * @brief Struct for pool configuration.
 */
typedef struct mpi_vb_pool_conf {
	UINT32 blk_size; /**< Block size in this pool. */
	UINT32 blk_cnt; /**< How many blocks in this pool. */
	UINT8 name[MPI_MAX_POOL_NAME_LEN]; /**< Pool name. */
} MPI_VB_POOL_CONF_S;

/**
 * @brief Struct for video buffer configuration.
 */
typedef struct mpi_vb_conf {
	UINT32 max_pool_cnt; /**< Max pool count including public and private pools. */
	MPI_VB_POOL_CONF_S pub_pool[MPI_MAX_PUB_POOL]; /**< Public pool configuration. */
} MPI_VB_CONF_S;

/* Interface function prototype */
INT32 MPI_VB_setConf(const MPI_VB_CONF_S *p_vb_conf);
INT32 MPI_VB_getConf(MPI_VB_CONF_S *p_vb_conf);
INT32 MPI_VB_init(VOID);
INT32 MPI_VB_exit(VOID);
INT32 MPI_SYS_init(VOID);
INT32 MPI_SYS_exit(VOID);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* !MPI_SYS_H_ */
