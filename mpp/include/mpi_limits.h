/*
 * AUGENTIX INC. - PROPRIETARY
 *
 * mpi_limits.h - Limits of MPI properties
 * Copyright (C) 2017-2019 Augentix Inc. - All Rights Reserved
 *
 * NOTICE: The information contained herein is the property of Augentix Inc.
 * Copying and distributing of this file, via any medium,
 * must be licensed by Augentix Inc.
 *
 * * Brief: Limits of MPI properties
 * *
 * * Author: NAME <MAIL@augentix.com>
 */

/**
 * @file mpi_limits.h
 * @brief Limits of MPI properties
 */

#ifndef MPI_LIMITS_H_
#define MPI_LIMITS_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define MPI_MAX_INPUT_PATH_NUM (2) /**< Maximum number of input path. */
#define MPI_MAX_VIDEO_DEV_NUM (1) /**< Maximum number of video device. */
#define MPI_MAX_VIDEO_CHN_NUM (4) /**< Maximum number of video channel. */
#define MPI_MAX_VIDEO_WIN_NUM (9) /**< Maximum number of video window. */
#define MPI_MAX_ENC_CHN_NUM (4) /**< Maximum number of encoder channel. */
#define MPI_MAX_MV_CHN_NUM (1) /**< Maximum number of MV channel. */
#define MPI_MAX_CHIP_CHN_NUM                                                                                           \
	(MPI_MAX_VIDEO_DEV_NUM * MPI_MAX_VIDEO_CHN_NUM * MPI_MAX_VIDEO_WIN_NUM) /**< Maximum number of chip channel */
#define MPI_MAX_MODLE_NUM (1) /**< Maximum number of module */
#define MPI_MAX_DATA_LANE_NUM (4) /**< Maximum number of LVDS data lane number. */
#define MPI_MAX_LVDSRX_LANE_NUM (MPI_MAX_DATA_LANE_NUM + 1) /**< Maximum number of LVDS receiver lane number. */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* MPI_LIMITS_H_ */
