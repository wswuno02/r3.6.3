################################################################################
# SDK related definitions
################################################################################

SDKSRC_DIR ?= $(realpath $(CURDIR)/../..)
include $(SDKSRC_DIR)/build/sdksrc.mk
include $(SDKSRC_DIR)/build/build_utils/lib.mk
include $(ABIVER_MAKE)

###############################################################################
# Chip type
###############################################################################

MPP_CHIP := $(call chip,$(CONFIG_CHIP))

################################################################################
# Build tools and commands
################################################################################

### First, specify the build tools.

CC := $(CROSS_COMPILE)gcc
CX := $(CROSS_COMPILE)g++
AR := $(CROSS_COMPILE)ar
RM := rm
LN := ln
MKDIR := mkdir
RMDIR := rmdir
STRIP := $(CROSS_COMPILE)strip
INSTALL := install

### Then, define global compilation and linking flags.

ifeq ($(CONFIG_HC1703_1723_1753_1783S),y)
DEFINE_ALL := HC17X3_SPECIFIC
else
DEFINE_ALL := HC17X2_SPECIFIC
endif

CFLAGS_ALL := -Wall -g -O2 -MMD ${PERF_NO_OMIT_FP} -D_GNU_SOURCE -mcpu=$(CONFIG_TARGET_CPU) -fPIC
LFLAGS_ALL :=

### Finally, define complete commands for all actions.

COMPILE = $(addprefix -D,$(DEFINE_ALL)) $(addprefix -D,$(DEFINE_LOCAL)) $(CFLAGS_ALL) $(CFLAGS_LOCAL) -o $@ -c $<
LINK = -o $@ $^ $(LFLAGS_ALL) $(LFLAGS_LOCAL)
ARCHIVE_STATIC = $(AR) rcsD $@ $^
ARCHIVE_SHARED = -shared -Wl,--build-id -Wl,--soname,$(basename $(basename $(notdir $@))) -o $@ \
                 -Wl,--whole-archive $^ -Wl,--no-whole-archive

################################################################################
# Build rules
################################################################################

### First, define default build target.

.PHONY: all
all: primary

### Second, define variables for collecting outputs and temporarily objects.

TARGET_BINS   :=
TARGET_DOCS   :=
TARGET_MODS   :=
TARGET_SLIBS  :=
TARGET_DLIBS  :=
TARGET_TESTS  :=
CLEAN_FILES   :=
CLEAN_DIRS    :=
CLEAN_TARGETS :=

### Then, collect the knowledge about how to build all outputs.

root := $(MPP_PATH)
dir := $(root)/build

### Define helper variables if need.

CPVS_SRC := $(SDKSRC_DIR)/cpvs/$(MPP_CHIP)/src

ifneq ($(CONFIG_RELEASE),y)
    ### In SDK manifest, traverse directory tree to include Rules.mk files.

    subdir := $(root)/dip
    include $(subdir)/Rules.mk

    subdir := $(root)/iva
    include $(subdir)/Rules.mk

    subdir := $(root)/libdebug
    include $(subdir)/Rules.mk

    subdir := $(root)/mpi
    include $(subdir)/Rules.mk

    subdir := $(root)/sensor
    include $(subdir)/Rules.mk

    subdir := $(root)/utrc
    include $(subdir)/Rules.mk

    subdir := $(root)/vb
    include $(subdir)/Rules.mk

    subdir := $(root)/vpc
    include $(subdir)/Rules.mk

    subdir := $(root)/enc
    include $(subdir)/Rules.mk

    subdir := $(root)/is
    include $(subdir)/Rules.mk

    subdir := $(root)/isp
    include $(subdir)/Rules.mk

    subdir := $(root)/osd
    include $(subdir)/Rules.mk

    subdir := $(root)/senif
    include $(subdir)/Rules.mk

    subdir := $(root)/sr
    include $(subdir)/Rules.mk

    subdir := $(root)/rc
    include $(subdir)/Rules.mk

    subdir := $(root)/uvpp
    include $(subdir)/Rules.mk

    subdir := $(root)/audio
    include $(subdir)/Rules.mk

    subdir := $(root)/kvpp
    include $(subdir)/Rules.mk

    subdir := $(root)/aec
    include $(subdir)/Rules.mk

    CLEAN_FILES += $(root)/ko/enc.ko \
                   $(root)/ko/is.ko \
                   $(root)/ko/isp.ko \
                   $(root)/ko/osd.ko \
                   $(root)/ko/rc.ko \
                   $(root)/ko/senif.ko \
                   $(root)/ko/sr.ko \
                   $(root)/ko/utrc.ko \
                   $(root)/ko/vb.ko

ifneq ($(CONFIG_HC1703_1723_1753_1783S),y)

    CLEAN_FILES += $(root)/ko/vda.ko

else

    CLEAN_FILES += $(root)/ko/audio_pcm.ko \
                   $(root)/ko/adc.ko \
                   $(root)/ko/aec.ko

endif # ifeq ($(CONFIG_HC1703_1723_1753_1783S),y)

    CLEAN_DIRS  += $(root)/ko

endif # ifneq ($(CONFIG_RELEASE),y)

TARGET_MODS += $(root)/ko/enc.ko \
               $(root)/ko/is.ko \
               $(root)/ko/isp.ko \
               $(root)/ko/osd.ko \
               $(root)/ko/rc.ko \
               $(root)/ko/senif.ko \
               $(root)/ko/sr.ko \
               $(root)/ko/utrc.ko \
               $(root)/ko/vb.ko

ifneq ($(CONFIG_HC1703_1723_1753_1783S),y)

    TARGET_MODS += $(root)/ko/vda.ko

else

    TARGET_MODS += $(root)/ko/audio_pcm.ko \
                   $(root)/ko/adc.ko \
                   $(root)/ko/aec.ko

endif # ifeq ($(CONFIG_HC1703_1723_1753_1783S),y)

### Finally, define essential build targets.

################################################################################
# Dependency
################################################################################

# ------------------ Build and install custom sensor library -------------------

.PHONY: custom-sensor custom-sensor-clean custom-sensor-install custom-sensor-uninstall

custom-sensor:
	$(Q)$(MAKE) -C $(root)/custom/sensor

custom-sensor-clean:
	$(Q)$(MAKE) -C $(root)/custom/sensor clean

custom-sensor-install:
	$(Q)$(MAKE) -C $(root)/custom/sensor install

custom-sensor-uninstall:
	$(Q)$(MAKE) -C $(root)/custom/sensor uninstall


# ------------------------ Build and install libraries -------------------------

TARGET_SLIB_$(dir) := $(root)/lib/libmpp.a
TARGET_DLIB_$(dir) := $(root)/lib/libmpp.so.$(REAL_SUFFIX)

ifneq ($(CONFIG_RELEASE),y)

ifeq ($(CONFIG_HC1703_1723_1753_1783S),y)

    $(TARGET_SLIB_$(dir)) $(TARGET_DLIB_$(dir)): $(OBJS_$(root)/dip) \
                                                 $(OBJS_$(root)/enc) \
                                                 $(OBJS_$(root)/is) \
                                                 $(OBJS_$(root)/isp) \
                                                 $(OBJS_$(root)/iva) \
                                                 $(OBJS_$(root)/libdebug) \
                                                 $(OBJS_$(root)/mpi) \
                                                 $(OBJS_$(root)/osd) \
                                                 $(OBJS_$(root)/senif) \
                                                 $(OBJS_$(root)/sensor) \
                                                 $(OBJS_$(root)/sr) \
                                                 $(OBJS_$(root)/utrc) \
                                                 $(OBJS_$(root)/vb) \
                                                 $(OBJS_$(root)/vpc/rs) \
                                                 $(OBJS_$(root)/vpc/vplat) \
                                                 $(OBJS_$(root)/audio) \
                                                 $(OBJS_$(root)/aec)

else

    $(TARGET_SLIB_$(dir)) $(TARGET_DLIB_$(dir)): $(OBJS_$(root)/dip) \
                                                 $(OBJS_$(root)/iva) \
                                                 $(OBJS_$(root)/libdebug) \
                                                 $(OBJS_$(root)/mpi) \
                                                 $(OBJS_$(root)/sensor) \
                                                 $(OBJS_$(root)/utrc) \
                                                 $(OBJS_$(root)/uvpp) \
                                                 $(OBJS_$(root)/vb) \
                                                 $(OBJS_$(root)/vpc/rs) \
                                                 $(OBJS_$(root)/vpc/vplat)

endif # ifeq ($(CONFIG_HC1703_1723_1753_1783S),y)

    $(TARGET_SLIB_$(dir)):
	@printf "  AR\t$@\n"
	$(Q)$(MKDIR) -p $(@D)
	$(Q)$(ARCHIVE_STATIC)

    $(TARGET_DLIB_$(dir)):
	@printf "  CC\t$@\n"
	$(Q)$(MKDIR) -p $(@D)
	$(Q)$(CC) $(ARCHIVE_SHARED)
	$(Q)$(LN) -sfT $(notdir $@) $(basename $(basename $(basename $@)))

    CLEAN_FILES  += $(TARGET_SLIB_$(dir)) $(TARGET_DLIB_$(dir)) \
                    $(basename $(basename $(basename $(TARGET_DLIB_$(dir)))))
    CLEAN_DIRS   += $(dir $(TARGET_SLIB_$(dir))) $(dir $(TARGET_DLIB_$(dir)))

endif # ifneq ($(CONFIG_RELEASE),y)

TARGET_SLIBS += $(TARGET_SLIB_$(dir))
TARGET_DLIBS += $(TARGET_DLIB_$(dir))

################################################################################
# Targets (Must be placed after "Subdirectories" section)
################################################################################

.PHONY:	primary
ifneq ($(CONFIG_RELEASE),y)
    ### In SDK manifest, build primary outputs if necessary.
    primary:
	$(Q)$(MAKE) -s check
	$(Q)$(MAKE) custom-sensor $(TARGET_SLIBS) $(TARGET_DLIBS) $(TARGET_MODS)
else
    ### In APP manifest, create link to pre-built primary outputs.
    primary: custom-sensor
	@echo - Override MPP configuration with $(MPP_CHIP)
	$(Q)$(LN) -sfT lib.$(MPP_CHIP) $(root)/lib
	$(Q)$(LN) -sfT ko.$(MPP_CHIP) $(root)/ko
endif

.PHONY: test
ifneq ($(CONFIG_RELEASE),y)
    ### In SDK manifest, build the test outputs if necessary.
    test: primary $(TARGET_TESTS)
else
    ### In APP manifest, do nothing.
    test:
endif

.PHONY: doc
ifneq ($(CONFIG_RELEASE),y)
    ### In SDK manifest, build the doc outputs if necessary.
    doc: $(TARGET_DOCS)
else
    ### In APP manifest, do nothing.
    doc:
endif

.PHONY: clean
ifneq ($(CONFIG_RELEASE),y)
    ### In SDK manifest, remove primary outputs and temporarily objects.
    clean: custom-sensor-clean $(CLEAN_TARGETS)
	@printf "  CLEAN\t$(root)\n"
	$(Q)$(RM) -f $(CLEAN_FILES)
	$(Q)$(foreach d, $(CLEAN_DIRS), $(RM) -rf $(d); )
else
    ### In APP manifest, remove the link to pre-build primary outputs.
    clean: custom-sensor-clean
	@printf "  CLEAN\t$(root)\n"
	$(Q)$(RM) -f $(root)/lib
	$(Q)$(RM) -f $(root)/ko
endif

.PHONY: distclean
distclean: clean

.PHONY: install
install: primary custom-sensor-install
	$(Q)$(MKDIR) -p $(CUSTOM_LIB)
	$(Q)$(foreach f, $(TARGET_DLIBS), \
		printf "  INSTALL\t$(CUSTOM_LIB)/$(notdir $(f))\n"; \
		find $(CUSTOM_LIB) -type f \( -name $(basename $(basename $(basename $(notdir $(f))))).* -not -name $(notdir $(f)) \) -exec rm -rf {} \;; \
		$(INSTALL) -m 644 -t $(CUSTOM_LIB) $(f); \
		$(LN) -sfT $(notdir $(f)) $(CUSTOM_LIB)/$(basename $(basename $(notdir $(f)))); \
	)
	$(Q)$(foreach f, $(TARGET_MODS), \
		printf "  INSTALL\t$(CUSTOM_LIB)/$(notdir $(f))\n"; \
		$(INSTALL) -m 644 -t $(CUSTOM_LIB) $(f); \
	)

.PHONY: uninstall
uninstall: custom-sensor-uninstall
	$(Q)$(foreach f, $(TARGET_DLIBS), \
		printf "  UNINSTALL\t$(CUSTOM_LIB)/$(notdir $(f))\n"; \
		$(RM) -f $(CUSTOM_LIB)/$(basename $(basename $(basename $(notdir $(f))))).*; \
	)
	$(Q)$(foreach f, $(TARGET_MODS), \
		printf "  UNINSTALL\t$(CUSTOM_LIB)/$(notdir $(f))\n"; \
		$(RM) -f $(CUSTOM_LIB)/$(notdir $(f)); \
	)
	$(Q)if [ -d $(CUSTOM_LIB) ]; then \
		$(RMDIR) --ignore-fail-on-non-empty $(CUSTOM_LIB); \
	fi

.PHONY: help
help:
	@echo ""
	@echo "  Syntax:"
	@echo "    make [options] <target>"
	@echo ""
	@echo "  Options:"
	@echo "    V=0         Hide command lines. (default)"
	@echo "    V=1         Show command lines."
	@echo ""
	@echo "  Targets:"
	@echo "    all         Build primary outputs. (default)"
	@echo "    test        Build test programs."
	@echo "    doc         Build documents."
	@echo "    clean       Clean all outputs."
	@echo "    distclean   Clean all outputs and configurations."
	@echo "    install     Install primary outputs"
	@echo "    uninstall   Uninstall primary outputs"
	@echo "    help        Show help messages."
	@echo ""

.PHONY: check
check:
ifeq ($(CONFIG_TARGET_CPU),)
	$(error Unknown parameter CONFIG_TARGET_CPU, please try setting product configuration)
endif

################################################################################
# General rules
################################################################################

%.o: %.c
	@printf "  CC\t$@\n"
	$(Q)$(CC) $(COMPILE)

%.o: %.cpp
	@printf "  CX\t$@\n"
	$(Q)$(CX) $(COMPILE)

################################################################################
# Quiet options
################################################################################

V ?= 0
ifeq ($(V),1)
	Q :=
	VOUT :=
else
	Q := @
	VOUT := 2>&1 1>/dev/null
endif

################################################################################
# Prevent make from removing any build targets, including intermediate ones
################################################################################

.SECONDARY: $(CLEAN_FILES) $(CLEAN_DIRS)
