#ifndef __SSV_CONF_PARSER_H__
#define __SSV_CONF_PARSER_H__
char const *conf_parser[] = {
"CONFIG_SSV_BUILD_AS_ONE_KO",
"CONFIG_FW_ALIGNMENT_CHECK",
"CONFIG_PLATFORM_SDIO_OUTPUT_TIMING_0",
"__CHECK_ENDIAN__",
"DEBUG",
"SSV_SUPPORT_SSV6006",
"CONFIG_SSV6200_CLI_ENABLE",
"CONFIG_SSV_TX_LOWTHRESHOLD",
"RATE_CONTROL_REALTIME_UPDATE",
"CONFIG_SSV6200_HAS_RX_WORKQUEUE",
"USE_THREAD_RX",
"USE_THREAD_TX",
"ENABLE_AGGREGATE_IN_TIME",
"ENABLE_INCREMENTAL_AGGREGATION",
"USE_GENERIC_DECI_TBL",
"SSV6200_ECO",
"CONFIG_SMARTLINK",
"CONFIG_SSV_SMARTLINK",
"CONFIG_SSV_CCI_IMPROVEMENT",
"HWIF_SDIO_RX_IRQ",
"CONFIG_STA_BCN_FILTER",
"CONFIG_SSV_USE_SDIO_DAT1_AS_INT",
"CONFIG_SDIO_FAVOR_RX",
"HWIF_IGNORE_SAFE_RW_REG",
"CONFIG_HW_SCAN",
"CONFIG_USB_TX_MULTI_URB",
""};
#endif // __SSV_CONF_PARSER_H__
