/dts-v1/;
#include "hc1703.dtsi"
#include <dt-bindings/gpio/gpio.h>

/ {
	model = "Augentix AGT804-20";

	soc@80000000 {
		i2c0: i2c@80070000 {
			status = "okay";
		};

		i2c1: i2c@80080000 {
			status = "okay";
		};

		uart0: uart@80820000 {
			baudrate = <115200>;
			clock-frequency = <125000000>;
			status = "okay";
		};

		sdc0: sdc@818A0000 {
			pinctrl-names = "default", "sd_poc_1v8";//, "volt_a_dis", "volt_a_en";
			pinctrl-0 = <&pad_sd3v3_pinctrl>;
			pinctrl-1 = <&pad_sd1v8_pinctrl>;
			// pinctrl-2 = <&pad_sd_power_disable>;
			// pinctrl-3 = <&pad_sd_power_enable>;
			status = "okay";
		};

		sdc1_p0: sdc1_p0@818C0000 {
			pinctrl-names = "default";
			pinctrl-0 = <&pad_sdc1_activate_p0>;
			// status = "okay";
		};

		sdc1_p1: sdc1_p1@818C0000 {
			pinctrl-names = "default";
			pinctrl-0 = <&pad_sdc1_activate_p1>;
			// status = "okay";
		};

		pwm: pwm@80060000 {
			pinctrl-names = "default";
			pinctrl-0 = <&pad_pwm>;
			// status = "okay";
		};

		usbc: usbc@A0000000 {
			dr_mode = "host";
			status = "okay";
		};

		usb2_phy: usbphy@0 {
			status = "okay";
		};

		dummy_drive: dummy {
			status = "okay";
		};

		enc: enc@0x81001000 {
			bsb-size = <0x80000 0x40000 0x0 0x0>;
			rd-buf-size = <0x20000>;
			snapshot-size = <0x80000>;
			rd-buf-num = <4>;
			status = "ok";
		};

		qspi: qspi@83610000{
			status = "okay";
			nor@0{
				#address-cells = <1>;
				#size-cells = <1>;
				compatible = "augentix,m25p80", "augentix,spi-nor";
				reg = <0>;
				spi-max-frequency = <46875000>;
				spi-tx-bus-width = <4>;
				spi-rx-bus-width = <4>;
				m25p,fast-read;
				status = "okay";
			};
		};

		gpio: gpio@80001800 {
			aioc_gpio = <0x0 0x0>;
			pioc0_gpio = <0x0 0x0>;
			pioc1_gpio = <0x0 0x0>;
			pioc2_gpio = <0x0 0x0>;
			mipi_tx_gpio_o_0 = <0x0>;
			status = "okay";
		};

		gpio_test: gpio_test {
			compatible = "augentix,gpio-test";
			aioc-gpios = <&gpio 68 GPIO_ACTIVE_HIGH>;
			pioc0-gpios = <&gpio 0 GPIO_ACTIVE_HIGH>, <&gpio 15 GPIO_ACTIVE_HIGH>, <&gpio 20 GPIO_ACTIVE_HIGH>, <&gpio 25 GPIO_ACTIVE_HIGH>, <&gpio 31 GPIO_ACTIVE_HIGH>;
			pioc1-gpios = <&gpio 32 GPIO_ACTIVE_HIGH>, <&gpio 40 GPIO_ACTIVE_HIGH>, <&gpio 50 GPIO_ACTIVE_HIGH>, <&gpio 55 GPIO_ACTIVE_HIGH>, <&gpio 63 GPIO_ACTIVE_HIGH>;
			pioc2-gpios = <&gpio 64 GPIO_ACTIVE_HIGH>, <&gpio 65 GPIO_ACTIVE_HIGH>, <&gpio 66 GPIO_ACTIVE_HIGH>, <&gpio 67 GPIO_ACTIVE_HIGH>;
			mipitx-gpios = <&gpio 81 GPIO_ACTIVE_HIGH>, <&gpio 84 GPIO_ACTIVE_HIGH>, <&gpio 86 GPIO_ACTIVE_HIGH>, <&gpio 88 GPIO_ACTIVE_HIGH>, <&gpio 90 GPIO_ACTIVE_HIGH>;
			status = "disable";
		};

		clk_test: clk_test {
			compatible = "augentix,clk-test";
			clocks = <&clkc 0>, <&clkc 1>, <&clkc 2>, <&clkc 3>, <&clkc 4>, <&clkc 5>, <&clkc 6>, <&clkc 7>, <&clkc 8>, <&clkc 9>, <&clkc 10>, <&clkc 11>, <&clkc 12>, <&clkc 13>, <&clkc 14>, <&clkc 15>, <&clkc 16>, <&clkc 17>, <&clkc 18>, <&clkc 19>, <&clkc 20>, <&clkc 21>, <&clkc 22>, <&clkc 23>, <&clkc 24>, <&clkc 25>, <&clkc 26>, <&clkc 27>, <&clkc 28>, <&clkc 29>, <&clkc 30>, <&clkc 31>, <&clkc 32>, <&clkc 33>, <&clkc 34>, <&clkc 35>, <&clkc 36>, <&clkc 37>, <&clkc 38>, <&clkc 39>, <&clkc 40>, <&clkc 41>, <&clkc 42>, <&clkc 43>, <&clkc 44>, <&clkc 45>, <&clkc 46>, <&clkc 47>, <&clkc 48>, <&clkc 49>, <&clkc 50>, <&clkc 51>, <&clkc 52>, <&clkc 53>;
			clock-names = "clk_dramc", "clk_dramc_hdr", "clk_axi_dramc", "clk_dma", "clk_apb_spi0", "clk_apb_spi1", "clk_apb_uart0", "clk_apb_uart1", "clk_apb_uart2", "clk_apb_uart3", "clk_apb_uart4", "clk_apb_uart5", "clk_apb_sdc0", "clk_apb_sdc1", "clk_apb_emac", "clk_axi_rom", "clk_axi_ram", "clk_ahb_master", "clk_ahb_usb", "clk_ahb_sdc_0", "clk_ahb_sdc_1", "clk_emac_rgmii", "clk_emac_rmii", "clk_qspi", "clk_isp", "clk_vp", "clk_sensor", "clk_senif", "clk_is", "clk_disp", "clk_sdc_0", "clk_sdc_1", "clk_enc", "clk_efuse", "clk_eirq", "clk_is_lp", "clk_i2cm0", "clk_i2cm1", "clk_spi0", "clk_spi1", "clk_timer0", "clk_timer1", "clk_timer2", "clk_timer3", "clk_pwm_0", "clk_pwm_1", "clk_pwm_2", "clk_pwm_3", "clk_pwm_4", "clk_pwm_5", "clk_audio_in", "clk_audio_out", "clk_usb_utmi";
			clock-frequency = <800000000 400000000 400000000 100000000 250000000 250000000 250000000 250000000 250000000 250000000 250000000 250000000 250000000 250000000 250000000 250000000 250000000 250000000 250000000 250000000 250000000 250000000 50000000 200000000 375000000 375000000 37125000 297000000 445500000 445500000 200000000 200000000 333000000 24000000 24000000 24000000 24000000 24000000 24000000 24000000 24000000 24000000 24000000 24000000 24000000 24000000 50000000 50000000 24000000 24000000 140000000 100000000 60000000>;
			status = "disable";
		};

		pinctrl: pinctrl@80001000 {
			xin-pcfg = <1>;
			resetb-pcfg = <4>;
			pad_sd1v8_pinctrl: pad_sd1v8_pinctrl@0 {
				SD_POC_1V8 {
					augentix,pins = <83>;
					pins = "PAD_SD_POC_PCFG";
					power-source = <1>; // 1 -> 1v8
				};
				SD_VOLT_REG_1V8 {
					augentix,pins = <64>;
					augentix,pmx = <3>;
					pins = "SD_VOLT_REG_1V8";
					function = "pad_sd1v8_pinctrl";
				};
			};
			pad_sd3v3_pinctrl: pad_sd3v3_pinctrl@0 {
				SD_POC_3V3 {
					augentix,pins = <83>;
					pins = "PAD_SD_POC_PCFG";
					power-source = <0>; // 0 -> 3v3
				};
				SD_VOLT_REG_3V3 {
					augentix,pins = <64>;
					augentix,pmx = <5>;
					pins = "SD_VOLT_REG_3V3";
					function = "pad_sd3v3_pinctrl";
				};
			};
			pad_sd_power_enable: pad_sd_power_enable@0 {
				SD_VOLT_A_ENABLE {
					augentix,pins = <63>;
					augentix,pmx = <2>;
					pins = "SD_VOLT_A_ENABLE";
					function = "pad_sd_power_enable";
				};
			};
			pad_sd_power_disable: pad_sd_power_disable@0 {
				SD_VOLT_A_DISABLE {
					augentix,pins = <63>;
					augentix,pmx = <0>;
					pins = "SD_VOLT_A_DISABLE";
					function = "pad_sd_power_disable";
				};
			};
			pad_sdc1_activate_p0: pad_sdc1_activate_p0@0 {
				PAD_SDC1_P0_CONFIG {
					augentix,pins = <27 28 31 32 34 35 38 41>;
					pins = "PAD_EMAC_TX_CK", "PAD_EMAC_TX_CTL", "PAD_EMAC_TX_D1", "PAD_EMAC_TX_D0",
						   "PAD_EMAC_RX_D0", "PAD_EMAC_RX_D1", "PAD_EMAC_RX_CTL", "PAD_SPI1_SDI";
					drive-strength = <0>;
				};
				PAD_SDC1_P0_GROUP {
					augentix,pins = <27 28 31 32 34 35 38 41>;
					augentix,pmx = <2 2 2 2 2 2 2 4>;
					pins = "PAD_SDC1_P0_GROUP";
					function = "pad_sdc1_activate_p0";
				};
			};
			pad_sdc1_activate_p1: pad_sdc1_activate_p1@0 {
				PAD_SDC1_P1_CONFIG {
					augentix,pins = <29 30 36 37 41 42 44>;
					pins = "PAD_EMAC_TX_D3", "PAD_EMAC_TX_D2", "PAD_EMAC_RX_D2", "PAD_EMAC_RX_D3",
						   "PAD_SPI1_SDI", "PAD_SPI1_SDO", "PAD_GPIO7";
					drive-strength = <3>;
				};
				PAD_SDC1_P1_GROUP {
					augentix,pins = <29 30 36 37 41 42 44>;
					augentix,pmx = <2 2 2 2 2 2 2>;
					pins = "PAD_SDC1_P1_GROUP";
					function = "pad_sdc1_activate_p1";
				};
			};
			pad_audio_output_amp_en: pad_audio_output_amp_en {
				PAD_AMP_EN_GPIO {
					augentix,pins = <66>;
					augentix,pmx = <0>;
					pins = "PAD_AMP_EN_GPIO";
					function = "pad_audio_output_amp_en";
				};
			};

			pad_pwm: pad_pwm@0 {
				PAD_PWM_GROUP {
					augentix,pins = <65 12 48 64 13 3>;
					augentix,pmx = <1 1 1 1 1 2>;
					pins = "PAD_PWM_GROUP";
					function = "pad_pwm";
				};
			};
		};

		ethernet2@81880000 {
			status = "okay";
			phy-reset-gpios = <&gpio 26 GPIO_ACTIVE_HIGH>;
		};

		adc@80100000 {
			status = "okay";
		};

		audio_pcm@80100000 {
			status = "okay";
			pinctrl-names = "default";
			pinctrl-0 = <&pad_audio_output_amp_en>;
			audio-output-amp-en-gpios = <&gpio 66 GPIO_ACTIVE_HIGH>;
		};

		audio_machine {
			status = "okay";
		};
	};
};
