/dts-v1/;

#include "hc1782.dtsi"

/ {
	model = "Augentix MA80007 V1";
	compatible = "augentix,hc18xx", "augentix,agt-ma80007-v1";

	chosen {
		bootargs = "ubi.mtd=6 root=ubi0:rootfs rootfstype=ubifs rw console=ttyAS0,115200 clk_ignore_unused";
	};

	amba {
		enc: enc@80400000 {
			bsb-size = <0x600000 0x180000 0x000000 0x100000>;
		};

		nrs: nrs {
			augentix,nrs = <0x00000420 0x00030000>;
		};

		pinctrl: pinctrl@80000000 {

			i2s_pins: i2s_pins@0 {
				reg = <0>;
				augentix,pmx = <
					53 0x01
					54 0x01
					55 0x01
					56 0x01
					57 0x01
					58 0x01
				>;
			};

			/*
			eintc_test_pins: eintc_test_pins@0 {
				reg = <0>;
				augentix,pmx = <
					56 0x02
					57 0x02
				>;
		    };
			*/

			/* Test for eintc_pir */
			eintc_pins: eintc_pins@0 {
				reg = <0>;
				augentix,pmx = <
					2 0x02
				>;
			};
		};

		/*
		eintc: eintc@80020020 {
			pinctrl-names = "default";
			pinctrl-0 = <&eintc_test_pins>;
			motor_v_eirq: eirq@4 {
				reg = <4>;
			};
			motor_h_eirq: eirq@5 {
				reg = <5>;
			};
		};

		eintc_test: eintc_test {
			compatible = "augentix,eirq-test";
			interrupt-parent = <&eintc>;
			interrupts = <4 5>;
			status = "okay";
		};
		*/

		eintc: eintc@80020020 {
			pinctrl-names = "default";
			pinctrl-0 = <&eintc_pins>;
			pir_eirq: eirq@0 {
				reg = <0>;
			};
		};

		eintc_pir: eintc_pir {
			compatible = "augentix,eirq-pir";
			interrupt-parent = <&eintc>;
			interrupts = <0>;
			gpio-pinctrl = <3>;
			status = "okay";
		};

		uart0: uart@88800000 {
			pinctrl-names = "default";
			pinctrl-0 = <&uart0_pins>;
			status = "okay";
		};

		usbc: usbc@90000000 {
			dr_mode = "host";
			status = "okay";
			pinctrl-names = "default";
			pinctrl-0 = <&usbvbus_pins>;
		};

		usb2_phy: usbphy@0 {
			status = "okay";
		};

		pwm: pwm@805F0000 {
			status = "okay";
		};

		sdc: sdc@88200000 {
			pinctrl-names = "default";
			pinctrl-0 = <&sdc0_pins>;
			status = "okay";
		};

		gmac: gmac@88300000 {
			/* local-mac-address = [00 55 7B B5 7D F7]; */
			max-speed = <100>;
			phy-mode = "rmii";  /*rmii or rgmii*/
			snps,phy-addr = <1>; /*RGMII: 0 , RMII: 1*/
			snps,pbl = <32>;
			snps,fixed-burst;
			snps,force_sf_dma_mode;
			snps,reset-gpio = <&pinctrl 9 0>;
			snps,reset-active-low;
			snps,reset-delays-us = <0 10000 10000000>;
			pinctrl-names = "default";
			pinctrl-0 = <&gmac_pins>;
			status = "okay";
		};

		qspi: qspi@88400000 {
			pinctrl-names = "default";
			pinctrl-0 = <&qspi0_pins>;
			status = "okay";
		};

		i2cm0: auge-i2c@80520000 {
			pinctrl-names = "default";
			pinctrl-0 = <&i2cm0_pins>;
			status = "okay";

			ak4637: ak4637@12 {
				status = "okay";
			};
			cjc8990: cjc8990@1b {
				status = "okay";
			};
			rt5660: rt5660@1c {
				status = "okay";
			};
			wm8731: wm8731@1a {
				status = "okay";
			};
		};

		i2cm1: auge-i2c@80530000 {
			pinctrl-names = "default";
			pinctrl-0 = <&i2cm1_pins>;
			status = "okay";
		};

		senif: senif@80560000 {
			pinctrl-names = "default";
		};

		adc_ctrl: adc_ctrl@80540000 {
			channels = <1 0 0>;
			status = "okay";
		};

		adc_env: adc_env@80540000 {
			status = "okay";
		};

		pcm: pcm@80540000 {
			pinctrl-names = "default";
			pinctrl-0 = <&i2s_pins>;
			status = "okay";
		};

		aenc: aenc@80542000 {
			status = "okay";
		};

		adec: adec@80552000 {
			status = "okay";
		};

		adc: adc@80544000{
			status = "okay";
		};

		enc: enc@80400000 {
			rd-buf-size = <0x100000>;
			status = "okay";
		};

		hc18xx-adc {
			status = "okay";
		};

		hc18xx-ak4637 {
			status = "okay";
		};

		hc18xx-cjc8990 {
			status = "okay";
		};

		hc18xx-dummy {
			augentix,i2s-clk-src = <I2S_CBM_CFM>;
			status = "okay";
		};

		hc18xx-rt5660 {
			status = "okay";
		};

		hc18xx-wm8731 {
			status = "okay";
		};

	};
};
