#include <string.h>

#include "address_map.h"
#include "checksum.h"
#include "delay.h"

#include "nvspc_define.h"
#include "nvspc_flash.h"

#include "hw_dram.h"
#include "hw_qspi.h"
#include "uart.h"
#include "printf.h"
#include "nvsp_mach.h"

extern struct uart_dev g_uart0;

enum FLAG_ERROR_NUM {
	DDR_INIT_FAIL = 10,
	NVSPC_CHECKSUM_FAIL = 15,
	FLASH_INIT_FAIL = 30,
	BURN_DONE = 1000,
} flag_error_num;

void is_burn_ok(int flag, int index)
{
	if (flag == 0)
		printf("REQ(IMG_OK:%d)\n", index);
	else
		printf("REQ(IMG_FAIL:%d)\n", index);
}
#define MAX_BUFFER 65536
#define CHECK_NUM 10

#define PLL_CLK_RATE 126000000
#define PLL_CLK_RATE_LOW 96000000
#define PLL_CLK_RATE_STANDARD 58982400

BaudRateType get_baudrate_type(int baudrate)
{
	int i, temp;
	int baudrate_type[3] = { 7875000, 6000000, 921600 };

	if (baudrate == 0) //avoid div 0
		return BAUDRATE_FAIL;

	for (i = BAUDRATE_TYPE_7875000; i < BAUDRATE_MAX; i++) {
		temp = baudrate_type[i] % baudrate;
		if (temp == 0)
			return i;
	}
	return BAUDRATE_TYPE_7875000;
}

int mainEntry(void)
{
	int i;
	int j;

	BootConfig *boot_config;
	uint32_t cal_checksum;
	uint32_t part_table_addr;
	uint32_t part_table_size;
	uint32_t part_table_checksum;

	uint32_t flash_type;
	uint32_t programmer_type;

	uint32_t img_addr;
	uint32_t *image;

	uint32_t flag = 0;

#if 1 //original FSBL process
	BaudRateType baudrate_type;
	uint32_t target_baudrate = 115200;
	int err = 0;

	//Get target baudrate from UART server
	printf("REQ(GETBAUDRATE)\n", PLL_CLK_RATE);
	uart_receive(&g_uart0, 4, (uint8_t *)(&target_baudrate), 0);
	uart_transmit(&g_uart0, 1, (uint8_t *)".");
	for (i = 0; i < 300; i++)
		delay_ns_icache_on(1000000, PLL_OFF);

	baudrate_type = get_baudrate_type(target_baudrate);

	if (baudrate_type == BAUDRATE_TYPE_6000000) {
		err = nvsp_mach_nvsp_init(BAUDRATE_TYPE_6000000);
		g_uart0.ref_clk_rate = PLL_CLK_RATE_LOW;
		g_uart0.baud_rate = target_baudrate;
	} else if (baudrate_type == BAUDRATE_TYPE_921600) {
		err = nvsp_mach_nvsp_init(BAUDRATE_TYPE_921600);
		g_uart0.ref_clk_rate = PLL_CLK_RATE_STANDARD;
		g_uart0.baud_rate = target_baudrate;
	} else {
		err = nvsp_mach_nvsp_init(BAUDRATE_TYPE_7875000);
		g_uart0.ref_clk_rate = PLL_CLK_RATE;
		g_uart0.baud_rate = target_baudrate;
	}
	uart_init(&g_uart0); //re-initialization

	printf("ref_clk_rate = %d and baud_rate = %u\n", g_uart0.ref_clk_rate, g_uart0.baud_rate);

	printf("======== AUGENTIX v0.0.10 ========\n");
	printf("DDR = %s, data rate: %d MT/s\n", DDR_NAME, DDR_DATA_RATE);
	printf("DDR TYPE = %s, capacity: %d MB\n", DDR_TYPE, CONFIG_DRAM_CAPACITY);
	if (err < 0) {
		printf("[ERROR] DRAM INIT FAIL, errno: %d\n\n", err);
		printf("REQ(DONE:10)\n");
		goto Exit_NVSPC;
	} else {
		printf("DRAM INIT PASS, ret: 0x%08x\n\n", err);
	}
#endif

	printf("NVSPC start ver20210605\n");

	//partition table
	printf("REQ(PARTTBL_ADDR)\n");
	uart_receive(&g_uart0, 4, (uint8_t *)(&part_table_addr), 0);
	uart_transmit(&g_uart0, 1, (uint8_t *)".");

	printf("REQ(PARTTBL_SIZE)\n");
	uart_receive(&g_uart0, 4, (uint8_t *)(&part_table_size), 0);
	uart_transmit(&g_uart0, 1, (uint8_t *)".");

	boot_config = (BootConfig *)part_table_addr;
	printf("REQ(PARTTBL)\n");
	for (i = 0; i < part_table_size; i += UART_CHUNK_SIZE) {
		if (part_table_size - i >= UART_CHUNK_SIZE) {
			uart_receive(&g_uart0, UART_CHUNK_SIZE, (uint8_t *)(((uint32_t)boot_config) + i), 0);
		} else {
			uart_receive(&g_uart0, part_table_size - i, (uint8_t *)(((uint32_t)boot_config) + i), 0);
		}

		uart_transmit(&g_uart0, 1, (uint8_t *)".");
	}

	printf("REQ(PARTTBL_CHECKSUM)\n");
	uart_receive(&g_uart0, 4, (uint8_t *)(&part_table_checksum), 0);
	uart_transmit(&g_uart0, 1, (uint8_t *)".");

	cal_checksum = get_checksum((uint32_t *)boot_config, (part_table_size / sizeof(uint32_t)));

	if (cal_checksum != part_table_checksum) {
		printf("partition table check is error!get=0x%x cal=0x%x\n", part_table_checksum, cal_checksum);
		printf("Please terminate and try again!\n");
		goto Exit_NVSPC;
	}

	printf("REQ(IMG_ADDR)\n");
	uart_receive(&g_uart0, 4, (uint8_t *)(&img_addr), 0);
	uart_transmit(&g_uart0, 1, (uint8_t *)".");
	image = (uint32_t *)img_addr;

	//To check flash programmer type and init it.
	printf("REQ(PROG_MODE)\n");
	uart_receive(&g_uart0, 4, (uint8_t *)(&programmer_type), 0);
	uart_transmit(&g_uart0, 1, (uint8_t *)".");
	printf("programmer_type type=%d\n", programmer_type);

	//To check flash type and init it.
	printf("REQ(NVS_TYPE)\n");
	uart_receive(&g_uart0, 4, (uint8_t *)(&flash_type), 0);
	uart_transmit(&g_uart0, 1, (uint8_t *)".");

	flag = init_flash(flash_type);
	if (flag != ESUCCESS) {
		flag = FLASH_INIT_FAIL;
		goto Exit_NVSPC;
	}

	get_quad_mode_detection(flash_type);

	//Parsing Partition Table
	printf("REQ(IMG:0)\n");
	if (programmer_type == 0) {
		for (j = 0; j < boot_config->pt_list[0].img_size; j += UART_CHUNK_SIZE) {
			if (boot_config->pt_list[0].img_size - j >= UART_CHUNK_SIZE) {
				uart_receive(&g_uart0, UART_CHUNK_SIZE, (uint8_t *)(((uint32_t)image) + j), 0);
			} else {
				uart_receive(&g_uart0, boot_config->pt_list[0].img_size - j,
				             (uint8_t *)(((uint32_t)image) + j), 0);
			}
			uart_transmit(&g_uart0, 1, (uint8_t *)".");
		}
	} else {
		int img_size = boot_config->pt_list[0].img_size;
		int size;
		int offset = 0;
		while (img_size > 0) {
			if (img_size >= MAX_BUFFER)
				size = MAX_BUFFER;
			size = img_size;
			uart_receive(&g_uart0, size, (uint8_t *)(((uint32_t)image) + offset), 0);
			offset += size;
			img_size -= size;
		}
	}
	//get checksum
	cal_checksum = get_checksum(image, (boot_config->pt_list[0].img_size / sizeof(uint32_t)));
	if (cal_checksum == boot_config->pt_list[0].checksum) {
		printf("boot image checksum is correct!\n");
	} else {
		printf("ERROR! SOC calcute boot image checksum is 0x%08x but 0x%08x on PC!\n", cal_checksum,
		       boot_config->pt_list[0].checksum);
		printf("REQ(IMG_FAIL:0)\n");
		goto Exit_NVSPC;
	}

#if 1
	printf("======= Partition Table =======\nnvs_type = %d part_num = %d boot_idx = %d verify = %d\n",
	       boot_config->nvs_type, boot_config->part_num, boot_config->boot_idx, boot_config->verify);
	for (i = 0; i < boot_config->part_num; i++) {
		if (boot_config->pt_list[i].nvs_size == 0)
			continue;
		printf("[Partition %d: %s]\n\tbase = 0x%08x, size = 0x%08x\n", i, boot_config->pt_list[i].pt_label,
		       boot_config->pt_list[i].nvs_start, boot_config->pt_list[i].nvs_size);
	}
	printf("===================\n");

#endif

	//write boot.bin to flash
	if (flash_type == WRITE_TO_EMMC) { //it have to write to boot parttion

	} else {
		flag = write_to_partition(boot_config->pt_list[0].nvs_start, boot_config->pt_list[0].nvs_size, img_addr,
		                          boot_config->pt_list[0].img_size);

		is_burn_ok(flag, 0);
		if (flag != ESUCCESS)
			goto Exit_NVSPC;
	}

	//read image i=2->i=part_num from image table
	for (i = 1; i <= boot_config->part_num; i++) {
		if (i == boot_config->part_num) {
			flag = BURN_DONE;
			break;
		}
		if (boot_config->pt_list[i].checksum == 0) { //no image bin
			flag = nvspc_erase_partition(boot_config->pt_list[i].nvs_start,
			                             boot_config->pt_list[i].nvs_size);

			is_burn_ok(flag, i);
			continue;
		}

		if (boot_config->pt_list[i].img_size > 0) { //this size is flag
			if (programmer_type == 0) {
				printf("REQ(IMG:%d)\n", i);
				for (j = 0; j < boot_config->pt_list[i].img_size; j += UART_CHUNK_SIZE) {
					if (boot_config->pt_list[i].img_size - j >= UART_CHUNK_SIZE) {
						uart_receive(&g_uart0, UART_CHUNK_SIZE,
						             (uint8_t *)(((uint32_t)image) + j), 0);
					} else {
						uart_receive(&g_uart0, boot_config->pt_list[i].img_size - j,
						             (uint8_t *)(((uint32_t)image) + j), 0);
					}
					uart_transmit(&g_uart0, 1, (uint8_t *)".");
				}

				//Get Checksum
				cal_checksum =
				        get_checksum(image, (boot_config->pt_list[i].img_size / sizeof(uint32_t)));

				if (cal_checksum != boot_config->pt_list[i].checksum) {
					printf("\nChecksum error\n");
					printf("ERROR! SOC calcute checksum is 0x%08x but 0x%08x on PC!\n",
					       cal_checksum, boot_config->pt_list[i].checksum);
					printf("Burn fail via checksum, please run nvsp program again\n");
					printf("REQ(IMG_FAIL:%d)\n", i);
					break;
				}
			} else {
				int retry = CHECK_NUM;
				while (retry > 0) {
					int img_size = boot_config->pt_list[i].img_size;
					int size;
					int offset = 0;

					retry--;
					printf("REQ(IMG:%d)\n", i);
					while (img_size > 0) {
						if (img_size >= MAX_BUFFER)
							size = MAX_BUFFER;
						size = img_size;
						uart_receive(&g_uart0, size, (uint8_t *)(((uint32_t)image) + offset),
						             0);
						offset += size;
						img_size -= size;
					}
					cal_checksum = get_checksum(
					        image, (boot_config->pt_list[i].img_size / sizeof(uint32_t)));
					if (cal_checksum == boot_config->pt_list[i].checksum) {
						retry = -CHECK_NUM;
					}
				}
				if (retry != -CHECK_NUM) {
					printf("ERROR! SOC calcute checksum is 0x%08x but 0x%08x on PC!\n",
					       cal_checksum, boot_config->pt_list[i].checksum);
					printf("REQ(IMG_FAIL:%d)\n", i);
					break;
				}
			}
			//write image table to flash
			flag = write_to_partition(boot_config->pt_list[i].nvs_start, boot_config->pt_list[i].nvs_size,
			                          img_addr, boot_config->pt_list[i].img_size);
			is_burn_ok(flag, i);

			//find repeat image to partition
			for (j = i + 1; j < boot_config->part_num; j++) {
				if (boot_config->pt_list[j].checksum == boot_config->pt_list[i].checksum) {
					printf("REQ(IMG_RE:%d)\n", j);
					//write image table to flash
					flag = write_to_partition(boot_config->pt_list[j].nvs_start,
					                          boot_config->pt_list[j].nvs_size, img_addr,
					                          boot_config->pt_list[j].img_size);

					is_burn_ok(flag, j);
					boot_config->pt_list[j].img_size = 0;
				}
			}
		}
	}

Exit_NVSPC:
	exit_flash_program();

	if (flag == BURN_DONE) {
		printf("\nCongratulations!!! NVPS Program burn success!\n\n");
	}

	printf("REQ(DONE:%d)\n", flag);
	while (1) {
	}
	return 0;
}
