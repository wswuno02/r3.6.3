#ifndef NVSPC_FLASH_DEFINE_H_
#define NVSPC_FLASH_DEFINE_H_

int init_flash(uint32_t flash_type);
int nvspc_erase_partition(uint32_t part_base, uint32_t part_size);
int read_from_partition(uint32_t nvs_start, uint32_t img_addr, uint32_t img_size);
int get_quad_mode_detection(int flash_type);
int write_to_partition(uint32_t part_base, uint32_t part_size, uint32_t mem_base, uint32_t img_size);

#endif // NVSPC_FLASH_DEFINE_H_
