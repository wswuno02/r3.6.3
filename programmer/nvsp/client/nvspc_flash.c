#include "address_map.h"

#include "csr_bank_qspi.h"
#include "csr_bank_qspir.h"
#include "csr_bank_qspiw.h"
#include "hw_qspi.h"
#include "printf.h"
#include "uart.h"

#include "nvspc_define.h"

struct qspi_dev g_qspi = { .base_addr = QSPI_BASE,
	                   .ref_clk_rate = QSPI_REF_CLK_187500KHZ,
	                   .qspi_clk_rate = QSPI_IF_CLK_46875KHZ };

#define QSPI_PAGE_BASE 0

#include "hw_sdc.h"

#include "csr_bank_rst.h"
#include "csr_bank_sdc.h"
#include "csr_bank_sdc_cfg.h"

#if 0
struct sdc_dev g_sdc0 = { .sdc_base_addr = SDC0_BASE,
	                  .sdc_cfg_base_addr = SDC_CFG0_BASE,
	                  .sdc_emmc_clk_rate = 0,
	                  .rst_base_addr = RST_BASE,
	                  .ref_clk_rate = SDC_REF_CLK_50MHZ };
#endif
static uint32_t user_flash_type = 0; //when we init it, it can't modify
static uint32_t nor_flash_addr_mode = NOR_3BYTE_ADDR_MODE;

int init_flash(uint32_t flash_type)
{
	int status = 0, ret;

	if (flash_type == WRITE_TO_NAND) {
		ret = qspi_init(&g_qspi);
		printf("qspi_init=%d\n", ret);
		status |= ret;
		ret = nand_reset(&g_qspi);
		printf("nand_reset=%d\n", ret);
		status |= ret;
		ret = nand_init(&g_qspi);
		printf("nand_init=%d\n", ret);
		status |= ret;
	} else if (flash_type == WRITE_TO_NOR) {
		ret = qspi_init(&g_qspi);
		printf("qspi_init=%d\n", ret);
		status |= ret;

		ret = nor_reset(&g_qspi);
		printf("nor_reset=%d\n", ret);
		status |= ret;

		//ret = nor_init(&g_qspi); Dream: after discussion, don't to init 2021.05.14
		//printf("nor_init=%d\n", ret);

		if (nor_is_support_4byte_mode(&g_qspi)) {
			nor_enter_4byte_mode(&g_qspi);
			nor_flash_addr_mode = NOR_4BYTE_ADDR_MODE;
			printf("init NOR with 4byte addr\n");
		} else {
			nor_flash_addr_mode = NOR_3BYTE_ADDR_MODE;
			printf("init NOR with 3byte addr\n");
		}
		user_flash_type = WRITE_TO_NOR;
	} else if (flash_type == WRITE_TO_EMMC) {
		//hw_sdc_mmc_boot_operation_init(&g_sdc0);
		//printf("init eMMC\n");
	}
	user_flash_type = flash_type;

	return status;
}

int nvspc_erase_partition(uint32_t part_base, uint32_t part_size)
{
	uint32_t status = ESUCCESS;

	if (user_flash_type == WRITE_TO_NAND) {
		uint32_t block_idx = (uint32_t)nand_addr_to_block(part_base, BYTES_OFFSET_2048, PAGES_OFFSET_64);
		uint32_t block_num = (uint32_t)nand_addr_to_block(part_size, BYTES_OFFSET_2048, PAGES_OFFSET_64);

		printf("Prgram erase nand block_addr=%x block_num=%d\n", block_idx, block_num);
		nand_normal_erase(&g_qspi, block_idx, block_num);
	} else if (user_flash_type == WRITE_TO_NOR) {
		printf("Erase nor block =0x%x\n", part_base);
		status = nor_one_block_erase(&g_qspi, part_base, nor_flash_addr_mode);
	}

	return status;
}

int read_from_partition(uint32_t nvs_start, uint32_t img_addr, uint32_t img_size)
{
	uint32_t status = ESUCCESS;

	int load_byte_count;
	int byte_count;
	uint32_t *load_addr;
	int page = 0;
	uint32_t mem_start = img_addr;
	uint32_t size = img_size;

	printf("Load from flash base 0x%08x to DRAM 0x%08x, size = 0x%08x\n", nvs_start, mem_start, img_size);

	load_addr = (uint32_t *)mem_start;
	page = (nvs_start >> BYTES_OFFSET_2048);
	load_byte_count = size;

	while (load_byte_count > 0) {
		byte_count = (load_byte_count > NAND_FLASH_PAGE_SIZE) ? NAND_FLASH_PAGE_SIZE : load_byte_count;

		nand_normal_read(&g_qspi, load_addr, page, byte_count, QUAD_MODE);
		load_addr += byte_count / sizeof(int);
		load_byte_count -= byte_count;
		page++;
	}

	return status;
}

static int quad_dual_mode;
int get_quad_mode_detection(int flash_type)
{
	uint8_t pattern[8] = { 0 };
	uint32_t status = ESUCCESS;

	/* address 2040 to 2047 */
	if (flash_type == 0) {
		status = nand_write(&g_qspi, 2040, 8, (uint32_t *)pattern, 1, SINGLE_MODE);
		nand_page_read(&g_qspi, 0);
		nand_read(&g_qspi, 2040, 8, QUAD_MODE);
		qspi_fifo_read(&g_qspi, (uint32_t *)pattern, 8);
	} else {
		status = nor_write(&g_qspi, 2040, 8, (uint32_t *)pattern, nor_flash_addr_mode, SINGLE_MODE);
		nor_read(&g_qspi, 2040, 8, NOR_3BYTE_ADDR_MODE, QUAD_MODE);
		qspi_fifo_read(&g_qspi, (uint32_t *)pattern, 8);
	}

	if (pattern[0] == 0x6A && pattern[1] == 0x6A && pattern[2] == 0xA6 && pattern[3] == 0x6A &&
	    pattern[4] == 0x99 && pattern[5] == 0x95 && pattern[6] == 0x55 && pattern[7] == 0x59) {
		printf("Use QUAD_MODE %d\n", status);
		quad_dual_mode = QUAD_MODE;

	} else {
		printf("Use SINGLE_MODE %d\n", status);
		quad_dual_mode = SINGLE_MODE;
	}

	return status;
}

int write_to_partition(uint32_t part_base, uint32_t part_size, uint32_t mem_base, uint32_t img_size)
{
	uint32_t status = ESUCCESS;
	uint32_t i;

	if (user_flash_type == WRITE_TO_NAND) {
		printf("Write mem_address=0x%x to nand. part address=0x%x part size=0x%x, image size=0x%x\n", mem_base,
		       part_base, part_size, img_size);

		status = nand_normal_program(
		        &g_qspi, (uint32_t)nand_addr_to_block(part_base, BYTES_OFFSET_2048, PAGES_OFFSET_64),
		        (uint32_t)nand_addr_to_block(part_size, BYTES_OFFSET_2048, PAGES_OFFSET_64),
		        (uint32_t)QSPI_PAGE_BASE, (uint32_t)mem_base, (uint32_t)img_size, quad_dual_mode);

		if (status != ESUCCESS) {
			printf("[ERROR] [TEST_QSPI] nand_normal_quad_write failed\n.");
		}
	} else if (user_flash_type == WRITE_TO_NOR) {
#if Use_Section
		printf("Write mem_address=0x%x to NOR part=0x%x size=0x%x\n", mem_base, part_base, img_size);

		status = nor_normal_write_sector(&g_qspi, (uint32_t)part_base, (uint32_t)QSPI_PAGE_BASE,
		                                 (uint32_t)mem_base, (uint32_t)img_size, SECTORS_OFFSET_16,
		                                 nor_flash_addr_mode, quad_dual_mode);
#else
		printf("Erase NOR block part=0x%x size=0x%x\n", part_base, part_size);
		for (i = part_base; i < (part_base + part_size); i += (1 << BYTES_OFFSET_65536)) {
			status = nor_one_block_erase(&g_qspi, i, nor_flash_addr_mode);
			if (status != ESUCCESS) {
				printf("Erase nor fail\n");
				return status;
			}
		}

		printf("Write mem_address=0x%x to NOR block part=0x%x size=0x%x\n", mem_base, part_base, img_size);

		status = nor_normal_write_block(&g_qspi, (uint32_t)part_base, (uint32_t)QSPI_PAGE_BASE,
		                                (uint32_t)mem_base, (uint32_t)img_size, PAGES_OFFSET_256,
		                                nor_flash_addr_mode, quad_dual_mode);
#endif
		if (status != ESUCCESS) {
			printf("[ERROR] [TEST_QSPI] nor_normal_write (QUAD_MODE) failed\n.");
		}
	} else if (user_flash_type == WRITE_TO_EMMC) {
		printf("Write mem_address=0x%x to EMMC part address=0x%x size=0x%x\n", mem_base, part_base, img_size);
#if 0
		status = mmc_write(&g_sdc0, part_base, mem_base, img_size, MMC_PARTITION_USER);
		if (status != ESUCCESS) {
			printf("[ERROR] eMMC write failed. Status=%d\n", status);
		}
#endif
	}
	return status;
}

int write_to_emmc_boot_partition(uint32_t part_base, uint32_t mem_base, uint32_t size)
{
	int status = 0;
#if 0
	printf("boot area, pattion 1\n");
	status = mmc_write(&g_sdc0, 0x0000, mem_base, size, MMC_PARTITION_BOOT_1);
	if (status != ESUCCESS) {
		printf("[ERROR] eMMC write failed. Status=%d\n", status);
	}

	printf("boot area, pattion 2\n");
	status = mmc_write(&g_sdc0, 0x9000, mem_base, size, MMC_PARTITION_BOOT_2);

	if (status != ESUCCESS) {
		printf("[ERROR] eMMC write failed. Status=%d\n", status);
	}
#endif
	return status;
}

void exit_flash_program(void)
{
	if ((user_flash_type == WRITE_TO_NOR) && (nor_flash_addr_mode == NOR_4BYTE_ADDR_MODE)) {
		nor_exit_4byte_mode(&g_qspi);
	}
}
