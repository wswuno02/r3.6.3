import datetime

log_file = None

def init(filename):
    global log_file
    log_file = open(filename, 'w', newline = '')

def printlog(*args, **kwargs):
    global log_file
    print(''.join(map(str, args)), **kwargs)
    t = datetime.datetime.today()
    print('[', t, ']', ''.join(map(str, args)), **kwargs, file = log_file)

def close():
    global log_file
    log_file.close()
