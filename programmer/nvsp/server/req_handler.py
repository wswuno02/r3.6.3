import abc
import re

from define import *
from image import Image

import log
import common
import sys

uart_fsbl = None
nvspc = None

# Initialized by layout_parser
imgtbl = None
bootcfg = None

def init():
    global uart_fsbl
    global nvspc
    global imgtbl
    global bootcfg
    log.printlog("Loading NVSP client...")
    nvspc = Image(NVSPC_NAME)
    log.printlog("Loading Bootconfig...")
    bootcfg = Image(BOOTCONFIG_BIN)

# Command handling functions
def getUartFsblSize(intf, arg):
    global uart_fsbl
    return intf.sendData(uart_fsbl.size.to_bytes(4, byteorder='little'), 4)

def getUartFsblImg(intf, arg):
    global uart_fsbl
    return intf.sendImage(uart_fsbl)

def getUartFsblChecksum(intf, arg):
    global uart_fsbl
    return intf.sendData(uart_fsbl.checksum.to_bytes(4, byteorder='little'), 4)

def getUartFsblSignature(intf, arg):
    fsbl_sign = Image(FSBL_SIGN_NAME)
    return intf.sendImage(fsbl_sign)

def getUartFsblPubkey1Modulus(intf, arg):
    fsbl_pubkey1 = Image(PUBKEY1_NAME)
    return intf.sendImage(fsbl_pubkey1)

def getUartFsblPubkey2Modulus(intf, arg):
    fsbl_pubkey2 = Image(PUBKEY2_NAME)
    return intf.sendImage(fsbl_pubkey2)

def getUartFsblPubkey2ModulusSignature(intf, arg):
    fsbl_pubkey2_sign = Image(PUBKEY2_SIGN_NAME)
    return intf.sendImage(fsbl_pubkey2_sign)

def getBaudRate(intf, arg):
    log.printlog("Target PLL: ", arg, ", baudrate: ",  common.target_baudrate)
    ret = intf.sendData(common.target_baudrate.to_bytes(4, byteorder='little'), 4)
    # Set UART baudrate
    intf.setBaudrate(common.target_baudrate)
    return True

def getNvspcSize(intf, arg):
    global nvspc
    return intf.sendData(nvspc.size.to_bytes(4, byteorder='little'), 4)

def getNvspcImg(intf, arg):
    global nvspc
    return intf.sendImage(nvspc)

def getNvspcChksum(intf, arg):
    global nvspc
    return intf.sendData(nvspc.checksum.to_bytes(4, byteorder='little'), 4)

def getNvspcSignature(intf, arg):
    nvspc_sign = Image(NVSPC_SIGN_NAME)
    return intf.sendImage(nvspc_sign)

def getPartTblAddr(intf, arg):
    return intf.sendData(BOOTCONFIG_MEM_ADDR.to_bytes(4, byteorder='little'), 4)

def getPartTblSize(intf, arg):
    global bootcfg
    return intf.sendData(bootcfg.size.to_bytes(4, byteorder='little'), 4)

def getPartTblImg(intf, arg):
    global bootcfg
    return intf.sendImage(bootcfg)

def getPartTblChksum(intf, arg):
    global bootcfg
    return intf.sendData(bootcfg.checksum.to_bytes(4, byteorder='little'), 4)

def getNvsType(intf, arg):
    nvs_type = common.nvs_type
    nvs_type_val = common.nvs_type_dict[nvs_type]
    log.printlog("NVS type: ", nvs_type, "value: ", nvs_type_val)
    return intf.sendData(nvs_type_val.to_bytes(4, byteorder='little'), 4)

def getProgramMode(intf, arg):
    mode = 0
    return intf.sendData(mode.to_bytes(4, byteorder='little'), 4)

def getSysImg(intf, arg):
    imgtbl_list = common.imgtbl_list
    index = int(arg)
    img = imgtbl_list[index]
    if img == None:
        log.printlog("[Error] Image ", index, ": N/A")
        return
    else:
        log.printlog("Image ", index, ": ", img.filename, " (", img.checksum , ")")
        return intf.sendImage(img)

def getSysImgAddr(intf, arg):
    return intf.sendData(SYSIMG_MEM_ADDR.to_bytes(4, byteorder='little'), 4)

def getImgOK(intf, arg):
    return 0
	
def getImgFail(intf, arg):
	return 0
	
def getSysDone(intf, arg):
    input('Press any key to exit')
    log.close()
    sys.exit()
    return

class RequestHandler(abc.ABC):

    @abc.abstractmethod
    def parseRequest(self, msg):
        pass

    @abc.abstractmethod
    def handleRequest(self, req):
        pass

cmd_handler_dict = {

   # pass nvspc instead of uart_fsbl
   'SIZE'                      : getNvspcSize,
   'FSBL'                      : getNvspcImg,
   'FSBL_CHECKSUM'             : getNvspcChksum,

   # authentication flow
   'FSBL_SIGNATURE'            : getNvspcSignature,
   'PUBKEY1_MODULUS'           : getUartFsblPubkey1Modulus,
   'PUBKEY2_MODULUS'           : getUartFsblPubkey2Modulus,
   'PUBKEY2_MODULUS_SIGNATURE' : getUartFsblPubkey2ModulusSignature,

   # baudrate
   'GETBAUDRATE'               : getBaudRate,

   # bootconfig
   'PARTTBL_ADDR'              : getPartTblAddr,
   'PARTTBL_SIZE'              : getPartTblSize,
   'PARTTBL'                   : getPartTblImg,
   'PARTTBL_CHECKSUM'          : getPartTblChksum,

   'NVS_TYPE'                  : getNvsType,
   'PROG_MODE'                 : getProgramMode,
   'IMG'                       : getSysImg,
   'IMG_OK'                    : getImgOK,
   'IMG_FAIL'                  : getImgFail,
   'IMG_RE'                    : getImgOK,
   'IMG_ADDR'                  : getSysImgAddr,
   'DONE'                      : getSysDone
}

# REQ-ACK request parser
class TerminalRequestHandler(RequestHandler):
    __req_re = None
    global cmd_handler_dict

    def __init__(self, req_ptrn = r'REQ\((.*?)(:.*?)?\)'):
        self.__req_re = re.compile(req_ptrn)

    # Use regular expression (RE) to parse command + argument
    def parseRequest(self, msg):
        match = self.__req_re.search(msg)
        if match is None:
            if msg.find("..") == -1:
                log.printlog('[MACH LOG] ', msg, end='')
            return None
        else:
            cmd = match.group(1)
            if cmd not in cmd_handler_dict.keys():
                log.printlog('(Unsupported CMD) [MACH LOG] ', msg, end='')
                return None
            if match.group(2) is not None:
                arg = match.group(2)[1:] # Remove matched ':'
            else:
                arg = None
        return [cmd, arg]

    def handleRequest(self, intf, req):
        if req[0] in cmd_handler_dict.keys():
            log.printlog('Command: ' + req[0] + '; Optional argument: ', req[1])
            return (cmd_handler_dict[(req[0])])(intf, req[1])
        else:
            return None

