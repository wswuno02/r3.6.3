import os
import sys
import checksum
import log

class Image:

    def __init__(self, fname):
        if not os.path.exists(fname):
            log.printlog('[ERROR] image "', fname, '" does not exist!')
            sys.exit()
        self.filename = fname
        self.size = os.stat(self.filename).st_size
        self.checksum = checksum.calcFromFile(self.filename)
        log.printlog('\timage: ', self.filename, '\n\tsize:\t', self.size, ' Bytes\n\tchksum:\t', hex(self.checksum))
