import os
import sys
import abc
import serial
import time
import re

from define import *
from image import Image
import log

# Abstracted communication interface
class CommIntf(abc.ABC):

    @abc.abstractmethod
    def recvRequest(self):
        pass

    # Blocking until queued request is handled
    @abc.abstractmethod
    def sendImage(self, image):
        pass

# CI instance: TFTP server
class TftpCommIntf(CommIntf):

    def recvRequest(self):
        pass

    # Blocking until queued request is handled
    def sendImage(self, image):
        pass

class TerminalCommIntf(CommIntf):

    @abc.abstractmethod
    def sendData(self, data, size):
        pass

    @abc.abstractmethod
    def sendImage(self, image):
        pass

    @abc.abstractmethod
    def setBaudrate(self, baudrate):
        pass

# CI instance: UART
class UartTermCommIntf(TerminalCommIntf):

    def __init__(self, comm_port, baudrate = DEFAULT_UART_BAUDRATE, timeout = DEFAULT_UART_TIMEOUT):
        self.__comm_port = comm_port
        self.__baudrate = baudrate
        self.__serial = serial.Serial(self.__comm_port, self.__baudrate, write_timeout = timeout)
        self.__tout = timeout
        self.__chunk_size = UART_CHUNK_SIZE
        log.printlog("[UART] " + self.__comm_port + ", baudrate:", self.__baudrate)

    # send bytearray of given size
    def __send(self, msg, size):
        self.__serial.write(msg)

    #recv and send data
    def send(self, msg, size = 2):
        self.__send(msg, size)

    def recv(self, size = 1):
        return self.__recv(size)

    # read fixed-size data
    def __recv(self, size = 1):
        return self.__serial.read(size).decode('utf-8', 'ignore')

    # read a single line
    def __recvline(self):
        return self.__recvall('\n')

    # read until terminator is found
    def __recvall(self, term):
        buf = ''
        while term not in buf:
            if self.__serial.in_waiting:
                buf += self.__recv()                
        return buf

    def __readAck(self):
        self.__recvall(ACK) # Expect ACK; drop all read stuff
        return True

    def getBaudrate(self):
        return self.__baudrate

    def setBaudrate(self, baudrate):
        log.printlog("Baudrate change: ", self.__baudrate, " -> ", baudrate)
        self.__baudrate = baudrate
        self.__serial.close()
        self.__serial.baudrate = self.__baudrate # PySerial setter
        self.__serial.open()
        return True

    def sendData(self, data, size):
        log.printlog('\tData: ', hex(int.from_bytes(data, byteorder='little')), '\n')
        self.__send(data, size)
        return self.__readAck()

    def sendData_noAck(self, data, size):
        log.printlog('\tData: ', hex(int.from_bytes(data, byteorder='little')), '\n')
        self.__send(data, size)
        return True

    def sendImage(self, image):
        with open(image.filename, 'rb') as f:
            data_sent = 0
            log.printlog('Transfer image: ', image.filename)
            #log.printlog('\tSending image...\n');
            csize = 65536
            for cnt in range(image.size, 0, -csize):
                xfer_size = min(cnt, csize)
                chunk = f.read(xfer_size)
                self.__send(chunk, xfer_size)
                self.__readAck()
                data_sent += xfer_size                
                log.printlog('\r\tData sent: ', data_sent, '/', image.size, end='')
            log.printlog('\r\tData sent: ', data_sent, '/', image.size, '\n')

    def recvRequest(self):
        return self.__recvline()

# CI instance: stdin as dry console
class StdioTermCommIntf(TerminalCommIntf):
    __curr_req = None
    def __init__(self, comm_port, baudrate = DEFAULT_UART_BAUDRATE, timeout = DEFAULT_UART_TIMEOUT):
        self.__baudrate = baudrate
        log.printlog("[Stdio] Baudrate:", self.__baudrate)

    def __send(self, msg, size):
        log.printlog(msg)

    def __recvline(self, size = 128):
        return input("$ ") + '\n'

    # read until terminator is found
    def __recvall(self, term):
        str = input("$ ")
        while term not in str:
           str += input("$ ")
        return str

    def __readAck(self):
        log.printlog("Waiting for ACK...")
        self.__recvall(ACK) # Expect ACK; drop all read stuff
        log.printlog("Response ack-ed.")
        return True

    def getBaudrate(self):
        return self.__baudrate

    def setBaudrate(self, baudrate):
        log.printlog("Baudrate change: ", self.__baudrate, " -> ", baudrate)
        self.__baudrate = baudrate
        return True

    def sendData(self, data, size):
        log.printlog('\tData: ', hex(int.from_bytes(data, byteorder='little')), '\n')
        self.__send(data, size)
        self.__readAck()

    def sendImage(self, image):
        log.printlog('Image:' + image.filename, ', size:', image.size)
        self.__readAck()

    def recvRequest(self):
        print("Ready for receive:")
        return self.__recvline()
