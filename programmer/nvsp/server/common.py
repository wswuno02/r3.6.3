from define import *

imgtbl_list = []

nvs_type_dict = {'nand': 0, 'nor': 1, "emmc": 2}
# String; one of the key in nvs_type_dict
nvs_type = 'invalid'

target_baudrate = DEFAULT_UART_BAUDRATE
