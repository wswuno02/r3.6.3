import os
import sys
import configparser
import serial
import serial.tools.list_ports

import log

class ServerConfig:
    def __init__(self, filename):
        # Get configs from config file
        if not os.path.exists(filename):
            log.printlog('Config file "' + filename + '" not found!' )
            input('Press any key to exit')
            sys.exit()
        self.__config = configparser.ConfigParser()
        self.__config.read(filename)

    def getBaudrate(self):
        return int(self.__config['uart']['baudrate'])

    def getCommPortIndex(self):
        ports_avail = []
        port = None
        index = 0

        for p in serial.tools.list_ports.comports():
            if (p.pid) is not None:
                ports_avail.append(p.device)
                log.printlog(index , '. INFO: Find USB-to-Serial Comm Port:', ports_avail[-1])
                index = index + 1

        if ports_avail == []:
            log.printlog('Failed to find any COM port.')
            input('Press any key to exit')
            sys.exit()

        # Return if only one port is found
        if len(ports_avail) == 1:
            return ports_avail[0]

        # Otherwise, select from available ports
        while True:
            port_idx = int(input ('Please select Comm Port by index:'))
            if port_idx >= index or port_idx < 0:
                log.printlog('Invalid port index selected.')
            else:
                port = ports_avail[port_idx]
                break
        log.printlog('Selected port: ' + port)
        return port

    def isUartEnabled(self):
        return bool(int(self.__config['interface']['enable_uart']))

