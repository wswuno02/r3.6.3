import os
import sys
import configparser

from define import *
import log
import checksum
from image import Image
import common

part_num = 0
config = None

def __hexstr2byte(s):
    return int(s, 16).to_bytes(4, 'little')

def __intstr2byte(s):
    return int(s, 16).to_bytes(4, 'little')

def __int2byte(i):
    return i.to_bytes(4, 'little')

def __attach2BootImage(fout, in_fname, maxlen):
    if os.stat(in_fname).st_size > maxlen:
        log.printlog('[ERROR] File "', in_fname, '" size exceeds maximum (', maxlen, ')!')
        sys.exit()
    with open(in_fname, 'rb') as fin:
        offset = fout.tell()
        log.printlog('\twrite "', in_fname, '": offset ', offset)
        barray = fin.read(maxlen)
        fout.write(barray)
    fout.seek(offset + maxlen)

def __genNandBootImage():
    log.printlog('\nGenerating NAND bootstrap image "', BOOT_IMAGE_NAND, '"...')
    out_fname = BOOT_IMAGE_NAND
    with open(out_fname, 'wb') as fout:
        offset = 0
        __attach2BootImage(fout, BR_IMAGE[0], BR_IMAGE_MAXLEN)
        offset += 2048
        fout.seek(offset)
        __attach2BootImage(fout, BR_IMAGE[1], BR_IMAGE_MAXLEN)
        offset += 2048
        fout.seek(offset)
        __attach2BootImage(fout, BOOTCONFIG_BIN, BOOTCONFIG_MAXLEN)
        offset += 2048
        fout.seek(offset)
        __attach2BootImage(fout, FSBL_IMAGE_NAND[0], FSBL_IMAGE_MAXLEN)
        offset += 32768
        fout.seek(offset)
        __attach2BootImage(fout, FSBL_IMAGE_NAND[1], FSBL_IMAGE_MAXLEN)

def __genNorBootImage():
    log.printlog('\nGenerating NOR bootstrap image "', BOOT_IMAGE_NOR, '"...')
    out_fname = BOOT_IMAGE_NOR
    with open(out_fname, 'wb') as fout:
        offset = 0
        __attach2BootImage(fout, BR_IMAGE[0], BR_IMAGE_MAXLEN)
        offset += 2048
        fout.seek(offset)
        __attach2BootImage(fout, BR_IMAGE[1], BR_IMAGE_MAXLEN)
        offset += 2048
        fout.seek(offset)
        __attach2BootImage(fout, BOOTCONFIG_BIN, BOOTCONFIG_MAXLEN)
        offset += 2048
        fout.seek(offset)
        __attach2BootImage(fout, FSBL_IMAGE_NOR[0], FSBL_IMAGE_MAXLEN)
        offset += 32768
        fout.seek(offset)
        __attach2BootImage(fout, FSBL_IMAGE_NOR[1], FSBL_IMAGE_MAXLEN)

def __genEmmcBootImage():
    log.printlog('\nGenerating eMMC bootstrap image "', BOOT_IMAGE_EMMC[0], '", "', BOOT_IMAGE_EMMC[1], '"...')
    out_fname = BOOT_IMAGE_EMMC[0]
    with open(out_fname, 'wb') as fout:
        offset = 0
        __attach2BootImage(fout, BR_IMAGE[0], BR_IMAGE_MAXLEN)
        offset += 2048
        fout.seek(offset)
        __attach2BootImage(fout, FSBL_IMAGE_EMMC[0], FSBL_IMAGE_MAXLEN)
        offset += 24 * 1024 # Pad FSBL to 24KB
        fout.seek(offset)
        __attach2BootImage(fout, BOOTCONFIG_BIN, BOOTCONFIG_MAXLEN)
        offset += 2048
        fout.seek(offset)
        __attach2BootImage(fout, BR_IMAGE[1], BR_IMAGE_MAXLEN)
        offset += 2048
        fout.seek(offset)
        __attach2BootImage(fout, FSBL_IMAGE_EMMC[1], FSBL_IMAGE_MAXLEN)
        offset += 24 * 1024 # Pad FSBL to 24KB
        fout.seek(offset)
        __attach2BootImage(fout, BOOTCONFIG_BIN, BOOTCONFIG_MAXLEN)

# out_fname_1 only used by eMMC img gen
def generateBootstrapImage():
    global config

    # Generate partition table binary first
    generateBootconfig()
    nvs_type = common.nvs_type

    # Get NVS device type from layout file
    if (nvs_type == 'nand'):
        __genNandBootImage()
    elif (nvs_type == 'nor'):
        __genNorBootImage()
    elif (nvs_type == 'emmc'):
        __genEmmcBootImage()
    else:
        log.printlog('Illegal NVS type "', nvs_type, '"!')
        return False

def generateBootconfig():
    global config
    global part_num
    imgtbl_list = common.imgtbl_list

    log.printlog('\nGenerating Bootconfig...')
    out_fname = BOOTCONFIG_BIN
    with open(out_fname, 'wb') as f:

        common.nvs_type = config['bootconfig']['nvs_type']
        nvs_type_val = common.nvs_type_dict[common.nvs_type]
        boot_idx = config['bootconfig']['boot_idx']
        verify   = config['bootconfig']['verify']

        barray = b''
        barray += __int2byte(nvs_type_val)
        barray += __int2byte(part_num)
        barray += __intstr2byte(boot_idx)
        barray += __intstr2byte(verify)
        log.printlog('bootconfig: \n\tnvs_type = ', nvs_type_val, ' (', common.nvs_type, ')\n\tpart_num = ',
                part_num, '\n\tboot_idx = ', boot_idx, '\n\tverify = ', verify)

        for i in range(0, PT_MAX_NUM):
            section_name = 'partition_' + str(i)
            if section_name in config.sections():
                nvs_start = config[section_name]['nvs_start']
                nvs_size  = config[section_name]['nvs_size']
                pt_label  = config[section_name]['pt_label']

                barray += pt_label.ljust(PT_LABEL_MAXLEN, '\0').encode('utf-8')
                barray += __hexstr2byte(nvs_start)
                barray += __hexstr2byte(nvs_size)               

                log.printlog(section_name, ': ', pt_label, '\n\tnvs_start = ', nvs_start,
                        '\n\tnvs_size  = ', nvs_size, '\n\tpt_label  = ', pt_label)
                
                
                img_label = config[section_name]['img_label']
                barray += img_label.ljust(IMG_LABEL_MAXLEN, '\0').encode('utf-8')
                if img_label == '': # No image specified:                 
                    log.printlog('image_', i, ': N/A')
                    imgtbl_list.append(None)
                    # fill mem_start, file_name and checksum
                    barray += bytearray(3 * 4)
                else:
                    # Get memload value from partition section
                    mem_start = config[img_label]['mem_start']
                    img_filename = REL_BIN_DIR + config[img_label]['file_name']
                    log.printlog('image_', i, ': ', img_filename, '\n\t mem_start = ', mem_start)

                    img = Image(img_filename)
                    imgtbl_list.append(img)

                    barray += __hexstr2byte(mem_start)

                    img = imgtbl_list[i]
                    barray += __int2byte(img.size)
                    barray += __int2byte(img.checksum)

            else: # Partition not declared
                log.printlog(section_name, ': N/A')
                # fill memload, mem_start, file_name and checksum
                barray += bytearray(4 + 4 + PT_LABEL_MAXLEN)
        f.write(barray)

def init():
    global config

    layout_filename = LAYOUT_FILE
    if not os.path.exists(layout_filename):
        log.printlog('Layout file "' + layout_filename + '" not found!' )
        input('Press any key to exit')
        sys.exit()
    config = configparser.ConfigParser()
    config.read(layout_filename)

    # scan # of partitions
    global part_num
    part_num = 0
    for section in config.sections():
        if "partition_" in section:
            part_num += 1
