# define.py: Read-only variables

import os

# Path
REL_BIN_DIR = '..' + os.path.sep

# Server Config
CONFIG_FILE = 'setup.ini'

# Boot image naming
BOOT_IMAGE_NAND = REL_BIN_DIR + 'boot_flash.bin'
BOOT_IMAGE_NOR  = REL_BIN_DIR + 'boot_flash.bin'
BOOT_IMAGE_EMMC = [REL_BIN_DIR + 'boot_emmc.bin', REL_BIN_DIR + 'boot_emmc.bin']

# Source images
BR_IMAGE = [REL_BIN_DIR + 'br.bin', REL_BIN_DIR + 'br.bin']
BR_IMAGE_MAXLEN = 2048

FSBL_IMAGE_NAND = [REL_BIN_DIR + 'fsbl_flash.bin', REL_BIN_DIR + 'fsbl_flash.bin']
FSBL_IMAGE_NOR  = [REL_BIN_DIR + 'fsbl_flash.bin',  REL_BIN_DIR + 'fsbl_flash.bin' ]
FSBL_IMAGE_EMMC = [REL_BIN_DIR + 'fsbl_emmc.bin', REL_BIN_DIR + 'fsbl_emmc.bin']
FSBL_IMAGE_MAXLEN = 32 * 1024

# Partition Table
BOOTCONFIG_BIN =  REL_BIN_DIR + 'bootconfig.bin'
BOOTCONFIG_MAXLEN = 2048
BOOTCONFIG_MEM_ADDR = 0x00008000

# Storage layout config
LAYOUT_FILE = 'layout.ini'

# PT_LABEL_MAXLEN: Maximum length of partition label; conform to Linux MTD label length
PT_LABEL_MAXLEN = 16
IMG_LABEL_MAXLEN = 12

# PT_MAX_NUM: Maximum number of partitions
PT_MAX_NUM = 20

# FSBL
UART_FSBL_NAME = REL_BIN_DIR + 'fsbl_uart.bin'

# NVSPC
NVSPC_NAME = REL_BIN_DIR + 'nvspc.bin'
NVSPC_SIGN_NAME = REL_BIN_DIR + 'nvspc_sign.bin'

# System Image common
SYSIMG_MEM_ADDR = 0x00080000

# UART
DEFAULT_UART_BAUDRATE = 57600
DEFAULT_UART_TIMEOUT = 5
UART_CHUNK_SIZE = 32
ACK = '.'

# BootROM Cryptography
FSBL_SIGN_NAME    = REL_BIN_DIR + 'fsbl_sign.bin'
PUBKEY1_NAME      = REL_BIN_DIR + 'pubkey1_mod.bin'
PUBKEY2_NAME      = REL_BIN_DIR + 'pubkey2_mod.bin'
PUBKEY2_SIGN_NAME = REL_BIN_DIR + 'pubkey2_mod_sign.bin'

