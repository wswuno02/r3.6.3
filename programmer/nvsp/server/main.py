import os
import sys
import time
import datetime
from multiprocessing import Process
from multiprocessing import freeze_support

import serial
import serial.tools.list_ports
import threading

from define import *

from comm_intf import UartTermCommIntf
from comm_intf import StdioTermCommIntf
from comm_intf import TftpCommIntf

import req_handler
from req_handler import TerminalRequestHandler
from server_cfg_parser import ServerConfig
import layout_parser
import log
import common

flag_thread = True

def uart_starter():
    global endflag
    global intf
    tStart = time.time()
    endflag = 0
    while 1:
        intf.send([0xaa, 0x00])
        tEnd = time.time()
        if (tEnd - tStart) > 50:
            log.printlog("Timeout - No client detected. Please terminate the program.")
            os._exit(1)
            return
        if (endflag == 1):
            log.printlog("Client detected.")
            return

def serveLoop():
    global endflag
    global intf

    # Read server config from config file
    server_config = ServerConfig(CONFIG_FILE)
    common.target_baudrate = server_config.getBaudrate();

    # Initialize interfaces
    if server_config.isUartEnabled() is True:
        log.printlog("Default baudrate:", DEFAULT_UART_BAUDRATE)
        intf = UartTermCommIntf(server_config.getCommPortIndex(), DEFAULT_UART_BAUDRATE, DEFAULT_UART_TIMEOUT)
        reqhandler = TerminalRequestHandler()

        log.printlog("\nReady to handle request and send adv boot:")

        if (flag_thread == True):
            th = threading.Thread(target = uart_starter)
            th.start()
            while 1:
                ser_input = intf.recv()
                if(ser_input == '$'):
                    endflag = 1
                    break
    else:
        intf = StdioTermCommIntf('stdio')
        reqhandler = TerminalRequestHandler()

    while True:
        msg = intf.recvRequest()
        req = reqhandler.parseRequest(msg)
        if req:
            result = reqhandler.handleRequest(intf, req)


def main():
    log.printlog("\n===== Augentix NVSP Server V1.0.2=====\n")

    # BIG module
    layout_parser.init()
    layout_parser.generateBootconfig()

    # UART boot FSBL image initializer
    req_handler.init()

    # NVSP service
    try:
        serveLoop()
    except KeyboardInterrupt:
        log.printlog("Shutting server ...")

# Main function entry
if __name__ == "__main__":
    freeze_support()

    t = datetime.datetime.now()
    log_filename = str(t.year) + str(t.month) + str(t.day) + str(t.now().hour) + str(t.now().minute) + ".log"
    log.init(log_filename)

    main()
    log.close()
