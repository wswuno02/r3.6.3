# UART PROGRAM FLASH

This program is achieving program SoC flash via UART.
It includes 2 parts: host part, target part.

## Requirement

- Host
	- A computer can execute .exe file

- Target
	- Augentix HC18XX series with boot mode = UART boot

- File Tree
	- Host
		- uart_host.py
		- info_parser.py
		- bin/configure.ini (section must have file_name and addr options)
			- file_name: what file to be transmit
			- addr: where in DRAM to be download
		- bin/setup.ini (setup com_port)
		- bin/*.bin (all the binary files*1 in the configure.ini file)
		- bin/uart_fsbl.bin
		- bin/flash_prog.bin

_*_1 bootcfgs.bin, br.bin, fsbl_s1.bin, fsbl_s2.bin, hc18xx-em1.dtb.bin, rootfs.cpio.uboot.bin, u-boot.bin, uImage.bin

## How To

* Check List
	1. Make sure flash is ready on the target board
	2. Make sure UART device is connecting host and target.
	3. Make sure UART port is not occupied.

* Procedure
	1. Modify com_port and baudrate to your own port number in setup.ini
		* example: com_port = COM3
		*          baudrate = 921600*2
	2. Execute uart_host.exe
	3. Wait for program ready ("UART: Please power-on target." shows up)
	4. Power-on target board
	5. Wait for program finish
_*_2 Please set baudrate in the following list: 921600, 104100, 118900, 138800, 1665000, 2082000, 2776000, 4164000
