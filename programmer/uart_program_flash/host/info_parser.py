#!/usr/bin/python
"""
Uart program flash program
Author: Henry Liu
Augentix 2014-2018
"""
import os
import sys
import datetime
import configparser
# For checking os platform 
import platform

# define #
if platform.system()=='Linux':
	folder_symbol = '/'
elif platform.system()=='Darwin':
	folder_symbol = '/'
elif platform.system()=='Windows':
	folder_symbol = '\\'

cur_folder_path = os.path.split(os.path.abspath(sys.argv[0]))[0]
bin_folder_path = os.path.split(cur_folder_path)[0] + folder_symbol

def calcChecksum(name, size):
	inverse = 0xB139D2E5
	checksum = 0

	f = open(bin_folder_path + name, "rb")

	for i in range(0, int(size / 4), 1):
		checksum = ((checksum >> 1) | (checksum << 31)) & 0xFFFFFFFF
		checksum = checksum ^ (int.from_bytes(f.read(4), byteorder='little'))
		checksum = checksum ^ inverse
		checksum = checksum & 0xFFFFFFFF

	f.close()
	return checksum

def calcChecksum2(barray):
	inverse = 0xB139D2E5
	checksum = 0
	for i in range(0, int(len(barray)), 4):
		checksum = ((checksum >> 1) | (checksum << 31)) & 0xFFFFFFFF
		checksum = checksum ^ (int.from_bytes(barray[i:i + 4], byteorder='little'))
		checksum = checksum ^ inverse
		checksum = checksum & 0xFFFFFFFF
	return checksum

class BinaryFile:
	def __init__(self, name, addr, size, crc):
		self.name = name
		self.addr = addr
		self.size = size
		self.crc = crc
	def getName(self):
		return self.name
	def getAddr(self):
		return self.addr
	def getSize(self):
		return self.size
	def getCrc(self):
		return self.crc

class BinaryFileTable:
	def __init__(self, file_list):
		self.file_list = file_list
	def addTable(self, path):
		if (not self.checkTable(path)):
			self.completeFile(path)
		self.fillTable(path)
	def checkTable(self, path):
		ret = True
		config = configparser.ConfigParser()
		config.read(path)
		config.sections()
		for section in config.sections():
			if (config.has_option(section, 'file_name')):
				file = bin_folder_path + config[section]['file_name']
				if (not os.path.exists(file)):
					print('INFO: file %s does not exist' % file)
					exit()
			else:
				print('file_name section does not exist')
				exit()
			if (not config.has_option(section, 'addr')):
				print('%s does not have a mapping address' % config[section]['file_name'])
				exit()
			if (config.has_option(section, 'stamp')):
				if (config[section]['stamp'] != str(os.path.getmtime(file))):
					ret = False
			else:
				ret = False
		return ret
	def fillTable(self, path):
		config = configparser.ConfigParser()
		config.read(path)
		config.sections()
		for section in config.sections():
			self.add(config[section]["file_name"], config[section]["addr"], config[section]["size"], config[section]["crc"])
	def completeFile(self, path):
		print('INFO: completing ini file')
		config = configparser.ConfigParser()
		config.read(path)
		config.sections()
		for section in config.sections():
			name = config[section]["file_name"]
			size = os.stat(bin_folder_path + name).st_size
			crc = calcChecksum(name, size)
			config[section]['size'] = hex(size)
			config[section]['crc'] = hex(crc)
			config[section]['stamp'] = str(os.path.getmtime(bin_folder_path + name))
		time = datetime.datetime.now()
		with open(path, 'w') as configfile:
			config.write(configfile)
	def add(self, name, addr, size, crc):
		self.file_list.append(BinaryFile(name, addr, size, crc))
	def toBinary(self):
		x = bytearray()
		x += len(self.file_list).to_bytes(4, byteorder='little')
		for i in range(0, len(self.file_list)):
			x += int(self.file_list[i].getAddr(), 16).to_bytes(4, byteorder='little')
			x += int(self.file_list[i].getSize(), 16).to_bytes(4, byteorder='little')
			x += int(self.file_list[i].getCrc(), 16).to_bytes(4, byteorder='little')
		x += int(hex(calcChecksum2(x))[2:], 16).to_bytes(4, byteorder='little')
		return x
	def debugPrint(self):
		print("partition number = ", len(self.file_list))
		for i in range(0, len(self.file_list)):
			print(str(i))
			print("name = " + self.file_list[i].getName())
			print("addr = " + self.file_list[i].getAddr())
			print("size = " + str(self.file_list[i].getSize()))
			print("crc = " + str(self.file_list[i].getCrc()))
