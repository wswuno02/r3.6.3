#!/usr/bin/python
"""
Uart program flash program
Author: Henry Liu
Augentix 2014-2018
"""
import serial
import os
import datetime
import re
import configparser
import tempfile
import shutil
import sys
import signal
import serial.tools.list_ports
import time
import tftpy
from multiprocessing import Process
from multiprocessing import freeze_support
# For opening log file folder
import webbrowser

# For checking os platform 
import platform
# For checking if flashprog.img exists
from pathlib import Path
#from info_parser import BinaryFile
from info_parser import BinaryFileTable
from info_parser import calcChecksum

# define #
#COM_PORT = 'COM24'
#BAUD_RATE = 921600
FSBL_NAME = 'uart_fsbl.bin'
CONFIGURE_FILE = 'configure.ini'
SETUP_FILE = 'setup.ini'
TIME = datetime.datetime.now()

# define #
if platform.system()=='Linux':
	folder_symbol = '/'
elif platform.system()=='Darwin':
	folder_symbol = '/'
elif platform.system()=='Windows':
	folder_symbol = '\\'
# Variable Initial #
cur_folder_path = os.path.split(os.path.abspath(sys.argv[0]))[0]
bin_folder_path = os.path.split(cur_folder_path)[0] + folder_symbol
cur_folder_path += folder_symbol
tmp_folder_path = tempfile.gettempdir() + folder_symbol + 'uart_prog' + folder_symbol
if (not os.path.exists(tmp_folder_path)):
	os.mkdir(tmp_folder_path)
log_folder_path = tmp_folder_path + 'log' + folder_symbol
if (not os.path.exists(log_folder_path)):
	os.mkdir(log_folder_path)
log_file = open(log_folder_path + str(TIME.year) + str(TIME.month) + str(TIME.day)  + str(TIME.now().hour) + str(TIME.now().minute) + ".log", "w", newline="")
fp = open(bin_folder_path + FSBL_NAME, "rb")
#ser = serial.Serial(COM_PORT, 115200, timeout=None)
size = os.fstat(fp.fileno()).st_size
crc = bytearray(int(hex(calcChecksum(FSBL_NAME, size)), 16).to_bytes(4, byteorder='little'))
curr_stat = 0

# Functions #
def tftp_proc():
	# Disable tftpy messages
	sys.stdout = open(os.devnull, 'w')
	sys.stderr = open(os.devnull, 'w')
	server = tftpy.TftpServer('../')
	server.listen('0.0.0.0', listenport=69, timeout=20)


# Read an opened file by chunk
def read_chunk(fobj, size = (128 * 1024)):
	while True:
		chunk = fobj.read(size)
		if not chunk:
			break
		yield chunk

def transmit_bin(i):
	global bft
	cnt = 0
	intv = (128 * 1024) # Write interval = 128K
	name = bft.file_list[i].getName()
	print('Transmitting file %d/%d: %s, size=%d B' % ((i + 1), len(bft.file_list), name, size))
	for chunk in read_chunk(fp):
		ser.write(chunk)
		cnt += len(chunk)
		print('Progress: %d/%d' % (cnt, size) ,end='\r')
		ser.flush()
	print('')

def transmit_conf():
	global conf
	ser.write(conf)

def print_log(s):
	print(s)
	print(s, file=log_file)

# sends only the exact characters to stdout without an implicit newline
# Added for U-Boot log
def print_log_exact(s):
	sys.stdout.write(s)
	print(s, end = '', file=log_file)

def getDivisor(clk, baudrate):
	return int(clk / 16 / int(baudrate))

def uart_tranceiver():
	global conf
	global size
	global bft
	global fp
	global crc
	
	while 1:
		input = ser.readline()
		input = str(input.rstrip())[2:-1]
		print_log(input)
		# The first token 'REQ(SIZE)' may contain redundant strings,
		# so read the 'REQ(SIZE)' substring instead
		if ('REQ(SIZE)' in input):
			ser.write(int(size).to_bytes(4, byteorder='little'))
			ser.read() #read the ACK '.'
		if (input == 'REQ(FSBL)'):
			for cnt in range(int(size), 0, -32):
				if (cnt < 32):
					packet = fp.read(cnt)
				else:
					packet = fp.read(32)
				ser.write(bytearray(packet))
				ser.read()
			fp.seek(0)
		if (input == 'REQ(FSBL_CHECKSUM)'):
			ser.write(bytearray(crc))
		if ('REQ(BAUDRATE)' in input):
			if (len(input.split('-')) > 1):
				BAUDRATE_DIVISOR = getDivisor(int(input.split('-')[1]), BAUD_RATE)
			else:
				BAUDRATE_DIVISOR = getDivisor(133250000, BAUD_RATE)
			ser.write(BAUDRATE_DIVISOR.to_bytes(4, byteorder='little'))
			print("BAUDRATE_DIVISOR = ", BAUDRATE_DIVISOR)
		if (input == 'REQ(CONFSZ)'):
			ser.write(int(len(conf)).to_bytes(4, byteorder='little'))
		if (input == 'REQ(CONF)'):
			transmit_conf()
		if (re.match("REQ\([0-9]+\)", input)):
			fp.close()
			i = int(input[4:-1])
			if (i >= len(bft.file_list)):
				print("Error: File number " + str(i) + " > " + str(len(bft.file_list)))
				continue
			fp = open(bin_folder_path + bft.file_list[i].getName(), "rb")
			size = int(bft.file_list[i].getSize(), 16)
			crc = int(bft.file_list[i].getCrc(), 16).to_bytes(4, byteorder='little')
			transmit_bin(i)
		if (input == 'REQ(UBOOTNUM)'):
			# Find u-boot.bin's partition table index
			for i, file in enumerate(bft.file_list):
				name = file.getName()
				if (name == 'u-boot.bin'):
					break;
			if (i >= len(bft.file_list)):
				print("Error: File number " + str(i) + " > " + str(len(bft.file_list)))
				continue
			ser.write(int(i).to_bytes(4, byteorder='little'))
		if (input == 'REQ(MODE)'):
			# Mode used in uart_fsbl.c to indicate
			# 0: original "Augentix Flash Programmer"
			# 1: U-Boot Flash Programming
			ser.write(int(FLASH_MODE).to_bytes(4, byteorder='little'))
		if (input == 'Set PLL'):
			return True
		if (input == '[ERROR] Fail booting from UART.'):
			return False
		if (input == 'Done'):
			return True


def uart_uboot_tranceiver():
	global conf
	global size
	global bft
	global fp
	global crc
	uboot_cmds = ['setenv ipaddr ' + IPADDR, 'setenv netmask ' + NETMASK ,
	              'setenv gatewayip ' + GATEWAYIP, 'setenv serverip ' + SERVERIP,
	              'tftpboot ' + SCR_LOAD_ADDR  + ' uart_prog/flashprog.img; source ' + SCR_LOAD_ADDR]
	timeout = 0
	ser.timeout=1

	# Simulate keypress ctrl+c to stop U-Boot countdown
	ser.write('\x03'.encode('ascii'))
	while True:
		input = ser.readline()
		input = str(input.rstrip())[2:-1]
		print_log(input)
		if 'U-Boot>' in input:
			ser.write('\x03'.encode('ascii'))
			break

	# Execute U-Boot commands one-by-one
	for command in uboot_cmds:
		while 'U-Boot>' not in input:
			input = ser.readline()
			input = str(input.rstrip())[2:-1]
			print_log(input)
		command+='\n'
		ser.write(command.encode('ascii'))
		input = ' '

	timeout = time.time() + 20
	while 'Augentix U-Boot Whole Flash Programming' not in input:
		input = ser.readline()
		input = str(input.rstrip())[2:-1]
		print_log(input)
		if (time.time() > timeout):
			print_log ('Wait for U-Boot script start timeout!')
			return False

	# Keep on printing UART messages until flash programming terminates
	# 'U-Boot>': Just in case script terminates unexpectedly
	exit_keyword_arr = ['U-Boot>']
	while True:
		input = ser.readline()
		input = input.decode('ascii')
		print_log_exact(input)
		# Check if flash programming has ended
		if any(keyword in input for keyword in exit_keyword_arr):
			break

	return True

def uboot_init(config):
	global IPADDR
	global NETMASK
	global GATEWAYIP
	global SERVERIP
	global SCR_LOAD_ADDR
	global TFTP_SERVER_OPT
	global TFTP_SERVER_PROC

	# IP settings of development board ==============================
	if (config.has_option('uboot', 'ipaddr')):
		IPADDR = config['uboot']['ipaddr']
	else:
		print('INFO: Failed to get ipaddr from setup.ini')
		input("Press any key to exit")
		sys.exit()

	if (config.has_option('uboot', 'netmask')):
		NETMASK = config['uboot']['netmask']
	else:
		print('INFO: Failed to get netmask from setup.ini')
		input("Press any key to exit")
		sys.exit()

	if (config.has_option('uboot', 'gatewayip')):
		GATEWAYIP = config['uboot']['gatewayip']
	else:
		print('INFO: Failed to get gatewayip from setup.ini')
		input("Press any key to exit")
		sys.exit()

	# IP of PC containing flash programming files in TFTP server ====
	if (config.has_option('uboot', 'serverip')):
		SERVERIP = config['uboot']['serverip']
	else:
		print('INFO: Failed to get serverip from setup.ini')
		input("Press any key to exit")
		sys.exit()

	# U-Boot flash programming load dram address =====================
	if (config.has_option('uboot', 'scr_load_addr')):
		SCR_LOAD_ADDR = config['uboot']['scr_load_addr']
	else:
		print('INFO: Failed to get scr_load_addr from setup.ini')
		input("Press any key to exit")
		sys.exit()

	# U-Boot flash programming load dram address =====================
	if (config.has_option('uboot', 'tftp_server')):
		TFTP_SERVER_OPT = int(config['uboot']['tftp_server'])
	else:
		print('INFO: Failed to get tftp_server from setup.ini')
		input("Press any key to exit")
		sys.exit()
	#print(IPADDR + ' ' + NETMASK + ' ' + SERVERIP + ' ' + SCR_LOAD_ADDR)

	if (TFTP_SERVER_OPT == 0):
		# Open internal tftp server
		TFTP_SERVER_PROC = Process(target=tftp_proc, args=())
		TFTP_SERVER_PROC.start()

def init():
	global COM_PORT
	global BAUD_RATE
	global ser
	global FLASH_MODE

	COM_LIST = []
	COM_PORT = []
	cnt = 0
	port_find = 0
	FLASH_MODE = -1

	if (not os.path.exists(cur_folder_path + SETUP_FILE)):
		print('INFO: setup.ini not found.')
		input("Press any key to exit")
		sys.exit()
	if (not os.path.exists(cur_folder_path + CONFIGURE_FILE)):
		print('INFO: configure.ini not found.')
		input("Press any key to exit")
		sys.exit()

	config = configparser.ConfigParser()
	config.read(cur_folder_path + SETUP_FILE)
	ports = serial.tools.list_ports.comports(include_links=False)
	for port in ports :
		if (port.pid) is not None:
			COM_LIST.append(port)
			COM_LIST[cnt] = port.device
			COM_PORT = COM_LIST[cnt]
			print('INFO: Find USB-to-Serial Comm Port [', cnt, ']:', COM_PORT)
			cnt = cnt + 1
	if COM_PORT == []:
		print('Please connect the Comm Port...')
		input("Press any key to exit")
		sys.exit()
	while cnt > 1:
		idx_str = input ('Please choose the Comm Port index: ')
		try:
			idx = int(idx_str)
			if idx < cnt:
				COM_PORT = COM_LIST[idx]
				print("INFO: Comm port", COM_PORT, "selected.")
				break
			else:
				print("WARN: Index", idx, "out of range.")
		except ValueError:
			print("WARN: Unkwon index ", idx_str)
	if (config.has_option('setup', 'baudrate')):
		BAUD_RATE = config['setup']['baudrate']
	try:
		ser = serial.Serial(COM_PORT, 115200, write_timeout=None)
	except:
		print('INFO: Failed to open Comm Port:',COM_PORT)
		input("Press any key to exit")
		sys.exit()

	# Dealing with FLASH_MODE (Flash programming mode)

	# If flashprog.img doesn't exist, don't allow user to choose programming mode
	# Used to block early access to U-Boot flash programming in sdk_release
	flashprog_img = Path("./flashprog.img")
	if not flashprog_img.is_file():
		FLASH_MODE = 0
		return

	# Check if flash_mode option is in setup.ini
	if (config.has_option('setup', 'flash_mode')):
		FLASH_MODE = int(config['setup']['flash_mode'])
	else:
		print('WARN: flash_mode not found in setup.ini, assuming FLASH_MODE = 0')
		FLASH_MODE = 0

	while (FLASH_MODE != 0 and FLASH_MODE != 1):
		print('')
		print('Input flash programming mode:')
		print('0: Augentix Flash Programmer')
		print('1: U-Boot Flash Programming')
		FLASH_MODE = int(input (':'))
		print(FLASH_MODE)
		print('')

	# U-Boot flash programming related init
	if (FLASH_MODE == 1):
		uboot_init(config)
	else:
		return


def checkConfigureFile():
	configOrigin = configparser.ConfigParser()
	configTemp = configparser.ConfigParser()
	configOrigin.read(cur_folder_path + CONFIGURE_FILE)
	configTemp.read(tmp_folder_path + CONFIGURE_FILE)
	if (len(configOrigin.sections()) != len(configTemp.sections())):
		return False
	for section in configOrigin.sections():
		try:
			if ((configOrigin[section]["file_name"] != configTemp[section]["file_name"]) or configOrigin[section]["addr"] != configTemp[section]["addr"]):
				return False
		except KeyError:
			return False
	return True

def signal_handler(sig, frame):
	sys.exit()

def post_process():
	# If it is u-boot Flash programming and built-in TFTP server mode
	# Kill the process before exit
	if (FLASH_MODE == 1 and TFTP_SERVER_OPT == 0):
		TFTP_SERVER_PROC.kill()

def main():
	global ser
	global fp
	global conf
	global bft
	global COM_PORT

	signal.signal(signal.SIGINT, signal_handler)

	# Step 0: parse configure file
	print("UART: Initializing program...")
	init()
	bft = BinaryFileTable([])
	if ((not os.path.exists(tmp_folder_path + CONFIGURE_FILE)) or (checkConfigureFile() == False)):
		shutil.copy2(cur_folder_path + CONFIGURE_FILE, tmp_folder_path + CONFIGURE_FILE)
	bft.addTable(tmp_folder_path + CONFIGURE_FILE)
	conf = bft.toBinary()
	print("UART: Initialization finished.")
	print("INFO: Log file: " + log_file.name)
	print("UART: Please power-on target.")

	# Step 1: transmit FSBL to EVB
	if (uart_tranceiver() == False):
		return False

	ser.flushInput()
	ser.flushOutput()
	ser.baudrate = BAUD_RATE

	# Step one's FSBL download and execution time is excluded
	# -> To exclude the time variable of user "power-on target"
	start_time = time.time()

	# Step 2: Fufill FSBL's requests (Augentix Flash Programmer is finalized here)
	if (uart_tranceiver() == False):
		return False

	# Step 3: Only for U-Boot flash programming
	if (FLASH_MODE == 1):
		print_log("Jump to U-Boot")
		# U-Boot's baud rate may differ with UART flash programming setup.ini's setting
		ser.flushInput()
		ser.flushOutput()
		ser.baudrate = 115200
		ret = uart_uboot_tranceiver()
		if (ret == False):
			return False


	end_time = time.time()

	print_log('\nFlash programming took %.2f seconds' % (end_time - start_time))

	log_file.close()
	ser.close()

	# Print log file name again
	print("INFO: Log file: " + log_file.name)
	# Check log file count (If too many show message for user)
	file_count = len([1 for x in list(os.scandir(log_folder_path)) if x.is_file()])
	if (file_count > 5000):
		print('INFO: Log file count ' + str(file_count) + ' opening log folder path')
		print('       (Manually delete old logs if necessary)')
		webbrowser.open('file:///' + log_folder_path)

if __name__ == "__main__":
	freeze_support() # Needed for multiprocess to work for pyInstaller
	main()
	post_process()
	input("Press any key to exit")
