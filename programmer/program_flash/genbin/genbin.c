/******************************************************************************
*
* Copyright (c) Augentix Inc. - All Rights Reserved
*
* Unauthorized copying of this file, via any medium is strictly prohibited.
*
* Proprietary and confidential.
*
******************************************************************************/

#include "genbin.h"
#include "boot_flash.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include <unistd.h>

#include "iniparser.h"
#include "dictionary.h"

/*
 * struct check_flag_t - check flag
 * @check_binpath:  check manual entry binary file path or not.
 * @check_ini:      check manual entry ini file name or not.
 * @check_para_ini: check manual entry parameter ini file name or not.
 */
struct check_flag_t {
	int check_binpath;
	int check_ini;
	int check_para_ini;
} __attribute__((packed));

/*
 * struct file_name_t - struct of file name and file path
 * @bin_path:      binary file's location path.
 * @ini_file:      ini file name.
 * @para_ini_file: parameter ini file name.
 */
struct file_name_t {
	char *bin_path;
	char *ini_file;
	char *para_ini_file;
} __attribute__((packed));

struct boot_configs_t g_boot_config;
struct file_name_t fName = { "", "", "" };

char *strConcat(char *str1, char *str2);
char *getConstStr(const char *str1);
void prtHelpMsg(void);
int parseINI(void *ptr, struct check_flag_t chflag);
int getFileSize(char *file_name);
int getFileCsum(char *file_name);
int genPartBin(struct check_flag_t chflag);
uint32_t calcChecksum(uint32_t *data, uint32_t count);

/*
 * calcChecksum - calculate checksum
 *
 * calcChecksum is the function of calculate checksum for check partition
 * information table correct or not.
 */
uint32_t calcChecksum(uint32_t *data, uint32_t count)
{
	uint32_t sum = 0;
	uint32_t inverse = 0xB139D2E5;
	int i;

	for (i = 0; i < count; i++) {
		sum = (sum >> 1) | (sum << 31);
		sum = sum ^ data[i];
		sum = sum ^ inverse;
	}

	return sum;
}

/*
 * strConcat - concatenate two string
 *
 * strConcat is the function of concatenate binary file path and binary
 * file name.
 */
char *strConcat(char *str1, char *str2)
{
	int length = strlen(str1) + strlen(str2) + 1;
	char *result = (char *)malloc(sizeof(char) * length);
	strcpy(result, str1);
	strcat(result, str2);

	return result;
}

/*
 * getConstStr - get string from const type string
 *
 * getConstStr is the function of get string from const type string.
 * It can avoid incompatible type 'const char' to 'char'.
 */
char *getConstStr(const char *str1)
{
	int length = strlen(str1) + 1;
	char *result = (char *)malloc(sizeof(char) * length);
	strcpy(result, str1);

	return result;
}

/*
 * prtHelpMsg - print help message about parameter.
 *
 * prtHelpMsg is the function of introduce what parameters you
 * can use with this program.
 */
void prtHelpMsg(void)
{
	printf("\nUsage: [-h][-p binary_file_path][-i ini_file_name][-a para_ini_file_name]\n\n");
	printf("  Parameters:\n\n");
	printf("    h: show this help message.\n");
	printf("    p: enter your input & output binary file path.\n");
	printf("    i: enter your ini file name.\n");
	printf("    a: enter your parameter ini file name.\n\n");
}

int get_key_value(dictionary *ini, const char *parent, const char *child)
{
	char key[32];
	memset(key, 0, 32);
	sprintf(key, "%s:%s", parent, child);
	return iniparser_getint(ini, key, -1);
}

void replace_ch(const char *str)
{
	int i;
	char *p = (char *)str;
	for (i = 0; i < strlen(str); i++) {
		if (*(p + i) == '.')
			*(p + i) = ':';
	}
}

/*
 * parseINI - parse ini information from *.ini file.
 *
 * parseINI is the function of parse the ini information from the *.ini file.
 * It can avoid hard code file name in the code.
 */
int parseINI(void *ptr, struct check_flag_t chflag)
{
	const char *para_ini_name = PARA_MAP_INI_INFO;
	dictionary *para_ini;
	uint32_t para_num = 0;
	uint32_t para_idx = 0;
	const char *para_name;
	int type = 0;
	int length = 0;

	const char *ini_name = PART_INI_INFO;
	dictionary *ini;
	const char *descr_string;

	printf("==============================\n");
	printf("Start to parse partition information...\n");

	if (chflag.check_para_ini != 1) {
		para_ini = iniparser_load(para_ini_name);
	} else {
		para_ini = iniparser_load(fName.para_ini_file);
	}

	if (para_ini == NULL) {
		fprintf(stderr, "Cannot parse para_ini file: %s\n", para_ini_name);
		return -1;
	}

	if (chflag.check_ini != 1) {
		ini = iniparser_load(ini_name);
	} else {
		ini = iniparser_load(fName.ini_file);
	}

	if (ini == NULL) {
		fprintf(stderr, "Cannot parse ini file: %s\n", ini_name);
		return -1;
	}

	para_num = iniparser_getnsec(para_ini);
	while (para_idx < para_num) {
		para_name = iniparser_getsecname(para_ini, 0);

		if (strlen(para_name) >= 32) {
			printf("%s is over buffer limit!\n", para_name);
			return 32;
		}

		type = get_key_value(para_ini, para_name, "type");
		length = get_key_value(para_ini, para_name, "length");
		replace_ch(para_name);

		if (type == 0) { //special case
			descr_string = iniparser_getstring(ini, para_name, NULL);
			if (!strcmp(descr_string, "")) {
				*((uint32_t *)ptr) = 0;
				ptr += 4;
				*((uint32_t *)ptr) = 0;
			} else {
				uint32_t ret = getFileSize(getConstStr(descr_string));
				*((uint32_t *)ptr) = (ret == -1) ? 0 : ret;
				uint32_t ret2 = getFileCsum(getConstStr(descr_string));
				ptr += 4;
				*((uint32_t *)ptr) = (ret2 == -1) ? 0 : ret2;
			}
		} else if (type == 1) { //for uint32
			*((uint32_t *)ptr) = iniparser_getint(ini, para_name, -1);
		} else if (type == 2) { //for string
			descr_string = iniparser_getstring(ini, para_name, NULL);
			if (strlen(descr_string) > 16) {
				printf("label=%s length over 16\n", descr_string);
				return 16;
			}
			memcpy((uint8_t *)ptr, descr_string, strlen(descr_string));
		} else if (type == 3) {
			*((uint8_t *)ptr) = iniparser_getint(ini, para_name, -1);
		}

		ptr += length;
		para_idx++;
	}

	iniparser_freedict(ini);
	iniparser_freedict(para_ini);
	return 0;
}

/*
 * getFileSize - get binary file size
 *
 * getFileSize is the function of get binary file length and convert
 * it to page size.
 */
int getFileSize(char *file_name)
{
	FILE *pFile;
	uint32_t file_len;
	char *file_path = strConcat(fName.bin_path, file_name);

	pFile = fopen(file_path, "r");
	if (pFile == NULL) {
		printf("Binary file \"%s\" open failure!\n", file_name);
		return -1;
	}

	fseek(pFile, 0, SEEK_END);
	file_len = ftell(pFile);
	fclose(pFile);

	return file_len;
}

int getFileCsum(char *file_name)
{
	FILE *pFile;
	uint32_t file_len;
	char *file_path = strConcat(fName.bin_path, file_name);
	int *buf;
	int file_csum = 0;

	pFile = fopen(file_path, "r");
	if (pFile == NULL) {
		printf("Binary file \"%s\" open failure!\n", file_name);
		return -1;
	}

	fseek(pFile, 0, SEEK_END);
	file_len = ftell(pFile);
	fseek(pFile, 0, SEEK_SET);
	buf = malloc(file_len);
	memset(buf, 255, file_len);
	fread((void *)buf, sizeof(int), file_len >> 2, pFile);
	file_csum = calcChecksum((void *)buf, file_len >> 2);
	fclose(pFile);

	return file_csum;
}

/*
 * genPartBin - generate partition binary
 *
 * getPartBin is the function of collect description information
 * and file page size for generate partition binary.
 */
int genPartBin(struct check_flag_t chflag)
{
	FILE *pFile;
	char *bin_name = PART_BINARY_NAME;
	char *output_bin = "";

	printf("==============================\n");
	printf("Perpare to generate partition binary...\n");

	if (chflag.check_binpath == 1) {
		output_bin = strConcat(fName.bin_path, bin_name);
		printf("Partition binary is located at: %s\n", fName.bin_path);
		pFile = fopen(output_bin, "wb+");
	} else {
		printf("partition binary is located at: .\n");
		pFile = fopen(bin_name, "wb+");
	}

	if (pFile == NULL) {
		printf("Partition binary file create failure!\n");
		return 1;
	}

	/* Write bootcfgs as an entity */
	fwrite(&g_boot_config, sizeof(g_boot_config), 1, pFile);
	fclose(pFile);

	printf("Binary file name: %s\n\n", PART_BINARY_NAME);

	return 0;
}

int main(int argc, char *argv[])
{
	int choice;
	int check_help = 0;
	int ret = 0;
	struct check_flag_t chflag = { 0, 0, 0 };
	void *ptr = &g_boot_config;
	//opterr = 0; /* Do not print getopt error message*/
	//
	printf("\n");

	while ((choice = getopt(argc, argv, "hp:i:a:")) != -1) {
		switch (choice) {
		case 'h':
			prtHelpMsg();
			check_help = 1;
			break;
		case 'p':
			fName.bin_path = optarg;
			chflag.check_binpath = 1;
			printf("Set input & output binary file path to: %s\n", fName.bin_path);
			break;
		case 'i':
			fName.ini_file = optarg;
			chflag.check_ini = 1;
			printf("your entry ini file name: %s\n", fName.ini_file);
			break;
		case 'a':
			fName.para_ini_file = optarg;
			chflag.check_para_ini = 1;
			printf("your entry parameter ini file name: %s\n", fName.para_ini_file);
			break;
		}
	}

	snprintf(ptr, 16, "%s", "#BOOTCONFIGS");
	ptr += 16;

	if (check_help != 1) {
		ret = parseINI(ptr, chflag);
		g_boot_config.self_checksum =
		        calcChecksum((uint32_t *)&g_boot_config,
		                     (sizeof(g_boot_config) / sizeof(uint32_t)) - 1); //exclude self_checksum
		genPartBin(chflag);
	}
	return ret;
}
