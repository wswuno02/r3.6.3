CXX = $(CROSS_COMPILE)g++
CC = $(CROSS_COMPILE)gcc
AS = $(CROSS_COMPILE)as
AR = $(CROSS_COMPILE)ar
NM = $(CROSS_COMPILE)nm
LD = $(CROSS_COMPILE)ld
OBJDUMP = $(CROSS_COMPILE)objdump
OBJCOPY = $(CROSS_COMPILE)objcopy
RANLIB = $(CROSS_COMPILE)ranlib
STRIP = $(CROSS_COMPILE)strip

SDKSRC_DIR ?= $(realpath $(CURDIR)/../../..)
include $(SDKSRC_DIR)/build/sdksrc.mk

CPU = arm926ej-s

DEPLIBS :=$(FSBL_PATH)/lib/libmach.a
INC := -I. -I$(CSR_DIR) -I$(SDK_INC) -I$(FSBL_PATH)/include
LIB := -L$(FSBL_PATH)/lib -lmach

AFLAGS := -mcpu=$(CPU)
CFLAGS := -mcpu=$(CPU) -MMD -g -Os \
	 -fdata-sections -ffunction-sections $(INC)
LDFLAGS := --gc-sections $(LIB)

LINKER_SCRIPT := flash_prog.ld

BINS = flash_prog.bin
INSTALL_TRGTS := $(addprefix $(BINPKG_DIR)/, $(BINS))

.PHONY: all
all: flash_prog.hex flash_prog.asm flash_prog.bin

.PHONY: install
install: all $(BINPKG_DIR) $(INSTALL_TRGTS)

$(BINPKG_DIR):
	$(Q)install -d $@

$(BINPKG_DIR)/%: %
	$(Q)install -m 777 $< $@

$(DEPLIBS):
	$(Q)$(MAKE) -C $(FSBL_BUILD_PATH) all install

%.bin: %.elf
	@printf "  %-8s  $<\n" "OBJCOPY"
	$(Q)$(OBJCOPY) -O binary --strip-all --strip-debug flash_prog.elf flash_prog.bin

%.hex: %.bin
	@printf "  %-8s  $<\n" "HEXDUMP"
	$(Q)hexdump -v -e '1/4 "%08x " "\n"' flash_prog.bin > flash_prog.hex

%.asm: %.elf
	@printf "  %-8s  $<\n" "OBJDUMP"
	$(Q)$(OBJDUMP) -D flash_prog.elf > flash_prog.asm

%.elf: %.o fsbl_entry.o $(DEPLIBS)
	@printf "  %-8s  $<\n" "LD"
	$(Q)$(LD) -T $(LINKER_SCRIPT) $< fsbl_entry.o $(LDFLAGS) -o $@

%.o: %.s
	@printf "  %-8s  $<\n" "AS"
	$(Q)$(AS) $(AFLAGS) $< -o $@

%.o: %.c
	@printf "  %-8s  $<\n" "CC"
	$(Q)$(CC) $(CFLAGS) -c $< -o $@

.PHONY: clean
clean:
	$(Q)rm -f *.o *.elf *.asm *.bin *.hex *.d
