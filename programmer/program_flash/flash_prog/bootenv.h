/*
 * AUGENTIX INC. - PROPRIETARY AND CONFIDENTIAL
 *
 * bootenv.h - Definitions for nand bootenv settings
 * Unpublished Copyright (C) 2018 Augentix Inc. - All Rights Reserved
 *
 * NOTICE: The information contained herein is the property of Augentix Inc.
 * Unauthorized copying and distributing of this file, via any medium,
 * is strictly prohibited.
 *
 * * Author: ShihChieh Lin <shihchieh.lin@augentix.com>
 */
#ifndef BOOTENV_H
#define BOOTENV_H

/*
 * BOOTENV for U-Boot
 */
#define BOOTENV_PARTS 2 /* Number of BOOTENV partitions */
#define BOOTENV_MTDID 2
#define BOOTENV_REDUND_MTDID 3

#endif /* BOOTENV_H */
