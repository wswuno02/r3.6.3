#ifndef DEFINE_H_
#define DEFINE_H_

#include <stdint.h>

/*
 * DRAM address used by Flashprog and its ICE script
 */
#define BR_MEM_BASE 0x03C00000
#define BR_SIZE 0x1C
#define FSBL_S1_MEM_BASE 0x03C10000
#define FSBL_S1_SIZE 0x0800
#define FSBL_S2_MEM_BASE 0x03C20000
#define FSBL_S2_SIZE 0x3800
#define PART_INFO_MEM_BASE 0x03C30000
#define PART_INFO_SIZE 0x400
#define BOOTENV_MEM_BASE 0x03500000
#define BOOTENV_SIZE 0x800
#define BOOTENV_R_MEM_BASE 0x03520000
#define BOOTENV_R_SIZE 0x800

/* Enable image verification during flash programming */
#define VERIFY_ENABLE 1

/* Enable re-build dump flash binary workflow */
#define DUMP_FLASH 0

#define RETRY_TIMES 3

#endif // DEFINE_H_
