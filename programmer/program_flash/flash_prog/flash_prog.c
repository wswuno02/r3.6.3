/******************************************************************************
 *
 * Copyright (c) Augentix Inc. - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * Proprietary and confidential.
 *
 ******************************************************************************/
#include "flash_prog.h"
#include "boot_flash.h"
#include "define.h"
#include "bootenv.h"
#include "print.h"
#include "utils.h"
#include "config.h"
#include <stdint.h>

int parsePartTable(struct partition_table_t *part_table, uint8_t *part_table_base_addr);

struct nand_flash_dev nand_flash_ids[] = {
	{ { .id = { 0xEF, 0xAA, 0x20 } }, 64, 3 }, //Winbond W25N512GV
	{ { .id = { 0xEF, 0xAA, 0x21 } }, 128, 3 }, //Winbond W25N01GV
	{ { .id = { 0xEF, 0xAA, 0x22 } }, 256, 3 }, //Winbond W25N02KV
	{ { .id = { 0xC2, 0x12 } }, 128, 2 }, //MXIC MX35LF1GE4AB
	{ { .id = { 0xC2, 0x26, 0x03 } }, 256, 3 }, //MXIC MX35LF2GE4AD
	{ { .id = { 0xD5, 0x19 } }, 128, 2 }, //Etron EM73C044SNA
	{ { .id = { 0xD5, 0x11 } }, 128, 2 }, //Etron EM73C044SNB
	{ { .id = { 0xD5, 0x09 } }, 128, 2 }, //Etron EM73C044SNF
	{ { .id = { 0xD5, 0x01 } }, 64, 2 }, //Etron EM73B044VCA
	{ { .id = { 0xD5, 0x1C } }, 128, 2 }, //Etron EM73C044VCD
	{ { .id = { 0xC8, 0xD1 } }, 128, 2 }, //GD GD5F1GQ4U
	{ { .id = { 0xC8, 0xD2 } }, 256, 2 }, //GD GD5F2GQ4U
	{ { .id = { 0xC8, 0x32 } }, 256, 2 }, //GD GD5F2GQ5U
	{ { .id = { 0xC8, 0x01 } }, 128, 2 }, //ESMT F50L1G41LB
	{ { .id = { 0x0B, 0xE1 } }, 128, 2 }, //XTX XT26G01AWSEGA
	{ { .id = { 0x2C, 0x14 } }, 128, 2 }, //Micron MT29F1G01ABAFDWB
	{ { .id = { 0x2C, 0x24 } }, 256, 2 }, //Micron MT29F1G01ABAFDWB
	{ { .id = { 0x2C, 0x24 } }, 256, 2 } //XTX XT26G02ELGIGA

};

static int eraseBootEnv(struct partition_table_t *part_table)
{
	int bootenv_mtdid[BOOTENV_PARTS] = { BOOTENV_MTDID, BOOTENV_REDUND_MTDID };
	uint32_t bootenv_start_page, bootenv_size, bootenv_blk_cnt;
	int i, blk, ret, mtdid;
	uint32_t page;

	for (i = 0; i < BOOTENV_PARTS; ++i) {
		mtdid = bootenv_mtdid[i];
		bootenv_start_page = part_table->pInfo[mtdid].flash_start >> BYTES_OFFSET_2048;
		bootenv_size = part_table->pInfo[mtdid].flash_size;
		bootenv_blk_cnt = (bootenv_size >> (BYTES_OFFSET_2048 + PAGES_OFFSET_64));

		for (blk = 0, page = bootenv_start_page; blk < bootenv_blk_cnt;
		     blk++, page += PAGES_PER_BLK(PAGES_OFFSET_64)) {
			ret = eraseFlashBlock(page);
			if (ret != -ESUCCESS) {
				echo("Error: Bootenv not erased!");
				return ret;
			}
		}
	}
	return -ESUCCESS;
}

void printErr(int status)
{
	switch (status) {
	case -ETIMEOUT:
		echo("    - Timeout");
		break;
	case -EFAIL:
		echo("    - FAIL");
		break;
	case -ESUCCESS:
		echo("    - SUCCESS");
		break;
	default:
		break;
	}
}

/*
 * mainEntry - the first executed function
 *
 * The main function that control the flow of the program.
 * 1. System setup.
 * 2. Load partition information.
 * 3. Program data into NAND flash.
 */
void mainEntry()
{
	uint8_t data = 0;
	uint32_t blknum = 0;
	uint32_t csr;
	struct partition_table_t part_table;
	struct boot_configs_t bcfgs;
	uint32_t bytes_per_page_shift = BYTES_OFFSET_2048;
	uint32_t pages_per_blk_shift = PAGES_OFFSET_64;
	uint32_t image_page_num = 0;
	uint32_t gold_csum = 0;
	uint32_t *gold_addr = 0;
	uint32_t jid[3];
	struct nand_flash_dev *type;
	uint32_t final_img = 0;

	int status;
	int i;
	int retry_times;
	int nand_len = 0;
	int pt_part_num;
	uint32_t sckdv;

	echo("AUGENTIX FLASH PROGRAMMER");

	sckdv = (getChipVer() == 0x00) ? 12 : 10;

	initQspiIO();
	initSSI(sckdv, 0, 0, 15);
	initFlash();

	/* Enable ICache */
	asm volatile("mrc p15, 0, r1, c1, c0, 0\n"
	             "orr r1, r1, #0x1000\n"
	             "mcr p15, 0, r1, c1, c0, 0\n" ::
	                     : "r1", "memory");

	/* Load partition map */
	parsePartTable(&part_table, (uint8_t *)(PART_INFO_MEM_BASE + ((uint32_t)&bcfgs.part_tb - (uint32_t)&bcfgs)));

	/* Identify flash start */
	type = nand_flash_ids;
	status = readFlashJID(jid);
	if (status != -ESUCCESS || (jid[0] == 0 && jid[1] == 0 && jid[2] == 0)) {
		echo("[Error] Can not read JEDEC ID!");
		return;
	} else {
		echo("[Pass] Read JEDEC ID success!");
	}

	echo_hex_n(jid[0]);
	echo_n(" - ");
	echo_hex_n(jid[1]);
	if (jid[2] != 0 && jid[0] != jid[2]) {
		echo_n(" - ");
		echo_hex(jid[2]);
	} else {
		echo(" ");
	}

	for(i = 0; i < SIZE_OF_ARRAY(nand_flash_ids); i++) {
		if (type[i].id_len == 3) {
			if (type[i].id[0] == jid[0] && type[i].id[1] == jid[1] && type[i].id[2] == jid[2])
				nand_len = type[i].chipsize;
		} else {
			if (type[i].id[0] == jid[0] && type[i].id[1] == jid[1])
				nand_len = type[i].chipsize;
		}
	}
	if (nand_len == 0) {
		echo("[Error] Nand flash does not match the support list!");
		return;
	} else {
		echo("[Pass] Nand flash match the support list!");
	}

	pt_part_num = part_table.part_num;
	final_img = part_table.pInfo[pt_part_num - 1].flash_start + part_table.pInfo[pt_part_num - 1].flash_size;
	if (nand_len < (final_img >> 20)) {
		echo("[Error] Nand flash size is too small, please check again!");
		return;
	} else {
		echo("[Pass] Nand flash size check complete!");
	}
	/* Identify flash end */

	retry_times = RETRY_TIMES;
	echo("Write partition info");
	for (retry_times; retry_times > 0; retry_times--) {
		echo("  + Programming...");
		status = programFlash(
		        (uint32_t)nandAddrToBlock(PART_INFO_BASE, bytes_per_page_shift, pages_per_blk_shift),
		        (uint32_t)nandAddrToBlock(PART_INFO_PART_SIZE, bytes_per_page_shift, pages_per_blk_shift),
		        (uint32_t)PART_INFO_MEM_BASE, (uint32_t)PART_INFO_PAGE_BASE, (uint32_t)PART_INFO_SIZE,
		        pages_per_blk_shift);
		printErr(status);
#if VERIFY_ENABLE
		if (status != -ESUCCESS) {
			continue;
		}
		gold_addr = (uint32_t *)(PART_INFO_MEM_BASE + PART_INFO_SIZE - 4);
		gold_csum = gold_addr[0];
		echo("  + Verifying...");
		status = verifyFlash(
		        ((uint32_t)PART_INFO_BASE + ((uint32_t)PART_INFO_PAGE_BASE << bytes_per_page_shift)),
		        (uint32_t)PART_INFO_SIZE, (uint32_t *)PART_INFO_MEM_BASE, ((uint32_t)PART_INFO_SIZE) - 1,
		        (uint32_t)gold_csum);
		printErr(status);
#endif
		if (status == -ESUCCESS) {
			break;
		}
	}
	if (retry_times == 0 && status != -ESUCCESS) {
		echo("[ERROR] Programming failed.");
		return;
	}

	echo("Write Partitions");
	echo("  Erase block 0");
	eraseFlashBlock(0);
	echo("  Erase Bootenv");
	eraseBootEnv(&part_table);

#if DUMP_FLASH
	/* Program bootenv start*/
	retry_times = RETRY_TIMES;
	echo("  Write bootenv(For dump_flash)");
	for (retry_times; retry_times > 0; retry_times--) {
		echo("  + Programming...");
		status = programFlash(
		        (uint32_t)nandAddrToBlock(BOOTENV_BASE, bytes_per_page_shift, pages_per_blk_shift),
		        (uint32_t)nandAddrToBlock(BOOTENV_PART_SIZE, bytes_per_page_shift, pages_per_blk_shift),
		        (uint32_t)BOOTENV_MEM_BASE, (uint32_t)BOOTENV_PAGE_BASE, (uint32_t)BOOTENV_SIZE,
		        pages_per_blk_shift);
		printErr(status);
		if (status == -ESUCCESS) {
			break;
		}
	}
	if (retry_times == 0 && status != -ESUCCESS) {
		echo("[ERROR] Programming failed.");
		return;
	}

	retry_times = RETRY_TIMES;
	echo("  Write bootenv_redund(For dump_flash)");
	for (retry_times; retry_times > 0; retry_times--) {
		echo("  + Programming...");
		status = programFlash(
		        (uint32_t)nandAddrToBlock(BOOTENV_R_BASE, bytes_per_page_shift, pages_per_blk_shift),
		        (uint32_t)nandAddrToBlock(BOOTENV_R_PART_SIZE, bytes_per_page_shift, pages_per_blk_shift),
		        (uint32_t)BOOTENV_R_MEM_BASE, (uint32_t)BOOTENV_R_PAGE_BASE, (uint32_t)BOOTENV_R_SIZE,
		        pages_per_blk_shift);
		printErr(status);
		if (status == -ESUCCESS) {
			break;
		}
	}
	if (retry_times == 0 && status != -ESUCCESS) {
		echo("[ERROR] Programming failed.");
		return;
	}
	/* Program bootenv end*/
#endif

	retry_times = RETRY_TIMES;
#if CONFIG_BAREMETAL
	echo("  Bare-Metal");
#else
	echo("  Write system images...");
#endif
	for (retry_times; retry_times > 0; retry_times--) {
		for (i = 0; i < part_table.part_num; i++) {
			image_page_num = ((part_table.pInfo[i].image_size + 2047) >> 11);
			if (0 == image_page_num)
				continue;

			echo_n("  + Programming partition ");
			echo_dec_n(i);
			echo("...");
			//      printPartInfo(&part_table.pInfo[i]);
			status = programFlash((uint32_t)nandAddrToBlock(part_table.pInfo[i].flash_start,
			                                                bytes_per_page_shift, pages_per_blk_shift),
			                      (uint32_t)nandAddrToBlock(part_table.pInfo[i].flash_size,
			                                                bytes_per_page_shift, pages_per_blk_shift),
			                      (uint32_t)part_table.pInfo[i].mem_start, (uint32_t)0,
			                      (uint32_t)(part_table.pInfo[i].image_size), pages_per_blk_shift);
			printErr(status);
			if (status != -ESUCCESS) {
				break;
			}
#if VERIFY_ENABLE
			echo("  + Verifying...");
			status = verifyFlash((uint32_t)part_table.pInfo[i].flash_start,
			                     (uint32_t)part_table.pInfo[i].flash_size,
			                     (uint32_t *)part_table.pInfo[i].mem_start,
			                     (uint32_t)part_table.pInfo[i].image_size,
			                     (uint32_t)part_table.pInfo[i].image_csum);
			printErr(status);
			if (status != -ESUCCESS) {
				break;
			}
#endif
		}
		if (status == -ESUCCESS) {
			break;
		}
	}
	if (retry_times == 0 && status != -ESUCCESS) {
		echo("[ERROR] Programming failed.");
		return;
	}

#if DATA_FROM_UART
	fsbl_addr = 0x01000000;
	uboot_addr = 0x02000000;
	dtb_addr = 0x02800000;
	rootfs_addr = 0x02900000;
	linux_addr = 0x03000000;

	/* Program NAND flash */
	uartToDram("REQ(FSBL)", 2480, fsbl_addr);
	uartToDram("REQ(DTB)", 2008, dtb_addr);
	uartToDram("REQ(UBOOT)", 121816, uboot_addr);
	uartToDram("REQ(ROOTFS)", 4816097, rootfs_addr);
	uartToDram("REQ(LINUX)", 1535848, linux_addr);
#endif

	/* Disable ICache */
	asm volatile("mrc p15, 0, r1, c1, c0, 0\n"
	             "bic r1, r1, #0x1000\n"
	             "mcr p15, 0, r1, c1, c0, 0\n"
	             :
	             :
	             : "r1", "memory");

	/* Disable DCache */
	asm volatile("mrc p15, 0, r1, c1, c0, 0\n"
	             "bic r1, r1, #0x4\n"
	             "mcr p15, 0, r1, c1, c0, 0\n"
	             :
	             :
	             : "r1", "memory");

	echo("Done");
}

int parsePartTable(struct partition_table_t *part_table, uint8_t *part_table_base_addr)
{
	uint8_t *ptr;
	int i;

	ptr = (uint8_t *)part_table_base_addr;

	part_table->part_num = (*(ptr)&0xFF) | ((*(ptr + 1) << 8) & 0xFF00) | ((*(ptr + 2) << 16) & 0xFF0000) |
	                       ((*(ptr + 3) << 24) & 0xFF000000);
	ptr += 4;
	part_table->boot_part = (*(ptr)&0xFF) | ((*(ptr + 1) << 8) & 0xFF00) | ((*(ptr + 2) << 16) & 0xFF0000) |
	                        ((*(ptr + 3) << 24) & 0xFF000000);
	ptr += 4;

	if (part_table->part_num > PART_NUM) {
		echo("[ERROR] MTD partition exceeds software maximum");
		return -1;
	}

	for (i = 0; i < part_table->part_num; i++) {
		part_table->pInfo[i].flash_start = (*(ptr)&0xFF) | ((*(ptr + 1) << 8) & 0xFF00) |
		                                   ((*(ptr + 2) << 16) & 0xFF0000) | ((*(ptr + 3) << 24) & 0xFF000000);
		ptr += 4;

		part_table->pInfo[i].flash_size = (*(ptr)&0xFF) | ((*(ptr + 1) << 8) & 0xFF00) |
		                                  ((*(ptr + 2) << 16) & 0xFF0000) | ((*(ptr + 3) << 24) & 0xFF000000);
		ptr += 4;

		part_table->pInfo[i].mem_start = (*(ptr)&0xFF) | ((*(ptr + 1) << 8) & 0xFF00) |
		                                 ((*(ptr + 2) << 16) & 0xFF0000) | ((*(ptr + 3) << 24) & 0xFF000000);
		ptr += 4;

		part_table->pInfo[i].image_size = (*(ptr)&0xFF) | ((*(ptr + 1) << 8) & 0xFF00) |
		                                  ((*(ptr + 2) << 16) & 0xFF0000) | ((*(ptr + 3) << 24) & 0xFF000000);
		ptr += 4;

		part_table->pInfo[i].image_csum = (*(ptr)&0xFF) | ((*(ptr + 1) << 8) & 0xFF00) |
		                                  ((*(ptr + 2) << 16) & 0xFF0000) | ((*(ptr + 3) << 24) & 0xFF000000);
		ptr += 4;

		memcmp(&part_table->pInfo[i].label[i], ptr, 16);

		ptr += 16;
	}

	return 0;
}

#if DATA_FROM_UART
int uartToDram(char *req, uint32_t size, uint32_t addr)
{
	int i;
	uint8_t rx_buffer[UART_RX_FIFO_SIZE + 4];
	/*  uint32_t checksum;
  uint32_t golden;
  uint32_t * fsbl;*/
	uint32_t byte_count = 0;
	uint32_t read_byte_count = 0;

	for (i = 0; i < UART_RX_FIFO_SIZE; i++) {
		rx_buffer[i] = 0;
	}

	read_byte_count = size;

	echo(req);

	while (read_byte_count > 0) {
		byte_count = (read_byte_count > UART_RX_FIFO_SIZE) ? UART_RX_FIFO_SIZE : read_byte_count;
		receiveUart(CONSOLE_ID, byte_count, (uint8_t *)(((uint32_t)addr) + i));
		transmitUart(CONSOLE_ID, 1, ".");
		i += byte_count;
		read_byte_count -= byte_count;
	}
#if 0
  echo ( "REQ(FSBL_CHECKSUM)" );

  receiveUart ( CONSOLE_ID, 4, (uint8_t *) &golden );

  checksum = calcChecksum ( fsbl, 2048 / sizeof(uint32_t) );

  if ( checksum == golden )
  {
    echo ( "[INFO] FSBL is valid." );
  }
  else
  {
    echo ( "[WARN] FSBL is invalid." );
    return -EFAIL;
  }

  echo ( "[INFO] Booting FSBL @ 0xFFFF0000." );

  asm ( "bx %[fsbl]\n"
      :
      : [fsbl] "r" ( fsbl )
      : );
#endif
}

#endif
