/*
 * gfl.c
 * Copyright (C) 2019 im21 <im21@ts00>
 *
 * Distributed under terms of the MIT license.
 */
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	FILE *fp;
	unsigned int pos;
	unsigned int usc_sum;
	int pos_i;
	unsigned char usc[4];

//	printf("[G]Get file length\n");

	fp = fopen(argv[2], "rb");

	if (!fp) {
		printf("open error\n");
		return 1;
	}

	/* uboot_0 0xB4 ~ 0xB7*/
	/* linux_0 0xD8 ~ 0xDB*/
	/* dtb_0   0xFC ~ 0xFF*/

	pos = strtol(argv[1], NULL, 16);
//	printf("pos= 0x%x\n", pos);

	for( pos_i = 0; pos_i < 4; pos_i++ ) {
		fseek(fp, pos, SEEK_SET);
		usc[pos_i] = fgetc(fp);
//		printf("usc[%d]: 0x%x (%d)\n", pos_i, usc[pos_i], usc[pos_i]);
		pos++;
	}
	usc_sum = usc[0] + (usc[1] << 8)+ (usc[2] << 16) + (usc[3] << 24);
//	printf("usc_sum = %d\n", usc_sum);
	printf("%d\n", usc_sum);

	return 0;
}
