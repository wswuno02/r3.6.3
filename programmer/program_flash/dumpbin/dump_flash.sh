#! /bin/sh
#
# dump_flash.sh
# Copyright (C) 2019 im21 <im21@ts00>
#
# Distributed under terms of the MIT license.
#

NFS_DIR=$1
BK_DIR=$NFS_DIR/dump_flash
FW_PRINTENV=/usr/sbin/fw_printenv

rm -rf $BK_DIR
mkdir -p $BK_DIR
echo $BK_DIR

# fsbl
nanddump -f $BK_DIR/fsbl.bin -l 22528 /dev/mtd0
# bootcfgs
nanddump -f $BK_DIR/bootcfg.bin -l 1024 /dev/mtd1
# bootenv
nanddump -f $BK_DIR/bootenv.bin -l 16384 /dev/mtd2
# bootenv_redund
nanddump -f $BK_DIR/bootenv_r.bin -l 16384 /dev/mtd3
# uboot_0 *** 0xb4
uboot_len=$(gfl 0xb4 $BK_DIR/bootcfg.bin)
nanddump -f $BK_DIR/u-boot.bin -l $uboot_len /dev/mtd4

# Detect that boot from Slot A or Slot B
if [ $($FW_PRINTENV -n slot_a_active) == 1 ]; then
# slot A
# linux_0 *** 0xd8
linux_len=$(gfl 0xd8 $BK_DIR/bootcfg.bin)
nanddump -f $BK_DIR/uImage.bin -l $linux_len /dev/mtd5
# dtb_0 *** 0xfc
dtb_len=$(gfl 0xfc $BK_DIR/bootcfg.bin)
nanddump -f $BK_DIR/dtb.bin -l $dtb_len /dev/mtd6
else # Slot B active
# slot B
# linux_1 *** 0x168
linux_len=$(gfl 0x168 $BK_DIR/bootcfg.bin)
nanddump -f $BK_DIR/uImage.bin -l $linux_len /dev/mtd9
# dtb_1 *** 0x18c
dtb_len=$(gfl 0x18c $BK_DIR/bootcfg.bin)
nanddump -f $BK_DIR/dtb.bin -l $dtb_len /dev/mtd10
fi


# special case: Filesystem

# Clean the base
rm -rf /tmp/bk_rfs/
rm -rf /tmp/bk_usrdata/
rm -rf /tmp/bk_calib/

# rootfs_0
echo Create rootfs...
mkdir -p /tmp/bk_rfs/dev/
mkdir -p /tmp/bk_rfs/mnt/
mkdir -p /tmp/bk_rfs/proc/
mkdir -p /tmp/bk_rfs/sys/
mkdir -p /tmp/bk_rfs/calib/
mkdir -p /tmp/bk_rfs/usrdata/
cd /tmp/bk_rfs/
tar -cvf $BK_DIR/bk_rfs_1.tar /bin/ /etc/ /lib/ /lib32/ /linuxrc/ /media/ /opt/ /root/ /root/ /run/ /sbin/ /usr/ /var/ /init/ /tmp/ 2>/dev/null
tar -cvf $BK_DIR/bk_rfs_2.tar . 2>/dev/null
# delete temp files
rm -rf /tmp/bk_rfs/

# usrdata
echo Create usrdata...
tar -cvf $BK_DIR/bk_usrdata.tar /usrdata/ 2>/dev/null

# calib
echo Create calib...
tar -cvf $BK_DIR/bk_calib.tar /calib/ 2>/dev/null


