#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include "boot_flash.h"
#include "iniparser.h"
#include "dictionary.h"
#include "flash_prog.h"

/* per-partition info extracted from ini file */
struct ini_part_info {
	u_int64_t nand_start;
	u_int64_t nand_size;
	u_int32_t memory_start;
	char file_name[100];
	char label[100];
	char sec_name[100];
};

/* whole partition table extracted from ini file */
struct part_tbl {
	unsigned int part_num;
	/* ini_par_info_arr size is set to double the count PART_NUM
	* Due to partition spare blocks in-between declared blocks
	* (U-Boot mtd partition requires partitions to be consecutive)
	*/
	struct ini_part_info ini_part_info_arr[PART_NUM * 2];
};


/*
 * U-Boot script related definitions
 * UBOOT_SCR_LOAD_ADDR: base address for loading uboot script
 * UBOOT_MISC_LOAD_ADDR: Reserved to save checksum or other variables
 *                       during U-Boot flash programming
 * UBOOT_BINS_LOAD_ADDR: compiled binary files are loaded to this mem address
 *                       and programmed to flash one by one
 */
#define UBOOT_SCR_LOAD_ADDR 0x00080000
#define UBOOT_MISC_LOAD_ADDR (UBOOT_SCR_LOAD_ADDR + (128 * 1024))
#define UBOOT_BINS_SOURCE_CRC32 UBOOT_MISC_LOAD_ADDR
#define UBOOT_BINS_NAND_CRC32 (UBOOT_MISC_LOAD_ADDR + 4)
#define UBOOT_BINS_LOAD_ADDR UBOOT_MISC_LOAD_ADDR + (64 * 1024)

#define TFTP_RETRY_COUNT 50
#define TFTP_TIMEOUT_MS 50000UL
#define UBOOT_NETRETRY 5
//#define DEBUG 1

#ifdef DEBUG
#define DBG(fmt, args...)                                                                                              \
	do {                                                                                                           \
		printf(fmt, ##args);                                                                                   \
	} while (0)
#else
#define DBG(fmt, args...)                                                                                              \
	do {                                                                                                           \
		;                                                                                                      \
	} while (0)
#endif

int parse_ini_file(char *ini_name, char *jls_name);

int main(int argc, char *argv[])
{
	int status = 0;

	if (argc < 2) {
		printf("  usage: scriptgen <input.ini>.\n");
	} else {
		status = parse_ini_file(argv[1], ".mid"); //argv[2]);
	}
	return status;
}

/* gen_uboot_cmd_chk -
 * U-Boot commands have a return value, check the return value in script to see if
 * a unexpected error occured (terminate script if error occurred)
 */
void gen_uboot_cmd_chk(FILE *scr_ptr, char *cmd)
{
	fprintf(scr_ptr, "\nsetenv current_cmd %s\n", cmd);
	fprintf(scr_ptr, "if ${current_cmd}; then ; else echo Command; echo ${current_cmd}; echo exec err! Terminating script!; exit; fi\n\n");
}


/* gen_uboot_scr_pre(): Preprocess steps before starting U-Boot Flash Programming */
void gen_uboot_scr_pre(FILE *scr_ptr)
{
	char tmp_cmd[512];

	/* Current U-Boot doesn't have i/d cache enabled by default.
	 * Use command to enable icache
	 * (Enabling dcache causes some unknown issues, so it is not enabled for now)
	 */
	fprintf(scr_ptr, "icache flush;icache on;icache\n");
	fprintf(scr_ptr, "dcache\n");

	/* Environment variables setup */
	fprintf(scr_ptr, "\nsetenv success_count_hex 0\n");
	fprintf(scr_ptr, "setenv failed_count_hex 0\n");
	fprintf(scr_ptr, "setenv load_mem_start 0x%08X\n", UBOOT_BINS_LOAD_ADDR);

	sprintf(tmp_cmd, "nand erase.chip");
	gen_uboot_cmd_chk(scr_ptr, tmp_cmd);

	/* Setup mtdids (For mtdparts usage) */
	fprintf(scr_ptr, "setenv mtdids nand0=qspi\n");

	/* Set TFTP transfer error limit to a large value (To workaround "Sorcerer's Apprentice Syndrome") */
	fprintf(scr_ptr, "setenv tftptimeoutcountmax %u\n", TFTP_RETRY_COUNT);
	fprintf(scr_ptr, "setenv tftptimeout %lu\n", TFTP_TIMEOUT_MS);
	fprintf(scr_ptr, "setenv netretry %u\n", UBOOT_NETRETRY);
}


/* U-Boot flash programming has a lot a log output
 * Show a simple progress bar using simple environment variables
 * TODO: show success_count and using decimal instead of hex
 */
void gen_uboot_progress(FILE *scr_ptr, struct part_tbl *part_tbl_arr, int function)
{
	unsigned int part_idx, total_prog_part;
	struct ini_part_info *ini_part_info_arr = part_tbl_arr->ini_part_info_arr;

	if (function == 0) {
		/* Calculate the number of partitions that require flash programming (total_prog_part)*/
		for (part_idx = 0, total_prog_part = 0; part_idx < part_tbl_arr->part_num; part_idx++) {
			// Check if there is file to download/program
			if (strlen(ini_part_info_arr[part_idx].file_name) != 0) {
				total_prog_part++;
			}
		}

		fprintf(scr_ptr, "setenv total_prog_part_hex %x\n\n", total_prog_part);
	} else {
		fprintf(scr_ptr, "echo ==== Programming Progress ==== 0x${success_count_hex}/0x${total_prog_part_hex} =====\n");
	}
}

/* U-Boot TFTP download command (download to specified memory address)
 * Since UDP packet has checksum (makes sure the recieved data is correct)
 * save the downloaded file CRC32 checksum value (for verification purposes)
 */
void gen_uboot_scr_dload(FILE *scr_ptr, const char *file_name)
{
	char tmp_cmd[512];

	fprintf(scr_ptr, "echo ; echo + Downloading...\n");

	sprintf(tmp_cmd, "tftpboot ${load_mem_start} %s", file_name);
	gen_uboot_cmd_chk(scr_ptr, tmp_cmd);

	fprintf(scr_ptr, "crc32 ${fileaddr} ${filesize} 0x%08X\n\n", UBOOT_BINS_SOURCE_CRC32);
}

/* gen_uboot_scr_program()
 * erase and write the bin file to the specified partition
 */
void gen_uboot_scr_program(FILE *scr_ptr, uint64_t nand_start, uint64_t nand_size, const char *label_name)
{
	char tmp_cmd[512];

	fprintf(scr_ptr, "echo ; echo + Programming...\n");

	sprintf(tmp_cmd, "nand erase.part %s\n", label_name);
	gen_uboot_cmd_chk(scr_ptr, tmp_cmd);

	sprintf(tmp_cmd, "nand write ${load_mem_start} %s ${filesize}", label_name);
	gen_uboot_cmd_chk(scr_ptr, tmp_cmd);
}


/* gen_uboot_scr_verify()
 * read the file from NAND, and compare CRC32 value with the TFTP download value
 */
void gen_uboot_scr_verify(FILE *scr_ptr, uint64_t nand_start, const char *label_name)
{
	char tmp_cmd[512];

	fprintf(scr_ptr, "echo ; echo + Verifying...\n");
	fprintf(scr_ptr, "mw ${load_mem_start} 0 ${filesize}\n");

	sprintf(tmp_cmd, "nand read ${load_mem_start} %s ${filesize}\n", label_name);
	gen_uboot_cmd_chk(scr_ptr, tmp_cmd);

	fprintf(scr_ptr, "crc32 ${fileaddr} ${filesize} 0x%08X\n", UBOOT_BINS_NAND_CRC32);
	fprintf(scr_ptr, "if itest *0x%08X == *0x%08X ; then echo CRC ok ; setexpr success_count_hex ${success_count_hex} + 1 ;"
	                 " else echo CRC bad ; setexpr failed_count_hex ${failed_count_hex} + 1 ; fi\n\n",
	                 UBOOT_BINS_SOURCE_CRC32, UBOOT_BINS_NAND_CRC32);

	/* Exit flash programming script with error message for now (This shouldn't occur) */
	fprintf(scr_ptr, "if itest *0x%08X != *0x%08X ; then echo Programming Failed, Exit!; exit; fi\n",
	                 UBOOT_BINS_SOURCE_CRC32, UBOOT_BINS_NAND_CRC32);

}

/* compare()
 * compare function for qsort (sorts the nand partition according to NAND offset)
 */
int compare(const void *a, const void *b)
{
struct ini_part_info *part_a = (struct ini_part_info *)a;
	struct ini_part_info *part_b = (struct ini_part_info *)b;

	if (part_a->nand_start > part_b->nand_start)
		return 1;
	else if (part_a->nand_start < part_b->nand_start)
		return -1;
	else
		return 0;
}

/* gen_uboot_mtdparts_cmd()
 * Generate mtdparts U-Boot command & env variable.
 * So we can write to specified partition directly without crossing the partition declared size
 */
void gen_uboot_mtdparts_cmd(struct part_tbl *part_tbl_arr, FILE *scr_ptr)
{
	struct ini_part_info *ini_part_info_arr = part_tbl_arr->ini_part_info_arr;
	int orig_idx, spare_idx;
	char mtd_part_str[2048] = {'\0'}, tmp_str[512]; //buffer to store mtdparts command
	u_int64_t spare_size = 0;

	qsort((void *)ini_part_info_arr, part_tbl_arr->part_num, sizeof(struct ini_part_info), compare);
	/* Due to U-Boot command line single command char limit
	* mtd partition declaration is split into multiple setenv mtdpart commands
	*/
	for (orig_idx= 0, spare_idx = 0; orig_idx < part_tbl_arr->part_num; orig_idx++) {
		if (orig_idx == 0) {
			//First partiton don't need to check previous idx spare
			sprintf(tmp_str, "setenv mtdparts mtdparts=qspi:%lu(%s),\n",
				ini_part_info_arr[orig_idx].nand_size, ini_part_info_arr[orig_idx].label);
			strcat(mtd_part_str, tmp_str);
		} else {
			spare_size = ini_part_info_arr[orig_idx].nand_start
					- (ini_part_info_arr[orig_idx-1].nand_start + ini_part_info_arr[orig_idx-1].nand_size);
			/* Partition layout has unused spare blocks in between partitions,
			* -> This causes issues when using U-Boot "mtdparts"
			* declare spare_size partition to fill-in the gap between actual partitions when necessary
			*/
			if (spare_size != 0) {
				sprintf(tmp_str, "setenv mtdparts ${mtdparts}%lu(spare_%d),\n", spare_size, spare_idx);
				strcat(mtd_part_str, tmp_str);
				spare_idx++;
			}

			sprintf(tmp_str, "setenv mtdparts ${mtdparts}%lu(%s),\n",
				ini_part_info_arr[orig_idx].nand_size, ini_part_info_arr[orig_idx].label);
			strcat(mtd_part_str, tmp_str);
		}
	}

	DBG("%s\n", mtd_part_str);
	/* Save all mtd_part commands to script */
	fprintf(scr_ptr, "\n%s\n", mtd_part_str);
	/* print final partiton layout */
	fprintf(scr_ptr, "mtdparts\n");

	return;
}

/* ini_convert_arr()
 * convert ini file to array (for easier access of partition info/content)
 */
void ini_convert_arr(struct part_tbl *part_tbl_arr, dictionary *ini)
{
	int sec_num, i, tbl_idx;
	const char *sec_name, *label_name, *file_name;
	char sec_key_name[100];
	struct ini_part_info *ini_part_info_arr = part_tbl_arr->ini_part_info_arr;

	sec_num = iniparser_getnsec(ini);
	DBG("%d section(s) found.\n", sec_num);

	/* Convert ini file to array */
	// Skips i = 0 [partition]
	for (i = 0, tbl_idx = 0; i < sec_num; i++) {
		sec_name = iniparser_getsecname(ini, i);
		DBG(" - parsing section [%d] %s...\n", i, sec_name);
		/* Special case section "partition"
		 * get the partition total count
		 */
		if (!strcmp(sec_name, "partition")) {
			strcpy(sec_key_name, sec_name);
			strncat(sec_key_name, ":part_num", 9);
			DBG("  * key %s \n", sec_key_name);

			part_tbl_arr->part_num = iniparser_getint(ini, sec_key_name, 0);
			DBG("partition num %d\n", part_tbl_arr->part_num);

			continue;
		}

		/* Flow for all other ini config sections (which should be partition declarations */
		strcpy(ini_part_info_arr[tbl_idx].sec_name, sec_name);

		/* TODO: Using iniparser_getint may result in overflow
		*       should change to getstring and convert hex string to u_int64_t in the future
		*/
		strcpy(sec_key_name, sec_name);
		strncat(sec_key_name, ":nand_start", 11);
		DBG("  * key %s \n", sec_key_name);
		ini_part_info_arr[tbl_idx].nand_start = iniparser_getint(ini, sec_key_name, 0);

		strcpy(sec_key_name, sec_name);
		strncat(sec_key_name, ":nand_size", 10);
		DBG("  * key %s \n", sec_key_name);
		ini_part_info_arr[tbl_idx].nand_size = iniparser_getint(ini, sec_key_name, 0);

		strcpy(sec_key_name, sec_name);
		strncat(sec_key_name, ":file_name", 10);
		DBG("  * key %s \n", sec_key_name);
		file_name = iniparser_getstring(ini, sec_key_name, "\0");
		strcpy(ini_part_info_arr[tbl_idx].file_name, file_name);

		strcpy(sec_key_name, sec_name);
		strncat(sec_key_name, ":label", 6);
		DBG("  * key %s \n", sec_key_name);
		label_name = iniparser_getstring(ini, sec_key_name, "\0");
		strcpy(ini_part_info_arr[tbl_idx].label, label_name);

		strcpy(sec_key_name, sec_name);
		strncat(sec_key_name, ":memory_start", 13);
		DBG("  * key %s \n", sec_key_name);
		ini_part_info_arr[tbl_idx].memory_start = iniparser_getint(ini, sec_key_name, 0);

		/* Special case handling */
		if (!strcmp("ptable", ini_part_info_arr[tbl_idx].label)) {
			strcpy(ini_part_info_arr[tbl_idx].file_name, "bootcfgs.bin");
			/* bootcfgs.bin's nand flash offset is not indicated in partition table
			* (hardcoded in "Augentix Flash Programmer's flash_prog.c "Write partition info" stage)
			* -> Use the same pre-defined PART_INFO_BASE, and PART_INFO_SIZE varibles here
			*/
			ini_part_info_arr[tbl_idx].nand_start = PART_INFO_BASE;
			ini_part_info_arr[tbl_idx].nand_size = PART_INFO_PART_SIZE;
			ini_part_info_arr[tbl_idx].memory_start = PART_INFO_MEM_BASE;
		}
#if 0
		/* Current sdk-rel prebuilds script_gen.c, this causes DUMP_FLASH compile time c define not
		 * useful for differentiating if the below special cases need to be programmed
		 * Due to this reason, this is still placed in special case btenv.xxx scripts for now...
		 */
#if DUMP_FLASH
		if (!strcmp("bootenv", ini_part_info_arr[tbl_idx].label)) {
			strcpy(ini_part_info_arr[tbl_idx].file_name, "bootenv.bin");

			ini_part_info_arr[tbl_idx].memory_start = BOOTENV_MEM_BASE;
		}
		if (!strcmp("bootenv_redund", ini_part_info_arr[tbl_idx].label)) {
			strcpy(ini_part_info_arr[tbl_idx].file_name, "bootenv_r.bin");

			ini_part_info_arr[tbl_idx].memory_start = BOOTENV_R_MEM_BASE;
		}
#endif
#endif
		/* Partition table has ptable, bootenv, bootenv_redund which don't have file_name in ini file */
		DBG("%20s\t0x%x\t\t0x%x\t\t%s\t%s\t\n", ini_part_info_arr[tbl_idx].sec_name,
		                                        ini_part_info_arr[tbl_idx].nand_start, ini_part_info_arr[tbl_idx].nand_size,
		                                        ini_part_info_arr[tbl_idx].file_name, ini_part_info_arr[tbl_idx].label);
		tbl_idx++;
	}
}

/* Entire flow for generating U-Boot flash programming script */
void gen_uboot_scr(FILE *scr_ptr, dictionary *ini)
{
	unsigned int part_idx, check_idx, dealt_with;
	struct part_tbl part_tbl_arr;
	struct ini_part_info *ini_part_info_arr;
	char current_file[100];

	ini_convert_arr(&part_tbl_arr, ini);
	ini_part_info_arr = part_tbl_arr.ini_part_info_arr;

	gen_uboot_scr_pre(scr_ptr);

	gen_uboot_mtdparts_cmd(&part_tbl_arr, scr_ptr);
	/* Initialize U-Boot progress env variable */
	gen_uboot_progress(scr_ptr, &part_tbl_arr, 0);

	/* All partition download/program/verify */
	for (part_idx = 0; part_idx < part_tbl_arr.part_num; part_idx++) {
		// Check if there is file to download/program
		if (strlen(ini_part_info_arr[part_idx].file_name) == 0)
			continue;

		strcpy(current_file, ini_part_info_arr[part_idx].file_name);

		/* Check if partition has already been programmed */
		for (check_idx = 0, dealt_with = 0; check_idx < part_idx; check_idx++) {
			if (!strcmp(current_file, ini_part_info_arr[check_idx].file_name)) {
				dealt_with = 1;
				break;
			}
		}

		if (dealt_with == 1)
			continue;

		/* Download current_file from PC */
		gen_uboot_scr_dload(scr_ptr, current_file);

		/* Program all partitions using this file */
		for (check_idx = part_idx; check_idx < part_tbl_arr.part_num; check_idx++){
			if (!strcmp(current_file, ini_part_info_arr[check_idx].file_name)) {
				gen_uboot_scr_program(scr_ptr, ini_part_info_arr[check_idx].nand_start,
							ini_part_info_arr[check_idx].nand_size, ini_part_info_arr[check_idx].label);
				gen_uboot_scr_verify(scr_ptr, ini_part_info_arr[check_idx].nand_start, ini_part_info_arr[check_idx].label);
				/* Print progress */
				gen_uboot_progress(scr_ptr, &part_tbl_arr, 1);
			}
		}
	}

}

/* Entire flow for generating codeviser & jlink flash programming script */
void gen_ice_scr(FILE *jls_ptr, FILE *csf_ptr, dictionary *ini)
{
	unsigned int part_idx, check_idx, dealt_with;
	struct part_tbl part_tbl_arr;
	struct ini_part_info *ini_part_info_arr;
	char current_file[100];

	ini_convert_arr(&part_tbl_arr, ini);
	ini_part_info_arr = part_tbl_arr.ini_part_info_arr;

	/* All partition download */
	for (part_idx = 0; part_idx < part_tbl_arr.part_num; part_idx++) {
		// Check if there is file to download/program
		if (strlen(ini_part_info_arr[part_idx].file_name) == 0)
			continue;

		strcpy(current_file, ini_part_info_arr[part_idx].file_name);

		/* Check if partition has already been programmed */
		for (check_idx = 0, dealt_with = 0; check_idx < part_idx; check_idx++) {
			if (!strcmp(current_file, ini_part_info_arr[check_idx].file_name)) {
				dealt_with = 1;
				break;
			}
		}

		if (dealt_with == 1)
			continue;
		/* Generate ice_script download command */
		fprintf(jls_ptr, "loadbin ../../%s, 0x%08X\n", current_file, ini_part_info_arr[check_idx].memory_start);
		fprintf(csf_ptr, "data.load.binary ../../%s 0x%08X\n", current_file, ini_part_info_arr[check_idx].memory_start);
	}
}

int parse_ini_file(char *ini_name, char *out_name)
{
	dictionary *ini;
	char jls_name[100];
	char csf_name[100];
	char scr_name[100];
	FILE *jls_ptr;
	FILE *csf_ptr;
	FILE *scr_ptr;

	ini = iniparser_load(ini_name);
	if (ini == NULL) {
		fprintf(stderr, "cannot parse file: %s\n", ini_name);
		return -1;
	}
	strcpy(jls_name, out_name);
	strcat(jls_name, ".jls");
	strcpy(csf_name, out_name);
	strcat(csf_name, ".csf");
	strcpy(scr_name, out_name);
	strcat(scr_name, ".scr");

	jls_ptr = fopen(jls_name, "w");
	if (jls_ptr == NULL) {
		fprintf(stderr, "cannot create file: %s\n", jls_name);
		return -1;
	}
	csf_ptr = fopen(csf_name, "w");
	if (csf_ptr == NULL) {
		fprintf(stderr, "cannot create file: %s\n", csf_name);
		return -1;
	}
	scr_ptr = fopen(scr_name, "w");
	if (scr_ptr == NULL) {
		fprintf(stderr, "cannot create file: %s\n", scr_name);
		return -1;
	}

	/* Generate U-Boot flash programming script */
	gen_uboot_scr(scr_ptr, ini);
	/* Generate jlink, codeviser flash download script */
	gen_ice_scr(jls_ptr, csf_ptr, ini);

	DBG("%s generated.\n\n", out_name);

	iniparser_freedict(ini);
	fclose(jls_ptr);
	fclose(csf_ptr);
	fclose(scr_ptr);
	return 0;
}
