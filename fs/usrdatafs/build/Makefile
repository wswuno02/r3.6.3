SDKSRC_DIR ?= $(realpath $(CURDIR)/../../..)
include $(SDKSRC_DIR)/build/sdksrc.mk

include usrdata_ubifs.mk

# JFFs2 flash erase block size
EB_SIZE = 0x10000

# Filesystem image declarations

USRDATAFS_SRC  := $(USRDATAFS)
USRDATAFS_BIN_PATH := $(USRDATAFS_PATH)/bin
USRDATAFS_IMAGE := usrdata.ubifs
USRDATAFS_IMAGE_BIN := usrdata.bin
USRDATAFS_CFG := usrdata_ubifs.cfg

DEST := $(BINPKG_DIR)
INSTALL_TRGTS := $(addprefix $(DEST)/, $(USRDATAFS_IMAGE_BIN))

.PHONY: default
default: all

.PHONY: rebuild
rebuild: clean all

.PHONY: dubuild
dubuild:
	$(Q)mkfs.ubifs -d $(USRDATAFS_SRC) $(USRDATA_UBIFS_OPTS) -o $(USRDATAFS_BIN_PATH)/$(USRDATAFS_IMAGE)
	$(Q)ubinize -o $(USRDATAFS_BIN_PATH)/$(USRDATAFS_IMAGE_BIN) $(USRDATA_UBI_UBINIZE_OPTS) $(USRDATAFS_CFG)

.PHONY: all
all: $(USRDATAFS_BIN_PATH)/$(USRDATAFS_IMAGE_BIN)

.PHONY: install
install: $(DEST) $(USRDATAFS_BIN_PATH) FORCE
	$(Q)install -m 644 $(USRDATAFS_BIN_PATH)/$(USRDATAFS_IMAGE_BIN) $<

.PHONY: uninstall
uninstall:
	$(Q)rm -f $(INSTALL_TRGTS)

.PHONY: clean
clean: uninstall
	$(Q)rm -f $(USRDATAFS_BIN_PATH)/*

.PHONY: distclean
distclean: clean
	$(Q)rm -rf $(USRDATAFS_SRC)

.PHONY: FORCE
FORCE:

$(USRDATAFS_BIN_PATH)/$(USRDATAFS_IMAGE_BIN): $(USRDATAFS_BIN_PATH) $(USRDATAFS_SRC)
	$(Q)mkdir -p $(USRDATAFS_SRC)/active_setting
ifneq ("$(wildcard $(SYSTEMFS)/factory_default/*)","")
	$(Q)cp -rf $(SYSTEMFS)/factory_default/* $(USRDATAFS_SRC)/active_setting/
endif
	$(Q)rm -f $(USRDATAFS_BIN_PATH)/*
ifeq ($(CONFIG_USRDATA_JFFS2), y)
	$(Q)mkfs.jffs2 -d $(USRDATAFS_SRC) -e $(EB_SIZE) -l -o $(USRDATAFS_BIN_PATH)/$(USRDATAFS_IMAGE_BIN)
else
	$(Q)mkfs.ubifs -d $(USRDATAFS_SRC) $(USRDATA_UBIFS_OPTS) -o $(USRDATAFS_BIN_PATH)/$(USRDATAFS_IMAGE)
	$(Q)ubinize -o $(USRDATAFS_BIN_PATH)/$(USRDATAFS_IMAGE_BIN) $(USRDATA_UBI_UBINIZE_OPTS) $(USRDATAFS_CFG)
endif

$(USRDATAFS_BIN_PATH) $(USRDATAFS_SRC) $(DEST):
	$(Q)install -d $@

#===============================================================================
# SDKREL rules
# All rules with "-rel" postfix represents SDK release package rules
#===============================================================================

BINPKG_DIR_REL := $(CURDIR)/../output
BINPKG_DIR_DEBUG_REL := $(CURDIR)/../output_debug
DEST_REL := $(BINPKG_DIR_REL)
DEST_DEBUG_REL := $(BINPKG_DIR_DEBUG_REL)
INSTALL_TRGTS_REL := $(addprefix $(DEST_REL)/, $(USRDATAFS_IMAGE_BIN))
INSTALL_TRGTS_DEBUG_REL := $(addprefix $(DEST_DEBUG_REL)/, $(USRDATAFS_IMAGE_BIN))

.PHONY: rebuild-rel
rebuild-rel: clean-rel all

.PHONY: install-rel
install-rel:
	$(Q)install -d $(DEST_REL) $(DEST_DEBUG_REL)
	$(Q)install -m 644 $(USRDATAFS_BIN_PATH)/$(USRDATAFS_IMAGE_BIN) $(DEST_REL)
	$(Q)install -m 644 $(USRDATAFS_BIN_PATH)/$(USRDATAFS_IMAGE_BIN) $(DEST_DEBUG_REL)

.PHONY: uninstall-rel
uninstall-rel:
	$(Q)rm -f $(INSTALL_TRGTS_REL) $(INSTALL_TRGTS_DEBUG_REL)

.PHONY: clean-rel
clean-rel: uninstall-rel
	$(Q)rm -f $(USRDATAFS_BIN_PATH)/$(USRDATAFS_IMAGE_BIN)

.PHONY: distclean-rel
distclean-rel: clean-rel
	$(Q)rm -rf $(USRDATAFS_SRC)/*
