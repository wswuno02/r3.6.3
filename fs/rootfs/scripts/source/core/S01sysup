#!/bin/sh

#
# AUGENTIX INC. - PROPRIETARY
#
# sysup - System upstart
# Copyright (C) 2018 Augentix Inc. - All Rights Reserved
#
# NOTICE: The information contained herein is the property of Augentix Inc.
# Copying and distributing of this file, via any medium,
# must be licensed by Augentix Inc.
#
# * Brief: Restore U-Boot environement variable to non-update mode
# *
# * Author: ShihChieh Lin <shihchieh.lin@augentix.com>
# *         Carl Su <carl.su@augentix.com>
#

SW_VERSION_FILE=/etc/sw-version
update_file=/usrdata/update_file

FW_PRINTENV=/usr/sbin/fw_printenv
FW_SETENV=/usr/sbin/fw_setenv

BOOTPART=
SYSUPD_TYPE=

USRDATAFS_UBI_DEVID= # Runtime decided
CALIBFS_UBI_DEVID= # Runtime decided

USRDATAFS_PATH=/usrdata
USRDATAFS_VOL_NAME=usrdata

CALIBFS_PATH=/calib
CALIBFS_VOL_NAME=calib

USRDATAFS_MTDID=11
CALIBFS_MTDID=12

reset_file="/usrdata/reset_file"
DB_DEFAULT_PATH="/system/factory_default"
DB_ACTIVE_PATH="/usrdata/active_setting"
DB_CALIB_FLAG="/usrdata/update_cal_done"
SSH_KEY_PATH="/system/ssh/"

DEBUGFS_PATH="/sys/kernel/debug"

# Update sysupd info after reboot
chk_sysupd() {
	new_boot_slot=0
	if [ $BOOTPART == 2 ]; then
		if [ $($FW_PRINTENV -n slot_b_successful) != 1 ]; then
			$FW_SETENV bootcounter 0;
			$FW_SETENV slot_b_successful 1;
			new_boot_slot=1
			active_slot="B"
		fi
	else # Slot A active
		if [ $($FW_PRINTENV -n slot_a_successful) != 1 ]; then
			$FW_SETENV bootcounter 0;
			$FW_SETENV slot_a_successful 1;
			new_boot_slot=1
			active_slot="A"
		fi
	fi

	if [ $new_boot_slot == 1 ]; then
		echo -e "[SYSUPD] System updated successfully!\n"\
		        "\tCurrently active: Slot $active_slot"
	fi
}

print_version() {
	if [ -f $SW_VERSION_FILE ]; then
		printf "Software Version: "; cat $SW_VERSION_FILE
	fi
}

mount_usrdata() {
	USRDATAFS_MTDID=$USRDATAFS_MTDID
	echo "Mount Usrdatafs from /dev/mtd$USRDATAFS_MTDID (UBI$USRDATAFS_UBI_DEVID)"
	ubiattach /dev/ubi_ctrl -m $USRDATAFS_MTDID
	mount -t ubifs ubi$USRDATAFS_UBI_DEVID:$USRDATAFS_VOL_NAME $USRDATAFS_PATH
}

unmount_usrdata() {
	umount $USRDATAFS_PATH
	ubidetach -d $USRDATAFS_UBI_DEVID
}

mount_calib() {
	CALIBFS_MTDID=$CALIBFS_MTDID
	echo "Mount Calibfs from /dev/mtd$CALIBFS_MTDID (UBI$CALIBFS_UBI_DEVID)"
	ubiattach /dev/ubi_ctrl -m $CALIBFS_MTDID
	mount -t ubifs ubi$CALIBFS_UBI_DEVID:$CALIBFS_VOL_NAME $CALIBFS_PATH
}

unmount_calib() {
	umount $CALIBFS_PATH
	ubidetach -d $CALIBFS_UBI_DEVID
}

mount_debugfs() {
	mount -t debugfs nondev $DEBUGFS_PATH
}

unmount_debugfs() {
    umount $DEBUGFS_PATH
}

mountfs() {
	mount_usrdata
	# Calib and systemfs is not required for sysupd OS
	if [ $SYSUPD_TYPE == "dual" ] || [ $BOOTPART == 1 ] ; then
		mount_calib
	fi
	mount_debugfs
}

unmountfs() {
	unmount_debugfs
	if [ $SYSUPD_TYPE == "dual" ] || [ $BOOTPART == 1 ] ; then
		unmount_calib
	fi
	unmount_usrdata
}

start_update() {
	if [ -e "$update_file" ]; then
		cp -f $DB_DEFAULT_PATH/prio.conf $DB_ACTIVE_PATH/prio.conf
	fi
}

start_reset() {
	# Reset all user preferences
	if [ -e "$reset_file" ]; then
		echo "Finishing user data reset..."
		# SSH KEY
		rm -rf $SSH_KEY_PATH
		# Remove active settings
		rm -rf $DB_ACTIVE_PATH/*

		# Restore SSH config
		cp -f $DB_DEFAULT_PATH/sshd_config $DB_ACTIVE_PATH/sshd_config
		cp -f $DB_DEFAULT_PATH/ssh_config $DB_ACTIVE_PATH/ssh_config
		# Restore HTTP server config
		cp -rf $DB_DEFAULT_PATH/nginx $DB_ACTIVE_PATH/nginx
		# Restore timezone
		cp -f $DB_DEFAULT_PATH/TZ $DB_ACTIVE_PATH/TZ
		# Restore Time server config
		cp -f $DB_DEFAULT_PATH/prio.conf $DB_ACTIVE_PATH/prio.conf
		cp -f $DB_DEFAULT_PATH/sntp.conf $DB_ACTIVE_PATH/sntp.conf
		cp -f $DB_DEFAULT_PATH/timeMode.conf $DB_ACTIVE_PATH/timeMode.conf
		cp -f $DB_DEFAULT_PATH/TimeSwitch.conf $DB_ACTIVE_PATH/TimeSwitch.conf
		# Restore database
		cp -f $DB_DEFAULT_PATH/ini.db $DB_ACTIVE_PATH/ini.db
		# Restore network i/f setting
		cp -f $DB_DEFAULT_PATH/interfaces $DB_ACTIVE_PATH/interfaces
		# Restore audio config setting
		cp -f $DB_DEFAULT_PATH/asound.conf $DB_ACTIVE_PATH/asound.conf
		# Restore wifi wpa_supplicant.conf setting
		cp -f $DB_DEFAULT_PATH/wpa_supplicant.conf $DB_ACTIVE_PATH/wpa_supplicant

		# Reload calibration data, please refer to ccserver.c
		if [ -e $DB_CALIB_FLAG ]; then
			rm -f $DB_CALIB_FLAG
		fi

		# Synchronize data on disk with memory
		sync

		# Remove reset flag
		rm -f "$reset_file"
		# Reset factory done
		echo " - User data reset finished"
	fi
}

#################
#  Script Entry #
#################

# Check booting slot
if [ $($FW_PRINTENV -n slot_b_active) == 1 ]; then
	BOOTPART=2;
else
	BOOTPART=1;
fi

# Get SYSUPD_TYPE and UBIFS index
SYSUPD_TYPE=$($FW_PRINTENV -n sysupd_type)
if [ -d /sys/class/ubi/ubi0 ]; then
	USRDATAFS_UBI_DEVID=1
	CALIBFS_UBI_DEVID=2
else
	USRDATAFS_UBI_DEVID=0
	CALIBFS_UBI_DEVID=1
fi

case "$1" in
	start)
		chk_sysupd
		mountfs
		print_version
		start_reset
		start_update
	;;
	stop|restart)
		unmountfs
	;;
	*)
	echo "Usage: $0 {start|stop|restart}"
	exit 1
esac
exit 0

