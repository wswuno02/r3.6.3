#!/bin/sh
#
#  AUGENTIX INC. - PROPRIETARY
#
#  automount.sh - Automount script for removable media devices
#  Copyright (C) 2019 Augentix Inc. - All Rights Reserved
#
#  NOTICE: The information contained herein is the property of Augentix Inc.
#  Copying and distributing of this file, via any medium,
#  must be licensed by Augentix Inc.
#
#  * Author: ShihChieh Lin <shihchieh.lin@augentix.com>
#

# Mounting points
sdcard_destdir=/mnt/sdcard
usb_destdir=/mnt/usb

# Card insertion event flag
sdcd_flag="/tmp/sdcd"

# Log file for pluggable devices
logfile=/tmp/plog
syslog_cfg=/tmp/syslog.conf
syslog_cfg_bak=/tmp/syslog0.conf

#current_time=$(date "+%Y%m%d%H%M%S")
new_log_name="0.log"

LOGGING=/etc/init.d/S03logging

gen_logname()
{

   num=0
   dir=$1

   for file_name in `ls $dir | grep .log`; do


         end_idx=$(expr index $file_name '.log')
         end_idx=$(expr $end_idx - 1)
         file_num=${file_name:0:$end_idx}
         check=`echo "$file_num" | grep -E ^\-?[0-9]*\.?[0-9]+$`

        if [ "$check" != '' ]; then
          #it is numeric
          if [ "$file_num" -ge "$num" ]; then
            num=$(expr $file_num + 1)
          fi
        fi

   done

   echo "$num.log"
}

auto_mount() {
	local mounted=0
	case $1 in
	"sdcard")
		touch $sdcd_flag
		sleep 1
		if [ -e $sdcd_flag ]; then
			# Attempt to mount p1 first
			if [ -e /dev/${MDEV}p1 ]; then
				fsck.vfat -a /dev/${MDEV}p1
				mount -t vfat /dev/${MDEV}p1  $sdcard_destdir
				if [ $? -eq 0 ]; then
					echo "Mount SD card from /dev/${MDEV}p1" >> $logfile
					mounted=1
				fi
			fi

			# Attempt to mount blk0 if p1 is not mounted
			if [ $mounted -eq 0 ]; then
				fsck.vfat -a /dev/${MDEV}
				mount -t vfat /dev/${MDEV}  $sdcard_destdir
				if [ $? -eq 0 ]; then
					echo "Mount SD card from /dev/${MDEV}" >> $logfile
					mounted=1
				fi
			fi

			# Return if card is not mounted
			if [ $mounted -eq 0 ]; then
				echo "Failed to mount SD card!" >> $logfile
				return 1
			fi

			cp -f $syslog_cfg $syslog_cfg_bak
			if [ -e $sdcard_destdir/DumpLogToCard ]
			then
				$LOGGING stop
				cp -f $syslog_cfg $syslog_cfg_bak
				new_log_name=$(gen_logname $sdcard_destdir/log)
				echo "*.*                       $sdcard_destdir/log/$new_log_name" >> $syslog_cfg
				mkdir -p $sdcard_destdir/log
				$LOGGING start
			fi
		fi
		;;
	"usb")
		echo "Mount USB disk from /dev/$MDEV" >> $logfile
		mount -t auto /dev/$MDEV $usb_destdir
		;;
	*)
		;;
	esac
}

auto_umount() {
	case $1 in
	"sdcard")
		rm -rf $sdcd_flag
		sleep 1
		if [ ! -e $sdcd_flag ]; then
			echo "Unmount SD card from $sdcard_destdir" >> $logfile
			[ -f $syslog_cfg_bak ] && mv -f $syslog_cfg_bak /etc/syslog.conf
			$LOGGING restart
			umount $sdcard_destdir
		fi
		;;
	"usb")
		echo "Unmount USB disk from $usb_destdir" >> $logfile
		umount $usb_destdir
		;;
	*)
		;;
	esac
}

/bin/touch $logfile
if [ "$ACTION" == add ]; then
        auto_mount $1
elif [ "$ACTION" == remove ]; then
    auto_umount $1
else
    :
fi
