################################################################################
#
# nginx-http-flv
#
################################################################################

NGINX_HTTP_FLV_VERSION = 1.2.9
NGINX_HTTP_FLV_SOURCE = v$(NGINX_HTTP_FLV_VERSION).tar.gz
NGINX_HTTP_FLV_SITE = https://github.com/winshining/nginx-http-flv-module/archive
NGINX_HTTP_FLV_LICENSE = BSD-2-Clause
NGINX_HTTP_FLV_LICENSE_FILES = LICENCE
NGINX_HTTP_FLV_DEPENDENCIES = openssl pcre zlib

$(eval $(generic-package))
