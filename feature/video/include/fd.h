/******************************************************************************
*
* opyright (c) Augentix Inc. - All Rights Reserved
*
* Unauthorized copying of this file, via any medium is strictly prohibited.
*
* Proprietary and confidential.
*
******************************************************************************/

#ifndef FD_H_
#define FD_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/** @cond
*/
#include "mpi_base_types.h"
#include "mpi_index.h"
#include "mpi_iva.h"
/** @endcond
*/

#define FD_MIN_OBJ_LIFE_TH (0)
#define FD_MAX_OBJ_LIFE_TH (MPI_IVA_OD_MAX_LIFE)
#define FD_TIME_BUFFER_SIZE_MIN (16)
#define FD_ROI_AREA_MIN (16)
#define FD_PATH_LEN_MAX (128)

/**
 * @brief Enum for data control
 */
typedef enum { FD_DATA_NONE, FD_DATA_SAVE, FD_DATA_LOAD } FD_DATA_CTRL_E;

/**
 * @brief Enum for event type
 */
typedef enum { FD_OBJECT_MONITOR, FD_OBJECT_GUARDIAN, FD_OBJECT_DELIVERY, FD_EVENT_NUM } FD_EVENT_TYPE_E;

/**
 * @brief Enum for object monitor event
 */
typedef enum { FD_OBJECT_ABSENT, FD_OBJECT_PRESENT, FD_OBJECT_BOUNDARY, FD_OBJECT_ENTERING, FD_OBJECT_LEAVING } FD_OBJECT_EVT_E;

/**
 * @brief Struct for foreground detection of interest parameter.
 */
typedef struct {
	UINT8 sensitivity; /* range 0-255 */
	UINT8 boundary_thickness; /* range 0-255 */
	UINT8 quality; /* range 0-255 */
	INT16 obj_life_th;
	UINT32 time_buffer; /* time(s) * fps */
	MPI_RECT_S roi;
	FD_EVENT_TYPE_E event_type;
	INT32 suppression; /**< Enter/leaving event duration(frame) */
} FD_PARAM_S;

/**
 * @brief Struct for fg status.
 */
typedef struct {
	UINT8 fg_object;
	int motion_level;
	int boundary_event;
	int current_evt;
	FD_EVENT_TYPE_E event_type;
} FD_STATUS_S;

typedef VOID (*FD_ALARM_CB)(MPI_WIN idx, const FD_STATUS_S *result, const FD_PARAM_S *param);

INT32 FD_enable(MPI_WIN idx);
INT32 FD_disable(MPI_WIN idx);
INT32 FD_setParam(MPI_WIN idx, const FD_PARAM_S *param);
INT32 FD_getParam(MPI_WIN idx, FD_PARAM_S *param);
INT32 FD_getStat(MPI_WIN idx, FD_STATUS_S *status);
INT32 FD_registerAlarmCb(MPI_WIN idx, FD_ALARM_CB alarm_cb);
INT32 FD_reset(MPI_WIN idx);
INT32 FD_dataCtrl(MPI_WIN idx, const char *data_path, FD_DATA_CTRL_E ctrl);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* FD_H_ */
