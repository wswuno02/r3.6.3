#ifndef PFM_H_
#define PFM_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/** @cond
*/
#include "mpi_base_types.h"
#include "mpi_index.h"
/** @endcond
*/

#define PFM_SENSITIVITY_MIN 0 /**< minimal sensitivity. */
#define PFM_SENSITIVITY_MAX 255 /**< maxmal sensitivity. */
#define PFM_ENDURANCE_MIN 0 /**< minimal endurance. */
#define PFM_REMAINDER_MAX 100
#define PFM_REMAINDER_MIN 0

#define PFM_JIFFY_TIME_UNIT 100

/**
 * @brief Enumeration of pet feeding monsitor alarm.
 */
typedef enum {
	PFM_EVT_EMPTY,
	PFM_EVT_FILLING,
	PFM_EVT_EATING,
	PFM_EVT_FINISH,
} PFM_EVT_E;

/**
 * @brief Structure for the parameters of pet feeding monitor.
 */
typedef struct {
	INT32 sensitivity; /**< The sensitivity of pet feeding monitor. */
	INT32 endurance; /**< The time duration (X 20 Frames) for pet feeding monitor to determine the finish stage. */
	MPI_RECT_POINT_S roi; /**< The region of interest for PFM event */
} PFM_PARAM_S;

/**
 * @brief Structure for the result of pet feeding monitor.
 */
typedef struct {
	UINT8 remainder; /**< Pet Food remainder level */
	UINT32 timestamp; /**< Reserve. */
	INT32 evt; /**< Pet feeding monitor evt. */
} PFM_DATA_S;

typedef enum {
	PFM_REGIS_ENVIR = 1, /**< Reset whole background */
	PFM_REGIS_FEED = 2, /**< Same as PFM_REGIS_ENVIR*/
} PFM_RESET_E;

typedef VOID (*PFM_EVT_CB)(MPI_WIN idx, INT32 alarm, PFM_PARAM_S *param);

/* Interface function prototype */
INT32 PFM_enable(MPI_WIN idx);
INT32 PFM_disable(MPI_WIN idx);
INT32 PFM_setParam(MPI_WIN idx, const PFM_PARAM_S *param);
INT32 PFM_getParam(MPI_WIN idx, PFM_PARAM_S *param);
INT32 PFM_getData(MPI_WIN idx, PFM_DATA_S *data);
INT32 PFM_resetData(MPI_WIN idx, PFM_RESET_E reset_val);
INT32 PFM_regEvtCallback(MPI_WIN idx, PFM_EVT_CB evt_cb_fptr);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* PFM_H_ */
