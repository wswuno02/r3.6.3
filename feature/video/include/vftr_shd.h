/******************************************************************************
*
* Copyright (c) Augentix Inc. - All Rights Reserved
*
* Unauthorized copying of this file, via any medium is strictly prohibited.
*
* Proprietary and confidential.
*
******************************************************************************/

/**
 * @file vftr_shd.h
 * @brief Core feature-lib for shaking object detection (SHD)
 */

#ifndef VFTR_SHD_H_
#define VFTR_SHD_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/** 
 * @cond
 */

#include "mpi_base_types.h"
#include "mpi_iva.h"

/** 
 * @endcond
 */

#define VFTR_SHD_LONGTERM_NUM (10) /**< Support long term object number */
#define IVA_JIF_HZ (100) /**< IVA jiffer time (in Hz) */

#define VFTR_SHD_MIN_OBJ_LIFE_TH (0)
#define VFTR_SHD_MAX_OBJ_LIFE_TH (MPI_IVA_OD_MAX_LIFE)

#define VFTR_SHD_MAX_SEN_MIN_TH (1)
#define VFTR_SHD_MAX_SEN_MAX_TH (100)
#define VFTR_SHD_MAX_QUA_MIN_TH (1)
#define VFTR_SHD_MAX_QUA_MAX_TH (100)
#define VFTR_SHD_MAX_LONGTERM_ACTIVE_MIN_TH (1)

/** 
 * @cond
 */

typedef struct vftr_shd_algo_status_s VFTR_SHD_ALGO_STATUS_S;

/** 
 * @endcond
 */

/**
 * @brief Struct for shd long term attributes
 */
typedef struct {
	MPI_RECT_POINT_S rgn;
} VFTR_SHD_LT_OBJ_ATTR_S;

/**
 * @brief Struct for shd long term list
 */
typedef struct {
	int num;
	VFTR_SHD_LT_OBJ_ATTR_S item[VFTR_SHD_LONGTERM_NUM];
} VFTR_SHD_LONGTERM_LIST_S;

/**
 * @brief Struct for object shaking detection parameter
 */
typedef struct {
	INT32 en;
	UINT8 sensitivity;
	UINT8 quality;
	UINT8 longterm_life_th; /**< Minimum threshold for registered list item to be activated */
	UINT16 obj_life_th;
	UINT16 instance_duration; /**< instance object duration 0.1sec */
	UINT32 shaking_update_duration; /**< shaking object update duration 1sec */
	UINT32 longterm_dec_period; /**< registered list decrement period 1sec */
} VFTR_SHD_PARAM_S;

/**
 * @brief Struct for shd status.
 */
typedef struct {
	MPI_RECT_POINT_S shake_rect[MPI_IVA_MAX_OBJ_NUM]; /**< Shaking region */
	UINT8 shaking[MPI_IVA_MAX_OBJ_NUM]; /**< Detection result */
} VFTR_SHD_STATUS_S;

typedef struct {
	VFTR_SHD_PARAM_S param;
	VFTR_SHD_STATUS_S status;
	VFTR_SHD_ALGO_STATUS_S *algo_status;
} VFTR_SHD_INSTANCE_S;

VFTR_SHD_INSTANCE_S *VFTR_SHD_newInstance(void);
INT32 VFTR_SHD_deleteInstance(VFTR_SHD_INSTANCE_S **instance);
INT32 VFTR_SHD_setParam(VFTR_SHD_INSTANCE_S *instance, const VFTR_SHD_PARAM_S *param);
INT32 VFTR_SHD_getParam(const VFTR_SHD_INSTANCE_S *instance, VFTR_SHD_PARAM_S *param);
INT32 VFTR_SHD_detectShake(VFTR_SHD_INSTANCE_S *instance, const MPI_IVA_OBJ_LIST_S *obj_list, VFTR_SHD_STATUS_S *status);
INT32 VFTR_SHD_getStat(const VFTR_SHD_INSTANCE_S *instance, VFTR_SHD_STATUS_S *status);
INT32 VFTR_SHD_setUserLongTermList(VFTR_SHD_INSTANCE_S *instance, const VFTR_SHD_LONGTERM_LIST_S *lt_list);
INT32 VFTR_SHD_getUserLongTermList(const VFTR_SHD_INSTANCE_S *instance, VFTR_SHD_LONGTERM_LIST_S *lt_list);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* VFTR_SHD_H_ */
