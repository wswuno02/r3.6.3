/******************************************************************************
*
* opyright (c) Augentix Inc. - All Rights Reserved
*
* Unauthorized copying of this file, via any medium is strictly prohibited.
*
* Proprietary and confidential.
*
******************************************************************************/

/**
 * @file vftr_md.h
 * @brief Core feature-lib for motion detection
 */

#ifndef VFTR_MD_H_
#define VFTR_MD_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "mpi_base_types.h"
#include "mpi_iva.h"

#define VFTR_MD_MAX_THR_V_OBJ (255)
#define VFTR_MD_MIN_THR_V_OBJ (0)
#define VFTR_MD_MAX_THR_V_REG(W, H) ((W) * (H)*VFTR_MD_MAX_THR_V_OBJ)
#define VFTR_MD_MIN_THR_V_REG (0)
#define VFTR_MD_MAX_REG_NUM (64)
#define VFTR_MD_MIN_REG_NUM (0)
#define VFTR_MD_MAX_DET_SUBTRACT_NUM (3)
#define VFTR_MD_MIN_OBJ_LIFE_TH (0)
#define VFTR_MD_MAX_OBJ_LIFE_TH (MPI_IVA_OD_MAX_LIFE)
#define VFTR_MD_THR_V_REG_BIT_SHIFT 8 /**< Left shift of user set thr_v_reg */

typedef UINT16 VFTR_MD_OBJ_SP_BF; /**< Bit-field of object speed. */
typedef UINT64
        VFTR_MD_REG_MOT_BF; /**< Bit-field of region motion. Max bit :VFTR_MD_INSTANCE_SP_BF * UINT32 * MPI_IVA_MAX_OBJ_NUM */

/**
 * @brief Enumeration of motion detection alarm mode.
 */
typedef enum {
	VFTR_MD_ALARM_FALSE = 0, /**< Alarm none. */
	VFTR_MD_ALARM_TRUE = 1, /**< Alarm true. */
} VFTR_MD_ALARM_MODE_E;

/**
 * @brief Enumeration of motion detection detect mode.
 */
typedef enum {
	VFTR_MD_MOVING_AREA = 0, /**< Mode for detecting motion based on moving area. */
	VFTR_MD_MOVING_ENERGY = 1, /**< Mode for detection motion based on moving energy. */
} VFTR_MD_DET_MODE_E;

/**
 * @brief Enumeration of motion detection detect mode.
 */
typedef enum {
	VFTR_MD_DET_NORMAL = 0, /**< Method for detecting motion based on MD_DET_MODE_E */
	VFTR_MD_DET_SUBTRACT = 1, /**< Method for detection motion based on MD_DET_MODE_E with region-0 substraction. */
} VFTR_MD_DET_METHOD_E;

/**
 * @struct VFTR_MD_REG_ATTR_S
 * @brief Struct for region attributes.
 */
typedef struct {
	/* attr */
	INT16 id; /**< ID of region. */
	UINT16 obj_life_th; /**< life threshold of object */
	VFTR_MD_DET_METHOD_E det_method; /**< Detection method */
	MPI_RECT_POINT_S pts; /**< Coordinate of region. */
	VFTR_MD_OBJ_SP_BF
	        thr_v_obj_min; /**< Minimal threshold (unit: pixel/frame) on speed of objects. Range:[0, 255], default value: 5 */
	VFTR_MD_OBJ_SP_BF thr_v_obj_max; /**< Maximal threshold (unit: pixel/frame) on speed of objects, and its range is [0, 255], the default value is 255*/
	VFTR_MD_DET_MODE_E md_mode; /**< Mode of motion detection. default value: MD_MOVING_AREA */
	VFTR_MD_REG_MOT_BF
	thr_v_reg; /**< Rigt shifted minimal motion threshold (unit: pixel2 for MD_MOVING_AREA and pixel3/frame for MD_MOVING_ENERGY) of the region. Range: [0, (region width * region height * 255) >> MD_THR_V_REG_BIT_SHIFT], default value: 500 */
} VFTR_MD_REG_ATTR_S;

/**
 * @struct VFTR_MD_REG_STAT_S
 * @brief Struct for region status.
 */
typedef struct {
	VFTR_MD_ALARM_MODE_E alarm; /**< Alarm of region.*/
	VFTR_MD_REG_MOT_BF
	v_reg; /**< Motion quantity (unit: pixel2 for MD_MOVING_AREA and pixel3/frame for MD_MOVING_ENERGY) in the region */
} VFTR_MD_REG_STAT_S;

/**
 * @struct VFTR_MD_PARAM_S
 * @brief Struct for motion detection parameter.
 */
typedef struct {
	INT32 region_num; /**< Number of motion detection regions. Range: [1:64]*/
	VFTR_MD_REG_ATTR_S attr[VFTR_MD_MAX_REG_NUM]; /**< Attributes list of the motion detection region. */
} VFTR_MD_PARAM_S;

/**
 * @struct VFTR_MD_STATUS_S
 * @brief Struct for motion detection status.
 */
typedef struct {
	INT32 region_num; /**< Number of motion detection regions. Range: [1:64]*/
	VFTR_MD_REG_ATTR_S attr[VFTR_MD_MAX_REG_NUM]; /**< Attributes list of the motion detection region. */
	VFTR_MD_REG_STAT_S stat[VFTR_MD_MAX_REG_NUM]; /**< Status list of the motion detection region. */
} VFTR_MD_STATUS_S;

/**
 * @struct VFTR_MD_INSTANCE_S
 * @brief Struct for motion detection object. 
 * @note VFTR_MD_INSTANCE_S contains the whole context of motion detection,  
 * all function prototype of this file should affect on VFTR_MD_INSTANCE_S
 */
typedef struct {
	VFTR_MD_PARAM_S tmp_param; /**< Motion detection parameter */
	VFTR_MD_STATUS_S status; /**< Motion detection result */
} VFTR_MD_INSTANCE_S;

/* Interface function prototype */
VFTR_MD_INSTANCE_S *VFTR_MD_newInstance();
INT32 VFTR_MD_deleteInstance(VFTR_MD_INSTANCE_S **instance);
INT32 VFTR_MD_setParam(VFTR_MD_INSTANCE_S *instance, const VFTR_MD_PARAM_S *param);
INT32 VFTR_MD_checkParam(const VFTR_MD_PARAM_S *param, const MPI_SIZE_S *res);
INT32 VFTR_MD_getParam(const VFTR_MD_INSTANCE_S *instance, VFTR_MD_PARAM_S *param);
INT32 VFTR_MD_detectMotion(VFTR_MD_INSTANCE_S *instance, const MPI_IVA_OBJ_LIST_S *obj_list, VFTR_MD_STATUS_S *status);
INT32 VFTR_MD_getStat(const VFTR_MD_INSTANCE_S *instance, VFTR_MD_STATUS_S *stat);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* VFTR_MD_H_ */
