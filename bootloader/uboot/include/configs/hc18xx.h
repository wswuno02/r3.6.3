/*
 * (C) Copyright 2007-2008
 * Stelian Pop <stelian@popies.net>
 * Lead Tech Design <www.leadtechdesign.com>
 *
 * Configuation settings for the AT91SAM9260EK & AT91SAM9G20EK boards.
 *
 * SPDX-License-Identifier: GPL-2.0+
 */

#ifndef __CONFIG_H
#define __CONFIG_H

#include "../../../include/common/config.h"

/* #define DEBUG_EFI 1 */

/* commands to include */
#define CONFIG_CMD_CACHE

/* MTD support */
#define CONFIG_CMD_MTDPARTS
#define CONFIG_CMD_MTD_DEVICE
#define CONFIG_CMD_MTD_PARTITIONS
#define CONFIG_MTD_DEVICE
#define CONFIG_MTD_PARTITIONS

/*
 * SoC must be defined first, before hardware.h is included.
 * In this case SoC is defined in boards.cfg.
 */
#define CONFIG_ARCH_HC18XX
#define CONFIG_MACH_TYPE MACH_TYPE_HC18XX

/*
 * Exception vectors are to be place at 0xFFFF0000, which is an SRAM address
 */
#define CONFIG_SYS_EXCEPTION_VECTORS_HIGH 1

/*
 * Image locations
 *
 * Warning: changing CONFIG_SYS_TEXT_BASE requires
 * adapting the initial boot program.
 * Since the linker has to swallow that define, we must use a pure
 * hex number here!
 */
#define CONFIG_SYS_TEXT_BASE 0x00008000

/* GPIO definitions */
#define PULL_NONE 0
#define PULL_UP 1
#define PULL_DOWN 2
#define INPUT 0
#define OUTPUT 1

#define IOSEL_BASE 0x0
#define IOSEL_OFFS 0x1 /* Offset between each iosel register */
#define GPIO_BASE 0x50 /* Offset to gpio setting registers */
#define GPIO_OFFS 0x4 /* Offset between each gpio register */

/* PWM definitions */
#define PWMX_MODE_CONT 0x00
#define PWMX_MODE_FIXC 0x01

#define NUM_PWM_KER 6
#define NUM_PWM_OUT 12
#define HC_PERIOD_BASE 1000000000
#define HC_PRESCALER_BIT 5
#define HC_COUNT_PERIOD_BIT 8

#define SYS_CLK_FREQ 24000000

/* Region PWM */
#define PWMX_TRIG_OFFSET 0x00
#define PWMX_IRQC_OFFSET 0x02
#define PWMX_BUSY_OFFSET 0x04
#define PWMX_STAT_OFFSET 0x06
#define PWMX_IRQM_OFFSET 0x08
#define PWMX_MODE_OFFSET 0x0A
#define PWMX_PRSC_OFFSET 0x0C
#define PWMX_CNTP_OFFSET 0x0D
#define PWMX_CNTH_OFFSET 0x0E
#define PWMX_NUMP_OFFSET 0x0F
#define PWMO_SEL0_OFFSET 0x60

/*
 * Image layout in main memory
 */
#define UBOOT_START_ADDR 0x00008000
#define UBOOT_END_ADDR 0x00080000

#define KERNEL_ADDR 0x00080000
#define DTB_ADDR 0x01000000
#define DTB_SIZE 0x00020000

#define ROOTFS_ADDR -

/*
 * ARM asynchronous clock
 */
#define CONFIG_SYS_HC18XX_MAIN_CLOCK 24000000 /* main clock xtal */

/*
 * Timer settings
 */
#define TIMER_TRIG 0x80070000
#define TIMER_MODE 0x80070018
#define TIMER_STAT 0x80070010
#define TIMER_CNT 0x80070014
#define TIMER_CNT_TH 0x8007001C

#define CONFIG_SYS_TIMER_COUNTER TIMER_CNT
#define CONFIG_SYS_TIMER_COUNTS_DOWN

#define CONFIG_NR_DRAM_BANKS 1
#define CONFIG_SYS_SDRAM_BASE 0x00000000
#ifdef CONFIG_HC18XX_2GDRAM
#define CONFIG_SYS_SDRAM_SIZE 0x10000000
#elif CONFIG_HC18XX_1GDRAM
#define CONFIG_SYS_SDRAM_SIZE 0x08000000
#else
#define CONFIG_SYS_SDRAM_SIZE 0x04000000
#endif

#define CONFIG_SYS_LOAD_ADDR KERNEL_ADDR /* load address */
#define CONFIG_SYS_MEMTEST_START CONFIG_SYS_SDRAM_BASE
#define CONFIG_SYS_MEMTEST_END \
	CONFIG_SYS_SDRAM_BASE  \
	+CONFIG_SYS_SDRAM_SIZE

/*
 * Initial stack pointer END address before relocation
 */
#define CONFIG_SYS_INIT_SP_ADDR UBOOT_END_ADDR - GENERATED_GBL_DATA_SIZE

/* Misc CPU related */
#define CONFIG_ARCH_CPU_INIT
#define CONFIG_CMDLINE_TAG /* enable passing of ATAGs */
#define CONFIG_SETUP_MEMORY_TAGS
#define CONFIG_INITRD_TAG
#define CONFIG_DISPLAY_CPUINFO
#define CONFIG_SYS_GENERIC_GLOBAL_DATA 1

/*#define CONFIG_CMD_BOOTZ*/

/* serial console */
#define UART_CLOCK (133250000)
#define CONFIG_HC18XX_SERIAL
#define HC18XX_IOMUX_BASEADDR0 0x80000000
#define HC18XX_SERIAL_BASEADDR0 0x88800000
#define HC18XX_PWM_BASEADDR0 0x805F0000
#define CONFIG_BAUDRATE 115200

#define CONFIG_BOOTDELAY 3

/* EMAC */
#define HC18XX_EMAC_BASE_ADDR 0x88300000
#define HC18XX_EMACC_BASE_ADDR 0x805E0000
#define HC18XX_EMAC_CLK_BASE_ADDR 0x80010000

/*
 * BOOTP options
 */
#define CONFIG_BOOTP_BOOTFILESIZE 1
#define CONFIG_BOOTP_BOOTPATH 1
#define CONFIG_BOOTP_GATEWAY 1
#define CONFIG_BOOTP_HOSTNAME 1

/*
 * Environment settings
 * Note that the settings below must also apply to /etc/fw_env.config
 */

/* MTD Nand flash params */
#define CONFIG_SYS_MAX_NAND_DEVICE 1
#define CONFIG_SYS_MAX_FLASH_BANKS 1000
#define CONFIG_ENV_OVERWRITE
#define CONFIG_ENV_IS_IN_NAND
#define CONFIG_ENV_OFFSET 0x00080000
#define CONFIG_ENV_OFFSET_REDUND 0x000C0000
#define CONFIG_ENV_SIZE 0x00020000
#define CONFIG_ENV_RANGE 0x00040000
/* #define CONFIG_AUGENTIX_QSPI */

/* Booting NAND flash */
#define BOOT_NAND_DEVID 0
#include <configs/augentix/generated/flash_layout.h>

/*
 * Two-pass stringify precompilation procedure is required
 */
#define STR(str) #str
#define TO_STR(str) STR(str)

/*
 * Boot commands
 */

#define CONFIG_ETHADDR "02:00:00:00:00:00"
#define CONFIG_WIFIADDR "02:00:00:00:00:00"
#define CONFIG_AUGENTIX_UARTBOOT_RANDOM_ETHADDR
#define CONFIG_UDP_CHECKSUM

#ifdef CONFIG_AUGENTIX_SYSUPD_SOLO
#define SYSUPD_TYPE "solo"
#define NANDROOT_CMD                         \
	"setenv nandroot "                   \
	"root=/dev/mtdblock${rootfs_mtdid} " \
	"rootfstype=squashfs ro rootwait=1; "
#define NANDROOT_SQUASHFS_CMD                \
	"setenv nandroot "                   \
	"root=/dev/mtdblock${rootfs_mtdid} " \
	"rootfstype=squashfs ro rootwait=1; "
#else
#define SYSUPD_TYPE "dual"
#define NANDROOT_CMD                                \
	"setenv nandroot "                          \
	"ubi.mtd=${rootfs_mtdid} root=ubi0:rootfs " \
	"rootfstype=ubifs rw rootwait=1; "
#define NANDROOT_SQUASHFS_CMD                                            \
	"setenv nandroot "                                               \
	"ubi.mtd=${rootfs_mtdid} root=/dev/mtdblock${rootfs_ubi_mtdid} " \
	"rootfstype=squashfs ro rootwait=1;"

#endif /* CONFIG_AUGENTIX_SYSUPD_SOLO */

#define CONFIG_BOOTCOMMAND "run nandboot"
#define CONFIG_EXTRA_ENV_SETTINGS                                                                                                                                                                                                                                                                                                                         \
	"verify=no\0"                                                                                                                                                                                                                                                                                                                                     \
	"initrd_high=0xffffffff\0"                                                                                                                                                                                                                                                                                                                        \
	"bootcounter=0\0"                                                                                                                                                                                                                                                                                                                                 \
	"sysupd_type=" SYSUPD_TYPE "\0"                                                                                                                                                                                                                                                                                                                   \
	"factory_reset=0\0"                                                                                                                                                                                                                                                                                                                               \
	"slot_a_active=1\0"                                                                                                                                                                                                                                                                                                                               \
	"slot_a_bootable=1\0"                                                                                                                                                                                                                                                                                                                             \
	"slot_a_successful=0\0"                                                                                                                                                                                                                                                                                                                           \
	"slot_b_active=0\0"                                                                                                                                                                                                                                                                                                                               \
	"slot_b_bootable=1\0"                                                                                                                                                                                                                                                                                                                             \
	"slot_b_successful=0\0"                                                                                                                                                                                                                                                                                                                           \
	"ethaddr=" CONFIG_ETHADDR "\0"                                                                                                                                                                                                                                                                                                                    \
	"wifiaddr=" CONFIG_WIFIADDR "\0"                                                                                                                                                                                                                                                                                                                  \
	"kernel_memaddr=" TO_STR(                                                                                                                                                                                                                                                                                                                         \
	        KERNEL_ADDR) "\0"                                                                                                                                                                                                                                                                                                                         \
	                     "kernel_0_flashaddr=" TO_STR(                                                                                                                                                                                                                                                                                                \
	                             LINUX_0_NAND_START) "\0"                                                                                                                                                                                                                                                                                             \
	                                                 "kernel_1_flashaddr=" TO_STR(                                                                                                                                                                                                                                                                    \
	                                                         LINUX_1_NAND_START) "\0"                                                                                                                                                                                                                                                                 \
	                                                                             "fdt_memaddr=" TO_STR(                                                                                                                                                                                                                                               \
	                                                                                     DTB_ADDR) "\0"                                                                                                                                                                                                                                               \
	                                                                                               "fdt_size=" TO_STR(                                                                                                                                                                                                                                \
	                                                                                                       DTB_SIZE) "\0"                                                                                                                                                                                                                             \
	                                                                                                                 "fdt_0_flashaddr=" TO_STR(                                                                                                                                                                                                       \
	                                                                                                                         DTB_0_NAND_START) "\0"                                                                                                                                                                                                   \
	                                                                                                                                           "fdt_1_flashaddr=" TO_STR(                                                                                                                                                                             \
	                                                                                                                                                   DTB_1_NAND_START) "\0"                                                                                                                                                                         \
	                                                                                                                                                                     "rootfs_0_mtdid=" TO_STR(                                                                                                                                                    \
	                                                                                                                                                                             ROOTFS_0_MTDID) "\0"                                                                                                                                                 \
	                                                                                                                                                                                             "rootfs_1_mtdid=" TO_STR(                                                                                                                            \
	                                                                                                                                                                                                     ROOTFS_1_MTDID) "\0"                                                                                                                         \
	                                                                                                                                                                                                                     "rootfs_mtdid=\0"                                                                                                            \
	                                                                                                                                                                                                                     "rootfs_ubi_mtdid=" TO_STR(                                                                                                  \
	                                                                                                                                                                                                                             ROOTFS_UBI_MTDID) "\0"                                                                                               \
	                                                                                                                                                                                                                                               "rootfs_ubifs=" TO_STR(                                                                            \
	                                                                                                                                                                                                                                                       CONFIG_ROOTFS_UBIFS) "\0"                                                                  \
	                                                                                                                                                                                                                                                                            "nandroot=\0"                                                         \
	                                                                                                                                                                                                                                                                            "bootargs=\0"                                                         \
	                                                                                                                                                                                                                                                                            "set_boot_slot_a="                                                    \
	                                                                                                                                                                                                                                                                            "echo \"Boot from Slot A\"; "                                         \
	                                                                                                                                                                                                                                                                            "setenv kernel_flashaddr ${kernel_0_flashaddr}; "                     \
	                                                                                                                                                                                                                                                                            "setenv fdt_flashaddr ${fdt_0_flashaddr}; "                           \
	                                                                                                                                                                                                                                                                            "setenv rootfs_mtdid ${rootfs_0_mtdid};\0"                            \
	                                                                                                                                                                                                                                                                            "set_boot_slot_b="                                                    \
	                                                                                                                                                                                                                                                                            "echo \"Boot from Slot B\"; "                                         \
	                                                                                                                                                                                                                                                                            "setenv kernel_flashaddr ${kernel_1_flashaddr}; "                     \
	                                                                                                                                                                                                                                                                            "setenv fdt_flashaddr ${fdt_1_flashaddr}; "                           \
	                                                                                                                                                                                                                                                                            "setenv rootfs_mtdid ${rootfs_1_mtdid};\0"                            \
	                                                                                                                                                                                                                                                                            "set_fallback="                                                       \
	                                                                                                                                                                                                                                                                            "if test ${slot_b_active} = 1; then "                                 \
	                                                                                                                                                                                                                                                                            "if test ${slot_a_bootable} = 1; then "                               \
	                                                                                                                                                                                                                                                                            "setenv slot_a_active 1; "                                            \
	                                                                                                                                                                                                                                                                            "setenv slot_b_active 0; "                                            \
	                                                                                                                                                                                                                                                                            "fi; "                                                                \
	                                                                                                                                                                                                                                                                            "else "                                                               \
	                                                                                                                                                                                                                                                                            "if test ${slot_b_bootable} = 1; then "                               \
	                                                                                                                                                                                                                                                                            "setenv slot_b_active 1; "                                            \
	                                                                                                                                                                                                                                                                            "setenv slot_a_active 0; "                                            \
	                                                                                                                                                                                                                                                                            "fi;  "                                                               \
	                                                                                                                                                                                                                                                                            "fi; "                                                                \
	                                                                                                                                                                                                                                                                            "setenv bootcounter 0;\0"                                             \
	                                                                                                                                                                                                                                                                            "set_nandargs="                                                       \
	                                                                                                                                                                                                                                                                            "if test ${slot_b_active} = 1; then "                                 \
	                                                                                                                                                                                                                                                                            "run set_boot_slot_b; "                                               \
	                                                                                                                                                                                                                                                                            "else "                                                               \
	                                                                                                                                                                                                                                                                            "run set_boot_slot_a; "                                               \
	                                                                                                                                                                                                                                                                            "fi; "                                                                \
	                                                                                                                                                                                                                                                                            "if test ${rootfs_ubifs} = 1; then " NANDROOT_CMD                     \
	                                                                                                                                                                                                                                                                            "else " NANDROOT_SQUASHFS_CMD                                         \
	                                                                                                                                                                                                                                                                            "fi; "                                                                \
	                                                                                                                                                                                                                                                                            "setenv bootargs "                                                    \
	                                                                                                                                                                                                                                                                            "${nandroot} quiet console=ttyAS0,115200 clk_ignore_unused "          \
	                                                                                                                                                                                                                                                                            "ethaddr=${ethaddr} lpj=1191936\0"                                    \
	                                                                                                                                                                                                                                                                            "update_bootenv="                                                     \
	                                                                                                                                                                                                                                                                            "if test ${bootcounter} > 2; then "                                   \
	                                                                                                                                                                                                                                                                            "echo \"Switch boot mode...\" ;"                                      \
	                                                                                                                                                                                                                                                                            "run set_fallback; "                                                  \
	                                                                                                                                                                                                                                                                            "fi; "                                                                \
	                                                                                                                                                                                                                                                                            "if test ${slot_b_active} = 1; then "                                 \
	                                                                                                                                                                                                                                                                            "if test ${slot_b_successful} != 1; then "                            \
	                                                                                                                                                                                                                                                                            "setexpr bootcounter ${bootcounter} + 1; "                            \
	                                                                                                                                                                                                                                                                            "fi; "                                                                \
	                                                                                                                                                                                                                                                                            "else "                                                               \
	                                                                                                                                                                                                                                                                            "if test ${slot_a_successful} != 1; then "                            \
	                                                                                                                                                                                                                                                                            "setexpr bootcounter ${bootcounter} + 1; "                            \
	                                                                                                                                                                                                                                                                            "fi; "                                                                \
	                                                                                                                                                                                                                                                                            "fi; "                                                                \
	                                                                                                                                                                                                                                                                            "saveenv;\0"                                                          \
	                                                                                                                                                                                                                                                                            "nandboot="                                                           \
	                                                                                                                                                                                                                                                                            "run update_bootenv; "                                                \
	                                                                                                                                                                                                                                                                            "run set_nandargs; "                                                  \
	                                                                                                                                                                                                                                                                            "nand read.e ${fdt_memaddr} ${fdt_flashaddr} ${fdt_size}; "           \
	                                                                                                                                                                                                                                                                            "nboot.e ${kernel_memaddr} " TO_STR(                                  \
	                                                                                                                                                                                                                                                                                    BOOT_NAND_DEVID) " ${kernel_flashaddr}; "                     \
	                                                                                                                                                                                                                                                                                                     "fdt addr ${fdt_memaddr}; fdt updp; "        \
	                                                                                                                                                                                                                                                                                                     "bootm ${kernel_memaddr} - ${fdt_memaddr}\0" \
	                                                                                                                                                                                                                                                                                                     "manufacturer=Augentix\0"                    \
	                                                                                                                                                                                                                                                                                                     "model=MTXXX-X\0"                            \
	                                                                                                                                                                                                                                                                                                     "serial_number=00000000\0"                   \
	                                                                                                                                                                                                                                                                                                     "hardware_id=hc1702 MBXXX\0"

/*
 * Skip low level CPU, PLL and DRAM init function,
 * since ROM code and FSBL have handled this part
 */
#define CONFIG_SKIP_LOWLEVEL_INIT

/*
 * Misc system settings
 */
#define CONFIG_SYS_CBSIZE 256
#define CONFIG_SYS_MAXARGS 16
#define CONFIG_SYS_LONGHELP 1
#define CONFIG_CMDLINE_EDITING 1
#define CONFIG_AUTO_COMPLETE

/* NOR flash - no real NOR flash on this board */
#define CONFIG_SYS_NO_FLASH 1

/*
 * Size of malloc() pool
 */
#define CONFIG_SYS_MALLOC_LEN 0x400000

#endif
