/*
 * Configuation settings for hc1703/hc1723/hc1753/hc1783s based products.
 *
 * SPDX-License-Identifier: GPL-2.0+
 */

#ifndef __HC1703_1723_1753_1783_COMMON_CONFIG_H
#define __HC1703_1723_1753_1783_COMMON_CONFIG_H

/* Refers to SDK top-level config */
#include "../../../../../include/common/config.h"

#ifndef CONFIG_ROOTFS_UBIFS
#define CONFIG_ROOTFS_UBIFS 0
#endif

/* clang-format off */
/* memory system */
#define CONFIG_SYS_CACHELINE_SIZE 32

/* commands to include */
#define CONFIG_CMD_CACHE

/* MTD support */
#define CONFIG_CMD_MTDPARTS
#define CONFIG_CMD_MTD_DEVICE
#define CONFIG_CMD_MTD_PARTITIONS
#define CONFIG_MTD_DEVICE
#define CONFIG_MTD_PARTITIONS

/*
 * SoC must be defined first, before hardware.h is included.
 * In this case SoC is defined in boards.cfg.
 */
#define CONFIG_MACH_TYPE MACH_TYPE_HC1703_1723_1753_1783S

/* Watchdog definitions */
#define CONFIG_HC1703_1723_1753_1783S_WATCHDOG
#define CONFIG_AMC_BASEADDR         0x80530000
#define CONFIG_WDT_BASEADDR         0x80550000
#define CONFIG_WDT_CLK              24000000

/* GPIO definitions */
#define PULL_NONE 0
#define PULL_UP   1
#define PULL_DOWN 2
#define INPUT  0
#define OUTPUT 1

#define IOSEL_BASE 0x514
#define IOSEL_OFFS 0x4 /* Offset between each iosel register */
#define IOCFG_BASE 0x400
#define IOCFG_OFFS 0x4 /* Offset between each iocfg register */
#define GPIO_BASE  0x80C /* Offset to gpio setting registers */
#define GPIO_OFFS  0xC /* Offset between each gpio register */

/* PWM definitions */
#define PWMX_MODE_CONT 0x00
#define PWMX_MODE_FIXC 0x01

#define NUM_PWM_KER 6
#define NUM_PWM_OUT 12
#define HC_PERIOD_BASE 1000000000
#define HC_PRESCALER_BIT 5
#define HC_COUNT_PERIOD_BIT 8

#define SYS_CLK_FREQ 1008000000

/* Region PWM */
#define PWMX_TRIG_OFFSET 0x00
#define PWMX_IRQC_OFFSET 0x02
#define PWMX_BUSY_OFFSET 0x04
#define PWMX_STAT_OFFSET 0x06
#define PWMX_IRQM_OFFSET 0x08
#define PWMX_MODE_OFFSET 0x0A
#define PWMX_PRSC_OFFSET 0x0C
#define PWMX_CNTP_OFFSET 0x0D
#define PWMX_CNTH_OFFSET 0x0E
#define PWMX_NUMP_OFFSET 0x0F
#define PWMO_SEL0_OFFSET 0x60
#define PWMO_SEL4_OFFSET 0x64

/*
 * ARM asynchronous clock
 */

/*
 * Timer settings
 */
#define TIMER_TRIG    0x80050000
#define TIMER_MODE    0x80050018
#define TIMER_STAT    0x80050010
#define TIMER_CNT     0x80050014
#define TIMER_CNT_TH  0x8005001C

#define CONFIG_SYS_ARCH_TIMER
#define CONFIG_SYS_HZ_CLOCK   SYS_CLK_FREQ
/*#define CONFIG_SYS_TIMER_COUNTER    TIMER_CNT*/
/*#define CONFIG_SYS_TIMER_COUNTS_DOWN*/


/* Misc CPU related */
#define CONFIG_CMDLINE_TAG      /* enable passing of ATAGs */
#define CONFIG_SETUP_MEMORY_TAGS
#define CONFIG_INITRD_TAG
#define CONFIG_DISPLAY_CPUINFO
#define CONFIG_SYS_GENERIC_GLOBAL_DATA 1

/* serial console */
#define CONFIG_HC18XX_SERIAL
#define HC18XX_SERIAL_BASEADDR0 0x80820000

/* Pinctrl */
#define HC1703_1723_1753_1783S_IO_BASEADDR0    0x80001000 // Starting addr of Region IO registers
#define HC1703_1723_1753_1783S_IOMUX_BASEADDR0 (HC1703_1723_1753_1783S_IO_BASEADDR0 + IOSEL_BASE)
#define HC1703_1723_1753_1783S_IOCFG_BASEADDR0 (HC1703_1723_1753_1783S_IO_BASEADDR0 + IOCFG_BASE)

#define HC18XX_PWM_BASEADDR0 0x80060000

/* EMAC */
#define HC18XX_EMAC_BASE_ADDR       0x81360000
#define HC18XX_EMACC_BASE_ADDR      0x81880000
/*#define HC18XX_EMAC_CLK_BASE_ADDR   0x80010000*/

/*
 * Skip low level CPU, PLL and DRAM init function,
 * since ROM code and FSBL have handled this part
 */
#define CONFIG_SKIP_LOWLEVEL_INIT

/* MTD partition preset of Augentix reference products */

#define MTDIDS_DEFAULT_NAND "nand0=spi32766.0"
#define MTDIDS_DEFAULT_NOR "nor0=spi32766.0"

#define MTDPARTS_NAND_128MB_SLOT_AB "mtdparts=spi32766.0:512k(boot),256k(bootenv_0),256k(bootenv_1),8m(linux_0),48m(rootfs_0),8m(linux_1),48m(rootfs_1),8m(usrdata),-(calib)"

#define MTDPARTS_NAND_32MB_ROOTFS_AB "mtdparts=spi32766.0:512k(boot),256k(bootenv_0),256k(bootenv_1),2560k(linux),11m(rootfs_0),11m(rootfs_1),6656k(usrdata)"

#define MTDPARTS_NAND_32MB_ROOTFS_SINGLE "mtdparts=spi32766.0:512k(boot),256k(bootenv_0),256k(bootenv_1),2560k(linux),22m(rootfs_0),6656k(usrdata)"

#define MTDPARTS_NOR_16MB "mtdparts=spi32766.0:256k(boot),64k(bootenv),2560k(linux),11m(rootfs),2240k(usrdata)"

#define MTDPARTS_NOR_16MB_V2 "mtdparts=spi32766.0:256k(boot),64k(bootenv),2112k(linux),12224k(rootfs),1728k(usrdata)"

#define MTDPARTS_NOR_32MB "mtdparts=spi32766.0:256k(boot),64k(bootenv),2560k(linux),27m(rootfs),2240k(usrdata)"

/* MMC */
#define CONFIG_GENERIC_MMC
#define CONFIG_DWMMC
#define CONFIG_CMD_MMC
#define CONFIG_BOUNCE_BUFFER
#define SDC_BASE 0x818A0000

/* FAT */
#define CONFIG_FS_FAT
#define CONFIG_FAT_WRITE
#define CONFIG_CMD_FAT
#define CONFIG_CMD_FS_GENERIC
#define CONFIG_SUPPORT_VFAT
#define CONFIG_PARTITION_UUIDS
#define CONFIG_DOS_PARTITION
#define CONFIG_CMD_PART

/* clang-format on */
#endif
