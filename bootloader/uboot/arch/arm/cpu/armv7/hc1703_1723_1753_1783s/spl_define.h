#ifndef SPL_DEFINE_H_
#define SPL_DEFINE_H_

/* functions that is only called once in fsbl_s1 */
#define __common __attribute__((section(".common.text")))
#define __common_data __attribute__((section(".common.data")))

#include <stdint.h>

//#define DEBUG
//#define DISPLAY_CLKINFO
#define VERBOSE

/* feature */
#define VERIFY_EFUSE 0

/* UART console */
#define CONSOLE_ID 0

/* boot mode & boot args */
#define BOOT_MODE_FLASH 0
#define BOOT_MODE_UART 1
#define BOOT_MODE_SDC 2
#define BOOT_MODE_JTAG 3

/* memory map */
#define SYS_RAM_BASE 0xFFFF0000
#define SYS_RAM_SIZE 0x1000
#define SYS_ROM_BASE 0xFFFF8000
#define SYS_ROM_SIZE 0x2000

/* error code */
#define ESUCCESS 0x0
#define ETIMEOUT 0x1
#define EFAIL 0x2
#define EPAGE 0x3
#define ECHKSUM 0x4
#define EDATA 0x5

/***************** Debug info **********************/
#define DBG_ADDR 0x8000009C //Dream: it's RESV_0

#define set_progress(i)                                 \
	do {                                            \
		*(volatile uint32_t *)(DBG_ADDR) = (i); \
	} while (0)
/*****************************************************/
/* image data */
typedef struct {
	uint32_t memload;
	uint32_t mem_start;
	uint32_t size;
	uint32_t checksum;
} Image_Descriptor;

/* image table */
typedef struct {
	char imgtbl_magic[8];
	Image_Descriptor img_desc[20];
} Image_Table;

/*********** boot record ********************/
#define SHA224_LEN 28 // (224bit / 8bit) for each byte
#define MODULUS_LEN 256 // 2048-bit modulus
#define SSL_SIGN_LEN 256 // length of signature
typedef struct __attribute__((packed)) boot_record {
	uint8_t br_magic[6];
	uint16_t br_version;
	uint32_t page_offset; //Page offset of FSBL for NAND flash.
	uint32_t address_offset; //Address offset of FSBL for NOR flash.
	uint32_t size;
	uint32_t fsbl_checksum;
	uint8_t fsbl_sign[SSL_SIGN_LEN]; // RSA signature
	uint8_t pubkey1_mod[MODULUS_LEN]; //require hard-coded pubexp 65537
	uint8_t pubkey2_mod[MODULUS_LEN]; //require hard-coded pubexp 65537
	uint8_t pubkey2_mod_sign[SSL_SIGN_LEN]; // RSA signature
	uint32_t br_checksum;
} BootRecord;

typedef struct __attribute__((packed)) Partition_Descriptor {
	uint32_t nvs_start;
	uint32_t nvs_size;
	uint8_t pt_label[16];
} PartDesc;

typedef struct __attribute__((packed)) boot_config {
	uint32_t nvs_type; //0: nand, 1: nor, 2: emmc
	uint32_t part_num; //Number of defined partitions
	uint32_t boot_idx; //Index of the partition that contains image to be executed by FSBL
	uint32_t verify; //Whether to verify images during booting

	PartDesc pt_desc[20];
} BootConfig;

#endif // SPL_DEFINE_H_
