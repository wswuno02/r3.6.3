#include <common.h>

void reset_cpu(ulong ignored) __attribute__((noreturn));

void reset_cpu(ulong ignored)
{
	/*TODO: WDT reset*/
	for (;;)
		;
}

int arch_misc_init(void)
{
	return 0;
}

#if defined(CONFIG_DISPLAY_CPUINFO)
int print_cpuinfo(void)
{
	printf("SoC: Augentix HC1703_1723_1753_1783S\n.");
	return 0;
}
#endif

ulong get_tbclk(void)
{
	return 0;
}
