/*
 * (C) Copyright 2010
 * Reinhard Meyer, reinhard.meyer@emk-elektronik.de
 * (C) Copyright 2009
 * Jean-Christophe PLAGNIOL-VILLARD <plagnioj@jcrosoft.com>
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <asm/io.h>
#include <asm/global_data.h>

#ifndef CONFIG_SYS_HC18XX_MAIN_CLOCK
#define CONFIG_SYS_HC18XX_MAIN_CLOCK 24000000
#endif
/*
static inline unsigned long get_main_clk_rate(void)
{
    DECLARE_GLOBAL_DATA_PTR;
    return gd->arch.main_clk_rate_hz;
}

static inline unsigned long get_mck_clk_rate(void)
{
    DECLARE_GLOBAL_DATA_PTR;
    return gd->arch.mck_rate_hz;
}

static inline unsigned long get_cpu_clk_rate(void)
{
    DECLARE_GLOBAL_DATA_PTR;
    return gd->arch.cpu_clk_rate_hz;
}


int hc18xx_clock_init(unsigned long main_clock)
{
	gd->arch.timer_rate_hz = main_clock;
    return 0;
}
*/

void enable_caches(void)
{
#ifndef CONFIG_SYS_ICACHE_OFF
	icache_enable();
#endif
	/*
#ifndef CONFIG_SYS_DCACHE_OFF
	dcache_enable();
#endif
*/
}

int arch_cpu_init(void)
{
	/*	return hc18xx_clock_init(CONFIG_SYS_HC18XX_MAIN_CLOCK);*/
	enable_caches();
	return 0;
}

void arch_preboot_os(void)
{
}

extern void hw_watchdog_reboot(void);

void reset_cpu(ulong ignored)
{
	hw_watchdog_reboot();
	/* never reached */
	while (1)
		;
}

#if defined(CONFIG_DISPLAY_CPUINFO)
int print_cpuinfo(void)
{
	return 0;
}
#endif
/*
ulong get_tbclk(void)
{
	return 0;
}
*/
