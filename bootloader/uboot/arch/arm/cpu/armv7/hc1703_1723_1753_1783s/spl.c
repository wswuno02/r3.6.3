/*
 * (C) Copyright 2021 DreamYeh, Inc. Augentix
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */
#include "address_map.h"

#include "spl_define.h"
#include "dram/hw_dram.h"
#include "machine.h" //it have to below hw_draw.h
#include "csr_bank_syscfg.h"
#include "csr_bank_qspi.h"
#include "csr_bank_qspir.h"
#include "csr_bank_qspiw.h"
#include "qspi/hw_qspi.h"
#include "pll/hw_pll.h"
#include "printf.h"
#include "uart.h"

struct qspi_dev g_qspi = { .base_addr = QSPI_BASE,
	                   .ref_clk_rate = QSPI_REF_CLK_187500KHZ,
	                   .qspi_clk_rate = QSPI_IF_CLK_46875KHZ };

#define IMG_TABLE_SIZE 0x148

//SPL code==================================================
void board_init_f(unsigned long bootflag)
{
	int err;

	//arch_cpu_init();
	err = machine_init();

	printf("======== AUGENTIX FLASH SPL v0.0.10 ========\n");
	printf("DDR = %s, data rate: %d MT/s\n", DDR_NAME, DDR_DATA_RATE);
	printf("DDR TYPE = %s, capacity: %d MB\n", DDR_TYPE, CONFIG_DRAM_CAPACITY);
	if (err < 0) {
		printf("[ERROR] DRAM INIT FAIL, errno: %d\n\n", err);
	} else {
		printf("DRAM INIT PASS, ret: 0x%08x\n\n", err);
	}

	hw_pll_print_info();
}

void hang(void)
{
}

/***************************************************************************************************************

 ***************************************************************************************************************/

int qspi_quad_mode_detection(int flash_type)
{
	uint8_t pattern[8] = { 0 };
	uint32_t id;
	/* address 2040 to 2047 */
	if (flash_type == 0) {
		nand_page_read(&g_qspi, 0);
		nand_read(&g_qspi, 2040, 8, QUAD_MODE);
		qspi_fifo_read(&g_qspi, (uint32_t *)pattern, 8);
	} else {
		nor_read(&g_qspi, 2040, 8, NOR_3BYTE_ADDR_MODE, QUAD_MODE);
		qspi_fifo_read(&g_qspi, (uint32_t *)pattern, 8);
	}

	qspi_read_id(&g_qspi, &id);
	if (id == 0x52211852)
		return DUAL_MODE;

	if (pattern[0] == 0x6A && pattern[1] == 0x6A && pattern[2] == 0xA6 && pattern[3] == 0x6A &&
	    pattern[4] == 0x99 && pattern[5] == 0x95 && pattern[6] == 0x55 && pattern[7] == 0x59) {
		return QUAD_MODE;
	} else {
		return DUAL_MODE;
	}
}
//=======================================================================

#ifdef CONFIG_SPL_BOARD_INIT
/*
	it's spl board init functon, we use it for original FSBL process.
*/

#define Offset_UBOOT_ADDR 0x8000
#define Offset_UBOOT_PART 0x8000
#define Offset_UBOOT_SIZE 512 * 1024

void spl_board_init(void)
{
	uint32_t flash_mode = 0;
	uint32_t quad_dual_mode = DUAL_MODE;
	uint32_t *img;

	const char *QuadDualMode[] = { "None", "Single", "Dual", "Triple", "Quad" };

#ifdef CONFIG_FLASH_NOR
	flash_mode = 1;
	quad_dual_mode = qspi_quad_mode_detection(flash_mode);
	printf("Flash type: NOR and %s lane mode\n", QuadDualMode[quad_dual_mode]);

#else
	flash_mode = 0;
	quad_dual_mode = qspi_quad_mode_detection(flash_mode);
	printf("Flash type: NAND and %s lane mode\n", QuadDualMode[quad_dual_mode]);
#endif

	if (flash_mode == 0) {
		int load_byte_count;
		int byte_count;
		uint32_t *load_addr;
		int page = 0;
		printf("Nand:Load u-boot from part=0x%08x to 0x%08x size=%d\n", Offset_UBOOT_PART, Offset_UBOOT_ADDR,
		       Offset_UBOOT_SIZE);

		load_addr = (uint32_t *)Offset_UBOOT_ADDR;
		page = (Offset_UBOOT_PART >> BYTES_OFFSET_2048);
		load_byte_count = Offset_UBOOT_SIZE;

		while (load_byte_count > 0) {
			byte_count = (load_byte_count > NAND_FLASH_PAGE_SIZE) ? NAND_FLASH_PAGE_SIZE : load_byte_count;

			nand_normal_read(&g_qspi, load_addr, page, byte_count, quad_dual_mode);
			load_addr += byte_count / sizeof(int);
			load_byte_count -= byte_count;
			page++;
		}
	} else if (flash_mode == 1) {
		printf("NOR:Load u-boot from part=0x%08x to 0x%08x\n", Offset_UBOOT_PART, Offset_UBOOT_ADDR);
		nor_normal_read(&g_qspi, (uint32_t *)Offset_UBOOT_ADDR, Offset_UBOOT_PART, Offset_UBOOT_SIZE,
		                NOR_3BYTE_ADDR_MODE, quad_dual_mode);
	}

	disable_i_cache();

	img = (uint32_t *)Offset_UBOOT_ADDR;
	printf("SPL ends; jump to 0x%08x\n", img);

	asm volatile("bx %[img]\n" : : [img] "r"(img) :);

	while (1) {
		//halt at infinite loop
	}
}
#endif

//=========================================================================
unsigned int spl_boot_device(void)
{
	return 0;
}

#ifdef CONFIG_SPL_MMC_SUPPORT
u32 spl_boot_mode(void)
{
	return MMCSD_MODE_FS;
}
#endif

#ifdef CONFIG_SPL_OS_BOOT
int spl_start_uboot(void)
{
	/* boot linux */
	return 0;
}
#endif
