/*
 * (C) Copyright 2007-2008
 * Stelian Pop <stelian@popies.net>
 * Lead Tech Design <www.leadtechdesign.com>
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <asm/io.h>

DECLARE_GLOBAL_DATA_PTR;

/*
 * We're using the AT91CAP9/SAM9 PITC in 32 bit mode, by
 * setting the 20 bit counter period to its maximum (0xfffff).
 * (See the relevant data sheets to understand that this really works)
 *
 * We do also mimic the typical powerpc way of incrementing
 * two 32 bit registers called tbl and tbu.
 *
 * Those registers increment at 1/16 the main clock rate.
 */

#define TIMER_LOAD_VAL 0xffffffff

int timer_init(void)
{
	if (readl(TIMER_STAT) != 0) {
		writel(1, TIMER_TRIG);
	}

	writel(TIMER_LOAD_VAL, TIMER_CNT_TH);
	writel(1, TIMER_MODE);

	writel(1, TIMER_TRIG);
	gd->arch.timer_rate_hz = CONFIG_SYS_HC18XX_MAIN_CLOCK;

	return 0;
}

/*
 * Return the number of timer ticks per second.
 */

ulong get_tbclk(void)
{
	return gd->arch.timer_rate_hz;
}
