/*
 * augentix master QSPI core controller driver
 *
 * Copyright (C) 2017 Nick Lin<nick.lin@augentix.com>
 *
 */

#include <common.h>
#include <dm.h>
#include <errno.h>
#include <malloc.h>
#include <spi.h>
#include <fdtdec.h>
#include <linux/compat.h>
#include <asm/io.h>

DECLARE_GLOBAL_DATA_PTR;

/* Register offsets */
#define HC_SPI_CTRL0		0x00
#define HC_SPI_CTRL1		0x04
#define HC_SPI_SSIENR		0x08
#define HC_SPI_MWCR		0x0C
#define HC_SPI_SER		0x10
#define HC_SPI_BAUDR		0x14
#define HC_SPI_TXFLTR		0x18
#define HC_SPI_RXFLTR		0x1C
#define HC_SPI_TXFLR		0x20
#define HC_SPI_RXFLR		0x24
#define HC_SPI_SR		0x28
#define HC_SPI_IMR		0x2C
#define HC_SPI_ISR		0x30
#define HC_SPI_RISR		0x34
#define HC_SPI_TXOICR		0x38
#define HC_SPI_RXOICR		0x3C
#define HC_SPI_RXUICR		0x40
#define HC_SPI_MSTICR		0x44
#define HC_SPI_ICR		0x48
#define HC_SPI_DMACR		0x4c
#define HC_SPI_DMATDLR		0x50
#define HC_SPI_DMARDLR		0x54
#define HC_SPI_IDR		0x58
#define HC_SPI_VERSION		0x5C
#define HC_SPI_DR		0x60
#define HC_SPI_RDR		0x260
#define HC_SPI_RXSAMDLY		0xF0
#define HC_SPI_SPICTRLR0	0xF4

/* Bit fields in CTRLR0 */
#define SPI_DFS_OFFSET		0
/* SPI mode */
#define SPI_FRF_OFFSET		4
#define SPI_FRF_SPI		0x0
#define SPI_FRF_SSP		0x1
#define SPI_FRF_MICROWIRE	0x2
#define SPI_FRF_RESV		0x3
/* SPI/DSPI/QSPI */
#define SPI_SPI_FRF_SSPI	0x0
#define SPI_SPI_FRF_DSPI	0x1
#define SPI_SPI_FRF_QSPI	0x2

#define SPI_MODE_OFFSET		6
#define SPI_SCPH_OFFSET		6
#define SPI_SCOL_OFFSET		7

#define SPI_TMOD_OFFSET		8
#define SPI_TMOD_MASK		(0x3 << SPI_TMOD_OFFSET)
#define	SPI_TMOD_TR		0x0		/* xmit & recv */
#define SPI_TMOD_TO		0x1		/* xmit only */
#define SPI_TMOD_RO		0x2		/* recv only */
#define SPI_TMOD_EPROMREAD	0x3		/* eeprom read mode */

#define SPI_DFS_32_OFFSET	16
#define SPI_SPI_FRF_OFFSET	21

#define SPI_SLVOE_OFFSET	10
#define SPI_SRL_OFFSET		11
#define SPI_CFS_OFFSET		12

/* Bit fields in SR, 7 bits */
#define SR_MASK			GENMASK(6, 0)	/* cover 7 bits */
#define SR_BUSY			BIT(0)
#define SR_TF_NOT_FULL		BIT(1)
#define SR_TF_EMPT		BIT(2)
#define SR_RF_NOT_EMPT		BIT(3)
#define SR_RF_FULL		BIT(4)
#define SR_TX_ERR		BIT(5)
#define SR_DCOL			BIT(6)

#define RX_TIMEOUT		1000		/* timeout in ms */

#define QSPI_BASE               0x88400000
#define QSPI_MAX_FREQ           12000000 /* Need to modify */
#define QSPI_CLK                24000000 /* Need to modify */
#define QSPI_CUR_FREQ           6000000
#define QSPI_SSI_FREQ           133000000
//#define QSPI_TX_CLOCK         16600000
//#define QSPI_RX_CLOCK         33000000

#define HW_SSI_FIFO_DEPTH       16

struct hc18xx_qspi_slave {
	struct spi_slave slave;
	void __iomem *regs;
	u32 freq;		/* Default frequency */
	u32 mode;
	int bits_per_word;
	u8 cs;			/* chip select pin */
	u8 tmode;		/* TR/TO/RO/EEPROM */
	u8 type;		/* SPI/SSP/MicroWire */
	int len;

	u32 fifo_len;	/* depth of the FIFO buffer */
	u8 fifo_entry;
	void *tx;
	void *tx_end;
	void *rx;
	void *rx_end;
	u32 n_bytes;
	u8 enable_ssi_controller;
	u16 clk_div;
	u32 tx_clk;
	u32 rx_clk;
};

static inline u32 hc_readl(struct hc18xx_qspi_slave *priv, u32 offset)
{
	return __raw_readl(priv->regs + offset);
}

static inline void hc_writel(struct hc18xx_qspi_slave *priv, u32 offset, u32 val)
{
	__raw_writel(val, priv->regs + offset);
}

static inline void spi_enable_chip(struct hc18xx_qspi_slave *priv, int enable)
{
	hc_writel(priv, HC_SPI_SSIENR, (enable ? 1 : 0));
}

static inline void spi_enable_ser(struct hc18xx_qspi_slave *priv, int enable)
{
	hc_writel(priv, HC_SPI_SER, (enable ? 1 : 0));
}

static inline void spi_set_clk(struct hc18xx_qspi_slave *priv, u32 div)
{
	hc_writel(priv, HC_SPI_BAUDR, div);
}

static u32 bigTolittle(u32* val){
	u8* ptr = (u8*)val;
	u32 tmp =0;
	tmp =  (*(ptr+3) <<0 )|(*(ptr+2) <<8 )| (*(ptr+1) <<16 )| ((*ptr) << 24);
	return tmp;
}

static void hc_writer(struct hc18xx_qspi_slave *priv)
{
	u32 txw = 0;
	while ((priv->tx_end - priv->tx) > 0 && priv->fifo_entry < HW_SSI_FIFO_DEPTH) {

		if (priv->n_bytes == 1){
			txw = *(u8 *)(priv->tx);
			hc_writel(priv, HC_SPI_DR, txw);
		    //printf("txw=%x\n",txw);
		}
		else if (priv->n_bytes == 2){
			txw = *(u16 *)(priv->tx);
			hc_writel(priv, HC_SPI_DR, txw);
		}
		else{
			txw = *(u32 *)(priv->tx);
			hc_writel(priv, HC_SPI_DR, bigTolittle(&txw));
		}
		priv->tx += priv->n_bytes;
		priv->fifo_entry += 1;
	}
}

static void hc_reader(struct hc18xx_qspi_slave *priv)
{
	u32 rxw = 0;
	while ((priv->rx_end - priv->rx) > 0) {
		if (priv->n_bytes == 1){
		   rxw = hc_readl(priv, HC_SPI_DR);
		   *(u8 *)(priv->rx) = rxw;
		}
		else if (priv->n_bytes == 2){
		   *(u16 *)(priv->rx) = rxw;
		}
		else if (priv->n_bytes == 4){
		   rxw = hc_readl(priv, HC_SPI_RDR);
		   *(u32 *)(priv->rx) = rxw;
		}
		priv->rx += priv->n_bytes;
	}
}

#if 0
static void wait_tx_done(struct hc18xx_qspi_slave *priv)
{
	  u32 status;
	  u32 umask;
	  u32 expect;

	  /* Wait for TX FIFO empty and BUSY flag cleared */
	  umask = SR_TF_EMPT | SR_BUSY; // check TFE, BUSY
	  expect = SR_TF_EMPT; // expect TFE = 1, BUSY = 0

	  do{
			status = hc_readl(priv, HC_SPI_SR);
		    if ( ( status & umask ) == expect )
		    {
		      break;
		    }
			barrier();
	  } while(1);

}

static void wait_rx_done(struct hc18xx_qspi_slave *priv)
{
	  u32 rx_cnt;
	  u32 timeout = 256;
	  u32 rx_byte_count = 0;

	  if((priv->rx_end - priv->rx) > 0 ){
	  	rx_byte_count = (priv->len/priv->n_bytes);
	    do{
			  rx_cnt = hc_readl(priv, HC_SPI_RXFLR);
		      if (rx_cnt == rx_byte_count)
		      {
		      	//printf("wait_rx_done: %d\n",rx_cnt);
		        break;
		      }
	    } while ( --timeout > 0 );
	  }
}

static int poll_transfer(struct hc18xx_qspi_slave *priv, struct spi_slave *slave, u32 flags)
{
	hc_writer(priv);
	if ((flags & SPI_XFER_BEGIN)){
		hc_writel(priv, HC_SPI_SER, 1);
		wait_tx_done(priv);
	    //printf("wait_tx_done\n");
	}
	wait_rx_done(priv);
	hc_reader(priv);

	return 0;
}
#endif

static int hc_conti_read(struct hc18xx_qspi_slave *priv)
{
	int32_t  timeout = 255;
	uint32_t qspi_edr = (uint32_t)(priv->regs + HC_SPI_RDR);
	uint32_t qspi_rxflr = (uint32_t)(priv->regs + HC_SPI_RXFLR);
	uint32_t qspi_ser = (uint32_t)(priv->regs + HC_SPI_SER);
	int32_t  word_count = priv->len / priv->n_bytes;

	uint32_t *pBuf;
	uint32_t  pEnd;
	uint32_t  left_word_count;
	uint32_t  rx_cnt;
	uint32_t  loop = 0;
	int ret = 0;

	if (word_count > 16) {

		left_word_count = word_count & 0x7;

		pBuf = (uint32_t *)priv->rx;
		pEnd = (uint32_t)(pBuf + (word_count - left_word_count));

		asm volatile (
			"mov r1, %[pBuf]\n"		/* r1 holds address to destination memory */
			"preload_dcache:"
			"ldm r1!, {r2-r5}\n"		/* preload memory to d-cache */
			"ldm r1!, {r2-r5}\n"
			"cmp r1, %[pEnd]\n"		/* check if all memory loaded */
			"bne preload_dcache\n"
			"dummy_run_to_preload_icache:"
			"movs r1, #0\n"			/* r1 holds rxflr */
			"read_start:"
			"movs r1, r1, LSL #29\n"	/* shift rxflr[3] to C bit and rlxlr[2] to N bit */
			"ldmcs %[qspi_edr], {r2-r5}\n"	/* load 4 words if rxflr[3] == 1 */
			"stmcs %[pBuf]!, {r2-r5}\n"	/* store 4 words if rxflr[3] == 1 */
			"ldmcs %[qspi_edr], {r2-r5}\n"	/* load 4 words if rxflr[3] == 1 */
			"stmcs %[pBuf]!, {r2-r5}\n"	/* store 4 words if rxflr[3] == 1 */
			"ldmmi %[qspi_edr], {r2-r5}\n"	/* load 4 words if rxflr[2] == 1 */
			"stmmi %[pBuf]!, {r2-r5}\n"	/* store 4 words if rxflr[2] == 1 */
			"movs r1, r1, LSL #2\n"		/* shift rxflr[1] to C bit and rlxlr[0] to N bit */
			"ldmcs %[qspi_edr], {r2-r3}\n"	/* load 2 words if rxflr[1] == 1 */
			"stmcs %[pBuf]!, {r2-r3}\n"	/* store 2 words if rxflr[1] == 1 */
			"ldmmi %[qspi_edr], {r2}\n"	/* load 1 words if rxflr[0] == 1 */
			"stmmi %[pBuf]!, {r2}\n"	/* store 1 words if rxflr[0] == 1 */
			"check_finish:"
			"cmp %[pBuf], %[pEnd]\n"	/* check if all data copied to memory */
			"beq read_end\n"
			"read_rxflr:"
			"ldr r1, [%[qspi_rxflr]]\n"	/* read rxflr to r1 */
			"trigger_qspi:"
			"cmp %[loop], #0\n"		/* check if first time, send */
			"bne check_timeout\n"
			"mov r2, #1\n"
			"str r2, [%[qspi_ser]]\n"
			"check_timeout:"
			"add %[loop], %[loop], #1\n"	/* check if time out */
			"cmp %[loop], #0x10000\n"
			"bcs read_end\n"
			"b read_start\n"
			"read_end:"
			: [pBuf] "+r" (pBuf),
			  [loop] "+r" (loop)
			: [pEnd] "r" (pEnd),
			  [qspi_edr] "r" (qspi_edr),
			  [qspi_rxflr] "r" (qspi_rxflr),
			  [qspi_ser] "r" (qspi_ser)
			: "cc", "memory", "r1", "r2", "r3", "r4", "r5"
		);

		if (loop == 65537) {
			ret = -1;
		}

		if (left_word_count) {
			priv->rx = (void *)pBuf;

			do {
				rx_cnt = hc_readl(priv, HC_SPI_RXFLR);
				if (rx_cnt == left_word_count) {
					break;
				}
			} while (--timeout > 0);

			hc_reader(priv);
		}
	} else {
		hc_writel(priv, HC_SPI_SER, 1);

		do {
			rx_cnt = hc_readl(priv, HC_SPI_RXFLR);
			if (rx_cnt == word_count) {
				break;
			}
		} while (--timeout > 0);

		hc_reader(priv);
	}

	return ret;
}

static int hc_conti_write(struct hc18xx_qspi_slave *priv)
{
	uint32_t  qspi_edr   = (uint32_t)(priv->regs + HC_SPI_RDR);
	uint32_t  qspi_txflr = (uint32_t)(priv->regs + HC_SPI_TXFLR);
	uint32_t  qspi_ser   = (uint32_t)(priv->regs + HC_SPI_SER);
	int32_t   word_count = ((uint32_t)priv->tx_end - (uint32_t)priv->tx) / priv->n_bytes;
	uint32_t *pData;
	uint32_t  pEnd;
	uint32_t  is_trigger = 0;

	if (word_count > 0) {

		pData = (uint32_t *)priv->tx;
		pEnd  = (uint32_t)priv->tx_end;

		asm volatile (
			"mov r1, %[pData]\n"		/* r1 holds address to source memory */
			"preload_w_dcache:"
			"ldm r1!, {r2-r5}\n"		/* preload memory to d-cache */
			"ldm r1!, {r2-r5}\n"		/* preload memory to d-cache */
			"cmp r1, %[pEnd]\n"		/* check if all memory loaded */
			"bcc preload_w_dcache\n"
			"dummy_round_w:"
			"movs r1, #0\n"			/* r1 holds 0 for pre-load instruction */
			"write_start:"
			"movs r1, r1, LSL #29\n"	/* shift elem_count[3] to C bit and elem_count[2] to N bit */
			"ldmcs %[pData]!, {r2-r5}\n"	/* load 4 words if elem_count[3] == 1 */
			"stmcs %[qspi_edr], {r2-r5}\n"	/* store 4 words if elem_count[3] == 1 */
			"ldmcs %[pData]!, {r2-r5}\n"	/* load 4 words if elem_count[3] == 1 */
			"stmcs %[qspi_edr], {r2-r5}\n"	/* store 4 words if elem_count[3] == 1 */
			"ldmmi %[pData]!, {r2-r5}\n"	/* load 4 words if elem_count[2] == 1 */
			"stmmi %[qspi_edr], {r2-r5}\n"	/* store 4 words if elem_count[2] == 1 */
			"movs r1, r1, LSL #2\n"		/* shift elem_count[1] to C bit and elem_count[0] to N bit */
			"ldmcs %[pData]!, {r2-r3}\n"	/* load 2 words if elem_count[1] == 1 */
			"stmcs %[qspi_edr], {r2-r3}\n"	/* store 2 words if elem_count[1] == 1 */
			"ldmmi %[pData]!, {r2}\n"	/* load 1 words if elem_count[0] == 1 */
			"stmmi %[qspi_edr], {r2}\n"	/* store 1 words if elem_count[0] == 1 */
			"check_w_finish:"
			"cmp %[pData], %[pEnd]\n"	/* check if all data copied to FIFO */
			"beq write_end\n"
			"update_count:"
			"ldr r1, [%[qspi_txflr]]\n"	/* read txflr to r1 */
			"mov r2, #16\n"			/* r2 holds 16 */
			"sub r1, r2, r1\n"		/* elem_count(r1) = 16 - txflr */
			"sub r2, %[pEnd], %[pData]\n"	/* r2 = pEnd - pData */
			"mov r2, r2, LSR #2\n"		/* left_word_count(r2) = r2 >> 2 */
			"cmp r1, r2\n"			/* compare elem_count and left_word_count */
			"movgt r1, r2 \n"		/* Select min of elem_count and left_word_count */
			"trigger_w_qspi:"
			"cmp %[is_trigger], #0\n"		/* check if first time */
			"bne write_start\n"
			"mov %[is_trigger], #1\n"		/* set first time flag */
			"mov r2, #1\n"				/* set first time flag */
			"str r2, [%[qspi_ser]]\n"		/* trigger QSPI HW */
			"b write_start\n"
			"write_end:"
			: [pData] "+r" (pData),
			  [is_trigger] "+r" (is_trigger)
			: [pEnd] "r" (pEnd),
			  [qspi_edr] "r" (qspi_edr),
			  [qspi_txflr] "r" (qspi_txflr),
			  [qspi_ser] "r" (qspi_ser)
			: "cc", "memory", "r1", "r2", "r3", "r4", "r5"
		);
	} else if (word_count == 0) {
		hc_writel(priv, HC_SPI_SER, 1);
	}

	return 0;
}

static int poll_transfer_read(struct hc18xx_qspi_slave *priv, struct spi_slave *slave, u32 flags)
{
	int ret = 0;

	ret = hc_conti_read(priv);

	return ret;
}

static int poll_transfer_write(struct hc18xx_qspi_slave *priv, struct spi_slave *slave, u32 flags)
{
	int ret = 0;

	ret = hc_conti_write(priv);

	return ret;
}

static int setup_ssi_controller(struct hc18xx_qspi_slave *priv,struct spi_slave *slave)
{
	u32 dfs = 0;
	u32 spi_frf = SPI_SPI_FRF_SSPI;
	u32 cr0;
//	u16 clk_div;

	dfs = slave->bDFS_32 ? 32:8;

	spi_enable_ser(priv, 0);
	spi_enable_chip(priv, 0);

//	clk_div = QSPI_CLK / QSPI_CUR_FREQ;
//	clk_div = (clk_div + 1) & 0xfffe;
	spi_set_clk(priv, priv->clk_div);


	if (slave->bQualSPI) {
		spi_frf = SPI_SPI_FRF_QSPI;
	}
	else {
		spi_frf = SPI_SPI_FRF_SSPI;
	}

	/*CTRL0*/
	cr0 = (priv->type << SPI_FRF_OFFSET)       /* SPI/SSP/MicroWire */
	      | (slave->tmode << SPI_TMOD_OFFSET)  /* TR/RO/TO/EEROM */
	      | ((dfs - 1) << SPI_DFS_32_OFFSET)   /* DFS_32 */
	      | (spi_frf << SPI_SPI_FRF_OFFSET);   /* spi_frf */

	hc_writel(priv, HC_SPI_CTRL0, cr0);


	/*CTRL1*/
	if (slave->tmode == SPI_TMOD_TO) {
		hc_writel(priv, HC_SPI_CTRL1, 0);
	}else {
		hc_writel(priv, HC_SPI_CTRL1, slave->cr1);
	}

	/*SPI CTRL0*/
	hc_writel(priv, HC_SPI_SPICTRLR0, slave->spi_cr0);

	/*RX_SAMPLE_DELAY*/
	hc_writel(priv, HC_SPI_RXSAMDLY, slave->rx_sample_dly);

	spi_enable_chip(priv, 1);

	return 0;
}

static inline struct hc18xx_qspi_slave *to_hc18xx_qspi_priv(struct spi_slave *slave)
{
	return container_of(slave, struct hc18xx_qspi_slave, slave);
}

void spi_init(void)
{
}

int spi_cs_is_valid(unsigned int bus, unsigned int cs)
{
	//printf("spi_cs_is_valid\n");
	__raw_writel(0x01010000,0x80000024);
	__raw_writel(0x01010101,0x80000028);
	__raw_writel(0x00050000,0x800000EC);
	__raw_writel(0x00050000,0x800000F0);

	return 1;
}

struct spi_slave *spi_setup_slave(unsigned int bus, unsigned int cs,
				  unsigned int max_hz, unsigned int mode)
{
	struct hc18xx_qspi_slave *priv;
	uint32_t ver;

	if (!spi_cs_is_valid(bus, cs)) {
		printf("cs is not valid\n");
		return NULL;
	}

	priv = spi_alloc_slave(struct hc18xx_qspi_slave, bus, cs);
	if (!priv) {
		printf("cs is not valid\n");
		return NULL;
	}

	priv->regs = (void *)QSPI_BASE;
	priv->mode = mode;
	priv->type = SPI_FRF_SPI;

	ver = __raw_readl(0x80010004) & 0xFF;

	if (ver == 0x00) {
		priv->tx_clk = 11000000;
		priv->rx_clk = 22000000;
	} else {
		priv->tx_clk = 16600000;
		priv->rx_clk = 33000000;
	}

	__raw_writel(0x01010000,0x80000024);
	__raw_writel(0x01010101,0x80000028);
	__raw_writel(0x00050000,0x800000EC);
	__raw_writel(0x00050000,0x800000F0);

#if 0
	printf("0x800000EC = %x\n",__raw_readl(0x800000EC)); /*clkdiv*/
	printf("0x800000F0 = %x\n",__raw_readl(0x800000F0)); /*clkdiv*/
#endif

	return &priv->slave;
}

void spi_free_slave(struct spi_slave *slave)
{
	struct hc18xx_qspi_slave *ss = to_hc18xx_qspi_priv(slave);
	free(ss);
}

int spi_claim_bus(struct spi_slave *slave)
{
	struct hc18xx_qspi_slave *priv = to_hc18xx_qspi_priv(slave);
	priv->fifo_len = HW_SSI_FIFO_DEPTH;
	priv->enable_ssi_controller = 0;
	setup_ssi_controller(priv,slave);

	return 0;
}

void spi_release_bus(struct spi_slave *slave)
{
	printf("spi_release_bus\n");
}

int spi_xfer(struct spi_slave *slave, unsigned int bitlen, const void *dout,
	     void *din, unsigned long flags)
{
	struct hc18xx_qspi_slave *priv = to_hc18xx_qspi_priv(slave);
	const u8 *tx = dout;
	u8 *rx = din;
	int ret = 0;

	priv->len = bitlen;

	if( tx ){
		priv->tx = (void *)tx;
		priv->tx_end = priv->tx + priv->len;
		priv->clk_div = QSPI_SSI_FREQ / priv->tx_clk;
	}
	else if ( rx ){
		priv->rx = rx;
		priv->rx_end = priv->rx + priv->len;
		priv->clk_div = QSPI_SSI_FREQ / priv->rx_clk;
	}
	else{
		printf("Don't support tx=NULL and rx=NULL\n");
		return -EINVAL;
	}
	priv->clk_div = (priv->clk_div + 1) & 0xfffe;
//	printf("spi_xfer: (priv->clk_div) = (%d)\n", priv->clk_div);
//	printf("spi_xfer priv->tx/priv->rx: (%d)/(%d)\n", priv->tx, priv->rx);

	if(!priv->enable_ssi_controller){
		setup_ssi_controller(priv,slave);
		priv->enable_ssi_controller = 1;

		priv->fifo_entry = 0;
	}

    if (slave->bits_per_word == 8) {
		priv->n_bytes = 1;
	} else if (slave->bits_per_word == 16) {
		priv->n_bytes = 2;
	} else if (slave->bits_per_word == 32) {
		priv->n_bytes = 4;
	}

//	printf("spi_xfer= %u\n",flags);

//	poll_transfer(priv, slave, flags);


	if (tx){
		hc_writer(priv);
	}

	if (!flags) {
		if (rx) {
			ret = poll_transfer_read(priv, slave, flags);
		} else {
			ret = poll_transfer_write(priv, slave, flags);
		}
		priv->enable_ssi_controller = 0;
	}

	flags = 0;

	return ret;
}

