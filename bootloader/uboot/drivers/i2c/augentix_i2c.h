/*
 * (C) Copyright 2021 Augentix 
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef __AGTX_I2C_H_
#define __AGTX_I2C_H_

struct i2c_regs {
	u32 i2c_trig;
	u32 i2c_iclr;
	u32 i2c_isr;
	u32 i2c_ier;
	u32 i2c_sr;
	u32 i2c_trans_sr;
	u32 i2c_clk;
	u32 i2c_autoread;
	u32 i2c_addr;
	u32 i2c_bytes;
	u32 i2c_wr0;
	u32 i2c_wr1;
	u32 i2c_wr2;
	u32 i2c_wr3;
	u32 i2c_wr4;
	u32 i2c_wr5;
	u32 i2c_wr6;
	u32 i2c_wr7;
	u32 i2c_rd0;
	u32 i2c_rd1;
};

/* Region offsets for I2C controller. */
#define HC18XX_I2C_TRIG_OFFSET 0x00 /* Trigger I2C transfer, RO */
#define HC18XX_I2C_ICLR_OFFSET 0x04 /* Clear irq state, RO */
#define HC18XX_I2C_ISR_OFFSET 0x08 /* Irq status, RO */
#define HC18XX_I2C_IER_OFFSET 0x0C /* Enable irq, RW */
#define HC18XX_I2C_SR_OFFSET 0x10 /* Status register, RO */
#define HC18XX_I2C_TRANS_SR_OFFSET 0x14 /* Transaction status, RO */
#define HC18XX_I2C_CLK_OFFSET 0x18 /* Clock cycle, RW */
#define HC18XX_I2C_AUTOREAD_OFFSET 0x1C /* Control register, RW */
#define HC18XX_I2C_ADDR_OFFSET 0x20 /* I2C address register, RW */
#define HC18XX_I2C_BYTES_OFFSET 0x24 /* Read or write data length, RW */
#define HC18XX_I2C_WR0_OFFSET 0x28 /* Register for write data, RW */
#define HC18XX_I2C_WR1_OFFSET 0x2C /* Register for write data, RW */
#define HC18XX_I2C_WR2_OFFSET 0x30 /* Register for write data, RW */
#define HC18XX_I2C_WR3_OFFSET 0x34 /* Register for write data, RW */
#define HC18XX_I2C_WR4_OFFSET 0x38 /* Register for write data, RW */
#define HC18XX_I2C_WR5_OFFSET 0x3C /* Register for write data, RW */
#define HC18XX_I2C_WR6_OFFSET 0x40 /* Register for write data, RW */
#define HC18XX_I2C_WR7_OFFSET 0x44 /* Register for write data, RW */
#define HC18XX_I2C_RD0_OFFSET 0x48 /* Register for read data, RW */
#define HC18XX_I2C_RD1_OFFSET 0x4C /* Register for read data, RW */

/* Control register signal difinition */
#define HC18XX_I2C_TRIG_SET 0x01 /* Start transmit */
#define HC18XX_I2C_ICLR_SET 0x01 /* Clear irq status */
#define HC18XX_I2C_ISR_DONE 0x01 /* I2C interrupt*/
#define HC18XX_I2C_IER_DISABLE 0x01 /* Disable interrupt */
#define HC18XX_I2C_IER_ENABLE 0x00 /* Enable interrupt */
#define HC18XX_I2C_SR_BUSY 0x01 /* Bus busy */
#define HC18XX_I2C_ADDR_WRITE 0x00 /* I2C write */
#define HC18XX_I2C_ADDR_READ BIT(8) /* I2C read */
#define HC18XX_I2C_SR_ERRADDR BIT(16) /* Error address */
#define HC18XX_I2C_SR_ERRDATA BIT(17) /* Error data */
#define HC18XX_I2C_CLK_DEFAULT ((120 << 16) | 0x00) /* Frquency 100Khz */
#define HC18XX_I2C_AUTOREAD_ON ((10 << 16) | 0x01) /* Enable autogenread */
#define HC18XX_I2C_AUTOREAD_OFF ((10 << 16) | 0x00) /* Disable autogenread */

/* Define masks */
#define HC18XX_I2C_WRLEN_MASK 0xff
#define HC18XX_I2C_WRNUM_MASK 0xff00
#define HC18XX_I2C_RDLEN_MASK 0xff0000
#define HC18XX_I2C_CLK_MASK 0xff0000

/* Shift macros */
#define HC18XX_I2C_DELAY_CYCLE_SHIFT 0x08
#define HC18XX_I2C_RDLEN_SHIFT 0x10
#define HC18XX_I2C_CLK_SHIFT 0x10

/* Software definition */
#define HC18XX_I2C_DEFAULT_DELAY_CYCLE 0x04
#define HC18XX_I2C_DEFAULT_RATE (100 * 1000)
#define HC18XX_I2C_MAX_RATE (400 * 1000)
#define HC18XX_I2C_MAX_WR_LEN 0x20
#define HC18XX_I2C_MAX_RD_LEN 0x08
#define HC18XX_SYS_24M_HZ (24 * 1000000)
#define HC18XX_I2C_DELAY_CYCLE 0x1E

#define HC18XX_I2C0_SDA_IOSEL_OFFSET 0x1FC /* Bit 2:0 PAD_I2C0_SDA_IOSEL */
#define HC18XX_I2C0_SCL_IOSEL_OFFSET 0x200 /* Bit 2:0 PAD_I2C0_SCL_IOSEL */
#define HC18XX_I2C0_SDA_IOCFG_OFFSET 0xB8 /* Bit 18:16 PAD_I2C0_SDA_PCFG */
#define HC18XX_I2C0_SCL_IOCFG_OFFSET 0xBC /* Bit 18:16 PAD_I2C0_SCL_PCFG */
#define HC18XX_I2C1_SCL_IOSEL_OFFSET 0x14C /* Bit 2:0 PAD_I2C1_SCL_IOSEL */
#define HC18XX_I2C1_SDA_IOSEL_OFFSET 0x150 /* Bit 2:0 PAD_I2C1_SDA_IOSEL */
#define HC18XX_I2C1_SCL_IOCFG_OFFSET 0x3C /* Bit 18:16 PAD_I2C1_SCL_PCFG */
#define HC18XX_I2C1_SDA_IOCFG_OFFSET 0x40 /* Bit 18:16 PAD_I2C1_SDA_PCFG */

#define I2CM0_BASE 0x80070000
#define I2CM1_BASE 0x80080000
#define PIOC_BASE 0x80001400

#define CONFIG_SYS_I2C_BUS_MAX 2

#define	CONFIG_AGTX_I2C_SPEED	100000
#define	CONFIG_AGTX_I2C_SPEED1	100000
#define	CONFIG_AGTX_I2C_SLAVE	0x0
#define	CONFIG_AGTX_I2C_SLAVE1	0x0

/* Worst case timeout for 1 byte is kept as 2ms */
#define I2C_BYTE_TO		(CONFIG_SYS_HZ/500)
#define I2C_STOPDET_TO		(CONFIG_SYS_HZ/500)
#define I2C_BYTE_TO_BB		(I2C_BYTE_TO * 16)

#endif /* __AGTX_I2C_H_ */
