/*
 * (C) Copyright 2017
 * Nick Lin, augentix, nick.lin@augentix.com.
 *
 */

/*
 * augentix ethernet IP driver for U-Boot
 */

#ifndef _HC18XX_EMAC_H_
#define _HC18XX_EMAC_H_
#if 0
#define MACBASE 0x0000            // The Mac Base address offset is 0x0000
#define DMABASE 0x1000            // Dma base address starts with an offset 0x1000



/**********************************************************
 * GMAC registers Map
 * For Pci based system address is BARx + GmacRegisterBase
 * For any other system translation is done accordingly
 **********************************************************/
enum GmacRegisters {
    GmacConfig             = 0x0000,    /* Mac config Register                       */
    GmacFrameFilter        = 0x0004,    /* Mac frame filtering controls              */
    GmacHashHigh           = 0x0008,    /* Multi-cast hash table high                */
    GmacHashLow            = 0x000C,    /* Multi-cast hash table low                 */
    GmacGmiiAddr           = 0x0010,    /* GMII address Register(ext. Phy)           */
    GmacGmiiData           = 0x0014,    /* GMII data Register(ext. Phy)              */
    GmacFlowControl        = 0x0018,    /* Flow control Register                     */
    GmacVlan               = 0x001C,    /* VLAN tag Register (IEEE 802.1Q)           */

    GmacVersion           = 0x0020,    /* GMAC Core Version Register                */
    GmacWakeupAddr        = 0x0028,    /* GMAC wake-up frame filter adrress reg     */
    GmacPmtCtrlStatus     = 0x002C,    /* PMT control and status register           */

#ifdef LPI_SUPPORT
    GmacLPICtrlSts      = 0x0030,    /* LPI (low power idle) Control and Status Register          */
    GmacLPITimerCtrl    = 0x0034,    /* LPI timer control register               */
#endif

    GmacInterruptStatus    = 0x0038,    /* Mac Interrupt ststus register           */
    GmacInterruptMask      = 0x003C,    /* Mac Interrupt Mask register           */

    GmacAddr0High          = 0x0040,    /* Mac address0 high Register                */
    GmacAddr0Low           = 0x0044,    /* Mac address0 low Register                 */
    GmacAddr1High          = 0x0048,    /* Mac address1 high Register                */
    GmacAddr1Low           = 0x004C,    /* Mac address1 low Register                 */
    GmacAddr2High          = 0x0050,    /* Mac address2 high Register                */
    GmacAddr2Low           = 0x0054,    /* Mac address2 low Register                 */
    GmacAddr3High          = 0x0058,    /* Mac address3 high Register                */
    GmacAddr3Low           = 0x005C,    /* Mac address3 low Register                 */
    GmacAddr4High          = 0x0060,    /* Mac address4 high Register                */
    GmacAddr4Low           = 0x0064,    /* Mac address4 low Register                 */
    GmacAddr5High          = 0x0068,    /* Mac address5 high Register                */
    GmacAddr5Low           = 0x006C,    /* Mac address5 low Register                 */
    GmacAddr6High          = 0x0070,    /* Mac address6 high Register                */
    GmacAddr6Low           = 0x0074,    /* Mac address6 low Register                 */
    GmacAddr7High          = 0x0078,    /* Mac address7 high Register                */
    GmacAddr7Low           = 0x007C,    /* Mac address7 low Register                 */
    GmacAddr8High          = 0x0080,    /* Mac address8 high Register                */
    GmacAddr8Low           = 0x0084,    /* Mac address8 low Register                 */
    GmacAddr9High          = 0x0088,    /* Mac address9 high Register                */
    GmacAddr9Low           = 0x008C,    /* Mac address9 low Register                 */
    GmacAddr10High         = 0x0090,    /* Mac address10 high Register               */
    GmacAddr10Low          = 0x0094,    /* Mac address10 low Register                */
    GmacAddr11High         = 0x0098,    /* Mac address11 high Register               */
    GmacAddr11Low          = 0x009C,    /* Mac address11 low Register                */
    GmacAddr12High         = 0x00A0,    /* Mac address12 high Register               */
    GmacAddr12Low          = 0x00A4,    /* Mac address12 low Register                */
    GmacAddr13High         = 0x00A8,    /* Mac address13 high Register               */
    GmacAddr13Low          = 0x00AC,    /* Mac address13 low Register                */
    GmacAddr14High         = 0x00B0,    /* Mac address14 high Register               */
    GmacAddr14Low          = 0x00B4,    /* Mac address14 low Register                */
    GmacAddr15High         = 0x00B8,    /* Mac address15 high Register               */
    GmacAddr15Low          = 0x00BC,    /* Mac address15 low Register                */

    /*Time Stamp Register Map*/
    GmacTSControl              = 0x0700,  /* Controls the Timestamp update logic                         : only when IEEE 1588 time stamping is enabled in corekit            */

    GmacTSSubSecIncr           = 0x0704,  /* 8 bit value by which sub second register is incremented     : only when IEEE 1588 time stamping without external timestamp input */

    GmacTSHigh                = 0x0708,  /* 32 bit seconds(MS)                                          : only when IEEE 1588 time stamping without external timestamp input */
    GmacTSLow                 = 0x070C,  /* 32 bit nano seconds(MS)                                     : only when IEEE 1588 time stamping without external timestamp input */

    GmacTSHighUpdate        = 0x0710,  /* 32 bit seconds(MS) to be written/added/subtracted           : only when IEEE 1588 time stamping without external timestamp input */
    GmacTSLowUpdate         = 0x0714,  /* 32 bit nano seconds(MS) to be writeen/added/subtracted      : only when IEEE 1588 time stamping without external timestamp input */

    GmacTSAddend            = 0x0718,  /* Used by Software to readjust the clock frequency linearly   : only when IEEE 1588 time stamping without external timestamp input */

    GmacTSTargetTimeHigh       = 0x071C,  /* 32 bit seconds(MS) to be compared with system time          : only when IEEE 1588 time stamping without external timestamp input */
    GmacTSTargetTimeLow     = 0x0720,  /* 32 bit nano seconds(MS) to be compared with system time     : only when IEEE 1588 time stamping without external timestamp input */

    GmacTSHighWord          = 0x0724,  /* Time Stamp Higher Word Register (Version 2 only); only lower 16 bits are valid                                                   */
    //GmacTSHighWordUpdate    = 0x072C,  /* Time Stamp Higher Word Update Register (Version 2 only); only lower 16 bits are valid                                            */

    GmacTSStatus            = 0x0728,  /* Time Stamp Status Register                                                                                                       */
#ifdef AVB_SUPPORT
    GmacAvMacCtrl            = 0x0738,  /* AV mac control Register  */
#endif

};


/**********************************************************
 * GMAC DMA registers
 * For Pci based system address is BARx + GmaDmaBase
 * For any other system translation is done accordingly
 **********************************************************/

enum DmaRegisters {
    DmaBusMode        = 0x0000,    /* CSR0 - Bus Mode Register                          */
    DmaTxPollDemand   = 0x0004,    /* CSR1 - Transmit Poll Demand Register              */
    DmaRxPollDemand   = 0x0008,    /* CSR2 - Receive Poll Demand Register               */
    DmaRxBaseAddr     = 0x000C,    /* CSR3 - Receive Descriptor list base address       */
    DmaTxBaseAddr     = 0x0010,    /* CSR4 - Transmit Descriptor list base address      */
    DmaStatus         = 0x0014,    /* CSR5 - Dma status Register                        */
    DmaControl        = 0x0018,    /* CSR6 - Dma Operation Mode Register                */
    DmaInterrupt      = 0x001C,    /* CSR7 - Interrupt enable                           */
    DmaMissedFr       = 0x0020,    /* CSR8 - Missed Frame & Buffer overflow Counter     */
    DmaTxCurrDesc     = 0x0048,    /*      - Current host Tx Desc Register              */
    DmaRxCurrDesc     = 0x004C,    /*      - Current host Rx Desc Register              */
    DmaTxCurrAddr     = 0x0050,    /* CSR20 - Current host transmit buffer address      */
    DmaRxCurrAddr     = 0x0054,    /* CSR21 - Current host receive buffer address       */

#ifdef AVB_SUPPORT
    HwFeature         = 0x0058,    /* Hardware Feature Register                         */

    DmaSlotFnCtrlSts  = 0x0030,    /* Slot function control and status register         */

    DmaChannelCtrl    = 0x0060,    /* Channel Control register only for Channel1 and Channel2 */
    DmaChannelAvSts   = 0x0064,    /* Channel Status register only for Channel1 and  Channel2 */
    IdleSlopeCredit   = 0x0068,    /* Idle slope credit register                              */
    SendSlopeCredit   = 0x006C,    /* Send slope credit register                              */
    HighCredit        = 0x0070,    /* High Credit register                                    */
    LoCredit          = 0x0074,    /* Lo Credit Register                                      */
#endif

};

/**********************************************************
 * DMA Engine registers Layout
 **********************************************************/

/*DmaBusMode               = 0x0000,    CSR0 - Bus Mode */
enum DmaBusModeReg {
    DmaFixedBurstEnable     = 0x00010000,   /* (FB)Fixed Burst SINGLE, INCR4, INCR8 or INCR16   16     RW                */
    DmaFixedBurstDisable    = 0x00000000,   /*             SINGLE, INCR                                          0       */

    DmaTxPriorityRatio11    = 0x00000000,   /* (PR)TX:RX DMA priority ratio 1:1                15:14   RW        00      */
    DmaTxPriorityRatio21    = 0x00004000,   /* (PR)TX:RX DMA priority ratio 2:1                                          */
    DmaTxPriorityRatio31    = 0x00008000,   /* (PR)TX:RX DMA priority ratio 3:1                                          */
    DmaTxPriorityRatio41    = 0x0000C000,   /* (PR)TX:RX DMA priority ratio 4:1                                          */

    DmaBurstLengthx8        = 0x01000000,   /* When set mutiplies the PBL by 8                  24      RW        0      */

    DmaBurstLength256       = 0x01002000,   /*(DmaBurstLengthx8 | DmaBurstLength32) = 256      [24]:13:8                 */
    DmaBurstLength128       = 0x01001000,   /*(DmaBurstLengthx8 | DmaBurstLength16) = 128      [24]:13:8                 */
    DmaBurstLength64        = 0x01000800,   /*(DmaBurstLengthx8 | DmaBurstLength8) = 64        [24]:13:8                 */
    DmaBurstLength32        = 0x00002000,   /* (PBL) programmable Dma burst length = 32        13:8    RW                */
    DmaBurstLength16        = 0x00001000,   /* Dma burst length = 16                                                     */
    DmaBurstLength8         = 0x00000800,   /* Dma burst length = 8                                                      */
    DmaBurstLength4         = 0x00000400,   /* Dma burst length = 4                                                      */
    DmaBurstLength2         = 0x00000200,   /* Dma burst length = 2                                                      */
    DmaBurstLength1         = 0x00000100,   /* Dma burst length = 1                                                      */
    DmaBurstLength0         = 0x00000000,   /* Dma burst length = 0                                               0x00   */

    DmaRxBustLength32       = 0x00400000,   /* (RPBL) programable Dma rx bust length = 32      22:17                     */
    DmaRxBustLength16       = 0x00200000,   /* (RPBL) programable Dma rx bust length = 16      22:17                     */
    DmaRxBustLength8        = 0x00100000,   /* (RPBL) programable Dma rx bust length = 8       22:17                     */
    DmaRxBustLength4        = 0x00080000,   /* (RPBL) programable Dma rx bust length = 4       22:17                     */
    DmaRxBustLength2        = 0x00040000,   /* (RPBL) programable Dma rx bust length = 2       22:17                     */
    DmaRxBustLength1        = 0x00020000,   /* (RPBL) programable Dma rx bust length = 1       22:17                     */

    DmaDescriptor8Words     = 0x00000080,   /* Enh Descriptor works  1=> 8 word descriptor      7                  0    */
    DmaDescriptor4Words     = 0x00000000,   /* Enh Descriptor works  0=> 4 word descriptor      7                  0    */

    DmaDescriptorSkip16     = 0x00000040,   /* (DSL)Descriptor skip length (no.of dwords)       6:2     RW               */
    DmaDescriptorSkip8      = 0x00000020,   /* between two unchained descriptors                                         */
    DmaDescriptorSkip4      = 0x00000010,   /*                                                                           */
    DmaDescriptorSkip2      = 0x00000008,   /*                                                                           */
    DmaDescriptorSkip1      = 0x00000004,   /*                                                                           */
    DmaDescriptorSkip0      = 0x00000000,   /*                                                                    0x00   */

    DmaArbitRr              = 0x00000000,   /* (DA) DMA RR arbitration                            1     RW         0     */
    DmaArbitPr              = 0x00000002,   /* Rx has priority over Tx                                                   */

    DmaResetOn              = 0x00000001,   /* (SWR)Software Reset DMA engine                     0     RW               */
    DmaResetOff             = 0x00000000,   /*                                                                      0    */
};
	
struct hc_eth_dev {
	phys_addr_t iomux_base;
	phys_addr_t emacc_base;
	phys_addr_t emacdma_base;
	phys_addr_t emacclk_base;	
};
#endif

#define CONFIG_TX_DESCR_NUM	16
#define CONFIG_RX_DESCR_NUM	16
#define CONFIG_ETH_BUFSIZE	2048
#define TX_TOTAL_BUFSIZE	(CONFIG_ETH_BUFSIZE * CONFIG_TX_DESCR_NUM)
#define RX_TOTAL_BUFSIZE	(CONFIG_ETH_BUFSIZE * CONFIG_RX_DESCR_NUM)

#define CONFIG_MACRESET_TIMEOUT	(3 * CONFIG_SYS_HZ)
#define CONFIG_MDIO_TIMEOUT	(3 * CONFIG_SYS_HZ)

struct eth_mac_regs {
	u32 conf;		/* 0x00 */
	u32 framefilt;		/* 0x04 */
	u32 hashtablehigh;	/* 0x08 */
	u32 hashtablelow;	/* 0x0c */
	u32 miiaddr;		/* 0x10 */
	u32 miidata;		/* 0x14 */
	u32 flowcontrol;	/* 0x18 */
	u32 vlantag;		/* 0x1c */
	u32 version;		/* 0x20 */
	u8 reserved_1[20];
	u32 intreg;		/* 0x38 */
	u32 intmask;		/* 0x3c */
	u32 macaddr0hi;		/* 0x40 */
	u32 macaddr0lo;		/* 0x44 */
};

/* MAC configuration register definitions */
#define FRAMEBURSTENABLE	(1 << 21)
#define MII_PORTSELECT		(1 << 15)
#define FES_100			(1 << 14)
#define DISABLERXOWN		(1 << 13)
#define FULLDPLXMODE		(1 << 11)
#define RXENABLE		(1 << 2)
#define TXENABLE		(1 << 3)

/* MII address register definitions */
#define MII_BUSY		(1 << 0)
#define MII_WRITE		(1 << 1)
#define MII_CLKRANGE_60_100M	(0)
#define MII_CLKRANGE_100_150M	(0x4)
#define MII_CLKRANGE_20_35M	(0x8)
#define MII_CLKRANGE_35_60M	(0xC)
#define MII_CLKRANGE_150_250M	(0x10)
#define MII_CLKRANGE_250_300M	(0x14)

#define MIIADDRSHIFT		(11)
#define MIIREGSHIFT		(6)
#define MII_REGMSK		(0x1F << 6)
#define MII_ADDRMSK		(0x1F << 11)


struct eth_dma_regs {
	u32 busmode;		/* 0x00 */
	u32 txpolldemand;	/* 0x04 */
	u32 rxpolldemand;	/* 0x08 */
	u32 rxdesclistaddr;	/* 0x0c */
	u32 txdesclistaddr;	/* 0x10 */
	u32 status;		/* 0x14 */
	u32 opmode;		/* 0x18 */
	u32 intenable;		/* 0x1c */
	u32 reserved1[2];
	u32 axibus;		/* 0x28 */
	u32 reserved2[7];
	u32 currhosttxdesc;	/* 0x48 */
	u32 currhostrxdesc;	/* 0x4c */
	u32 currhosttxbuffaddr;	/* 0x50 */
	u32 currhostrxbuffaddr;	/* 0x54 */
};

#define DW_DMA_BASE_OFFSET	(0x1000)

/* Default DMA Burst length */
#ifndef CONFIG_DW_GMAC_DEFAULT_DMA_PBL
#define CONFIG_DW_GMAC_DEFAULT_DMA_PBL 8
#endif

/* Bus mode register definitions */
#define FIXEDBURST		(1 << 16)
#define PRIORXTX_41		(3 << 14)
#define PRIORXTX_31		(2 << 14)
#define PRIORXTX_21		(1 << 14)
#define PRIORXTX_11		(0 << 14)
#define DMA_PBL			(CONFIG_DW_GMAC_DEFAULT_DMA_PBL<<8)
#define RXHIGHPRIO		(1 << 1)
#define DMAMAC_SRST		(1 << 0)

/* Poll demand definitions */
#define POLL_DATA		(0xFFFFFFFF)

/* Operation mode definitions */
#define STOREFORWARD		(1 << 21)
#define FLUSHTXFIFO		(1 << 20)
#define TXSTART			(1 << 13)
#define TXSECONDFRAME		(1 << 2)
#define RXSTART			(1 << 1)

/* Descriptior related definitions */
#define MAC_MAX_FRAME_SZ	(1600)

struct dmamacdescr {
	u32 txrx_status;
	u32 dmamac_cntl;
	void *dmamac_addr;
	struct dmamacdescr *dmamac_next;
} __aligned(ARCH_DMA_MINALIGN);

/*
 * txrx_status definitions
 */

/* tx status bits definitions */
#if defined(CONFIG_DW_ALTDESCRIPTOR)

#define DESC_TXSTS_OWNBYDMA		(1 << 31)
#define DESC_TXSTS_TXINT		(1 << 30)
#define DESC_TXSTS_TXLAST		(1 << 29)
#define DESC_TXSTS_TXFIRST		(1 << 28)
#define DESC_TXSTS_TXCRCDIS		(1 << 27)

#define DESC_TXSTS_TXPADDIS		(1 << 26)
#define DESC_TXSTS_TXCHECKINSCTRL	(3 << 22)
#define DESC_TXSTS_TXRINGEND		(1 << 21)
#define DESC_TXSTS_TXCHAIN		(1 << 20)
#define DESC_TXSTS_MSK			(0x1FFFF << 0)

#else

#define DESC_TXSTS_OWNBYDMA		(1 << 31)
#define DESC_TXSTS_MSK			(0x1FFFF << 0)

#endif

/* rx status bits definitions */
#define DESC_RXSTS_OWNBYDMA		(1 << 31)
#define DESC_RXSTS_DAFILTERFAIL		(1 << 30)
#define DESC_RXSTS_FRMLENMSK		(0x3FFF << 16)
#define DESC_RXSTS_FRMLENSHFT		(16)

#define DESC_RXSTS_ERROR		(1 << 15)
#define DESC_RXSTS_RXTRUNCATED		(1 << 14)
#define DESC_RXSTS_SAFILTERFAIL		(1 << 13)
#define DESC_RXSTS_RXIPC_GIANTFRAME	(1 << 12)
#define DESC_RXSTS_RXDAMAGED		(1 << 11)
#define DESC_RXSTS_RXVLANTAG		(1 << 10)
#define DESC_RXSTS_RXFIRST		(1 << 9)
#define DESC_RXSTS_RXLAST		(1 << 8)
#define DESC_RXSTS_RXIPC_GIANT		(1 << 7)
#define DESC_RXSTS_RXCOLLISION		(1 << 6)
#define DESC_RXSTS_RXFRAMEETHER		(1 << 5)
#define DESC_RXSTS_RXWATCHDOG		(1 << 4)
#define DESC_RXSTS_RXMIIERROR		(1 << 3)
#define DESC_RXSTS_RXDRIBBLING		(1 << 2)
#define DESC_RXSTS_RXCRC		(1 << 1)

/*
 * dmamac_cntl definitions
 */

/* tx control bits definitions */
#if defined(CONFIG_DW_ALTDESCRIPTOR)

#define DESC_TXCTRL_SIZE1MASK		(0x1FFF << 0)
#define DESC_TXCTRL_SIZE1SHFT		(0)
#define DESC_TXCTRL_SIZE2MASK		(0x1FFF << 16)
#define DESC_TXCTRL_SIZE2SHFT		(16)

#else

#define DESC_TXCTRL_TXINT		(1 << 31)
#define DESC_TXCTRL_TXLAST		(1 << 30)
#define DESC_TXCTRL_TXFIRST		(1 << 29)
#define DESC_TXCTRL_TXCHECKINSCTRL	(3 << 27)
#define DESC_TXCTRL_TXCRCDIS		(1 << 26)
#define DESC_TXCTRL_TXRINGEND		(1 << 25)
#define DESC_TXCTRL_TXCHAIN		(1 << 24)

#define DESC_TXCTRL_SIZE1MASK		(0x7FF << 0)
#define DESC_TXCTRL_SIZE1SHFT		(0)
#define DESC_TXCTRL_SIZE2MASK		(0x7FF << 11)
#define DESC_TXCTRL_SIZE2SHFT		(11)

#endif

/* rx control bits definitions */
#if defined(CONFIG_DW_ALTDESCRIPTOR)

#define DESC_RXCTRL_RXINTDIS		(1 << 31)
#define DESC_RXCTRL_RXRINGEND		(1 << 15)
#define DESC_RXCTRL_RXCHAIN		(1 << 14)

#define DESC_RXCTRL_SIZE1MASK		(0x1FFF << 0)
#define DESC_RXCTRL_SIZE1SHFT		(0)
#define DESC_RXCTRL_SIZE2MASK		(0x1FFF << 16)
#define DESC_RXCTRL_SIZE2SHFT		(16)

#else

#define DESC_RXCTRL_RXINTDIS		(1 << 31)
#define DESC_RXCTRL_RXRINGEND		(1 << 25)
#define DESC_RXCTRL_RXCHAIN		(1 << 24)

#define DESC_RXCTRL_SIZE1MASK		(0x7FF << 0)
#define DESC_RXCTRL_SIZE1SHFT		(0)
#define DESC_RXCTRL_SIZE2MASK		(0x7FF << 11)
#define DESC_RXCTRL_SIZE2SHFT		(11)

#endif

struct hc_eth_dev {
	struct dmamacdescr tx_mac_descrtable[CONFIG_TX_DESCR_NUM];
	struct dmamacdescr rx_mac_descrtable[CONFIG_RX_DESCR_NUM];
	char txbuffs[TX_TOTAL_BUFSIZE] __aligned(ARCH_DMA_MINALIGN);
	char rxbuffs[RX_TOTAL_BUFSIZE] __aligned(ARCH_DMA_MINALIGN);

	u32 interface;
	u32 max_speed;
	u32 tx_currdescnum;
	u32 rx_currdescnum;

	struct eth_mac_regs *mac_regs_p;
	struct eth_dma_regs *dma_regs_p;
	phys_addr_t iomux_base;
	phys_addr_t emacc_base;
	phys_addr_t emacclk_base;		
#ifndef CONFIG_DM_ETH
	struct eth_device *dev;
#endif
	struct phy_device *phydev;
	struct mii_dev *bus;
};


#endif /*_HC18XX_EMAC_H_*/
