/*
 * (C) Copyright 2017-2018  Augentix Inc.
 * ShihChieh Lin, Augentix Inc., <shihchieh.lin@augentix.com>
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <asm/io.h>

#if defined(CONFIG_RESET_PHY_R) && defined(CONFIG_MACB)
#include <net.h>
#endif
#include <netdev.h>
#include <miiphy.h>
#include <configs/hc18xx.h>
#include <common.h>
#include <div64.h>

DECLARE_GLOBAL_DATA_PTR;

/* ------------------------------------------------------------------------- */
/*
 * Miscelaneous platform dependent initialisations
 */

extern int hc_emac_initialize(ulong base_addr, u32 interface);
extern void hc18xx_init_led(void);

#ifdef CONFIG_CMD_NAND
static void hc18xx_nand_hw_init(void)
{
}
#endif

#ifdef CONFIG_MACB
static void hc18xx_macb_hw_init(void)
{
	/* Initialize EMAC=MACB hardware */
}
#endif

int board_mmc_init(bd_t *bd)
{
	return 0;
}

void __init_gpio(int id, int direction, int pud)
{
	phys_addr_t base = HC18XX_IOMUX_BASEADDR0;
	u8 tmp;
	writeb(0, (base + IOSEL_BASE + (id * IOSEL_OFFS))); /* GPIO iomux */
	writeb(direction, (base + GPIO_BASE + (id * GPIO_OFFS) + 1)); /* output enable */
	tmp = readb((base + GPIO_BASE + (id * GPIO_OFFS) + 2));
	tmp &= ~(0xC);
	switch (pud) {
	case PULL_UP:
		tmp += 0x4;
		break;
	case PULL_DOWN:
		tmp += 0x8;
		break;
	case PULL_NONE:
	default:
		break;
	}
	writeb(tmp, (base + GPIO_BASE + (id * GPIO_OFFS) + 2)); /* output enable */
}

void __set_gpio_value(int id, int level)
{
	phys_addr_t base = HC18XX_IOMUX_BASEADDR0;
	writeb(level, (base + GPIO_BASE + (id * GPIO_OFFS)));
}

int get_sys_clk(void)
{
	return SYS_CLK_FREQ;
}

int hc_exp2(int exp)
{
	if (exp == 0)
		return 1;
	else
		return 2 * hc_exp2(exp - 1);
}

void __init_pwm(int id, int pwm_id, int io_mux, int period_ns, int duty_ns)
{
	phys_addr_t base = HC18XX_IOMUX_BASEADDR0;
	writeb(io_mux, (base + IOSEL_BASE + (id * IOSEL_OFFS))); /* PWM iomux */

	int cnt_psc = 0;
	unsigned long long period_freq;
	unsigned long long calc_high;
	unsigned long long calc_period;
	unsigned long long mod_prd;
	int max_freq;
	int min_freq;
	unsigned int pwm_addr = HC18XX_PWM_BASEADDR0 + (pwm_id * 0x10);

	period_freq = get_sys_clk();

	max_freq = get_sys_clk() / 2;

	if ((HC_PERIOD_BASE / period_ns) > max_freq) {
		return;
	}

	min_freq = get_sys_clk() / hc_exp2(hc_exp2(HC_PRESCALER_BIT - 1)) / (hc_exp2(HC_COUNT_PERIOD_BIT) - 1);

	if ((HC_PERIOD_BASE / period_ns) < min_freq) {
		return;
	}

	do {
		/*
		 *   period_ns     count_period
		 *  ----------- = --------------
		 *     10^9         frequency
		 *
		 * frequency = 24MHz / (2^prescaler)
		 *
		 * count_period = frequency * period_ns / 10^9
		 */

		calc_period = (unsigned long long)period_freq * period_ns;

		mod_prd = do_div(calc_period, HC_PERIOD_BASE);

		if (calc_period > 255) {
			cnt_psc = cnt_psc + 1;
			period_freq = period_freq / 2;
		} else {
			if (mod_prd > HC_PERIOD_BASE / 2) {
				calc_period = calc_period + 1;
			}

			printf("request frquency: %d Hz\n", HC_PERIOD_BASE / period_ns);
			do_div(period_freq, calc_period);
			printf("provide frquency: %llu Hz\n", period_freq);
		}
	} while (calc_period > 255);

	calc_high = calc_period * duty_ns;
	do_div(calc_high, period_ns);

	writeb(cnt_psc, pwm_addr + PWMX_PRSC_OFFSET);
	writeb(calc_period, pwm_addr + PWMX_CNTP_OFFSET);
	writeb(calc_high, pwm_addr + PWMX_CNTH_OFFSET);
}

void pwm_enable(int pwm_id)
{
	unsigned int pwm_addr = HC18XX_PWM_BASEADDR0 + (pwm_id * 0x10);

	if (readb(pwm_addr + PWMX_BUSY_OFFSET) != 0)
		return;

	writeb(1, pwm_addr + PWMX_IRQC_OFFSET);
	writeb(1, pwm_addr + PWMX_TRIG_OFFSET);
}

int board_early_init_f(void)
{
	/* peripheral clock enable */
	return 0;
}

int board_init(void)
{
	/* adress of boot parameters */
	gd->bd->bi_boot_params = CONFIG_SYS_SDRAM_BASE + 0x100;

#ifdef CONFIG_HC18XX_LED
	hc18xx_init_led();
#endif

#ifdef CONFIG_CMD_NAND
	hc18xx_nand_hw_init();
#endif

#ifdef CONFIG_HAS_DATAFLASH
/* Init SPI0 */
#endif

#ifdef CONFIG_MACB
	hc18xx_macb_hw_init();
#endif

#ifdef CONFIG_HC18XX_PWM_LED
	pwm_ledlight_init();
#endif

#if (defined(CONFIG_MS700_2) || defined(CONFIG_MS700_3))
#define PERI_RST_GPIO_ID 6
#define SENSOR_RST_GPIO_ID 66
	/* GPIO 6 and 66: OE=1, Pull-high */
	__init_gpio(PERI_RST_GPIO_ID, OUTPUT, PULL_UP);
	__init_gpio(SENSOR_RST_GPIO_ID, OUTPUT, PULL_UP);

	__set_gpio_value(PERI_RST_GPIO_ID, 1);
	__set_gpio_value(PERI_RST_GPIO_ID, 0);
	mdelay(300);
	__set_gpio_value(PERI_RST_GPIO_ID, 1);

//	__set_gpio_value(SENSOR_RST_GPIO_ID, 0);
//	__set_gpio_value(SENSOR_RST_GPIO_ID, 1);
#elif (defined(CONFIG_AGT903_3))

#define SD_PWR_EN_GPIO_ID 3
#define SENSOR_RST_GPIO_ID 66
#define FAC_RST_GPIO_ID 12

	__init_gpio(SD_PWR_EN_GPIO_ID, OUTPUT, PULL_NONE);
	__set_gpio_value(SD_PWR_EN_GPIO_ID, 1);

	__init_gpio(SENSOR_RST_GPIO_ID, OUTPUT, PULL_NONE);
	__set_gpio_value(SENSOR_RST_GPIO_ID, 1);

	__init_gpio(FAC_RST_GPIO_ID, INPUT, PULL_UP);

#elif (defined(CONFIG_HC1702_TB003))

#define JTAG_TMS_GPIO_ID 1
#define JTAG_TRST_N_GPIO_ID 4
	/* GPIO 1 and 4: OE=1, Pull-none */
	__init_gpio(JTAG_TMS_GPIO_ID, OUTPUT, PULL_NONE);
	__init_gpio(JTAG_TRST_N_GPIO_ID, OUTPUT, PULL_NONE);

	__set_gpio_value(JTAG_TMS_GPIO_ID, 1);
	__set_gpio_value(JTAG_TRST_N_GPIO_ID, 1);

#endif

/* Initial LED start */
#if (defined(CONFIG_LED_SUPPORT))
#if (defined(CONFIG_GPIO_CONTROL) || defined(TARGET_HC18XX))
#if (CONFIG_LED_GPIO1 != -1)
	__init_gpio(CONFIG_LED_GPIO1, OUTPUT, PULL_NONE);
#if (defined(CONFIG_HIGH_LEVEL_GPIO1))
	__set_gpio_value(CONFIG_LED_GPIO1, 1);
#elif (defined(CONFIG_LOW_LEVEL_GPIO1))
	__set_gpio_value(CONFIG_LED_GPIO1, 0);
#endif
#endif

#if (CONFIG_LED_GPIO2 != -1)
	__init_gpio(CONFIG_LED_GPIO2, OUTPUT, PULL_NONE);
#if (defined(CONFIG_HIGH_LEVEL_GPIO2))
	__set_gpio_value(CONFIG_LED_GPIO2, 1);
#elif (defined(CONFIG_LOW_LEVEL_GPIO2))
	__set_gpio_value(CONFIG_LED_GPIO2, 0);
#endif
#endif
#endif

#if (defined(CONFIG_PWM_CONTROL) || defined(TARGET_HC18XX))
#if (CONFIG_PWM_PIN1 != -1)
	__init_pwm(CONFIG_PWM_PIN1, CONFIG_PIN1_PWMID, CONFIG_PIN1_IOMUX, CONFIG_PWM_PERIOD,CONFIG_PWM_DUTY_CYCLE);
	pwm_enable(CONFIG_PIN1_PWMID);
#endif
#if (CONFIG_PWM_PIN2 != -1)
	__init_pwm(CONFIG_PWM_PIN2, CONFIG_PIN2_PWMID, CONFIG_PIN2_IOMUX, CONFIG_PWM_PERIOD,CONFIG_PWM_DUTY_CYCLE);
	pwm_enable(CONFIG_PIN2_PWMID);
#endif
#endif

#endif

	return 0;
}

int dram_init(void)
{
	gd->ram_size = get_ram_size((void *)CONFIG_SYS_SDRAM_BASE, CONFIG_SYS_SDRAM_SIZE);
	return 0;
}

#ifdef CONFIG_RESET_PHY_R
void reset_phy(void)
{
}
#endif

int board_eth_init(bd_t *bis)
{
	int ret = 0;
#ifdef CONFIG_HC18XX_ETH
	/* TODO hc_eth_initialize */
	u32 interface = PHY_INTERFACE_MODE_MII;
	if (hc_emac_initialize(HC18XX_EMAC_BASE_ADDR, interface) >= 0)
		ret++;
#endif

	return ret;
}
