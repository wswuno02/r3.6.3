#include <common.h>
#include <asm/io.h>
#include <div64.h>
#include <status_led.h>
#include <pwm.h>

#define HC18XX_PWM_BASEADDR0 0x805F0000
#define HC18XX_IOMUX_BASEADDR0 0x80000000

/* Region PWM */
#define PWMX_TRIG_OFFSET 0x00
#define PWMX_IRQC_OFFSET 0x02
#define PWMX_BUSY_OFFSET 0x04
#define PWMX_STAT_OFFSET 0x06
#define PWMX_IRQM_OFFSET 0x08
#define PWMX_MODE_OFFSET 0x0A
#define PWMX_PRSC_OFFSET 0x0C
#define PWMX_CNTP_OFFSET 0x0D
#define PWMX_CNTH_OFFSET 0x0E
#define PWMX_NUMP_OFFSET 0x0F
#define PWMO_SEL0_OFFSET 0x60

/* Control register */
#define PWMX_MODE_CONT 0x00
#define PWMX_MODE_FIXC 0x01

#define NUM_PWM_KER 6
#define NUM_PWM_OUT 12
#define HC_PERIOD_BASE 1000000000
#define HC_PRESCALER_BIT 5
#define HC_COUNT_PERIOD_BIT 8

#define SYS_CLK_FREQ 24000000

#define TRUE 1
#define FALSE 0

enum {

	PWM_OUT0 = 0,
	PWM_OUT1,
	PWM_OUT2,
	PWM_OUT3,
	PWM_OUT4,
	PWM_OUT5,
	PWM_OUT6,
	PWM_OUT7,
	PWM_OUT8,
	PWM_OUT9,
	PWM_OUT10,
	PWM_OUT11,
	PWM_OUT_END,
};

enum { PWM_UNKNOW = -1,
       PWM0 = 0,
       PWM1,
       PWM2,
       PWM3,
       PWM4,
       PWM5,
       PWM_END,
};

static struct hc_pwm_chip hcxx_pwm_set;

struct pwm_iomux_map {
	int pwm_out_id;
	int iomux_off;
	int start_off;
	int enable_value;
};

struct pwm_iomux_map pwm_mux_list[] = {

	{.pwm_out_id = PWM_OUT1, .iomux_off = 0x04, .start_off = 0, .enable_value = 2 },
	{.pwm_out_id = PWM_OUT2, .iomux_off = 0x04, .start_off = 8, .enable_value = 2 },
	{.pwm_out_id = PWM_OUT0, .iomux_off = 0x04, .start_off = 16, .enable_value = 1 },
	{.pwm_out_id = PWM_OUT7, .iomux_off = 0x04, .start_off = 24, .enable_value = 2 },

	{.pwm_out_id = PWM_OUT8, .iomux_off = 0x34, .start_off = 8, .enable_value = 2 },
	{.pwm_out_id = PWM_OUT9, .iomux_off = 0x34, .start_off = 16, .enable_value = 2 },

	{.pwm_out_id = PWM_OUT6, .iomux_off = 0x3C, .start_off = 0, .enable_value = 1 },
	{.pwm_out_id = PWM_OUT3, .iomux_off = 0x3C, .start_off = 8, .enable_value = 1 },

	{.pwm_out_id = PWM_OUT4, .iomux_off = 0x40, .start_off = 16, .enable_value = 1 },
	{.pwm_out_id = PWM_OUT5, .iomux_off = 0x40, .start_off = 24, .enable_value = 1 },

	{.pwm_out_id = PWM_OUT10, .iomux_off = 0x44, .start_off = 16, .enable_value = 2 },

	{.pwm_out_id = PWM_OUT11, .iomux_off = 0x48, .start_off = 8, .enable_value = 2 },

};

struct pwm_set {
	unsigned long long period;
	unsigned long long duty_cycle;
	int enable;
};

struct pwm_out_set {
	int pwm_out_id;
	int pwm_id;
};

struct pwm_set pwm_set_list[NUM_PWM_KER] = {
	{.period = 100000000, .duty_cycle = 50000000, .enable = FALSE },
	{.period = 100000000, .duty_cycle = 50000000, .enable = FALSE },
	{.period = 100000000, .duty_cycle = 50000000, .enable = FALSE },
	{.period = 100000000, .duty_cycle = 50000000, .enable = FALSE },
	{.period = 100000000, .duty_cycle = 50000000, .enable = FALSE },
	{.period = 500000000, .duty_cycle = 100000000, .enable = FALSE },

};

struct pwm_out_set pwm_out_set_list[] = {
	{.pwm_out_id = PWM_OUT0, .pwm_id = PWM_UNKNOW },  {.pwm_out_id = PWM_OUT1, .pwm_id = PWM_UNKNOW },
	{.pwm_out_id = PWM_OUT2, .pwm_id = PWM_UNKNOW },  {.pwm_out_id = PWM_OUT3, .pwm_id = PWM_UNKNOW },
	{.pwm_out_id = PWM_OUT4, .pwm_id = PWM_UNKNOW },  {.pwm_out_id = PWM_OUT5, .pwm_id = PWM_UNKNOW },
	{.pwm_out_id = PWM_OUT6, .pwm_id = PWM5 },        {.pwm_out_id = PWM_OUT7, .pwm_id = PWM_UNKNOW },
	{.pwm_out_id = PWM_OUT8, .pwm_id = PWM_UNKNOW },  {.pwm_out_id = PWM_OUT9, .pwm_id = PWM_UNKNOW },
	{.pwm_out_id = PWM_OUT10, .pwm_id = PWM_UNKNOW }, {.pwm_out_id = PWM_OUT11, .pwm_id = PWM_UNKNOW },
};

/*
struct pwm_out_set pwm_out_set_list[] =
{

  { .pwm_out_id=PWM_OUT6, .pwm_id=PWM5},
 
};

*/

void dumpreg(unsigned int addr)
{
	int i;

	printf("0x%x = 0x%x\n", addr + 0x60, readw(addr + 0x60));

	for (i = 0; i < 4; i++) {
		printf("0x%x = 0x%x\n", addr, readw(addr));
		addr += 4;
	}

	printf("++++++++\n\n");
}

int hc_gcd(int gcd_p, int gcd_d)
{
	int gcd_t = 0;
	while (gcd_d != 0) {
		gcd_t = gcd_p % gcd_d;
		gcd_p = gcd_d;
		gcd_d = gcd_t;
	}
	return gcd_p;
}

int hc_exp2(int exp)
{
	if (exp == 0)
		return 1;
	else
		return 2 * hc_exp2(exp - 1);
}

int hc_pow2(int pow)
{
	if (pow == 1)
		return 0;
	else
		return 1 + hc_pow2(pow >> 1);
}

int get_sys_clk()
{
	return SYS_CLK_FREQ;
}

int get_pwm_mux_idx(int pwm_out_id)
{
	int i;
	int result = -1;

	for (i = 0; i < NUM_PWM_OUT; i++) {
		if (pwm_mux_list[i].pwm_out_id == pwm_out_id) {
			result = i;
			break;
		}
	}

	return result;
}

void set_pwm_out_source(int pwm_out_id, int pwm_id)
{
	unsigned int pwm_addr = HC18XX_PWM_BASEADDR0 + PWMO_SEL0_OFFSET + pwm_out_id;

	writeb(pwm_id, pwm_addr);
}

void set_pwm_mux(int pwm_out_id)
{
	int id = get_pwm_mux_idx(pwm_out_id);

	writeb(pwm_mux_list[id].enable_value,
	       HC18XX_IOMUX_BASEADDR0 + pwm_mux_list[id].iomux_off + (pwm_mux_list[id].start_off >> 3));
}

int pwm_out_init(int pwm_out_id, int pwm_id)
{
	set_pwm_out_source(pwm_out_id, pwm_id);
	set_pwm_mux(pwm_out_id);

	return 0;
}

int pwm_config(int pwm_id, int duty_ns, int period_ns)
{
	int cnt_psc = 0;
	unsigned long long period_freq;
	unsigned long long calc_high;
	unsigned long long calc_period;
	unsigned long long mod_prd;
	int max_freq;
	int min_freq;
	unsigned int pwm_addr = HC18XX_PWM_BASEADDR0 + (pwm_id * 0x10);

	period_freq = get_sys_clk();

	max_freq = get_sys_clk() / 2;

	if ((HC_PERIOD_BASE / period_ns) > max_freq) {
		return 0;
	}

	min_freq = get_sys_clk() / hc_exp2(hc_exp2(HC_PRESCALER_BIT - 1)) / (hc_exp2(HC_COUNT_PERIOD_BIT) - 1);

	if ((HC_PERIOD_BASE / period_ns) < min_freq) {
		return 0;
	}

	do {
		/*
		 *   period_ns     count_period
		 *  ----------- = --------------
		 *     10^9         frequency
		 *
		 * frequency = 24MHz / (2^prescaler)
		 *
		 * count_period = frequency * period_ns / 10^9
		 */

		calc_period = (unsigned long long)period_freq * period_ns;

		mod_prd = do_div(calc_period, HC_PERIOD_BASE);

		if (calc_period > 255) {
			cnt_psc = cnt_psc + 1;
			period_freq = period_freq / 2;
		} else {
			if (mod_prd > HC_PERIOD_BASE / 2) {
				calc_period = calc_period + 1;
			}

			printf("request frquency: %d Hz\n", HC_PERIOD_BASE / period_ns);
			do_div(period_freq, calc_period);
			printf("provide frquency: %llu Hz\n", period_freq);
		}
	} while (calc_period > 255);

	calc_high = calc_period * duty_ns;
	do_div(calc_high, period_ns);

	writeb(cnt_psc, pwm_addr + PWMX_PRSC_OFFSET);
	writeb(calc_period, pwm_addr + PWMX_CNTP_OFFSET);
	writeb(calc_high, pwm_addr + PWMX_CNTH_OFFSET);

	return 0;
}

int pwm_enable(int pwm_id)
{
	unsigned int pwm_addr = HC18XX_PWM_BASEADDR0 + (pwm_id * 0x10);

	if (readb(pwm_addr + PWMX_BUSY_OFFSET) != 0)
		return 0;

	writeb(1, pwm_addr + PWMX_IRQC_OFFSET);
	writeb(1, pwm_addr + PWMX_TRIG_OFFSET);

	return 0;
}

void pwm_disable(int pwm_id)
{
	unsigned int pwm_addr = HC18XX_PWM_BASEADDR0 + (pwm_id * 0x10);

	if (readb(pwm_addr + PWMX_BUSY_OFFSET) == 0)
		return;

	writeb(1, pwm_addr + PWMX_IRQC_OFFSET);
	writeb(1, pwm_addr + PWMX_TRIG_OFFSET);
}

int pwm_on(int pwm_id, int duty_ns, int period_ns)
{
	printf("pw_id=%d, duty_ns=%d,period_ns=%d\n\n", pwm_id, duty_ns, period_ns);

	if (pwm_config(pwm_id, duty_ns, period_ns) != 0)
		return 1;

	if (pwm_enable(pwm_id) != 0)
		return 1;

	return 0;
}

int pwm_off(int pwm_id)
{
	pwm_disable(pwm_id);
	return 0;
}

int pwm_ledlight_init()
{
	int i;

	printf("pwm ledlight init\n");

	struct pwm_out_set *pwm_out_set_ptr = pwm_out_set_list;

	for (i = 0; i < ARRAY_SIZE(pwm_out_set_list); ++i, ++pwm_out_set_ptr) {
		if (pwm_out_set_ptr->pwm_id < PWM0 || pwm_out_set_ptr->pwm_id >= PWM_END)
			continue;

		if (pwm_out_set_ptr->pwm_out_id < PWM_OUT0 || pwm_out_set_ptr->pwm_out_id > PWM_OUT11)
			continue;

		pwm_out_init(pwm_out_set_ptr->pwm_out_id, pwm_out_set_ptr->pwm_id);

		if (pwm_set_list[pwm_out_set_ptr->pwm_id].enable == FALSE) {
			pwm_set_list[pwm_out_set_ptr->pwm_id].enable =
			        pwm_on(pwm_out_set_ptr->pwm_id, pwm_set_list[pwm_out_set_ptr->pwm_id].duty_cycle,
			               pwm_set_list[pwm_out_set_ptr->pwm_id].period);
		}

		//dumpreg(0x805F0000 + pwm_out_set_ptr->pwm_id*0x10);
	}

	return 0;
}
