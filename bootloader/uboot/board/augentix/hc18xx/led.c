/*
 * (C) Copyright 2017-2018  Augentix Inc.
 * ShihChieh Lin, Augentix Inc., <shihchieh.lin@augentix.com>
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <asm/io.h>
#include <status_led.h>
#include <configs/hc18xx.h>

enum led_state { LED_OFF = 0, LED_ON };

struct led_attr {
	char *label;
	int gpio_id;
	enum led_state state;
};

/* Define led_list based on the selected machine */
#if (defined(CONFIG_MS700_1) || defined(CONFIG_MS700_2) || defined(CONFIG_MS700_3))
struct led_attr led_list[] = {
	{.label = "LED_1_green", .gpio_id = 50, .state = LED_ON },
	{.label = "LED_2_red", .gpio_id = 48, .state = LED_OFF },
};
#endif

void __set_led(int id, int level)
{
	phys_addr_t base = HC18XX_IOMUX_BASEADDR0;
	writeb(0, (base + IOSEL_BASE + (id * IOSEL_OFFS)));
	writeb(level, (base + GPIO_BASE + (id * GPIO_OFFS)));
	writeb(1, (base + GPIO_BASE + (id * GPIO_OFFS) + 1));
}

void hc18xx_init_led(void)
{
	int i;
	struct led_attr *led = led_list;
	for (i = 0; i < ARRAY_SIZE(led_list); ++i, ++led)
		__set_led(led->gpio_id, led->state);
	return;
}

void coloured_led_init(void)
{
	return;
}
