/*
 * (C) Copyright 2017-2018  Augentix Inc.
 * ShihChieh Lin, Augentix Inc., <shihchieh.lin@augentix.com>
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <malloc.h>
#include <asm/io.h>

#if defined(CONFIG_RESET_PHY_R) && defined(CONFIG_MACB)
#include <net.h>
#endif
#include <netdev.h>
#include <miiphy.h>
#include <common.h>
#include <div64.h>

#include <dwmmc.h>

DECLARE_GLOBAL_DATA_PTR;

/* ------------------------------------------------------------------------- */
/*
 * Miscelaneous platform dependent initialisations
 */
extern int eqos_emac_initialize(u32 interface);
extern int hc_emac_initialize(ulong base_addr, u32 interface);
extern void hc18xx_init_led(void);

#ifdef CONFIG_CMD_NAND
static void hc18xx_nand_hw_init(void)
{
}
#endif

#ifdef CONFIG_MACB
static void hc18xx_macb_hw_init(void)
{
	/* Initialize EMAC=MACB hardware */
}
#endif

int board_mmc_init(bd_t *bd)
{
	struct dwmci_host *host = NULL;

	host = malloc(sizeof(struct dwmci_host));
	if (!host) {
		printf("dwmci_host malloc fail!\n");
		return 1;
	}

	memset(host, 0, sizeof(struct dwmci_host));
	host->name = "Synopsys Mobile storage";
	host->ioaddr = (void *)SDC_BASE;
	host->buswidth = 4;
	host->dev_index = 0;
	host->bus_hz = 100000000;
	host->fifo_mode = true;
	// host->fifoth_val = 0x100;

	add_dwmci(host, host->bus_hz, 400000);

	return 0;
}

void __set_iomux(int gpio_id, int mux)
{
	phys_addr_t io_base = HC1703_1723_1753_1783S_IO_BASEADDR0;
	uint32_t offset = 0;

	if (gpio_id < 0 || gpio_id > 80)
		return;

	if (gpio_id <= 19) { // 0 ~ 19 starts from 0x514
		offset = 0x514 + ((gpio_id - 0) * IOSEL_OFFS);
	} else if (gpio_id <= 67) { // 20 ~ 67 starts from 0x598
		offset = 0x598 + ((gpio_id - 20) * IOSEL_OFFS);
	} else if (gpio_id <= 76) { // 68 ~ 76 starts from 0x44
		offset = 0x44 + ((gpio_id - 68) * IOSEL_OFFS);
	} else if (gpio_id <= 78) { // 77 ~ 78 starts from 0x3C
		offset = 0x3C + ((gpio_id - 77) * IOSEL_OFFS);
	} else if (gpio_id <= 80) { // 79 ~ 80 starts from 0x68
		offset = 0x68 + ((gpio_id - 79) * IOSEL_OFFS);
	}

	writel(mux, io_base + offset);
}

void __init_gpio(int gpio_id, uint32_t direction, uint32_t pinconf)
{
	phys_addr_t io_base = HC1703_1723_1753_1783S_IO_BASEADDR0;
	phys_addr_t gpio_direction_base = io_base + GPIO_BASE + 4; // GPIO_OE_*
	int gpio_group, gpio_offs;
	uint32_t val;

	__set_iomux(gpio_id, 0);

	if (gpio_id > 10) // SD_POC insertion
		writel(pinconf, io_base + IOCFG_BASE + (gpio_id * IOCFG_OFFS) + 4); // set IOCFG
	else
		writel(pinconf, io_base + IOCFG_BASE + (gpio_id * IOCFG_OFFS)); // set IOCFG

	gpio_group = gpio_id >> 5; // gpio_group = gpio_id / 32
	gpio_offs = gpio_id & 0x1F; // gpio_offs  = gpio_id % 31

	gpio_direction_base += (gpio_group * GPIO_OFFS);

	val = readl(gpio_direction_base);
	if (direction == INPUT)
		val &= ~(1 << gpio_offs);
	else
		val |= (1 << gpio_offs);
	writel(val, gpio_direction_base);
}

void __set_gpio_value(int gpio_id, uint32_t level)
{
	phys_addr_t gpio_output_base = HC1703_1723_1753_1783S_IO_BASEADDR0 + GPIO_BASE;
	int gpio_group, gpio_offs;
	uint32_t val;

	gpio_group = gpio_id >> 5; // gpio_group = gpio_id / 32
	gpio_offs = gpio_id & 0x1F; // gpio_offs  = gpio_id % 31

	gpio_output_base += (gpio_group * GPIO_OFFS); //GPIO_O_*

	val = readl(gpio_output_base);
	if (level == 0)
		val &= ~(1 << gpio_offs);
	else
		val |= (1 << gpio_offs);
	writel(val, gpio_output_base);
}

int get_sys_clk(void)
{
	return SYS_CLK_FREQ;
}

int hc_exp2(int exp)
{
	if (exp == 0)
		return 1;
	else
		return 2 * hc_exp2(exp - 1);
}

void __init_pwm(int pwm_id, int period_ns, int duty_ns)
{
	int cnt_psc = 0;
	unsigned long long period_freq;
	unsigned long long calc_high;
	unsigned long long calc_period;
	unsigned long long mod_prd;
	unsigned long long t_reg;
	int max_freq;
	int min_freq;
	unsigned int pwm_addr = HC18XX_PWM_BASEADDR0 + (pwm_id * 0x10);
	unsigned int pwm_outsel_addr;
	unsigned int pwm_outsel_val;

	if (pwm_id < 4) {
		pwm_outsel_addr = HC18XX_PWM_BASEADDR0 + PWMO_SEL0_OFFSET;
	} else {
		pwm_outsel_addr = HC18XX_PWM_BASEADDR0 + PWMO_SEL4_OFFSET;
	}

	pwm_outsel_val = readl(pwm_outsel_addr);

	if (pwm_id < 4) {
		pwm_outsel_val |= (pwm_id << (0x8 * pwm_id));
	} else {
		pwm_outsel_val |= (pwm_id << (0x8 * (pwm_id - 4)));
	}

	writel(pwm_outsel_val, pwm_outsel_addr);

	period_freq = get_sys_clk();

	max_freq = get_sys_clk() / 2;

	if ((HC_PERIOD_BASE / period_ns) > max_freq) {
		return;
	}

	min_freq = get_sys_clk() / hc_exp2(hc_exp2(HC_PRESCALER_BIT - 1)) / (hc_exp2(HC_COUNT_PERIOD_BIT) - 1);

	if ((HC_PERIOD_BASE / period_ns) < min_freq) {
		return;
	}

	do {
		/*
		 *   period_ns     count_period
		 *  ----------- = --------------
		 *     10^9         frequency
		 *
		 * frequency = 24MHz / (2^prescaler)
		 *
		 * count_period = frequency * period_ns / 10^9
		 */

		calc_period = (unsigned long long)period_freq * period_ns;

		mod_prd = do_div(calc_period, HC_PERIOD_BASE);

		if (calc_period > 255) {
			cnt_psc = cnt_psc + 1;
			period_freq = period_freq / 2;
		} else {
			if (mod_prd > HC_PERIOD_BASE / 2) {
				calc_period = calc_period + 1;
			}

			printf("request frquency: %d Hz\n", HC_PERIOD_BASE / period_ns);
			do_div(period_freq, calc_period);
			printf("provide frquency: %llu Hz\n", period_freq);
		}
	} while (calc_period > 255);

	calc_high = calc_period * duty_ns;
	do_div(calc_high, period_ns);

	t_reg = (cnt_psc | (calc_period << 8) | (calc_high << 16));
	writel(t_reg, pwm_addr + PWMX_PRSC_OFFSET);
}

void pwm_enable(int pwm_id)
{
	unsigned int pwm_addr = HC18XX_PWM_BASEADDR0 + (pwm_id * 0x10);

	if ((readl(pwm_addr + PWMX_BUSY_OFFSET) & 0x1) != 0)
		return;

	writel(0x10000, pwm_addr + PWMX_TRIG_OFFSET); // clear IRQ
	writel(0x1, pwm_addr + PWMX_TRIG_OFFSET); // trigger PWM
}

int board_early_init_f(void)
{
	return 0;
}

int board_init(void)
{
	/* adress of boot parameters */
	gd->bd->bi_boot_params = CONFIG_SYS_SDRAM_BASE + 0x100;
	return 0;
}

int dram_init(void)
{
	gd->ram_size = get_ram_size((void *)CONFIG_SYS_SDRAM_BASE, CONFIG_SYS_SDRAM_SIZE);
	return 0;
}

int board_eth_init(bd_t *bis)
{
	int reset_gpio_pin = 26;
	__init_gpio(reset_gpio_pin, OUTPUT, 0);
	__set_gpio_value(reset_gpio_pin, 1);
	__set_gpio_value(reset_gpio_pin, 0);
	mdelay(20); //reset hold time
	__set_gpio_value(reset_gpio_pin, 1);
	mdelay(150); //mdio ready after reset

	eqos_emac_initialize(PHY_INTERFACE_MODE_RMII);
	return 0;
}
