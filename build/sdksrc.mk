###############################################################################
# Augentix SDK Makefile Boilerplate
###############################################################################

SERIES = HC18-SERIES
TOOLCHAIN = $(CROSS_COMPILE:-=)
TOOLCHAIN_SUFFIX = $(lastword $(subst -, ,$(TOOLCHAIN)))
CHIP_LIST = hc1702 hc1752 hc1772 hc1782 hc1783s
SDK_KERNEL_VERSION=3.18.31

# Buildroot directories
BUILDROOT_PATH = $(SDKSRC_DIR)/buildroot
BUILDROOT_BUILD_PATH = $(BUILDROOT_PATH)/build
BUILDROOT_SRC_PATH = $(BUILDROOT_PATH)/src
BUILDROOT_CONFIG_PATH = $(BUILDROOT_SRC_PATH)/configs
BUILDROOT_OUTPUT_PATH = $(BUILDROOT_PATH)/output
BUILDROOT_OUTPUT_BUILD_PATH=$(BUILDROOT_OUTPUT_PATH)/build
BUILDROOT_HOST_BIN = $(BUILDROOT_OUTPUT_PATH)/host/usr/bin
BUILDROOT_HOST_LIB = $(BUILDROOT_OUTPUT_PATH)/host/lib
BUILDROOT_HOST_INC = $(BUILDROOT_OUTPUT_PATH)/host/usr/arm-buildroot-linux-$(TOOLCHAIN_SUFFIX)/sysroot/usr/include
BUILDROOT_TARGET_LIB = $(BUILDROOT_OUTPUT_PATH)/target/usr/lib

# Buildroot libraries
include $(SDKSRC_DIR)/build/buildroot.mk

TOOLCHAIN_PATH = $(SDKSRC_DIR)/toolchain
ifeq ($(TOOLCHAIN), arm-linux-gnueabihf)
TOOLCHAIN_LIBGCC_PATH = $(TOOLCHAIN_PATH)/gcc-linaro-4.9-2016.02-x86_64_arm-linux-gnueabihf/lib/gcc/arm-linux-gnueabihf/4.9.4 -lgcc
else ifeq ($(TOOLCHAIN), arm-linux-gnueabi)
TOOLCHAIN_LIBGCC_PATH = $(TOOLCHAIN_PATH)/gcc-linaro-4.9.4-2017.01-x86_64_arm-linux-gnueabi/lib/gcc/arm-linux-gnueabi/4.9.4 -lgcc
else ifeq ($(TOOLCHAIN), arm-augentix-linux-gnueabi)
TOOLCHAIN_LIBGCC_PATH = $(TOOLCHAIN_PATH)/arm-augentix-linux-gnueabi/lib/gcc/arm-augentix-linux-gnueabi/4.9.3 -lgcc
endif

# CPVS header / libraries directories
CPVS_PATH = $(SDKSRC_DIR)/cpvs
CPVS_INC = $(CPVS_PATH)/include
CPVS_LIB = $(CPVS_PATH)/lib
CPVS_KO = $(CPVS_PATH)/ko
CPVS_CSR_INC = $(CPVS_INC)/csr
CPVS_SENIF_INC = $(CPVS_INC)/senif

KERNEL_PATH = $(SDKSRC_DIR)/linux
UBOOT_PATH = $(SDKSRC_DIR)/bootloader/uboot
UBOOT_ENV_PATH = $(UBOOT_PATH)/tools/env
HOST_UTILS_PATH = $(SDKSRC_DIR)/utils/host/bin
FSBL_PATH = $(SDKSRC_DIR)/bootloader/fsbl
FSBL_BUILD_PATH = $(SDKSRC_DIR)/bootloader/fsbl/build
FSBL_LIB_PATH = $(SDKSRC_DIR)/bootloader/fsbl/lib
ROOTFS_PATH = $(SDKSRC_DIR)/fs/rootfs
ROOTFS_BUILD_PATH = $(ROOTFS_PATH)/build
ROOTFS_OUTPUT_PATH = $(ROOTFS_PATH)/output
ROOTFS_IMAGE_PATH = $(ROOTFS_OUTPUT_PATH)/images
ROOTFS_CPIO_FILE = rootfs.cpio
ROOTFS_CPIO_UBOOT_FILE = $(ROOTFS_CPIO_FILE).uboot
ROOTFS_CPIO_UBOOT_FILE_DBG = $(ROOTFS_CPIO_UBOOT_FILE)_debug
ROOTFS_CPIO_UBOOT_FILE_REL = $(ROOTFS_CPIO_UBOOT_FILE)
DTS_PATH = $(KERNEL_PATH)/arch/arm/boot/dts
MPP_PATH = $(SDKSRC_DIR)/mpp
MPP_UTILS_PATH = $(MPP_PATH)/utils
KVPP_PATH = $(MPP_PATH)/kvpp
UVPP_PATH = $(MPP_PATH)/uvpp
UAPP_PATH = $(MPP_PATH)/uapp
MPP_BUILD_PATH = $(MPP_PATH)/build
EXTDRV_PATH = $(SDKSRC_DIR)/extdrv
AUDIO_PATH = $(EXTDRV_PATH)/audio
AUDIO_BUILD_PATH = $(AUDIO_PATH)/build
AUDIO_XCM_PATH = $(EXTDRV_PATH)/audio_xcm
AUDIO_XCM_BUILD_PATH = $(AUDIO_XCM_PATH)/build
VB_PATH = $(MPP_PATH)/vb
SENSOR_PATH = $(MPP_PATH)/custom/sensor
ISP_PATH = $(MPP_PATH)/isp
BUILD_UTILS_PATH = $(SDKSRC_DIR)/build/build_utils
UTILS_PATH = $(SDKSRC_DIR)/utils
UTILS_BUILD_PATH = $(UTILS_PATH)/build
UTILS_HOST_PATH = $(UTILS_PATH)/host
UTILS_TARGET_PATH = $(UTILS_PATH)/target
SCRIPTS_PATH = $(ROOTFS_PATH)/scripts
NVSP_PATH = $(SDKSRC_DIR)/programmer/nvsp
FLASHBOOT_PATH = $(SDKSRC_DIR)/programmer/program_flash
UARTFLASH_PATH = $(SDKSRC_DIR)/programmer/uart_program_flash
ICESCRIPT_PATH = $(SDKSRC_DIR)/programmer/ice_script
LIBINIPARSER_PATH = $(FLASHBOOT_PATH)/libiniparser
UT_PATH = $(SDKSRC_DIR)/ut_framework
CUTEST_PATH = $(UT_PATH)/cutest
KCONFIG_PATH = $(BUILD_UTILS_PATH)/kconfig

APP_PATH = $(SDKSRC_DIR)/application

# Used by featurelib

DEBUG_PATH = $(MPP_PATH)/libdebug
DUMPBIN_PATH = $(FLASHBOOT_PATH)/dumpbin
BT_PATH = $(KERNEL_PATH)/drivers/bluetooth/realtek8723du

# Feature_video and feature_audio include and library
FEATURE_VIDEO_PATH = $(SDKSRC_DIR)/feature/video
FEATURE_AUDIO_PATH = $(SDKSRC_DIR)/feature/audio
FEATURE_IR_CONTROL_PATH = $(SDKSRC_DIR)/feature/ir_control
FEATURE_PATHS = $(FEATURE_VIDEO_PATH) $(FEATURE_AUDIO_PATH) $(FEATURE_IR_CONTROL_PATH)

FEATURE_VIDEO_BUILD_PATH = $(FEATURE_VIDEO_PATH)/build
FEATURE_AUDIO_BUILD_PATH = $(FEATURE_AUDIO_PATH)/build
FEATURE_IR_CONTROL_BUILD_PATH = $(FEATURE_IR_CONTROL_PATH)/build
FEATURE_BUILD_PATHS = $(FEATURE_VIDEO_BUILD_PATH) $(FEATURE_AUDIO_BUILD_PATH) $(FEATURE_IR_CONTROL_BUILD_PATH)

FEATURE_VIDEO_LIB_PATH = $(FEATURE_VIDEO_PATH)/lib
FEATURE_AUDIO_LIB_PATH = $(FEATURE_AUDIO_PATH)/lib
FEATURE_IR_CONTROL_LIB_PATH = $(FEATURE_IR_CONTROL_PATH)/lib
FEATURE_LIB_PATHS = $(FEATURE_VIDEO_LIB_PATH) $(FEATURE_AUDIO_LIB_PATH) $(FEATURE_IR_CONTROL_LIB_PATH)

FEATURE_VIDEO_INC_PATH = $(FEATURE_VIDEO_PATH)/include
FEATURE_AUDIO_INC_PATH = $(FEATURE_AUDIO_PATH)/include
FEATURE_IR_CONTROL_INC_PATH = $(FEATURE_IR_CONTROL_PATH)/include
FEATURE_INC_PATHS = $(FEATURE_VIDEO_INC_PATH) $(FEATURE_AUDIO_INC_PATH) $(FEATURE_IR_CONTROL_INC_PATH)

LD_PATH = $(FEATURE_VIDEO_PATH)/src/ld
TD_PATH = $(FEATURE_VIDEO_PATH)/src/td
MD_PATH = $(FEATURE_VIDEO_PATH)/src/md
EF_PATH = $(FEATURE_VIDEO_PATH)/src/ef
AROI_PATH = $(FEATURE_VIDEO_PATH)/src/aroi
OSC_PATH = $(FEATURE_VIDEO_PATH)/src/osc
SHD_PATH = $(FEATURE_VIDEO_PATH)/src/shd
EAIF_PATH = $(FEATURE_VIDEO_PATH)/src/eaif
PFM_PATH = $(FEATURE_VIDEO_PATH)/src/pfm
FD_PATH = $(FEATURE_VIDEO_PATH)/src/fd
DK_PATH = $(FEATURE_VIDEO_PATH)/src/dk
FLD_PATH = $(FEATURE_VIDEO_PATH)/src/fld

SD_PATH =$(FEATURE_AUDIO_PATH)/src/sd
AC_PATH = $(FEATURE_AUDIO_PATH)/src/ac

FEATURE_VIDEO_UT_PATH = $(addsuffix /unit_test, $(addprefix $(FEATURE_VIDEO_PATH)/src/, $(basename $(notdir $(wildcard $(FEATURE_VIDEO_INC_PATH)/*.h)))))
FEATURE_AUDIO_UT_PATH = $(addsuffix /unit_test, $(addprefix $(FEATURE_AUDIO_PATH)/src/, $(basename $(notdir $(wildcard $(FEATURE_AUDIO_INC_PATH)/*.h)))))
FEATURE_UT_PATHS = $(FEATURE_VIDEO_UT_PATH) $(FEATURE_AUDIO_UT_PATH)

FEATURE_VIDEO_LIB = vftr
FEATURE_AUDIO_LIB = aftr
FEATURE_IR_CONTROL_LIB = ir_control
FEATURE_LIBS = $(FEATURE_VIDEO_LIB) $(FEATURE_AUDIO_LIB) $(FEATURE_IR_CONTROL_LIB)

# Include libraries
SDK_INC = $(SDKSRC_DIR)/include/common
GEN_INC = $(SDKSRC_DIR)/include/generated
CSR_DIR = $(SDKSRC_DIR)/include/csr/define

# MPP include/library directories
MPP_INC = $(MPP_PATH)/include
MPP_LIB = $(MPP_PATH)/lib
MPP_KO = $(MPP_PATH)/ko

# MPI application include and library
MPI_PATH = $(MPP_PATH)/mpi

# DIP include and library
DIP_PATH = $(MPP_PATH)/dip
DIP_LIB = $(DIP_PATH)/lib
DIP_INC = $(DIP_PATH)/include

# AE include and library
AE_PATH = $(DIP_PATH)/hc_ae
AE_LIB = $(AE_PATH)
AE_INC = $(AE_PATH)

# AWB include and library
AWB_PATH = $(DIP_PATH)/hc_awb
AWB_LIB = $(AWB_PATH)
AWB_INC = $(AWB_PATH)

# IVA include and library
IVA_PATH = $(MPP_PATH)/iva
OBJDET_PATH = $(IVA_PATH)/object_detect
OBJDET_INC = $(OBJDET_PATH)

# KVPP include and library
KVPP_INC = $(KVPP_PATH)
KVPP_TRC_INC = $(KVPP_PATH)/trc

# UVPP include and library
UVPP_LIB = $(UVPP_PATH)/lib
UVPP_INC = $(UVPP_PATH)/include
LIBIS_INC = $(UVPP_PATH)/libis
LIBISP_INC = $(UVPP_PATH)/libisp
LIBENC_INC = $(UVPP_PATH)/libenc
LIBOSD_INC = $(UVPP_PATH)/libosd
LIBSR_INC = $(UVPP_PATH)/libsr

# libsensor
LIBSENSOR_INC = $(SENSOR_PATH)
LIBSENSOR_LIB = $(SENSOR_PATH)/lib

# libsenif
LIBSENIF_PATH = $(UVPP_PATH)/libsenif
LIBSENIF_INC = $(LIBSENIF_PATH)

# libsns
SNS_PATH = $(MPP_PATH)/sensor
SNS_INC = $(SNS_PATH)

# libnrs
NRS_DRV_PATH = $(EXTDRV_PATH)/nrs
NRS_DRV_BUILD = $(NRS_DRV_PATH)/build
NRS_DRV_SRC = $(NRS_DRV_PATH)/src
NRS_DRV_INC = $(NRS_DRV_PATH)/include
NRS_DRV_KO = $(NRS_DRV_PATH)/ko
NRS_DRV_SRC = $(NRS_DRV_PATH)/src

# libvb
VB_INC = $(VB_PATH)/include
VB_LIB = $(VB_PATH)/lib

# libutrc
UTRC_PATH = $(MPP_PATH)/utrc
UTRC_COMMON_INC = $(MPP_PATH)/common/utrc
UTRC_INC = $(MPP_PATH)/utrc/include
KTRC_INC = $(KVPP_PATH)/trc

# libsr
SR_COMMON_INC = $(MPP_PATH)/common/sr

# VPC include and library
VPC_PATH = $(MPP_PATH)/vpc
VPLAT_PATH = $(VPC_PATH)/vplat
VPLAT_INC = $(VPLAT_PATH)
RS_PATH = $(VPC_PATH)/rs
RS_INC = $(RS_PATH)

# Utility libraries
DEBUG_INC = $(DEBUG_PATH)/inc

# ABI versioning file
ABIVER_MAKE = $(SDKSRC_DIR)/build/versioning.mk
ABIVER_FILE = $(SDKSRC_DIR)/build/.version

# Output/Release directories
SYSROOT_DBG = $(ROOTFS_OUTPUT_PATH)/target_debug
SYSROOT_REL = $(ROOTFS_OUTPUT_PATH)/target
SYSTEMFS = $(ROOTFS_OUTPUT_PATH)/system
SYSROOT = $(SYSROOT_DBG)

SYSTEM_BIN = $(SYSTEMFS)/bin
SYSTEM_LIB = $(SYSTEMFS)/lib

AGTX_MOD_PATH = $(SYSTEM_LIB)/modules/$(SDK_KERNEL_VERSION)/augentix

# AUDIO include and library

AUDIO_INC = $(AUDIO_PATH)/include
AUDIO_KO = $(AUDIO_PATH)/ko
AUDIO_XCM_INC = $(AUDIO_XCM_PATH)/include
AUDIO_XCM_KO = $(AUDIO_XCM_PATH)/ko
AUDIO_XCM_SRC = $(AUDIO_XCM_PATH)/src
UAPP_INC = $(UAPP_PATH)/include

# Doxygen template directory
DOXYGEN_PATH = $(SDKSRC_DIR)/build/doxygen

# Alias for systemfs
CUSTOMFS = $(SYSTEMFS)
CUSTOM_BIN = $(SYSTEM_BIN)
CUSTOM_LIB = $(SYSTEM_LIB)

USRDATAFS_PATH = $(SDKSRC_DIR)/fs/usrdatafs
USRDATAFS_BUILD_PATH = $(USRDATAFS_PATH)/build
USRDATAFS = $(USRDATAFS_PATH)/usrdata

CALIBFS_PATH = $(SDKSRC_DIR)/fs/calibfs
CALIBFS_BUILD_PATH = $(CALIBFS_PATH)/build
CALIBFS = $(CALIBFS_PATH)/calib

BINPKG_DIR = $(SDKSRC_DIR)/build/output
BINPKG_DIR_DEBUG = $(SDKSRC_DIR)/build/output_debug
SDKREL_DIR = $(SDKSRC_DIR)/build/SDK_release
TARGET_INIT_SCRIPTS_DIR = $(SYSROOT)/etc/init.d

ERROR_MSG_FILE = .error.txt

include $(SDKSRC_DIR)/build/import_config.mk

ifneq ("$(wildcard $(SDKSRC_DIR)/.release)", "")
	CONFIG_RELEASE := y
else
	CONFIG_RELEASE :=
endif

V ?= 0
ifeq ($(V),1)
	Q :=
	VOUT :=
else
	Q := @
	VOUT := 2>&1 1>/dev/null
endif

define show_error
	@cat $(ERROR_MSG_FILE)
endef
