SDKSRC_DIR ?= $(realpath $(CURDIR)/..)
include $(SDKSRC_DIR)/build/sdksrc.mk

MAJOR := $(strip $(shell cat $(ABIVER_FILE) | grep -P "major:" | sed "s/major://"))
MINOR := $(strip $(shell cat $(ABIVER_FILE) | grep -P "minor:" | sed "s/minor://"))
PATCH := $(strip $(shell cat $(ABIVER_FILE) | grep -P "patch:" | sed "s/patch://"))

# Parsing major number

ifeq ($(shell echo $(MAJOR) | grep -wP "[0-9]+"),)
	$(error Major number should be an integer!)
endif
ABI_VER := $(MAJOR)

# Parsing minor number

ifeq ($(shell echo $(MINOR) | grep -wP "[0-9]+"),)
	$(error Minor number should be an integer!)
endif
ifneq ($(CONFIG_RELEASE), y)
	ABI_VER := $(ABI_VER).$(shell date '+%Y%m%d')
else
	ABI_VER := $(ABI_VER).$(MINOR)
endif

# Parsing patch number

ifeq ($(shell echo $(PATCH) | grep -wP "[0-9]+"),)
	$(error Patch number should be an integer!)
endif
ifneq ($(CONFIG_RELEASE), y)
	ABI_VER := $(ABI_VER).$(shell expr $(PATCH) + 1)
else
	ABI_VER := $(ABI_VER).$(shell expr $(PATCH))
endif


# Provide macros to externel makefiles

SONAME_SUFFIX := $(MAJOR)
REAL_SUFFIX := $(ABI_VER)
