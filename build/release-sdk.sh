#!/usr/bin/env bash
set -Eeuo pipefail

SRC_DIR=$(readlink -e ..)
readonly SRC_DIR
readonly SRC_BUILD_DIR=${SRC_DIR}/build

readonly DST_DIR=${SRC_BUILD_DIR}/SDK_release
readonly DST_BUILD_DIR=${DST_DIR}/build

readonly CONFIG_FILE=${SRC_BUILD_DIR}/.config

SDK_VER=

: "${CONFIG_HC1702_1752_1772_1782:=}"
is_hc17x2() {
	[ "$CONFIG_HC1702_1752_1772_1782" = 'y' ]
}

: "${CONFIG_HC1703_1723_1753_1783S:=}"
is_hc17x3() {
	[ "$CONFIG_HC1703_1723_1753_1783S" = 'y' ]
}

: "${CONFIG_CHIP:=}"
chip_type() {
	local tmp="$CONFIG_CHIP"
	tmp="${tmp,,}"     # pipe 1: convert string to lowercase
	tmp="${tmp//_/-}"  # pipe 2: replace all underscore to the hyphen
	echo "$tmp"
}

sync_build() {
	rsync -avrq --exclude='.git' "$SRC_BUILD_DIR/build_utils" "$DST_BUILD_DIR"
	rsync -avrq --exclude='.git' "$SRC_BUILD_DIR/rule.make" "$DST_BUILD_DIR"
	rm -f "$DST_BUILD_DIR/build_utils/.exclude_file"
	rm -f "$DST_BUILD_DIR/build_utils/.include_file"
	rm -f "$DST_BUILD_DIR/build_utils/*.csv"
	rm -f "$DST_BUILD_DIR/build_utils/*.sh"
	rm -f "$DST_BUILD_DIR/build_utils/padding"
	cp -f "$SRC_BUILD_DIR/.version" "$DST_BUILD_DIR"
	cp -f "$SRC_BUILD_DIR/import_config.mk" "$DST_BUILD_DIR"
	cp -f "$SRC_BUILD_DIR/sdksrc.mk" "$DST_BUILD_DIR"
	cp -f "$SRC_BUILD_DIR/versioning.mk" "$DST_BUILD_DIR"
	cp -f "$SRC_BUILD_DIR/sdk-version" "$DST_BUILD_DIR"
	cp -f "$SRC_BUILD_DIR/Makefile" "$DST_BUILD_DIR"
	cp -f "$SRC_BUILD_DIR/buildroot.mk" "$DST_BUILD_DIR"
}

sync_bootloader() {
	mkdir -p "$DST_DIR/bootloader/fsbl"
	rsync -avrq --exclude='.git' "$SRC_DIR/bootloader/fsbl/build" "$DST_DIR/bootloader/fsbl"
	if is_hc17x2; then
        rsync -avrq --exclude='.git' "$SRC_DIR/bootloader/fsbl/bin" "$DST_DIR/bootloader/fsbl"
        mv "$DST_DIR/bootloader/fsbl/bin" "$DST_DIR/bootloader/fsbl/bin.$(chip_type)"
		rsync -avrq --exclude='.git' "$SRC_DIR/bootloader/fsbl/lib" "$DST_DIR/bootloader/fsbl"
		mv "$DST_DIR/bootloader/fsbl/lib" "$DST_DIR/bootloader/fsbl/lib.$(chip_type)"
		rsync -avrq --exclude='.git' "$SRC_DIR/bootloader/fsbl/include" "$DST_DIR/bootloader/fsbl"
	fi
	rsync -avrq --exclude='.git' "$SRC_DIR/bootloader/uboot" "$DST_DIR/bootloader"
}

sync_cpvs() {
	mkdir -p "$DST_DIR/cpvs"
	rsync -avrq --exclude='.git' "$SRC_DIR/cpvs/build" "$DST_DIR/cpvs"
	if is_hc17x3; then
		rsync -avrq --exclude='.git' "$SRC_DIR/cpvs/lib" "$DST_DIR/cpvs"
		mv "$DST_DIR/cpvs/lib" "$DST_DIR/cpvs/lib.$(chip_type)"
		rsync -avrq --exclude='.git' "$SRC_DIR/cpvs/ko" "$DST_DIR/cpvs"
		mv "$DST_DIR/cpvs/ko" "$DST_DIR/cpvs/ko.$(chip_type)"
		rsync -avrq --exclude='.git' "$SRC_DIR/cpvs/$(chip_type)/include" "$DST_DIR/cpvs"
		mv "$DST_DIR/cpvs/include" "$DST_DIR/cpvs/include.$(chip_type)"
	fi
}

sync_extdrv() {
	rsync -avrq --exclude='.git' "$SRC_DIR/extdrv" "$DST_DIR"
	rm -rf "$DST_DIR/extdrv/audio_xcm"
	rm -rf "$DST_DIR/extdrv/nrs"
	mkdir -p "$DST_DIR/extdrv/audio_xcm"
	rsync -avrq --exclude='.git' "$SRC_DIR/extdrv/audio_xcm/build" "$DST_DIR/extdrv/audio_xcm"
	rsync -avrq --exclude='.git' "$SRC_DIR/extdrv/audio_xcm/include" "$DST_DIR/extdrv/audio_xcm"
	rsync -avrq --exclude='.git' "$SRC_DIR/extdrv/audio_xcm/ko" "$DST_DIR/extdrv/audio_xcm"
	mv "$DST_DIR/extdrv/audio_xcm/ko" "$DST_DIR/extdrv/audio_xcm/ko.$(chip_type)"
	mkdir -p "$DST_DIR/extdrv/nrs"
	rsync -avrq --exclude='.git' "$SRC_DIR/extdrv/nrs/build" "$DST_DIR/extdrv/nrs"
	rsync -avrq --exclude='.git' "$SRC_DIR/extdrv/nrs/include" "$DST_DIR/extdrv/nrs"
	rsync -avrq --exclude='.git' "$SRC_DIR/extdrv/nrs/ko" "$DST_DIR/extdrv/nrs"
	mv "$DST_DIR/extdrv/nrs/ko" "$DST_DIR/extdrv/nrs/ko.$(chip_type)"
}

sync_mpp() {
	mkdir -p "$DST_DIR/mpp"
	mkdir -p "$DST_DIR/mpp/custom"
	rsync -avrq --exclude='.git' "$SRC_DIR/mpp/build" "$DST_DIR/mpp"
	rsync -avrq --exclude='.git' "$SRC_DIR/mpp/include" "$DST_DIR/mpp"
	rsync -avrq --exclude='.git' "$SRC_DIR/mpp/ko" "$DST_DIR/mpp"
	mv "$DST_DIR/mpp/ko" "$DST_DIR/mpp/ko.$(chip_type)"
	rsync -avrq --exclude='.git' "$SRC_DIR/mpp/lib" "$DST_DIR/mpp"
	mv "$DST_DIR/mpp/lib" "$DST_DIR/mpp/lib.$(chip_type)"
	rsync -avrq --exclude='.git' "$SRC_DIR/mpp/custom/sensor" "$DST_DIR/mpp/custom"
}

sync_iva() {
	mkdir -p "$DST_DIR/feature"
	mkdir -p "$DST_DIR/feature/audio"
	mkdir -p "$DST_DIR/feature/video"
	mkdir -p "$DST_DIR/feature/ir_control"
	rsync -avrq --exclude='.git' "$SRC_DIR/feature/audio/build" "$DST_DIR/feature/audio"
	rsync -avrq --exclude='.git' "$SRC_DIR/feature/audio/include" "$DST_DIR/feature/audio"
	rsync -avrq --exclude='.git' "$SRC_DIR/feature/audio/lib" "$DST_DIR/feature/audio"
	mv "$DST_DIR/feature/audio/lib" "$DST_DIR/feature/audio/lib.$(chip_type)"
	rsync -avrq --exclude='.git' "$SRC_DIR/feature/video/build" "$DST_DIR/feature/video"
	rsync -avrq --exclude='.git' "$SRC_DIR/feature/video/include" "$DST_DIR/feature/video"
	rsync -avrq --exclude='.git' "$SRC_DIR/feature/video/lib" "$DST_DIR/feature/video"
	mv "$DST_DIR/feature/video/lib" "$DST_DIR/feature/video/lib.$(chip_type)"
	rsync -avrq --exclude='.git' "$SRC_DIR/feature/ir_control/build" "$DST_DIR/feature/ir_control"
	rsync -avrq --exclude='.git' "$SRC_DIR/feature/ir_control/include" "$DST_DIR/feature/ir_control"
	rsync -avrq --exclude='.git' "$SRC_DIR/feature/ir_control/lib" "$DST_DIR/feature/ir_control"
	mv "$DST_DIR/feature/ir_control/lib" "$DST_DIR/feature/ir_control/lib.$(chip_type)"
}

sync_utils() {
	mkdir -p "$DST_DIR/utils/host"
	mkdir -p "$DST_DIR/utils/target"
	rsync -avrq --exclude='.git' "$SRC_DIR/utils/build" "$DST_DIR/utils"
	rsync -avrq --exclude='.git' "$SRC_DIR/utils/host/bin" "$DST_DIR/utils/host"
	mv "$DST_DIR/utils/host/bin" "$DST_DIR/utils/host/bin.$(chip_type)"
	rsync -avrq --exclude='.git' "$SRC_DIR/utils/target/bin" "$DST_DIR/utils/target"
	mv "$DST_DIR/utils/target/bin" "$DST_DIR/utils/target/bin.$(chip_type)"
}

sync_app() {
	rsync -avrq --exclude='.git' "$SRC_DIR/application" "$DST_DIR"
}

sync_fs() {
	mkdir -p "$DST_DIR/fs"
	rsync -avrq --exclude='.git' "$SRC_DIR/fs/rootfs" "$DST_DIR/fs"
	rsync -avrq --exclude='.git' "$SRC_DIR/fs/usrdatafs" "$DST_DIR/fs"
	rsync -avrq --exclude='.git' "$SRC_DIR/fs/calibfs" "$DST_DIR/fs"
}

sync_remaining() {
	rsync -avrq --exclude='.git' "$SRC_DIR/buildroot" "$DST_DIR"
	rsync -avrq --exclude='.git' "$SRC_DIR/programmer" "$DST_DIR"
	rsync -avrq --exclude='.git' "$SRC_DIR/include" "$DST_DIR"
	rsync -avrq --exclude='.git' "$SRC_DIR/linux" "$DST_DIR"
	rsync -avrq --exclude='.git' "$SRC_DIR/toolchain" "$DST_DIR"
}

sync_sdk() {
	sync_build
	sync_bootloader
	echo "Generating cpvs..."
	sync_cpvs
	sync_extdrv
	sync_mpp
	sync_iva
	sync_utils
	sync_app
	sync_fs
	sync_remaining
}

post_install() {
	make -C "$DST_BUILD_DIR" update-builddate
	make -C "$DST_BUILD_DIR" sdk-info-install SDK_VER="$SDK_VER"
	make -C "$DST_BUILD_DIR" version-bump
}

read_sdk_ver() {
	if [ $# -eq 0 ] ; then
		read -rp "Enter SDK version: " SDK_VER
	else
		SDK_VER=$1
	fi
}

check_env() {
	[ ! -f "$CONFIG_FILE" ] && \
		echo "Missing config file." && \
		return 1
	return 0
}

handle_error() {
	echo "Fatal error: Failed to generate SDK release."
}

main() {
        # import product config
        # shellcheck source=/dev/null
	. <(sed '/AUDIO_CODEC/d' "$CONFIG_FILE")

	echo " - SDK version: $SDK_VER"

	echo "Initializing SDK..."
	rm -rf "$DST_DIR"
	mkdir -p "$DST_DIR"
	mkdir -p "$DST_DIR/.release"
	mkdir -p "$DST_BUILD_DIR"

	echo "Building SDK..."
	sync_sdk
	post_install

	echo "The SDK staging environment has been generated successfully."
}

trap handle_error ERR
check_env
read_sdk_ver "$@"

main
