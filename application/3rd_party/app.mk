mod := $(notdir $(subdir))

# 3rd-party paths
TUYA_SERVICE_PATH := $(APP_PATH)/3rd_party/tuya_service
TUYA_IPC_SDK_DIR := $(CONFIG_APP_TUYA_IPC_SDK_PATH)
export TUYA_IPC_SDK_DIR

app-$(CONFIG_APP_TUYA_SERVICE) += tuya_service
PHONY += tuya_service tuya_service-clean tuya_service-distclean
PHONY += tuya_service-install tuya_service-uninstall
tuya_service: libado libavftr libeaif libcm libprio
	$(Q)$(MAKE) -C $(EVENTD_PATH)/libledevt all
	$(Q)$(MAKE) -C $(TUYA_SERVICE_PATH) all

tuya_service-clean: libprio-clean libcm-clean \
	libavftr-clean libado-clean libeaif-clean
	$(Q)$(MAKE) -C $(TUYA_SERVICE_PATH) clean
	$(Q)$(MAKE) -C $(EVENTD_PATH)/libledevt clean

tuya_service-distclean: libprio-distclean libcm-distclean \
	libavftr-distclean libado-distclean libeaif-distclean
	$(Q)$(MAKE) -C $(TUYA_SERVICE_PATH) distclean
	$(Q)$(MAKE) -C $(EVENTD_PATH)/libledevt distclean

tuya_service-install: libado-install libavftr-install libeaif-install \
	libcm-install libprio-install
	$(Q)$(MAKE) -C $(TUYA_SERVICE_PATH) install

tuya_service-uninstall: libprio-uninstall libcm-uninstall libeaif-uninstall \
	libavftr-uninstall libado-uninstall
	$(Q)$(MAKE) -C $(TUYA_SERVICE_PATH) uninstall

PHONY += $(mod) $(mod)-clean $(mod)-distclean
PHONY += $(mod)-install $(mod)-uninstall
$(mod): $(app-y)
$(mod)-clean: $(addsuffix -clean,$(app-y))
$(mod)-distclean: $(addsuffix -distclean,$(app-y))
$(mod)-install: $(addsuffix -install,$(app-y))
$(mod)-uninstall: $(addsuffix -uninstall,$(app-y))

APP_BUILD_DEPS += $(mod)
APP_CLEAN_DEPS += $(mod)-clean
APP_DISTCLEAN_DEPS += $(mod)-distclean
APP_INTALL_DEPS += $(mod)-install
APP_UNINTALL_DEPS += $(mod)-uninstall
