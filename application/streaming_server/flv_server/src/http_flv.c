#include "http_flv.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>

#include "log_define.h"
#include "http_flv_parser.h"
#include "video.h"

void exit_err(const char *sc)
{
	perror(sc);
	exit(1);
}

bool HTTP_checkCodecChange(char chn_num)
{
	MPI_VENC_ATTR_S venc_attr;
	MPI_ENC_getVencAttr(MPI_ENC_CHN(chn_num), &venc_attr);
	if (venc_attr.type != MPI_VENC_TYPE_H264) {
		return true;
	}

	return false;
}

void HTTP_checkResponseSize(char *src, uint32_t strlen, char *size)
{
	/*remove space*/
	char size_cnt = 0;
	for (int i = 0; i < strlen; i++) {
		if ((src[i] != 0x00) & (src[i] != '\0')) {
			size_cnt++;
		}
	}
	*size = size_cnt;
}

int HTTP_executeDeviceCmd(struct message *m)
{
	if (m->method == HTTP_GET) {
		FLVLOG("url: %s\r\n", m->request_url);
		m->chn_num = -1;
		m->is_audio = false;
		/*format TBD*/
		char audio[16] = { 0 };
		sscanf(m->request_url, "/%[^/]/%[^/]\r\n", &audio[0], &(m->chn_num));
		m->chn_num = m->chn_num - 0x30;

		if (m->chn_num == -1) {
			FLVERR("src not support\r\n");
			return -EACCES;
		} else {
			FLVLOG("get in chn %d\r\n", m->chn_num);
		}

		if (NULL != strstr(&audio[0], "liveaudio")) {
			m->is_audio = true;
		} else if (NULL != strstr(&audio[0], "live")) {
			m->is_audio = false;
		}

	} else {
		FLVERR("Req not support\r\n");
		return -EPERM;
	}

	return 0;
}

int HTTP_setlistenPort(int port, int timeout_s)
{
	int sockfd = 0;
	struct sockaddr_in info;

	sockfd = socket(PF_INET, SOCK_STREAM, 0);
	if (sockfd == -1) {
		FLVERR("failed to create server sock\r\n");
		return -EIO;
	}

	int reuseaddr = 1;
	int len = sizeof(reuseaddr);
	int ret = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, len);
	if (ret == -1) {
		FLVERR("Failed to set re-use addr\r\n");
	}

	memset(&info, 0, sizeof(info));

	info.sin_family = AF_INET;
	info.sin_port = htons(port);
	info.sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(sockfd, (struct sockaddr *)&info, sizeof(info)) < 0)
		exit_err("bind");

	printf("Start to listen at port %d\n", port);

	if (listen(sockfd, SOMAXCONN) < 0) {
		exit_err("listen");
	}

	return (sockfd);
}