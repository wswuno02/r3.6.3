#include "wss_msg.h"

#include <stdlib.h>
#include <string.h>
#include <json.h>
#include "errno.h"

#include "log_define.h"

int freeParseJson(json_object *config_obj)
{
	json_object_put(config_obj);
	return 0;
}

int WSS_parseWssPcmMessage(char *fd, PcmMessage *msg)
{
	json_object *test_obj = NULL;
	json_object *tmp_obj = NULL;
	json_object *tmp1_obj = NULL;
	char dataBuf[64];

	/* load json config from json file */
	test_obj = json_tokener_parse(fd);
	if (!test_obj) {
		FLVERR("Cannot open fd, invaild json\n");
		goto end;
	}

	json_object_object_get_ex(test_obj, "id", &tmp_obj);
	if (!tmp_obj) {
		FLVERR("Failed to find ID, invalid json\n");
		goto end;
	}
	msg->id = json_object_get_int(tmp_obj);
	FLVLOG("Date: %d\r\n", (int)msg->id);

	json_object_object_get_ex(test_obj, "type", &tmp_obj);
	if (!tmp_obj) {
		FLVERR("Failed to find msg type, invalid json\n");
		goto end;
	}
	sprintf(&(msg->msg_type[0]), "%s", json_object_get_string(tmp_obj));
	FLVLOG("type: %s\r\n", msg->msg_type);

	if (0 == strcmp(msg->msg_type, "ping")) {
		FLVLOG("ping type\r\n");
		goto save_end;
	} else if (0 == strcmp(msg->msg_type, "pong")) {
		FLVLOG("pong type\r\n");
		goto save_end;
	} else if (0 == strcmp(msg->msg_type, "call_start")) {
		goto save_end;
	} else if (0 == strcmp(msg->msg_type, "data_available")) {
		FLVLOG("data available\r\n");
	} else if (0 == strcmp(msg->msg_type, "call_end")) {
		goto save_end;
	} else {
		FLVERR("Unknown type, invalid json\n");
		goto end;
	}
	json_object_object_get_ex(test_obj, "mimeType", &tmp_obj);
	if (!tmp_obj) {
		FLVERR("Failed to find mime type, invalid json\n");
		goto end;
	}
	sprintf(&dataBuf[0], "%s", json_object_get_string(tmp_obj));
	FLVLOG("mime type: %s\r\n", dataBuf);

	if (0 == strcmp(dataBuf, "audio/l16")) {
		msg->type = S16LE;
	} else if (0 == strcmp(dataBuf, "audio/pcma")) {
		msg->type = ALAW;
		FLVLOG("format alaw\r\n");
	} else if (0 == strcmp(dataBuf, "audio/pcmu")) {
		msg->type = MULAW;
		FLVLOG("format mulaw\r\n");
	} else {
		FLVERR("failed to get pcm format\r\n");
		goto end;
	}

	json_object_object_get_ex(test_obj, "length", &tmp_obj);
	if (!tmp_obj) {
		FLVERR("Failed to find pcm length, invalid json\n");
		goto end;
	}
	msg->length = json_object_get_int(tmp_obj);
	FLVLOG("len: %d\r\n", msg->length);

	json_object_object_get_ex(test_obj, "data", &tmp_obj);
	if (!tmp_obj) {
		FLVERR("Failed to find pcm data, invalid json\n");
		goto end;
	}
	if (!tmp_obj) {
		FLVERR("failed to get pcm data\r\n");
		free(msg->data);
		goto end;
	}

	int array_len = json_object_array_length(tmp_obj);
	if (array_len < 0) {
		FLVERR("Failed to parse pcm array, invalid json\n");
		goto end;
	}
	msg->data = malloc(msg->length);
	if (msg->data == NULL) {
		FLVERR("Failed to alloc pcm buf\r\n");
		goto end;
	}
	if (array_len != msg->length) {
		FLVERR("pcm array and data length diff(%d, %d)\r\n", array_len, msg->length);
	}
	FLVLOG("data len: %d\r\n", array_len);
	for (int i = 0; i < msg->length; i++) {
		tmp1_obj = json_object_array_get_idx(tmp_obj, i);
		if (!tmp1_obj) {
			FLVERR("Failed to get chn id\r\n");
			break;
		}

		msg->data[i] = atoi(json_object_get_string(tmp1_obj));
	}
save_end:
	freeParseJson(test_obj);
	return 0;
end:
	freeParseJson(test_obj);
	return -EINVAL;
}