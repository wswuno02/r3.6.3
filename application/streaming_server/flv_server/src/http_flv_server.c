
#include "http_flv_server.h"

#include <stdio.h>
#include <string.h> // for strlen
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <sys/socket.h>
#include <arpa/inet.h> // for inet_addr
#include <unistd.h> // for write
#include <pthread.h> // for threading, link with lpthread
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include "video.h"
#include "audio.h"
#include "flv-muxer.h"
#include "log_define.h"
#include "http_flv.h"
#include "http_flv_parser.h"

static const char *g_device[] = { "default", "hw:0,0" }; /* sound device */
extern int g_run_flag;

int writeFLVOutput(void *src, uint32_t len, int clientfd)
{
	int ret = send(clientfd, src, len, 0);
	return ret;
}

int sendResponse(char *response, int len, int socketfd)
{
	char size;
	HTTP_checkResponseSize(&response[0], len, &size);

	printf("==Response Message[%d]==\n%s", size, response);

	return send(socketfd, response, size, 0);
}

static void *__processHttpMessage(void *arg)
{
	struct message *m;
	int clientfd = *((int *)arg);
	char *buf;
	int ret = 0;
#if 1
	buf = malloc(RET_BUF_SIZE);
	if (!buf) {
		fprintf(stderr, "Failed to allocate memory !\r\n");
		goto close_fd;
	}

	/* initialize parser */
	m = malloc(sizeof(struct message));
	if (!m) {
		FLVERR("Failed to allocate memory !");
		goto free_buf;
	}

	struct http_parser *parser = parser_init(HTTP_REQUEST);
	if (!parser) {
		FLVERR("Failed to allocate memory !");
		goto free_msg;
	}

	do {
		int len, nparsed;
		bzero(m, sizeof(*m));

		/* receive a complete message */
		do {
			len = recv(clientfd, buf, RET_BUF_SIZE, 0);
			FLVLOG("recv len:%d\r\n", len);
			if (len <= 0) {
				goto end;
			}

			/* print recevied string */
			buf[len] = '\0';
			FLVINFO("==Received String==\n%s\n", buf);

			/* start parsing */
			nparsed = parse(parser, buf, len, m);

			if (nparsed == -1) {
				goto end;
			}
		} while ((!m->body_is_final && m->content_length) && g_run_flag);

		/* response*/
		ret = HTTP_executeDeviceCmd(m);

		char dateBuf[32];
		time_t now = time(0);
		struct tm tm = *gmtime(&now);
		strftime(dateBuf, sizeof(dateBuf), "%a, %d %b %Y %H:%M:%S %Z", &tm);
		char response[256] = { 0 };
		int nsent = -1;

		if (ret != 0) {
			if (ret == -EACCES) {
				snprintf(&response[0], sizeof(response),
				         "HTTP/1.1 500 Internal Server Error\r\nServer: %s\r\nDate: %s\r\n\r\n",
				         "Augentix", dateBuf);
			} else {
				snprintf(&response[0], sizeof(response),
				         "HTTP/1.1 400 Bad Request\r\nServer: %s\r\nDate: %s\r\n\r\n", "Augentix",
				         dateBuf);
			}

			sendResponse(&response[0], strlen(response), clientfd);
			goto end;
		}

		/*init bistream*/
		int ret;
		uint8_t aac_ret;

		MPI_BCHN bchn;
		VIDEO_STREAM_DATA StreamData;
		bool is_first_frame = true;
		ret = initStream(m->chn_num, &bchn); /*bchn -> bchn*/
		if (ret != 0) {
			FLVERR("failed to init bitstream\n");
			snprintf(&response[0], sizeof(response),
			         "HTTP/1.1 500 Internal Server Error\r\nServer: %s\r\nDate: %s\r\n\r\n", "Augentix",
			         dateBuf);
			sendResponse(&response[0], strlen(response), clientfd);
			uninitStream(&bchn);
			goto end;
		}

		/*audio init*/
		snd_pcm_stream_t stream = SND_PCM_STREAM_CAPTURE;
		snd_pcm_format_t format = SND_PCM_FORMAT_S16_LE;
		unsigned int rate = 8000;
		int dev_id = 0;
		snd_pcm_uframes_t frame = 1024;
		int channel = 1;
		int buf_len = 1024 * 2 * 1;
		char aac_buf[buf_len];
		char pcm_buf[buf_len];
		ty_media_aac_handle_s hdl;
		int frames = 1024;
		bool is_first_aac = true; /*is_first_aac*/
		uint32_t audio_ts = 0;
		snd_pcm_t *p_capture;

		bool is_audio = m->is_audio; /*is_audio*/

		if (is_audio) {
			ret = agtx_pcm_init(&p_capture, g_device[dev_id], stream, format, frame, rate, channel);
			if (ret < 0) {
				FLVERR("failed to init pcm, %d\n", ret);
				FLVERR("failed to init audio, send video only\r\n");
				is_audio = false;
			} else {
				ret = aac_encoder_init(&hdl, channel, rate, rate);
				if (ret < 0) {
					FLVERR("Failed to init AAC encoder.\n");
					is_audio = false;
					aac_encoder_uninit(&hdl);
				}
			}

			if (ret < 0) {
				FLVERR("failed to init audio, send video only\r\n");
			} else {
				snd_pcm_nonblock(p_capture, 1);
			}
		}

		snprintf(&response[0], sizeof(response),
		         "HTTP/1.1 200 OK\r\n"
		         "Server: %s Server <0.1>\r\n"
		         "Content-Type:  video/x-flv\r\n"
		         "Connection: keep-alive\r\n"
		         "Expires: -1\r\n"
		         "Access-Control-Allow-Origin: *\r\n"
		         "Access-Control-Allow-Credentials: true\r\n\r\n",
		         "Augentix");
		nsent = sendResponse(&response[0], strlen(response), clientfd);

		MediaSrcInfo srcInfo;
		srcInfo.chnNum = m->chn_num;
		srcInfo.outputFd = clientfd;
		srcInfo.fCheckCodecChange = HTTP_checkCodecChange;
		srcInfo.fWriteFlv = writeFLVOutput;

		write_flv_header(&srcInfo, is_audio, true);

		unsigned int time_diff = 0;
		unsigned int start_time_jiff = 0;
		while (((g_run_flag) && (nsent != -1)) && m->should_keep_alive) {
			ret = readFrame(&bchn, &StreamData);
			if (ret != 0) {
				/*getbistream error handling*/
				snprintf(&response[0], sizeof(response),
				         "HTTP/1.1 500 Internal Server Error\r\nServer: %s\r\nDate: %s\r\n\r\n",
				         "Augentix", dateBuf);
				sendResponse(&response[0], strlen(response), clientfd);
				FLVERR("no data\r\n");
				nsent = -1;
				break;
			}
			/*need to wait until first Idr*/
			if ((is_first_frame) && (StreamData.params.seg[0].type != MPI_FRAME_TYPE_SPS)) {
				FLVLOG("Wait for first I frame\r\n");
				releaseFrame(&bchn, &StreamData);
				continue;
			}

			if (true == HTTP_checkCodecChange(m->chn_num)) {
				/*change codec, getbistream error handling*/
				snprintf(&response[0], sizeof(response),
				         "HTTP/1.1 500 Internal Server Error\r\nServer: %s\r\nDate: %s\r\n\r\n",
				         "Augentix", dateBuf);
				sendResponse(&response[0], strlen(response), clientfd);
				FLVERR("no data\r\n");
				nsent = -1;
				break;
			}

			assert((StreamData.params.seg[0].type == MPI_FRAME_TYPE_SPS) ||
			       (StreamData.params.seg[0].type == MPI_FRAME_TYPE_P));
			if (StreamData.params.seg[0].type == MPI_FRAME_TYPE_SPS) {
				/*check sps nalu*/
				assert((StreamData.params.seg[0].uaddr[0] == 0x00) &&
				       (StreamData.params.seg[0].uaddr[1] == 0x00) &&
				       (StreamData.params.seg[0].uaddr[2] == 0x00) &&
				       (StreamData.params.seg[0].uaddr[3] == 0x01) &&
				       (StreamData.params.seg[0].uaddr[4] == 0x67));
				/*check pps nalu*/
				assert((StreamData.params.seg[1].uaddr[0] == 0x00) &&
				       (StreamData.params.seg[1].uaddr[1] == 0x00) &&
				       (StreamData.params.seg[1].uaddr[2] == 0x00) &&
				       (StreamData.params.seg[1].uaddr[3] == 0x01) &&
				       (StreamData.params.seg[1].uaddr[4] == 0x68));
				/*check IDR nalu*/
				assert((StreamData.params.seg[2].uaddr[0] == 0x00) &&
				       (StreamData.params.seg[2].uaddr[1] == 0x00) &&
				       (StreamData.params.seg[2].uaddr[2] == 0x00) &&
				       (StreamData.params.seg[2].uaddr[3] == 0x01) &&
				       (StreamData.params.seg[2].uaddr[4] == 0x65));

			} else {
				/*check p frame nalu*/
				assert((StreamData.params.seg[0].uaddr[0] == 0x00) &&
				       (StreamData.params.seg[0].uaddr[1] == 0x00) &&
				       (StreamData.params.seg[0].uaddr[2] == 0x00) &&
				       (StreamData.params.seg[0].uaddr[3] == 0x01) &&
				       (StreamData.params.seg[0].uaddr[4] == 0x41));
			}

			/*Mux video tag + send*/
			if (is_first_frame) {
				start_time_jiff = StreamData.params.jiffies;
				time_diff = 0;
				audio_ts = 0; /*for avsync*/
				nsent = write_avc_sequence_header_tag(&srcInfo, StreamData.params.seg[0].uaddr + 4,
				                                      StreamData.params.seg[0].size - 4,
				                                      StreamData.params.seg[1].uaddr + 4,
				                                      StreamData.params.seg[1].size - 4, time_diff);
				if (nsent < 0) {
					releaseFrame(&bchn, &StreamData);
					FLVLOG("\r\n");
					break;
				}

				is_first_frame = false;
			} else {
				time_diff = (StreamData.params.jiffies - start_time_jiff) * 10;
				if (StreamData.params.frame_id == 0) {
					FLVINFO("re-start enc\r\n");
					audio_ts = time_diff;
				}
			}

			if (StreamData.params.seg[0].type == MPI_FRAME_TYPE_SPS) {
				uint32_t sps_len = StreamData.params.seg[0].size - 4;
				uint32_t pps_len = StreamData.params.seg[1].size - 4;

				uint32_t size = StreamData.params.seg[2].size - 4;
				if (StreamData.params.seg_cnt > 3) {
					for (int i = 3; i < StreamData.params.seg_cnt; i++) {
						size += StreamData.params.seg[i].size;
					}
				}
				//printf("[%d]chn %d jiffies: %lu\r\n", clientfd, srcInfo.chnNum, (unsigned long)StreamData.params.jiffies);

				nsent = write_avc_data_tag(&srcInfo, StreamData.params.seg[0].uaddr + 4, sps_len,
				                           StreamData.params.seg[1].uaddr + 4, pps_len,
				                           StreamData.params.seg[2].uaddr + 4, size, time_diff, 1);
				if (nsent == -EACCES) {
					releaseFrame(&bchn, &StreamData);
					snprintf(&response[0], sizeof(response),
					         "HTTP/1.1 500 Internal Server Error\r\nServer: %s\r\nDate: %s\r\n\r\n",
					         "Augentix", dateBuf);
					sendResponse(&response[0], strlen(response), clientfd);
					break;
				} else if (nsent == -EIO) { /*change profile*/

					audio_ts = time_diff;

					nsent = write_avc_sequence_header_tag(
					        &srcInfo, StreamData.params.seg[0].uaddr + 4,
					        StreamData.params.seg[0].size - 4, StreamData.params.seg[1].uaddr + 4,
					        StreamData.params.seg[1].size - 4, time_diff);
					nsent = write_avc_data_tag(&srcInfo, StreamData.params.seg[0].uaddr + 4,
					                           sps_len, StreamData.params.seg[1].uaddr + 4, pps_len,
					                           StreamData.params.seg[2].uaddr + 4, size, time_diff,
					                           1);

				} else if (nsent < 0) {
					releaseFrame(&bchn, &StreamData);
					FLVLOG("\r\n");
					break;
				}

			} else {
				uint32_t size = StreamData.params.seg[0].size - 4;
				if (StreamData.params.seg_cnt > 1) {
					for (int i = 1; i < StreamData.params.seg_cnt; i++) {
						size += StreamData.params.seg[i].size;
					}
				}
				nsent = write_avc_data_tag(&srcInfo, NULL, 0, NULL, 0,
				                           StreamData.params.seg[0].uaddr + 4, size, time_diff, 0);
				if (nsent < 0) {
					releaseFrame(&bchn, &StreamData);
					FLVLOG("\r\n");
					break;
				}
			}

			releaseFrame(&bchn, &StreamData);

			if (is_audio) {
				/*always flush pcm buffer to empty*/
				while ((g_run_flag == 1) && (ret != -EAGAIN)) {
					ret = snd_pcm_readi(p_capture, pcm_buf, frames);
					if (ret == -EPIPE) {
						snd_pcm_prepare(p_capture);
						FLVERR("-EPIPE\n");
						continue;
					} else if (ret == -EAGAIN) {
						/* means there is no data, break for loop */
						break;
					} else if (ret < 0) {
						FLVERR("snd pcm unknown err: %d\r\n", ret);
						break;
					}
					FLVLOG("get audio sample: %d\r\n", ret);
					int aac_recv = ret * 2;
					aac_ret = aac_encoder_data(&hdl, pcm_buf, ret * 2, ret, aac_buf, &aac_recv);
					if (aac_ret != AACENC_OK) {
						//FLVERR("aac_ret != AACENC_OK\n");
						break;
					}

					audio_ts += 128;
					if (is_first_aac == true) {
						nsent = write_aac_sequence_header_tag(&srcInfo, rate, channel, 0);
						if (nsent < 0) {
							FLVLOG("\r\n");
							break;
						}
						is_first_aac = false;
					}

					nsent = write_aac_data_tag(&srcInfo, (uint8_t *)&aac_buf[7], aac_recv - 7,
					                           audio_ts);
					if (nsent < 0) {
						FLVLOG("\r\n");
						break;
					}
				}
			}
		}

		if (is_audio) {
			ret = agtx_pcm_uninit(p_capture);
			if (ret < 0) {
				FLVERR("failed to uninit pcm, %d\n", ret);
			}
			ret = aac_encoder_uninit(&hdl);
			if (ret < 0) {
				FLVERR("failed to uninit pcm, %d\n", ret);
			}
		}

		uninitStream(&bchn);

		if (nsent == -1) {
			FLVERR("nsent == -1\r\n");
			goto end;
		}

	} while ((m->should_keep_alive) && g_run_flag);

	FLVLOG("leave");
	parser_free(parser);
	free(m);
	free(buf);
	close(clientfd);

	pthread_detach(pthread_self());
	return NULL;

end:
	FLVLOG("parser free\r\n");
	parser_free(parser);
free_msg:
	free(m);
free_buf:
	free(buf);
close_fd:
#endif
	close(clientfd);
	pthread_detach(pthread_self());
	return NULL;
}

void *runHttpServerListenThread(void *arg)
{
	int server_sock, client_sock;
	static struct sockaddr_in cli_addr;

	socklen_t length = sizeof(cli_addr);
	int port = 8000; /*only for test*/

	server_sock = HTTP_setlistenPort(port, 2);
	while (g_run_flag) {
		client_sock = accept(server_sock, (struct sockaddr *)&cli_addr, &length);

		if (client_sock == -1) {
			FLVERR("accept error\r\n");
		}

		FLVINFO("Accept a connection from %s:%d.\n", "localhost", port);
		pthread_attr_t attr;
		pthread_attr_init(&attr);
		pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

		pthread_t t0;

		if (pthread_create(&t0, &attr, __processHttpMessage, (void *)&client_sock) != 0) {
			perror("pthread_create");
		}
		pthread_attr_destroy(&attr);
	}

	pthread_detach(pthread_self());

	close(server_sock);

	FLVLOG("close sock\r\n");

	return NULL;
}
