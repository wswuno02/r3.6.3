#include <stdio.h>
#include <string.h> // for strlen
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h> // for write
#include <pthread.h> // for threading, link with lpthread
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <getopt.h>

#include "log_define.h"
#include "http_flv_server.h"
#include "https_flv_server.h"
#include "wss_server.h"

#include "https_flv.h"

#include "video.h"
#include "audio.h"

#define FILE_NAME_LEN (64)
#define SLEEP_INTERVAL (2)

int g_run_flag = 0;

static void handleSigInt(int signo)
{
	if (signo == SIGINT) {
		FLVERR("Caught SIGINT!\n");
	} else if (signo == SIGTERM) {
		FLVERR("Caught SIGTERM!\n");
	} else if (signo == SIGPIPE) {
		FLVERR("Caught SIGPIPE!\n");
		return;
	} else {
		perror("Unexpected signal!\n");
	}
	g_run_flag = 0;
}

void help()
{
	printf("usage:\r\n"
	       "-w --wss  run wss 2-way audio server\r\n"
	       "-t --http run http flv server\r\n"
	       "-s --https run https flv server\r\n"
	       "-k TLS key path\r\n"
	       "-c TLS cert path\r\n"
	       "-h help()\r\n");
}

int main(int argc, char *argv[])
{
	if (signal(SIGINT, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGINT!\n");
		exit(1);
	}

	if (signal(SIGTERM, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGTERM!\n");
		exit(1);
	}

	if (signal(SIGPIPE, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGPIPE!\n");
		exit(1);
	}

	const char *optstring = "hwtsc:k:";
	int c;
	struct option opts[] = { { "wss", 0, NULL, 'w' },
		                 { "http", 0, NULL, 't' },
		                 { "https", 0, NULL, 's' },
		                 { "cert", 1, NULL, 'c' },
		                 { "key", 1, NULL, 'k' } };

	bool has_wss = false;
	bool has_http = false;
	bool has_https = false;
	char *cert_file = NULL;
	char *key_file = NULL;
	HttpsServerInfo https_info;
	WssServerInfo wss_info;

	while ((c = getopt_long(argc, argv, optstring, opts, NULL)) != -1) {
		switch (c) {
		case 'h':
			help();
			exit(1);
			break;
		case 'w':
			has_wss = true;
			break;
		case 't':
			has_http = true;
			break;
		case 's':
			has_https = true;
			break;
		case 'c':
			cert_file = malloc(FILE_NAME_LEN);
			snprintf(cert_file, FILE_NAME_LEN, "%s", argv[optind - 1]);
			break;
		case 'k':
			key_file = malloc(FILE_NAME_LEN);
			snprintf(key_file, FILE_NAME_LEN, "%s", argv[optind - 1]);
			break;
		default:
			help();
			exit(1);
		}
	}

	FLVINFO("has_wss: %d, has_http: %d, has_https: %d\r\ncert path: %s\r\nkey path: %s\r\n", has_wss, has_http,
	        has_https, cert_file, key_file);

	if (((has_https) || (has_wss)) && ((access(cert_file, F_OK) == -1) || (access(key_file, F_OK) == -1))) {
		FLVERR("failed to open TLS file: %s, %s\r\n", cert_file, key_file);
		if (cert_file != NULL) {
			free(cert_file);
		}

		if (key_file != NULL) {
			free(key_file);
		}
		return -EACCES;
	}

	g_run_flag = 1;

	if (MPI_SYS_init() != MPI_SUCCESS) {
		FLVERR("Failed to MPI_SYS_init!");
		return -EPERM;
	}

	int ret = MPI_initBitStreamSystem(); /*only once, first clients,  destroy if no client*/
	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "MPI_initBitStreamSystem_failed...\n");
		g_run_flag = 0;
		return -EPERM;
	}

	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

	if (has_http) {
		pthread_t t0;
		if (pthread_create(&t0, NULL, runHttpServerListenThread, NULL) != 0) {
			FLVERR("failed to create thread\r\n");
		}
		if (pthread_setname_np(t0, "http-flv") != 0) {
			FLVERR("failed to set thread name\r\n");
		}
	}

	if (has_https) {
		pthread_t t0;
		https_info.port = 8443;
		https_info.cert_file = malloc(FILE_NAME_LEN);
		https_info.key_file = malloc(FILE_NAME_LEN);

		memcpy(https_info.cert_file, cert_file, FILE_NAME_LEN);
		memcpy(https_info.key_file, key_file, FILE_NAME_LEN);

		if (pthread_create(&t0, NULL, runHttpsServerListenThread, (void *)&https_info) != 0) {
			FLVERR("failed to create thread\r\n");
		}
		if (pthread_setname_np(t0, "https-flv") != 0) {
			FLVERR("failed to set thread name\r\n");
		}
	}

	if (has_wss) {
		pthread_t t0;
		wss_info.port = WS_PORT;
		wss_info.cert_file = malloc(FILE_NAME_LEN);
		wss_info.key_file = malloc(FILE_NAME_LEN);

		memcpy(wss_info.cert_file, cert_file, FILE_NAME_LEN);
		memcpy(wss_info.key_file, key_file, FILE_NAME_LEN);

		if (pthread_create(&t0, NULL, runWssServerListenThread, (void *)&wss_info) != 0) {
			FLVERR("failed to create thread\r\n");
		}
		if (pthread_setname_np(t0, "wss-server") != 0) {
			FLVERR("failed to set thread name\r\n");
		}
	}

	if (cert_file != NULL) {
		free(cert_file);
	}

	if (key_file != NULL) {
		free(key_file);
	}

	pthread_attr_destroy(&attr);

	while (g_run_flag) {
		sleep(SLEEP_INTERVAL);
	}

	if (has_https) {
		FLVLOG("free https files\r\n");
		free(https_info.cert_file);
		free(https_info.key_file);
	}

	if (has_wss) {
		FLVLOG("free WSS files\r\n");
		free(wss_info.cert_file);
		free(wss_info.key_file);
	}

	MPI_exitBitStreamSystem();
	MPI_SYS_exit();

	return 0;
}