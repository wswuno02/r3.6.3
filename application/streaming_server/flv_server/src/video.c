#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "video.h"

int initStream(uint8_t chnNum, MPI_BCHN *pBchn)
{
	if ((chnNum > MPI_MAX_ENC_CHN_NUM) || (chnNum < 0)) { /*mpi get chn num*/
		fprintf(stderr, "Invalid chn num\n");
		return -1;
	}

	int ret = MPI_FAILURE;
	MPI_ECHN chn_idx;

	chn_idx = MPI_ENC_CHN(chnNum);
	*pBchn = MPI_createBitStreamChn(chn_idx);
	printf("bchn.value = %08X\n", pBchn->value);
	if (pBchn->value == MPI_VALUE_INVALID) {
		fprintf(stderr, "MPI_createBitStreamChn failed...\n");
		return -EACCES;
	}

	/*check codec*/
	MPI_VENC_ATTR_S attr;
	ret = MPI_ENC_getVencAttr(chn_idx, &attr);
	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "Failed to MPI_ENC_getVencAttr. %d\n", ret);
		return -EACCES;
	}
	if (attr.type != MPI_VENC_TYPE_H264) {
		fprintf(stderr, "Codec should be H264. %d\n", attr.type);
		return -ENOEXEC;
	}

	ret = MPI_ENC_requestIdr(chn_idx);
	printf("request IDR\n");
	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "Failed to MPI_ENC_requestIdr.\n");
	}

	return 0;
}

int uninitStream(MPI_BCHN *pBchn)
{
	printf("uninitStream\r\n");
	INT32 ret = MPI_FAILURE;
	ret = MPI_destroyBitStreamChn(*pBchn);
	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "Failed to destroy bitstream system");
		return -EACCES;
	}

	return 0;
}

#define FLV_JIFFIES "FLV_JIFFIES"
int readFrame(MPI_BCHN *pBchn, VIDEO_STREAM_DATA *pStreamData)
{
	INT32 ret = MPI_FAILURE;
	ret = MPI_getBitStream(*pBchn, &(pStreamData->params), 10000 /*ms*/);
	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "Failed to get stream param!");
		return -EIO;
	}
	char *p = secure_getenv(FLV_JIFFIES);
	if (p == NULL) {
	} else if (atoi(p) == 1) {
		printf("jiffies: %lu\r\n", (unsigned long)pStreamData->params.jiffies);
	}
#ifdef FLV_DEBUG
	printf("%d seg_cnt:%d type:%d \r\n", ret, pStreamData->params.seg_cnt, pStreamData->params.seg[0].type);
	printf("jiffies: %lu\r\n", (unsigned long)pStreamData->params.jiffies);
#endif
	return 0;
}

int releaseFrame(MPI_BCHN *pBchn, VIDEO_STREAM_DATA *pStreamData)
{
	INT32 ret = MPI_FAILURE;
	ret = MPI_releaseBitStream(*pBchn, &(pStreamData->params));
	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "Failed to release bit stream!\n");
		return -1;
	}
	return 0;
}
