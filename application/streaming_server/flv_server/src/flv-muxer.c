#include <stdio.h>
#include <stdlib.h>

#include "flv-muxer.h"
#include "amf-byte-stream.h"

struct flv_tag {
	uint8_t type;
	uint8_t data_size[3];
	uint8_t timestamp[3];
	uint8_t timestamp_ex;
	uint8_t streamid[3];
} __attribute__((__packed__));

typedef struct flv_tag flv_tag_t;

int write_flv_header(MediaSrcInfo *info, bool is_have_audio, bool is_have_video)
{
	char flv_file_header[] = "FLV\x1\x5\0\0\0\x9\0\0\0\0"; // have audio and have video

	if (is_have_audio && is_have_video) {
		flv_file_header[4] = 0x05;
	} else if (is_have_audio && !is_have_video) {
		flv_file_header[4] = 0x04;
	} else if (!is_have_audio && is_have_video) {
		flv_file_header[4] = 0x01;
	} else {
		flv_file_header[4] = 0x00;
	}

	return info->fWriteFlv(flv_file_header, 13, info->outputFd);
}

/*
* @brief write video tag
* @param[in] buf:
* @param[in] buf_len: flv tag body size
* @param[in] timestamp: flv tag timestamp
*/

int write_tag_ts(MediaSrcInfo *info, uint32_t buf_len, uint32_t timestamp, int type)
{
	flv_tag_t flvtag;

	memset(&flvtag, 0, sizeof(flvtag));

	flvtag.type = type;
	ui24_to_bytes(flvtag.data_size, buf_len);
	flvtag.timestamp_ex = (uint8_t)((timestamp >> 24) & 0xff);
	flvtag.timestamp[0] = (uint8_t)((timestamp >> 16) & 0xff);
	flvtag.timestamp[1] = (uint8_t)((timestamp >> 8) & 0xff);
	flvtag.timestamp[2] = (uint8_t)((timestamp)&0xff);

	return info->fWriteFlv(&flvtag, sizeof(flvtag), info->outputFd);
}

int write_tag_src(MediaSrcInfo *info, uint8_t *buf, uint32_t buf_len)
{
	return info->fWriteFlv(buf, buf_len, info->outputFd);
}

int write_tag_prevsize(MediaSrcInfo *info, uint32_t buf_len)
{
	uint8_t prev_size[4] = { 0 };
	ui32_to_bytes(prev_size, buf_len + (uint32_t)sizeof(flv_tag_t));

	return info->fWriteFlv(prev_size, 4, info->outputFd);
}

/*
* @brief write header of video tag data part, fixed 5 bytes
*
*/
int write_avc_data_tag(MediaSrcInfo *info, const uint8_t *sps, uint32_t sps_len, const uint8_t *pps, uint32_t pps_len,
                       const uint8_t *data, uint32_t data_len, uint32_t timestamp, int is_keyframe)
{
	uint8_t buf = 1;
	uint8_t buf32[4] = { 0 };
	uint8_t buf24[3] = { 0 };
	uint8_t flag;
	uint32_t sum_len = 4 + data_len + 5;
	if (sps_len > 0) {
		if ((sps[1] != info->videoProfile) || (sps[3] != info->videoLevel)) {
			fprintf(stderr, "Need resend seq\r\n");
			info->videoProfile = sps[1];
			info->videoLevel = sps[3];
			return -EIO;
		}

		sum_len += 4 + sps_len;
	}

	if (pps_len > 0) {
		sum_len += 4 + pps_len;
	}

	int ret = 0;

	ret = write_tag_ts(info, sum_len, timestamp, FLV_TAG_TYPE_VIDEO);
	if (ret < 0) {
		return ret;
	}
	// (FrameType << 4) | CodecID, 1 - keyframe, 2 - inner frame, 7 - AVC(h264)
	if (is_keyframe) {
		flag = 0x17;
	} else {
		flag = 0x27;
	}

	ret = write_tag_src(info, &flag, 1);
	if (ret < 0) {
		return ret;
	}
	ret = write_tag_src(info, &buf, 1); // AVCPacketType: 0x00 - AVC sequence header; 0x01 - AVC NALU
	if (ret < 0) {
		return ret;
	}

	ret = write_tag_src(info, &buf24[0], 3); // composition time
	if (ret < 0) {
		return ret;
	}

	if (sps_len > 0) {
		ui32_to_bytes(&buf32[0], sps_len);
		ret = write_tag_src(info, &buf32[0], 4);
		if (ret < 0) {
			return ret;
		}

		ret = write_tag_src(info, (uint8_t *)sps, sps_len);
		if (ret < 0) {
			return ret;
		}
	}

	if (pps_len > 0) {
		ui32_to_bytes(&buf32[0], pps_len);
		ret = write_tag_src(info, &buf32[0], 4);
		if (ret < 0) {
			return ret;
		}

		ret = write_tag_src(info, (uint8_t *)pps, pps_len);
		if (ret < 0) {
			return ret;
		}
	}

	ui32_to_bytes(&buf32[0], data_len);
	ret = write_tag_src(info, &buf32[0], 4);
	if (ret < 0) {
		return ret;
	}
	ret = write_tag_src(info, (uint8_t *)data, data_len);
	if (ret < 0) {
		return ret;
	}

	ret = write_tag_prevsize(info, sum_len);
	if (ret < 0) {
		return ret;
	}

	return 0;
}

/*
* @brief write AVC sequence header in header of video tag data part, the first video tag
*/
int write_avc_sequence_header_tag(MediaSrcInfo *info, const uint8_t *sps, uint32_t sps_len, const uint8_t *pps,
                                  uint32_t pps_len, uint32_t timestamp)
{
	uint8_t flag = 0;
	//uint32_t timestamp = 0;
	uint8_t buf = 0;
	uint8_t buf16[2] = { 0 };
	uint8_t buf24[3] = { 0 };

	flag = (1 << 4) // frametype "1 == keyframe"
	       | 7; // codecid "7 == AVC"

	int ret = 0;

	ret = write_tag_ts(info, sps_len + pps_len + 16, timestamp, FLV_TAG_TYPE_VIDEO);
	if (ret < 0) {
		return ret;
	}
	ret = write_tag_src(info, &flag, 1);
	if (ret < 0) {
		return ret;
	}
	ret = write_tag_src(info, &buf, 1); // AVCPacketType: 0x00 - AVC sequence header; 0x01 - AVC NALU
	if (ret < 0) {
		return ret;
	}
	ret = write_tag_src(info, &buf24[0], 3); // composition time
	if (ret < 0) {
		return ret;
	}

	// generate AVCC with sps and pps, AVCDecoderConfigurationRecord
	buf = 1;
	ret = write_tag_src(info, &buf, 1); // configurationVersion
	if (ret < 0) {
		return ret;
	}

	ui08_to_bytes(&buf, sps[1]); // AVCProfileIndication
	ret = write_tag_src(info, &buf, 1);
	if (ret < 0) {
		return ret;
	}

	ui08_to_bytes(&buf, sps[2]); // profile_compatibility
	ret = write_tag_src(info, &buf, 1);
	if (ret < 0) {
		return ret;
	}

	ui08_to_bytes(&buf, sps[3]); // AVCLevelIndication
	ret = write_tag_src(info, &buf, 1);
	if (ret < 0) {
		return ret;
	}

	info->videoProfile = sps[1];
	info->videoLevel = sps[3];

	// 6 bits reserved (111111) + 2 bits nal size length - 1
	// (Reserved << 2) | Nal_Size_length = (0x3F << 2) | 0x03 = 0xFF
	ui08_to_bytes(&buf, 0xff);
	ret = write_tag_src(info, &buf, 1);
	if (ret < 0) {
		return ret;
	}

	// 3 bits reserved (111) + 5 bits number of sps (00001)
	// (Reserved << 5) | Number_of_SPS = (0x07 << 5) | 0x01 = 0xe1
	ui08_to_bytes(&buf, 0xe1);
	ret = write_tag_src(info, &buf, 1);
	if (ret < 0) {
		return ret;
	}

	// sps
	ui16_to_bytes(&buf16[0], (uint16_t)sps_len);
	ret = write_tag_src(info, &buf16[0], 2);
	if (ret < 0) {
		return ret;
	}
	ret = write_tag_src(info, (uint8_t *)sps, sps_len);
	if (ret < 0) {
		return ret;
	}

	// pps
	ui08_to_bytes(&buf, 1); // number of pps
	ret = write_tag_src(info, &buf, 1);
	if (ret < 0) {
		return ret;
	}
	ui16_to_bytes(&buf16[0], (uint16_t)pps_len);
	ret = write_tag_src(info, &buf16[0], 2);
	if (ret < 0) {
		return ret;
	}

	ret = write_tag_src(info, (uint8_t *)pps, pps_len);
	if (ret < 0) {
		return ret;
	}

	ret = write_tag_prevsize(info, sps_len + pps_len + 16);

	return ret;
}

int write_aac_sequence_header_tag(MediaSrcInfo *info, int sample_rate, int channel, uint32_t timestamp)
{
	uint8_t flag = 0xa0 + 0x0f; //AAC-LC u(4:7-4)
	uint8_t buf = 0;
	uint8_t aacSeqHeader[5] = { 0x15, 0x88, 0x56, 0xe5, 0x00 };
	int ret = 0;

	ret = write_tag_ts(info, 7, timestamp, FLV_TAG_TYPE_AUDIO);
	if (ret < 0) {
		return ret;
	}
	ret = write_tag_src(info, &flag, 1); //codec type AAC
	if (ret < 0) {
		return ret;
	}

	ret = write_tag_src(info, &buf, 1); // AACPacketType: 0x00 - AAC specific config; 0x01 - Frame data
	if (ret < 0) {
		return ret;
	}

	ret = write_tag_src(info, (uint8_t *)&aacSeqHeader[0], 5);
	if (ret < 0) {
		return ret;
	}

	ret = write_tag_prevsize(info, 7);
	if (ret < 0) {
		return ret;
	}

	return ret;
}

int write_aac_data_tag(MediaSrcInfo *info, const uint8_t *data, uint32_t data_len, uint32_t timestamp)
{
	uint8_t flag = 0xa0 + 0x0f; //AAC-LC u(4:7-4)
	uint8_t buf = 1;
	int ret = 0;

	ret = write_tag_ts(info, data_len + 2, timestamp, FLV_TAG_TYPE_AUDIO);
	if (ret < 0) {
		return ret;
	}

	ret = write_tag_src(info, &flag, 1); //codec type AAC
	if (ret < 0) {
		return ret;
	}

	ret = write_tag_src(info, &buf, 1); // AACPacketType: 0x00 - AAC specific config; 0x01 - Frame data
	if (ret < 0) {
		return ret;
	}

	ret = write_tag_src(info, (uint8_t *)data, data_len);
	if (ret < 0) {
		return ret;
	}

	ret = write_tag_prevsize(info, data_len + 2);
	return ret;
}
