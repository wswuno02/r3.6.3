#include "wss_server.h"

#include <libwebsockets.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>

#include <time.h>
#include <json.h>
#include "log_define.h"
#include "wss_msg.h"

snd_pcm_t *g_capture;

ServerStat g_stat = INIT;
MimeType g_mime = ALAW;
ServingClient g_client_info = { .client_num = 0, .wsi = NULL };

char *g_jsonBuf;
int g_buf_size;
extern int g_run_flag;

int WSS_pcmInitbyMimetype(MimeType type)
{
	/*snd_pcm init*/
	snd_pcm_uframes_t frame = 256;
	unsigned int channel = 1;
	unsigned int rate = 8000;
	unsigned int gain = 25;
	unsigned int stream = SND_PCM_STREAM_PLAYBACK;
	unsigned int format = SND_PCM_FORMAT_A_LAW; /*need parse correct format*/
	if (type == MULAW) {
		FLVLOG("format MULAW\r\n");
		format = SND_PCM_FORMAT_MU_LAW;
	}

	int err = 0;
	FLVLOG("pcm init format:%d\r\n", format);
	err = agtx_pcm_init(&g_capture, "default", stream, format, frame, rate, channel);
	if (err < 0) {
		FLVERR("pcm init failed\r\n");
		return -EIO;
	}

	err = set_gain(gain);
	if (err < 0) {
		FLVERR("set_gain failed: %s\n", snd_strerror(err));
	}

	return err;
}

int WSS_pcmDeinit()
{
	int err = agtx_pcm_uninit(g_capture);
	return err;
}

int WSS_pcmPlay(char *data, int len)
{
	int buf_size = 256;
	int remain_size = len;
	int idx = 0;
	int err = 0;
	while (remain_size > 0) {
		err = snd_pcm_writei(g_capture, data + idx, buf_size);
		if (err == -EPIPE) {
			/* EPIPE means xrun, 0 data */
			FLVLOG("Underrun occrred: %s\n", snd_strerror(err));
			snd_pcm_prepare(g_capture);
		} else if (err < 0) {
			fprintf(stderr, "error from write: %s\n", snd_strerror(err));
		} else if (err != buf_size) {
			fprintf(stderr, "Short write (expected %d, wrote %d)\n", (int)buf_size, (int)err);
		}

		remain_size -= buf_size;
		idx += buf_size;
	}

	return 0;
}

int wsCallback(struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len)
{
	int ret = 0;
	PcmMessage msg = { 0 };
	SessionData *data = (SessionData *)user;

	switch (reason) {
	case LWS_CALLBACK_SERVER_NEW_CLIENT_INSTANTIATED:
		FLVLOG("ask need to close old client?\r\n");
		FLVLOG("enter client[%d]\r\n", g_client_info.client_num);
		g_client_info.client_num += 1;
		if (g_client_info.client_num == 2) {
			ServingClient *old_client = &g_client_info;
			lws_set_timeout(old_client->wsi, PENDING_TIMEOUT_AWAITING_PROXY_RESPONSE, LWS_TO_KILL_ASYNC);
		}
		g_client_info.wsi = wsi;
	case LWS_CALLBACK_ESTABLISHED:
		FLVLOG("Client connected.\n");
		lws_set_timeout(wsi, PENDING_TIMEOUT_AWAITING_PROXY_RESPONSE, 10);
		break;
	case LWS_CALLBACK_RECEIVE:
		data->fin = lws_is_final_fragment(wsi);
		data->bin = lws_frame_is_binary(wsi);
		lws_rx_flow_control(wsi, 0);

		if (g_jsonBuf == NULL) {
			g_buf_size = 0;
			g_jsonBuf = malloc(MAX_PAYLOAD_SIZE);
			memcpy(g_jsonBuf + g_buf_size, in, len);
			g_buf_size += len;
		} else if ((g_jsonBuf != NULL) && (g_buf_size < (MAX_PAYLOAD_SIZE - len - 1))) {
			memcpy(g_jsonBuf + g_buf_size, in, len);
			g_buf_size += len;
		}

		if (data->fin == 1) {
			FLVLOG("sum recv: %d\r\n", g_buf_size);

			/*parse msg when fin*/
			ret = WSS_parseWssPcmMessage(g_jsonBuf, &msg);
			g_buf_size = 0;

			if (ret != 0) {
				FLVERR("failed to parse\r\n");
				free(g_jsonBuf);
				g_jsonBuf = NULL;
				ServingClient *old_client = &g_client_info;
				lws_set_timeout(old_client->wsi, PENDING_TIMEOUT_AWAITING_PROXY_RESPONSE,
				                LWS_TO_KILL_ASYNC);
				break;
			}

			if (g_jsonBuf != NULL) {
				free(g_jsonBuf);
				g_jsonBuf = NULL;
				FLVLOG("free g_jsonBuf\r\n");
			}

			if (0 == strcmp(msg.msg_type, "ping")) {
				sprintf((char *)&data->buf[LWS_PRE], "{\"id\":%lu,\"type\":\"pong\"}",
				        (unsigned long)time(NULL));
				data->len = LWS_PRE + strlen("{\"id\":1634210417470,\"type\":\"pong\"}");
				FLVLOG("send pong, len:%d\r\n", data->len);
			} else if (0 == strcmp(msg.msg_type, "pong")) {
				/*do nothing*/
			} else if (0 == strcmp(msg.msg_type, "call_start")) {
				FLVLOG("call start\r\n");
				if ((g_stat == END) || (g_stat == INIT)) {
					g_stat = START;
				} else {
					FLVERR("unavailable stat:%d\r\n", g_stat);
				}

				lws_set_timeout(wsi, PENDING_TIMEOUT_AWAITING_PROXY_RESPONSE, 10);
			} else if (0 == strcmp(msg.msg_type, "call_end")) {
				FLVLOG("call end\r\n");
				if (g_stat == AVAIL) {
					FLVLOG("do snd pcm deinit\r\n");
					g_stat = END;
				} else {
					FLVERR("unavailable stat:%d\r\n", g_stat);
				}

				lws_set_timeout(wsi, PENDING_TIMEOUT_AWAITING_PROXY_RESPONSE, 1);

			} else if (0 == strcmp(msg.msg_type, "data_available")) {
				FLVLOG("data_available\r\n");
				if (g_stat == START) {
					FLVLOG("do snd pcm init\r\n");
					WSS_pcmInitbyMimetype(msg.type);
					WSS_pcmPlay(msg.data, msg.length);
					g_stat = AVAIL;
				} else if (g_stat == AVAIL) {
					/*check mimetype change*/
					WSS_pcmPlay(msg.data, msg.length);
				} else {
					FLVERR("unavailable stat:%d\r\n", g_stat);
				}

				lws_set_timeout(wsi, PENDING_TIMEOUT_AWAITING_PROXY_RESPONSE, 10);
			}

			if (msg.data != NULL) {
				free(msg.data);
			}
		}

		lws_callback_on_writable(wsi);
		break;
	case LWS_CALLBACK_SERVER_WRITEABLE:
		if (data->len > 0) {
			lws_write(wsi, &data->buf[LWS_PRE], data->len, LWS_WRITE_TEXT);
		}
		data->len = 0;
		lws_rx_flow_control(wsi, 1);
		break;
	case LWS_CALLBACK_WS_PEER_INITIATED_CLOSE:
		FLVERR("client end an unsolicited Close WS packet.\n");
		break;
	case LWS_CALLBACK_CLOSED:
		FLVINFO("Client disconnected.\n");
		WSS_pcmDeinit();

		if (msg.data != NULL) {
			free(msg.data);
		}
		if (g_jsonBuf != NULL) {
			free(g_jsonBuf);
			g_jsonBuf = NULL;
			FLVLOG("free g_jsonBuf\r\n");
		}
		g_client_info.client_num -= 1;
		g_stat = END;
		break;
	default:
		/* other reasons */
		break;
	}
	return EXIT_SUCCESS;
}

void *runWssServerListenThread(void *data)
{
	WssServerInfo *info = (WssServerInfo *)data;

#ifdef FLV_DEBUG
	int logs = LLL_USER | LLL_ERR | LLL_WARN | LLL_NOTICE | LLL_DEBUG;
#else
	int logs = LLL_USER | LLL_ERR | LLL_WARN;
#endif
	/*ws init*/
	lws_set_log_level(logs, NULL);
	lwsl_user("wss_server | visit http://localhost:%d\n", WS_PORT);

	struct lws_context_creation_info context_info = { 0 };
	context_info.port = info->port;
	context_info.protocols = protocols;
	context_info.extensions = extensions;
	context_info.ssl_cert_filepath = info->cert_file;
	context_info.ssl_private_key_filepath = info->key_file;
	context_info.options = LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT;
	struct lws_context *context = lws_create_context(&context_info);
	if (!context) {
		lwsl_err("lws init failed\n");
		return NULL;
	}

	while (g_run_flag) {
		lws_service(context, 0);
	}

	lws_context_destroy(context);
	pthread_detach(pthread_self());
	return NULL;
}