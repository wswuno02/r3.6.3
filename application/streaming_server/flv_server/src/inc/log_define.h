#ifndef LOG_DEFINE_H_
#define LOG_DEFINE_H_

//#define FLV_DEBUG
#ifdef FLV_DEBUG
#define FLVLOG(format, args...) fprintf(stderr, "[%s:%d]" format, __func__, __LINE__, ##args);
#else
#define FLVLOG(format, args...)
#endif
#define FLVERR(format, args...) fprintf(stderr, "[%s:%d]" format, __func__, __LINE__, ##args);
#define FLVINFO(format, args...) fprintf(stderr, "[%s:%d]" format, __func__, __LINE__, ##args);

#endif