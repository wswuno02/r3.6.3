#ifndef FLV_MUXER_H_
#define FLV_MUXER_H_

#include <stdbool.h>
#include <stdint.h>
#include <errno.h>

#include "mpi_dip_sns.h"
#include "mpi_enc.h"

typedef struct {
	bool hasVideo;
	bool hasAudio;
	int chnNum;
	char audioCodec;
	char videoCodec;
	char videoProfile;
	char videoLevel;
	int videoLen;
	int outputFd;
	bool (*fCheckCodecChange)(char /*chnNum*/);
	int (*fWriteFlv)(void *, uint32_t, int /*flv socketfd*/);
	int (*fFlvOpen)(char *);
	int (*fFlvClose)();
} MediaSrcInfo;

typedef struct {
	MPI_STREAM_PARAMS_S params;
	unsigned char *p_sei_nalu;
	int sei_nalu_len;
} VIDEO_STREAM_DATA;

int write_flv_header(MediaSrcInfo *info, bool is_have_audio, bool is_have_video);

int write_aac_sequence_header_tag(MediaSrcInfo *info, int sample_rate, int channel, uint32_t timestamp);

int write_avc_sequence_header_tag(MediaSrcInfo *info, const uint8_t *sps, uint32_t sps_len, const uint8_t *pps,
                                  uint32_t pps_len, uint32_t timestamp);

int write_aac_data_tag(MediaSrcInfo *info, const uint8_t *data, uint32_t data_len, uint32_t timestamp);

int write_avc_data_tag(MediaSrcInfo *info, const uint8_t *sps, uint32_t sps_len, const uint8_t *pps, uint32_t pps_len,
                       const uint8_t *data, uint32_t data_len, uint32_t timestamp, int is_keyframe);

#endif // FLV_MUXER_H_