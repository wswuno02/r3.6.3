#ifndef HTTP_FLV_H
#define HTTP_FLV_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string.h>
#include <unistd.h> // for write
#include "http_flv_parser.h"

void HTTP_checkResponseSize(char *src, uint32_t strlen, char *size);
int HTTP_executeDeviceCmd(struct message *m);
int HTTP_setlistenPort(int port, int timeout_s);
bool HTTP_checkCodecChange(char chn_num);

#endif