#ifndef VIDEO_H_
#define VIDEO_H_

#include "mpi_dip_sns.h"
#include "mpi_sys.h"
#include "mpi_dev.h"
#include "mpi_enc.h"
#include "mpi_osd.h"
#include "mpi_iva.h"
#include "mpi_limits.h"

#include "flv-muxer.h"

int initStream(uint8_t chnNum, MPI_BCHN *pBchn);

int uninitStream(MPI_BCHN *pBchn);

int readFrame(MPI_BCHN *pBchn, VIDEO_STREAM_DATA *pStreamData);

int releaseFrame(MPI_BCHN *pBchn, VIDEO_STREAM_DATA *pStreamData);

#endif