#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <getopt.h>
#include <pthread.h>
#include <errno.h>
#include "time.h"

#include "flv-muxer.h"

#include "audio.h"
#include "video.h"


#define FLV_DEBUG
#ifdef FLV_DEBUG
#define LOG(format, args...) fprintf(stderr, "[%s:%d]" format, __func__, __LINE__, ##args);
#else
#define LOG(format, args...)
#endif
#define ERR(format, args...) fprintf(stderr, "[%s:%d]" format, __func__, __LINE__, ##args);
#define INFO(format, args...) fprintf(stderr, "[%s:%d]" format, __func__, __LINE__, ##args);

static const char *g_device[] = { "default", "hw:0,0" }; /* sound device */
int g_run_flag = 0;
FILE *g_fp;

bool checkCodecChange(char chnNum)
{
	MPI_VENC_ATTR_S venc_attr;
	MPI_ENC_getVencAttr(MPI_ENC_CHN(chnNum), &venc_attr);
	if (venc_attr.type != MPI_VENC_TYPE_H264) {
		return true;
	}

	return false;
}

int writeFLVOutput(void *src, uint32_t len, int fd)
{
	if (g_fp == NULL) {
		ERR("fp is null\r\n");
		return -EIO;
	}

	fwrite(src, len, 1, g_fp);
	return 0;
}

int openFLVOutput(char *filename)
{
	if (NULL == filename) {
		return -EIO;
	}

	g_fp = fopen(filename, "wb");
	if (g_fp == NULL) {
		fprintf(stderr, "failed to open flv\r\n");
	}

	return 0;
}

int closeFLVOutput()
{
	fclose(g_fp);
	return 0;
}

static void handleSigInt(int signo)
{
	if (signo == SIGINT) {
		INFO("Caught SIGINT!\n");
	} else if (signo == SIGTERM) {
		INFO("Caught SIGTERM!\n");
	} else if (signo == SIGPIPE) {
		INFO("Caught SIGPIPE!\n");
		ERR("pls re-start flv-recorder\n");
		return;
	} else {
		perror("Unexpected signal!\n");
	}
	g_run_flag = 0;
}

void help()
{
	printf("[Usage]:\r\n");
	printf("-i save .flv path, dft ./save_flv.flv\r\n");
	printf("-c video chn idx\r\n");
	printf("-a has audio or not 1 or 0\r\n");
	printf("-h help()\r\n");
}

int main(int argc, char **argv)
{
	if (signal(SIGINT, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGINT!\n");
		exit(1);
	}

	if (signal(SIGTERM, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGTERM!\n");
		exit(1);
	}

	if (signal(SIGPIPE, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGPIPE!\n");
		exit(1);
	}

	int c;
	uint8_t chnNum = 0;
	int isAudio = 1;
	char savePath[128];
	snprintf(&savePath[0], 128, "%s", "./save_flv.flv");

	while ((c = getopt(argc, argv, "hc:a:i:")) != -1) {
		switch (c) {
		case 'h':
			help();
			exit(1);
			break;
		case 'c':
			chnNum = atoi(argv[optind - 1]);
			break;
		case 'i':
			snprintf(&savePath[0], 128, "%s", argv[optind - 1]);
			break;
		case 'a':
			isAudio = atoi(argv[optind - 1]);
			break;
		default:
			help();
			exit(1);
		}
	}

	INFO("flv mux video chn[%d], audio: %s, save to: %s\r\n", chnNum, (isAudio == 1) ? "yes" : "no", savePath);

	/*init bistream*/
	int ret;
	uint8_t aac_ret;

	MPI_BCHN Bchn;
	VIDEO_STREAM_DATA StreamData;
	bool is_first_frame = true;

	if (MPI_SYS_init() != MPI_SUCCESS) {
		ERR("Failed to MPI_SYS_init!");
		return -1;
	}

	ret = MPI_initBitStreamSystem(); /*only once, first clients,  destroy if no client*/
	if (ret != MPI_SUCCESS) {
		ERR("MPI_initBitStreamSystem_failed...\n");
		g_run_flag = 0;
		return -EIO;
	}

	ret = initStream(chnNum, &Bchn);
	if (ret != 0) {
		ERR("failed to init bitstream\n");
		return -EIO;
	}

	/*audio init*/
	snd_pcm_stream_t stream = SND_PCM_STREAM_CAPTURE;
	snd_pcm_format_t format = SND_PCM_FORMAT_S16_LE;
	unsigned int rate = 8000;
	int dev_id = 0;
	snd_pcm_uframes_t frame = 1024;
	int channel = 1;
	int buf_len = 1024 * 2 * 1;
	char aac_buf[buf_len];
	char pcm_buf[buf_len];
	ty_media_aac_handle_s hdl;
	int frames = 1024;
	bool is_first_aac = true;
	uint32_t audio_ts = 0;
	snd_pcm_t *p_capture;

	if (isAudio) {
		ret = agtx_pcm_init(&p_capture, g_device[dev_id], stream, format, frame, rate, channel);
		if (ret < 0) {
			ERR("failed to init pcm, %d\n", ret);
			isAudio = 0;
		} else {
			if (aac_encoder_init(&hdl, channel, rate, rate)) {
				ERR("Failed to init AAC encoder.\n");
				isAudio = 0;
			} else {
				snd_pcm_nonblock(p_capture, 1);
			}
		}
	}

	g_run_flag = 1;

	MediaSrcInfo srcInfo;
	srcInfo.chnNum = chnNum;
	srcInfo.outputFd = 0;
	srcInfo.fCheckCodecChange = checkCodecChange;
	srcInfo.fWriteFlv = writeFLVOutput;
	srcInfo.fFlvOpen = openFLVOutput;
	srcInfo.fFlvClose = closeFLVOutput;

	srcInfo.fFlvOpen(&savePath[0]);
	write_flv_header(&srcInfo, isAudio, true);

	unsigned int time_diff = 0;
	int start_time_jiff = 0;

	while (g_run_flag) {
		ret = readFrame(&Bchn, &StreamData);
		if (ret != 0) {
			g_run_flag = 0;
			break;
		}
		/*Mux video tag + send*/
		if (is_first_frame) {
			start_time_jiff = StreamData.params.jiffies;
			time_diff = 0;
			write_avc_sequence_header_tag(&srcInfo, StreamData.params.seg[0].uaddr + 4,
			                              StreamData.params.seg[0].size - 4,
			                              StreamData.params.seg[1].uaddr + 4,
			                              StreamData.params.seg[1].size - 4, time_diff);

			is_first_frame = false;
		} else {
			time_diff = (StreamData.params.jiffies - start_time_jiff) * 10;
		}

		if (StreamData.params.seg[0].type == MPI_FRAME_TYPE_SPS) {
			uint32_t sps_len = StreamData.params.seg[0].size - 4;
			uint32_t pps_len = StreamData.params.seg[1].size - 4;

			uint32_t size = StreamData.params.seg[2].size - 4;
			if (StreamData.params.seg_cnt > 3) {
				for (int i = 3; i < StreamData.params.seg_cnt; i++) {
					size += StreamData.params.seg[i].size;
				}
			}
			LOG("I size %d\r\n", size);
			ret = write_avc_data_tag(&srcInfo, StreamData.params.seg[0].uaddr + 4, sps_len,
			                         StreamData.params.seg[1].uaddr + 4, pps_len,
			                         StreamData.params.seg[2].uaddr + 4, size, time_diff, 1);
			if (ret == -EACCES) {
				releaseFrame(&Bchn, &StreamData);
				g_run_flag = 0;
				fprintf(stderr, "Unavailable codec\r\n");
				break;
			} else if (ret == -EIO) { /*change profile*/
				write_avc_sequence_header_tag(&srcInfo, StreamData.params.seg[0].uaddr + 4,
				                              StreamData.params.seg[0].size - 4,
				                              StreamData.params.seg[1].uaddr + 4,
				                              StreamData.params.seg[1].size - 4, time_diff);
				write_avc_data_tag(&srcInfo, StreamData.params.seg[0].uaddr + 4, sps_len,
				                   StreamData.params.seg[1].uaddr + 4, pps_len,
				                   StreamData.params.seg[2].uaddr + 4, size, time_diff, 1);
				audio_ts = time_diff;
			}

		} else {
			uint32_t size = StreamData.params.seg[0].size - 4;
			if (StreamData.params.seg_cnt > 1) {
				for (int i = 1; i < StreamData.params.seg_cnt; i++) {
					size += StreamData.params.seg[i].size;
				}
			}

			write_avc_data_tag(&srcInfo, NULL, 0, NULL, 0, StreamData.params.seg[0].uaddr + 4, size,
			                   time_diff, 0);
		}

		releaseFrame(&Bchn, &StreamData);

		if (isAudio) {
			while ((g_run_flag == 1) && (ret != -EAGAIN)) {
				ret = snd_pcm_readi(p_capture, pcm_buf, frames);
				if (ret == -EPIPE) {
					snd_pcm_prepare(p_capture);
					ERR("-EPIPE\n");
					continue;
				} else if (ret == -EAGAIN) {
					/* means there is no data, break for loop */
					break;
				} else if (ret < 0) {
					ERR("snd pcm unknown err: %d\r\n", ret);
					break;
				}
				int aac_recv = ret * 2;
				aac_ret = aac_encoder_data(&hdl, pcm_buf, ret * 2, ret, aac_buf, &aac_recv);
				if (aac_ret != AACENC_OK) {
					//FLVERR("aac_ret != AACENC_OK\n");
					break;
				}

				audio_ts += 128;
				if (is_first_aac == true) {
					write_aac_sequence_header_tag(&srcInfo, rate, channel, 0);

					is_first_aac = false;
				}

				write_aac_data_tag(&srcInfo, (uint8_t *)&aac_buf[7], aac_recv - 7, audio_ts);
			}
		}
	}

	if (isAudio) {
		ret = agtx_pcm_uninit(p_capture);
		if (ret < 0) {
			ERR("failed to uninit pcm, %d\n", ret);
			isAudio = 0;
		}
	}

	srcInfo.fFlvClose();
	uninitStream(&Bchn);

	return 0;
}