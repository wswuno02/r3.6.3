#include "sock/sockutil.h"
#include "sock/system.h"
#include "rtmp/rtmp-client.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <signal.h>
#include <getopt.h>
#include <pthread.h>

#include "assert.h"
#include <sys/msg.h>

#include "mpi_dip_sns.h"
#include "mpi_sys.h"
#include "mpi_dev.h"
#include "mpi_enc.h"
#include "mpi_osd.h"
#include "mpi_iva.h"
#include <alsa/asoundlib.h>
#include <alsa/hwdep.h>
#include <alsa/error.h>
#include <pcm_interfaces.h>

#include "flv/aac.h"
#include "flv/flv_mux.h"
#include "rtmp/rtmp-msg.h"

#define RTMP_JIFFIES "RTMP_JIFFIES"

static const char *g_device[] = { "default", "hw:0,0" }; /* sound device */

int g_re_connect_signal = 0;

int chn_run_flag[2] = { 0, 0 };
int chn_audio_flag[2] = { 0, 0 };

//#define VERBOSE

int rtmp_publish_stop(int chn)
{
	chn_run_flag[chn] = 0;
	chn_audio_flag[chn] = 0;
	return 0;
}

int agtx_pcm_init(snd_pcm_t **pcm_handle, const char *device, snd_pcm_stream_t stream, snd_pcm_format_t format,
                  snd_pcm_uframes_t frame, unsigned int rate, unsigned int channels)
{
	int err = 0;
	int ret;
	snd_pcm_hw_params_t *params;
	snd_pcm_sw_params_t *swparams;

	/* return error if already initialized */
	err = snd_pcm_open(pcm_handle, device, stream, SND_PCM_NONBLOCK);
	//err = snd_pcm_open(pcm_handle, device, stream, 0);
	if (err < 0) {
		*pcm_handle = NULL;
		return err;
	}

	/* configure alsa devicei, including layout, format, channels, sample rate, and periords */
	snd_pcm_hw_params_malloc(&params);
	snd_pcm_hw_params_any(*pcm_handle, params);
	if (snd_pcm_hw_params_set_access(*pcm_handle, params, SND_PCM_ACCESS_RW_INTERLEAVED) < 0) {
		goto no_support;
	}

	if (snd_pcm_hw_params_set_format(*pcm_handle, params, format) < 0) {
		goto no_support;
	}

	if (snd_pcm_hw_params_set_channels(*pcm_handle, params, channels) < 0) {
		goto no_support;
	}

	if (snd_pcm_hw_params_set_rate_near(*pcm_handle, params, &rate, 0) < 0) {
		goto no_support;
	}

	//int period = snd_pcm_hw_params_set_period_size_near(*pcm_handle, params, &frame, 0);
	//fprintf(stdout, "period: %d\n", frame);

	if (snd_pcm_hw_params_set_period_size_near(*pcm_handle, params, &frame, 0) < 0) {
		goto no_support;
	}

	/* apply settings to hardware */
	if (snd_pcm_hw_params(*pcm_handle, params) < 0) {
		goto no_support;
	}

	snd_pcm_hw_params_free(params);

	if (snd_pcm_nonblock(*pcm_handle, 1) < 0) {
		fprintf(stderr, "failed to set ALSA pcm nonblock\r\n");
	}

	//fprintf(stdout, "pcm hw params set\n");
	/*Enable tstamp in sw_params*/
	snd_pcm_sw_params_alloca(&swparams);
	ret = snd_pcm_sw_params_current(*pcm_handle, swparams);
	if (ret)
		goto no_support;

	ret = snd_pcm_sw_params_set_tstamp_mode(*pcm_handle, swparams, SND_PCM_TSTAMP_ENABLE);
	if (ret)
		goto no_support;

	ret = snd_pcm_sw_params_set_tstamp_type(*pcm_handle, swparams, SND_PCM_TSTAMP_TYPE_GETTIMEOFDAY);
	if (ret)
		goto no_support;

	snd_pcm_sw_params(*pcm_handle, swparams);
	if (ret < 0)
		goto no_support;

	return 0;

no_support:
	perror("configure alsa device failure");
	snd_pcm_close(*pcm_handle);
	*pcm_handle = NULL;
	return -1;
}

int help()
{
	printf("USAGE:\n");
	printf("\t-i rtmp server ip\n");
	printf("\t-s 0/1 select v chn\n");
	printf("\t-a y/n, default:n on/off audio\n");
	printf("\t-h help()\n");

	return 0;
}

static void handleSigInt(int signo)
{
	if (signo == SIGINT) {
		printf("Caught SIGINT!\n");
	} else if (signo == SIGTERM) {
		printf("Caught SIGTERM!\n");
	} else if (signo == SIGPIPE) {
		printf("Caught SIGPIPE!\n");
		rtmp_publish_stop(0);
		rtmp_publish_stop(1);
		fprintf(stderr, "Failed to connect rtmp server, pls re-start publisher\n");
		g_re_connect_signal = 1;
		return;
	} else {
		perror("Unexpected signal!\n");
	}
	rtmp_publish_stop(0);
	rtmp_publish_stop(1);
}

int initStream(uint8_t chnNum, MPI_BCHN *pBchn)
{
	if ((chnNum > 1) || (chnNum < 0)) {
		fprintf(stderr, "Invalid chn num\n");
		return -1;
	}

	int ret = MPI_FAILURE;
	MPI_ECHN chn_idx;

	if (chnNum == 0) {
		chn_idx = MPI_ENC_CHN(0);
	} else {
		chn_idx = MPI_ENC_CHN(1);
	}

	ret = MPI_initBitStreamSystem();
	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "MPI_initBitStreamSystem_failed...\n");
		return -1;
	}

	*pBchn = MPI_createBitStreamChn(chn_idx);
	PRINTF("bchn.value = %08X\n", pBchn->value);
	if (pBchn->value == MPI_VALUE_INVALID) {
		fprintf(stderr, "MPI_createBitStreamChn failed...\n");
		return -1;
	}
	ret = MPI_ENC_requestIdr(chn_idx);
	PRINTF("request IDR\n");
	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "Failed to MPI_ENC_requestIdr.\n");
	}
	return 0;
}

int releaseFrame(MPI_BCHN *pBchn, VIDEO_STREAM_DATA *pStreamData)
{
	INT32 ret = MPI_FAILURE;
	ret = MPI_releaseBitStream(*pBchn, &(pStreamData->params));
	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "Failed to release bit stream!\n");
		return -1;
	}
	return 0;
}

int readFrame(MPI_BCHN *pBchn, VIDEO_STREAM_DATA *pStreamData)
{
	INT32 ret = MPI_FAILURE;
	ret = MPI_getBitStream(*pBchn, &(pStreamData->params), -1 /*ms*/);
	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "Failed to get stream param!");
	}
	char *p = secure_getenv(RTMP_JIFFIES);
	if (p == NULL) {
	} else if (atoi(p) == 1) {
		printf("jiffies: %lu\r\n", (unsigned long)pStreamData->params.jiffies);
	}

	//PRINTF("seg_cnt:%d type:%d\n", pStreamData->params.seg_cnt, pStreamData->params.seg[0].type);

	return 0;
}

int uninitStream(MPI_BCHN *pBchn)
{
	INT32 ret = MPI_FAILURE;

	ret = MPI_destroyBitStreamChn(*pBchn);
	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "Failed to exit bitstream system");
		return -2;
	}
	return 0;
}

static int rtmp_client_send(void *param, const void *header, size_t len, const void *data, size_t bytes)
{
	socket_t *socket = (socket_t *)param;
	socket_bufvec_t vec[2];
	socket_setbufvec(vec, 0, (void *)header, len);
	socket_setbufvec(vec, 1, (void *)data, bytes);

	return socket_send_v_all_by_time(*socket, vec, bytes > 0 ? 2 : 1, 0, 1000);
}

static void rtmp_client_push(rtmp_client_t *rtmp, int chn_num)
{
	int r = 0;
	uint32_t timestamp = 0;
	uint32_t last_jiffies = 0;
	uint32_t jiffies = 0;
	uint32_t diff = 0;

	/*init bistream*/
	int ret;
	int chnNum = chn_num;
	MPI_BCHN Bchn;
	VIDEO_STREAM_DATA StreamData;
	bool isFirstFrame = true;

	ret = initStream(chnNum, &Bchn);
	if (ret != 0) {
		fprintf(stderr, "failed to init bitstream\n");
		return;
	}

	/*audio init*/
	snd_pcm_stream_t stream = SND_PCM_STREAM_CAPTURE;
	snd_pcm_format_t format = SND_PCM_FORMAT_S16_LE;
	unsigned int rate = 8000;
	//int bytes_per_sample = 2;
	int dev_id = 0;
	snd_pcm_uframes_t frame = 1024;
	int channel = 1;
	int buf_len = 1024 * 2 * 1;
	char aac_buf[buf_len];
	char pcm_buf[buf_len];
	ty_media_aac_handle_s hdl;
	int frames = 1024;
	int isFirstAAC = 1;
	uint32_t audioTs = 0;
	snd_pcm_t *p_capture;

	if (chn_audio_flag[chn_num]) {
		ret = agtx_pcm_init(&p_capture, g_device[dev_id], stream, format, frame, rate, channel);
		if (ret < 0) {
			fprintf(stderr, "failed to init pcm, %d\n", ret);
			chn_audio_flag[chn_num] = 0;
		} else {
			if (aac_encoder_init(&hdl, channel, rate, rate)) {
				fprintf(stderr, "Failed to init AAC encoder.\n");
				chn_audio_flag[chn_num] = 0;
			} else {
				snd_pcm_nonblock(p_capture, 1);
			}
		}
	}

	MEDIA_INFO info = { .hasVideo = true,
		            .hasAudio = true,
		            .audioCodec = UNKNOWN_CONF,
		            .videoCodec = VIDEO_CODEC_AVC,
		            .audioSampleRate = UNKNOWN_CONF,
		            .audioSndSize = UNKNOWN_CONF,
		            .audioChannels = UNKNOWN_CONF,
		            .chn_idx = MPI_ENC_CHN(chnNum) };

	ret = initFlvMuxer(&info, NULL);
	if (ret == VIDEO_INVALID_CODEC) {
		rtmp_publish_stop(chn_num);
		g_re_connect_signal = 2;
	}
	uint8_t VideoAVCTag[64];
	uint8_t VideoTag[TAG_MAX];
	int tagLen;
	uint8_t AACSpecificConf[SPEC_CONF_TAG_MAX];
	uint8_t AudioTag[TAG_MAX];
	int audioTagLen;

	while (chn_run_flag[chn_num]) {
		readFrame(&Bchn, &StreamData);

		jiffies = StreamData.params.jiffies;

		if (last_jiffies == 0) {
			diff = 33;
		} else {
			diff = (jiffies - last_jiffies) * 10; // jiffies = 1/100 sec
		}
		timestamp += diff;
		last_jiffies = jiffies;

		if ((isFirstFrame == true) && (StreamData.params.seg[0].type == MPI_FRAME_TYPE_SPS)) {
			/*avc seq*/
			muxVideoSeq(&info, &StreamData, &VideoAVCTag[0], &tagLen);
			isFirstFrame = false;
			r = rtmp_client_push_video(rtmp, VideoAVCTag, tagLen, timestamp);
#ifdef VERBOSE
			printf("video push: %u, %lu\n", tagLen, timestamp);
			printf("[%d]", tagLen);
			for (int i = 0; i < tagLen; i++) {
				printf("%0x ", VideoAVCTag[i]);
			}
			printf("\n");
#endif
		}

		if (isFirstFrame == false) {
			ret = muxVideo(&info, &StreamData, &VideoTag[0], &tagLen);
			if (ret == VIDEO_CHANGE_FORMAT) {
				audioTs = timestamp;
				muxVideoSeq(&info, &StreamData, &VideoAVCTag[0], &tagLen);
				r = rtmp_client_push_video(rtmp, VideoAVCTag, tagLen, timestamp);
			} else if (ret == VIDEO_INVALID_CODEC) {
				releaseFrame(&Bchn, &StreamData);
				rtmp_publish_stop(chn_num);
				fprintf(stderr, "stop streaming\n");
				g_re_connect_signal = 2;
				break;
			}
#ifdef VERBOSE
			PRINTF("[%d]:", tagLen);
			for (int i = 0; i < 16; i++) {
				PRINTF("%0x ", VideoTag[i]);
			}
			PRINTF("\n");
			printf("video push: %u, %lu\n", tagLen, timestamp);
#endif
			r = rtmp_client_push_video(rtmp, VideoTag, tagLen, timestamp);
		}

		if (r == -1) {
			fprintf(stderr, "Failed to connect rtmp server, pls re-start publisher\n");
			g_re_connect_signal = 1;
			break;
		}

		releaseFrame(&Bchn, &StreamData);

		for (int i = 0; i < 2 && chn_audio_flag[chn_num]; ++i) {
			ret = snd_pcm_readi(p_capture, pcm_buf, frames);

			if (ret == -EPIPE) {
				snd_pcm_prepare(p_capture);
				//fprintf(stdout, "-EPIPE\n");
				continue;
			} else if (ret == -EAGAIN) {
				/* means there is no data, break for loop */
				break;
			} else if (ret < 0) {
				goto end;
			}

			int aac_recv = ret * 2;

			if (aac_encoder_data(&hdl, pcm_buf, ret * 2, ret, aac_buf, &aac_recv)) {
				//fprintf(stderr, "AAC encoding error\n");
				continue;
			}

			//PRINTF(" %d aac %d samples\n", ret, aac_recv);
			/*
			 * NOTICE: this 
			 */
			audioTs += 128;
			char *p = secure_getenv(RTMP_JIFFIES);

			if (p == NULL) {
				; // do nothing
			} else if (atoi(p) == 1) {
				printf("audio ts: %d\r\n", audioTs);
			}

			if (isFirstAAC) {
				muxAudioAACSpecificConf(&AACSpecificConf[0], &audioTagLen);
#ifdef VERBOSE
				printf("len:%d: %0x %0x %0x\n", audioTagLen, AACSpecificConf[0], AACSpecificConf[1],
				       AACSpecificConf[2]);
				printf("audio push:%d, %lu \n", audioTagLen, audioTs);
#endif
				isFirstAAC = 0;
				r = rtmp_client_push_audio(rtmp, AACSpecificConf, audioTagLen, audioTs);
			}

			muxAudioAAC(&aac_buf[0], aac_recv, &AudioTag[0], &audioTagLen);
#ifdef VERBOSE
			printf("len:%d: %0x %0x %0x %0x\n", audioTagLen, AudioTag[0], AudioTag[1], AudioTag[2],
			       AudioTag[3]);
			printf("audio push:%d, %lu \n", audioTagLen, audioTs);
#endif
			r = rtmp_client_push_audio(rtmp, AudioTag, audioTagLen, audioTs);

			if (r == -1) {
				fprintf(stderr, "Failed to connect rtmp server, pls re-start publisher\n");
				g_re_connect_signal = 1;
				goto end;
			}
		}
	}

end:
	uninitStream(&Bchn);

	if (chn_audio_flag[chn_num]) {
		ret = snd_pcm_drop(p_capture);
		if (ret < 0) {
			fprintf(stderr, "Failed snd_pcm_drop, %d\n", ret);
		}

		ret = snd_pcm_close(p_capture);
		if (ret < 0) {
			fprintf(stderr, "Failed snd_pcm_close, %d\n", ret);
		}
	}

	uninitFlvMuxer(&info);

	return;
}

int rtmp_publish(const char *host, const char *app, const char *stream)
{
	int ret;
	struct sched_param tparam;
	pthread_t tid = pthread_self();
	tparam.sched_priority = 20;
	ret = pthread_setschedparam(tid, SCHED_RR, &tparam);
	if (ret) {
		fprintf(stderr, "Failed to set cloud recording policy to SCHED_RR\n");
	}

	char packet[2 * 1024 * 1024];
	snprintf(packet, sizeof(packet), "rtmp://%s/%s", host, app); // tcurl
	PRINTF("send to %s\n", packet);

	struct rtmp_client_handler_t handler;
	memset(&handler, 0, sizeof(handler));
	handler.send = rtmp_client_send;

	socket_init();
	socket_t socket = socket_connect_host(host, 1935, 2000);

	socket_setnonblock(socket, 0);
	socket_setnondelay(socket, 1); //disable Nagle

	rtmp_client_t *rtmp = rtmp_client_create(app, stream, packet /*tcurl*/, &socket, &handler);
	int r = rtmp_client_start(rtmp, 0);

	while (4 != rtmp_client_getstate(rtmp) && (r = socket_recv(socket, packet, sizeof(packet), 0)) > 0) {
		assert(0 == rtmp_client_input(rtmp, packet, r));
	}

	socket_setnonblock(socket, 1);
	if (strcmp(stream, "0") == 0) {
		rtmp_client_push(rtmp, 0);
	} else {
		rtmp_client_push(rtmp, 1);
	}

	rtmp_client_destroy(rtmp);
	socket_close(socket);
	socket_cleanup();
	printf("bitstream exit: %s\n", stream);
	return 0;
}

int checkSupportCodec(MPI_ECHN chn_idx)
{
	int ret = MPI_SUCCESS;
	int retryTimes = 10;

	MPI_VENC_ATTR_S p_venc_attr;
	ret = MPI_ENC_getVencAttr(chn_idx, &p_venc_attr);
	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "Failed to MPI_ENC_getVencAttr %d.\n", chn_idx.chn);
	}

	while (p_venc_attr.type != MPI_VENC_TYPE_H264) {
		if ((chn_run_flag[0] = 0) && (chn_run_flag[1] == 0)) {
			return -1;
		}

		ret = MPI_ENC_getVencAttr(chn_idx, &p_venc_attr);
		if (ret != MPI_SUCCESS) {
			fprintf(stderr, "Failed to MPI_ENC_getVencAttr %d.\n", chn_idx.chn);
		}

		usleep(10000);

		retryTimes--;
		if (retryTimes == 0) {
			return -1;
		}
	}

	return 0;
}

int main(int argc, char **argv)
{
	printf("rtmp publisher start\n");
	int ret;

	char *ip = "127.0.0.1";
	char *streamName = "0";
	int c;
	char *isAudio;

	if (signal(SIGINT, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGINT!\n");
		exit(1);
	}

	if (signal(SIGTERM, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGTERM!\n");
		exit(1);
	}

	if (signal(SIGPIPE, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGPIPE!\n");
		exit(1);
	}

	while ((c = getopt(argc, argv, "hs:i:a:")) != -1) {
		switch (c) {
		case 'h':
			help();
			exit(1);
			break;
		case 's':
			streamName = argv[optind - 1];
			break;
		case 'i':
			ip = argv[optind - 1];
			break;
		case 'a':
			isAudio = argv[optind - 1];
			break;
		default:
			help();
			exit(1);
		}
	}

	chn_audio_flag[0] = 0;
	chn_audio_flag[1] = 0;

	if (strcmp(isAudio, "0") == 0) {
		chn_audio_flag[0] = 1;
	} else if (strcmp(isAudio, "1") == 0) {
		chn_audio_flag[1] = 1;
	} else if (strcmp(isAudio, "y") == 0) {
		chn_audio_flag[0] = 1;
		chn_audio_flag[1] = 1;
	}

	MPI_ECHN chn_idx;
	if (strcmp(streamName, "0") == 0) {
		chn_idx.chn = 0;
	} else if (strcmp(streamName, "1") == 0) {
		chn_idx.chn = 1;
	} else {
		fprintf(stderr, "Unsuuport chn idx\r\n");
		return -1;
	}

	if (MPI_SYS_init() != MPI_SUCCESS) {
		fprintf(stderr, "Failed to MPI_SYS_init!");
		return -1;
	}

	ret = checkSupportCodec(chn_idx);
	if (ret != 0) {
		fprintf(stderr, "Unknown codec type\r\n");
		return -1;
	}

	if ((strcmp(streamName, "1") == 0) || (strcmp(streamName, "0") == 0)) {
		chn_run_flag[atoi(streamName)] = 1;
		rtmp_publish(ip, "live", streamName);
		while (g_re_connect_signal >= 1) {
			ret = checkSupportCodec(chn_idx);
			if (ret != 0) {
				fprintf(stderr, "Unknown codec type\r\n");
				break;
			}
			fprintf(stderr, "Wait for re-connect\n");
			chn_run_flag[atoi(streamName)] = 0;
			chn_audio_flag[atoi(streamName)] = 0;
			g_re_connect_signal = 0;
			sleep(5);
			chn_run_flag[atoi(streamName)] = 1;
			chn_audio_flag[atoi(streamName)] = 1;
			rtmp_publish(ip, "live", streamName);
		}
		/*reconnect signal= 2, codec change*/
	} else {
		fprintf(stderr, "unknown chn number\n");
	}

	return 0;
}
