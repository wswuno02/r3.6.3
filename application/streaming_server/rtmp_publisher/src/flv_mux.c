#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <assert.h>

#include "flv/flv_mux.h"

uint32_t u32tSwitchEndian(uint32_t var)
{
	uint32_t ret = 0;
	ret = ((var >> 24) & 0xff) | // move byte 3 to byte 0
	      ((var << 8) & 0xff0000) | // move byte 1 to byte 2
	      ((var >> 8) & 0xff00) | // move byte 2 to byte 1
	      ((var << 24) & 0xff000000); // byte 0 to byte 3
	return ret;
}

int initFlvMuxer(MEDIA_INFO *pMediaInfo, char *pFileName)
{
	if (pMediaInfo->isRecord == true) {
		if (pFileName == NULL) {
			fprintf(stderr, "failed to open flv file\n");
			return -1;
		}
	}
	MPI_VENC_ATTR_S venc_attr;
	MPI_ENC_getVencAttr(pMediaInfo->chn_idx, &venc_attr);
	if (venc_attr.type != MPI_VENC_TYPE_H264) {
		return VIDEO_INVALID_CODEC;
	}

	if (pMediaInfo == NULL) {
		fprintf(stderr, "Invalid flv info\n");
	}
	return 0;
}

int muxVideoSeq(MEDIA_INFO *pMediaInfo, VIDEO_STREAM_DATA *pStreamdata, uint8_t *pVideoTag, int *pTagLen)
{
	bool isKeyFrame = true;
	uint8_t packetType = PKT_AVC_SEQ_HEADER;
	uint8_t cts = 0x00;
	uint8_t version = 0x01;
	uint8_t avc_profile = pStreamdata->params.seg[0].uaddr[4 + 1];
	pMediaInfo->videoProfile = avc_profile;
	pMediaInfo->videoLen = pStreamdata->params.seg[0].uaddr[4 + 8];
	uint8_t compability = 0x00;
	uint8_t level = pStreamdata->params.seg[0].uaddr[4 + 3];
	uint8_t nalu_len_minus_one = AVC_NALU_LEN_SIZE_MINUS_1;
	uint8_t sps_num = 1;
	uint8_t sps_size = pStreamdata->params.seg[0].size - 4;
	uint8_t pps_num = 1;
	uint8_t pps_size = pStreamdata->params.seg[1].size - 4;

	uint8_t avc_seq_tag_idx = 0;

	if (isKeyFrame == true) {
		char codec_tag = 0x10 + pMediaInfo->videoCodec;
		memcpy(pVideoTag + avc_seq_tag_idx, &codec_tag, 1);
	} else {
		char codec_tag = 0x20 + pMediaInfo->videoCodec;
		memcpy(pVideoTag + avc_seq_tag_idx, &codec_tag, 1);
	}

	avc_seq_tag_idx++;
	memcpy(pVideoTag + avc_seq_tag_idx, &packetType, 1);
	avc_seq_tag_idx++;
	char zero = 0x00;
	memcpy(pVideoTag + avc_seq_tag_idx, &zero, 1);
	avc_seq_tag_idx++;
	memcpy(pVideoTag + avc_seq_tag_idx, &zero, 1);
	avc_seq_tag_idx++;
	memcpy(pVideoTag + avc_seq_tag_idx, &cts, 1);
	avc_seq_tag_idx++;
	memcpy(pVideoTag + avc_seq_tag_idx, &version, 1);
	avc_seq_tag_idx++;
	memcpy(pVideoTag + avc_seq_tag_idx, &avc_profile, 1);
	avc_seq_tag_idx++;
	memcpy(pVideoTag + avc_seq_tag_idx, &compability, 1);
	avc_seq_tag_idx++;
	memcpy(pVideoTag + avc_seq_tag_idx, &level, 1);
	avc_seq_tag_idx++;
	char nalu_len_tag = 0xfc + nalu_len_minus_one; /*u(2:b1-0)*/
	memcpy(pVideoTag + avc_seq_tag_idx, &nalu_len_tag, 1);
	avc_seq_tag_idx++;
	char sps_num_tag = 0xe0 + sps_num;
	memcpy(pVideoTag + avc_seq_tag_idx, &sps_num_tag, 1); /*u(5:b4-0)*/
	avc_seq_tag_idx++;
	memcpy(pVideoTag + avc_seq_tag_idx, &zero, 1);
	avc_seq_tag_idx++;
	memcpy(pVideoTag + avc_seq_tag_idx, &sps_size, 1);
	avc_seq_tag_idx++;
	memcpy(pVideoTag + avc_seq_tag_idx, pStreamdata->params.seg[0].uaddr + 4, sps_size);
	avc_seq_tag_idx += sps_size;
	memcpy(pVideoTag + avc_seq_tag_idx, &pps_num, 1);
	avc_seq_tag_idx++;
	memcpy(pVideoTag + avc_seq_tag_idx, &zero, 1);
	avc_seq_tag_idx++;
	memcpy(pVideoTag + avc_seq_tag_idx, &pps_size, 1);
	avc_seq_tag_idx++;
	memcpy(pVideoTag + avc_seq_tag_idx, pStreamdata->params.seg[0].uaddr + 4, pps_size);
	avc_seq_tag_idx += pps_size;

	*pTagLen = avc_seq_tag_idx;

	//PRINTF("len:%d,SPS[%d], PPS[%d]\n", avc_seq_tag_idx, sps_size, pps_size);
	assert(avc_seq_tag_idx == (sps_size + pps_size + 16));

	return avc_seq_tag_idx;
}

int muxVideo(MEDIA_INFO *pMediaInfo, VIDEO_STREAM_DATA *pStreamdata, uint8_t *pVideoTag, int *pTagLen)
{
	bool isKeyFrame;
	if (pStreamdata->params.seg[0].type == MPI_FRAME_TYPE_SPS) {
		isKeyFrame = true;
		/*check if profile or level or codec is changed*/
		MPI_VENC_ATTR_S venc_attr;
		MPI_ENC_getVencAttr(pMediaInfo->chn_idx, &venc_attr);
		if (venc_attr.type != MPI_VENC_TYPE_H264) {
			return VIDEO_INVALID_CODEC;
		}

		if (pMediaInfo->videoProfile != pStreamdata->params.seg[0].uaddr[4 + 1]) {
			fprintf(stderr, "[FLV Muxer]change sps, re-send avc seq header\n");
			return VIDEO_CHANGE_FORMAT;
		}

		if (pMediaInfo->videoLen != pStreamdata->params.seg[0].uaddr[4 + 8]) {
			fprintf(stderr, "[FLV Muxer]change sps, re-send avc seq header\n");
			return VIDEO_CHANGE_FORMAT;
		}

		assert((pStreamdata->params.seg[2].size - 4) + 5 < TAG_MAX);
	} else {
		isKeyFrame = false;
		assert((pStreamdata->params.seg[0].size - 4) + 5 < TAG_MAX);
	}

	uint8_t packetType = PKT_AVC_NALU;
	uint8_t cts = 0x28; /*not really sure*/

	uint32_t sps_len = 0;
	uint32_t pps_len = 0;
	//uint32_t sei_len = 0;
	uint32_t idr_len = 0;
	uint32_t p_frame_len = 0;

	if (isKeyFrame == true) {
		sps_len = pStreamdata->params.seg[0].size - 4;
		pps_len = pStreamdata->params.seg[1].size - 4;
		idr_len = pStreamdata->params.seg[2].size - 4;

		if (pStreamdata->params.seg_cnt > 3) {
			for (unsigned int i = 3; i < pStreamdata->params.seg_cnt; i++) {
				idr_len += pStreamdata->params.seg[i].size;
			}
		}
		//PRINTF("iskey %d %d %d\n", sps_len, pps_len, idr_len);
		sps_len = u32tSwitchEndian(sps_len);
		pps_len = u32tSwitchEndian(pps_len);
		idr_len = u32tSwitchEndian(idr_len);
	} else {
		p_frame_len = pStreamdata->params.seg[0].size - 4;
		if (pStreamdata->params.seg_cnt > 1) {
			for (unsigned int i = 1; i < pStreamdata->params.seg_cnt; i++) {
				p_frame_len += pStreamdata->params.seg[i].size;
			}
		}
		//PRINTF("not key %d\n", p_frame_len);
		p_frame_len = u32tSwitchEndian(p_frame_len);
	}

	uint32_t avc_seq_tag_idx = 0;

	char zero = 0x00;
	if (isKeyFrame == true) {
		char codec_tag = 0x10 + pMediaInfo->videoCodec;
		memcpy(pVideoTag + avc_seq_tag_idx, &codec_tag, 1);
	} else {
		char codec_tag = 0x20 + pMediaInfo->videoCodec;
		memcpy(pVideoTag + avc_seq_tag_idx, &codec_tag, 1);
	}
	avc_seq_tag_idx++;
	memcpy(pVideoTag + avc_seq_tag_idx, &packetType, 1);
	avc_seq_tag_idx++;
	memcpy(pVideoTag + avc_seq_tag_idx, &zero, 1);
	avc_seq_tag_idx++;
	memcpy(pVideoTag + avc_seq_tag_idx, &zero, 1);
	avc_seq_tag_idx++;
	memcpy(pVideoTag + avc_seq_tag_idx, &cts, 1);
	avc_seq_tag_idx++;
	if (isKeyFrame == true) {
		memcpy(pVideoTag + avc_seq_tag_idx, &sps_len, 4);
		avc_seq_tag_idx += 4;

		memcpy(pVideoTag + avc_seq_tag_idx, &pStreamdata->params.seg[0].uaddr + 4,
		                     pStreamdata->params.seg[0].size - 4);
		avc_seq_tag_idx += pStreamdata->params.seg[0].size - 4;

		memcpy(pVideoTag + avc_seq_tag_idx, &pps_len, 4);
		avc_seq_tag_idx += 4;
		memcpy(pVideoTag + avc_seq_tag_idx, pStreamdata->params.seg[1].uaddr + 4,
		                     pStreamdata->params.seg[1].size - 4);
		avc_seq_tag_idx += pStreamdata->params.seg[1].size - 4;

		memcpy(pVideoTag + avc_seq_tag_idx, &idr_len, 4);
		avc_seq_tag_idx += 4;
		assert((pStreamdata->params.seg[2].size - 4) <= (TAG_MAX - avc_seq_tag_idx));

		memcpy(pVideoTag + avc_seq_tag_idx, pStreamdata->params.seg[2].uaddr + 4,
		                     pStreamdata->params.seg[2].size - 4);
		avc_seq_tag_idx += pStreamdata->params.seg[2].size - 4;
		if (pStreamdata->params.seg_cnt > 3) {
			for (unsigned int i = 3; i < pStreamdata->params.seg_cnt; i++) {
				assert((pStreamdata->params.seg[i].size) <= (TAG_MAX - avc_seq_tag_idx));
				memcpy(pVideoTag + avc_seq_tag_idx, pStreamdata->params.seg[i].uaddr,
				                     pStreamdata->params.seg[i].size);
				avc_seq_tag_idx += pStreamdata->params.seg[i].size;
			}
		}
		//PRINTF("iskey idx %d\n", avc_seq_tag_idx);
	} else {
		memcpy(pVideoTag + avc_seq_tag_idx, &p_frame_len, 4);
		avc_seq_tag_idx += 4;

		assert((pStreamdata->params.seg[0].size - 4) <= (TAG_MAX - avc_seq_tag_idx));

		memcpy(pVideoTag + avc_seq_tag_idx, pStreamdata->params.seg[0].uaddr + 4,
		                     pStreamdata->params.seg[0].size - 4);
		avc_seq_tag_idx += pStreamdata->params.seg[0].size - 4;

		if (pStreamdata->params.seg_cnt > 1) {
			for (unsigned int i = 1; i < pStreamdata->params.seg_cnt; i++) {
				assert((pStreamdata->params.seg[i].size) <= (TAG_MAX - avc_seq_tag_idx));
				memcpy(pVideoTag + avc_seq_tag_idx, pStreamdata->params.seg[i].uaddr,
				                     pStreamdata->params.seg[i].size);
				avc_seq_tag_idx += pStreamdata->params.seg[i].size;
			}
		}
		//PRINTF("not key idx %d\n", avc_seq_tag_idx);
	}

	*pTagLen = avc_seq_tag_idx;
	//PRINTF("tag[%d]\n", avc_seq_tag_idx);

	assert(avc_seq_tag_idx <= TAG_MAX);
	/*len = 5 + (4 + nalus)*seg_cnt */

	return avc_seq_tag_idx;
}

int muxAudioAACSpecificConf(uint8_t *pAACSpecificConf, int *pTagLen)
{
	/*now only support aac 8khz mono*/
	uint8_t acodec = 0x0f + AUDIO_CODEC_AAC; //u(4:7-4)
	uint8_t packetType = PKT_AAC_SPECIAL_CONF;
	uint8_t aac_special_conf[5] = { 0x15, 0x88, 0x56, 0xe5, 0x00 };

	*pTagLen = 2 + 5;
	assert(*pTagLen <= SPEC_CONF_TAG_MAX);

	memcpy(pAACSpecificConf, &acodec, 1);
	memcpy(pAACSpecificConf + 1, &packetType, 1);
	memcpy(pAACSpecificConf + 2, &aac_special_conf[0], 5);

	return 0;
}

int muxAudioAAC(char *aac_buf, int aac_len, uint8_t *pAudioTag, int *pTagLen)
{
	/*now only support aac 8khz mono*/
	uint8_t acodec = 0x0f + AUDIO_CODEC_AAC; //u(4:7-4)
	uint8_t packetType = PKT_AAC_PAYLOAD;

	*pTagLen = (aac_len - 7) + 2;
	assert(*pTagLen <= TAG_MAX);

	uint32_t aac_tag_idx = 0;

	memcpy(pAudioTag + aac_tag_idx, &acodec, 1);
	aac_tag_idx++;
	memcpy(pAudioTag + aac_tag_idx, &packetType, 1);
	aac_tag_idx++;
	memcpy(pAudioTag + aac_tag_idx, (uint8_t *)(aac_buf + 7), aac_len - 7);

#ifdef VERBOSE
	for (int i = 7; i < 16; i++) {
		printf("%0x ", (uint8_t **)(aac_buf[i]));
	}
#endif
	return 0;
}

int uninitFlvMuxer(MEDIA_INFO *pMediaInfo)
{
	if (pMediaInfo->isRecord == true) {
		/*SAVE AND CLOSE FLV FILE*/
	}

	return 0;
}