#ifndef _RTMP_MSG_H_
#define _RTMP_MSG_H_

#define MSG_FILE_0 "/tmp/rtmp0"
#define MSG_FILE_1 "/tmp/rtmp1"

#define MSG_CMD_TYPE_1 (881)
#define MSG_CMD_TYPE_0 (880)
#define MSG_CMD_TYPE (888)
#define MSG_RET_TYPE (999)
#define MSG_LEN (256)

struct msg_form {
	int mtype;
	char mtext[MSG_LEN];
};

#endif
