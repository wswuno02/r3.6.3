#ifndef FLV_RECORD_H_
#define FLV_RECORD_H_

#include "flv_mux.h"

int open_flv_file(MEDIA_INFO *info, char *file_name);
int write_flv_video_data(char *data, int len);
int close_flv_file();

#endif