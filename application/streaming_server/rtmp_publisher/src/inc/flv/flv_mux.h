#ifndef FLV_MUX_H_
#define FLV_MUX_H_

#include <stdbool.h>

#include "mpi_dip_sns.h"
#include "mpi_sys.h"
#include "mpi_dev.h"
#include "mpi_enc.h"
#include "mpi_osd.h"
#include "mpi_iva.h"

#define DEBUG
#ifdef DEBUG
#define PRINTF(format, args...) fprintf(stderr, format, ##args)
#else
#define PRINTF(format, args...)
#endif

#define UNKNOWN_CONF (0xff)

#define VIDEO_CODEC_AVC (0x07)

#define VIDEO_KEYFRAME (0x01)
#define VIDEO_INTRAFRAME (0x02)

#define PKT_AVC_SEQ_HEADER (0x00)
#define PKT_AVC_NALU (0x01)
#define PKT_AVC_END (0x02)

#define AVC_PROFILE_BASELINE (0x42)
#define AVC_PROFILE_MAIN (0x4d)
#define AVC_PROFILE_HIGH (0x58)

#define AVC_LEVEL_32 (0x20)
#define AVC_NALU_LEN_SIZE_MINUS_1 (0x03)

#define AUDIO_CODEC_A_LAW (0x07)
#define AUDIO_CODEC_MU_LAW (0x08)
#define AUDIO_CODEC_AAC (0xa0)

#define AUDIO_SND_RATE_5_5K (0x00)
#define AUDIO_SND_RATE_11K (0x01)
#define AUDIO_SND_RATE_22K (0x02)
#define AUDIO_SND_RATE_44K (0x03)

#define AUDIO_SNDSIZE_8BIT (0x00)
#define AUDIO_SNDSIZE_16BIT (0x01)

#define PKT_AAC_SPECIAL_CONF (0x00)
#define PKT_AAC_PAYLOAD (0x01)

#define VIDEO_CHANGE_FORMAT (-2)
#define VIDEO_INVALID_CODEC (-3)

#define TAG_MAX (1000000)
#define SPEC_CONF_TAG_MAX (64)

typedef struct {
	bool hasVideo;
	bool hasAudio;
	char audioCodec;
	char videoCodec;
	char videoProfile;
	int videoLen;
	int videoWidth;
	char audioSampleRate;
	char audioSndSize;
	char audioChannels;
	bool isRecord;
	MPI_ECHN chn_idx;
} MEDIA_INFO;

typedef struct {
	MPI_STREAM_PARAMS_S params;
	unsigned char *p_sei_nalu;
	int sei_nalu_len;
} VIDEO_STREAM_DATA;

int initFlvMuxer(MEDIA_INFO *pMediaInfo, char *pFileName);

int muxVideoSeq(MEDIA_INFO *pMediaInfo, VIDEO_STREAM_DATA *pStreamdata, uint8_t *pVideoTag, int *pTagLen);

int muxVideo(MEDIA_INFO *pMediaInfo, VIDEO_STREAM_DATA *pStreamdata, uint8_t *pVideoTag, int *pTagLen);

int muxAudioAACSpecificConf(uint8_t *pAACSpecificConf, int *pTagLen);

int muxAudioAAC(char *aac_buf, int aac_len, uint8_t *pAudioTag, int *pTagLen);

int uninitFlvMuxer(MEDIA_INFO *pMediaInfo);

#endif