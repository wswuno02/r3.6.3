mod := $(notdir $(subdir))


app-$(CONFIG_APP_LIVE555) += live555
PHONY += live555 live555-clean live555-distclean
PHONY += live555-install live555-uninstall
live555: libado libcm libavftr libeaif libinf libprio
	$(Q)$(MAKE) -C $(LIVE555_PATH) all

live555-clean: libprio-clean libavftr-clean libeaif-clean libcm-clean libado-clean
	$(Q)$(MAKE) -C $(LIVE555_PATH) clean

live555-distclean: libprio-distclean libavftr-distclean \
	libcm-distclean libado-distclean libeaif-distclean
	$(Q)$(MAKE) -C $(LIVE555_PATH) distclean

live555-install: libado-install libcm-install \
	libavftr-install libprio-install libeaif-install
	$(Q)$(MAKE) -C $(LIVE555_PATH) install

live555-uninstall: libprio-uninstall libavftr-uninstall \
	libcm-uninstall libado-uninstall libeaif-uninstall
	$(Q)$(MAKE) -C $(LIVE555_PATH) uninstall

app-$(CONFIG_APP_RTMP_PUBLISHER) += rtmp_publisher
PHONY += rtmp_publisher rtmp_publisher-clean rtmp_publisher-distclean
PHONY += rtmp_publisher-install rtmp_publisher-uninstall
rtmp_publisher: libprio
	$(Q)$(MAKE) -C $(RTMP_PATH)/build all

rtmp_publisher-clean: libprio-clean
	$(Q)$(MAKE) -C $(RTMP_PATH)/build clean

rtmp_publisher-distclean: libprio-distclean
	$(Q)$(MAKE) -C $(RTMP_PATH)/build distclean

rtmp_publisher-install: libprio-install
	$(Q)$(MAKE) -C $(RTMP_PATH)/build install

rtmp_publisher-uninstall: libprio-uninstall
	$(Q)$(MAKE) -C $(RTMP_PATH)/build uninstall

app-$(CONFIG_APP_FLV_SERVER) += flv_server
PHONY += flv_server flv_server-clean flv_server-distclean
PHONY += flv_server-install flv_server-uninstall
flv_server: libprio
	$(Q)$(MAKE) -C $(FLV_PATH)/build all

flv_server-clean: libprio-clean
	$(Q)$(MAKE) -C $(FLV_PATH)/build clean

flv_server-distclean: libprio-distclean
	$(Q)$(MAKE) -C $(FLV_PATH)/build distclean

flv_server-install: libprio-install
	$(Q)$(MAKE) -C $(FLV_PATH)/build install

flv_server-uninstall: libprio-uninstall
	$(Q)$(MAKE) -C $(FLV_PATH)/build uninstall

PHONY += $(mod) $(mod)-clean $(mod)-distclean
PHONY += $(mod)-install $(mod)-uninstall
$(mod): $(app-y)
$(mod)-clean: $(addsuffix -clean,$(app-y))
$(mod)-distclean: $(addsuffix -distclean,$(app-y))
$(mod)-install: $(addsuffix -install,$(app-y))
$(mod)-uninstall: $(addsuffix -uninstall,$(app-y))

APP_BUILD_DEPS += $(mod)
APP_CLEAN_DEPS += $(mod)-clean
APP_DISTCLEAN_DEPS += $(mod)-distclean
APP_INTALL_DEPS += $(mod)-install
APP_UNINTALL_DEPS += $(mod)-uninstall
