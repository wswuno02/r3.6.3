#include "AgtxH265VideoRTPSink.hh"
#include "H265VideoRTPSink.hh"
#include "Base64.hh"
#include "H265VideoRTPSource.hh"

AgtxH265VideoRTPSink *AgtxH265VideoRTPSink::createNew(UsageEnvironment &env, Groupsock *RTPgs,
                                                      unsigned char rtpPayloadFormat, MPI_ECHN chnIdx)
{
	return new AgtxH265VideoRTPSink(env, RTPgs, rtpPayloadFormat, chnIdx);
}

AgtxH265VideoRTPSink::AgtxH265VideoRTPSink(UsageEnvironment &env, Groupsock *RTPgs, unsigned char rtpPayloadFormat,
                                           MPI_ECHN chnIdx)
        : H265VideoRTPSink(env, RTPgs, rtpPayloadFormat)
        , fChnIdx(chnIdx)
{
}

AgtxH265VideoRTPSink::~AgtxH265VideoRTPSink()
{
}

static unsigned const rtpHeaderSize = 12;
void AgtxH265VideoRTPSink::sendPacketIfNecessary()
{
	if (fNumFramesUsedSoFar > 0) {
		if (!fRTPInterface.sendPacket(fOutBuf->packet(), fOutBuf->curPacketSize())) {
			// if failure handler has been specified, call it
			if (fOnSendErrorFunc != NULL)
				(*fOnSendErrorFunc)(fOnSendErrorData);
		}
		++fPacketCount;
		fTotalOctetCount += fOutBuf->curPacketSize();
		fOctetCount +=
		        fOutBuf->curPacketSize() - rtpHeaderSize - fSpecialHeaderSize - fTotalFrameSpecificHeaderSizes;

		++fSeqNo; // for next time
	}

	if (fOutBuf->haveOverflowData() && fOutBuf->totalBytesAvailable() > fOutBuf->totalBufferSize() / 2) {
		// Efficiency hack: Reset the packet start pointer to just in front of
		// the overflow data (allowing for the RTP header and special headers),
		// so that we probably don't have to "memmove()" the overflow data
		// into place when building the next packet:
		unsigned newPacketStart =
		        fOutBuf->curPacketSize() - (rtpHeaderSize + fSpecialHeaderSize + frameSpecificHeaderSize());
		fOutBuf->adjustPacketStart(newPacketStart);
	} else {
		// Normal case: Reset the packet start pointer back to the start:
		fOutBuf->resetPacketStart();
	}
	fOutBuf->resetOffset();
	fNumFramesUsedSoFar = 0;

	if (fNoFramesLeft) {
		// We're done:
		onSourceClosure();
	} else {
		// We have more frames left to send.  Figure out when the next frame
		// is due to start playing, then make sure that we wait this long before
		// sending the next packet.
		if (fOutBuf->haveOverflowData() /*for MJPEG*/ ||
		    (((H264or5Fragmenter *)fOurFragmenter)->fNumValidDataBytes != 1)) {
			MultiFramedRTPSink::sendNext(this);
		} else {
			sendNext(this);
		}
	}
}
