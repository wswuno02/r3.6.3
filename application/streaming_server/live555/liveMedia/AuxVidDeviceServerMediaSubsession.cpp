#include "mpi_index.h"
#include "AuxVidDeviceServerMediaSubsession.hh"
#include "BasicUsageEnvironment.hh"
#include "AuxVidDeviceSource.hh"
#include "DeviceSource.hh"
#include "H264VideoRTPSink.hh"
#include "AgtxH264VideoRTPSink.hh"
#include "H264VideoStreamFramer.hh"
#include "H265VideoRTPSink.hh"
#include "AgtxH265VideoRTPSink.hh"
#include "H265VideoStreamFramer.hh"
#include "CameraJPEGDeviceSource.hh"
#include "JPEGVideoRTPSink.hh"
#include "AgtxJPEGVideoRTPSink.hh"

extern RtspServerConf gServerconf;

DeviceServerMediaSubsession *DeviceServerMediaSubsession::createNew(UsageEnvironment &env, Boolean reuseFirstSource,
                                                                    unsigned int channel)
{
	return new DeviceServerMediaSubsession(env, reuseFirstSource, channel);
}
DeviceServerMediaSubsession::DeviceServerMediaSubsession(UsageEnvironment &env, Boolean reuseFirstSource,
                                                         unsigned int channel)
        : OnDemandServerMediaSubsession(env, reuseFirstSource)
        , fAuxSDPLine(NULL)
        , fDoneFlag(0)
        , fDummyRTPSink(NULL)
{
	fchannel = channel;
	if (MPI_SYS_init() != MPI_SUCCESS) {
		fprintf(stderr, "Failed to MPI_SYS_init!");
	}
}

DeviceServerMediaSubsession::~DeviceServerMediaSubsession()
{
	delete[] fAuxSDPLine;
	MPI_SYS_exit();
}

FramedSource *DeviceServerMediaSubsession::createNewStreamSource(unsigned clientSessionId, unsigned &estBitrate)
{
	int ret = 0;
	MPI_ECHN chn_idx;
	chn_idx.chn = fchannel;
	MPI_VENC_ATTR_S p_venc_attr;
	ret = MPI_ENC_getVencAttr(chn_idx, &p_venc_attr);
	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "Failed to MPI_ENC_getVencAttr %d.\n", chn_idx.chn);
		return NULL;
	}

	if (p_venc_attr.type == MPI_VENC_TYPE_H265) {
		DeviceSource *source = AuxVidDeviceSource::createNew(envir(), fchannel);
		return H265VideoStreamFramer::createNew(envir(), source);
	} else if (p_venc_attr.type == MPI_VENC_TYPE_H264) {
		DeviceSource *source = AuxVidDeviceSource::createNew(envir(), fchannel);
		return H264VideoStreamFramer::createNew(envir(), source);
	} else if (p_venc_attr.type == MPI_VENC_TYPE_MJPEG) {
		return CameraJPEGDeviceSource::createNew(envir(), clientSessionId, fchannel);
	}

	fprintf(stderr, "%s : unknown encode type %d\n", __func__, p_venc_attr.type);
	return NULL;
}

RTPSink *DeviceServerMediaSubsession::createNewRTPSink(Groupsock *rtpGroupsock, unsigned char rtpPayloadTypeIfDynamic,
                                                       FramedSource * /*inputSource*/)
{
	int ret = 0;
	MPI_ECHN chn_idx;
	chn_idx.chn = fchannel;
	MPI_VENC_ATTR_S p_venc_attr;
	ret = MPI_ENC_getVencAttr(chn_idx, &p_venc_attr);
	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "Failed to MPI_ENC_getVencAttr %d.\n", chn_idx.chn);
		return NULL;
	}

	if (p_venc_attr.type == MPI_VENC_TYPE_H265) {
		if (gServerconf.launchMode == DftMode) {
			return H265VideoRTPSink::createNew(envir(), rtpGroupsock, rtpPayloadTypeIfDynamic);
		} else {
			return AgtxH265VideoRTPSink::createNew(envir(), rtpGroupsock, rtpPayloadTypeIfDynamic, chn_idx);
		}
	} else if (p_venc_attr.type == MPI_VENC_TYPE_H264) {
		if (gServerconf.launchMode == DftMode) {
			return H264VideoRTPSink::createNew(envir(), rtpGroupsock, rtpPayloadTypeIfDynamic);
		} else {
			return AgtxH264VideoRTPSink::createNew(envir(), rtpGroupsock, rtpPayloadTypeIfDynamic, chn_idx);
		}
	} else if (p_venc_attr.type == MPI_VENC_TYPE_MJPEG) {
		if (gServerconf.launchMode == DftMode) {
			return JPEGVideoRTPSink::createNew(envir(), rtpGroupsock);
		} else {
			return AgtxJPEGVideoRTPSink::createNew(envir(), rtpGroupsock, chn_idx);
		}
	}

	fprintf(stderr, "%s : unknown encode type %d\n", __func__, p_venc_attr.type);
	return NULL;
}

static void afterPlayingDummy(void *clientData)
{
	DeviceServerMediaSubsession *subsess = (DeviceServerMediaSubsession *)clientData;
	subsess->afterPlayingDummy1();
}

void DeviceServerMediaSubsession::afterPlayingDummy1()
{
	// Unschedule any pending 'checking' task:
	envir().taskScheduler().unscheduleDelayedTask(nextTask());
	// Signal the event loop that we're done:
	setDoneFlag();
}

static void checkForAuxSDPLine(void *clientData)
{
	DeviceServerMediaSubsession *subsess = (DeviceServerMediaSubsession *)clientData;
	subsess->checkForAuxSDPLine1();
}

void DeviceServerMediaSubsession::checkForAuxSDPLine1()
{
	char const *dasl;

	if (fAuxSDPLine != NULL) {
		// Signal the event loop that we're done:
		setDoneFlag();
	} else if (fDummyRTPSink != NULL && (dasl = fDummyRTPSink->auxSDPLine()) != NULL) {
		fAuxSDPLine = strDup(dasl);
		fDummyRTPSink = NULL;

		// Signal the event loop that we're done:
		setDoneFlag();
	} else if (!fDoneFlag) {
		// try again after a brief delay:
		int uSecsToDelay = 100000; // 100 ms
		nextTask() =
		        envir().taskScheduler().scheduleDelayedTask(uSecsToDelay, (TaskFunc *)checkForAuxSDPLine, this);
	}
}
char const *DeviceServerMediaSubsession::getAuxSDPLine(RTPSink *rtpSink, FramedSource *inputSource)
{
	int ret;
	MPI_ECHN chn_idx;
	chn_idx.chn = fchannel;
	MPI_VENC_ATTR_S p_venc_attr;
	ret = MPI_ENC_getVencAttr(chn_idx, &p_venc_attr);
	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "Failed to MPI_ENC_getVencAttr %d.\n", chn_idx.chn);
	}

	if (p_venc_attr.type == MPI_VENC_TYPE_MJPEG) {
		fDummyRTPSink = rtpSink;
		return fDummyRTPSink->auxSDPLine();
	} else {
		if (fAuxSDPLine != NULL)
			return fAuxSDPLine; // it's already been set up (for a previous client)

		if (fDummyRTPSink == NULL) { // we're not already setting it up for another, concurrent stream
			// Note: For H264 video files, the 'config' information ("profile-level-id"
			// and "sprop-parameter-sets") isn't known
			// until we start reading the file.  This means that "rtpSink"s
			// "auxSDPLine()" will be NULL initially,
			// and we need to start reading data from our file until this changes.
			fDummyRTPSink = rtpSink;

			// Start reading the file:
			fDummyRTPSink->startPlaying(*inputSource, afterPlayingDummy, this);

			// Check whether the sink's 'auxSDPLine()' is ready:
			checkForAuxSDPLine(this);
		}

		envir().taskScheduler().doEventLoop(&fDoneFlag);

		return fAuxSDPLine;
	}
}
