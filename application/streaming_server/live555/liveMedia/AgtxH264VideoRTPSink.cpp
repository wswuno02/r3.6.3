#include "AgtxH264VideoRTPSink.hh"
#include "H264VideoRTPSink.hh"
#include "Base64.hh"
#include "H264VideoRTPSource.hh"

//#define DEBUG
#ifdef DEBUG
#define PRINTF_TRACE(format, args...) fprintf(stderr, "[%s:%d] " format, __FILE__, __LINE__, ##args)
#else
#define PRINTF_TRACE(format, args...)
#endif

AgtxH264VideoRTPSink *AgtxH264VideoRTPSink::createNew(UsageEnvironment &env, Groupsock *RTPgs,
                                                      unsigned char rtpPayloadFormat, MPI_ECHN chnIdx)
{
	return new AgtxH264VideoRTPSink(env, RTPgs, rtpPayloadFormat, chnIdx);
}

AgtxH264VideoRTPSink::AgtxH264VideoRTPSink(UsageEnvironment &env, Groupsock *RTPgs, unsigned char rtpPayloadFormat,
                                           MPI_ECHN chnIdx)
        : H264VideoRTPSink(env, RTPgs, rtpPayloadFormat)
        , fChnIdx(chnIdx)
{
	gettimeofdayMonotonic(&fPrevoiusSendTime, NULL);
	int ret;
	MPI_CHN_ATTR_S chnAttr;
	ret = MPI_DEV_getChnAttr(MPI_VIDEO_CHN(0, fChnIdx.chn), &chnAttr);
	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "failed to MPI_DEV_getDevAttr\r\n");
		onSourceClosure();
	}
	fRescheduleInterval = (1000000 / (chnAttr.fps)); /*us*/
	fFps = chnAttr.fps;
}

AgtxH264VideoRTPSink::~AgtxH264VideoRTPSink()
{
}

static unsigned const rtpHeaderSize = 12;
void AgtxH264VideoRTPSink::sendPacketIfNecessary()
{
	if (fNumFramesUsedSoFar > 0) {
		if (!fRTPInterface.sendPacket(fOutBuf->packet(), fOutBuf->curPacketSize())) {
			// if failure handler has been specified, call it
			if (fOnSendErrorFunc != NULL)
				(*fOnSendErrorFunc)(fOnSendErrorData);
		}
		++fPacketCount;
		fTotalOctetCount += fOutBuf->curPacketSize();
		fOctetCount +=
		        fOutBuf->curPacketSize() - rtpHeaderSize - fSpecialHeaderSize - fTotalFrameSpecificHeaderSizes;

		++fSeqNo; // for next time
	}

	if (fOutBuf->haveOverflowData() && fOutBuf->totalBytesAvailable() > fOutBuf->totalBufferSize() / 2) {
		// Efficiency hack: Reset the packet start pointer to just in front of
		// the overflow data (allowing for the RTP header and special headers),
		// so that we probably don't have to "memmove()" the overflow data
		// into place when building the next packet:
		unsigned newPacketStart =
		        fOutBuf->curPacketSize() - (rtpHeaderSize + fSpecialHeaderSize + frameSpecificHeaderSize());
		fOutBuf->adjustPacketStart(newPacketStart);
	} else {
		// Normal case: Reset the packet start pointer back to the start:
		fOutBuf->resetPacketStart();
	}
	fOutBuf->resetOffset();
	fNumFramesUsedSoFar = 0;

	if (fNoFramesLeft) {
		// We're done:
		onSourceClosure();
	} else {
		// We have more frames left to send.  Figure out when the next frame
		// is due to start playing, then make sure that we wait this long before
		// sending the next packet.
		if (fOutBuf->haveOverflowData() /*data > RTP max*/ ||
		    (((H264or5Fragmenter *)fOurFragmenter)->fNumValidDataBytes != 1)) {
			sendNext(this);
		} else {
			sendNext(this);
		}


	}
}
