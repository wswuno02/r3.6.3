#include "AgtxPCMAudioRTPSink.hh"
#include "SimpleRTPSink.hh"
#include "Base64.hh"

#ifdef DEBUG
#define PRINTF_TRACE(format, args...) fprintf(stderr, "[%s:%d] " format, __FILE__, __LINE__, ##args)
#else
#define PRINTF_TRACE(format, args...)
#endif


AgtxPCMAudioRTPSink *AgtxPCMAudioRTPSink::createNew(UsageEnvironment &env, Groupsock *RTPgs, unsigned char rtpPayloadFormat,
								unsigned rtpTimestampFrequency, char const *sdpMediaTypeString,
								char const *rtpPayloadFormatName, unsigned numChannels, 
								int frameSize)
{
	return new AgtxPCMAudioRTPSink(env, RTPgs, rtpPayloadFormat, rtpTimestampFrequency, sdpMediaTypeString,
	                         rtpPayloadFormatName, numChannels, frameSize);
}

AgtxPCMAudioRTPSink::AgtxPCMAudioRTPSink(UsageEnvironment &env, Groupsock *RTPgs, unsigned char rtpPayloadFormat,
								unsigned rtpTimestampFrequency, char const *sdpMediaTypeString,
								char const *rtpPayloadFormatName, unsigned numChannels,
								int frameSize)
							: SimpleRTPSink(env, RTPgs, rtpPayloadFormat, rtpTimestampFrequency, sdpMediaTypeString,
	                         rtpPayloadFormatName, numChannels, true, true)
{
	gettimeofdayMonotonic(&fPrevoiusSendTime, NULL);
	fRegularSleep = (1024 * 1000000/ frameSize) - 1000;

	PRINTF_TRACE("sleep %lu, size :%d\r\n", fRegularSleep, frameSize);
}

int AgtxPCMAudioRTPSink::getSleepinterval(long *sleepInterval)
{
	int ret;

	struct timeval nowTime;
	gettimeofdayMonotonic(&nowTime, NULL);

	int secsDiff = nowTime.tv_sec - fPrevoiusSendTime.tv_sec;
	long uSecondsToGo = secsDiff * 1000000 + (nowTime.tv_usec - fPrevoiusSendTime.tv_usec);
	PRINTF_TRACE("%ld.%ld %ld.%ld\r\n", nowTime.tv_sec, nowTime.tv_usec, fPrevoiusSendTime.tv_sec,
	             fPrevoiusSendTime.tv_usec);
	//PRINTF_TRACE("sec diff %ld, us diff:%ld\r\n", secsDiff, uSecondsToGo);
	long diff = 0;
	if (uSecondsToGo < fRegularSleep) {
		diff = fRegularSleep - uSecondsToGo;
		*sleepInterval = diff;
	} 

	*sleepInterval = diff;

	gettimeofdayMonotonic(&fPrevoiusSendTime, NULL);
	return 0;
}

static unsigned const rtpHeaderSize = 12;
void AgtxPCMAudioRTPSink::sendPacketIfNecessary()
{
	if (fNumFramesUsedSoFar > 0) {
// Send the packet:
#ifdef TEST_LOSS
		if ((our_random() % 10) != 0) // simulate 10% packet loss #####
#endif
			if (!fRTPInterface.sendPacket(fOutBuf->packet(), fOutBuf->curPacketSize())) {
				// if failure handler has been specified, call it
				if (fOnSendErrorFunc != NULL)
					(*fOnSendErrorFunc)(fOnSendErrorData);
			}
		++fPacketCount;
		fTotalOctetCount += fOutBuf->curPacketSize();
		fOctetCount +=
		        fOutBuf->curPacketSize() - rtpHeaderSize - fSpecialHeaderSize - fTotalFrameSpecificHeaderSizes;

		++fSeqNo; // for next time
	}

	if (fOutBuf->haveOverflowData() && fOutBuf->totalBytesAvailable() > fOutBuf->totalBufferSize() / 2) {
		// Efficiency hack: Reset the packet start pointer to just in front of
		// the overflow data (allowing for the RTP header and special headers),
		// so that we probably don't have to "memmove()" the overflow data
		// into place when building the next packet:
		unsigned newPacketStart =
		        fOutBuf->curPacketSize() - (rtpHeaderSize + fSpecialHeaderSize + frameSpecificHeaderSize());
		fOutBuf->adjustPacketStart(newPacketStart);
	} else {
		// Normal case: Reset the packet start pointer back to the start:
		fOutBuf->resetPacketStart();
	}
	fOutBuf->resetOffset();
	fNumFramesUsedSoFar = 0;

	if (fNoFramesLeft) {
		// We're done:
		onSourceClosure();
	} else {
		// We have more frames left to send.  Figure out when the next frame
		// is due to start playing, then make sure that we wait this long before
		// sending the next packet.
		if (fOutBuf->haveOverflowData() /*only MJPEG use here*/) {
			MultiFramedRTPSink::sendNext(this);
		} else {
			nextTask() = envir().taskScheduler().scheduleDelayedTask(12800, (TaskFunc *)sendNext, this);
		}
	}
}

