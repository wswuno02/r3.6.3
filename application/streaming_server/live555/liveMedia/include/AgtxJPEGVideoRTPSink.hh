#ifndef _AGTX_JPEG_VIDEO_RTP_SINK_HH
#define _AGTX_JPEG_VIDEO_RTP_SINK_HH

#include "mpi_dev.h"
#include "JPEGVideoRTPSink.hh"
#ifndef _VIDEO_RTP_SINK_HH
#include "VideoRTPSink.hh"
#endif

class AgtxJPEGVideoRTPSink : public JPEGVideoRTPSink {
    public:
	static AgtxJPEGVideoRTPSink *createNew(UsageEnvironment &env, Groupsock *RTPgs, MPI_ECHN chnIdx);

    protected:
	AgtxJPEGVideoRTPSink(UsageEnvironment &env, Groupsock *RTPgs, MPI_ECHN chnIdx);
	// called only by createNew()

	virtual ~AgtxJPEGVideoRTPSink();
	virtual void sendPacketIfNecessary();

    private:
	MPI_ECHN fChnIdx;
};

#endif