#ifndef _AGTX_PCM_AUDIO_RTP_SINK_HH
#define _AGTX_PCM_AUDIO_RTP_SINK_HH

#include "SimpleRTPSink.hh"
#include "GroupsockHelper.hh"
#ifndef _MULTI_FRAMED_RTP_SINK_HH
#include "MultiFramedRTPSink.hh"
#endif

class AgtxPCMAudioRTPSink : public SimpleRTPSink {
    public:
	static AgtxPCMAudioRTPSink *createNew(UsageEnvironment &env, Groupsock *RTPgs, unsigned char rtpPayloadFormat,
								unsigned rtpTimestampFrequency, char const *sdpMediaTypeString,
								char const *rtpPayloadFormatName, unsigned numChannels, 
								int frameSize);

    protected:
	AgtxPCMAudioRTPSink(UsageEnvironment &env, Groupsock *RTPgs, unsigned char rtpPayloadFormat,
								unsigned rtpTimestampFrequency, char const *sdpMediaTypeString,
								char const *rtpPayloadFormatName, unsigned numChannels,
								int frameSize);
	// called only by createNew()
	virtual ~AgtxPCMAudioRTPSink() {};
	virtual void sendPacketIfNecessary();
	int getSleepinterval(long *sleepInterval);

    private:
	struct timeval fPrevoiusSendTime;
	long fRegularSleep;

};

#endif
