#ifndef _AGTX_H265_VIDEO_RTP_SINK_HH
#define _AGTX_H265_VIDEO_RTP_SINK_HH

#include "mpi_dev.h"
#include "H265VideoRTPSink.hh"
#ifndef _H264_OR_5_VIDEO_RTP_SINK_HH
#include "H264or5VideoRTPSink.hh"
#endif

class AgtxH265VideoRTPSink : public H265VideoRTPSink {
    public:
	static AgtxH265VideoRTPSink *createNew(UsageEnvironment &env, Groupsock *RTPgs, unsigned char rtpPayloadFormat,
	                                       MPI_ECHN chnIdx);

    protected:
	AgtxH265VideoRTPSink(UsageEnvironment &env, Groupsock *RTPgs, unsigned char rtpPayloadFormat, MPI_ECHN chnIdx);
	virtual ~AgtxH265VideoRTPSink();
	virtual void sendPacketIfNecessary();

    private:
	MPI_ECHN fChnIdx;
};

#endif