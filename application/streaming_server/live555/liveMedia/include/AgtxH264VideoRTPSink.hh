#ifndef _AGTX_H264_VIDEO_RTP_SINK_HH
#define _AGTX_H264_VIDEO_RTP_SINK_HH

#include "mpi_dev.h"
#include "H264VideoRTPSink.hh"
#include "GroupsockHelper.hh"
#ifndef _H264_OR_5_VIDEO_RTP_SINK_HH
#include "H264or5VideoRTPSink.hh"
#endif

class AgtxH264VideoRTPSink : public H264VideoRTPSink {
    public:
	static AgtxH264VideoRTPSink *createNew(UsageEnvironment &env, Groupsock *RTPgs, unsigned char rtpPayloadFormat,
	                                       MPI_ECHN chnIdx);

    protected:
	AgtxH264VideoRTPSink(UsageEnvironment &env, Groupsock *RTPgs, unsigned char rtpPayloadFormat, MPI_ECHN chnIdx);
	virtual ~AgtxH264VideoRTPSink();
	virtual void sendPacketIfNecessary();
    private:
	MPI_ECHN fChnIdx;
	struct timeval fPrevoiusSendTime;
	long fRescheduleInterval;
	float fFps;

};

#endif