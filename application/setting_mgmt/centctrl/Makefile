SDKSRC_DIR ?= $(realpath $(CURDIR)/../../..)
include $(SDKSRC_DIR)/application/internal.mk

CC=$(CROSS_COMPILE)gcc
AR=$(CROSS_COMPILE)ar

TRGTS = ccserver ccclient
LIBTRGT = libccdata.a
DEST := $(CUSTOMFS)/bin
INSTALL_TRGTS := $(addprefix $(DEST)/, $(TRGTS))

INC    := -I$(JSON_INC) -I$(LIBSQL_INC) -I$(LIBCM_INC) -I$(APP_INC) -I$(CENTCTRL_PATH)/include
CFLAGS := -Wall -g -O2 ${PERF_NO_OMIT_FP} -std=gnu99 -MMD

LDFLAGS := -L$(ZLIB_LIB) -L$(SQLITE3_LIB) -L$(JSON_LIB) -L$(APP_LIB)
LDFLAGS += -L$(LIBSQL_PATH) -L$(LIBCM_PATH)
LDFLAGS += -pthread -lrt -lcm -ljson-c -lsql -lz -lsqlite3

SRCS := $(filter-out cli.c, $(wildcard *.c))
OBJS := $(patsubst %.c, %.o, $(SRCS))

.PHONY: default
default:all

.PHONY: all
all: $(TRGTS) $(LIBTRGT)

ccserver: $(OBJS)
	$(Q)$(CC) $(CFLAGS) -o $@ $(OBJS) $(INC) $(LDFLAGS)

ccclient: cli.o cc_data.o
	$(Q)$(CC) $^ $(LDFLAGS) $(CFLAGS) -o $@

$(LIBTRGT): cc_data.o
	$(Q)$(AR) rcs $@ $^

%.o: %.c
	$(Q)$(CC) $< -c $(CFLAGS) -o $@ $(INC)

.PHONY: install
install: $(DEST) $(INSTALL_TRGTS)

$(DEST):
	$(Q)install -d $@

$(DEST)/%: %
	$(Q)install -m 777 $< $@

.PHONY: uninstall
uninstall:
	$(Q)rm -f $(INSTALL_TRGTS)

.PHONY: clean
clean:
	$(Q)find . -type f -name "*.[doa]" -exec rm -f {} \;
	$(Q)rm -f $(TRGTS)

.PHONY: distclean
distclean: uninstall clean
