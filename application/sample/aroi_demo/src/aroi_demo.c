#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include "mpi_base_types.h"
#include "mpi_dev.h"
#include "mpi_iva.h"
#include "aroi_demo.h"
#include "vftr_aroi.h"
#include "video_ptz.h"

#include "avftr_conn.h"

#define _DEBUG

#ifdef _DEBUG
#define DBG(...) printf(__VA_ARGS__)
#else
#define DBG(...)
#endif

#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define MIN(a, b) (((a) < (b)) ? (a) : (b))

extern int g_aroi_running;
extern int g_detect_interval;

#ifdef CONFIG_APP_AROI_SUPPORT_SEI
extern AVFTR_CTX_S *av_ftr_res_shm;
extern AVFTR_VIDEO_CTX_S *vftr_res_shm;
extern AVFTR_AUDIO_CTX_S *aftr_res_shm;

static int updateBufferIdx(AVFTR_VIDEO_BUF_INFO_S *buf_info, UINT32 timestamp)
{
	buf_info->buf_cur_idx = ((buf_info->buf_cur_idx + 1) % AVFTR_VIDEO_RING_BUF_SIZE);
	buf_info->buf_ready[buf_info->buf_cur_idx] = 0;
	buf_info->buf_time[buf_info->buf_cur_idx] = timestamp;
	buf_info->buf_cur_time = timestamp;
	return buf_info->buf_cur_idx;
}

/**
 * @brief fill object context for display
 */
int getObjList(MPI_WIN idx, UINT32 timestamp, VIDEO_FTR_OBJ_LIST_S *obj_list)
{
	VIDEO_OD_CTX_S *vftr_od_ctx = vftr_res_shm->od_ctx;
	vftr_od_ctx[0].en = 1;

	if (vftr_od_ctx[0].en) {
		int ret = 0;
		MPI_RECT_POINT_S *bd = &vftr_od_ctx[0].bdry;
		MPI_RECT_POINT_S *obj;
		MPI_RECT_POINT_S *final_obj;
		int obj_cnt = 0;

		ret = MPI_IVA_getBitStreamObjList(idx, timestamp, &obj_list->basic_list);
		if (ret != MPI_SUCCESS) {
			fprintf(stderr, "MPI_IVA_getBitStreamObjList ret = %d\n", ret);
			goto err;
		}

		for (int i = 0; i < obj_list->basic_list.obj_num; i++) {
			/* Initialize object attr */
			obj_list->obj_attr[obj_cnt].cat[0] = '\0';
			obj_list->obj_attr[obj_cnt].shaking = 0;

			/* Limit OL boundary */
			obj = &obj_list->basic_list.obj[i].rect;
			final_obj = &obj_list->basic_list.obj[obj_cnt].rect;

			/* NOTICE: the following code remove the object that out of boundary
			 *         or crop the object to fit the output image */
			if (bd->sx != -1) {
				if (obj->ex < bd->sx) {
					continue;
				}
				final_obj->sx = obj->sx > bd->sx ? obj->sx : bd->sx;
			} else {
				final_obj->sx = obj->sx;
			}

			if (bd->sy != -1) {
				if (obj->ey < bd->sy) {
					continue;
				}
				final_obj->sy = obj->sy > bd->sy ? obj->sy : bd->sy;
			} else {
				final_obj->sy = obj->sy;
			}

			if (bd->ex != -1) {
				if (obj->sx > bd->ex) {
					continue;
				}
				final_obj->ex = obj->ex > bd->ex ? bd->ex : obj->ex;
			} else {
				final_obj->ex = obj->ex;
			}

			if (bd->ey != -1) {
				if (obj->sy > bd->ey) {
					continue;
				}
				final_obj->ey = obj->ey > bd->ey ? bd->ey : obj->ey;
			} else {
				final_obj->ey = obj->ey;
			}

			obj_list->basic_list.obj[obj_cnt].id = obj_list->basic_list.obj[i].id;
			obj_list->basic_list.obj[obj_cnt].life = obj_list->basic_list.obj[i].life;
			obj_list->basic_list.obj[obj_cnt].mv = obj_list->basic_list.obj[i].mv;

			/* increase the index */
			obj_cnt++;
		}

		obj_list->basic_list.obj_num = obj_cnt;
	}

	return MPI_SUCCESS;
err:
	return MPI_FAILURE;
}

#ifdef CONFIG_APP_PTZ_SUPPORT
static void ptz_roiConvertBoundary(MPI_RECT_POINT_S *roi, MPI_RECT_S *roi_corrd)
{
	roi_corrd->x = roi->sx;
	roi_corrd->y = roi->sy;
	roi_corrd->width = abs(roi->ex - roi->sx);
	roi_corrd->height = abs(roi->ey - roi->sy);
}
#endif /* CONFIG_APP_PTZ_SUPPORT */
#endif

int detectAroi(MPI_WIN win_idx, MPI_SIZE_S *res, VFTR_AROI_PARAM_S *aroi_attr, VIDEO_FTR_PTZ_PARAM_S *ptz_param)
{
	UINT32 timestamp = 0;
	INT32 timeout = 0;
	INT32 ret = 0;
	int frame_interval_cnt = 0;
	MPI_WIN wait_win = MPI_VIDEO_WIN(win_idx.dev, win_idx.chn, 0);
	VFTR_AROI_INSTANCE_S *aroi;
	MPI_IVA_OBJ_LIST_S *obj_list;

	/* bd = boundary, set boundary to filter object list */
#ifdef CONFIG_APP_AROI_SUPPORT_SEI
	/* get index 0 video context */
	VIDEO_OD_CTX_S *od_ctx = &vftr_res_shm->od_ctx[0];
	MPI_RECT_POINT_S *bd = &od_ctx->bdry;

	/* OD information */
	od_ctx->en = 1;
	od_ctx->en_shake_det = 0;
	od_ctx->en_crop_outside_obj = 0;
	od_ctx->idx = win_idx;
	od_ctx->cb = NULL;
	bd->sx = 0;
	bd->sy = 0;
	bd->ex = res->width - 1;
	bd->ey = res->height - 1;

	/* AROI information */
	AVFTR_AROI_CTX_S *aroi_ctx = &vftr_res_shm->aroi_ctx[0];
	aroi_ctx->en = 1;
	aroi_ctx->reg = 1;
	aroi_ctx->idx = win_idx;
	aroi_ctx->cb = NULL;
#endif

	/* create AROI instance */
	aroi = VFTR_AROI_newInstance();
	if (!aroi) {
		fprintf(stderr, "Failed to create AROI instance\n");
		return -EINVAL;
	}

	/* set parameter to aroi */
	ret = VFTR_AROI_setParam(aroi, res, aroi_attr);
	if (ret) {
		fprintf(stderr, "Failed to set AROI parameters.\n");
		return -EINVAL;
	}

	while (g_aroi_running) {
		/* wait one video frame processed */
		if (MPI_DEV_waitWin(wait_win, &timestamp, timeout) != MPI_SUCCESS) {
			fprintf(stderr, "Wait ISP statistics fail for win:0x%x\n", wait_win.value);
			continue;
		}

		/* do motion detection once every g_interval */
		if (frame_interval_cnt < g_detect_interval) {
			++frame_interval_cnt;
			continue;
		} else {
			frame_interval_cnt = 0;
		}

#ifdef CONFIG_APP_AROI_SUPPORT_SEI
		/* get buffer index, fill the result to shared memory
		 * it will be read and embedded as SEI data through RTSP streaming
		 * */
		AVFTR_VIDEO_BUF_INFO_S *buf_info = &vftr_res_shm->buf_info[0];
		int buf_idx = updateBufferIdx(buf_info, timestamp);

		VIDEO_FTR_OBJ_LIST_S *vftr_obj_list = &od_ctx->ol[buf_idx];

		/**
		 * get object list from MPI
		 */
		ret = getObjList(win_idx, timestamp, vftr_obj_list);
		if (ret != MPI_SUCCESS) {
			fprintf(stderr, "Failed to getBitStreamObjList.[LINE]: %d\n", __LINE__);
			continue;
		}

		obj_list = &vftr_obj_list->basic_list;
#else
		MPI_IVA_OBJ_LIST_S obj_instance;

		ret = MPI_IVA_getBitStreamObjList(win_idx, timestamp, &obj_instance);
		if (ret != MPI_SUCCESS) {
			fprintf(stderr, "MPI_IVA_getBitStreamObjList ret = %d\n", ret);
			continue;
		}

		obj_list = &obj_instance;
		/* DBG("still alive %u\n", timestamp); */
#endif /* CONFIG_APP_AROI_SUPPORT_SEI */

		VFTR_AROI_STATUS_S *aroi_res;
#ifdef CONFIG_APP_AROI_SUPPORT_SEI
		aroi_res = &aroi_ctx->aroi_res[buf_idx];
#else
		VFTR_AROI_STATUS_S status;
		aroi_res = &status;
#endif /* CONFIG_APP_AROI_SUPPORT_SEI */

		/**
		 * detect aroi detection
		 */
		ret = VFTR_AROI_detectRoi(aroi, obj_list, aroi_res);
		if (ret != MPI_SUCCESS) {
			fprintf(stderr, "Failed to AROI_detectMotion.[LINE]: %d\n", __LINE__);
			continue;
		}

#ifdef CONFIG_APP_PTZ_SUPPORT
		MPI_WIN apply_idx = MPI_VIDEO_WIN(0, ptz_param->win_id->chn, ptz_param->win_id->win);
		const MPI_RECT_POINT_S p = aroi_res->roi;
		MPI_RECT_POINT_S roi_tmp = { 0 };
		MPI_RECT_S roi_coord = { 0 };
		MPI_SIZE_S res = { .width = 1920, .height = 1080 };

#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
		roi_tmp.sx = MAX(p.sx * 1024 / MAX(res.width - 1, 1), 0);
		roi_tmp.sy = MAX(p.sy * 1024 / MAX(res.height - 1, 1), 0);
		roi_tmp.ex = MIN(MAX(p.ex * 1024 / MAX(res.width - 1, 1), 0), 1024);
		roi_tmp.ey = MIN(MAX(p.ey * 1024 / MAX(res.height - 1, 1), 0), 1024);

		ptz_roiConvertBoundary(&roi_tmp, &roi_coord);

		for (int i = 0; i < ptz_param->win_num; i++) {
			ret = MPI_DEV_setWindowRoi(apply_idx, &roi_coord);
			if (ret != MPI_SUCCESS) {
				fprintf(stderr, "Failed to set AROI_Window.[LINE]: %d\n", __LINE__);
				return -1;
			}
		}
#endif /* CONFIG_APP_PTZ_SUPPORT */

		/* 
		 * write result to shared memory so that RTSP server can read
		 * */
		/* mark buffer ready for display */
#ifdef CONFIG_APP_AROI_SUPPORT_SEI
		buf_info->buf_ready[buf_idx] = 1;
#endif
	}

	/* delete AROI instance */
	VFTR_AROI_deleteInstance(&aroi);

	return 0;
}
