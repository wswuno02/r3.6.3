#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <getopt.h>
#include <time.h>
#include <json.h>
#include "error.h"

#include "mpi_index.h"
#include "mpi_dip_types.h"
#include "mpi_dip_sns.h"
#include "mpi_sys.h"
#include "mpi_osd.h"
#include "mpi_enc.h"
#include "mpi_dev.h"
#include "mpi_index.h"

#include "libosd.h"

#define DEBUG
#ifdef DEBUG
#define LOG(format, args...) fprintf(stderr, "[%s:%d]" format, __func__, __LINE__, ##args);
#else
#define LOG(format, args...)
#endif
#define ERR(format, args...) fprintf(stderr, "[%s:%d]" format, __func__, __LINE__, ##args);

extern void logAllOsdHandle(OsdHandle *phd);
extern int calcNewLine(int startx, int starty, int endx, int endy, int thickness, OsdRegion *targetReg,
                       OsdLine *output);
extern int saveAYUV(char *filePath, uint32_t w, uint32_t h, char *payuvBuf, int bufSize);
extern int alignUVVal(char *ayuv_buf, int width, int height);

OsdHandle *g_osd_handle[3];
OSD_HANDLE g_osd_chn_handle[3][MAX_CANVAS];
MPI_OSD_CANVAS_ATTR_S g_osd_canvas_attr[3][MAX_CANVAS];

int gRunflag = 0;

uint16_t unicode_by_day[7][3] = { { 0x661f, 0x671f, 0x5929 }, { 0x661f, 0x671f, 0x4e00 }, { 0x661f, 0x671f, 0x4e8c },
	                          { 0x661f, 0x671f, 0x4e09 }, { 0x661f, 0x671f, 0x56db }, { 0x661f, 0x671f, 0x4e94 },
	                          { 0x661f, 0x671f, 0x516d } };

uint16_t g_text_unicode_array[22] = { 0x0030, 0x0031, 0x0032,       0x0033,       0x0034,       0x0035, 0x0036, 0x0037,
	                              0x0038, 0x0039, 0x003a /*:*/, 0x002d /*-*/, 0x0020 /* */, 0x661f, 0x671f, 0x5929,
	                              0x4e00, 0x4e8c, 0x4e09,       0x56db,       0x4e94,       0x516d };

typedef enum { NONE, UTF8, UNICODE } TIMESTAMP_TYPE;
typedef struct {
	int region_num[3];
	int region_list[3][MAX_OSD];
	TIMESTAMP_TYPE region_type[3][MAX_OSD];
	OsdText region_txt[3][MAX_OSD];
	char *ayuv_src_list[3][MAX_OSD];
} DynamicTimestampRegions;

#define MAX_STEP (16)
typedef struct {
	int step;
	int max_step;
	OsdPoint point[MAX_STEP];
	int interval;
} MoveRegion;

typedef struct {
	int region_num[3];
	int region_list[3][MAX_CANVAS];
	MoveRegion move_obj[3][MAX_CANVAS];
} DynamicPositionRegions;

DynamicTimestampRegions g_timestamp_regions;
DynamicPositionRegions g_moving_regions;

INT32 createOsdInstance(const MPI_OSD_RGN_ATTR_S *attr, const MPI_OSD_BIND_ATTR_S *bind, OSD_HANDLE *handle,
                        MPI_OSD_CANVAS_ATTR_S *canvas)
{
	INT32 ret = MPI_SUCCESS;

	ret = MPI_createOsdRgn(handle, attr);
	if (ret != MPI_SUCCESS) {
		ERR("MPI_createOsdRgn() failed. err: %d\n", ret);
		return ret;
	}

	ret = MPI_getOsdCanvas(*handle, canvas);
	if (ret != MPI_SUCCESS) {
		ERR("MPI_getOsdCanvas() failed. err: %d\n", ret);
		goto release;
	}

	ret = MPI_bindOsdToChn(*handle, bind);
	if (ret != MPI_SUCCESS) {
		ERR("Bind OSD %d to encoder channel %d failed. err: %d\n", *handle, bind->idx.chn, ret);
		goto release;
	}

	return ret;

release:

	MPI_destroyOsdRgn(*handle);

	return ret;
}

/*this can only change start x,y, not width and height*/
INT32 moveOsdInstance(MPI_OSD_BIND_ATTR_S *bind, OSD_HANDLE *handle)
{
	INT32 ret = MPI_SUCCESS;
	MPI_OSD_BIND_ATTR_S bind_old;
	ret = MPI_getOsdBindAttr(*handle, &bind_old);
	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "Failed to get MPI_getOsdBindAttr\r\n");
	}
	bind_old.point.x = bind->point.x;
	bind_old.point.y = bind->point.y;
	ret = MPI_setOsdBindAttr(*handle, &bind_old);
	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "Set OSD %d to encoder channel %d failed. err: %d\n", *handle, bind->idx.chn, ret);
	}

	return ret;
}

int stopEnc(int channel)
{
	MPI_ECHN echn_idx;
	int ret;

	echn_idx = MPI_ENC_CHN(0);
	echn_idx.chn = channel;

	ret = MPI_ENC_stopChn(echn_idx);
	if (ret != MPI_SUCCESS) {
		ERR("Stop encoder channel %d failed.\n", MPI_GET_ENC_CHN(echn_idx));
		return -EACCES;
	}

	return 0;
}

int checkChnExist(int channel, MPI_ENC_CHN_ATTR_S *chn_attr)
{
	int ret = 0;
	MPI_CHN_STAT_S stat;
	MPI_CHN chn_idx = MPI_VIDEO_CHN(0, channel);
	stat.status = MPI_STATE_NONE;
	ret = MPI_DEV_queryChnState(chn_idx, &stat);
	if (ret != MPI_SUCCESS) {
		ERR("failed to find chn %d\r\n", channel);
		return -EINVAL;
	}
	if (stat.status == MPI_STATE_NONE) {
		ERR("failed, chn %d not exist\r\n", channel);
		return -EACCES;
	}

	if (stat.status == MPI_STATE_STOP) {
		ERR("failed, chn %d stop\r\n", channel);
		return -EACCES;
	}

	if (stat.status == MPI_STATE_SUSPEND) {
		ERR("failed, chn %d suspend\r\n", channel);
		return -EACCES;
	}

	MPI_ECHN echn_idx;

	echn_idx = MPI_ENC_CHN(0);
	echn_idx.chn = channel;

	ret = MPI_ENC_getChnAttr(echn_idx, chn_attr);
	if (ret != MPI_SUCCESS) {
		LOG("failed to get encoder channel %d  rect\n", MPI_GET_ENC_CHN(echn_idx));
		return -EACCES;
	}

	return 0;
}

int resumeEnc(int channel)
{
	int ret = 0;
	MPI_ECHN echn_idx;
	MPI_ENC_BIND_INFO_S bind_info;

	bind_info.idx = MPI_VIDEO_CHN(0, channel);
	echn_idx.chn = channel;

	ret = MPI_ENC_bindToVideoChn(echn_idx, &bind_info);
	if (ret != MPI_SUCCESS) {
		ERR("Bind encoder channel %d failed.\n", MPI_GET_ENC_CHN(echn_idx));
		return MPI_FAILURE;
	}

	ret = MPI_ENC_startChn(echn_idx);
	if (ret != MPI_SUCCESS) {
		ERR("Start encoder channel %d failed.\n", MPI_GET_ENC_CHN(echn_idx));
		return MPI_FAILURE;
	}

	return 0;
}

int parseTimestampUnicode(uint16_t *unicode_txt)
{
	time_t t = { 0 };
	struct tm tm = { 0 };
	char text[64];
	uint16_t unicode_list[31];

	t = time(NULL);
	tm = *localtime(&t);

	snprintf(&text[0], 64, "   %d-%02d-%02d  ", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday);
	for (int i = 0; i < 15; i++) {
		unicode_list[i] = OSD_trans2Unicode(text[i]);
	}

	memcpy(&unicode_list[15], &unicode_by_day[tm.tm_wday][0], sizeof(uint16_t) * 3);

	snprintf(&text[0], 64, "  %02d:%02d:%02d   ", tm.tm_hour, tm.tm_min, tm.tm_sec);

	for (int i = 0; i < 13; i++) {
		unicode_list[15 + 3 + i] = OSD_trans2Unicode(text[i]);
	}

	memcpy(unicode_txt, &unicode_list[0], sizeof(unicode_list));
	return 0;
}

int parseUTF8(json_object *src_obj, OsdText *txt, int chn_idx, int reg_idx)
{
	json_object *tmp4_obj = NULL;
	json_object *tmp5_obj = NULL;

	json_object_object_get_ex(src_obj, "txt", &tmp4_obj);
	if (!tmp4_obj) {
		ERR("Cannot open txt\n");
		goto end;
	}
	if (strcmp("timestamp", json_object_get_string(tmp4_obj)) == 0) {
		ERR("utf8 has no timestamp");
	} else {
		sprintf(&txt->txt[0], "%s", json_object_get_string(tmp4_obj));
	}
	json_object_object_get_ex(src_obj, "ttf_path", &tmp4_obj);
	if (!tmp4_obj) {
		ERR("Cannot open ttf path\n");
		goto end;
	}
	sprintf(&txt->ttf_path[0], "%s", json_object_get_string(tmp4_obj));

	json_object_object_get_ex(src_obj, "forecol", &tmp4_obj);
	if (!tmp4_obj) {
		ERR("Cannot find forecol\n");
		goto end;
	}
	for (int i = 0; i < 3; i++) {
		tmp5_obj = json_object_array_get_idx(tmp4_obj, i);
		if (!tmp5_obj) {
			ERR("Cannot open forecol\n");
			goto end;
		}
		txt->color[i] = json_object_get_int(tmp5_obj);
	}

	json_object_object_get_ex(src_obj, "outcol", &tmp4_obj);
	if (!tmp4_obj) {
		ERR("Cannot find outcol\n");
		goto end;
	}
	for (int i = 0; i < 3; i++) {
		tmp5_obj = json_object_array_get_idx(tmp4_obj, i);
		if (!tmp5_obj) {
			ERR("Cannot open outline_color\n");
			goto end;
		}
		txt->outline_color[i] = json_object_get_int(tmp5_obj);
	}

	json_object_object_get_ex(src_obj, "backcol", &tmp4_obj);
	if (strcmp("WHITE", json_object_get_string(tmp4_obj)) == 0) {
		txt->background = WHITE;
	} else if (strcmp("BLACK", json_object_get_string(tmp4_obj)) == 0) {
		txt->background = BLACK;
	} else if (strcmp("TRANSPARENT", json_object_get_string(tmp4_obj)) == 0) {
		txt->background = TRANSPARENT;
	}

	json_object_object_get_ex(src_obj, "size", &tmp4_obj);
	if (!tmp4_obj) {
		ERR("Cannot open size\n");
		goto end;
	}
	txt->size = json_object_get_int(tmp4_obj);

	json_object_object_get_ex(src_obj, "outline_width", &tmp4_obj);
	if (!tmp4_obj) {
		ERR("Cannot open outline_width\n");
		goto end;
	}
	txt->outline_width = json_object_get_int(tmp4_obj);

	return 0;
end:
	return -1;
}
#define TIMESTAMP (2)
int parseUnicode(json_object *src_obj, OsdText *txt, int chn_idx, int reg_idx, COLOR_MODE mode)
{
	json_object *tmp4_obj = NULL;
	json_object *tmp5_obj = NULL;

	char is_timestamp = 0;
	uint16_t timestamp_list[31];

	txt->mode = mode;

	json_object_object_get_ex(src_obj, "txt", &tmp4_obj);
	if (!tmp4_obj) {
		ERR("Cannot open txt\n");
		goto end;
	}
	if (strcmp("timestamp", json_object_get_string(tmp4_obj)) == 0) {
		ERR("unicode timestamp\r\n");
		parseTimestampUnicode(&timestamp_list[0]);
		g_timestamp_regions.region_list[chn_idx][g_timestamp_regions.region_num[chn_idx]] = reg_idx;
		g_timestamp_regions.region_type[chn_idx][g_timestamp_regions.region_num[chn_idx]] = UNICODE;

		is_timestamp = 1;
	} else {
	}
	json_object_object_get_ex(src_obj, "ttf_path", &tmp4_obj);
	if (!tmp4_obj) {
		ERR("Cannot open ttf path\n");
		goto end;
	}
	sprintf(&txt->ttf_path[0], "%s", json_object_get_string(tmp4_obj));

	json_object_object_get_ex(src_obj, "forecol", &tmp4_obj);
	if (!tmp4_obj) {
		ERR("Cannot find forecol\n");
		goto end;
	}
	for (int i = 0; i < 3; i++) {
		tmp5_obj = json_object_array_get_idx(tmp4_obj, i);
		if (!tmp5_obj) {
			ERR("Cannot open forecol\n");
			goto end;
		}
		txt->color[i] = json_object_get_int(tmp5_obj);
	}

	json_object_object_get_ex(src_obj, "outcol", &tmp4_obj);
	if (!tmp4_obj) {
		ERR("Cannot open outcol\n");
		goto end;
	}
	for (int i = 0; i < 3; i++) {
		tmp5_obj = json_object_array_get_idx(tmp4_obj, i);
		if (!tmp5_obj) {
			ERR("Cannot open outline_color\n");
			goto end;
		}
		txt->outline_color[i] = json_object_get_int(tmp5_obj);
	}

	json_object_object_get_ex(src_obj, "backcol", &tmp4_obj);
	if (strcmp("WHITE", json_object_get_string(tmp4_obj)) == 0) {
		txt->background = WHITE;
	} else if (strcmp("BLACK", json_object_get_string(tmp4_obj)) == 0) {
		txt->background = BLACK;
	} else if (strcmp("TRANSPARENT", json_object_get_string(tmp4_obj)) == 0) {
		txt->background = TRANSPARENT;
	}

	json_object_object_get_ex(src_obj, "size", &tmp4_obj);
	if (!tmp4_obj) {
		ERR("Cannot open size\n");
		goto end;
	}
	txt->size = json_object_get_int(tmp4_obj);

	json_object_object_get_ex(src_obj, "outline_width", &tmp4_obj);
	if (!tmp4_obj) {
		ERR("Cannot open outline_width\n");
		goto end;
	}
	txt->outline_width = json_object_get_int(tmp4_obj);

	if (is_timestamp) {
		LOG("save ayuv list at : %d, %d <--\r\n", chn_idx, reg_idx);
		g_timestamp_regions.region_num[chn_idx] += 1;
		if (mode == PALETTE_8) {
			g_timestamp_regions.ayuv_src_list[chn_idx][reg_idx] =
			        OSD_createUnicodeFontList8bit(txt, &g_text_unicode_array[0], 22);
		} else if (mode == AYUV_3544) {
			g_timestamp_regions.ayuv_src_list[chn_idx][reg_idx] =
			        OSD_createUnicodeFontList(txt, &g_text_unicode_array[0], 22);
		}

		int total_width = 0;
		int total_height = 0;

		OSD_getUnicodeSizetoGenerate(&timestamp_list[0], 31,
		                             g_timestamp_regions.ayuv_src_list[chn_idx][reg_idx], &total_width,
		                             &total_height);

		LOG("get total (%d, %d)\r\n", total_width, total_height);
#ifdef DEBUG
		if (mode == AYUV_3544) {
			char ayuv_buf[total_width * total_height * 2];
			OSD_generateUnicodeFromList(&timestamp_list[0], 31,
			                            g_timestamp_regions.ayuv_src_list[chn_idx][reg_idx], &ayuv_buf[0],
			                            total_width, total_height);
			saveAYUV("/mnt/nfs/ethnfs/merge-demo.ayuv", total_width, total_height, &ayuv_buf[0],
			         total_width * total_height * 2);
		}
#endif
		goto parse_timestamp;
	}

	return 0;
end:
	return -1;
parse_timestamp:
	return TIMESTAMP;
}

int parseBmpAyuv(json_object *src_obj, char *psrc_path)
{
	json_object *tmp4_obj = NULL;
	json_object_object_get_ex(src_obj, "src_path", &tmp4_obj);
	if (!tmp4_obj) {
		ERR("Cannot open src path\n");
		return -EINVAL;
	}
	sprintf(psrc_path, "%s", json_object_get_string(tmp4_obj));

	return 0;
}

int parsePrivacyMask(json_object *src_obj, char *pcolor)
{
	json_object *tmp4_obj = NULL;
	json_object *tmp5_obj = NULL;
	char color[4] = { 0, 0, 0, 0xff };
	json_object_object_get_ex(src_obj, "color", &tmp4_obj);
	if (!tmp4_obj) {
		ERR("Failed to parse line end\r\n");
		return -EINVAL;
	}
	tmp5_obj = json_object_array_get_idx(tmp4_obj, 0);
	color[0] = json_object_get_int(tmp5_obj);
	tmp5_obj = json_object_array_get_idx(tmp4_obj, 1);
	color[1] = json_object_get_int(tmp5_obj);
	tmp5_obj = json_object_array_get_idx(tmp4_obj, 2);
	color[2] = json_object_get_int(tmp5_obj);
	tmp5_obj = json_object_array_get_idx(tmp4_obj, 3);
	color[3] = json_object_get_int(tmp5_obj);

	*pcolor = color[0];
	pcolor++;
	*pcolor = color[1];
	pcolor++;
	*pcolor = color[2];
	pcolor++;
	*pcolor = color[3];

	return 0;
}

int freeParseHandle(json_object *config_obj)
{
	json_object_put(config_obj);
	return 0;
}

int parseMoveObj(json_object *src_obj, int chn_idx, int reg_idx)
{
	json_object *tmp4_obj = NULL;
	json_object *tmp5_obj = NULL;
	json_object *tmp6_obj = NULL;

	json_object_object_get_ex(src_obj, "position", &tmp4_obj);
	if (!tmp4_obj) {
		ERR("Cannot open position\n");
		goto end;
	}
	int position_num = json_object_array_length(tmp4_obj);
	LOG("num of position: %d\r\n", position_num);
	g_moving_regions.move_obj[chn_idx][g_moving_regions.region_num[chn_idx]].step = position_num;

	for (int i = 0; i < position_num; i++) {
		tmp5_obj = json_object_array_get_idx(tmp4_obj, i);
		if (!tmp5_obj) {
			ERR("Cannot open position[%d]\n", i);
			goto end;
		}

		tmp6_obj = json_object_array_get_idx(tmp5_obj, 0);
		g_moving_regions.move_obj[chn_idx][g_moving_regions.region_num[chn_idx]].point[i].x =
		        json_object_get_int(tmp6_obj);
		LOG("GET x %d\r\n", json_object_get_int(tmp6_obj));
		tmp6_obj = json_object_array_get_idx(tmp5_obj, 1);
		g_moving_regions.move_obj[chn_idx][g_moving_regions.region_num[chn_idx]].point[i].y =
		        json_object_get_int(tmp6_obj);
		LOG("GET y %d\r\n", json_object_get_int(tmp6_obj));
	}

	json_object_object_get_ex(src_obj, "interval", &tmp4_obj);
	if (!tmp4_obj) {
		ERR("Cannot open interval\n");
		goto end;
	}
	LOG("[%d][%d]get interval:%d\r\n", chn_idx, reg_idx, json_object_get_int(tmp4_obj));
	g_moving_regions.move_obj[chn_idx][g_moving_regions.region_num[chn_idx]].interval =
	        json_object_get_int(tmp4_obj);
	g_moving_regions.move_obj[chn_idx][g_moving_regions.region_num[chn_idx]].max_step = position_num;
	g_moving_regions.region_list[chn_idx][g_moving_regions.region_num[chn_idx]] = reg_idx;

	g_moving_regions.region_num[chn_idx] += 1;

	return 0;

end:
	return -1;
}

int parseMoveSrc(json_object *src_obj, int chn_idx, OsdHandle *hd, int reg_idx, COLOR_MODE mode)
{
	json_object *tmp4_obj = NULL;
	json_object *tmp5_obj = NULL;
	json_object *tmp6_obj = NULL;
	json_object *tmp7_obj = NULL;

	json_object_object_get_ex(src_obj, "src", &tmp4_obj);
	if (!tmp4_obj) {
		ERR("Cannot open moving src\n");
		return -EINVAL;
	}

	if (strcmp("utf8", json_object_get_string(tmp4_obj)) == 0) {
		LOG("get utf8\r\n");

		OsdText txt;
		int w, h;

		if (parseUTF8(src_obj, &txt, chn_idx, reg_idx) != 0) {
			return -EINVAL;
		}
		txt.mode = mode;

		char *ayuv_src;
		if (mode == PALETTE_8) {
			ayuv_src = OSD_createTextUTF8Src8bit(&txt, &w, &h);
		} else {
			ayuv_src = OSD_createTextUTF8Src(&txt, &w, &h);
		}

		OSD_setImageAYUVptr(
		        g_osd_handle[chn_idx], reg_idx, ayuv_src, w, h, mode,
		        (char *)(g_osd_canvas_attr[chn_idx][hd->region[reg_idx].include_canvas].canvas_addr));

		if (ayuv_src == NULL) {
			OSDERR("failed to create utf8 src ptr\r\n");
			return -EINVAL;
		}

		OSD_destroySrc(ayuv_src);

	} else if (strcmp("unicode", json_object_get_string(tmp4_obj)) == 0) {
		LOG("get unicode\r\n");

		OsdText txt;
		int ret = parseUnicode(src_obj, &txt, chn_idx, reg_idx, mode);
		LOG("parse unicode ret: %d\r\n", ret);

		if (ret != TIMESTAMP) {
			ERR("reg[%d] unicode\r\n", reg_idx);
			int w, h;
			char *ayuv_unicode_src = OSD_createTextUnicodeSrc(&txt, &w, &h);
			if (ayuv_unicode_src == NULL) {
				OSDERR("Failed to create ayuv src\r\n");
				return -EINVAL;
			}
			OSD_setImageAYUVptr(
			        g_osd_handle[chn_idx], reg_idx, ayuv_unicode_src, w, h, mode,
			        (char *)(g_osd_canvas_attr[chn_idx][hd->region[reg_idx].include_canvas].canvas_addr));

			OSD_destroySrc(ayuv_unicode_src);

		} else if (ret == TIMESTAMP) {
			LOG("reg[%d] unicode-timestamp\r\n", reg_idx);
		} else if (ret < 0) {
			return -EINVAL;
		}

	} else if (strcmp("ayuv", json_object_get_string(tmp4_obj)) == 0) {
		if (mode == PALETTE_8) {
			ERR("palette 8 mode can't set ayuv\r\n");
			return -EINVAL;
		}

		char src_path[128];

		parseBmpAyuv(src_obj, &src_path[0]);
		OSD_setImage(g_osd_handle[chn_idx], reg_idx, &src_path[0],
		             (char *)(g_osd_canvas_attr[chn_idx][hd->region[reg_idx].include_canvas].canvas_addr));

	} else if (strcmp("bmp", json_object_get_string(tmp4_obj)) == 0) {
		if (mode == PALETTE_8) {
			ERR("palette 8 mode can't set bmp\r\n");
			return -EINVAL;
		}

		char src_path[128];
		parseBmpAyuv(src_obj, &src_path[0]);
		OSD_setImageBmp(g_osd_handle[chn_idx], reg_idx, &src_path[0],
		                (char *)(g_osd_canvas_attr[chn_idx][hd->region[reg_idx].include_canvas].canvas_addr));

	} else if (strcmp("line", json_object_get_string(tmp4_obj)) == 0) {
		int line_num = 0;
		json_object_object_get_ex(src_obj, "line_num", &tmp4_obj);
		if (!tmp4_obj) {
			ERR("Cannot open line num\n");
			return -EINVAL;
		}

		line_num = json_object_get_int(tmp4_obj);
		OsdLine newLine[line_num];
		OsdPoint p[2];
		char color[4] = { 0, 0, 0, 0xff };
		int thickness;
		json_object_object_get_ex(src_obj, "thickness", &tmp4_obj);
		if (!tmp4_obj) {
			ERR("Cannot open line num\n");
			return -EINVAL;
		}
		thickness = json_object_get_int(tmp4_obj);

		json_object_object_get_ex(src_obj, "color", &tmp4_obj);
		if (!tmp4_obj) {
			ERR("Failed to parse line end\r\n");
			return -EINVAL;
		}
		tmp5_obj = json_object_array_get_idx(tmp4_obj, 0);
		color[0] = json_object_get_int(tmp5_obj);
		tmp5_obj = json_object_array_get_idx(tmp4_obj, 1);
		color[1] = json_object_get_int(tmp5_obj);
		tmp5_obj = json_object_array_get_idx(tmp4_obj, 2);
		color[2] = json_object_get_int(tmp5_obj);
		tmp5_obj = json_object_array_get_idx(tmp4_obj, 3);
		color[3] = json_object_get_int(tmp5_obj);

		json_object_object_get_ex(src_obj, "lines", &tmp4_obj);
		if (!tmp4_obj) {
			ERR("Cannot open line num\n");
			return -EINVAL;
		}
		for (int j = 0; j < line_num; j++) {
			tmp5_obj = json_object_array_get_idx(tmp4_obj, j);
			if (!tmp5_obj) {
				ERR("Cannot open line[%d]\n", j);
				return -EINVAL;
			}
			for (int k = 0; k < 2; k++) {
				/*start or end*/
				tmp6_obj = json_object_array_get_idx(tmp5_obj, k);
				if (!tmp6_obj) {
					ERR("Cannot open line[%d]\n", j);
					return -EINVAL;
				}
				tmp7_obj = json_object_array_get_idx(tmp6_obj, 0);
				p[k].x = json_object_get_int(tmp7_obj);
				tmp7_obj = json_object_array_get_idx(tmp6_obj, 1);
				p[k].y = json_object_get_int(tmp7_obj);
			}

			LOG("%d %d %d %d\r\n", p[0].x, p[0].y, p[1].x, p[1].y);
			if (calcNewLine(p[0].x, p[0].y, p[1].x, p[1].y, thickness, &hd->region[reg_idx], &newLine[j]) !=
			    0) {
				ERR("Failed to calc line [%d]", j);
				return -EINVAL;
			}

			memcpy(&newLine[j].color[0], &color[0], sizeof(color));
			newLine[j].mode = mode;
			OSD_setLine(
			        g_osd_handle[chn_idx], reg_idx, &newLine[j],
			        (char *)(g_osd_canvas_attr[chn_idx][hd->region[reg_idx].include_canvas].canvas_addr));
		}

	} else if (strcmp("privacy_mask", json_object_get_string(tmp4_obj)) == 0) {
		LOG("get privacy mask\r\n");

		char color[4] = { 0, 0, 0, 0xff };

		parsePrivacyMask(src_obj, &color[0]);

		OSD_setPrivacyMask(
		        g_osd_handle[chn_idx], reg_idx, &color[0], mode,
		        (char *)(g_osd_canvas_attr[chn_idx][hd->region[reg_idx].include_canvas].canvas_addr));

	} else {
		ERR("Invalid moving src type: %s\r\n", json_object_get_string(tmp4_obj));
		return -EINVAL;
	}

	return 0;
}

int setRegionSrc(char *file_name, int idx, OsdHandle *hd, COLOR_MODE mode)
{
	json_object *test_obj = NULL;
	json_object *tmp_obj = NULL;
	json_object *tmp1_obj = NULL;
	json_object *tmp2_obj = NULL;
	json_object *tmp3_obj = NULL;
	json_object *tmp4_obj = NULL;
	json_object *tmp5_obj = NULL;
	json_object *tmp6_obj = NULL;
	json_object *tmp7_obj = NULL;

	/* load json config from json file */
	test_obj = json_object_from_file(file_name);
	if (!test_obj) {
		ERR("Cannot open %s\n", file_name);
		goto end;
	}

	json_object_object_get_ex(test_obj, "regions", &tmp_obj);
	tmp1_obj = json_object_array_get_idx(tmp_obj, idx);
	if (!tmp1_obj) {
		ERR("Cannot open %s\n", file_name);
		goto end;
	}

	json_object_object_get_ex(tmp1_obj, "src", &tmp2_obj);
	if (!tmp2_obj) {
		ERR("failed to find src\r\n");
		goto end;
	}
	int src_num = json_object_array_length(tmp2_obj);
	if (src_num < hd->osd_num) {
		ERR("some region no define src. src_num: %d, region_num: %d\r\n", src_num, hd->osd_num);
		goto end;
	}

	for (int i = 0; i < hd->osd_num; i++) {
		LOG("CHN %d has osd: %d\r\n", idx, i);
		tmp3_obj = json_object_array_get_idx(tmp2_obj, i);
		if (!tmp3_obj) {
			ERR("Cannot open %s\n", file_name);
			goto end;
		}

		json_object_object_get_ex(tmp3_obj, "type", &tmp4_obj);
		if (!tmp4_obj) {
			ERR("Cannot open %s\n", file_name);
			goto end;
		}

		if (strcmp("none", json_object_get_string(tmp4_obj)) == 0) {
			ERR("reg[%d] define empty, skip it\r\n", i);
			continue;
		}

		if (strcmp("move_obj", json_object_get_string(tmp4_obj)) == 0) {
			if (hd->osd_num > 4) {
				ERR("reg[%d] define move_obj, skip it\r\n", i);
				continue;
			}
			if (0 != parseMoveObj(tmp3_obj, idx, i)) {
				continue;
			}

			if (0 != parseMoveSrc(tmp3_obj, idx, hd, i, mode)) {
				continue;
			}

		} else if (strcmp("utf8", json_object_get_string(tmp4_obj)) == 0) {
			LOG("reg[%d] utf8\r\n", i);
			OsdText txt;
			int w, h;

			if (parseUTF8(tmp3_obj, &txt, idx, i) != 0) {
				goto end;
			}
			txt.mode = mode;

			char *ayuv_src;
			if (mode == PALETTE_8) {
				ayuv_src = OSD_createTextUTF8Src8bit(&txt, &w, &h);
			} else {
				ayuv_src = OSD_createTextUTF8Src(&txt, &w, &h);
			}

			if (ayuv_src == NULL) {
				OSDERR("failed to create utf8 src ptr\r\n");
				goto end;
			}

#if 0
			saveAYUV("save-at.ayuv", w, h, ayuv_src, w * h * 2);
#endif

			OSD_setImageAYUVptr(g_osd_handle[idx], i, ayuv_src, w, h, mode,
			                    (char *)(g_osd_canvas_attr[idx][hd->region[i].include_canvas].canvas_addr));

			OSD_destroySrc(ayuv_src);
		} else if (strcmp("unicode", json_object_get_string(tmp4_obj)) == 0) {
			OsdText txt;
			int ret = parseUnicode(tmp3_obj, &txt, idx, i, mode);
			LOG("parse unicode ret: %d\r\n", ret);

			if (ret != TIMESTAMP) {
				ERR("reg[%d] unicode\r\n", i);

				int w, h;
				char *ayuv_unicode_src = OSD_createTextUnicodeSrc(&txt, &w, &h);
				if (ayuv_unicode_src == NULL) {
					OSDERR("Failed to create ayuv src\r\n");
					goto end;
				}
				OSD_setImageAYUVptr(
				        g_osd_handle[idx], i, ayuv_unicode_src, w, h, mode,
				        (char *)(g_osd_canvas_attr[idx][hd->region[i].include_canvas].canvas_addr));

				OSD_destroySrc(ayuv_unicode_src);

			} else if (ret == TIMESTAMP) {
				LOG("reg[%d] unicode-timestamp\r\n", i);
			} else if (ret < 0) {
				goto end;
			}

		} else if (strcmp("ayuv", json_object_get_string(tmp4_obj)) == 0) {
			LOG("reg[%d] ayuv\r\n", i);
			if (mode == PALETTE_8) {
				ERR("PALETTE_8 color mode not support ayuv3544 src, skip it\r\n");
				continue;
			}
			char src_path[128];

			parseBmpAyuv(tmp3_obj, &src_path[0]);

			OSD_setImage(g_osd_handle[idx], i, &src_path[0],
			             (char *)(g_osd_canvas_attr[idx][hd->region[i].include_canvas].canvas_addr));

		} else if (strcmp("bmp", json_object_get_string(tmp4_obj)) == 0) {
			LOG("reg[%d] bmp\r\n", i);
			if (mode == PALETTE_8) {
				ERR("PALETTE_8 color mode not support ayuv3544 src, skip it\r\n");
				continue;
			}

			char src_path[128];
			parseBmpAyuv(tmp3_obj, &src_path[0]);

			OSD_setImageBmp(g_osd_handle[idx], i, &src_path[0],
			                (char *)(g_osd_canvas_attr[idx][hd->region[i].include_canvas].canvas_addr));
		} else if (strcmp("line", json_object_get_string(tmp4_obj)) == 0) {
			LOG("reg[%d] line\r\n", i);

			int line_num = 0;
			json_object_object_get_ex(tmp3_obj, "line_num", &tmp4_obj);
			if (!tmp4_obj) {
				ERR("Cannot open line num\n");
				goto end;
			}

			line_num = json_object_get_int(tmp4_obj);
			OsdLine newLine[line_num];
			OsdPoint p[2];
			char color[4] = { 0, 0, 0, 0xff };
			int thickness;
			json_object_object_get_ex(tmp3_obj, "thickness", &tmp4_obj);
			if (!tmp4_obj) {
				ERR("Cannot open line num\n");
				goto end;
			}
			thickness = json_object_get_int(tmp4_obj);

			json_object_object_get_ex(tmp3_obj, "color", &tmp4_obj);
			if (!tmp4_obj) {
				ERR("Failed to parse line end\r\n");
				goto end;
			}
			tmp5_obj = json_object_array_get_idx(tmp4_obj, 0);
			color[0] = json_object_get_int(tmp5_obj);
			tmp5_obj = json_object_array_get_idx(tmp4_obj, 1);
			color[1] = json_object_get_int(tmp5_obj);
			tmp5_obj = json_object_array_get_idx(tmp4_obj, 2);
			color[2] = json_object_get_int(tmp5_obj);
			tmp5_obj = json_object_array_get_idx(tmp4_obj, 3);
			color[3] = json_object_get_int(tmp5_obj);

			json_object_object_get_ex(tmp3_obj, "lines", &tmp4_obj);
			if (!tmp4_obj) {
				ERR("Cannot open line num\n");
				goto end;
			}
			for (int j = 0; j < line_num; j++) {
				tmp5_obj = json_object_array_get_idx(tmp4_obj, j);
				if (!tmp5_obj) {
					ERR("Cannot open line[%d]\n", j);
					goto end;
				}
				for (int k = 0; k < 2; k++) {
					/*start or end*/
					tmp6_obj = json_object_array_get_idx(tmp5_obj, k);
					if (!tmp6_obj) {
						ERR("Cannot open line[%d]\n", j);
						goto end;
					}
					tmp7_obj = json_object_array_get_idx(tmp6_obj, 0);
					p[k].x = json_object_get_int(tmp7_obj);
					tmp7_obj = json_object_array_get_idx(tmp6_obj, 1);
					p[k].y = json_object_get_int(tmp7_obj);
				}

				LOG("%d %d %d %d\r\n", p[0].x, p[0].y, p[1].x, p[1].y);
				if (calcNewLine(p[0].x, p[0].y, p[1].x, p[1].y, thickness, &hd->region[i],
				                &newLine[j]) != 0) {
					ERR("Failed to calc line [%d]", j);
					goto end;
				}

				memcpy(&newLine[j].color[0], &color[0], sizeof(color));
				newLine[j].mode = mode;
				OSD_setLine(g_osd_handle[idx], i, &newLine[j],
				            (char *)(g_osd_canvas_attr[idx][hd->region[i].include_canvas].canvas_addr));
			}

		} else if (strcmp("privacy_mask", json_object_get_string(tmp4_obj)) == 0) {
			LOG("reg[%d] privacy mask\r\n", i);

			char color[4] = { 0, 0, 0, 0xff };

			parsePrivacyMask(tmp3_obj, &color[0]);
			OSD_setPrivacyMask(g_osd_handle[idx], i, &color[0], mode,
			                   (char *)(g_osd_canvas_attr[idx][hd->region[i].include_canvas].canvas_addr));

		} else {
			ERR("Invalid region type: %s\r\n", json_object_get_string(tmp4_obj));
			break;
		}

		if (MPI_updateOsdCanvas(g_osd_chn_handle[idx][hd->region[i].include_canvas]) != MPI_SUCCESS) {
			OSDERR("failed to update canvas\r\n");
			MPI_destroyOsdRgn(g_osd_chn_handle[idx][hd->region[i].include_canvas]);
		}
	}

end:
	freeParseHandle(test_obj);
	return 0;
}

int getColorMode(char *file_name, COLOR_MODE *mode)
{
	json_object *test_obj = NULL;
	json_object *tmp_obj = NULL;

	/* load json config from json file */
	test_obj = json_object_from_file(file_name);
	if (!test_obj) {
		ERR("Cannot open %s\n", file_name);
		goto end;
	}

	json_object_object_get_ex(test_obj, "color_mode", &tmp_obj);
	if (!tmp_obj) {
		ERR("Failed to find color mode\r\n");
		goto end;
	}

	if (strcmp("AYUV_3544", json_object_get_string(tmp_obj)) == 0) {
		*mode = AYUV_3544;
	} else if (strcmp("PALETTE_8", json_object_get_string(tmp_obj)) == 0) {
		*mode = PALETTE_8;
	} else {
		ERR("Failed to find color mode\r\n");
		goto end;
	}

	freeParseHandle(test_obj);
	return 0;
end:
	freeParseHandle(test_obj);
	return -EBADF;
}

int getRegionRect(char *file_name, int idx, int reg_idx, int *x, int *y, int *w, int *h)
{
	json_object *test_obj = NULL;
	json_object *tmp_obj = NULL;
	json_object *tmp1_obj = NULL;
	json_object *tmp2_obj = NULL;
	json_object *tmp3_obj = NULL;
	json_object *tmp4_obj = NULL;

	/* load json config from json file */
	test_obj = json_object_from_file(file_name);
	if (!test_obj) {
		ERR("Cannot open %s\n", file_name);
		return -EBADF;
	}

	json_object_object_get_ex(test_obj, "regions", &tmp_obj);
	tmp1_obj = json_object_array_get_idx(tmp_obj, idx);
	if (!tmp1_obj) {
		ERR("Cannot open %s\n", file_name);
		goto end;
	}
	json_object_object_get_ex(tmp1_obj, "region", &tmp2_obj);
	tmp3_obj = json_object_array_get_idx(tmp2_obj, reg_idx);
	if (!tmp3_obj) {
		ERR("Cannot open %s\n", file_name);
		goto end;
	}

	tmp4_obj = json_object_array_get_idx(tmp3_obj, 0);
	*x = json_object_get_int(tmp4_obj);

	tmp4_obj = json_object_array_get_idx(tmp3_obj, 1);
	*y = json_object_get_int(tmp4_obj);

	tmp4_obj = json_object_array_get_idx(tmp3_obj, 2);
	*w = json_object_get_int(tmp4_obj);

	tmp4_obj = json_object_array_get_idx(tmp3_obj, 3);
	*h = json_object_get_int(tmp4_obj);

	json_object_object_get_ex(tmp1_obj, "src", &tmp2_obj);
	tmp3_obj = json_object_array_get_idx(tmp2_obj, reg_idx);
	if (!tmp3_obj) {
		ERR("Cannot src[%d]\n", reg_idx);
		goto end;
	}

	json_object_object_get_ex(tmp3_obj, "type", &tmp4_obj);
	if (!tmp4_obj) {
		ERR("Failed to find src type\r\n");
		goto end;
	}

	/*calc max region size needed*/
	if (strcmp("unicode", json_object_get_string(tmp4_obj)) == 0) {
		ERR("reg[%d] is unicode\r\n", reg_idx);
		json_object_object_get_ex(tmp3_obj, "txt", &tmp4_obj);
		if (!tmp4_obj) {
			ERR("Failed to find src type\r\n");
			goto end;
		}

		if (strcmp("timestamp", json_object_get_string(tmp4_obj)) == 0) {
			ERR("reg[%d] is timestamp\r\n", reg_idx);
		}

		OsdText txt = { .background = WHITE,
			        .color = { 0xff, 0x00, 0x00 },
			        .outline_color = { 0xff, 0xff, 0x00 } };

		getColorMode(file_name, &txt.mode);

		json_object_object_get_ex(tmp3_obj, "ttf_path", &tmp4_obj);
		if (!tmp4_obj) {
			ERR("Cannot open ttf path\n");
			goto end;
		}
		sprintf(&txt.ttf_path[0], "%s", json_object_get_string(tmp4_obj));

		json_object_object_get_ex(tmp3_obj, "size", &tmp4_obj);
		if (!tmp4_obj) {
			ERR("Cannot open size\n");
			goto end;
		}
		txt.size = json_object_get_int(tmp4_obj);

		json_object_object_get_ex(tmp3_obj, "outline_width", &tmp4_obj);
		if (!tmp4_obj) {
			ERR("Cannot open outline_width\n");
			goto end;
		}
		txt.outline_width = json_object_get_int(tmp4_obj);

		char *tmp_fontlist = NULL;

		if (txt.mode == PALETTE_8) {
			tmp_fontlist = OSD_createUnicodeFontList8bit(&txt, &g_text_unicode_array[0], 22);
		} else if (txt.mode == AYUV_3544) {
			tmp_fontlist = OSD_createUnicodeFontList(&txt, &g_text_unicode_array[0], 22);
		}
		LOG("(%d, %d)\n", *w, *h);
		AyuvSrcList *ayuv_list = (AyuvSrcList *)tmp_fontlist;
		*w = ayuv_list->src[0].width;
		*h = ayuv_list->src[0].height;
		/*timestamp format"   yyyy-MM-DD  hh:mm:ss  <text><text><text>   "*/
		/*get max height*/
		for (int i = 0; i < ayuv_list->len; i++) {
			if (*h < ayuv_list->src[i].height) {
				*h = ayuv_list->src[i].height;
			}
		}

		/*get max width in format*/
		/*get max number width, font list 0 - 9 are numbers*/
		for (int i = 0; i < 9; i++) {
			if (*w < ayuv_list->src[i].width) {
				*w = ayuv_list->src[i].width;
			}
		}
		*w = *w * 14;
		/*add static space '-' ':' size*/
		*w += ayuv_list->src[12].width * 10; /*has 10 space*/
		*w += ayuv_list->src[11].width * 2; /*has 2 '-'*/
		*w += ayuv_list->src[10].width * 2; /*has 2 ':'*/
		/*add max text size*/
		int text_width = 0;
		for (int i = 13; i < 21; i++) {
			if (text_width < ayuv_list->src[i].width) {
				text_width = ayuv_list->src[i].width;
			}
		}
		*w += text_width * 3;
		LOG("set w,h (%d, %d)\r\n", *w, *h);

		OSD_destroyUnicodeFontList(tmp_fontlist);
	}

end:
	freeParseHandle(test_obj);
	return 0;
}

int getRegionNum(char *file_name, int idx, int *num)
{
	json_object *test_obj = NULL;
	json_object *tmp_obj = NULL;
	json_object *tmp1_obj = NULL;
	json_object *tmp2_obj = NULL;

	/* load json config from json file */
	test_obj = json_object_from_file(file_name);
	if (!test_obj) {
		ERR("Cannot open %s\n", file_name);
		goto end;
	}

	json_object_object_get_ex(test_obj, "regions", &tmp_obj);
	tmp1_obj = json_object_array_get_idx(tmp_obj, idx);
	if (!tmp1_obj) {
		ERR("Cannot open %s\n", file_name);
		goto end;
	}
	json_object_object_get_ex(tmp1_obj, "region_num", &tmp2_obj);
	if (json_object_get_int(tmp2_obj) > MAX_OSD) {
		ERR("region num > max %d", MAX_OSD);
		goto end;
	}
	*num = json_object_get_int(tmp2_obj);

	freeParseHandle(test_obj);
	return 0;

end:
	freeParseHandle(test_obj);
	return -EINVAL;
}

int getHandleRect(char *file_name, int idx, int *width, int *height)
{
	json_object *test_obj = NULL;
	json_object *tmp_obj = NULL;
	json_object *tmp1_obj = NULL;
	json_object *tmp2_obj = NULL;

	/* load json config from json file */
	test_obj = json_object_from_file(file_name);
	if (!test_obj) {
		ERR("Cannot open %s\n", file_name);
		goto end;
	}

	json_object_object_get_ex(test_obj, "rect", &tmp_obj);
	tmp1_obj = json_object_array_get_idx(tmp_obj, idx);
	if (!tmp1_obj) {
		ERR("Cannot open %s\n", file_name);
		goto end;
	}
	tmp2_obj = json_object_array_get_idx(tmp1_obj, 0);
	*width = json_object_get_int(tmp2_obj);
	tmp2_obj = json_object_array_get_idx(tmp1_obj, 1);
	*height = json_object_get_int(tmp2_obj);

	freeParseHandle(test_obj);
	return 0;
end:
	freeParseHandle(test_obj);
	return -EBADF;
}

int getConfigChnNum(char *file_name, int *num, char *channel_id)
{
	json_object *test_obj = NULL;
	json_object *tmp_obj = NULL;
	json_object *tmp1_obj = NULL;
	int channel_idx = 0;

	/* load json config from json file */
	test_obj = json_object_from_file(file_name);
	if (!test_obj) {
		ERR("Cannot open %s\n", file_name);
		return -EBADF;
	}

	json_object_object_get_ex(test_obj, "channel", &tmp_obj);
	if (!tmp_obj) {
		ERR("failed to find channel\r\n");
	}
	*num = json_object_array_length(tmp_obj);
	LOG("channel num: %d\r\n", *num);

	for (int i = 0; i < *num; i++) {
		tmp1_obj = json_object_array_get_idx(tmp_obj, i);
		if (!tmp1_obj) {
			ERR("Failed to get chn id\r\n");
			break;
		}
		channel_idx = json_object_get_int(tmp1_obj);
		memcpy((void *)(channel_id + i), &channel_idx, 1);
	}

	freeParseHandle(test_obj);

	return 0;
}

void help()
{
	printf("usage:\r\n");
	printf("-h help()\r\n");
	printf("-c config path\r\n");
	return;
}

static void handleSigInt(int signo)
{
	if (signo == SIGINT) {
		printf("Caught SIGINT!\n");
	} else if (signo == SIGTERM) {
		printf("Caught SIGTERM!\n");
	} else if (signo == SIGPIPE) {
		printf("Caught SIGPIPE!\n");
		return;
	} else {
		perror("Unexpected signal!\n");
	}
	gRunflag = 0;
}

int main(int argc, char **argv)
{
	if (signal(SIGINT, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGINT!\n");
		exit(1);
	}

	if (signal(SIGTERM, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGTERM!\n");
		exit(1);
	}

	if (signal(SIGPIPE, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGPIPE!\n");
		exit(1);
	}

	int c = 0;
	char config_path[128];
	while ((c = getopt(argc, argv, "hc:")) != -1) {
		switch (c) {
		case 'h':
			help();
			exit(1);
			break;
		case 'c':
			printf("config : %s\r\n", argv[optind - 1]);
			snprintf(&config_path[0], 128, "%s", argv[optind - 1]);
			break;
		}
	}
	MPI_SYS_init();

	int channel_num = 0;
	MPI_ENC_CHN_ATTR_S chn_attr[3];
	for (int i = 0; i < 3; i++) {
		if (0 == checkChnExist(i, &chn_attr[i])) {
			channel_num++;
		} else {
			break;
		}
	}

	for (int i = 0; i < channel_num; i++) {
		stopEnc(i);
		LOG("%d, %d\r\n", chn_attr[i].res.width, chn_attr[i].res.height);
	}

	sleep(2);

	int config_num, width, height, region_num;
	char osd_channel[3];
	if (0 != getConfigChnNum(&config_path[0], &config_num, &osd_channel[0])) {
		goto end;
	}
	LOG("CONFIG num: %d\r\n", config_num);

	if (config_num > channel_num) {
		ERR("Config chn > exist chanenel, err");
		goto end;
	}

	for (int i = 0; i < config_num; i++) {
		LOG("[%d] chn %d\r\n", i, osd_channel[i]);
	}

	OSD_init();

	memset(&g_timestamp_regions, 0, sizeof(g_timestamp_regions));

	MPI_ECHN echn_idx;
	echn_idx = MPI_ENC_CHN(0);
	MPI_OSD_BIND_ATTR_S osd_bind = { { 0 } };

	MPI_OSD_RGN_ATTR_S osd_attr = { .show = true,
		                        .qp_enable = false,
		                        .color_format = MPI_OSD_COLOR_FORMAT_AYUV_3544,
		                        .osd_type = MPI_OSD_OVERLAY_BITMAP };
	COLOR_MODE mode = AYUV_3544;
	if (0 != getColorMode(&config_path[0], &mode)) {
		goto deinit;
	}

	if (mode == PALETTE_8) {
		ERR("get color mode PALETTE_8\r\n");
		osd_attr.color_format = MPI_OSD_COLOR_FORMAT_PALETTE_8;
	}

	for (int i = 0; i < config_num; i++) {
		if (0 != checkChnExist(osd_channel[i], &chn_attr[i])) {
			ERR("chn[%d] not exist\r\n", i);
			goto deinit;
		}

		if (0 != getHandleRect(&config_path[0], i, &width, &height)) {
			goto end;
		}

		LOG("(%d, %d)\r\n", width, height);

		if ((width <= chn_attr[i].res.width) && (height <= chn_attr[i].res.height)) {
			g_osd_handle[i] = OSD_create(width, height);
		} else {
			ERR("exceed width, height of chn %d, %d\r\n", width, height);
			goto deinit;
		}

		if (0 != getRegionNum(&config_path[0], i, &region_num)) {
			goto deinit;
		}

		LOG("REGION num: %d\r\n", region_num);
		OsdRegion region[8];
		memset(&region[0], 0, sizeof(region));

		int x, y, w, h;

		for (int j = 0; j < region_num; j++) {
			getRegionRect(&config_path[0], i, j, &x, &y, &w, &h);
			region[j].startX = x;
			region[j].startY = y;
			region[j].width = w;
			region[j].height = h;
			LOG("get %d %d %d %d\r\n", x, y, w, h);
		}

		for (int j = 0; j < region_num; j++) {
			if (OSD_addOsd(g_osd_handle[i], j, &region[j]) != 0) {
				goto deinit;
			}
		}

		if (region_num > 4) {
			if (OSD_calcCanvasbygroup(g_osd_handle[i]) != 0) {
				goto deinit;
			}
		} else {
			if (OSD_calcCanvas(g_osd_handle[i]) != 0) {
				goto deinit;
			}
		}

		logAllOsdHandle(g_osd_handle[i]);

		echn_idx = MPI_ENC_CHN(0);
		echn_idx.chn = osd_channel[i];
		osd_bind.idx = echn_idx;

		int calc_canvas = 4;
		if (region_num < 4) {
			calc_canvas = region_num;
		}
		for (int j = 0; j < calc_canvas; j++) {
			osd_attr.size.width = g_osd_handle[i]->canvas[j].width;
			osd_attr.size.height = g_osd_handle[i]->canvas[j].height;
			osd_bind.point.x = g_osd_handle[i]->canvas[j].startX;
			osd_bind.point.y = g_osd_handle[i]->canvas[j].startY;
			if (0 != createOsdInstance(&osd_attr, &osd_bind, &g_osd_chn_handle[i][j],
			                           &g_osd_canvas_attr[i][j])) {
				ERR("failed to init canvas[%d]\r\n", j);
				goto deinit;
			}
		}
		setRegionSrc(&config_path[0], i, g_osd_handle[i], mode);
	}

	for (int i = 0; i < channel_num; i++) {
		resumeEnc(i);
	}

	gRunflag = 1;
	int include_canvas = 0;
	int reg_idx = 0;
	uint16_t timestamp_list[31];
	int total_ayuv_width = 0;
	int total_ayuv_height = 0;
	int cnt = 0;
	OsdRegion region2Move;

	while (gRunflag) {
		for (int i = 0; i < 3; i++) {
			LOG("chn %d, has move obj :%d\r\n", i, g_moving_regions.region_num[i]);
			for (int j = 0; j < g_moving_regions.region_num[i]; j++) {
				reg_idx = g_moving_regions.region_list[i][j];
				include_canvas = g_osd_handle[i]->region[reg_idx].include_canvas;

				if (g_moving_regions.move_obj[i][j].step == g_moving_regions.move_obj[i][j].max_step) {
					g_moving_regions.move_obj[i][j].step = 0;
				}

				if ((cnt % g_moving_regions.move_obj[i][j].interval) == 0) {
					region2Move.startX = g_moving_regions.move_obj[i][j]
					                             .point[g_moving_regions.move_obj[i][j].step]
					                             .x;
					region2Move.startY = g_moving_regions.move_obj[i][j]
					                             .point[g_moving_regions.move_obj[i][j].step]
					                             .y;
					region2Move.width = g_osd_handle[i]->region[reg_idx].width;
					region2Move.height = g_osd_handle[i]->region[reg_idx].height;

					OSD_moveOsdRegion(g_osd_handle[i], reg_idx, &region2Move);
					OSD_calcCanvas(g_osd_handle[i]);

					osd_bind.point.x = g_osd_handle[i]->canvas[include_canvas].startX;
					osd_bind.point.y = g_osd_handle[i]->canvas[include_canvas].startY;

					if (moveOsdInstance(&osd_bind, &g_osd_chn_handle[i][include_canvas]) !=
					    MPI_SUCCESS) {
						ERR("failed to move canvas\r\n");
						MPI_destroyOsdRgn(g_osd_chn_handle[i][include_canvas]);
						gRunflag = 0;
					}

					if (MPI_updateOsdCanvas(g_osd_chn_handle[i][include_canvas]) != MPI_SUCCESS) {
						ERR("failed to update canvas\r\n");
						MPI_destroyOsdRgn(g_osd_chn_handle[i][include_canvas]);
						gRunflag = 0;
					}

					LOG("chn[%d][%d] move to (%d %d), step: %d\r\n", i, include_canvas,
					    region2Move.startX, region2Move.startY,
					    g_moving_regions.move_obj[i][j].step);
					g_moving_regions.move_obj[i][j].step += 1;
				}
			}

			LOG("update timestamp\r\n");

			for (int j = 0; j < g_timestamp_regions.region_num[i]; j++) {
				LOG("type: %d, idx: %d\r\n", g_timestamp_regions.region_type[i][j],
				    g_timestamp_regions.region_list[i][j]);
				reg_idx = g_timestamp_regions.region_list[i][j];

				include_canvas = g_osd_handle[i]->region[reg_idx].include_canvas;

				OSD_setRegionTransparent(g_osd_handle[i], reg_idx, mode,
				                         (char *)(g_osd_canvas_attr[i][include_canvas].canvas_addr));

				if (g_timestamp_regions.region_type[i][j] == UTF8) {
					ERR("utf8 not support timestamp\r\n");
				} else if (g_timestamp_regions.region_type[i][j] == UNICODE) {
					parseTimestampUnicode(&timestamp_list[0]);
					OSD_getUnicodeSizetoGenerate(&timestamp_list[0], 31,
					                             g_timestamp_regions.ayuv_src_list[i][reg_idx],
					                             &total_ayuv_width, &total_ayuv_height);

					LOG("get total (%d, %d)\r\n", total_ayuv_width, total_ayuv_height);

					char ayuv_buf[total_ayuv_width * total_ayuv_height * 2];
					if (mode == PALETTE_8) {
						OSD_generateUnicodeFromList8bit(
						        &timestamp_list[0], 31,
						        g_timestamp_regions.ayuv_src_list[i][reg_idx], &ayuv_buf[0],
						        total_ayuv_width, total_ayuv_height);
					} else if (mode == AYUV_3544) {
						OSD_generateUnicodeFromList(
						        &timestamp_list[0], 31,
						        g_timestamp_regions.ayuv_src_list[i][reg_idx], &ayuv_buf[0],
						        total_ayuv_width, total_ayuv_height);
					}

					LOG("set timestamp to %d %d\r\n", i, reg_idx);
					OSD_setImageAYUVptr(g_osd_handle[i], reg_idx, &ayuv_buf[0], total_ayuv_width,
					                    total_ayuv_height, mode,
					                    (char *)(g_osd_canvas_attr[i][include_canvas].canvas_addr));
#ifdef DEBUG
					char ayuv_name[32];
					sprintf(&ayuv_name[0], "/mnt/nfs/ethnfs/%d-%d-%d.ayuv", i, reg_idx, cnt);
					saveAYUV(&ayuv_name[0], total_ayuv_width, total_ayuv_height, &ayuv_buf[0],
					         total_ayuv_width * total_ayuv_width * 2);

#endif
				}

				if (MPI_updateOsdCanvas(g_osd_chn_handle[i][include_canvas]) != MPI_SUCCESS) {
					OSDERR("failed to update canvas\r\n");
					MPI_destroyOsdRgn(g_osd_chn_handle[i][include_canvas]);
					gRunflag = 0;
				}
			}
		}

		sleep(1);
		cnt++;
	}

	for (int i = 0; i < channel_num; i++) {
		stopEnc(i);
	}

	sleep(2);

	/*remove all old osd canvas*/
	LOG("remove all OSD\r\n");
	int all_canvas_in_handle = 4;
	for (int i = 0; i < config_num; i++) {
		if (g_osd_handle[i]->osd_num < 4) {
			all_canvas_in_handle = g_osd_handle[i]->osd_num;
		}

		echn_idx.chn = osd_channel[i];
		osd_bind.idx = echn_idx;
		for (int j = 0; j < all_canvas_in_handle; j++) {
			if (MPI_unbindOsdFromChn(g_osd_chn_handle[i][j], &osd_bind) != MPI_SUCCESS) {
				LOG("failed to unbind chn\r\n");
			}

			if (MPI_destroyOsdRgn(g_osd_chn_handle[i][j]) != MPI_SUCCESS) {
				LOG("failed to unbind chn\r\n");
			}
		}
	}

	for (int i = 0; i < config_num; i++) {
		OSD_destroy(g_osd_handle[i]);
	}

	for (int i = 0; i < 3; i++) {
		LOG("chn %d, has timestamp :%d\r\n", i, g_timestamp_regions.region_num[i]);
		for (int j = 0; j < g_timestamp_regions.region_num[i]; j++) {
			reg_idx = g_timestamp_regions.region_list[i][j];
			LOG("at region: %d\r\n", reg_idx);
			OSD_destroyUnicodeFontList(g_timestamp_regions.ayuv_src_list[i][reg_idx]);
		}
	}

deinit:
	OSD_deinit();

end:
	for (int i = 0; i < channel_num; i++) {
		resumeEnc(i);
	}

	MPI_SYS_exit();

	return 0;
}
