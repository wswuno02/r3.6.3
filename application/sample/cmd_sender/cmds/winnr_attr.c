#include "cmdparser.h"

#include <errno.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

#include "mpi_dip_alg.h"

#include "cmd_util.h"

#define NUM_NR_ATTR (58)

static INT32 GET(WinNr)(CMD_DATA_S *opt)
{
	return MPI_getWinNrAttr(opt->win_idx, opt->data);
}

static INT32 SET(WinNr)(const CMD_DATA_S *opt)
{
	return MPI_setWinNrAttr(opt->win_idx, opt->data);
}

static void ARGS(WinNr)(void)
{
	printf("\t'--winnr dev_idx chn_idx win_idx mode nr_auto.y_level_3d[0 ~ MPI_ISO_LUT_ENTRY_NUM-1] nr_auto.c_level_3d[0 ~ MPI_ISO_LUT_ENTRY_NUM-1] nr_auto.y_level_2d[0 ~ MPI_ISO_LUT_ENTRY_NUM-1] nr_auto.c_level_2d[0 ~ MPI_ISO_LUT_ENTRY_NUM-1] \n");
	printf("\t      nr_manual.y_level_3d nr_manual.c_level_3d nr_manual.y_level_2d nr_manual.c_level_2d motion_comp trail_suppress ghost_remove ma_y_strength mc_y_strength ma_c_strength ratio_3d mc_y_level_offset me_frame_fallback_en'\n");
	printf("\t'--winnr 0 0 0 0 65 90 115 140 180 205 230 255 255 255 255 65 90 115 140 180 205 230 255 255 255 255 32 96 120 255 255 255 255 255 255 255 255 32 96 120 255 255 255 255 255 255 255 255'\n");
}

static void HELP(WinNr)(const char *str)
{
	CMD_PRINT_HELP(str, "'--winnr <MPI_WIN> [NR_ATTR]'", "Set NR attributes");
}

static void SHOW(WinNr)(const CMD_DATA_S *opt)
{
	MPI_NR_ATTR_S *attr = opt->data;
	int i;

	printf("device index: %d, channel index: %d, window index: %d\n", opt->win_idx.dev, opt->win_idx.chn,
	       opt->win_idx.win);

	printf("mode=%d\n", attr->mode);
	for (i = 0; i < MPI_ISO_LUT_ENTRY_NUM; ++i) {
		printf("nr_auto.y_level_3d[%d]=%d\n", i, attr->nr_auto.y_level_3d[i]);
	}
	for (i = 0; i < MPI_ISO_LUT_ENTRY_NUM; ++i) {
		printf("nr_auto.c_level_3d[%d]=%d\n", i, attr->nr_auto.c_level_3d[i]);
	}
	for (i = 0; i < MPI_ISO_LUT_ENTRY_NUM; ++i) {
		printf("nr_auto.y_level_2d[%d]=%d\n", i, attr->nr_auto.y_level_2d[i]);
	}
	for (i = 0; i < MPI_ISO_LUT_ENTRY_NUM; ++i) {
		printf("nr_auto.c_level_2d[%d]=%d\n", i, attr->nr_auto.c_level_2d[i]);
	}
	printf("nr_manual.y_level_3d=%d\n", attr->nr_manual.y_level_3d);
	printf("nr_manual.c_level_3d=%d\n", attr->nr_manual.c_level_3d);
	printf("nr_manual.y_level_2d=%d\n", attr->nr_manual.y_level_2d);
	printf("nr_manual.c_level_2d=%d\n", attr->nr_manual.c_level_2d);
	printf("motion_comp=%d\n", attr->motion_comp);
	printf("trail_suppress=%d\n", attr->trail_suppress);
	printf("ghost_remove=%d\n", attr->ghost_remove);
	printf("ma_y_strength=%d\n", attr->ma_y_strength);
	printf("mc_y_strength=%d\n", attr->mc_y_strength);
	printf("ma_c_strength=%d\n", attr->ma_c_strength);
	printf("ratio_3d=%d\n", attr->ratio_3d);
	printf("mc_y_level_offset=%d\n", attr->mc_y_level_offset);
	printf("me_frame_fallback_en=%d\n", attr->me_frame_fallback_en);
}

static int PARSE(WinNr)(int argc, char **argv, CMD_DATA_S *opt)
{
	MPI_NR_ATTR_S *data = opt->data;
	int num = argc - optind;
	int i;

	if (num == (NUM_NR_ATTR + 3)) {
		opt->action = CMD_ACTION_SET;
		opt->win_idx.dev = atoi(argv[optind]);
		optind++;
		opt->win_idx.chn = atoi(argv[optind]);
		optind++;
		opt->win_idx.win = atoi(argv[optind]);
		optind++;

		data->mode = atoi(argv[optind]);
		optind++;

		for (i = 0; i < MPI_ISO_LUT_ENTRY_NUM; ++i) {
			data->nr_auto.y_level_3d[i] = atoi(argv[optind]);
			optind++;
		}
		for (i = 0; i < MPI_ISO_LUT_ENTRY_NUM; ++i) {
			data->nr_auto.c_level_3d[i] = atoi(argv[optind]);
			optind++;
		}
		for (i = 0; i < MPI_ISO_LUT_ENTRY_NUM; ++i) {
			data->nr_auto.y_level_2d[i] = atoi(argv[optind]);
			optind++;
		}
		for (i = 0; i < MPI_ISO_LUT_ENTRY_NUM; ++i) {
			data->nr_auto.c_level_2d[i] = atoi(argv[optind]);
			optind++;
		}
		data->nr_manual.y_level_3d = atoi(argv[optind]);
		optind++;
		data->nr_manual.c_level_3d = atoi(argv[optind]);
		optind++;
		data->nr_manual.y_level_2d = atoi(argv[optind]);
		optind++;
		data->nr_manual.c_level_2d = atoi(argv[optind]);
		optind++;
		data->motion_comp = atoi(argv[optind]);
		optind++;
		data->trail_suppress = atoi(argv[optind]);
		optind++;
		data->ghost_remove = atoi(argv[optind]);
		optind++;
		data->ma_y_strength = atoi(argv[optind]);
		optind++;
		data->mc_y_strength = atoi(argv[optind]);
		optind++;
		data->ma_c_strength = atoi(argv[optind]);
		optind++;
		data->ratio_3d = atoi(argv[optind]);
		optind++;
		data->mc_y_level_offset = atoi(argv[optind]);
		optind++;
		data->me_frame_fallback_en = atoi(argv[optind]);
		optind++;
	} else if (num == 3) {
		opt->action = CMD_ACTION_GET;
		opt->win_idx.dev = atoi(argv[optind]);
		optind++;
		opt->win_idx.chn = atoi(argv[optind]);
		optind++;
		opt->win_idx.win = atoi(argv[optind]);
		optind++;
	} else {
		return -EINVAL;
	}

	return 0;
}

static CMD_S winnr_ops = MAKE_CMD("winnr", MPI_NR_ATTR_S, WinNr);

__attribute__((constructor)) void regWinNrCmd(void)
{
	CMD_register(&winnr_ops);
}
