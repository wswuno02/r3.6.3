#include "cmdparser.h"

#include <errno.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

#include "mpi_dip_alg.h"

#include "cmd_util.h"

#define NUM_DCC_ATTR (8)

static INT32 GET(DccAttr)(CMD_DATA_S *opt)
{
	return MPI_getDccAttr(opt->path_idx, opt->data);
}

static INT32 SET(DccAttr)(const CMD_DATA_S *opt)
{
	return MPI_setDccAttr(opt->path_idx, opt->data);
}

static void ARGS(DccAttr)(void)
{
	printf("\t'--dcc dev_idx path_idx gain0 offset0 gain1 offset1 gain2 offset2 gain3 offset3'\n");
	printf("\t'--dcc 0 0 1092 0 2449 0 1632 0 1092 0'\n");
}

static void HELP(DccAttr)(const char *str)
{
	CMD_PRINT_HELP(str, "'--dcc <MPI_PATH> [DCC_ATTR]'", "Set DCC attributes");
}

static void SHOW(DccAttr)(const CMD_DATA_S *opt)
{
	MPI_DCC_ATTR_S *attr = (MPI_DCC_ATTR_S *)opt->data;

	printf("device index: %d, path index: %d\n", opt->path_idx.dev, opt->path_idx.path);
	printf("gain[0]=%d, offset[0]=%d\n", attr->gain[0], attr->offset_2s[0]);
	printf("gain[1]=%d, offset[1]=%d\n", attr->gain[1], attr->offset_2s[1]);
	printf("gain[2]=%d, offset[2]=%d\n", attr->gain[2], attr->offset_2s[2]);
	printf("gain[3]=%d, offset[3]=%d\n", attr->gain[3], attr->offset_2s[3]);
}

static int PARSE(DccAttr)(int argc, char **argv, CMD_DATA_S *opt)
{
	MPI_DCC_ATTR_S *data = (MPI_DCC_ATTR_S *)opt->data;
	int num = argc - optind;
	int i;

	if (num == (NUM_DCC_ATTR + 2)) {
		opt->action = CMD_ACTION_SET;
		opt->path_idx.dev = atoi(argv[optind]);
		optind++;
		opt->path_idx.path = atoi(argv[optind]);
		optind++;

		for (i = 0; i < MPI_DCC_CHN_NUM; ++i) {
			data->gain[i] = atoi(argv[optind]);
			optind++;
			data->offset_2s[i] = atoi(argv[optind]);
			optind++;
		}
	} else if (num == 2) {
		opt->action = CMD_ACTION_GET;
		opt->path_idx.dev = atoi(argv[optind]);
		optind++;
		opt->path_idx.path = atoi(argv[optind]);
		optind++;
	} else {
		return -EINVAL;
	}

	return 0;
}

static CMD_S dcc_ops = MAKE_CMD("dcc", MPI_DCC_ATTR_S, DccAttr);

__attribute__((constructor)) void regDccCmd(void)
{
	CMD_register(&dcc_ops);
}