#include "cmdparser.h"

#include <errno.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

#include "mpi_dip_alg.h"

#include "cmd_util.h"

#define NUM_CSM_ATTR (15)

static INT32 GET(CsmAttr)(CMD_DATA_S *opt)
{
	return MPI_getCsmAttr(opt->path_idx, opt->data);
}

static INT32 SET(CsmAttr)(const CMD_DATA_S *opt)
{
	return MPI_setCsmAttr(opt->path_idx, opt->data);
}

static void ARGS(CsmAttr)(void)
{
	printf("\t'--csm dev_idx path_idx bw_en hue_angle mode csm_auto.saturation[0 ~ MPI_ISO_LUT_ENTRY_NUM-1] csm_manual.saturation'\n");
	printf("\t'--csm 0 0 0 0 0 160 150 140 128 85 85 85 85 85 85 85 128'\n");
}

static void HELP(CsmAttr)(const char *str)
{
	CMD_PRINT_HELP(str, "'--csm [MPI_PATH] [CSM_ATTR]'", "Set CSM attributes");
}

static void SHOW(CsmAttr)(const CMD_DATA_S *opt)
{
	MPI_CSM_ATTR_S *attr = (MPI_CSM_ATTR_S *)opt->data;
	int i;

	printf("device index: %d, path index: %d\n", opt->path_idx.dev, opt->path_idx.path);
	printf("bw_en=%d\n", attr->bw_en);
	printf("hue_angle=%d\n", attr->hue_angle);
	printf("mode=%d\n", attr->mode);
	for (i = 0; i < MPI_ISO_LUT_ENTRY_NUM; ++i) {
		printf("csm_auto.saturation[%d]=%d\n", i, attr->csm_auto.saturation[i]);
	}
	printf("csm_manual.saturation=%d\n", attr->csm_manual.saturation);
}

static int PARSE(CsmAttr)(int argc, char **argv, CMD_DATA_S *opt)
{
	MPI_CSM_ATTR_S *data = (MPI_CSM_ATTR_S *)opt->data;
	int num = argc - optind;
	int i;

	if (num == (NUM_CSM_ATTR + 2)) {
		opt->action = CMD_ACTION_SET;
		opt->path_idx.dev = atoi(argv[optind]);
		optind++;
		opt->path_idx.path = atoi(argv[optind]);
		optind++;

		data->bw_en = atoi(argv[optind]);
		optind++;
		data->hue_angle = atoi(argv[optind]);
		optind++;
		data->mode = atoi(argv[optind]);
		optind++;

		for (i = 0; i < MPI_ISO_LUT_ENTRY_NUM; ++i) {
			data->csm_auto.saturation[i] = atoi(argv[optind]);
			optind++;
		}

		data->csm_manual.saturation = atoi(argv[optind]);
		optind++;
	} else if (num == 2) {
		opt->action = CMD_ACTION_GET;
		opt->path_idx.dev = atoi(argv[optind]);
		optind++;
		opt->path_idx.path = atoi(argv[optind]);
		optind++;
	} else {
		return -EINVAL;
	}

	return 0;
}

static CMD_S csm_ops = MAKE_CMD("csm", MPI_CSM_ATTR_S, CsmAttr);

__attribute__((constructor)) void regCsmCmd(void)
{
	CMD_register(&csm_ops);
}
