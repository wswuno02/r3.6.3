#include "cmdparser.h"

#include <errno.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

#include "mpi_dip_alg.h"

#include "cmd_util.h"

#define NUM_SIGN_SHP_ATTR (1 + 1 * MPI_ISO_LUT_ENTRY_NUM + 1)

static INT32 GET(ShpAttrV2)(CMD_DATA_S *opt)
{
	return MPI_getShpAttrV2(opt->path_idx, opt->data);
}

static INT32 SET(ShpAttrV2)(const CMD_DATA_S *opt)
{
	return MPI_setShpAttrV2(opt->path_idx, opt->data);
}

static void ARGS(ShpAttrV2)(void)
{
	printf("\t'--shpv2 dev_idx path_idx mode shp_auto_v2.sharpness[0 ~ MPI_ISO_LUT_ENTRY_NUM-1] shp_manual_v2.sharpness'\n");
	printf("\t'--shpv2 0 0 0 255 100 60 0 0 0 0 0 0 0 0 128'\n");
}

static void HELP(ShpAttrV2)(const char *str)
{
	CMD_PRINT_HELP(str, "'--shp <MPI_PATH> [SHP_V2_ATTR]'", "Set SHP_V2 attributes");
}

static void SHOW(ShpAttrV2)(const CMD_DATA_S *opt)
{
	MPI_SHP_ATTR_V2_S *attr = (MPI_SHP_ATTR_V2_S *)opt->data;
	int i;

	printf("device index: %d, path index: %d\n", opt->path_idx.dev, opt->path_idx.path);
	printf("mode=%d\n", attr->mode);
	for (i = 0; i < MPI_ISO_LUT_ENTRY_NUM; ++i) {
		printf("shp_auto_v2.sharpness[%d]=%d\n", i, attr->shp_auto_v2.sharpness[i]);
	}
	printf("shp_manual_v2.sharpness=%d\n", attr->shp_manual_v2.sharpness);
}

static int PARSE(ShpAttrV2)(int argc, char **argv, CMD_DATA_S *opt)
{
	MPI_SHP_ATTR_V2_S *data = (MPI_SHP_ATTR_V2_S *)opt->data;
	int num = argc - optind;
	int i;

	if (num == (NUM_SIGN_SHP_ATTR + 2)) {
		opt->action = CMD_ACTION_SET;
		opt->path_idx.dev = atoi(argv[optind]);
		optind++;
		opt->path_idx.path = atoi(argv[optind]);
		optind++;

		data->mode = atoi(argv[optind]);
		optind++;

		for (i = 0; i < MPI_ISO_LUT_ENTRY_NUM; ++i) {
			data->shp_auto_v2.sharpness[i] = atoi(argv[optind]);
			optind++;
		}

		data->shp_manual_v2.sharpness = atoi(argv[optind]);
		optind++;
	} else if (num == 2) {
		opt->action = CMD_ACTION_GET;
		opt->path_idx.dev = atoi(argv[optind]);
		optind++;
		opt->path_idx.path = atoi(argv[optind]);
		optind++;
	} else {
		return -EINVAL;
	}

	return 0;
}

static CMD_S shpv2_ops = MAKE_CMD("shpv2", MPI_SHP_ATTR_V2_S, ShpAttrV2);

__attribute__((constructor)) void regShpV2Cmd(void)
{
	CMD_register(&shpv2_ops);
}