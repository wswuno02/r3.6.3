#define _GNU_SOURCE
#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <json.h>

#include "mpi_base_types.h"
#include "mpi_dev.h"
#include "mpi_sys.h"
#include "vftr_td.h"

/* include this header to show result on multiplayer */
#include "avftr_conn.h"

#include <td_demo.h>

MPI_WIN g_win_idx;

int g_td_running; /* stop and start td parameter*/

#ifdef CONFIG_APP_TD_SUPPORT_SEI
extern AVFTR_CTX_S *av_ftr_res_shm;
#endif

#ifdef DEBUG
#define DBG(...) printf(__VA_ARGS__)
#else
#define DBG(...)
#endif

static void handleSigInt(int signo)
{
	DBG("[%s]leave %s\n", __func__, __FILE__);

	if (signo == SIGINT) {
		printf("Caught SIGINT!\n");
		g_td_running = 0;
	} else if (signo == SIGTERM) {
		/* The defailt sent by kill and killall */
		printf("Caught SIGTERM!\n");
	} else if (signo == SIGKILL) {
		printf("Caught SIGKILL\n");
	} else if (signo == SIGQUIT) {
		/* Sent when you type CTRL +C */
		printf("Caught SIGQUIT!\n");
	} else {
		perror("Unexpected signal!\n");
	}

	signal(SIGINT, SIG_DFL);
	MPI_IVA_disableObjDet(g_win_idx);
	AVFTR_exitServer();
	MPI_SYS_exit();

	exit(0);
}

int readJsonFromFile(char *file_name, VFTR_TD_PARAM_S *td_attr)
{
	int ret = 0;
	json_object *test_obj = NULL;
	json_object *tmp1_obj = NULL;

	/* load json config from json file */
	test_obj = json_object_from_file(file_name);
	if (!test_obj) {
		fprintf(stderr, "Cannot open %s\n", file_name);
		return -EBADF;
	}

	/* TD parameter */
	json_object_object_get_ex(test_obj, "sensitivity", &tmp1_obj);
	td_attr->sensitivity = json_object_get_int(tmp1_obj);
	if (!tmp1_obj) {
		fprintf(stderr, "Cannot get %s object\n", "sensitivity");
		ret = -ENOCSI;
		goto error;
	}

	json_object_object_get_ex(test_obj, "endurance", &tmp1_obj);
	td_attr->endurance = json_object_get_int(tmp1_obj);
	if (!tmp1_obj) {
		fprintf(stderr, "Cannot get %s object\n", "endurance");
		ret = -ENOCSI;
		goto error;
	}
	DBG("sensitivity: %d\n", td_attr->sensitivity);
	DBG("endurance: %d\n", td_attr->endurance);

	int sen_min_map = 16;
	int sen_max_map = 216;
	int temp = ((td_attr->sensitivity * (sen_max_map - sen_min_map) / 100) + sen_min_map);
	td_attr->sensitivity = (temp > VFTR_TD_SENSITIVITY_MAX) ? VFTR_TD_SENSITIVITY_MAX : temp;

error:
	json_object_put(test_obj);

	return ret;
}

void help(void)
{
	printf("USAGE:\ttd_demo -i <CONFIG>\t\n");
	printf("\t-i <file>\t\ttamper detection config in .json file\n");
	printf("\tsensitivity: The sensitivity of tamper detection, it ranges [1, 255] recommended default value is 60.\n");
	printf("\tendurance: Event remaining time period, it represent how long the time remaining \n");
	printf("\twhen the tamper detection detected an event for alarm trigger.\n");
	printf("\tIt ranges [0, 255], and the recommended default value is 60. (unit is number of frame)\n");
	printf("OPTIONS:\n");
	printf("\t-c <channel>\t\tSpecify which video channel to use. (Deatult 0).\n");
	printf("\n");
	printf("For example:\n");
	printf("\tmpi_stream -d /system/mpp/case_config_1001FHD -precord_enable=1 -poutput_file=/dev/null -pframe_num=-1 &\n");
	printf("\ttd_demo -i /system/mpp/td_config/td_conf_1080p.json &\n");
	printf("\ttestOnDemandRTSPServer 0 -S\n");
	printf("\n");
}

int main(int argc, char **argv)
{
	int ret;
	int c;
	int chn_idx = 0;
	char cfg_file_name[256] = { 0 };

	VFTR_TD_PARAM_S td_attr;

	while ((c = getopt(argc, argv, "i:c:h")) != -1) {
		switch (c) {
		case 'i':
			sprintf(cfg_file_name, optarg);
			DBG("input .json file:%s\n", cfg_file_name);
			break;
		case 'c':
			chn_idx = atoi(optarg);
			DBG("set device channel:%d\n", chn_idx);
			break;
		case 'h':
			help();
			exit(0);
		default:
			abort();
		}
	}

	if (signal(SIGINT, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGINT!\n");
		exit(1);
	}

	if (signal(SIGTERM, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGINT!\n");
		exit(1);
	}

	if (signal(SIGPIPE, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGINT!\n");
		exit(1);
	}

	if (strlen(cfg_file_name) == 0) {
		fprintf(stderr, "Config file path is not specified !\n");
		exit(1);
	}

	ret = readJsonFromFile(cfg_file_name, &td_attr);
	if (ret != MPI_SUCCESS) {
		DBG("Read Json form file:%s\n", cfg_file_name);
		return -EINVAL;
	}

	MPI_CHN_ATTR_S chn_attr;
	MPI_CHN_LAYOUT_S layout_attr;
	MPI_CHN chn = MPI_VIDEO_CHN(0, chn_idx);

	/* Initialize MPI system */
	MPI_SYS_init();

	ret = MPI_DEV_getChnAttr(chn, &chn_attr);
	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "Failed to MPI_DEV_getChnAttr.\n");
		return -EINVAL;
	}

	ret = MPI_DEV_getChnLayout(chn, &layout_attr);
	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "Failed to MPI_DEV_getlayoutAttr.\n");
		return -EINVAL;
	}

	/* NOTICE: we only support the case that only one window in a channel */
	assert(layout_attr.window_num == 1);
	assert(layout_attr.window[0].x == 0);
	assert(layout_attr.window[0].y == 0);
	assert(layout_attr.window[0].width == chn_attr.res.width);
	assert(layout_attr.window[0].height == chn_attr.res.height);

	/* Check parameters */
	ret = VFTR_TD_checkParam(&td_attr);
	if (ret) {
		fprintf(stderr, "Invalid TD parameters\n");
		return -EINVAL;
	}

#ifdef CONFIG_APP_TD_SUPPORT_SEI
	/* init AV server to wait RTSP client connectd
	 * allow transfer IVA result to RTSP streaming server */
	/* NOTE: this step only for showing result from RTSP server
	 * related code is not needed
	 */
	ret = AVFTR_initServer();
	if (ret) {
		fprintf(stderr, "Failed to initalize AV server %d\n", ret);
		return -ENOPROTOOPT;
	}

	/* Reset shared memory to zeros */
	memset(av_ftr_res_shm, 0, AVFTR_RES_SIZE);
#endif

	UINT32 dev_idx = 0;
	UINT32 win_idx = 0;
	MPI_WIN g_win_idx = MPI_VIDEO_WIN(dev_idx, chn.chn, win_idx);

	/* DetectTdObject() */
	ret = detectTdObject(g_win_idx, &layout_attr, &td_attr);

	/* init server */
	/* NOTE: this step only for showing result from RTSP server
	 * related code is not needed
	 */

#ifdef CONFIG_APP_TD_SUPPORT_SEI
	AVFTR_exitServer();
#endif
	MPI_SYS_exit();
	/* disableod */
	return 0;
}
