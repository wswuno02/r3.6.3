#include <errno.h>
#include <unistd.h>
#include <stdio.h>

#include "mpi_base_types.h"
#include "mpi_dev.h"
#include "mpi_iva.h"
#include "vftr_td.h"
#include "avftr_conn.h"

#include "td_demo.h"

#define _DEBUG

#ifdef _DEBUG
#define DBG(...) printf(__VA_ARGS__)
#else
#define DBG(...)
#endif

#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define MIN(a, b) (((a) < (b)) ? (a) : (b))

extern int g_td_running;
TD_CTX_S g_td_ctx;

#ifdef CONFIG_APP_TD_SUPPORT_SEI
extern AVFTR_CTX_S *av_ftr_res_shm;
extern AVFTR_VIDEO_CTX_S *vftr_res_shm;
extern AVFTR_AUDIO_CTX_S *aftr_res_shm;

static int updateBufferIdx(AVFTR_VIDEO_BUF_INFO_S *buf_info, UINT32 timestamp)
{
	buf_info->buf_cur_idx = ((buf_info->buf_cur_idx + 1) % AVFTR_VIDEO_RING_BUF_SIZE);
	buf_info->buf_ready[buf_info->buf_cur_idx] = 0;
	buf_info->buf_time[buf_info->buf_cur_idx] = timestamp;
	buf_info->buf_cur_time = timestamp;
	return buf_info->buf_cur_idx;
}
#endif

static int updateMpiInfo(MPI_WIN win_idx, TD_CTX_S *ctx)
{
	int ret = 0;

	ret = MPI_getStatistics(ctx->path, &ctx->mpi_input.dip_stat);
	if (ret != 0) {
		DBG("Get DIP stat on configuration of window %u\n", win_idx.value);
		return ret;
	}
	return 0;
}

int detectTdObject(MPI_WIN win_idx, MPI_CHN_LAYOUT_S *chn_layout, VFTR_TD_PARAM_S *td_param)
{
	UINT32 timestamp = 0;
	INT32 timeout = 0;
	INT32 ret = 0;
	int g_td_running = 1;
	MPI_RECT_S ini_roi;
	MPI_WIN_ATTR_S win_attr;
	MPI_WIN wait_win = MPI_VIDEO_WIN(win_idx.dev, win_idx.chn, 0);
	TD_CTX_S ctx;

	/* Check win state */
	ret = MPI_DEV_getWindowAttr(win_idx, &win_attr);
	if (ret != 0) {
		DBG("Get window %u attributes failed.\n", win_idx.value);
		return ret;
	}

	/* register path */
	UINT32 dev_idx = MPI_GET_VIDEO_DEV(win_idx);
	int path_idx = 0;
	ctx.path = MPI_INPUT_PATH(dev_idx, path_idx);

#ifdef CONFIG_APP_TD_SUPPORT_SEI
	AVFTR_TD_CTX_S *td_ctx = vftr_res_shm->td_ctx;
	td_ctx->en = 1;
	td_ctx->reg = 1;
	td_ctx->idx = win_idx;
	td_ctx->cb = NULL;
#endif
	/* create TD instance */
	VFTR_TD_INSTANCE_S *td;
	td = VFTR_TD_newInstance();
	if (!td) {
		fprintf(stderr, "Failed to create TD instance\n");
		return -EINVAL;
	}

	/* get input resolution & luma roi for TD */
	MPI_ROI_ATTR_S roi_attr;
	MPI_PATH_ATTR_S path_attr;

	ret = MPI_DEV_getPathAttr(ctx.path, &path_attr);
	if (ret != 0) {
		DBG("Get path attr on configuration of window %u\n", win_idx.value);
		return ret;
	}
	ret = MPI_getRoiAttr(ctx.path, &roi_attr);
	if (ret != 0) {
		DBG("Get roi attr on configuration of window %u\n", win_idx.value);
		return ret;
	}

	ini_roi.width = (roi_attr.luma_roi.ex - roi_attr.luma_roi.sx) * path_attr.res.width / 1024;
	ini_roi.height = (roi_attr.luma_roi.ey - roi_attr.luma_roi.sy) * path_attr.res.height / 1024;

	ret = VFTR_TD_init(td, &ini_roi, (int)win_attr.fps);
	if (ret != 0) {
		DBG("Init TD failed !\n");
	}

	/* set parameter to td */
	ret = VFTR_TD_setParam(td, td_param);
	if (ret) {
		fprintf(stderr, "Failed to TD parameters.\n");
		return -EINVAL;
	}

	while (g_td_running) {
		/* wait one video frame processed */
		if (MPI_DEV_waitWin(wait_win, &timestamp, timeout) != MPI_SUCCESS) {
			fprintf(stderr, "Wait ISP statistics fail for win:0x%x\n", wait_win.value);
			continue;
		}
#ifdef CONFIG_APP_TD_SUPPORT_SEI
		/* Find cur buffer idx */
		AVFTR_VIDEO_BUF_INFO_S *buf_info = &vftr_res_shm->buf_info[0];
		int buf_idx = updateBufferIdx(buf_info, timestamp);
#endif
		/* Update MPI info */
		ret = updateMpiInfo(win_idx, &ctx);
		if (ret != 0) {
			DBG("Update Mpi information failed on win %u\n", win_idx.value);
			return ret;
		}

		VFTR_TD_STATUS_S *td_res = NULL;

#ifdef CONFIG_APP_TD_SUPPORT_SEI
		td_res = &td_ctx->td_res[buf_idx];
#else
		VFTR_TD_STATUS_S status;
		td_res = &status;
#endif
		/**
		 * tamper detection
		 */
		ret = VFTR_TD_detect(td, &ctx.mpi_input, td_res);
		if (ret != MPI_SUCCESS) {
			fprintf(stderr, "Failed to TD_detect.\n");
			continue;
		}
		if (td_res->alarm == (1 << VFTR_TD_ALARM_BLOCK)) {
			fprintf(stderr, "[Warning] Camera tampered!!\n");
		}

		/* mark buffer ready for display */
#ifdef CONFIG_APP_TD_SUPPORT_SEI
		buf_info->buf_ready[buf_idx] = 1;
#endif
	}

	/* delete TD instance */
	VFTR_TD_deleteInstance(&td);

	return 0;
}
