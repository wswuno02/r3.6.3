#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <getopt.h>
#include <signal.h>
#include <unistd.h>
#include <sys/io.h>

#include "mpi_base_types.h"
#include "mpi_sys.h"
#include "mpi_dip_types.h"
#include "mpi_dip_alg.h"
#include "ir_control.h"

#define ENABLE_PWM
#ifdef ENABLE_PWM
#include "pwm.h"
#endif

#include "light.h"
#include "gpio.h"

#define AT_IRDBG(format, args...) fprintf(stderr, "[%s:%d] " format, "AT IrControl extra", __LINE__, ##args)

int main(int argc, char **argv)
{
	MPI_SYS_init();
	int scene_luma = calcMpiSceneLuma(0, 0);
	AT_IRDBG("scene luam: %d\r\n", scene_luma);
	MPI_SYS_exit();

	return 0;
}
