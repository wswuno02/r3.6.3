#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include "pcm_interfaces.h"
#include "anr.h"

void help()
{
	printf("Usage: asample [OPTIONS]\n");
	printf("\n");
	printf("Options:\n");
	printf("-e <#>  | enable                  [0,1] Def:1\n");
	printf("-a <#>  | auto_noise_mag          [0,1] Def:0\n");
	printf("-w <#>  | window_size_opt         [0-2] Def:2\n");
	printf("-t <#>  | noise_mag_thd           [0-1023] Def:600\n");
	printf("-L <#>  | large_mag_gain          [1-7] Def:1\n");
	printf("-r <#>  | noise_level_rto         [1,2,4,8] Def:4\n");
	printf("-g <#>  | global_min_avg_iir_wei  [0-16] Def:15\n");
	printf("-G <#>  | global_avg_to_nthd_rate [0-31] Def:9\n");
	printf("-n <#>  | noise_mag_auto_thd_base [0-2047] Def:790\n");
	printf("-m <#>  | nr_min_level            [0-256] Def:50\n");
	printf("-M <#>  | nr_max_level            [0-256] Def:230\n");
	printf("-D <#>  | gaindecade_en           [0,1] Def:1\n");
	printf("-d <#>  | gain_decrease           [20-2000] Def:500\n");
	printf("-i <#>  | gain_increase           [20-200] Def:30\n");
}

void print_params(AnrParams params)
{
	printf("auto_noise_mag = %d\n", params.auto_noise_mag);
	printf("window_size_opt = %d\n", params.window_size_opt);
	printf("noise_mag_thd = %d\n", params.noise_mag_thd);
	printf("large_mag_gain = %d\n", params.large_mag_gain);
	printf("noise_level_rto = %d\n", params.noise_level_rto);
	printf("global_min_avg_iir_wei = %d\n", params.global_min_avg_iir_wei);
	printf("global_avg_to_nthd_rate = %d\n", params.global_avg_to_nthd_rate);
	printf("noise_mag_auto_thd_base = %d\n", params.noise_mag_auto_thd_base);
	printf("nr_min_level = %d\n", params.nr_min_level);
	printf("nr_max_level = %d\n", params.nr_max_level);
	printf("gaindecade_en = %d\n", params.gaindecade_en);
	printf("gain_decrease = %d\n", params.gain_decrease);
	printf("gain_increase = %d\n", params.gain_increase);
}

int main(int argc, char **argv)
{
	int c;
	int en = 0;
	int r_en = 0;
	AnrParams params;
	AnrParams r_params;

	if (anr_get_params(&r_params) < 0) {
		printf("%d: get_params failed\n", __LINE__);
		return -1;
	}
	params = r_params;

	while ((c = getopt(argc, argv, "e:a:w:t:L:r:g:G:n:m:M:D:d:i:h")) != -1) {
		switch (c) {
		case 'e':
			en = atoi(optarg);
			anr_en(en);
			anr_get_en(&r_en);
			printf("en = %d, r_en = %d\n", en, r_en);
			break;
		case 'a':
			params.auto_noise_mag = atoi(optarg);
			break;
		case 'w':
			params.window_size_opt = atoi(optarg);
			break;
		case 't':
			params.noise_mag_thd = atoi(optarg);
			break;
		case 'L':
			params.large_mag_gain = atoi(optarg);
			break;
		case 'r':
			params.noise_level_rto = atoi(optarg);
			break;
		case 'g':
			params.global_min_avg_iir_wei = atoi(optarg);
			break;
		case 'G':
			params.global_avg_to_nthd_rate = atoi(optarg);
			break;
		case 'n':
			params.noise_mag_auto_thd_base = atoi(optarg);
			break;
		case 'm':
			params.nr_min_level = atoi(optarg);
			break;
		case 'M':
			params.nr_max_level = atoi(optarg);
			break;
		case 'D':
			params.gaindecade_en = atoi(optarg);
			break;
		case 'd':
			params.gain_decrease = atoi(optarg);
			break;
		case 'i':
			params.gain_increase = atoi(optarg);
			break;
		case 'h':
			help();
			return 0;
		default:
			help();
			return -1;
			break;
		}
	}

	printf("=====================\nOrigin params:\n");
	print_params(r_params);

	if (anr_set_params(params) < 0) {
		printf("%d: set_params failed\n", __LINE__);
		return -1;
	}
	if (anr_get_params(&r_params) < 0) {
		printf("%d: get_params failed\n", __LINE__);
		return -1;
	}
	printf("\n=====================\nUpdated params:\n");
	print_params(r_params);

	return 0;
}
