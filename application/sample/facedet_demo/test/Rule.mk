TEST_SRCS=../test/main.c
TEST_BIN=../test/main

TEST_ENV=$(HOME)/local/libs/linux
TFLITE_INC=-I$(TEST_ENV)/tflite/inc
TFLITE_LIBS=-L$(TEST_ENV)/tflite/lib -ltensorflow-lite -Wl,-rpath=$(TEST_ENV)/tflite/lib
INF_LIBS=-L$(LIBINF_PATH)/lib -linf -Wl,-rpath=$(LIBINF_PATH)/lib
TEST_INC=\
-I$(LIBINF_INC) \
-I$(MPP_INC) \
$(TFLITE_INC)

CFLAGS=-Wall -g
LDFLAGS=\
$(TFLITE_LIBS) \
$(INF_LIBS) \
-lstdc++ -lm -pthread -ldl -lrt

test: $(TEST_BIN)

.PHONY: test-clean
test-clean: 
	$(RM) -rf $(TEST_BIN)

.PHONY: test

$(TEST_BIN): CC=gcc

$(TEST_BIN): $(TEST_SRCS)
	$(Q)$(CC) -o $@ $^ $(CFLAGS) $(TEST_INC) -Wl,--start-group $(LDFLAGS) -Wl,--end-group


