#!/bin/sh

###### Configurations #######

# times to collect raw images
SNAPSHOT_CNT=5

# either do the jpeg snapshot by using mpi_snapshot
DO_JPEG_SNAPSHOT=1

# either need to dump the registers when DIP is turned off
DUMP_DISABLE_CSR=0

# if some Augentix's programs are not put under $PATH (e.g. /system/bin/)
# please provide the location of those programs
# programs/scripts required: cmdsender, csr, ddr2pgm, dump_csr, mpi_snapshot
# dump_csr also need to be modified if "dump" program is not in $PATH
PROGRAM_PATH=""

#### Program redirection ####

if [ "$PROGRAM_PATH" ]; then
	CMDSENDER="${PROGRAM_PATH}/cmdsender"
	CSR="${PROGRAM_PATH}/csr"
	DDR2PGM="${PROGRAM_PATH}/ddr2pgm"
	MPI_SNAPSHOT="${PROGRAM_PATH}/mpi_snapshot"
	DUMP_CSR="${PROGRAM_PATH}/dump_csr"
else
	CMDSENDER="cmdsender"
	CSR="csr"
	DDR2PGM="ddr2pgm"
	MPI_SNAPSHOT="mpi_snapshot"
	DUMP_CSR="dump_csr"
fi

RET=0

if [ "$(command -v ${CMDSENDER})" -a -f "$(command -v ${CMDSENDER})" ]; then :
else
	echo "Warn: Cannot find cmdsender program!"
	RET=1
fi

if [ "$(command -v ${CSR})" -a -f "$(command -v ${CSR})" ]; then :
else
	echo "Warn: Cannot find csr program!"
	RET=1
fi

if [ "$(command -v ${DDR2PGM})" -a -f "$(command -v ${DDR2PGM})" ]; then :
else
	echo "Warn: Cannot find ddr2pgm program!"
	RET=1
fi

if [ "$(command -v ${MPI_SNAPSHOT})" -a -f "$(command -v ${MPI_SNAPSHOT})" ]; then :
else
	echo "Warn: Cannot find mpi_snapshot program!"
	RET=1
fi

if [ "$(command -v ${DUMP_CSR})" -a -f "$(command -v ${DUMP_CSR})" ]; then :
else
	echo "Warn: Cannot find dump_csr program!"
	RET=1
fi

if [ $RET = "1" ]; then
	exit
fi

### Current CTRL settings ###

CTRL_DEFAULT=$("${CMDSENDER}" --dip 0 0 | tail -n +2 | tr -cd '[:digit:] ' )
CTRL_TRAIL=$(echo -n "$CTRL_DEFAULT" | cut -c 3- )
CTRL_TRAIL_DISABLE=$(echo -n "$CTRL_TRAIL" | tr '[:digit:]' '0' )
CAL_DEFAULT=$("${CMDSENDER}" --cal 0 0 | tail -n +2 | tr '\n' ' ' | tr -cd '[:digit:] ' )
CAL_TRAIL=$(echo -n "$CAL_DEFAULT" | cut -c 3- )
CAL_TRAIL_DISABLE=$(echo -n "$CAL_TRAIL" | tr '[:digit:]' '0' )

##### Snapshot settings #####

BAYER=$("${CSR}" bsp0.bsp.bayer_ini_phase_o | cut -c 34-34 )
EDP_MODE=$("${CSR}" edp0.edp.mode | cut -c 21-21 )

#############################

TIMESTAMP=$(date +%Y-%m-%d.%H-%M-%S )

if [ $# -eq 0 ]; then
	echo "Usage: $0 <path> [pattern]"
	echo "path - a subdirectory will be created under this path for collecting the data"
	echo "pattern - name of the pattern, will use current time of the machine if not specified"
	# echo "pattern id - pattern id for C model"
	echo "Eg. $0 /mnt/nfs/ethnfs/data laboratory"
	echo "    $0 /mnt/sdcard/data"
	exit 1;
fi

STORE_PATH="$1"

if [ "$2" ]; then
	PATTERN_NAME="$2"
else
	PATTERN_NAME="data-${TIMESTAMP}"
fi

DATA_PATH="${STORE_PATH}/${PATTERN_NAME}"

if [ -f "$DATA_PATH" -o -d "$DATA_PATH" ]; then
	echo "Path \"$DATA_PATH\" exists."
	echo "Please specify another path or pattern name,"
	echo "or remove the existing file/directory and try again."
	exit
fi

mkdir -p "$DATA_PATH"

DIP_INFO="${DATA_PATH}/dip_info.log"

# Get jpeg snapshots
# notice that jpeg can't present the color correctly due to its color space
if [ "$DO_JPEG_SNAPSHOT" = 1 ]; then
	echo "Collecting jpeg snapshots..."
	"${MPI_SNAPSHOT}" jpeg 1 0 "${DATA_PATH}/current_jpeg.jpg" > /dev/null 2>&1
	"${MPI_SNAPSHOT}" jpeg2 1 0 80 "${DATA_PATH}/current_jpeg2.jpg" > /dev/null 2>&1
fi

# remove augentix.log in order to save dip info log
rm -f /tmp/augentix.log*

# dump dip info
echo "Collecting DIP information..."
echo "${PATTERN_NAME}" > "$DIP_INFO"
echo >> "$DIP_INFO"

echo -----[Export]----- >> "$DIP_INFO"
export >> "$DIP_INFO"

echo >> "$DIP_INFO"

echo -----[Library info]----- >> "$DIP_INFO"
if [ -z "$(command -v file)" ]; then
	echo "Cannot find \"file\" program." >> "$DIP_INFO"
else
	LIBMPP_PATH=$(ls -l /system/lib/libmpp.so.? | tr -s ' ' '\n' | tail -n 1 )
	case "$LIBMPP_PATH" in
		/*)
		file "$LIBMPP_PATH" >> "$DIP_INFO" 2>&1
		;;
		*)
		file "/system/lib/${LIBMPP_PATH}" >> "$DIP_INFO" 2>&1
		;;
	esac
fi

echo >> "$DIP_INFO"

echo -----[Collect lum1 stat]----- >> "$DIP_INFO"
rm -f /tmp/augentix.log*
"${CMDSENDER}" --dip 0 0 4 $CTRL_TRAIL >> /dev/null
sleep 1
"${CMDSENDER}" --dip 0 0 1 $CTRL_TRAIL >> /dev/null
if [ -f /tmp/augentix.log.0 ]; then
	cat /tmp/augentix.log.0 /tmp/augentix.log | tail -n 100 >> "$DIP_INFO"
elif [ -f /tmp/augentix.log ]; then
	tail -n 100 /tmp/augentix.log >> "$DIP_INFO"
else
	echo "Cannot fetch lum1 statistics from syslog." >> "$DIP_INFO"
fi

echo >> "$DIP_INFO"

echo -----[Collect awb stat]----- >> "$DIP_INFO"
rm -f /tmp/augentix.log*
"${CMDSENDER}" --dip 0 0 5 $CTRL_TRAIL >> /dev/null
sleep 1
"${CMDSENDER}" --dip 0 0 1 $CTRL_TRAIL >> /dev/null
if [ -f /tmp/augentix.log.0 ]; then
	cat /tmp/augentix.log.0 /tmp/augentix.log | tail -n 100 >> "$DIP_INFO"
elif [ -f /tmp/augentix.log ]; then
	tail -n 100 /tmp/augentix.log >> "$DIP_INFO"
else
	echo "Cannot fetch awb statistics from syslog." >> "$DIP_INFO"
fi

echo >> "$DIP_INFO"

echo -----[Collect share info]----- >> "$DIP_INFO"
rm -f /tmp/augentix.log*
"${CMDSENDER}" --dip 0 0 8 $CTRL_TRAIL >> /dev/null
sleep 1
"${CMDSENDER}" --dip 0 0 1 $CTRL_TRAIL >> /dev/null
if [ -f /tmp/augentix.log.0 ]; then
	cat /tmp/augentix.log.0 /tmp/augentix.log | tail -n 100 >> "$DIP_INFO"
elif [ -f /tmp/augentix.log ]; then
	tail -n 100 /tmp/augentix.log >> "$DIP_INFO"
else
	echo "Cannot fetch shared information from syslog." >> "DIP_INFO"
fi

echo >> "$DIP_INFO"

echo -----[EXP]----- >> "$DIP_INFO"
"${CMDSENDER}" --exp 0 0 > "${DATA_PATH}/exp.info" 2> /dev/null || \
echo "Failed to fetch EXP information." > "${DATA_PATH}/exp.info"
cat "${DATA_PATH}/exp.info" >> "$DIP_INFO"

echo -----[WB]----- >> "$DIP_INFO"
"${CMDSENDER}" --wb 0 0 >> "$DIP_INFO" 2> /dev/null || \
echo "Failed to fetch WB information." >> "$DIP_INFO"

echo -----[CTRL]----- >> "$DIP_INFO"
"${CMDSENDER}" --dip 0 0 >> "$DIP_INFO" 2> /dev/null || \
echo "Failed to fetch CTRL information." >> "$DIP_INFO"

echo -----[CAL]----- >> "$DIP_INFO"
"${CMDSENDER}" --cal 0 0 >> "$DIP_INFO" 2> /dev/null || \
echo "Failed to fetch CAL information." >> "$DIP_INFO"

echo -----[DBC]----- >> "$DIP_INFO"
"${CMDSENDER}" --dbc 0 0 >> "$DIP_INFO" 2> /dev/null || \
echo "Failed to fetch DBC information." >> "$DIP_INFO"

echo -----[DCC]----- >> "$DIP_INFO"
"${CMDSENDER}" --dcc 0 0 >> "$DIP_INFO" 2> /dev/null || \
echo "Failed to fetch DCC information." >> "$DIP_INFO"

echo -----[LSC]----- >> "$DIP_INFO"
"${CMDSENDER}" --lsc 0 0 >> "$DIP_INFO" 2> /dev/null || \
echo "Failed to fetch LSC information." >> "$DIP_INFO"

echo -----[ROI]----- >> "$DIP_INFO"
"${CMDSENDER}" --roi 0 0 >> "$DIP_INFO" 2> /dev/null || \
echo "Failed to fetch ROI information." >> "$DIP_INFO"

echo -----[STAT]----- >> "$DIP_INFO"
"${CMDSENDER}" --stat 0 0 >> "$DIP_INFO" 2> /dev/null || \
echo "Failed to fetch STAT information." >> "$DIP_INFO"

echo -----[AE]----- >> "$DIP_INFO"
"${CMDSENDER}" --ae 0 0 >> "$DIP_INFO" 2> /dev/null || \
echo "Failed to fetch AE information." >> "$DIP_INFO"

echo -----[AWB]----- >> "$DIP_INFO"
"${CMDSENDER}" --awb 0 0 >> "$DIP_INFO" 2> /dev/null || \
echo "Failed to fetch AWB information." >> "$DIP_INFO"

echo -----[PTA]----- >> "$DIP_INFO"
"${CMDSENDER}" --pta 0 0 >> "$DIP_INFO" 2> /dev/null || \
echo "Failed to fetch PTA information." >> "$DIP_INFO"

echo -----[CSM]----- >> "$DIP_INFO"
"${CMDSENDER}" --csm 0 0 >> "$DIP_INFO" 2> /dev/null || \
echo "Failed to fetch CSM information." >> "$DIP_INFO"

echo -----[SHP]----- >> "$DIP_INFO"
"${CMDSENDER}" --shp 0 0 >> "$DIP_INFO" 2> /dev/null || \
echo "Failed to fetch SHP information." >> "$DIP_INFO"

echo -----[SHP_V2]----- >> "$DIP_INFO"
"${CMDSENDER}" --shpv2 0 0 >> "$DIP_INFO" 2> /dev/null || \
echo "Failed to fetch SHP_V2 information." >> "$DIP_INFO"

echo -----[NR]----- >> "$DIP_INFO"
"${CMDSENDER}" --nr 0 0 >> "$DIP_INFO" 2> /dev/null || \
echo "Failed to fetch NR information." >> "$DIP_INFO"

echo -----[CORING]----- >> "$DIP_INFO"
"${CMDSENDER}" --coring 0 0 >> "$DIP_INFO" 2> /dev/null || \
echo "Failed to fetch CORING information." >> "$DIP_INFO"

echo -----[GAMMA]----- >> "$DIP_INFO"
"${CMDSENDER}" --gamma 0 0 >> "$DIP_INFO" 2> /dev/null || \
echo "Failed to fetch GAMMA information." >> "$DIP_INFO"

echo -----[TE]----- >> "$DIP_INFO"
"${CMDSENDER}" --te 0 0 >> "$DIP_INFO" 2> /dev/null || \
echo "Failed to fetch TE information." >> "$DIP_INFO"

echo -----[ISO]----- >> "$DIP_INFO"
"${CMDSENDER}" --iso 0 0 >> "$DIP_INFO" 2> /dev/null || \
echo "Failed to fetch ISO information." >> "$DIP_INFO"

echo -----[ENH]----- >> "$DIP_INFO"
"${CMDSENDER}" --enh 0 0 >> "$DIP_INFO" 2> /dev/null || \
echo "Failed to fetch ENH information." >> "$DIP_INFO"

echo -----[HDR_SYNTH]----- >> "$DIP_INFO"
"${CMDSENDER}" --hdr_synth 0 0 >> "$DIP_INFO" 2> /dev/null || \
echo "Failed to fetch HDR_SYNTH information." >> "$DIP_INFO"

echo -----[FCS]----- >> "$DIP_INFO"
"${CMDSENDER}" --fcs 0 0 >> "$DIP_INFO" 2> /dev/null || \
echo "Failed to fetch FCS information." >> "$DIP_INFO"

echo -----[VENC]----- >> "$DIP_INFO"
"${CMDSENDER}" --venc --enc_idx=0 --get_vattr >> "$DIP_INFO" 2> /dev/null || \
"${CMDSENDER}" --rc 0 >> "$DIP_INFO" 2> /dev/null || \
echo "Failed to fetch VENC information" >> "$DIP_INFO"

echo -----[QP]----- >> "$DIP_INFO"
for i in $(seq 0 4); do
	sleep 1
	"${CSR}" venc.venc.qp_1 >> "$DIP_INFO" 2>&1
done

# remove augentix.log in order to save dip cfg log
rm -f /tmp/augentix.log*

echo "Collecting DIP configurations..."
"${CMDSENDER}" --dip 0 0 9 $CTRL_TRAIL >> /dev/null
sleep 1
"${CMDSENDER}" --dip 0 0 1 $CTRL_TRAIL >> /dev/null

# save dip cfg log
if [ -f /tmp/augentix.log.0 ]; then
	cat /tmp/augentix.log.0 /tmp/augentix.log > "${DATA_PATH}/dip_cfg.log"
elif [ -f /tmp/augentix.log ]; then
	cp /tmp/augentix.log "${DATA_PATH}/dip_cfg.log"
else
	echo "Cannot fetch DIP configurations from syslog." > "${DATA_PATH}/dip_cfg.log"
fi

# dump csr
echo "Dumping system registers..."
mkdir -p $DATA_PATH/current_csr/bin
"${DUMP_CSR}" $DATA_PATH/current_csr/bin/

# dump pgm
echo "Dumping raw images, please do not quit this script until it's finished."

# bypass dip and fetch IS out
"${CMDSENDER}" --dip 0 0 2 $CTRL_TRAIL >> /dev/null

PGM_DIR="$DATA_PATH/004.000.000.${PATTERN_NAME}_isw"
mkdir -p "$PGM_DIR"
for i in $(seq 0 $((${SNAPSHOT_CNT} - 1)) ); do
	# dump raw based on non-hdr/hdr
	if [ "$EDP_MODE" -eq 0 ]; then
		PGM_NAME=$(printf "%08d_bayer.pgm" ${i} )
		BIN_NAME=$(printf "%08d_bayer.bin" ${i} )
		"${DDR2PGM}" -z -p${BAYER} ISW-WP0 "$PGM_DIR/$PGM_NAME" "$PGM_DIR/$BIN_NAME" >> /dev/null
	else
		PGM_SE_NAME=$(printf "%08d_bayer_se.pgm" ${i} )
		BIN_SE_NAME=$(printf "%08d_bayer_se.bin" ${i} )
		PGM_LE_NAME=$(printf "%08d_bayer_le.pgm" ${i} )
		BIN_LE_NAME=$(printf "%08d_bayer_le.bin" ${i} )
		"${DDR2PGM}" -z -p${BAYER} ISW-WP0 "$PGM_DIR/$PGM_SE_NAME" "$PGM_DIR/$BIN_SE_NAME" >> /dev/null
		"${DDR2PGM}" -z -p${BAYER} ISW-WP1 "$PGM_DIR/$PGM_LE_NAME" "$PGM_DIR/$BIN_LE_NAME" >> /dev/null
	fi
done

# disable every module and fetch sensor raw
"${CMDSENDER}" --cal 0 0 1 $CAL_TRAIL_DISABLE >> /dev/null
"${CMDSENDER}" --dip 0 0 1 $CTRL_TRAIL_DISABLE >> /dev/null

sleep 1

PGM_DIR="$DATA_PATH/004.000.000.${PATTERN_NAME}_raw"
mkdir -p "$PGM_DIR"
for i in $(seq 0 $((${SNAPSHOT_CNT} - 1)) ); do
	# dump raw based on non-hdr/hdr, TBD: stitch?
	if [ "$EDP_MODE" -eq 0 ]; then
		PGM_NAME=$(printf "%08d_bayer.pgm" ${i} )
		BIN_NAME=$(printf "%08d_bayer.bin" ${i} )
		"${DDR2PGM}" -z -p${BAYER} ISW-WP0 "$PGM_DIR/$PGM_NAME" "$PGM_DIR/$BIN_NAME" >> /dev/null
	else
		PGM_SE_NAME=$(printf "%08d_bayer_se.pgm" ${i} )
		BIN_SE_NAME=$(printf "%08d_bayer_se.bin" ${i} )
		PGM_LE_NAME=$(printf "%08d_bayer_le.pgm" ${i} )
		BIN_LE_NAME=$(printf "%08d_bayer_le.bin" ${i} )
		"${DDR2PGM}" -z -p${BAYER} ISW-WP0 "$PGM_DIR/$PGM_SE_NAME" "$PGM_DIR/$BIN_SE_NAME" >> /dev/null
		"${DDR2PGM}" -z -p${BAYER} ISW-WP1 "$PGM_DIR/$PGM_LE_NAME" "$PGM_DIR/$BIN_LE_NAME" >> /dev/null
	fi
done

# dump disabled csr
if [ "$DUMP_DISABLE_CSR" -eq 1 ]; then
	echo "Dumping system registers with DIP disabled..."
	mkdir -p "${DATA_PATH}/disable_csr/bin"
	"${DUMP_CSR}" "${DATA_PATH}/disable_csr/bin/"
fi

# enable cal and dip module after dump RAW
"${CMDSENDER}" --cal 0 0 1 $CAL_TRAIL >> /dev/null
"${CMDSENDER}" --dip 0 0 1 $CTRL_TRAIL >> /dev/null

# copy ini
if [ -f /system/mpp/script/sensor_0.ini ]; then
	cp /system/mpp/script/sensor_0.ini "${DATA_PATH}/sensor_0.ini"
fi
if [ -f /system/mpp/script/sensor_1.ini ]; then
	cp /system/mpp/script/sensor_1.ini "${DATA_PATH}/sensor_1.ini"
fi

rm -f /tmp/augentix.log*

echo "Has finished collecting data to $DATA_PATH/"
