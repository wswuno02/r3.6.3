#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <pthread.h>
#include <errno.h>

#include "sample_sys.h"
#include "parse.h"

typedef int (*SampleFunction)(SAMPLE_CONF_S *const conf);

// TODO: should these global variable be removed?
SAMPLE_CONF_S g_conf = { { 0 } };
MPI_DEV g_dev_idx;

int SAMPLE_normalStream(SAMPLE_CONF_S *const conf);

SampleFunction sample_list[] = {
	NULL, /* 0 */
	SAMPLE_normalStream, /* 1 */
};

static void handleSigInt(int signo)
{
	if (signo == SIGINT) {
		printf("Caught SIGINT!\n");
	} else if (signo == SIGTERM) {
		printf("Caught SIGTERM!\n");
	} else {
		perror("Unexpected signal!\n");
		exit(1);
	}

	/* Remove pthread_join, which is not signal-safe function */
	switch (g_conf.casegen.sid) {
	case SAMPLE_CASE_NORMAL_STREAM:
		for (int idx = 0; idx < g_conf.enc_chn_cnt; idx++) {
			g_bsb_run[idx] = 0;
		}

		break;
	default:
		break;
	}
}

int SAMPLE_normalStream(SAMPLE_CONF_S *const conf)
{
	UINT32 stream_idx = 0;
	INT32 ret = MPI_FAILURE;
	INT32 frame_num_n1 = 0;
	int flag = 0;

	ret = SAMPLE_startStream(g_dev_idx, conf);
	if (ret != MPI_SUCCESS) {
		return 0;
	}

	SAMPLE_initGetStream();
	/* Be noted: Encode chn is one-by-one mapped to stream in mpi_stream */
	for (stream_idx = 0; stream_idx < conf->enc_chn_cnt; ++stream_idx) {
		printf("[SAMPLE] %s: start get stream from channel %d\n", __func__, stream_idx);
		ret = SAMPLE_startGetStream(&conf->enc_chn[stream_idx]);
		frame_num_n1 |= (conf->enc_chn[stream_idx].casep.frame_num == -1);

		printf("[SAMPLE] %s: start Encode channel %d\n", __func__, stream_idx);
		ret = SAMPLE_startEncChn(stream_idx, &conf->enc_chn[stream_idx]);
		if (ret != MPI_SUCCESS) {
			return 0;
		}
	}

	do {
		usleep(300000);
		flag = 0;

		for (stream_idx = 0; stream_idx < conf->enc_chn_cnt; ++stream_idx) {
			flag |= g_bsb_run[stream_idx];
		}

	} while (flag);

	for (stream_idx = 0; stream_idx < conf->enc_chn_cnt; ++stream_idx) {
		SAMPLE_stopGetStream(&conf->enc_chn[stream_idx]);
		SAMPLE_stopEncChn(stream_idx, &conf->enc_chn[stream_idx]);
	}

	SAMPLE_exitGetStream();

	SAMPLE_stopStream(g_dev_idx, conf);

	return 0;
}

int main(int argc, char **argv)
{
	g_dev_idx = MPI_VIDEO_DEV(0);

	init_conf(&g_conf);

	if (!parse(argc, argv, &g_conf)) {
		return -1;
	}

	if (g_conf.casegen.show_params) {
		show_result(&g_conf);
	}

	if (signal(SIGINT, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGINT!\n");
		exit(1);
	}

	printf("running sample_list[%d](*g_conf)\n", g_conf.casegen.sid);

	sample_list[g_conf.casegen.sid](&g_conf);

	printf("\n");

	return 0;
}
