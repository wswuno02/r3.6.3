#ifdef SAMPLE_AVFTR_ENABLE
#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "ini.h"

#include "mpi_base_types.h"
#include "mpi_index.h"
#include "mpi_errno.h"

#include "agtx_prio.h"

#include "avftr.h"
#include "avftr_conn.h"

#include "avftr_aroi.h"
#include "avftr_dk.h"
#include "avftr_eaif.h"
#include "avftr_ef.h"
#include "avftr_fld.h"
#include "avftr_md.h"
#include "avftr_td.h"

#include "video_bm.h"
#include "avftr_ld.h"
#include "video_od.h"
#include "avftr_pd.h"
#include "video_ptz.h"
#include "video_rms.h"
#include "avftr_shd.h"

#include "vftr_aroi.h"
#include "vftr_md.h"
#include "sample_iva.h"
#include "sample_sys.h"

#define SAMPLE_IVA_PARAM(TYPE, NAME)                                   \
	typedef struct {                                               \
		int en;                                                \
		int update;                                            \
		union {                                                \
			int sensitivity; /* For param conversion */    \
			int register_scene; /* For param conversion */ \
		};                                                     \
		TYPE param;                                            \
	} SAMPLE_##NAME##_PARAM_S

SAMPLE_IVA_PARAM(AVFTR_AROI_PARAM_S, AROI);
SAMPLE_IVA_PARAM(VFTR_EF_PARAM_S, EF);
SAMPLE_IVA_PARAM(AVFTR_EAIF_PARAM_S, EAIF);
SAMPLE_IVA_PARAM(AVFTR_DK_PARAM_S, DK);
SAMPLE_IVA_PARAM(AVFTR_FLD_PARAM_S, FLD);
SAMPLE_IVA_PARAM(AVFTR_MD_PARAM_S, MD);
SAMPLE_IVA_PARAM(AVFTR_TD_PARAM_S, TD);

SAMPLE_IVA_PARAM(VIDEO_FTR_OD_PARAM_S, OD);
SAMPLE_IVA_PARAM(VIDEO_FTR_PTZ_PARAM_S, PTZ);
SAMPLE_IVA_PARAM(VIDEO_FTR_RMS_PARAM_S, RMS);

SAMPLE_IVA_PARAM(AVFTR_PD_PARAM_S, PD);
SAMPLE_IVA_PARAM(VFTR_LD_PARAM_S, LD);
SAMPLE_IVA_PARAM(AVFTR_SHD_PARAM_S, SHD);

/* Define the config struct type */
typedef struct {
	int enable_cnt;
	MPI_WIN IDX;
	SAMPLE_AROI_PARAM_S AROI;
	SAMPLE_EF_PARAM_S EF;
	SAMPLE_PD_PARAM_S PD;
	SAMPLE_EAIF_PARAM_S EAIF;
	SAMPLE_DK_PARAM_S DK;
	SAMPLE_FLD_PARAM_S FLD;
	SAMPLE_MD_PARAM_S MD;
	SAMPLE_TD_PARAM_S TD;
	SAMPLE_SHD_PARAM_S SHD;

	SAMPLE_LD_PARAM_S LD;
	SAMPLE_OD_PARAM_S OD;
	SAMPLE_RMS_PARAM_S RMS;
	SAMPLE_PTZ_PARAM_S PTZ;

	// after param conversion
	SAMPLE_OD_PARAM_S OD_RE;
	SAMPLE_PD_PARAM_S PD_RE;
	SAMPLE_MD_PARAM_S MD_RE;
	SAMPLE_TD_PARAM_S TD_RE;
	SAMPLE_AROI_PARAM_S AROI_RE;
	SAMPLE_EF_PARAM_S EF_RE;
	SAMPLE_DK_PARAM_S DK_RE;
	SAMPLE_LD_PARAM_S LD_RE;
	SAMPLE_PTZ_PARAM_S PTZ_RE;
} SAMPLE_IVA_S;

typedef struct {
	int size;
	SAMPLE_IVA_S iva[SAMPLE_IVA_RUNNER_MAX];
} SAMPLE_IVA_CONF_S;

typedef struct {
	MPI_WIN idx;
	MPI_SIZE_S res;
} AROI_CB_INFO;

static AROI_CB_INFO g_aroi_cb_info = { { { 0 } } };
static AROI_CB_INFO g_aroi_cb_info_pre = { { { 0 } } };

// config file
static SAMPLE_IVA_CONF_S g_iva_conf = {};
static const char *g_iva_fname = NULL;
static struct stat g_iva_time = {};
static int g_iva_conf_idx = 0;
static int g_b_new_idx_sess = 0;

// pthread info
static int g_iva_ini_run = 0;
static char *g_tid_vftr_name;
static pthread_t g_tid_vftr;

// utils
static int checkWinStat(MPI_WIN idx);

// config
static int handler(void *user, const char *section, const char *name, const char *value);
static int checkIniFileStat(const char *filename);
static int parseIvaIniFile(const char *filename, SAMPLE_IVA_CONF_S *iva_conf);

// libavftr
static int getWinInfo(const MPI_WIN idx, MPI_SIZE_S *res, float *fps);
static int registerIva(SAMPLE_IVA_S *vftr);
static int unregisterIva(SAMPLE_IVA_S *vftr);
static void dumpConfig(const SAMPLE_IVA_S *cfg);
static void aroiVideoCbEmpty(MPI_WIN idx, const VFTR_AROI_STATUS_S *status, const VIDEO_FTR_OBJ_LIST_S *obj_list);
static void aroiVideoCb(MPI_WIN idx, const VFTR_AROI_STATUS_S *status, const VIDEO_FTR_OBJ_LIST_S *obj_list);

// parameter convert and validate
static int convertMdParam(const MPI_SIZE_S *size, const SAMPLE_MD_PARAM_S *src, SAMPLE_MD_PARAM_S *dst);
static int convertAROIParam(const MPI_SIZE_S *size, const SAMPLE_AROI_PARAM_S *src, SAMPLE_AROI_PARAM_S *dst);
static int convertDKParam(const MPI_SIZE_S *size, const SAMPLE_DK_PARAM_S *src, SAMPLE_DK_PARAM_S *dst);
static int convertPTZParam(const SAMPLE_PTZ_PARAM_S *src, SAMPLE_PTZ_PARAM_S *dst);
static int convertODParam(MPI_WIN idx, uint8_t fpu, const SAMPLE_OD_PARAM_S *src, SAMPLE_OD_PARAM_S *dst);
static int convertPDParam(MPI_WIN idx, const MPI_SIZE_S *size, const SAMPLE_PD_PARAM_S *src, SAMPLE_PD_PARAM_S *dst);
static int convertEFParam(const MPI_SIZE_S *size, const SAMPLE_EF_PARAM_S *src, SAMPLE_EF_PARAM_S *dst);
static int convertLDParam(const MPI_SIZE_S *size, const SAMPLE_LD_PARAM_S *src, SAMPLE_LD_PARAM_S *dst);
static int convertTDParam(const SAMPLE_TD_PARAM_S *src, SAMPLE_TD_PARAM_S *dst);
static int convertParam(SAMPLE_IVA_CONF_S *cfg);

static void *updateVftrThread(void *args);

static int checkWinStat(MPI_WIN idx)
{
	MPI_CHN_LAYOUT_S layout_attr;
	MPI_CHN_STAT_S chn_stat;
	MPI_CHN chn = { .value = idx.value };
	int i = 0;

	int ret = MPI_DEV_queryChnState(chn, &chn_stat);
	if (ret != MPI_SUCCESS) {
		return ret;
	}

	if (!MPI_STATE_IS_ADDED(chn_stat.status)) {
		return MPI_ERR_NOT_EXIST;
	}

	if (MPI_DEV_getChnLayout(chn, &layout_attr) != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	/* FIXME: check window state */
	for (i = 0; i < layout_attr.window_num; i++) {
		if (idx.value == layout_attr.win_id[i].value) {
			break;
		}
	}

	if (i == layout_attr.window_num) {
		return MPI_FAILURE;
	}
	return 0; // success
}

static int handler(void *user, const char *section, const char *name, const char *value)
{
	static int i = 0;
	SAMPLE_IVA_CONF_S *iva_conf = (SAMPLE_IVA_CONF_S *)user;
	SAMPLE_IVA_S *cfg = &iva_conf->iva[g_iva_conf_idx];

	if (strcmp(section, "IDX") == 0 && strcmp(name, "dev") == 0) {
		iva_conf->size++;
		if (g_b_new_idx_sess == 0)
			g_b_new_idx_sess = 1;
		else {
			g_iva_conf_idx++;
			cfg += 1;
		} // cfg array increment
	}

	//if (strcmp(name, "en") == 0) i = 0; // refresh array index

	if (0)
		;
	else if (strcmp(name, "i") == 0)
		i = atoi(value); // array index
#define CFG(s, n, default) \
	else if (strcmp(section, #s) == 0 && strcmp(name, #n) == 0) cfg->s.n = (__typeof__(cfg->s.n))atof(value);
#include "iva_config.def"

	return 1;
}

static int checkIniFileStat(const char *filename)
{
	struct stat buf;

	/* Get File Statistics for stat.c. */
	if (stat(filename, &buf) != 0) {
		perror("Problem getting information");
		return -1;
	} else { /* Print the date/time last modified */
		if (buf.st_mtime != g_iva_time.st_mtime) {
			g_iva_time.st_mtime = buf.st_mtime;
			return 1;
		}
	}

	return 0;
}

static int parseIvaIniFile(const char *filename, SAMPLE_IVA_CONF_S *iva_conf)
{
	g_iva_conf_idx = 0;
	g_b_new_idx_sess = 0;
	if (ini_parse(filename, handler, iva_conf) < 0) {
		printf("!!!!!!!! Can't load '%s', using defaults !!!!!!!!!!\n", filename);
		return -1;
	} else {
		printf("!!!!!!!! Total size of IVA window num = %d !!!!!!!! \n", iva_conf->size);
		dumpConfig(&iva_conf->iva[0]);
	}
	return 0;
}

static void dumpConfig(const SAMPLE_IVA_S *cfg)
{
	//	int i = 0;
	//#define CFG(s, n, default) fprintf(stderr, "%s_%s = %d\n", #s, #n, (int)cfg->s.n);
	//#include "iva_config.def"
}

static void aroiVideoCbEmpty(MPI_WIN idx, const VFTR_AROI_STATUS_S *status, const VIDEO_FTR_OBJ_LIST_S *obj_list)
{
	return;
}

static void aroiVideoCb(MPI_WIN idx, const VFTR_AROI_STATUS_S *status, const VIDEO_FTR_OBJ_LIST_S *obj_list)
{
	const MPI_RECT_POINT_S *p = &status->roi;
	MPI_RECT_POINT_S roi_tmp = { 0 };
	MPI_SIZE_S *res = &g_aroi_cb_info.res;

	roi_tmp.sx = MAX(p->sx * 1024 / MAX(res->width, 1), 0);
	roi_tmp.sy = MAX(p->sy * 1024 / MAX(res->height, 1), 0);
	roi_tmp.ex = MIN(MAX(p->ex * 1024 / MAX(res->width, 1), 0), 1023);
	roi_tmp.ey = MIN(MAX(p->ey * 1024 / MAX(res->height, 1), 0), 1023);
	VIDEO_FTR_updateAutoPtz(&roi_tmp);
	return;
}

static int getWinInfo(const MPI_WIN idx, MPI_SIZE_S *res, float *fps)
{
	INT32 ret;
	INT32 i;
	UINT32 dev_idx = MPI_GET_VIDEO_DEV(idx);
	UINT32 chn_idx = MPI_GET_VIDEO_CHN(idx);
	UINT32 win_idx = MPI_GET_VIDEO_WIN(idx);
	MPI_CHN_STAT_S chn_stat;
	MPI_CHN chn = { .dev = idx.dev, .chn = idx.chn };

	ret = MPI_DEV_queryChnState(chn, &chn_stat);
	if (ret != 0) {
		printf("Query channel state on channel %d on device %d failed\n", chn_idx, dev_idx);
		return -1;
	}

	if (!MPI_STATE_IS_ADDED(chn_stat.status)) {
		printf("Channel %d on device %d is not added\n", chn_idx, dev_idx);
		return -1;
	}

	MPI_CHN_LAYOUT_S layout_attr;

	ret = MPI_DEV_getChnLayout(chn, &layout_attr);
	if (ret != 0) {
		printf("Get video channel %d layout attributes failed.\n", chn_idx);
		return -1;
	}

	/* FIXME: check window state */
	for (i = 0; i < layout_attr.window_num; i++) {
		if (idx.value == layout_attr.win_id[i].value) {
			res->width = layout_attr.window[i].width;
			res->height = layout_attr.window[i].height;
			break;
		}
	}
	if (i == layout_attr.window_num) {
		printf("Invalid video window index %d from video channel %d", win_idx, chn_idx);
		return -1;
	}

	MPI_WIN_ATTR_S win_attr;

	ret = MPI_DEV_getWindowAttr(idx, &win_attr);
	if (ret != 0) {
		printf("Get window index dev:%d chn:%d win:%d fps fail\n", idx.dev, idx.chn, idx.win);
		return -1;
	}

	*fps = win_attr.fps;
	return 0;
}

static void rescaleSize(const MPI_SIZE_S *win_size, const MPI_SIZE_S *src, MPI_SIZE_S *dst)
{
	dst->width = (src->width * win_size->width + 50) / 100;
	dst->height = (src->height * win_size->height + 50) / 100;
}

static void rescaleRect(const MPI_SIZE_S *win_size, const MPI_RECT_POINT_S *src, MPI_RECT_POINT_S *dst)
{
	dst->sx = src->sx * (win_size->width - 1) / 100;
	dst->sy = src->sy * (win_size->height - 1) / 100;
	dst->ex = src->ex * (win_size->width - 1) / 100;
	dst->ey = src->ey * (win_size->height - 1) / 100;
}

static int convertMdParam(const MPI_SIZE_S *size, const SAMPLE_MD_PARAM_S *src, SAMPLE_MD_PARAM_S *dst)
{
#define MD_ENERGY_TH_V_REF_RATIO (15) /* FIXME: Tuning the featurelib or formula */
#define MD_ENERGY_TH_V_REF_RATIO_MAX (255)

	*dst = *src;
	const VFTR_MD_PARAM_S *md = &src->param.md_param;
	VFTR_MD_PARAM_S *md_re = &dst->param.md_param;
	for (int j = 0; j < md->region_num; j++) {
		rescaleRect(size, &md->attr[j].pts, &md_re->attr[j].pts);
		if (md->attr[j].md_mode == VFTR_MD_MOVING_AREA) {
			md_re->attr[j].thr_v_reg =
			        ((size->width * size->height / 100) * (100 - md->attr[j].thr_v_reg)) / 1000;
		} else { //VFTR_MD_MODE_MOVING_ENERGY
			md_re->attr[j].thr_v_reg =
			        (((((size->width * size->height) / 100) * md->attr[j].thr_v_obj_max) /
			          MD_ENERGY_TH_V_REF_RATIO) *
			         (100 - md->attr[j].thr_v_reg)) /
			        1000;
		}
	}
	int ret = VFTR_MD_checkParam(md_re, size);
	return ret;
}

static int convertAROIParam(const MPI_SIZE_S *size, const SAMPLE_AROI_PARAM_S *src, SAMPLE_AROI_PARAM_S *dst)
{
	*dst = *src;
	const VFTR_AROI_PARAM_S *aroi = &src->param.aroi_param;
	VFTR_AROI_PARAM_S *aroi_re = &dst->param.aroi_param;
	rescaleSize(size, &aroi->min_roi, &aroi_re->min_roi);
	rescaleSize(size, &aroi->max_roi, &aroi_re->max_roi);

	int ret = VFTR_AROI_checkParam(aroi_re, size);
	return ret;
}

static int convertDKParam(const MPI_SIZE_S *size, const SAMPLE_DK_PARAM_S *src, SAMPLE_DK_PARAM_S *dst)
{
	*dst = *src;
	const VFTR_DK_PARAM_S *dk = &src->param.dk_param;
	VFTR_DK_PARAM_S *dk_re = &dst->param.dk_param;
	rescaleRect(size, &dk->roi_pts, &dk_re->roi_pts);

	int ret = VFTR_DK_checkParam(dk_re);
	return ret;
}

static int convertPTZParam(const SAMPLE_PTZ_PARAM_S *src, SAMPLE_PTZ_PARAM_S *dst)
{
	*dst = *src;
	VIDEO_FTR_PTZ_PARAM_S *ptz = &dst->param;
	int ret = 0;
	for (int i = 0; i < ptz->win_num; i++) {
		ptz->win_id[i] = MPI_VIDEO_WIN(ptz->win_id[i].dev, ptz->win_id[i].chn, ptz->win_id[i].win);
		ret |= checkWinStat(ptz->win_id[i]);
	}
	return ret;
}

static int convertODParam(MPI_WIN idx, uint8_t fpu, const SAMPLE_OD_PARAM_S *src, SAMPLE_OD_PARAM_S *dst)
{
#define VFTR_OD_MAX_DETECTION_SEC 3
#define VFTR_OD_MIN_DETECTION_SEC 0
#define VFTR_OD_DETECTION_SECOND 0.9 //Recommend Default
#define VFTR_OD_DETECTION_PERCENT 70 //Recommend Default

#define VFTR_OD_MAX_TRACK_REFINE_SEC 2
#define VFTR_OD_MIN_TRACK_REFINE_SEC 0
#define VFTR_OD_TRACK_REFINE_SECOND 0.267 //Recommend Default
#define VFTR_OD_TRACK_REFINE_PERCENT 86 //Recommend Default
	*dst = *src;
	const MPI_IVA_OD_PARAM_S *od = &src->param.od_param;
	MPI_IVA_OD_PARAM_S *od_re = &dst->param.od_param;
	int od_qual_t = MPI_IVA_OD_MAX_QUA + 1 -
	                (fpu * (100 - od->od_qual) * (VFTR_OD_MAX_DETECTION_SEC - VFTR_OD_MIN_DETECTION_SEC)) / 100;

	if (od_qual_t < MPI_IVA_OD_MIN_QUA) {
		od_qual_t = MPI_IVA_OD_MIN_QUA;
	} else if (od_qual_t > MPI_IVA_OD_MAX_QUA) {
		od_qual_t = MPI_IVA_OD_MAX_QUA;
	}

	int od_track_refine_t =
	        MPI_IVA_OD_MAX_TRACK_REFINE + 1 -
	        (fpu * (100 - od->od_track_refine) * (VFTR_OD_MAX_TRACK_REFINE_SEC - VFTR_OD_MIN_TRACK_REFINE_SEC)) /
	                100;

	if (od_track_refine_t < MPI_IVA_OD_MIN_TRACK_REFINE) {
		od_track_refine_t = MPI_IVA_OD_MIN_TRACK_REFINE;
	} else if (od_track_refine_t > MPI_IVA_OD_MAX_TRACK_REFINE) {
		od_track_refine_t = MPI_IVA_OD_MAX_TRACK_REFINE;
	}

	od_re->od_qual = od_qual_t;
	od_re->od_track_refine = od_track_refine_t;
	od_re->od_size_th = od->od_size_th * MPI_IVA_OD_MAX_OBJ_SIZE / 100;
	od_re->od_sen = od->od_sen * MPI_IVA_OD_MAX_SEN / 100;
	// // FIXME use check param function
	int ret = MPI_IVA_setObjParam(idx, od_re);
	if (ret == MPI_ERR_DEV_INVALID_PARAM) {
		printf("Check IVA param failed!\n");
		return -1;
	}
	return 0;
}

static int convertPDParam(MPI_WIN idx, const MPI_SIZE_S *size, const SAMPLE_PD_PARAM_S *src, SAMPLE_PD_PARAM_S *dst)
{
	*dst = *src;
	int length = (size->width > size->height) ? size->height : size->width;
	const AVFTR_PD_PARAM_S *pd = &src->param;
	AVFTR_PD_PARAM_S *pd_re = &dst->param;

	pd_re->min_sz.width = pd->min_sz.width * length / 100;
	pd_re->min_sz.height = pd->min_sz.height * length / 100;
	pd_re->max_sz.width = pd->max_sz.width * length / 100;
	pd_re->max_sz.height = pd->max_sz.height * length / 100;
	pd_re->obj_life_th = pd->obj_life_th;
	// FIXME use check param function
	int ret = AVFTR_PD_setParam(idx, pd_re);
	if (ret) {
		printf("Check PD param failed!\n");
	}
	return ret;
}

static int convertEFParam(const MPI_SIZE_S *size, const SAMPLE_EF_PARAM_S *src, SAMPLE_EF_PARAM_S *dst)
{
	*dst = *src;
	const VFTR_EF_PARAM_S *ef = &src->param;
	VFTR_EF_PARAM_S *ef_re = &dst->param;
	int width = size->width;
	int height = size->height;
	int i = 0;
	for (i = 0; i < ef->fence_num; i++) {
		/*Convert from percent to actually point*/
		ef_re->attr[i].line.sx = MIN(MAX((width - 1) * ef->attr[i].line.sx / 100, 0), width - 1);
		ef_re->attr[i].line.sy = MIN(MAX((height - 1) * ef->attr[i].line.sy / 100, 0), height - 1);
		ef_re->attr[i].line.ex = MIN(MAX((width - 1) * ef->attr[i].line.ex / 100, 0), width - 1);
		ef_re->attr[i].line.ey = MIN(MAX((height - 1) * ef->attr[i].line.ey / 100, 0), height - 1);
		ef_re->attr[i].obj_size_min.width = width * ef->attr[i].obj_size_min.width / 100;
		ef_re->attr[i].obj_size_min.height = height * ef->attr[i].obj_size_min.height / 100;
		ef_re->attr[i].obj_size_max.width = width * ef->attr[i].obj_size_max.width / 100;
		ef_re->attr[i].obj_size_max.height = height * ef->attr[i].obj_size_max.height / 100;
		ef_re->attr[i].obj_area = ef->attr[i].obj_area * width * height / 100;
		ef_re->attr[i].obj_v_th = ef->attr[i].obj_v_th * VFTR_EF_MAX_THR_V_OBJ / 100;
		ef_re->attr[i].obj_life_th = ef->attr[i].obj_life_th;
		ef_re->attr[i].mode = ef->attr[i].mode;
	}
	int ret = VFTR_EF_checkParam(ef_re, size);
	return ret;
}

static int convertLDParam(const MPI_SIZE_S *size, const SAMPLE_LD_PARAM_S *src, SAMPLE_LD_PARAM_S *dst)
{
	*dst = *src;
	const VFTR_LD_PARAM_S *ld = &src->param;
	VFTR_LD_PARAM_S *ld_re = &dst->param;
	int width = size->width;
	int height = size->height;
	int sensitivity = src->sensitivity;
	/* sen_th             : min=12 mid=62 max=112 */
	/* alarm_latency      : fixed=5               */
	/* alarm_latency_cycle: min=1  mid=3  max=5   */
	/* det_period         : min=2  mid=6  max=10  */

	ld_re->sen_th = 112 - sensitivity;

	ld_re->alarm_supr = 7 - ((sensitivity + 10) / 20);
	ld_re->alarm_latency = 5;
	ld_re->alarm_latency_cycle = ((sensitivity + 12) / 25) + 1;
	ld_re->det_period = (10 - (sensitivity * 8 / 100));
	ld_re->trig_cond = ld->trig_cond;

	ld_re->roi.sx = ld->roi.sx * (width - 1) / 100;
	ld_re->roi.sy = ld->roi.sy * (height - 1) / 100;
	ld_re->roi.ex = ld->roi.ex * (width - 1) / 100;
	ld_re->roi.ey = ld->roi.ey * (height - 1) / 100;
	// FIXME use check param function
	return 0;
}

static int convertTDParam(const SAMPLE_TD_PARAM_S *src, SAMPLE_TD_PARAM_S *dst)
{
	*dst = *src;
	VFTR_TD_PARAM_S *td_re = &dst->param.td_param;

	/*Mapping to useful sensitivity range*/
	int sen_min_map = 16;
	int sen_max_map = 216;
	int temp = ((td_re->sensitivity * (sen_max_map - sen_min_map) / 100) + sen_min_map);
	td_re->sensitivity = (temp > VFTR_TD_SENSITIVITY_MAX) ? VFTR_TD_SENSITIVITY_MAX : temp;
	int ret = VFTR_TD_checkParam(td_re);
	return ret;
}

static int convertParam(SAMPLE_IVA_CONF_S *cfg)
{
	MPI_SIZE_S size = {};
	float fps = 20.0;
	int ret = 0;
	int fail = 0;

	for (int i = 0; i < cfg->size; i++) {
		SAMPLE_IVA_S *iva = &cfg->iva[i];
		iva->IDX = MPI_VIDEO_WIN(iva->IDX.dev, iva->IDX.chn, iva->IDX.win);
		getWinInfo(iva->IDX, &size, &fps);
		uint8_t ufps = fps;

		if (iva->MD.en) {
			ret = convertMdParam(&size, &iva->MD, &iva->MD_RE);
			if (ret) {
				iva->MD.en = iva->MD_RE.en = 0;
				fail |= 1;
			}
		}

		if (iva->AROI.en) {
			ret = convertAROIParam(&size, &iva->AROI, &iva->AROI_RE);
			if (!ret) {
				g_aroi_cb_info_pre.idx = iva->IDX;
				g_aroi_cb_info_pre.res = size;
			} else {
				iva->AROI.en = iva->AROI_RE.en = 0;
				fail |= 1;
			}
		}

		if (iva->DK.en) {
			ret = convertDKParam(&size, &iva->DK, &iva->DK_RE);
			if (ret) {
				iva->DK.en = iva->DK_RE.en = 0;
				fail |= 1;
			}
		}

		if (iva->PTZ.en) {
			ret = convertPTZParam(&iva->PTZ, &iva->PTZ_RE);
			if (ret) {
				iva->PTZ.en = iva->PTZ_RE.en = 0;
				fail |= 1;
			}
		}

		if (iva->OD.en) {
			ret = convertODParam(iva->IDX, ufps, &iva->OD, &iva->OD_RE);
			if (ret) {
				iva->OD.en = iva->OD_RE.en = 0;
				fail |= 1;
			}
		}

		if (iva->PD.en) {
			ret = convertPDParam(iva->IDX, &size, &iva->PD, &iva->PD_RE);
			if (ret) {
				iva->PD.en = iva->PD_RE.en = 0;
				fail |= 1;
			}
		}

		if (iva->EF.en) {
			ret = convertEFParam(&size, &iva->EF, &iva->EF_RE);
			if (ret) {
				iva->EF.en = iva->EF_RE.en = 0;
				fail |= 1;
			}
		}

		if (iva->TD.en) {
			ret = convertTDParam(&iva->TD, &iva->TD_RE);
			if (ret) {
				iva->TD.en = iva->TD_RE.en = 0;
				fail |= 1;
			}
		}

		if (iva->LD.en) {
			ret = convertLDParam(&size, &iva->LD, &iva->LD_RE);
			if (ret) {
				iva->LD.en = iva->LD_RE.en = 0;
				fail |= 1;
			}
		}

		if (iva->FLD.en) {
			ret = VFTR_FLD_checkParam(&iva->FLD.param.fld_param);
			if (ret) {
				iva->FLD.en = 0;
				fail |= 1;
			}
		}
	}
	return fail;
}

#if 0
void print_md_param(const AVFTR_MD_PARAM_S *mmd)
{
    AVFTR_MD_PARAM_S md = *mmd;
#define PRINTP(x) printf("%s = %d\n", #x, (int)x)
    PRINTP(md.en_skip_shake);
    PRINTP(md.en_skip_pd);
    PRINTP(md.duration);
    PRINTP(md.md_param.region_num);
    PRINTP(md.md_param.attr[0].id);
    PRINTP(md.md_param.attr[0].pts.sx);
    PRINTP(md.md_param.attr[0].pts.sy);
    PRINTP(md.md_param.attr[0].pts.ex);
    PRINTP(md.md_param.attr[0].pts.ey);
    PRINTP(md.md_param.attr[0].det_method);
    PRINTP(md.md_param.attr[0].md_mode);
    PRINTP(md.md_param.attr[0].thr_v_obj_min);
    PRINTP(md.md_param.attr[0].thr_v_obj_max);
    PRINTP(md.md_param.attr[0].obj_life_th);
    PRINTP(md.md_param.attr[0].thr_v_reg);
}

void print_avftr_md(void)
{
    printf("\n\n!!!!!!!!!!!!!!!! print avftr md !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
    AVFTR_MD_PARAM_S md;
    AVFTR_MD_getParam(MPI_VIDEO_WIN(0,0,0), &md);
    print_md_param(&md);
}

void print_local_md(void)
{
    printf("\n\n!!!!!!!!!!!!!!!! print local md !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
    AVFTR_MD_PARAM_S *md = &g_iva_conf.iva[0].MD_RE.param;
    print_md_param(md);
}

void print_ef_param(const VFTR_EF_PARAM_S *eef)
{
    VFTR_EF_PARAM_S ef = *eef;
#define PRINTP(x) printf("%s = %d\n", #x, (int)x)
    PRINTP(ef.fence_num);
    PRINTP(ef.attr[0].id);
    PRINTP(ef.attr[0].line.sx);
    PRINTP(ef.attr[0].line.sy);
    PRINTP(ef.attr[0].line.ex);
    PRINTP(ef.attr[0].line.ey);
    PRINTP(ef.attr[0].obj_size_min.width);
    PRINTP(ef.attr[0].obj_size_min.height);
    PRINTP(ef.attr[0].obj_size_max.width);
    PRINTP(ef.attr[0].obj_size_max.height);
    PRINTP(ef.attr[0].obj_area);
    PRINTP(ef.attr[0].obj_life_th);
    PRINTP(ef.attr[0].mode);
}

void print_avftr_ef(void)
{
    printf("\n\n!!!!!!!!!!!!!!!! print avftr ef !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
    VFTR_EF_PARAM_S ef;
    AVFTR_EF_getParam(MPI_VIDEO_WIN(0,0,0), &ef);
    print_ef_param(&ef);
}

void print_local_ef(void)
{
    printf("\n\n!!!!!!!!!!!!!!!! print local ef !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
    VFTR_EF_PARAM_S *ef = &g_iva_conf.iva[0].EF_RE.param;
    print_ef_param(ef);
}
#endif

void SAMPLE_loadIvaConf(const char *filename)
{
	g_iva_fname = filename;
	SAMPLE_IVA_CONF_S *iva_conf = &g_iva_conf;

	if (!parseIvaIniFile(filename, iva_conf)) {
		convertParam(iva_conf);
		checkIniFileStat(filename);
		printf("Updated by ini %s.\n", filename);
	}
}

void SAMPLE_initAVFTRServ(void)
{
	if (AVFTR_initServer() < 0) {
		printf("Init AVFTR server failed\n");
		return;
	}
	return;
}

void SAMPLE_exitAVFTRServ(void)
{
	if (AVFTR_exitServer() < 0) {
		printf("Exit AVFTR server failed\n");
		return;
	}
	return;
}

#define AVFTR_ADD_OBJ(addobj_func, set_param, enable_func, pvftr, idx) \
	{                                                              \
		if (pvftr.en) {                                        \
			ret = addobj_func(idx);                        \
			if (ret != 0)                                  \
				return ret;                            \
			ret = set_param(idx, &pvftr.param);            \
			if (ret != 0)                                  \
				return ret;                            \
			ret = enable_func(idx);                        \
			if (ret != 0)                                  \
				return ret;                            \
			printf("%s success!\n", #enable_func);         \
			sum += 1;                                      \
		}                                                      \
	}

#define AVFTR_DEL_OBJ(delobj_func, disable_func, pvftr, idx) \
	{                                                    \
		if (pvftr.en) {                              \
			ret = disable_func(idx);             \
			if (ret != 0)                        \
				return ret;                  \
			ret = delobj_func(idx);              \
			if (ret != 0)                        \
				return ret;                  \
			pvftr.en = 0;                        \
			remove += 1;                         \
		}                                            \
	}

#define AVFTR_UPDATE_OBJ(addobj_func, set_param, enable_func, delobj_func, disable_func, update_func, p_old, p_new, \
                         idx)                                                                                       \
	{                                                                                                           \
		if (p_old.en != p_new.en) {                                                                         \
			if (p_new.en == 0)                                                                          \
				AVFTR_DEL_OBJ(delobj_func, disable_func, p_old, idx);                               \
			else                                                                                        \
				AVFTR_ADD_OBJ(addobj_func, set_param, enable_func, p_new, idx);                     \
		} else {                                                                                            \
			ret = update_func(idx, set_param, p_old, p_new);                                            \
			if (ret != 0)                                                                               \
				return ret;                                                                         \
		}                                                                                                   \
	}

static inline int fake_add_obj(MPI_WIN idx)
{
	return 0;
}

static inline int fake_del_obj(MPI_WIN idx)
{
	return 0;
}

static int SAMPLE_TD_disable(MPI_WIN idx)
{
	int ret = AVFTR_TD_disable(idx);
	if (ret)
		return ret;
	ret = AVFTR_TD_releaseMpiInfo(idx);
	return ret;
}

static int SAMPLE_AROI_setParam(MPI_WIN idx, const AVFTR_AROI_PARAM_S *aroi)
{
	int ret = AVFTR_AROI_setParam(idx, aroi);
	if (ret != 0)
		return ret;
	g_aroi_cb_info = g_aroi_cb_info_pre;
	return 0;
}

static int SAMPLE_PTZ_enable(MPI_WIN idx)
{
	return VIDEO_FTR_enablePtz();
}

static int SAMPLE_PTZ_setParam(MPI_WIN idx, const VIDEO_FTR_PTZ_PARAM_S *ptz)
{
	int ret = 0;
	switch (ptz->mode) {
	case VIDEO_FTR_PTZ_MODE_MANUAL:
	case VIDEO_FTR_PTZ_MODE_SCAN: {
		ret = AVFTR_AROI_regCallback(g_aroi_cb_info.idx, aroiVideoCbEmpty);
		if (ret != 0)
			return ret;
		break;
	}
	case VIDEO_FTR_PTZ_MODE_AUTO: {
		ret = AVFTR_AROI_regCallback(g_aroi_cb_info.idx, aroiVideoCb);
		if (ret != 0)
			return ret;
		break;
	}
	default: {
		ret = AVFTR_AROI_regCallback(g_aroi_cb_info.idx, aroiVideoCbEmpty);
		if (ret != 0)
			return ret;
		break;
	}
	}
	ret = VIDEO_FTR_setPtzParam(ptz);
	return ret;
}

static int SAMPLE_PTZ_disable(MPI_WIN idx)
{
	return VIDEO_FTR_disablePtz();
}

static INT32 registerIva(SAMPLE_IVA_S *vftr)
{
	INT32 ret = 0;
	INT32 sum = 0;
	MPI_WIN idx = vftr->IDX;

	AVFTR_ADD_OBJ(fake_add_obj, VIDEO_FTR_setOdParam, VIDEO_FTR_enableOd, vftr->OD_RE, idx);
	AVFTR_ADD_OBJ(AVFTR_PD_addInstance, AVFTR_PD_setParam, AVFTR_PD_enable, vftr->PD_RE, idx);

	AVFTR_ADD_OBJ(fake_add_obj, VIDEO_FTR_setRmsParam, VIDEO_FTR_enableRms, vftr->RMS, idx);
	AVFTR_ADD_OBJ(AVFTR_AROI_addInstance, SAMPLE_AROI_setParam, AVFTR_AROI_enable, vftr->AROI_RE, idx);
	AVFTR_ADD_OBJ(AVFTR_MD_addInstance, AVFTR_MD_setParamWithROI, AVFTR_MD_enable, vftr->MD_RE, idx);

	AVFTR_ADD_OBJ(AVFTR_EF_addInstance, AVFTR_EF_setParam, AVFTR_EF_enable, vftr->EF_RE, idx);
	AVFTR_ADD_OBJ(AVFTR_TD_addInstance, AVFTR_TD_setParam, AVFTR_TD_enable, vftr->TD_RE, idx);
	AVFTR_ADD_OBJ(AVFTR_LD_addInstance, AVFTR_LD_setParam, AVFTR_LD_enable, vftr->LD_RE, idx);

	AVFTR_ADD_OBJ(AVFTR_DK_addInstance, AVFTR_DK_setParam, AVFTR_DK_enable, vftr->DK_RE, idx);
	AVFTR_ADD_OBJ(AVFTR_FLD_addInstance, AVFTR_FLD_setParam, AVFTR_FLD_enable, vftr->FLD, idx);
	AVFTR_ADD_OBJ(AVFTR_EAIF_addInstance, AVFTR_EAIF_setParam, AVFTR_EAIF_enable, vftr->EAIF, idx);

	AVFTR_ADD_OBJ(fake_add_obj, SAMPLE_PTZ_setParam, SAMPLE_PTZ_enable, vftr->PTZ_RE, idx);

	vftr->enable_cnt = sum;
	printf("Registered all iva!\n");
	return 0;
}

static INT32 unregisterIva(SAMPLE_IVA_S *vftr)
{
	INT32 ret = 0;
	INT32 remove = 0;
	MPI_WIN idx = vftr->IDX;

	AVFTR_DEL_OBJ(fake_del_obj, VIDEO_FTR_disableOd, vftr->OD_RE, idx)
	AVFTR_DEL_OBJ(AVFTR_PD_deleteInstance, AVFTR_PD_disable, vftr->PD_RE, idx);

	AVFTR_DEL_OBJ(fake_del_obj, VIDEO_FTR_disableRms, vftr->RMS, idx);
	AVFTR_DEL_OBJ(AVFTR_AROI_deleteInstance, AVFTR_AROI_disable, vftr->AROI_RE, idx);
	AVFTR_DEL_OBJ(AVFTR_MD_deleteInstance, AVFTR_MD_disable, vftr->MD_RE, idx);
	AVFTR_DEL_OBJ(AVFTR_EF_deleteInstance, AVFTR_EF_disable, vftr->EF_RE, idx);

	AVFTR_DEL_OBJ(AVFTR_TD_deleteInstance, SAMPLE_TD_disable, vftr->TD_RE, idx);
	AVFTR_DEL_OBJ(AVFTR_LD_deleteInstance, AVFTR_LD_disable, vftr->LD_RE, idx);

	AVFTR_DEL_OBJ(AVFTR_DK_deleteInstance, AVFTR_DK_disable, vftr->DK_RE, idx);
	AVFTR_DEL_OBJ(AVFTR_FLD_deleteInstance, AVFTR_FLD_disable, vftr->FLD, idx);
	AVFTR_DEL_OBJ(AVFTR_EAIF_deleteInstance, AVFTR_EAIF_disable, vftr->EAIF, idx);

	AVFTR_DEL_OBJ(fake_del_obj, SAMPLE_PTZ_disable, vftr->PTZ_RE, idx);

	vftr->enable_cnt = 0;
	printf("Unregistered all iva!\n");
	return 0;
}

#if 0
static INT32 updateAllIva(SAMPLE_IVA_CONF_S *old, SAMPLE_IVA_CONF_S *new)
{
	//INT32 ret = 0;
	//INT32 sum = 0;
	if (old->size != new->size) {
		printf("IVA config should have same number of window! old(%d) vs new(%d)\n", old->size, new->size);
		return -1;
	}
	return 0;
#if 0
    SAMPLE_IVA_S *p_old = NULL;
    SAMPLE_IVA_S *p_new = NULL;

    for (int i = 0; i < iva_conf->size; i++) {
        p_old = &old->iva[i];
        p_new = &new->iva[i];

        AVFTR_ADD_OR_DEL_OBJ(fake_add_obj, VIDEO_FTR_setOdParam, VIDEO_FTR_enableOd, new->OD,  )
        AVFTR_ADD_OBJ(fake_add_obj, VIDEO_FTR_setOdParam, VIDEO_FTR_enableOd, &vftr->od, vftr->od, vftr->IDX);
        AVFTR_ADD_OBJ(fake_add_obj, VIDEO_FTR_setPtzParam, VIDEO_FTR_enablePtz, &iva->ptz, vftr->ptz, vftr->IDX);

        AVFTR_ADD_OBJ(AVFTR_DK_addObj, AVFTR_DK_setParam, AVFTR_DK_enable, &iva->dk, vftr->dk, vftr->IDX);
        AVFTR_ADD_OBJ(AVFTR_MD_addObj, AVFTR_MD_setParam, AVFTR_MD_enable, &iva->md, vftr->md, vftr->IDX);
        //AVFTR_ADDOBJ(AVFTR_TD_addObj, vftr->td);

        //ret = AVFTR_TD_regMpiInfo(vftr->td.idx);
        //if (ret != 0) return ret;
        AVFTR_ADD_OBJ(AVFTR_FLD_addObj, AVFTR_FLD_setParam, AVFTR_FLD_enable, &iva->fld, vftr->fld);
        //AVFTR_ADD_OBJ(AVFTR_EF_addObj, init_ef, AVFTR_EF_setParam, AVFTR_EF_enable, &iva->ef, vftr->ef);
        //AVFTR_ADD_OBJ(AVFTR_EAIF_addObj, init_eaif, AVFTR_EAIF_setParam, AVFTR_EAIF_enable, &iva->eaif, vftr->eaif);
        AVFTR_ADD_OBJ(AVFTR_AROI_addObj, AVFTR_AROI_setParam, AVFTR_AROI_enable, &iva->aroi, vftr->aroi, vftr->idx);
    }
#endif

	return 0;
}
#endif

/**
 * @brief thread for updating IQ parameters in .ini
 * @param[in] arg pointer to video device index
 */
static void *updateVftrThread(void *arg)
{
	char *thread_name = (char *)arg;
	SAMPLE_IVA_CONF_S *old = &g_iva_conf;
	int ret = 0;
	if (!setThreadSchedAttr(thread_name)) {
		printf("%s(): Success to set thread [%s] schedule attribute!\n", __func__, thread_name);
	} else {
		printf("%s(): Failed to set thread [%s] schedule attribute!\n", __func__, thread_name);
	}

	while (g_iva_ini_run) {
		/* Check if iva ini file is being updated */
		if (checkIniFileStat(g_iva_fname)) {
			SAMPLE_IVA_CONF_S iva = {};
			/* Parse iva ini file to get iva-related attributes */
			// TODO parse and check
			ret = parseIvaIniFile(g_iva_fname, &iva);
			if (!ret) {
				ret = convertParam(&iva);
				if (!ret) {
					SAMPLE_stopIva();
					*old = iva;
					SAMPLE_startIva();
				} else {
					printf("invalid iva param in config file!\n");
				}

			} else {
				printf("invalid iva config file %s!\n", g_iva_fname);
			}
		}
		usleep(1000000);
	}

	return NULL;
}

void SAMPLE_startIva(void)
{
	SAMPLE_IVA_CONF_S *vftr_conf = &g_iva_conf;
	SAMPLE_IVA_S *vftr = NULL;
	int ret = 0;
	for (int i = 0; i < vftr_conf->size; i++) {
		vftr = &vftr_conf->iva[i];
		ret = registerIva(vftr);
		if (ret < 0) {
			return;
		}
	}

	for (int i = 0; i < vftr_conf->size; i++) {
		vftr = &vftr_conf->iva[i];
		ret = AVFTR_runIva(vftr->IDX);
		if (ret < 0) {
			return;
		}
	}

	return;
}

void SAMPLE_stopIva(void)
{
	SAMPLE_IVA_CONF_S *vftr_conf = &g_iva_conf;
	SAMPLE_IVA_S *vftr = NULL;
	int ret = 0;

	for (int i = 0; i < vftr_conf->size; i++) {
		vftr = &vftr_conf->iva[i];
		ret = AVFTR_exitIva(vftr->IDX);
		if (ret < 0) {
			printf("Cannot detroy VFTR_runIVA thread for chn:%d, win:%d!\n", vftr->IDX.chn, vftr->IDX.win);
			return;
		}
	}

	for (int i = 0; i < vftr_conf->size; i++) {
		vftr = &vftr_conf->iva[i];
		ret = unregisterIva(vftr);
		if (ret < 0) {
			printf("Cannot unregister all iva for chn:%d, win:%d \n", vftr->IDX.chn, vftr->IDX.win);
			return;
		}
	}

	vftr_conf->size = 0;
	return;
}

void SAMPLE_createIvaUpdateThread(const char *fname)
{
	if (access(fname, R_OK) != -1) {
		g_iva_fname = fname;
		g_tid_vftr_name = VFTR_UPDATE_TH_NAME;
		g_iva_ini_run = 1;

		if (pthread_create(&g_tid_vftr, NULL, updateVftrThread, g_tid_vftr_name) != 0) {
			printf("Create thread updateVftrThread failed.\n");
			return;
		}

		if (pthread_setname_np(g_tid_vftr, g_tid_vftr_name) != 0) {
			printf("Set thread name to updateVftrThread failed.\n");
			return;
		}

		printf("Create thread updateVftrThread.\n");
	}
	return;
}

void SAMPLE_destroyIvaUpdateThread(void)
{
	if (g_iva_fname != NULL) {
		g_iva_ini_run = 0;

		if (pthread_join(g_tid_vftr, NULL) != 0) {
			printf("Join thread updateVftrThread failed.\n");
			return;
		}

		g_iva_fname = NULL;
		memset(&g_iva_time, 0, sizeof(struct stat));

		printf("Join thread updateVftrThread.\n");
	}
	return;
}

#else // !SAMPLE_AVFTR_ENABLE
#include "sample_iva.h"
void SAMPLE_initAVFTRServ(void)
{
	return;
}
void SAMPLE_exitAVFTRServ(void)
{
	return;
}
void SAMPLE_startIva(void)
{
	return;
}
void SAMPLE_stopIva(void)
{
	return;
}
void SAMPLE_loadIvaConf(const char *filename)
{
	return;
}
void SAMPLE_createIvaUpdateThread(const char *filename)
{
	return;
}
void SAMPLE_destroyIvaUpdateThread(void)
{
	return;
}
#endif // !SAMPLE_AVFTR_ENABLE
