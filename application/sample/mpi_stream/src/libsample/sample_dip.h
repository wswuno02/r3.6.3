#ifndef SAMPLE_DIP_H_
#define SAMPLE_DIP_H_

#ifdef __cplusplus
extern "C" {
#endif /**< __cplusplus */

#include "mpi_index.h"

/* Interface function prototype */
void SAMPLE_initDipSystem(MPI_PATH idx);
void SAMPLE_exitDipSystem(MPI_PATH idx);
void SAMPLE_updateIni(MPI_DEV dev_idx, char *filename);
void SAMPLE_createIniUpdateThread(MPI_DEV dev_idx, char *filename);
void SAMPLE_destroyIniUpdateThread(MPI_DEV dev_idx);

#ifdef __cplusplus
}
#endif /**< __cplusplus */

#endif /**< SAMPLE_DIP_H_ */
