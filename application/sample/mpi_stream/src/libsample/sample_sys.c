#include "sample_sys.h"

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/time.h>

#include "mpi_index.h"
#include "mpi_dip_types.h"
#include "mpi_dip_sns.h"
#include "mpi_sys.h"
#include "mpi_osd.h"
#include "sensor.h"

#include "udp_socket.h"
#include "generic_file.h"

#include "sample_dip.h"
#include "sample_iva.h"

#include "sample_osd.h"

extern CUSTOM_SNS_CTRL_S custom_sns(SNS0_ID);
#ifdef SNS1
extern CUSTOM_SNS_CTRL_S custom_sns(SNS1_ID);
#endif

#define INC_INT(x) (x) = ((x) == INT_MAX) ? 0 : (x) + 1

#define UDP_STREAM_NAME "udp_stream"
#define UDP_XMT_LEN 8192

#define _STR(s) #s
#define STR(s) _STR(s)

#define DIP_FILE_PATH "/system/mpp/script"
#define IVA_FILE_PATH "/system/mpp/script"

char g_dip_ini_file[128];
char g_iva_ini_file[128];

typedef struct {
	char path[256];
	MPI_ECHN echn_idx; //echn_idx
	int frame_num;
	FILE *fp;
	char udpstream_enable; //udp stream enable 0 : off 1 : on
	char client_ip[32]; //remote ip address
	int client_port; //remote client port
	MPI_SIZE_S res;
} SAMPLE_STREAM_INFO_S;

char g_dip_ini_file[128];
char g_bsb_run[MPI_MAX_ENC_CHN_NUM] = { 0 };

static CUSTOM_SNS_CTRL_S *p_custom_sns[] = {
	&custom_sns(SNS0_ID),
#ifdef SNS1
	&custom_sns(SNS1_ID),
#endif
};

/**
 * @brief Covert window rectangle from percentage to pixel.
 */
static inline void toMpiLayoutWindow(const MPI_RECT_S *pos, MPI_SIZE_S *chn_res, MPI_RECT_S *lyt_res)
{
#define MIN(a, b) ((a) < (b) ? (a) : (b))
	lyt_res->x = (((pos->x * (chn_res->width - 1) + 512) >> 10) + 8) & 0xFFFFFFF0;
	lyt_res->y = (((pos->y * (chn_res->height - 1) + 512) >> 10) + 16) & 0xFFFFFFE0;
	lyt_res->width = MIN((((pos->width * (chn_res->width - 1) + 512) >> 10) + 9) & 0xFFFFFFF0, chn_res->width);

	/* Handle boundary condition */
	if (pos->y + pos->height == 1024) {
		lyt_res->height = chn_res->height - lyt_res->y;
	} else {
		lyt_res->height = (((pos->height * (chn_res->height - 1) + 512) >> 10) + 16) & 0xFFFFFFE0;
	}
}

/**
 * @brief Exception handle for the case that starting a video device failed.
 * @return The execution result.
 *
 * @The sample function is used to handle the case that starting a video device failed.
 * @It will run the procedure to stop the video device so that user can try to
 * @start the program again.
 */
static INT32 SAMPLE_handleDevStartFail(MPI_DEV dev_idx, SAMPLE_CONF_S *const conf)
{
	INT32 ret = MPI_FAILURE;
	UINT32 p_idx = 0;
	MPI_PATH path_idx;
	UINT32 sensor_idx;

	printf("Start the video device %d failed, program is being terminated ...\n", MPI_GET_VIDEO_DEV(dev_idx));

	ret = MPI_DEV_stopDev(dev_idx);
	if (ret != MPI_SUCCESS) {
		printf("Stop video device %d failed.\n", MPI_GET_VIDEO_DEV(dev_idx));
		return MPI_FAILURE;
	}

	SAMPLE_destroyIniUpdateThread(dev_idx);

	for (p_idx = 0; p_idx < conf->dev[dev_idx.dev].path_cnt; ++p_idx) {
		path_idx = MPI_INPUT_PATH(dev_idx.dev, conf->dev[dev_idx.dev].path[p_idx].path_idx);

		SAMPLE_exitDipSystem(path_idx);

		sensor_idx = conf->dev[dev_idx.dev].path[p_idx].gen.sensor_idx;

		p_custom_sns[sensor_idx]->dereg_callback(path_idx);

		MPI_DEV_deletePath(path_idx);
	}

	ret = MPI_DEV_destroyDev(dev_idx);
	if (ret != MPI_SUCCESS) {
		printf("Destroy video device %d failed.\n", MPI_GET_VIDEO_DEV(dev_idx));
		return MPI_FAILURE;
	}

	ret = MPI_VB_exit();
	if (ret != MPI_SUCCESS) {
		printf("Exit video buffer failed.\n");
		return MPI_FAILURE;
	}

	ret = MPI_SYS_exit();
	if (ret != MPI_SUCCESS) {
		printf("Exit system failed.\n");
		return MPI_FAILURE;
	}

	printf("Terminating program complete, please run the program again!\n");

	return MPI_SUCCESS;
}

/**
 * @brief Start encoder channel.
 * @see SAMPLE_stopEncChn()
 * @return The execution result.
 *
 * @The sample function is used to start encoder stream.
 * @The default video device index is 0. These MPIs
 * @are supposed to be executed in sequence like that in this function.
 */
INT32 SAMPLE_startEncChn(UINT32 idx, CONF_ENC_CHN_PARAM *const enc_chn)
{
	INT32 ret = MPI_FAILURE;
	MPI_ECHN echn_idx;

	MPI_ENC_CHN_ATTR_S *enc_chn_attr;
	MPI_ENC_BIND_INFO_S *bind_info;
	MPI_VENC_ATTR_S *venc_attr;
	MPI_VENC_ATTR_EX_S *venc_attr_ex;

	/* By encoder channel setting */
	echn_idx = MPI_ENC_CHN(enc_chn->chn_idx);

	enc_chn_attr = &enc_chn->chn;
	ret = MPI_ENC_createChn(echn_idx, enc_chn_attr);
	if (ret != MPI_SUCCESS) {
		printf("Create encoder channel %d failed.\n", MPI_GET_ENC_CHN(echn_idx));
		return MPI_FAILURE;
	}

	bind_info = &enc_chn->bind_info;
	ret = MPI_ENC_bindToVideoChn(echn_idx, bind_info);
	if (ret != MPI_SUCCESS) {
		printf("Bind encoder channel %d failed.\n", MPI_GET_ENC_CHN(echn_idx));
		return MPI_FAILURE;
	}

	venc_attr = &enc_chn->venc;
	ret = MPI_ENC_setVencAttr(echn_idx, venc_attr);
	if (ret != MPI_SUCCESS) {
		printf("Set VENC attr for encoder channel %d failed.\n", MPI_GET_ENC_CHN(echn_idx));
		return MPI_FAILURE;
	}

	venc_attr_ex = &enc_chn->venc_ex;
	ret = MPI_ENC_setVencAttrEx(echn_idx, venc_attr_ex);
	if (ret != MPI_SUCCESS) {
		printf("Set VENC attr ex for encoder channel %d failed.\n", MPI_GET_ENC_CHN(echn_idx));
		return MPI_FAILURE;
	}

	ret = MPI_ENC_startChn(echn_idx);
	if (ret != MPI_SUCCESS) {
		printf("Start encoder channel %d failed.\n", MPI_GET_ENC_CHN(echn_idx));
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

/**
 * @brief Stop encoder channel.
 * @see SAMPLE_startEncChn()
 * @return The execution result.
 *
 * @The sample function is used to stop encoder channel. The default video
 * @device index is 0. These MPIs are supposed to be executed in sequence
 * @like that in this function.
 */
INT32 SAMPLE_stopEncChn(UINT32 idx, CONF_ENC_CHN_PARAM *const enc_chn)
{
	INT32 ret = MPI_FAILURE;
	MPI_ECHN echn_idx;

	echn_idx = MPI_ENC_CHN(enc_chn->chn_idx);

	ret = MPI_ENC_stopChn(echn_idx);
	if (ret != MPI_SUCCESS) {
		printf("Stop encoder channel %d failed.\n", MPI_GET_ENC_CHN(echn_idx));
		return MPI_FAILURE;
	}

	ret = MPI_ENC_unbindFromVideoChn(echn_idx);
	if (ret != MPI_SUCCESS) {
		printf("Unbind encoder channel %d failed.\n", MPI_GET_ENC_CHN(echn_idx));
		return MPI_FAILURE;
	}

	ret = MPI_ENC_destroyChn(echn_idx);
	if (ret != MPI_SUCCESS) {
		printf("Destroy encoder channel %d failed.\n", MPI_GET_ENC_CHN(echn_idx));
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

/**
 * @brief Start stream.
 * @details The sample function is used to start stream. The default 
 * resolution is 1920x1080 and could be modified by changing res.width 
 * and res.height in this function. The default sensor path index and 
 * video device index are both 0. These MPIs are supposed to be executed 
 * in sequence like that in this function.
 * @param[in] dev_idx video device index
 * @param[in] conf    pointer to the application configuration
 * @see SAMPLE_stopStream()
 * @return The execution result.
 * @retval 0      SUCCESS
 * @retval others FAILURE
 */
INT32 SAMPLE_startStream(MPI_DEV dev_idx, SAMPLE_CONF_S *const conf)
{
	INT32 ret = MPI_FAILURE;
	UINT32 p_idx = 0;
	UINT32 c_idx = 0;
	UINT32 w_idx = 0;
	MPI_PATH path_idx;
	MPI_CHN chn_idx;
	MPI_WIN win_idx;
	INT32 output_num;
	UINT32 sensor_idx;

	MPI_VB_CONF_S *vb_conf;
	MPI_PATH_ATTR_S *path_attr;
	MPI_DEV_ATTR_S *dev_attr;
	MPI_CHN_ATTR_S *chn_attr;
	MPI_CHN_LAYOUT_S *chn_layout_tmp;
	MPI_WIN_ATTR_S *win_attr;
	MPI_STITCH_ATTR_S *stitch_attr;
	MPI_LDC_ATTR_S *ldc_attr;
	MPI_PANORAMA_ATTR_S *pano_attr;
	MPI_PANNING_ATTR_S *pann_attr;
	MPI_SURROUND_ATTR_S *surr_attr;
	MPI_CHN_LAYOUT_S chn_layout = { 0 };
	UINT16 width;
	UINT16 height;

	ret = MPI_SYS_init();
	if (ret != MPI_SUCCESS) {
		printf("Initialize system failed. err: %d\n", ret);
		return ret;
	}

	vb_conf = &conf->sys.vb_conf;
	ret = MPI_VB_setConf(vb_conf);
	if (ret != MPI_SUCCESS) {
		printf("Configure video buffer failed. err: %d\n", ret);
		return ret;
	}

	ret = MPI_VB_init();
	if (ret != MPI_SUCCESS) {
		printf("Initialize video buffer failed. err: %d\n", ret);
		return ret;
	}

	/* By device setting */
	dev_attr = &conf->dev[dev_idx.dev].gen;
	/* TODO: use case config to define path bmp */
	dev_attr->path.bmp = (conf->dev[dev_idx.dev].path_cnt == 2) ? 0x3 : 0x1;
	ret = MPI_DEV_createDev(dev_idx, dev_attr);
	if (ret != MPI_SUCCESS) {
		printf("Create video device %d failed. err: %d\n", MPI_GET_VIDEO_DEV(dev_idx), ret);
		return ret;
	}

	/* By path setting */
	for (p_idx = 0; p_idx < conf->dev[dev_idx.dev].path_cnt; ++p_idx) {
		path_idx = MPI_INPUT_PATH(dev_idx.dev, conf->dev[dev_idx.dev].path[p_idx].path_idx);

		path_attr = &conf->dev[dev_idx.dev].path[p_idx].gen;

		sensor_idx = path_attr->sensor_idx;

		ret = MPI_DEV_addPath(path_idx, path_attr);
		if (ret != MPI_SUCCESS) {
			printf("Set input path %d failed. err: %d\n", MPI_GET_INPUT_PATH(path_idx), ret);
			return ret;
		}

		p_custom_sns[sensor_idx]->reg_callback(path_idx);

		SAMPLE_initDipSystem(path_idx);
	}

	/**
	 * 1. Use default name to search .ini if no command argument is parsed.
	 * 2. Use specified .ini if -s <path_name> exists. 
	 */
	if (!conf->dev[dev_idx.dev].path[0].ini_cnt) {
		sprintf(g_dip_ini_file, "%s/sensor_0.ini", DIP_FILE_PATH);
	} else {
		for (int i = 0; i < conf->dev[dev_idx.dev].path[0].ini_cnt; ++i) {
			memcpy(g_dip_ini_file, conf->dev[dev_idx.dev].path[0].ini_files[i], sizeof(g_dip_ini_file));
			SAMPLE_updateIni(dev_idx, conf->dev[dev_idx.dev].path[0].ini_files[i]);
		}
	}

	SAMPLE_createIniUpdateThread(dev_idx, g_dip_ini_file);

	ret = MPI_DEV_startDev(dev_idx);
	if (ret != MPI_SUCCESS) {
		printf("Start video device %d failed. err: %d\n", MPI_GET_VIDEO_DEV(dev_idx), ret);
		SAMPLE_handleDevStartFail(dev_idx, conf);
		return ret;
	}

	/* By video channel setting */
	output_num = conf->dev[dev_idx.dev].chn_cnt;

	SAMPLE_initOsd();

	for (c_idx = 0; c_idx < output_num; ++c_idx) {
		chn_idx = MPI_VIDEO_CHN(dev_idx.dev, conf->dev[dev_idx.dev].chn[c_idx].chn_idx);

		chn_attr = &conf->dev[dev_idx.dev].chn[c_idx].gen;
		chn_layout_tmp = &conf->dev[dev_idx.dev].chn[c_idx].layout;
		ret = MPI_DEV_addChn(chn_idx, chn_attr);
		if (ret != MPI_SUCCESS) {
			printf("Add video channel %d failed. err: %d\n", MPI_GET_VIDEO_CHN(chn_idx), ret);
			return ret;
		}

		chn_layout.window_num = chn_layout_tmp->window_num;
		for (w_idx = 0; w_idx < chn_layout.window_num; ++w_idx) {
			chn_layout.win_id[w_idx] = MPI_VIDEO_WIN(dev_idx.dev, chn_idx.chn, w_idx);
			toMpiLayoutWindow(&chn_layout_tmp->window[w_idx], &chn_attr->res, &chn_layout.window[w_idx]);
		}

		ret = MPI_DEV_setChnLayout(chn_idx, &chn_layout);
		if (ret != MPI_SUCCESS) {
			printf("Set video channel layout %d failed. err: %d\n", MPI_GET_VIDEO_CHN(chn_idx), ret);
			return ret;
		}

		for (w_idx = 0; w_idx < chn_layout.window_num; ++w_idx) {
			win_idx = MPI_VIDEO_WIN(dev_idx.dev, conf->dev[dev_idx.dev].chn[c_idx].chn_idx, w_idx);

			win_attr = &conf->dev[dev_idx.dev].chn[c_idx].win[w_idx];
			ret = MPI_DEV_setWindowAttr(win_idx, win_attr);
			if (ret != MPI_SUCCESS) {
				printf("Set video window %d failed. err: %d\n", MPI_GET_VIDEO_WIN(win_idx), ret);
				return ret;
			}
		}

		width = conf->dev[dev_idx.dev].chn[c_idx].gen.res.width;
		height = conf->dev[dev_idx.dev].chn[c_idx].gen.res.height;

		ret = SAMPLE_createOsd(conf->osd_enable, chn_idx, output_num, width, height);
		if (ret != MPI_SUCCESS) {
			printf("Start OSD %d failed. err: %d\n", MPI_GET_VIDEO_CHN(chn_idx), ret);
			return ret;
		}
	}

	SAMPLE_createUpdateThread();

	/* Only apply on d,c,w = 0,0,0 */
	chn_idx = MPI_VIDEO_CHN(dev_idx.dev, 0);
	win_idx = MPI_VIDEO_WIN(dev_idx.dev, 0, 0);

	stitch_attr = &conf->dev[dev_idx.dev].chn[chn_idx.chn].stitch;
	ret = MPI_DEV_setStitchAttr(win_idx, stitch_attr);
	if (ret != MPI_SUCCESS) {
		printf("Set STITCH attr for window %d failed. err: %d\n", MPI_GET_VIDEO_WIN(win_idx), ret);
		return ret;
	}

	ldc_attr = &conf->dev[dev_idx.dev].chn[chn_idx.chn].ldc;
	ret = MPI_DEV_setLdcAttr(win_idx, ldc_attr);
	if (ret != MPI_SUCCESS) {
		printf("Set LDC attr for window %d failed. err: %d\n", MPI_GET_VIDEO_WIN(win_idx), ret);
		return ret;
	}

	pano_attr = &conf->dev[dev_idx.dev].chn[chn_idx.chn].panorama;
	ret = MPI_DEV_setPanoramaAttr(win_idx, pano_attr);
	if (ret != MPI_SUCCESS) {
		printf("Set PANORAMA attr for window %d failed. err: %d\n", MPI_GET_VIDEO_WIN(win_idx), ret);
		return ret;
	}

	pann_attr = &conf->dev[dev_idx.dev].chn[chn_idx.chn].panning;
	ret = MPI_DEV_setPanningAttr(win_idx, pann_attr);
	if (ret != MPI_SUCCESS) {
		printf("Set PANNING attr for window %d failed. err: %d\n", MPI_GET_VIDEO_WIN(win_idx), ret);
		return ret;
	}

	surr_attr = &conf->dev[dev_idx.dev].chn[chn_idx.chn].surround;
	ret = MPI_DEV_setSurroundAttr(win_idx, surr_attr);
	if (ret != MPI_SUCCESS) {
		printf("Set SURROUND attr for window %d failed. err: %d\n", MPI_GET_VIDEO_WIN(win_idx), ret);
		return ret;
	}

	if (output_num == 1) {
		ret = MPI_DEV_startChn(chn_idx);
		if (ret != MPI_SUCCESS) {
			printf("Start video channel %d failed. err: %d\n", MPI_GET_VIDEO_CHN(chn_idx), ret);
			return ret;
		}
	} else {
		ret = MPI_DEV_startAllChn(dev_idx);
		if (ret != MPI_SUCCESS) {
			printf("Start all video channels on video device %d failed. err: %d\n",
			       MPI_GET_VIDEO_DEV(dev_idx), ret);
			return ret;
		}
	}

	if (conf->iva_enable) {
		if (strlen(conf->iva_ini_name) == 0)
			sprintf(g_iva_ini_file, "%s/iva.ini", IVA_FILE_PATH);
		else
			strncpy(g_iva_ini_file, conf->iva_ini_name, 128);
		SAMPLE_initAVFTRServ();
		SAMPLE_loadIvaConf(g_iva_ini_file);
		SAMPLE_startIva();
		SAMPLE_createIvaUpdateThread(g_iva_ini_file);
	}

	printf("Start stream succeeded!\n");
	return MPI_SUCCESS;
}

/**
 * @brief Stop stream.
 * @see SAMPLE_startStream()
 * @return The execution result.
 *
 * @The sample function is used to stop stream. The default sensor path
 * @index and video device index are both 0. These MPIs are supposed to be
 * @executed in sequence like that in this function.
 */
INT32 SAMPLE_stopStream(MPI_DEV dev_idx, const SAMPLE_CONF_S *conf)
{
	INT32 ret = MPI_FAILURE;
	UINT32 c_idx = 0;
	UINT32 p_idx = 0;
	MPI_CHN chn_idx;
	MPI_PATH path_idx;
	INT32 output_num;
	UINT32 sensor_idx;

	output_num = conf->dev[dev_idx.dev].chn_cnt;

	if (conf->iva_enable) {
		SAMPLE_destroyIvaUpdateThread();
		SAMPLE_stopIva();
		SAMPLE_exitAVFTRServ();
	}

	if (output_num > 1) {
		ret = MPI_DEV_stopAllChn(dev_idx);
		if (ret != MPI_SUCCESS) {
			printf("Stop all video channels on video device %d failed. err: %d\n",
			       MPI_GET_VIDEO_DEV(dev_idx), ret);
			return ret;
		}
	}

	for (c_idx = 0; c_idx < output_num; ++c_idx) {
		chn_idx = MPI_VIDEO_CHN(dev_idx.dev, conf->dev[dev_idx.dev].chn[c_idx].chn_idx);

		if (output_num == 1) {
			ret = MPI_DEV_stopChn(chn_idx);
			if (ret != MPI_SUCCESS) {
				printf("Stop video channel %d failed. err: %d\n", MPI_GET_VIDEO_CHN(chn_idx), ret);
				return ret;
			}
		}

		ret = SAMPLE_stopOsd(chn_idx);

		if (ret != MPI_SUCCESS) {
			printf("Stop OSD %d failed. err: %d\n", MPI_GET_VIDEO_CHN(chn_idx), ret);
			return ret;
		}
	}

	SAMPLE_free_memory();

	ret = MPI_DEV_stopDev(dev_idx);
	if (ret != MPI_SUCCESS) {
		printf("Stop video device %d failed. err: %d\n", MPI_GET_VIDEO_DEV(dev_idx), ret);
		return ret;
	}

	SAMPLE_destroyIniUpdateThread(dev_idx);

	/* Deleting channels after all firmware stopped */
	for (c_idx = 0; c_idx < output_num; ++c_idx) {
		chn_idx = MPI_VIDEO_CHN(dev_idx.dev, conf->dev[dev_idx.dev].chn[c_idx].chn_idx);

		ret = MPI_DEV_deleteChn(chn_idx);
		if (ret != MPI_SUCCESS) {
			printf("Delete video channel %d failed. err: %d\n", MPI_GET_VIDEO_CHN(chn_idx), ret);
			return ret;
		}
	}

	for (p_idx = 0; p_idx < conf->dev[dev_idx.dev].path_cnt; ++p_idx) {
		path_idx = MPI_INPUT_PATH(dev_idx.dev, conf->dev[dev_idx.dev].path[p_idx].path_idx);

		SAMPLE_exitDipSystem(path_idx);
		sensor_idx = conf->dev[dev_idx.dev].path[p_idx].gen.sensor_idx;
		p_custom_sns[sensor_idx]->dereg_callback(path_idx);
		MPI_DEV_deletePath(path_idx);
	}

	ret = MPI_DEV_destroyDev(dev_idx);
	if (ret != MPI_SUCCESS) {
		printf("Destroy video device %d failed. err: %d\n", MPI_GET_VIDEO_DEV(dev_idx), ret);
		return ret;
	}

	ret = MPI_VB_exit();
	if (ret != MPI_SUCCESS) {
		printf("Exit video buffer failed. err: %d\n", ret);
		return ret;
	}

	ret = MPI_SYS_exit();
	if (ret != MPI_SUCCESS) {
		printf("Exit system failed. err: %d\n", ret);
		return ret;
	}

	printf("Stop stream succeeded!\n");
	return MPI_SUCCESS;
}

/**
 * @brief Initialize the application to get bit stream data.
 * @see SAMPLE_exitGetStream()
 * @return The execution result.
 *
 * @The sample function is used to initialize the application to get bit
 * @stream data.
 */
INT32 SAMPLE_initGetStream(void)
{
	INT32 ret = MPI_FAILURE;

	ret = MPI_initBitStreamSystem();
	if (ret != MPI_SUCCESS) {
		printf("Bit stream system init failed. err: %d\n", ret);
		return ret;
	}

	return MPI_SUCCESS;
}

/**
 * @brief Exit the application to get bit stream data.
 * @see SAMPLE_initGetStream()
 * @return The execution result.
 *
 * @The sample function is used to exit the application to get bit
 * @stream data.
 */
INT32 SAMPLE_exitGetStream(void)
{
	INT32 ret = MPI_FAILURE;

	ret = MPI_exitBitStreamSystem();
	if (ret != MPI_SUCCESS) {
		printf("Bit stream system exit failed. err: %d\n", ret);
		return ret;
	}

	return MPI_SUCCESS;
}

/**
 * @brief Thread to get bit stream data.
 *
 * @The function will create channel to get bit stream at first, then call MPIs
 * @to get the parameters of bit stream. With these parameter, we can read bit
 * @stream data at the address of stream buffer and  write data to a file in an
 * @user-defined path. After that, the bit stream buffer could be released (to be implemented).
 */
static void SAMPLE_getStreamThread(void *p)
{
	SAMPLE_STREAM_INFO_S *info = (SAMPLE_STREAM_INFO_S *)p;
	MPI_STREAM_PARAMS_S params;
	MPI_BUF_SEG_S *seg = NULL;
	SinkUdp *sink = NULL;

	INT32 ret = 0;
	INT32 cnt = 0;
	INT32 packet = 0;
	INT32 frame_cnt = 0;
	MPI_BCHN bchn = MPI_INVALID_ENC_BCHN;
	time_t now = 0;
	time_t old = 0;
	int i;

	uint32_t total_bytes = 0;

	memset(&params, 0, sizeof(MPI_STREAM_PARAMS_S));

	bchn = MPI_createBitStreamChn(info->echn_idx);
	if (bchn.value == MPI_VALUE_INVALID) {
		printf("Failed to create bit stream channel.\n");
		goto del_file;
	}

	if (info->udpstream_enable) {
		sink = createUdpSink(UDP_STREAM_NAME, info->client_ip, info->client_port);
		if (sink == NULL) {
			printf("Failed to create File Sink!\n");
			goto exit_sink;
		}

		ret = sink->ops->open(sink->info, 0);
		if (ret < 0) {
			printf("[ENCSTREAM] Cannot Open %s, will discard all frames!\n", sink->name);
		}
	}

	old = now = time(0);

	while (g_bsb_run[info->echn_idx.chn]) {
		now = time(0);
		/**< if udp stream not enable && record not enable(frame_num=-1) leave thread*/
		/**< if udp stream not enable && record frame_num over config setting*/
		if (!info->udpstream_enable && (cnt == info->frame_num)) {
			printf("[SAMPLE] %s: Terminating stream %d\n", __func__, MPI_GET_ENC_CHN(info->echn_idx));
			break;
		}

		ret = MPI_getBitStream(bchn, &params, 1200 /*ms*/);
		if (ret != MPI_SUCCESS) {
			/* Request frame again if timeout occurred */
			if (ret == -ETIMEDOUT || ret == -EAGAIN || ret == -EINTR) {
				continue;
			}

			if (ret == -EFAULT) {
				printf("Unable to read from/write to MPI stream's parameter.\n");
			}

			/**
			 * Wait encoder run again.
			 * In some scenarios, user might temporary stop encoder to modify some
			 * static configurations (Ex. OSD, codec type), process should not exit
			 * in these cases.
			 */
			if (ret == -ENODATA) {
				frame_cnt = 0;
				total_bytes = 0;
				continue;
			}

			printf("Failed to get parameters of stream %d.\n", MPI_GET_ENC_CHN(info->echn_idx));
			goto del_mem;
		}

		seg = params.seg;
		if (seg == NULL) {
			continue;
		}

		if (info->fp != NULL && (cnt != info->frame_num)) {
			for (i = 0; i < params.seg_cnt; ++i) {
				seg = &params.seg[i];
				fwrite(seg->uaddr, sizeof(char), seg->size, info->fp);
				total_bytes += seg->size;
			}
		}

		if (sink != NULL) {
			for (i = 0; i < params.seg_cnt; ++i) {
				seg = &params.seg[i];
				for (packet = seg->size; packet > 0; packet -= UDP_XMT_LEN) {
					if (packet <= UDP_XMT_LEN) {
						if (sink->ops->write(sink->info, seg->uaddr, packet) < 0)
							printf("stream write error \n");
					} else {
						if (sink->ops->write(sink->info, seg->uaddr, UDP_XMT_LEN) < 0)
							printf("stream write error \n");
						seg->uaddr += UDP_XMT_LEN;
					}
				}

				total_bytes += seg->size;
			}
		}

		if (seg->type >= MPI_FRAME_TYPE_I) {
			frame_cnt++;

			if (old != now) {
				printf("stream %d, fps %d, bps %d Kbps\n", MPI_GET_ENC_CHN(info->echn_idx), frame_cnt,
				       (total_bytes + 64) >> 7);
				frame_cnt = 0;
				old = now;
				total_bytes = 0;
			}

			if (cnt % 10 == 0 && (cnt != info->frame_num)) {
				printf("stream %d, %d frames are saved!\n", MPI_GET_ENC_CHN(info->echn_idx), cnt);
			}
			if (cnt % 30 == 0 && info->udpstream_enable) {
				printf("live stream %d, %d frames are send!\n", MPI_GET_ENC_CHN(info->echn_idx), cnt);
			}

			INC_INT(cnt);
		}

		MPI_releaseBitStream(bchn, &params);
	}

del_mem:
	MPI_destroyBitStreamChn(bchn);

exit_sink:
	if (sink != NULL) {
		releaseUdpSink(sink);
	}

del_file:
	if (info->fp != NULL) {
		fclose(info->fp);
		info->fp = NULL;
	}

	g_bsb_run[info->echn_idx.chn] = 0;
	free(info);
	/* indicate main thread to exit */

	return;
}

/**
 * @brief Start a thread to get bit stream data.
 *
 * @The function is used to create a thread to get bit stream data. User should
 * @specify a user-defined file path, physical/extended channel index and the
 * @total number of frame to be get.
 */
INT32 SAMPLE_startGetStream(CONF_ENC_CHN_PARAM *const conf)
{
	SAMPLE_STREAM_INFO_S *info = malloc(sizeof(SAMPLE_STREAM_INFO_S));
	const char *path = conf->casep.record_enable ? conf->casep.output_file : NULL;

	if (info == NULL) {
		printf("Failed to allocate memory space for stream info.\n");
		return MPI_FAILURE;
	}

	info->fp = NULL;

	if (path) {
		info->fp = fopen(path, "wb");

		if (info->fp == NULL) {
			printf("Cannot Open %s, all frames will be discarded!\n", path);
		}
	}

	info->echn_idx = MPI_ENC_CHN(conf->chn_idx);
	info->frame_num = info->fp ? conf->casep.frame_num : -1;
	info->udpstream_enable = conf->casep.udpstream_enable;
	info->client_port = conf->casep.client_port;
	strcpy(info->client_ip, conf->casep.client_ip);
	info->res = conf->chn.res;
	g_bsb_run[conf->chn_idx] = 1;

	if (pthread_create(&conf->tid, NULL, (void *)SAMPLE_getStreamThread, (void *)info)) {
		printf("Failed to create thread to get stream data of channel %d !\n", MPI_GET_ENC_CHN(info->echn_idx));
		goto err;
	}

	return MPI_SUCCESS;

err:
	g_bsb_run[conf->chn_idx] = 0;

	if (info->fp != NULL) {
		fclose(info->fp);
		info->fp = NULL;
	}

	free(info);

	return MPI_FAILURE;
}

/**
 * @brief Stop a thread to get bit stream data.
 *
 * @The function is used to stop a thread to get bit stream data. It should be
 * @called after obtaining all the wanted bit stream data.
 */
void SAMPLE_stopGetStream(CONF_ENC_CHN_PARAM *const conf)
{
	g_bsb_run[conf->chn_idx] = 0;
	pthread_join(conf->tid, NULL);
}
