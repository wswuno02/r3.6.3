#define _GNU_SOURCE //For pthread_setname_np
#include "sample_dip.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <pthread.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "ini.h"
#include "mpi_dip_types.h"
#include "mpi_dip_sns.h"
#include "mpi_dip_alg.h"
#include "mpi_dev.h"

#include "agtx_prio.h"
typedef enum {
	DIP_STAT_NULL,
	DIP_STAT_SINGLE,
	DIP_STAT_DUAL,
	DIP_STAT_STITCH,
	DIP_STAT_NUM,
} SAMPLE_DIP_STAT_E;

typedef struct {
	INT32 shp_select;
} SAMPLE_SHP_SELECT_S;

typedef struct {
	INT32 enable;
} SAMPLE_STAT_ENABLE_S;

/* Define the config struct type */
typedef struct {
	SAMPLE_DIP_STAT_E mode;
	INT32 hdr_en;

	// for single path
	MPI_AE_ATTR_S AE;
	MPI_ISO_ATTR_S DIP_ISO;
	MPI_AWB_ATTR_S AWB;
	MPI_PTA_ATTR_S PTA;
	MPI_CSM_ATTR_S CSM;
	MPI_SHP_ATTR_S SHP;
	MPI_NR_ATTR_S NR;
	MPI_GAMMA_ATTR_S GAMMA;
	MPI_TE_ATTR_S TE;
	MPI_DIP_ATTR_S DIP;
	MPI_DBC_ATTR_S DBC0;
	MPI_DCC_ATTR_S DCC0;
	MPI_LSC_ATTR_S LSC0;
	MPI_CAL_ATTR_S CAL0;
	MPI_DBC_ATTR_S DBC1;
	MPI_DCC_ATTR_S DCC1;
	MPI_LSC_ATTR_S LSC1;
	MPI_CAL_ATTR_S CAL1;
	MPI_ROI_ATTR_S ROI0;
	MPI_ROI_ATTR_S ROI1;
	MPI_ENH_ATTR_S ENH;
	MPI_FCS_ATTR_S FCS;
	MPI_CORING_ATTR_S CORING;
	MPI_SHP_ATTR_V2_S SHP_V2;
	SAMPLE_SHP_SELECT_S SAMPLE_SHP_SELECT;
	SAMPLE_STAT_ENABLE_S SAMPLE_STAT_CONFIG;
	MPI_STAT_CFG_S STAT;

	// for dual paths
	MPI_AE_ATTR_S AE0;
	MPI_AE_ATTR_S AE1;
	MPI_ISO_ATTR_S DIP_ISO0;
	MPI_ISO_ATTR_S DIP_ISO1;
	MPI_AWB_ATTR_S AWB0;
	MPI_AWB_ATTR_S AWB1;
	MPI_PTA_ATTR_S PTA0;
	MPI_PTA_ATTR_S PTA1;
	MPI_CSM_ATTR_S CSM0;
	MPI_CSM_ATTR_S CSM1;
	MPI_SHP_ATTR_S SHP0;
	MPI_SHP_ATTR_S SHP1;
	MPI_NR_ATTR_S NR0;
	MPI_NR_ATTR_S NR1;
	MPI_GAMMA_ATTR_S GAMMA0;
	MPI_GAMMA_ATTR_S GAMMA1;
	MPI_TE_ATTR_S TE0;
	MPI_TE_ATTR_S TE1;
	MPI_DIP_ATTR_S DIP0;
	MPI_DIP_ATTR_S DIP1;
	MPI_ENH_ATTR_S ENH0;
	MPI_ENH_ATTR_S ENH1;
	MPI_FCS_ATTR_S FCS0;
	MPI_FCS_ATTR_S FCS1;
	MPI_CORING_ATTR_S CORING0;
	MPI_CORING_ATTR_S CORING1;
	MPI_SHP_ATTR_V2_S SHP_V2_0;
	MPI_SHP_ATTR_V2_S SHP_V2_1;
	SAMPLE_SHP_SELECT_S SAMPLE_SHP_SELECT0;
	SAMPLE_SHP_SELECT_S SAMPLE_SHP_SELECT1;
	MPI_HDR_SYNTH_ATTR_S HDR_SYNTH;
	SAMPLE_STAT_ENABLE_S SAMPLE_STAT_CONFIG0;
	SAMPLE_STAT_ENABLE_S SAMPLE_STAT_CONFIG1;
	MPI_STAT_CFG_S STAT0;
	MPI_STAT_CFG_S STAT1;
} SAMPLE_DIP_CONF_S;

/* Global variables declaration */
pthread_t g_tid_ini;
char *g_tid_name = NULL;

SAMPLE_DIP_CONF_S g_config;
char *g_filename = NULL;
static struct stat g_mod_time = { 0 };
static MPI_DEV g_dev;
static int g_ini_run = 0;

/* Internal function prototype */
static int handler(void *user, const char *section, const char *name, const char *value);
static void dump_config(SAMPLE_DIP_CONF_S *cfg);
static int checkIniFileStat(const char *filename);
static int parseIniFile(const char *filename);
static void *updateIniThread(void *arg);
static void setDipAttr(MPI_DEV idx);
static void getDipAttr(MPI_DEV idx);
static void getDevAttr(MPI_DEV idx);

/* Process a line of the INI file, storing valid values into config struct */
static int handler(void *user, const char *section, const char *name, const char *value)
{
	SAMPLE_DIP_CONF_S *cfg = (SAMPLE_DIP_CONF_S *)user;
	if (0)
		;
#define CFG(s, n, default)                                                                                             \
	else if (strcmp(section, #s) == 0 && strcmp(name, #n) == 0) cfg->s.n = (__typeof__(cfg->s.n))atof(value);
#include "config.def"
	return 1;
}

/* Print all the variables in the config, one per line */
static void dump_config(SAMPLE_DIP_CONF_S *cfg)
{
//#define CFG(s, n, default) printf("%s_%s = %.2lf\n", #s, #n, (float)cfg->s.n);
//#include "config.def"
}

/**
 * @brief compare to the last modified time and return 1 if the file is updated
 * @return whether the ini file is updated
 * @retval  0 not updated yet
 * @retval  1 is updated
 * @retval -1 return error
 */
static int checkIniFileStat(const char *filename)
{
	struct stat buf;

	/* Get File Statistics for stat.c. */
	if (stat(filename, &buf) != 0) {
		perror("Problem getting information");
		return -1;
	} else { /* Print the date/time last modified */
		if (buf.st_mtime != g_mod_time.st_mtime) {
			g_mod_time.st_mtime = buf.st_mtime;
			return 1;
		}
	}

	return 0;
}

/**
 * @brief dump the IQ configuration to global variable
 * @return the execution result
 * @retval  0 success
 * @retval -1 failure
 */
static int parseIniFile(const char *filename)
{
	if (ini_parse(filename, handler, &g_config) < 0) {
		printf("Can't load '%s', using defaults\n", filename);
		return -1;
	} else {
		dump_config(&g_config);
	}

	return 0;
}

/**
 * @brief thread for updating IQ parameters in .ini
 * @param[in] arg pointer to video device index
 */
static void *updateIniThread(void *arg)
{
	MPI_DEV *idx;
	idx = (MPI_DEV *)arg;

	getDevAttr(*idx);
	getDipAttr(*idx);

	if (!setThreadSchedAttr(g_tid_name)) {
		printf("%s(): Success to set thread [%s] schedule attribute!\n", __func__, g_tid_name);
	} else {
		printf("%s(): Failed to set thread [%s] schedule attribute!\n", __func__, g_tid_name);
	}

	while (g_ini_run) {
		/* Check if DIP ini file is being updated */
		if (checkIniFileStat(g_filename)) {
			/* Parse DIP ini file to get DIP-related attributes */
			int ret = parseIniFile(g_filename);

			if (ret == 0) {
				setDipAttr(*idx);
			}
		}

		usleep(200000);
	}

	return NULL;
}

/**
 * @brief update the device operating state to global variable
 */
static void getDevAttr(MPI_DEV idx)
{
	MPI_DEV_ATTR_S dev_attr;
	UINT8 path_cnt;
	MPI_DEV_getDevAttr(idx, &dev_attr);

	path_cnt = (dev_attr.path.bmp == 0x3) ? 2 : 1;

	/* Determine parameters for get/set DIP attribute */
	if (dev_attr.stitch_en == 0 && path_cnt == 1) {
		g_config.mode = DIP_STAT_SINGLE; // Only vaild on (d, p) = (0, 0)

	} else if (dev_attr.stitch_en == 0 && path_cnt == 2) {
		g_config.mode = DIP_STAT_DUAL; // all vaild (d, p)

	} else if (dev_attr.stitch_en == 1 && path_cnt == 2) {
		g_config.mode = DIP_STAT_STITCH; // DBC, DCC, LSC, CAL, ROI could accept (d, p) = (0~1, 0~1)

	} else {
		g_config.mode = DIP_STAT_NULL;
		printf("%s(): Invaild device attribute, stitch_en = %d, path_cnt = %d, impossible case!\n", __func__, dev_attr.stitch_en, path_cnt);
	}

	g_config.hdr_en = (dev_attr.hdr_mode == 0) ? 0 : 1;
}

/**
 * @brief write the proper DIP attributes to video device
 */
static void setDipAttr(MPI_DEV idx)
{
	MPI_PATH path_idx;

	/* Set attribute */
	switch (g_config.mode) {
	case DIP_STAT_SINGLE:
		path_idx = MPI_INPUT_PATH(idx.dev, 0);
		MPI_setAeAttr(path_idx, &g_config.AE);
		MPI_setIsoAttr(path_idx, &g_config.DIP_ISO);
		MPI_setAwbAttr(path_idx, &g_config.AWB);
		MPI_setPtaAttr(path_idx, &g_config.PTA);
		MPI_setCsmAttr(path_idx, &g_config.CSM);
		if (g_config.SAMPLE_SHP_SELECT.shp_select == 0) {
			MPI_setShpAttr(path_idx, &g_config.SHP);
		} else {
			MPI_setShpAttrV2(path_idx, &g_config.SHP_V2);
		}
		MPI_setNrAttr(path_idx, &g_config.NR);
		MPI_setTeAttr(path_idx, &g_config.TE);
		MPI_setGammaAttr(path_idx, &g_config.GAMMA);
		MPI_setDipAttr(path_idx, &g_config.DIP);
		MPI_setRoiAttr(path_idx, &g_config.ROI0);
		MPI_setDbcAttr(path_idx, &g_config.DBC0);
		MPI_setDccAttr(path_idx, &g_config.DCC0);
		MPI_setLscAttr(path_idx, &g_config.LSC0);
		MPI_setCalAttr(path_idx, &g_config.CAL0);
		MPI_setEnhAttr(path_idx, &g_config.ENH);
		MPI_setFcsAttr(path_idx, &g_config.FCS);
		MPI_setCoringAttr(path_idx, &g_config.CORING);
		if (g_config.SAMPLE_STAT_CONFIG.enable) {
			MPI_setStatisticsConfig(path_idx, &g_config.STAT);
		}
		if (g_config.hdr_en) {
			MPI_setHdrSynthAttr(path_idx, &g_config.HDR_SYNTH);
		}
		break;
	case DIP_STAT_DUAL:
		path_idx = MPI_INPUT_PATH(idx.dev, 0);
		MPI_setAeAttr(path_idx, &g_config.AE0);
		MPI_setIsoAttr(path_idx, &g_config.DIP_ISO0);
		MPI_setAwbAttr(path_idx, &g_config.AWB0);
		MPI_setPtaAttr(path_idx, &g_config.PTA0);
		MPI_setCsmAttr(path_idx, &g_config.CSM0);
		if (g_config.SAMPLE_SHP_SELECT0.shp_select == 0) {
			MPI_setShpAttr(path_idx, &g_config.SHP0);
		} else {
			MPI_setShpAttrV2(path_idx, &g_config.SHP_V2_0);
		}
		MPI_setNrAttr(path_idx, &g_config.NR0);
		MPI_setTeAttr(path_idx, &g_config.TE0);
		MPI_setGammaAttr(path_idx, &g_config.GAMMA0);
		MPI_setDipAttr(path_idx, &g_config.DIP0);
		MPI_setRoiAttr(path_idx, &g_config.ROI0);
		MPI_setDbcAttr(path_idx, &g_config.DBC0);
		MPI_setDccAttr(path_idx, &g_config.DCC0);
		MPI_setLscAttr(path_idx, &g_config.LSC0);
		MPI_setCalAttr(path_idx, &g_config.CAL0);
		MPI_setEnhAttr(path_idx, &g_config.ENH0);
		MPI_setFcsAttr(path_idx, &g_config.FCS0);
		MPI_setCoringAttr(path_idx, &g_config.CORING0);
		if (g_config.SAMPLE_STAT_CONFIG0.enable) {
			MPI_setStatisticsConfig(path_idx, &g_config.STAT0);
		}

		path_idx = MPI_INPUT_PATH(idx.dev, 1);
		MPI_setAeAttr(path_idx, &g_config.AE1);
		MPI_setIsoAttr(path_idx, &g_config.DIP_ISO1);
		MPI_setAwbAttr(path_idx, &g_config.AWB1);
		MPI_setPtaAttr(path_idx, &g_config.PTA1);
		MPI_setCsmAttr(path_idx, &g_config.CSM1);
		if (g_config.SAMPLE_SHP_SELECT1.shp_select == 0) {
			MPI_setShpAttr(path_idx, &g_config.SHP1);
		} else {
			MPI_setShpAttrV2(path_idx, &g_config.SHP_V2_1);
		}
		MPI_setNrAttr(path_idx, &g_config.NR1);
		MPI_setTeAttr(path_idx, &g_config.TE1);
		MPI_setGammaAttr(path_idx, &g_config.GAMMA1);
		MPI_setDipAttr(path_idx, &g_config.DIP1);
		MPI_setRoiAttr(path_idx, &g_config.ROI1);
		MPI_setDbcAttr(path_idx, &g_config.DBC1);
		MPI_setDccAttr(path_idx, &g_config.DCC1);
		MPI_setLscAttr(path_idx, &g_config.LSC1);
		MPI_setCalAttr(path_idx, &g_config.CAL1);
		MPI_setEnhAttr(path_idx, &g_config.ENH1);
		MPI_setFcsAttr(path_idx, &g_config.FCS1);
		MPI_setCoringAttr(path_idx, &g_config.CORING1);
		if (g_config.SAMPLE_STAT_CONFIG1.enable) {
			MPI_setStatisticsConfig(path_idx, &g_config.STAT1);
		}
		break;
	case DIP_STAT_STITCH:
		path_idx = MPI_INPUT_PATH(idx.dev, 0);
		MPI_setAeAttr(path_idx, &g_config.AE);
		MPI_setIsoAttr(path_idx, &g_config.DIP_ISO);
		MPI_setAwbAttr(path_idx, &g_config.AWB);
		MPI_setPtaAttr(path_idx, &g_config.PTA);
		MPI_setCsmAttr(path_idx, &g_config.CSM);
		if (g_config.SAMPLE_SHP_SELECT.shp_select == 0) {
			MPI_setShpAttr(path_idx, &g_config.SHP);
		} else {
			MPI_setShpAttrV2(path_idx, &g_config.SHP_V2);
		}
		MPI_setNrAttr(path_idx, &g_config.NR);
		MPI_setTeAttr(path_idx, &g_config.TE);
		MPI_setGammaAttr(path_idx, &g_config.GAMMA);
		MPI_setDipAttr(path_idx, &g_config.DIP);
		MPI_setRoiAttr(path_idx, &g_config.ROI0);
		MPI_setDbcAttr(path_idx, &g_config.DBC0);
		MPI_setDccAttr(path_idx, &g_config.DCC0);
		MPI_setLscAttr(path_idx, &g_config.LSC0);
		MPI_setCalAttr(path_idx, &g_config.CAL0);
		MPI_setEnhAttr(path_idx, &g_config.ENH);
		MPI_setFcsAttr(path_idx, &g_config.FCS);
		MPI_setCoringAttr(path_idx, &g_config.CORING);
		if (g_config.SAMPLE_STAT_CONFIG.enable) {
			MPI_setStatisticsConfig(path_idx, &g_config.STAT);
		}

		path_idx = MPI_INPUT_PATH(idx.dev, 1);
		MPI_setRoiAttr(path_idx, &g_config.ROI1);
		MPI_setDbcAttr(path_idx, &g_config.DBC1);
		MPI_setDccAttr(path_idx, &g_config.DCC1);
		MPI_setLscAttr(path_idx, &g_config.LSC1);
		MPI_setCalAttr(path_idx, &g_config.CAL1);
		break;
	default:
		printf("%s(): Invaild DIP_STAT(%d)\n", __func__, g_config.mode);
		break;
	}

	return;
}

/**
 * @brief update the DIP attributes to video device
 */
static void getDipAttr(MPI_DEV idx)
{
	MPI_PATH path_idx;

	/* Get attribute */
	switch (g_config.mode) {
	case DIP_STAT_SINGLE:
		path_idx = MPI_INPUT_PATH(idx.dev, 0);
		MPI_getAeAttr(path_idx, &g_config.AE);
		MPI_getIsoAttr(path_idx, &g_config.DIP_ISO);
		MPI_getAwbAttr(path_idx, &g_config.AWB);
		MPI_getPtaAttr(path_idx, &g_config.PTA);
		MPI_getCsmAttr(path_idx, &g_config.CSM);
		MPI_getShpAttr(path_idx, &g_config.SHP);
		MPI_getShpAttrV2(path_idx, &g_config.SHP_V2);
		MPI_getNrAttr(path_idx, &g_config.NR);
		MPI_getTeAttr(path_idx, &g_config.TE);
		MPI_getGammaAttr(path_idx, &g_config.GAMMA);
		MPI_getDipAttr(path_idx, &g_config.DIP);
		MPI_getRoiAttr(path_idx, &g_config.ROI0);
		MPI_getDbcAttr(path_idx, &g_config.DBC0);
		MPI_getDccAttr(path_idx, &g_config.DCC0);
		MPI_getLscAttr(path_idx, &g_config.LSC0);
		MPI_getCalAttr(path_idx, &g_config.CAL0);
		MPI_getEnhAttr(path_idx, &g_config.ENH);
		MPI_getFcsAttr(path_idx, &g_config.FCS);
		MPI_getCoringAttr(path_idx, &g_config.CORING);
		MPI_getStatisticsConfig(path_idx, &g_config.STAT);
		MPI_getHdrSynthAttr(path_idx, &g_config.HDR_SYNTH);
		break;
	case DIP_STAT_DUAL:
		path_idx = MPI_INPUT_PATH(idx.dev, 0);
		MPI_getAeAttr(path_idx, &g_config.AE0);
		MPI_getIsoAttr(path_idx, &g_config.DIP_ISO0);
		MPI_getAwbAttr(path_idx, &g_config.AWB0);
		MPI_getPtaAttr(path_idx, &g_config.PTA0);
		MPI_getCsmAttr(path_idx, &g_config.CSM0);
		MPI_getShpAttr(path_idx, &g_config.SHP0);
		MPI_getShpAttrV2(path_idx, &g_config.SHP_V2_0);
		MPI_getNrAttr(path_idx, &g_config.NR0);
		MPI_getTeAttr(path_idx, &g_config.TE0);
		MPI_getGammaAttr(path_idx, &g_config.GAMMA0);
		MPI_getDipAttr(path_idx, &g_config.DIP0);
		MPI_getRoiAttr(path_idx, &g_config.ROI0);
		MPI_getDbcAttr(path_idx, &g_config.DBC0);
		MPI_getDccAttr(path_idx, &g_config.DCC0);
		MPI_getLscAttr(path_idx, &g_config.LSC0);
		MPI_getCalAttr(path_idx, &g_config.CAL0);
		MPI_getEnhAttr(path_idx, &g_config.ENH0);
		MPI_getFcsAttr(path_idx, &g_config.FCS0);
		MPI_getCoringAttr(path_idx, &g_config.CORING0);
		MPI_getStatisticsConfig(path_idx, &g_config.STAT0);

		path_idx = MPI_INPUT_PATH(idx.dev, 1);
		MPI_getAeAttr(path_idx, &g_config.AE1);
		MPI_getIsoAttr(path_idx, &g_config.DIP_ISO1);
		MPI_getAwbAttr(path_idx, &g_config.AWB1);
		MPI_getPtaAttr(path_idx, &g_config.PTA1);
		MPI_getCsmAttr(path_idx, &g_config.CSM1);
		MPI_getShpAttr(path_idx, &g_config.SHP1);
		MPI_getShpAttrV2(path_idx, &g_config.SHP_V2_1);
		MPI_getNrAttr(path_idx, &g_config.NR1);
		MPI_getTeAttr(path_idx, &g_config.TE1);
		MPI_getGammaAttr(path_idx, &g_config.GAMMA1);
		MPI_getDipAttr(path_idx, &g_config.DIP1);
		MPI_getRoiAttr(path_idx, &g_config.ROI1);
		MPI_getDbcAttr(path_idx, &g_config.DBC1);
		MPI_getDccAttr(path_idx, &g_config.DCC1);
		MPI_getLscAttr(path_idx, &g_config.LSC1);
		MPI_getCalAttr(path_idx, &g_config.CAL1);
		MPI_getEnhAttr(path_idx, &g_config.ENH1);
		MPI_getFcsAttr(path_idx, &g_config.FCS1);
		MPI_getCoringAttr(path_idx, &g_config.CORING1);
		MPI_getStatisticsConfig(path_idx, &g_config.STAT1);
		break;
	case DIP_STAT_STITCH:
		path_idx = MPI_INPUT_PATH(idx.dev, 0);
		MPI_getAeAttr(path_idx, &g_config.AE);
		MPI_getIsoAttr(path_idx, &g_config.DIP_ISO);
		MPI_getAwbAttr(path_idx, &g_config.AWB);
		MPI_getPtaAttr(path_idx, &g_config.PTA);
		MPI_getCsmAttr(path_idx, &g_config.CSM);
		MPI_getShpAttr(path_idx, &g_config.SHP);
		MPI_getShpAttrV2(path_idx, &g_config.SHP_V2);
		MPI_getNrAttr(path_idx, &g_config.NR);
		MPI_getTeAttr(path_idx, &g_config.TE);
		MPI_getGammaAttr(path_idx, &g_config.GAMMA);
		MPI_getDipAttr(path_idx, &g_config.DIP);
		MPI_getRoiAttr(path_idx, &g_config.ROI0);
		MPI_getDbcAttr(path_idx, &g_config.DBC0);
		MPI_getDccAttr(path_idx, &g_config.DCC0);
		MPI_getLscAttr(path_idx, &g_config.LSC0);
		MPI_getCalAttr(path_idx, &g_config.CAL0);
		MPI_getEnhAttr(path_idx, &g_config.ENH);
		MPI_getFcsAttr(path_idx, &g_config.FCS);
		MPI_getCoringAttr(path_idx, &g_config.CORING);
		MPI_getStatisticsConfig(path_idx, &g_config.STAT);
		path_idx = MPI_INPUT_PATH(idx.dev, 1);
		MPI_getRoiAttr(path_idx, &g_config.ROI1);
		MPI_getDbcAttr(path_idx, &g_config.DBC1);
		MPI_getDccAttr(path_idx, &g_config.DCC1);
		MPI_getLscAttr(path_idx, &g_config.LSC1);
		MPI_getCalAttr(path_idx, &g_config.CAL1);
		break;
	default:
		printf("%s(): Invaild DIP_STAT(%d)\n", __func__, g_config.mode);
		break;
	}

	return;
}

/**
 * @details register default library for AE and AWB, then update the sensor parameters
 */
void SAMPLE_initDipSystem(MPI_PATH idx)
{
	/* Register AE, AWB lib */
	MPI_regAeDftLib(idx);
	MPI_regAwbDftLib(idx);

	/* Get parameter from sensor driver */
	MPI_updateSnsParam(idx);
}

void SAMPLE_exitDipSystem(MPI_PATH idx)
{
	/* Deregister AE, AWB lib */
	MPI_deregAeDftLib(idx);
	MPI_deregAwbDftLib(idx);
}

/**
 * @brief update IQ parameters once by specified .ini
 * @param[in] idx      video device index
 * @param[in] filename name of the .ini file
 */
void SAMPLE_updateIni(MPI_DEV idx, char *filename)
{
	getDevAttr(idx);
	getDipAttr(idx);

	if (!parseIniFile(filename)) {
		setDipAttr(idx);
		printf("updated by ini %s.\n", filename);
	}
}

/**
 * @brief create a thread to track the new IQ parameters if the .ini is updated
 * @param[in] idx      video device index
 * @param[in] filename name of the .ini file
 * @see SAMPLE_destroyIniUpdateThread()
 */
void SAMPLE_createIniUpdateThread(MPI_DEV idx, char *filename)
{
	g_dev.value = idx.value;

	if (access(filename, R_OK) != -1) {
		g_filename = filename;
		g_tid_name = "ini_update";
		g_ini_run  = 1;

		if (pthread_create(&g_tid_ini, NULL, updateIniThread, &g_dev) != 0) {
			printf("Create thread updateIniThread failed.\n");
			return;
		}

		if (pthread_setname_np(g_tid_ini, g_tid_name) != 0) {
			printf("Set thread name to updateIniThread failed.\n");
			return;
		}

		printf("Create thread updateIniThread.\n");
	}

	return;
}

/**
 * @brief stop the thread of tracking the new IQ parameters
 * @param[in] idx      video device index
 * @param[in] filename name of the .ini file
 * @see SAMPLE_destroyIniUpdateThread()
 */
void SAMPLE_destroyIniUpdateThread(MPI_DEV idx)
{
	if (g_filename != NULL) {
		g_ini_run = 0;

		if (pthread_join(g_tid_ini, NULL) != 0) {
			printf("Join thread updateIniThread failed.\n");
			return;
		}

		g_filename = NULL;
		memset(&g_mod_time, 0, sizeof(struct stat));

		printf("Join thread updateIniThread.\n");
	}

	return;
}
