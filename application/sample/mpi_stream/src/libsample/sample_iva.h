#ifndef SAMPLE_IVA_H_
#define SAMPLE_IVA_H_

#ifdef __cplusplus
extern "C" {
#endif /**< __cplusplus */

#define VFTR_UPDATE_TH_NAME "vftr_update"
#define SAMPLE_IVA_RUNNER_MAX (2)

/* Interface function prototype */
void SAMPLE_initAVFTRServ(void);
void SAMPLE_exitAVFTRServ(void);
void SAMPLE_startIva(void);
void SAMPLE_stopIva(void);
void SAMPLE_loadIvaConf(const char *filename);
void SAMPLE_createIvaUpdateThread(const char *filename);
void SAMPLE_destroyIvaUpdateThread(void);

#ifdef __cplusplus
}
#endif /**< __cplusplus */

#endif /**< SAMPLE_SYS_H_ */
