#ifndef SAMPLE_SYS_H_
#define SAMPLE_SYS_H_

#ifdef __cplusplus
extern "C" {
#endif /**< __cplusplus */

#include <pthread.h>

#include "mpi_base_types.h"
#include "mpi_index.h"
#include "mpi_sys.h"
#include "mpi_dev.h"
#include "mpi_enc.h"
#include "mpi_osd.h"
#include "mpi_iva.h"

#define SAMPLE_MAX_INI_FILES (2)
#define SAMPLE_MAX_INPUT_PATH_NUM (2)
#define SAMPLE_MAX_VIDEO_DEV_NUM (1)
#define SAMPLE_MAX_VIDEO_CHN_NUM (4)

#define MAX_IP_LENGTH 32

typedef MPI_STITCH_ATTR_S SAMPLE_STITCH_ATTR_S;

/* MPI unrelated parameters */
typedef struct {
	INT32 sid; /**< Function to execute */
	UINT8 show_params; /**< 0: hide, 1: verbose */
} CONF_CASE_GEN_PARAM;

typedef struct {
	INT32 frame_num;
	UINT8 udpstream_enable;
	UINT8 record_enable;
	char client_ip[MAX_IP_LENGTH];
	UINT32 client_port;
	char output_file[256]; /* TODO */
} CONF_CASE_CHN_PARAM;

typedef struct {
	MPI_OSD_RGN_ATTR_S osd_attr;
	MPI_OSD_BIND_ATTR_S bind; /* TODO */
} CONF_CASE_OSD_PARAM;

/**
 * @struct CONF_SYS_PARAM
 * @brief structure for storing VB configuration
 */
typedef struct {
	MPI_VB_CONF_S vb_conf;
} CONF_SYS_PARAM;

/**
 * @struct CONF_PATH_PARAM
 * @brief structure for storing video path configuration
 */
typedef struct {
	UINT32 path_idx;
	UINT32 ini_cnt;
	char *ini_files[SAMPLE_MAX_INI_FILES];
	MPI_PATH_ATTR_S gen;
} CONF_PATH_PARAM;

typedef struct {
	UINT32 chn_idx;
	MPI_CHN_ATTR_S gen;
	MPI_CHN_LAYOUT_S layout;
	MPI_WIN_ATTR_S win[MPI_MAX_VIDEO_WIN_NUM];

	MPI_STITCH_ATTR_S stitch;
	MPI_LDC_ATTR_S ldc;
	MPI_PANORAMA_ATTR_S panorama;
	MPI_PANNING_ATTR_S panning;
	MPI_SURROUND_ATTR_S surround;

	CONF_CASE_OSD_PARAM osd;
	/* Add Channel attr here */
} CONF_CHN_PARAM;

/**
 * @struct CONF_DEV_PARAM
 * @brief structure for storing configurations about video device,
 * input paths and video channels
 */
typedef struct {
	UINT32 dev_idx;
	MPI_DEV_ATTR_S gen;
	MPI_IVA_OD_PARAM_S obj_det;

	CONF_PATH_PARAM path[MPI_MAX_INPUT_PATH_NUM];
	CONF_CHN_PARAM chn[MPI_MAX_VIDEO_CHN_NUM];
	UINT32 path_cnt;
	UINT32 chn_cnt;
} CONF_DEV_PARAM;

/**
 * @struct CONF_ENC_CHN_PARAM
 * @brief structure for storing video encoder configurations
 */
typedef struct {
	UINT32 chn_idx;
	pthread_t tid;
	MPI_ENC_CHN_ATTR_S chn;
	MPI_ENC_BIND_INFO_S bind_info;
	MPI_VENC_ATTR_S venc;
	MPI_VENC_ATTR_EX_S venc_ex;
	CONF_CASE_CHN_PARAM casep;
} CONF_ENC_CHN_PARAM;

typedef struct {
	CONF_CASE_GEN_PARAM casegen;
	CONF_SYS_PARAM sys;

	bool osd_enable;
	bool iva_enable;
	CONF_DEV_PARAM dev[MPI_MAX_VIDEO_DEV_NUM];
	CONF_ENC_CHN_PARAM enc_chn[MPI_MAX_ENC_CHN_NUM];

	char iva_ini_name[128];
	UINT32 dev_cnt;
	UINT32 enc_chn_cnt;
} SAMPLE_CONF_S;

extern char g_bsb_run[MPI_MAX_ENC_CHN_NUM];

/* Interface function prototype */
INT32 SAMPLE_startEncChn(UINT32 enc_chn_cnt, CONF_ENC_CHN_PARAM *const enc_chn);
INT32 SAMPLE_stopEncChn(UINT32 enc_chn_cnt, CONF_ENC_CHN_PARAM *const enc_chn);
INT32 SAMPLE_startStream(MPI_DEV dev_idx, SAMPLE_CONF_S *const conf);
INT32 SAMPLE_stopStream(MPI_DEV dev_idx, const SAMPLE_CONF_S *conf);

INT32 SAMPLE_initGetStream(void);
INT32 SAMPLE_exitGetStream(void);
INT32 SAMPLE_startGetStream(CONF_ENC_CHN_PARAM *const conf);
void SAMPLE_stopGetStream(CONF_ENC_CHN_PARAM *const conf);

#ifdef __cplusplus
}
#endif /**< __cplusplus */

#endif /**< SAMPLE_SYS_H_ */
