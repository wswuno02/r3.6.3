#include "parse_osd.h"

#include <stdio.h>
#include <string.h>

#include "parse_utils.h"

static void get_osd_color(MPI_OSD_OVERLAY_E *dest)
{
	char *val = strtok(NULL, " =\n");

	parse_str_to_upper(val);

	if (!strcmp(val, "RED")) {
		*dest = 0x8A6E;
	} else if (!strcmp(val, "YELLOW")) {
		*dest = 0x9F18;
	} else if (!strcmp(val, "YELLOW1")) {
		*dest = 0x993B;
	} else if (!strcmp(val, "GREEN")) {
		*dest = 0x9F21;
	} else if (!strcmp(val, "BLUE")) {
		*dest = 0x84E7;
	} else if (!strcmp(val, "BLUE1")) {
		*dest = 0x9F91;
	} else if (!strcmp(val, "PURPLE")) {
		*dest = 0x93AA;
	} else if (!strcmp(val, "PINK")) {
		*dest = 0x8DDE;
	} else {
		printf("ERROR: Invalid precision (%s)\n", val);
	}

	return;
}

void get_osd_type(MPI_OSD_OVERLAY_E *dest)
{
	char *val = strtok(NULL, " =\n");

	parse_str_to_upper(val);

	if (!strcmp(val, "RECTANGLE")) {
		*dest = MPI_OSD_OVERLAY_POLYGON;
	} else if (!strcmp(val, "LINE")) {
		*dest = MPI_OSD_OVERLAY_LINE;
	} else if (!strcmp(val, "BITMAP")) {
		*dest = MPI_OSD_OVERLAY_BITMAP;
	} else if (!strcmp(val, "BITMAP_BYPASS")) {
		*dest = MPI_OSD_OVERLAY_BITMAP_BYPASS;
	} else {
		printf("ERROR: Invalid precision (%s)\n", val);
	}

	return;
}

void print_osd_attr(CONF_CASE_OSD_PARAM *osd)
{
	printf("OSD Attributes:\n");
	printf("OSD type = %d.\n", osd->osd_attr.osd_type);
	printf("OSD x = %d y = %d width = %d height = %d.\n", osd->bind.point.x, osd->bind.point.y,
	       osd->osd_attr.size.width, osd->osd_attr.size.height);

	switch (osd->osd_attr.osd_type) {
	case MPI_OSD_OVERLAY_POLYGON:
		printf("OSD color %x.\n", osd->osd_attr.polygon.color);
		break;
	case MPI_OSD_OVERLAY_LINE:
		printf("OSD color %x.\n", osd->osd_attr.line.color);
		break;
	case MPI_OSD_OVERLAY_BITMAP:
		break;
	default:
		break;
	}

	printf("\n");
}

int parse_osd_polygon_attr(char *tok, MPI_OSD_POLYGON_ATTR_S *p)
{
	int hit = 1;

	if (!strcmp(tok, "osd_color")) {
		get_osd_color((void *)&p->color);
	} else {
		hit = 0;
	}

	return hit;
}

int parse_osd_line_attr(char *tok, MPI_OSD_LINE_ATTR_S *p)
{
	int hit = 1;

	if (!strcmp(tok, "osd_color")) {
		get_osd_color((void *)&p->color);
	} else {
		hit = 0;
	}

	return hit;
}