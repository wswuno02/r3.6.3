#ifndef PARSE_OSD_H_
#define PARSE_OSD_H_

#ifdef __cplusplus
extern "C" {
#endif /* !__cplusplus */

#include "mpi_osd.h"

#include "sample_sys.h"

void get_osd_type(MPI_OSD_OVERLAY_E *dest);
void print_osd_attr(CONF_CASE_OSD_PARAM *osd);
int parse_osd_polygon_attr(char *tok, MPI_OSD_POLYGON_ATTR_S *p);
int parse_osd_line_attr(char *tok, MPI_OSD_LINE_ATTR_S *p);

#ifdef __cplusplus
}
#endif /* !__cplusplus */

#endif /* !PARSE_OSD_H_ */