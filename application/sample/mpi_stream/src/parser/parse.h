#ifndef PARSE_H_
#define PARSE_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "sample_sys.h"

/* Note if the order of enum change, please also modify case configuration file */
enum sample_case_enum {
	SAMPLE_CASE_NONE = 0,
	SAMPLE_CASE_NORMAL_STREAM = 1,
	SAMPLE_CASE_NUM,
};

int parse(int argc, char *argv[], SAMPLE_CONF_S *conf);
void init_conf(SAMPLE_CONF_S *conf);
void help(char *str);
void show_result(SAMPLE_CONF_S *conf);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* !PARSE_H_ */
