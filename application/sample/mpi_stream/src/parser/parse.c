#include "parse.h"

#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "parse_dev.h"
#include "parse_enc.h"
#include "parse_osd.h"
#include "parse_utils.h"
#include "parse_sys.h"

static int parse_case_param(char *tok, SAMPLE_CONF_S *conf)
{
	int hit = 1;
	CONF_CASE_GEN_PARAM *p = &conf->casegen;

	if (!strcmp(tok, "sid")) {
		get_value((void *)&p->sid, TYPE_INT16);
	} else if (!strcmp(tok, "show_params")) {
		get_value((void *)&p->show_params, TYPE_UINT8);
	} else {
		hit = 0;
	}

	return hit;
}

static int parse_iva_param(char *tok, SAMPLE_CONF_S *conf)
{
	int hit = 1;
	bool *iva_enable = &conf->iva_enable;

	if (!strcmp(tok, "iva_enable")) {
		get_value((void *)iva_enable, TYPE_UINT8);
	} else {
		hit = 0;
	}

	return hit;
}

/**
 * @brief parse 1 parameter
 * @param[in]  str     string in format "key=value"
 * @param[out] conf    pointer to configuration struct
 * @return The execution result
 */
static int parse_param(char *str, SAMPLE_CONF_S *conf)
{
	int hit = 0;
	char *tok = strtok(str, " =");

	while (tok != NULL) {
		hit = 0;

		/* Stop parsing this line when comment sign found */
		if (!strncmp(tok, "#", strlen("#"))) {
			hit = 1;
			break;
		}

		/* Parse case general parameter */
		hit = parse_case_param(tok, conf);
		if (hit) {
			goto next;
		}

		/* Parse system parameter */
		hit = parse_sys_param(tok, conf);
		if (hit) {
			goto next;
		}

		/* Parse device parameter */
		hit = parse_dev_param(tok, conf);
		if (hit) {
			goto next;
		}

		/* Parse encoder parameter */
		hit = parse_enc_chn_param(tok, conf);
		if (hit) {
			goto next;
		}

		hit = parse_iva_param(tok, conf);
		if (hit) {
			goto next;
		}

		if (!hit) {
			/* Bypass error for newline sign */
			if (!strcmp(tok, "\n")) {
				hit = 1;
			} else {
				/* Stop parsing when unknown parameter found */
				printf("Unknown parameter: %s\n", tok);
				break;
			}
		}

	next:
		/* Parse other parameters in same line */
		tok = strtok(NULL, " =");
	}

	return hit;
}

/**
 * @brief parse the configuartion file
 * @param[in] filename name of the configuration file
 * @param[out] conf    pointer to configuration struct
 * @return The execution result
 * @retval 0        nothing is parsed
 * @retval positive parsed
 * @retval negative error occurs
 */
static int parse_config_file(char *filename, SAMPLE_CONF_S *conf)
{
	int ret = 0;
	char str[256];
	FILE *fp;

	/* Open config file for parsing */
	fp = fopen(filename, "r");
	if (fp == NULL) {
		printf("Error opening file. err: %d\n", -errno);
		return -EINVAL;
	}

	/* Parse config file line by line */
	while (fgets(str, sizeof(str), fp) != NULL) {
		ret = parse_param(str, conf);

		/* Stop parsing when a unknown parameter found */
		if (!ret) {
			printf("Parsing parameter file failed.\n");
			break;
		}
	}

	fclose(fp);

	return ret;
}

/**
 * @brief parse the .ini name
 * @details the filename will saved at configuration video path 0
 * @param[in] filename name of the .ini file
 * @param[out] conf    pointer to configuration struct
 * @return The execution result
 * @retval 0        nothing is parsed, or error occurs
 * @retval positive parsed
 */
static int parse_ini_name(char *filename, SAMPLE_CONF_S *conf)
{
	UINT32 *idx = &(conf->dev[0].path[0].ini_cnt);
	char *name = conf->dev[0].path[0].ini_files[*idx];

	if (*idx >= SAMPLE_MAX_INI_FILES) {
		printf("Support at most %d ini files\n", SAMPLE_MAX_INI_FILES);
		return 0;
	}

	if (access(filename, F_OK) == -1) {
		printf("file %s doesn't exist\n", filename);
		return 0;
	}

	memcpy(name, filename, 128);
	(*idx)++;
	return 1;
}

/**
 * @brief parse the .ini name
 * @details the filename will saved at configuration video path 0
 * @param[in] filename name of the .ini file
 * @param[out] conf    pointer to configuration struct
 * @return The execution result
 * @retval 0        nothing is parsed, or error occurs
 * @retval positive parsed
 */
static int parse_iva_ini_name(char *filename, SAMPLE_CONF_S *conf)
{
	char *name = conf->iva_ini_name;

	if (access(filename, F_OK) == -1) {
		printf("file %s doesn't exist\n", filename);
		return 0;
	}

	strncpy(name, filename, 128);
	return 1;
}

/**
 * @brief print the content of SAMPLE_CONF_S to stdout
 * @param[in] conf    pointer to configuration struct
 */
void show_result(SAMPLE_CONF_S *conf)
{
	int i, j, k;

	printf("Case general parameters:\n");
	printf("sid = %d, show_params = %d.\n", conf->casegen.sid, conf->casegen.show_params);
	printf("\n");

	printf("IVA enable: %d\n\n", conf->iva_enable);

	printf("VB config:\n");
	printf("max pool cnt = %d.\n", conf->sys.vb_conf.max_pool_cnt);
	for (i = 0; i < MPI_MAX_PUB_POOL; ++i) {
		printf("VB Pool %d:\n", i);
		printf("block size = %d, block cnt = %d, name = %s.\n", conf->sys.vb_conf.pub_pool[i].blk_size,
		       conf->sys.vb_conf.pub_pool[i].blk_cnt, conf->sys.vb_conf.pub_pool[i].name);
	}

	printf("\n");

	for (i = 0; i < conf->dev_cnt; ++i) {
		CONF_DEV_PARAM *dev = &conf->dev[i];

		printf("Video device %d:\n", dev->dev_idx);
		printf("stitch_en = %d, eis_en = %d, hdr_mode = %d, input_frame_rate = %f, bayer = %d.\n",
		       dev->gen.stitch_en, dev->gen.eis_en, dev->gen.hdr_mode, dev->gen.fps, dev->gen.bayer);
		printf("\n");

		for (j = 0; j < dev->path_cnt; ++j) {
			CONF_PATH_PARAM *path = &dev->path[j];
			printf("Input path %d:\n", path->path_idx);
			printf("sensor index = %d, res= %dx%d.\n", path->gen.sensor_idx, path->gen.res.width,
			       path->gen.res.height);
		}
		printf("\n");

		for (j = 0; j < dev->chn_cnt; ++j) {
			CONF_CHN_PARAM *chn = &conf->dev[i].chn[j];

			printf("Video channel %d:\n", chn->chn_idx);
			printf("res = %dx%d, fps = %f.\n", chn->gen.res.width, chn->gen.res.height, chn->gen.fps);

			printf("Channel layout:\n");
			print_chn_layout(&chn->layout);
			printf("\n");

			printf("Window Attribute:\n");
			for (k = 0; k < chn->layout.window_num; ++k) {
				print_window_attr(k, &chn->win[k]);
			}
			printf("\n");

			print_stitch_attr(&chn->stitch);
			print_ldc_attr(&chn->ldc);
			print_panorama_attr(&chn->panorama);
			print_panning_attr(&chn->panning);
			print_surround_attr(&chn->surround);

			print_osd_attr(&chn->osd);

			//print_vftr_attr(&chn->vftr);

			printf("\n");
		}
	}

	for (i = 0; i < conf->enc_chn_cnt; ++i) {
		CONF_ENC_CHN_PARAM *enc_chn = &conf->enc_chn[i];

		printf("Encoder channel %d:\n", enc_chn->chn_idx);
		printf("General param:\n");
		printf("udpstream = %d, record = %d.\n", enc_chn->casep.udpstream_enable,
		       enc_chn->casep.record_enable);
		printf("client_ip = %s, client_port = %d.\n", enc_chn->casep.client_ip,
		       enc_chn->casep.client_port);
		printf("output_file = %s, frame_num = %d\n", enc_chn->casep.output_file,
		       enc_chn->casep.frame_num);
		printf("Channel attr:\n");
		printf("res = %dx%d.\n", enc_chn->chn.res.width, enc_chn->chn.res.height);
		printf("Binding info:\n");
		printf("bind_dev_idx = %d, bind_chn_idx = %d.\n", enc_chn->bind_info.idx.dev,
		       enc_chn->bind_info.idx.chn);
		printf("Encoder: type = %d\n", enc_chn->venc.type);
		print_venc_attr(&enc_chn->venc);
		printf("Encoder extension:\n");
		printf("obs = %u, obs_off_period = %u.\n", enc_chn->venc_ex.obs, enc_chn->venc_ex.obs_off_period);

		printf("\n");
	}
}

/**
 * @brief Initialize SAMPLE_CONF_S
 * @details
 * Rules to initialize configurations:
 *  - Enable bit always set to 0.
 *  - Use common / normal setting for enum. ex: op_mode = MPI_OP_MODE_NORMAL.
 * @param[out] conf    pointer to configuration struct
 */
void init_conf(SAMPLE_CONF_S *conf)
{
	CONF_CASE_GEN_PARAM *casegen;
	CONF_DEV_PARAM *dev;
	CONF_CHN_PARAM *chn;
	MPI_WIN_ATTR_S *win;
	int i, j, k;

	conf->dev_cnt = 0;
	conf->enc_chn_cnt = 0;
	conf->osd_enable = true;

	casegen = &conf->casegen;

	casegen->sid = SAMPLE_CASE_NONE; /* exception */
	casegen->show_params = 0;

	init_sys_conf(&conf->sys);

	for (i = 0; i < MPI_MAX_VIDEO_DEV_NUM; ++i) {
		dev = &conf->dev[i];

		dev->dev_idx = -1;
		dev->path_cnt = 0;
		dev->chn_cnt = 0;

		dev->gen.hdr_mode = MPI_HDR_MODE_NONE;
		dev->gen.stitch_en = 0;
		dev->gen.eis_en = 0;
		dev->gen.fps = 0.0;
		dev->gen.bayer = MPI_BAYER_PHASE_NUM;
		dev->gen.path.bmp = 0x0;

		for (j = 0; j < MPI_MAX_INPUT_PATH_NUM; ++j) {
			init_path_conf(&dev->path[j]);
		}

		for (j = 0; j < MPI_MAX_VIDEO_CHN_NUM; ++j) {
			chn = &dev->chn[j];
			init_chn_conf(chn);

			for (k = 0; k < MPI_MAX_VIDEO_WIN_NUM; ++k) {
				win = &dev->chn[j].win[k];
				init_win_conf(win);
			}
		}
	}

	for (i = 0; i < MPI_MAX_ENC_CHN_NUM; ++i) {
		init_enc_chn_conf(&conf->enc_chn[i]);
	}
}

/**
 * @brief Print help messages to stdout
 * @param[in] str    name of executable
 */
void help(char *str)
{
	printf("USAGE:\n");
	printf("\t%s [-h] [-d case_config] [-s sensorIni0]...[-s sensorIni1] [-p Param1=Value1]...[-p ParamM=ValueM] "
#ifdef SAMPLE_AVFTR_ENABLE
	       "[-i ivaIni]"
#endif /* !SAMPLE_AVFTR_ENABLE */
	       "\n",
	       str);
	printf("\n");
	printf("EXAMPLE:\n");
	printf("\t%s -d case_config/case_config_1\n", str);
	printf("\n");
	printf("OPTION:\n");
	printf("\t'-d' Select configuration file\n");
	printf("\n");
	printf("\t'-s' Select sensor ini file (default /system/mpp/script/sensor_0.ini)\n");
	printf("\n");
	printf("\t'-p' Set parameter <ParamM> to <ValueM>\n");
	printf("\n");
#ifdef SAMPLE_OSD_ENABLE
	printf("\t'-o' Hide OSD\n");
	printf("\n");
#endif /* !SAMPLE_OSD_ENABLE */
#ifdef SAMPLE_AVFTR_ENABLE
	printf("\t'-i' Select iva config ini file (default /system/mpp/script/iva.ini)\n");
	printf("\n");
#endif /* !SAMPLE_OSD_ENABLE */
	printf("\t'-h' Help\n");
	printf("\n");
}

/**
 * @brief parse the command arguments and convert as configuration struct
 * @param[in] argc     argument count
 * @param[in] argv     argument vector
 * @param[out] conf    pointer to configuration struct
 * @return The execution result
 * @retval 0        nothing is parsed or some error occurs
 * @retval positive parsed
 */
int parse(int argc, char *argv[], SAMPLE_CONF_S *conf)
{
	int ret = 0;
	int c;

	while ((c = getopt(argc, argv, "d:s:p:i:oh")) != -1) {
		switch (c) {
		case 'd':
			/* Parse parameter from config file */
			ret = parse_config_file(optarg, conf);
			if (!ret) {
				return ret;
			}

			break;

		case 's':
			/* Parse DIP attributes from .ini file */
			ret = parse_ini_name(optarg, conf);
			if (!ret) {
				return ret;
			}

			break;

		case 'i':
			ret = parse_iva_ini_name(optarg, conf);
			if (!ret) {
				return ret;
			}
			break;

		case 'p':
			/* Parse parameter from command line */
			ret = parse_param(optarg, conf);
			if (!ret) {
				return ret;
			}

			break;

		case 'o':
			conf->osd_enable = false;
			break;

		case 'h':
			help(argv[0]);
			return 0;

		case ':':
			printf("oops\n");
			break;

		case '?':
			if ((optopt == 'd') || (optopt == 'p')) {
				printf("Option -%c requires an argument.\n", optopt);
			} else if (isprint(optopt)) {
				printf("Unknown option '-%c'.\n", optopt);
			} else {
				printf("Unknow option character '\\x%x'.\n", optopt);
			}

		default:
			return 0;
		}
	}

	return ret;
}