#include "gtest/gtest.h"
#include "parse.h"

#include <unistd.h>
#include <getopt.h>

#define MAX_ARG_LENGTH 128

extern int g_argc;
extern char** g_argv;

/**
 * @brief Verify the correctness of libparser.
 * @details This library parse the command line arguments by getopt() 
 * family. This test fixture is available to run on simulation 
 * environment (for example: QEMU)
 */
class TEST_PARSER : public ::testing::Test
{
protected:
	void SetUp()
	{
		init_conf(&conf);
		optind = 1;
	}

	void TearDown() 
	{
		if (argc && argv) {
			reset_argv();
		}
	}

	SAMPLE_CONF_S conf;
	int argc;
	char **argv;

private:
	void reset_argv()
	{
		for (int i = 0; i < argc; ++i) {
			if (argv[i]) {
				delete argv[i];
			}
		}
		delete argv;

		argc = 0;
		argv = NULL;
	}
};

/**
 * @brief verification of prasing nothing
 */
TEST_F(TEST_PARSER, parse_nothing)
{
	argc = 1;
	argv = new char*[argc];
	for (int i = 0; i < argc; ++i) {
		argv[i] = new char[MAX_ARG_LENGTH];
	}

	strcpy(argv[0], g_argv[0]);

	EXPECT_EQ(0, parse(argc, argv, &conf))
		<< "parse() should return 0 to indicate that no input arguments are provided" << std::endl;
}

/**
 * @brief verification of prasing "-d <path_to_case_config>"
 */
TEST_F(TEST_PARSER, parse_case_config)
{
	argc = 3;
	argv = new char*[argc];
	for (int i = 0; i < argc; ++i) {
		argv[i] = new char[MAX_ARG_LENGTH];
	}

	strcpy(argv[0], g_argv[0]);
	strcpy(argv[1], "-d");
	strcpy(argv[2], "case_config_sample");

	EXPECT_EQ(1, parse(argc, argv, &conf))
		<< "parse() should return 0 to indicate that argument(s) is provided" << std::endl;

	strcpy(argv[1], "-d");
	strcpy(argv[2], "case_config_not_exist");

	EXPECT_EQ(0, parse(argc, argv, &conf))
		<< "parse() should return -1 to indicate that some argument(s) are invalid" << std::endl;

}

/**
 * @brief verification of prasing multiple "-d <path_to_case_config>"
 */
TEST_F(TEST_PARSER, parse_multi_case_config)
{
	argc = 5;
	argv = new char*[argc];
	for (int i = 0; i < argc; ++i) {
		argv[i] = new char[MAX_ARG_LENGTH];
	}

	strcpy(argv[0], g_argv[0]);
	strcpy(argv[1], "-d");
	strcpy(argv[2], "case_config_sample");
	strcpy(argv[3], "-d");
	strcpy(argv[4], "case_config_sample");

	EXPECT_EQ(1, parse(argc, argv, &conf))
		<< "parse() should return 0 to indicate that argument(s) is provided" << std::endl;
}

/**
 * @brief verification of prasing "-s <path_to_sensor_ini>"
 */
TEST_F(TEST_PARSER, parse_sensor_ini_path)
{
	argc = 3;
	argv = new char*[argc];
	for (int i = 0; i < argc; ++i) {
		argv[i] = new char[MAX_ARG_LENGTH];
	}

	strcpy(argv[0], g_argv[0]);
	strcpy(argv[1], "-s");
	strcpy(argv[2], "sensor_0_sample.ini");

	EXPECT_EQ(1, parse(argc, argv, &conf))
		<< "parse() should return 1 to indicate that argument(s) is provided" << std::endl;
	EXPECT_EQ(1, conf.dev[0].path[0].ini_cnt);
	EXPECT_STREQ(argv[2], conf.dev[0].path[0].ini_files[0]);
}

/**
 * @brief verification of prasing "-s <path_to_sensor_ini>" with wrong file name
 */
TEST_F(TEST_PARSER, parse_sensor_ini_path_not_exist)
{
	argc = 3;
	argv = new char*[argc];
	for (int i = 0; i < argc; ++i) {
		argv[i] = new char[MAX_ARG_LENGTH];
	}

	strcpy(argv[0], g_argv[0]);
	strcpy(argv[1], "-s");
	strcpy(argv[2], "sensor_not_exist.ini");

	EXPECT_EQ(0, parse(argc, argv, &conf))
		<< "parse() should return 1 to indicate that argument(s) is provided" << std::endl;
	EXPECT_EQ(0, conf.dev[0].path[0].ini_cnt);
}

/**
 * @brief verification of prasing "-s <path_to_sensor_ini> -s <path_to_sensor_ini>"
 */
TEST_F(TEST_PARSER, parse_multi_sensor_ini)
{
	argc = 5;
	argv = new char*[argc];
	for (int i = 0; i < argc; ++i) {
		argv[i] = new char[MAX_ARG_LENGTH];
	}

	strcpy(argv[0], g_argv[0]);
	strcpy(argv[1], "-s");
	strcpy(argv[2], "sensor_0_sample.ini");
	strcpy(argv[3], "-s");
	strcpy(argv[4], "sensor_1_sample.ini");

	EXPECT_EQ(1, parse(argc, argv, &conf))
		<< "parse() should return 1 to indicate that argument(s) is provided" << std::endl;
	EXPECT_EQ(2, conf.dev[0].path[0].ini_cnt);
	EXPECT_STREQ(argv[2], conf.dev[0].path[0].ini_files[0]);
	EXPECT_STREQ(argv[4], conf.dev[0].path[0].ini_files[1]);
}

/**
 * @brief verification of prasing "-s <path_to_sensor_ini> -s <path_to_sensor_ini>"
 */
TEST_F(TEST_PARSER, parse_multi_sensor_ini_2)
{
	argc = 7;
	argv = new char*[argc];
	for (int i = 0; i < argc; ++i) {
		argv[i] = new char[MAX_ARG_LENGTH];
	}

	strcpy(argv[0], g_argv[0]);
	strcpy(argv[1], "-s");
	strcpy(argv[2], "sensor_0_sample.ini");
	strcpy(argv[3], "-s");
	strcpy(argv[4], "sensor_1_sample.ini");
	strcpy(argv[5], "-s");
	strcpy(argv[6], "sensor_0_sample.ini");

	EXPECT_EQ(0, parse(argc, argv, &conf));
}

/**
 * @brief verification of prasing "-p<key>=<value>"
 */
TEST_F(TEST_PARSER, parse_single_param)
{
	argc = 2;
	argv = new char*[argc];
	for (int i = 0; i < argc; ++i) {
		argv[i] = new char[MAX_ARG_LENGTH];
	}

	strcpy(argv[0], g_argv[0]);
	strcpy(argv[1], "-pudp_enable=1");
	
	EXPECT_EQ(0, parse(argc, argv, &conf))
		<< "parse() should return 0 to indicate that argument(s) is provided" << std::endl;

	strcpy(argv[1], "-pinvalidkey=anything");

	EXPECT_EQ(0, parse(argc, argv, &conf))
		<< "parse() should return -1 to indicate that some argument(s) are invalid" << std::endl;

}

/**
 * @brief verification of prasing combinations
 */
TEST_F(TEST_PARSER, parse_combination)
{
	argc = 6;
	argv = new char*[argc];
	for (int i = 0; i < argc; ++i) {
		argv[i] = new char[MAX_ARG_LENGTH];
	}

	strcpy(argv[0], g_argv[0]);
	strcpy(argv[1], "-d");
	strcpy(argv[2], "case_config_sample");
	strcpy(argv[3], "-s");
	strcpy(argv[4], "sensor_0_sample.ini");
	strcpy(argv[5], "-precord_enable=1");
	
	EXPECT_EQ(1, parse(argc, argv, &conf))
		<< "parse() should return 0 to indicate that argument(s) is provided" << std::endl;
}
