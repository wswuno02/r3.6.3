SDKSRC_DIR ?= $(realpath $(CURDIR)/../../../../)
include $(SDKSRC_DIR)/application/internal.mk

# specify build flags
MAKEFLAGS += -j

# include build options
KCONFIG_CONFIG ?= $(APP_PATH)/.config
-include $(KCONFIG_CONFIG)

# specify build tools
CC = $(CROSS_COMPILE)gcc
AR = $(CROSS_COMPILE)ar

# specify CFLAGS
CFLAGS = -Wall -Werror -g -O2 ${PERF_NO_OMIT_FP} -std=gnu99 -MMD -MP
ifeq ($(CONFIG_APP_MPI_STREAM_USE_LIBOSD),y)
ifeq ($(CONFIG_APP_MPI_STREAM_INSTALL_SIMPLIFIED_CHINESE), y)
CFLAGS += -DINSTALL_SIMPLIFIED_OTF
else
CFLAGS += -DINSTALL_TRADITIONAL_OTF
endif
endif

# paths
root = $(realpath $(CURDIR)/..)

SAMPLE    := $(root)/src/libsample
PARSER    := $(root)/src/parser
INIPARSER := $(root)/src/iniparser

SRC_CONFIG_PATH    := $(root)/case_config
SRC_ASSETS_PATH    := $(root)/assets
TARGET_CONFIG_PATH := $(SYSTEMFS)/mpp/case_config
TARGET_ASSETS_PATH := $(SYSTEMFS)/mpp/font

SRCS :=
INCS :=
LIBS :=
LIBDIRS :=

# specify product specific parameters
SNS := $(shell echo $(SENSOR) | tr '[A-Z]' '[a-z]')

DEFS := SNS=$(SNS) SNS0_ID=0
ifneq ($(SENSOR1),)
DEFS += SNS1=$(SENSOR1) SNS1_ID=1
endif

SENSOR_LIBS := sensor_0

ifneq ($(SENSOR1),)
ifneq ($(SENSOR1),$(SENSOR))
    SENSOR_LIBS += sensor_1
endif
endif

ifeq ($(CONFIG_APP_MPI_STREAM_ENABLE_OSD),y)
ifeq ($(CONFIG_APP_MPI_STREAM_USE_LIBOSD),y)
DEFS += LIB_OSD_ENABLE
else
DEFS += SAMPLE_OSD_ENABLE
endif
endif

ifeq ($(CONFIG_APP_MPI_STREAM_ENABLE_AVFTR),y)
DEFS += SAMPLE_AVFTR_ENABLE
INCS += $(LIBAVFTR_INC) \
        $(LIBEAIF_INC) \
        $(FEATURE_AUDIO_INC_PATH) \
        $(FEATURE_VIDEO_INC_PATH)
LIBS += avftr \
        vftr \
        aftr \
        eaif \
        inf \
        json-c \
        curl \
        ssl \
        crypto \
        z \
        ado \
        asound
LIBDIRS += $(LIBAVFTR_LIB) \
           $(LIBEAIF_LIB) \
           $(FEATURE_AUDIO_LIB_PATH) \
           $(FEATURE_VIDEO_LIB_PATH) \
           $(JSON_LIB) \
           $(LIBCURL_LIB) \
           $(OPENSSL_ROOT) \
           $(ZLIB_LIB) \
           $(LIBADO_LIB) \
           $(ALSA_LIB) \
           $(LIBINF_LIB)

ifeq ($(CONFIG_LIBEAIF_INFERENCE_INAPP),y)
ifeq ($(CONFIG_LIBINF_TFLITE),y)
LIBDIRS += $(TFLITE_LIB)
LIBS += stdc++ \
        tensorflow-lite \
        dl
else ifeq ($(CONFIG_LIBINF_TFLITE_OPTIM),y)
LIBDIRS += $(TFLITE_OPTIM_LIB)
LIBS += stdc++ \
        tensorflow-lite-optim \
        dl
else ifeq ($(CONFIG_LIBINF_MICROLITE),y)
LIBDIRS += $(MICROLITE_LIB)
LIBS += stdc++ \
        tensorflow-microlite
endif
endif
endif

# specify source files and include directories
SRCS += $(wildcard $(root)/src/*.c) \
       $(wildcard $(root)/src/iniparser/*.c) \
       $(wildcard $(root)/src/libsample/*.c) \
       $(wildcard $(root)/src/parser/*.c)
INCS += $(SAMPLE) \
       $(PARSER) \
       $(INIPARSER) \
       $(MPP_INC) \
       $(SENSOR_PATH) \
       $(UDPS_INC) \
       $(LIBPRIO_INC)

ifeq ($(CONFIG_LIBOSD),y)
INCS += $(LIBOSD_INC) \
	   $(ZLIB_INC) \
	   $(LIBPNG_INC) \
	   $(SDL2_INC) \
	   $(SDL2TTF_INC) \
	   $(FREETYPE_INC)
endif

LIBS += $(SENSOR_LIBS) \
       mpp \
       pthread \
       m \
       prio \
       udps \
       rt
ifeq ($(CONFIG_APP_MPI_STREAM_USE_LIBOSD),y)
LIBS += osd SDL2 SDL2_ttf z png16 freetype
endif
LIBDIRS += $(MPP_LIB) \
          $(LIBSENSOR_LIB) \
          $(UDPS_LIB) \
          $(FILE_LIB) \
          $(LIBPRIO_PATH)
ifeq ($(CONFIG_APP_MPI_STREAM_USE_LIBOSD),y)
LIBDIRS += $(LIBOSD_LIB) \
          $(FREETYPE_LIB) \
          $(ZLIB_LIB) \
          $(LIBPNG_LIB) \
          $(SDL2_LIB) \
          $(SDL2TTF_LIB)
endif

# [DON'T TOUCH] calculate corresponding object files and auto-dependencies
OBJS = $(SRCS:.c=.o)
DEPS = $(SRCS:.c=.d)

# specify bin
BIN = mpi_stream

# sepcify targets
.DEFAULT_GOAL := default

.PHONY: default
default: all

.PHONY: all
all: $(root)/$(BIN)

$(root)/$(BIN): $(OBJS)
	@printf "  %-8s$@\n" "CC"
	$(Q)$(CC) $(CFLAGS) $^ -o $@ $(addprefix -L,$(LIBDIRS)) $(addprefix -l,$(LIBS))

.PHONY: install
install: $(SYSTEM_BIN)/$(BIN) install-config install-assets

$(SYSTEM_BIN)/$(BIN): $(root)/$(BIN) | $(SYSTEM_BIN)
	@printf "  %-8s$@\n" "INSTALL"
	$(Q)install -m 777 $< $(dir $@)

.PHONY: install-config
install-config: | $(TARGET_CONFIG_PATH)
	@printf "  %-8s$(TARGET_CONFIG_PATH)/*\n" "INSTALL"
	$(Q)$(foreach f, $(wildcard $(SRC_CONFIG_PATH)/*), \
		cp -r $(f) $(TARGET_CONFIG_PATH)/$(notdir $(f)); \
	)

.PHONY: install-assets
install-assets: | $(TARGET_ASSETS_PATH)
	@printf "  %-8s$(TARGET_ASSETS_PATH)/*.ayuv\n" "INSTALL"
	$(Q)$(foreach f, $(wildcard $(SRC_ASSETS_PATH)/*.ayuv), \
		cp -r $(f) $(TARGET_ASSETS_PATH)/$(notdir $(f)); \
	)
	@printf "  %-8s$(TARGET_ASSETS_PATH)/*.imgayuv\n" "INSTALL"
	$(Q)$(foreach f, $(wildcard $(SRC_ASSETS_PATH)/*.imgayuv), \
		cp -r $(f) $(TARGET_ASSETS_PATH)/$(notdir $(f)); \
	)
	@printf "  %-8s$(TARGET_ASSETS_PATH)/palette\n" "INSTALL"
	$(Q)$(foreach f, $(wildcard $(SRC_ASSETS_PATH)/palette), \
		cp -r $(f) $(TARGET_ASSETS_PATH)/$(notdir $(f)); \
	)
ifeq ($(CONFIG_APP_MPI_STREAM_USE_LIBOSD),y)
ifeq ($(CONFIG_APP_MPI_STREAM_INSTALL_SIMPLIFIED_CHINESE), y)
	$(Q)$(foreach f, $(wildcard $(SRC_ASSETS_PATH)/SourceHanSansCN-Regular_1.otf), \
		cp -r $(f) $(TARGET_ASSETS_PATH)/$(notdir $(f)); \
	)
	@printf "  %-8s$(TARGET_ASSETS_PATH)/SourceHanSansCN-Regular_1.otf\n" "INSTALL"
	
else
	$(Q)$(foreach f, $(wildcard $(SRC_ASSETS_PATH)/SourceHanSansTC-Normal.otf), \
		cp -r $(f) $(TARGET_ASSETS_PATH)/$(notdir $(f)); \
	)
	@printf "  %-8s$(TARGET_ASSETS_PATH)/SourceHanSansTC-Normal.otf\n" "INSTALL"

endif
endif

$(SYSTEM_BIN) $(TARGET_CONFIG_PATH) $(TARGET_ASSETS_PATH):
	@printf "  %-8s$@\n" "MKDIR"
	$(Q)mkdir -p $@

.PHONY: uninstall
uninstall: uninstall-config uninstall-assets
	@printf "  %-8s$(SYSTEM_BIN)/$(BIN)\n" "RM"
	$(Q)rm -f $(SYSTEM_BIN)/$(BIN)
	$(Q)if [ -d $(SYSTEMFS)/bin ]; then \
		rmdir --ignore-fail-on-non-empty $(SYSTEMFS)/bin; \
	fi
	$(Q)if [ -d $(SYSTEMFS)/mpp ]; then \
		rmdir --ignore-fail-on-non-empty $(SYSTEMFS)/mpp; \
	fi

.PHONY: uninstall-config
uninstall-config:
	@printf "  %-8s$(TARGET_CONFIG_PATH)\n" "RM"
	$(Q)rm -rf $(TARGET_CONFIG_PATH)

.PHONY: uninstall-assets
uninstall-assets:
	@printf "  %-8s$(TARGET_ASSETS_PATH)\n" "RM"
	$(Q)rm -rf $(TARGET_ASSETS_PATH)

.PHONY: clean
clean:
	@printf "  %-8s$(root)\n" "CLEAN"
	$(Q)rm -f $(root)/$(BIN)
	$(Q)find $(root) -name "*.[ado]" -exec rm -f {} \;

.PHONY: distclean
distclean: uninstall clean

# general directory independent targets
%.o: %.c
	@printf "  %-8s$@\n" "CC"
	$(Q)$(CC) $(CFLAGS) $(addprefix -D,$(DEFS)) $< -c -o $@ $(addprefix -iquote,$(INCS))

# Autodependencies
-include $(DEPS)
