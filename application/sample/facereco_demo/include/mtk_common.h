/******************************************************************************
*
* opyright (c) AUGENTIX Inc. - All Rights Reserved
*
* Unauthorized copying of this file, via any medium is strictly prohibited.
*
* Proprietary and confidential.
*
******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/select.h>
#include <sys/time.h>
#include <errno.h>

#ifndef AUGENTIX_COMMON_H_
#define AUGENTIX_COMMON_H_

#define THREAD_NAME_LEN 16

#define SYS_DEBUG
#ifdef SYS_DEBUG
#define SYS_TRACE(format, args...) printf("[%s:%d] " format, __func__, __LINE__, ##args)
#else
#define SYS_TRACE(args...)
#endif

#endif /* AUGENTIX_COMMON_H_ */
