/**
* @cond
*
* code fragment skipped by Doxygen.
*/
#define _GNU_SOURCE //For pthread_setname_np
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/stat.h> /* For mode constants */
#include <pthread.h>
#include <stdlib.h>
#include <fcntl.h> /* For O_* constants */
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>

#include "agtx_prio.h"

#include "avftr_conn.h"
#include "avftr.h"

#define AVFTR_FMT(fmt) "[AVFTR] " fmt

#define AVFTR_TR(fmt, args...) printf(AVFTR_FMT(fmt), ##args)
#define AVFTR_ERR(fmt, args...) AVFTR_TR(fmt, ##args)
#define AVFTR_WARN(fmt, args...) AVFTR_TR(fmt, ##args)
#define AVFTR_INFO_H(fmt, args...) AVFTR_TR(fmt, ##args)
#define AVFTR_INFO_M(fmt, args...) AVFTR_TR(fmt, ##args)
#define AVFTR_INFO_L(fmt, args...)
#define AVFTR_DBG(fmt, args...)

#define AVFTR_MAX_CLIENT_NUM (3)
#define AVFTR_CONN_WAIT_SEC (0) //sec
#define AVFTR_CONN_WAIT_USEC (1000000) //usec
#define AVFTR_SERV_THREAD_EXIT (0xF)
#define AVFTR_SERV_THREAD_NAME "avftr_srv"

typedef struct {
	int *thread_res;
} AVFTR_SERV_TH_CTX_S;

typedef enum {
	AVFTR_START,
	AVFTR_CLOSING,
	AVFTR_STOP,
} AVFTR_SERV_STATE_E;

// Server
int avftrUnxSktFD; //Unix Socket FD
int avftrResShmFD; //Shared memory for result FD
AVFTR_CTX_S *av_ftr_res_shm;
AVFTR_VIDEO_CTX_S *vftr_res_shm;
AVFTR_AUDIO_CTX_S *aftr_res_shm;
pthread_t avftr_waitclient_tid;
AVFTR_SERV_STATE_E g_avftr_serv_stat = AVFTR_STOP;

/**
 * @brief Clean old file descriptors.
 * @param[in]  fileName    file name.
 * @see none
 * @retval  none.
 */
static void cleanOldFd(const char *fileName)
{
	char cmd[256];

	if (!access(fileName, F_OK)) {
		AVFTR_ERR("Deleting old unix file descriptor.\n");

		if (!remove(fileName)) {
			sprintf(cmd, "rm -f %s", AVFTR_SKT_PATH);
			system(cmd); //force delete
		}
	}
}

/**
 * @brief Close client while it's not connected, update max. update max. of socket fd.
 * @param[in]  client_sd   pointer of client socket descriptor.
 * @param[in]  master_set  pointer of master file descriptor set.
 * @param[in]  max_sd      pointer of maximal number of socket descriptor
 * @see none
 * @retval  none.
 */
static void closeClientConn(int *client_sd, fd_set *master_set, int *max_sd)
{
	close(*client_sd);
	FD_CLR(*client_sd, master_set);
	*client_sd = 0;
	if (*client_sd == *max_sd) {
		while (FD_ISSET(*max_sd, master_set) == 0) {
			*max_sd -= 1;
		}
	}
}

/**
 * @brief Scan all possible client for handling its disconnection.
 * @param[in]  client_sd   pointer of client socket descriptor array.
 * @param[in]  master_set  pointer of master file descriptor set.
 * @param[in]  max_sd      pointer of maximal number of socket descriptor.
 * @see none
 * @retval  none.
 */
static void scanAllClient(int *client_sd, fd_set *working_set, fd_set *master_set, int *max_sd)
{
	int idx, ret, cmd;
	for (idx = 0; idx < AVFTR_MAX_CLIENT_NUM; ++idx) {
		if (FD_ISSET(client_sd[idx], working_set) && (client_sd[idx] != avftrUnxSktFD)) {
			AVFTR_INFO_L("Client %d start recv\n", client_sd[idx]);
			ret = recv(client_sd[idx], &cmd, sizeof(cmd), 0);
			if (ret <= 0) { //ret = 0 for disconnect, < 0 for other err
				AVFTR_INFO_L("Client %d disconnected.\n", client_sd[idx]);
				closeClientConn(&client_sd[idx], master_set, max_sd);
				continue;
			}
		} /* End of existing connection is readable */
	} /* End of for-loop for each client */
}

/**
 * @brief Add new client into client socket array and add into master set, update max. of socket fd.
 * @param[in]  client_sd   pointer of client socket descriptor array.
 * @param[in]  master_set  pointer of master file descriptor set.
 * @param[in]  max_sd      pointer of maximal number of socket descriptor
 * @see none
 * @retval  0  success.
 * @retval -1  failure.
 */
static int addNewClient(int *client_sd, fd_set *master_set, int *max_sd)
{
	struct sockaddr_un sktaddr;
	int addrlen = sizeof(sktaddr);
	int new_sd, idx;

	if ((new_sd = accept(avftrUnxSktFD, (struct sockaddr *)&sktaddr, (socklen_t *)&addrlen)) < 0) {
		AVFTR_INFO_H("Socket accept error\n");
		g_avftr_serv_stat = AVFTR_CLOSING;
		return -1;
	}
	AVFTR_INFO_L("New client %d connected\n", new_sd);
	FD_SET(new_sd, master_set);

	for (idx = 0; idx < AVFTR_MAX_CLIENT_NUM; ++idx) {
		if (client_sd[idx] == 0) {
			client_sd[idx] = new_sd;
			AVFTR_INFO_L("Add new client and max_sd = %d\n", *max_sd);
			break;
		}
	}

	if (idx == AVFTR_MAX_CLIENT_NUM) {
		AVFTR_INFO_H("No empty socket is available\n");
		return -1;
	}

	if (new_sd > *max_sd) {
		*max_sd = new_sd;
		AVFTR_INFO_L("New client, update max_sd = %d\n", *max_sd);
	}

	return 0;
}

/**
 * @brief thread to wait new client and handle client request.
 * @param[in]  thread_res   pointer of thread excution result.
 * @see none
 * @retval  none
 */
static void *waitClientGetRes(void *data)
{
	int max_sd = avftrUnxSktFD;
	int client_sd[AVFTR_MAX_CLIENT_NUM] = { 0 };
	struct timeval tv;
	fd_set master_set, working_set;
	int ret = 0;
	int *thread_res = (int *)data;

	if (setThreadSchedAttr(AVFTR_SERV_THREAD_NAME)) {
		printf("Failed to set thread %s!\n", AVFTR_SERV_THREAD_NAME);
	}

	FD_ZERO(&master_set);
	FD_SET(avftrUnxSktFD, &master_set);

	AVFTR_INFO_M("Server start.\n");

	while (g_avftr_serv_stat != AVFTR_CLOSING) {
		tv.tv_sec = AVFTR_CONN_WAIT_SEC;
		tv.tv_usec = AVFTR_CONN_WAIT_USEC;
		memcpy(&working_set, &master_set, sizeof(master_set));

		/* Select a connection on a socket */
		ret = select(max_sd + 1, &working_set, NULL, NULL, &tv);
		if (ret == 0) { /* Time out */
			AVFTR_INFO_L("Select time out.\n");
			continue;
		} else if (ret < 0) {
			AVFTR_WARN("Server catch signal on select(): %s.\n", strerror(errno));
			continue;
		} else {
			/* Return the number of fd contained in the three returned descriptor sets */
			AVFTR_INFO_L("Select result %d.\n", ret);
		}

		if (FD_ISSET(avftrUnxSktFD, &working_set)) {
			AVFTR_INFO_L("New client detected\n");
			if (addNewClient(client_sd, &master_set, &max_sd) == -1) {
				continue;
			}
		}

		scanAllClient(client_sd, &working_set, &master_set, &max_sd);
	} /* End of while (g_avftr_serv_stat == AVFTR_CLOSING) */

	*(int *)thread_res = AVFTR_SERV_THREAD_EXIT;
	AVFTR_DBG("Leave %s\n", __func__);

	return thread_res;
}
/**
 * @endcond
 */

/**
 * @brief Initialize avftr get result server.
 * @param[in]  none
 * @see VIDEO_FTR_exitServer()
 * @retval  0 success.
 * @retval -1 failure.
 */
int AVFTR_initServer()
{
	int addrlen;
	struct sockaddr_un sktaddr;

	int *thread_res = NULL;
	int opt = 1;

	AVFTR_DBG("Enter %s.\n", __func__);

	if (g_avftr_serv_stat != AVFTR_STOP) {
		AVFTR_ERR("Server is running or was closed incorrectly. Server state = %d\n", g_avftr_serv_stat);
		goto errexit;
	}

	thread_res = malloc(sizeof(int));
	if (thread_res == NULL) {
		AVFTR_WARN("Allocate thread result failed.\n");
		goto erralloc;
	}
	//	thread_res = 0;

	g_avftr_serv_stat = AVFTR_START;

	/* Clean shm first */
	cleanOldFd(AVFTR_RES_PATH);

	/* Open shared memory for result */
	avftrResShmFD = shm_open(AVFTR_RES_PATH, O_CREAT | O_RDWR | O_TRUNC, S_IRUSR | S_IWUSR);

	if (avftrResShmFD == -1) {
		AVFTR_ERR("Open shared memeory for result failed. errno(%d): %s\n", errno, strerror(errno));
		goto erropen;
	}

	/* truncate shared memeory file size */
	ftruncate(avftrResShmFD, AVFTR_RES_SIZE);

	/* Map shared memory object */
	av_ftr_res_shm = (AVFTR_CTX_S *)mmap(0, AVFTR_RES_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, avftrResShmFD, 0);
	if (av_ftr_res_shm == MAP_FAILED) {
		AVFTR_ERR("Map mermory failed\n");
		goto errmmap;
	}
	vftr_res_shm = &av_ftr_res_shm->vftr;
	aftr_res_shm = &av_ftr_res_shm->aftr;

	/* Clean update any orphan socket before starting server */
	cleanOldFd(AVFTR_SKT_PATH);

	addrlen = sizeof(sktaddr);

	/* Type of socket (Unix socket) */
	memset(&sktaddr, 0, addrlen);
	sktaddr.sun_family = AF_UNIX;
	strcpy(sktaddr.sun_path, AVFTR_SKT_PATH);

	/* Create socket */
	if ((avftrUnxSktFD = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
		AVFTR_ERR("Socket creating error.\n");
		goto errskt;
	}

	/* Set master socket to allow multiple connections */
	if (setsockopt(avftrUnxSktFD, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0) {
		AVFTR_ERR("Sockt set opt error.\n");
		goto errsetskt;
	}

	/* Bind the socket to socketfd file */
	if (bind(avftrUnxSktFD, (struct sockaddr *)&sktaddr, addrlen) < 0) {
		AVFTR_ERR("Socket binding error.\n");
		goto errsetskt;
	}

	/* Listen on the socketfile */
	if (listen(avftrUnxSktFD, AVFTR_MAX_CLIENT_NUM) < 0) {
		AVFTR_ERR("Socket listen error\n");
		goto errsetskt;
	}

	if (pthread_create(&avftr_waitclient_tid, NULL, waitClientGetRes, (void *)thread_res) != 0) {
		AVFTR_WARN("Create thread to wait client get result failed.\n");
		goto errsetskt;
	}

	if (pthread_setname_np(avftr_waitclient_tid, AVFTR_SERV_THREAD_NAME) != 0) {
		AVFTR_WARN("Set avftr server thread name failed.\n");
		goto errsetskt;
	}

	AVFTR_DBG("Leave %s.\n", __func__);
	return 0;

errsetskt:
	avftr_waitclient_tid = -1;
	close(avftrUnxSktFD);
	avftrUnxSktFD = -1;
	unlink(AVFTR_SKT_PATH);
errskt:
	munmap(av_ftr_res_shm, AVFTR_RES_SIZE);
	av_ftr_res_shm = NULL;
	vftr_res_shm = NULL;
	aftr_res_shm = NULL;
errmmap:
	close(avftrResShmFD);
	avftrResShmFD = -1;
	shm_unlink(AVFTR_RES_PATH);
erropen:
	g_avftr_serv_stat = AVFTR_STOP;
erralloc:
	if (thread_res != NULL) {
		free(thread_res);
		thread_res = NULL;
	}

errexit:
	AVFTR_DBG("Leave error %s.\n", __func__);
	return -1;
}

/**
 * @brief Exit avftr get result server.
 * @param[in]  none
 * @see VIDEO_FTR_initServer()
 * @retval  0 success.
 * @retval -1 failure.
 */
int AVFTR_exitServer()
{
	int *thread_res;

	AVFTR_DBG("Enter %s.\n", __func__);

	if (g_avftr_serv_stat != AVFTR_START) {
		AVFTR_ERR("Server state is not RUNNING is %d\n", g_avftr_serv_stat);
	}
	g_avftr_serv_stat = AVFTR_CLOSING;

	/* Wait fot server thread return */
	if (pthread_join(avftr_waitclient_tid, (void **)&thread_res) != 0) {
		AVFTR_ERR("Join thread waiting client thread failed.\n");
		goto errexit;
	}

	if (*thread_res == AVFTR_SERV_THREAD_EXIT) {
		AVFTR_INFO_L("Waiting client thread is canceled.\n");
	} else {
		AVFTR_ERR("Waiting client thread isn't exit correctly.\n");
		goto errexit;
	}

errexit:
	if (avftrUnxSktFD != -1) {
		close(avftrUnxSktFD);
		avftrUnxSktFD = -1;
	}
	unlink(AVFTR_SKT_PATH);

	if (av_ftr_res_shm != NULL) {
		munmap(av_ftr_res_shm, AVFTR_RES_SIZE);
		av_ftr_res_shm = NULL;
		vftr_res_shm = NULL;
		aftr_res_shm = NULL;
	}

	if (avftrResShmFD != -1) {
		close(avftrResShmFD);
		avftrResShmFD = -1;
	}
	shm_unlink(AVFTR_RES_PATH);

	if (thread_res != NULL) {
		free(thread_res);
	}

	g_avftr_serv_stat = AVFTR_STOP;

	AVFTR_DBG("Leave %s.\n", __func__);

	return 0;
}

/**
 * @brief Initialize avftr get result client.
 * @param[out]  shm_fd    pointer of result shared memory file descriptor
 * @param[out]  skt_fd    pointer of client socket file descriptor
 * @param[out]  res_shm   pointer of result shared memory address
 * @see VIDEO_FTR_exitClient()
 * @retval  0 success.
 * @retval -1 failure.
 */
int AVFTR_initClient(int *shm_fd, int *skt_fd, AVFTR_CTX_S **res_shm)
{
	int addrlen;
	struct sockaddr_un sktaddr;

	AVFTR_DBG("Enter %s.\n", __func__);

	/* Open shared memory for result */
	*shm_fd = shm_open(AVFTR_RES_PATH, O_RDONLY, S_IRUSR | S_IWUSR);
	if (*shm_fd < 0) {
		AVFTR_ERR("Client open shared memory failed.\n");
		goto errexit;
	}

	/* Map shared memory object */
	*res_shm = (AVFTR_CTX_S *)mmap(0, AVFTR_RES_SIZE, PROT_READ, MAP_SHARED, *shm_fd, 0);
	if (*res_shm == MAP_FAILED) {
		AVFTR_ERR("Map mermory failed\n");
		goto errmmap;
	}

	addrlen = sizeof(sktaddr);

	/* Type of socket (Unix socket) */
	memset(&sktaddr, 0, addrlen);
	sktaddr.sun_family = AF_UNIX;
	strcpy(sktaddr.sun_path, AVFTR_SKT_PATH);

	/* Create socket */
	if ((*skt_fd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
		AVFTR_ERR("Socket creating error\n");
		goto errskt;
	}

	/* Connect the socket to socketfd file */
	if (connect(*skt_fd, (struct sockaddr *)&sktaddr, addrlen) < 0) {
		AVFTR_ERR("Socket connecting error\n");
		goto errout;
	}
	AVFTR_DBG("Leave %s.\n", __func__);
	return 0;
errout:
	close(*skt_fd);
	*skt_fd = -1;
errskt:
	munmap(*res_shm, AVFTR_RES_SIZE);
	*res_shm = NULL;
errmmap:
	close(*shm_fd);
	*shm_fd = -1;
errexit:
	AVFTR_DBG("Leave %s.\n", __func__);
	return -1;
}

/**
 * @brief Exit audio/video feature get result client.
 * @param[in]  shm_fd    pointer of result shared memory file descriptor
 * @param[in]  skt_fd    pointer of client socket file descriptor
 * @param[in]  res_shm   pointer of result shared memory address
 * @see VIDEO_FTR_initClient()
 * @retval  0 success.
 * @retval -1 failure.
 */
int AVFTR_exitClient(int *shm_fd, int *skt_fd, AVFTR_CTX_S **res_shm)
{
	AVFTR_DBG("Enter %s.\n", __func__);

	close(*skt_fd);
	*skt_fd = -1;

	if (*res_shm != NULL) {
		munmap(*res_shm, AVFTR_RES_SIZE);
		*res_shm = NULL;
	}

	close(*shm_fd);
	*shm_fd = -1;

	AVFTR_DBG("Leave %s.\n", __func__);

	return 0;
}
