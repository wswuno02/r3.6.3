mod := $(notdir $(subdir))

app-$(CONFIG_APP_ACTL) += actl
PHONY += actl actl-clean actl-distclean
PHONY += actl-install actl-uninstall
actl:
	$(Q)$(MAKE) -C $(ACTL_PATH) all

actl-clean:
	$(Q)$(MAKE) -C $(ACTL_PATH) clean

actl-distclean:
	$(Q)$(MAKE) -C $(ACTL_PATH) distclean

actl-install:
	$(Q)$(MAKE) -C $(ACTL_PATH) install

actl-uninstall:
	$(Q)$(MAKE) -C $(ACTL_PATH) uninstall

app-$(CONFIG_APP_ANR_CTRL) += anr_ctrl
PHONY += anr_ctrl anr_ctrl-clean anr_ctrl-distclean
PHONY += anr_ctrl-install anr_ctrl-uninstall
anr_ctrl: libado
	$(Q)$(MAKE) -C $(ANR_CTRL_PATH) all

anr_ctrl-clean: libado-clean
	$(Q)$(MAKE) -C $(ANR_CTRL_PATH) clean

anr_ctrl-distclean: libado-distclean
	$(Q)$(MAKE) -C $(ANR_CTRL_PATH) distclean

anr_ctrl-install: libado-install
	$(Q)$(MAKE) -C $(ANR_CTRL_PATH) install

anr_ctrl-uninstall: libado-uninstall
	$(Q)$(MAKE) -C $(ANR_CTRL_PATH) uninstall

app-$(CONFIG_APP_G726) += g726
PHONY += g726 g726-clean g726-distclean
PHONY += g726-install g726-uninstall
g726:
	$(Q)$(MAKE) -C $(G726_PATH)/build all

g726-clean:
	$(Q)$(MAKE) -C $(G726_PATH)/build clean

g726-distclean:
	$(Q)$(MAKE) -C $(G726_PATH)/build distclean

g726-install:
	$(Q)$(MAKE) -C $(G726_PATH)/build install

g726-uninstall:
	$(Q)$(MAKE) -C $(G726_PATH)/build uninstall

app-$(CONFIG_APP_HELLO_APP) += hello_app
PHONY += hello_app hello_app-clean hello_app-distclean
PHONY += hello_app-install hello_app-uninstall
hello_app:
	$(Q)$(MAKE) -C $(HELLO_APP_PATH)/build all

hello_app-clean:
	$(Q)$(MAKE) -C $(HELLO_APP_PATH)/build clean

hello_app-distclean:
	$(Q)$(MAKE) -C $(HELLO_APP_PATH)/build distclean

hello_app-install:
	$(Q)$(MAKE) -C $(HELLO_APP_PATH)/build install

hello_app-uninstall:
	$(Q)$(MAKE) -C $(HELLO_APP_PATH)/build uninstall

app-$(CONFIG_APP_CMD_SENDER) += cmd_sender
PHONY += cmd_sender cmd_sender-clean cmd_sender-distclean
PHONY += cmd_sender-install cmd_sender-uninstall
cmd_sender:
	$(Q)$(MAKE) -C $(CMD_SENDER_PATH) all

cmd_sender-clean:
	$(Q)$(MAKE) -C $(CMD_SENDER_PATH) clean

cmd_sender-distclean:
	$(Q)$(MAKE) -C $(CMD_SENDER_PATH) distclean

cmd_sender-install:
	$(Q)$(MAKE) -C $(CMD_SENDER_PATH) install

cmd_sender-uninstall:
	$(Q)$(MAKE) -C $(CMD_SENDER_PATH) uninstall

app-$(CONFIG_APP_MPI_SNAPSHOT) += mpi_snapshot
PHONY += mpi_snapshot mpi_snapshot-clean mpi_snapshot-distclean
PHONY += mpi_snapshot-install mpi_snapshot-uninstall
mpi_snapshot:
	$(Q)$(MAKE) -C $(MPI_SNAPSHOT_PATH) all

mpi_snapshot-clean:
	$(Q)$(MAKE) -C $(MPI_SNAPSHOT_PATH) clean

mpi_snapshot-distclean:
	$(Q)$(MAKE) -C $(MPI_SNAPSHOT_PATH) distclean

mpi_snapshot-install:
	$(Q)$(MAKE) -C $(MPI_SNAPSHOT_PATH) install

mpi_snapshot-uninstall:
	$(Q)$(MAKE) -C $(MPI_SNAPSHOT_PATH) uninstall

app-$(CONFIG_APP_MPI_STREAM) += mpi_stream
PHONY += mpi_stream mpi_stream-clean mpi_stream-distclean
PHONY += mpi_stream-install mpi_stream-uninstall
mpi_stream: libprio libfsink
	$(Q)$(MAKE) -C $(MPI_STREAM_PATH)/build all

mpi_stream-clean: libfsink-clean libprio-clean
	$(Q)$(MAKE) -C $(MPI_STREAM_PATH)/build clean

mpi_stream-distclean: libfsink-distclean libprio-distclean
	$(Q)$(MAKE) -C $(MPI_STREAM_PATH)/build distclean

mpi_stream-install: libprio-install libfsink-install
	$(Q)$(MAKE) -C $(MPI_STREAM_PATH)/build install

mpi_stream-uninstall: libfsink-uninstall libprio-uninstall
	$(Q)$(MAKE) -C $(MPI_STREAM_PATH)/build uninstall

app-$(CONFIG_APP_REQUEST_IDR) += request_idr
PHONY += request_idr request_idr-clean request_idr-distclean
PHONY += request_idr-install request_idr-uninstall
request_idr:
	$(Q)$(MAKE) -C $(REQUEST_IDR_PATH) all

request_idr-clean:
	$(Q)$(MAKE) -C $(REQUEST_IDR_PATH) clean

request_idr-distclean:
	$(Q)$(MAKE) -C $(REQUEST_IDR_PATH) distclean

request_idr-install:
	$(Q)$(MAKE) -C $(REQUEST_IDR_PATH) install

request_idr-uninstall:
	$(Q)$(MAKE) -C $(REQUEST_IDR_PATH) uninstall

app-$(CONFIG_APP_MPI_SCRIPT) += mpi_script
PHONY += mpi_script mpi_script-clean mpi_script-distclean
PHONY += mpi_script-install mpi_script-uninstall
mpi_script:
	$(Q)$(MAKE) -C $(MPI_SCRIPT_PATH) all

mpi_script-clean:
	$(Q)$(MAKE) -C $(MPI_SCRIPT_PATH) clean

mpi_script-distclean:
	$(Q)$(MAKE) -C $(MPI_SCRIPT_PATH) distclean

mpi_script-install:
	$(Q)$(MAKE) -C $(MPI_SCRIPT_PATH) install

mpi_script-uninstall:
	$(Q)$(MAKE) -C $(MPI_SCRIPT_PATH) uninstall

app-$(CONFIG_APP_AROI_DEMO) += aroi_demo
PHONY += aroi_demo aroi_demo-clean aroi_demo-distclean
PHONY += aroi_demo-install aroi_demo-uninstall
aroi_demo:
	$(Q)$(MAKE) -C $(AROI_DEMO_PATH)/build all

aroi_demo-clean:
	$(Q)$(MAKE) -C $(AROI_DEMO_PATH)/build clean

aroi_demo-distclean:
	$(Q)$(MAKE) -C $(AROI_DEMO_PATH)/build distclean

aroi_demo-install:
	$(Q)$(MAKE) -C $(AROI_DEMO_PATH)/build install

aroi_demo-uninstall:
	$(Q)$(MAKE) -C $(AROI_DEMO_PATH)/build uninstall

app-$(CONFIG_APP_DK_DEMO) += dk_demo
PHONY += dk_demo dk_demo-clean dk_demo-distclean
PHONY += dk_demo-install dk_demo-uninstall
dk_demo:
	$(Q)$(MAKE) -C $(DK_DEMO_PATH)/build all

dk_demo-clean:
	$(Q)$(MAKE) -C $(DK_DEMO_PATH)/build clean

dk_demo-distclean:
	$(Q)$(MAKE) -C $(DK_DEMO_PATH)/build distclean

dk_demo-install:
	$(Q)$(MAKE) -C $(DK_DEMO_PATH)/build install

dk_demo-uninstall:
	$(Q)$(MAKE) -C $(DK_DEMO_PATH)/build uninstall

app-$(CONFIG_APP_EF_DEMO) += ef_demo
PHONY += ef_demo ef_demo-clean ef_demo-distclean
PHONY += ef_demo-install ef_demo-uninstall
ef_demo:
	$(Q)$(MAKE) -C $(EF_DEMO_PATH)/build all

ef_demo-clean:
	$(Q)$(MAKE) -C $(EF_DEMO_PATH)/build clean

ef_demo-distclean:
	$(Q)$(MAKE) -C $(EF_DEMO_PATH)/build distclean

ef_demo-install:
	$(Q)$(MAKE) -C $(EF_DEMO_PATH)/build install

ef_demo-uninstall:
	$(Q)$(MAKE) -C $(EF_DEMO_PATH)/build uninstall

app-$(CONFIG_APP_FLD_DEMO) += fld_demo
PHONY += fld_demo fld_demo-clean fld_demo-distclean
PHONY += fld_demo-install fld_demo-uninstall
fld_demo:
	$(Q)$(MAKE) -C $(FLD_DEMO_PATH)/build all

fld_demo-clean:
	$(Q)$(MAKE) -C $(FLD_DEMO_PATH)/build clean

fld_demo-distclean:
	$(Q)$(MAKE) -C $(FLD_DEMO_PATH)/build distclean

fld_demo-install:
	$(Q)$(MAKE) -C $(FLD_DEMO_PATH)/build install

fld_demo-uninstall:
	$(Q)$(MAKE) -C $(FLD_DEMO_PATH)/build uninstall

app-$(CONFIG_APP_HD_DEMO) += hd_demo
PHONY += hd_demo hd_demo-clean hd_demo-distclean
PHONY += hd_demo-install hd_demo-uninstall
hd_demo: libeaif libinf
	$(Q)$(MAKE) -C $(HD_DEMO_PATH)/build all

hd_demo-clean: libeaif-clean libinf-clean
	$(Q)$(MAKE) -C $(HD_DEMO_PATH)/build clean

hd_demo-distclean: libeaif-distclean libinf-distclean
	$(Q)$(MAKE) -C $(HD_DEMO_PATH)/build distclean

hd_demo-install: libeaif-install libinf-install
	$(Q)$(MAKE) -C $(HD_DEMO_PATH)/build install

hd_demo-uninstall: libeaif-uninstall libinf-uninstall
	$(Q)$(MAKE) -C $(HD_DEMO_PATH)/build uninstall

app-$(CONFIG_APP_IR_DEMO) += ir_demo
PHONY += ir_demo ir_demo-clean ir_demo-distclean
PHONY += ir_demo-install ir_demo-uninstall
ir_demo:
	$(Q)$(MAKE) -C $(IR_DEMO_PATH)/build all

ir_demo-clean:
	$(Q)$(MAKE) -C $(IR_DEMO_PATH)/build clean

ir_demo-distclean:
	$(Q)$(MAKE) -C $(IR_DEMO_PATH)/build distclean

ir_demo-install:
	$(Q)$(MAKE) -C $(IR_DEMO_PATH)/build install

ir_demo-uninstall:
	$(Q)$(MAKE) -C $(IR_DEMO_PATH)/build uninstall

app-$(CONFIG_APP_MD_DEMO) += md_demo
PHONY += md_demo md_demo-clean md_demo-distclean
PHONY += md_demo-install md_demo-uninstall
md_demo:
	$(Q)$(MAKE) -C $(MD_DEMO_PATH)/build all

md_demo-clean:
	$(Q)$(MAKE) -C $(MD_DEMO_PATH)/build clean

md_demo-distclean:
	$(Q)$(MAKE) -C $(MD_DEMO_PATH)/build distclean

md_demo-install:
	$(Q)$(MAKE) -C $(MD_DEMO_PATH)/build install

md_demo-uninstall:
	$(Q)$(MAKE) -C $(MD_DEMO_PATH)/build uninstall

app-$(CONFIG_APP_OSD_DEMO) += osd_demo
PHONY += osd_demo osd_demo-clean
PHONY += osd_demo-install osd_demo-uninstall
osd_demo:
	$(Q)$(MAKE) -C $(OSD_DEMO_PATH)/build all

osd_demo-clean:
	$(Q)$(MAKE) -C $(OSD_DEMO_PATH)/build clean

osd_demo-install:
	$(Q)$(MAKE) -C $(OSD_DEMO_PATH)/build install

osd_demo-uninstall:
	$(Q)$(MAKE) -C $(OSD_DEMO_PATH)/build uninstall

app-$(CONFIG_APP_TD_DEMO) += td_demo
PHONY += td_demo td_demo-clean td_demo-distclean
PHONY += td_demo-install td_demo-uninstall
td_demo:
	$(Q)$(MAKE) -C $(TD_DEMO_PATH)/build all

td_demo-clean:
	$(Q)$(MAKE) -C $(TD_DEMO_PATH)/build clean

td_demo-distclean:
	$(Q)$(MAKE) -C $(TD_DEMO_PATH)/build distclean

td_demo-install:
	$(Q)$(MAKE) -C $(TD_DEMO_PATH)/build install

td_demo-uninstall:
	$(Q)$(MAKE) -C $(TD_DEMO_PATH)/build uninstall

app-$(CONFIG_APP_FACEDET_DEMO) += facedet_demo
PHONY += facedet_demo facedet_demo-clean facedet_demo-distclean
PHONY += facedet_demo-install facedet_demo-uninstall
facedet_demo:
	$(Q)$(MAKE) -C $(FACEDET_DEMO_PATH)/build all

facedet_demo-clean:
	$(Q)$(MAKE) -C $(FACEDET_DEMO_PATH)/build clean

facedet_demo-distclean:
	$(Q)$(MAKE) -C $(FACEDET_DEMO_PATH)/build distclean

facedet_demo-install:
	$(Q)$(MAKE) -C $(FACEDET_DEMO_PATH)/build install

facedet_demo-uninstall:
	$(Q)$(MAKE) -C $(FACEDET_DEMO_PATH)/build uninstall

app-$(CONFIG_APP_FACERECO_DEMO) += facereco_demo
PHONY += facereco_demo facereco_demo-clean facereco_demo-distclean
PHONY += facereco_demo-install facereco_demo-uninstall
facereco_demo:
	$(Q)$(MAKE) -C $(FACERECO_DEMO_PATH)/build all

facereco_demo-clean:
	$(Q)$(MAKE) -C $(FACERECO_DEMO_PATH)/build clean

facereco_demo-distclean:
	$(Q)$(MAKE) -C $(FACERECO_DEMO_PATH)/build distclean

facereco_demo-install:
	$(Q)$(MAKE) -C $(FACERECO_DEMO_PATH)/build install

facereco_demo-uninstall:
	$(Q)$(MAKE) -C $(FACERECO_DEMO_PATH)/build uninstall

PHONY += $(mod) $(mod)-clean $(mod)-distclean
PHONY += $(mod)-install $(mod)-uninstall
$(mod): $(app-y)
$(mod)-clean: $(addsuffix -clean,$(app-y))
$(mod)-distclean: $(addsuffix -distclean,$(app-y))
$(mod)-install: $(addsuffix -install,$(app-y))
$(mod)-uninstall: $(addsuffix -uninstall,$(app-y))

APP_BUILD_DEPS += $(mod)
APP_CLEAN_DEPS += $(mod)-clean
APP_DISTCLEAN_DEPS += $(mod)-distclean
APP_INTALL_DEPS += $(mod)-install
APP_UNINTALL_DEPS += $(mod)-uninstall
