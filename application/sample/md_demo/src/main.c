#define _GNU_SOURCE
#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <json.h>

#include "mpi_base_types.h"
#include "mpi_dev.h"
#include "mpi_iva.h"
#include "mpi_sys.h"
#include "vftr_md.h"
#include "vftr_shd.h"
#include "md_demo.h"

/* include this header to show result on multiplayer */
#include "avftr_conn.h"

VFTR_MD_PARAM_S g_md_param;
MPI_WIN g_win_idx;

VFTR_SHD_PARAM_S g_shd_param;
VFTR_SHD_LONGTERM_LIST_S g_shd_long_list;

int g_md_running;

#ifdef CONFIG_APP_MD_SUPPORT_SEI
extern AVFTR_CTX_S *av_ftr_res_shm;
#endif

#ifdef CONFIG_APP_SHD_SUPPORT
#endif

#ifdef CONFIG_APP_SHD_SUPPORT
#endif

#ifdef DEBUG
#define DBG(...) printf(__VA_ARGS__)
#else
#define DBG(...)
#endif

static void handleSigInt(int signo)
{
	DBG("[%s]leave %s\n", __func__, __FILE__);

	if (signo == SIGINT) {
		printf("Caught SIGINT!\n");
		g_md_running = 0;
	} else if (signo == SIGTERM) {
		printf("Caught SIGTERM!\n");
	} else if (signo == SIGKILL) {
		printf("Caught SIGKILL\n");
	} else if (signo == SIGQUIT) {
		printf("Caught SIGQUIT!\n");
	} else {
		perror("Unexpected signal!\n");
	}

	signal(SIGINT, SIG_DFL);
	MPI_IVA_disableObjDet(g_win_idx);
	AVFTR_exitServer();
	MPI_SYS_exit();

	exit(0);
}

int readJsonFromFile(char *file_name)
{
	int ret = 0;
	json_object *test_obj = NULL;
	json_object *tmp_obj = NULL;
	json_object *tmp1_obj = NULL;
	json_object *tmp2_obj = NULL;
	json_object *tmp3_obj = NULL;
	VFTR_MD_PARAM_S *md_para = &g_md_param;

	/* load json config from json file */
	test_obj = json_object_from_file(file_name);
	if (!test_obj) {
		fprintf(stderr, "Cannot open %s\n", file_name);
		return -EBADF;
	}

	json_object_object_get_ex(test_obj, "roi", &tmp_obj);
	if (!tmp_obj) {
		fprintf(stderr, "Cannot get %s object\n", "roi");
		goto error;
	}

	int roi_cnt = json_object_array_length(tmp_obj);
	md_para->region_num = roi_cnt;
	DBG("roi_cnt:%d\n", roi_cnt);

	for (int i = 0; i < roi_cnt; i++) {
		VFTR_MD_REG_ATTR_S *md_attr = &g_md_param.attr[i];
		tmp1_obj = json_object_array_get_idx(tmp_obj, i);
		if (!tmp1_obj) {
			ret = -1;
			break;
		}

		json_object_object_get_ex(tmp1_obj, "start", &tmp2_obj);
		tmp3_obj = json_object_array_get_idx(tmp2_obj, 0);
		DBG("%s[x] = %d\n", "start", json_object_get_int(tmp3_obj));
		md_attr->pts.sx = (unsigned int)json_object_get_int(tmp3_obj);
		tmp3_obj = json_object_array_get_idx(tmp2_obj, 1);
		DBG("%s[y] = %d\n", "start", json_object_get_int(tmp3_obj));
		md_attr->pts.sy = json_object_get_int(tmp3_obj);

		json_object_object_get_ex(tmp1_obj, "end", &tmp2_obj);
		tmp3_obj = json_object_array_get_idx(tmp2_obj, 0);
		DBG("%s[x] = %d\n", "end", json_object_get_int(tmp3_obj));
		md_attr->pts.ex = json_object_get_int(tmp3_obj);
		tmp3_obj = json_object_array_get_idx(tmp2_obj, 1);
		DBG("%s[y] = %d\n", "end", json_object_get_int(tmp3_obj));
		md_attr->pts.ey = json_object_get_int(tmp3_obj);
		json_object_object_get_ex(tmp1_obj, "ratio", &tmp2_obj);
		if (!tmp2_obj) {
			fprintf(stderr, "Cannot get %s object\n", "ratio");
			ret = -1;
			goto error;
		}
		int area_percent = atoi(json_object_get_string(tmp2_obj));
		DBG("%s = %s\n", "ratio", json_object_get_string(tmp2_obj));
		int width = md_attr->pts.ex - md_attr->pts.sx;
		int height = md_attr->pts.ey - md_attr->pts.sy;

		/* critical point */
		md_attr->obj_life_th = 0;
		md_attr->thr_v_obj_min = 5;
		md_attr->thr_v_obj_max = 255;
		md_attr->thr_v_reg = width / 100 * height / 100 * area_percent / 100;
		md_attr->md_mode = VFTR_MD_MOVING_AREA;
		md_attr->det_method = VFTR_MD_DET_NORMAL;
	}
	/* parser shd*/
	VFTR_SHD_PARAM_S *shd_param = &g_shd_param;
	VFTR_SHD_LONGTERM_LIST_S *shd_long_list = &g_shd_long_list;

	json_object_object_get_ex(test_obj, "shd", &tmp1_obj);
	if (!tmp_obj) {
		fprintf(stderr, "Cannot get %s object\n", "shd");
		goto error;
	}

	json_object_object_get_ex(tmp1_obj, "en", &tmp2_obj);
	shd_param->en = json_object_get_int(tmp2_obj);
	if (!tmp2_obj) {
		fprintf(stderr, "Cannot get %s object\n", "en");
		ret = -ENOCSI;
		goto error;
	}

	json_object_object_get_ex(tmp1_obj, "sensitivity", &tmp2_obj);
	shd_param->sensitivity = json_object_get_int(tmp2_obj);
	if (!tmp2_obj) {
		fprintf(stderr, "Cannot get %s object\n", "sensitivity");
		ret = -ENOCSI;
		goto error;
	}

	json_object_object_get_ex(tmp1_obj, "quality", &tmp2_obj);
	shd_param->quality = json_object_get_int(tmp2_obj);
	if (!tmp2_obj) {
		fprintf(stderr, "Cannot get %s object\n", "quality");
		ret = -ENOCSI;
		goto error;
	}

	json_object_object_get_ex(tmp1_obj, "obj_life_th", &tmp2_obj);
	shd_param->obj_life_th = json_object_get_int(tmp2_obj);
	if (!tmp2_obj) {
		fprintf(stderr, "Cannot get %s object\n", "obj_life_th");
		ret = -ENOCSI;
		goto error;
	}

	json_object_object_get_ex(tmp1_obj, "longterm_life_th", &tmp2_obj);
	shd_param->longterm_life_th = json_object_get_int(tmp2_obj);
	if (!tmp2_obj) {
		fprintf(stderr, "Cannot get %s object\n", "longterm_life_th");
		ret = -ENOCSI;
		goto error;
	}

	json_object_object_get_ex(tmp1_obj, "instance_duration", &tmp2_obj);
	shd_param->instance_duration = json_object_get_int(tmp2_obj);
	if (!tmp2_obj) {
		fprintf(stderr, "Cannot get %s object\n", "instance_duration");
		ret = -ENOCSI;
		goto error;
	}

	json_object_object_get_ex(tmp1_obj, "shaking_update_duration", &tmp2_obj);
	shd_param->shaking_update_duration = json_object_get_int(tmp2_obj);
	if (!tmp2_obj) {
		fprintf(stderr, "Cannot get %s object\n", "shaking_update_duration");
		ret = -ENOCSI;
		goto error;
	}

	json_object_object_get_ex(tmp1_obj, "longterm_dec_period", &tmp2_obj);
	shd_param->longterm_dec_period = json_object_get_int(tmp2_obj);
	if (!tmp2_obj) {
		fprintf(stderr, "Cannot get %s object\n", "longterm_dec_period");
		ret = -ENOCSI;
		goto error;
	}

	json_object_object_get_ex(tmp1_obj, "longterm_num", &tmp2_obj);
	shd_long_list->num = json_object_get_int(tmp2_obj);
	if (!tmp2_obj) {
		fprintf(stderr, "Cannot get %s object\n", "longterm_num");
		ret = -ENOCSI;
		goto error;
	}

	json_object_object_get_ex(tmp1_obj, "longterm_list", &tmp2_obj);
	if (!tmp2_obj) {
		fprintf(stderr, "Cannot get %s object\n", "longterm_list");
		goto error;
	}

	int longterm_list = json_object_array_length(tmp2_obj);
	for (int i = 0; i < longterm_list; i++) {
		tmp1_obj = json_object_array_get_idx(tmp2_obj, i);
		if (!tmp1_obj) {
			ret = -ENOCSI;
			break;
		}
		json_object_object_get_ex(tmp1_obj, "start", &tmp2_obj);
		tmp3_obj = json_object_array_get_idx(tmp2_obj, 0);
		shd_long_list->item->rgn.sx = json_object_get_int(tmp3_obj);
		tmp3_obj = json_object_array_get_idx(tmp2_obj, 1);
		shd_long_list->item->rgn.sy = json_object_get_int(tmp3_obj);

		json_object_object_get_ex(tmp1_obj, "end", &tmp2_obj);
		tmp3_obj = json_object_array_get_idx(tmp2_obj, 0);
		shd_long_list->item->rgn.ex = json_object_get_int(tmp3_obj);
		tmp3_obj = json_object_array_get_idx(tmp2_obj, 1);
		shd_long_list->item->rgn.ey = json_object_get_int(tmp3_obj);
	}

error:
	json_object_put(test_obj);

	return ret;
}

int enableOd(MPI_WIN win, const MPI_IVA_OD_PARAM_S *od)
{
	int ret = 0;

	ret = MPI_IVA_setObjParam(win, od);

	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "Failed to set OD param.\n");
		goto error;
	}

	ret = MPI_IVA_enableObjDet(win);

	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "Failed to enable OD.\n");
		goto error;
	}

	return MPI_SUCCESS;

error:
	return MPI_FAILURE;
}
/* OD parameters share to all region */

void help(void)
{
	printf("USAGE:\tmd_demo -i <CONFIG>\t\n");
	printf("\t-i <file>\t\tROI config list in .json file\n");
	printf("OPTIONS:\n");
	printf("\t-c <channel>\t\tSpecify which video channel to use. (Deatult 0).\n");
	printf("\t-d <detect interval>\tSpecify which video detect interval. (Deatult 30).\n");
	/* UINT8 od_qual; < Quality index of OD performance. */
	printf("\t-q <value>\t\tSpecify OD quality index.[0-63] (Deatult 46).\n");
	/* UINT8 od_sen; < sensitivity index of OD performance. */
	printf("\t-s <sensitivity>\t\tSpecify OD sensitivity.[0-255] (Deatult 254).\n");
	printf("\n");
	printf("For example:\n");
	printf("\tmpi_stream -d /system/mpp/case_config_1001FHD -precord_enable=1 -poutput_file=/dev/null -pframe_num=-1 &\n");
	printf("\tmd_demo -i /system/mpp/md_config/md_conf_1080p_roi_1.json &\n");
	printf("\ttestOnDemandRTSPServer 0 -S\n");
	printf("\n");
}

int main(int argc, char **argv)
{
	int ret;
	int c;
	int chn_idx = 0;
	char cfg_file_name[256] = { 0 };

	MPI_IVA_OD_PARAM_S od_param = {
		.od_qual = 46, .od_track_refine = 42, .od_size_th = 40, .od_sen = 254, .en_stop_det = 0
	};

	VFTR_MD_PARAM_S *md_attr = &g_md_param;
	VFTR_SHD_PARAM_S *shd_attr = &g_shd_param;
	VFTR_SHD_LONGTERM_LIST_S *shd_long_list = &g_shd_long_list;

	while ((c = getopt(argc, argv, "i:c:q:s:h")) != -1) {
		switch (c) {
		case 'i':
			sprintf(cfg_file_name, optarg);
			DBG("input .json file:%s\n", cfg_file_name);
			break;
		case 'c':
			chn_idx = atoi(optarg);
			DBG("set device channel:%d\n", chn_idx);
			break;
		case 'q':
			od_param.od_qual = atoi(optarg);
			DBG("set detect quality index:%s\n", optarg);
			break;
		case 's':
			od_param.od_sen = atoi(optarg);
			DBG("set detect sensitivity:%s\n", optarg);
			break;
		case 'h':
			help();
			exit(0);
		default:
			abort();
		}
	}

	if (signal(SIGINT, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGINT!\n");
		exit(1);
	}

	if (strlen(cfg_file_name) == 0) {
		fprintf(stderr, "Config file path is not specified !\n");
		exit(1);
	}

	readJsonFromFile(cfg_file_name);
	//DBG("Read Json form file:%s\n", cfg_file_name);

	MPI_CHN_ATTR_S chn_attr;
	MPI_CHN_LAYOUT_S layout_attr;
	MPI_CHN chn = MPI_VIDEO_CHN(0, chn_idx);

	/* Initialize MPI system */
	MPI_SYS_init();

	ret = MPI_DEV_getChnAttr(chn, &chn_attr);
	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "Failed to MPI_DEV_getChnAttr.\n");
		return -EINVAL;
	}

	ret = MPI_DEV_getChnLayout(chn, &layout_attr);
	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "Failed to MPI_DEV_getlayoutAttr.\n");
		return -EINVAL;
	}

	/* NOTICE: we only support the case that only one window in a channel */
	assert(layout_attr.window_num == 1);
	assert(layout_attr.window[0].x == 0);
	assert(layout_attr.window[0].y == 0);
	assert(layout_attr.window[0].width == chn_attr.res.width);
	assert(layout_attr.window[0].height == chn_attr.res.height);

	/* Check parameters */
	ret = VFTR_MD_checkParam(md_attr, &chn_attr.res);
	if (ret) {
		fprintf(stderr, "Invalid MD parameters\n");
		return -EINVAL;
	}

#ifdef CONFIG_APP_MD_SUPPORT_SEI
	/* init AV server to wait RTSP client connectd
	 * allow transfer IVA result to RTSP streaming server */
	/* NOTE: this step only for showing result from RTSP server
	 * related code is not needed
	 */
	ret = AVFTR_initServer();
	if (ret) {
		fprintf(stderr, "failed to initalize AV server %d\n", ret);
		return -ENOPROTOOPT;
	}

	/* reset shared memory to zeros */
	memset(av_ftr_res_shm, 0, AVFTR_RES_SIZE);
#endif

	/* set channel index */
	g_win_idx.chn = chn_idx;

	/* Implment the following function */
	/* enableOd() */
	ret = enableOd(g_win_idx, &od_param);
	if (ret < 0) {
		fprintf(stderr, "Failed to enable OD, please check if OD is enabled\n");
	}

	/* detectMdObject(); */
	ret = detectMdObject(g_win_idx, &chn_attr.res, md_attr, shd_attr, shd_long_list);
	ret = MPI_IVA_disableObjDet(g_win_idx);

	/* init server */
	/* NOTE: this step only for showing result from RTSP server
	 * related code is not needed
	 */

#ifdef CONFIG_APP_MD_SUPPORT_SEI
	AVFTR_exitServer();
#endif
	MPI_SYS_exit();
	/* disableod */
	return 0;
}