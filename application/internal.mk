include $(SDKSRC_DIR)/build/sdksrc.mk

APP_LIB = $(APP_PATH)/library/output

AVMAIN_PATH = $(APP_PATH)/av_daemon/av_main
AVMAIN2_PATH = $(APP_PATH)/av_daemon/av_main2

CONNSEL_PATH = $(APP_PATH)/network_application/connsel
EAIF_SERV_PATH = $(APP_PATH)/network_application/eaif_server
EAIF_SERV_BUILD_PATH = $(EAIF_SERV_PATH)/build
ONVIF_SERVER_PATH = $(APP_PATH)/network_application/onvif_server
IPASSIGN_PATH = $(APP_PATH)/network_application/ip_assign
STAMEN_PATH = $(APP_PATH)/network_application/stamen
UNICORN_PATH = $(APP_PATH)/network_application/unicorn
WEBCGI_PATH = $(APP_PATH)/network_application/webcgi
WIFI_PATH= $(APP_PATH)/network_application/wifi
WS_DISCOVERY_PATH = $(APP_PATH)/network_application/ws_discovery

# ONVIF_DISCOVERY_PATH is deprecated.
# Please use WS_DISCOVERY_PATH instead.
ONVIF_DISCOVERY_PATH = $(WS_DISCOVERY_PATH)

ACTL_PATH = $(APP_PATH)/sample/actl
ANR_CTRL_PATH = $(APP_PATH)/sample/anr_ctrl
G726_PATH = $(APP_PATH)/sample/g726
HELLO_APP_PATH = $(APP_PATH)/sample/hello_app
AROI_DEMO_PATH = $(APP_PATH)/sample/aroi_demo
DK_DEMO_PATH = $(APP_PATH)/sample/dk_demo
EF_DEMO_PATH = $(APP_PATH)/sample/ef_demo
FLD_DEMO_PATH = $(APP_PATH)/sample/fld_demo
HD_DEMO_PATH = $(APP_PATH)/sample/hd_demo
IR_DEMO_PATH = $(APP_PATH)/sample/ir_demo
MD_DEMO_PATH = $(APP_PATH)/sample/md_demo
TD_DEMO_PATH = $(APP_PATH)/sample/td_demo
FACEDET_DEMO_PATH = $(APP_PATH)/sample/facedet_demo
FACERECO_DEMO_PATH = $(APP_PATH)/sample/facereco_demo

CENTCTRL_PATH = $(APP_PATH)/setting_mgmt/centctrl
CENTCTRL_INC = $(CENTCTRL_PATH)/include
DBMONITOR_PATH = $(APP_PATH)/setting_mgmt/dbmonitor
SQLAPP_PATH = $(APP_PATH)/setting_mgmt/sqlapp
SQLAPP_INC = $(SQLAPP_PATH)/include

APP_INC = $(APP_PATH)/library/libagtx/include
LIBAGTX_PATH = $(APP_PATH)/library/libagtx
LIBAGTX_INC = $(APP_INC)
LIBAGTX_LIB = $(APP_PATH)/library/libagtx/lib
LIVE555_PATH = $(APP_PATH)/streaming_server/live555
RTMP_PATH = $(APP_PATH)/streaming_server/rtmp_publisher
FLV_PATH = $(APP_PATH)/streaming_server/flv_server

ACCESS_MODE_PATH = $(APP_PATH)/system_utility/access_mode
ALARMOUT_PATH = $(APP_PATH)/system_utility/alarm_out
DAY_NIGHT_MODE_PATH = $(APP_PATH)/system_utility/day_night_mode
EVENTD_PATH = $(APP_PATH)/system_utility/event_daemon
GPIO_DAEMONS_PATH = $(APP_PATH)/system_utility/gpio_daemons
MP_AUTOTEST_PATH = $(APP_PATH)/system_utility/mp_autotest
NRS_PATH = $(APP_PATH)/system_utility/nrs
NRS_BUILD = $(NRS_PATH)/build
NRS_INC = $(NRS_PATH)/include
NRS_LIB = $(NRS_PATH)/lib
SNTP_PATH = $(APP_PATH)/system_utility/sntp
SYSUPD_TOOLS_PATH = $(APP_PATH)/system_utility/sysupd_tools
UVC_PATH = $(APP_PATH)/system_utility/uvc
THERMAL_PROTECTION_PATH = $(APP_PATH)/system_utility/thermal_protection
WATCHDOG_PATH = $(APP_PATH)/system_utility/watchdog

VIDEO_FTR_INC = $(APP_PATH)/library/libavftr/include
LIBAVFTR_PATH = $(APP_PATH)/library/libavftr
LIBAVFTR_BUILD_PATH = $(LIBAVFTR_PATH)/build
LIBAVFTR_INC = $(LIBAVFTR_PATH)/include
LIBAVFTR_LIB = $(LIBAVFTR_PATH)/lib

LIBADO_PATH = $(APP_PATH)/library/libado
LIBADO_INC = $(LIBADO_PATH)/include
LIBADO_LIB = $(LIBADO_PATH)

LIBCM_PATH = $(APP_PATH)/library/libcm
LIBCM_INC = $(LIBCM_PATH)/include
LIBCM_LIB = $(LIBCM_PATH)

LIBEAIF_PATH = $(APP_PATH)/library/libeaif
LIBEAIF_INC = $(LIBEAIF_PATH)/include
LIBEAIF_LIB = $(LIBEAIF_PATH)/lib

LIBINF_PATH = $(APP_PATH)/library/libinf
LIBINF_INC = $(LIBINF_PATH)/include
LIBINF_LIB = $(LIBINF_PATH)/lib

LIBFOO_PATH = $(APP_PATH)/library/libfoo
LIBFOO_INC = $(LIBFOO_PATH)/include
LIBFOO_LIB = $(LIBFOO_PATH)/lib

LIBGPIO_PATH = $(APP_PATH)/library/libgpio
LIBLEDEVT_PATH = $(EVENTD_PATH)/libledevt
LIBLEDEVT_INC = $(LIBLEDEVT_PATH)
LIBLEDEVT_LIB = $(LIBLEDEVT_PATH)

LIBOSD_PATH = $(APP_PATH)/library/libosd
LIBOSD_INC = $(LIBOSD_PATH)/include
LIBOSD_LIB = $(LIBOSD_PATH)/lib

LIBUTILS_PATH = $(APP_PATH)/library/libutils
LIBUTILS_INC = $(LIBUTILS_PATH)/include
LIBUTILS_LIB = $(LIBUTILS_PATH)/lib


LIBPRIO_PATH = $(APP_PATH)/library/libprio
LIBPRIO_INC = $(LIBPRIO_PATH)
LIBPRIO_LIB = $(LIBPRIO_PATH)

LIBPWM_PATH = $(APP_PATH)/library/libpwm

LIBSQL_PATH = $(APP_PATH)/library/libsql
LIBSQL_INC = $(LIBSQL_PATH)
LIBSQL_LIB = $(LIBSQL_PATH)

LIBTZ_PATH = $(APP_PATH)/library/libtz
LIBTZ_INC = $(LIBTZ_PATH)/include
LIBTZ_LIB = $(LIBTZ_PATH)

FSINK_PATH = $(APP_PATH)/library/libfsink
FSINK_INC = $(FSINK_PATH)/inc

FILE_PATH = $(FSINK_PATH)/file
FILE_INC = $(FSINK_PATH)/inc
FILE_LIB = $(FSINK_PATH)/file

UDPS_PATH = $(FSINK_PATH)/udps
UDPS_INC = $(FSINK_PATH)/inc
UDPS_LIB = $(FSINK_PATH)/udps

# MPI APP program
MPI_STREAM_PATH = $(APP_PATH)/sample/mpi_stream
LIBSAMPLE_INC = $(MPI_STREAM_PATH)/src/libsample

CMD_SENDER_PATH = $(APP_PATH)/sample/cmd_sender
MPI_SNAPSHOT_PATH = $(APP_PATH)/sample/snapshot
REQUEST_IDR_PATH = $(APP_PATH)/sample/request_idr
MPI_SCRIPT_PATH = $(APP_PATH)/sample/mpi_script
