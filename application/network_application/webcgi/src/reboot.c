#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>

#include <fcgi_stdio.h>

int main()
{
	pid_t pid;
	while (FCGI_Accept() >= 0) {
		pid = fork();
		if (pid == 0) {
			printf("Content-type:text/html\r\n"
			       "\r\n");
			execl("/sbin/reboot", "reboot", NULL);
		}
	}
	return 0;
}
