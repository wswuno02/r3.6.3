#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>

#include <fcgi_stdio.h>

int main()
{
	char *str_len = NULL;
	int len = 0;
	char buf[100] = "";
	char dt1[50] = "";
	char dt2[50] = "";
	char ch1, ch2;
	char yy[10] = "";
	char zero[5] = "0";
	unsigned int i = 0, j = 0;
	char str[6][50];
	pid_t pid;

	while (FCGI_Accept() >= 0) {
		str_len = getenv("CONTENT_LENGTH");

		if ((str_len == NULL) || (sscanf(str_len, "%d", &len) != 1) || (len > 100)) {
			printf("sorry!Error!\n");
		}

		// fgets(buf,len+1,stdin);
		strcpy(buf, getenv("QUERY_STRING"));
		sscanf(buf, "%[^/]/%[^/]/%[^ ] %[^:]:%[^:]:%s", str[0], str[1], str[2], str[3], str[4], str[5]);

		for (i = 0; i < 6; i++)
			if (strlen(str[i]) < 2) {
				zero[0] = '0';
				strcat(zero, str[i]);
				strcpy(str[i], zero);
				memset(zero, 0, sizeof(zero));
			}

		yy[0] = str[5][2];
		yy[1] = str[5][3];
		strcat(dt1, str[1]);
		strcat(dt1, str[2]);
		strcat(dt1, str[3]);
		strcat(dt1, str[4]);
		strcat(dt1, str[5]);
		strcat(dt1, yy);

		for (i = 0; i < strlen(dt1); i++) {
			dt2[i] = dt1[i + 2];
		}

		j = strlen(dt2);
		ch1 = dt2[j - 2];
		ch2 = dt2[j - 1];
		dt2[j - 2] = '.';
		dt2[j - 1] = ch1;
		dt2[j] = ch2;
		pid = fork();

		if (pid == 0) {
			printf("Content-type:text/html\r\n"
			       "\r\n");
			execl("/system/www/time.sh", "time.sh", "-s", dt2, NULL);
		}
	}

	return 0;
}
