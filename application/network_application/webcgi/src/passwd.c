#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>

#include <fcgi_stdio.h>

int main()
{
	char *str_len = NULL;
	int len = 0;
	char user[50] = "";
	int fd = 0;
	char buf[100] = "";
	char passwd[100] = "";
	// char b[5] = ":";
	pid_t pid;

	while (FCGI_Accept() >= 0) {
		printf("Content-type:text/html\r\n"
		       "\r\n");

		str_len = getenv("CONTENT_LENGTH");

		// printf("%i", str_len);
		if ((str_len == NULL) || (sscanf(str_len, "%d", &len) != 1) || (len > 100)) {
			printf("sorry!Error!\n");
		}

		// fgets(buf,len+1,stdin);
		strcpy(buf, getenv("QUERY_STRING"));
		printf("buf: %s\r\n", buf);
		sscanf(buf, "name=%[^&]&pwd=%s", user, passwd);
		printf("name: %s\r\n", user);
		printf("pwd: %s\r\n", passwd);
		fd = open("/etc/nginx/.htpasswd", O_WRONLY | O_CREAT, 0660);
		pid = fork();

		if (pid == 0) {
			dup2(fd, STDOUT_FILENO);
			execl("/system/www/cgi-bin/passwd.sh", "passwd.sh", user, passwd, NULL);
		}
	}

	return 0;
}
