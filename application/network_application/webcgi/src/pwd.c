#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <fcgi_stdio.h>

int main()
{
	char *str_len = NULL;
	int len = 0;
	char user[50] = "";
	int fd = 0;
	char buf[100] = "";
	char passwd[100] = "";
	char b[5] = ":";
	pid_t pid;
	pid = fork();

	while (FCGI_Accept() >= 0) {
		str_len = getenv("CONTENT_LENGTH");

		if ((str_len == NULL) || (sscanf(str_len, "%d", &len) != 1) || (len > 100)) {
			printf("sorry!Error!\n");
		}

		fgets(buf, len + 1, stdin);
		sscanf(buf, "name=%[^&]&pwd=%s", user, passwd);
		strcat(user, b);
		fd = open("/etc/nginx/.htpasswd", O_WRONLY | O_CREAT, 0660);

		if (pid == 0) {
			printf("Content-type:text/html\r\n"
			       "\r\n");
			dup2(fd, STDOUT_FILENO);
			execl("/bin/pwd.sh", "pwd.sh", user, passwd, NULL);
		}
	}

	return 0;
}
