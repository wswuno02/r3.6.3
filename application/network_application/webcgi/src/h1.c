#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/un.h>
#include <time.h>
#include <unistd.h>

int main(void)
{
	pid_t pid = fork();

	if (pid == 0) {
		execl("/system/www/cgi-bin/time.sh", "time.sh", "\'2018-12-17 1:23:59\'", (char *)NULL);
	}

	printf("exec example");
	return 0;
}
