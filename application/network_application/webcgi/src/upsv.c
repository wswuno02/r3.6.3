#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define SERVER_PORT 9010

char msg[] = "HTTP/1.1 200 OK\r\nConnection: Keep-Alive\r\nContent-Type: "
             "text/html; charset=utf-8\r\nDate: Thu, 11 Aug 2016 15:23:13 "
             "GMT\r\nKeep-Alive: timeout=5, max=1000\r\n"
             "Last-Modified: Mon, 25 Jul 2016 04:32:39 GMT\r\n"
             "Access-Control-Allow-Origin: *\r\n"
             "Server: Augentix\r\n"
             "\r\n"
             "OK\r\n";

int process_packet(FILE *fp, char *packet, int packet_len, char *key, int key_len, char *buf, int step)
{
	int match = 0;
	int data_start, data_end;
	int i, j;

	if (step == 0) {
		// Find key in the pack for start
		for (i = 0; i < packet_len - key_len + 1; i++) {
			for (j = 0; j < key_len; j++) {
				if (packet[i + j] != key[j])
					break;
			}
			if (j == key_len) {
				match = 1;
				data_start = i + key_len + 2;
				char *p = &packet[data_start];
				char *q = strstr(p, "\r\n\r\n");
				data_start = q - packet + 4;
				break;
			}
		}

		if (match == 1) {
			printf("Start key found!\n");
			process_packet(fp, packet + data_start, packet_len - data_start, key, key_len, buf, 1);
			return 1;
		} else {
			// not found in this packet, copy key_len-1 characters to buf beginning
			for (i = 0; i < key_len - 1; i++) {
				buf[i] = packet[packet_len - key_len + 1 + i];
			}
			return 0;
		}
	} else if (step == 1) {
		// Find key for ending
		for (i = 0; i < packet_len - key_len + 1; i++) {
			for (j = 0; j < key_len; j++) {
				if (packet[i + j] != key[j]) {
					break;
				}
			}

			if (j == key_len) {
				match = 1;
				data_end = i;
				break;
			}
		}

		if (match == 1) {
			printf("End key found!\n");
			for (j = 0; j < 4; j++) {
				if (packet[data_end - j - 1] == 0xD) {
					break;
				}
			}
			data_end -= (j + 1);
			if (data_end > 0) {
				fwrite(packet, data_end, 1, fp);
			}
			return 2;
		} else {
			// not found in this packet, copy key_len-1 characters to buf beginning
			fwrite(packet, packet_len - key_len + 1, 1, fp);
			for (int i = 0; i < key_len - 1; i++) {
				buf[i] = packet[packet_len - key_len + 1 + i];
			}
			return 1;
		}
	} else {
		return 1;
	}
}

int main()
{
	FILE *fp;
	int serverSocket;
	struct sockaddr_in server_addr;
	struct sockaddr_in clientAddr;
	int addr_len = sizeof(clientAddr);
	int client;
	char buffer[8192];
	int iDataNum;
	int packets = 0;
	unsigned int content_length = 0;
	unsigned int required_data_cnt, header_size;
	int contain_file;
	char key[100];
	char *packet;
	int packet_len;
	int step;
	int keybufcount;

	if ((serverSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		return 1;
	}

	memset(&server_addr, 0, sizeof(server_addr));

	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(SERVER_PORT);

	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(serverSocket, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
		perror("connect");
		return 1;
	}

	if (listen(serverSocket, 5) < 0) {
		perror("listen");
		return 1;
	}

	while (1) {
		printf("Listen port: %d\n", SERVER_PORT);

		client = accept(serverSocket, (struct sockaddr *)&clientAddr, (socklen_t *)&addr_len);

		if (client < 0) {
			perror("accept");
			continue;
		}
		printf("Terminating video engine...\n");
		system("/system/bin/vsterm");

		printf("Waiting for message..\n");

		printf("IP is %s\n", inet_ntoa(clientAddr.sin_addr));
		printf("Port is %d\n", htons(clientAddr.sin_port));
		packets = 0;

		iDataNum = recv(client, buffer, 8192, 0);
		// send reply
		// send(client,msg,sizeof(msg),0);
		printf("Packet %d:%d\n", packets++, iDataNum);
		char *start_data = strstr(buffer, "\r\n\r\n");
		char *length_data = strstr(buffer, "Content-Length:");
		char *keypos = strstr(buffer, "boundary=");
		int keylen;
		if (keypos != NULL) {
			sscanf(keypos + 9, "%s\r\n", key);
			keylen = strlen(key);
			printf("Key:%s length=%d\n", key, keylen);
			contain_file = 1;
		} else {
			printf("No boundary key\n");
			contain_file = 0;
		}

		if (start_data != NULL && length_data != NULL) {
			start_data[1] = 0;

			printf("%s", buffer);
			sscanf(length_data + 15, "%u", &content_length);
			printf("content_length=%d\n", content_length);
			header_size = start_data + 4 - buffer;
			printf("Header=%d\n", start_data + 4 - buffer);
			required_data_cnt = header_size + content_length;
			// process packet 0
			if (contain_file) {
				if ((fp = fopen("/tmp/update.swu", "wb")) == NULL) {
					perror("Open file error,exit\n");
					close(serverSocket);
					exit(1);
				}
				packet = start_data + 4;
				packet_len = iDataNum - header_size;
				if (packet_len < keylen) {
					keybufcount = recv(client, &buffer[iDataNum], keylen, 0);
					iDataNum += keybufcount;
					packet_len += keybufcount;
				}

				step = process_packet(fp, packet, packet_len, key, keylen, buffer, 0);
			}
			required_data_cnt -= iDataNum;
			while (required_data_cnt > 0) {
				iDataNum = recv(client, &buffer[keylen - 1], 8192 - keylen + 1, 0);

				printf("Packet %d:%d\n", packets++, iDataNum);
				if (contain_file) {
					packet_len = iDataNum + keylen - 1;
					packet = buffer;
					step = process_packet(fp, packet, packet_len, key, keylen, buffer, step);
					printf("step:%d\n", step);
				}
				if (iDataNum <= 0) {
					perror("recv null");
					break;
				}
				required_data_cnt -= iDataNum;
			}

		} else { // no header found
			perror(" No header part found!!\n");
		}
		// send reply
		send(client, msg, sizeof(msg), 0);
		close(client);
		fclose(fp);
	}
	return 0;
}
