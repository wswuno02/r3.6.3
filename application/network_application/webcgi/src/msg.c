#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>

#include <fcgi_stdio.h>

#define JSON_BUF_STR_SIZE 8192
void error(const char *);

char buf[8192];

int ishex(int x)
{
	return (x >= '0' && x <= '9') || (x >= 'a' && x <= 'f') || (x >= 'A' && x <= 'F');
}

int decode(const char *s, char *dec)
{
	char *o;
	const char *end = s + strlen(s);
	int c;

	for (o = dec; s <= end; o++) {
		c = *s++;

		if (c == '%' && (!ishex(*s++) || !ishex(*s++) || !sscanf(s - 2, "%2x", (unsigned int *)&c))) {
			return -1;
		}

		if (dec) {
			*o = c;
		}
	}

	return o - dec;
}

char cmd1[] = "{\"master_id\":0, \"cmd_id\":1048577, \"cmd_type\":\"ctrl\", "
              "\"name\":\"CGI\"}";
char cmd2[] = "{\"master_id\":0, \"cmd_id\":1048578, \"cmd_type\":\"ctrl\"}";

int main(void)
{
	int sockfd, servlen, n;
	struct sockaddr_un serv_addr;
	char buffer[JSON_BUF_STR_SIZE];

	memset((char *)&serv_addr, 0, sizeof(serv_addr));
	serv_addr.sun_family = AF_UNIX;
	strcpy(serv_addr.sun_path, "/tmp/ccUnxSkt");
	servlen = strlen(serv_addr.sun_path) + sizeof(serv_addr.sun_family);

	if ((sockfd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
		error("Creating socket");
	}

	if (connect(sockfd, (struct sockaddr *)&serv_addr, servlen) < 0) {
		error("Connecting");
	}

	write(sockfd, cmd1, strlen(cmd1));
	n = read(sockfd, buffer, JSON_BUF_STR_SIZE - 2);
	write(sockfd, cmd2, strlen(cmd1));
	n = read(sockfd, buffer, JSON_BUF_STR_SIZE - 2);

	while (FCGI_Accept() >= 0) {
		printf("Content-type: text/html\r\n"
		       "\r\n");
		decode(getenv("QUERY_STRING"), buf);
		write(sockfd, buf, strlen(buf));
		n = read(sockfd, buffer, JSON_BUF_STR_SIZE - 2);
		buffer[n] = 0;
		printf(buffer);
	}

	return 0;
}
