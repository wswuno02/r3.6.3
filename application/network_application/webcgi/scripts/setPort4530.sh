#!/bin/bash
#
# Start/stop setPort4530
#

PIDFILE=/var/run/testOnDemandRTSPServer_4530.pid
RTSP_BIN=/system/bin/testOnDemandRTSPServer
export LD_LIBRARY_PATH=/system/lib

case "$1" in
  start)
#    start-stop-daemon -S -m -b -x $RTSP_BIN 0 -p 4530 -s live/0  -p $PIDFILE
    /system/bin/testOnDemandRTSPServer  -p 4530 -s live/1 -c 1 &  echo $! > $PIDFILE
    ;;
  stop)
#    killall -9 testOnDemandRTSPServer
   start-stop-daemon -K -o -p $PIDFILE
    ;;
  restart|reload)
    "$0" stop
    "$0" start
    ;;
  *)
    echo "Usage: $0 {start|stop|restart}"
    exit 1
esac
