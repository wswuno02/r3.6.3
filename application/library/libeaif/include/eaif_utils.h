#ifndef EAIF_UTILS_H_
#define EAIF_UTILS_H_

#include "mpi_base_types.h"
#include "mpi_errno.h"
#include "mpi_index.h"
#include "mpi_iva.h"
#include "mpi_dev.h"

typedef MPI_SIZE_S EaifFixedPointSize;
#define EAIF_FIXED_POINT_BS (8)

int EAIF_calcScaleFactor(int dst_width, int dst_height, const MPI_SIZE_S *src, EaifFixedPointSize *scale_factor);
int EAIF_assignFrameInfo(int fr_width, int fr_height, MPI_VIDEO_FRAME_INFO_S *frame_info);
int EAIF_copyScaledListWithBoundary(const MPI_SIZE_S *resoln, const EaifFixedPointSize *scale_factor,
                                    const MPI_IVA_OBJ_LIST_S *src, MPI_IVA_OBJ_LIST_S *dst);
int EAIF_fillImageDataJpeg(MPI_ECHN chn, unsigned char *data);
int EAIF_fillImageDataSnapshot(MPI_WIN idx, MPI_VIDEO_FRAME_INFO_S *frame_info);

#endif // EAIF_UTILS_H_
