#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "mpi_base_types.h"
#include "mpi_sys.h"
#include "mpi_dev.h"
#include "mpi_enc.h"

#include "cm_iva_eaif_resp.h"
#include "eaif.h"
#include "eaif_trc.h"
#include "eaif_algo.h"

//#define EAIF_STATIC_IMAGE

static const int g_fixed_point_sf_one = 1 << EAIF_FIXED_POINT_BS;

void eaif_initAlgo(const EAIF_PARAM_S *param, EaifAlgo *algo)
{
	algo->p = param;
	algo->face_preserve_prev = FALSE;
	algo->face_det_roi = FALSE;
}

void eaif_resetCounter(const EaifAlgo *algo, EaifStatusInternal *status)
{
	EaifObjAttrEx *obj_attr = NULL;
	int frame_count_th = 0;

	for (int i = 0; i < status->obj_cnt; i++) {
		obj_attr = &status->obj_attr_ex[i];

		if (obj_attr->confid_counter >= algo->p->pos_stop_count_th) {
			continue;
		}

		frame_count_th = (obj_attr->basic.label_num) ? algo->p->pos_classify_period :
		                                               algo->p->neg_classify_period;
		if (obj_attr->frame_counter > frame_count_th || !obj_attr->infer_counter) {
			obj_attr->frame_counter = 0;
			obj_attr->infer_counter++;
		}
	}
#if EAIF_DEBUG_INFO
	printf("[%s %d]: obj cnt: %d (id,confid,frame_count)", __func__, __LINE__, status->obj_cnt);
	for (int i = 0; i < status->obj_cnt; i++) {
		obj_attr = &status->obj_attr_ex[i];
		printf("(%d,%d,%d),", obj_attr->basic.id, obj_attr->confid_counter, obj_attr->frame_counter);
	}
	printf("\n");
#endif
}

void eaif_updateObjAttr(const MPI_IVA_OBJ_LIST_S *inObj_list, UINT16 obj_life_th, EaifStatusInternal *status)
{
	EaifObjAttrEx new_obj_attr[MPI_IVA_MAX_OBJ_NUM] = {};
	int marker[MPI_IVA_MAX_OBJ_NUM] = {};

	for (int i = 0; i < inObj_list->obj_num; i++) {
		const MPI_IVA_OBJ_ATTR_S *iva_obj = &inObj_list->obj[i];
		EaifObjAttrEx *new_eaif_obj = &new_obj_attr[i];
		EaifObjAttrEx *g_eaif_obj;

		bool b_new = 1;
		for (int j = 0; j < status->obj_cnt; j++) {
			if (marker[j])
				continue;
			g_eaif_obj = &status->obj_attr_ex[j];
			if (iva_obj->id == g_eaif_obj->basic.id) {
				marker[j] = 1;
				b_new = 0;
				if (inObj_list->obj[i].life >= obj_life_th) {
					g_eaif_obj->frame_counter++;
				} else {
					g_eaif_obj->basic.label_num = 0;
				}
				*new_eaif_obj = *g_eaif_obj;
				break;
			}
		}

		if (b_new) {
			new_eaif_obj->basic.id = iva_obj->id;
			new_eaif_obj->frame_counter = 0;
			new_eaif_obj->confid_counter = 0;
			new_eaif_obj->infer_counter = 0;
		}
	}

	status->obj_cnt = inObj_list->obj_num;
	memcpy(status->obj_attr_ex, new_obj_attr, sizeof(EaifObjAttrEx) * status->obj_cnt);
#if EAIF_DEBUG_INFO
	EaifObjAttrEx *obj_attr;
	static int cnt = 0;
	printf("[%s %d]: frame:%d obj cnt: %d (id,confid,frame_count)", __func__, __LINE__, cnt++, status->obj_cnt);
	for (int i = 0; i < status->obj_cnt; i++) {
		obj_attr = &status->obj_attr_ex[i];
		printf("(%d,%d,%d),", obj_attr->basic.id, obj_attr->confid_counter, obj_attr->frame_counter);
	}
	printf("\n");
#endif
	//eaif_tr("Status cnt:%d obj[0] Id:%d life:%d fr:%d\n", status->obj_cnt, status->obj_attr[0].id, inObj_list->obj[0].life, status->obj_attr[0].frame_counter);
	return;
}

int eaif_checkAppendable(const MPI_IVA_OBJ_LIST_S *ori_obj_list, const EaifAlgo *algo,
                         struct eaif_status_internal_s *status, MPI_IVA_OBJ_LIST_S *fin_obj_list)
{
	const int const append_priority_max = 0xffffff;

	int appendable = 0;
	int obj_num = 0;
	int priority_offset = 0;
	int max_priority = 0;
	int max_pri_obj_idx = 0;
	int frame_counter_th = algo->p->pos_stop_count_th;
	int priority_list[MPI_IVA_MAX_OBJ_NUM] = {};
	fin_obj_list->timestamp = ori_obj_list->timestamp;

	const EaifObjAttrEx *obj_attr = NULL;

	if (!status->obj_cnt) {
		status->obj_exist_any = 0;
		status->obj_exist_any_counter = 0;
		return appendable;
	}

	if (algo->p->obj_exist_classify_period) {
		if (status->obj_exist_any) {
			//printf("129 obj_exist_counter %d\n", status->obj_exist_any_counter);
			if (status->obj_exist_any_counter < algo->p->obj_exist_classify_period) {
				status->obj_exist_any_counter++;
				return appendable;
			} else
				status->obj_exist_any_counter = 0;
		}
	}

	for (int i = 0; i < status->obj_cnt; i++) {
		if (ori_obj_list->obj[i].life >= algo->p->obj_life_th) {
			obj_attr = &status->obj_attr_ex[i];

			if (obj_attr->confid_counter >= algo->p->pos_stop_count_th) {
				continue;
			}

			if (obj_attr->basic.label_num) {
				frame_counter_th = algo->p->pos_classify_period;
				priority_offset = 0;
			} else {
				frame_counter_th = algo->p->neg_classify_period;
				priority_offset = frame_counter_th;
			}

			if (!obj_attr->infer_counter) {
				priority_list[obj_num++] = append_priority_max;
				max_pri_obj_idx = i;
				appendable = 1;
				break;

			} else if (obj_attr->frame_counter > frame_counter_th) {
				eaif_info_l("APPEND CNT: %d, id:%d, frame:confid:infer(%d,%d,%d)\n", i,
				            obj_attr->basic.id, obj_attr->frame_counter, obj_attr->confid_counter,
				            obj_attr->infer_counter);
				priority_list[obj_num] =
				        abs(obj_attr->frame_counter - frame_counter_th) + priority_offset;
				obj_num++;
				appendable = 1;
				if (max_priority < priority_list[obj_num - 1]) {
					max_priority = priority_list[obj_num - 1];
					max_pri_obj_idx = i;
				}
			}
		}
	}

	fin_obj_list->obj[0] = ori_obj_list->obj[max_pri_obj_idx];
	fin_obj_list->obj_num = 1;

#if EAIF_DEBUG_INFO
	printf("[%s %d]: append:%d obj cnt: %d (id,confid:%d,frame,infer)", __func__, __LINE__, appendable, obj_num,
	       algo->p->pos_stop_count_th);
	for (int i = 0; i < status->obj_cnt; i++) {
		obj_attr = &status->obj_attr_ex[i];
		printf("(%d,%d,%d,%d),", obj_attr->basic.id, obj_attr->confid_counter, obj_attr->frame_counter,
		       obj_attr->infer_counter);
	}
	printf("\n");
#endif
	return appendable;
}

int eaif_checkDetAppendable(const MPI_IVA_OBJ_LIST_S *ori_obj_list, const EaifAlgo *algo,
                            MPI_IVA_OBJ_LIST_S *fin_obj_list)
{
	int obj_num = 0;
	for (int i = 0; i < ori_obj_list->obj_num; i++) {
		if (ori_obj_list->obj[i].life < algo->p->obj_life_th)
			continue;
		fin_obj_list->obj[obj_num] = ori_obj_list->obj[i];
		obj_num++;
	}
	fin_obj_list->obj_num = obj_num;
	return obj_num > 0;
}

///////////////////////////////////////////////////
/// object list
/// status copy utility functions
///////////////////////////////////////////////////

void eaif_copyObjList(const MPI_IVA_OBJ_LIST_S *src, MPI_IVA_OBJ_LIST_S *dst)
{
	dst->timestamp = src->timestamp;
	dst->obj_num = src->obj_num;
	memcpy(dst->obj, src->obj, sizeof(MPI_IVA_OBJ_ATTR_S) * src->obj_num);
}

void eaif_copyScaledObjList(const EaifFixedPointSize *scale_factor, const MPI_IVA_OBJ_LIST_S *src,
                            MPI_IVA_OBJ_LIST_S *dst)
{
	if (scale_factor->width == g_fixed_point_sf_one && scale_factor->height == g_fixed_point_sf_one) {
		eaif_copyObjList(src, dst);
		return;
	}

	dst->timestamp = src->timestamp;
	dst->obj_num = src->obj_num;
	MPI_IVA_OBJ_ATTR_S *left = NULL;
	const MPI_IVA_OBJ_ATTR_S *right = NULL;

	for (int i = 0; i < src->obj_num; i++) {
		right = &src->obj[i];
		left = &dst->obj[i];
		left->id = right->id;
		left->life = right->life;
		left->mv = right->mv;
		left->rect.sx = ((int)right->rect.sx * scale_factor->width) >> EAIF_FIXED_POINT_BS;
		left->rect.ex = ((int)right->rect.ex * scale_factor->width) >> EAIF_FIXED_POINT_BS;
		left->rect.sy = ((int)right->rect.sy * scale_factor->height) >> EAIF_FIXED_POINT_BS;
		left->rect.ey = ((int)right->rect.ey * scale_factor->height) >> EAIF_FIXED_POINT_BS;
	}
}

int EAIF_copyScaledListWithBoundary(const MPI_SIZE_S *resoln, const EaifFixedPointSize *scale_factor,
                                    const MPI_IVA_OBJ_LIST_S *src, MPI_IVA_OBJ_LIST_S *dst)
{
#define MIN_OBJ_SIZE (10)

	if (!resoln || !scale_factor || !src || !dst)
		return -EFAULT;

	dst->timestamp = src->timestamp;
	dst->obj_num = 0;
	MPI_IVA_OBJ_ATTR_S *left = NULL;
	const MPI_IVA_OBJ_ATTR_S *right = NULL;

	for (int i = 0; i < src->obj_num; i++) {
		right = &src->obj[i];

		MPI_RECT_POINT_S rect = right->rect;
		rect.sx = clamp(rect.sx, 0, rect.ex);
		rect.sy = clamp(rect.sy, 0, rect.ey);
		rect.ex = clamp(rect.ex, rect.sx, resoln->width);
		rect.ey = clamp(rect.ey, rect.sy, resoln->height);
		int obj_w = rect.ex - rect.sx + 1;
		int obj_h = rect.ey - rect.sy + 1;

		if (obj_w < MIN_OBJ_SIZE || obj_h < MIN_OBJ_SIZE)
			continue;

		left = &dst->obj[dst->obj_num];
		left->id = right->id;
		left->life = right->life;
		left->mv = right->mv;
		left->rect.sx = ((int)rect.sx * scale_factor->width) >> EAIF_FIXED_POINT_BS;
		left->rect.ex = ((int)rect.ex * scale_factor->width) >> EAIF_FIXED_POINT_BS;
		left->rect.sy = ((int)rect.sy * scale_factor->height) >> EAIF_FIXED_POINT_BS;
		left->rect.ey = ((int)rect.ey * scale_factor->height) >> EAIF_FIXED_POINT_BS;
		dst->obj_num++;
	}
	return 0;
}

int eaif_cpyInternalStatus(const EaifStatusInternal *src, EaifStatusInternal *dst)
{
	dst->timestamp = src->timestamp;
	dst->server_reachable = src->server_reachable;
	dst->obj_cnt = src->obj_cnt;
	memcpy(dst->obj_attr_ex, src->obj_attr_ex, sizeof(EaifObjAttrEx) * src->obj_cnt);

	return 0;
}

int eaif_mergeInternalStatus(const EaifStatusInternal *src, EaifStatusInternal *dst)
{
	dst->timestamp = src->timestamp;
	dst->server_reachable = src->server_reachable;
	dst->obj_exist_any = src->obj_exist_any;
	const EaifObjAttrEx *src_obj = NULL;
	EaifObjAttrEx *dst_obj = NULL;
	int marker[MPI_IVA_MAX_OBJ_NUM] = {};

	for (int i = 0; i < dst->obj_cnt; i++) {
		dst_obj = &dst->obj_attr_ex[i];
		for (int j = 0; j < src->obj_cnt; j++) {
			if (marker[j])
				continue;
			src_obj = &src->obj_attr_ex[j];
			if (dst_obj->basic.id == src_obj->basic.id) {
				marker[j] = 1;
				dst_obj->basic = src_obj->basic;
				dst_obj->confid_counter = src_obj->confid_counter;
				dst_obj->infer_counter = src_obj->infer_counter;
			}
		}
	}
	return 0;
}

int eaif_cpyRespStatus(const EaifStatusInternal *src, EAIF_STATUS_S *dst)
{
	dst->timestamp = src->timestamp;
	dst->server_reachable = src->server_reachable;
	dst->obj_cnt = src->obj_cnt;
	for (int i = 0; i < src->obj_cnt; i++)
		dst->obj_attr[i] = src->obj_attr_ex[i].basic;
	return 0;
}

int eaif_cpyScaledDetectionStatus(const EaifFixedPointSize *scale_factor, const EaifStatusInternal *src,
                                  EaifStatusInternal *dst)
{
	if (scale_factor->width == g_fixed_point_sf_one && scale_factor->height == g_fixed_point_sf_one) {
		*dst = *src;
		return 0;
	}

	dst->timestamp = src->timestamp;
	dst->obj_cnt = src->obj_cnt;
	EaifObjAttrEx *left = NULL;
	const EaifObjAttrEx *right = NULL;

	for (int i = 0; i < src->obj_cnt; i++) {
		right = &src->obj_attr_ex[i];
		left = &dst->obj_attr_ex[i];
		left->basic.id = right->basic.id;
		left->confid_counter = right->confid_counter;
		left->infer_counter = right->infer_counter;
		left->basic.rect.sx = ((int)right->basic.rect.sx << EAIF_FIXED_POINT_BS) / scale_factor->width;
		left->basic.rect.ex = ((int)right->basic.rect.ex << EAIF_FIXED_POINT_BS) / scale_factor->width;
		left->basic.rect.sy = ((int)right->basic.rect.sy << EAIF_FIXED_POINT_BS) / scale_factor->height;
		left->basic.rect.ey = ((int)right->basic.rect.ey << EAIF_FIXED_POINT_BS) / scale_factor->height;
		left->basic.label_num = right->basic.label_num;
		for (int j = 0; j < left->basic.label_num; j++) {
			strncpy(left->basic.category[j], right->basic.category[j], EAIF_CHAR_LEN - 1);
			strncpy(left->basic.prob[j], right->basic.prob[j], EAIF_CHAR_LEN - 1);
		}
	}
	return 0;
}

static inline int overlap(const MPI_RECT_POINT_S *r0, const MPI_RECT_POINT_S *r1)
{
	if ((r0->ex < r1->sx) || (r0->sx > r1->ex) || (r0->ey < r1->sy) || (r0->sy > r1->ey))
		return 0;
	return 1;
}

static void assignIdByOverlapRoi(const MPI_IVA_OBJ_LIST_S *ol, EaifStatusInternal *dst)
{
	EaifObjAttrEx *left = NULL;
	const MPI_IVA_OBJ_ATTR_S *right = NULL;

	int marker[MPI_IVA_MAX_OBJ_NUM] = {};
	for (int i = 0; i < dst->obj_cnt; i++) {
		left = &dst->obj_attr_ex[i];
		int assign = 0;
		for (int j = 0; j < ol->obj_num; j++) {
			if (marker[j] == 1)
				continue;
			right = &ol->obj[j];
			if (overlap(&left->basic.rect, &right->rect)) {
				left->basic.id = right->id;
				marker[j] = 1;
				assign = 1;
				break;
			}
		}
		if (!assign)
			left->basic.id = 0x3fffffff;
	}
}

int eaif_cpyScaledFaceStatus(const EaifFixedPointSize *scale_factor, const EaifStatusInternal *src,
                             const MPI_IVA_OBJ_LIST_S *ol, EaifStatusInternal *dst)
{
	if (scale_factor->width == g_fixed_point_sf_one && scale_factor->height == g_fixed_point_sf_one) {
		*dst = *src;
		assignIdByOverlapRoi(ol, dst);
		return 0;
	}

	dst->timestamp = src->timestamp;
	dst->obj_cnt = src->obj_cnt;
	EaifObjAttrEx *left = NULL;
	const EaifObjAttrEx *right = NULL;

	for (int i = 0; i < src->obj_cnt; i++) {
		right = &src->obj_attr_ex[i];
		left = &dst->obj_attr_ex[i];
		left->basic.id = right->basic.id;
		left->confid_counter = right->confid_counter;
		left->infer_counter = right->infer_counter;
		left->basic.rect.sx = ((int)right->basic.rect.sx << EAIF_FIXED_POINT_BS) / scale_factor->width;
		left->basic.rect.ex = ((int)right->basic.rect.ex << EAIF_FIXED_POINT_BS) / scale_factor->width;
		left->basic.rect.sy = ((int)right->basic.rect.sy << EAIF_FIXED_POINT_BS) / scale_factor->height;
		left->basic.rect.ey = ((int)right->basic.rect.ey << EAIF_FIXED_POINT_BS) / scale_factor->height;
		left->basic.label_num = right->basic.label_num;
		for (int j = 0; j < left->basic.label_num; j++) {
			strncpy(left->basic.category[j], right->basic.category[j], EAIF_CHAR_LEN - 1);
			strncpy(left->basic.prob[j], right->basic.prob[j], EAIF_CHAR_LEN - 1);
		}
	}
	assignIdByOverlapRoi(ol, dst);
	return 0;
}