/**
* @cond
*
* code fragment skipped by Doxygen.
*/

#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#include "agtx_prio.h"

#include "mpi_index.h"
#include "mpi_sys.h"
#include "mpi_base_types.h"
#include "mpi_errno.h"
#include "mpi_dev.h"
#include "mpi_enc.h"
#include "mpi_iva.h"

#include "eaif.h"
#include "eaif_algo.h"
#include "eaif_trc.h"
#include "eaif_utils.h"
#include "eaif_utils_internal.h"
#include "eaif_trc.h"

MPI_ECHN g_snapshot_chn;
MPI_BCHN bchn;
int g_eaif_verbose = 0;
int g_eaif_lite_debug = 0;

#define EAIF_SLEEP_TIME (10 * 1000)
#define EAIF_TRY_SLEEP_TIME 2
#define EAIF_SNAPSHOT_CHN 3
#define EAIF_TH_NAME "eaif"

#define EXPAND_ST(infost)                                                                                            \
	{                                                                                                            \
		eaif_tr("info->st %d (%s) [%d %d %d %d]\n", infost.obj_cnt, infost.obj_attr_ex[0].basic.category[0], \
		        infost.obj_attr_ex[0].basic.rect.sx, infost.obj_attr_ex[0].basic.rect.sy,                    \
		        infost.obj_attr_ex[0].basic.rect.ex, infost.obj_attr_ex[0].basic.rect.ey);                   \
	}

static int debugEmptyCb(const EaifInfo *info)
{
	return 0;
}

// conflict with avftr_common
int getMpiWinSize(const MPI_WIN idx, MPI_SIZE_S *res)
{
	INT32 ret;
	INT32 i;
	UINT32 dev_idx = MPI_GET_VIDEO_DEV(idx);
	UINT32 chn_idx = MPI_GET_VIDEO_CHN(idx);
	UINT32 win_idx = MPI_GET_VIDEO_WIN(idx);
	MPI_CHN_STAT_S chn_stat;
	MPI_CHN chn = { .dev = idx.dev, .chn = idx.chn };

	ret = MPI_DEV_queryChnState(chn, &chn_stat);
	if (ret != 0) {
		eaif_warn("Query channel state on channel %d on device %d failed\n", chn_idx, dev_idx);
		return ret;
	}

	if (!MPI_STATE_IS_ADDED(chn_stat.status)) {
		eaif_warn("Channel %d on device %d is not added\n", chn_idx, dev_idx);
		return ENODEV;
	}

	MPI_CHN_LAYOUT_S layout_attr;

	ret = MPI_DEV_getChnLayout(chn, &layout_attr);
	if (ret != 0) {
		eaif_warn("Get video channel %d layout attributes failed.\n", chn_idx);
		return ret;
	}

	/* FIXME: check window state */
	for (i = 0; i < layout_attr.window_num; i++) {
		if (idx.value == layout_attr.win_id[i].value) {
			res->width = layout_attr.window[i].width;
			res->height = layout_attr.window[i].height;
			break;
		}
	}
	if (i == layout_attr.window_num) {
		eaif_warn("Invalid video window index %d from video channel %d", win_idx, chn_idx);
		return EINVAL;
	}

	return 0;
}

static void genEaifDetect(const MPI_IVA_OBJ_LIST_S *ol, const EaifStatusInternal *src, EAIF_STATUS_S *dst)
{
	int i;
	//MPI_IVA_OBJ_ATTR_S *od = NULL;
	EAIF_OBJ_ATTR_S *obj = NULL;
	const EaifObjAttrEx *eobj = NULL;

	dst->obj_cnt = src->obj_cnt;
	for (i = 0; i < src->obj_cnt; i++) {
		obj = &dst->obj_attr[i];
		eobj = &src->obj_attr_ex[i];
		*obj = eobj->basic;
	}
}

static void genEaifClassify(const MPI_IVA_OBJ_LIST_S *ol, const EaifStatusInternal *src, EAIF_STATUS_S *dst)
{
	int i, j, set;
	EAIF_OBJ_ATTR_S *obj = NULL;
	const MPI_IVA_OBJ_ATTR_S *od = NULL;
	const EaifObjAttrEx *eobj = NULL;

	for (i = 0; i < ol->obj_num; i++) {
		od = &ol->obj[i];
		obj = &dst->obj_attr[i];
		set = 0;
		for (j = 0; j < src->obj_cnt; j++) {
			eobj = &src->obj_attr_ex[j];
			if (od->id == eobj->basic.id) {
				*obj = eobj->basic;
				set = 1;
				break;
			}
		}
		if (set == 0) {
			obj->id = od->id;
			obj->rect = od->rect;
			obj->label_num = 0;
		}
	}
}

static void genEaifRes(const EAIF_PARAM_S *p, const MPI_IVA_OBJ_LIST_S *obj_list, const EaifStatusInternal *src,
                       EAIF_STATUS_S *dst)
{
	int i;
	EAIF_OBJ_ATTR_S *obj = NULL;
	const MPI_IVA_OBJ_ATTR_S *od = NULL;
	if (src->server_reachable == EAIF_URL_REACHABLE) {
		/* TODO: Modify OD attr to support prob or other attri */
		if (p->api == EAIF_API_CLASSIFY || p->api == EAIF_API_CLASSIFY_CV ||
		    p->api == EAIF_API_HUMAN_CLASSIFY) {
			genEaifClassify(obj_list, src, dst);
		} else if (p->api == EAIF_API_DETECT || p->api == EAIF_API_FACEDET || p->api == EAIF_API_FACERECO) {
			genEaifDetect(obj_list, src, dst);
		}
	} else {
		if (p->api == EAIF_API_DETECT || p->api == EAIF_API_FACEDET) {
			dst->obj_cnt = 0;
		} else {
			for (i = 0; i < obj_list->obj_num; i++) {
				od = &obj_list->obj[i];
				obj = &dst->obj_attr[i];
				obj->id = od->id;
				obj->rect = od->rect;
				obj->label_num = 0;
			}
		}
	}
}

void *runEaif(void *inputs)
{
	/* 1. init param */
	/* 2. test url example get method */
	eaif_log_entry();
	const char *th_name = EAIF_TH_NAME;
	if (setThreadSchedAttr(th_name)) {
		printf("Failed to set thread %s!\n", th_name);
	}

	char url[EAIF_URL_CHAR_LEN] = { 0 };
	//EaifCtx *ctx = EAIF_GET_CTX();
	EAIF_ALGO_STATUS_S *algo_obj = (EAIF_ALGO_STATUS_S *)inputs;

	EAIF_PARAM_S *param = &algo_obj->param;
	EaifStatusInternal *status = &algo_obj->status;
	EaifInfo *info = &algo_obj->info;
	EaifAlgo *algo = &algo_obj->algo;
	InferenceModel *model = &algo_obj->model;
	MPI_IVA_OBJ_LIST_S local_obj_list = {};
	int ret = 0;
	int req_ret = 0;
	eaif_utilsSetUrl(info->mode, param, url);

	if (info->mode == EAIF_MODE_REMOTE) {
		while (1) {
			ret = eaif_utilsTesturl(param->url, status);
			if (ret || status->server_reachable == EAIF_URL_NONREACHABLE) {
				eaif_warn("Cannot reach target url:%s\n", param->url);
				sleep(EAIF_TRY_SLEEP_TIME);
			} else {
				eaif_debug("Server reaches! resp:%s\n", info->resp.data);
				break;
			}
			pthread_testcancel();
		}
	} else {
		status->server_reachable = EAIF_URL_REACHABLE;
	}

	do {
		pthread_testcancel();
		ret = eaif_utilsInitRequestBuf(info);
		usleep(EAIF_RUN_USLEEP);
	} while (ret == -1);

	EAIF_calcScaleFactor(param->snapshot_width, param->snapshot_height, &info->src_resoln, &info->scale_factor);
	if (info->mode == EAIF_MODE_INAPP) {
		EAIF_assignFrameInfo(param->snapshot_width, param->snapshot_height,
		                     &algo_obj->info.payload.inapp->frame_info);
	}

	/* onload face data upload activation for face recognition model */
	if (info->mode == EAIF_MODE_INAPP && param->api == EAIF_API_FACERECO) {
		if (param->api == EAIF_API_FACERECO) {
			EAIF_INF_UTILS_S inf_utils = param->inf_utils;
			inf_utils.cmd = EAIF_INF_FACE_LOAD;
			char face_file[256] = {};
			sprintf(face_file, "%s/%s", inf_utils.dir, inf_utils.face_db);
			if (access(face_file, F_OK) == 0) {
				int ret = eaif_faceUtils(&inf_utils, model);
				if (ret) {
					eaif_warn("Cannot preload existing face data!\n");
				}
			}
		}
	}

	/* 3. start loop */
	while (1) {
		ret = 0;

		if (pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL) != 0) {
			eaif_warn("Cannot set disable pthread cancel state.\n");
		}

		if (algo_obj->running == 0) {
			info->req_sta.val = 0;
			if (pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL) != 0) {
				eaif_warn("Set cancelstate ENABLE failed.\n");
			}
			break;
		}

		if (info->req_sta.append) {
			pthread_mutex_lock(&algo_obj->lock);
			if (param->api != EAIF_API_DETECT && param->api != EAIF_API_FACEDET &&
			    param->api != EAIF_API_FACERECO) {
				eaif_debug("IS NOT DETECT\n");
				// lock for state & input copy
				eaif_resetCounter(algo, status);
				//EXPAND_ST(info->st);
				eaif_cpyInternalStatus(status, &info->st);
				//EXPAND_ST(info->st);
			}
			local_obj_list.obj_num = 0;
			eaif_copyObjList(&info->obj_list, &local_obj_list);
			pthread_mutex_unlock(&algo_obj->lock);
			/* Do not send if no append request */
			algo_obj->fill_payload_cb(&local_obj_list, param, info);

			//eaif_debug(" od: time:%s %s\n", info->payload.time_payload, info->payload.meta_payload);
			if ((info->mode == EAIF_MODE_REMOTE && info->payload.remote->data.size == 0) ||
			    (info->mode == EAIF_MODE_INAPP && info->payload.inapp->size == 0)) {
				info->req_sta.append = 0;
				sleep(EAIF_TRY_SLEEP_TIME);
				if (pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL) != 0) {
					eaif_warn("Set cancelstate ENABLE failed.\n");
				}
				pthread_testcancel();
				continue;
			}

			// lock for state request state writer
			pthread_mutex_lock(&algo_obj->lock);
			info->req_sta.append = 0;
			info->req_sta.wait_res = 1;
			pthread_mutex_unlock(&algo_obj->lock);

			pthread_mutex_lock(&algo_obj->inf_lock);
			ret = eaif_utilsSendRequest(url, info, model);
			pthread_mutex_unlock(&algo_obj->inf_lock);
			info->agtx_resp.success = 0;

			pthread_mutex_lock(&algo_obj->lock);
			info->req_sta.wait_res = 0;
			if (!ret) {
				req_ret = eaif_utilsDecodeResult(info->mode, model, &info->resp, &info->st,
				                                 &info->agtx_resp);
				//EXPAND_ST(info->st);
				algo_obj->debug_cb(info);

				// lock for state request state writer
				info->req_sta.wait_res = 0;
				if (req_ret || info->agtx_resp.success == 0) {
					info->req_sta.respond = 0;
					eaif_debug("Decode fails: resp msg:%s\n", info->resp.data);
				} else {
					info->req_sta.respond = 1;
				}
				info->req_sta.recv = 0;
			}
			pthread_mutex_unlock(&algo_obj->lock);

			if (ret || info->agtx_resp.success == 0) {
				eaif_warn("Request fails from %s!\n", url);
				eaif_debug("resp: %s\n", info->resp.data);
				info->st.obj_cnt = 0;
				sleep(EAIF_TRY_SLEEP_TIME);
				if (pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL) != 0) {
					eaif_warn("Set cancelstate ENABLE failed.\n");
				}
			} else {
			}
			if (pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL) != 0) {
				eaif_warn("Cannot set disable pthread cancel state.\n");
			}
			pthread_mutex_lock(&algo_obj->lock);
			//EXPAND_ST(info->st);
			if (param->api == EAIF_API_CLASSIFY || param->api == EAIF_API_HUMAN_CLASSIFY)
				eaif_mergeInternalStatus(&info->st, status);
			else if (param->api == EAIF_API_DETECT || param->api == EAIF_API_FACEDET)
				eaif_cpyScaledDetectionStatus(&info->scale_factor, &info->st, status);
			else if (param->api == EAIF_API_FACERECO)
				eaif_cpyScaledFaceStatus(&info->scale_factor, &info->st, &local_obj_list, status);
			//EXPAND_ST(algo_obj->status);
			pthread_mutex_unlock(&algo_obj->lock);

			eaif_debug(" resp: %s id:%d name:\"%s\" obj_cnt:%d->%d\n", info->resp.data,
			           info->st.obj_attr_ex[0].basic.id, info->st.obj_attr_ex[0].basic.category[0],
			           info->st.obj_cnt, status->obj_cnt);

		} else {
		}

		if (pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL) != 0) {
			eaif_warn("Set cancelstate ENABLE failed.\n");
		}

		pthread_testcancel();
		usleep(EAIF_RUN_USLEEP);
	}
	eaif_log_exit();
	/* 4. if obj_list is appending to send request => send */
	return 0;
}

/**
 * @endcond
 */

/**
 * @brief Activate EAIF running thread
 * @param[in]  instance        EAIF_INSTANCE_S pointer
 * @return The execution result.
 * @retval 0                           success.
 * @retval -EFAULT                     input pointer is null.
 * @retval -EINVAL                     unknown fail to create running thread.
 * @see EAIF_deactivate()
 */
int EAIF_activate(EAIF_INSTANCE_S *instance)
{
	eaif_log_entry();
	int ret = 0;

	if (!instance) {
		eaif_warn("Input instance is null!\n");
		return -EFAULT;
	}

	EAIF_ALGO_STATUS_S *algo_obj = instance->algo_status;
	MPI_WIN idx = algo_obj->idx;

	EaifInfo *info = &algo_obj->info;
	pthread_mutex_lock(&algo_obj->lock);
	if (algo_obj->running) {
		eaif_warn("EAIF is already running!\n");
		goto unlock;
	}

	MPI_CHN_STAT_S chn_stat;
	MPI_CHN chn = MPI_VIDEO_CHN(idx.dev, idx.chn);
	ret = MPI_DEV_queryChnState(chn, &chn_stat);
	if (ret != 0) {
		eaif_warn("Query channel state on channel %d on device %d failed\n", idx.chn, idx.dev);
		goto unlock;
	}

	ret = MPI_STATE_IS_ADDED(chn_stat.status);
	if (ret == 0) {
		eaif_warn("Channel %d on device %d is not added\n", idx.chn, idx.dev);
		goto unlock;
	}

	MPI_CHN_ATTR_S chn_attr;
	ret = MPI_DEV_getChnAttr(chn, &chn_attr);
	if (ret != 0) {
		eaif_warn("Get video channel %d layout attributes failed.\n", idx.chn);
		goto unlock;
	}

	char *verbose = getenv("EAIF_VERBOSE");
	if (verbose) {
		g_eaif_verbose = atoi(verbose);
		eaif_tr("EAIF VERBOSE is detected: %d!\n", g_eaif_verbose);
	}

	char *capture = getenv("INF_CAP_PREFIX");
	if (capture) {
		g_eaif_lite_debug = 1;
		eaif_tr("EAIF INF CAPTURE is detected: %s!\n", capture);
	}

	algo_obj->info.mode = eaif_utilsDetermineMode(algo_obj->param.url);
	algo_obj->info.algo = &algo_obj->algo;

	if (algo_obj->info.mode == EAIF_MODE_INAPP) {
		strncpy(algo_obj->model.path, algo_obj->param.api_models[algo_obj->param.api], EAIF_MODEL_LEN);
	}
	algo_obj->fill_payload_cb = eaif_utilsSetupRequest;
	algo_obj->debug_cb = debugEmptyCb;

	getMpiWinSize(idx, &info->src_resoln);
	eaif_initAlgo(&algo_obj->param, &algo_obj->algo);
	eaif_utilsInitModule(&algo_obj->param, algo_obj->info.mode, &algo_obj->model, &g_snapshot_chn, &bchn);

	algo_obj->running = 1;
	info->req_sta.val = 0;
	info->prev_obj_list.obj_num = 0;

	if (pthread_create(&algo_obj->tid_eaif, NULL, runEaif, (void *)algo_obj) != 0) {
		eaif_warn("Cannot create EAIF thread for window 0x%x\n.", idx.value);
		ret = -EINVAL;
		goto error;
	}
	char *name = EAIF_TH_NAME; // max. length is 16
	if (pthread_setname_np(algo_obj->tid_eaif, name) != 0) {
		eaif_warn("Set EAIF thread name failed.\n");
		goto error;
	}
	pthread_mutex_unlock(&algo_obj->lock);

	return 0;

error:
	eaif_utilsExitModule(&algo_obj->param, &algo_obj->model, bchn);
unlock:
	pthread_mutex_unlock(&algo_obj->lock);
	eaif_log_exit();
	return ret;
}

/**
 * @brief Dectivate EAIF running thread
 * @param[in]  instance        EAIF_INSTANCE_S pointer
 * @return The execution result.
 * @retval 0                           success.
 * @retval -ENODEV                     EAIF thread is not runnning.	
 * @retval -EFAULT                     Fail to join thread/ input pointer is null.
 * @see EAIF_activate()
 */
int EAIF_deactivate(EAIF_INSTANCE_S *instance)
{
	eaif_log_entry();
	EAIF_ALGO_STATUS_S *algo_obj = instance->algo_status;
	EaifInfo *info = &algo_obj->info;
	MPI_WIN idx = algo_obj->idx;
	VOID *res;
	int ret = 0;

	if (!instance) {
		eaif_warn("Input pointer cannot be null!\n");
		return -EFAULT;
	}

	if (algo_obj->running == 0) {
		eaif_warn("EAIF is not running at win:0x%x!\n", idx.value);
		return -ENODEV;
	}

	ret = pthread_cancel(algo_obj->tid_eaif);
	if (ret != 0) {
		eaif_err("Cancel thread to run eaif failed.\n");
		return -EFAULT;
	}

	ret = pthread_join(algo_obj->tid_eaif, &res);
	if (ret != 0) {
		eaif_err("Join thread to run eaif failed.\n");
		return -EFAULT;
	}

	if (res == PTHREAD_CANCELED) {
		eaif_info_l("Edge AI assisted feature thread was canceled\n");
	} else {
		eaif_info_l("Edge AI assisted feature wasn't canceled (shouldn't happen!)\n");
		return -EFAULT;
	}

	eaif_utilsExitModule(&algo_obj->param, &algo_obj->model, bchn);

	pthread_mutex_lock(&algo_obj->lock);
	algo_obj->running = 0;
	info->req_sta.val = 0;
	eaif_utilsReleaseBuf(info);
	pthread_mutex_unlock(&algo_obj->lock);
	eaif_log_exit();
	return ret;
}

/**
 * @brief Send request to eaif module waiting list and get most updated result.
 * @param[in]  instance        EAIF_INSTANCE_S pointer
 * @param[in]  obj_list        MPI object list
 * @param[out]  status         most updated result from inference module
 * @return The execution result.
 * @retval 0                           success.
 * @retval -EFAULT                     input pointers ars null.
 * @see EAIF_testRequestV2()
 */
int EAIF_testRequest(EAIF_INSTANCE_S *instance, const MPI_IVA_OBJ_LIST_S *obj_list, EAIF_STATUS_S *status)
{
	eaif_log_entry();
	if (!instance || !obj_list || !status) {
		eaif_warn("Pointer to the inputs of EAIF_testRequest() should not be NULL.\n");
		return -EFAULT;
	}

	EAIF_ALGO_STATUS_S *algo_obj = instance->algo_status;
	EaifInfo *info = &algo_obj->info;
	EaifStatusInternal *pstatus = &algo_obj->status;
	const EaifAlgo *algo = &algo_obj->algo;
	const EAIF_PARAM_S *p = &algo_obj->param;
	MPI_WIN idx = algo_obj->idx;

	pthread_mutex_lock(&algo_obj->lock);
	if (algo_obj->running == 0) {
		eaif_warn("[EAIF WARN] eaif on window 0x%x is not running!\n", idx.value);
		pthread_mutex_unlock(&algo_obj->lock);
		return 0;
	}

	if (pstatus->server_reachable == EAIF_URL_REACHABLE) {
		if (algo_obj->param.api == EAIF_API_DETECT || algo_obj->param.api == EAIF_API_FACEDET ||
		    algo_obj->param.api == EAIF_API_FACERECO) {
			int detect_period = (p->detection_period) ? p->detection_period : 1;
			if (!info->detect_counter)
				info->req_sta.append = 1;
			info->detect_counter++;
			if (info->detect_counter >= detect_period)
				info->detect_counter = 0;
			//info->obj_list.obj_num = 0;
			if (algo_obj->param.api == EAIF_API_FACEDET && algo->face_preserve_prev &&
			    obj_list->obj_num == 0) {
				info->obj_list = info->prev_obj_list;
			} else {
				if (p->inf_with_obj_list) {
					if (!eaif_checkDetAppendable(obj_list, algo, &info->obj_list)) {
						info->req_sta.append = 0;
						info->req_sta.recv = 1;
					}
				} else {
					info->obj_list = *obj_list;
				}
			}
		} else if (obj_list->obj_num == 0) {
			info->req_sta.append = 0;
			info->req_sta.recv = 1;
			info->obj_list.obj_num = 0;
			info->st.obj_exist_any = 0;
			pstatus->timestamp = obj_list->timestamp;
			pstatus->obj_cnt = 0;
			pstatus->obj_exist_any = 0;
			pstatus->obj_exist_any_counter = 0;
		} else {
			eaif_updateObjAttr(obj_list, p->obj_life_th, pstatus);
			int appendable = eaif_checkAppendable(obj_list, algo, pstatus, &info->obj_list);
			eaif_debug("req_sta:0x%x ol_cnt:%d ol_num(extlife):id:%d \n", info->req_sta.val,
			           info->obj_list.obj_num, info->obj_list.obj[0].id);
			if (appendable) {
				info->req_sta.append = 1;
			} else {
				info->req_sta.append = 0;
				info->req_sta.recv = 1;
			}
			eaif_debug("req_sta:0x%x ol_cnt:%d ol_num(extlife):id:%d \n", info->req_sta.val,
			           info->obj_list.obj_num, info->obj_list.obj[0].id);
		}

		if (status) {
			/* copy current status */
			if (info->req_sta.respond) {
				eaif_debug("status cnt:%d (%s) [%d %d %d %d]\n",
					pstatus->obj_cnt, pstatus->obj_attr_ex[0].basic.category[0],
					pstatus->obj_attr_ex[0].basic.rect.sx,
					pstatus->obj_attr_ex[0].basic.rect.sy,
					pstatus->obj_attr_ex[0].basic.rect.ex,
					pstatus->obj_attr_ex[0].basic.rect.ey);
				eaif_cpyRespStatus(pstatus, status);
				info->req_sta.respond = 0;
				info->req_sta.recv = 1;
			} else if (info->req_sta.recv) {
				eaif_cpyRespStatus(pstatus, status);
			} else {
				// do nothing
			}
		}
	} else {
		status->server_reachable = EAIF_URL_NONREACHABLE;
	}

	pthread_mutex_unlock(&algo_obj->lock);
	eaif_log_exit();
	return 0;
}

/**
 * @brief Send request to eaif module waiting list and get most updated result.
 * @param[in]  instance        EAIF_INSTANCE_S pointer
 * @param[in]  obj_list        MPI object list
 * @param[out]  status         most updated result and merge to obj list deps on params.
 * @return The execution result.
 * @retval 0                           success.
 * @retval -EFAULT                     input pointers ars null.
 * @see EAIF_testRequestV2()
 */
int EAIF_testRequestV2(EAIF_INSTANCE_S *instance, const MPI_IVA_OBJ_LIST_S *obj_list, EAIF_STATUS_S *status)
{
	eaif_log_entry();
	if (!instance || !obj_list || !status) {
		eaif_warn("Pointer to the inputs of EAIF_testRequest() should not be NULL.\n");
		return -EFAULT;
	}

	EAIF_ALGO_STATUS_S *algo_obj = instance->algo_status;
	EaifInfo *info = &algo_obj->info;
	EaifStatusInternal *pstatus = &algo_obj->status;
	const EaifAlgo *algo = &algo_obj->algo;
	const EAIF_PARAM_S *p = &algo_obj->param;
	MPI_WIN idx = algo_obj->idx;

	pthread_mutex_lock(&algo_obj->lock);
	if (algo_obj->running == 0) {
		eaif_warn("[EAIF WARN] eaif on window 0x%x is not running!\n", idx.value);
		pthread_mutex_unlock(&algo_obj->lock);
		return 0;
	}

	if (pstatus->server_reachable == EAIF_URL_REACHABLE) {
		if (algo_obj->param.api == EAIF_API_DETECT || algo_obj->param.api == EAIF_API_FACEDET ||
		    algo_obj->param.api == EAIF_API_FACERECO) {
			int detect_period = (p->detection_period) ? p->detection_period : 1;
			if (!info->detect_counter)
				info->req_sta.append = 1;
			info->detect_counter++;
			if (info->detect_counter >= detect_period)
				info->detect_counter = 0;
			//info->obj_list.obj_num = 0;
			if (algo_obj->param.api == EAIF_API_FACEDET && algo->face_preserve_prev &&
			    obj_list->obj_num == 0) {
				info->obj_list = info->prev_obj_list;
			} else {
				if (p->inf_with_obj_list) {
					if (!eaif_checkDetAppendable(obj_list, algo, &info->obj_list)) {
						info->req_sta.append = 0;
						info->req_sta.recv = 1;
					}
				} else {
					info->obj_list = *obj_list;
				}
			}
		} else if (obj_list->obj_num == 0) {
			info->req_sta.append = 0;
			info->req_sta.recv = 1;
			info->obj_list.obj_num = 0;
			info->st.obj_exist_any = 0;
			pstatus->timestamp = obj_list->timestamp;
			pstatus->obj_cnt = 0;
			pstatus->obj_exist_any = 0;
			pstatus->obj_exist_any_counter = 0;
		} else {
			eaif_updateObjAttr(obj_list, p->obj_life_th, pstatus);
			int appendable = eaif_checkAppendable(obj_list, algo, pstatus, &info->obj_list);
			eaif_debug("req_sta:0x%x ol_cnt:%d ol_num(extlife):id:%d \n", info->req_sta.val,
			           info->obj_list.obj_num, info->obj_list.obj[0].id);
			if (appendable) {
				info->req_sta.append = 1;
			} else {
				info->req_sta.append = 0;
				info->req_sta.recv = 1;
			}
			eaif_debug("req_sta:0x%x ol_cnt:%d ol_num(extlife):id:%d \n", info->req_sta.val,
			           info->obj_list.obj_num, info->obj_list.obj[0].id);
		}

		if (status) {
			/* copy current status */
			if (info->req_sta.respond) {
				genEaifRes(p, obj_list, pstatus, status);
				info->req_sta.respond = 0;
				info->req_sta.recv = 1;
			} else if (info->req_sta.recv) {
				genEaifRes(p, obj_list, pstatus, status);
			} else {
				// do nothing
			}
		}
	} else {
		status->server_reachable = EAIF_URL_NONREACHABLE;
	}

	pthread_mutex_unlock(&algo_obj->lock);
	eaif_log_exit();
	return 0;
}

/**
 * @brief create EAIF instance.
 * @param[in]  idx         video window index.
 * @see EAIF_deleteInstance
 * @retval NULL                    No more space to register idx / malloc EAIF instance failed
 * @retval EAIF_INSTANCE_S*        success.
 */
EAIF_INSTANCE_S *EAIF_newInstance(MPI_WIN idx)
{
	eaif_log_entry();
	EAIF_INSTANCE_S *eaif_instance = (EAIF_INSTANCE_S *)malloc(sizeof(EAIF_INSTANCE_S));
	if (!eaif_instance) {
		eaif_warn(" Fail to malloc memory for eaif_instance.\n");
		return NULL;
	}

	eaif_instance->algo_status = (EAIF_ALGO_STATUS_S *)malloc(sizeof(EAIF_ALGO_STATUS_S));
	if (!eaif_instance->algo_status) {
		free(eaif_instance);
		eaif_warn(" Fail to malloc memory for eaif_algo_status.\n");
		return NULL;
	}

	*eaif_instance->algo_status = (EAIF_ALGO_STATUS_S) { .lock = PTHREAD_MUTEX_INITIALIZER,
												.inf_lock = PTHREAD_MUTEX_INITIALIZER,
												.running = 0,
												.idx = idx,
												.param = { .target_idx = { { 0 } },
															.snapshot_width = 0,
															.snapshot_height = 0,
															.api = 0,
															.url = { "http://192.168.87.87:1234" },
															.pos_stop_count_th = 3,
															.pos_classify_period = 100,
															.neg_classify_period = 25,
															.obj_exist_classify_period = 0,
															.data_fmt = EAIF_DATA_JPEG,
															.api_models = {
																"face_reco",
																"detect",
																"classify",
																"classify_cv",
																"human_classify"
															},
															},
												.info = { 0 },
												.status = { 0 } };

	eaif_log_exit();
	return eaif_instance;
}

/**
 * @brief Delete EAIF instance.
 * @param[in]  instance      EAIF instance to be deleted.
 * @see EAIF_addInstance
 * @retval 0                 success.
 * @retval -EFAULT           input variables are null
 */
int EAIF_deleteInstance(EAIF_INSTANCE_S **instance)
{
	eaif_log_entry();
	/* Free EAIF instance*/
	if (instance == NULL) {
		eaif_warn("Input pointer address EAIF instance should not be NULL.\n");
		return -EFAULT;
	}

	if (*instance == NULL) {
		eaif_warn("Pointer to the EAIF instance should not be NULL.\n");
		return -EFAULT;
	}

	if ((*instance)->algo_status == NULL) {
		eaif_warn("Pointer to the EAIF algo instance should not be NULL.\n");
		return -EFAULT;
	}

	free((*instance)->algo_status);
	free(*instance);
	*instance = NULL;

	eaif_log_exit();
	return 0;
}

/**
 * @brief Get parameters of EAIF.
 * @param[in]  instance   EAIF instance.
 * @param[out] param      EAIF parameters.
 * @see EAIF_setParam
 * @retval 0              success.
 * @retval -EFAULT        input variables are null
 */
int EAIF_getParam(EAIF_INSTANCE_S *instance, EAIF_PARAM_S *param)
{
	eaif_log_entry();
	if (!instance || !param) {
		eaif_warn("Input pointer variables are NULL!\n");
		return -EFAULT;
	}

	pthread_mutex_lock(&instance->algo_status->lock);
	memcpy(param, &instance->algo_status->param, sizeof(EAIF_PARAM_S));
	pthread_mutex_unlock(&instance->algo_status->lock);
	eaif_log_exit();
	return 0;
}

/**
 * @brief Check parameters of EAIF.
 * @param[in]  param      EAIF parameters.
 * @see EAIF_setParam
 * @retval 0              success.
 * @retval -EFAULT        input variable is = null
 * @retval -EINVAL        input variable is invalid
 * @retval -ENODEV        video index, inapp target file not found.
 */
int EAIF_checkParam(const EAIF_PARAM_S *param)
{
	if (!param) {
		return -EFAULT;
	}
	int ret = eaif_utilsCheckParam(param);
	return -ret;
}

/**
 * @brief set parameters of EAIF.
 * @param[in]  instance   EAIF instance.
 * @param[in]  param      EAIF parameters.
 * @see EAIF_getParam
 * @retval 0              success.
 * @retval -EFAULT        inputs variables are null
 */
int EAIF_setParam(EAIF_INSTANCE_S *instance, const EAIF_PARAM_S *param)
{
	eaif_log_entry();
	if (!instance || !param) {
		eaif_warn("Input variables of EAIF_setParam Cannot be NULL!\n");
		return -EFAULT;
	}

	/* set param */
	EAIF_ALGO_STATUS_S *algo_obj = instance->algo_status;
	EAIF_PARAM_S *local_param = &algo_obj->param;
	EaifStatusInternal *status = &algo_obj->status;

	int restart = 0;
	int running = algo_obj->running;

	// check for url, snapshot index, snapshot size, api method
	if (strcmp(local_param->url, param->url) || (local_param->target_idx.value != param->target_idx.value) ||
	    local_param->api != param->api || local_param->snapshot_width != param->snapshot_width ||
	    local_param->snapshot_height != param->snapshot_height || local_param->data_fmt != param->data_fmt ||
	    strncmp(local_param->api_models[param->api], param->api_models[param->api], EAIF_URL_CHAR_LEN)) {
		restart = 1;
	}

	if (restart && running)
		EAIF_deactivate(instance);

	pthread_mutex_lock(&algo_obj->lock);
	memcpy(local_param, param, sizeof(EAIF_PARAM_S));
	eaif_initAlgo(&algo_obj->param, &algo_obj->algo);
	status->obj_cnt = 0;
	pthread_mutex_unlock(&algo_obj->lock);

	if (restart && running)
		EAIF_activate(instance);


	eaif_log_exit();
	return 0;
}

/**
 * @brief Apply face utils for inapp eaif.
 * @param[in]  instance   EAIF instance.
 * @param[in]  param      EAIF parameters.
 * @see EAIF_setParam
 * @retval 0              success.
 * @retval -EFAULT        inputs variables are null
 * @retval -ENODEV        input files are invalid/ not found, modules not activated
 * @retval -EINVAL        onloaded model format is not compatible for face application
 */
int EAIF_applyFaceUtils(EAIF_INSTANCE_S *instance, const EAIF_PARAM_S *param)
{
	eaif_log_entry();
	if (!instance || !param) {
		eaif_warn("Input variables of EAIF_setParam Cannot be NULL!\n");
		return -EFAULT;
	}

	/* set param */
	EAIF_ALGO_STATUS_S *algo_obj = instance->algo_status;
	InferenceModel *model = &algo_obj->model;

	int running = algo_obj->running;

	// check for url, snapshot index, snapshot size, api method
	if (!running || algo_obj->info.mode != EAIF_MODE_INAPP) {
		eaif_warn("EAIF inapp mode is not running!\n");
		return -ENODEV;
	}

	pthread_mutex_lock(&algo_obj->inf_lock);
	int ret = eaif_faceUtils(&param->inf_utils, model);
	pthread_mutex_unlock(&algo_obj->inf_lock);

	eaif_log_exit();
	return ret;
}
