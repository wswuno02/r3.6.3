#ifndef EAIF_TRC_H_
#define EAIF_TRC_H_

#include <time.h>
#include <stdio.h>
#include <sys/stat.h>
#include "eaif_algo.h"

#define EAIF_DEBUG_INFO (0)

#ifdef UNIT_TEST
#define EAIF_STATIC
#else
#define EAIF_STATIC
#endif /* !UNIT_TEST */

#define EAIF_FMT(fmt) "[%s:%d] " fmt
#define EAIF_FMT_WARN(fmt) "[EAIF] [WARN] " fmt
#define EAIF_FMT_ERR(fmt) "[EAIF] [WARN] " fmt

#define eaif_tr(fmt, args...) printf(EAIF_FMT(fmt), __func__, __LINE__, ##args)

#define eaif_info_h(fmt, args...) eaif_tr(fmt, ##args)
#define eaif_info_m(fmt, args...)
#define eaif_info_l(fmt, args...)
#define eaif_warn(fmt, args...) eaif_tr(EAIF_FMT_WARN(fmt), ##args)
#define eaif_err(fmt, args...) eaif_tr(EAIF_FMT_ERR(fmt), ##args)

//#define DEBUG_EAIF_API

//#define EAIF_ALGO_DEBUG
#ifdef EAIF_ALGO_DEBUG
#define EAIF_FMT_DEBUG(fmt) "[%s:%d] [DEBUG]" fmt
#define eaif_debug(fmt, args...) printf(EAIF_FMT_DEBUG(fmt), __func__, __LINE__, ##args)
#else
#define eaif_debug(fmt, args...)
#endif /* !EAIF_DEBUG */

//#define EAIF_ENTRY
#ifdef EAIF_ENTRY
#define eaif_log_entry() printf("%s:%d Entry\n", __func__, __LINE__)
#define eaif_log_exit() printf("%s:%d Exit\n", __func__, __LINE__)
#else
#define eaif_log_entry()
#define eaif_log_exit()
#endif

#define TI_TIC(start) clock_gettime(CLOCK_MONOTONIC_RAW, &start)

#define TI_TOC(str, start)                                                                               \
	do {                                                                                             \
		struct timespec end;                                                                     \
		uint64_t delta_us;                                                                       \
		float delta_s;                                                                           \
		clock_gettime(CLOCK_MONOTONIC_RAW, &end);                                                \
		delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000; \
		delta_s = (float)delta_us / 1000000;                                                     \
		printf("%s Elapsed time: %.8f (s).\n", str, delta_s);                                    \
	} while (0)

#endif /* !EAIF_TRC_H_ */