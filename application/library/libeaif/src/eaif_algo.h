#ifndef EAIF_ALGO_H_
#define EAIF_ALGO_H_

#include <pthread.h>
#include <string.h>

#include "cm_iva_eaif_resp.h"

#include "eaif.h"
#include "eaif_utils.h"
#include "eaif_utils_internal.h"

//#define EAIF_ALGO_DEBUG

#define EAIF_STR_DATA "data"
#define EAIF_STR_FMT "format"
#define EAIF_STR_META "meta"
#define EAIF_STR_TIME "time"
#define EAIF_STR_SHAPE "shape"

#define EAIF_STR_API_FMT "%s/predict/%s"

#define EAIF_STR_FMT_JPG "jpg"
#define EAIF_STR_FMT_RGB "rgb"
#define EAIF_STR_FMT_Y "y__"
#define EAIF_STR_FMT_MPI_JPG "mpi_jpg"
#define EAIF_STR_FMT_MPI_RGB "mpi_rgb"
#define EAIF_STR_FMT_MPI_Y "mpi_y__"

#define min(x, y) ((x) > (y)) ? (y) : (x)
#define max(x, y) ((x) < (y)) ? (y) : (x)
#define clamp(a, l, h) (((a) < (l)) ? (l) : (((a) > (h)) ? (h) : (a)))

typedef enum {
	EAIF_REQUEST_NONE,
	EAIF_REQUEST_APPEND,
	EAIF_REQUEST_WAIT_RES,
	EAIF_REQUEST_RESPOND,
	EAIF_REQUEST_RECV,
	EAIF_REQUEST_NUM,
} EAIF_REQUEST_STATUS_E;

typedef union {
	struct {
		unsigned int append : 1;
		unsigned int wait_res : 1;
		unsigned int respond : 1;
		unsigned int recv : 1;
	};
	unsigned int val;
} EaifRequestState;

typedef struct {
	int frame_counter;
	int confid_counter;
	int infer_counter;
	EAIF_OBJ_ATTR_S basic;
} EaifObjAttrEx;

typedef struct eaif_status_internal_s {
	EAIF_URL_REACHABLE_E server_reachable;
	UINT32 timestamp;
	UINT32 obj_cnt;
	UINT32 obj_exist_any;
	UINT32 obj_exist_any_counter;
	EaifObjAttrEx obj_attr_ex[MPI_IVA_MAX_OBJ_NUM];
} EaifStatusInternal;

typedef struct {
	const EAIF_PARAM_S *p;
	bool face_preserve_prev;
	bool face_det_roi;
} EaifAlgo;

typedef struct eaif_info_s {
	EAIF_INFERENCE_MODE_E mode;
	int payload_size;
	int server_reachable;
	int detect_counter;
	EaifRequestState req_sta;
	MPI_SIZE_S src_resoln;
	EaifFixedPointSize scale_factor;
	MPI_IVA_OBJ_LIST_S obj_list;
	MPI_IVA_OBJ_LIST_S prev_obj_list;
	AGTX_IVA_EAIF_RESP_S agtx_resp;
	EaifStatusInternal st;
	EaifRespond resp;
	EaifRequestData payload;
	const EaifAlgo *algo;
	EAIF_INF_COMMAND_E inf_cmd;
} EaifInfo;

struct eaif_algo_status_s {
	pthread_mutex_t lock; /* lock for algo */
	pthread_mutex_t inf_lock; /* lock for inference module */
	pthread_t tid_eaif;
	int running;
	MPI_WIN idx;
	EAIF_PARAM_S param;
	EaifInfo info;
	EaifAlgo algo;
	EaifStatusInternal status;
	InferenceModel model;
	int (*fill_payload_cb)(const MPI_IVA_OBJ_LIST_S *, const EAIF_PARAM_S *, EaifInfo *);
	int (*debug_cb)(const EaifInfo *);
};

void eaif_copyObjList(const MPI_IVA_OBJ_LIST_S *src, MPI_IVA_OBJ_LIST_S *dst);
void eaif_copyScaledObjList(const EaifFixedPointSize *scale_factor, const MPI_IVA_OBJ_LIST_S *src,
                            MPI_IVA_OBJ_LIST_S *dst);
int eaif_cpyInternalStatus(const EaifStatusInternal *src, EaifStatusInternal *dst);
int eaif_mergeInternalStatus(const EaifStatusInternal *src, EaifStatusInternal *dst);
int eaif_cpyRespStatus(const EaifStatusInternal *src, EAIF_STATUS_S *dst);
int eaif_cpyScaledDetectionStatus(const EaifFixedPointSize *scale_factor, const EaifStatusInternal *src,
                                  EaifStatusInternal *dst);
int eaif_cpyScaledFaceStatus(const EaifFixedPointSize *scale_factor, const EaifStatusInternal *src,
                             const MPI_IVA_OBJ_LIST_S *ol, EaifStatusInternal *dst);

void eaif_initAlgo(const EAIF_PARAM_S *param, EaifAlgo *algo);
void eaif_resetCounter(const EaifAlgo *algo, EaifStatusInternal *status);
int eaif_checkDetAppendable(const MPI_IVA_OBJ_LIST_S *ori_obj_list, const EaifAlgo *algo,
                            MPI_IVA_OBJ_LIST_S *fin_obj_list);
int eaif_checkAppendable(const MPI_IVA_OBJ_LIST_S *ori_obj_list, const EaifAlgo *algo, EaifStatusInternal *stats,
                         MPI_IVA_OBJ_LIST_S *fin_obj_list);
void eaif_updateObjAttr(const MPI_IVA_OBJ_LIST_S *inObj_list, UINT16 obj_life_th, EaifStatusInternal *status);

void *runEaif(void *input);

#endif /* EAIF_ALGO_H_ */
