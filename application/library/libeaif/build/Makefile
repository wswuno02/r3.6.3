################################################################################
# SDK related definitions
################################################################################

SDKSRC_DIR ?= $(realpath $(CURDIR)/../../../..)
include $(SDKSRC_DIR)/application/internal.mk

KCONFIG_CONFIG ?= $(APP_PATH)/.config
-include $(KCONFIG_CONFIG)
include config.mk

DEBUG ?= 0

################################################################################
# Build tools and commands
################################################################################

### First, specify the build tools.
AR := $(CROSS_COMPILE)ar
CC := $(CROSS_COMPILE)gcc
CXX := $(CROSS_COMPILE)g++
STRIP := $(CROSS_COMPILE)strip
ARFLAGS = rcsD
MKDIR := mkdir
MKDIR_P := $(MKDIR) -p
CP := cp
MV := mv
RM := rm -f
INSTALL = install
### paths

# root = $(realpath $(CURDIR)/..)
# bindir = $(SYSTEM_BIN)
# libdir = $(SYSTEM_LIB)
# LIB_OUT_PATH = $(root)/lib

root = ..
bindir = $(SYSTEM_BIN)
libdir = $(SYSTEM_LIB)
LIB_OUT_PATH = $(root)/lib
BIN_OUT_PATH = $(root)/bin
BUILD_PATH = $(root)/build
OBJ_PATH = $(root)/obj
SRC_PATH = $(root)/src
SRC_INC_PATH = $(root)/src/inc

### Then, collect the knowledge about how to build all outputs.

INC_PATHS := \
$(root)/include \
$(root)/src \
$(MPP_INC) \
$(SRC_PATH) \
$(LIBPRIO_INC) \
$(LIBINF_INC) \
$(CONFIG_INCS)

INCS = $(addprefix -iquote, $(INC_PATHS))

SRCS = \
$(wildcard $(root)/src/*.c) \
$(wildcard $(root)/src/inference/src/*.c) \
$(wildcard $(root)/src/inference/src/*.cc)

OBJS := \
$(patsubst $(SRC_PATH)/%.o, $(OBJ_PATH)/%.o, \
$(patsubst %.c,%.o,$(patsubst %.cc,%.o,$(SRCS))))

DEPS := $(patsubst %.c,%.d,$(patsubst %.cc,%.d,$(SRCS)))

### Then, define global compilation and linking flags.

OPTIMIZE = -O2
ifeq ($(DEBUG),1)
OPTIMIZE =
endif
WARNING	= -Wall -Wfatal-errors -Wno-unused-function

CFLAGS_COMMON := -g3 -MMD -MT -DSOCKLEN_T=socklen_t -D_GNU_SOURCE -D_LARGEFILE_SOURCE=1 -D_FILE_OFFSET_BITS=64 -mcpu=$(CONFIG_TARGET_CPU) -Wno-deprecated -DBSD=1
CFLAGS_COMMON += $(CONFIG_CFLAGS) $(OPTIMIZE) $(WARNING)

ifeq ($(CROSS_COMPILE), arm-linux-gnueabihf-)
CFLAGS_COMMON += -mfpu=neon-vfpv4
endif

CFLAGS    := $(CFLAGS_COMMON) -std=gnu99 $(INCS)
CXXFLAGS  := $(CFLAGS_COMMON) -std=c++11 $(INCS)

################################################################################
# Build rules
################################################################################

.PHONY: all

LIB_NAME = eaif
LIB_MAJOR = 1
LIB_MINOR = 0
LIB_PATCH = 0

LIB_VERSION = $(LIB_MAJOR).$(LIB_MINOR).$(LIB_PATCH)
LIB_ARNAME = lib$(LIB_NAME).a
LIB_REALNAME = lib$(LIB_NAME).so.$(LIB_VERSION)
LIB_SONAME = lib$(LIB_NAME).so.$(LIB_MAJOR)
LIB_LINKERNAME = lib$(LIB_NAME).so

LIB_STATIC = $(LIB_ARNAME)
LIB_STATIC_OUTPUT = $(addprefix $(LIB_OUT_PATH)/,$(LIB_STATIC))

LIB_SHARED = $(LIB_LINKERNAME) $(LIB_SONAME) $(LIB_REALNAME)
LIB_SHARED_OUTPUT = $(addprefix $(LIB_OUT_PATH)/,$(LIB_SHARED))
LIB_SHARED_TARGET = $(addprefix $(libdir)/,$(LIB_SHARED))

### Finally, define essential build targets.
$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	$(Q)$(MKDIR_P) $(dir $@)
	$(Q)$(CC) $(CFLAGS) -c $< -o $@

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.cc
	$(Q)$(MKDIR_P) $(dir $@)
	$(Q)$(CXX) $(CXXFLAGS) -c  $< -o $@

.PHONY: all
all: $(LIB_STATIC_OUTPUT)

.PHONY: test
test: $(TARGET_TESTS)

.PHONY: clean
clean: uninstall
	@printf "  CLEAN\t$(root)\n"
	$(Q)$(RM) -r $(OBJ_PATH) $(DEPS) $(LIB_STATIC_OUTPUT) $(LIB_OUT_PATH)

.PHONY: distclean
distclean: uninstall clean

.PHONY: install
install: all
	$(Q)$(MKDIR) -p $(APP_LIB)
	$(Q)$(INSTALL) -m 644 -t $(APP_LIB) $(LIB_STATIC_OUTPUT)

.PHONY: uninstall
uninstall:
	$(Q)$(RM) -r $(APP_LIB)/$(LIB_ARNAME)

.PHONY: check
check:
	@echo obj: $(OBJ_PATH) $(OBJS)
	@echo src: $(SRCS) $(SRC_PATH)
	@echo inc: $(INC_PATHS)
	@echo cflags: $(CFLAGS)
	@echo tar: $(LIB_STATIC_OUTPUT)

$(LIB_STATIC_OUTPUT) : $(OBJS)
	$(Q)$(MKDIR_P) $(LIB_OUT_PATH)
	$(Q)$(AR) $(ARFLAGS) $@ $^

################################################################################
# General rules
################################################################################

-include	$(DEPS)

