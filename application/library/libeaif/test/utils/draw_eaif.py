import cv2
import numpy as np
import sys
import os
import json
import traceback

fourcc = cv2.VideoWriter_fourcc(*'mp4v')
life_th = 0
OBJ_NO = 128
__COLOR_RAND = [np.random.randint(150, 255, size=(3)).tolist() for i in range(OBJ_NO)]

class COLORE:
	BLUE = (255,70,70)
	BLACK = (0,0,0)
	RED = (70,70,255)
	WHITE = (255,255,255)
	GREEN = (70,255,70)

def info(*args):
	print("[INFO]", *args)

class Vid(object):
	def __init__(self, video_fn):
		cap = cv2.VideoCapture(video_fn)
		if not cap.isOpened():
			help("Cannot open %s" % video_fn)
			sys.exit(0)
		self.fps = float(cap.get(cv2.CAP_PROP_FPS))
		self.w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
		self.h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
		self.cap = cap
		info("Video info %dx%d : %.4f : %s" % (self.w, self.h, self.fps, video_fn))

def LoadLog(log_fn):
	fp = open(log_fn, "r")
	if not fp:
		help("Cannot open %s" % log_fn)
		sys.exit(0)
	obj_lists = []
	lines = fp.readlines()
	fp.close()
	for l in lines:
		try:
			ol = json.loads(l)
		except:
			print(traceback.format_exc())
			print(l)
			sys.exit(0)
		obj_lists.append(ol)
	info("total %d Lines from log file : %s" % (len(obj_lists), log_fn))
	return obj_lists

def draw_frame(fr, ol, fr_id):
	COLOR = COLORE
	COLOR_RAND = __COLOR_RAND

	cv2.putText(fr, "%-4d" % fr_id, (0,40), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 0), 2, cv2.LINE_AA)

	for obj in ol["od"]:
		box = obj['obj']
		if (box['life'] < life_th):
			continue
		o_id = box['id']
		box_id = o_id % OBJ_NO # temp solution
		cat = box['cat']
		color = COLOR_RAND[box_id]
		back = (0,0,0)
		sx, sy, ex, ey = box["rect"]
		draw_cat = 0
		if cat is not None or cat != "":
			draw_cat = 1
		if draw_cat:
			if "person" in cat or "human" in cat:
				cat = "human"
				color = COLOR.BLUE
				back = COLOR.WHITE
		st = (sx, sy)
		et = (ex, ey)
		image_in = cv2.rectangle(fr, st, et, color, thickness = 3)
		if draw_cat:
			cv2.putText(image_in, cat, (sx+4, ey-15), cv2.FONT_HERSHEY_SIMPLEX, 1.2, back, 3, 16)
			cv2.putText(image_in, cat, (sx+8, ey-15), cv2.FONT_HERSHEY_SIMPLEX, 1.2, back, 3, 16)
			cv2.putText(image_in, cat, (sx+6, ey-18), cv2.FONT_HERSHEY_SIMPLEX, 1.2, back, 3, 16)
			cv2.putText(image_in, cat, (sx+6, ey-12), cv2.FONT_HERSHEY_SIMPLEX, 1.2, back, 3, 16)
			cv2.putText(image_in, cat, (sx+6, ey-15), cv2.FONT_HERSHEY_SIMPLEX, 1.2, color, 3, 16)

def run(video_fn, log_fn, out_fn):
	vid = Vid(video_fn)
	ols = LoadLog(log_fn)
	output = cv2.VideoWriter(out_fn, fourcc, vid.fps, (vid.w, vid.h))
	if not output.isOpened():
		info("Cannot open %s" % out_fn)
		return
	i = 0
	while True:
		ret, fr = vid.cap.read()
		if not ret:
			break
		draw_frame(fr, ols[i], i)
		output.write(fr)
		if i % 20 == 0: info("Drawing Process: %d/%d" % (i+1, len(ols)))
		i += 1
	info("Drawing Process: %d/%d" % (i, len(ols)))
	vid.cap.release()

def help(msg=""):
	if msg != "":
		print("\t", msg)
	print("\n\t Usage :")
	print("\t\tpython draw_eaif.py [input_video] [generated_eaif_log] [output_mp4_name]\n")

def main():
	args = sys.argv
	if len(args) != 4:
		help()
		return
	video = args[1]
	log = args[2]
	out = args[3]
	assert os.path.exists(video), "Cannot Find %s " % video 
	assert os.path.exists(log), "Cannot Find %s " % log
	run(video, log, out)

if __name__ == '__main__':
	main()