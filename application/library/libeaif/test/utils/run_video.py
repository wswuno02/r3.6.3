import os
import sys
import subprocess as sp
import glob
import re

EXTRACT_SEI = "utils/extract_sei.sh %s %s"
PARSE_SEI = "python utils/parse_obj_list.py %s "
UPDATE_CONF = "eaif_param_file=config/eaif.param\n" + \
              "eaif_objList_file=%s\n" + \
              "video_input_file=%s\n" + \
              "video_fps=%.4f\n" + \
              "resoln=%dx%d\n" + \
              "num_frames=%d\n"

CONFIG_FILE = "config/eaif_test.ini"
RUN_EAIF_TEST = "./eaif_demo.elf config/eaif_test.ini 1"
DRAW_VIDEO = "python utils/draw_eaif.py %s %s %s"

PREFIX = "output"

def info(*args):
    print("[INFO]", *args)

def get_video_info(msg_stdout, msg_stderr):
    num_frames = 0
    img_h = 0
    img_w = 0
    fps = 0.0
    msg = msg_stderr.decode().split("\n")
    i = 0
    for l in msg:
        i+=1
        if img_w == 0:
            resolutions = re.findall('[0-9]+x[0-9]+', l)
            if resolutions:
                for resolution in resolutions:
                    resoln = resolution.split('x')
                    if "0" in resoln:
                        continue
                    img_h = int(resoln[1])
                    img_w = int(resoln[0])
        if num_frames == 0:
            no_frames = re.findall('frame=[\ ]*[0-9]+', l)
            if (no_frames):
                num_frames = int(no_frames[0].split("=")[1])
        if fps == 0.0:
            fpsStr = re.findall('\ (\d+(?:\.\d+)?)\ fps,', l)
            if (fpsStr):
                fps = float(fpsStr[0])
    if img_h == 0 or num_frames == 0 or fps == 0:
        info("Cannot parse video information!")
        info("See %s" % (msg_stderr.decode()))
        return
    return img_h, img_w, num_frames, fps

def run_one_video(path):
    if not os.path.exists(path):
        info("Cannot find %s" % path)
        return
    filename = os.path.basename(path)
    txtname = PREFIX + "/" + filename[:-4] + ".txt"
    binname = PREFIX + "/" + filename[:-4] + ".bin"
    logname = PREFIX + "/" + filename[:-4] + ".log"
    drawname = PREFIX + "/" + filename[:-4] + "_draw.mp4"

    CMD = EXTRACT_SEI % (path, txtname)
    info(CMD)
    ret, err = cmd(CMD)
    img_h, img_w, num_frames, fps = get_video_info(ret, err)
    info("Video info %dx%d fps:%.4f frames:%d" % (img_w, img_h, fps, num_frames))

    CMD = PARSE_SEI % txtname
    info(CMD)
    ret, err = cmd(CMD)
    print(ret.decode(), err.decode())

    CONFIG = UPDATE_CONF % (
        binname,
        path,
        fps,
        img_w, img_h,
        num_frames)
    with open(CONFIG_FILE,"w") as fw:
        fw.write(CONFIG)

    CMD = RUN_EAIF_TEST
    info(CMD)
    ret, err = cmd(CMD)
    print(ret.decode(), err.decode())

    CMD = DRAW_VIDEO % (path, logname, drawname)
    info(CMD)
    ret = cmd(CMD)
    info("Complete %s" % path)

def run_all_video(path):
    paths = glob.glob(path)
    for i in range(len(paths)):
        p = paths[i]
        info("Running %d/%d %s" % (i, len(paths), p))
        run_one_video(p)

def cmd(*args):
    p = sp.Popen(*args,shell=True, stdout=sp.PIPE, stderr=sp.PIPE)
    ret, err = p.communicate()
    return ret, err

def main(target_path):
    if not os.path.exists(PREFIX):
        os.mkdir(PREFIX)
    run_all_video(target_path)

if __name__ == "__main__":
    path = sys.argv[1]
    main(path)

