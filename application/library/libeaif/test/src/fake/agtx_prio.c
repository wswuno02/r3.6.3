/*
 * AUGENTIX INC. - PROPRIETARY
 *
 * agtx_prio.c - Priority getter/setter
 * Copyright (C) 2019 im14, Augentix Inc. <MAIL@augentix.com>
 *
 * NOTICE: The information contained herein is the property of Augentix Inc.
 * Copying and distributing of this file, via any medium,
 * must be licensed by Augentix Inc.
 *
 */

#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/resource.h>
#include <sys/syscall.h>
#define gettid() syscall(__NR_gettid)

#include "agtx_prio.h"

#define PRIO_FILE "/etc/prio.conf"
#define COLUMN 3 /* Column count of the prio.conf */

#define BUFFER_LEN 128

#define SCHED_PLCY_NUM 3

int getThreadSchedAttr(const char *th_name, int *policy, int *np)
{
	return 1;
}

int setThreadSchedAttr(const char *th_name)
{
	return 0;
}

void printThreadSchedAttr(void)
{
}
