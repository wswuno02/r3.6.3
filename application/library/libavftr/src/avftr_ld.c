/**
* @cond
*
* code fragment skipped by Doxygen.
*/

#include <stdio.h>
#include <pthread.h>

#include "mtk_common.h"
#include "mpi_dev.h"

#include "avftr_ld.h"
#include "avftr.h"
#include "avftr_common.h"

typedef struct {
	VFTR_LD_PARAM_S param;
	VFTR_LD_INSTANCE_S *instance;
	unsigned int is_write;
	unsigned int is_reset;
	unsigned int is_setRoi;

	UINT8 mcvp_avg_y_cfg_idx;

	pthread_mutex_t lock;
	pthread_mutex_t cb_lock;
} LD_CTX_S;

static LD_CTX_S g_ld_ctx[AVFTR_LD_MAX_SUPPORT_NUM] = { { { 0 } } };

#define LD_GET_CTX(idx) &g_ld_ctx[idx]

extern AVFTR_VIDEO_CTX_S *vftr_res_shm;

// LD_STATUS_S *vftr_ld_res[AVFTR_LD_MAX_SUPPORT_NUM];

static int findLdCtx(MPI_WIN idx, AVFTR_LD_CTX_S *ctx, int *empty)
{
	int i = 0;
	int find_idx = -1;
	int emp_idx = -1;

	if (empty == NULL) {
		emp_idx = -2;
	} else {
		emp_idx = -1;
	}

	for (i = 0; i < AVFTR_LD_MAX_SUPPORT_NUM; i++) {
		if (find_idx == -1 && ctx[i].idx.value == idx.value && ctx[i].reg) {
			find_idx = i;
		} else if (emp_idx == -1 && !ctx[i].reg) {
			emp_idx = i;
		}
	}

	if (empty != NULL) {
		*empty = emp_idx;
	}

	return find_idx;
}

/**
 * @brief Invoke callback function when alarm condition is satisfied.
 * @param[in]  idx        video window index.
 * @param[in] res         light detection result.
 * @see VIDEO_FTR_getLdRes()
 * @retval none.
 */
static void genLdAlarm(AVFTR_LD_CTX_S *vftr_ld_ctx, const VFTR_LD_COND_E trig_cond)
{
	if (vftr_ld_ctx->cb == NULL) {
		return;
	}

	if (trig_cond != VFTR_LD_LIGHT_NONE) {
		vftr_ld_ctx->cb();
		return;
	}
}

static void getLdResOffset(MPI_WIN idx, AVFTR_RECT_POINT_S *roi)
{
	uint32_t x = 0;
	uint32_t y = 0;
	uint8_t i;
	MPI_CHN_LAYOUT_S layout_attr;
	MPI_CHN chn = MPI_VIDEO_CHN(idx.dev, idx.chn);
	if (MPI_DEV_getChnLayout(chn, &layout_attr) < 0) {
		SYS_TRACE("Cannot get channel layout for chn:%d\n", chn.chn);
		return;
	}
	for (i = 0; i < layout_attr.window_num; i++) {
		if (idx.value == layout_attr.win_id[i].value) {
			break;
		}
	}
	if (i == layout_attr.window_num) {
		SYS_TRACE("Window %d does not exist in channel %d\n", idx.win, idx.chn);
		return;
	}

	x = layout_attr.window[i].x;
	y = layout_attr.window[i].y;
	roi->sx += x;
	roi->sy += y;
	roi->ex += x;
	roi->ey += y;
}

/**
 * @brief Get predefined metadata format for Multiplayer.
 * @param[in]  src_idx         mpi win index of source window
 * @param[in]  dst_idx         mpi win index of destination window
 * @param[in]  src_rect        source window
 * @param[in]  dst_rect        destination window
 * @param[in] res         light detection result.
 * @param[in] str         metadata string buffer.
 * @see VIDEO_FTR_getLdRes()
 * @retval length of metadata.
 */
static int getLdMeta(MPI_WIN src_idx, MPI_WIN dst_idx, const MPI_RECT_S *src_rect, const MPI_RECT_S *dst_rect,
                     const MPI_RECT_S *src_roi, const MPI_RECT_S *dst_roi, const AVFTR_LD_STATUS_S *res, char *str)
{
	int offset = 0;

	AVFTR_RECT_POINT_S roi = res->roi;

	if (src_idx.value != dst_idx.value) {
		rescaleMpiRectPoint(src_rect, dst_rect, src_roi, dst_roi, &roi);
	}

	offset += sprintf(&str[offset],
#ifdef IVA_FORMAT_XML
	                  "<LD><ROI RECT=\"%d %d %d %d\" ALARM=\"%d\"/></LD>"
#else /* IVA_FORMAT_JSON */
	                  "\"ld\":{\"roi\":{\"rect\":[%d,%d,%d,%d],\"alarm\":%d}},"
#endif /*! !IVA_FORMAT_XML */
	                  ,
	                  roi.sx, roi.sy, roi.ex, roi.ey, res->status.trig_cond);
	return offset;
}

/**
 * @brief Empty callback function for initialization.
 * @param[in] none.
 * @see VIDEO_FTR_enableLd()
 * @retval none.
 */
static void alarmEmptyCb()
{
	SYS_TRACE("Please registrate light detection alarm callback function.\n");
	return;
}

/**
 * @brief Update light detection status.
 * @param[in]  idx        video window index.
 * @param[in] param       light detection parameters.
 * @param[in] status      light detection result.
 * @see VIDEO_FTR_enableLd()
 * @retval none
 */
// static VOID updateLdStatus(MPI_WIN idx, const VFTR_LD_PARAM_S *param, const VFTR_LD_STATUS_S *status)
// {
// 	AVFTR_LD_CTX_S *vftr_ld_ctx = vftr_res_shm->ld_ctx;
// 	int enable_idx = findLdCtx(idx, vftr_ld_ctx, NULL);
// 	if (enable_idx < 0) {
// 		SYS_TRACE("LD is not enabled in win %d\n", idx.win);
// 		return;
// 	}
// 	pthread_mutex_lock(&ld_res_lock);
// 	memcpy(&vftr_ld_res[enable_idx]->trig_cond, &status->trig_cond, sizeof(LD_COND_E));
// 	pthread_mutex_unlock(&ld_res_lock);
// }

/**
 * @endcond
 */

/**
 * @brief Get enable status of light detection.
 * @param[in]  idx        video window index.
 * @see none
 * @retval enable status of light detection.
 */
int AVFTR_LD_getStat(MPI_WIN idx, AVFTR_LD_CTX_S *vftr_ld_ctx)
{
	int enable_idx = findLdCtx(idx, vftr_ld_ctx, NULL);

	return enable_idx < 0 ? 0 : vftr_ld_ctx[enable_idx].en;
}

/**
 * @brief Get results of light detection.
 * @param[in]  idx        video window index.
 * @param[out] str         metadata string buffer.
 * @see none
 * @retval length of metadata.
 */
int AVFTR_LD_getRes(MPI_WIN idx, int buf_idx)
{
	AVFTR_LD_CTX_S *vftr_ld_ctx = vftr_res_shm->ld_ctx;

	int enable_idx = findLdCtx(idx, vftr_ld_ctx, NULL);
	int ret = 0;

	if (enable_idx < 0) {
		/* idx is not registered yet */
		//SYS_TRACE("idx:%d is not registered yet!\n",idx.value);
		return ENODEV;
	}

	if (!vftr_ld_ctx[enable_idx].en) {
		/* idx is not enabled yet */
		//SYS_TRACE("idx:%d is not enabled yet!\n",idx.value);
		return EAGAIN;
	}

	LD_CTX_S *ctx = LD_GET_CTX(enable_idx);
	if (ctx->instance == NULL) {
		SYS_TRACE("LD object is NULL!\n");
		return EFAULT;
	}

	VFTR_LD_STATUS_S *ld_status = &vftr_ld_ctx[enable_idx].ld_res[buf_idx].status;

	VFTR_LD_PARAM_S param;
	ret = AVFTR_LD_getParam(idx, &param);
	if (ret != 0) {
		SYS_TRACE("Get light detection ROI on win %d failed.\n", idx.win);
		return ret;
	}

	pthread_mutex_lock(&ctx->lock);
	ret = VFTR_LD_detect(ctx->instance, ctx->mcvp_avg_y_cfg_idx, ld_status);
	pthread_mutex_unlock(&ctx->lock);

	if (ret != 0) {
		SYS_TRACE("Failed to run tamper detection!\n");
		return ret;
	}

	pthread_mutex_lock(&ctx->cb_lock);
	genLdAlarm(&vftr_ld_ctx[enable_idx], ld_status->trig_cond);
	pthread_mutex_unlock(&ctx->cb_lock);

	getLdResOffset(idx, &param.roi);

	return 0;
}

/**
 * @brief Get predefined metadata format for Multiplayer.
 * @param[in] src_idx  mpi win index of source window
 * @param[in] dst_idx  mpi win index of destination window
 * @param[in] src_rect source window
 * @param[in] dst_rect destination window
 * @param[in] res         light detection result.
 * @param[in] str         metadata string buffer.
 * @see VIDEO_FTR_getLdRes()
 * @retval length of metadata.
 */
int AVFTR_LD_transRes(AVFTR_LD_CTX_S *vftr_ld_ctx, MPI_WIN src_idx, MPI_WIN dst_idx, const MPI_RECT_S *src_rect,
                      const MPI_RECT_S *dst_rect, const MPI_RECT_S *src_roi, const MPI_RECT_S *dst_roi, char *str,
                      int buf_idx)
{
	int enable_idx = findLdCtx(src_idx, vftr_ld_ctx, NULL);
	if (enable_idx < 0)
		return 0;
	if (vftr_ld_ctx[enable_idx].en)
		return getLdMeta(src_idx, dst_idx, src_rect, dst_rect, src_roi, dst_roi,
		                 &vftr_ld_ctx[enable_idx].ld_res[buf_idx], str);
	return 0;
}

/**
 * @brief Add LD instance.
 * @param[in]  idx         video window index.
 * @see AVFTR_LD_deleteInstance
 * @retval 0                 success.
 * @retval ENOMEM            No more space to register idx / malloc LD instance failed
 * @retval EFAULT            LD instance is NULL
 */
int AVFTR_LD_addInstance(MPI_WIN idx)
{
	AVFTR_LD_CTX_S *vftr_ld_ctx = vftr_res_shm->ld_ctx;
	int empty_idx;
	int set_idx = findLdCtx(idx, vftr_ld_ctx, &empty_idx);

	if (set_idx >= 0) {
		/* idx is registered */
		SYS_TRACE("idx:%d is registerd\n", idx.value);
		return 0;
	} else if (set_idx < 0 && empty_idx >= 0) {
		/* idx is not registered yet but there is empty space to be registerd*/
		LD_CTX_S *ctx = LD_GET_CTX(empty_idx);
		ctx->instance = VFTR_LD_newInstance();
		if (!ctx->instance) {
			SYS_TRACE("Failed to create LD instance \n");
			return ENOMEM;
		}
		ctx->lock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
		ctx->cb_lock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;

		vftr_ld_ctx[empty_idx].idx = idx;
		vftr_ld_ctx[empty_idx].reg = 1;
		vftr_ld_ctx[empty_idx].en = 0;

		pthread_mutex_lock(&ctx->cb_lock);
		vftr_ld_ctx[empty_idx].cb = NULL;
		pthread_mutex_unlock(&ctx->cb_lock);

	} else {
		/* No more space to register idx */
		SYS_TRACE("add LD instance failed on win %u.\n", idx.win);
		return ENOMEM;
	}

	return 0;
}

/**
 * @brief Delete LD instance.
 * @param[in]  idx         video window index.
 * @see AVFTR_LD_addInstance
 * @retval 0                 success.
 * @retval EAGAIN            idx is enabled, not to remove
 */
int AVFTR_LD_deleteInstance(MPI_WIN idx)
{
	AVFTR_LD_CTX_S *vftr_ld_ctx = vftr_res_shm->ld_ctx;
	int enable_idx = findLdCtx(idx, vftr_ld_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered */
		//SYS_TRACE("idx:%d is not registered yet!\n", idx.value);
		return 0;
	}
	if (vftr_ld_ctx[enable_idx].en) {
		/* idx is enabled */
		SYS_TRACE("idx:%d is still enable, can not be deleted!\n", idx.value);
		return EAGAIN;
	}

	INT32 ret = 0;
	LD_CTX_S *ctx = LD_GET_CTX(enable_idx);

	pthread_mutex_lock(&ctx->lock);
	ret = VFTR_LD_deleteInstance(&ctx->instance);
	pthread_mutex_unlock(&ctx->lock);

	if (ret != 0) {
		SYS_TRACE("Free td instance failed!\n");
		return ret;
	}
	vftr_ld_ctx[enable_idx].reg = 0;
	vftr_ld_ctx[enable_idx].en = 0;

	pthread_mutex_lock(&ctx->cb_lock);
	vftr_ld_ctx[enable_idx].cb = NULL;
	pthread_mutex_unlock(&ctx->cb_lock);
	return 0;
}

/**
 * @brief Enable light detection.
 * @param[in]  idx         video window index.
 * @see AVFTR_LD_disable
 * @retval 0                 success.
 * @retval ENODEV            idx is not registered yet
 */
int AVFTR_LD_enable(MPI_WIN idx)
{
	AVFTR_LD_CTX_S *vftr_ld_ctx = vftr_res_shm->ld_ctx;
	int enable_idx = findLdCtx(idx, vftr_ld_ctx, NULL);

	if (enable_idx < 0) {
		/* idx is not registered */
		SYS_TRACE("idx:%d is not registered yet!\n", idx.value);
		return ENODEV;
	}

	if (vftr_ld_ctx[enable_idx].en) {
		/* idx is enabled */
		SYS_TRACE("idx:%d is enabled, no need to enable again!\n", idx.value);
		return 0;
	}

	LD_CTX_S *ctx = LD_GET_CTX(enable_idx);

	INT32 ret = 0;
	ret = vftrYAvgResDec();
	if (ret != 0) {
		SYS_TRACE("[WARN] All MPI YAVG ROI Resource are being used !\n");
		return ret;
	}

	pthread_mutex_lock(&ctx->cb_lock);
	if (vftr_ld_ctx[enable_idx].cb == NULL) {
		//SYS_TRACE("Door Keerper alarm callback function is not registered on win %u.\n", idx.win);
		vftr_ld_ctx[enable_idx].cb = alarmEmptyCb;
	}
	pthread_mutex_unlock(&ctx->cb_lock);

	vftr_ld_ctx[enable_idx].en = 1;

	return 0;
}

/**
 * @brief Disable tamper detection.
 * @param[in]  idx        video window index.
 * @see AVFTR_LD_enable
 * @retval 0                 success.
 * @retval ENODEV            idx is not registered yet
 */
int AVFTR_LD_disable(MPI_WIN idx)
{
	AVFTR_LD_CTX_S *vftr_ld_ctx = vftr_res_shm->ld_ctx;
	int enable_idx = findLdCtx(idx, vftr_ld_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registerd yet, no need to disable\n", idx.value);
		return ENODEV;
	}

	if (!vftr_ld_ctx[enable_idx].en) {
		/* idx is not enabled */
		SYS_TRACE("idx:%d is not enabled yet, no need to disable\n", idx.value);
		return 0;
	}

	vftrYAvgResInc();
	vftr_ld_ctx[enable_idx].en = 0;

	return 0;
}

/**
 * @brief Get parameters of tamper detection.
 * @param[in]  idx        video window index.
 * @param[out] param      tamper detection parameters.
 * @see AVFTR_LD_setParam
 * @retval 0              success.
 * @retval ENODEV         idx is not registered yet
 */
int AVFTR_LD_getParam(MPI_WIN idx, VFTR_LD_PARAM_S *param)
{
	AVFTR_LD_CTX_S *vftr_ld_ctx = vftr_res_shm->ld_ctx;
	int enable_idx = findLdCtx(idx, vftr_ld_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registered\n", idx.value);
		return ENODEV;
	}

	LD_CTX_S *ctx = LD_GET_CTX(enable_idx);
	int ret;

	// Use this to check win state
	MPI_WIN_ATTR_S win_attr;
	ret = MPI_DEV_getWindowAttr(idx, &win_attr);
	if (ret != 0) {
		SYS_TRACE("Get window %u attributes failed.\n", idx.value);
		return ret;
	}
	*param = ctx->param;

	return 0;
}

/**
 * @brief Set parameters of tamper detection.
 * @param[in]  idx        video window index.
 * @param[out] param      tamper detection parameters.
 * @param[in]  param      tamper detection parameters.
 * @see AVFTR_LD_getParam
 * @retval 0              success.
 * @retval ENODEV         idx is not registered yet
 */
int AVFTR_LD_setParam(MPI_WIN idx, const VFTR_LD_PARAM_S *param)
{
	AVFTR_LD_CTX_S *vftr_ld_ctx = vftr_res_shm->ld_ctx;
	int enable_idx = findLdCtx(idx, vftr_ld_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registered\n", idx.value);
		return ENODEV;
	}

	LD_CTX_S *ctx = LD_GET_CTX(enable_idx);

	int ret;

	MPI_SIZE_S res = {};
	ret = getMpiSize(idx, &res);
	if (ret != 0) {
		return ret;
	}

	ret = VFTR_LD_checkParam(&res, param);
	if (ret != 0) {
		return ret;
	}

	// Copy param to temp buffer and prepare to set to vftr_dk
	pthread_mutex_lock(&ctx->lock);
	ctx->param = *param;
	ctx->is_write = 1;
	//SYS_TRACE("[AVFTR_LD] set LD param success\n");
	pthread_mutex_unlock(&ctx->lock);

	return 0;
}

/**
 * @brief Write parameters to LD Object 
 * @param[in]  idx        video window index.
 * @see AVFTR_LD_getParam
 * @retval 0              success.
 * @retval ENODEV         idx is not registered yet
 */
int AVFTR_LD_writeParam(MPI_WIN idx)
{
	AVFTR_LD_CTX_S *vftr_ld_ctx = vftr_res_shm->ld_ctx;
	int enable_idx = findLdCtx(idx, vftr_ld_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		//SYS_TRACE("idx:%d is not registered\n", idx.value);
		return ENODEV;
	}

	LD_CTX_S *ctx = LD_GET_CTX(enable_idx);
	VFTR_LD_PARAM_S *param = &ctx->param;

	int ret;
	MPI_SIZE_S res = {};
	ret = getMpiSize(idx, &res);
	if (ret != 0) {
		return ret;
	}

	// Use this to check win state
	MPI_WIN_ATTR_S win_attr;
	ret = MPI_DEV_getWindowAttr(idx, &win_attr);
	if (ret != 0) {
		SYS_TRACE("Get window %u attributes failed.\n", idx.value);
		return ret;
	}

	int is_write = ctx->is_write;
	if (is_write == 1) {
		pthread_mutex_lock(&ctx->lock);
		ret = VFTR_LD_setParam(ctx->instance, &res, param);
		ctx->is_write = 0;
		//SYS_TRACE("[reader] write LD param success, sensitivity is %d\n", param->ld_param.sensitivity);
		pthread_mutex_unlock(&ctx->lock);
		if (ret != 0) {
			return ret;
		}
	}

	return 0;
}

/**
 * @brief Register alarm callback function of tamper detection.
 * @param[in]  idx             video window index.
 * @param[in]  alarm_cb_fptr   function pointer of callback function.
 * @see none
 * @retval 0          success.
 * @retval EFAULT     NULL pointer of cb function 
 * @retval ENODEV     idx is not registered yet
 */
int AVFTR_LD_regCallback(MPI_WIN idx, const AVFTR_LD_ALARM_CB alarm_cb_fptr)
{
	if (alarm_cb_fptr == NULL) {
		SYS_TRACE("Pointer to tamper detection alarm callback function should not be NULL.\n");
		return EFAULT;
	}

	AVFTR_LD_CTX_S *vftr_ld_ctx = vftr_res_shm->ld_ctx;
	int enable_idx = findLdCtx(idx, vftr_ld_ctx, NULL);

	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registered\n", idx.value);
		return ENODEV;
	}

	LD_CTX_S *ctx = LD_GET_CTX(enable_idx);

	pthread_mutex_lock(&ctx->cb_lock);
	vftr_ld_ctx[enable_idx].cb = alarm_cb_fptr;
	pthread_mutex_unlock(&ctx->cb_lock);

	return 0;
}

int AVFTR_LD_resetShm(MPI_WIN idx)
{
	/*  FIXME: by win used
	int enable_idx = findLdCtx(idx, vftr_ld_ctx, NULL);

	if (enable_idx < 0) {
		return -1;
	}

	if (vftr_ld_ctx[enable_idx].en) {
		vftr_ld_ctx[enable_idx].ld_res.alarm = 0;
	}
	*/
	AVFTR_LD_CTX_S *vftr_ld_ctx = vftr_res_shm->ld_ctx;
	AVFTR_VIDEO_BUF_INFO_S *buf_info = vftr_res_shm->buf_info;
	int i;

	for (i = 0; i < AVFTR_LD_MAX_SUPPORT_NUM; i++) {
		if (vftr_ld_ctx[i].en) {
			vftr_ld_ctx[i].ld_res[buf_info[i].buf_cur_idx].status.trig_cond = VFTR_LD_LIGHT_NONE;
		}
	}

	return 0;
}

int AVFTR_LD_resume(MPI_WIN idx)
{
#if (0) /* Do nothing */
	int i;
	for (i = 0; i < AVFTR_LD_MAX_SUPPORT_NUM; i++) {
		if (vftr_ld_ctx[i].en) {
			// do nothing now
		}
	}
#endif
	return 0;
}

int AVFTR_LD_regMpiInfo(MPI_WIN idx, const MPI_RECT_POINT_S *roi)
{
	AVFTR_LD_CTX_S *vftr_ld_ctx = vftr_res_shm->ld_ctx;
	int enable_idx = findLdCtx(idx, vftr_ld_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		//SYS_TRACE("idx:%d is not registerd yet, can not register mpi info\n", idx.value);
		return ENODEV;
	}

	LD_CTX_S *ctx = LD_GET_CTX(enable_idx);

	int ret;

	if (ctx->is_setRoi == 1) {
		pthread_mutex_lock(&ctx->lock);
		ret = MPI_DEV_rmIspYAvgCfg(idx, ctx->mcvp_avg_y_cfg_idx);
		pthread_mutex_unlock(&ctx->lock);
		if (ret != 0) {
			//SYS_TRACE("Release y avg configuration %d on win %u failed\n", ctx->y_avg_cfg_idx, idx.value);
			return ENODEV;
		}
		ctx->is_setRoi = 0;
	}

	MPI_ISP_Y_AVG_CFG_S y_avg_cfg = { .roi = *roi, .diff_thr = 0 };

	pthread_mutex_lock(&ctx->lock);
	ret = MPI_DEV_addIspYAvgCfg(idx, &y_avg_cfg, &ctx->mcvp_avg_y_cfg_idx);
	pthread_mutex_unlock(&ctx->lock);

	if (ret != 0) {
		//SYS_TRACE("Add luma avg configuration failed on win %u\n", idx.value);
		return ret;
	}

	ctx->is_setRoi = 1;
	return 0;
}

int AVFTR_LD_releaseMpiInfo(MPI_WIN idx)
{
	AVFTR_LD_CTX_S *vftr_ld_ctx = vftr_res_shm->ld_ctx;
	int enable_idx = findLdCtx(idx, vftr_ld_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered */
		//SYS_TRACE("idx:%d is not registered yet!\n", idx.value);
		return 0;
	}
	if (vftr_ld_ctx[enable_idx].en) {
		/* idx is enabled */
		//SYS_TRACE("idx:%d is still enable, ROI can not be deleted!\n", idx.value);
		return EAGAIN;
	}

	LD_CTX_S *ctx = LD_GET_CTX(enable_idx);

	int ret;

	if (ctx->is_setRoi == 0) {
		//SYS_TRACE("ROI config is not set yet\n");
		return 0;
	}

	pthread_mutex_lock(&ctx->lock);
	ret = MPI_DEV_rmIspYAvgCfg(idx, ctx->mcvp_avg_y_cfg_idx);
	pthread_mutex_unlock(&ctx->lock);

	if (ret != 0) {
		//SYS_TRACE("Release y avg configuration %d on win %u failed\n", ctx->y_avg_cfg_idx, idx.value);
		return ENODEV;
	}

	ctx->is_setRoi = 0;
	return 0;
}

int AVFTR_LD_updateMpiInfo(MPI_WIN idx)
{
	// AVFTR_LD_CTX_S *vftr_ld_ctx = vftr_res_shm->ld_ctx;
	// int enable_idx = findBmCtx(idx, vftr_ld_ctx, NULL);
	// if (enable_idx < 0) {
	// 	/* idx is not registered yet */
	// 	SYS_TRACE("idx:%d is not registerd yet, can not get mpi info\n", idx.value);
	// 	return ENODEV;
	// }

	// if (!vftr_ld_ctx[enable_idx].en) {
	// 	/* idx is not enabled */
	// 	// SYS_TRACE("idx:%d is not enabled yet, no need to get mpi info\n", idx.value);
	// 	return 0;
	// }

	// LD_CTX_S *ctx = LD_GET_CTX(enable_idx);
	// INT32 ret = 0;

	// ret = MPI_DEV_getIspYAvg(idx, ctx->y_avg_cfg_idx, &ctx->mpi_input.y_shp_avg);
	// if (ret != 0) {
	// 	SYS_TRACE("Get luma avg on win %u failed\n", idx.value);
	// 	return ret;
	// }

	return 0;
}
