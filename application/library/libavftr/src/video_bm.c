#include <assert.h>
#include <stdio.h>
#include <stdint.h>

#include "fd.h"
#include "mtk_common.h"

#include "avftr_common.h"
#include "video_bm.h"
#include "avftr.h"

//#define VFTR_BM_DEBUG_API

#ifdef VFTR_BM_DEBUG_API
#define VFTR_BM_API_INFO(fmt, args...) printf("[BM] " fmt, ##args)
#else
#define VFTR_BM_API_INFO(fmt, args...) 
#endif

typedef struct {
	VIDEO_FTR_BM_STATUS_S bm_res;
	VIDEO_FTR_BM_PARAM_S param;
	pthread_mutex_t lock;
} BM_CTX_S;

static BM_CTX_S g_bm_ctx[VIDEO_BM_MAX_SUPPORT_NUM] = { { { 0 } } };
extern AVFTR_VIDEO_CTX_S *vftr_res_shm;

#define BM_GET_CTX(idx) &g_bm_ctx[idx]

static void emptyBmCallback(MPI_WIN idx, VIDEO_FTR_BM_EVENT_E evt,	const VIDEO_FTR_BM_PARAM_S *param)
{
    return;
}

static int findBmCtx(MPI_WIN idx, VIDEO_BM_CTX_S* ctx, int *empty)
{
	int i = 0;
	int find_idx = -1;
	int emp_idx = -1;

	if (empty == NULL) {
		emp_idx = -2;
	} else {
		emp_idx = -1;
	}

	for (i = 0; i < VIDEO_BM_MAX_SUPPORT_NUM; i++) {
		if (find_idx == -1 && ctx[i].idx.value == idx.value) {
			find_idx = i;
		} else if (emp_idx == -1 && !ctx[i].en) {
			emp_idx = i;
		}
	}

	if (empty != NULL) {
		*empty = emp_idx;
	}

	return find_idx;
}

/**
 * @brief Invoke callback function when baby monitor event is satisfied.
 * @param[in] bm_ctx        baby monitor context.
 * @param[in] bm_evt  baby monitor event
 * @param[in] param  baby monitor param
 * @see VIDEO_FTR_getTdRes()
 * @retval none.
 */
static void genBmAlarm(const VIDEO_BM_CTX_S *bm_ctx, int bm_evt, const VIDEO_FTR_BM_PARAM_S *param)
{
	bm_ctx->cb(bm_ctx->idx, bm_evt, param);
	return;
}

static void determineBmRes(const FD_STATUS_S *fd_res, const VIDEO_FTR_BM_PARAM_S *param, VIDEO_FTR_BM_STATUS_S *bm_res, VIDEO_FTR_BM_STATUS_S *bm_res_shm)
{

#define BM_ACTIVE_TH (8)
#define BM_AWAKE_TH (4)

	bm_res->fd_stat = *fd_res;
	bm_res->roi = param->fd_param.roi;
	switch (fd_res->current_evt) {
		case FD_OBJECT_ABSENT:
			bm_res->current_evt = VIDEO_BM_EVENT_ABSENT;
			break;
		case FD_OBJECT_BOUNDARY:
			bm_res->current_evt = VIDEO_BM_EVENT_BOUNDARY;
			bm_res->duration_active++;
			break;
		case FD_OBJECT_ENTERING:
			bm_res->current_evt = VIDEO_BM_EVENT_ENTERING;
			break;
		case FD_OBJECT_LEAVING:
			bm_res->current_evt = VIDEO_BM_EVENT_LEAVING;
			break;
		case FD_OBJECT_PRESENT:
			if (fd_res->motion_level >= BM_ACTIVE_TH) {
				bm_res->current_evt = VIDEO_BM_EVENT_ACTIVE;
				bm_res->duration_active++;
			} else if (fd_res->motion_level >= BM_AWAKE_TH) {
				bm_res->current_evt = VIDEO_BM_EVENT_AWAKE;
				bm_res->duration_awake++;
			} else {
				bm_res->current_evt = VIDEO_BM_EVENT_SLEEP;
				bm_res->duration_sleep++;
			}
			break;
		default:
			assert(0);
	}

	*bm_res_shm = *bm_res;

#if	0
	const static char *bm_evt_str[] = {
		"ABSENT", "ACTIVE", "AWAKE", "SLEEP", "DANGEROUS", "ENTERING", "LEAVING"
	};
	printf("%s BM res: evt:%s fg:%d, roi:[%d %d %d %d], motion level:%d bnd:%d filter_val:%d(inc:%d) stable:%d(%d/%d)\n",
	__func__, bm_evt_str[bm_res->current_evt], bm_res->fd_stat.fg_object, bm_res->roi.x, bm_res->roi.y,
	bm_res->roi.width, bm_res->roi.height, bm_res->fd_stat.motion_level,
	bm_res->fd_stat.boundary_event, filter->val, filter->val_inc, filter->stable_out, filter->stable_cnt, filter->stable_max);
#endif
	return;
}

static int getBmMeta(MPI_WIN src_idx, MPI_WIN dst_idx, const MPI_RECT_S *src_rect, const MPI_RECT_S *dst_rect,
                    const MPI_RECT_S *src_roi, const MPI_RECT_S *dst_roi,
                    const VIDEO_FTR_BM_STATUS_S *bm_res, char *str)
{
	int offset = 0;
	MPI_RECT_POINT_S copy_roi = {.sx = bm_res->roi.x, .sy = bm_res->roi.y,
	                            .ex = bm_res->roi.x + bm_res->roi.width - 1,
	                            .ey = bm_res->roi.y + bm_res->roi.height - 1};

	if (src_idx.value != dst_idx.value) {
		rescaleMpiRectPoint(src_rect, dst_rect, src_roi, dst_roi, &copy_roi);
	}

#ifdef IVA_FORMAT_XML /* IVA_FORMAT_XML */
	offset += sprintf(&str[offset], "<BM>");
#else /* IVA_FORMAT_JSON */
	offset += sprintf(&str[offset], "\"bm\":{");
#endif /* !IVA_FORMAT_XML */
	offset += sprintf(&str[offset],
#ifdef IVA_FORMAT_XML /* IVA_FORMAT_XML */
		                  "<ROI RECT=\"%d %d %d %d\" EVT=\"%d\"/>"
                          "<REPORT ACTIVE=\"%d\" AWAKE=\"%d\" SLEEP=\"%d\"/>"
#ifdef BM_SHOW_FD_REPORT
                          "<FD MOT_LVL=\"%d\" FG=\"%d\" BND_EVT=\"%d\"/>"
#endif /* BM_SHOW_FD_REPORT */
#else /* IVA_FORMAT_JSON */
		                  "\"roi\":{\"rect\":[%d,%d,%d,%d],\"evt\":%d},"
                          "\"report\":{\"active\":%d,\"awake\":%d,\"sleep\":%d}"
#ifdef BM_SHOW_FD_REPORT
                          ",\"fd\":{\"mot_lvl\":%d,\"fg\":%d,\"bnd_evt\":%d}"
#endif /* BM_SHOW_FD_REPORT */
#endif /* !IVA_FORMAT_XML */
		                 ,copy_roi.sx, copy_roi.sy, copy_roi.ex, copy_roi.ey, bm_res->current_evt,
		                 bm_res->duration_active, bm_res->duration_awake, bm_res->duration_sleep
#ifdef BM_SHOW_FD_REPORT
                         ,bm_res->fd_stat.motion_level, bm_res->fd_stat.fg_object, bm_res->fd_stat.boundary_event
#endif /* BM_SHOW_FD_REPORT */
                         );
#ifdef IVA_FORMAT_XML /* IVA_FORMAT_XML */
	offset += sprintf(&str[offset], "</BM>");
#else /* IVA_FORMAT_JSON */
	offset += sprintf(&str[offset], "},");
#endif /* !IVA_FORMAT_XML */
	return offset;
}


/**
 * @brief Get enable status of baby monitor.
 * @param[in]  idx         video window index.
 * @param[in]  vftr_bm_ctx video baby monitor control.
 * @see none
 * @retval enable status of baby monitor.
 */
int VIDEO_FTR_getBmStat(MPI_WIN idx, VIDEO_BM_CTX_S *vftr_bm_ctx)
{
	int enable_idx = findBmCtx(idx, vftr_bm_ctx, NULL);
	return enable_idx < 0 ? 0 : vftr_bm_ctx[enable_idx].en;
}

int VIDEO_FTR_getBmRes(MPI_WIN idx, int buf_idx)
{
#ifdef REFACTOR_DONE
	VIDEO_BM_CTX_S* vftr_bm_ctx = vftr_res_shm->bm_ctx;

	int enable_idx = findBmCtx(idx, vftr_bm_ctx, NULL);
	int ret = 0;

	VFTR_BM_API_INFO("%s enter\n", __func__);
	if (enable_idx < 0) {
		goto err;
	}

	if (vftr_bm_ctx[enable_idx].en) {
		BM_CTX_S *ctx = BM_GET_CTX(enable_idx);
		VIDEO_FTR_BM_STATUS_S *bm_result_shm = &vftr_bm_ctx[enable_idx].bm_res[buf_idx];
		VIDEO_FTR_BM_STATUS_S *bm_result = &ctx->bm_res;
		VIDEO_FTR_BM_PARAM_S *param = &ctx->param;
		FD_STATUS_S fd_result = { 0 };
		ret = FD_getStat(idx, &fd_result);
		if (ret != MPI_SUCCESS) {
			SYS_TRACE("Failed to get foreground detection result.\n");
			goto err;
		}
		determineBmRes(&fd_result, param, bm_result, bm_result_shm);
		genBmAlarm(&vftr_bm_ctx[enable_idx], bm_result->current_evt, param);
	}
	VFTR_BM_API_INFO("%s exit\n", __func__);
	return MPI_SUCCESS;
err:
	return MPI_FAILURE;

#endif
	return 0;
}

int VIDEO_FTR_transBmRes(VIDEO_BM_CTX_S *vftr_bm_ctx, MPI_WIN src_idx, MPI_WIN dst_idx,
                         const MPI_RECT_S *src_rect, const MPI_RECT_S *dst_rect,
                         const MPI_RECT_S *src_roi, const MPI_RECT_S *dst_roi, char *str, int buf_idx)
{
#ifdef REFACTOR_DONE
	VFTR_BM_API_INFO("%s enter\n", __func__);
	int enable_idx = findBmCtx(src_idx, vftr_bm_ctx, NULL);

	if (enable_idx < 0) {
		return 0;
	}
	if (vftr_bm_ctx[enable_idx].en)
		return getBmMeta(src_idx, dst_idx, src_rect, dst_rect, src_roi, dst_roi, &vftr_bm_ctx[enable_idx].bm_res[buf_idx], str);
	VFTR_BM_API_INFO("%s exit\n", __func__);
	return 0;
#endif
	return 0;
}

/**
 * @brief Enable baby monitor.
 * @param[in]  idx         video window index.
 * @see VIDEO_FTR_disableBm
 * @retval MPI_SUCCESS                 success.
 * @retval MPI_FAILURE                 unexpected fail.
 */
int VIDEO_FTR_enableBm(MPI_WIN idx)
{
#ifdef REFACTOR_DONE
	VIDEO_BM_CTX_S* vftr_bm_ctx = vftr_res_shm->bm_ctx;

	int empty_idx;
	int set_idx = findBmCtx(idx, vftr_bm_ctx, &empty_idx);
	int enable_idx;
	VFTR_BM_API_INFO("%s enter\n", __func__);
	if (set_idx >= 0) {
		enable_idx = set_idx;
	} else if (empty_idx >= 0) {
		enable_idx = empty_idx;
		vftr_bm_ctx[enable_idx].idx = idx;
        BM_CTX_S *ctx = BM_GET_CTX(enable_idx);
        ctx->lock = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;
	} else {
		SYS_TRACE("FD enable failed on win %u.\n", idx.win);
		goto err;
	}

	if (!vftr_bm_ctx[enable_idx].en) {
		INT32 ret = 0;

		ret = vftrYAvgResDec();
		if (ret != 0) {
			SYS_TRACE("[WARN] All MPI YAVG ROI Resource are being used !\n");
			goto err;
		}

		ret = VIDEO_FTR_enableOd_implicit(idx);
		if (ret != MPI_SUCCESS) {
			vftrYAvgResInc();
			goto err;
		}

		//BM_CTX_S *ctx = BM_GET_CTX(enable_idx);

		ret = FD_enable(idx);
		if (ret != MPI_SUCCESS) {
			SYS_TRACE("Enable Foreground detection on win %u failed.\n", idx.win);
			goto err;
		}
		if (vftr_bm_ctx[enable_idx].cb == NULL) {
			//SYS_TRACE("Foreground detection alarm callback function is not registered on win %u.\n",
			//          idx.win);
			vftr_bm_ctx[enable_idx].cb = emptyBmCallback;
		}
		vftr_bm_ctx[enable_idx].en = 1;
	}
	VFTR_BM_API_INFO("%s exit\n", __func__);
	return MPI_SUCCESS;
err:
	return MPI_FAILURE;

#endif
	return 0;
}

/**
 * @brief Disable baby monitor.
 * @param[in]  idx        video window index.
 * @see VIDEO_FTR_enableTd
 * @retval MPI_SUCCESS     success.
 * @retval MPI_FAILURE     unexpected fail.
 */
int VIDEO_FTR_disableBm(MPI_WIN idx)
{
#ifdef REFACTOR_DONE
	VIDEO_BM_CTX_S* vftr_bm_ctx = vftr_res_shm->bm_ctx;

	int enable_idx = findBmCtx(idx, vftr_bm_ctx, NULL);

	VFTR_BM_API_INFO("%s enter\n", __func__);

	if (enable_idx < 0) {
		return MPI_SUCCESS;
	}
	if (vftr_bm_ctx[enable_idx].en) {
		INT32 ret = 0;

		vftrYAvgResInc();

		ret = VIDEO_FTR_disableOd_implicit(idx);
		if (ret != MPI_SUCCESS) {
			SYS_TRACE("Disable object detection on win %d failed!\n", idx.win);
			goto err;
		}
		ret = FD_disable(idx);
		if (ret != MPI_SUCCESS) {
			SYS_TRACE("Disable foreground detection on win %u failed.\n", idx.win);
			goto err;
		}
		vftr_bm_ctx[enable_idx].en = 0;
	}
	VFTR_BM_API_INFO("%s exit\n", __func__);
	return MPI_SUCCESS;
err:
	return MPI_FAILURE;
#endif
	return 0;
}

/**
 * @brief Get parameters of baby monitor.
 * @param[in]  idx        video window index.
 * @param[out] param      baby monitor parameters.
 * @see VIDEO_FTR_setBmParam
 * @retval MPI_SUCCESS              success.
 * @retval MPI_ERR_NULL_POINTER     input pointer is NULL.
 * @retval MPI_ERR_DEV_INVALID_WIN  invalid video window index.
 * @retval MPI_ERR_DEV_INVALID_CHN  invalid video channel index.
 * @retval MPI_ERR_DEV_INVALID_DEV  invalid device index.
 * @retval MPI_ERR_NOT_EXIST        device/channel doesn't exist.
 * @retval MPI_FAILURE              unexpected fail.
 */
int VIDEO_FTR_getBmParam(MPI_WIN idx, VIDEO_FTR_BM_PARAM_S *param)
{
#ifdef REFACTOR_DONE
	VIDEO_BM_CTX_S* vftr_bm_ctx = vftr_res_shm->bm_ctx;
    int enable_idx = findBmCtx(idx, vftr_bm_ctx, NULL);
	if (enable_idx < 0) {
		return MPI_SUCCESS;
	}

	int ret = FD_getParam(idx, &param->fd_param);
	if (ret)
		return ret;
	return 0;
#endif
	return 0;
}

/**
 * @brief Set parameters of baby monitor.
 * @param[in]  idx        video window index.
 * @param[in]  param      baby monitor parameters.
 * @see VIDEO_FTR_getBmParam
 * @retval MPI_SUCCESS              success.
 * @retval MPI_ERR_NULL_POINTER     input pointer is NULL.
 * @retval MPI_ERR_DEV_INVALID_WIN  invalid video window index.
 * @retval MPI_ERR_DEV_INVALID_CHN  invalid video channel index.
 * @retval MPI_ERR_DEV_INVALID_DEV  invalid device index.
 * @retval MPI_ERR_NOT_EXIST        device/channel doesn't exist.
 * @retval MPI_ERR_INVALID_PARAM    invalid parameters.
 * @retval MPI_FAILURE              unexpected fail.
 */
int VIDEO_FTR_setBmParam(MPI_WIN idx, const VIDEO_FTR_BM_PARAM_S *param)
{
#ifdef REFACTOR_DONE
	VIDEO_BM_CTX_S* vftr_bm_ctx = vftr_res_shm->bm_ctx;
	BM_CTX_S *ctx = NULL;
	int empty_idx;
	int set_idx = findBmCtx(idx, vftr_bm_ctx, &empty_idx);

	VFTR_BM_API_INFO("%s enter\n", __func__);

	if (set_idx<0 && empty_idx>=0) {
		vftr_bm_ctx[empty_idx].idx.value = idx.value;
		ctx = BM_GET_CTX(empty_idx);
		ctx->lock = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;
	} else if (set_idx < 0 && empty_idx < 0) {
		SYS_TRACE("Cannot find valid window (%d) in VFTR context!\n", idx.win);
		return 0;
	} else {
		ctx = BM_GET_CTX(set_idx);
	}

	FD_PARAM_S fd_param = param->fd_param;
	fd_param.event_type = FD_OBJECT_MONITOR;
	int ret = FD_setParam(idx, &fd_param);
	if (ret < 0) {
		return ret;
	}
	pthread_mutex_lock(&ctx->lock);
	ctx->param.fd_param = fd_param;
	pthread_mutex_unlock(&ctx->lock);
	VFTR_BM_API_INFO("%s exit\n", __func__);
	return MPI_SUCCESS;
#endif
	return 0;
}

/**
 * @brief Register alarm callback function of baby monitor.
 * @param[in]  idx             video window index.
 * @param[in]  alarm_cb_fptr   function pointer of callback function.
 * @see none
 * @retval MPI_SUCCESS          success.
 * @retval MPI_FAILURE          unexpected fail.
 */
int VIDEO_FTR_regBmCallback(MPI_WIN idx, const VIDEO_FTR_BM_ALARM_CB alarm_cb_fptr)
{
#ifdef REFACTOR_DONE
	if (alarm_cb_fptr == NULL) {
		SYS_TRACE("Pointer to baby monitor event callback function should not be NULL.\n");
		return MPI_FAILURE;
	}

	VIDEO_BM_CTX_S* vftr_bm_ctx = vftr_res_shm->bm_ctx;
	int empty_idx, enable_idx;
	int set_idx = findBmCtx(idx, vftr_bm_ctx, &empty_idx);

	if (set_idx >= 0) {
		enable_idx = set_idx;
	} else if (empty_idx >= 0) {
		enable_idx = empty_idx;
		vftr_bm_ctx[empty_idx].idx.value = idx.value;
		BM_CTX_S *ctx = BM_GET_CTX(empty_idx);
		ctx->lock = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;
	} else {
		SYS_TRACE("No available seting of baby monitor on the win %u.\n", idx.win);
		goto err;
	}

	vftr_bm_ctx[enable_idx].cb = alarm_cb_fptr;

	return MPI_SUCCESS;
err:
	return MPI_FAILURE;
#endif
	return 0;
}

int VIDEO_FTR_resetBmData(MPI_WIN idx)
{
#ifdef REFACTOR_DONE
	VFTR_BM_API_INFO("%s enter\n", __func__);
	VIDEO_BM_CTX_S* vftr_bm_ctx = vftr_res_shm->bm_ctx;
	int enable_idx = findBmCtx(idx, vftr_bm_ctx, NULL);
	if (enable_idx < 0) {
		/* SYS_TRACE("Baby monitor has not been enabled") */
		return 0;
	}
	int ret = FD_reset(idx);
	if (ret < 0) {
		return MPI_FAILURE;
	}
	BM_CTX_S *ctx = BM_GET_CTX(enable_idx);
	pthread_mutex_lock(&ctx->lock);
	ctx->bm_res.duration_active = 0;
	ctx->bm_res.duration_awake = 0;
	ctx->bm_res.duration_sleep = 0;
	ctx->bm_res.current_evt = 0;
	pthread_mutex_unlock(&ctx->lock);
	VFTR_BM_API_INFO("%s exit\n", __func__);
	return MPI_SUCCESS;
#endif
	return 0;
}

int VIDEO_FTR_suppressBm(MPI_WIN idx)
{
	//VIDEO_BM_CTX_S* vftr_bm_ctx = vftr_res_shm->bm_ctx;
	/* TODO: define suppression behavior */
	return 0;
}

int VIDEO_FTR_resumeBm(MPI_WIN idx)
{
	VFTR_BM_API_INFO("%s enter\n", __func__);
    /* TODO: define suppression behavior */
    VFTR_BM_API_INFO("%s exit\n", __func__);
    return 0;
}

/**
 * @brief Reset share memory of baby monitor.
 * @param[in]  idx             video window index.
 * @see none
 * @retval MPI_SUCCESS          success.
  */
int VIDEO_FTR_resetBmShm(MPI_WIN idx)
{
#ifdef REFACTOR_DONE
	VIDEO_BM_CTX_S* vftr_bm_ctx = vftr_res_shm->bm_ctx;
	int i, j;
	int enable_idx = findBmCtx(idx, vftr_bm_ctx, NULL);
	VFTR_BM_API_INFO("%s enter\n", __func__);
	if (enable_idx < 0) {
		/* SYS_TRACE("Baby monitor has not been enabled") */
		return 0;
	}
	BM_CTX_S *ctx = BM_GET_CTX(enable_idx);

	for (i = 0; i < VIDEO_BM_MAX_SUPPORT_NUM; i++) {
		pthread_mutex_lock(&ctx->lock);
		if (vftr_bm_ctx[i].en) {
			for (j = 0; j < AVFTR_VIDEO_RING_BUF_SIZE; j++)
				vftr_bm_ctx[i].bm_res[j].current_evt = VIDEO_BM_EVENT_ABSENT;
		}
		pthread_mutex_unlock(&ctx->lock);
	}
	VFTR_BM_API_INFO("%s exit\n", __func__);
	return MPI_SUCCESS;
#endif
	return 0;
}

/**
 * @brief Data control for baby monitor.
 * @param[in]  idx             video window index.
 * @param[in]  data_path       specified the data path for save/load.
 * @param[in]  ctrl            control method.
 * @see none
 * @retval MPI_SUCCESS          success.
 * @retval MPI_FAILURE          unexpected fail.
 */
int VIDEO_FTR_ctrlBmData(MPI_WIN idx, const char *data_path, VIDEO_BM_DATA_CTRL_E ctrl)
{
#ifdef REFACTOR_DONE
	VIDEO_BM_CTX_S* vftr_bm_ctx = vftr_res_shm->bm_ctx;
	int enable_idx = findBmCtx(idx, vftr_bm_ctx, NULL);
	VFTR_BM_API_INFO("%s enter\n", __func__);
	if (enable_idx < 0) {
		/* SYS_TRACE("Baby monitor has not been enabled") */
		return 0;
	}

	if (ctrl != FD_DATA_NONE) {
		int ret = FD_dataCtrl(idx, data_path, ctrl);
		if (ret) {
			/* SYS_TRACE("Baby monitor has not been enabled") */
			return MPI_SUCCESS;
		}
	}

	VFTR_BM_API_INFO("%s exit\n", __func__);
	return MPI_SUCCESS;
#endif
	return 0;
}
