#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#include "mtk_common.h"

#include "agtx_prio.h"

#include "mpi_index.h"
#include "mpi_sys.h"
#include "mpi_base_types.h"
#include "mpi_errno.h"
#include "mpi_dev.h"
#include "mpi_enc.h"
#include "mpi_iva.h"

#include "avftr.h"
#include "avftr_common.h"

#include "avftr_eaif.h"
#include "eaif.h"

/**
* @cond
*
* code fragment skipped by Doxygen.
*/

#ifdef AVFTR_EAIF_DEBUG_API
#define AVFTR_EAIF_API_INFO(fmt, args...) printf("[EAIF] " fmt, ##args)
#define eaif_log_entry() printf("[EAIF] %s Enter.\n", __func__)
#define eaif_log_exit() printf("[EAIF] %s Exit.\n", __func__)
#else
#define AVFTR_EAIF_API_INFO(fmt, args...)
#define eaif_log_entry()
#define eaif_log_exit()
#endif

extern AVFTR_VIDEO_CTX_S *vftr_res_shm;

typedef struct {
	pthread_mutex_t lock;
	AVFTR_EAIF_PARAM_S param;
	EAIF_INSTANCE_S *instance;
} EAIF_CTX_S;

static EAIF_CTX_S g_eaif_ctx[AVFTR_EAIF_MAX_SUPPORT_NUM] = { { { { 0 } } } };
#define EAIF_GET_CTX(idx) &g_eaif_ctx[idx]

static void alarmEmptyCb(MPI_WIN idx, void *args)
{
	//SYS_TRACE("Please registrate eaif alarm callback function.\n");
	return;
}

static int g_replace_od = 0;

static int findEaifCtx(MPI_WIN idx, AVFTR_EAIF_CTX_S *ctx, int *empty)
{
	int i = 0;
	int find_idx = -1;
	int emp_idx = -1;

	if (empty == NULL) {
		emp_idx = -2;
	} else {
		emp_idx = -1;
	}
	for (i = 0; i < AVFTR_EAIF_MAX_SUPPORT_NUM; i++) {
		if (find_idx == -1 && ctx[i].idx.value == idx.value && ctx[i].reg) {
			find_idx = i;
		} else if (emp_idx == -1 && !ctx[i].reg) {
			emp_idx = i;
		}
	}
	if (empty != NULL) {
		*empty = emp_idx;
	}
	return find_idx;
}

static void genAvftrEaifDetect(const EAIF_STATUS_S *status, VIDEO_FTR_OBJ_LIST_S *ol)
{
	int i;
	VIDEO_FTR_OBJ_ATTR_S *obj = NULL;
	MPI_IVA_OBJ_ATTR_S *od = NULL;
	const EAIF_OBJ_ATTR_S *eobj = NULL;

	ol->basic_list.obj_num = status->obj_cnt;
	for (i = 0; i < status->obj_cnt; i++) {
		eobj = &status->obj_attr[i];
		obj = &ol->obj_attr[i];
		od = &ol->basic_list.obj[i];

		od->id = eobj->id; // TBD how to maintain detection id ?
		od->mv = (MPI_MOTION_VEC_S){ 0, 0 }; // TBD how to get detection motion vector ?
		od->life = EAIF_MAX_OBJ_LIFE_TH; // TBD how to determine detection life ?
		od->rect = eobj->rect;
		obj->shaking = 0;
		strncpy(obj->cat, eobj->category[0], VFTR_OBJ_CAT_LEN);
		strncpy(obj->conf, eobj->prob[0], VFTR_OBJ_CONF_LEN);
	}
}

static void genAvftrEaifClassify(const EAIF_STATUS_S *status, VIDEO_FTR_OBJ_LIST_S *ol)
{
	int i, j, k, set;
	int datasize;
	int range;
	VIDEO_FTR_OBJ_ATTR_S *obj = NULL;
	MPI_IVA_OBJ_ATTR_S *od = NULL;
	const EAIF_OBJ_ATTR_S *eobj = NULL;

	for (i = 0; i < ol->basic_list.obj_num; i++) {
		obj = &ol->obj_attr[i];
		od = &ol->basic_list.obj[i];
		set = 0;
		for (j = 0; j < status->obj_cnt; j++) {
			eobj = &status->obj_attr[j];
			if (od->id == eobj->id) {
				datasize = 0;
				range = VFTR_OBJ_CAT_LEN;
				for (k = 0; k < eobj->label_num && range >= 0; ++k) {
					datasize += snprintf(obj->cat + datasize, range, "%s ", eobj->category[k]);
					range = VFTR_OBJ_CAT_LEN - datasize;
				}
				set = 1;
				break;
			}
		}
		if (set == 0) {
			strcpy(obj->cat, "NoMatch");
		}
	}
	// eaif_tr("[DEBUG] status cnt:%d [0]rect:%d %d %d %d vs obj num:%d [0] rect:%d %d %d %d\n", status->obj_cnt,
	//         status->obj_attr[0].rect.sx, status->obj_attr[0].rect.sy, status->obj_attr[0].rect.ex,
	//         status->obj_attr[0].rect.ey, ol->basic_list.obj_num, ol->basic_list.obj[0].rect.sx,
	//         ol->basic_list.obj[0].rect.sy, ol->basic_list.obj[0].rect.ex, ol->basic_list.obj[0].rect.ey);
}

static void genAvftrEaifRes(const AVFTR_EAIF_PARAM_S *p, const EAIF_STATUS_S *status, VIDEO_FTR_OBJ_LIST_S *ol)
{
	int i;
	VIDEO_FTR_OBJ_ATTR_S *obj = NULL;
	if (status->server_reachable == EAIF_URL_REACHABLE) {
		/* TODO: Modify OD attr to support prob or other attri */
		if (p->api == EAIF_API_FACERECO || p->api == EAIF_API_CLASSIFY || p->api == EAIF_API_CLASSIFY_CV ||
		    p->api == EAIF_API_HUMAN_CLASSIFY) {
			genAvftrEaifClassify(status, ol);
		} else if (p->api == EAIF_API_DETECT) {
			if (g_replace_od)
				genAvftrEaifDetect(status, ol);
		}
	} else {
		for (i = 0; i < ol->basic_list.obj_num; i++) {
			obj = &ol->obj_attr[i];
			obj->cat[0] = '\0';
		}
	}
}

// toAvoid extra function definition without using avftr_common
static void rescaleEaifMpiRectPoint(const MPI_RECT_S *src_rect, const MPI_RECT_S *dst_rect, const MPI_RECT_S *src_roi,
                                    const MPI_RECT_S *dst_roi, MPI_RECT_POINT_S *roi)
{
	INT32 roi_sx = roi->sx - src_rect->x;
	INT32 roi_sy = roi->sy - src_rect->y;
	INT32 roi_ex = roi->ex - src_rect->x;
	INT32 roi_ey = roi->ey - src_rect->y;

	INT32 src_roi_width = src_roi->width;
	INT32 src_roi_height = src_roi->height;
	INT32 src_roi_x = src_roi->x;
	INT32 src_roi_y = src_roi->y;

	INT32 dst_roi_width = dst_roi->width;
	INT32 dst_roi_height = dst_roi->height;
	INT32 dst_roi_x = dst_roi->x;
	INT32 dst_roi_y = dst_roi->y;

	INT32 src_rect_width = src_rect->width;
	INT32 src_rect_height = src_rect->height;

	INT32 dst_rect_width = dst_rect->width;
	INT32 dst_rect_height = dst_rect->height;
	INT32 dst_rect_x = dst_rect->x;
	INT32 dst_rect_y = dst_rect->y;

	roi_sx = (roi_sx * src_roi_width + (src_rect_width >> 1)) / src_rect_width + src_roi_x - dst_roi_x;
	roi_sx = (roi_sx * dst_rect_width + (dst_roi_width >> 1)) / dst_roi_width;
	roi_ex = (roi_ex * src_roi_width + (src_rect_width >> 1)) / src_rect_width + src_roi_x - dst_roi_x;
	roi_ex = (roi_ex * dst_rect_width + (dst_roi_width >> 1)) / dst_roi_width;

	roi_sy = (roi_sy * src_roi_height + (src_rect_height >> 1)) / src_rect_height + src_roi_y - dst_roi_y;
	roi_sy = (roi_sy * dst_rect_height + (dst_roi_height >> 1)) / dst_roi_height;
	roi_ey = (roi_ey * src_roi_height + (src_rect_height >> 1)) / src_rect_height + src_roi_y - dst_roi_y;
	roi_ey = (roi_ey * dst_rect_height + (dst_roi_height >> 1)) / dst_roi_height;

	roi_sx += dst_rect_x;
	roi_sy += dst_rect_y;
	roi_ex += dst_rect_x;
	roi_ey += dst_rect_y;

	roi->sx = CLAMP(roi_sx, SHRT_MIN, SHRT_MAX);
	roi->sy = CLAMP(roi_sy, SHRT_MIN, SHRT_MAX);
	roi->ex = CLAMP(roi_ex, SHRT_MIN, SHRT_MAX);
	roi->ey = CLAMP(roi_ey, SHRT_MIN, SHRT_MAX);
}

/**
 * @brief Get predefined formated AROI result string for Multiplayer.
 * @param[in]  src_idx         mpi win index of source window
 * @param[in]  dst_idx         mpi win index of destination window
 * @param[in]  src_rect        source window
 * @param[in]  dst_rect        destination window
 * @param[in]  aroi_stat   automatic region of interest result.
 * @param[out] str          formated AROI result string buffer.
 * @see VIDEO_FTR_getAroiRes()
 * @retval length of formated AROI result.
 */
static int getEaifMeta(MPI_WIN src_idx, MPI_WIN dst_idx, const MPI_RECT_S *src_rect, const MPI_RECT_S *dst_rect,
                       const MPI_RECT_S *src_roi, const MPI_RECT_S *dst_roi, const EAIF_STATUS_S *eaif_stat, char *str)
{
	int offset = 0;
	int i, j;
	if (eaif_stat->obj_attr[0].rect.sx == EAIF_RECT_INIT)
		return 0;

	EAIF_STATUS_S dst_stat = *eaif_stat;
	EAIF_OBJ_ATTR_S *attr = NULL;

	if (src_idx.value != dst_idx.value) {
		for (int i = 0; i < dst_stat.obj_cnt; i++) {
			rescaleEaifMpiRectPoint(src_rect, dst_rect, src_roi, dst_roi, &dst_stat.obj_attr[i].rect);
		}
	}

	offset += print_meta(&str[offset], "<EAIF>", "\"eaif\":[");
	for (i = 0; i < dst_stat.obj_cnt; i++) {
		attr = &dst_stat.obj_attr[i];
		char categories[256] = {};
		int str_size = 0;
		for (j = 0; j < attr->label_num; j++)
			str_size += sprintf(&categories[str_size], "%s ", attr->category[j]);
		if (attr->label_num)
			categories[str_size - 1] = 0;

		offset += print_meta(&str[offset], "<OBJ ID=\"%d\" RECT=\"%d %d %d %d\" CAT=\"%s\"/>",
		                     "{\"obj\":{\"id\":%d,\"rect\":[%d,%d,%d,%d],\"cat\":\"%s\"}},", attr->id,
		                     attr->rect.sx, attr->rect.sy, attr->rect.ex, attr->rect.ey, categories);
	}

#ifndef IVA_FORMAT_XML /* IVA_FORMAT_XML */
	if (dst_stat.obj_cnt)
		offset--;
#endif
	offset += print_meta(&str[offset], "</MD>", "],");
	return offset;
}

/**
 * @brief Get predefined formated IVA result for Multiplayer.
 * @param[in]  vftr_eaif_ctx   automatic region of interest result.
 * @param[in]  src_idx         mpi win index of source window
 * @param[in]  dst_idx         mpi win index of destination window
 * @param[in]  src_rect        source window
 * @param[in]  dst_rect        destination window
 * @param[out] str         formated IVA result string buffer.
 * @see VIDEO_FTR_getAroiRes()
 * @retval length of formated IVA result.
 */
int AVFTR_EAIF_transRes(AVFTR_EAIF_CTX_S *vftr_eaif_ctx, MPI_WIN src_idx, MPI_WIN dst_idx, const MPI_RECT_S *src_rect,
                        const MPI_RECT_S *dst_rect, const MPI_RECT_S *src_roi, const MPI_RECT_S *dst_roi, char *str,
                        int buf_idx)
{
	int enable_idx = findEaifCtx(src_idx, vftr_eaif_ctx, NULL);

	if (enable_idx < 0) {
		return 0;
	}
	if (vftr_eaif_ctx[enable_idx].en)
		return getEaifMeta(src_idx, dst_idx, src_rect, dst_rect, src_roi, dst_roi,
		                   &vftr_eaif_ctx[enable_idx].stat[buf_idx], str);
	return 0;
}

int AVFTR_EAIF_addInstance(MPI_WIN idx)
{
	eaif_log_entry();
	AVFTR_EAIF_CTX_S *vftr_eaif_ctx = vftr_res_shm->eaif_ctx;
	int empty_idx;
	int set_idx = findEaifCtx(idx, vftr_eaif_ctx, &empty_idx);

	if (set_idx >= 0) {
		/* idx is registered */
		SYS_TRACE("idx:%d is registerd\n", idx.value);
		return 0;
	} else if (set_idx < 0 && empty_idx >= 0) {
		/* idx is not registered yet but there is empty space to be registerd*/
		EAIF_CTX_S *ctx = EAIF_GET_CTX(empty_idx);

		ctx->instance = EAIF_newInstance(idx);
		if (!ctx->instance) {
			SYS_TRACE("add EAIF instance failed on win %u.\n", idx.win);
			return -ENOMEM;
		}

		vftr_eaif_ctx[empty_idx].idx = idx;
		vftr_eaif_ctx[empty_idx].reg = 1;
		vftr_eaif_ctx[empty_idx].en = 0;
		vftr_eaif_ctx[empty_idx].cb = NULL;
	} else {
		/* No more space to register idx */
		SYS_TRACE("add EAIF instance failed on win %u.\n", idx.win);
		return -ENOMEM;
	}
	eaif_log_exit();
	return 0;
}

/**
 * @brief Delete EAIF instance.
 * @param[in]  idx         video window index.
 * @see AVFTR_EAIF_addInstance
 * @retval 0                 success.
 * @retval -EAGAIN            idx is enabled, not to remove
 * @retval -EFAULT            idx is enabled, but the instance is null
 */
int AVFTR_EAIF_deleteInstance(MPI_WIN idx)
{
	eaif_log_entry();
	AVFTR_EAIF_CTX_S *vftr_eaif_ctx = vftr_res_shm->eaif_ctx;
	int enable_idx = findEaifCtx(idx, vftr_eaif_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered */
		SYS_TRACE("idx:%d is not registered yet!\n", idx.value);
		return 0;
	}
	if (vftr_eaif_ctx[enable_idx].en) {
		/* idx is enabled */
		SYS_TRACE("idx:%d is still enable, can not be deleted!\n", idx.value);
		return -EAGAIN;
	}

	EAIF_CTX_S *ctx = EAIF_GET_CTX(enable_idx);

	/* Free EAIF instance*/
	if (ctx->instance == NULL) {
		SYS_TRACE("Pointer to the EAIF instance should not be NULL.\n");
		return -EFAULT;
	}

	if (ctx->instance->algo_status == NULL) {
		SYS_TRACE("Pointer to the EAIF algo instance should not be NULL.\n");
		return -EFAULT;
	}

	pthread_mutex_lock(&ctx->lock);
	EAIF_deleteInstance(&ctx->instance);
	pthread_mutex_unlock(&ctx->lock);

	if (ctx->instance) {
		SYS_TRACE("Cannot delete eaif instance.\n");
		return -EFAULT;
	}

	/* !Free EAIF object*/
	vftr_eaif_ctx[enable_idx].reg = 0;
	vftr_eaif_ctx[enable_idx].en = 0;
	vftr_eaif_ctx[enable_idx].cb = NULL;
	eaif_log_exit();
	return 0;
}

/**
 * @brief Enable EAIF (including OD).
 * @param[in]  idx         video window index.
 * @see AVFTR_EAIF_disable
 * @retval 0                 success.
 * @retval ENODEV            idx is not registered yet
 * @retval <0                unexpected error
 */
int AVFTR_EAIF_enable(MPI_WIN idx)
{
	eaif_log_entry();
	AVFTR_EAIF_CTX_S *vftr_eaif_ctx = vftr_res_shm->eaif_ctx;
	int enable_idx = findEaifCtx(idx, vftr_eaif_ctx, NULL);

	if (enable_idx < 0) {
		/* idx is not registered */
		SYS_TRACE("idx:%d is not registered yet!\n", idx.value);
		return ENODEV;
	}

	if (vftr_eaif_ctx[enable_idx].en) {
		/* idx is enabled */
		SYS_TRACE("idx:%d is enabled, no need to enable again!\n", idx.value);
		return 0;
	}

	INT32 ret = 0;
	ret = VIDEO_FTR_enableOd(idx);
	if (ret != 0) {
		return ret;
	}

	if (vftr_eaif_ctx[enable_idx].cb == NULL) {
		//SYS_TRACE("Door Keerper alarm callback function is not registered on win %u.\n", idx.win);
		vftr_eaif_ctx[enable_idx].cb = alarmEmptyCb;
	}

	EAIF_CTX_S *ctx = EAIF_GET_CTX(enable_idx);

	//EAIF_ENABLE
	pthread_mutex_lock(&ctx->lock);
	ret = EAIF_activate(ctx->instance);
	pthread_mutex_unlock(&ctx->lock);

	if (ret != 0) {
		return ret;
	}

	vftr_eaif_ctx[enable_idx].en = 1;
	eaif_log_exit();
	return 0;
}

/**
 * @brief Enable EAIF.
 * @param[in]  idx         video window index.
 * @see AVFTR_EAIF_disableV2
 * @retval 0                 success.
 * @retval ENODEV            idx is not registered yet
 */
int AVFTR_EAIF_enableV2(MPI_WIN idx)
{
	eaif_log_entry();
	AVFTR_EAIF_CTX_S *vftr_eaif_ctx = vftr_res_shm->eaif_ctx;
	int enable_idx = findEaifCtx(idx, vftr_eaif_ctx, NULL);

	if (enable_idx < 0) {
		/* idx is not registered */
		SYS_TRACE("idx:%d is not registered yet!\n", idx.value);
		return ENODEV;
	}

	if (vftr_eaif_ctx[enable_idx].en) {
		/* idx is enabled */
		SYS_TRACE("idx:%d is enabled, no need to enable again!\n", idx.value);
		return 0;
	}

	if (vftr_eaif_ctx[enable_idx].cb == NULL) {
		//SYS_TRACE("Door Keerper alarm callback function is not registered on win %u.\n", idx.win);
		vftr_eaif_ctx[enable_idx].cb = alarmEmptyCb;
	}

	EAIF_CTX_S *ctx = EAIF_GET_CTX(enable_idx);

	//EAIF_ENABLE
	int ret = EAIF_activate(ctx->instance);
	if (ret != 0) {
		return ret;
	}

	vftr_eaif_ctx[enable_idx].en = 1;
	eaif_log_exit();
	return 0;
}

/**
 * @brief Disable EAIF (including OD).
 * @param[in]  idx        video window index.
 * @see AVFTR_EAIF_enable
 * @retval 0                 success.
 * @retval ENODEV            idx is not registered yet
 * @retval <0              unexpected error
 */
int AVFTR_EAIF_disable(MPI_WIN idx)
{
	eaif_log_entry();
	AVFTR_EAIF_CTX_S *vftr_eaif_ctx = vftr_res_shm->eaif_ctx;
	int enable_idx = findEaifCtx(idx, vftr_eaif_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registerd yet, no need to disable\n", idx.value);
		return ENODEV;
	}

	if (!vftr_eaif_ctx[enable_idx].en) {
		/* idx is not enabled */
		SYS_TRACE("idx:%d is not enabled yet, no need to disable\n", idx.value);
		return 0;
	}

	int ret = 0;

	EAIF_CTX_S *ctx = EAIF_GET_CTX(enable_idx);

	//Disable EAIF
	pthread_mutex_lock(&ctx->lock);
	ret = EAIF_deactivate(ctx->instance);
	pthread_mutex_unlock(&ctx->lock);
	if (ret != 0) {
		return ret;
	}

	ret = VIDEO_FTR_disableOd(idx);
	if (ret != 0) {
		SYS_TRACE("Disable object detection on win %d failed!\n", idx.win);
		return ret;
	}
	vftr_eaif_ctx[enable_idx].en = 0;
	eaif_log_exit();
	return 0;
}

/**
 * @brief Disable EAIF.
 * @param[in]  idx        video window index.
 * @see AVFTR_EAIF_enableV2
 * @retval 0                 success.
 * @retval ENODEV            idx is not registered yet
 */
int AVFTR_EAIF_disableV2(MPI_WIN idx)
{
	eaif_log_entry();
	AVFTR_EAIF_CTX_S *vftr_eaif_ctx = vftr_res_shm->eaif_ctx;
	int enable_idx = findEaifCtx(idx, vftr_eaif_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registerd yet, no need to disable\n", idx.value);
		return ENODEV;
	}

	if (!vftr_eaif_ctx[enable_idx].en) {
		/* idx is not enabled */
		SYS_TRACE("idx:%d is not enabled yet, no need to disable\n", idx.value);
		return 0;
	}

	int ret = 0;

	EAIF_CTX_S *ctx = EAIF_GET_CTX(enable_idx);

	//Disable EAIF
	ret = EAIF_deactivate(ctx->instance);
	if (ret != 0) {
		return ret;
	}

	vftr_eaif_ctx[enable_idx].en = 0;
	eaif_log_exit();
	return 0;
}

/**
 * @brief Get parameters of EAIF.
 * @param[in]  idx        video window index.
 * @param[out] param      EAIF parameters.
 * @see AVFTR_EAIF_setParam
 * @retval 0              success.
 * @retval ENODEV         idx is not registered yet
 */
int AVFTR_EAIF_getParam(MPI_WIN idx, AVFTR_EAIF_PARAM_S *param)
{
	eaif_log_entry();
	AVFTR_EAIF_CTX_S *vftr_eaif_ctx = vftr_res_shm->eaif_ctx;
	int enable_idx = findEaifCtx(idx, vftr_eaif_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registered\n", idx.value);
		return ENODEV;
	}

	EAIF_CTX_S *ctx = EAIF_GET_CTX(enable_idx);
	pthread_mutex_lock(&ctx->lock);
	int ret = EAIF_getParam(ctx->instance, param);
	pthread_mutex_unlock(&ctx->lock);
	if (ret) {
		SYS_TRACE("Cannot get eaif param!\n");
		return ret;
	}
	eaif_log_exit();
	return 0;
}

/**
 * @brief set parameters of EAIF.
 * @param[in]  idx        video window index.
 * @param[in]  param      EAIF parameters.
 * @see AVFTR_EAIF_getParam
 * @retval 0              success.
 * @retval ENODEV         idx is not registered yet
 */
int AVFTR_EAIF_setParam(MPI_WIN idx, const AVFTR_EAIF_PARAM_S *param)
{
	eaif_log_entry();
	AVFTR_EAIF_CTX_S *vftr_eaif_ctx = vftr_res_shm->eaif_ctx;
	int enable_idx = findEaifCtx(idx, vftr_eaif_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registered\n", idx.value);
		return ENODEV;
	}

	if (param == NULL) {
		SYS_TRACE("Pointer to the eaif parameter should not be NULL.\n");
		return EINVAL;
	}

	EAIF_CTX_S *ctx = EAIF_GET_CTX(enable_idx);

	int ret = EAIF_checkParam(param);
	if (ret) {
		SYS_TRACE("Invalid EAIF input param!\n");
		return ret;
	}

	pthread_mutex_lock(&ctx->lock);
	ret = EAIF_setParam(ctx->instance, param);
	pthread_mutex_unlock(&ctx->lock);
	if (ret) {
		SYS_TRACE("Fail to set EAIF param!\n");
		return ret;
	}

	if (vftr_eaif_ctx[enable_idx].en && param->inf_utils.cmd != EAIF_INF_NONE && param->api == EAIF_API_FACERECO &&
	    !strcmp(param->url, EAIF_INFERENCE_INAPP_STR)) {
		ret = EAIF_applyFaceUtils(ctx->instance, param);
		if (ret) {
			SYS_TRACE("Fail to apply face utils!\n");
			return ret;
		}
	}

	pthread_mutex_lock(&ctx->lock);
	ctx->param = *param;
	pthread_mutex_unlock(&ctx->lock);
	eaif_log_exit();
	return 0;
}

/**
 * @brief Get result of EAIF.
 * @param[in]  idx         video window index.
 * @param[in]  vftr_eaif_ctx video EAIF control.
 * @see none
 * @retval 0                 success.
 * @retval ENODEV            idx is not registered
 * @retval EAGAIN            idx is not enabled
 * @retval EFAULT            EAIF object is NULL
 */
int AVFTR_EAIF_getRes(MPI_WIN idx, VIDEO_FTR_OBJ_LIST_S *ol, int buf_idx)
{
	eaif_log_entry();
	AVFTR_EAIF_CTX_S *vftr_eaif_ctx = vftr_res_shm->eaif_ctx;

	int enable_idx = findEaifCtx(idx, vftr_eaif_ctx, NULL);

	if (enable_idx < 0) {
		/* idx is not registered yet */
		//SYS_TRACE("idx:%d is not registered yet!\n",idx.value);
		return -ENODEV;
	}

	if (!vftr_eaif_ctx[enable_idx].en) {
		/* idx is not enabled yet */
		//SYS_TRACE("idx:%d is not enabled yet!\n",idx.value);
		return -EAGAIN;
	}

	EAIF_CTX_S *ctx = EAIF_GET_CTX(enable_idx);
	if (ctx->instance == NULL) {
		SYS_TRACE("EAIF instance is NULL!\n");
		return -EFAULT;
	}

	EAIF_STATUS_S *status = &vftr_eaif_ctx[enable_idx].stat[buf_idx];

	EAIF_testRequest(ctx->instance, &ol->basic_list, status);
	pthread_mutex_lock(&ctx->lock);
	genAvftrEaifRes(&ctx->param, status, ol);
	pthread_mutex_unlock(&ctx->lock);
	eaif_log_exit();
	return 0;
}

/**
 * @brief Get enable status of edge AI assisted feature
 * @param[in] idx	 video window index.
 * @param[in] vftr_eaif_ctx	 video eaif ctx.
 * @see none
 * @retval get status of eaif.
 */
int AVFTR_EAIF_getStat(MPI_WIN idx, AVFTR_EAIF_CTX_S *vftr_eaif_ctx)
{
	eaif_log_entry();
	int enable_idx = findEaifCtx(idx, vftr_eaif_ctx, NULL);
	eaif_log_exit();
	return enable_idx < 0 ? 0 : vftr_eaif_ctx[enable_idx].en;
}

/**
 * @brief Check edge AI assisted feature parameter
 * @param[in] param	 eaif parameters.
 * @see none
 * @retval -EFAULT input parameter is null.
 * @retval -EINVAL input parameter is invalid.
 * @retval 0 input parameter is valid.
 */
int AVFTR_EAIF_checkParam(const AVFTR_EAIF_PARAM_S *param)
{
	if (!param) {
		return -EFAULT;
	}
	return -EAIF_checkParam(param);
}
