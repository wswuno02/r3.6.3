/**
* @cond
*
* code fragment skipped by Doxygen.
*/
#include <stdio.h>
#include <errno.h>

#include "avftr_md.h"
#include "video_od.h"
#include "avftr.h"
#include "avftr_common.h"
#include "mtk_common.h"
#include "mpi_dev.h"

extern AVFTR_VIDEO_CTX_S *vftr_res_shm;

typedef struct {
	uint32_t start_time; /**< Param to store alarm start time */
	AVFTR_MD_PARAM_S param;
	VFTR_MD_INSTANCE_S *instance;
	int s_flag;
	pthread_mutex_t lock;
	pthread_mutex_t cb_lock;
} MD_CTX_S;

static MD_CTX_S g_md_ctx[AVFTR_MD_MAX_SUPPORT_NUM] = { { 0 } };

#define MD_GET_CTX(idx) &g_md_ctx[idx]

static int findMdCtx(MPI_WIN idx, AVFTR_MD_CTX_S *ctx, int *empty)
{
	int i = 0;
	int find_idx = -1;
	int emp_idx = -1;

	if (empty == NULL) {
		emp_idx = -2;
	} else {
		emp_idx = -1;
	}

	for (i = 0; i < AVFTR_MD_MAX_SUPPORT_NUM; i++) {
		if (find_idx == -1 && ctx[i].idx.value == idx.value && ctx[i].reg) {
			find_idx = i;
		} else if (emp_idx == -1 && !ctx[i].reg) {
			emp_idx = i;
		}
	}

	if (empty != NULL) {
		*empty = emp_idx;
	}

	return find_idx;
}

static void getMdResOffset(MPI_WIN idx, VFTR_MD_STATUS_S *stat)
{
	uint32_t x = 0;
	uint32_t y = 0;
	int32_t i;
	MPI_CHN_LAYOUT_S layout_attr;
	MPI_CHN chn = MPI_VIDEO_CHN(idx.dev, idx.chn);
	if (MPI_DEV_getChnLayout(chn, &layout_attr) < 0) {
		SYS_TRACE("Cannot get channel layout for chn:%d\n", chn.chn);
		return;
	}
	for (i = 0; i < layout_attr.window_num; i++) {
		if (idx.value == layout_attr.win_id[i].value) {
			break;
		}
	}
	if (i == layout_attr.window_num) {
		SYS_TRACE("Window %d does not exist in channel %d\n", idx.win, idx.chn);
		return;
	}

	x = layout_attr.window[i].x;
	y = layout_attr.window[i].y;

	for (i = 0; i < stat->region_num; i++) {
		stat->attr[i].pts.sx += x;
		stat->attr[i].pts.sy += y;
		stat->attr[i].pts.ex += x;
		stat->attr[i].pts.ey += y;
	}
}

static void mdProcShakeObjectList(VIDEO_FTR_OBJ_LIST_S *vol, const VIDEO_FTR_OBJ_LIST_S *obj_list)
{
	int i;
	int obj_cnt = 0;
	MPI_IVA_OBJ_LIST_S *ol = &vol->basic_list;
	for (i = 0; i < obj_list->basic_list.obj_num; i++) {
		if (obj_list->obj_attr[i].shaking) {
			/* skip shaking objects */
			//memcpy(&ol->obj[i].rect, &obj_list->obj_attr[i].shake_rect, sizeof(MPI_RECT_POINT_S));
		} else {
			ol->obj[obj_cnt] = obj_list->basic_list.obj[i];
			strcpy(vol->obj_attr[obj_cnt].cat, obj_list->obj_attr[i].cat);
			vol->obj_attr[obj_cnt].shaking = obj_list->obj_attr[i].shaking;
			obj_cnt++;
		}
	}
	ol->obj_num = obj_cnt;
}

static void mdProcPdObjectList(VIDEO_FTR_OBJ_LIST_S *vol, const VIDEO_FTR_OBJ_LIST_S *obj_list)
{
	int i;
	int obj_cnt = 0;
	MPI_IVA_OBJ_LIST_S *ol = &vol->basic_list;
	for (i = 0; i < obj_list->basic_list.obj_num; i++) {
		if (!strcmp(obj_list->obj_attr[i].cat, AVFTR_PD_UNKNOWN)) {
			/* skip shaking objects */
			//memcpy(&ol->obj[i].rect, &obj_list->obj_attr[i].shake_rect, sizeof(MPI_RECT_POINT_S));
		} else {
			ol->obj[obj_cnt] = obj_list->basic_list.obj[i];
			strcpy(vol->obj_attr[obj_cnt].cat, obj_list->obj_attr[i].cat);
			vol->obj_attr[obj_cnt].shaking = obj_list->obj_attr[i].shaking;
			obj_cnt++;
		}
	}
	ol->obj_num = obj_cnt;
}

/**
 * @brief Invoke callback function when alarm condition is satisfied.
 * @param[in] idx         video channel index.
 * @param[in] stat        detected result.
 * @see VIDEO_FTR_getMdRes()
 * @retval none.
 */
static void genMdAlarm(AVFTR_MD_CTX_S *vftr_md_ctx, const VFTR_MD_STATUS_S *stat, MD_CTX_S *alarm_ctx,
                       const VIDEO_FTR_OBJ_LIST_S *obj_list)
{
	if (vftr_md_ctx->cb == NULL) {
		return;
	}

	int i;
	uint32_t timestamp = obj_list->basic_list.timestamp;
	uint32_t diff;
	for (i = 0; i < stat->region_num; i++) {
		if (stat->stat[i].alarm == VFTR_MD_ALARM_TRUE) {
			if (alarm_ctx->start_time) {
				/* Alarm buffer duration checking */
				diff = timestamp - alarm_ctx->start_time;
				//printf("[%s] frtime: %u - start: %u = %u > buf: %u ?\n", __func__, timestamp, alarm_ctx->start_time, diff, alarm_ctx->duration);
				if (diff > alarm_ctx->param.duration) {
					vftr_md_ctx->total_alarm = 1;
					vftr_md_ctx->cb(1);
				}
			} else {
				/* Register alarm start time */
				alarm_ctx->start_time = timestamp;
			}
			return;
		}
	}
	if (vftr_md_ctx->total_alarm) {
		vftr_md_ctx->cb(0);
		vftr_md_ctx->total_alarm = 0;
	}
	/* Reset alarm buffer */
	alarm_ctx->start_time = 0;
}

/**
 * @brief Get predefined metadata format.
 * @param[in]  stat            detected result.
 * @param[in]  src_idx         mpi win index of source window
 * @param[in]  dst_idx         mpi win index of destination window
 * @param[in]  src_rect        source window
 * @param[in]  dst_rect        destination window
 * @param[in] str     metadata string buffer.
 * @see VIDEO_FTR_getMdRes()
 * @retval length of metadata string.
 */
static int getMdMeta(MPI_WIN src_idx, MPI_WIN dst_idx, const MPI_RECT_S *src_rect, const MPI_RECT_S *dst_rect,
                     const MPI_RECT_S *src_roi, const MPI_RECT_S *dst_roi, const VFTR_MD_STATUS_S *stat, char *str)
{
	int offset = 0;
	int i = 0;
	VFTR_MD_STATUS_S dst_stat = *stat;

	if (src_idx.value != dst_idx.value) {
		for (int i = 0; i < dst_stat.region_num; i++) {
			rescaleMpiRectPoint(src_rect, dst_rect, src_roi, dst_roi, &dst_stat.attr[i].pts);
		}
	}

#ifdef IVA_FORMAT_XML /* IVA_FORMAT_XML */
	offset += sprintf(&str[offset], "<MD>");
#else /* IVA_FORMAT_JSON */
	offset += sprintf(&str[offset], "\"md\":[ ");
#endif /* !IVA_FORMAT_XML */
	for (i = 0; i < dst_stat.region_num; i++) {
		offset += sprintf(&str[offset],
#ifdef IVA_FORMAT_XML /* IVA_FORMAT_XML */
		                  "<REG ID=\"%d\" ALARM=\"%d\" RECT=\"%d %d %d %d\"/>",
#else /* IVA_FORMAT_JSON */
		                  "{\"reg\":{\"id\":%d,\"alarm\":%d,\"rect\":[%d,%d,%d,%d]}},",
#endif /* !IVA_FORMAT_XML */
		                  dst_stat.attr[i].id, dst_stat.stat[i].alarm, dst_stat.attr[i].pts.sx,
		                  dst_stat.attr[i].pts.sy, dst_stat.attr[i].pts.ex, dst_stat.attr[i].pts.ey);
	}
#ifdef IVA_FORMAT_XML /* IVA_FORMAT_XML */
	offset += sprintf(&str[offset], "</MD>");
#else /* IVA_FORMAT_JSON */
	offset += (sprintf(&str[offset - 1], "],") - 1);
#endif /* !IVA_FORMAT_XML */
	return offset;
}

/**
 * @brief Empty callback function for initialization.
 * @param[in] none.
 * @see VIDEO_FTR_enableMd()
 * @retval none.
 */
static void alarmEmptyCb(uint8_t alarm)
{
	SYS_TRACE("Please registrate motion detection alarm callback function.\n");
	return;
}

/**
 * @endcond
 */

/**
 * @brief Get enable status of motion detection.
 * @param[in]  idx         video window index.
 * @param[in]  vftr_md_ctx video motion detection control.
 * @see none
 * @retval enable status of motion detection.
 */
int AVFTR_MD_getStat(MPI_WIN idx, AVFTR_MD_CTX_S *vftr_md_ctx)
{
	int enable_idx = findMdCtx(idx, vftr_md_ctx, NULL);

	return enable_idx < 0 ? 0 : vftr_md_ctx[enable_idx].en;
}

/**
 * @brief Get result of motion detection.
 * @param[in]  idx         video window index.
 * @param[in]  vftr_md_ctx video motion detection control.
 * @see none
 * @retval 0                 success.
 * @retval ENODEV            idx is not registered
 * @retval EAGAIN            idx is not enabled
 * @retval EFAULT            MD instance is NULL
 */
int AVFTR_MD_getRes(MPI_WIN idx, const VIDEO_FTR_OBJ_LIST_S *obj_list, int buf_idx)
{
	AVFTR_MD_CTX_S *vftr_md_ctx = vftr_res_shm->md_ctx;

	int enable_idx = findMdCtx(idx, vftr_md_ctx, NULL);
	int ret = 0;

	if (enable_idx < 0) {
		/* idx is not registered yet */
		//SYS_TRACE("idx:%d is not registered yet!\n",idx.value);
		return ENODEV;
	}

	if (!vftr_md_ctx[enable_idx].en) {
		/* idx is not enabled yet */
		//SYS_TRACE("idx:%d is not enabled yet!\n",idx.value);
		return EAGAIN;
	}

	VIDEO_FTR_OBJ_LIST_S ol, ol1;
	const VIDEO_FTR_OBJ_LIST_S *ol_ptr, *ol_ptr1;

	/* idx is registered */
	MD_CTX_S *ctx = MD_GET_CTX(enable_idx);
	if (ctx->instance == NULL) {
		SYS_TRACE("MD instance is NULL!\n");
		return EFAULT;
	}

	VFTR_MD_STATUS_S *md_result_shm = &vftr_md_ctx[enable_idx].md_res[buf_idx];

	ol_ptr1 = obj_list;
	if (ctx->param.en_skip_shake) {
		mdProcShakeObjectList(&ol, obj_list);
		ol_ptr1 = &ol;
	}
	ol_ptr = ol_ptr1;
	if (ctx->param.en_skip_pd) {
		mdProcPdObjectList(&ol1, ol_ptr1);
		ol_ptr = &ol1;
	}

	pthread_mutex_lock(&ctx->lock);
	ret = VFTR_MD_detectMotion(ctx->instance, &ol_ptr->basic_list, md_result_shm);
	pthread_mutex_unlock(&ctx->lock);

	if (ret != 0) {
		SYS_TRACE("Failed to run motion detection!\n");
		return ret;
	}

	pthread_mutex_lock(&ctx->cb_lock);
	genMdAlarm(&vftr_md_ctx[enable_idx], md_result_shm, ctx, obj_list);
	pthread_mutex_unlock(&ctx->cb_lock);

	getMdResOffset(idx, md_result_shm);

	return 0;
}

/**
 * @brief Get predefined metadata format.
 * @param[in]  src_idx         mpi win index of source window
 * @param[in]  dst_idx         mpi win index of destination window
 * @param[in]  src_rect        source window
 * @param[in]  dst_rect        destination window
 * @param[in] str     metadata string buffer.
 * @see AVFTR_MD_getRes()
 * @retval length of metadata string.
 * @retval 0                 do nothing.
 * @retval ENODEV            idx is not registered
 */
int AVFTR_MD_transRes(AVFTR_MD_CTX_S *vftr_md_ctx, MPI_WIN src_idx, MPI_WIN dst_idx, const MPI_RECT_S *src_rect,
                      const MPI_RECT_S *dst_rect, const MPI_RECT_S *src_roi, const MPI_RECT_S *dst_roi, char *str,
                      int buf_idx)
{
	int enable_idx = findMdCtx(src_idx, vftr_md_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		return 0;
	}

	if (vftr_md_ctx[enable_idx].en) {
		/* idx is registered and enable */
		return getMdMeta(src_idx, dst_idx, src_rect, dst_rect, src_roi, dst_roi,
		                 &vftr_md_ctx[enable_idx].md_res[buf_idx], str);
	}
	return 0;
}

/**
 * @brief Add motion detection instance.
 * @param[in]  idx         video window index.
 * @see AVFTR_MD_deleteInstance
 * @retval 0                 success.
 * @retval ENOMEM            No more space to register idx / malloc MD instance failed
 */
int AVFTR_MD_addInstance(MPI_WIN idx)
{
	AVFTR_MD_CTX_S *vftr_md_ctx = vftr_res_shm->md_ctx;
	int empty_idx;
	int set_idx = findMdCtx(idx, vftr_md_ctx, &empty_idx);

	if (set_idx >= 0) {
		/* idx is registered */
		SYS_TRACE("idx:%d is registerd\n", idx.value);
		return 0;
	} else if (set_idx < 0 && empty_idx >= 0) {
		/* idx is not registered yet but there is empty space to be registerd*/
		MD_CTX_S *ctx = MD_GET_CTX(empty_idx);
		ctx->instance = VFTR_MD_newInstance();
		if (!ctx->instance) {
			SYS_TRACE("Failed to create MD instance \n");
			return ENOMEM;
		}
		ctx->lock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
		ctx->cb_lock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;

		vftr_md_ctx[empty_idx].idx = idx;
		vftr_md_ctx[empty_idx].reg = 1;
		vftr_md_ctx[empty_idx].en = 0;

		pthread_mutex_lock(&ctx->cb_lock);
		vftr_md_ctx[empty_idx].cb = NULL;
		pthread_mutex_unlock(&ctx->cb_lock);

	} else {
		/* No more space to register idx */
		SYS_TRACE("add MD instance failed on win %u.\n", idx.win);
		return ENOMEM;
	}

	return 0;
}

/**
 * @brief Delete motion detection instance.
 * @param[in]  idx         video window index.
 * @see AVFTR_MD_addInstance
 * @retval 0                 success.
 * @retval EAGAIN            idx is enabled, not to remove
 */
int AVFTR_MD_deleteInstance(MPI_WIN idx)
{
	AVFTR_MD_CTX_S *vftr_md_ctx = vftr_res_shm->md_ctx;
	int enable_idx = findMdCtx(idx, vftr_md_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered */
		SYS_TRACE("idx:%d is not registered yet!\n", idx.value);
		return 0;
	}
	if (vftr_md_ctx[enable_idx].en) {
		/* idx is enabled */
		SYS_TRACE("idx:%d is still enable, can not be deleted!\n", idx.value);
		return EAGAIN;
	}

	INT32 ret = 0;
	MD_CTX_S *ctx = MD_GET_CTX(enable_idx);

	pthread_mutex_lock(&ctx->lock);
	ret = VFTR_MD_deleteInstance(&ctx->instance);
	pthread_mutex_unlock(&ctx->lock);

	if (ret != 0) {
		SYS_TRACE("Free md instance failed!\n");
		return ret;
	}
	vftr_md_ctx[enable_idx].reg = 0;
	vftr_md_ctx[enable_idx].en = 0;

	pthread_mutex_lock(&ctx->cb_lock);
	vftr_md_ctx[enable_idx].cb = NULL;
	pthread_mutex_unlock(&ctx->cb_lock);

	return 0;
}

/**
 * @brief Enable motion detection.
 * @param[in]  idx         video window index.
 * @see AVFTR_MD_disable
 * @retval 0                 success.
 * @retval ENODEV            idx is not registered yet
 */
int AVFTR_MD_enable(MPI_WIN idx)
{
	AVFTR_MD_CTX_S *vftr_md_ctx = vftr_res_shm->md_ctx;
	int enable_idx = findMdCtx(idx, vftr_md_ctx, NULL);

	if (enable_idx < 0) {
		/* idx is not registered */
		SYS_TRACE("idx:%d is not registered yet!\n", idx.value);
		return ENODEV;
	}

	if (vftr_md_ctx[enable_idx].en) {
		/* idx is enabled */
		SYS_TRACE("idx:%d is enabled, no need to enable again!\n", idx.value);
		return 0;
	}

	int ret;
	MD_CTX_S *ctx = MD_GET_CTX(enable_idx);
	ret = VIDEO_FTR_enableOd_implicit(idx);
	if (ret != 0) {
		return ret;
	}

	pthread_mutex_lock(&ctx->cb_lock);
	if (vftr_md_ctx[enable_idx].cb == NULL) {
		//SYS_TRACE("Motion detection alarm callback function is not registered on win %d.\n", idx.win);
		vftr_md_ctx[enable_idx].cb = alarmEmptyCb;
	}
	pthread_mutex_unlock(&ctx->cb_lock);

	pthread_mutex_lock(&ctx->lock);
	ctx->param.duration = AVFTR_MD_ALRAM_BUF_DEFAULT * AVFTR_VIDEO_JIF_HZ;
	pthread_mutex_unlock(&ctx->lock);
	vftr_md_ctx[enable_idx].en = 1;

	return 0;
}

/**
 * @brief Disable motion detection.
 * @param[in]  idx         video window index.
 * @see AVFTR_MD_enable
 * @retval 0                 success.
 * @retval ENODEV            idx is not registered yet
 */
int AVFTR_MD_disable(MPI_WIN idx)
{
	AVFTR_MD_CTX_S *vftr_md_ctx = vftr_res_shm->md_ctx;

	int enable_idx = findMdCtx(idx, vftr_md_ctx, NULL);

	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registerd yet, no need to disable\n", idx.value);
		return ENODEV;
	}

	if (!vftr_md_ctx[enable_idx].en) {
		/* idx is not enabled */
		SYS_TRACE("idx:%d is not enabled yet, no need to disable\n", idx.value);
		return 0;
	}

	INT32 ret = 0;
	ret = VIDEO_FTR_disableOd_implicit(idx);
	if (ret != 0) {
		SYS_TRACE("Disable object detection on win %d failed!\n", idx.win);
		return ret;
	}
	vftr_md_ctx[enable_idx].en = 0;

	return 0;
}

/**
 * @brief Get parameters of motion detection.
 * @param[in]  idx        video window index.
 * @param[out] param      motion detection parameters.
 * @see AVFTR_MD_setParam, AVFTR_MD_writeParam
 * @retval 0              success.
 * @retval ENODEV         idx is not registered yet
 */
int AVFTR_MD_getParam(MPI_WIN idx, AVFTR_MD_PARAM_S *param)
{
	AVFTR_MD_CTX_S *vftr_md_ctx = vftr_res_shm->md_ctx;
	int enable_idx = findMdCtx(idx, vftr_md_ctx, NULL);

	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registered\n", idx.value);
		return ENODEV;
	}

	MD_CTX_S *ctx = MD_GET_CTX(enable_idx);

	int ret = 0;

	ret = checkMpiDevValid(idx);
	if (ret != 0) {
		return ret;
	}

	*param = ctx->param;

	return 0;
}

/**
 * @brief Set parameters of motion detection (reg_list is unchanged).
 * @param[in] idx                video window index.
 * @param[in] param           motion detection parameters.
 * @see AVFTR_MD_getParam, AVFTR_MD_writeParam
 * @retval 0              success.
 * @retval EFAULT         Pointer to MD parameter is NULL
 * @retval ENODEV         idx is not registered yet
 * @retval EINVAL         Invalid MD parameter
 */
int AVFTR_MD_setParam(MPI_WIN idx, const AVFTR_MD_PARAM_S *param)
{
	if (param == NULL) {
		SYS_TRACE("Pointer to the motion detection parameter should not be NULL.\n");
		return EFAULT;
	}

	AVFTR_MD_CTX_S *vftr_md_ctx = vftr_res_shm->md_ctx;
	int enable_idx = findMdCtx(idx, vftr_md_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registered\n", idx.value);
		return ENODEV;
	}

	MD_CTX_S *ctx = MD_GET_CTX(enable_idx);

	if (param->duration > (AVFTR_MD_ALARM_BUF_MAX * AVFTR_VIDEO_JIF_HZ)) {
		SYS_TRACE("Alarm buffer duration exceeds max threshold %d seconds.\n", AVFTR_MD_ALARM_BUF_MAX);
		return EINVAL;
	}

	pthread_mutex_lock(&ctx->lock);
	ctx->param.en_skip_shake = param->en_skip_shake;
	ctx->param.en_skip_pd = param->en_skip_pd;
	ctx->param.duration = param->duration;
	pthread_mutex_unlock(&ctx->lock);

	return 0;
}

/**
 * @brief Set parameters of motion detection (reg_list is unchanged).
 * @param[in] idx                video window index.
 * @param[in] param           motion detection parameters.
 * @see AVFTR_MD_getParam, AVFTR_MD_writeParam
 * @retval 0              success.
 * @retval EFAULT         Pointer to MD parameter is NULL
 * @retval ENODEV         idx is not registered yet
 * @retval EINVAL         Invalid MD parameter
 */
int AVFTR_MD_setParamWithROI(MPI_WIN idx, const AVFTR_MD_PARAM_S *param)
{
	if (param == NULL) {
		SYS_TRACE("Pointer to the motion detection parameter should not be NULL.\n");
		return EFAULT;
	}

	AVFTR_MD_CTX_S *vftr_md_ctx = vftr_res_shm->md_ctx;
	int enable_idx = findMdCtx(idx, vftr_md_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registered\n", idx.value);
		return ENODEV;
	}

	MD_CTX_S *ctx = MD_GET_CTX(enable_idx);

	if (param->duration > (AVFTR_MD_ALARM_BUF_MAX * AVFTR_VIDEO_JIF_HZ)) {
		SYS_TRACE("Alarm buffer duration exceeds max threshold %d seconds.\n", AVFTR_MD_ALARM_BUF_MAX);
		return EINVAL;
	}

	if (param->md_param.region_num >= VFTR_MD_MAX_REG_NUM) {
		SYS_TRACE("Exceed maximal motion detection ROI on win %d.\n", idx.win);
		return EINVAL;
	}

	MPI_SIZE_S res = {};
	int ret = getMpiSize(idx, &res);
	if (ret != 0) {
		return ret;
	}

	ret = VFTR_MD_checkParam(&param->md_param, &res);
	if (ret != 0) {
		return ret;
	}

	pthread_mutex_lock(&ctx->lock);
	ctx->param = *param;
	ctx->s_flag = 1;
	pthread_mutex_unlock(&ctx->lock);

	return 0;
}

/**
 * @brief Write parameters of motion detection to MD instance
 * @param[in] idx                video window index.
 * @see AVFTR_MD_getParam, AVFTR_MD_setParam
 * @retval 0              success.
 * @retval ENODEV         idx is not registered yet
 */
int AVFTR_MD_writeParam(MPI_WIN idx)
{
	AVFTR_MD_CTX_S *vftr_md_ctx = vftr_res_shm->md_ctx;
	int enable_idx = findMdCtx(idx, vftr_md_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		//SYS_TRACE("idx:%d is not registered\n", idx.value);
		return ENODEV;
	}

	MD_CTX_S *ctx = MD_GET_CTX(enable_idx);

	int ret = 0;

	AVFTR_MD_PARAM_S *param = &ctx->param;

	int s_flag = ctx->s_flag;
	if (s_flag == 1) {
		pthread_mutex_lock(&ctx->lock);
		ret = VFTR_MD_setParam(ctx->instance, &param->md_param);
		ctx->s_flag = 0;
		pthread_mutex_unlock(&ctx->lock);
		if (ret != 0) {
			return ret;
		}
	}

	return 0;
}

/**
 * @brief Set motion detection alarm buffer duration.
 * @param[in]  idx        video window index.
 * @param[in]  alarm_buf_duration    alarm buffer duration (unit: 10msecond).
 * @see AVFTR_MD_getAlarmBuff
 * @retval MPI_SUCCESS                 success.
 * @retval MPI_FAILURE                 unexpected fail.
 */
int AVFTR_MD_setAlarmBuff(MPI_WIN idx, uint32_t duration)
{
	AVFTR_MD_CTX_S *vftr_md_ctx = vftr_res_shm->md_ctx;
	MD_CTX_S *alarm_ctx;
	int enable_idx = findMdCtx(idx, vftr_md_ctx, NULL);

	if (enable_idx < 0) {
		//SYS_TRACE("Set Md alarm buff duration failed!\n");
		return 0;
	}

	if (duration > (AVFTR_MD_ALARM_BUF_MAX * AVFTR_VIDEO_JIF_HZ)) {
		SYS_TRACE("Alarm buffer duration exceeds max threshold %d seconds.\n", AVFTR_MD_ALARM_BUF_MAX);
		return 0;
	}

	alarm_ctx = MD_GET_CTX(enable_idx);

	pthread_mutex_lock(&alarm_ctx->lock);
	alarm_ctx->param.duration = duration;
	pthread_mutex_unlock(&alarm_ctx->lock);

	return 0;
}

/**
 * @brief Set motion detection alarm buffer duration.
 * @param[in]  idx        video window index.
 * @param[in]  alarm_buf_duration    alarm buffer duration (unit: seconds * AVFTR_VIDEO_JIF_HZ).
 * @see VIDEO_FTR_setMdAlarmBuff
 * @retval MPI_SUCCESS                 success.
 * @retval MPI_FAILURE                 unexpected fail.
 */
int AVFTR_MD_getAlarmBuff(MPI_WIN idx, uint32_t *duration)
{
	AVFTR_MD_CTX_S *vftr_md_ctx = vftr_res_shm->md_ctx;
	MD_CTX_S *alarm_ctx;
	int enable_idx = findMdCtx(idx, vftr_md_ctx, NULL);

	if (enable_idx < 0) {
		//SYS_TRACE("Get Md alarm buff duration failed!\n");
		return 0;
	}

	alarm_ctx = MD_GET_CTX(enable_idx);

	pthread_mutex_lock(&alarm_ctx->lock);
	*duration = alarm_ctx->param.duration;
	pthread_mutex_unlock(&alarm_ctx->lock);

	return 0;
}

/**
 * @brief Get region parameters of motion detection.
 * @param[in]  idx        video window index.
 * @param[in]  roi_idx    region index.
 * @param[out] roi        region parameters.
 * @see VIDEO_FTR_setMdRoi
 * @retval 0              success.
 * @retval EFAULT         pointer to MD ROI is NULL
 * @retval EINVAL         ROI index is out of range
 * @retval ENODEV         idx is not registered yet
 */
int AVFTR_MD_getRoi(MPI_WIN idx, uint8_t roi_idx, AVFTR_MD_REG_ATTR_S *roi)
{
	if (roi == NULL) {
		SYS_TRACE("Pointer to the motion detection ROI should not be NULL.\n");
		return EFAULT;
	}
	if (roi_idx < 1 || roi_idx > VFTR_MD_MAX_REG_NUM) {
		SYS_TRACE("Incorrect roi index.\n");
		return EINVAL;
	}

	int ret = 0;
	VFTR_MD_PARAM_S param = { 0 };

	AVFTR_MD_CTX_S *vftr_md_ctx = vftr_res_shm->md_ctx;

	int enable_idx = findMdCtx(idx, vftr_md_ctx, NULL);
	if (enable_idx < 0) {
		SYS_TRACE("MD on win %d is not enabled\n", idx.win);
		return ENODEV;
	}

	MD_CTX_S *ctx = MD_GET_CTX(enable_idx);

	param = ctx->param.md_param;

	if (ret != 0) {
		SYS_TRACE("MD_getParam on win %d failed.\n", idx.win);
		return ret;
	}

	UINT8 _roi_idx = vftr_md_ctx[enable_idx].roi_list[roi_idx - 1];
	if (_roi_idx == 0) {
		SYS_TRACE("ROI %d does not exist on win %d.\n", roi_idx, idx.win);
		return ENODEV;
	}

	*roi = param.attr[_roi_idx];
	return 0;
}

/**
 * @brief Set region parameters of motion detection.
 * @param[in]  idx        video window index.
 * @param[in]  roi_idx    region index.
 * @param[in]  roi        region parameters.
 * @see AVFTR_MD_getRoi
 * @retval 0              success.
 * @retval EFAULT         pointer to MD ROI is NULL
 * @retval EINVAL         ROI index is out of range
 * @retval ENODEV         idx is not registered yet
 */
int AVFTR_MD_setRoi(MPI_WIN idx, uint8_t roi_idx, const AVFTR_MD_REG_ATTR_S *roi)
{
	if (roi == NULL) {
		SYS_TRACE("Pointer to the motion detection ROI should not be NULL.\n");
		return EFAULT;
	}

	if (roi_idx < 1 || roi_idx > VFTR_MD_MAX_REG_NUM) {
		SYS_TRACE("Incorrect roi index.\n");
		return EINVAL;
	}

	AVFTR_MD_CTX_S *vftr_md_ctx = vftr_res_shm->md_ctx;
	int enable_idx = findMdCtx(idx, vftr_md_ctx, NULL);
	if (enable_idx < 0) {
		SYS_TRACE("MD on win %d is not enabled\n", idx.win);
		return ENODEV;
	}

	MD_CTX_S *ctx = MD_GET_CTX(enable_idx);
	VFTR_MD_PARAM_S param = { 0 };
	int ret;

	param = ctx->param.md_param;

	UINT8 _roi_idx = vftr_md_ctx[enable_idx].roi_list[roi_idx - 1];
	if (_roi_idx == 0) {
		SYS_TRACE("ROI %d does not exist on win %d.\n", roi_idx, idx.win);
		return ENODEV;
	}

	MPI_SIZE_S res = {};
	ret = getMpiSize(idx, &res);
	if (ret != 0) {
		return ret;
	}

	ret = VFTR_MD_checkParam(&param, &res);
	if (ret != 0) {
		return ret;
	}

	param.attr[_roi_idx] = *roi;
	pthread_mutex_lock(&ctx->lock);
	ctx->param.md_param = param;
	ctx->s_flag = 1;
	pthread_mutex_unlock(&ctx->lock);

	return 0;
}

/**
 * @brief Add a new region of motion detection.
 * @param[in]  idx        video window index.
 * @param[in]  roi        region parameters.
 * @param[out] roi_idx    region index.
 * @see VIDEO_FTR_rmMdRoi
 * @retval 0              success.
 * @retval EFAULT         pointer to MD ROI is NULL.
 * @retval EINVAL         Invalid parameter.
 */
int AVFTR_MD_addRoi(MPI_WIN idx, const AVFTR_MD_REG_ATTR_S *roi, uint8_t *roi_idx)
{
	if (roi == NULL) {
		SYS_TRACE("Pointer to the motion detection ROI should not be NULL.\n");
		return EFAULT;
	}

	if (roi_idx == NULL) {
		SYS_TRACE("Pointer to the motion detection ROI index should not be NULL.\n");
		return EINVAL;
	}

	AVFTR_MD_CTX_S *vftr_md_ctx = vftr_res_shm->md_ctx;
	int enable_idx = findMdCtx(idx, vftr_md_ctx, NULL);
	if (enable_idx < 0) {
		return 0;
	}

	MD_CTX_S *ctx = MD_GET_CTX(enable_idx);
	VFTR_MD_PARAM_S param = { 0 };
	int ret;
	int i;

	pthread_mutex_lock(&ctx->lock);
	param = ctx->param.md_param;
	pthread_mutex_unlock(&ctx->lock);

	if (param.region_num >= VFTR_MD_MAX_REG_NUM) {
		SYS_TRACE("Exceed maximal motion detection ROI on win %d.\n", idx.win);
		return EINVAL;
	}

	for (i = 0; i < VFTR_MD_MAX_REG_NUM; i++) {
		if (vftr_md_ctx[enable_idx].roi_list[i] == 0) {
			*roi_idx = i + 1;
			break;
		}
	}

	if (i == VFTR_MD_MAX_REG_NUM) {
		SYS_TRACE("ROI list does not have space on win %d.\n", idx.win);
		return EINVAL;
	}

	param.attr[param.region_num] = *roi;
	param.attr[param.region_num].id = *roi_idx;
	param.region_num++;
	vftr_md_ctx[enable_idx].roi_list[*roi_idx - 1] = param.region_num;

	MPI_SIZE_S res = {};
	ret = getMpiSize(idx, &res);
	if (ret != 0) {
		return ret;
	}

	ret = VFTR_MD_checkParam(&param, &res);
	if (ret != 0) {
		return ret;
	}

	pthread_mutex_lock(&ctx->lock);
	ctx->param.md_param = param;
	ctx->s_flag = 1;
	pthread_mutex_unlock(&ctx->lock);

	return 0;
}

/**
 * @brief Remove a region of motion detection.
 * @param[in]  idx        video window index.
 * @param[in]  roi_idx    region index.
 * @see VIDEO_FTR_addMdRoi
 * @retval 0              success.
 * @retval EFAULT         pointer to MD ROI is NULL
 * @retval EINVAL         ROI index is out of range
 * @retval ENODEV         idx is not registered yet
 */
int AVFTR_MD_rmRoi(MPI_WIN idx, uint8_t roi_idx)
{
	AVFTR_MD_CTX_S *vftr_md_ctx = vftr_res_shm->md_ctx;
	int enable_idx = findMdCtx(idx, vftr_md_ctx, NULL);
	if (enable_idx < 0) {
		SYS_TRACE("Remove MD Roi failed. MD on win %d is not enabled\n", idx.win);
		return ENODEV;
	}

	MD_CTX_S *ctx = MD_GET_CTX(enable_idx);
	VFTR_MD_PARAM_S param = { 0 };
	int ret;
	int i;

	if (roi_idx < 1 || roi_idx > VFTR_MD_MAX_REG_NUM) {
		SYS_TRACE("Incorrect roi index.\n");
		return EINVAL;
	}

	UINT8 _roi_idx = vftr_md_ctx[enable_idx].roi_list[roi_idx - 1];
	if (_roi_idx == 0) {
		SYS_TRACE("ROI %d does not exist on win %d.\n", roi_idx, idx.win);
		return ENODEV;
	}

	param = ctx->param.md_param;

	vftr_md_ctx[enable_idx].roi_list[roi_idx - 1] = 0;
	for (i = _roi_idx + 1; i < param.region_num; i++) {
		vftr_md_ctx[enable_idx].roi_list[param.attr[i].id - 1] = i;
		memcpy(&param.attr[i - 1], &param.attr[i], sizeof(VFTR_MD_REG_ATTR_S));
	}
	param.region_num--;

	MPI_SIZE_S res = {};
	ret = getMpiSize(idx, &res);
	if (ret != 0) {
		return ret;
	}

	ret = VFTR_MD_checkParam(&param, &res);
	if (ret != 0) {
		return ret;
	}

	pthread_mutex_lock(&ctx->lock);
	ctx->param.md_param = param;
	ctx->s_flag = 1;
	pthread_mutex_unlock(&ctx->lock);

	return 0;
}

/**
 * @brief Register alarm callback function of motion detection.
 * @param[in]  idx        video window index.
 * @param[in]  alarm_cb_fptr   function pointer of callback function.
 * @see none
 * @retval 0                           success.
 * @retval EFAULT                      pointer of alarm callback is NULL.
 * @retval ENODEV                      idx is not registered yet.     
 */
int AVFTR_MD_regCallback(MPI_WIN idx, const AVFTR_MD_ALARM_CB alarm_cb_fptr)
{
	if (alarm_cb_fptr == NULL) {
		SYS_TRACE("Pointer to motion detection alarm callback function should not be NULL.\n");
		return EFAULT;
	}

	AVFTR_MD_CTX_S *vftr_md_ctx = vftr_res_shm->md_ctx;
	int enable_idx = findMdCtx(idx, vftr_md_ctx, NULL);

	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registered\n", idx.value);
		return ENODEV;
	}

	MD_CTX_S *ctx = MD_GET_CTX(enable_idx);

	pthread_mutex_lock(&ctx->cb_lock);
	vftr_md_ctx[enable_idx].cb = alarm_cb_fptr;
	pthread_mutex_unlock(&ctx->cb_lock);

	return 0;
}

int AVFTR_MD_setSkipshakeStat(MPI_WIN idx, int en_skip_shake)
{
	AVFTR_MD_CTX_S *vftr_md_ctx = vftr_res_shm->md_ctx;
	int enable_idx = findMdCtx(idx, vftr_md_ctx, NULL);

	if (enable_idx < 0) {
		//SYS_TRACE("MD for the win %u is not found\n", idx.win);
		return 0;
	}

	g_md_ctx[enable_idx].param.en_skip_shake = en_skip_shake;
	return 0;
}

int AVFTR_MD_getSkipshakeStat(MPI_WIN idx, int *en_skip_shake)
{
	AVFTR_MD_CTX_S *vftr_md_ctx = vftr_res_shm->md_ctx;
	int enable_idx = findMdCtx(idx, vftr_md_ctx, NULL);

	if (enable_idx < 0) {
		//SYS_TRACE("MD for the win %u is not found\n", idx.win);
		return 0;
	}

	*en_skip_shake = g_md_ctx[enable_idx].param.en_skip_shake;
	return 0;
}
