/**
 * @cond
 *
 * code fragment skipped by Doxygen.
 */

#include "avftr_shd.h"

#include <pthread.h>

#include "mtk_common.h"

#include "avftr.h"

#define AVFTR_SHD_MAX_SUPPORT_NUM 1

typedef struct {
	AVFTR_SHD_PARAM_S param;
	AVFTR_SHD_LONGTERM_LIST_S lt_list;
	VFTR_SHD_INSTANCE_S *instance;
	int s_flag;
	pthread_mutex_t lock;
} SHD_CTX_S;

extern AVFTR_VIDEO_CTX_S *vftr_res_shm;

static SHD_CTX_S g_shd_ctx[AVFTR_SHD_MAX_SUPPORT_NUM] = { { { 0 } } };

/**
 * @brief Find CTX with specified WIN_IDX.
 * @details This function also sets empty_idx if target WIN_IDX is not found.
 */
static inline int findShdCtx(MPI_WIN idx, const AVFTR_SHD_CTX_S *ctx, int *empty)
{
	int find_idx = -1;
	int emp_idx = -1;
	int i = 0;

	if (empty == NULL) {
		emp_idx = -2;
	}

	for (i = 0; i < AVFTR_SHD_MAX_SUPPORT_NUM; ++i) {
		if (find_idx == -1 && ctx[i].idx.value == idx.value && ctx[i].reg) {
			find_idx = i;
		}
		if (emp_idx == -1 && !ctx[i].reg) {
			emp_idx = i;
		}
	}

	if (empty != NULL) {
		*empty = emp_idx;
	}

	return find_idx;
}

/**
 * @endcond
 */

/**
 * @brief Add shaking object detection object to target window
 * @param[in] idx    index of the video window to register SHD
 * @return The execution result.
 * @retval 0          success (or created already)
 * @retval -ENOMEM    memory not enough
 * @see AVFTR_SHD_deleteInstance()
 */
int AVFTR_SHD_addInstance(MPI_WIN idx)
{
	AVFTR_SHD_CTX_S *vftr_shd_ctx = vftr_res_shm->shd_ctx;
	SHD_CTX_S *ctx;
	int empty_idx;
	int set_idx;

	set_idx = findShdCtx(idx, vftr_shd_ctx, &empty_idx);
	if (set_idx >= 0) {
		SYS_TRACE("idx: %d is registered.\n", idx.value);
		return 0;
	}

	if (empty_idx >= 0) {
		ctx = &g_shd_ctx[empty_idx];
		ctx->instance = VFTR_SHD_newInstance();
		if (ctx->instance == NULL) {
			SYS_TRACE("Failed to create SHD instance.\n");
			return -ENOMEM;
		}
		ctx->s_flag = 0;
		ctx->lock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;

		vftr_shd_ctx[empty_idx] = (AVFTR_SHD_CTX_S){ .idx = idx, .reg = 1, .en = 0 };

		return 0;
	}

	return -ENOMEM;
}

/**
 * @brief Delete shaking object detection instance from target window
 * @param[in] idx    index of the video window to deregister SHD
 * @return The execution result.
 * @retval 0          success (or not exist before)
 * @retval -EAGAIN    SHD is enabling
 */
int AVFTR_SHD_deleteInstance(MPI_WIN idx)
{
	AVFTR_SHD_CTX_S *vftr_shd_ctx = vftr_res_shm->shd_ctx;
	SHD_CTX_S *ctx;
	int ret;
	int enable_idx;

	enable_idx = findShdCtx(idx, vftr_shd_ctx, NULL);
	if (enable_idx < 0) {
		SYS_TRACE("idx:%d is not registered yet!\n", idx.value);
		return 0;
	}

	if (vftr_shd_ctx[enable_idx].en) {
		SYS_TRACE("idx:%d is still enable, cannot be deleted!\n", idx.value);
		return -EAGAIN;
	}

	ctx = &g_shd_ctx[enable_idx];

	pthread_mutex_lock(&ctx->lock);
	ret = VFTR_SHD_deleteInstance(&ctx->instance);
	pthread_mutex_unlock(&ctx->lock);

	if (ret != 0) {
		SYS_TRACE("Free SHD obj failed.\n");
		return ret;
	}

	vftr_shd_ctx[enable_idx].reg = 0;
	vftr_shd_ctx[enable_idx].en = 0;

	return 0;
}

/**
 * @brief Get enable statue of shaking object detection.
 */
int AVFTR_SHD_getStat(MPI_WIN idx, const AVFTR_SHD_CTX_S *vftr_shd_ctx)
{
	if (vftr_shd_ctx == NULL) {
		SYS_TRACE("Input parameter cannot be NULL.\n");
		return -EFAULT;
	}

	int enable_idx = findShdCtx(idx, vftr_shd_ctx, NULL);

	return enable_idx < 0 ? 0 : vftr_shd_ctx[enable_idx].en;
}

/**
 * @brief Get detect result.
 */
int AVFTR_SHD_detectShake(MPI_WIN idx, const MPI_IVA_OBJ_LIST_S *obj_list, AVFTR_SHD_STATUS_S *status)
{
	AVFTR_SHD_CTX_S *vftr_shd_ctx = vftr_res_shm->shd_ctx;
	int get_idx = findShdCtx(idx, vftr_shd_ctx, NULL);
	SHD_CTX_S *ctx;
	int ret;

	if (get_idx < 0) {
		//SYS_TRACE("idx: %d is not registered.\n", idx.value);
		return -ENOENT;
	}

	ctx = &g_shd_ctx[get_idx];
	if (!vftr_shd_ctx[get_idx].en) {
		return -EAGAIN;
	}

	pthread_mutex_lock(&ctx->lock);
	ret = VFTR_SHD_detectShake(ctx->instance, obj_list, status);
	pthread_mutex_unlock(&ctx->lock);

	if (ret != 0) {
		return ret;
	}

	return 0;
}

int AVFTR_SHD_enable(MPI_WIN idx)
{
	AVFTR_SHD_CTX_S *vftr_shd_ctx = vftr_res_shm->shd_ctx;
	int enable_idx = findShdCtx(idx, vftr_shd_ctx, NULL);

	if (enable_idx < 0) {
		SYS_TRACE("idx: %d is not registered yet!\n", idx.value);
		return -ENOENT;
	}

	if (vftr_shd_ctx[enable_idx].en) {
		return 0;
	}

	vftr_shd_ctx[enable_idx].en = 1;

	return 0;
}

int AVFTR_SHD_disable(MPI_WIN idx)
{
	AVFTR_SHD_CTX_S *vftr_shd_ctx = vftr_res_shm->shd_ctx;
	int enable_idx = findShdCtx(idx, vftr_shd_ctx, NULL);

	if (enable_idx < 0) {
		SYS_TRACE("idx: %d is not registered yet!\n", idx.value);
		return -ENOENT;
	}

	if (!vftr_shd_ctx[enable_idx].en) {
		return 0;
	}

	vftr_shd_ctx[enable_idx].en = 1;

	return 0;
}

int AVFTR_SHD_setParam(MPI_WIN idx, const AVFTR_SHD_PARAM_S *param)
{
	AVFTR_SHD_CTX_S *vftr_shd_ctx = vftr_res_shm->shd_ctx;
	int set_idx = findShdCtx(idx, vftr_shd_ctx, NULL);
	SHD_CTX_S *ctx;

	if (set_idx < 0) {
		SYS_TRACE("idx: %d is not registered.\n", idx.value);
		return -ENOENT;
	}

	if (param == NULL) {
		SYS_TRACE("Input parameter cannot be NULL.\n");
		return -EFAULT;
	}

	ctx = &g_shd_ctx[set_idx];

	pthread_mutex_lock(&ctx->lock);
	memcpy(&ctx->param, param, sizeof(AVFTR_SHD_PARAM_S));
	ctx->s_flag = 1;
	pthread_mutex_unlock(&ctx->lock);

	return 0;
}

int AVFTR_SHD_getParam(MPI_WIN idx, AVFTR_SHD_PARAM_S *param)
{
	AVFTR_SHD_CTX_S *vftr_shd_ctx = vftr_res_shm->shd_ctx;
	int get_idx = findShdCtx(idx, vftr_shd_ctx, NULL);
	SHD_CTX_S *ctx;

	if (get_idx < 0) {
		SYS_TRACE("idx: %d is not registered.\n", idx.value);
		return -ENOENT;
	}

	if (param == NULL) {
		SYS_TRACE("Input parameter cannot be NULL.\n");
		return -EFAULT;
	}

	ctx = &g_shd_ctx[get_idx];
	pthread_mutex_lock(&ctx->lock);
	memcpy(param, &ctx->param, sizeof(AVFTR_SHD_PARAM_S));
	pthread_mutex_unlock(&ctx->lock);

	return 0;
}

int AVFTR_SHD_writeParam(MPI_WIN idx)
{
	AVFTR_SHD_CTX_S *vftr_shd_ctx = vftr_res_shm->shd_ctx;
	int set_idx = findShdCtx(idx, vftr_shd_ctx, NULL);
	SHD_CTX_S *ctx;
	int ret = 0;

	if (set_idx < 0) {
		SYS_TRACE("idx: %d is not registered.\n", idx.value);
		return -ENOENT;
	}

	ctx = &g_shd_ctx[set_idx];
	if (ctx->s_flag == 1) {
		pthread_mutex_lock(&ctx->lock);
		ret = VFTR_SHD_setParam(ctx->instance, &ctx->param);
		ctx->s_flag = 0;
		pthread_mutex_unlock(&ctx->lock);
	}

	return ret;
}

int AVFTR_SHD_setUsrList(MPI_WIN idx, const AVFTR_SHD_LONGTERM_LIST_S *lt_list)
{
	AVFTR_SHD_CTX_S *vftr_shd_ctx = vftr_res_shm->shd_ctx;
	int set_idx = findShdCtx(idx, vftr_shd_ctx, NULL);
	SHD_CTX_S *ctx;

	if (set_idx < 0) {
		SYS_TRACE("idx: %d is not registered.\n", idx.value);
		return -ENOENT;
	}

	if (lt_list == NULL) {
		SYS_TRACE("Input parameter cannot be NULL.\n");
		return -EFAULT;
	}

	ctx = &g_shd_ctx[set_idx];
	pthread_mutex_lock(&ctx->lock);
	memcpy(&ctx->lt_list, lt_list, sizeof(AVFTR_SHD_LONGTERM_LIST_S));
	pthread_mutex_unlock(&ctx->lock);

	return 0;
}

int AVFTR_SHD_getUsrList(MPI_WIN idx, AVFTR_SHD_LONGTERM_LIST_S *lt_list)
{
	AVFTR_SHD_CTX_S *vftr_shd_ctx = vftr_res_shm->shd_ctx;
	int get_idx = findShdCtx(idx, vftr_shd_ctx, NULL);
	SHD_CTX_S *ctx;

	if (get_idx < 0) {
		SYS_TRACE("idx: %d is not registered.\n", idx.value);
		return -ENOENT;
	}

	if (lt_list == NULL) {
		SYS_TRACE("Input parameter cannot be NULL.\n");
		return -EFAULT;
	}

	ctx = &g_shd_ctx[get_idx];
	pthread_mutex_lock(&ctx->lock);
	memcpy(lt_list, &ctx->lt_list, sizeof(AVFTR_SHD_LONGTERM_LIST_S));
	pthread_mutex_unlock(&ctx->lock);

	return 0;
}

int AVFTR_SHD_writeUsrList(MPI_WIN idx)
{
	AVFTR_SHD_CTX_S *vftr_shd_ctx = vftr_res_shm->shd_ctx;
	int set_idx = findShdCtx(idx, vftr_shd_ctx, NULL);
	SHD_CTX_S *ctx;
	int ret = 0;

	if (set_idx < 0) {
		SYS_TRACE("idx: %d is not registered.\n", idx.value);
		return -ENOENT;
	}

	ctx = &g_shd_ctx[set_idx];
	pthread_mutex_lock(&ctx->lock);
	if ((ret = VFTR_SHD_setUserLongTermList(g_shd_ctx[set_idx].instance, &ctx->lt_list))) {
		SYS_TRACE("VFTR_SHD_setUserLongTermList failed. err: %d.\n", ret);
	}
	pthread_mutex_unlock(&ctx->lock);

	return ret;
}
