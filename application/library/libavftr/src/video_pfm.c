/**
* @cond
*
* code fragment skipped by Doxygen.
*/

#include <stdio.h>
#include <pthread.h>
#include <time.h>

#include "mtk_common.h"
#include "mpi_dev.h"

#include "video_pfm.h"
#include "video_od.h"
#include "avftr.h"

//#define VFTR_PFM_API_DEBUG
#ifdef VFTR_PFM_API_DEBUG
#define VFTR_PFM_DEBUG(fmt, args...) printf("[%s:%d] " fmt, __func__, __LINE__, ##args)
#else
#define VFTR_PFM_DEBUG(fmt, args...)
#endif

extern AVFTR_VIDEO_CTX_S *vftr_res_shm;
#define VFTR_PFM_TIME_INIT 999999

typedef struct {
	uint32_t set_time; /**< Param to store alarm start time */
	uint32_t hist_time;
	VIDEO_FTR_PFM_PARAM_S param;
	pthread_mutex_t lock;
} PFM_CTX_S;

static PFM_CTX_S g_pfm_ctx[VIDEO_PFM_MAX_SUPPORT_NUM] = { { 0 } };

//static inline PFM_CTX_S *GET_PFM_CTX(int idx) { return &g_pfm_ctx[idx];}
#define GET_PFM_CTX(idx) (&g_pfm_ctx[idx])

static inline int align16(int x)
{
	return ((x / 16) * 16);
}

static int findPfmCtx(MPI_WIN idx, VIDEO_PFM_CTX_S *ctx, int *empty)
{
	int i = 0;
	int find_idx = -1;
	int emp_idx = -1;

	if (empty == NULL) {
		emp_idx = -2;
	} else {
		emp_idx = -1;
	}

	for (i = 0; i < VIDEO_PFM_MAX_SUPPORT_NUM; i++) {
		if (find_idx == -1 && ctx[i].idx.value == idx.value) {
			find_idx = i;
		} else if (emp_idx == -1 && !ctx[i].en) {
			emp_idx = i;
		}
	}

	if (empty != NULL) {
		*empty = emp_idx;
	}

	return find_idx;
}

/**
 * @brief Invoke callback function when alarm condition is satisfied.
 * @param[in] idx         video window index.
 * @param[in] pfm_stat     pet feeding monitor result.
 * @see VIDEO_FTR_getPfmRes()
 * @retval none.
 */
static void genPfmEvt(VIDEO_PFM_CTX_S *pfm_ctx, const VIDEO_PFM_DATA_S *pfm_result)
{
	return;
	//FIXME: add pfm_ctx->cb() if alarm is needed
}

static void addPfmResOffset(MPI_WIN idx, MPI_RECT_POINT_S *roi)
{
	uint32_t x = 0;
	uint32_t y = 0;
	uint8_t i;
	MPI_CHN_LAYOUT_S layout_attr;
	MPI_CHN chn = MPI_VIDEO_CHN(idx.dev, idx.chn);
	if (MPI_DEV_getChnLayout(chn, &layout_attr) < 0) {
		SYS_TRACE("Cannot get channel layout for chn:%d\n", chn.chn);
		return;
	}
	for (i = 0; i < layout_attr.window_num; i++) {
		if (idx.value == layout_attr.win_id[i].value) {
			break;
		}
	}
	if (i == layout_attr.window_num) {
		SYS_TRACE("Window %d does not exist in channel %d\n", idx.win, idx.chn);
		return;
	}

	x = layout_attr.window[i].x;
	y = layout_attr.window[i].y;
	roi->sx += x;
	roi->sy += y;
	roi->ex += x;
	roi->ey += y;
}

/**
 * @brief Empty callback function for initialization.
 * @param[in] none.
 * @see VIDEO_FTR_enablePfm()
 * @retval none.
 */
static void alarmEmptyCb()
{
	//SYS_TRACE("Please registrate pet feeding monitor alarm callback function.\n");
	return;
}

static inline uint32_t getCurrentTime(void)
{
	time_t linux_time = time(NULL);
	struct tm *now;
	now = localtime(&linux_time);
	return (uint32_t)now->tm_hour * 3600 + now->tm_min * 60 + now->tm_sec;
}

static int checkPfmSchedule(MPI_WIN idx, PFM_CTX_S *ctx, uint32_t timestamp)
{
	int32_t regis_time, feeding_time;
	const VIDEO_PFM_SCHEDULE_S *schedule = &ctx->param.schedule;
	int i, ret = 0;
	int cur_time;
	if (ctx->hist_time == VFTR_PFM_TIME_INIT) {
		ctx->hist_time = timestamp;
	} else {
		if ((ctx->hist_time / AVFTR_VIDEO_JIF_HZ) == (timestamp / AVFTR_VIDEO_JIF_HZ)) {
			return 0;
		}
	}

	if (schedule->time_num == 0)
		return 0;

	cur_time = getCurrentTime();

	//VFTR_PFM_DEBUG(" set:%u cur:%u schedule:num:%d [%u,%u,..]\n",
	//*set_time, cur_time, schedule->time_num, schedule->times[0], schedule->times[1]);

	if (ctx->set_time == cur_time)
		return 0;

	/* TODO: dealing with newday issue */
	for (i = 0; i < schedule->time_num && i < VIDEO_PFM_SCHEDULE_MAX_NUM; i++) {
		regis_time = (schedule->times[i] - schedule->regisBg_feed_interval);
		regis_time = (regis_time >= 0) ? regis_time : (VIDEO_PFM_SEC_PER_DAY + regis_time);
		feeding_time = schedule->times[i] - 1;
		feeding_time = (feeding_time >= 0) ? feeding_time : (VIDEO_PFM_SEC_PER_DAY + feeding_time);
		if (regis_time && cur_time == regis_time) {
			ret = VIDEO_FTR_resetPfmData(idx, PFM_REGIS_ENVIR);
			if (ret) {
				SYS_TRACE("Failed to register Pfm Background Data\n");
				return ret;
			}
		}
		if (cur_time == feeding_time) {
			ret = VIDEO_FTR_resetPfmData(idx, PFM_REGIS_FEED);
			if (ret) {
				SYS_TRACE("Failed to register Pfm Feeding information\n");
				return ret;
			}
		}
	}
	ctx->set_time = cur_time;
	ctx->hist_time = timestamp;
	return 0;
}

static int checkVftrPfmParam(const VIDEO_FTR_PFM_PARAM_S *param)
{
	const VIDEO_PFM_SCHEDULE_S *schedule = &param->schedule;
	int i;
	if (schedule->time_num >= VIDEO_PFM_SCHEDULE_MAX_NUM) {
		SYS_TRACE("[PFM] Exceed maximum schedule number(%d) vs expected(%d)!\n", VIDEO_PFM_SCHEDULE_MAX_NUM,
		          schedule->time_num);
		return -1;
	}
	for (i = 0; i < schedule->time_num; i++) {
		if (schedule->times[i] > VIDEO_PFM_SEC_PER_DAY - 1) {
			SYS_TRACE("[PFM] Exceed maximum seconds per day(%d) vs expected(%d)!\n",
			          VIDEO_PFM_SEC_PER_DAY - 1, schedule->times[i]);
			return -1;
		}
	}
	if (schedule->regisBg_feed_interval > VIDEO_PFM_REGIS_INTERVAL_MAX) {
		SYS_TRACE("[PFM] Exceed maximum registration-feeding interval(%d) vs expected(%d)!\n",
		          VIDEO_PFM_REGIS_INTERVAL_MAX, schedule->regisBg_feed_interval);
	}
	return 0;
}

/**
 * @brief Get predefined metadata format for Multiplayer.
 * @param[in]  src_idx         mpi win index of source window
 * @param[in]  dst_idx         mpi win index of destination window
 * @param[in]  src_rect        source window
 * @param[in]  dst_rect        destination window
 * @param[in] res         Pet feeding monitor result.
 * @param[in] str         metadata string buffer.
 * @see VIDEO_FTR_getLdRes()
 * @retval length of metadata.
 */
static int getPfmMeta(MPI_WIN src_idx, MPI_WIN dst_idx, const MPI_RECT_S *src_rect, const MPI_RECT_S *dst_rect,
                      const MPI_RECT_S *src_roi, const MPI_RECT_S *dst_roi, const VIDEO_PFM_DATA_S *res, char *str)
{
	int offset = 0;

	MPI_RECT_POINT_S roi = res->roi;

	if (src_idx.value != dst_idx.value) {
		rescaleMpiRectPoint(src_rect, dst_rect, src_roi, dst_roi, &roi);
	}

	offset += sprintf(&str[offset],
#ifdef IVA_FORMAT_XML
	                  "<PFM><ROI RECT=\"%d %d %d %d\" EVT=\"%d\" RMDR=\"%d\" /></PFM>"
#else /* IVA_FORMAT_JSON */
	                  "\"pfm\":{\"roi\":{\"rect\":[%d,%d,%d,%d],\"evt\":%d},\"rmdr\":%d},"
#endif /*! !IVA_FORMAT_XML */
	                  ,
	                  roi.sx, roi.sy, roi.ex, roi.ey, res->data.evt, res->data.remainder);
	return offset;
}

/**
 * @endcond
 */

/**
 * @brief Get enable status of pet feeding monitor.
 * @param[in] idx     video window index.
 * @see none
 * @retval enable status of pet feeding monitor.
 */
int VIDEO_FTR_getPfmStat(MPI_WIN idx, VIDEO_PFM_CTX_S *vftr_pfm_ctx)
{
#ifdef REFACTOR_DONE
	int enable_idx = findPfmCtx(idx, vftr_pfm_ctx, NULL);

	return enable_idx < 0 ? 0 : vftr_pfm_ctx[enable_idx].en;
#endif
	return 0;
}

/**
 * @brief Get results of pet feeding monitor.
 * @param[in]  idx         video window index.
 * @param[in]  obj_list    object list.
 * @param[out] str         metadata string buffer.
 * @see none
 * @retval length of metadata.
 */
int VIDEO_FTR_getPfmRes(MPI_WIN idx, UINT32 timestamp, int buf_idx)
{
#ifdef REFACTOR_DONE
	VIDEO_PFM_CTX_S *vftr_pfm_ctx = vftr_res_shm->pfm_ctx;
	PFM_CTX_S *ctx = NULL;
	int enable_idx = findPfmCtx(idx, vftr_pfm_ctx, NULL);

	if (enable_idx < 0) {
		goto err;
	}

	if (vftr_pfm_ctx[enable_idx].en) {
		VIDEO_PFM_DATA_S *pfm_result = &vftr_pfm_ctx[enable_idx].stat[buf_idx];
		ctx = GET_PFM_CTX(enable_idx);
		PFM_PARAM_S p = { 0 };
		PFM_getData(idx, &pfm_result->data);
		PFM_getParam(idx, &p);
		pfm_result->roi = p.roi;
		genPfmEvt(&vftr_pfm_ctx[enable_idx], pfm_result);
		pthread_mutex_lock(&ctx->lock);
		checkPfmSchedule(idx, ctx, timestamp);
		pthread_mutex_unlock(&ctx->lock);
		addPfmResOffset(idx, &pfm_result->roi);
	}
	return MPI_SUCCESS;
err:
	return MPI_FAILURE;
#endif
	return 0;
}

/**
 * @brief Get predefined metadata format for Multiplayer.
 * @param[in] src_idx  mpi win index of source window
 * @param[in] dst_idx  mpi win index of destination window
 * @param[in] src_rect source window
 * @param[in] dst_rect destination window
 * @param[in] res         light detection result.
 * @param[in] str         metadata string buffer.
 * @see VIDEO_FTR_getLdRes()
 * @retval length of metadata.
 */
int VIDEO_FTR_transPfmRes(VIDEO_PFM_CTX_S *vftr_pfm_ctx, MPI_WIN src_idx, MPI_WIN dst_idx, const MPI_RECT_S *src_rect,
                          const MPI_RECT_S *dst_rect, const MPI_RECT_S *src_roi, const MPI_RECT_S *dst_roi, char *str, int buf_idx)
{
#ifdef REFACTOR_DONE
	int enable_idx = findPfmCtx(src_idx, vftr_pfm_ctx, NULL);
	if (enable_idx < 0)
		return 0;
	if (vftr_pfm_ctx[enable_idx].en)
		return getPfmMeta(src_idx, dst_idx, src_rect, dst_rect, src_roi, dst_roi, &vftr_pfm_ctx[enable_idx].stat[buf_idx], str);
	return 0;
#endif
	return 0;
}

/**
 * @brief Enable pet feeding monitor.
 * @param[in]  idx            video window index.
 * @see VIDEO_FTR_disablePfm
 * @retval MPI_SUCCESS        success.
 * @retval MPI_FAILURE        unexpected fail.
 */
int VIDEO_FTR_enablePfm(MPI_WIN idx)
{
#ifdef REFACTOR_DONE
	VFTR_PFM_DEBUG("%s\n", "enter");
	VIDEO_PFM_CTX_S *vftr_pfm_ctx = vftr_res_shm->pfm_ctx;
	PFM_CTX_S *pfm_ctx = NULL;
	int empty_idx;
	int set_idx = findPfmCtx(idx, vftr_pfm_ctx, &empty_idx);
	int enable_idx;
	int ret;
	int i;

	if (set_idx >= 0) {
		enable_idx = set_idx;
	} else if (empty_idx >= 0) {
		enable_idx = empty_idx;
		vftr_pfm_ctx[enable_idx].idx.value = idx.value;
		pfm_ctx = GET_PFM_CTX(enable_idx);
		pfm_ctx->lock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
		pfm_ctx->hist_time = VFTR_PFM_TIME_INIT;
	} else {
		SYS_TRACE("PFM detection enable failed on win %u.\n", idx.win);
		goto err;
	}

	if (!vftr_pfm_ctx[enable_idx].en) {
		ret = vftrYAvgResDec();
		if (ret != 0) {
			SYS_TRACE("[WARN] All MPI YAVG ROI Resource are being used !\n");
			goto err;
		}

		ret = VIDEO_FTR_enableOd_implicit(idx);
		if (ret != MPI_SUCCESS) {
			vftrYAvgResInc();
			goto err;
		}
		if (vftr_pfm_ctx[enable_idx].cb == NULL) {
			//SYS_TRACE("Pet feeding monitor alarm callback function is not registered on win %u.\n",
			//          idx.win);
			vftr_pfm_ctx[enable_idx].cb = alarmEmptyCb;
		}
		MPI_CHN_LAYOUT_S layout_attr;
		MPI_CHN chn_id = MPI_VIDEO_CHN(idx.dev, idx.chn);
		if (MPI_DEV_getChnLayout(chn_id, &layout_attr) != MPI_SUCCESS) {
			SYS_TRACE("Pet feeding monitor get attribues on win %u failed.\n", chn_id.chn);
			goto err;
		}
		for (i = 0; i < layout_attr.window_num; i++) {
			if (idx.value == layout_attr.win_id[i].value) {
				break;
			}
		}
		if (i == layout_attr.window_num) {
			SYS_TRACE("Window %d does not exist in channel %d\n", idx.win, idx.chn);
			goto err;
		}
		ret = PFM_enable(idx);
		if (ret != MPI_SUCCESS) {
			SYS_TRACE("Enable pet feeding monitor on win %u failed.\n", idx.win);
			goto err;
		}
		if (vftr_pfm_ctx[enable_idx].cb == NULL) {
			//SYS_TRACE("Pet feeding monitor event callback function is not registered on win %u.\n",
			//          idx.win);
			vftr_pfm_ctx[enable_idx].cb = alarmEmptyCb;
		}
		vftr_pfm_ctx[enable_idx].en = 1;
	}
	VFTR_PFM_DEBUG("%s\n", "exit");
	return MPI_SUCCESS;
err:
	return MPI_FAILURE;
#endif
	return 0;
}

/**
 * @brief Disable pet feeding monitor.
 * @param[in]  idx        video window index.
 * @see VIDEO_FTR_enablePfm
 * @retval MPI_SUCCESS                 success.
 * @retval MPI_FAILURE                 unexpected fail.
 */
int VIDEO_FTR_disablePfm(MPI_WIN idx)
{
#ifdef REFACTOR_DONE
	VFTR_PFM_DEBUG("%s\n", "enter");
	VIDEO_PFM_CTX_S *vftr_pfm_ctx = vftr_res_shm->pfm_ctx;

	int enable_idx = findPfmCtx(idx, vftr_pfm_ctx, NULL);

	if (enable_idx < 0) {
		return MPI_SUCCESS;
	}

	if (vftr_pfm_ctx[enable_idx].en) {
		INT32 ret;

		vftrYAvgResInc();

		ret = VIDEO_FTR_disableOd_implicit(idx);
		if (ret != MPI_SUCCESS) {
			return MPI_FAILURE;
		}
		ret = PFM_disable(idx);
		if (ret != MPI_SUCCESS) {
			return MPI_FAILURE;
		}
		vftr_pfm_ctx[enable_idx].en = 0;
	}
	VFTR_PFM_DEBUG("%s\n", "exit");
	return MPI_SUCCESS;
#endif
	return 0;
}

/**
 * @brief Get parameters of pet feeding monitor.
 * @param[in]  idx        video window index.
 * @param[out] param      pet feeding monitor parameters.
 * @see VIDEO_FTR_setPfmParam
 * @retval MPI_SUCCESS             success.
 * @retval MPI_ERR_NULL_POINTER    input pointer is NULL.
 * @retval MPI_ERR_DEV_INVALID_WIN invalid video window index.
 * @retval MPI_ERR_DEV_INVALID_CHN invalid video channel index.
 * @retval MPI_ERR_DEV_INVALID_DEV invalid device index.
 * @retval MPI_ERR_NOT_EXIST       device/channel doesn't exist.
 * @retval MPI_FAILURE             unexpected fail.
 */
int VIDEO_FTR_getPfmParam(MPI_WIN idx, VIDEO_FTR_PFM_PARAM_S *param)
{
#ifdef REFACTOR_DONE
	VFTR_PFM_DEBUG("%s\n", "enter");
	VIDEO_PFM_CTX_S *vftr_pfm_ctx = vftr_res_shm->pfm_ctx;
	PFM_CTX_S *ctx = NULL;
	int empty_idx;
	int set_idx = findPfmCtx(idx, vftr_pfm_ctx, &empty_idx);
	if (set_idx < 0) {
		//SYS_TRACE("Get Pfm param failed!\n");
		return MPI_SUCCESS;
	}

	ctx = GET_PFM_CTX(set_idx);
	int ret = PFM_getParam(idx, &param->pfm_param);
	if (ret != MPI_SUCCESS) {
		return ret;
	}
	pthread_mutex_lock(&ctx->lock);
	param->schedule = ctx->param.schedule;
	pthread_mutex_unlock(&ctx->lock);
	VFTR_PFM_DEBUG("%s\n", "exit");
	return ret;
#endif
	return 0;
}

/**
 * @brief Set parameters of pet feeding monitor.
 * @param[in] idx          video window index.
 * @param[in] param        pet feeding monitor parameters.
 * @see VIDEO_FTR_getPfmParam
 * @retval MPI_SUCCESS             success.
 * @retval MPI_ERR_NULL_POINTER    input pointer is NULL.
 * @retval MPI_ERR_DEV_INVALID_WIN invalid video window index.
 * @retval MPI_ERR_DEV_INVALID_CHN invalid video channel index.
 * @retval MPI_ERR_DEV_INVALID_DEV invalid device index.
 * @retval MPI_ERR_NOT_EXIST       device/channel doesn't exist.
 * @retval MPI_ERR_INVALID_PARAM   invalid parameters.
 * @retval MPI_FAILURE             unexpected fail.
 */
int VIDEO_FTR_setPfmParam(MPI_WIN idx, const VIDEO_FTR_PFM_PARAM_S *param)
{
#ifdef REFACTOR_DONE
	VIDEO_PFM_CTX_S *vftr_pfm_ctx = vftr_res_shm->pfm_ctx;
	PFM_CTX_S *ctx = NULL;
	int empty_idx;
	int set_idx = findPfmCtx(idx, vftr_pfm_ctx, &empty_idx);
	int ret = 0;
	VFTR_PFM_DEBUG("%s sens:%d endur:%d roi:[%d,%d,%d,%d] interval:%d num:%d schedule:[%d,%d,...]\n", "enter",
	               param->pfm_param.sensitivity, param->pfm_param.endurance, param->pfm_param.roi.sx,
	               param->pfm_param.roi.sy, param->pfm_param.roi.ex, param->pfm_param.roi.ey,
	               param->schedule.regisBg_feed_interval, param->schedule.time_num, param->schedule.times[0],
	               param->schedule.times[1]);

	/* FIXME: if case when both smaller than 0 */
	if (set_idx < 0 && empty_idx >= 0) {
		vftr_pfm_ctx[empty_idx].idx.value = idx.value;
		set_idx = empty_idx;
		ctx = GET_PFM_CTX(set_idx);
		ctx->lock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
	} else if (set_idx < 0 && empty_idx < 0) {
		SYS_TRACE("Cannot Set Pfm Param for window index:0x%x\n", idx.value);
		return 0;
	} else {
		ctx = GET_PFM_CTX(set_idx);
	}

	ret = checkVftrPfmParam(param);
	if (ret) {
		return -1;
	}
	ret = PFM_setParam(idx, &param->pfm_param);
	if (ret) {
		SYS_TRACE("[PFM] Failed to set PFM parameters\n");
		return -1;
	}
	pthread_mutex_lock(&ctx->lock);
	ctx->set_time = getCurrentTime();
	ctx->hist_time = VFTR_PFM_TIME_INIT;
	memcpy(&ctx->param, param, sizeof(VIDEO_FTR_PFM_PARAM_S));
	pthread_mutex_unlock(&ctx->lock);
	VFTR_PFM_DEBUG("%s\n", "exit");
	return 0;
#endif
	return 0;
}

/**
 * @brief Register alarm callback function of pet feeding monitor.
 * @param[in] idx              video window index.
 * @param[in]  alarm_cb_fptr   function pointer of callback function.
 * @see none
 * @retval MPI_SUCCESS          success.
 * @retval MPI_FAILURE          unexpected fail.
 */
int VIDEO_FTR_regPfmCallback(MPI_WIN idx, const VIDEO_FTR_PFM_ALARM_CB alarm_cb_fptr)
{
#ifdef REFACTOR_DONE
	VFTR_PFM_DEBUG("%s\n", "enter");
	if (alarm_cb_fptr == NULL) {
		SYS_TRACE("Pointer to pet feeding monitor alarm callback function should not be NULL.\n");
		return MPI_FAILURE;
	}

	VIDEO_PFM_CTX_S *vftr_pfm_ctx = vftr_res_shm->pfm_ctx;
	PFM_CTX_S *ctx = NULL;
	int empty_idx, enable_idx;
	int set_idx = findPfmCtx(idx, vftr_pfm_ctx, &empty_idx);

	if (set_idx >= 0) {
		enable_idx = set_idx;
	} else if (empty_idx >= 0) {
		enable_idx = empty_idx;
		vftr_pfm_ctx[empty_idx].idx.value = idx.value;
		ctx = GET_PFM_CTX(empty_idx);
		ctx->lock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
	} else {
		SYS_TRACE("No available seting of PFM on the win %u.\n", idx.win);
		goto err;
	}

	vftr_pfm_ctx[enable_idx].cb = alarm_cb_fptr;
	VFTR_PFM_DEBUG("%s\n", "exit");
	return MPI_SUCCESS;
err:
	return MPI_FAILURE;
#endif
	return 0;
}

/**
 * @brief Reset registered model of pet feeding monitoring.
 * @param[in]  idx             video window index.
 * @see none
 * @retval MPI_SUCCESS              success.
 * @retval MPI_ERR_DEV_INVALID_WIN  invalid video window index.
 * @retval MPI_ERR_DEV_INVALID_CHN  invalid video channel index.
 * @retval MPI_ERR_DEV_INVALID_DEV  invalid device index.
 * @retval MPI_ERR_NOT_EXIST        device/channel doesn't exist.
 * @retval MPI_ERR_NOT_PERM         pet feeding monitoring hasn't been enabled.
 * @retval MPI_FAILURE              unexpected fail.
 */
int VIDEO_FTR_resetPfmData(MPI_WIN idx, PFM_RESET_E reset_val)
{
#ifdef REFACTOR_DONE
	return PFM_resetData(idx, reset_val);
#endif
	return 0;
}

/**
 * @brief Reset registered model of pet feeding monitoring.
 * @param[in]  idx             video window index.
 * @see none
 * @retval MPI_SUCCESS              success.
 * @retval MPI_ERR_DEV_INVALID_WIN  invalid video window index.
 * @retval MPI_ERR_DEV_INVALID_CHN  invalid video channel index.
 * @retval MPI_ERR_DEV_INVALID_DEV  invalid device index.
 * @retval MPI_ERR_NOT_EXIST        device/channel doesn't exist.
 * @retval MPI_ERR_NOT_PERM         pet feeding monitoring hasn't been enabled.
 * @retval MPI_FAILURE              unexpected fail.
 */
int VIDEO_FTR_resetPfmShm(MPI_WIN idx)
{
#ifdef REFACTOR_DONE
	/*  FIXME: by win used
	int enable_idx = findTdCtx(idx, vftr_td_ctx, NULL);

	if (enable_idx < 0) {
		return -1;
	}

	if (vftr_td_ctx[enable_idx].en) {
		vftr_td_ctx[enable_idx].td_res.alarm = 0;
	}
	*/
	VFTR_PFM_DEBUG("%s\n", "enter");
	VIDEO_PFM_CTX_S *vftr_pfm_ctx = vftr_res_shm->pfm_ctx;
	AVFTR_VIDEO_BUF_INFO_S *buf_info = vftr_res_shm->buf_info;
	int i;

	for (i = 0; i < AVFTR_TD_MAX_SUPPORT_NUM; i++) {
		if (vftr_pfm_ctx[i].en) {
			vftr_pfm_ctx[i].stat[buf_info[i].buf_cur_idx].data.evt = 0;
		}
	}
	VFTR_PFM_DEBUG("%s\n", "exit");
	return 0;
#endif
	return 0;
}

int VIDEO_FTR_resumePfm(MPI_WIN idx)
{
#ifdef REFACTOR_DONE
	VIDEO_PFM_CTX_S *vftr_pfm_ctx = vftr_res_shm->pfm_ctx;

	/* FIXME: not support by win for notify
	int enable_idx = findTdCtx(idx, vftr_td_ctx, NULL);

	if (enable_idx < 0) {
		return -1;
	}

	if (vftr_pfm_ctx[enable_idx].en) {
		PFM_resetData(vftr_td_ctx[enable_idx].idx);
	}
	*/
	VFTR_PFM_DEBUG("%s\n", "enter");
	int i;
	for (i = 0; i < VIDEO_PFM_MAX_SUPPORT_NUM; i++) {
		if (vftr_pfm_ctx[i].en) {
			PFM_resetData(vftr_pfm_ctx[i].idx, PFM_REGIS_ENVIR);
		}
	}
	VFTR_PFM_DEBUG("%s\n", "exit");
	return 0;
#endif
	return 0;
}
