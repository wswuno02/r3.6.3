/**
* @cond
*
* code fragment skipped by Doxygen.
*/

#include <stdio.h>

#include "mtk_common.h"
#include "mpi_dev.h"

#include "avftr_ef.h"
#include "video_od.h"
#include "avftr.h"
#include "avftr_common.h"

extern AVFTR_VIDEO_CTX_S *vftr_res_shm;

typedef struct {
	VFTR_EF_PARAM_S param;
	VFTR_EF_INSTANCE_S *instance;
	int s_flag;
	pthread_mutex_t lock;
	pthread_mutex_t cb_lock;
} EF_CTX_S;

static EF_CTX_S g_ef_ctx[AVFTR_EF_MAX_SUPPORT_NUM] = { { { 0 } } };

#define EF_GET_CTX(idx) &g_ef_ctx[idx]

static int findEfCtx(MPI_WIN idx, AVFTR_EF_CTX_S *ctx, int *empty)
{
	int i = 0;
	int find_idx = -1;
	int emp_idx = -1;

	if (empty == NULL) {
		emp_idx = -2;
	} else {
		emp_idx = -1;
	}

	for (i = 0; i < AVFTR_EF_MAX_SUPPORT_NUM; i++) {
		if (find_idx == -1 && ctx[i].idx.value == idx.value && ctx[i].reg) {
			find_idx = i;
		} else if (emp_idx == -1 && !ctx[i].reg) {
			emp_idx = i;
		}
	}

	if (empty != NULL) {
		*empty = emp_idx;
	}

	return find_idx;
}

/**
 * @brief Invoke callback function when alarm condition is satisfied.
 * @param[in] vftr_ef_ctx         efence control ptr.
 * @param[in] list        electronic fence detected result.
 * @see VIDEO_FTR_getEfRes()
 * @retval none.
 */
static void genEfAlarm(AVFTR_EF_CTX_S *vftr_ef_ctx, const VFTR_EF_STATUS_S *status)
{
	if (vftr_ef_ctx->cb == NULL) {
		return;
	}
	vftr_ef_ctx->cb();
	return;
}

/**
 * @brief Get predefined metadata format for Multiplayer.
 * @param[in]  src_idx         mpi win index of source window
 * @param[in]  dst_idx         mpi win index of destination window
 * @param[in]  src_rect        source window
 * @param[in]  dst_rect        destination window
 * @param[in] list        electronic fence detected result.
 * @param[in] str         metadata string buffer.
 * @see VIDEO_FTR_getEfRes()
 * @retval length of metadata.
 */
static int getEfMeta(MPI_WIN src_idx, MPI_WIN dst_idx, const MPI_RECT_S *src_rect, const MPI_RECT_S *dst_rect,
                     const MPI_RECT_S *src_roi, const MPI_RECT_S *dst_roi, const VFTR_EF_STATUS_S *list, char *str)
{
	int offset = 0;
	int i = 0;
	VFTR_EF_STATUS_S dst_list;

	for (i = 0; i < list->fence_num; i++) {
		dst_list.attr[i].line = list->attr[i].line;
	}

	if (src_idx.value != dst_idx.value) {
		for (i = 0; i < list->fence_num; i++) {
			rescaleMpiRectPoint(src_rect, dst_rect, src_roi, dst_roi, &dst_list.attr[i].line);
		}
	}
#ifdef IVA_FORMAT_XML /* IVA_FORMAT_XML */
	offset += sprintf(&str[offset], "<EF>");
#else /* IVA_FORMAT_JSON */
	offset += sprintf(&str[offset], "\"ef\":[ ");
#endif /* !IVA_FORMAT_XML */
	for (i = 0; i < list->fence_num; i++) {
		offset += sprintf(&str[offset],
#ifdef IVA_FORMAT_XML /* IVA_FORMAT_XML */
		                  "<VL ID=\"%d\" LINE=\"%d %d %d %d\" MODE=\"%d\" "
		                  "ALARM=\"%d\" NAME=\"%s\" CNTP=\"%d\" CNTN=\"%d\"/>",
#else /* IVA_FORMAT_JSON */
		                  "{\"vl\":{\"id\":%d,\"line\":[%d,%d,%d,%d],\"mode\":%d,"
		                  "\"alarm\":%d,\"name\":\"%s\",\"cntp\":%d,\"cntn\":%d}},",
#endif /* !IVA_FORMAT_XML */
		                  list->attr[i].id, dst_list.attr[i].line.sx, dst_list.attr[i].line.sy,
		                  dst_list.attr[i].line.ex, dst_list.attr[i].line.ey, list->attr[i].mode,
		                  list->stat[i].alarm, "", /* FIXME: add name in fence struct */
		                  list->stat[i].cnt[0], list->stat[i].cnt[1]);
	}
#ifdef IVA_FORMAT_XML /* IVA_FORMAT_XML */
	offset += sprintf(&str[offset], "</EF>");
#else /* IVA_FORMAT_JSON */
	offset += (sprintf(&str[offset - 1], "],") - 1);
#endif /* !IVA_FORMAT_XML */
	return offset;
}

static void getEfResOffset(MPI_WIN idx, VFTR_EF_STATUS_S *list)
{
	uint32_t x = 0;
	uint32_t y = 0;
	int32_t i;
	MPI_CHN_LAYOUT_S layout_attr;
	MPI_CHN chn = MPI_VIDEO_CHN(idx.dev, idx.chn);
	if (MPI_DEV_getChnLayout(chn, &layout_attr) < 0) {
		SYS_TRACE("Cannot get channel layout for chn:%d\n", chn.chn);
		return;
	}
	for (i = 0; i < layout_attr.window_num; i++) {
		if (idx.value == layout_attr.win_id[i].value) {
			break;
		}
	}
	if (i == layout_attr.window_num) {
		SYS_TRACE("Window %d does not exist in channel %d\n", idx.win, idx.chn);
		return;
	}
	x = layout_attr.window[i].x;
	y = layout_attr.window[i].y;

	for (i = 0; i < list->fence_num; i++) {
		list->attr[i].line.sx += x;
		list->attr[i].line.sy += y;
		list->attr[i].line.ex += x;
		list->attr[i].line.ey += y;
	}
}

/**
 * @brief Empty callback function for initialization.
 * @param[in] none.
 * @see VIDEO_FTR_enableEf()
 * @retval none.
 */
static void alarmEmptyCb()
{
	//	SYS_TRACE("Please registrate electronic fence alarm callback function.\n");
	return;
}

/**
 * @endcond
 */

/**
 * @brief Get enable status of electronic fence.
 * @param[in]  idx     video window index.
 * @see none
 * @retval enable status of electronic fence.
 */
int AVFTR_EF_getStat(MPI_WIN idx, AVFTR_EF_CTX_S *vftr_ef_ctx)
{
	int enable_idx = findEfCtx(idx, vftr_ef_ctx, NULL);

	return enable_idx < 0 ? 0 : vftr_ef_ctx[enable_idx].en;
}

/**
 * @brief Get results of electronic fence.
 * @param[in]  idx     video window index.
 * @param[in]  obj_list    object list.
 * @see none
 * @retval length of metadata.
 */
int AVFTR_EF_getRes(MPI_WIN idx, const VIDEO_FTR_OBJ_LIST_S *obj_list, int buf_idx)
{
	AVFTR_EF_CTX_S *vftr_ef_ctx = vftr_res_shm->ef_ctx;

	int enable_idx = findEfCtx(idx, vftr_ef_ctx, NULL);
	int ret = 0;

	if (enable_idx < 0) {
		/* idx is not registered yet */
		//SYS_TRACE("idx:%d is not registered yet!\n",idx.value);
		return ENODEV;
	}

	if (!vftr_ef_ctx[enable_idx].en) {
		/* idx is not enabled yet */
		//SYS_TRACE("idx:%d is not enabled yet!\n",idx.value);
		return EAGAIN;
	}

	EF_CTX_S *ctx = EF_GET_CTX(enable_idx);
	if (ctx->instance == NULL) {
		SYS_TRACE("Ef instance is NULL!\n");
		return EFAULT;
	}

	VFTR_EF_STATUS_S *list = &vftr_ef_ctx[enable_idx].ef_res[buf_idx];

	pthread_mutex_lock(&ctx->lock);
	ret = VFTR_EF_detect(ctx->instance, &obj_list->basic_list, list);
	pthread_mutex_unlock(&ctx->lock);
	if (ret != 0) {
		SYS_TRACE("Failed to run ef detection!\n");
		return ret;
	}

	/* Invoke callback function when alarm condition is satisfied. */
	pthread_mutex_lock(&ctx->cb_lock);
	genEfAlarm(&vftr_ef_ctx[enable_idx], list);
	pthread_mutex_unlock(&ctx->cb_lock);
	getEfResOffset(idx, list);

	return 0;
}

/**
 * @brief Get predefined formated metadata result for Multiplayer.
 * @param[in]  vftr_aroi_ctx   automatic region of interest result.
 * @param[in] src_idx  mpi win index of source window
 * @param[in] dst_idx  mpi win index of destination window
 * @param[in] src_rect source window
 * @param[in] dst_rect destination window
 * @param[in] list        electronic fence detected result.
 * @param[in] str         formated IVA metadata string buffer.
 * @see VIDEO_FTR_getEfRes()
 * @retval length of formated metadata.
 */
int AVFTR_EF_transRes(AVFTR_EF_CTX_S *vftr_ef_ctx, MPI_WIN src_idx, MPI_WIN dst_idx, const MPI_RECT_S *src_rect,
                      const MPI_RECT_S *dst_rect, const MPI_RECT_S *src_roi, const MPI_RECT_S *dst_roi, char *str,
                      int buf_idx)
{
	int enable_idx = findEfCtx(src_idx, vftr_ef_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		return 0;
	}

	if (vftr_ef_ctx[enable_idx].en) {
		/* idx is registered and enable */
		return getEfMeta(src_idx, dst_idx, src_rect, dst_rect, src_roi, dst_roi,
		                 &vftr_ef_ctx[enable_idx].ef_res[buf_idx], str);
	}
	return 0;
}

/**
 * @brief Add ef instance.
 * @param[in]  idx         video window index.
 * @see AVFTR_EF_deleteInstance
 * @retval 0                 success.
 * @retval ENOMEM            No more space to register idx / malloc EF instance failed
 * @retval EFAULT            EF instance is NULL
 */
int AVFTR_EF_addInstance(MPI_WIN idx)
{
	AVFTR_EF_CTX_S *vftr_ef_ctx = vftr_res_shm->ef_ctx;
	int empty_idx;
	int set_idx = findEfCtx(idx, vftr_ef_ctx, &empty_idx);

	if (set_idx >= 0) {
		/* idx is registered */
		SYS_TRACE("idx:%d is registerd\n", idx.value);
		return 0;
	} else if (set_idx < 0 && empty_idx >= 0) {
		/* idx is not registered yet but there is empty space to be registerd*/
		EF_CTX_S *ctx = EF_GET_CTX(empty_idx);
		ctx->instance = VFTR_EF_newInstance();
		if (!ctx->instance) {
			SYS_TRACE("Failed to create EF instance \n");
			return ENOMEM;
		}
		ctx->lock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
		ctx->cb_lock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;

		vftr_ef_ctx[empty_idx].idx = idx;
		vftr_ef_ctx[empty_idx].reg = 1;
		vftr_ef_ctx[empty_idx].en = 0;

		pthread_mutex_lock(&ctx->cb_lock);
		vftr_ef_ctx[empty_idx].cb = NULL;
		pthread_mutex_unlock(&ctx->cb_lock);
	} else {
		/* No more space to register idx */
		SYS_TRACE("add EF instance failed on win %u.\n", idx.win);
		return ENOMEM;
	}

	return 0;
}

/**
 * @brief Delete ef instance.
 * @param[in]  idx         video window index.
 * @see AVFTR_EF_addInstance
 * @retval 0                 success.
 * @retval EAGAIN            idx is enabled, not to remove
 */
int AVFTR_EF_deleteInstance(MPI_WIN idx)
{
	AVFTR_EF_CTX_S *vftr_ef_ctx = vftr_res_shm->ef_ctx;
	int enable_idx = findEfCtx(idx, vftr_ef_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered */
		SYS_TRACE("idx:%d is not registered yet!\n", idx.value);
		return 0;
	}
	if (vftr_ef_ctx[enable_idx].en) {
		/* idx is enabled */
		SYS_TRACE("idx:%d is still enable, can not be deleted!\n", idx.value);
		return EAGAIN;
	}

	INT32 ret = 0;
	EF_CTX_S *ctx = EF_GET_CTX(enable_idx);

	pthread_mutex_lock(&ctx->lock);
	ret = VFTR_EF_deleteInstance(&ctx->instance);
	pthread_mutex_unlock(&ctx->lock);

	if (ret != 0) {
		SYS_TRACE("Free ef instance failed!\n");
		return ret;
	}
	vftr_ef_ctx[enable_idx].reg = 0;
	vftr_ef_ctx[enable_idx].en = 0;

	pthread_mutex_lock(&ctx->cb_lock);
	vftr_ef_ctx[enable_idx].cb = NULL;
	pthread_mutex_unlock(&ctx->cb_lock);

	return 0;
}

/**
 * @brief Enable electronic fence.
 * @param[in] idx         video window index.
 * @see AVFTR_EF_disable
 * @retval 0                 success.
 * @retval ENODEV            idx is not registered yet
 */
int AVFTR_EF_enable(MPI_WIN idx)
{
	AVFTR_EF_CTX_S *vftr_ef_ctx = vftr_res_shm->ef_ctx;
	int enable_idx = findEfCtx(idx, vftr_ef_ctx, NULL);

	if (enable_idx < 0) {
		/* idx is not registered */
		SYS_TRACE("idx:%d is not registered yet!\n", idx.value);
		return ENODEV;
	}

	if (vftr_ef_ctx[enable_idx].en) {
		/* idx is enabled */
		SYS_TRACE("idx:%d is enabled, no need to enable again!\n", idx.value);
		return 0;
	}

	int ret;

	EF_CTX_S *ctx = EF_GET_CTX(enable_idx);

	ret = VIDEO_FTR_enableOd_implicit(idx);
	if (ret != 0) {
		return ret;
	}

	pthread_mutex_lock(&ctx->cb_lock);
	if (vftr_ef_ctx[enable_idx].cb == NULL) {
		//SYS_TRACE("Motion detection alarm callback function is not registered on win %d.\n", idx.win);
		vftr_ef_ctx[enable_idx].cb = alarmEmptyCb;
	}
	pthread_mutex_unlock(&ctx->cb_lock);
	vftr_ef_ctx[enable_idx].en = 1;

	return 0;
}

/**
 * @brief Disable electronic fence.
 * @param[in] idx         video window index.
 * @see AVFTR_EF_enable
 * @retval 0                 success.
 * @retval ENODEV            idx is not registered yet
 */
int AVFTR_EF_disable(MPI_WIN idx)
{
	AVFTR_EF_CTX_S *vftr_ef_ctx = vftr_res_shm->ef_ctx;

	int enable_idx = findEfCtx(idx, vftr_ef_ctx, NULL);

	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registerd yet, no need to disable\n", idx.value);
		return ENODEV;
	}

	if (!vftr_ef_ctx[enable_idx].en) {
		/* idx is not enabled */
		SYS_TRACE("idx:%d is not enabled yet, no need to disable\n", idx.value);
		return 0;
	}

	INT32 ret = 0;
	ret = VIDEO_FTR_disableOd_implicit(idx);
	if (ret != 0) {
		SYS_TRACE("Disable object detection on win %d failed!\n", idx.win);
		return ret;
	}
	vftr_ef_ctx[enable_idx].en = 0;

	return 0;
}

/**
 * @brief Get parameters of ef.
 * @param[in]  idx        video window index.
 * @param[out] param      ef parameters.
 * @see AVFTR_EF_setParam
 * @retval 0              success.
 * @retval ENODEV         idx is not registered yet
 */
int AVFTR_EF_getParam(MPI_WIN idx, VFTR_EF_PARAM_S *param)
{
	AVFTR_EF_CTX_S *vftr_ef_ctx = vftr_res_shm->ef_ctx;
	int enable_idx = findEfCtx(idx, vftr_ef_ctx, NULL);

	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registered\n", idx.value);
		return ENODEV;
	}

	EF_CTX_S *ctx = EF_GET_CTX(enable_idx);

	int ret = 0;

	ret = checkMpiDevValid(idx);
	if (ret != 0) {
		return ret;
	}

	*param = ctx->param;

	return 0;
}

/**
 * @brief Set parameters of ef .
 * @param[in] idx                video window index.
 * @param[in] ef_param           ef parameters.
 * @see AVFTR_EF_getParam
 * @retval 0              success.
 * @retval ENODEV         idx is not registered yet
 */
int AVFTR_EF_setParam(MPI_WIN idx, const VFTR_EF_PARAM_S *param)
{
	if (param == NULL) {
		SYS_TRACE("Pointer to the ef parameter should not be NULL.\n");
		return EFAULT;
	}

	AVFTR_EF_CTX_S *vftr_ef_ctx = vftr_res_shm->ef_ctx;
	int enable_idx = findEfCtx(idx, vftr_ef_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registered\n", idx.value);
		return ENODEV;
	}

	EF_CTX_S *ctx = EF_GET_CTX(enable_idx);

	int ret;

	MPI_SIZE_S res = {};
	ret = getMpiSize(idx, &res);
	if (ret != 0) {
		return ret;
	}

	ret = VFTR_EF_checkParam(param, &res);
	if (ret != 0) {
		return ret;
	}

	// Copy param to temp buffer and prepare to set to vftr_ef
	pthread_mutex_lock(&ctx->lock);
	ctx->param = *param;
	ctx->s_flag = 1;
	pthread_mutex_unlock(&ctx->lock);

	return 0;
}

/**
 * @brief Write parameters to ef instance 
 * @param[in]  idx        video window index.
 * @see AVFTR_EF_getParam
 * @retval 0              success.
 * @retval ENODEV         idx is not registered yet
 */
int AVFTR_EF_writeParam(MPI_WIN idx)
{
	AVFTR_EF_CTX_S *vftr_ef_ctx = vftr_res_shm->ef_ctx;
	int enable_idx = findEfCtx(idx, vftr_ef_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		//SYS_TRACE("idx:%d is not registered\n", idx.value);
		return ENODEV;
	}

	EF_CTX_S *ctx = EF_GET_CTX(enable_idx);

	int ret;

	int s_flag = ctx->s_flag;
	if (s_flag == 1) {
		pthread_mutex_lock(&ctx->lock);
		ret = VFTR_EF_setParam(ctx->instance, &ctx->param);
		ctx->s_flag = 0;
		pthread_mutex_unlock(&ctx->lock);
		if (ret != 0) {
			return ret;
		}
	}
	return 0;
}

int AVFTR_EF_checkParam(MPI_WIN idx)
{
	AVFTR_EF_CTX_S *vftr_ef_ctx = vftr_res_shm->ef_ctx;
	int enable_idx = findEfCtx(idx, vftr_ef_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registered\n", idx.value);
		return ENODEV;
	}

	EF_CTX_S *ctx = EF_GET_CTX(enable_idx);

	int ret;

	MPI_SIZE_S res = {};
	ret = getMpiSize(idx, &res);
	if (ret != 0) {
		return ret;
	}

	ret = VFTR_EF_checkParam(&ctx->param, &res);
	if (ret != 0) {
		return ret;
	}
	return 0;
}

int AVFTR_EF_addVl(MPI_WIN idx, AVFTR_EF_VL_ATTR_S *fence)
{
	if (fence == NULL) {
		SYS_TRACE("Pointer to the efence should not be NULL.\n");
		return EFAULT;
	}

	AVFTR_EF_CTX_S *vftr_ef_ctx = vftr_res_shm->ef_ctx;
	int enable_idx = findEfCtx(idx, vftr_ef_ctx, NULL);
	if (enable_idx < 0) {
		SYS_TRACE("ADD EF fence failed. EF on win %d is not enabled\n", idx.win);
		return ENODEV;
	}

	EF_CTX_S *ctx = EF_GET_CTX(enable_idx);

	UINT32 dev_idx = MPI_GET_VIDEO_DEV(idx);
	UINT32 chn_idx = MPI_GET_VIDEO_CHN(idx);
	UINT32 win_idx = MPI_GET_VIDEO_DEV(idx);

	if (chn_idx >= MPI_MAX_VIDEO_CHN_NUM) {
		SYS_TRACE("Video channel index should be less than %d.\n", MPI_MAX_VIDEO_CHN_NUM);
		return EINVAL;
	}

	if (dev_idx >= MPI_MAX_VIDEO_DEV_NUM) {
		SYS_TRACE("Video device index should be less than %d.\n", MPI_MAX_VIDEO_DEV_NUM);
		return EINVAL;
	}

	if (win_idx >= MPI_MAX_VIDEO_WIN_NUM) {
		SYS_TRACE("Video window index should be less than %d.\n", MPI_MAX_VIDEO_WIN_NUM);
		return EINVAL;
	}

	VFTR_EF_PARAM_S param = { 0 };

	pthread_mutex_lock(&ctx->lock);
	param = ctx->param;
	pthread_mutex_unlock(&ctx->lock);

	fence->id = 0;
	for (INT32 i = 0; i < VFTR_EF_MAX_FENCE_NUM; i++) {
		if (param.attr[i].id == 0) {
			fence->id = i + 1;
			param.fence_num++;
			memcpy(&param.attr[i], fence, sizeof(AVFTR_EF_VL_ATTR_S));
			break;
		}
	}

	if (fence->id == 0) {
		SYS_TRACE("Maximum fence num is reached.\n");
		return EAGAIN;
	}

	pthread_mutex_lock(&ctx->lock);
	ctx->param = param;
	ctx->s_flag = 1;
	pthread_mutex_unlock(&ctx->lock);

	return 0;
}

int AVFTR_EF_rmVl(MPI_WIN idx, INT16 fence_id)
{
	AVFTR_EF_CTX_S *vftr_ef_ctx = vftr_res_shm->ef_ctx;
	int enable_idx = findEfCtx(idx, vftr_ef_ctx, NULL);
	if (enable_idx < 0) {
		SYS_TRACE("Remove EF Fence failed. EF on win %d is not enabled\n", idx.win);
		return ENODEV;
	}

	EF_CTX_S *ctx = EF_GET_CTX(enable_idx);

	UINT32 dev_idx = MPI_GET_VIDEO_DEV(idx);
	UINT32 chn_idx = MPI_GET_VIDEO_CHN(idx);
	UINT32 win_idx = MPI_GET_VIDEO_DEV(idx);

	if (fence_id < VFTR_EF_VL_MIN_FENCE_ID || fence_id > VFTR_EF_VL_MAX_FENCE_ID) {
		SYS_TRACE("Fence index %d exceeds boundary.\n", fence_id);
		return EINVAL;
	}

	if (chn_idx >= MPI_MAX_VIDEO_CHN_NUM) {
		SYS_TRACE("Video channel index should be less than %d.\n", MPI_MAX_VIDEO_CHN_NUM);
		return EINVAL;
	}

	if (dev_idx >= MPI_MAX_VIDEO_DEV_NUM) {
		SYS_TRACE("Video device index should be less than %d.\n", MPI_MAX_VIDEO_DEV_NUM);
		return EINVAL;
	}

	if (win_idx >= MPI_MAX_VIDEO_WIN_NUM) {
		SYS_TRACE("Video window index should be less than %d.\n", MPI_MAX_VIDEO_WIN_NUM);
		return EINVAL;
	}

	VFTR_EF_PARAM_S param = { 0 };

	pthread_mutex_lock(&ctx->lock);
	param = ctx->param;
	pthread_mutex_unlock(&ctx->lock);

	/* Remove EFence */
	if (param.attr[fence_id - 1].id == 0) {
		SYS_TRACE("Fence id %d does not exist.\n", fence_id);
		return ENODEV;
	}

	param.fence_num--;
	param.attr[fence_id - 1].id = 0;

	pthread_mutex_lock(&ctx->lock);
	ctx->param = param;
	ctx->s_flag = 1;
	pthread_mutex_unlock(&ctx->lock);

	return 0;
}

/**
 * @brief Register alarm callback function of electronic fence.
 * @param[in]  idx         video window index.
 * @param[in]  alarm_cb_fptr   function pointer of callback function.
 * @see none
 * @retval MPI_SUCCESS                 success.
 * @retval MPI_FAILURE                 unexpected fail.
 */
int AVFTR_EF_regCallback(MPI_WIN idx, const AVFTR_EF_ALARM_CB alarm_cb_fptr)
{
	if (alarm_cb_fptr == NULL) {
		SYS_TRACE("Pointer to electric fence alarm callback function should not be NULL.\n");
		return EFAULT;
	}

	AVFTR_EF_CTX_S *vftr_ef_ctx = vftr_res_shm->ef_ctx;
	int enable_idx = findEfCtx(idx, vftr_ef_ctx, NULL);

	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registered\n", idx.value);
		return ENODEV;
	}

	EF_CTX_S *ctx = EF_GET_CTX(enable_idx);

	pthread_mutex_lock(&ctx->cb_lock);
	vftr_ef_ctx[enable_idx].cb = alarm_cb_fptr;
	pthread_mutex_unlock(&ctx->cb_lock);

	return 0;
}
