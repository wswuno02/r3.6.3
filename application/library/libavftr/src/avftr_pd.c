/**
 * @cond
 *
 * code fragment skipped by Doxygen.
 */

#include "avftr_pd.h"

#include <pthread.h>
#include <stdio.h>

#include "mtk_common.h"
#include "mpi_dev.h"
#include "video_od.h"
#include "avftr.h"

typedef struct {
	AVFTR_PD_PARAM_S param;
	VFTR_OSC_INSTANCE_S *instance;
	int s_flag;
	pthread_mutex_t lock;
	pthread_mutex_t cb_lock;
} PD_CTX_S;

extern AVFTR_VIDEO_CTX_S *vftr_res_shm;

static PD_CTX_S g_pd_ctx[AVFTR_PD_MAX_SUPPORT_NUM] = { { { { 0 } } } };

#define PD_GET_CTX(idx) &g_pd_ctx[idx]

/**
 * @brief Find CTX with specified WIN_IDX.
 * @details This function also sets empty_idx if target WIN_IDX is not found.
 */
static inline int findPdCtx(MPI_WIN idx, const AVFTR_PD_CTX_S *ctx, int *empty)
{
	int i = 0;
	int find_idx = -1;
	int emp_idx = -1;

	if (empty == NULL) {
		emp_idx = -2;
	}

	for (i = 0; i < AVFTR_PD_MAX_SUPPORT_NUM; i++) {
		if (find_idx == -1 && ctx[i].idx.value == idx.value && ctx[i].reg) {
			find_idx = i;
		} else if (emp_idx == -1 && !ctx[i].reg) {
			emp_idx = i;
		}
	}

	if (empty != NULL) {
		*empty = emp_idx;
	}

	return find_idx;
}

/**
 * @brief Invoke callback function when alarm condition is satisfied.
 * @param[in] idx         video window index.
 * @param[in] pd_stat     pedestrian detection result.
 * @see VIDEO_FTR_getPdRes()
 * @retval none.
 */
static void genPdAlarm(AVFTR_PD_CTX_S *pd_ctx, const VFTR_OSC_STATUS_S *osc_stat)
{
	if (pd_ctx->cb == NULL) {
		return;
	}

	//FIXME: add pd_ctx->cb() if alarm is needed
	return;
}

/**
 * @brief Get predefined metadata format for Multiplayer.
 * @param[in]  pd_stat    pedestrian detection result.
 * @param[out] str        metadata string buffer.
 * @see VIDEO_FTR_getPdRes()
 * @retval length of metadata.
 */
static int getPdMeta(const VFTR_OSC_STATUS_S *pd_stat, VIDEO_FTR_OBJ_LIST_S *list)
{
	MPI_IVA_OBJ_LIST_S *obj_list = &list->basic_list;
	VIDEO_FTR_OBJ_ATTR_S *attr = list->obj_attr;
	const UINT8 *pd = pd_stat->category;
	int i;

	for (i = 0; i < obj_list->obj_num; i++) {
		if (pd[i] == 1) {
			strcpy(attr[i].cat, "PEDESTRIAN");
		} else {
			strcpy(attr[i].cat, "UNKNOWN");
		}
	}

	return 0;
}

/**
 * @brief Empty callback function for initialization.
 * @param[in] none.
 * @see VIDEO_FTR_enablePd()
 * @retval none.
 */
static void alarmEmptyCb()
{
	SYS_TRACE("Please registrate pedestrian detection alarm callback function.\n");
	return;
}

/**
 * @endcond
 */

/**
 * @brief Add pedestrian detection instance.
 * @param[in] idx    index of the video window to add PD instance.
 * @return The execution result
 * @retval 0          success
 * @retval -ENOMEM    memory is not enough
 * @see AVFTR_PD_deleteInstance()
 */
int AVFTR_PD_addInstance(MPI_WIN idx)
{
	AVFTR_PD_CTX_S *shm_ctx = vftr_res_shm->pd_ctx;
	PD_CTX_S *ctx;
	int empty_idx;
	int set_idx;

	set_idx = findPdCtx(idx, shm_ctx, &empty_idx);
	if (set_idx >= 0) {
		SYS_TRACE("idx: %d is registered.\n", idx.value);
		return 0;
	}

	/** Create object in process local memory, then register object to cross process memory */
	if (empty_idx < 0) {
		SYS_TRACE("Cannot register more PD instance.\n");
		return -ENOMEM;
	}

	ctx = &(g_pd_ctx[empty_idx]);
	ctx->instance = VFTR_OSC_newInstance();
	if (ctx->instance == NULL) {
		SYS_TRACE("Failed to create OSC instance.\n");
		return -ENOMEM;
	}
	ctx->s_flag = 0;
	ctx->lock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
	ctx->cb_lock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;

	pthread_mutex_lock(&ctx->cb_lock);
	shm_ctx[empty_idx] = (AVFTR_PD_CTX_S){ .idx = idx, .reg = 1, .en = 0, .cb = NULL };
	pthread_mutex_unlock(&ctx->cb_lock);
	return 0;
}

/**
 * @brief Delete prdestrian detection instance.
 * @see AVFTR_PD_addInstance()
 */
int AVFTR_PD_deleteInstance(MPI_WIN idx)
{
	AVFTR_PD_CTX_S *shm_ctx = vftr_res_shm->pd_ctx;
	int enable_idx = findPdCtx(idx, shm_ctx, NULL);
	INT32 ret;

	if (enable_idx < 0) {
		SYS_TRACE("idx: %d is not registered yet.\n", idx.value);
		return 0;
	}

	if (shm_ctx[enable_idx].en) {
		SYS_TRACE("idx: %d is still enable, cannot be deleted!\n", idx.value);
		return -EAGAIN;
	}

	PD_CTX_S *ctx = PD_GET_CTX(enable_idx);

	pthread_mutex_lock(&ctx->lock);
	ret = VFTR_OSC_deleteInstance(&ctx->instance);
	pthread_mutex_unlock(&ctx->lock);

	if (ret != 0) {
		SYS_TRACE("Free OSC instance failed. err: %d\n", ret);
		return ret;
	}

	shm_ctx[enable_idx].reg = 0;
	shm_ctx[enable_idx].en = 0;

	pthread_mutex_lock(&ctx->cb_lock);
	shm_ctx[enable_idx].cb = NULL;
	pthread_mutex_unlock(&ctx->cb_lock);

	return 0;
}

/**
 * @brief Get enable status of pedestrian detection.
 * @param[in] idx     video window index.
 * @return enable status of pedestrian detection.
 */
int AVFTR_PD_getStat(MPI_WIN idx, const AVFTR_PD_CTX_S *vftr_pd_ctx)
{
	if (vftr_pd_ctx == NULL) {
		SYS_TRACE("Input parameter cannot be NULL.\n");
		return -EFAULT;
	}

	int enable_idx = findPdCtx(idx, vftr_pd_ctx, NULL);

	return enable_idx < 0 ? 0 : vftr_pd_ctx[enable_idx].en;
}

/**
 * @brief Get results of pedestrian detection.
 * @param[in] idx         video window index.
 * @param[in] obj_list    object list.
 * @param[in] buf_idx     index of the buffer to store result.
 * @return The execution result.
 * @retval 0          success
 * @retval -EFAULT    input parameter is NULL.
 * @retval -ENOENT    no such entry in target window index
 * @retval -EAGAIN    not enabled yet.
 */
int AVFTR_PD_getRes(MPI_WIN idx, VIDEO_FTR_OBJ_LIST_S *obj_list, int buf_idx)
{
	AVFTR_PD_CTX_S *vftr_pd_ctx = vftr_res_shm->pd_ctx;
	int enable_idx = findPdCtx(idx, vftr_pd_ctx, NULL);

	if (obj_list == NULL) {
		SYS_TRACE("obj_list cannot be NULL.\n");
		return -EFAULT;
	}

	if (enable_idx < 0) {
		//SYS_TRACE("idx: %d is not registered yet.\n", idx.value);
		return -ENOENT;
	}

	if (!vftr_pd_ctx[enable_idx].en) {
		return -EAGAIN;
	}

	PD_CTX_S *ctx = PD_GET_CTX(enable_idx);

	VFTR_OSC_STATUS_S *osc_stat = &vftr_pd_ctx[enable_idx].stat[buf_idx];

	pthread_mutex_lock(&g_pd_ctx[enable_idx].lock);
	VFTR_OSC_classify(g_pd_ctx[enable_idx].instance, &obj_list->basic_list, osc_stat);
	pthread_mutex_unlock(&g_pd_ctx[enable_idx].lock);

	pthread_mutex_lock(&ctx->cb_lock);
	genPdAlarm(&vftr_pd_ctx[enable_idx], osc_stat);
	pthread_mutex_unlock(&ctx->cb_lock);

	getPdMeta(osc_stat, obj_list);

	return 0;
}

/**
 * @brief Enable pedestrian detection.
 * @param[in]  idx            video window index.
 * @retval 0          success.
 * @retval -ENOENT    PD is not added in target video window
 * @retval -1         unexpected fail.
 * @see AVFTR_PD_disable()
 */
int AVFTR_PD_enable(MPI_WIN idx)
{
	AVFTR_PD_CTX_S *vftr_pd_ctx = vftr_res_shm->pd_ctx;
	int enable_idx = findPdCtx(idx, vftr_pd_ctx, NULL);
	int ret;
	int i;

	if (enable_idx < 0 || g_pd_ctx[enable_idx].instance == NULL) {
		SYS_TRACE("idx:%d is not registered yet.\n", idx.value);
		return -ENOENT;
	}

	/** Check whether PD is already enabled. Exit function if true. */
	if (vftr_pd_ctx[enable_idx].en) {
		SYS_TRACE("idx: %d is already enabled.\n", idx.value);
		return 0;
	}

	PD_CTX_S *ctx = PD_GET_CTX(enable_idx);

	if ((ret = VIDEO_FTR_enableOd(idx))) {
		SYS_TRACE("idx: %d cannot enable OD. err: %d.\n", idx.value, ret);
		return ret;
	}

	MPI_CHN_LAYOUT_S layout_attr;
	MPI_CHN chn_id = MPI_VIDEO_CHN(idx.dev, idx.chn);
	if ((ret = MPI_DEV_getChnLayout(chn_id, &layout_attr))) {
		SYS_TRACE("Pedestrain detection get attribues on win %u failed.\n", chn_id.chn);
		return ret;
	}
	for (i = 0; i < layout_attr.window_num; i++) {
		if (idx.value == layout_attr.win_id[i].value) {
			break;
		}
	}

	if (i == layout_attr.window_num) {
		SYS_TRACE("Window %d does not exist in channel %d\n", idx.win, idx.chn);
		return -ENOENT;
	}

	VFTR_OSC_PARAM_S param = { .min_sz = { .width = 64, .height = 64 },
		                   .max_sz = { .width = layout_attr.window[i].width / 3,
		                               .height = layout_attr.window[i].height },
		                   .min_aspect_ratio = 18725,
		                   .max_aspect_ratio = 43691 };

	MPI_SIZE_S size = { .width = layout_attr.window[i].width, .height = layout_attr.window[i].height };

	pthread_mutex_lock(&g_pd_ctx[enable_idx].lock);
	ret = VFTR_OSC_setParam(g_pd_ctx[enable_idx].instance, &size, &param);
	pthread_mutex_unlock(&g_pd_ctx[enable_idx].lock);

	if (ret != 0) {
		SYS_TRACE("Pedestrain detection failed to set default parameters on win %u.\n", idx.win);
		return ret;
	}

	pthread_mutex_lock(&ctx->cb_lock);
	if (vftr_pd_ctx[enable_idx].cb == NULL) {
		vftr_pd_ctx[enable_idx].cb = alarmEmptyCb;
	}
	pthread_mutex_unlock(&ctx->cb_lock);

	vftr_pd_ctx[enable_idx].en = 1;

	return 0;
}

/**
 * @brief Disable pedestrian detection.
 * @param[in] idx    video window index.
 * @retval 0         success.
 * @retval -ENOENT   PD is not added in target video window
 * @retval others    unexpected fail.
 * @see AVFTR_PD_enable()
 */
int AVFTR_PD_disable(MPI_WIN idx)
{
	AVFTR_PD_CTX_S *vftr_pd_ctx = vftr_res_shm->pd_ctx;
	int enable_idx = findPdCtx(idx, vftr_pd_ctx, NULL);
	INT32 ret;

	if (enable_idx < 0) {
		SYS_TRACE("idx: %d is not registered yet.\n", idx.value);
		return -ENOENT;
	}

	if (vftr_pd_ctx[enable_idx].en) {
		if ((ret = VIDEO_FTR_disableOd(idx))) {
			return ret;
		}
		vftr_pd_ctx[enable_idx].en = 0;
	}

	return 0;
}

/**
 * @brief Get parameters of pedestrian detection.
 * @details Get from buffer parameter.
 * @param[in]  idx        video window index.
 * @param[out] param      pedestrian detection parameters.
 * @retval 0          success.
 * @retval -EFAULT    input pointer is NULL.
 * @retval -ENOENT    device/channel doesn't exist.
 * @see AVFTR_PD_setParam()
 */
int AVFTR_PD_getParam(MPI_WIN idx, AVFTR_PD_PARAM_S *param)
{
	AVFTR_PD_CTX_S *vftr_pd_ctx = vftr_res_shm->pd_ctx;
	int enable_idx = findPdCtx(idx, vftr_pd_ctx, NULL);
	PD_CTX_S *ctx;

	if (param == NULL) {
		SYS_TRACE("idx: %d, input parameter cannot be NULL.\n", idx.value);
		return -EFAULT;
	}

	if (enable_idx < 0 || g_pd_ctx[enable_idx].instance == NULL) {
		SYS_TRACE("idx: %d is not registered yet.\n", idx.value);
		return -ENOENT;
	}

	ctx = &g_pd_ctx[enable_idx];

	pthread_mutex_lock(&ctx->lock);
	memcpy(param, &ctx->param, sizeof(AVFTR_PD_PARAM_S));
	pthread_mutex_unlock(&ctx->lock);

	return 0;
}

/**
 * @brief Set parameters of pedestrian detection.
 * @details Set to buffer, then it can be updated actually by AVFTR_PD_writeParam()
 * @param[in] idx          video window index.
 * @param[in] param        pedestrian detection parameters.
 * @return The execution result.
 * @retval 0          success.
 * @retval -EFAULT    input pointer is NULL.
 * @retval -ENOENT    device/channel doesn't exist.
 * @see AVFTR_PD_writeParam()
 * @see AVFTR_PD_getParam()
 */
int AVFTR_PD_setParam(MPI_WIN idx, const AVFTR_PD_PARAM_S *param)
{
	AVFTR_PD_CTX_S *vftr_pd_ctx = vftr_res_shm->pd_ctx;
	int set_idx = findPdCtx(idx, vftr_pd_ctx, NULL);
	PD_CTX_S *ctx;

	if (param == NULL) {
		return -EFAULT;
	}

	if (set_idx < 0) {
		return -ENOENT;
	}

	ctx = &g_pd_ctx[set_idx];

	pthread_mutex_lock(&ctx->lock);
	memcpy(&ctx->param, param, sizeof(AVFTR_PD_PARAM_S));
	ctx->s_flag = 1;
	pthread_mutex_unlock(&ctx->lock);

	return 0;
}

/**
 * @brief Overwrite parameter to object member
 */
int AVFTR_PD_writeParam(MPI_WIN idx)
{
	AVFTR_PD_CTX_S *vftr_pd_ctx = vftr_res_shm->pd_ctx;
	MPI_CHN chn_id = MPI_VIDEO_CHN(idx.dev, idx.chn);
	MPI_CHN_LAYOUT_S layout_attr;
	int set_idx = findPdCtx(idx, vftr_pd_ctx, NULL);
	int ret;
	int i;

	if ((ret = MPI_DEV_getChnLayout(chn_id, &layout_attr))) {
		SYS_TRACE("Pedestrain detection get attribues on win %u failed.\n", chn_id.chn);
		return ret;
	}

	for (i = 0; i < layout_attr.window_num; i++) {
		if (idx.value == layout_attr.win_id[i].value) {
			break;
		}
	}

	if (i == layout_attr.window_num) {
		SYS_TRACE("Window %d does not exist in channel %d\n", idx.win, idx.chn);
		return -ENOENT;
	}

	MPI_SIZE_S size = { .width = layout_attr.window[i].width, .height = layout_attr.window[i].height };

	PD_CTX_S *ctx = &g_pd_ctx[set_idx];
	if (ctx->s_flag == 1) {
		pthread_mutex_lock(&ctx->lock);
		ret = VFTR_OSC_setParam(g_pd_ctx[set_idx].instance, &size, &ctx->param);
		ctx->s_flag = 0;
		pthread_mutex_unlock(&ctx->lock);
	}

	return ret;
}

/**
 * @brief Register alarm callback function of pedestrian detection.
 * @param[in] idx              video window index.
 * @param[in]  alarm_cb_fptr   function pointer of callback function.
 * @retval 0          success.
 * @retval -1          unexpected fail.
 */
int AVFTR_PD_regCallback(MPI_WIN idx, const AVFTR_PD_ALARM_CB alarm_cb_fptr)
{
	AVFTR_PD_CTX_S *vftr_pd_ctx = vftr_res_shm->pd_ctx;
	int enable_idx = findPdCtx(idx, vftr_pd_ctx, NULL);

	if (alarm_cb_fptr == NULL) {
		SYS_TRACE("Pointer to pedestrian detection alarm callback function should not be NULL.\n");
		return -EFAULT;
	}

	if (enable_idx < 0) {
		SYS_TRACE("No available seting of PD on the win %u.\n", idx.win);
		return -ENOENT;
	}

	PD_CTX_S *ctx = PD_GET_CTX(enable_idx);

	pthread_mutex_lock(&ctx->cb_lock);
	vftr_pd_ctx[enable_idx].cb = alarm_cb_fptr;
	pthread_mutex_unlock(&ctx->cb_lock);

	return 0;
}
