/**
* @cond
*
* code fragment skipped by Doxygen.
*/

#include <stdio.h>
#include <time.h>
#include <stdatomic.h>

#include "mtk_common.h"
#include "mpi_dev.h"

#include "avftr_td.h"
#include "avftr.h"
#include "avftr_common.h"

typedef struct {
	AVFTR_TD_PARAM_S param;
	VFTR_TD_MPI_INPUT_S mpi_input;
	VFTR_TD_INSTANCE_S *instance;
	unsigned int is_write;
	unsigned int is_reset;

	MPI_PATH path;

	pthread_mutex_t lock;
	pthread_mutex_t cb_lock;
} TD_CTX_S;

static TD_CTX_S g_td_ctx[AVFTR_TD_MAX_SUPPORT_NUM] = { { { { 0 } } } };

#define TD_GET_CTX(idx) &g_td_ctx[idx]

extern AVFTR_VIDEO_CTX_S *vftr_res_shm;

static int findTdCtx(MPI_WIN idx, AVFTR_TD_CTX_S *ctx, int *empty)
{
	int i = 0;
	int find_idx = -1;
	int emp_idx = -1;

	if (empty == NULL) {
		emp_idx = -2;
	} else {
		emp_idx = -1;
	}

	for (i = 0; i < AVFTR_TD_MAX_SUPPORT_NUM; i++) {
		if (find_idx == -1 && ctx[i].idx.value == idx.value && ctx[i].reg) {
			find_idx = i;
		} else if (emp_idx == -1 && !ctx[i].reg) {
			emp_idx = i;
		}
	}

	if (empty != NULL) {
		*empty = emp_idx;
	}

	return find_idx;
}

/**
 * @brief Invoke callback function when alarm condition is satisfied.
 * @param[in] idx           video channel index.
 * @param[in] tamper_alarm  tamper detection result.
 * @see VIDEO_FTR_getTdRes()
 * @retval none.
 */
static void genTdAlarm(AVFTR_TD_CTX_S *td_ctx, int tamper_alarm)
{
	if (td_ctx->cb == NULL) {
		return;
	}

	if (tamper_alarm) {
		td_ctx->cb();
		return;
	}
	/*if (tamper_alarm & (1 << TD_ALARM_MULTIPLE)) {
	} else if (tamper_alarm & (1 << TD_ALARM_BLOCK)) {
	} else if (tamper_alarm & (1 << TD_ALARM_DEFOCUSING)) {
	} else if (tamper_alarm & (1 << TD_ALARM_REDIRECTING)) {
	}*/
}

/**
 * @brief Get predefined metadata format for Multiplayer.
 * @param[in] tamper_alarm  tamper detection result.
 * @param[in] str           metadata string buffer.
 * @see VIDEO_FTR_getTdRes()
 * @retval length of metadata.
 */
static int getTdMeta(int tamper_alarm, char *str)
{
	int offset = 0;
#ifdef IVA_FORMAT_XML /* IVA_FORMAT_XML */
	offset += sprintf(&str[offset], "<TD>");
#else /* IVA_FORMAT_JSON */
	offset += sprintf(&str[offset], "\"td\":{");
#endif /* !IVA_FORMAT_XML */
	if (tamper_alarm & (1 << VFTR_TD_ALARM_BLOCK)) {
		offset += sprintf(&str[offset],
#ifdef IVA_FORMAT_XML /* IVA_FORMAT_XML */
		                  "<TYPE>block</TYPE>");
#else /* IVA_FORMAT_JSON */
		                  "\"type\":\"block tamper\"");
#endif /* !IVA_FORMAT_XML */
	}

#ifdef IVA_FORMAT_XML /* IVA_FORMAT_XML */
	offset += sprintf(&str[offset], "</TD>");
#else /* IVA_FORMAT_JSON */
	offset += sprintf(&str[offset], "},");
#endif /* !IVA_FORMAT_XML */
	return offset;
}

/**
 * @brief Empty callback function for initialization.
 * @param[in] none.
 * @see VIDEO_FTR_enableTd()
 * @retval none.
 */
static void alarmEmptyCb()
{
	SYS_TRACE("Please registrate tamper detection alarm callback function.\n");
	return;
}

/**
 * @endcond
 */

/**
 * @brief Get enable status of tamper detection.
 * @param[in] idx          video window index.
 * @see none
 * @retval enable status of tamper detection.
 */
int AVFTR_TD_getStat(MPI_WIN idx, AVFTR_TD_CTX_S *vftr_td_ctx)
{
	int enable_idx = findTdCtx(idx, vftr_td_ctx, NULL);

	return enable_idx < 0 ? 0 : vftr_td_ctx[enable_idx].en;
}

/**
 * @brief Get results of tamper detection.
 * @param[in] idx          video window index.
 * @param[out] str         metadata string buffer.
 * @see none
 * @retval 0                 success.
 * @retval ENODEV            idx is not registered
 * @retval EAGAIN            idx is not enabled
 * @retval EFAULT            DK object is NULL
 */
int AVFTR_TD_getRes(MPI_WIN idx, int buf_idx)
{
	AVFTR_TD_CTX_S *vftr_td_ctx = vftr_res_shm->td_ctx;

	int enable_idx = findTdCtx(idx, vftr_td_ctx, NULL);
	int ret = 0;

	if (enable_idx < 0) {
		/* idx is not registered yet */
		//SYS_TRACE("idx:%d is not registered yet!\n",idx.value);
		return ENODEV;
	}

	if (!vftr_td_ctx[enable_idx].en) {
		/* idx is not enabled yet */
		//SYS_TRACE("idx:%d is not enabled yet!\n",idx.value);
		return EAGAIN;
	}

	TD_CTX_S *ctx = TD_GET_CTX(enable_idx);
	if (ctx->instance == NULL) {
		SYS_TRACE("TD object is NULL!\n");
		return EFAULT;
	}

	VFTR_TD_STATUS_S *td_status = &vftr_td_ctx[enable_idx].td_res[buf_idx];

	pthread_mutex_lock(&ctx->lock);
	ret = VFTR_TD_detect(ctx->instance, &ctx->mpi_input, td_status);
	pthread_mutex_unlock(&ctx->lock);

	if (ret != 0) {
		SYS_TRACE("Failed to run tamper detection!\n");
		return ret;
	}

	pthread_mutex_lock(&ctx->cb_lock);
	genTdAlarm(&vftr_td_ctx[enable_idx], td_status->alarm);
	pthread_mutex_unlock(&ctx->cb_lock);

	return 0;
}

/**
 * @brief Get predefined metadata format for Multiplayer.
 * @param[in] src_idx  mpi win index of source window
 * @param[in] dst_idx  mpi win index of destination window
 * @param[in] src_rect source window
 * @param[in] dst_rect destination window
 * @param[in] tamper_alarm  tamper detection result.
 * @param[in] str           metadata string buffer.
 * @see VIDEO_FTR_getTdRes()
 * @retval length of metadata.
 */
int AVFTR_TD_transRes(AVFTR_TD_CTX_S *vftr_td_ctx, MPI_WIN src_idx, MPI_WIN dst_idx, const MPI_RECT_S *src_rect,
                      const MPI_RECT_S *dst_rect, const MPI_RECT_S *src_roi, const MPI_RECT_S *dst_roi, char *str,
                      int buf_idx)
{
	int enable_idx = findTdCtx(src_idx, vftr_td_ctx, NULL);

	if (enable_idx < 0) {
		/* idx is not registered yet */
		return 0;
	}

	if (vftr_td_ctx[enable_idx].en) {
		/* idx is registered and enable */
		return getTdMeta(vftr_td_ctx[enable_idx].td_res[buf_idx].alarm, str);
	}

	return 0;
}

/**
 * @brief Add TD instance.
 * @param[in]  idx         video window index.
 * @see AVFTR_TD_deleteInstance
 * @retval 0                 success.
 * @retval ENOMEM            No more space to register idx / malloc TD instance failed
 * @retval EFAULT            TD instance is NULL
 */
int AVFTR_TD_addInstance(MPI_WIN idx)
{
	AVFTR_TD_CTX_S *vftr_td_ctx = vftr_res_shm->td_ctx;
	int empty_idx;
	int set_idx = findTdCtx(idx, vftr_td_ctx, &empty_idx);

	if (set_idx >= 0) {
		/* idx is registered */
		SYS_TRACE("idx:%d is registerd\n", idx.value);
		return 0;
	} else if (set_idx < 0 && empty_idx >= 0) {
		/* idx is not registered yet but there is empty space to be registerd*/
		TD_CTX_S *ctx = TD_GET_CTX(empty_idx);
		ctx->instance = VFTR_TD_newInstance();
		if (!ctx->instance) {
			SYS_TRACE("Failed to create TD instance \n");
			return ENOMEM;
		}
		ctx->lock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
		ctx->cb_lock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;

		vftr_td_ctx[empty_idx].idx = idx;
		vftr_td_ctx[empty_idx].reg = 1;
		vftr_td_ctx[empty_idx].en = 0;

		pthread_mutex_lock(&ctx->cb_lock);
		vftr_td_ctx[empty_idx].cb = NULL;
		pthread_mutex_unlock(&ctx->cb_lock);

	} else {
		/* No more space to register idx */
		SYS_TRACE("add TD instance failed on win %u.\n", idx.win);
		return ENOMEM;
	}

	return 0;
}

/**
 * @brief Delete TD instance.
 * @param[in]  idx         video window index.
 * @see AVFTR_TD_addInstance
 * @retval 0                 success.
 * @retval EAGAIN            idx is enabled, not to remove
 */
int AVFTR_TD_deleteInstance(MPI_WIN idx)
{
	AVFTR_TD_CTX_S *vftr_td_ctx = vftr_res_shm->td_ctx;
	int enable_idx = findTdCtx(idx, vftr_td_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered */
		//SYS_TRACE("idx:%d is not registered yet!\n", idx.value);
		return 0;
	}
	if (vftr_td_ctx[enable_idx].en) {
		/* idx is enabled */
		SYS_TRACE("idx:%d is still enable, can not be deleted!\n", idx.value);
		return EAGAIN;
	}

	INT32 ret = 0;
	TD_CTX_S *ctx = TD_GET_CTX(enable_idx);

	pthread_mutex_lock(&ctx->lock);
	ret = VFTR_TD_deleteInstance(&ctx->instance);
	pthread_mutex_unlock(&ctx->lock);

	if (ret != 0) {
		SYS_TRACE("Free td instance failed!\n");
		return ret;
	}
	vftr_td_ctx[enable_idx].reg = 0;
	vftr_td_ctx[enable_idx].en = 0;

	pthread_mutex_lock(&ctx->cb_lock);
	vftr_td_ctx[enable_idx].cb = NULL;
	pthread_mutex_unlock(&ctx->cb_lock);

	return 0;
}

/**
 * @brief Enable tamper detection.
 * @param[in]  idx         video window index.
 * @see AVFTR_TD_disable
 * @retval 0                 success.
 * @retval ENODEV            idx is not registered yet
 */
int AVFTR_TD_enable(MPI_WIN idx)
{
	AVFTR_TD_CTX_S *vftr_td_ctx = vftr_res_shm->td_ctx;
	int enable_idx = findTdCtx(idx, vftr_td_ctx, NULL);

	if (enable_idx < 0) {
		/* idx is not registered */
		SYS_TRACE("idx:%d is not registered yet!\n", idx.value);
		return ENODEV;
	}

	if (vftr_td_ctx[enable_idx].en) {
		/* idx is enabled */
		SYS_TRACE("idx:%d is enabled, no need to enable again!\n", idx.value);
		return 0;
	}

	INT32 ret = 0;
	ret = vftrYAvgResDec();
	if (ret != 0) {
		SYS_TRACE("[WARN] All MPI YAVG ROI Resource are being used !\n");
		return ret;
	}

	TD_CTX_S *ctx = TD_GET_CTX(enable_idx);

	pthread_mutex_lock(&ctx->lock);
	ctx->is_reset = 1;
	//SYS_TRACE("[writer] enable TD success\n");
	pthread_mutex_unlock(&ctx->lock);

	pthread_mutex_lock(&ctx->cb_lock);
	if (vftr_td_ctx[enable_idx].cb == NULL) {
		//SYS_TRACE("Door Keerper alarm callback function is not registered on win %u.\n", idx.win);
		vftr_td_ctx[enable_idx].cb = alarmEmptyCb;
	}
	pthread_mutex_unlock(&ctx->cb_lock);

	vftr_td_ctx[enable_idx].en = 1;
	vftr_td_ctx[enable_idx].resource_registered = 0;

	return 0;
}

/**
 * @brief Disable tamper detection.
 * @param[in]  idx        video window index.
 * @see AVFTR_TD_enable
 * @retval 0                 success.
 * @retval ENODEV            idx is not registered yet
 */
int AVFTR_TD_disable(MPI_WIN idx)
{
	AVFTR_TD_CTX_S *vftr_td_ctx = vftr_res_shm->td_ctx;
	int enable_idx = findTdCtx(idx, vftr_td_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registerd yet, no need to disable\n", idx.value);
		return ENODEV;
	}

	if (!vftr_td_ctx[enable_idx].en) {
		/* idx is not enabled */
		SYS_TRACE("idx:%d is not enabled yet, no need to disable\n", idx.value);
		return 0;
	}

	vftrYAvgResInc();
	vftr_td_ctx[enable_idx].en = 0;

	return 0;
}

/**
 * @brief Get parameters of tamper detection.
 * @param[in]  idx        video window index.
 * @param[out] param      tamper detection parameters.
 * @see AVFTR_TD_setParam
 * @retval 0              success.
 * @retval ENODEV         idx is not registered yet
 */
int AVFTR_TD_getParam(MPI_WIN idx, AVFTR_TD_PARAM_S *param)
{
	AVFTR_TD_CTX_S *vftr_td_ctx = vftr_res_shm->td_ctx;
	int enable_idx = findTdCtx(idx, vftr_td_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registered\n", idx.value);
		return ENODEV;
	}

	TD_CTX_S *ctx = TD_GET_CTX(enable_idx);
	int ret;

	// Use this to check win state
	MPI_WIN_ATTR_S win_attr;
	ret = MPI_DEV_getWindowAttr(idx, &win_attr);
	if (ret != 0) {
		SYS_TRACE("Get window %u attributes failed.\n", idx.value);
		return ret;
	}
	param->td_param = ctx->param.td_param;

	return 0;
}

/**
 * @brief Set parameters of tamper detection.
 * @param[in]  idx        video window index.
 * @param[out] param      tamper detection parameters.
 * @param[in]  param      tamper detection parameters.
 * @see AVFTR_TD_getParam
 * @retval 0              success.
 * @retval ENODEV         idx is not registered yet
 */
int AVFTR_TD_setParam(MPI_WIN idx, const AVFTR_TD_PARAM_S *param)
{
	AVFTR_TD_CTX_S *vftr_td_ctx = vftr_res_shm->td_ctx;
	int enable_idx = findTdCtx(idx, vftr_td_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registered\n", idx.value);
		return ENODEV;
	}

	TD_CTX_S *ctx = TD_GET_CTX(enable_idx);

	int ret;

	ret = VFTR_TD_checkParam(&param->td_param);
	if (ret != 0) {
		return ret;
	}

	// Copy param to temp buffer and prepare to set to vftr_dk
	pthread_mutex_lock(&ctx->lock);
	ctx->param.td_param = param->td_param;
	ctx->is_write = 1;
	//SYS_TRACE("[AVFTR_TD] set TD param success\n");
	pthread_mutex_unlock(&ctx->lock);

	return 0;
}

/**
 * @brief Write parameters to TD Object 
 * @param[in]  idx        video window index.
 * @see AVFTR_TD_getParam
 * @retval 0              success.
 * @retval ENODEV         idx is not registered yet
 */
int AVFTR_TD_writeParam(MPI_WIN idx)
{
	AVFTR_TD_CTX_S *vftr_td_ctx = vftr_res_shm->td_ctx;
	int enable_idx = findTdCtx(idx, vftr_td_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		//SYS_TRACE("idx:%d is not registered\n", idx.value);
		return ENODEV;
	}

	TD_CTX_S *ctx = TD_GET_CTX(enable_idx);
	AVFTR_TD_PARAM_S *param = &ctx->param;

	int ret;

	// Use this to check win state
	MPI_WIN_ATTR_S win_attr;
	ret = MPI_DEV_getWindowAttr(idx, &win_attr);
	if (ret != 0) {
		SYS_TRACE("Get window %u attributes failed.\n", idx.value);
		return ret;
	}

	int is_write = ctx->is_write;
	if (is_write == 1) {
		pthread_mutex_lock(&ctx->lock);
		ret = VFTR_TD_setParam(ctx->instance, &param->td_param);
		ctx->is_write = 0;
		//SYS_TRACE("[reader] write TD param success, sensitivity is %d\n", param->td_param.sensitivity);
		pthread_mutex_unlock(&ctx->lock);
		if (ret != 0) {
			return ret;
		}
	}

	/* reset TD internal status */
	int is_reset = ctx->is_reset;
	if (is_reset == 1) {
		/* get input resolution & luma roi for TD */
		MPI_RECT_S roi;
		MPI_ROI_ATTR_S roi_attr;
		MPI_PATH_ATTR_S path_attr;

		ret = MPI_DEV_getPathAttr(ctx->path, &path_attr);
		if (ret != 0) {
			SYS_TRACE("Get path attr on configuration of window %u\n", idx.value);
			return ret;
		}

		ret = MPI_getRoiAttr(ctx->path, &roi_attr);
		if (ret != 0) {
			SYS_TRACE("Get roi attr on configuration of window %u\n", idx.value);
			return ret;
		}

		roi.width = (roi_attr.luma_roi.ex - roi_attr.luma_roi.sx) * path_attr.res.width / 1024;
		roi.height = (roi_attr.luma_roi.ey - roi_attr.luma_roi.sy) * path_attr.res.height / 1024;

		pthread_mutex_lock(&ctx->lock);
		ret = VFTR_TD_init(ctx->instance, &roi, (int)win_attr.fps);
		pthread_mutex_unlock(&ctx->lock);

		if (ret != 0) {
			SYS_TRACE("Init TD failed !\n");
			return ret;
		}
		pthread_mutex_lock(&ctx->lock);
		//SYS_TRACE("[reader] reset TD success\n");
		ctx->is_reset = 0;
		pthread_mutex_unlock(&ctx->lock);
	}

	return 0;
}

/**
 * @brief Register alarm callback function of tamper detection.
 * @param[in]  idx             video window index.
 * @param[in]  alarm_cb_fptr   function pointer of callback function.
 * @see none
 * @retval 0          success.
 * @retval EFAULT     NULL pointer of cb function 
 * @retval ENODEV     idx is not registered yet
 */
int AVFTR_TD_regCallback(MPI_WIN idx, const AVFTR_TD_ALARM_CB alarm_cb_fptr)
{
	if (alarm_cb_fptr == NULL) {
		SYS_TRACE("Pointer to tamper detection alarm callback function should not be NULL.\n");
		return EFAULT;
	}

	AVFTR_TD_CTX_S *vftr_td_ctx = vftr_res_shm->td_ctx;
	int enable_idx = findTdCtx(idx, vftr_td_ctx, NULL);

	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registered\n", idx.value);
		return ENODEV;
	}

	TD_CTX_S *ctx = TD_GET_CTX(enable_idx);

	pthread_mutex_lock(&ctx->cb_lock);
	vftr_td_ctx[enable_idx].cb = alarm_cb_fptr;
	pthread_mutex_unlock(&ctx->cb_lock);

	return 0;
}

/**
 * @brief Reset registered model of tamper detection.
 * @param[in]  idx             video window index.
 * @see none
 * @retval MPI_SUCCESS              success.
 * @retval MPI_ERR_DEV_INVALID_WIN  invalid video window index.
 * @retval MPI_ERR_DEV_INVALID_CHN  invalid video channel index.
 * @retval MPI_ERR_DEV_INVALID_DEV  invalid device index.
 * @retval MPI_ERR_NOT_EXIST        device/channel doesn't exist.
 * @retval MPI_ERR_NOT_PERM         tamper detection hasn't been enabled.
 * @retval MPI_FAILURE              unexpected fail.
 */
int AVFTR_TD_reset(MPI_WIN idx)
{
	AVFTR_TD_CTX_S *vftr_td_ctx = vftr_res_shm->td_ctx;
	int enable_idx = findTdCtx(idx, vftr_td_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registered\n", idx.value);
		return ENODEV;
	}

	TD_CTX_S *ctx = TD_GET_CTX(enable_idx);

	pthread_mutex_lock(&ctx->lock);
	ctx->is_reset = 1;
	//SYS_TRACE("[AVFTR_TD] set reset TD success\n");
	pthread_mutex_unlock(&ctx->lock);

	return 0;
}

int AVFTR_TD_resetShm(MPI_WIN idx)
{
	/*  FIXME: by win used
	int enable_idx = findTdCtx(idx, vftr_td_ctx, NULL);

	if (enable_idx < 0) {
		return -1;
	}

	if (vftr_td_ctx[enable_idx].en) {
		vftr_td_ctx[enable_idx].td_res.alarm = 0;
	}
	*/
	AVFTR_TD_CTX_S *vftr_td_ctx = vftr_res_shm->td_ctx;
	int i, j;

	for (i = 0; i < AVFTR_TD_MAX_SUPPORT_NUM; i++) {
		if (vftr_td_ctx[i].en) {
			for (j = 0; j < AVFTR_VIDEO_RING_BUF_SIZE; j++)
				vftr_td_ctx[i].td_res[j].alarm = 0;
		}
	}

	return 0;
}

int AVFTR_TD_resume(MPI_WIN idx)
{
	AVFTR_TD_CTX_S *vftr_td_ctx = vftr_res_shm->td_ctx;

	/* FIXME: not support by win for notify
	int enable_idx = findTdCtx(idx, vftr_td_ctx, NULL);

	if (enable_idx < 0) {
		return -1;
	}

	if (vftr_td_ctx[enable_idx].en) {
		TD_resetData(vftr_td_ctx[enable_idx].idx);
	}
	*/

	int i;
	for (i = 0; i < AVFTR_TD_MAX_SUPPORT_NUM; i++) {
		if (vftr_td_ctx[i].en) {
			int enable_idx = findTdCtx(vftr_td_ctx[i].idx, vftr_td_ctx, NULL);
			if (enable_idx < 0) {
				/* idx is not registered yet */
				SYS_TRACE("idx:%d is not registered\n", idx.value);
				return ENODEV;
			}

			TD_CTX_S *ctx = TD_GET_CTX(enable_idx);
			pthread_mutex_lock(&ctx->lock);
			ctx->is_reset = 1;
			//SYS_TRACE("[AVFTR_TD] set reset TD success\n");
			pthread_mutex_unlock(&ctx->lock);
		}
	}

	return 0;
}

int AVFTR_TD_regMpiInfo(MPI_WIN idx)
{
	AVFTR_TD_CTX_S *vftr_td_ctx = vftr_res_shm->td_ctx;
	int enable_idx = findTdCtx(idx, vftr_td_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		SYS_TRACE("idx:%d is not registerd yet, can not register mpi info\n", idx.value);
		return ENODEV;
	}

	if (vftr_td_ctx[enable_idx].resource_registered) {
		SYS_TRACE("Resource of TD has been registed.\n");
		return 0;
	}

	TD_CTX_S *ctx = TD_GET_CTX(enable_idx);

	int ret;

	MPI_CHN_LAYOUT_S chn_layout;
	MPI_CHN chn = (MPI_CHN){ .value = idx.value };
	if (MPI_DEV_getChnLayout(chn, &chn_layout) != 0) {
		SYS_TRACE("Get video channel %u layout failed.\n", chn.value);
		return -1;
	}
	int i = 0;
	for (i = 0; i < chn_layout.window_num; i++) {
		if (idx.value == chn_layout.win_id[i].value) {
			break;
		}
	}
	UINT32 win_idx = MPI_GET_VIDEO_WIN(idx);
	if (i == chn_layout.window_num) {
		SYS_TRACE("Invalid video window index %d from video channel %d", win_idx, idx.chn);
		return -1;
	}

	UINT32 dev_idx = MPI_GET_VIDEO_DEV(idx);
	MPI_WIN_ATTR_S win_attr;
	ret = MPI_DEV_getWindowAttr(idx, &win_attr);
	if (ret != 0) {
		SYS_TRACE("Get window attributes failed.\n");
		return ENODEV;
	}

	int path_idx;
	if (win_attr.path.bit.path0_en) {
		path_idx = 0;
	} else if (win_attr.path.bit.path1_en) {
		path_idx = 1;
	} else {
		SYS_TRACE("Wrong path bmp %d setting\n", win_attr.path.bmp);
		return EINVAL;
	}

	ctx->path = MPI_INPUT_PATH(dev_idx, path_idx);
	vftr_td_ctx[enable_idx].resource_registered = 1;
	return 0;
}

int AVFTR_TD_releaseMpiInfo(MPI_WIN idx)
{
	AVFTR_TD_CTX_S *vftr_td_ctx = vftr_res_shm->td_ctx;
	int enable_idx = findTdCtx(idx, vftr_td_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered */
		SYS_TRACE("idx:%d is not registered yet!\n", idx.value);
		return 0;
	}
	if (vftr_td_ctx[enable_idx].en) {
		/* idx is enabled */
		SYS_TRACE("idx:%d is still enable, ROI can not be deleted!\n", idx.value);
		return EAGAIN;
	}

	if (!vftr_td_ctx[enable_idx].resource_registered) {
		//SYS_TRACE("Resource of TD has not been registered.\n");
		return 0;
	}

	vftr_td_ctx[enable_idx].resource_registered = 0;

	return 0;
}

int AVFTR_TD_updateMpiInfo(MPI_WIN idx)
{
	AVFTR_TD_CTX_S *vftr_td_ctx = vftr_res_shm->td_ctx;
	int enable_idx = findTdCtx(idx, vftr_td_ctx, NULL);
	if (enable_idx < 0) {
		/* idx is not registered yet */
		//SYS_TRACE("idx:%d is not registerd yet, can not get mpi info\n", idx.value);
		return ENODEV;
	}

	if (!vftr_td_ctx[enable_idx].en) {
		/* idx is not enabled */
		// SYS_TRACE("idx:%d is not enabled yet, no need to get mpi info\n", idx.value);
		return 0;
	}

	if (!vftr_td_ctx[enable_idx].resource_registered) {
		SYS_TRACE("MPI Resource of TD has not been registered yet!\n");
		return 0;
	}

	TD_CTX_S *ctx = TD_GET_CTX(enable_idx);
	INT32 ret = 0;

	ret = MPI_getStatistics(ctx->path, &ctx->mpi_input.dip_stat);
	if (ret != 0) {
		SYS_TRACE("Get IS stat on configuration of window %u\n", idx.value);
		return ret;
	}

	return 0;
}
