#ifndef VIDEO_PFM_H_
#define VIDEO_PFM_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "avftr_common.h"
#include "video_od.h"
#include "pfm.h"
#include "mpi_base_types.h"
#include <stdint.h>

#define VIDEO_PFM_SCHEDULE_MAX_NUM 10
#define VIDEO_PFM_SEC_PER_DAY 86400
#define VIDEO_PFM_REGIS_INTERVAL_MAX 3600

/**
 * @brief Callback function type of video pet feeding monitoringalarm.
 */
typedef VOID (*VIDEO_FTR_PFM_ALARM_CB)(void);

/**
 * @brief Structure of video pet feeding monitoringattributes.
 */
typedef struct {
	PFM_DATA_S data;
	MPI_RECT_POINT_S roi;
} VIDEO_PFM_DATA_S;

/**
 * @brief Structure of video pet feeding monitoringattributes.
 */
typedef struct {
	UINT8 en; /**< Enable Pet Feeding Monitoring*/
	MPI_WIN idx; /**< Window index */
	VIDEO_FTR_PFM_ALARM_CB cb; /**< Callback function when alarm triggered*/
	VIDEO_PFM_DATA_S stat[AVFTR_VIDEO_RING_BUF_SIZE];
} VIDEO_PFM_CTX_S;

/**
 * @brief Callback function type of video pet feeding monitoringalarm.
 */
typedef struct {
	UINT8 time_num;
	UINT32 times[VIDEO_PFM_SCHEDULE_MAX_NUM]; /**< Absolute time(s) per day [0-86399]*/
	UINT32 regisBg_feed_interval; /**< Time interval(s) between background registration and feeding*/
} VIDEO_PFM_SCHEDULE_S;

/**
 * @brief Callback function type of video pet feeding monitoringalarm.
 */
typedef struct {
	PFM_PARAM_S pfm_param;
	VIDEO_PFM_SCHEDULE_S schedule;
} VIDEO_FTR_PFM_PARAM_S;

int VIDEO_FTR_getPfmStat(MPI_WIN idx, VIDEO_PFM_CTX_S *vftr_pfm_ctx);
int VIDEO_FTR_getPfmRes(MPI_WIN idx, UINT32 timestamp, int buf_idx);
int VIDEO_FTR_enablePfm(MPI_WIN idx);
int VIDEO_FTR_disablePfm(MPI_WIN idx);
int VIDEO_FTR_getPfmParam(MPI_WIN idx, VIDEO_FTR_PFM_PARAM_S *param);
int VIDEO_FTR_setPfmParam(MPI_WIN idx, const VIDEO_FTR_PFM_PARAM_S *param);
int VIDEO_FTR_regPfmCallback(MPI_WIN idx, const VIDEO_FTR_PFM_ALARM_CB alarm_cb_fptr);
int VIDEO_FTR_transPfmRes(VIDEO_PFM_CTX_S *vftr_pfm_ctx, MPI_WIN src_idx, MPI_WIN dst_idx, const MPI_RECT_S *src_rect,
                          const MPI_RECT_S *dst_rect, const MPI_RECT_S *src_roi, const MPI_RECT_S *dst_roi, char *str, int buf_idx);
int VIDEO_FTR_resetPfmData(MPI_WIN idx, PFM_RESET_E reset_val);
int VIDEO_FTR_resumePfm(MPI_WIN idx);
int VIDEO_FTR_resetPfmShm(MPI_WIN idx);

#ifdef __cplusplus
}
#endif

#endif /* !VIDEO_PFM_H_ */
