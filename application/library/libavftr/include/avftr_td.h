#ifndef AVFTR_TD_H_
#define AVFTR_TD_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "vftr_td.h"

#include <stdint.h>

#include "avftr_common.h"
#include "mpi_base_types.h"

/**
 * @brief Callback function type of video tamper detection alarm.
 */
typedef VOID (*AVFTR_TD_ALARM_CB)(void);

typedef struct {
	VFTR_TD_PARAM_S td_param;
} AVFTR_TD_PARAM_S;

/**
 * @brief Structure of video tamper detection attributes.
 */
typedef struct {
	UINT8 en; /**< Enable status of tamper detection*/
	UINT8 reg; /**< Flag for instance registration */
	UINT8 resource_registered; /* Flag for resource registration */
	MPI_WIN idx; /**< window index */
	AVFTR_TD_ALARM_CB cb; /**< Callback function when alarm triggered*/
	VFTR_TD_STATUS_S td_res[AVFTR_VIDEO_RING_BUF_SIZE];
} AVFTR_TD_CTX_S;

int AVFTR_TD_getStat(MPI_WIN idx, AVFTR_TD_CTX_S *vftr_td_ctx);
int AVFTR_TD_getRes(MPI_WIN idx, int buf_idx);
int AVFTR_TD_transRes(AVFTR_TD_CTX_S *vftr_td_ctx, MPI_WIN src_idx, MPI_WIN dst_idx, const MPI_RECT_S *src_rect,
                      const MPI_RECT_S *dst_rect, const MPI_RECT_S *src_roi, const MPI_RECT_S *dst_roi, char *str,
                      int buf_idx);
int AVFTR_TD_addInstance(MPI_WIN idx);
int AVFTR_TD_deleteInstance(MPI_WIN idx);
int AVFTR_TD_enable(MPI_WIN idx);
int AVFTR_TD_disable(MPI_WIN idx);
int AVFTR_TD_getParam(MPI_WIN idx, AVFTR_TD_PARAM_S *param);
int AVFTR_TD_setParam(MPI_WIN idx, const AVFTR_TD_PARAM_S *param);
int AVFTR_TD_writeParam(MPI_WIN idx);

int AVFTR_TD_regCallback(MPI_WIN idx, const AVFTR_TD_ALARM_CB alarm_cb_fptr);
int AVFTR_TD_reset(MPI_WIN idx);
int AVFTR_TD_suppress(MPI_WIN idx);
int AVFTR_TD_resume(MPI_WIN idx);
int AVFTR_TD_resetShm(MPI_WIN idx);

int AVFTR_TD_regMpiInfo(MPI_WIN idx);
int AVFTR_TD_releaseMpiInfo(MPI_WIN idx);
int AVFTR_TD_updateMpiInfo(MPI_WIN idx);

#ifdef __cplusplus
}
#endif

#endif /* !VIDEO_TD_H_ */
