#ifndef VIDEO_BM_H_
#define VIDEO_BM_H_

#ifdef __cplusplus
extern "C" {
#endif

#define BM_SHOW_FD_REPORT

#include "fd.h"
#include "mpi_base_types.h"

#include "avftr_common.h"

typedef enum {
	VIDEO_BM_EVENT_ABSENT = 0,
	VIDEO_BM_EVENT_ACTIVE,
	VIDEO_BM_EVENT_AWAKE,
	VIDEO_BM_EVENT_SLEEP,
	VIDEO_BM_EVENT_BOUNDARY,
	VIDEO_BM_EVENT_ENTERING,
	VIDEO_BM_EVENT_LEAVING
} VIDEO_FTR_BM_EVENT_E;

typedef struct {
	UINT32 duration_active;
	UINT32 duration_awake;
	UINT32 duration_sleep;
	VIDEO_FTR_BM_EVENT_E current_evt;
	MPI_RECT_S roi;
	FD_STATUS_S fd_stat;
} VIDEO_FTR_BM_STATUS_S;

typedef struct {
	FD_PARAM_S fd_param;
} VIDEO_FTR_BM_PARAM_S;

typedef VOID (*VIDEO_FTR_BM_ALARM_CB)(MPI_WIN idx,
	VIDEO_FTR_BM_EVENT_E evt,
	const VIDEO_FTR_BM_PARAM_S *param);

typedef struct {
	UINT8 en;
	MPI_WIN idx;
	VIDEO_FTR_BM_ALARM_CB cb;
	VIDEO_FTR_BM_STATUS_S bm_res[AVFTR_VIDEO_RING_BUF_SIZE];
} VIDEO_BM_CTX_S;

typedef FD_DATA_CTRL_E VIDEO_BM_DATA_CTRL_E;

int VIDEO_FTR_getBmStat(MPI_WIN idx, VIDEO_BM_CTX_S *vftr_bm_ctx);
int VIDEO_FTR_getBmRes(MPI_WIN idx, int buf_idx);
int VIDEO_FTR_transBmRes(VIDEO_BM_CTX_S *vftr_bm_ctx, MPI_WIN src_idx, MPI_WIN dst_idx,
                         const MPI_RECT_S *src_rect, const MPI_RECT_S *dst_rect, const MPI_RECT_S *src_roi,
                         const MPI_RECT_S *dst_roi, char *str, int buf_idx);
int VIDEO_FTR_enableBm(MPI_WIN idx);
int VIDEO_FTR_disableBm(MPI_WIN idx);
int VIDEO_FTR_getBmParam(MPI_WIN idx, VIDEO_FTR_BM_PARAM_S *param);
int VIDEO_FTR_setBmParam(MPI_WIN idx, const VIDEO_FTR_BM_PARAM_S *param);
int VIDEO_FTR_regBmCallback(MPI_WIN idx, const VIDEO_FTR_BM_ALARM_CB alarm_cb_fptr);
int VIDEO_FTR_resetBmData(MPI_WIN idx);
int VIDEO_FTR_suppressBm(MPI_WIN idx);
int VIDEO_FTR_resumeBm(MPI_WIN idx);
int VIDEO_FTR_resetBmShm(MPI_WIN idx);
int VIDEO_FTR_ctrlBmData(MPI_WIN idx, const char *data_path, VIDEO_BM_DATA_CTRL_E ctrl);

#ifdef __cplusplus
}
#endif

#endif /* VIDEO_BM_H_ */