#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <getopt.h>
#include <time.h>

#include "libosd.h"

int main(int argc, char **argv)
{
	OSDLOG("%s start\r\n", __FILE__);
	return 0;
}

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif