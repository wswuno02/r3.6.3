#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <stdbool.h>

#include "libosd.h"
#include "txt2ayuv.h"
#include "drawline.h"

int OSD_init()
{
	OSDERR("init\r\n");
	return 0;
}

int OSD_deinit()
{
	OSDERR("deinit\r\n");
	return 0;
}

OsdHandle *OSD_create(int width, int height)
{
	OsdHandle *ptr = malloc(sizeof(OsdHandle));
	if (ptr == NULL) {
		OSDERR("failed to create OSD handle\r\n");
		return NULL;
	}
	memset(ptr, 0xff, sizeof(OsdHandle));
	ptr->width = width;
	ptr->height = height;
	ptr->osd_num = 0;
	for (int i = 0; i < MAX_CANVAS; i++) {
		for (int j = 0; j < MAX_OSD; j++) {
			ptr->canvas[i].osd_list[j] = 0xff;
		}
	}

	for (int i = 0; i < MAX_OSD; i++) {
		ptr->osd_index[i] = -1;
	}

	OSDLOG("create (%d, %d)\r\n", width, height);

	return ptr;
}

uint32_t CEILINGALIGN16(uint32_t x)
{
	uint32_t tmp = 0;
	tmp = (x + 15) / 16;
	tmp = tmp * 16;
	return tmp;
}

uint32_t ALIGN16(uint32_t x)
{
	uint32_t tmp = 0;
	tmp = x / 16;
	tmp = tmp * 16;

	return tmp;
}

int OSD_destroy(OsdHandle *phd)
{
	OSDLOG("detroy handle(%d, %d)\r\n", phd->width, phd->height);
	free(phd);

	return 0;
}

int OSD_addOsd(OsdHandle *phd, int osd_idx, OsdRegion *region)
{
	if (phd->osd_num == MAX_OSD) {
		OSDERR("handle has 8 osd\r\n");
		return -EINVAL;
	}

	if (region->startX > phd->width) {
		OSDERR("startX exceed handle width\r\n");
		return -EINVAL;
	}

	if (region->startY > phd->height) {
		OSDERR("startY exceed handle height\r\n");
		return -EINVAL;
	}

	if (CEILINGALIGN16(region->startX + region->width) > phd->width) {
		OSDERR("canvas > width after align 16\r\n");
		return -EINVAL;
	}

	if (CEILINGALIGN16(region->startY + region->height) > phd->height) {
		OSDERR("canvas > height after align 16\r\n");
		return -EINVAL;
	}

	if (phd->osd_index[osd_idx] == -1) {
		phd->osd_num += 1;
		phd->osd_index[osd_idx] = osd_idx;
	}

	memcpy(&phd->region[osd_idx], region, sizeof(OsdRegion));

	OSDERR("add [%d] (%d, %d, %d, %d) to hd, now has region:[%d]\r\n", osd_idx, region->startX, region->startY,
	       region->width, region->height, phd->osd_num - 1);
	return 0;
}

int OSD_delOsd(OsdHandle *phd, int osd_idx)
{
	if (phd->osd_num == 0) {
		OSDERR("handle has no osd\r\n");
		return -EINVAL;
	}

	if ((osd_idx > MAX_OSD) || (osd_idx < 0)) {
		OSDERR("invalid del idx: %d\r\n", osd_idx);
		return -EINVAL;
	}

	int tmp_idx = -1;

	for (int i = 0; i < MAX_OSD; ++i) {
		if (phd->osd_index[i] == osd_idx) {
			tmp_idx = i;
			OSDLOG("get osd_index[%d]: %d\r\n", i, osd_idx);
			break;
		}
	}

	if (tmp_idx == -1) {
		OSDERR("can't find this idx:%d\r\n", osd_idx);
		return -EINVAL;
	}

	OSDERR("del [%d] from hd->region[%d]\r\n", osd_idx, tmp_idx);

	phd->osd_num -= 1;
	phd->osd_index[tmp_idx] = -1;
	memset(&phd->region[tmp_idx], 0xff, sizeof(OsdRegion));

	return 0;
}

int OSD_calcCanvasbygroup(OsdHandle *phd)
{
	if (phd->osd_num == 0) {
		OSDERR("handle has no osd\r\n");
		return -EINVAL;
	}

	/*0-3: (0,1,2,3) or all*/
	OsdHandle tmp_hd[2];
	memset(&tmp_hd[0], 0xff, sizeof(OsdHandle) * 2);
	int tmp_area[2] = { 0, 0 };

	for (int i = 0; i < MAX_CANVAS; i++) {
		memset(&phd->canvas[i], 0xff, sizeof(OsdCanvas));
	}

	/*grouping algo*/
	int add_list[MAX_OSD] = { 0 };
	int add_idx = 0;
	for (int i = 0; i < MAX_OSD; i++) {
		if (phd->osd_index[i] != -1) {
			OSDLOG("add %d\r\n", phd->osd_index[i]);
			add_list[add_idx] = phd->osd_index[i];
			add_idx++;
		}
	}

	if (add_idx > phd->osd_num) {
		OSDERR("add idx != region num: %d, %d\r\n", add_idx, phd->osd_num);
		return -EINVAL;
	}

	for (int i = 0; i < add_idx; i++) {
		phd->region[add_list[i]].include_canvas = 0;
	}

	for (int i = 0; i < MAX_CANVAS; i++) {
		OSDLOG("First add reg %d (%d, %d, %d, %d)\r\n", add_list[i], phd->region[add_list[i]].startX,
		       phd->region[add_list[i]].startY, phd->region[add_list[i]].width,
		       phd->region[add_list[i]].height);
		tmp_hd[0].canvas[i].osd_num = 1;
		tmp_hd[0].canvas[i].osd_list[0] = add_list[i];
		tmp_hd[0].canvas[i].startX = ALIGN16(phd->region[add_list[i]].startX);
		tmp_hd[0].canvas[i].startY = ALIGN16(phd->region[add_list[i]].startY);
		uint32_t width_tmp = phd->region[add_list[i]].startX + phd->region[add_list[i]].width;
		OSDLOG("width tmp : %d\r\n", width_tmp);
		width_tmp = width_tmp - tmp_hd[0].canvas[i].startX;
		OSDLOG("width tmp : %d\r\n", width_tmp);
		tmp_hd[0].canvas[i].width = CEILINGALIGN16(width_tmp);
		uint32_t height_tmp = phd->region[add_list[i]].startY + phd->region[add_list[i]].height;
		OSDLOG("height tmp : %d\r\n", height_tmp);
		height_tmp = height_tmp - tmp_hd[0].canvas[i].startY;
		OSDLOG("height tmp : %d, start y = %d\r\n", height_tmp, phd->canvas[i].startY);
		tmp_hd[0].canvas[i].height = CEILINGALIGN16(height_tmp);
		tmp_hd[0].region[add_list[i]].include_canvas = i;
		tmp_area[0] += tmp_hd[0].canvas[i].width * tmp_hd[0].canvas[i].height;
		OSDLOG("[%d] num:%d, %d, (%d, %d, %d, %d) = %d\r\n", i, tmp_hd[0].canvas[i].osd_num,
		       tmp_hd[0].canvas[i].osd_list[0], tmp_hd[0].canvas[i].startX, tmp_hd[0].canvas[i].startY,
		       tmp_hd[0].canvas[i].width, tmp_hd[0].canvas[i].height, tmp_area[0]);
	}

	tmp_hd[1].canvas[0].osd_num = 4;
	int startx_list[4];
	int starty_list[4];
	int endx_list[4];
	int endy_list[4];
	for (int i = 0; i < 4; i++) {
		tmp_hd[1].canvas[0].osd_list[i] = add_list[i];
		tmp_hd[1].region[i].include_canvas = 0;
		startx_list[i] = ALIGN16(phd->region[add_list[i]].startX);
		starty_list[i] = ALIGN16(phd->region[add_list[i]].startY);
		endx_list[i] = CEILINGALIGN16(phd->region[add_list[i]].startX + phd->region[add_list[i]].width);
		endy_list[i] = CEILINGALIGN16(phd->region[add_list[i]].startY + phd->region[add_list[i]].height);
	}
	tmp_hd[1].canvas[0].startX = get_min(&startx_list[0], 4);
	tmp_hd[1].canvas[0].startY = get_min(&starty_list[0], 4);
	tmp_hd[1].canvas[0].width = get_max(&endx_list[0], 4) - tmp_hd[1].canvas[0].startX;
	tmp_hd[1].canvas[0].height = get_max(&endy_list[0], 4) - tmp_hd[1].canvas[0].startY;
	tmp_area[1] = tmp_hd[1].canvas[0].width * tmp_hd[1].canvas[0].height;
	OSDLOG("case 2: (%d, %d, %d, %d) area; %d\r\n", tmp_hd[1].canvas[0].startX, tmp_hd[1].canvas[0].startY,
	       tmp_hd[1].canvas[0].width, tmp_hd[1].canvas[0].height, tmp_area[1]);

	if (tmp_area[0] < tmp_area[1]) {
		OSDLOG("select 0\r\n");
		memcpy(&phd->canvas[0], &tmp_hd[0].canvas[0], sizeof(OsdCanvas) * 4);
		for (int i = 0; i < MAX_OSD; i++) {
			phd->region[i].include_canvas = tmp_hd[0].region[i].include_canvas;
		}
	} else {
		OSDLOG("select 1\r\n");
		memcpy(&phd->canvas[0], &tmp_hd[1].canvas[0], sizeof(OsdCanvas) * 4);
		for (int i = 0; i < 4; i++) {
			phd->region[i].include_canvas = 0;
		}
	}

#ifdef OSD_DEBUG
	logAllOsdHandle(phd);
#endif
	/*add region[4]*/
	OsdHandle tmp_hd_loop[11];
	int tmp_area_loop[11];
	int loop_idx = 0;
	memset(&tmp_hd_loop[0], 0, sizeof(tmp_hd_loop));
	memset(&tmp_area_loop[0], 0, sizeof(tmp_area_loop));

	/*0,1 means 0,1 merge, new add to 1, 2,3,4 copy*/
	/*0,2 means 0,2 merge, new add to 2, 1,3,4 copy*/
	int old_num, new_num;
	int compare_list[2];
	int new_calc_num = 4;

	for (new_calc_num = 4; new_calc_num < phd->osd_num; new_calc_num++) {
		OSDLOG("\r\n add: [%d]: idx: %d\r\n", new_calc_num, add_list[new_calc_num]);
		for (int merge_num = 0; merge_num < MAX_CANVAS; merge_num++) {
			for (int add_num = merge_num + 1; add_num < MAX_CANVAS + 1; add_num++) {
#ifdef OSD_DEBUG
				printf("[%d]merge num: %d, add num: %d , other num: ", loop_idx, merge_num, add_num);
#endif
				memcpy(&tmp_hd_loop[loop_idx].region[0], &phd->region[0], sizeof(OsdRegion) * MAX_OSD);
				tmp_hd_loop[loop_idx].osd_num = phd->osd_num;

				/*merge with new add*/
				if (add_num + 1 == 5) {
					/* Add osd_list to bottom */
					old_num = phd->canvas[merge_num].osd_num;
					new_num = old_num + 1; /*only new one*/
					tmp_hd_loop[loop_idx].canvas[merge_num].osd_num = new_num;

					/* copy osd_list */
					memcpy(&tmp_hd_loop[loop_idx].canvas[merge_num].osd_list[0],
					       &phd->canvas[merge_num].osd_list[0],
					       sizeof(uint8_t) * phd->canvas[merge_num].osd_num);
					tmp_hd_loop[loop_idx].canvas[merge_num].osd_list[old_num] =
					        add_list[new_calc_num];
#ifdef OSD_DEBUG
					//logAllOsdHandle(&tmp_hd_loop[loop_idx]);
#endif
					/* calc new rect (x,y,w,h)*/
					compare_list[0] = (phd->canvas[merge_num].startX);
					compare_list[1] = (phd->region[add_list[new_calc_num]].startX);
					tmp_hd_loop[loop_idx].canvas[merge_num].startX = get_min(&compare_list[0], 2);

					compare_list[0] = (phd->canvas[merge_num].startY);
					compare_list[1] = (phd->region[add_list[new_calc_num]].startY);
					tmp_hd_loop[loop_idx].canvas[merge_num].startY = get_min(&compare_list[0], 2);

					compare_list[0] =
					        (phd->canvas[merge_num].startX + phd->canvas[merge_num].width);
					compare_list[1] = (phd->region[add_list[new_calc_num]].startX +
					                   phd->region[add_list[new_calc_num]].width);
					tmp_hd_loop[loop_idx].canvas[merge_num].width =
					        get_max(&compare_list[0], 2) -
					        tmp_hd_loop[loop_idx].canvas[merge_num].startX;

					compare_list[0] =
					        (phd->canvas[merge_num].startY + phd->canvas[merge_num].height);
					compare_list[1] = (phd->region[add_list[new_calc_num]].startY +
					                   phd->region[add_list[new_calc_num]].height);
					tmp_hd_loop[loop_idx].canvas[merge_num].height =
					        get_max(&compare_list[0], 2) -
					        tmp_hd_loop[loop_idx].canvas[merge_num].startY;

					/* merge together, then use add_num space to add new one*/
					int region_id;
					for (int i = 0; i < tmp_hd_loop[loop_idx].canvas[merge_num].osd_num; i++) {
						region_id = tmp_hd_loop[loop_idx].canvas[merge_num].osd_list[i];
						tmp_hd_loop[loop_idx].region[region_id].include_canvas = merge_num;
					}

					/*copy other canvas as usual*/
					for (int other = 0; other < MAX_CANVAS; other++) {
						if ((other != merge_num) && (other != add_num)) {
#ifdef OSD_DEBUG
							printf(" %d", other);
#endif
							memcpy(&tmp_hd_loop[loop_idx].canvas[other],
							       &phd->canvas[other], sizeof(OsdCanvas));
						}
					}

				} else {
					/* Add osd_list to bottom */
					old_num = phd->canvas[merge_num].osd_num;
					new_num = phd->canvas[merge_num].osd_num + phd->canvas[add_num].osd_num;
					tmp_hd_loop[loop_idx].canvas[merge_num].osd_num = new_num;

					/* copy osd_list */
					memcpy(&tmp_hd_loop[loop_idx].canvas[merge_num].osd_list[0],
					       &phd->canvas[merge_num].osd_list[0],
					       sizeof(uint8_t) * phd->canvas[merge_num].osd_num);
					memcpy(&tmp_hd_loop[loop_idx].canvas[merge_num].osd_list[old_num],
					       &phd->canvas[add_num].osd_list[0],
					       sizeof(uint8_t) * phd->canvas[add_num].osd_num);
#ifdef OSD_DEBUG
					//logAllOsdHandle(&tmp_hd_loop[loop_idx]);
#endif

					/* calc new rect (x,y,w,h)*/
					compare_list[0] = (phd->canvas[merge_num].startX);
					compare_list[1] = (phd->canvas[add_num].startX);
					tmp_hd_loop[loop_idx].canvas[merge_num].startX = get_min(&compare_list[0], 2);

					compare_list[0] = (phd->canvas[merge_num].startY);
					compare_list[1] = (phd->canvas[add_num].startY);
					tmp_hd_loop[loop_idx].canvas[merge_num].startY = get_min(&compare_list[0], 2);

					compare_list[0] =
					        (phd->canvas[merge_num].startX + phd->canvas[merge_num].width);
					compare_list[1] = (phd->canvas[add_num].startX + phd->canvas[add_num].width);
					tmp_hd_loop[loop_idx].canvas[merge_num].width =
					        get_max(&compare_list[0], 2) -
					        tmp_hd_loop[loop_idx].canvas[merge_num].startX;

					compare_list[0] =
					        (phd->canvas[merge_num].startY + phd->canvas[merge_num].height);
					compare_list[1] = (phd->canvas[add_num].startY + phd->canvas[add_num].height);
					tmp_hd_loop[loop_idx].canvas[merge_num].height =
					        get_max(&compare_list[0], 2) -
					        tmp_hd_loop[loop_idx].canvas[merge_num].startY;

					/* merge together, then use add_num space to add new one*/
					int region_id;
					for (int i = 0; i < tmp_hd_loop[loop_idx].canvas[merge_num].osd_num; i++) {
						region_id = tmp_hd_loop[loop_idx].canvas[merge_num].osd_list[i];
						tmp_hd_loop[loop_idx].region[region_id].include_canvas = merge_num;
					}

					/*copy new to add_num*/
					tmp_hd_loop[loop_idx].canvas[add_num].startX =
					        phd->region[add_list[new_calc_num]].startX;
					tmp_hd_loop[loop_idx].canvas[add_num].startY =
					        phd->region[add_list[new_calc_num]].startY;
					tmp_hd_loop[loop_idx].canvas[add_num].width =
					        phd->region[add_list[new_calc_num]].width;
					tmp_hd_loop[loop_idx].canvas[add_num].height =
					        phd->region[add_list[new_calc_num]].height;
					tmp_hd_loop[loop_idx].canvas[add_num].osd_num = 1;
					tmp_hd_loop[loop_idx].canvas[add_num].osd_list[0] = add_list[new_calc_num];
					tmp_hd_loop[loop_idx].region[add_list[new_calc_num]].include_canvas = add_num;

					/*copy other canvas as usual*/
					for (int other = 0; other < MAX_CANVAS; other++) {
						if ((other != merge_num) && (other != add_num)) {
#ifdef OSD_DEBUG
							printf(" %d", other);
#endif
							memcpy(&tmp_hd_loop[loop_idx].canvas[other],
							       &phd->canvas[other], sizeof(OsdCanvas));
						}
					}
				}

				/*calc area of this loop*/
				for (int i = 0; i < MAX_CANVAS; i++) {
					tmp_area_loop[loop_idx] += tmp_hd_loop[loop_idx].canvas[i].width *
					                           tmp_hd_loop[loop_idx].canvas[i].height;
				}

#ifdef OSD_DEBUG
				printf(", area: %d\r\n", tmp_area_loop[loop_idx]);
				logAllOsdHandle(&tmp_hd_loop[loop_idx]);
#endif
				loop_idx += 1;
			}
		}

		/*all*/
		tmp_hd_loop[10].canvas[0].osd_num = new_calc_num + 1;
		int startx_list_all[new_calc_num + 1];
		int starty_list_all[new_calc_num + 1];
		int endx_list_all[new_calc_num + 1];
		int endy_list_all[new_calc_num + 1];
		for (int i = 0; i < new_calc_num + 1; i++) {
			tmp_hd_loop[10].canvas[0].osd_list[i] = phd->osd_index[add_list[i]];
			tmp_hd_loop[10].region[add_list[i]].include_canvas = 0;
			startx_list_all[i] = (phd->region[add_list[i]].startX);
			starty_list_all[i] = (phd->region[add_list[i]].startY);
			endx_list_all[i] = (phd->region[add_list[i]].startX + phd->region[add_list[i]].width);
			endy_list_all[i] = (phd->region[add_list[i]].startY + phd->region[add_list[i]].height);
		}
		tmp_hd_loop[10].canvas[0].startX = get_min(&startx_list_all[0], new_calc_num + 1);
		tmp_hd_loop[10].canvas[0].startY = get_min(&starty_list_all[0], new_calc_num + 1);
		tmp_hd_loop[10].canvas[0].width =
		        get_max(&endx_list_all[0], new_calc_num + 1) - tmp_hd_loop[10].canvas[0].startX;
		tmp_hd_loop[10].canvas[0].height =
		        get_max(&endy_list_all[0], new_calc_num + 1) - tmp_hd_loop[10].canvas[0].startY;
		tmp_area_loop[10] = tmp_hd_loop[10].canvas[0].width * tmp_hd_loop[10].canvas[0].height;
		OSDLOG("case all: (%d, %d, %d, %d) area; %d\r\n", tmp_hd_loop[10].canvas[0].startX,
		       tmp_hd_loop[10].canvas[0].startY, tmp_hd_loop[10].canvas[0].width,
		       tmp_hd_loop[10].canvas[0].height, tmp_area_loop[10]);
#ifdef OSD_DEBUG
		logAllOsdHandle(phd);
#endif

		int min_idx;
		get_min_idx(&tmp_area_loop[0], 11, &min_idx);
		OSDLOG("min[%d]:%d\r\n", min_idx, tmp_area_loop[min_idx]);

		memcpy(&phd->canvas[0], &tmp_hd_loop[min_idx].canvas[0], sizeof(OsdCanvas) * 4);
		for (int i = 0; i < new_calc_num + 1; i++) {
			phd->region[add_list[i]].include_canvas =
			        tmp_hd_loop[min_idx].region[add_list[i]].include_canvas;
		}
#ifdef OSD_DEBUG
		logAllOsdHandle(phd);
#endif
		loop_idx = 0;
		memset(&tmp_area_loop[0], 0, sizeof(tmp_area_loop));
		memset(&tmp_hd_loop[0], 0, sizeof(tmp_hd_loop));
	}

	OSDLOG("***after align: \r\n");
	for (int i = 0; i < MAX_CANVAS; i++) {
		phd->canvas[i].width =
		        CEILINGALIGN16(phd->canvas[i].startX + phd->canvas[i].width - ALIGN16(phd->canvas[i].startX));
		phd->canvas[i].startX = ALIGN16(phd->canvas[i].startX);
		phd->canvas[i].height =
		        CEILINGALIGN16(phd->canvas[i].startY + phd->canvas[i].height - ALIGN16(phd->canvas[i].startY));
		phd->canvas[i].startY = ALIGN16(phd->canvas[i].startY);
	}
#ifdef OSD_DEBUG
	logAllOsdHandle(phd);
#endif
	return 0;
}

int OSD_calcCanvas(OsdHandle *phd)
{
	if (phd->osd_num == 0) {
		OSDERR("handle has no osd\r\n");
		return -EINVAL;
	}

	if (phd->osd_num > 4) {
		OSDERR("handle osd > 4\r\n");
		return -EINVAL;
	}

	for (int i = 0; i < MAX_CANVAS; i++) {
		memset(&phd->canvas[i], 0xff, sizeof(OsdCanvas));
	}
	/*grouping algo*/
	int add_list[8] = { 0 };
	int add_idx = 0;
	for (int i = 0; i < MAX_OSD; i++) {
		if (phd->osd_index[i] != -1) {
			OSDLOG("add %d\r\n", phd->osd_index[i]);
			add_list[add_idx] = phd->osd_index[i];
			add_idx++;
		}
	}

	if (add_idx > phd->osd_num) {
		OSDERR("add idx != region num: %d, %d\r\n", add_idx, phd->osd_num);
		return -EINVAL;
	}
	for (int i = 0; i < phd->osd_num; i++) {
		phd->canvas[i].osd_num = 1;
		phd->canvas[i].osd_list[0] = add_list[i];
		phd->canvas[i].startX = ALIGN16(phd->region[add_list[i]].startX);
		phd->canvas[i].startY = ALIGN16(phd->region[add_list[i]].startY);
		phd->canvas[i].width = CEILINGALIGN16(phd->region[add_list[i]].startX + phd->region[add_list[i]].width -
		                                      phd->canvas[i].startX);
		phd->canvas[i].height = CEILINGALIGN16(phd->region[add_list[i]].startY +
		                                       phd->region[add_list[i]].height - phd->canvas[i].startY);
		phd->region[add_list[i]].include_canvas = i;

		OSDLOG("Assign canvas[%d] = osd [%d] %d , (%d, %d, %d, %d)\r\n", i, i, add_list[i],
		       phd->canvas[i].startX, phd->canvas[i].startY, phd->canvas[i].width, phd->canvas[i].height);
	}
	return 0;
}

int OSD_setTextUnicodewithOutLine(OsdHandle *hd, int osd_idx, OsdText *txt, char *out)
{
	int tmp_idx = -1;
	for (int i = 0; i < MAX_OSD; ++i) {
		if (hd->osd_index[i] == osd_idx) {
			tmp_idx = i;
			OSDLOG("get tmp_idx = %d\r\n", tmp_idx);
		}
	}

	if (tmp_idx == -1) {
		OSDERR("index %d not found\r\n", osd_idx);
		return -EINVAL;
	}

	int x_offset;
	int y_offset, canvas_width, osd_width, osd_height;
	int flag = 0;

	for (int i = 0; i < hd->osd_num; i++) {
		for (int j = 0; j < hd->canvas[i].osd_num; j++) {
			if (hd->canvas[i].osd_list[j] == osd_idx) {
				x_offset = hd->region[tmp_idx].startX - hd->canvas[i].startX;

				y_offset = hd->region[tmp_idx].startY - hd->canvas[i].startY;
				canvas_width = hd->canvas[i].width;
				osd_width = hd->region[tmp_idx].width;
				osd_height = hd->region[tmp_idx].height;
				OSDLOG("get[%d] : %d, %d %d %d %d %d\r\n", i, osd_idx, x_offset, y_offset, canvas_width,
				       osd_width, osd_height);
				flag = 1;
				break;
			}
		}
		if (flag == 1) {
			break;
		}
	}

	if (TTF_Init() < 0) {
		fprintf(stderr, "failed to init sdl2ttf\r\n");
		SDL_Quit();
	}

	TTF_Font *font, *fontout;
	font = TTF_OpenFont(txt->ttf_path, txt->size);
	fontout = TTF_OpenFont(txt->ttf_path, txt->size);
	TTF_SetFontOutline(fontout, txt->outline_width);

	if ((font == NULL) || (fontout == NULL)) {
		fprintf(stderr, "failed to open font: %s\r\n", SDL_GetError());
		TTF_Quit();
		return -EINVAL;
	}

	SDL_Surface *text, *textout;

	SDL_Color forecol = { txt->color[0], txt->color[1], txt->color[2] };
	SDL_Color outcol = { txt->outline_color[0], txt->outline_color[1], txt->outline_color[2] };
	SDL_Color backcol = { 0xff, 0xff, 0xff };
	if (txt->background != WHITE) {
		setBackgroundColor(&txt->color[0], txt->background, &backcol);
	}

	OSDLOG("for (%d, %d, %d) bak (%d, %d, %d) out (%d, %d, %d), size: %d\r\n", forecol.r, forecol.g, forecol.b,
	       backcol.r, backcol.g, backcol.b, outcol.r, outcol.g, outcol.b, txt->outline_width);

	text = TTF_RenderUNICODE_Blended(font, (uint16_t *)&txt->unicode_txt[0], forecol);
	textout = TTF_RenderUNICODE_Blended(fontout, (uint16_t *)&txt->unicode_txt[0], outcol);
	SDL_Rect rect = { txt->outline_width, txt->outline_width, text->w, text->h };

	SDL_SetSurfaceBlendMode(text, SDL_BLENDMODE_BLEND);
	SDL_BlitSurface(text, NULL, textout, &rect);

	if (out == NULL) {
		OSDERR("ptr == NULL, save to /mnt/nfs/ethnfs/text-out-unicode.bmp\r\n");
		SDL_SaveBMP(textout, "/mnt/nfs/ethnfs/text-out-unicode.bmp");
	}

	OSDLOG("load bmp (w,h) = %d %d\r\n", textout->w, textout->h);

	char *bgra = (char *)textout->pixels;
	int width, height, y, u, v, i, j;
	width = textout->w;
	height = textout->h;
	int r;
	int g;
	int b;
	int a = 255;

	char ayuv_buf[width * height * 3];
	int ayuv_idx = 0;

	for (i = 0; i < height; i++) {
		for (j = 0; j < width * 4; j += 4) {
			b = (int)bgra[i * width * 4 + j];
			g = (int)bgra[i * width * 4 + j + 1];
			r = (int)bgra[i * width * 4 + j + 2];
			a = (int)bgra[i * width * 4 + j + 3];

			if (txt->background != TRANSPARENT) {
				if (a == 0) {
					b = backcol.b;
					g = backcol.g;
					r = backcol.r;
					a = 255;
				}
			}

			RGBTrans2YUV(&r, &g, &b, &y, &u, &v);
			normalizeAYUV3544(&a, &y, &u, &v);

			ayuv_buf[ayuv_idx] = MERGE44(u, v);
			ayuv_idx += 1;
			ayuv_buf[ayuv_idx] = MERGE35(a, y);
			ayuv_idx += 1;
		}
	}

	alignUVVal(&ayuv_buf[0], width, height);

	/*load bmp & convert*/
	if (out == NULL) {
		OSDERR("out ptr not found, save: /mnt/nfs/ethnfs/save-out-unicode.ayuv\r\n");
		saveAYUV("/mnt/nfs/ethnfs/save-out-unicode.ayuv", width, height, &ayuv_buf[0], width * height * 2);
		SDL_FreeSurface(text);
		SDL_FreeSurface(textout);
		TTF_CloseFont(font);
		TTF_CloseFont(fontout);
		TTF_Quit();

		return -EINVAL;
	}

	int line = 0;
	if ((width != osd_width) || (height != osd_height)) {
		OSDLOG("Img size != OSD size, (%d, %d) (%d, %d)\r\n", width, height, osd_width, osd_height);
	}

	int copy_width, copy_height;
	if (width > osd_width) {
		copy_width = osd_width;
	} else {
		copy_width = width;
	}

	if (height > osd_height) {
		copy_height = osd_height;
	} else {
		copy_height = height;
	}

	for (int i = y_offset; i < y_offset + copy_height; i++) {
		memcpy((void *)out + (canvas_width * i + x_offset) * 2, (void *)&ayuv_buf[line * width * 2],
		       copy_width * 2);
		line++;
	}

	SDL_FreeSurface(text);
	SDL_FreeSurface(textout);
	TTF_CloseFont(font);
	TTF_CloseFont(fontout);
	TTF_Quit();

	return 0;
}

int OSD_setTextUnicode(OsdHandle *hd, int osd_idx, OsdText *txt, char *out)
{
	OSDLOG("init TTF\r\n");

	if (txt->outline_width > 0) {
		OSDLOG("go to outline \r\n");
		OSD_setTextUnicodewithOutLine(hd, osd_idx, txt, out);
		return 0;
	}

	int ret = 0;
	if (TTF_Init() < 0) {
		fprintf(stderr, "failed to init sdlttf\r\n");
		SDL_Quit();
		return -EPERM;
	}

	TTF_Font *font;
	font = TTF_OpenFont(txt->ttf_path, txt->size);

	SDL_Surface *text, *tmp;
	SDL_Color forecol = { txt->color[0], txt->color[1], txt->color[2] };
	SDL_Color backcol = { 0xff, 0xff, 0xff };

	if (txt->background != WHITE) {
		setBackgroundColor(&txt->color[0], txt->background, &backcol);
	}

	text = TTF_RenderUNICODE_Shaded(font, (uint16_t *)&txt->unicode_txt[0], forecol, backcol);

	if (text == NULL) {
		fprintf(stderr, "Invaild Unicode word\r\n");
		SDL_FreeSurface(text);
		ret = -EINVAL;
		return ret;
	}

	SDL_PixelFormat *fmt;
	fmt = (SDL_PixelFormat *)malloc(sizeof(SDL_PixelFormat));
	if (fmt == NULL) {
		fprintf(stderr, "failed to alloc a SDL_PixelFormat\r\n");
	}
	memset(fmt, 0, sizeof(SDL_PixelFormat));

	fmt->BitsPerPixel = 32;
	fmt->BytesPerPixel = 4;

	tmp = SDL_ConvertSurface(text, fmt, 0);
	if (tmp == NULL) {
		fprintf(stderr, "failed convert interface, %s\r\n", SDL_GetError());
	}

	free(fmt);

	OSDLOG("save %s, fcolor: %0x %0x %0x\r\n", "/tmp/save.bmp", txt->color[0], txt->color[1], txt->color[2]);

	int width, height;
	width = tmp->w;
	height = tmp->h;
	char *bgra = (char *)tmp->pixels;
	OSDLOG("get (%d, %d)\r\n", width, height);

	char ayuv_buf[width * height * 2];
	int y, u, v, i, j;
	int r;
	int g;
	int b;
	int a = 255;
	int ayuv_idx = 0;

	for (i = 0; i < height; i++) {
		for (j = 0; j < width * 4; j += 4) {
			b = (int)bgra[i * width * 4 + j];
			g = (int)bgra[i * width * 4 + j + 1];
			r = (int)bgra[i * width * 4 + j + 2];
			a = 255;

			if (txt->background == TRANSPARENT) {
				if (((b == backcol.b) && (g == backcol.g)) && (r == backcol.r)) {
					a = 0x00;
				} else {
					a = 255;
				}
			}

			RGBTrans2YUV(&r, &g, &b, &y, &u, &v);
			normalizeAYUV3544(&a, &y, &u, &v);

			ayuv_buf[ayuv_idx] = MERGE44(u, v);
			ayuv_idx += 1;
			ayuv_buf[ayuv_idx] = MERGE35(a, y);
			ayuv_idx += 1;
		}
	}

	alignUVVal(&ayuv_buf[0], width, height);

	int tmp_idx = -1;
	for (int i = 0; i < MAX_OSD; ++i) {
		if (hd->osd_index[i] == osd_idx) {
			tmp_idx = i;
		}
	}

	if (tmp_idx == -1) {
		OSDERR("index %d not found\r\n", osd_idx);
		return -EINVAL;
	}

	int x_offset;
	int y_offset, canvas_width, osd_width, osd_height;
	int flag = 0;
	for (int i = 0; i < hd->osd_num; i++) {
		for (int j = 0; j < hd->canvas[i].osd_num; j++) {
			if (hd->canvas[i].osd_list[j] == osd_idx) {
				x_offset = hd->region[tmp_idx].startX - hd->canvas[i].startX;
				y_offset = hd->region[tmp_idx].startY - hd->canvas[i].startY;
				canvas_width = hd->canvas[i].width;
				osd_width = hd->region[tmp_idx].width;
				osd_height = hd->region[tmp_idx].height;

				OSDLOG("get[%d] : %d, %d %d %d %d %d\r\n", i, osd_idx, x_offset, y_offset, canvas_width,
				       osd_width, osd_height);
				flag = 1;
				break;
			}
		}
		if (flag == 1) {
			break;
		}
	}

	OSDLOG("start fopen\r\n");
	int img_width = 0;
	int img_height = 0;

	img_width = width;
	img_height = height;

	int line = 0;

	if ((img_width != osd_width) || (img_height != osd_height)) {
		OSDLOG("Img size != OSD size, (%d, %d) (%d, %d)\r\n", img_width, img_height, osd_width, osd_height);
	}

	int copy_width, copy_height;
	if (img_width > osd_width) {
		copy_width = osd_width;
	} else {
		copy_width = img_width;
	}

	if (img_height > osd_width) {
		copy_height = osd_height;
	} else {
		copy_height = img_height;
	}

	if (out == NULL) {
		OSDERR("out ptr is null\r\n");
		saveAYUV("/tmp/save-1.ayuv", width, height, &ayuv_buf[0], width * height * 2);

		SDL_FreeSurface(text);
		SDL_FreeSurface(tmp);
		TTF_CloseFont(font);
		TTF_Quit();

		ret = -EINVAL;
		return ret;
	}

	for (int i = y_offset; i < y_offset + copy_height; i++) {
		OSDLOG("cp %d len %d to %d\r\n", line * copy_width * 2, copy_width * 2, ((i * canvas_width)) * 2);
		memcpy((void *)out + (canvas_width * i + x_offset) * 2, (void *)&ayuv_buf[line * img_width * 2],
		       copy_width * 2);

		line++;
	}

	SDL_FreeSurface(text);
	SDL_FreeSurface(tmp);
	TTF_CloseFont(font);
	TTF_Quit();
	return 0;
}

int OSD_setImage(OsdHandle *hd, int osd_idx, const char *image_path, char *out)
{
	int tmp_idx = -1;
	for (int i = 0; i < MAX_OSD; ++i) {
		if (hd->osd_index[i] == osd_idx) {
			tmp_idx = i;
			OSDLOG("get tmp_idx = %d\r\n", tmp_idx);
		}
	}

	if (tmp_idx == -1) {
		OSDERR("index %d not found\r\n", osd_idx);
		return -EINVAL;
	}

	int x_offset;
	int y_offset, canvas_width, osd_width, osd_height;
	int flag = 0;

	for (int i = 0; i < hd->osd_num; i++) {
		for (int j = 0; j < hd->canvas[i].osd_num; j++) {
			if (hd->canvas[i].osd_list[j] == osd_idx) {
				x_offset = hd->region[tmp_idx].startX - hd->canvas[i].startX;
				y_offset = hd->region[tmp_idx].startY - hd->canvas[i].startY;
				canvas_width = hd->canvas[i].width;
				osd_width = hd->region[tmp_idx].width;
				osd_height = hd->region[tmp_idx].height;

				OSDLOG("get[%d] : %d, %d %d %d %d %d\r\n", i, osd_idx, x_offset, y_offset, canvas_width,
				       osd_width, osd_height);
				flag = 1;
				break;
			}
		}
		if (flag) {
			break;
		}
	}

	FILE *fp;
	fp = fopen(image_path, "rb");
	if (fp == NULL) {
		OSDERR("Failed to open .ayuv\r\n");
		return -EINVAL;
	}
	uint32_t img_width, img_height;
	fread(&img_width, sizeof(uint32_t), 1, fp);
	fseek(fp, sizeof(uint32_t), SEEK_SET);
	fread(&img_height, sizeof(uint32_t), 1, fp);
	fseek(fp, sizeof(uint32_t) * 3, SEEK_SET);

	char ayuv_buf[img_width * img_height * 2];
	fread(&ayuv_buf[0], img_width * img_height * 2, 1, fp);
	fclose(fp);
	alignUVVal(&ayuv_buf[0], img_width, img_height);

	if (out == NULL) {
		saveAYUV("/mnt/nfs/ethnfs/save-logo.ayuv", img_width, img_height, &ayuv_buf[0],
		         img_width * img_height * 2);
		OSDERR("save to save-logo.ayuv\r\n");
		return 0;
	}

	int line = 0;

	if ((img_width != osd_width) || (img_height != osd_height)) {
		OSDERR("Img size != OSD size, (%d, %d) (%d, %d)\r\n", img_width, img_height, osd_width, osd_height);
	}

	int copy_width, copy_height;
	if (img_width > osd_width) {
		copy_width = osd_width;
	} else {
		copy_width = img_width;
	}

	if (img_height > osd_height) {
		copy_height = osd_height;
	} else {
		copy_height = img_height;
	}

	for (int i = y_offset; i < y_offset + copy_height; i++) {
		OSDLOG("cp %d len %d to %d\r\n", line * copy_width * 2, copy_width * 2,
		       ((i * canvas_width + x_offset)) * 2);

		memcpy((void *)out + (canvas_width * i + x_offset) * 2, (void *)&ayuv_buf[line * img_width * 2],
		       copy_width * 2);

		line++;
	}

	OSDLOG("start cp\r\n");

	return 0;
}

int OSD_setImageBmp(OsdHandle *hd, int osd_idx, const char *image_path, char *out)
{
	int tmp_idx = -1;
	for (int i = 0; i < MAX_OSD; ++i) {
		if (hd->osd_index[i] == osd_idx) {
			tmp_idx = i;
			OSDERR("get tmp_idx = %d\r\n", tmp_idx);
		}
	}

	if (tmp_idx == -1) {
		OSDERR("index %d not found\r\n", osd_idx);
		return -EINVAL;
	}

	int x_offset;
	int y_offset, canvas_width, osd_width, osd_height;
	int flag = 0;

	for (int i = 0; i < hd->osd_num; i++) {
		for (int j = 0; j < hd->canvas[i].osd_num; j++) {
			if (hd->canvas[i].osd_list[j] == osd_idx) {
				x_offset = hd->region[tmp_idx].startX - hd->canvas[i].startX;

				y_offset = hd->region[tmp_idx].startY - hd->canvas[i].startY;
				canvas_width = hd->canvas[i].width;
				osd_width = hd->region[tmp_idx].width;
				osd_height = hd->region[tmp_idx].height;
				OSDLOG("get[%d] : %d, %d %d %d %d %d\r\n", i, osd_idx, x_offset, y_offset, canvas_width,
				       osd_width, osd_height);
				flag = 1;
				break;
			}
		}
		if (flag == 1) {
			break;
		}
	}

	SDL_Surface *tmp;
	SDL_PixelFormat *fmt;

	tmp = SDL_LoadBMP(image_path);
	if (tmp == NULL) {
		OSDERR("SDL failed to read BMP: %s\r\n", SDL_GetError());
		return -EINVAL;
	}

	fmt = (SDL_PixelFormat *)malloc(sizeof(SDL_PixelFormat));
	if (fmt == NULL) {
		OSDERR("failed to alloc a SDL_PixelFormat\r\n");
	}
	memset(fmt, 0, sizeof(SDL_PixelFormat));
	fmt->BitsPerPixel = 32;
	fmt->BytesPerPixel = 4;

	tmp = SDL_ConvertSurface(tmp, fmt, 0);
	if (tmp == NULL) {
		OSDERR("failed convert interface, %s\r\n", SDL_GetError());
	}
	free(fmt);

	OSDLOG("load bmp (w,h) = %d %d\r\n", tmp->w, tmp->h);

	int width, height, y, u, v, i, j;
	int r = 0;
	int g = 0;
	int b = 0;
	int a = 255;
	width = tmp->w;
	height = tmp->h;

	char *bgra = (char *)tmp->pixels;

	char ayuv_buf[width * height * 3];
	int ayuv_idx = 0;

	for (i = 0; i < height; i++) {
		for (j = 0; j < width * 4; j += 4) {
			b = (int)bgra[i * width * 4 + j];
			g = (int)bgra[i * width * 4 + j + 1];
			r = (int)bgra[i * width * 4 + j + 2];
			a = 255;

			RGBTrans2YUV(&r, &g, &b, &y, &u, &v);
			normalizeAYUV3544(&a, &y, &u, &v);

			ayuv_buf[ayuv_idx] = MERGE44(u, v);
			ayuv_idx += 1;
			ayuv_buf[ayuv_idx] = MERGE35(a, y);
			ayuv_idx += 1;
		}
	}

	alignUVVal(&ayuv_buf[0], width, height);

	/*load bmp & convert*/
	if (out == NULL) {
		OSDERR("out ptr not found\r\n");
		saveAYUV("/mnt/nfs/ethnfs/save-out.ayuv", width, height, &ayuv_buf[0], width * height * 2);
		SDL_FreeSurface(tmp);
		return -EINVAL;
	}

	SDL_FreeSurface(tmp);

	int line = 0;

	if ((width != osd_width) || (height != osd_height)) {
		OSDERR("Img size != OSD size, (%d, %d) (%d, %d)\r\n", width, height, osd_width, osd_height);
	}

	int copy_width, copy_height;
	if (width > osd_width) {
		copy_width = osd_width;
	} else {
		copy_width = width;
	}

	if (height > osd_height) {
		copy_height = osd_height;
	} else {
		copy_height = height;
	}

	for (int i = y_offset; i < y_offset + copy_height; i++) {
		memcpy((void *)out + (canvas_width * i + x_offset) * 2, (void *)&ayuv_buf[line * width * 2],
		       copy_width * 2);

		line++;
	}

	return 0;
}

int OSD_setTextUTF8withOutLine(OsdHandle *hd, int osd_idx, OsdText *txt, char *out)
{
	int tmp_idx = -1;
	for (int i = 0; i < MAX_OSD; ++i) {
		if (hd->osd_index[i] == osd_idx) {
			tmp_idx = i;
			OSDERR("get tmp_idx = %d\r\n", tmp_idx);
		}
	}

	if (tmp_idx == -1) {
		OSDERR("index %d not found\r\n", osd_idx);
		return -EINVAL;
	}

	int x_offset;
	int y_offset, canvas_width, osd_width, osd_height;
	int flag = 0;

	for (int i = 0; i < hd->osd_num; i++) {
		for (int j = 0; j < hd->canvas[i].osd_num; j++) {
			if (hd->canvas[i].osd_list[j] == osd_idx) {
				x_offset = hd->region[tmp_idx].startX - hd->canvas[i].startX;

				y_offset = hd->region[tmp_idx].startY - hd->canvas[i].startY;
				canvas_width = hd->canvas[i].width;
				osd_width = hd->region[tmp_idx].width;
				osd_height = hd->region[tmp_idx].height;
				OSDLOG("get[%d] : %d, %d %d %d %d %d\r\n", i, osd_idx, x_offset, y_offset, canvas_width,
				       osd_width, osd_height);
				flag = 1;
				break;
			}
		}
		if (flag == 1) {
			break;
		}
	}

	if (TTF_Init() < 0) {
		fprintf(stderr, "failed to init sdl2ttf\r\n");
		SDL_Quit();
	}

	TTF_Font *font, *fontout;
	font = TTF_OpenFont(txt->ttf_path, txt->size);
	fontout = TTF_OpenFont(txt->ttf_path, txt->size);
	TTF_SetFontOutline(fontout, txt->outline_width);

	if ((font == NULL) || (fontout == NULL)) {
		fprintf(stderr, "failed to open font: %s\r\n", SDL_GetError());
		TTF_Quit();
		return -EINVAL;
	}

	SDL_Surface *text, *textout;

	SDL_Color forecol = { txt->color[0], txt->color[1], txt->color[2] };
	SDL_Color outcol = { txt->outline_color[0], txt->outline_color[1], txt->outline_color[2] };
	SDL_Color backcol = { 0xff, 0xff, 0xff };
	if (txt->background != WHITE) {
		setBackgroundColor(&txt->color[0], txt->background, &backcol);
	}

	OSDLOG("for (%d, %d, %d) bak (%d, %d, %d) out (%d, %d, %d), size: %d\r\n", forecol.r, forecol.g, forecol.b,
	       backcol.r, backcol.g, backcol.b, outcol.r, outcol.g, outcol.b, txt->outline_width);

	char txt_tmp[64];
	snprintf(&txt_tmp[0], 64, "%s", txt->txt);

	text = TTF_RenderUTF8_Blended(font, txt_tmp, forecol);
	textout = TTF_RenderUTF8_Blended(fontout, txt_tmp, outcol);
	if ((text == NULL) || (textout == NULL)) {
		OSDERR("Invaild UTF8 word\r\n");
	}
	SDL_Rect rect = { txt->outline_width, txt->outline_width, text->w, text->h };

	SDL_SetSurfaceBlendMode(text, SDL_BLENDMODE_BLEND);
	SDL_BlitSurface(text, NULL, textout, &rect);

	if (out == NULL) {
		SDL_SaveBMP(textout, "/mnt/nfs/ethnfs/text-out.bmp");
	}

	OSDLOG("load bmp (w,h) = %d %d\r\n", textout->w, textout->h);

	char *bgra = (char *)textout->pixels;
	int width, height, y, u, v, i, j;
	width = textout->w;
	height = textout->h;
	int r;
	int g;
	int b;
	int a = 0x07;

	char ayuv_buf[width * height * 3];
	int ayuv_idx = 0;

	for (i = 0; i < height; i++) {
		for (j = 0; j < width * 4; j += 4) {
			b = (int)bgra[i * width * 4 + j];
			g = (int)bgra[i * width * 4 + j + 1];
			r = (int)bgra[i * width * 4 + j + 2];
			a = (int)bgra[i * width * 4 + j + 3];

			if (txt->background != TRANSPARENT) {
				if (a == 0) {
					b = backcol.b;
					g = backcol.g;
					r = backcol.r;
					a = 255;
				}
			}

			RGBTrans2YUV(&r, &g, &b, &y, &u, &v);
			normalizeAYUV3544(&a, &y, &u, &v);

			ayuv_buf[ayuv_idx] = MERGE44(u, v);
			ayuv_idx += 1;
			ayuv_buf[ayuv_idx] = MERGE35(a, y);
			ayuv_idx += 1;
		}
	}

	alignUVVal(&ayuv_buf[0], width, height);

	/*load bmp & convert*/
	if (out == NULL) {
		OSDERR("out ptr not found, save: /mnt/nfs/ethnfs/save-out.ayuv\r\n");
		saveAYUV("/mnt/nfs/ethnfs/save-out.ayuv", width, height, &ayuv_buf[0], width * height * 2);
		SDL_FreeSurface(text);
		SDL_FreeSurface(textout);
		TTF_CloseFont(font);
		TTF_CloseFont(fontout);
		TTF_Quit();

		return -EINVAL;
	}

	int line = 0;
	if ((width != osd_width) || (height != osd_height)) {
		OSDERR("Img size != OSD size, (%d, %d) (%d, %d)\r\n", width, height, osd_width, osd_height);
	}

	int copy_width, copy_height;
	if (width > osd_width) {
		copy_width = osd_width;
	} else {
		copy_width = width;
	}

	if (height > osd_height) {
		copy_height = osd_height;
	} else {
		copy_height = height;
	}

	for (int i = y_offset; i < y_offset + copy_height; i++) {
		memcpy((void *)out + (canvas_width * i + x_offset) * 2, (void *)&ayuv_buf[line * width * 2],
		       copy_width * 2);
		line++;
	}

	SDL_FreeSurface(text);
	SDL_FreeSurface(textout);
	TTF_CloseFont(font);
	TTF_CloseFont(fontout);
	TTF_Quit();

	return 0;
}

int OSD_setTextUTF8(OsdHandle *hd, int osd_idx, OsdText *txt, char *out)
{
	if (txt->outline_width > 0) {
		OSDLOG("go to outline \r\n");
		OSD_setTextUTF8withOutLine(hd, osd_idx, txt, out);
		return 0;
	}

	int tmp_idx = -1;
	for (int i = 0; i < MAX_OSD; ++i) {
		if (hd->osd_index[i] == osd_idx) {
			tmp_idx = i;
			OSDLOG("get tmp_idx = %d\r\n", tmp_idx);
		}
	}

	if (tmp_idx == -1) {
		OSDERR("index %d not found\r\n", osd_idx);
		return -EINVAL;
	}

	int x_offset;
	int y_offset, canvas_width, osd_width, osd_height;
	int flag = 0;

	for (int i = 0; i < hd->osd_num; i++) {
		for (int j = 0; j < hd->canvas[i].osd_num; j++) {
			if (hd->canvas[i].osd_list[j] == osd_idx) {
				x_offset = hd->region[tmp_idx].startX - hd->canvas[i].startX;

				y_offset = hd->region[tmp_idx].startY - hd->canvas[i].startY;
				canvas_width = hd->canvas[i].width;
				osd_width = hd->region[tmp_idx].width;
				osd_height = hd->region[tmp_idx].height;
				OSDLOG("get[%d] : %d, %d %d %d %d %d\r\n", i, osd_idx, x_offset, y_offset, canvas_width,
				       osd_width, osd_height);
				flag = 1;
				break;
			}
		}
		if (flag == 1) {
			break;
		}
	}

	if (TTF_Init() < 0) {
		fprintf(stderr, "failed to init sdl2ttf\r\n");
		SDL_Quit();
	}

	TTF_Font *font;
	font = TTF_OpenFont(txt->ttf_path, txt->size);

	if (font == NULL) {
		fprintf(stderr, "failed to open font: %s\r\n", SDL_GetError());
		TTF_Quit();
		return -EINVAL;
	}

	SDL_PixelFormat *fmt;
	SDL_Surface *text, *tmp;

	SDL_Color forecol = { txt->color[0], txt->color[1], txt->color[2] };
	SDL_Color backcol = { 0xff, 0xff, 0xff };

	if (txt->background != WHITE) {
		setBackgroundColor(&txt->color[0], txt->background, &backcol);
	}

	OSDLOG("fcolor(%d, %d, %d), bcolor(%d, %d, %d)\r\n", forecol.r, forecol.g, forecol.b, backcol.r, backcol.g,
	       backcol.b);

	char txt_tmp[64];
	snprintf(&txt_tmp[0], 64, "%s", txt->txt);

	text = TTF_RenderUTF8_Shaded(font, txt_tmp, forecol, backcol);
	if (text == NULL) {
		fprintf(stderr, "Invaild UTF8 word\r\n");
	}

	fmt = (SDL_PixelFormat *)malloc(sizeof(SDL_PixelFormat));
	if (fmt == NULL) {
		fprintf(stderr, "failed to alloc a SDL_PixelFormat\r\n");
	}
	memset(fmt, 0, sizeof(SDL_PixelFormat));

	fmt->BitsPerPixel = 32;
	fmt->BytesPerPixel = 4;

	tmp = SDL_ConvertSurface(text, fmt, 0);
	if (tmp == NULL) {
		fprintf(stderr, "failed convert interface, %s\r\n", SDL_GetError());
	}

	free(fmt);

	OSDLOG("load bmp (w,h) = %d %d\r\n", tmp->w, tmp->h);

	int width, height, y, u, v, i, j;
	int r;
	int g;
	int b;
	int a = 255;

	width = tmp->w;
	height = tmp->h;

	char *bgra = (char *)tmp->pixels;

	char ayuv_buf[width * height * 2];
	int ayuv_idx = 0;

	for (i = 0; i < height; i++) {
		for (j = 0; j < width * 4; j += 4) {
			b = (int)bgra[i * width * 4 + j];
			g = (int)bgra[i * width * 4 + j + 1];
			r = (int)bgra[i * width * 4 + j + 2];
			a = 255;

			if (txt->background == TRANSPARENT) {
				if (((b == backcol.b) && (g == backcol.g)) && (r == backcol.r)) {
					a = 0x00;
				} else {
					a = 255;
				}
			}

			RGBTrans2YUV(&r, &g, &b, &y, &u, &v);
			normalizeAYUV3544(&a, &y, &u, &v);

			ayuv_buf[ayuv_idx] = MERGE44(u, v);
			ayuv_idx += 1;
			ayuv_buf[ayuv_idx] = MERGE35(a, y);
			ayuv_idx += 1;
		}
	}

	alignUVVal(&ayuv_buf[0], width, height);

	/*load bmp & convert*/
	if (out == NULL) {
		OSDERR("out ptr not found\r\n");
		saveAYUV("/mnt/nfs/ethnfs/save-out.ayuv", width, height, &ayuv_buf[0], width * height * 2);
		SDL_FreeSurface(tmp);
		return -EINVAL;
	}

	int line = 0;

	if ((width != osd_width) || (height != osd_height)) {
		OSDERR("Img size != OSD size, (%d, %d) (%d, %d)\r\n", width, height, osd_width, osd_height);
	}

	int copy_width, copy_height;
	if (width > osd_width) {
		copy_width = osd_width;
	} else {
		copy_width = width;
	}

	if (height > osd_height) {
		copy_height = osd_height;
	} else {
		copy_height = height;
	}

	for (int i = y_offset; i < y_offset + copy_height; i++) {
		memcpy((void *)out + (canvas_width * i + x_offset) * 2, (void *)&ayuv_buf[line * width * 2],
		       copy_width * 2);

		line++;
	}

	SDL_FreeSurface(text);
	SDL_FreeSurface(tmp);
	TTF_CloseFont(font);
	TTF_Quit();
	return 0;
}

char *OSD_createTextUTF8Src(OsdText *txt, int *width, int *height)
{
	if (TTF_Init() < 0) {
		fprintf(stderr, "failed to init sdlttf\r\n");
		SDL_Quit();
		return NULL;
	}

	TTF_Font *font, *fontout;
	font = TTF_OpenFont(txt->ttf_path, txt->size);

	SDL_Surface *text;
	SDL_Surface *tmp;
	SDL_Color forecol = { txt->color[0], txt->color[1], txt->color[2] };
	SDL_Color backcol = { 0xff, 0xff, 0xff };
	if (txt->background != WHITE) {
		setBackgroundColor(&txt->color[0], txt->background, &backcol);
	}

	if (txt->outline_width == 0) {
		SDL_PixelFormat *fmt;
		fmt = (SDL_PixelFormat *)malloc(sizeof(SDL_PixelFormat));
		if (fmt == NULL) {
			fprintf(stderr, "failed to alloc a SDL_PixelFormat\r\n");
		}
		memset(fmt, 0, sizeof(SDL_PixelFormat));
		fmt->BitsPerPixel = 32;
		fmt->BytesPerPixel = 4;

		text = TTF_RenderUTF8_Shaded(font, &txt->txt[0], forecol, backcol);
		if (text == NULL) {
			OSDERR("Invaild utf8 word\r\n");
			SDL_FreeSurface(text);
			TTF_CloseFont(font);
			return NULL;
		}
		tmp = SDL_ConvertSurface(text, fmt, 0);
		if (tmp == NULL) {
			OSDERR("failed convert interface, %s\r\n", SDL_GetError());
		}
#ifdef OSD_DEBUG
		char bmp_name[32];
		sprintf(&bmp_name[0], "/mnt/nfs/ethnfs/%s.bmp", &txt->txt[0]);

		SDL_SaveBMP(tmp, &bmp_name[0]);
#endif
		free(fmt);
	} else {
		fontout = TTF_OpenFont(txt->ttf_path, txt->size);
		TTF_SetFontOutline(fontout, txt->outline_width);

		if (fontout == NULL) {
			fprintf(stderr, "failed to open font: %s\r\n", SDL_GetError());
			TTF_Quit();
			return NULL;
		}

		SDL_Color outcol = { txt->outline_color[0], txt->outline_color[1], txt->outline_color[2] };
		text = TTF_RenderUTF8_Blended(font, &txt->txt[0], forecol);
		tmp = TTF_RenderUTF8_Blended(fontout, &txt->txt[0], outcol);
		if ((text == NULL) || (tmp == NULL)) {
			OSDERR("Invaild UTF8 word\r\n");
		}

		SDL_Rect rect = { txt->outline_width, txt->outline_width, text->w, text->h };

		SDL_SetSurfaceBlendMode(text, SDL_BLENDMODE_BLEND);
		SDL_BlitSurface(text, NULL, tmp, &rect);
#ifdef OSD_DEBUG
		char bmp_name[32];
		sprintf(&bmp_name[0], "/mnt/nfs/ethnfs/%s-outline.bmp", &txt->txt[0]);

		SDL_SaveBMP(tmp, &bmp_name[0]);
#endif
	}

	OSDLOG("alloc size: %d %d %d\r\n", tmp->w, tmp->h, tmp->w * tmp->h * 2);
	char *src = malloc(tmp->w * tmp->h * 2);
	int ayuv_idx = 0;
	*width = tmp->w;
	*height = tmp->h;
	char *bgra = (char *)tmp->pixels;
	int i, j, a, y, u, v, r, g, b;

	for (i = 0; i < tmp->h; i++) {
		for (j = 0; j < tmp->w * 4; j += 4) {
			b = (int)bgra[i * tmp->w * 4 + j];
			g = (int)bgra[i * tmp->w * 4 + j + 1];
			r = (int)bgra[i * tmp->w * 4 + j + 2];
			a = 255;

			if (txt->outline_width == 0) {
				a = 255;
				if (txt->background == TRANSPARENT) {
					if (((b == backcol.b) && (g == backcol.g)) && (r == backcol.r)) {
						a = 0x00;
					} else {
						a = 255;
					}
				}
			} else {
				a = (int)bgra[i * tmp->w * 4 + j + 3];
				if (txt->background != TRANSPARENT) {
					if (a == 0) {
						b = backcol.b;
						g = backcol.g;
						r = backcol.r;
						a = 255;
					}
				}
			}

			RGBTrans2YUV(&r, &g, &b, &y, &u, &v);
			normalizeAYUV3544(&a, &y, &u, &v);
			src[ayuv_idx] = MERGE44(u, v);
			ayuv_idx += 1;
			src[ayuv_idx] = MERGE35(a, y);
			ayuv_idx += 1;
		}
	}

	alignUVVal(&src[0], tmp->w, tmp->h);

	SDL_FreeSurface(tmp);
	SDL_FreeSurface(text);

	TTF_CloseFont(font);

	if (txt->outline_width > 0) {
		TTF_CloseFont(fontout);
	}

	TTF_Quit();

	return src;
}

char *OSD_createTextUTF8Src8bit(OsdText *txt, int *width, int *height)
{
	if (txt->mode != PALETTE_8) {
		OSDERR("wrong format\r\n");
		return NULL;
	}

	if (TTF_Init() < 0) {
		OSDERR("failed to init sdlttf\r\n");
		SDL_Quit();
		return NULL;
	}

	TTF_Font *font, *fontout;
	font = TTF_OpenFont(txt->ttf_path, txt->size);
	if (font == NULL) {
		OSDERR("failed to open font: %s\r\n", SDL_GetError());
		TTF_Quit();
		return NULL;
	}

	SDL_Surface *text;
	SDL_Surface *tmp;
	SDL_Color forecol = { 0xff, 0x00, 0x00 };
	SDL_Color backcol = { 0x00, 0x00, 0x00 };
	if (txt->outline_width > 0) {
		fontout = TTF_OpenFont(txt->ttf_path, txt->size);
		TTF_SetFontOutline(fontout, txt->outline_width);

		if (fontout == NULL) {
			fprintf(stderr, "failed to open font: %s\r\n", SDL_GetError());
			TTF_Quit();
			return NULL;
		}
		SDL_Color outcol = { 0x00, 0xff, 0x00 };

		text = TTF_RenderUTF8_Blended(font, &txt->txt[0], forecol);
		tmp = TTF_RenderUTF8_Blended(fontout, &txt->txt[0], outcol);
		if ((text == NULL) || (tmp == NULL)) {
			OSDERR("Invaild UTF8 word\r\n");
		}

		SDL_Rect rect = { txt->outline_width, txt->outline_width, text->w, text->h };

		SDL_SetSurfaceBlendMode(text, SDL_BLENDMODE_BLEND);
		SDL_BlitSurface(text, NULL, tmp, &rect);
#ifdef OSD_DEBUG
		char bmp_name[32];
		sprintf(&bmp_name[0], "/mnt/nfs/ethnfs/%s-outline-8bpp.bmp", &txt->txt[0]);

		SDL_SaveBMP(tmp, &bmp_name[0]);
#endif
	} else {
		SDL_PixelFormat *fmt;
		fmt = (SDL_PixelFormat *)malloc(sizeof(SDL_PixelFormat));
		if (fmt == NULL) {
			fprintf(stderr, "failed to alloc a SDL_PixelFormat\r\n");
		}
		memset(fmt, 0, sizeof(SDL_PixelFormat));
		fmt->BitsPerPixel = 32;
		fmt->BytesPerPixel = 4;

		text = TTF_RenderUTF8_Shaded(font, &txt->txt[0], forecol, backcol);
		if (text == NULL) {
			OSDERR("Invaild utf8 word: %s\r\n", SDL_GetError());
			SDL_FreeSurface(text);
			TTF_CloseFont(font);
			return NULL;
		}
		tmp = SDL_ConvertSurface(text, fmt, 0);
		if (tmp == NULL) {
			OSDERR("failed convert interface, %s\r\n", SDL_GetError());
		}
#ifdef OSD_DEBUG
		char bmp_name[32];
		sprintf(&bmp_name[0], "/mnt/nfs/ethnfs/%s-8bpp.bmp", &txt->txt[0]);

		SDL_SaveBMP(tmp, &bmp_name[0]);
#endif
		free(fmt);
	}

	OSDLOG("alloc size: %d %d %d\r\n", tmp->w, tmp->h, tmp->w * tmp->h);
	char *src = malloc(tmp->w * tmp->h);
	int ayuv_idx = 0;
	*width = tmp->w;
	*height = tmp->h;
	char *bgra = (char *)tmp->pixels;
	int i, j, a, r, g, b;

	for (i = 0; i < tmp->h; i++) {
		for (j = 0; j < tmp->w * 4; j += 4) {
			b = (int)bgra[i * tmp->w * 4 + j];
			g = (int)bgra[i * tmp->w * 4 + j + 1];
			r = (int)bgra[i * tmp->w * 4 + j + 2];
			a = (int)bgra[i * tmp->w * 4 + j + 3];

			if (txt->outline_width > 0) {
				if ((r > g) && (r > b)) {
					src[ayuv_idx] = MERGE35(txt->color[0], txt->color[1]);
				} else if (a != 0) {
					/*outline*/
					src[ayuv_idx] = MERGE35(txt->outline_color[0], txt->outline_color[1]);
				} else {
					a = 255;
					if (txt->background == TRANSPARENT) {
						src[ayuv_idx] = MERGE35(0, 0x00);
					} else if (txt->background == WHITE) {
						src[ayuv_idx] = MERGE35(a, 0xff);
					} else if (txt->background == BLACK) {
						src[ayuv_idx] = MERGE35(a, 0x00);
					}
				}
			} else if (txt->outline_width == 0) {
				a = 255;
				if ((r > g) && (r > b)) {
					src[ayuv_idx] = MERGE35(txt->color[0], txt->color[1]);
				} else {
					if (txt->background == TRANSPARENT) {
						src[ayuv_idx] = MERGE35(0, 0x00);
					} else if (txt->background == WHITE) {
						src[ayuv_idx] = MERGE35(a, 0xff);
					} else if (txt->background == BLACK) {
						src[ayuv_idx] = MERGE35(a, 0x00);
					}
				}
			}
			ayuv_idx += 1;
		}
	}

	SDL_FreeSurface(tmp);
	SDL_FreeSurface(text);

	TTF_CloseFont(font);

	if (txt->outline_width > 0) {
		TTF_CloseFont(fontout);
	}

	TTF_Quit();

	return src;
}

char *OSD_createTextUnicodeSrc(OsdText *txt, int *width, int *height)
{
	if (TTF_Init() < 0) {
		fprintf(stderr, "failed to init sdlttf\r\n");
		SDL_Quit();
		return NULL;
	}

	TTF_Font *font, *fontout;
	font = TTF_OpenFont(txt->ttf_path, txt->size);
	if (font == NULL) {
		OSDERR("failed to open font, %s\r\n", SDL_GetError());
		return NULL;
	}

	SDL_Surface *text;
	SDL_Surface *tmp;
	SDL_Color forecol = { txt->color[0], txt->color[1], txt->color[2] };
	SDL_Color backcol = { 0xff, 0xff, 0xff };
	if (txt->background != WHITE) {
		setBackgroundColor(&txt->color[0], txt->background, &backcol);
	}

	if (txt->outline_width == 0) {
		SDL_PixelFormat *fmt;
		fmt = (SDL_PixelFormat *)malloc(sizeof(SDL_PixelFormat));
		if (fmt == NULL) {
			fprintf(stderr, "failed to alloc a SDL_PixelFormat\r\n");
		}
		memset(fmt, 0, sizeof(SDL_PixelFormat));
		fmt->BitsPerPixel = 32;
		fmt->BytesPerPixel = 4;
		text = TTF_RenderUNICODE_Shaded(font, (uint16_t *)&txt->unicode_txt[0], forecol, backcol);
		if (text == NULL) {
			OSDERR("Invaild Unicode word, %s\r\n", SDL_GetError());
			SDL_FreeSurface(text);
			TTF_CloseFont(font);
			return NULL;
		}
		tmp = SDL_ConvertSurface(text, fmt, 0);
		if (tmp == NULL) {
			OSDERR("failed convert interface, %s\r\n", SDL_GetError());
		}
#ifdef OSD_DEBUG
		char bmp_name[32];
		sprintf(&bmp_name[0], "/mnt/nfs/ethnfs/%d.bmp", (int)&txt->unicode_txt[0]);

		SDL_SaveBMP(tmp, &bmp_name[0]);
#endif
		free(fmt);
	} else {
		fontout = TTF_OpenFont(txt->ttf_path, txt->size);
		TTF_SetFontOutline(fontout, txt->outline_width);

		if (fontout == NULL) {
			fprintf(stderr, "failed to open font: %s\r\n", SDL_GetError());
			TTF_Quit();
			return NULL;
		}

		SDL_Color outcol = { txt->outline_color[0], txt->outline_color[1], txt->outline_color[2] };
		text = TTF_RenderUNICODE_Blended(font, (uint16_t *)&txt->unicode_txt[0], forecol);
		tmp = TTF_RenderUNICODE_Blended(fontout, (uint16_t *)&txt->unicode_txt[0], outcol);
		if ((text == NULL) || (tmp == NULL)) {
			OSDERR("Invaild Unicode word\r\n");
			SDL_FreeSurface(text);
			TTF_CloseFont(font);
			TTF_CloseFont(fontout);
			free(tmp);
			return NULL;
		}

		SDL_Rect rect = { txt->outline_width, txt->outline_width, text->w, text->h };

		SDL_SetSurfaceBlendMode(text, SDL_BLENDMODE_BLEND);
		SDL_BlitSurface(text, NULL, tmp, &rect);
#ifdef OSD_DEBUG
		char bmp_name[32];
		sprintf(&bmp_name[0], "/mnt/nfs/ethnfs/%s-outline.bmp", &txt->txt[0]);

		SDL_SaveBMP(tmp, &bmp_name[0]);
#endif
	}

	char *src = malloc(tmp->w * tmp->h * 2);
	int ayuv_idx = 0;
	*width = tmp->w;
	*height = tmp->h;
	char *bgra = (char *)tmp->pixels;
	int i, j, a, y, u, v, r, g, b;

	for (i = 0; i < tmp->h; i++) {
		for (j = 0; j < tmp->w * 4; j += 4) {
			b = (int)bgra[i * tmp->w * 4 + j];
			g = (int)bgra[i * tmp->w * 4 + j + 1];
			r = (int)bgra[i * tmp->w * 4 + j + 2];
			a = 255;

			if (txt->outline_width == 0) {
				a = 255;
				if (txt->background == TRANSPARENT) {
					if (((b == backcol.b) && (g == backcol.g)) && (r == backcol.r)) {
						a = 0x00;
					} else {
						a = 255;
					}
				}
			} else {
				a = (int)bgra[i * tmp->w * 4 + j + 3];
				if (txt->background != TRANSPARENT) {
					if (a == 0) {
						b = backcol.b;
						g = backcol.g;
						r = backcol.r;
						a = 255;
					}
				}
			}

			RGBTrans2YUV(&r, &g, &b, &y, &u, &v);
			normalizeAYUV3544(&a, &y, &u, &v);
			src[ayuv_idx] = MERGE44(u, v);
			ayuv_idx += 1;
			src[ayuv_idx] = MERGE35(a, y);
			ayuv_idx += 1;
		}
	}

	SDL_FreeSurface(tmp);
	SDL_FreeSurface(text);

	TTF_CloseFont(font);

	if (txt->outline_width > 0) {
		TTF_CloseFont(fontout);
	}

	TTF_Quit();

	return src;
}

char *OSD_createTextUnicodeSrc8bit(OsdText *txt, int *width, int *height)
{
	if (txt->mode != PALETTE_8) {
		OSDERR("wrong format\r\n");
		return NULL;
	}

	if (TTF_Init() < 0) {
		OSDERR("failed to init sdlttf\r\n");
		SDL_Quit();
		return NULL;
	}

	TTF_Font *font, *fontout;
	font = TTF_OpenFont(txt->ttf_path, txt->size);
	if (font == NULL) {
		OSDERR("failed to open font: %s\r\n", SDL_GetError());
		TTF_Quit();
		return NULL;
	}

	SDL_Surface *text;
	SDL_Surface *tmp;
	SDL_Color forecol = { 0xff, 0x00, 0x00 };
	SDL_Color backcol = { 0x00, 0x00, 0x00 };
	if (txt->outline_width > 0) {
		fontout = TTF_OpenFont(txt->ttf_path, txt->size);
		TTF_SetFontOutline(fontout, txt->outline_width);

		if (fontout == NULL) {
			fprintf(stderr, "failed to open font: %s\r\n", SDL_GetError());
			TTF_Quit();
			return NULL;
		}
		SDL_Color outcol = { 0x00, 0xff, 0x00 };

		text = TTF_RenderUNICODE_Blended(font, (uint16_t *)&txt->unicode_txt[0], forecol);
		tmp = TTF_RenderUNICODE_Blended(fontout, (uint16_t *)&txt->unicode_txt[0], outcol);
		if ((text == NULL) || (tmp == NULL)) {
			OSDERR("Invaild UTF8 word\r\n");
		}

		SDL_Rect rect = { txt->outline_width, txt->outline_width, text->w, text->h };

		SDL_SetSurfaceBlendMode(text, SDL_BLENDMODE_BLEND);
		SDL_BlitSurface(text, NULL, tmp, &rect);
#ifdef OSD_DEBUG
		char bmp_name[32];
		sprintf(&bmp_name[0], "/mnt/nfs/ethnfs/unicode-outline-8bpp.bmp");

		SDL_SaveBMP(tmp, &bmp_name[0]);
#endif
	} else {
		SDL_PixelFormat *fmt;
		fmt = (SDL_PixelFormat *)malloc(sizeof(SDL_PixelFormat));
		if (fmt == NULL) {
			fprintf(stderr, "failed to alloc a SDL_PixelFormat\r\n");
		}
		memset(fmt, 0, sizeof(SDL_PixelFormat));
		fmt->BitsPerPixel = 32;
		fmt->BytesPerPixel = 4;

		text = TTF_RenderUNICODE_Shaded(font, (uint16_t *)&txt->unicode_txt[0], forecol, backcol);
		if (text == NULL) {
			OSDERR("Invaild utf8 word: %s\r\n", SDL_GetError());
			SDL_FreeSurface(text);
			TTF_CloseFont(font);
			return NULL;
		}
		tmp = SDL_ConvertSurface(text, fmt, 0);
		if (tmp == NULL) {
			OSDERR("failed convert interface, %s\r\n", SDL_GetError());
		}
#ifdef OSD_DEBUG
		char bmp_name[32];
		sprintf(&bmp_name[0], "/mnt/nfs/ethnfs/unicode-8bpp.bmp");

		SDL_SaveBMP(tmp, &bmp_name[0]);
#endif
		free(fmt);
	}

	OSDLOG("alloc size: %d %d %d\r\n", tmp->w, tmp->h, tmp->w * tmp->h);
	char *src = malloc(tmp->w * tmp->h);
	int ayuv_idx = 0;
	*width = tmp->w;
	*height = tmp->h;
	char *bgra = (char *)tmp->pixels;
	int i, j, a, r, g, b;

	for (i = 0; i < tmp->h; i++) {
		for (j = 0; j < tmp->w * 4; j += 4) {
			b = (int)bgra[i * tmp->w * 4 + j];
			g = (int)bgra[i * tmp->w * 4 + j + 1];
			r = (int)bgra[i * tmp->w * 4 + j + 2];
			a = (int)bgra[i * tmp->w * 4 + j + 3];

			if (txt->outline_width > 0) {
				if ((r > g) && (r > b)) {
					src[ayuv_idx] = MERGE35(txt->color[0], txt->color[1]);
				} else if (a != 0) {
					/*outline*/
					src[ayuv_idx] = MERGE35(txt->outline_color[0], txt->outline_color[1]);
				} else {
					a = 255;
					if (txt->background == TRANSPARENT) {
						src[ayuv_idx] = MERGE35(0, 0x00);
					} else if (txt->background == WHITE) {
						src[ayuv_idx] = MERGE35(a, 0xff);
					} else if (txt->background == BLACK) {
						src[ayuv_idx] = MERGE35(a, 0x00);
					}
				}
			} else if (txt->outline_width == 0) {
				a = 255;
				if ((r > g) && (r > b)) {
					src[ayuv_idx] = MERGE35(txt->color[0], txt->color[1]);
				} else {
					if (txt->background == TRANSPARENT) {
						src[ayuv_idx] = MERGE35(0, 0x00);
					} else if (txt->background == WHITE) {
						src[ayuv_idx] = MERGE35(a, 0xff);
					} else if (txt->background == BLACK) {
						src[ayuv_idx] = MERGE35(a, 0x00);
					}
				}
			}
			ayuv_idx += 1;
		}
	}

	SDL_FreeSurface(tmp);
	SDL_FreeSurface(text);

	TTF_CloseFont(font);

	if (txt->outline_width > 0) {
		TTF_CloseFont(fontout);
	}

	TTF_Quit();

	return src;
}

int OSD_destroySrc(char *ptr)
{
	if (ptr == NULL) {
		OSDERR("ayuv src == null, can't destroy\r\n");
		return -EINVAL;
	} else {
		OSDERR("need to free\r\n");
	}
	free(ptr);
	return 0;
}

int OSD_setLine(OsdHandle *hd, int osd_idx, OsdLine *line, char *out)
{
	int tmp_idx = -1;
	for (int i = 0; i < MAX_OSD; ++i) {
		if (hd->osd_index[i] == osd_idx) {
			tmp_idx = i;
		}
	}

	if (tmp_idx == -1) {
		OSDERR("index %d not found\r\n", osd_idx);
		return -EINVAL;
	}

	/* trans to related position in canvas*/
	OsdPoint p[2];

	p[0].x = line->start.x - hd->region[tmp_idx].startX;
	p[0].y = line->start.y - hd->region[tmp_idx].startY;
	p[1].x = line->end.x - hd->region[tmp_idx].startX;
	p[1].y = line->end.y - hd->region[tmp_idx].startY;

	int include_canvas = hd->region[tmp_idx].include_canvas;
	int x_offset = hd->region[tmp_idx].startX - hd->canvas[include_canvas].startX;
	int y_offset = hd->region[tmp_idx].startY - hd->canvas[include_canvas].startY;
	/*move to start of region, not start of canvas*/

	out = out + (y_offset * hd->canvas[include_canvas].width + x_offset) * 2;

	OSDLOG("related canvas[%d], p(%d, %d), (%d, %d), offset: %d, %d\r\n", tmp_idx, p[0].x, p[0].y, p[1].x, p[1].y,
	       x_offset, y_offset);

	if (out == NULL) {
		char ayuv_buf[hd->region[tmp_idx].width * hd->region[tmp_idx].height * 2];
		memset(&ayuv_buf[0], 0x00, sizeof(ayuv_buf));
		drawThickLineSimple(p[0].x, p[0].y, p[1].x, p[1].y, line->thickness, LINE_THICKNESS_MIDDLE,
		                    &line->color[0], hd->region[tmp_idx].width, 0, 0, &hd->region[tmp_idx], line->mode,
		                    &ayuv_buf[0]);

		saveAYUV("/mnt/nfs/ethnfs/tmp-1.ayuv", hd->region[tmp_idx].width, hd->region[tmp_idx].height,
		         &ayuv_buf[0], hd->region[tmp_idx].width * hd->region[tmp_idx].height * 2);

		return 0;
	}

	drawThickLineSimple(p[0].x, p[0].y, p[1].x, p[1].y, line->thickness, LINE_THICKNESS_MIDDLE, &line->color[0],
	                    hd->canvas[include_canvas].width, x_offset, y_offset, &hd->region[tmp_idx], line->mode,
	                    out);

	return 0;
}

int OSD_setPrivacyMask(OsdHandle *hd, int osd_idx, char *p_color, COLOR_MODE mode, char *out)
{
	int tmp_idx = -1;
	for (int i = 0; i < MAX_OSD; ++i) {
		if (hd->osd_index[i] == osd_idx) {
			tmp_idx = i;
			OSDLOG("get tmp_idx = %d\r\n", tmp_idx);
		}
	}

	if (tmp_idx == -1) {
		OSDERR("index %d not found\r\n", osd_idx);
		return -EINVAL;
	}

	int x_offset;
	int y_offset, canvas_width, osd_width, osd_height;
	int flag = 0;

	for (int i = 0; i < hd->osd_num; i++) {
		for (int j = 0; j < hd->canvas[i].osd_num; j++) {
			if (hd->canvas[i].osd_list[j] == osd_idx) {
				x_offset = hd->region[tmp_idx].startX - hd->canvas[i].startX;

				y_offset = hd->region[tmp_idx].startY - hd->canvas[i].startY;
				canvas_width = hd->canvas[i].width;
				osd_width = hd->region[tmp_idx].width;
				osd_height = hd->region[tmp_idx].height;
				OSDLOG("get[%d] : %d, %d %d %d %d %d\r\n", i, osd_idx, x_offset, y_offset, canvas_width,
				       osd_width, osd_height);
				flag = 1;
				break;
			}
		}
		if (flag == 1) {
			break;
		}
	}

	if (mode == AYUV_3544) {
		char ayuv_buf[osd_width * osd_height * 2];
		int ayuv_idx = 0;
		char *rgba = p_color;
		int r, g, b, a, y, u, v;
		r = rgba[0];
		g = rgba[1];
		b = rgba[2];
		a = rgba[3];

		RGBTrans2YUV(&r, &g, &b, &y, &u, &v);
		normalizeAYUV3544(&a, &y, &u, &v);

		for (int i = 0; i < osd_height; i++) {
			for (int j = 0; j < osd_width * 4; j += 4) {
				ayuv_buf[ayuv_idx] = MERGE44(u, v);
				ayuv_idx += 1;
				ayuv_buf[ayuv_idx] = MERGE35(a, y);
				ayuv_idx += 1;
			}
		}

		/*load bmp & convert*/
		if (out == NULL) {
			OSDERR("out ptr not found\r\n");
			saveAYUV("/mnt/nfs/ethnfs/save-out-privacy.ayuv", osd_width, osd_height, &ayuv_buf[0],
			         osd_width * osd_height * 2);
			return -EINVAL;
		}

		int line = 0;
		for (int i = y_offset; i < y_offset + osd_height; i++) {
			memcpy((void *)out + (canvas_width * i + x_offset) * 2, (void *)&ayuv_buf[line * osd_width * 2],
			       osd_width * 2);

			line++;
		}
	} else if (mode == PALETTE_8) {
		char color_by_idx = MERGE35(p_color[0] /*Alpha*/, p_color[1] /*mode 9 idx*/);
		char ayuv_buf[osd_width * osd_height];
		memset(&ayuv_buf[0], color_by_idx, osd_width * osd_height);
		if (out == NULL) {
			OSDERR("out ptr not found\r\n");
			saveAYUV("/mnt/nfs/ethnfs/save-out-privacy-8bpp.ayuv", osd_width, osd_height, &ayuv_buf[0],
			         osd_width * osd_height);
			return -EINVAL;
		}

		int line = 0;
		for (int i = y_offset; i < y_offset + osd_height; i++) {
			memcpy((void *)out + (canvas_width * i + x_offset), (void *)&ayuv_buf[line * osd_width],
			       osd_width);

			line++;
		}
	}

	return 0;
}

uint16_t OSD_trans2Unicode(char txt)
{
	uint16_t ret = 0x0030;
	switch (txt) {
	case '0':
		ret = 0x0030;
		break;
	case '1':
		ret = 0x0031;
		break;
	case '2':
		ret = 0x0032;
		break;
	case '3':
		ret = 0x0033;
		break;
	case '4':
		ret = 0x0034;
		break;
	case '5':
		ret = 0x0035;
		break;
	case '6':
		ret = 0x0036;
		break;
	case '7':
		ret = 0x0037;
		break;
	case '8':
		ret = 0x0038;
		break;
	case '9':
		ret = 0x0039;
		break;
	case ':':
		ret = 0x003a;
		break;
	case '-':
		ret = 0x002d;
		break;
	case ' ':
		ret = 0x0020;
		break;
	default:
		break;
	}

	return ret;
}

int OSD_setRegionTransparent(OsdHandle *hd, int reg_idx, COLOR_MODE mode, char *out)
{
	int tmp_idx = -1;
	for (int i = 0; i < MAX_OSD; ++i) {
		if (hd->osd_index[i] == reg_idx) {
			tmp_idx = i;
		}
	}

	if (tmp_idx == -1) {
		OSDERR("index %d not found\r\n", reg_idx);
		return -EINVAL;
	}

	/* trans to related position in canvas*/
	int include_canvas = hd->region[tmp_idx].include_canvas;
	int x_offset = hd->region[tmp_idx].startX - hd->canvas[include_canvas].startX;
	int y_offset = hd->region[tmp_idx].startY - hd->canvas[include_canvas].startY;
	int canvas_width = hd->canvas[include_canvas].width;

	if (mode == AYUV_3544) {
		char transparentAYUV[2] = { 0xff, 0x1f };

		for (int i = y_offset; i < y_offset + hd->region[tmp_idx].height; i++) {
			for (int j = 0; j < hd->region[tmp_idx].width; j++) {
				memcpy((void *)out + (canvas_width * i + x_offset + j) * 2, (void *)&transparentAYUV[0],
				       2);
			}
		}

	} else if (mode == PALETTE_8) {
		char transparentAYUV = 0x00;

		for (int i = y_offset; i < y_offset + hd->region[tmp_idx].height; i++) {
			for (int j = 0; j < hd->region[tmp_idx].width; j++) {
				memcpy((void *)out + (canvas_width * i + x_offset + j), (void *)&transparentAYUV, 1);
			}
		}
	}

	return 0;
}

char *OSD_createUnicodeFontList(OsdText *txt, uint16_t *text_list, int len)
{
	if (TTF_Init() < 0) {
		fprintf(stderr, "failed to init sdlttf\r\n");
		SDL_Quit();
		return NULL;
	}

	TTF_Font *font, *fontout;
	font = TTF_OpenFont(txt->ttf_path, txt->size);
	SDL_Surface *tmp[len];

	SDL_Surface *text;
	SDL_Color forecol = { txt->color[0], txt->color[1], txt->color[2] };
	SDL_Color backcol = { 0xff, 0xff, 0xff };
	if (txt->background != WHITE) {
		setBackgroundColor(&txt->color[0], txt->background, &backcol);
	}

	if (txt->outline_width == 0) {
		SDL_PixelFormat *fmt;
		fmt = (SDL_PixelFormat *)malloc(sizeof(SDL_PixelFormat));
		if (fmt == NULL) {
			fprintf(stderr, "failed to alloc a SDL_PixelFormat\r\n");
		}
		memset(fmt, 0, sizeof(SDL_PixelFormat));
		fmt->BitsPerPixel = 32;
		fmt->BytesPerPixel = 4;

		for (int i = 0; i < len; i++) {
			txt->unicode_txt[0] = text_list[i];
			text = TTF_RenderUNICODE_Shaded(font, (uint16_t *)&txt->unicode_txt[0], forecol, backcol);
			if (text == NULL) {
				fprintf(stderr, "Invaild Unicode word\r\n");
				SDL_FreeSurface(text);
				break;
			}

			tmp[i] = SDL_ConvertSurface(text, fmt, 0);
			if (tmp[i] == NULL) {
				fprintf(stderr, "failed convert interface, %s\r\n", SDL_GetError());
			}

			OSDLOG("save %s, fcolor: %0x %0x %0x\r\n", "/tmp/save.bmp", txt->color[0], txt->color[1],
			       txt->color[2]);
			OSDERR("get (%d, %d)\r\n", tmp[i]->w, tmp[i]->h);
			SDL_FreeSurface(text);
#ifdef OSD_DEBUG
			char bmp_name[32];
			sprintf(&bmp_name[0], "/mnt/nfs/ethnfs/%d.bmp", text_list[i]);

			SDL_SaveBMP(tmp[i], &bmp_name[0]);
#endif
		}

		free(fmt);
	} else {
		fontout = TTF_OpenFont(txt->ttf_path, txt->size);
		TTF_SetFontOutline(fontout, txt->outline_width);

		if (fontout == NULL) {
			fprintf(stderr, "failed to open font: %s\r\n", SDL_GetError());
			TTF_Quit();
			return NULL;
		}

		SDL_Color outcol = { txt->outline_color[0], txt->outline_color[1], txt->outline_color[2] };

		for (int i = 0; i < len; i++) {
			txt->unicode_txt[0] = text_list[i];
			text = TTF_RenderUNICODE_Blended(font, (uint16_t *)&txt->unicode_txt[0], forecol);
			tmp[i] = TTF_RenderUNICODE_Blended(fontout, (uint16_t *)&txt->unicode_txt[0], outcol);
			if ((text == NULL) || (tmp[i] == NULL)) {
				OSDERR("Invaild UTF8 word\r\n");
			}
			SDL_Rect rect = { txt->outline_width, txt->outline_width, text->w, text->h };

			SDL_SetSurfaceBlendMode(text, SDL_BLENDMODE_BLEND);
			SDL_BlitSurface(text, NULL, tmp[i], &rect);
#ifdef DEBUG
			char bmp_name[32];
			sprintf(&bmp_name[0], "/mnt/nfs/ethnfs/%d-outline.bmp", text_list[i]);

			SDL_SaveBMP(tmp[i], &bmp_name[0]);
#endif
			SDL_FreeSurface(text);
		}
	}

	AyuvSrcList *ayuv_list;
	ayuv_list = malloc(sizeof(AyuvSrcList));
	ayuv_list->len = len;
	ayuv_list->src = malloc(sizeof(AyuvSrc) * ayuv_list->len);

	int ayuv_idx = 0;
	char *tmp_ptr;
	int i, j, k, r, g, b, a, y, u, v;
	for (i = 0; i < ayuv_list->len; i++) {
		ayuv_idx = 0;
		OSDLOG("gen [%d], idx: %d, loc: %d\r\n", i, ayuv_idx, tmp[i]->w * tmp[i]->h * 2);

		ayuv_list->src[i].width = tmp[i]->w;
		ayuv_list->src[i].height = tmp[i]->h;
		ayuv_list->src[i].unicode = text_list[i];
		ayuv_list->src[i].src = malloc(tmp[i]->w * tmp[i]->h * 2);

		tmp_ptr = tmp[i]->pixels;
		for (j = 0; j < tmp[i]->h; j++) {
			for (k = 0; k < tmp[i]->w * 4; k += 4) {
				b = (int)tmp_ptr[j * tmp[i]->w * 4 + k];
				g = (int)tmp_ptr[j * tmp[i]->w * 4 + k + 1];
				r = (int)tmp_ptr[j * tmp[i]->w * 4 + k + 2];

				if (txt->outline_width == 0) {
					a = 255;
					if (txt->background == TRANSPARENT) {
						if (((b == backcol.b) && (g == backcol.g)) && (r == backcol.r)) {
							a = 0x00;
						} else {
							a = 255;
						}
					}
				} else {
					a = (int)tmp_ptr[j * tmp[i]->w * 4 + k + 3];
					if (txt->background != TRANSPARENT) {
						if (a == 0) {
							b = backcol.b;
							g = backcol.g;
							r = backcol.r;
							a = 255;
						}
					}
				}

				RGBTrans2YUV(&r, &g, &b, &y, &u, &v);
				normalizeAYUV3544(&a, &y, &u, &v);

				ayuv_list->src[i].src[ayuv_idx] = MERGE44(u, v);
				ayuv_idx += 1;
				ayuv_list->src[i].src[ayuv_idx] = MERGE35(a, y);
				ayuv_idx += 1;
			}
		}
#ifdef OSD_DEBUG
		char ayuv_name[32];
		sprintf(&ayuv_name[0], "/mnt/nfs/ethnfs/%d-outline.bmp", i);
		saveAYUV(&ayuv_name[0], tmp[i]->w, tmp[i]->h, &ayuv_list->src[i].src[0], tmp[i]->w * tmp[i]->w * 2);
#endif
	}

	/*sdl save exit*/
	for (int i = 0; i < ayuv_list->len; i++) {
		SDL_FreeSurface(tmp[i]);
	}

	TTF_CloseFont(font);
	if (txt->outline_width > 0) {
		TTF_CloseFont(fontout);
	}
	TTF_Quit();

	return (char *)ayuv_list;
}

char *OSD_createUnicodeFontList8bit(OsdText *txt, uint16_t *text_list, int len)
{
	OSDERR("enter\r\n");
	if (txt->mode != PALETTE_8) {
		OSDERR("wrong format\r\n");
		return NULL;
	}

	if (TTF_Init() < 0) {
		OSDERR("failed to init sdlttf\r\n");
		SDL_Quit();
		return NULL;
	}

	TTF_Font *font, *fontout;
	font = TTF_OpenFont(txt->ttf_path, txt->size);
	if (font == NULL) {
		OSDERR("failed to open font: %s\r\n", SDL_GetError());
		TTF_Quit();
		return NULL;
	}

	SDL_Surface *tmp[len];
	SDL_Surface *text;
	SDL_Color forecol = { 0xff, 0x00, 0x00 };
	SDL_Color backcol = { 0x00, 0x00, 0x00 };

	if (txt->outline_width == 0) {
		SDL_PixelFormat *fmt;
		fmt = (SDL_PixelFormat *)malloc(sizeof(SDL_PixelFormat));
		if (fmt == NULL) {
			fprintf(stderr, "failed to alloc a SDL_PixelFormat\r\n");
		}
		memset(fmt, 0, sizeof(SDL_PixelFormat));
		fmt->BitsPerPixel = 32;
		fmt->BytesPerPixel = 4;

		for (int i = 0; i < len; i++) {
			txt->unicode_txt[0] = text_list[i];
			text = TTF_RenderUNICODE_Shaded(font, (uint16_t *)&txt->unicode_txt[0], forecol, backcol);
			if (text == NULL) {
				fprintf(stderr, "Invaild Unicode word\r\n");
				SDL_FreeSurface(text);
				break;
			}

			tmp[i] = SDL_ConvertSurface(text, fmt, 0);
			if (tmp[i] == NULL) {
				fprintf(stderr, "failed convert interface, %s\r\n", SDL_GetError());
			}

			OSDLOG("save %s, fcolor: %0x %0x %0x\r\n", "/tmp/save.bmp", txt->color[0], txt->color[1],
			       txt->color[2]);
			OSDLOG("get (%d, %d)\r\n", tmp[i]->w, tmp[i]->h);
			SDL_FreeSurface(text);
#ifdef OSD_DEBUG
			char bmp_name[32];
			sprintf(&bmp_name[0], "/mnt/nfs/ethnfs/%d.bmp", text_list[i]);

			SDL_SaveBMP(tmp[i], &bmp_name[0]);
#endif
		}

		free(fmt);
	} else {
		fontout = TTF_OpenFont(txt->ttf_path, txt->size);
		TTF_SetFontOutline(fontout, txt->outline_width);

		if (fontout == NULL) {
			fprintf(stderr, "failed to open font: %s\r\n", SDL_GetError());
			TTF_Quit();
			return NULL;
		}

		SDL_Color outcol = { 0x00, 0xff, 0x00 };

		for (int i = 0; i < len; i++) {
			txt->unicode_txt[0] = text_list[i];
			text = TTF_RenderUNICODE_Blended(font, (uint16_t *)&txt->unicode_txt[0], forecol);
			tmp[i] = TTF_RenderUNICODE_Blended(fontout, (uint16_t *)&txt->unicode_txt[0], outcol);
			if ((text == NULL) || (tmp[i] == NULL)) {
				OSDERR("Invaild UTF8 word\r\n");
			}
			SDL_Rect rect = { txt->outline_width, txt->outline_width, text->w, text->h };

			SDL_SetSurfaceBlendMode(text, SDL_BLENDMODE_BLEND);
			SDL_BlitSurface(text, NULL, tmp[i], &rect);
#ifdef DEBUG
			char bmp_name[32];
			sprintf(&bmp_name[0], "/mnt/nfs/ethnfs/%d-outline.bmp", text_list[i]);

			SDL_SaveBMP(tmp[i], &bmp_name[0]);
#endif
			SDL_FreeSurface(text);
		}
	}

	AyuvSrcList *ayuv_list;
	ayuv_list = malloc(sizeof(AyuvSrcList));
	ayuv_list->len = len;
	ayuv_list->src = malloc(sizeof(AyuvSrc) * ayuv_list->len);

	int ayuv_idx = 0;
	char *tmp_ptr;
	int i, j, k, r, g, b, a;
	for (i = 0; i < ayuv_list->len; i++) {
		ayuv_idx = 0;
		OSDLOG("gen [%d], idx: %d, loc: %d\r\n", i, ayuv_idx, tmp[i]->w * tmp[i]->h);

		ayuv_list->src[i].width = tmp[i]->w;
		ayuv_list->src[i].height = tmp[i]->h;
		ayuv_list->src[i].unicode = text_list[i];
		ayuv_list->src[i].src = malloc(tmp[i]->w * tmp[i]->h);

		OSDLOG("-->%d %d %d %d\n", ayuv_list->len, ayuv_list->src[i].width, ayuv_list->src[i].height,
		       ayuv_list->src[i].unicode);

		tmp_ptr = tmp[i]->pixels;
		for (j = 0; j < tmp[i]->h; j++) {
			for (k = 0; k < tmp[i]->w * 4; k += 4) {
				b = (int)tmp_ptr[j * tmp[i]->w * 4 + k];
				g = (int)tmp_ptr[j * tmp[i]->w * 4 + k + 1];
				r = (int)tmp_ptr[j * tmp[i]->w * 4 + k + 2];
				a = (int)tmp_ptr[j * tmp[i]->w * 4 + k + 3];

				if (txt->outline_width == 0) {
					a = 255;
					if ((r > g) && (r > b)) {
						ayuv_list->src[i].src[ayuv_idx] = MERGE35(txt->color[0], txt->color[1]);
					} else {
						if (txt->background == TRANSPARENT) {
							ayuv_list->src[i].src[ayuv_idx] = MERGE35(0, 0x00);
						} else if (txt->background == WHITE) {
							ayuv_list->src[i].src[ayuv_idx] = MERGE35(a, 0xff);
						} else if (txt->background == BLACK) {
							ayuv_list->src[i].src[ayuv_idx] = MERGE35(a, 0x00);
						}
					}
				} else {
					if ((r > g) && (r > b)) {
						ayuv_list->src[i].src[ayuv_idx] = MERGE35(txt->color[0], txt->color[1]);
					} else if (a != 0) {
						/*outline*/
						ayuv_list->src[i].src[ayuv_idx] =
						        MERGE35(txt->outline_color[0], txt->outline_color[1]);
					} else {
						a = 255;
						if (txt->background == TRANSPARENT) {
							ayuv_list->src[i].src[ayuv_idx] = MERGE35(0, 0x00);
						} else if (txt->background == WHITE) {
							ayuv_list->src[i].src[ayuv_idx] = MERGE35(a, 0xff);
						} else if (txt->background == BLACK) {
							ayuv_list->src[i].src[ayuv_idx] = MERGE35(a, 0x00);
						}
					}
				}

				ayuv_idx += 1;
			}
		}
#ifdef OSD_DEBUG
		char ayuv_name[32];
		sprintf(&ayuv_name[0], "/mnt/nfs/ethnfs/%d-outline.bmp", i);
#endif
	}

	/*sdl save exit*/
	for (int i = 0; i < ayuv_list->len; i++) {
		SDL_FreeSurface(tmp[i]);
	}

	TTF_CloseFont(font);
	if (txt->outline_width > 0) {
		TTF_CloseFont(fontout);
	}
	TTF_Quit();

	return (char *)ayuv_list;
}

int OSD_getUnicodeSizetoGenerate(uint16_t *text_list, int len, char *src_ptr, int *width, int *height)
{
	int total_width = 0;
	int total_height = 0;
	AyuvSrcList *ayuv_list = (AyuvSrcList *)src_ptr;
	for (int i = 0; i < len; i++) {
		for (int j = 0; j < ayuv_list->len; j++) {
			if (text_list[i] == ayuv_list->src[j].unicode) {
				total_width += ayuv_list->src[j].width;
				if (total_height < ayuv_list->src[j].height) {
					total_height = ayuv_list->src[j].height;
				}
			}
		}
	}

	*width = total_width;
	*height = total_height;
	return 0;
}

int OSD_generateUnicodeFromList(uint16_t *text_list, int len, char *src_ptr, char *dst_ptr, int dst_width,
                                int dst_height)
{
	int x_offset = 0;
	int x_end_offset = 0;
	int y_offset = 0;
	int tmp_idx;

	char *ayuv_src;
	for (int i = 0; i < len; i++) {
		OSDLOG("x offset: %d\r\n", x_offset);
		for (int j = 0; j < ((AyuvSrcList *)src_ptr)->len; j++) {
			if (text_list[i] == ((AyuvSrcList *)src_ptr)->src[j].unicode) {
				tmp_idx = j;
				ayuv_src = (char *)((AyuvSrcList *)src_ptr)->src[j].src;
				break;
			}
		}
		x_end_offset = ((AyuvSrcList *)src_ptr)->src[tmp_idx].width;
		OSDLOG("x end offset: %d\r\n", x_end_offset);
		y_offset = dst_height - ((AyuvSrcList *)src_ptr)->src[tmp_idx].height;
		if (y_offset > 0) {
			OSDLOG("height < dst hieght, offset: %d\r\n", y_offset);
		}

		char blank[2] = { ayuv_src[0], ayuv_src[1] };
		for (int j = 0; j < y_offset; j++) {
			for (int k = 0; k < x_end_offset * 2; k += 2) {
				memcpy(&dst_ptr[x_offset * 2 + (j * dst_width) * 2 + k], &blank[0], 2 * sizeof(char));
			}
		}

		for (int j = y_offset; j < dst_height; j++) {
			for (int k = 0; k < x_end_offset * 2; k += 2) {
				OSDLOG("[%d]copy %d len %d to %d\r\n", k, (j - y_offset) * x_end_offset * 2 + k + k,
				       2 * sizeof(char), x_offset * 2 + (j * dst_width) * 2 + k);
				memcpy(&dst_ptr[x_offset * 2 + (j * dst_width) * 2 + k],
				       ayuv_src + ((j - y_offset) * x_end_offset * 2 + k), 2 * sizeof(char));
			}
		}
		x_offset += x_end_offset;
	}

	return 0;
}

int OSD_generateUnicodeFromList8bit(uint16_t *text_list, int len, char *src_ptr, char *dst_ptr, int dst_width,
                                    int dst_height)
{
	int x_offset = 0;
	int x_end_offset = 0;
	int y_offset = 0;
	int tmp_idx;

	char *ayuv_src;
	for (int i = 0; i < len; i++) {
		OSDLOG("x offset: %d\r\n", x_offset);
		for (int j = 0; j < ((AyuvSrcList *)src_ptr)->len; j++) {
			if (text_list[i] == ((AyuvSrcList *)src_ptr)->src[j].unicode) {
				tmp_idx = j;
				ayuv_src = (char *)((AyuvSrcList *)src_ptr)->src[j].src;
				break;
			}
		}
		x_end_offset = ((AyuvSrcList *)src_ptr)->src[tmp_idx].width;
		OSDLOG("x end offset: %d\r\n", x_end_offset);
		y_offset = dst_height - ((AyuvSrcList *)src_ptr)->src[tmp_idx].height;

		if (y_offset > 0) {
			OSDLOG("height < dst hieght, offset: %d\r\n", y_offset);
		}

		char blank = ayuv_src[0];
		for (int j = 0; j < y_offset; j++) {
			for (int k = 0; k < x_end_offset; k += 1) {
				memcpy(&dst_ptr[x_offset + (j * dst_width) + k], &blank, sizeof(char));
			}
		}

		for (int j = y_offset; j < dst_height; j++) {
			for (int k = 0; k < x_end_offset; k += 1) {
				OSDLOG("[%d]copy %d len %d to %d\r\n", k, (j - y_offset) * x_end_offset + k,
				       sizeof(char), x_offset + (j * dst_width) + k);
				memcpy(&dst_ptr[x_offset + (j * dst_width) + k],
				       ayuv_src + ((j - y_offset) * x_end_offset + k), sizeof(char));
			}
		}

		x_offset += x_end_offset;
	}

	return 0;
}

int OSD_destroyUnicodeFontList(char *ptr)
{
	AyuvSrcList *ayuv_list = (AyuvSrcList *)ptr;
	for (int i = 0; i < ayuv_list->len; i++) {
		free(ayuv_list->src[i].src);
	}
	free(ayuv_list->src);
	free(ayuv_list);

	return 0;
}

int OSD_setImageAYUVptr(OsdHandle *hd, int osd_idx, char *src_ptr, int img_width, int img_height, COLOR_MODE mode,
                        char *out)
{
	int tmp_idx = -1;
	for (int i = 0; i < MAX_OSD; ++i) {
		if (hd->osd_index[i] == osd_idx) {
			tmp_idx = i;
			OSDLOG("get tmp_idx = %d\r\n", tmp_idx);
		}
	}

	if (tmp_idx == -1) {
		OSDERR("index %d not found\r\n", osd_idx);
		return -EINVAL;
	}

	if (out == NULL) {
		OSDERR("out ptr not found\r\n");
		return -EINVAL;
	}

	int x_offset;
	int y_offset, canvas_width, osd_width, osd_height;
	int flag = 0;

	for (int i = 0; i < hd->osd_num; i++) {
		for (int j = 0; j < hd->canvas[i].osd_num; j++) {
			if (hd->canvas[i].osd_list[j] == osd_idx) {
				x_offset = hd->region[tmp_idx].startX - hd->canvas[i].startX;
				y_offset = hd->region[tmp_idx].startY - hd->canvas[i].startY;
				canvas_width = hd->canvas[i].width;
				osd_width = hd->region[tmp_idx].width;
				osd_height = hd->region[tmp_idx].height;

				OSDLOG("get[%d] : %d, %d %d %d %d %d\r\n", i, osd_idx, x_offset, y_offset, canvas_width,
				       osd_width, osd_height);
				flag = 1;
				break;
			}
		}
		if (flag) {
			break;
		}
	}

	if ((img_width != osd_width) || (img_height != osd_height)) {
		OSDLOG("Img size != OSD size, (%d, %d) (%d, %d)\r\n", img_width, img_height, osd_width, osd_height);
	}

	int copy_width, copy_height;
	if (img_width > osd_width) {
		copy_width = osd_width;
	} else {
		copy_width = img_width;
	}

	if (img_height > osd_height) {
		copy_height = osd_height;
	} else {
		copy_height = img_height;
	}

	if (src_ptr == NULL) {
		OSDERR("SRC is null\r\n");
		return -EINVAL;
	}


	if (out == NULL) {
		OSDERR("ptr == NULL\r\n");
		return -EINVAL;
	}

	int line = 0;
	if (mode == AYUV_3544) {
		alignUVVal(src_ptr, img_width, img_height);

		for (int i = y_offset; i < y_offset + copy_height; i++) {
			OSDLOG("cp %d len %d to %d\r\n", line * img_width * 2, copy_width * 2,
			       ((i * canvas_width + x_offset)) * 2);

			memcpy((void *)out + (canvas_width * i + x_offset) * 2, (void *)&src_ptr[line * img_width * 2],
			       copy_width * 2);

			line++;
		}
	} else if (mode == PALETTE_8) {
		for (int i = y_offset; i < y_offset + copy_height; i++) {
			OSDLOG("cp %d len %d to %d\r\n", line * img_width, copy_width, ((i * canvas_width + x_offset)));
			memcpy((void *)out + (canvas_width * i + x_offset), (void *)&src_ptr[line * img_width],
			       copy_width);

			line++;
		}
	}

	return 0;
}

int OSD_moveOsdRegion(OsdHandle *phd, int osd_idx, OsdRegion *region)
{
	if (region->startX > phd->width) {
		OSDERR("startX exceed handle width\r\n");
		return -EINVAL;
	}

	if (region->startY > phd->height) {
		OSDERR("startY exceed handle height\r\n");
		return -EINVAL;
	}

	if (CEILINGALIGN16(region->startX + region->width) > phd->width) {
		OSDERR("canvas > width after align 16\r\n");
		return -EINVAL;
	}

	if (CEILINGALIGN16(region->startY + region->height) > phd->height) {
		OSDERR("canvas > height after align 16\r\n");
		return -EINVAL;
	}

	if ((osd_idx > MAX_OSD) || (osd_idx < 0)) {
		OSDERR("invalid del idx: %d\r\n", osd_idx);
		return -EINVAL;
	}

	int tmp_idx = -1;
	for (int i = 0; i < MAX_OSD; ++i) {
		if (phd->osd_index[i] == osd_idx) {
			tmp_idx = i;
			OSDLOG("get osd_index[%d]: %d\r\n", i, osd_idx);
			break;
		}
	}

	if (tmp_idx == -1) {
		OSDERR("can't find this idx:%d\r\n", osd_idx);
		return -EINVAL;
	}

	int old_x, new_x, old_y, new_y;
	old_x = phd->region[tmp_idx].startX;
	old_y = phd->region[tmp_idx].startY;
	new_x = region->startX;
	new_y = region->startY;

	/*Mod 16 should be 0*/
	if ((((new_x - old_x) & 0x0f) != 0) || (((new_y - old_y) & 0x0f) != 0)) {
		OSDERR("move step not align 16 (%d, %d)\r\n", (new_x - old_x), (new_y - old_y));
		return -EINVAL;
	}

	OSDLOG("move [%d] (%d, %d) --> (%d, %d)\r\n", osd_idx, phd->region[tmp_idx].startX, phd->region[tmp_idx].startY,
	       region->startX, region->startY);

	phd->region[tmp_idx].startX = region->startX;
	phd->region[tmp_idx].startY = region->startY;

	return 0;
}
