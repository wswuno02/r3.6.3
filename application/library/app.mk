mod := $(notdir $(subdir))

app-$(CONFIG_LIBADO) += libado
PHONY += libado libado-clean libado-distclean
PHONY += libado-install libado-uninstall
libado:
	$(Q)$(MAKE) -C $(LIBADO_PATH) all

libado-clean:
	$(Q)$(MAKE) -C $(LIBADO_PATH) clean

libado-distclean:
	$(Q)$(MAKE) -C $(LIBADO_PATH) distclean

libado-install:
	$(Q)$(MAKE) -C $(LIBADO_PATH) install

libado-uninstall:
	$(Q)$(MAKE) -C $(LIBADO_PATH) uninstall

app-$(CONFIG_LIBAGTX) += libagtx
PHONY += libagtx libagtx-clean libagtx-distclean
PHONY += libagtx-install libagtx-uninstall
libagtx:
	$(Q)$(MAKE) -C $(LIBAGTX_PATH)/build all

libagtx-clean:
	$(Q)$(MAKE) -C $(LIBAGTX_PATH)/build clean

libagtx-distclean:
	$(Q)$(MAKE) -C $(LIBAGTX_PATH)/build distclean

libagtx-install:
	$(Q)$(MAKE) -C $(LIBAGTX_PATH)/build install

libagtx-uninstall:
	$(Q)$(MAKE) -C $(LIBAGTX_PATH)/build uninstall

app-$(CONFIG_LIBAVFTR) += libavftr
PHONY += libavftr libavftr-clean libavftr-distclean
PHONY += libavftr-install libavftr-uninstall
libavftr:
	$(MAKE) -C $(LIBAVFTR_BUILD_PATH) all

libavftr-clean:
	$(MAKE) -C $(LIBAVFTR_BUILD_PATH) clean

libavftr-distclean:
	$(MAKE) -C $(LIBAVFTR_BUILD_PATH) distclean

libavftr-install:
	$(MAKE) -C $(LIBAVFTR_BUILD_PATH) install

libavftr-uninstall:
	$(MAKE) -C $(LIBAVFTR_BUILD_PATH) uninstall

app-$(CONFIG_LIBCM) += libcm
PHONY += libcm libcm-clean libcm-distclean
PHONY += libcm-install libcm-uninstall
libcm:
	$(Q)$(MAKE) -C $(LIBCM_PATH) all

libcm-clean:
	$(Q)$(MAKE) -C $(LIBCM_PATH) clean

libcm-distclean:
	$(Q)$(MAKE) -C $(LIBCM_PATH) distclean

libcm-install:
	$(Q)$(MAKE) -C $(LIBCM_PATH) install

libcm-uninstall:
	$(Q)$(MAKE) -C $(LIBCM_PATH) uninstall

app-$(CONFIG_LIBCM_X86) += libcm_x86
PHONY += libcm_x86 libcm_x86-clean libcm_x86-distclean
PHONY += libcm_x86-install libcm_x86-uninstall
libcm_x86:
	$(Q)cp -rf $(LIBCM_PATH) $(LIBCM_PATH)_tmp
	$(Q)cp -f $(LIBCM_PATH)_tmp/Makefile-x86 $(LIBCM_PATH)_tmp/Makefile
	$(Q)$(MAKE) -C $(LIBCM_PATH)_tmp clean
	$(Q)$(MAKE) -C $(LIBCM_PATH)_tmp all install

libcm_x86-clean:
ifneq ("$(wildcard $(LIBCM_PATH)_tmp)", "")
	$(Q)$(MAKE) -C $(LIBCM_PATH)_tmp clean || true
endif
	$(Q)rm -rf $(LIBCM_PATH)_tmp

libcm_x86-distclean:
ifneq ("$(wildcard $(LIBCM_PATH)_tmp)", "")
	$(Q)$(MAKE) -C $(LIBCM_PATH)_tmp distclean || true
endif
	$(Q)rm -rf $(LIBCM_PATH)_tmp

libcm_x86-install: ;
libcm_x86-uninstall: ;

app-$(CONFIG_FOO) += libfoo
PHONY += libfoo libfoo-clean libfoo-distclean
PHONY += libfoo-install libfoo-uninstall
libfoo:
	$(Q)$(MAKE) -C $(LIBFOO_PATH)/build all

libfoo-clean:
	$(Q)$(MAKE) -C $(LIBFOO_PATH)/build clean

libfoo-distclean:
	$(Q)$(MAKE) -C $(LIBFOO_PATH)/build distclean

libfoo-install:
	$(Q)$(MAKE) -C $(LIBFOO_PATH)/build install

libfoo-uninstall:
	$(Q)$(MAKE) -C $(LIBFOO_PATH)/build uninstall

app-$(CONFIG_LIBFSINK) += libfsink
PHONY += libfsink libfsink-clean libfsink-distclean
PHONY += libfsink-install libfsink-uninstall
libfsink:
	$(Q)$(MAKE) -C $(FILE_PATH) all
	$(Q)$(MAKE) -C $(UDPS_PATH) all

libfsink-clean:
	$(Q)$(MAKE) -C $(UDPS_PATH) clean
	$(Q)$(MAKE) -C $(FILE_PATH) clean

libfsink-distclean:
	$(Q)$(MAKE) -C $(UDPS_PATH) distclean
	$(Q)$(MAKE) -C $(FILE_PATH) distclean

libfsink-install:
	$(Q)$(MAKE) -C $(FILE_PATH) install
	$(Q)$(MAKE) -C $(UDPS_PATH) install

libfsink-uninstall:
	$(Q)$(MAKE) -C $(UDPS_PATH) uninstall
	$(Q)$(MAKE) -C $(FILE_PATH) uninstall

app-$(CONFIG_LIBGPIO) += libgpio
PHONY += libgpio libgpio-clean libgpio-distclean
PHONY += libgpio-install libgpio-uninstall
libgpio:
	$(Q)$(MAKE) -C $(LIBGPIO_PATH) all

libgpio-clean:
	$(Q)$(MAKE) -C $(LIBGPIO_PATH) clean

libgpio-distclean:
	$(Q)$(MAKE) -C $(LIBGPIO_PATH) distclean

libgpio-install:
	$(Q)$(MAKE) -C $(LIBGPIO_PATH) install

libgpio-uninstall:
	$(Q)$(MAKE) -C $(LIBGPIO_PATH) uninstall

app-$(CONFIG_LIBOSD) += libosd
PHONY += libosd libosd-clean libosd-distclean
PHONY += libosd-install libosd-uninstall
libosd:
	$(Q)$(MAKE) -C $(LIBOSD_PATH)/build all

libosd-clean:
	$(Q)$(MAKE) -C $(LIBOSD_PATH)/build clean

libosd-distclean:
	$(Q)$(MAKE) -C $(LIBOSD_PATH)/build distclean

libosd-install:
	$(Q)$(MAKE) -C $(LIBOSD_PATH)/build install

libosd-uninstall:
	$(Q)$(MAKE) -C $(LIBOSD_PATH)/build uninstall



app-$(CONFIG_LIBUTILS) += libutils
PHONY += libutils libutils-clean libutils-distclean
PHONY += libutils-install libutils-uninstall
libutils:
	$(Q)$(MAKE) -C $(LIBUTILS_PATH)/build all

libutils-clean:
	$(Q)$(MAKE) -C $(LIBUTILS_PATH)/build clean

libutils-distclean:
	$(Q)$(MAKE) -C $(LIBUTILS_PATH)/build clean

libutils-install:
	$(Q)$(MAKE) -C $(LIBUTILS_PATH)/build install

libutils-uninstall:
	$(Q)$(MAKE) -C $(LIBUTILS_PATH)/build uninstall

app-$(CONFIG_LIBPRIO) += libprio
PHONY += libprio libprio-clean libprio-distclean
PHONY += libprio-install libprio-uninstall
libprio:
	$(Q)$(MAKE) -C $(LIBPRIO_PATH) all

libprio-clean:
	$(Q)$(MAKE) -C $(LIBPRIO_PATH) clean

libprio-distclean:
	$(Q)$(MAKE) -C $(LIBPRIO_PATH) distclean

libprio-install:
	$(Q)$(MAKE) -C $(LIBPRIO_PATH) install

libprio-uninstall:
	$(Q)$(MAKE) -C $(LIBPRIO_PATH) uninstall

PHONY += libpwm libpwm-clean libpwm-distclean
PHONY += libpwm-install libpwm-uninstall
app-$(CONFIG_LIBPWM) += libpwm
libpwm:
	$(Q)$(MAKE) -C $(LIBPWM_PATH) all

libpwm-clean:
	$(Q)$(MAKE) -C $(LIBPWM_PATH) clean

libpwm-distclean:
	$(Q)$(MAKE) -C $(LIBPWM_PATH) distclean

libpwm-install:
	$(Q)$(MAKE) -C $(LIBPWM_PATH) install

libpwm-uninstall:
	$(Q)$(MAKE) -C $(LIBPWM_PATH) uninstall

app-$(CONFIG_LIBSQL) += libsql
PHONY += libsql libsql-clean libsql-distclean
PHONY += libsql-install libsql-uninstall
libsql:
	$(Q)$(MAKE) -C $(LIBSQL_PATH) all

libsql-clean:
	$(Q)$(MAKE) -C $(LIBSQL_PATH) clean

libsql-distclean:
	$(Q)$(MAKE) -C $(LIBSQL_PATH) distclean

libsql-install:
	$(Q)$(MAKE) -C $(LIBSQL_PATH) install

libsql-uninstall:
	$(Q)$(MAKE) -C $(LIBSQL_PATH) uninstall

app-$(CONFIG_LIBTZ) += libtz
PHONY += libtz libtz-clean libtz-distclean
PHONY += libtz-install libtz-uninstall
libtz:
	$(Q)$(MAKE) -C $(LIBTZ_PATH) all

libtz-clean:
	$(Q)$(MAKE) -C $(LIBTZ_PATH) clean

libtz-distclean:
	$(Q)$(MAKE) -C $(LIBTZ_PATH) distclean

libtz-install:
	$(Q)$(MAKE) -C $(LIBTZ_PATH) install

libtz-uninstall:
	$(Q)$(MAKE) -C $(LIBTZ_PATH) uninstall

app-$(CONFIG_LIBEAIF) += libeaif
PHONY += libeaif libeaif-clean libeaif-distclean
PHONY += libeaif-install libeaif-uninstall

libeaif:
	$(Q)$(MAKE) -C $(LIBEAIF_PATH)/build all

libeaif-clean:
	$(Q)$(MAKE) -C $(LIBEAIF_PATH)/build clean

libeaif-install:
	$(Q)$(MAKE) -C $(LIBEAIF_PATH)/build install

libeaif-uninstall:
	$(Q)$(MAKE) -C $(LIBEAIF_PATH)/build uninstall

libeaif-distclean:
	$(Q)$(MAKE) -C $(LIBEAIF_PATH)/build distclean

app-$(CONFIG_LIBINF) += libinf
PHONY += libinf libinf-clean libinf-distclean
PHONY += libinf-install libinf-uninstall

libinf:
	$(Q)$(MAKE) -C $(LIBINF_PATH)/build all

libinf-clean:
	$(Q)$(MAKE) -C $(LIBINF_PATH)/build clean

libinf-install:
	$(Q)$(MAKE) -C $(LIBINF_PATH)/build install

libinf-uninstall:
	$(Q)$(MAKE) -C $(LIBINF_PATH)/build uninstall

libinf-distclean:
	$(Q)$(MAKE) -C $(LIBINF_PATH)/build distclean

PHONY += $(mod) $(mod)-clean $(mod)-distclean
PHONY += $(mod)-install $(mod)-uninstall
$(mod): $(app-y)
$(mod)-clean: $(addsuffix -clean,$(app-y))
$(mod)-distclean: $(addsuffix -distclean,$(app-y))
$(mod)-install: $(addsuffix -install,$(app-y))
$(mod)-uninstall: $(addsuffix -uninstall,$(app-y))

APP_BUILD_DEPS += $(mod)
APP_CLEAN_DEPS += $(mod)-clean
APP_DISTCLEAN_DEPS += $(mod)-distclean
APP_INTALL_DEPS += $(mod)-install
APP_UNINTALL_DEPS += $(mod)-uninstall
