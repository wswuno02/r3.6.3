#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "agtx_prio.h"

void *dummy_func(void *data)
{
	const char *th_name = (const char *)data;
	printf("Thread %s\n", th_name);
	printThreadSchedAttr();
	printf("\tUpdate thread scheduling policy...\n");
	if (!setThreadSchedAttr(th_name))
		printThreadSchedAttr();
	else
		printf("Failed to set thread %s!\n", th_name);
	/* Main body of the function */
	return NULL;
}

void usage(const char *app)
{
	printf("Usage:\t%s [thread_name]\n", app);
}

int main(int argc, char *argv[])
{
	pthread_t tid;
	char *th_name;
	int ret, i;

	if (argc == 1) {
		usage(argv[0]);
		return 0;
	}

	for (i = 1; i < argc; ++i) {
		th_name = argv[i];
		ret = pthread_create(&tid, NULL, dummy_func, th_name);
		if (ret) {
			printf("Failed to create thread %s\n", th_name);
			return -1;
		}
		pthread_join(tid, NULL);
	}

	return 0;
}
