/*
 * AUGENTIX INC. - PROPRIETARY
 *
 * agtx_prio.c - Priority getter/setter
 * Copyright (C) 2019 im14, Augentix Inc. <MAIL@augentix.com>
 *
 * NOTICE: The information contained herein is the property of Augentix Inc.
 * Copying and distributing of this file, via any medium,
 * must be licensed by Augentix Inc.
 *
 */

#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/resource.h>
#include <sys/syscall.h>
#define gettid() syscall(__NR_gettid)

#include "agtx_prio.h"

#define PRIO_FILE "/etc/prio.conf"
#define COLUMN 3 /* Column count of the prio.conf */

#define BUFFER_LEN 128

#define SCHED_PLCY_NUM 3
static const char *sched[SCHED_PLCY_NUM] = { "SCHED_OTHER", "SCHED_FIFO", "SCHED_RR" };

int getThreadSchedAttr(const char *th_name, int *policy, int *np)
{
	char line[BUFFER_LEN];
	char *buf[COLUMN];
	char *delim = "\t \n";
	char *tmp = NULL;
	int i;
	int line_num = 1;

	FILE *fp = fopen(PRIO_FILE, "r");
	if (fp == NULL) {
		printf("Error: Failed to open %s\n", PRIO_FILE);
		return -1;
	}

	while (fgets(line, sizeof(line), fp)) {
		tmp = strtok(line, delim);

		/* Skip comments or empty lines */
		if (tmp == NULL || tmp[0] == '#') {
			++line_num;
			continue;
		}

		/* Matched condition */
		if (strncmp(th_name, tmp, BUFFER_LEN) == 0) {
			for (i = 0; i < COLUMN; ++i) {
				if (tmp == NULL) {
					printf("Line %d, file %s: Mismatched format!\n", line_num, PRIO_FILE);
					return -1;
				}
				buf[i] = tmp;
				tmp = strtok(NULL, delim);
			}

			/* Get policy */
			for (i = 0; i < SCHED_PLCY_NUM; ++i) {
				if (strcmp(buf[1], sched[i]) == 0) {
					*policy = i;
					break;
				}
			}
			if (i >= SCHED_PLCY_NUM) {
				printf("Policy %s not supported!\n", buf[1]);
				return -1;
			}

			/* Get niceness/priority */
			*np = strtol(buf[2], NULL, 10);
			fclose(fp);
			return 0;
		}
		++line_num;
	}

	fclose(fp);
	return 1;
}

int setThreadSchedAttr(const char *th_name)
{
	int ret, pl, np;
	struct sched_param tparam;
	pthread_t tid = pthread_self();
	id_t __tid = gettid();

	/* Get current sched_param */
	pthread_getschedparam(tid, &pl, &tparam);

	/* libprio API: get scheduling policy and niceness/priority */
	ret = getThreadSchedAttr(th_name, &pl, &np);
	if (ret == 1) {
		printf("Thread priority not specified!\n");
		return -1;
	}

	if (pl == SCHED_RR || pl == SCHED_FIFO) {
		tparam.sched_priority = np;
		ret = pthread_setschedparam(tid, pl, &tparam);
		if (ret) {
			printf("Failed to set scheduling parameters\n");
			return -1;
		}
	} else if (pl == SCHED_OTHER) {
		ret = pthread_setschedparam(tid, pl, &tparam);
		if (ret) {
			printf("Failed to set scheduling policy\n");
			return -1;
		}
		ret = setpriority(PRIO_PROCESS, __tid, np);
		if (ret) {
			printf("Failed to set thread niceness\n");
			return -1;
		}
	}
	return 0;
}

void printThreadSchedAttr(void)
{
	struct sched_param tparam;
	int pl, np, ret;
	pthread_t tid = pthread_self();
	id_t __tid = gettid();

	ret = pthread_getschedparam(tid, &pl, &tparam);
	if (ret == 1) {
		printf("Thread not found!\n");
		return;
	}

	if (pl == SCHED_RR || pl == SCHED_FIFO) {
		np = tparam.sched_priority;
		printf("\t(Policy, Priority) = (%s, %d)\n", sched[pl], np);
	} else if (pl == SCHED_OTHER) {
		np = getpriority(PRIO_PROCESS, __tid);
		printf("\t(Policy, Niceness) = (%s, %d)\n", sched[pl], np);
	}
}
