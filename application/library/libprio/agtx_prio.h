#ifndef __AGTX_PRIO_H
#define __AGTX_PRIO_H

/*
 * AUGENTIX INC. - PROPRIETARY AND CONFIDENTIAL
 *
 * agtx_prio.h - Schedule priority attribute reader
 * Copyright (C) 2019 ShihChieh Lin, Augentix Inc. <shihchieh.lin@augentix.com>
 *
 * NOTICE: The information contained herein is the property of Augentix Inc.
 * Unauthorized copying and distributing of this file, via any medium,
 * is strictly prohibited.
 *
 */

#ifdef __cplusplus
extern "C" {
#endif

/*
 * getSchedAttr: Read thread priority attrbutes from config file
 *
 * Returns policy index in @policy, and niceness/priority in @np.
 * For threads with SCHED_OTHER, niceness is present.
 * For threads with SCHED_FIFO/SCHED_RR, priority value is present.
 * niceness: Scale goes to [-20:19], with 19 being the lowest priority
 * priority: Scale goes to [1, 99], with 99 being the highest priority
 *
 * @th_name: Thread name
 * @policy: Scheduling policy (SCHED_OTHER, SCHED_FIFO, SCHED_RR)
 * @np: Niceness or priority level
 */
int getThreadSchedAttr(const char *th_name, int *policy, int *np);

/*
 * setThreadSchedAttr: Set thread scheduling policy and privacy/niceness
 *
 * @th_name: Token (name) of the thread as in prio.conf file.
 * Returns 0 on success, -1 otherwise
 */
int setThreadSchedAttr(const char *th_name);

/*
 * printThreadSchedAttr: Print thread scheduling policy and privacy/niceness
 */
void printThreadSchedAttr(void);

#ifdef __cplusplus
}
#endif

#endif /* __AGTX_PRIO_H */
