/******************************************************************************
*
* opyright (c) Augentix Inc. - All Rights Reserved
*
* Unauthorized copying of this file, via any medium is strictly prohibited.
*
* Proprietary and confidential.
*
******************************************************************************/


#ifndef AGTX_COLOR_CONF_H_
#define AGTX_COLOR_CONF_H_


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#include "agtx_types.h"
#include "agtx_common.h"


typedef enum {
	AGTX_COLOR_MODE_DAY,
	AGTX_COLOR_MODE_NIGHT
} AGTX_COLOR_MODE_E;


typedef struct {
	AGTX_COLOR_MODE_E color_mode;
} AGTX_COLOR_CONF_S;


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* !AGTX_COLOR_CONF_H_ */
