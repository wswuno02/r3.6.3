/******************************************************************************
*
* opyright (c) Augentix Inc. - All Rights Reserved
*
* Unauthorized copying of this file, via any medium is strictly prohibited.
*
* Proprietary and confidential.
*
******************************************************************************/


#ifndef AGTX_DIP_DCC_CONF_H_
#define AGTX_DIP_DCC_CONF_H_


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#include "agtx_types.h"
#include "agtx_common.h"


#define MAX_AGTX_DIP_DCC_ATTR_S_GAIN_SIZE 4
#define MAX_AGTX_DIP_DCC_ATTR_S_OFFSET_SIZE 4
#define MAX_AGTX_DIP_DCC_CONF_S_DCC_SIZE 2

typedef struct {
	AGTX_INT32 gain[MAX_AGTX_DIP_DCC_ATTR_S_GAIN_SIZE];
	AGTX_INT32 offset[MAX_AGTX_DIP_DCC_ATTR_S_OFFSET_SIZE];
} AGTX_DIP_DCC_ATTR_S;

typedef struct {
	AGTX_DIP_DCC_ATTR_S dcc[MAX_AGTX_DIP_DCC_CONF_S_DCC_SIZE];
	AGTX_INT32 video_dev_idx;
} AGTX_DIP_DCC_CONF_S;


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* !AGTX_DIP_DCC_CONF_H_ */
