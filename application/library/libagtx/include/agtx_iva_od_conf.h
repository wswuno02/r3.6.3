#ifndef AGTX_IVA_OD_CONF_H_
#define AGTX_IVA_OD_CONF_H_

#include "agtx_types.h"
struct json_object;


typedef struct {
	AGTX_INT32 en_crop_outside_obj;
	AGTX_INT32 en_shake_det;
	AGTX_INT32 en_stop_det;
	AGTX_INT32 enabled;
	AGTX_INT32 od_qual;
	AGTX_INT32 od_sen;
	AGTX_INT32 od_size_th;
	AGTX_INT32 od_track_refine;
	AGTX_INT32 video_chn_idx;
} AGTX_IVA_OD_CONF_S;


#endif /* AGTX_IVA_OD_CONF_H_ */
