#include "app_dip_api.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>

#include "mpi_dip_alg.h"

#define CLAMP_8B(x) ((x) > 255 ? 255 : ((x) < 0 ? 0 : x))
#define PREF_TH (50)
#define MAX_INPUT_PATH 2

INT32 APP_DIP_setCal(MPI_DEV dev_idx, const AGTX_DIP_CAL_CONF_S *cal_cfg)
{
	INT32 ret = 0;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
	MPI_CAL_ATTR_S cal_attr[MAX_INPUT_PATH] = { { 0 } };

	for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
		path_idx.path = sns_path;
		ret = MPI_getCalAttr(path_idx, &cal_attr[sns_path]);

		cal_attr[sns_path].cal_en = (UINT8)cal_cfg->cal[sns_path].cal_en;
		cal_attr[sns_path].dbc_en = (UINT8)cal_cfg->cal[sns_path].dbc_en;
		cal_attr[sns_path].dcc_en = (UINT8)cal_cfg->cal[sns_path].dcc_en;
		cal_attr[sns_path].lsc_en = (UINT8)cal_cfg->cal[sns_path].lsc_en;

		/* set to mpi */
		path_idx.path = sns_path;
		ret = MPI_setCalAttr(path_idx, &cal_attr[sns_path]);
#if 0
		if (ret)
			return ret;
#endif
	}

	return MPI_SUCCESS;
}

INT32 APP_DIP_getCal(MPI_DEV dev_idx, AGTX_DIP_CAL_CONF_S *cal_cfg)
{
	INT32 ret = 0;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
	MPI_CAL_ATTR_S cal_attr[MAX_INPUT_PATH] = { { 0 } };

	for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
		path_idx.path = sns_path;
		ret = MPI_getCalAttr(path_idx, &cal_attr[sns_path]);
#if 0
		if (ret)
			return ret;
#endif

		/* assign to agtx interface */
		cal_cfg->cal[sns_path].cal_en = cal_attr[sns_path].cal_en;
		cal_cfg->cal[sns_path].dbc_en = cal_attr[sns_path].dbc_en;
		cal_cfg->cal[sns_path].dcc_en = cal_attr[sns_path].dcc_en;
		cal_cfg->cal[sns_path].lsc_en = cal_attr[sns_path].lsc_en;
	}
	cal_cfg->video_dev_idx = 0;

	return MPI_SUCCESS;
}

INT32 APP_DIP_setDbc(MPI_DEV dev_idx, const AGTX_DIP_DBC_CONF_S *dbc_cfg)
{
	INT32 ret = 0;
	MPI_DBC_ATTR_S dbc_attr[MAX_INPUT_PATH] = { { 0 } };
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
	INT32 dbc_chn = 0;

	for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
		path_idx.path = sns_path;
		ret = MPI_getDbcAttr(path_idx, &dbc_attr[sns_path]);

		dbc_attr[sns_path].mode = dbc_cfg->dbc[sns_path].mode;
		dbc_attr[sns_path].dbc_level = (UINT16)dbc_cfg->dbc[sns_path].dbc_level;
		dbc_attr[sns_path].type = dbc_cfg->dbc[sns_path].type;

		for (dbc_chn = 0; dbc_chn < MPI_DBC_CHN_NUM; dbc_chn++) {
			dbc_attr[sns_path].dbc_manual.manual.black_level[dbc_chn] =
			        (UINT16)dbc_cfg->dbc[sns_path].manual_black_level[dbc_chn];
			dbc_attr[sns_path].dbc_auto.auto_table[0].black_level[dbc_chn] =
			        (UINT16)dbc_cfg->dbc[sns_path].auto_black_level_0[dbc_chn];
			dbc_attr[sns_path].dbc_auto.auto_table[1].black_level[dbc_chn] =
			        (UINT16)dbc_cfg->dbc[sns_path].auto_black_level_1[dbc_chn];
			dbc_attr[sns_path].dbc_auto.auto_table[2].black_level[dbc_chn] =
			        (UINT16)dbc_cfg->dbc[sns_path].auto_black_level_2[dbc_chn];
			dbc_attr[sns_path].dbc_auto.auto_table[3].black_level[dbc_chn] =
			        (UINT16)dbc_cfg->dbc[sns_path].auto_black_level_3[dbc_chn];
			dbc_attr[sns_path].dbc_auto.auto_table[4].black_level[dbc_chn] =
			        (UINT16)dbc_cfg->dbc[sns_path].auto_black_level_4[dbc_chn];
			dbc_attr[sns_path].dbc_auto.auto_table[5].black_level[dbc_chn] =
			        (UINT16)dbc_cfg->dbc[sns_path].auto_black_level_5[dbc_chn];
			dbc_attr[sns_path].dbc_auto.auto_table[6].black_level[dbc_chn] =
			        (UINT16)dbc_cfg->dbc[sns_path].auto_black_level_6[dbc_chn];
			dbc_attr[sns_path].dbc_auto.auto_table[7].black_level[dbc_chn] =
			        (UINT16)dbc_cfg->dbc[sns_path].auto_black_level_7[dbc_chn];
			dbc_attr[sns_path].dbc_auto.auto_table[8].black_level[dbc_chn] =
			        (UINT16)dbc_cfg->dbc[sns_path].auto_black_level_8[dbc_chn];
			dbc_attr[sns_path].dbc_auto.auto_table[9].black_level[dbc_chn] =
			        (UINT16)dbc_cfg->dbc[sns_path].auto_black_level_9[dbc_chn];
			dbc_attr[sns_path].dbc_auto.auto_table[10].black_level[dbc_chn] =
			        (UINT16)dbc_cfg->dbc[sns_path].auto_black_level_10[dbc_chn];
		}

		path_idx.path = sns_path;
		ret = MPI_setDbcAttr(path_idx, &dbc_attr[sns_path]);
#if 0
		if (ret)
			return ret;
#endif
	}

	return MPI_SUCCESS;
}

INT32 APP_DIP_getDbc(MPI_DEV dev_idx, AGTX_DIP_DBC_CONF_S *dbc_cfg)
{
	INT32 ret = 0;
	MPI_DBC_ATTR_S dbc_attr[MAX_INPUT_PATH] = { { 0 } };
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
	INT32 dbc_chn = 0;

	for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
		path_idx.path = sns_path;
		ret = MPI_getDbcAttr(path_idx, &dbc_attr[sns_path]);

#if 0
		if (ret)
			return ret;
#endif

		dbc_cfg->dbc[sns_path].mode = dbc_attr[sns_path].mode;
		dbc_cfg->dbc[sns_path].dbc_level = dbc_attr[sns_path].dbc_level;
		dbc_cfg->dbc[sns_path].type = dbc_attr[sns_path].type;

		for (dbc_chn = 0; dbc_chn < MPI_DBC_CHN_NUM; dbc_chn++) {
			dbc_cfg->dbc[sns_path].manual_black_level[dbc_chn] =
			        dbc_attr[sns_path].dbc_manual.manual.black_level[dbc_chn];
			dbc_cfg->dbc[sns_path].auto_black_level_0[dbc_chn] =
			        dbc_attr[sns_path].dbc_auto.auto_table[0].black_level[dbc_chn];
			dbc_cfg->dbc[sns_path].auto_black_level_1[dbc_chn] =
			        dbc_attr[sns_path].dbc_auto.auto_table[1].black_level[dbc_chn];
			dbc_cfg->dbc[sns_path].auto_black_level_2[dbc_chn] =
			        dbc_attr[sns_path].dbc_auto.auto_table[2].black_level[dbc_chn];
			dbc_cfg->dbc[sns_path].auto_black_level_3[dbc_chn] =
			        dbc_attr[sns_path].dbc_auto.auto_table[3].black_level[dbc_chn];
			dbc_cfg->dbc[sns_path].auto_black_level_4[dbc_chn] =
			        dbc_attr[sns_path].dbc_auto.auto_table[4].black_level[dbc_chn];
			dbc_cfg->dbc[sns_path].auto_black_level_5[dbc_chn] =
			        dbc_attr[sns_path].dbc_auto.auto_table[5].black_level[dbc_chn];
			dbc_cfg->dbc[sns_path].auto_black_level_6[dbc_chn] =
			        dbc_attr[sns_path].dbc_auto.auto_table[6].black_level[dbc_chn];
			dbc_cfg->dbc[sns_path].auto_black_level_7[dbc_chn] =
			        dbc_attr[sns_path].dbc_auto.auto_table[7].black_level[dbc_chn];
			dbc_cfg->dbc[sns_path].auto_black_level_8[dbc_chn] =
			        dbc_attr[sns_path].dbc_auto.auto_table[8].black_level[dbc_chn];
			dbc_cfg->dbc[sns_path].auto_black_level_9[dbc_chn] =
			        dbc_attr[sns_path].dbc_auto.auto_table[9].black_level[dbc_chn];
			dbc_cfg->dbc[sns_path].auto_black_level_10[dbc_chn] =
			        dbc_attr[sns_path].dbc_auto.auto_table[10].black_level[dbc_chn];
		}
	}

	dbc_cfg->video_dev_idx = 0;

	return MPI_SUCCESS;
}

INT32 APP_DIP_setDcc(MPI_DEV dev_idx, const AGTX_DIP_DCC_CONF_S *dcc_cfg)
{
	INT32 ret = 0;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
	MPI_DCC_ATTR_S dcc_attr[MAX_INPUT_PATH];

	for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
		path_idx.path = sns_path;
		ret = MPI_getDccAttr(path_idx, &dcc_attr[sns_path]);

		for (INT32 num = 0; num < MPI_DCC_CHN_NUM; num++) {
			dcc_attr[sns_path].gain[num] = (UINT16)dcc_cfg->dcc[sns_path].gain[num];
			dcc_attr[sns_path].offset_2s[num] = (UINT16)dcc_cfg->dcc[sns_path].offset[num];
		}

		ret = MPI_setDccAttr(path_idx, &dcc_attr[sns_path]);
#if 0
		if (ret)
			return ret;
#endif
	}

	return MPI_SUCCESS;
}

INT32 APP_DIP_getDcc(MPI_DEV dev_idx, AGTX_DIP_DCC_CONF_S *dcc_cfg)
{
	INT32 ret = 0;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
	MPI_DCC_ATTR_S dcc_attr[MAX_INPUT_PATH] = { { { 0 } } };

	for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
		path_idx.path = sns_path;
		ret = MPI_getDccAttr(path_idx, &dcc_attr[sns_path]);
#if 0
		if (ret)
			return ret;
#endif

		for (INT32 num = 0; num < MPI_DCC_CHN_NUM; num++) {
			dcc_cfg->dcc[sns_path].gain[num] = dcc_attr[sns_path].gain[num];
			dcc_cfg->dcc[sns_path].offset[num] = dcc_attr[sns_path].offset_2s[num];
		}
	}

	dcc_cfg->video_dev_idx = 0;

	return MPI_SUCCESS;
}

INT32 APP_DIP_setLsc(MPI_DEV dev_idx, const AGTX_DIP_LSC_CONF_S *lsc_cfg)
{
	INT32 ret = 0;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
	MPI_LSC_ATTR_S lsc_attr[MAX_INPUT_PATH] = { { 0 } };

	for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
		path_idx.path = sns_path;
		ret = MPI_getLscAttr(path_idx, &lsc_attr[sns_path]);

		lsc_attr[sns_path].origin = (INT32)lsc_cfg->lsc[sns_path].origin;
		lsc_attr[sns_path].x_trend_2s = (UINT32)lsc_cfg->lsc[sns_path].x_trend;
		lsc_attr[sns_path].y_trend_2s = (UINT32)lsc_cfg->lsc[sns_path].y_trend;
		lsc_attr[sns_path].x_curvature = (UINT32)lsc_cfg->lsc[sns_path].x_curvature;
		lsc_attr[sns_path].y_curvature = (UINT32)lsc_cfg->lsc[sns_path].y_curvature;
		lsc_attr[sns_path].tilt_2s = (INT32)lsc_cfg->lsc[sns_path].tilt;

		ret = MPI_setLscAttr(path_idx, &lsc_attr[sns_path]);
#if 0
		if (ret)
			return ret;
#endif
	}

	return MPI_SUCCESS;
}

INT32 APP_DIP_getLsc(MPI_DEV dev_idx, AGTX_DIP_LSC_CONF_S *lsc_cfg)
{
	INT32 ret = 0;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
	MPI_LSC_ATTR_S lsc_attr[MAX_INPUT_PATH] = { { 0 } };

	for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
		path_idx.path = sns_path;
		ret = MPI_getLscAttr(path_idx, &lsc_attr[sns_path]);
#if 0
		if (ret)
			return ret;
#endif

		lsc_cfg->lsc[sns_path].origin = lsc_attr[sns_path].origin;
		lsc_cfg->lsc[sns_path].x_trend = lsc_attr[sns_path].x_trend_2s;
		lsc_cfg->lsc[sns_path].y_trend = lsc_attr[sns_path].y_trend_2s;
		lsc_cfg->lsc[sns_path].x_curvature = lsc_attr[sns_path].x_curvature;
		lsc_cfg->lsc[sns_path].y_curvature = lsc_attr[sns_path].y_curvature;
		lsc_cfg->lsc[sns_path].tilt = lsc_attr[sns_path].tilt_2s;
	}

	lsc_cfg->video_dev_idx = 0;

	return MPI_SUCCESS;
}

INT32 APP_DIP_setRoi(MPI_DEV dev_idx, const AGTX_DIP_ROI_CONF_S *roi_cfg)
{
	INT32 ret = 0;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
	MPI_ROI_ATTR_S roi_attr;
	INT32 sns_path = 0;

	for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; ++sns_path) {
		path_idx.path = sns_path;
		ret = MPI_getRoiAttr(path_idx, &roi_attr);

		roi_attr.luma_roi.sx = (INT16)roi_cfg->roi[sns_path].luma_roi_sx;
		roi_attr.luma_roi.sy = (INT16)roi_cfg->roi[sns_path].luma_roi_sy;
		roi_attr.luma_roi.ex = (INT16)roi_cfg->roi[sns_path].luma_roi_ex;
		roi_attr.luma_roi.ey = (INT16)roi_cfg->roi[sns_path].luma_roi_ey;
		roi_attr.awb_roi.sx = (INT16)roi_cfg->roi[sns_path].awb_roi_sx;
		roi_attr.awb_roi.sy = (INT16)roi_cfg->roi[sns_path].awb_roi_sy;
		roi_attr.awb_roi.ex = (INT16)roi_cfg->roi[sns_path].awb_roi_ex;
		roi_attr.awb_roi.ey = (INT16)roi_cfg->roi[sns_path].awb_roi_ey;
		roi_attr.zone_lum_avg_roi.sx = (INT16)roi_cfg->roi[sns_path].zone_lum_avg_roi_sx;
		roi_attr.zone_lum_avg_roi.sy = (INT16)roi_cfg->roi[sns_path].zone_lum_avg_roi_sy;
		roi_attr.zone_lum_avg_roi.ex = (INT16)roi_cfg->roi[sns_path].zone_lum_avg_roi_ex;
		roi_attr.zone_lum_avg_roi.ey = (INT16)roi_cfg->roi[sns_path].zone_lum_avg_roi_ey;

		path_idx.path = sns_path;
		ret = MPI_setRoiAttr(path_idx, &roi_attr);
#if 0
		if (ret)
			return ret;
#endif
	}

	return MPI_SUCCESS;
}

INT32 APP_DIP_getRoi(MPI_DEV dev_idx, AGTX_DIP_ROI_CONF_S *roi_cfg)
{
	INT32 ret = 0;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
	MPI_ROI_ATTR_S roi_attr[MAX_INPUT_PATH] = { { { 0 } } };

	for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; ++sns_path) {
		path_idx.path = sns_path;
		ret = MPI_getRoiAttr(path_idx, &roi_attr[sns_path]);

#if 0
		if (ret)
			return ret;
#endif
		roi_cfg->roi[sns_path].luma_roi_sx = roi_attr[sns_path].luma_roi.sx;
		roi_cfg->roi[sns_path].luma_roi_sy = roi_attr[sns_path].luma_roi.sy;
		roi_cfg->roi[sns_path].luma_roi_ex = roi_attr[sns_path].luma_roi.ex;
		roi_cfg->roi[sns_path].luma_roi_ey = roi_attr[sns_path].luma_roi.ey;
		roi_cfg->roi[sns_path].awb_roi_sx = roi_attr[sns_path].awb_roi.sx;
		roi_cfg->roi[sns_path].awb_roi_sy = roi_attr[sns_path].awb_roi.sy;
		roi_cfg->roi[sns_path].awb_roi_ex = roi_attr[sns_path].awb_roi.ex;
		roi_cfg->roi[sns_path].awb_roi_ey = roi_attr[sns_path].awb_roi.ey;
		roi_cfg->roi[sns_path].zone_lum_avg_roi_sx = roi_attr[sns_path].zone_lum_avg_roi.sx;
		roi_cfg->roi[sns_path].zone_lum_avg_roi_sy = roi_attr[sns_path].zone_lum_avg_roi.sy;
		roi_cfg->roi[sns_path].zone_lum_avg_roi_ex = roi_attr[sns_path].zone_lum_avg_roi.ex;
		roi_cfg->roi[sns_path].zone_lum_avg_roi_ey = roi_attr[sns_path].zone_lum_avg_roi.ey;
	}

	roi_cfg->video_dev_idx = 0;

	return MPI_SUCCESS;
}

INT32 APP_DIP_setCtrl(MPI_DEV dev_idx, const AGTX_DIP_CTRL_CONF_S *ctrl_cfg)
{
	INT32 ret = 0;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
	MPI_DIP_ATTR_S ctrl_attr;

	ret = MPI_getDipAttr(path_idx, &ctrl_attr);

	ctrl_attr.is_dip_en = (UINT8)ctrl_cfg->is_dip_en;
	ctrl_attr.is_ae_en = (UINT8)ctrl_cfg->is_ae_en;
	ctrl_attr.is_iso_en = (UINT8)ctrl_cfg->is_iso_en;
	ctrl_attr.is_awb_en = (UINT8)ctrl_cfg->is_awb_en;
	ctrl_attr.is_nr_en = (UINT8)ctrl_cfg->is_nr_en;
	ctrl_attr.is_te_en = (UINT8)ctrl_cfg->is_te_en;
	ctrl_attr.is_pta_en = (UINT8)ctrl_cfg->is_pta_en;
	ctrl_attr.is_csm_en = (UINT8)ctrl_cfg->is_csm_en;
	ctrl_attr.is_shp_en = (UINT8)ctrl_cfg->is_shp_en;
	ctrl_attr.is_gamma_en = (UINT8)ctrl_cfg->is_gamma_en;
	ctrl_attr.is_dpc_en = (UINT8)ctrl_cfg->is_dpc_en;
	ctrl_attr.is_dms_en = (UINT8)ctrl_cfg->is_dms_en;
	ctrl_attr.is_me_en = (UINT8)ctrl_cfg->is_me_en;
	ctrl_attr.is_enh_en = (UINT8)ctrl_cfg->is_enh_en;
	ctrl_attr.is_coring_en = (UINT8)ctrl_cfg->is_coring_en;
	ctrl_attr.is_fcs_en = (UINT8)ctrl_cfg->is_fcs_en;

	ret = MPI_setDipAttr(path_idx, &ctrl_attr);
	if (ret)
		return ret;

	return MPI_SUCCESS;
}

INT32 APP_DIP_getCtrl(MPI_DEV dev_idx, AGTX_DIP_CTRL_CONF_S *ctrl_cfg)
{
	INT32 ret = 0;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
	MPI_DIP_ATTR_S ctrl_attr;

	ret = MPI_getDipAttr(path_idx, &ctrl_attr);
	if (ret)
		return ret;

	ctrl_cfg->is_dip_en = ctrl_attr.is_dip_en;
	ctrl_cfg->is_ae_en = ctrl_attr.is_ae_en;
	ctrl_cfg->is_iso_en = ctrl_attr.is_iso_en;
	ctrl_cfg->is_awb_en = ctrl_attr.is_awb_en;
	ctrl_cfg->is_nr_en = ctrl_attr.is_nr_en;
	ctrl_cfg->is_te_en = ctrl_attr.is_te_en;
	ctrl_cfg->is_pta_en = ctrl_attr.is_pta_en;
	ctrl_cfg->is_csm_en = ctrl_attr.is_csm_en;
	ctrl_cfg->is_shp_en = ctrl_attr.is_shp_en;
	ctrl_cfg->is_gamma_en = ctrl_attr.is_gamma_en;
	ctrl_cfg->is_dpc_en = ctrl_attr.is_dpc_en;
	ctrl_cfg->is_dms_en = ctrl_attr.is_dms_en;
	ctrl_cfg->is_me_en = ctrl_attr.is_me_en;
	ctrl_cfg->is_enh_en = ctrl_attr.is_enh_en;
	ctrl_cfg->is_coring_en = ctrl_attr.is_coring_en;
	ctrl_cfg->is_fcs_en = ctrl_attr.is_fcs_en;

	ctrl_cfg->video_dev_idx = 0;

	return MPI_SUCCESS;
}

INT32 APP_DIP_setAe(MPI_DEV dev_idx, const AGTX_DIP_AE_CONF_S *ae_cfg)
{
	INT32 ret = 0;
	MPI_AE_ATTR_S ae_attr;

	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_getAeAttr(path_idx, &ae_attr);

	/* exp strategy*/
	ae_attr.strategy.mode = ae_cfg->exp_strategy;

	/* fps mode*/
	ae_attr.fps_mode = ae_cfg->fps_mode;

	ae_attr.sys_gain_range.max = (UINT32)ae_cfg->max_sys_gain;
	ae_attr.sys_gain_range.min = (UINT32)ae_cfg->min_sys_gain;
	ae_attr.sensor_gain_range.max = (UINT32)ae_cfg->max_sensor_gain;
	ae_attr.sensor_gain_range.min = (UINT32)ae_cfg->min_sensor_gain;
	ae_attr.isp_gain_range.max = (UINT32)ae_cfg->max_isp_gain;
	ae_attr.isp_gain_range.min = (UINT32)ae_cfg->min_isp_gain;
	ae_attr.frame_rate = (FLOAT)ae_cfg->frame_rate;
	ae_attr.slow_frame_rate = (FLOAT)ae_cfg->slow_frame_rate;
	ae_attr.speed = (UINT8)ae_cfg->speed;
	ae_attr.black_speed_bias = (UINT8)ae_cfg->black_speed_bias;
	ae_attr.interval = (UINT8)ae_cfg->interval;
	ae_attr.brightness = (UINT16)ae_cfg->brightness;
	ae_attr.tolerance = (UINT16)ae_cfg->tolerance;
	ae_attr.gain_thr_up = (UINT16)ae_cfg->gain_thr_up;
	ae_attr.gain_thr_down = (UINT16)ae_cfg->gain_thr_down;

	ae_attr.strategy.strength = (INT32)ae_cfg->exp_strength;

	ae_attr.roi.awb_weight = (UINT8)ae_cfg->roi_awb_weight;
	ae_attr.roi.luma_weight = (UINT8)ae_cfg->roi_luma_weight;
	ae_attr.roi.zone_lum_avg_weight = (UINT8)ae_cfg->roi_zone_lum_avg_weight;

	ae_attr.delay.black_delay_frame = (UINT16)ae_cfg->black_delay_frame;
	ae_attr.delay.white_delay_frame = (UINT16)ae_cfg->white_delay_frame;

	ae_attr.anti_flicker.enable = (BOOL)ae_cfg->anti_flicker.enable;
	ae_attr.anti_flicker.frequency = (UINT8)ae_cfg->anti_flicker.frequency;
	ae_attr.anti_flicker.luma_delta = (UINT16)ae_cfg->anti_flicker.luma_delta;

	ae_attr.manual.is_valid = (BOOL)ae_cfg->manual.enabled;
	ae_attr.manual.enable.val = (UINT32)ae_cfg->manual.flag;
	ae_attr.manual.exp_value = (UINT32)ae_cfg->manual.exp_value;
	ae_attr.manual.inttime = (UINT32)ae_cfg->manual.inttime;
	ae_attr.manual.sensor_gain = (UINT32)ae_cfg->manual.sensor_gain;
	ae_attr.manual.isp_gain = (UINT32)ae_cfg->manual.isp_gain;
	ae_attr.manual.sys_gain = (UINT32)ae_cfg->manual.sys_gain;

	for (INT32 i = 0; i < MPI_AE_ZONE_ROW; ++i) {
		for (INT32 j = 0; j < MPI_AE_ZONE_COLUMN; ++j) {
			ae_attr.zone_weight.manual_table[i][j] =
			        ae_cfg->zone_weight.manual_table[i * MPI_AE_ZONE_COLUMN + j];
		}
	}

	ae_attr.zone_weight.mode = ae_cfg->zone_weight.mode;

	ae_attr.inttime_range.max = (UINT32)ae_cfg->max_inttime;
	ae_attr.inttime_range.min = (UINT32)ae_cfg->min_inttime;

	ret = MPI_setAeAttr(path_idx, &ae_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 APP_DIP_getAe(MPI_DEV dev_idx, AGTX_DIP_AE_CONF_S *ae_cfg)
{
	INT32 ret = 0;
	MPI_AE_ATTR_S ae_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_getAeAttr(path_idx, &ae_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	/* exp strategy*/
	ae_cfg->exp_strategy = ae_attr.strategy.mode;

	/* fps mode*/
	if (ae_attr.fps_mode == AE_FPS_FIXED) {
		ae_cfg->fps_mode = AGTX_FPS_MO_FIXED;
	} else if (ae_attr.fps_mode == AE_FPS_DROP) {
		ae_cfg->fps_mode = AGTX_FPS_MO_VARIABLE;
	} else {
		printf("AGTX_DIP_AE_CONF_S fps_mode is out of range\n");
		return MPI_FAILURE;
	}

	ae_cfg->max_sys_gain = ae_attr.sys_gain_range.max;
	ae_cfg->min_sys_gain = ae_attr.sys_gain_range.min;
	ae_cfg->max_sensor_gain = ae_attr.sensor_gain_range.max;
	ae_cfg->min_sensor_gain = ae_attr.sensor_gain_range.min;
	ae_cfg->max_isp_gain = ae_attr.isp_gain_range.max;
	ae_cfg->min_isp_gain = ae_attr.isp_gain_range.min;
	ae_cfg->frame_rate = ae_attr.frame_rate;
	ae_cfg->slow_frame_rate = ae_attr.slow_frame_rate;
	ae_cfg->speed = ae_attr.speed;
	ae_cfg->black_speed_bias = ae_attr.black_speed_bias;
	ae_cfg->interval = ae_attr.interval;
	ae_cfg->brightness = ae_attr.brightness;
	ae_cfg->tolerance = ae_attr.tolerance;
	ae_cfg->gain_thr_up = ae_attr.gain_thr_up;
	ae_cfg->gain_thr_down = ae_attr.gain_thr_down;

	ae_cfg->exp_strength = ae_attr.strategy.strength;

	ae_cfg->roi_awb_weight = ae_attr.roi.awb_weight;
	ae_cfg->roi_luma_weight = ae_attr.roi.luma_weight;
	ae_cfg->roi_zone_lum_avg_weight = ae_attr.roi.zone_lum_avg_weight;

	ae_cfg->black_delay_frame = ae_attr.delay.black_delay_frame;
	ae_cfg->white_delay_frame = ae_attr.delay.white_delay_frame;

	ae_cfg->anti_flicker.enable = ae_attr.anti_flicker.enable;
	ae_cfg->anti_flicker.frequency = ae_attr.anti_flicker.frequency;
	ae_cfg->anti_flicker.luma_delta = ae_attr.anti_flicker.luma_delta;

	ae_cfg->manual.enabled = ae_attr.manual.is_valid;
	ae_cfg->manual.flag = ae_attr.manual.enable.val;
	ae_cfg->manual.exp_value = ae_attr.manual.exp_value;
	ae_cfg->manual.inttime = ae_attr.manual.inttime;
	ae_cfg->manual.sensor_gain = ae_attr.manual.sensor_gain;
	ae_cfg->manual.isp_gain = ae_attr.manual.isp_gain;
	ae_cfg->manual.sys_gain = ae_attr.manual.sys_gain;

	for (INT32 i = 0; i < MPI_AE_ZONE_ROW; ++i) {
		for (INT32 j = 0; j < MPI_AE_ZONE_COLUMN; ++j) {
			ae_cfg->zone_weight.manual_table[i * MPI_AE_ZONE_COLUMN + j] =
			        ae_attr.zone_weight.manual_table[i][j];
		}
	}

	ae_cfg->zone_weight.mode = ae_attr.zone_weight.mode;

	ae_cfg->max_inttime = ae_attr.inttime_range.max;
	ae_cfg->min_inttime = ae_attr.inttime_range.min;

	ae_cfg->video_dev_idx = 0;

	return MPI_SUCCESS;
}

INT32 APP_DIP_setIso(MPI_DEV dev_idx, const AGTX_DIP_ISO_CONF_S *iso_cfg)
{
	INT32 ret = 0;
	MPI_ISO_ATTR_S iso_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_getIsoAttr(path_idx, &iso_attr);

	iso_attr.mode = iso_cfg->mode;
	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		iso_attr.iso_auto.effective_iso[num] = (INT32)iso_cfg->auto_iso_table[num];
	}
	iso_attr.iso_manual.effective_iso = (INT32)iso_cfg->manual_iso;

	ret = MPI_setIsoAttr(path_idx, &iso_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 APP_DIP_getIso(MPI_DEV dev_idx, AGTX_DIP_ISO_CONF_S *iso_cfg)
{
	INT32 ret = 0;

	MPI_ISO_ATTR_S iso_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
	ret = MPI_getIsoAttr(path_idx, &iso_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	iso_cfg->mode = iso_attr.mode;
	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		iso_cfg->auto_iso_table[num] = iso_attr.iso_auto.effective_iso[num];
	}
	iso_cfg->manual_iso = iso_attr.iso_manual.effective_iso;

	iso_cfg->video_dev_idx = 0;

	return MPI_SUCCESS;
}

INT32 APP_DIP_setAwb(MPI_DEV dev_idx, const AGTX_DIP_AWB_CONF_S *awb_cfg)
{
	INT32 ret = 0;
	MPI_AWB_ATTR_S awb_attr;

	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
	ret = MPI_getAwbAttr(path_idx, &awb_attr);

	awb_attr.speed = (UINT8)awb_cfg->speed;
	awb_attr.wht_density = (UINT8)awb_cfg->wht_density;
	awb_attr.r_extra_gain = (UINT8)awb_cfg->r_extra_gain;
	awb_attr.b_extra_gain = (UINT8)awb_cfg->b_extra_gain;
	awb_attr.g_extra_gain = (UINT8)awb_cfg->g_extra_gain;
	awb_attr.wht_weight = (UINT8)awb_cfg->wht_weight;
	awb_attr.gwd_weight = (UINT8)awb_cfg->gwd_weight;
	awb_attr.color_tolerance = (UINT8)awb_cfg->color_tolerance;
	awb_attr.max_lum_gain = (UINT8)awb_cfg->max_lum_gain;
	awb_attr.low_k = (UINT16)awb_cfg->low_k;
	awb_attr.high_k = (UINT16)awb_cfg->high_k;
	awb_attr.over_exp_th = (UINT16)awb_cfg->over_exp_th;
	awb_attr.ccm_domain = awb_cfg->ccm_domain;
	awb_attr.k_table_valid_size = (UINT8)awb_cfg->k_table_valid_size;

	for (INT32 k_num = 0; k_num < MPI_K_TABLE_ENTRY_NUM; k_num++) {
		awb_attr.k_table[k_num].k = (UINT16)awb_cfg->k_table_list[k_num].k;

		for (INT32 num = 0; num < MPI_AWB_CHN_NUM; num++) {
			awb_attr.k_table[k_num].gain[num] = (UINT16)awb_cfg->k_table_list[k_num].gain[num];
			awb_attr.delta_table[k_num].gain[num] = (INT16)awb_cfg->delta_table_list[k_num].gain[num];
		}

		for (INT32 m_num = 0; m_num < (MPI_COLOR_CHN_NUM * MPI_COLOR_CHN_NUM); m_num++) {
			awb_attr.k_table[k_num].matrix[m_num] = (UINT16)awb_cfg->k_table_list[k_num].maxtrix[m_num];
		}

		awb_attr.bias_table[k_num].k = (UINT16)awb_cfg->k_table_bias_list[k_num].k;
		awb_attr.bias_table[k_num].color_tolerance_bias =
		        (UINT16)awb_cfg->k_table_bias_list[k_num].color_tolerance_bias;
		awb_attr.bias_table[k_num].wht_weight_bias = (UINT16)awb_cfg->k_table_bias_list[k_num].wht_weight_bias;
		awb_attr.bias_table[k_num].gwd_weight_bias = (UINT16)awb_cfg->k_table_bias_list[k_num].gwd_weight_bias;
		awb_attr.bias_table[k_num].r_extra_gain_bias =
		        (UINT16)awb_cfg->k_table_bias_list[k_num].r_extra_gain_bias;
		awb_attr.bias_table[k_num].g_extra_gain_bias =
		        (UINT16)awb_cfg->k_table_bias_list[k_num].g_extra_gain_bias;
		awb_attr.bias_table[k_num].b_extra_gain_bias =
		        (UINT16)awb_cfg->k_table_bias_list[k_num].b_extra_gain_bias;
	}


	ret = MPI_setAwbAttr(path_idx, &awb_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 APP_DIP_getAwb(MPI_DEV dev_idx, AGTX_DIP_AWB_CONF_S *awb_cfg)
{
	INT32 ret = 0;
	MPI_AWB_ATTR_S awb_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_getAwbAttr(path_idx, &awb_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	awb_cfg->speed = awb_attr.speed;
	awb_cfg->wht_density = awb_attr.wht_density;
	awb_cfg->r_extra_gain = awb_attr.r_extra_gain;
	awb_cfg->b_extra_gain = awb_attr.b_extra_gain;
	awb_cfg->g_extra_gain = awb_attr.g_extra_gain;
	awb_cfg->wht_weight = awb_attr.wht_weight;
	awb_cfg->gwd_weight = awb_attr.gwd_weight;
	awb_cfg->color_tolerance = awb_attr.color_tolerance;
	awb_cfg->max_lum_gain = awb_attr.max_lum_gain;
	awb_cfg->low_k = awb_attr.low_k;
	awb_cfg->high_k = awb_attr.high_k;
	awb_cfg->over_exp_th = awb_attr.over_exp_th;
	awb_cfg->ccm_domain = awb_attr.ccm_domain;
	awb_cfg->k_table_valid_size = awb_attr.k_table_valid_size;

	for (INT32 k_num = 0; k_num < MPI_K_TABLE_ENTRY_NUM; k_num++) {
		awb_cfg->k_table_list[k_num].k = awb_attr.k_table[k_num].k;

		for (INT32 num = 0; num < MPI_AWB_CHN_NUM; num++) {
			awb_cfg->k_table_list[k_num].gain[num] = awb_attr.k_table[k_num].gain[num];
			awb_cfg->delta_table_list[k_num].gain[num] = awb_attr.delta_table[k_num].gain[num];
		}

		for (INT32 m_num = 0; m_num < (MPI_COLOR_CHN_NUM * MPI_COLOR_CHN_NUM); m_num++) {
			awb_cfg->k_table_list[k_num].maxtrix[m_num] = awb_attr.k_table[k_num].matrix[m_num];
		}

		awb_cfg->k_table_bias_list[k_num].k = awb_attr.bias_table[k_num].k;
		awb_cfg->k_table_bias_list[k_num].color_tolerance_bias =
		        awb_attr.bias_table[k_num].color_tolerance_bias;
		awb_cfg->k_table_bias_list[k_num].wht_weight_bias = awb_attr.bias_table[k_num].wht_weight_bias;
		awb_cfg->k_table_bias_list[k_num].gwd_weight_bias = awb_attr.bias_table[k_num].gwd_weight_bias;
		awb_cfg->k_table_bias_list[k_num].r_extra_gain_bias = awb_attr.bias_table[k_num].r_extra_gain_bias;
		awb_cfg->k_table_bias_list[k_num].g_extra_gain_bias = awb_attr.bias_table[k_num].g_extra_gain_bias;
		awb_cfg->k_table_bias_list[k_num].b_extra_gain_bias = awb_attr.bias_table[k_num].b_extra_gain_bias;
	}

	awb_cfg->video_dev_idx = 0;

	return MPI_SUCCESS;
}

INT32 APP_DIP_setCsm(MPI_DEV dev_idx, const AGTX_DIP_CSM_CONF_S *csm_cfg)
{
	INT32 ret = 0;
	MPI_CSM_ATTR_S csm_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
	ret = MPI_getCsmAttr(path_idx, &csm_attr);

	csm_attr.mode = csm_cfg->mode;
	csm_attr.bw_en = (UINT8)csm_cfg->bw_en;
	csm_attr.hue_angle = (INT16)csm_cfg->hue;
	csm_attr.csm_manual.saturation = (UINT8)csm_cfg->manual_sat;

	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		csm_attr.csm_auto.saturation[num] = (UINT8)csm_cfg->auto_sat_table[num];
	}


	ret = MPI_setCsmAttr(path_idx, &csm_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 APP_DIP_getCsm(MPI_DEV dev_idx, AGTX_DIP_CSM_CONF_S *csm_cfg)
{
	INT32 ret = 0;
	MPI_CSM_ATTR_S csm_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_getCsmAttr(path_idx, &csm_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	csm_cfg->mode = csm_attr.mode;
	csm_cfg->bw_en = csm_attr.bw_en;
	csm_cfg->hue = csm_attr.hue_angle;
	csm_cfg->manual_sat = csm_attr.csm_manual.saturation;

	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		csm_cfg->auto_sat_table[num] = csm_attr.csm_auto.saturation[num];
	}

	csm_cfg->video_dev_idx = 0;

	return MPI_SUCCESS;
}

INT32 APP_DIP_setPta(MPI_DEV dev_idx, const AGTX_DIP_PTA_CONF_S *pta_cfg)
{
	INT32 ret = 0;
	MPI_PTA_ATTR_S pta_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_getPtaAttr(path_idx, &pta_attr);

	pta_attr.mode = pta_cfg->mode;
	pta_attr.brightness = (UINT8)pta_cfg->brightness;
	pta_attr.contrast = (UINT8)pta_cfg->contrast;
	pta_attr.break_point = (UINT8)pta_cfg->break_point;

	for (INT32 num = 0; num < MPI_PTA_CURVE_ENTRY_NUM; num++) {
		pta_attr.pta_manual.curve[num] = (UINT32)pta_cfg->curve[num];
	}

	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		pta_attr.pta_auto.tone[num] = (UINT32)pta_cfg->auto_tone_table[num];
	}

	ret = MPI_setPtaAttr(path_idx, &pta_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 APP_DIP_getPta(MPI_DEV dev_idx, AGTX_DIP_PTA_CONF_S *pta_cfg)
{
	INT32 ret = 0;
	MPI_PTA_ATTR_S pta_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
	ret = MPI_getPtaAttr(path_idx, &pta_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	pta_cfg->mode = pta_attr.mode;
	pta_cfg->brightness = pta_attr.brightness;
	pta_cfg->contrast = pta_attr.contrast;
	pta_cfg->break_point = pta_attr.break_point;

	for (INT32 num = 0; num < MPI_PTA_CURVE_ENTRY_NUM; num++) {
		pta_cfg->curve[num] = pta_attr.pta_manual.curve[num];
	}

	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		pta_cfg->auto_tone_table[num] = pta_attr.pta_auto.tone[num];
	}

	pta_cfg->video_dev_idx = 0;

	return MPI_SUCCESS;
}

INT32 APP_DIP_setShp(MPI_DEV dev_idx, const AGTX_DIP_SHP_CONF_S *shp_cfg)
{
	INT32 ret = 0;
	MPI_SHP_ATTR_V2_S shp_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_getShpAttrV2(path_idx, &shp_attr);

	shp_attr.mode = shp_cfg->mode;
	shp_attr.shp_manual_v2.sharpness = shp_cfg->manual_shp;

	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		shp_attr.shp_auto_v2.sharpness[num] = shp_cfg->auto_shp_table[num];
	}


	ret = MPI_setShpAttrV2(path_idx, &shp_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 APP_DIP_getShp(MPI_DEV dev_idx, AGTX_DIP_SHP_CONF_S *shp_cfg)
{
	INT32 ret = 0;
	MPI_SHP_ATTR_V2_S shp_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_getShpAttrV2(path_idx, &shp_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	shp_cfg->mode = shp_attr.mode;
	shp_cfg->manual_shp = shp_attr.shp_manual_v2.sharpness;

	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		shp_cfg->auto_shp_table[num] = shp_attr.shp_auto_v2.sharpness[num];
	}

	shp_cfg->video_dev_idx = 0;

	return MPI_SUCCESS;
}

INT32 APP_DIP_setNr(MPI_DEV dev_idx, const AGTX_DIP_NR_CONF_S *nr_cfg)
{
	INT32 ret = 0;
	MPI_NR_ATTR_S nr_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_getNrAttr(path_idx, &nr_attr);

	nr_attr.mode = nr_cfg->mode;
	nr_attr.motion_comp = (UINT8)nr_cfg->motion_comp;
	nr_attr.trail_suppress = (UINT8)nr_cfg->trail_suppress;
	nr_attr.ghost_remove = (UINT8)nr_cfg->ghost_remove;
	nr_attr.ma_y_strength = (UINT8)nr_cfg->ma_y_strength;
	nr_attr.mc_y_strength = (UINT8)nr_cfg->mc_y_strength;
	nr_attr.ma_c_strength = (UINT8)nr_cfg->ma_c_strength;
	nr_attr.ratio_3d = (UINT8)nr_cfg->ratio_3d;
	nr_attr.mc_y_level_offset = (INT16)nr_cfg->mc_y_level_offset;
	nr_attr.me_frame_fallback_en = (UINT8)nr_cfg->me_frame_fallback_en;

	nr_attr.nr_manual.y_level_3d = (UINT8)nr_cfg->manual_y_level_3d;
	nr_attr.nr_manual.c_level_3d = (UINT8)nr_cfg->manual_c_level_3d;
	nr_attr.nr_manual.y_level_2d = (UINT8)nr_cfg->manual_y_level_2d;
	nr_attr.nr_manual.c_level_2d = (UINT8)nr_cfg->manual_c_level_2d;

	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		nr_attr.nr_auto.y_level_3d[num] = (UINT8)nr_cfg->auto_y_level_3d_list[num];
		nr_attr.nr_auto.c_level_3d[num] = (UINT8)nr_cfg->auto_c_level_3d_list[num];
		nr_attr.nr_auto.y_level_2d[num] = (UINT8)nr_cfg->auto_y_level_2d_list[num];
		nr_attr.nr_auto.c_level_2d[num] = (UINT8)nr_cfg->auto_c_level_2d_list[num];
	}

	ret = MPI_setNrAttr(path_idx, &nr_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 APP_DIP_getNr(MPI_DEV dev_idx, AGTX_DIP_NR_CONF_S *nr_cfg)
{
	INT32 ret = 0;
	MPI_NR_ATTR_S nr_attr;

	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
	ret = MPI_getNrAttr(path_idx, &nr_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	nr_cfg->mode = nr_attr.mode;
	nr_cfg->motion_comp = nr_attr.motion_comp;
	nr_cfg->trail_suppress = nr_attr.trail_suppress;
	nr_cfg->ghost_remove = nr_attr.ghost_remove;
	nr_cfg->ma_y_strength = nr_attr.ma_y_strength;
	nr_cfg->mc_y_strength = nr_attr.mc_y_strength;
	nr_cfg->ma_c_strength = nr_attr.ma_c_strength;
	nr_cfg->ratio_3d = nr_attr.ratio_3d;
	nr_cfg->mc_y_level_offset = nr_attr.mc_y_level_offset;
	nr_cfg->me_frame_fallback_en = nr_attr.me_frame_fallback_en;

	nr_cfg->manual_y_level_3d = nr_attr.nr_manual.y_level_3d;
	nr_cfg->manual_c_level_3d = nr_attr.nr_manual.c_level_3d;
	nr_cfg->manual_y_level_2d = nr_attr.nr_manual.y_level_2d;
	nr_cfg->manual_c_level_2d = nr_attr.nr_manual.c_level_2d;

	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		nr_cfg->auto_y_level_3d_list[num] = nr_attr.nr_auto.y_level_3d[num];
		nr_cfg->auto_c_level_3d_list[num] = nr_attr.nr_auto.c_level_3d[num];
		nr_cfg->auto_y_level_2d_list[num] = nr_attr.nr_auto.y_level_2d[num];
		nr_cfg->auto_c_level_2d_list[num] = nr_attr.nr_auto.c_level_2d[num];
	}

	nr_cfg->video_dev_idx = 0;

	return MPI_SUCCESS;
}

INT32 APP_DIP_setWinShp(MPI_WIN win_idx, const AGTX_SHP_WINDOW_PARAM_S *shp_cfg)
{
	INT32 ret = 0;
	MPI_SHP_ATTR_V2_S shp_attr;

	ret = MPI_getWinShpAttrV2(win_idx, &shp_attr);

	shp_attr.mode = shp_cfg->mode;
	shp_attr.shp_manual_v2.sharpness = shp_cfg->manual_shp;

	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		shp_attr.shp_auto_v2.sharpness[num] = shp_cfg->auto_shp_table[num];
	}

	ret = MPI_setWinShpAttrV2(win_idx, &shp_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 APP_DIP_getWinShp(MPI_WIN win_idx, AGTX_SHP_WINDOW_PARAM_S *shp_cfg)
{
	INT32 ret = 0;
	MPI_SHP_ATTR_V2_S shp_attr;
	ret = MPI_getWinShpAttrV2(win_idx, &shp_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	shp_cfg->mode = shp_attr.mode;
	shp_cfg->manual_shp = shp_attr.shp_manual_v2.sharpness;

	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		shp_cfg->auto_shp_table[num] = shp_attr.shp_auto_v2.sharpness[num];
	}

	return MPI_SUCCESS;
}

INT32 APP_DIP_setWinNr(MPI_WIN win_idx, const AGTX_NR_WINDOW_PARAM_S *nr_cfg)
{
	INT32 ret = 0;
	MPI_NR_ATTR_S nr_attr;

	ret = MPI_getWinNrAttr(win_idx, &nr_attr);

	nr_attr.mode = nr_cfg->mode;

	nr_attr.motion_comp = (UINT8)nr_cfg->motion_comp;
	nr_attr.trail_suppress = (UINT8)nr_cfg->trail_suppress;
	nr_attr.ghost_remove = (UINT8)nr_cfg->ghost_remove;
	nr_attr.ma_y_strength = (UINT8)nr_cfg->ma_y_strength;
	nr_attr.mc_y_strength = (UINT8)nr_cfg->mc_y_strength;
	nr_attr.ma_c_strength = (UINT8)nr_cfg->ma_c_strength;
	nr_attr.ratio_3d = (UINT8)nr_cfg->ratio_3d;
	nr_attr.mc_y_level_offset = (INT16)nr_cfg->mc_y_level_offset;
	nr_attr.me_frame_fallback_en = (UINT8)nr_cfg->me_frame_fallback_en;

	nr_attr.nr_manual.y_level_3d = (UINT8)nr_cfg->manual_y_level_3d;
	nr_attr.nr_manual.c_level_3d = (UINT8)nr_cfg->manual_c_level_3d;
	nr_attr.nr_manual.y_level_2d = (UINT8)nr_cfg->manual_y_level_2d;
	nr_attr.nr_manual.c_level_2d = (UINT8)nr_cfg->manual_c_level_2d;

	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		nr_attr.nr_auto.y_level_3d[num] = (UINT8)nr_cfg->auto_y_level_3d_list[num];
		nr_attr.nr_auto.c_level_3d[num] = (UINT8)nr_cfg->auto_c_level_3d_list[num];
		nr_attr.nr_auto.y_level_2d[num] = (UINT8)nr_cfg->auto_y_level_2d_list[num];
		nr_attr.nr_auto.c_level_2d[num] = (UINT8)nr_cfg->auto_c_level_2d_list[num];
	}

	ret = MPI_setWinNrAttr(win_idx, &nr_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 APP_DIP_getWinNr(MPI_WIN win_idx, AGTX_NR_WINDOW_PARAM_S *nr_cfg)
{
	INT32 ret = 0;
	MPI_NR_ATTR_S nr_attr;

	ret = MPI_getWinNrAttr(win_idx, &nr_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	nr_cfg->mode = nr_attr.mode;
	nr_cfg->motion_comp = nr_attr.motion_comp;
	nr_cfg->trail_suppress = nr_attr.trail_suppress;
	nr_cfg->ghost_remove = nr_attr.ghost_remove;
	nr_cfg->ma_y_strength = nr_attr.ma_y_strength;
	nr_cfg->mc_y_strength = nr_attr.mc_y_strength;
	nr_cfg->ma_c_strength = nr_attr.ma_c_strength;
	nr_cfg->ratio_3d = nr_attr.ratio_3d;
	nr_cfg->mc_y_level_offset = nr_attr.mc_y_level_offset;
	nr_cfg->me_frame_fallback_en = nr_attr.me_frame_fallback_en;

	nr_cfg->manual_y_level_3d = nr_attr.nr_manual.y_level_3d;
	nr_cfg->manual_c_level_3d = nr_attr.nr_manual.c_level_3d;
	nr_cfg->manual_y_level_2d = nr_attr.nr_manual.y_level_2d;
	nr_cfg->manual_c_level_2d = nr_attr.nr_manual.c_level_2d;

	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		nr_cfg->auto_y_level_3d_list[num] = nr_attr.nr_auto.y_level_3d[num];
		nr_cfg->auto_c_level_3d_list[num] = nr_attr.nr_auto.c_level_3d[num];
		nr_cfg->auto_y_level_2d_list[num] = nr_attr.nr_auto.y_level_2d[num];
		nr_cfg->auto_c_level_2d_list[num] = nr_attr.nr_auto.c_level_2d[num];
	}

	return MPI_SUCCESS;
}

INT32 APP_DIP_setTe(MPI_DEV dev_idx, const AGTX_DIP_TE_CONF_S *te_cfg)
{
	INT32 ret = 0;
	MPI_TE_ATTR_S te_attr;
	INT32 i = 0;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_getTeAttr(path_idx, &te_attr);

	te_attr.mode = te_cfg->mode;

	for (INT32 num = 0; num < MPI_TE_CURVE_ENTRY_NUM; num++) {
		te_attr.te_normal.curve[num] = (UINT32)te_cfg->normal_ctl[num];
	}

	te_attr.te_wdr.brightness = (UINT16)te_cfg->wdr_ctl.brightness;
	te_attr.te_wdr.strength = (UINT16)te_cfg->wdr_ctl.strength;
	te_attr.te_wdr.saliency = (UINT16)te_cfg->wdr_ctl.saliency;
	te_attr.te_wdr.iso_weight = (UINT8)te_cfg->wdr_ctl.iso_weight;
	te_attr.te_wdr.dark_enhance = (UINT8)te_cfg->wdr_ctl.dark_enhance;
	te_attr.te_wdr.iso_max = (UINT32)te_cfg->wdr_ctl.iso_max;
	te_attr.te_wdr.interval = (UINT8)te_cfg->wdr_ctl.interval;
	te_attr.te_wdr.precision = (UINT8)te_cfg->wdr_ctl.precision;

	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		te_attr.te_wdr.noise_cstr[num] = (UINT16)te_cfg->wdr_ctl.noise_cstr[num];
	}

	te_attr.te_wdr_auto.dri_type = te_cfg->wdr_auto_ctl.dri_type;

	for (i = 0; i < MPI_ISO_LUT_ENTRY_NUM; i++) {
		te_attr.te_wdr_auto.dri_gain[i] = (INT32)te_cfg->wdr_auto_ctl.dri_gain[i];
		te_attr.te_wdr_auto.dri_offset[i] = (INT32)te_cfg->wdr_auto_ctl.dri_offset[i];
		te_attr.te_wdr_auto.noise_cstr[i] = (UINT16)te_cfg->wdr_auto_ctl.noise_cstr[i];
	}

	for (i = 0; i < MPI_DRI_LUT_ENTRY_NUM; i++) {
		te_attr.te_wdr_auto.strength[i] = (UINT16)te_cfg->wdr_auto_ctl.strength[i];
		te_attr.te_wdr_auto.brightness[i] = (UINT16)te_cfg->wdr_auto_ctl.brightness[i];
		te_attr.te_wdr_auto.saliency[i] = (UINT16)te_cfg->wdr_auto_ctl.saliency[i];
		te_attr.te_wdr_auto.dark_enhance[i] = (UINT8)te_cfg->wdr_auto_ctl.dark_enhance[i];
	}

	te_attr.te_wdr_auto.iso_max = (UINT32)te_cfg->wdr_auto_ctl.iso_max;
	te_attr.te_wdr_auto.iso_weight = (UINT8)te_cfg->wdr_auto_ctl.iso_weight;
	te_attr.te_wdr_auto.interval = (UINT8)te_cfg->wdr_auto_ctl.interval;
	te_attr.te_wdr_auto.precision = (UINT8)te_cfg->wdr_auto_ctl.precision;

	ret = MPI_setTeAttr(path_idx, &te_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 APP_DIP_getTe(MPI_DEV dev_idx, AGTX_DIP_TE_CONF_S *te_cfg)
{
	INT32 ret = 0;
	MPI_TE_ATTR_S te_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
	INT32 i = 0;

	ret = MPI_getTeAttr(path_idx, &te_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	te_cfg->mode = te_attr.mode;

	for (INT32 num = 0; num < MPI_TE_CURVE_ENTRY_NUM; num++) {
		te_cfg->normal_ctl[num] = te_attr.te_normal.curve[num];
	}

	te_cfg->wdr_ctl.brightness = te_attr.te_wdr.brightness;
	te_cfg->wdr_ctl.strength = te_attr.te_wdr.strength;
	te_cfg->wdr_ctl.saliency = te_attr.te_wdr.saliency;
	te_cfg->wdr_ctl.iso_weight = te_attr.te_wdr.iso_weight;
	te_cfg->wdr_ctl.dark_enhance = te_attr.te_wdr.dark_enhance;
	te_cfg->wdr_ctl.iso_max = te_attr.te_wdr.iso_max;
	te_cfg->wdr_ctl.interval = te_attr.te_wdr.interval;
	te_cfg->wdr_ctl.precision = te_attr.te_wdr.precision;

	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		te_cfg->wdr_ctl.noise_cstr[num] = te_attr.te_wdr.noise_cstr[num];
	}

	te_cfg->wdr_auto_ctl.dri_type = te_attr.te_wdr_auto.dri_type;

	for (i = 0; i < MPI_ISO_LUT_ENTRY_NUM; i++) {
		te_cfg->wdr_auto_ctl.dri_gain[i] = te_attr.te_wdr_auto.dri_gain[i];
		te_cfg->wdr_auto_ctl.dri_offset[i] = te_attr.te_wdr_auto.dri_offset[i];
		te_cfg->wdr_auto_ctl.noise_cstr[i] = te_attr.te_wdr_auto.noise_cstr[i];
	}

	for (i = 0; i < MPI_DRI_LUT_ENTRY_NUM; i++) {
		te_cfg->wdr_auto_ctl.strength[i] = te_attr.te_wdr_auto.strength[i];
		te_cfg->wdr_auto_ctl.brightness[i] = te_attr.te_wdr_auto.brightness[i];
		te_cfg->wdr_auto_ctl.saliency[i] = te_attr.te_wdr_auto.saliency[i];
		te_cfg->wdr_auto_ctl.dark_enhance[i] = te_attr.te_wdr_auto.dark_enhance[i];
	}

	te_cfg->wdr_auto_ctl.iso_max = te_attr.te_wdr_auto.iso_max;
	te_cfg->wdr_auto_ctl.iso_weight = te_attr.te_wdr_auto.iso_weight;
	te_cfg->wdr_auto_ctl.interval = te_attr.te_wdr_auto.interval;
	te_cfg->wdr_auto_ctl.precision = te_attr.te_wdr_auto.precision;

	te_cfg->video_dev_idx = 0;

	return MPI_SUCCESS;
}

INT32 APP_DIP_setGamma(MPI_DEV dev_idx, const AGTX_DIP_GAMMA_CONF_S *gamma_cfg)
{
	INT32 ret = 0;
	MPI_GAMMA_ATTR_S gamma_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_getGammaAttr(path_idx, &gamma_attr);

	gamma_attr.mode = gamma_cfg->gamma;

	for (int i = 0; i < MAX_AGTX_DIP_GAMMA_CONF_S_GAMMA_MANUAL_SIZE; i++) {
		gamma_attr.gma_manual.curve[i] = gamma_cfg->gamma_manual[i];
	}

	ret = MPI_setGammaAttr(path_idx, &gamma_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 APP_DIP_getGamma(MPI_DEV dev_idx, AGTX_DIP_GAMMA_CONF_S *gamma_cfg)
{
	INT32 ret = 0;
	MPI_GAMMA_ATTR_S gamma_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_getGammaAttr(path_idx, &gamma_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	gamma_cfg->gamma = gamma_attr.mode;

	for (int i = 0; i < MAX_AGTX_DIP_GAMMA_CONF_S_GAMMA_MANUAL_SIZE; i++) {
		gamma_cfg->gamma_manual[i] = gamma_attr.gma_manual.curve[i];
	}

	gamma_cfg->video_dev_idx = 0;

	return MPI_SUCCESS;
}

INT32 APP_DIP_setEnh(MPI_DEV dev_idx, const AGTX_DIP_ENH_CONF_S *enh_cfg)
{
	INT32 ret = 0;
	MPI_ENH_ATTR_S enh_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_getEnhAttr(path_idx, &enh_attr);

	/* check and convert to attr_s */
	enh_attr.mode = enh_cfg->mode;
	enh_attr.enh_manual.y_txr_strength = enh_cfg->manual_y_txr_strength;
	enh_attr.enh_manual.y_txr_edge = enh_cfg->manual_y_txr_edge;
	enh_attr.enh_manual.y_txr_detail = enh_cfg->manual_y_txr_detail;
	enh_attr.enh_manual.y_zone_strength = enh_cfg->manual_y_zone_strength;
	enh_attr.enh_manual.y_zone_edge = enh_cfg->manual_y_zone_edge;
	enh_attr.enh_manual.y_zone_detail = enh_cfg->manual_y_zone_detail;
	enh_attr.enh_manual.y_zone_radius = enh_cfg->manual_y_zone_radius;
	enh_attr.enh_manual.y_zone_weight = enh_cfg->manual_y_zone_weight;
	enh_attr.enh_manual.c_strength = enh_cfg->manual_c_strength;
	enh_attr.enh_manual.c_radius = enh_cfg->manual_c_radius;
	enh_attr.enh_manual.c_edge = enh_cfg->manual_c_edge;
	for (INT32 i = 0; i < MPI_ISO_LUT_ENTRY_NUM; i++) {
		enh_attr.enh_auto.y_txr_strength[i] = enh_cfg->auto_y_txr_strength_list[i];
		enh_attr.enh_auto.y_txr_edge[i] = enh_cfg->auto_y_txr_edge_list[i];
		enh_attr.enh_auto.y_txr_detail[i] = enh_cfg->auto_y_txr_detail_list[i];
		enh_attr.enh_auto.y_zone_strength[i] = enh_cfg->auto_y_zone_strength_list[i];
		enh_attr.enh_auto.y_zone_edge[i] = enh_cfg->auto_y_zone_edge_list[i];
		enh_attr.enh_auto.y_zone_detail[i] = enh_cfg->auto_y_zone_detail_list[i];
		enh_attr.enh_auto.y_zone_radius[i] = enh_cfg->auto_y_zone_radius_list[i];
		enh_attr.enh_auto.y_zone_weight[i] = enh_cfg->auto_y_zone_weight_list[i];
		enh_attr.enh_auto.c_strength[i] = enh_cfg->auto_c_strength_list[i];
		enh_attr.enh_auto.c_radius[i] = enh_cfg->auto_c_radius_list[i];
		enh_attr.enh_auto.c_edge[i] = enh_cfg->auto_c_edge_list[i];
	}

	ret = MPI_setEnhAttr(path_idx, &enh_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 APP_DIP_getEnh(MPI_DEV dev_idx, AGTX_DIP_ENH_CONF_S *enh_cfg)
{
	INT32 ret = 0;
	MPI_ENH_ATTR_S enh_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_getEnhAttr(path_idx, &enh_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	/* check and convert to attr_s */
	enh_cfg->mode = enh_attr.mode;
	enh_cfg->manual_y_txr_strength = enh_attr.enh_manual.y_txr_strength;
	enh_cfg->manual_y_txr_edge = enh_attr.enh_manual.y_txr_edge;
	enh_cfg->manual_y_txr_detail = enh_attr.enh_manual.y_txr_detail;
	enh_cfg->manual_y_zone_strength = enh_attr.enh_manual.y_zone_strength;
	enh_cfg->manual_y_zone_edge = enh_attr.enh_manual.y_zone_edge;
	enh_cfg->manual_y_zone_detail = enh_attr.enh_manual.y_zone_detail;
	enh_cfg->manual_y_zone_radius = enh_attr.enh_manual.y_zone_radius;
	enh_cfg->manual_y_zone_weight = enh_attr.enh_manual.y_zone_weight;
	enh_cfg->manual_c_strength = enh_attr.enh_manual.c_strength;
	enh_cfg->manual_c_radius = enh_attr.enh_manual.c_radius;
	enh_cfg->manual_c_edge = enh_attr.enh_manual.c_edge;

	for (INT32 i = 0; i < MPI_ISO_LUT_ENTRY_NUM; i++) {
		enh_cfg->auto_y_txr_strength_list[i] = enh_attr.enh_auto.y_txr_strength[i];
		enh_cfg->auto_y_txr_edge_list[i] = enh_attr.enh_auto.y_txr_edge[i];
		enh_cfg->auto_y_txr_detail_list[i] = enh_attr.enh_auto.y_txr_detail[i];
		enh_cfg->auto_y_zone_strength_list[i] = enh_attr.enh_auto.y_zone_strength[i];
		enh_cfg->auto_y_zone_edge_list[i] = enh_attr.enh_auto.y_zone_edge[i];
		enh_cfg->auto_y_zone_detail_list[i] = enh_attr.enh_auto.y_zone_detail[i];
		enh_cfg->auto_y_zone_radius_list[i] = enh_attr.enh_auto.y_zone_radius[i];
		enh_cfg->auto_y_zone_weight_list[i] = enh_attr.enh_auto.y_zone_weight[i];
		enh_cfg->auto_c_strength_list[i] = enh_attr.enh_auto.c_strength[i];
		enh_cfg->auto_c_radius_list[i] = enh_attr.enh_auto.c_radius[i];
		enh_cfg->auto_c_edge_list[i] = enh_attr.enh_auto.c_edge[i];
	}

	enh_cfg->video_dev_idx = 0;

	return MPI_SUCCESS;
}

INT32 APP_DIP_setCoring(MPI_DEV dev_idx, const AGTX_DIP_CORING_CONF_S *coring_cfg)
{
	INT32 ret = 0;
	MPI_CORING_ATTR_S coring_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_getCoringAttr(path_idx, &coring_attr);

	/* check and convert to attr_s */
	coring_attr.mode = coring_cfg->mode;
	coring_attr.coring_slope = coring_cfg->coring_slope;
	coring_attr.coring_manual.abs_th = coring_cfg->manual_abs_th;
	for (INT32 i = 0; i < MPI_ISO_LUT_ENTRY_NUM; i++) {
		coring_attr.coring_auto.abs_th[i] = coring_cfg->auto_abs_th_list[i];
	}

	ret = MPI_setCoringAttr(path_idx, &coring_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 APP_DIP_getCoring(MPI_DEV dev_idx, AGTX_DIP_CORING_CONF_S *coring_cfg)
{
	INT32 ret = 0;
	MPI_CORING_ATTR_S coring_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_getCoringAttr(path_idx, &coring_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	/* check and convert to attr_s */
	coring_cfg->mode = coring_attr.mode;
	coring_cfg->coring_slope = coring_attr.coring_slope;
	coring_cfg->manual_abs_th = coring_attr.coring_manual.abs_th;
	for (INT32 i = 0; i < MPI_ISO_LUT_ENTRY_NUM; i++) {
		coring_cfg->auto_abs_th_list[i] = coring_attr.coring_auto.abs_th[i];
	}

	coring_cfg->video_dev_idx = 0;

	return MPI_SUCCESS;
}

INT32 APP_DIP_setFcs(MPI_DEV dev_idx, const AGTX_DIP_FCS_CONF_S *fcs_cfg)
{
	INT32 ret = 0;
	MPI_FCS_ATTR_S fcs_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_getFcsAttr(path_idx, &fcs_attr);

	/* check and convert to attr_s */
	fcs_attr.mode = fcs_cfg->mode;
	fcs_attr.fcs_manual.strength = fcs_cfg->manual_strength;
	fcs_attr.fcs_manual.threshold = fcs_cfg->manual_threshold;
	fcs_attr.fcs_manual.offset = fcs_cfg->manual_offset;

	for (INT32 i = 0; i < MPI_ISO_LUT_ENTRY_NUM; i++) {
		fcs_attr.fcs_auto.strength[i] = fcs_cfg->auto_strength_list[i];
		fcs_attr.fcs_auto.threshold[i] = fcs_cfg->auto_threshold_list[i];
		fcs_attr.fcs_auto.offset[i] = fcs_cfg->auto_offset_list[i];
	}

	ret = MPI_setFcsAttr(path_idx, &fcs_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 APP_DIP_getFcs(MPI_DEV dev_idx, AGTX_DIP_FCS_CONF_S *fcs_cfg)
{
	INT32 ret = 0;
	MPI_FCS_ATTR_S fcs_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_getFcsAttr(path_idx, &fcs_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	/* check and convert to attr_s */
	fcs_cfg->mode = fcs_attr.mode;
	fcs_cfg->manual_strength = fcs_attr.fcs_manual.strength;
	fcs_cfg->manual_threshold = fcs_attr.fcs_manual.threshold;
	fcs_cfg->manual_offset = fcs_attr.fcs_manual.offset;

	for (INT32 i = 0; i < MPI_ISO_LUT_ENTRY_NUM; i++) {
		fcs_cfg->auto_strength_list[i] = fcs_attr.fcs_auto.strength[i];
		fcs_cfg->auto_threshold_list[i] = fcs_attr.fcs_auto.threshold[i];
		fcs_cfg->auto_offset_list[i] = fcs_attr.fcs_auto.offset[i];
	}

	fcs_cfg->video_dev_idx = 0;

	return MPI_SUCCESS;
}

INT32 APP_DIP_setHdrSynth(MPI_DEV dev_idx, const AGTX_DIP_HDR_SYNTH_CONF_S *hdr_synth_cfg)
{
	INT32 ret = 0;
	MPI_HDR_SYNTH_ATTR_S hdr_synth_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_getHdrSynthAttr(path_idx, &hdr_synth_attr);

	/* check and convert to attr_s */
	hdr_synth_attr.local_fb_th = hdr_synth_cfg->local_fb_th;
	hdr_synth_attr.weight.se_weight_th_min = hdr_synth_cfg->weight_se_weight_th_min;
	hdr_synth_attr.weight.se_weight_slope = hdr_synth_cfg->weight_se_weight_slope;
	hdr_synth_attr.weight.se_weight_min = hdr_synth_cfg->weight_se_weight_min;
	hdr_synth_attr.weight.se_weight_max = hdr_synth_cfg->weight_se_weight_max;
	hdr_synth_attr.weight.le_weight_th_max = hdr_synth_cfg->weight_le_weight_th_max;
	hdr_synth_attr.weight.le_weight_slope = hdr_synth_cfg->weight_le_weight_slope;
	hdr_synth_attr.weight.le_weight_min = hdr_synth_cfg->weight_le_weight_min;
	hdr_synth_attr.weight.le_weight_max = hdr_synth_cfg->weight_le_weight_max;

	ret = MPI_setHdrSynthAttr(path_idx, &hdr_synth_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 APP_DIP_getHdrSynth(MPI_DEV dev_idx, AGTX_DIP_HDR_SYNTH_CONF_S *hdr_synth_cfg)
{
	INT32 ret = 0;
	MPI_HDR_SYNTH_ATTR_S hdr_synth_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_getHdrSynthAttr(path_idx, &hdr_synth_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	/* check and convert to attr_s */
	hdr_synth_cfg->local_fb_th = hdr_synth_attr.local_fb_th;
	hdr_synth_cfg->weight_se_weight_th_min = hdr_synth_attr.weight.se_weight_th_min;
	hdr_synth_cfg->weight_se_weight_slope = hdr_synth_attr.weight.se_weight_slope;
	hdr_synth_cfg->weight_se_weight_min = hdr_synth_attr.weight.se_weight_min;
	hdr_synth_cfg->weight_se_weight_max = hdr_synth_attr.weight.se_weight_max;
	hdr_synth_cfg->weight_le_weight_th_max = hdr_synth_attr.weight.le_weight_th_max;
	hdr_synth_cfg->weight_le_weight_slope = hdr_synth_attr.weight.le_weight_slope;
	hdr_synth_cfg->weight_le_weight_min = hdr_synth_attr.weight.le_weight_min;
	hdr_synth_cfg->weight_le_weight_max = hdr_synth_attr.weight.le_weight_max;

	hdr_synth_cfg->video_dev_idx = 0;

	return MPI_SUCCESS;
}

INT32 APP_DIP_setStat(MPI_DEV dev_idx, const AGTX_DIP_STAT_CONF_S *stat_cfg)
{
	INT32 ret = 0;
	MPI_STAT_CFG_S stat_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_getStatisticsConfig(path_idx, &stat_attr);

	/* check and convert to attr_s */
	stat_attr.wb.lum_max = stat_cfg->wb.lum_max;
	stat_attr.wb.lum_min = stat_cfg->wb.lum_min;
	stat_attr.wb.lum_slope = stat_cfg->wb.lum_slope;

	for (INT32 i = 0; i < MPI_WB_RB_POINT_NUM; i++) {
		stat_attr.wb.rb_point_x[i] = stat_cfg->wb.rb_point_x[i];
		stat_attr.wb.rb_point_y[i] = stat_cfg->wb.rb_point_y[i];
	}

	for (INT32 i = 0; i < MPI_WB_RB_POINT_NUM - 1; i++) {
		stat_attr.wb.rb_rgn_th[i] = stat_cfg->wb.rb_rgn_th[i];
		stat_attr.wb.rb_rgn_slope[i] = stat_cfg->wb.rb_rgn_slope[i];
	}

	ret = MPI_setStatisticsConfig(path_idx, &stat_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 APP_DIP_getStat(MPI_DEV dev_idx, AGTX_DIP_STAT_CONF_S *stat_cfg)
{
	INT32 ret = 0;
	MPI_STAT_CFG_S stat_attr;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_getStatisticsConfig(path_idx, &stat_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	/* check and convert to attr_s */
	stat_cfg->wb.lum_max = stat_attr.wb.lum_max;
	stat_cfg->wb.lum_min = stat_attr.wb.lum_min;
	stat_cfg->wb.lum_slope = stat_attr.wb.lum_slope;

	for (INT32 i = 0; i < MPI_WB_RB_POINT_NUM; i++) {
		stat_cfg->wb.rb_point_x[i] = stat_attr.wb.rb_point_x[i];
		stat_cfg->wb.rb_point_y[i] = stat_attr.wb.rb_point_y[i];
	}

	for (INT32 i = 0; i < MPI_WB_RB_POINT_NUM - 1; i++) {
		stat_cfg->wb.rb_rgn_th[i] = stat_attr.wb.rb_rgn_th[i];
		stat_cfg->wb.rb_rgn_slope[i] = stat_attr.wb.rb_rgn_slope[i];
	}

	stat_cfg->video_dev_idx = 0;

	return MPI_SUCCESS;
}

INT32 APP_DIP_getExposureInfo(MPI_DEV dev_idx, AGTX_DIP_EXPOSURE_INFO_S *exp_info)
{
	INT32 ret = 0;
	MPI_EXPOSURE_INFO_S mpi_exp_info;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_queryExposureInfo(path_idx, &mpi_exp_info);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	/* check and convert to attr_s */
	exp_info->inttime = mpi_exp_info.inttime;
	exp_info->sensor_gain = mpi_exp_info.sensor_gain;
	exp_info->isp_gain = mpi_exp_info.isp_gain;
	exp_info->sys_gain = mpi_exp_info.sys_gain;
	exp_info->iso = mpi_exp_info.iso;
	exp_info->frame_delay = mpi_exp_info.frame_delay;
	exp_info->flicker_free_conf = mpi_exp_info.flicker_free_conf;
	exp_info->fps = mpi_exp_info.fps;
	exp_info->ratio = mpi_exp_info.ratio;
	exp_info->luma_avg = mpi_exp_info.luma_avg;
	exp_info->video_dev_idx = 0;

	return MPI_SUCCESS;
}
INT32 APP_DIP_getWhiteBalanceInfo(MPI_DEV dev_idx, AGTX_DIP_WHITE_BALANCE_INFO_S *wb_info)
{
	INT32 ret = 0;
	MPI_WHITE_BALANCE_INFO_S mpi_wb_info;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_queryWhiteBalanceInfo(path_idx, &mpi_wb_info);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	for (int i = 0; i < MPI_AWB_CHN_NUM; ++i) {
		wb_info->gain0[i] = mpi_wb_info.gain0[i];
		wb_info->gain1[i] = mpi_wb_info.gain1[i];
	}

	for (int i = 0; i < MPI_COLOR_CHN_NUM * MPI_COLOR_CHN_NUM; ++i) {
		wb_info->matrix[i] = mpi_wb_info.matrix[i];
	}

	wb_info->color_temp = mpi_wb_info.color_temp;

	return MPI_SUCCESS;
}
