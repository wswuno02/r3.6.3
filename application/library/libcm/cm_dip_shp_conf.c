#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#include <stdio.h>
#include <string.h>

#include "json.h"

#include "agtx_dip_shp_conf.h"


void parse_dip_shp_conf(AGTX_DIP_SHP_CONF_S *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	int i;

	if (json_object_object_get_ex(cmd_obj, "auto_shp_table", &tmp_obj)) {
		for (i = 0; i < MAX_AGTX_DIP_SHP_CONF_S_AUTO_SHP_TABLE_SIZE; i++) {
			data->auto_shp_table[i] = json_object_get_int(json_object_array_get_idx(tmp_obj, i));
		}
	}
	if (json_object_object_get_ex(cmd_obj, "manual_shp", &tmp_obj)) {
		data->manual_shp = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "mode", &tmp_obj)) {
		data->mode = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "video_dev_idx", &tmp_obj)) {
		data->video_dev_idx = json_object_get_int(tmp_obj);
	}
}

void comp_dip_shp_conf(struct json_object *ret_obj, AGTX_DIP_SHP_CONF_S *data)
{
	struct json_object *tmp_obj = NULL;
	struct json_object *int_obj = NULL;

	int i;
	tmp_obj = json_object_new_array();
	if (tmp_obj) {
		for (i = 0; i < MAX_AGTX_DIP_SHP_CONF_S_AUTO_SHP_TABLE_SIZE; i++) {
			int_obj = json_object_new_int(data->auto_shp_table[i]);
			json_object_array_add(tmp_obj, int_obj);
		}
		json_object_object_add(ret_obj, "auto_shp_table", tmp_obj);
	} else {
		printf("Cannot create array %s object\n", "auto_shp_table");
	}

	tmp_obj = json_object_new_int(data->manual_shp);
	if (tmp_obj) {
		json_object_object_add(ret_obj, "manual_shp", tmp_obj);
	} else {
		printf("Cannot create %s object\n", "manual_shp");
	}

	tmp_obj = json_object_new_int(data->mode);
	if (tmp_obj) {
		json_object_object_add(ret_obj, "mode", tmp_obj);
	} else {
		printf("Cannot create %s object\n", "mode");
	}

	tmp_obj = json_object_new_int(data->video_dev_idx);
	if (tmp_obj) {
		json_object_object_add(ret_obj, "video_dev_idx", tmp_obj);
	} else {
		printf("Cannot create %s object\n", "video_dev_idx");
	}

}

#ifdef __cplusplus
}
#endif /* __cplusplus */
