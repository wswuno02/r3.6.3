#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#include <stdio.h>
#include <string.h>

#include "json.h"

#include "agtx_dip_dcc_conf.h"


void parse_agtx_dip_dcc_attr_s(AGTX_DIP_DCC_ATTR_S *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	int i;

	if (json_object_object_get_ex(cmd_obj, "gain", &tmp_obj)) {
		for (i = 0; i < MAX_AGTX_DIP_DCC_ATTR_S_GAIN_SIZE; i++) {
			data->gain[i] = json_object_get_int(json_object_array_get_idx(tmp_obj, i));
		}
	}
	if (json_object_object_get_ex(cmd_obj, "offset", &tmp_obj)) {
		for (i = 0; i < MAX_AGTX_DIP_DCC_ATTR_S_OFFSET_SIZE; i++) {
			data->offset[i] = json_object_get_int(json_object_array_get_idx(tmp_obj, i));
		}
	}
}

void parse_dip_dcc_conf(AGTX_DIP_DCC_CONF_S *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	int i;

	if (json_object_object_get_ex(cmd_obj, "dcc", &tmp_obj)) {
		for (i = 0; i < MAX_AGTX_DIP_DCC_CONF_S_DCC_SIZE; i++) {
			parse_agtx_dip_dcc_attr_s(&(data->dcc[i]), json_object_array_get_idx(tmp_obj, i));
		}
	}
	if (json_object_object_get_ex(cmd_obj, "video_dev_idx", &tmp_obj)) {
		data->video_dev_idx = json_object_get_int(tmp_obj);
	}
}

void comp_dip_dcc_attr(struct json_object *ret_obj, AGTX_DIP_DCC_ATTR_S *data)
{
	struct json_object *tmp_obj = NULL;
	struct json_object *int_obj = NULL;

	tmp_obj = json_object_new_object();

	if (tmp_obj) {
		struct json_object *tmp1_obj = NULL;

		int i;
		tmp1_obj = json_object_new_array();
		if (tmp1_obj) {
			for (i = 0; i < MAX_AGTX_DIP_DCC_ATTR_S_GAIN_SIZE; i++) {
				int_obj = json_object_new_int(data->gain[i]);
				json_object_array_add(tmp1_obj, int_obj);
			}
			json_object_object_add(tmp_obj, "gain", tmp1_obj);
		} else {
			printf("Cannot create array %s object\n", "gain");
		}

		tmp1_obj = json_object_new_array();
		if (tmp1_obj) {
			for (i = 0; i < MAX_AGTX_DIP_DCC_ATTR_S_OFFSET_SIZE; i++) {
				int_obj = json_object_new_int(data->offset[i]);
				json_object_array_add(tmp1_obj, int_obj);
			}
			json_object_object_add(tmp_obj, "offset", tmp1_obj);
		} else {
			printf("Cannot create array %s object\n", "offset");
		}

		json_object_array_add(ret_obj, tmp_obj);
	} else {
		printf("Cannot create array object\n");
	}
}

void comp_dip_dcc_conf(struct json_object *ret_obj, AGTX_DIP_DCC_CONF_S *data)
{
	struct json_object *tmp_obj = NULL;

	int i;
	tmp_obj = json_object_new_array();
	if (tmp_obj) {
		for (i = 0; i < MAX_AGTX_DIP_DCC_CONF_S_DCC_SIZE; i++) {
			comp_dip_dcc_attr(tmp_obj, &(data->dcc[i]));
		}
		json_object_object_add(ret_obj, "dcc", tmp_obj);
	} else {
		printf("Cannot create array %s object\n", "dcc");
	}

	tmp_obj = json_object_new_int(data->video_dev_idx);
	if (tmp_obj) {
		json_object_object_add(ret_obj, "video_dev_idx", tmp_obj);
	} else {
		printf("Cannot create %s object\n", "video_dev_idx");
	}

}

#ifdef __cplusplus
}
#endif /* __cplusplus */
