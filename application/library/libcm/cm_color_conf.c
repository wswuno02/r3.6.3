#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#include <stdio.h>
#include <string.h>

#include "json.h"

#include "agtx_color_conf.h"


const char * agtx_color_mode_e_map[] = {
	"DAY",
	"NIGHT"
};

void parse_color_conf(AGTX_COLOR_CONF_S *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	int i;
	const char *str;

	if (json_object_object_get_ex(cmd_obj, "color_mode", &tmp_obj)) {
		str = json_object_get_string(tmp_obj);
		for (i = 0; i < sizeof(agtx_color_mode_e_map)/sizeof(char *); i++) {
			if (strcmp(agtx_color_mode_e_map[i], str) == 0) {
				data->color_mode = (AGTX_COLOR_MODE_E) i;
				break;
			}
		}
	}
}

void comp_color_conf(struct json_object *ret_obj, AGTX_COLOR_CONF_S *data)
{
	struct json_object *tmp_obj = NULL;

	const char *str;
	str = agtx_color_mode_e_map[data->color_mode];
	tmp_obj = json_object_new_string(str);
	if (tmp_obj) {
		json_object_object_add(ret_obj, "color_mode", tmp_obj);
	} else {
		printf("Cannot create %s object\n", "color_mode");
	}

}

#ifdef __cplusplus
}
#endif /* __cplusplus */
