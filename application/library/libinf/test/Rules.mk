########################################
# [DON'T TOUCH] Traverse directory tree
########################################
sp             := $(sp).x
dirstack_$(sp) := $(dir)
dir            := $(subdir)

########################################
# Define Node in directory tree
########################################

D=TTEST

SRCS_$(D) := $(wildcard $(dir)/*.c)
BINS_$(D) := $(SRCS_$(D):.c=.elf)

SRCS_CXX_$(D) := $(wildcard $(dir)/*.cc)
BINS_CXX_$(D) := $(SRCS_CXX_$(D):.cc=.elf)

MIN_SRCS := $(dir)/minimal.cc
BINS_MIN := $(dir)/minimal

LIB_COMMON= \
-L$(CONFIG_INF_LIB_PATH) \
$(CONFIG_INF_LIB) \
-pthread -ldl -lrt -lm -lstdc++

LIB_$(D)= \
-Wl,--start-group \
-L$(LIBINF_LIB) -linf \
$(LIB_COMMON) \
-Wl,--end-group

ifeq ($(HOST), linux)
LIB_$(D)=\
-Wl,--start-group \
-L$(LIBINF_LIB) -linf -Wl,-rpath=$(LIBINF_LIB)\
$(LIB_COMMON) \
-Wl,-rpath=$(CONFIG_INF_LIB_PATH)\
-Wl,--end-group
endif

test : test-c test-cxx test-min

test-c: $(BINS_$(D))

$(BINS_$(D)): CFLAGS := -g3 -std=gnu99 -Wall $(INCS)
$(BINS_$(D)): $(LIB_STATIC_OUTPUT) $(LIB_SHARED_OUTPUT)

%.elf : %.c
	@printf "    %-8s $@\n" "LD" $(VOUT)
	$(Q)$(CC) -o $@ $< $(CFLAGS) $(LIB_$(D))

test-cxx : $(BINS_CXX_$(D))

$(BINS_CXX_$(D)): CXXFLAGS := -g3 -std=c++11 -Wall $(INCS)
$(BINS_CXX_$(D)): $(LIB_STATIC_OUTPUT) $(LIB_SHARED_OUTPUT)

%.elf : %.cc
	@printf "    %-8s $@\n" "LD" $(VOUT)
	$(Q)$(CXX) -o $@ $< $(CXXFLAGS) $(LIB_$(D))

test-min : $(BINS_MIN)

$(BINS_MIN) : CXXFLAGS := -g3 -std=c++11 -Wall $(INCS)
$(BINS_MIN) : LIB_$(D) := $(LIB_COMMON) \
			 -Wl,-rpath=$(CONFIG_INF_LIB_PATH)\

$(BINS_MIN) : $(MIN_SRCS)
	@printf "    %-8s $@\n" "LD" $(VOUT)
	$(Q)$(CXX) -o $@ $< $(CXXFLAGS) $(LIB_$(D))

test-clean:
	$(Q)$(RM) $(BINS_$(D)) $(BINS_CXX_$(D)) $(BINS_MIN)

.PHONY: call-test-min
call-test-min:
	@echo BIN: $(BINS_$(D))
	@echo SRC: $(SRCS_CC_$(D)) $(SRCS_CXX_$(D))
	@echo OBJ: $(OBJS_$(D))
	@echo $(subdir)
	@echo $(dir)

########################################
# [DON'T TOUCH] Traverse directory tree
########################################
dir		:= $(dirstack_$(sp))
sp		:= $(basename $(sp))
