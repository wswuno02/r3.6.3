#include <memory>

#include "tensorflow/lite/interpreter.h"
#include "tensorflow/lite/kernels/register.h"
#include "tensorflow/lite/model.h"

#include "inf_utils_lite.h"
#include "inf_trc.h"

InfDataType utils::lite::GetDataType(const tflite::Interpreter *interpreter, int tensor_idx)
{
	switch (interpreter->tensor(tensor_idx)->type) {
		case kTfLiteUInt8: {
			return Inf8U;
			break;
		} case kTfLiteInt8: {
			return Inf8S;
			break;
		} case kTfLiteFloat32: {
			return Inf32F;
			break;
		} default: {
			inf_err("TfLite inference is not implemented for this datatype yet %d!\n", interpreter->tensor(tensor_idx)->type);
			return InfUnknownType;
		}
	}
}

int utils::lite::LoadModels(const char* tflite_model,
	std::unique_ptr<tflite::Interpreter>& interpreter,
	std::unique_ptr<tflite::FlatBufferModel>& model)
{
	model =	tflite::FlatBufferModel::BuildFromFile(tflite_model);
	if (model == nullptr) {
		interpreter.reset();
		return -1;
	}

	// Build the interpreter
	tflite::ops::builtin::BuiltinOpResolver resolver;
	tflite::InterpreterBuilder builder(*model, resolver);
	builder(&interpreter);
	if (interpreter == nullptr) {
		return -1;
	}
	return 0;
}

void utils::lite::GetQuantInfo(const std::unique_ptr<tflite::Interpreter> &model, QuantInfo *info, int num_output)
{
	std::vector<int> outputs = model->outputs();
	if (outputs.size() != (uint32_t) num_output) {
		inf_err("number of output is not correct!\n");
	}
	for (uint32_t i = 0; i < outputs.size() ; ++i) {
		const TfLiteQuantizationParams *p = &model->tensor(outputs[i])->params;
		info[i].zero = p->zero_point;
		info[i].scale = p->scale;
		inf_info_l("out:%d float: %.9f, zero: %d\n", i, info[i].m_scale, info[i].m_zero);
	}
}

InfImage utils::lite::GetInputImage(const std::unique_ptr<tflite::Interpreter> &model)
{
	InfImage img{};

	if (model == nullptr)
		return img;

	const int input_idx = model->inputs()[0];
	const TfLiteIntArray *in_dims = model->tensor(input_idx)->dims;

	img.dtype = utils::lite::GetDataType(model.get(), input_idx);

	img.h = in_dims->data[1];
	img.w = in_dims->data[2];
	img.c = in_dims->data[3];
	img.buf_owner = 0;
	if (img.dtype == Inf8U) {
		img.data = (uint8_t*)model->typed_tensor<uint8_t>(input_idx);
	} else if (img.dtype == Inf8S) {
		img.data = (uint8_t*)model->typed_tensor<int8_t>(input_idx);
	} else {
		img.data = 0;
	}
	return img;
}