#include <stdio.h>

#include "inf_detect.h"
#include "inf_model.h"

#include "inf_model_internal.h"
#include "inf_trc.h"
#include "inf_utils.h"

int Inf_InvokeDetect(InfModelCtx *ctx, const InfImage *img, InfDetList *result)
{
	CHECKPTR((!ctx || !ctx->model || !img || !result), "input pointer cannot be null!\n", -EFAULT);
	InfModel* model = static_cast<InfModel*>(ctx->model);
	model->Detect(img, result);
	return 0;
}

int Inf_InvokeDetectObjList(InfModelCtx *ctx, const InfImage *img, const MPI_IVA_OBJ_LIST_S *ol,
                InfDetList *result)
{
	CHECKPTR((!ctx || !ctx->model || !img || !ol || !result), "input pointer cannot be null!\n", -EFAULT);
	InfModel* model = static_cast<InfModel*>(ctx->model);
	model->Detect(img, ol, result);
	return 0;
}
