#ifndef LITE_SCRFD_H_
#define LITE_SCRFD_H_

#ifdef USE_TFLITE

#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include <assert.h>
#include <stdint.h>

#include "tensorflow/lite/interpreter.h"
#include "tensorflow/lite/model.h"

#include "inf_types.h"
#include "inf_trc.h"

#include "inf_model_internal.h"
#include "inf_face_internal.h"

class LiteScrfd : public InfFaceDetect {
    public:
	LiteScrfd()
	{
	}

	~LiteScrfd(void) override;

	int FaceDetect(const InfImage *img, std::vector<FaceBox> &result) override;
	int FaceDetect(const InfImage *img, const MPI_IVA_OBJ_LIST_S *obj_list, std::vector<FaceBox> &result) override;
	int Detect(const InfImage *img, InfDetList *result) override;
	int Detect(const InfImage *img, const MPI_IVA_OBJ_LIST_S *obj_list, InfDetList *result) override;

	int LoadModels(const char *model_dir, const InfStrList *model_paths) override;

	void SetModelThreads(int nthread) override;

	void SetupConfig(InfModelInfo *conf);

	int GetOutputInfo(void);

	private:
	template<typename Tbuffer>
	int TRunNet(const InfImage& img, const MPI_RECT_POINT_S* roi, std::vector<FaceBox>& face_list);

	int FaceDetect(const InfImage *img, const MPI_RECT_POINT_S& roi, std::vector<FaceBox> &result);

	std::unique_ptr<tflite::Interpreter> m_model;
	std::unique_ptr<tflite::FlatBufferModel> m_model_fb;

	static constexpr int ScrfdMaxOutputSize = 10;

	int m_input_dim[3];

	/* describe Scrfd output */
	int m_feature_output_pairs = 0; // fmc
	int m_num_anchors_per_feature = 0; // num_anchors
	int m_use_kps = 0; // use_kps
	int m_feature_stride[10]; // feat_stride_fpn
	int m_output_info_obtained = 0;

 	/* ascending dimension of feature output index */
 	/* need to parse output index*/
 	QuantInfo m_qinfo[10];
 	int m_output_dim[10][3];

	int m_output_prob_idx[5]{};
	int m_output_reg_idx[5]{};
	int m_output_landmark_idx[5]{};

	/* postprocessing step
	1. inf -> output -> unquant
	vector<bbox> boxes;
	for i in prediction(scores, bbox)
		if scores[i] > conf_thresh:
			2. bbox_pre * stride
			3. generate anchor-center meshgrid x,y
			3.a (input_height // stride) X (input_width // stride)
			3.b array number is multiplied by num_anchor of 3.a
			4. distance2bbox(anchor_center, prediction box)
			4.a x1 = anchor-center_i_x - box_0
			4.b y1 = anchor-center_i_y - box_1
			4.c x2 = anchor-center_i_x + box_2
			4.d y2 = anchor-center_i_y + box_3
			boxes.push_back([x1,y1,x2,y2,score])
	5. nms(boxes)
	5.a sort scores
	5.b for each box
	5.c     calc iou score
	5.d     if iou score < thresh: keep
	           -> else filter.
	return
	*/
};

#endif // USE_TFLITE

#endif /* LITE_SCRFD_H_*/
