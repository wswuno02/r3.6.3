#ifndef LITE_FACEENCODE_H_
#define LITE_FACEENCODE_H_

#ifdef USE_TFLITE

#include "tensorflow/lite/interpreter.h"
#include "tensorflow/lite/model.h"

#include "inf_types.h"
#include "inf_model.h"

#include "inf_face_internal.h"

class LiteFaceEncode : public InfFaceEncode {
    public:
	LiteFaceEncode(InfModelInfo *info);
	int EncodeFace(const InfImage *img, const MPI_RECT_POINT_S *roi, std::vector<float> &face) override;
	int LoadModels(const char *model_path) override;
	InfImage GetInputImage(void) override;
	~LiteFaceEncode() override;

	std::unique_ptr<tflite::Interpreter> m_model;
	std::unique_ptr<tflite::FlatBufferModel> m_model_fb;
};

#endif // USE_TFLITE

#endif // LITE_FACEENCODE_H_