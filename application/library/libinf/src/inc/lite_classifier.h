#ifndef LITE_CLASSIFIER_INTERNAL_
#define LITE_CLASSIFIER_INTERNAL_

#ifdef USE_TFLITE

#include "tensorflow/lite/interpreter.h"
#include "tensorflow/lite/model.h"

#include "inf_types.h"
#include "inf_model.h"

#include "inf_model_internal.h"

class LiteClassifier : public InfModel {
    public:
	LiteClassifier(InfModelInfo *info);
	int Classify(const InfImage *img, const MPI_IVA_OBJ_LIST_S *obj_list, InfResultList *result);
	int LoadModels(const char *model_path);
	~LiteClassifier() override;

	std::unique_ptr<tflite::Interpreter> m_model;
	std::unique_ptr<tflite::FlatBufferModel> m_model_fb;

	int m_input_dim[3]; /* h x w x c */
	int m_output_dim[4]; /* b x result x reserved*/

};

#endif // USE_TFINF

#endif // INF_CLASSIFIER_INTERNAL_