#ifndef INF_UTILS_H_
#define INF_UTILS_H_

#include <vector>

#include "inf_types.h"

#define SNAPSHOT_FORMAT "%s/%s/%05d_%.3f.%s"
#define SNAPSHOT_FR_FORMAT "%s/%s/%05d_%.3f%s.%s"
#define EPSILON (1.0e-7)

#define QuantConvert(qinfo, x) ((float)(((int)(x)-qinfo.zero) * (qinfo.scale)))

#define CHECKPTR(cond, msg, err)       \
	{                              \
		if ((cond)) {          \
			inf_err(msg); \
			return (err);  \
		}                      \
	}

#define ASSERT_MSG(cond, msg)  \
	{                              \
		if ((cond)) {          \
			inf_err(msg); \
			assert(0);  \
		}                      \
	}

inline const char *GetDTypeString(int type)
{
	switch (type) {
	case Inf8UC3:
	case Inf8U: {
		return "uint8";
	}
	case Inf8SC3:
	case Inf8S: {
		return "int8";
	}
	case Inf32F:
	case Inf32FC3: {
		return "float32";
	}
	default: {
		return "unknown type!!";
	}
	}
}

typedef float (*VecDistFuncPtr)(const std::vector<float> &, const std::vector<float> &);

struct DistMeasure {
	VecDistFuncPtr dist_funcptr;
	int ascend;
};

struct Shape {
	int w;
	int h;
};

struct Pads {
	int top;
	int right;
	int bot;
	int left;
};

#define Clamp(a, l, h) (((a) < (l)) ? (l) : (((a) > (h)) ? (h) : (a)))

const char *GetInfTypeStr(InfRunType inference_type);
int GetImageType(int dtype, uint32_t chn);
int GetDSize(int dtype);
int GetImageTypeChn(int dtype);
int GetOutputType(const char *output_type_str);
void PrintResult(int output_dim, float *output_buf);

int ParseModelConfig(const char *model_config_path, InfModelInfo *config);
// float QuantConvert(uint8_t in, int zero, float scale);
// float QuantConvertS(int8_t in, int zero, float scale);
void Sigmoid(float *output_addr, int num_classes);
void Softmax(float *output_addr, int num_classes);
void PostProcess(const float *output_addr, int num_classes, float conf_thresh, int topk, const InfIntList *filter_cls,
                 const InfIntList *filter_out_cls, InfResult *result);
void ReleaseConfig(InfModelInfo *config);
void ReleaseIntList(InfIntList *list);
void ReleaseStrList(InfStrList *list);
void ReleaseFloatList(InfFloatList *list);
void ReleaseDetResult(InfDetResult *det);

unsigned char *LoadModelData(const char *model_path, int *model_data_len);

void ImsaveBin(const char *name, const InfImage *p_img);

float VecDistL2Norm(const std::vector<float>& v0, const std::vector<float>& v1);
float VecDistL1Norm(const std::vector<float>& v0, const std::vector<float>& v1);
float VecCosineSimilarity(const std::vector<float>& v0, const std::vector<float>& v1);

void Grouping(const MPI_IVA_OBJ_LIST_S* obj_list, MPI_RECT_POINT_S& roi);
void PadAndRescale(int pad, const Shape& img, const Shape& dst, const MPI_RECT_POINT_S& roi,
							MPI_RECT_POINT_S& dst_roi);
void RescaleFaceBox(int net_h, int net_w, MPI_RECT_POINT_S& roi);
void GetPadInfo(const InfImage* img, Pads& pad, MPI_RECT_POINT_S& roi);

int SetupDebugTool(char *prefix);

#endif /* INF_UTILS_H_ */
