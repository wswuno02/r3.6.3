#ifndef INF_UTILS_LITE_H_
#define INF_UTILS_LITE_H_

#ifdef USE_TFLITE

#include <memory>

#include "tensorflow/lite/interpreter.h"
#include "tensorflow/lite/kernels/register.h"
#include "tensorflow/lite/model.h"

#include "inf_types.h"

namespace utils
{
namespace lite
{
InfDataType GetDataType(const tflite::Interpreter *interpreter, int tensor_idx);

void GetQuantInfo(const std::unique_ptr<tflite::Interpreter> &model, QuantInfo *info, int num_output);

int LoadModels(const char *tflite_model, std::unique_ptr<tflite::Interpreter> &interpreter,
               std::unique_ptr<tflite::FlatBufferModel> &model);

InfImage GetInputImage(const std::unique_ptr<tflite::Interpreter> &model);

} // lite
} // utils

#endif // USE_TFLITE

#endif