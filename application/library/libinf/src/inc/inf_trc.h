#ifndef INF_TRC_H_
#define INF_TRC_H_

#ifdef USE_TFLITE
#ifdef USE_MICROINF
#error Cannot enale tflite and microinf at the same time!
#endif // USE_MICROINF
#endif // USE_TFINF

#ifdef __cplusplus
extern "C" {
#endif

#include <errno.h>
#include <time.h>
#include <stdio.h>
#include <sys/time.h>

#define DEB(fmt, args...) printf("%s %d " fmt " \n", __func__, __LINE__, ##args)
#define TRC_NOT_IMPL(fmt, args...) printf("%s is not implemented! \n", __func__)

#define LLOG(fmt) "[INFO] " fmt
#define LERR(fmt) "[ERROR] %s(): " fmt
#define LWARN(fmt) "[WARNINGS] %s(): " fmt

#define inf_trc(fmt, args...) printf(LLOG(fmt), ##args)
#define inf_warn(fmt, args...) printf(LWARN(fmt), __func__, ##args)
#define inf_err(fmt, args...) fprintf(stderr, LERR(fmt), __func__, ##args)
#define inf_info_l(fmt, args...)
#define inf_info_m(fmt, args...)
#define inf_info_h(fmt, args...) inf_trc(fmt, ##args)

#define inf_check(cond)                                                                 \
	do {                                                                             \
		if (!(cond)) {                                                           \
			inf_err("%s %d assert fail %s !\n", __func__, __LINE__, #cond); \
		}                                                                        \
	} while (0)

#define TIC(start) clock_gettime(CLOCK_MONOTONIC_RAW, &start)

#define TOC(str, start)                                                                                  \
	do {                                                                                             \
		struct timespec end;                                                                     \
		uint64_t delta_us;                                                                       \
		float delta_s;                                                                           \
		clock_gettime(CLOCK_MONOTONIC_RAW, &end);                                                \
		delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000; \
		delta_s = (float)delta_us / 1000000;                                                     \
		printf("%s Elapsed time: %.8f (s).\n", str, delta_s);                                    \
	} while (0)

#ifdef __cplusplus
}
#endif

#endif