#ifdef USE_TFLITE

#include <stdio.h>
#include <string.h>
#include <time.h>

#include "tensorflow/lite/interpreter.h"
#include "tensorflow/lite/kernels/register.h"
#include "tensorflow/lite/model.h"

#include "inf_classifier.h"
#include "inf_face.h"
#include "inf_image.h"
#include "inf_model.h"
#include "inf_types.h"

#include "inf_trc.h"
#include "inf_utils.h"
#include "inf_utils_lite.h"
#include "lite_faceencode.h"

#define Max(a, b) (((a) > (b)) ? (a) : (b))
#define Min(a, b) (((a) < (b)) ? (a) : (b))

LiteFaceEncode::LiteFaceEncode(InfModelInfo *info)
{
	m_config = info;
}

LiteFaceEncode::~LiteFaceEncode()
{
	ReleaseConfig(m_config);
	delete m_config;
	m_config = nullptr;
}

int LiteFaceEncode::LoadModels(const char *model_path)
{
	// 1. Build the interpreter

	if (utils::lite::LoadModels(model_path, m_model, m_model_fb)) {
		inf_err("Cannot find %s for %s!\n", model_path, __func__);
		return -1;
	}

	// 2. Fetch model info
	std::vector<int> outputs = m_model->outputs();
	if (outputs.size() != 1) {
		inf_err("number of output is not correct!\n");
		return -1;
	}

	// 2.1 Fetch Quant info
	const TfLiteQuantizationParams *p = &m_model->tensor(outputs[0])->params;
	m_config->quant.zero = p->zero_point;
	m_config->quant.scale = p->scale;

	const int output_idx = m_model->outputs()[0];
	const int input_idx = m_model->inputs()[0];

	const TfLiteIntArray *outdims = m_model->tensor(output_idx)->dims;
	m_encode_dim = outdims->data[1];

	const TfLiteIntArray *indims = m_model->tensor(input_idx)->dims;
	m_input_dim[0] = indims->data[1];
	m_input_dim[1] = indims->data[2];
	m_input_dim[2] = indims->data[3];

	m_config->dtype = (InfDataType)utils::lite::GetDataType(m_model.get(), input_idx);
	m_type = m_config->dtype;

	if (m_config->dtype == -1) {
		inf_err("Unknown model Datatype for %s!\n", model_path);
		return -1;
	}

	inf_info_h("tflite model input dim is %dx%dx%d type is %s\n",
		m_input_dim[0], m_input_dim[1], m_input_dim[2], GetDTypeString(m_config->dtype));
	inf_info_h("tflite face encode model output dimension is %d\n", m_encode_dim);

	m_model->SetNumThreads(m_num_thread);

	if (m_model->AllocateTensors() != kTfLiteOk) {
		inf_err("Cannot allocate tensor!\n");
		return -1;
	}

	return 0;
}

int LiteFaceEncode::EncodeFace(const InfImage* img, const MPI_RECT_POINT_S* roi, std::vector<float>& face)
{
	tflite::Interpreter *interpreter = m_model.get();

	const int input = interpreter->inputs()[0];
	const int output_idx = interpreter->outputs()[0];

	const int input_h = m_input_dim[0];
	const int input_w = m_input_dim[1];
	const int input_chn = m_input_dim[2];
	const int output_dim = m_encode_dim;

	const int align_margin = m_config->align_margin;
	face.resize(output_dim, 0.0f);

	Pads pad{};
	MPI_RECT_POINT_S box = *roi;

	// Expand box by margin
	box.sx = box.sx - align_margin / 2;
	box.sy = box.sy - align_margin / 2;
	box.ex = box.ex + align_margin / 2;
	box.ey = box.ey + align_margin / 2;

	// Get padding info
	GetPadInfo(img, pad, box);
	int is_pad = pad.left || pad.right || pad.top || pad.right;

	int sx = box.sx;
	int sy = box.sy;
	int ex = box.ex;
	int ey = box.ey;

	InfImage wimg;
	wimg.w = m_input_dim[1];
	wimg.h = m_input_dim[0];
	wimg.c = m_input_dim[2];
	wimg.buf_owner = 0;

	uint8_t *input_addr = nullptr;
	uint8_t *output_addr = nullptr;;

	if (m_config->dtype == Inf8U) {
		input_addr = interpreter->typed_tensor<uint8_t>(input);
	} else if (m_config->dtype == Inf8S) {
		input_addr = (uint8_t*)interpreter->typed_tensor<int8_t>(input);
	} else {
		inf_warn("datatype not supported!\n");
		return -1;
	}

	wimg.dtype = (InfDataType)GetImageType((int)m_config->dtype, input_chn);		
	wimg.data = input_addr;

	if (m_config->dtype == Inf8U || m_config->dtype == Inf8S) {

		if (m_verbose)
			TIC(start);

		if (is_pad)
			Inf_ImcropPadResize(img, sx, sy, ex, ey, pad.top, pad.bot, pad.left, pad.right,
				&wimg, input_w, input_h);
		else
			Inf_ImcropResize(img, sx, sy, ex, ey, &wimg, input_w, input_h);

		if (m_verbose)
			TOC("Preprocess WImage for obj", start);

		if (m_verbose)
			TIC(start);

		if (interpreter->Invoke() != kTfLiteOk) {
			inf_warn("Cannot Invoke tflite model!\n");
			return 0;
		}

		if (m_verbose)
			TOC("Inference Call", start);

		if (m_config->dtype == Inf8U) {
			output_addr = interpreter->typed_tensor<uint8_t>(output_idx);
			for (int j = 0; j < output_dim; j++)
				face[j] = QuantConvert(m_config->quant, output_addr[j]);
		} else {
			output_addr = (uint8_t*)interpreter->typed_tensor<int8_t>(output_idx);
			for (int j = 0; j < output_dim; j++)
				face[j] = QuantConvert(m_config->quant, ((int8_t*)output_addr)[j]);
		}
	} else {
		inf_warn("Currently not support floating point inference!\n");
	}
	return 0;
}


InfImage LiteFaceEncode::GetInputImage(void)
{
	return utils::lite::GetInputImage(m_model);
}

#endif // USE_TFLITE
