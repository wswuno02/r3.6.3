#ifdef USE_TFLITE

#include <libgen.h>
#include <stdint.h>
#include <string.h>
#include <memory>

#include "tensorflow/lite/interpreter.h"
#include "tensorflow/lite/kernels/register.h"
#include "tensorflow/lite/model.h"
#include "tensorflow/lite/optional_debug_tools.h"

#include "inf_types.h"
#include "inf_image.h"
#include "inf_model.h"

#include "inf_trc.h"
#include "inf_utils.h"
#include "inf_utils_lite.h"

#include "lite_scrfd.h"

void LiteScrfd::SetModelThreads(int nthreads)
{
	m_config->num_threads = nthreads;
	m_num_thread = nthreads;
	m_model->SetNumThreads(nthreads);
}

LiteScrfd::~LiteScrfd()
{
	ReleaseConfig(m_config);
	delete m_config;
	m_config = nullptr;
}

int LiteScrfd::LoadModels(const char* model_dir, const InfStrList* model_paths)
{
	if (!model_paths->size) {
		inf_warn("Number of model paths do not equal to 1!\n");
		return -1;
	}
	char net_fname[256] = {};

	snprintf(net_fname, 255, "%s/%s", model_dir, model_paths->data[0]);

	if (m_verbose) TIC(start);

	utils::lite::LoadModels(net_fname, m_model, m_model_fb);

	if(m_model== nullptr) {
		inf_err("Cannot init model\n");
		return -1;
	}

	m_model->SetNumThreads(m_num_thread);

	int input_idx = m_model->inputs()[0];
	std::vector<int> output_idx = m_model->outputs();

	m_input_dim[0] = m_model->tensor(input_idx)->dims->data[1];
	m_input_dim[1] = m_model->tensor(input_idx)->dims->data[2];
	m_input_dim[2] = m_model->tensor(input_idx)->dims->data[3];

	//int num_outputs = output_idx.size();

	m_type =  utils::lite::GetDataType(m_model.get(), input_idx);

	if (m_model->AllocateTensors() != kTfLiteOk) {
		inf_err("Cannot allocate tensor\n!");
		return -1;
	}

	/* Get Actual output information after first invoke
	int ret = GetOutputInfo();
	if (ret)
		return -1;
	*/

	if (m_verbose) TOC("Load SCRFD", start);

	inf_info_h("SCRFD model input dimension is %dx%dx%d\n", m_input_dim[0], m_input_dim[1], m_input_dim[2]);
	inf_info_h("SCRFD model type is %s\n", GetDTypeString(m_type));
	return 0;
}

void LiteScrfd::SetupConfig(InfModelInfo *conf)
{
	m_config = conf;
	m_use_kps = conf->use_kps;
	m_feature_output_pairs = conf->feature_output_pairs;
	m_num_anchors_per_feature = conf->num_anchors_per_feature;
	memcpy(m_feature_stride, conf->feature_stride,
		sizeof(int) *m_feature_output_pairs * m_num_anchors_per_feature);
	m_verbose = conf->verbose;
	m_debug = conf->debug;
	m_num_thread = conf->num_threads;
}

int LiteScrfd::GetOutputInfo(void)
{
	const std::unique_ptr<tflite::Interpreter>& interpreter = m_model;
	const std::vector<int> output_idx = m_model->outputs();

	if (output_idx.size() >= ScrfdMaxOutputSize) {
		inf_info_h("Cannot handle (%d) of network output, should be <= (%d)!\n",
			(int)output_idx.size(), ScrfdMaxOutputSize);
		return -1;
	}

	/* Assign Quantization parameters and output dimension version */
	for (size_t i = 0; i < output_idx.size(); i++) {
		const TfLiteIntArray* dim = interpreter->tensor(output_idx[i])->dims;
		const TfLiteQuantizationParams* p = &interpreter->tensor(output_idx[i])->params;

		QuantInfo& info = m_qinfo[i];
		info.zero = p->zero_point;
		info.scale = p->scale;
		int *output_dim = m_output_dim[i];

		if (dim->size != 3) {
			inf_info_h("Scrfd Network output[number:%d,id:%d] dim size (%d) not supported, should be (3)!\n",
				(int)i, output_idx[i], static_cast<int>(dim->size));
			return -1;
		}
		/* Only if the tensor content is not transposed */
		output_dim[0] = 1;
		output_dim[1] = std::max(dim->data[1], dim->data[2]);
		output_dim[2] = std::min(dim->data[1], dim->data[2]);
	}

	/* Sort and assign dimension ind with descending order */
	/* So that feature stride is start from small to large */
	std::vector<int> ind(static_cast<int>(output_idx.size()),0);

	for (size_t i = 1; i< output_idx.size(); i++)
		ind[i] = i;

	std::sort(ind.begin(), ind.end(), [&](const int& a, const int& b)
		{
			return m_output_dim[a][1] > m_output_dim[b][1];
		});

	int ind_divident = (m_use_kps) ? 3 : 2;

	for (size_t i = 0; i < ind.size(); i++) {
		int output_ind = i/ind_divident;
		if (!m_output_prob_idx[output_ind] && m_output_dim[ind[i]][2] == 1)
			m_output_prob_idx[output_ind] = ind[i];
		else if (!m_output_reg_idx[output_ind] && m_output_dim[ind[i]][2] == 4)
			m_output_reg_idx[output_ind] = ind[i];
		else if (m_use_kps && !m_output_landmark_idx[output_ind] &&
			m_output_dim[ind[i]][2] == 10)
			m_output_landmark_idx[output_ind] = ind[i];
	}
	return 0;
}

static int RunNetwork(std::unique_ptr<tflite::Interpreter>& interpreter, const InfImage& img,
	const Shape& size, int chn, InfDataType mtype, const MPI_RECT_POINT_S *roi, const Pads *pads, int verbose)
{
	/* Get input/output tensor index */
	const int input = interpreter->inputs()[0];
	int ret = 0;
	struct timespec start;

	InfDataType dtype = (InfDataType)GetImageType(mtype, chn);
	uint8_t* input_addr = nullptr;

	if (mtype == Inf8U) {
		input_addr = interpreter->typed_tensor<uint8_t>(input);
	} else if (mtype == Inf8S) {
		input_addr = (uint8_t*)interpreter->typed_tensor<int8_t>(input);
	} else {
		inf_warn("%s does not support floating point inference\n", __func__);
		return 0;
	}

	InfImage dst{size.w, size.h, chn, input_addr, 0, dtype};

	if (verbose) TIC(start);

	if (!roi)
		Inf_Imresize(&img, size.w, size.h, &dst);
	else if (!pads)
		Inf_ImcropResize(&img, roi->sx, roi->sy, roi->ex, roi->ey, &dst, size.w, size.h);
	else
		Inf_ImcropPadResize(&img, roi->sx, roi->sy, roi->ex, roi->ey,
			pads->top, pads->bot, pads->left, pads->right, &dst, size.w, size.h);

	if (verbose) TOC("Image preprocess", start);

	if (verbose) TIC(start);

	if (interpreter->Invoke() != kTfLiteOk) {
		ret = -1;
	}

	if (verbose) TOC("Invoke", start);

	return ret;
}

struct Scaler {
	float w;
	float h;
};

template<typename Tbuffer>
int LiteScrfd::TRunNet(const InfImage& img, const MPI_RECT_POINT_S* roi, std::vector<FaceBox>& face_list)
{
	std::unique_ptr<tflite::Interpreter>& interpreter = m_model; 
	const int height = m_input_dim[0];
	const int width = m_input_dim[1];
	const int chn = m_input_dim[2];
	const Shape size{width, height};
	Scaler scale_factor{};
	Pads pads{};
	int need_padding = 0;
	int ret = 0;
	const std::vector<int> outputs = interpreter->outputs();
	std::vector<FaceBox> feature_list;

	if (roi) {
		if (roi->sx < 0) pads.left = -roi->sx;
		if (roi->sy < 0) pads.top = -roi->sy;
		if (roi->ex >= img.w) pads.right = roi->ex + 1 - img.w;
		if (roi->ey >= img.h) pads.bot = roi->ey + 1 - img.h;

		if (pads.left || pads.top || pads.right || pads.bot) {
			need_padding = 1;
		}
		scale_factor.w = (float)(roi->ex - roi->sx + 1) / width;
		scale_factor.h = (float)(roi->ey - roi->sy + 1) / height;
	} else {
		scale_factor.w = (float)img.w / width;
		scale_factor.h = (float)img.h / height;
	}

	if (need_padding) {
		ret = RunNetwork(interpreter, img, size, chn, m_type, roi, &pads, m_verbose);
	} else {
		ret = RunNetwork(interpreter, img, size, chn, m_type, roi, NULL, m_verbose);
	}

	if (ret)
		return -1;

	if (m_output_info_obtained == 0) {
		int ret = GetOutputInfo();
		if (ret)
			return -1;
		m_output_info_obtained = 1;
	}

	if (m_verbose) TIC(start);

	for (int i = 0; i < m_feature_output_pairs; i++) {
		const int conf_index = m_output_prob_idx[i];
		const int reg_index = m_output_reg_idx[i];
		const Tbuffer *conf_data_ptr = interpreter->typed_tensor<Tbuffer>(outputs[conf_index]);
		const Tbuffer *reg_data_ptr = interpreter->typed_tensor<Tbuffer>(outputs[reg_index]);
		const QuantInfo& conf_info = m_qinfo[conf_index];
		const QuantInfo& reg_info = m_qinfo[reg_index];

		std::vector<FaceBox> face_list_local;

		ScrfdPostProcessConfig conf = {};
		conf.num_anchors = m_num_anchors_per_feature;
		conf.feature_stride = m_feature_stride[i];
		conf.number_output = m_output_dim[conf_index][1];
		conf.conf_thresh = m_config->conf_thresh.data[0];
		conf.iou_thresh = m_config->iou_thresh;
		conf.input.h = m_input_dim[0];
		conf.input.w = m_input_dim[1];

		ScrfdPostProcess(conf_data_ptr, reg_data_ptr, conf_info, reg_info, conf, face_list_local);

		if (face_list_local.size())
			feature_list.insert(feature_list.end(), face_list_local.begin(), face_list_local.end());
	}

	NmsBoxes(feature_list, m_config->iou_thresh, NMS_UNION, face_list);

	if (roi) {
		for (auto& box : face_list) {
			box.x0 = box.x0 * scale_factor.w + roi->sx;
			box.y0 = box.y0 * scale_factor.h + roi->sy;
			box.x1 = box.x1 * scale_factor.w + roi->sx;
			box.y1 = box.y1 * scale_factor.h + roi->sy;
		}
	} else {
		for (auto& box : face_list) {
			box.x0 = box.x0 * scale_factor.w;
			box.y0 = box.y0 * scale_factor.h;
			box.x1 = box.x1 * scale_factor.w;
			box.y1 = box.y1 * scale_factor.h;
		}
	}

	if (m_verbose) {
		char msg[64] = {};
		sprintf(msg, "PostProcess (#%d detections)", (int)face_list.size());
		TOC(msg, start);
	}

	// Posprocessing
	/*
	ScrfdPostProcessConfig conf{
	int m_feature_output_pairs = 0; // fmc
	int m_num_anchors_per_feature = 0; // num_anchors
	int m_use_kps = 0; // use_kps
	int m_feature_stride[5]; // feat_stride_fpn
	}
	
	_PostProcess(a, )
	*/
	return 0;
}

int LiteScrfd::FaceDetect(const InfImage* img, std::vector<FaceBox>& face_list)
{
	int ret = 0;
	if (m_type == Inf8U) {
		ret = TRunNet<uint8_t>(*img, nullptr, face_list);
	} else if (m_type == Inf8S) {
		ret = TRunNet<int8_t>(*img, nullptr, face_list);
	} else if (m_type == Inf32F) {
		ret = TRunNet<float>(*img, nullptr, face_list);
	} else {
		inf_warn("Unsupport datatype for current Scrfd inference!\n");
		return -1;
	}
	return ret;
}

int LiteScrfd::FaceDetect(const InfImage* img, const MPI_RECT_POINT_S& roi, std::vector<FaceBox>& face_list)
{
	int ret = 0;
	if (m_type == Inf8U) {
		ret = TRunNet<uint8_t>(*img, &roi, face_list);
	} else if (m_type == Inf8S) {
		ret = TRunNet<int8_t>(*img, &roi, face_list);
	} else if (m_type == Inf32F) {
		ret = TRunNet<float>(*img, &roi, face_list);
	} else {
		inf_warn("Unsupport datatype for current Scrfd inference!\n");
		return -1;
	}
	return ret;
}

int LiteScrfd::FaceDetect(const InfImage* img, const MPI_IVA_OBJ_LIST_S* obj_list, std::vector<FaceBox>& face_list)
{
#define PAD_PIX_SIZE (32)

	MPI_RECT_POINT_S group_roi = {};
	MPI_RECT_POINT_S input_roi = {};
	const Shape img_shape{img->w, img->h};
	const Shape input{m_input_dim[1], m_input_dim[0]};

	if (!img || !obj_list) {
		inf_warn("input cannot be NULL pointer!\n");
		return 0;
	}

	if (!obj_list->obj_num)
		return 0;

	Grouping(obj_list, group_roi);

	PadAndRescale(PAD_PIX_SIZE, img_shape, input, group_roi, input_roi);

	FaceDetect(img, input_roi, face_list);

	return 0;

#undef PAD_PIX_SIZE
}

int LiteScrfd::Detect(const InfImage* img, InfDetList* result)	
{
	std::vector<FaceBox> face_list;
	int ret = FaceDetect(img, face_list);
	TransformResult(face_list, result, 1.0f, m_config->labels.size);
	return ret;
}

int LiteScrfd::Detect(const InfImage* img, const MPI_IVA_OBJ_LIST_S* obj_list, InfDetList* result)
{
	std::vector<FaceBox> face_list;
	int ret = FaceDetect(img, obj_list, face_list);
	TransformResult(face_list, result, 1.0f, m_config->labels.size);
	return ret;
}

#endif // USE_TFLITE
