#ifdef USE_TFLITE

/**
* @cond
*/

#include <stdio.h>
#include <string.h>
#include <time.h>

#include "tensorflow/lite/interpreter.h"
#include "tensorflow/lite/kernels/register.h"
#include "tensorflow/lite/model.h"

#include "inf_types.h"
#include "inf_classifier.h"
#include "inf_image.h"
#include "inf_model.h"

#include "inf_model.h"
#include "inf_trc.h"
#include "inf_utils.h"
#include "inf_utils_lite.h"
#include "lite_classifier.h"

LiteClassifier::LiteClassifier(InfModelInfo *info)
{
	m_config = info;
}

LiteClassifier::~LiteClassifier()
{
	ReleaseConfig(m_config);
	delete m_config;
	m_config = nullptr;
}


int LiteClassifier::LoadModels(const char *model_path)
{
	// 1. Build the interpreter

	if (utils::lite::LoadModels(model_path, m_model, m_model_fb)) {
		inf_err("Cannot find %s for %s!\n", model_path, __func__);
		return -1;
	}

	// 2. Fetch model info
	std::vector<int> outputs = m_model->outputs();
	if (outputs.size() != 1) {
		inf_err("number of output is not correct!\n");
		return -1;
	}

	// 2.1 Fetch Quant info
	const TfLiteQuantizationParams *p = &m_model->tensor(outputs[0])->params;
	m_config->quant.zero = p->zero_point;
	m_config->quant.scale = p->scale;

	const int output_idx = m_model->outputs()[0];
	const int input_idx = m_model->inputs()[0];
	const TfLiteIntArray *outdims = m_model->tensor(output_idx)->dims;

	m_output_dim[0] = outdims->data[0];
	m_output_dim[1] = outdims->data[1];

	const TfLiteIntArray *indims = m_model->tensor(input_idx)->dims;
	m_input_dim[0] = indims->data[1];
	m_input_dim[1] = indims->data[2];
	m_input_dim[2] = indims->data[3];

	m_config->dtype = (InfDataType)utils::lite::GetDataType(m_model.get(), input_idx);

	if (m_config->dtype == -1) {
		inf_err("Unknown model Datatype for %s!\n", model_path);
		return -1;
	}

	inf_info_h("tflite model input dim is %dx%dx%d type is %s\n",
		m_input_dim[0], m_input_dim[1], m_input_dim[2], GetDTypeString(m_config->dtype));
	inf_info_h("tflite model number of output is %dx%d\n", m_output_dim[0], m_output_dim[1]);

	m_model->SetNumThreads(m_num_thread);

	if (m_model->AllocateTensors() != kTfLiteOk) {
		inf_err("Cannot allocate tensor!\n");
		return -1;
	}

	return 0;
}

int LiteClassifier::Classify(const InfImage *img, const MPI_IVA_OBJ_LIST_S *obj_list, InfResultList *result)
{
	int i;
	tflite::Interpreter *interpreter = m_model.get();

	const int input = interpreter->inputs()[0];
	const int output_idx = interpreter->outputs()[0];

	const int input_h = m_input_dim[0];
	const int input_w = m_input_dim[1];
	const int input_chn = m_input_dim[2];
	const int output_dim = m_output_dim[1];
	const int resize_aspect_ratio = m_config->resize_aspect_ratio;
	const float conf_thresh = m_config->conf_thresh.data[0];

	result->size = obj_list->obj_num;
	result->data = (InfResult*) calloc(result->size, sizeof(InfResult));

	InfResult *single_result = nullptr;
	InfImage wimg;

	wimg.w = m_input_dim[1];
	wimg.h = m_input_dim[0];
	wimg.c = m_input_dim[2];
	wimg.buf_owner = 0;

	uint8_t *input_addr = nullptr;
	uint8_t *output_addr = nullptr;;

	if (m_config->dtype == Inf8U) {
		input_addr = interpreter->typed_tensor<uint8_t>(input);
		output_addr = interpreter->typed_tensor<uint8_t>(output_idx);

		wimg.dtype = (InfDataType)GetImageType((int)Inf8U, input_chn);
		wimg.data = (uint8_t*)input_addr;

	} else if (m_config->dtype == Inf8S) {
		input_addr = (uint8_t*)interpreter->typed_tensor<int8_t>(input);
		output_addr = (uint8_t*)interpreter->typed_tensor<int8_t>(output_idx);

		wimg.dtype = (InfDataType)GetImageType((int)Inf8S, input_chn);
		wimg.data = input_addr;
	}

	if (m_config->dtype == Inf8U || m_config->dtype == Inf8S) {
		
		std::vector<float> output_buf(output_dim, 0.0);

		for (i = 0; i < obj_list->obj_num; ++i) {

			single_result = &result->data[i];

			auto &obj = obj_list->obj[i];
			auto &box = obj.rect;

			if (m_verbose)
				TIC(start);

			if (resize_aspect_ratio)
				Inf_ImcropResizeAspectRatio(img, box.sx, box.sy, box.ex, box.ey, &wimg, input_w, input_h);
			else
				Inf_ImcropResize(img, box.sx, box.sy, box.ex, box.ey, &wimg, input_w, input_h);

			if (m_verbose)
				TOC("Preprocess WImage for obj", start);

			if (m_verbose)
				TIC(start);

			if (interpreter->Invoke() != kTfLiteOk) {
				inf_warn("Cannot Invoke tflite model!\n");
				return 0;
			}

			if (m_verbose)
				TOC("Inference Call", start);

			if (m_config->dtype == Inf8U) {
				for (int j = 0; j < output_dim; j++)
					output_buf[j] = QuantConvert(m_config->quant, output_addr[j]);
			} else {
				for (int j = 0; j < output_dim; j++)
					output_buf[j] = QuantConvert(m_config->quant, ((int8_t*)output_addr)[j]);
			}

            if (m_verbose)
				PrintResult(output_dim, output_buf.data());

			if (m_config->output_type == InfSigmoid)
				Sigmoid(output_buf.data(), m_config->labels.size);
			else if (m_config->output_type == InfSoftmax)
				Softmax(output_buf.data(), m_config->labels.size);

			if (m_debug) {
				char snapshot_img_name[256] = {};
				sprintf(snapshot_img_name, SNAPSHOT_FORMAT, m_snapshot_prefix,
					output_buf[0] >= conf_thresh ? "pos" : "neg",
					m_snapshot_cnt,
					output_buf[0],
					wimg.c == 1 ? "pgm" : "ppm");
				Inf_Imwrite(snapshot_img_name, &wimg);
				m_snapshot_cnt++;
			}

			PostProcess(output_buf.data(), m_config->labels.size,
				conf_thresh, m_config->topk,
				&m_config->filter_cls, &m_config->filter_out_cls, single_result);

			single_result->id = obj.id;
		}
	} else {
		inf_warn("Currently not support floating point inference!\n");
	}
	return 0;
}

/**
 * @endcond
 */

/**
 * @brief Invoke classification model with image.
 * @details
 * @param[in] ctx              model context
 * @param[in] img              image input
 * @param[in] obj_list         obj list w.r.t current image
 * @param[out] result          classification result
 * @retval 0                   success
 * @retval -EFAULT             input variables are null.
 * @see Inf_InitModel()
 */
int Inf_InvokeClassify(InfModelCtx *ctx, const InfImage *img, const MPI_IVA_OBJ_LIST_S *obj_list, InfResultList *result)
{
	CHECKPTR(!ctx || !ctx->model || !img || !obj_list || !result, "input pointer cannot be null!\n", -EINVAL);
	if (ctx->info->inference_type != InfRunClassify) {
		inf_warn("Incompatible model type (%s) inference %s!\n", GetInfTypeStr(ctx->info->inference_type), __func__);
		return 0;
	}

	InfModel *inf_model = (InfModel*)ctx->model;
	return inf_model->Classify(img, obj_list, result);
}

#endif // USE_TFLITE
