/**
* @cond
*/

#include <errno.h>
#include <libgen.h>

#include "inf_classifier.h"
#include "inf_face.h"

#include "inf_trc.h"
#include "inf_utils.h"
#include "lite_classifier.h"
#include "lite_faceencode.h"
#include "lite_mtcnn.h"
#include "lite_scrfd.h"

InfModel *InfModelFactory::Create(const char* model_config)
{
	InfModelInfo *config = new InfModelInfo{};
	ParseModelConfig(model_config, config);

	InfModel *model = nullptr;

	if (!config->model_paths.size) {
		inf_warn("No model path provided in the config \"%s\"\n", config->config_path);
		ReleaseConfig(config);
		delete config;
		return nullptr;
	}

	switch (config->inference_type) {
		case InfRunClassify: {
			model = InfModelFactory::CreateClassify(config);
			break;
		} case InfRunDetect: {
			if (!strcmp(config->model_name, INF_FACEDET_MTCNN_NAME) ||
				!strcmp(config->model_name, INF_FACEDET_SCRFD_NAME)) {
				model = InfModelFactory::CreateFaceDet(config);
			}
			break;
		} case InfRunFaceEncode: {
			model = InfModelFactory::CreateFaceEncode(config);
			break;
		} case InfRunFaceReco: {
			model = InfModelFactory::CreateFaceReco(config);
			break;
		} default: {
			inf_err("Cannot create \"%s\" model from config: \"%s\"!\n",
				GetInfTypeStr(config->inference_type), model_config);
			break;
		}
	}
	if (!model) {
		ReleaseConfig(config);
		delete config;
		return model;
	}

	if (model->m_config->dtype != Inf8U && model->m_config->dtype != Inf8S) {
		inf_err("%s only supports %s, %s datatype!\n", __func__,
			GetDTypeString(Inf8U), GetDTypeString(Inf8S));
		delete model;
		model = nullptr;
	}

	return model;
}

InfModel *InfModelFactory::CreateClassify(InfModelInfo *info)
{
	LiteClassifier *classify = new LiteClassifier(info);
	InfModel *model = nullptr;

	int ret = classify->LoadModels(info->model_paths.data[0]);
	if (ret) {
		delete classify;
	} else {
		model = dynamic_cast<InfModel*>(classify);
		if (!model) {
			delete classify;
		}
	}
	return model;
}

/***@brief create face detection model
  **@param[in/out] ctx     face detection context info.
  **@param[in] config      face detection config path.
  **@retval 0              initialize model failure.
 **/
InfModel *InfModelFactory::CreateFaceDet(InfModelInfo *info)
{
	char model_dir_str[INF_MODEL_PATH_LEN]{};
	strncpy(model_dir_str, info->config_path, INF_MODEL_PATH_LEN-1);

	InfModel *model = nullptr;

	char *model_dir = dirname(model_dir_str);

	if (!strcmp(info->model_name, INF_FACEDET_MTCNN_NAME)) {
		LiteMtcnn *lmtcnn = new LiteMtcnn();
		lmtcnn->m_config = info;
		lmtcnn->SetNumThreads(info->num_threads);
		lmtcnn->SetVerbose(info->verbose);
		lmtcnn->SetDebug(info->debug);
		lmtcnn->SetupConfig(info);
		int ret = lmtcnn->LoadModels(model_dir, &info->model_paths);
		if (ret) {
			delete lmtcnn;
			inf_warn("Cannot initalize mtcnn!\n");
			return nullptr;
		}
		model = dynamic_cast<InfModel*>(lmtcnn);
		if (!model) {
			delete lmtcnn;
			inf_warn("Cannot upcast mtcnn!\n");
			return nullptr;
		}
	} else if (!strcmp(info->model_name, INF_FACEDET_SCRFD_NAME)) {
		LiteScrfd *lscrfd = new LiteScrfd();
		lscrfd->SetupConfig(info);
		int ret = lscrfd->LoadModels(model_dir, &info->model_paths);
		if (ret) {
			delete lscrfd;
			inf_warn("Cannot initalize mtcnn!\n");
			return nullptr;
		}
		model = dynamic_cast<InfModel*>(lscrfd);
		if (!model) {
			delete lscrfd;
			inf_warn("Cannot upcast lscrfd!\n");
			return nullptr;
		}
	} else {
		inf_err("Unknown Face detection model \"%s\"!\n", info->model_name);
		return nullptr;
	}

	return model;
}


/***@brief init face_encode model
  **@param[in] config      face_encode config path.
  **@retval nullptr              Cannot create model!
 **/
InfModel *InfModelFactory::CreateFaceEncode(InfModelInfo *info)
{
	InfModel *model = nullptr;

	char *model_dir = dirname(info->config_path);
	char model_path[256] = {};
	snprintf(model_path, 255, "%s/%s", model_dir, info->model_paths.data[0]);

	LiteFaceEncode *face_encode = new LiteFaceEncode(info);
	int ret = face_encode->LoadModels(model_path);
	if (ret) {
		delete face_encode;
		return nullptr;
	} else {
		model = dynamic_cast<InfModel*>(face_encode);
		if (!model) {
			delete face_encode;
			return nullptr;
		}
	}
	model->m_config = info;
	return model;
}

/***@brief init face_encode model
  **@param[in] config      face_encode config path.
  **@retval nullptr              Cannot create model!
 **/
InfModel *InfModelFactory::CreateFaceReco(InfModelInfo *info)
{
	InfFaceReco *face_reco = nullptr;
	InfFaceDetect *face_det = nullptr;
	InfFaceEncode *face_enc = nullptr;

	char model_config[256] = {};
	char *model_dir = nullptr;

	if (info->model_paths.size != 2) {
		return nullptr;
	}

	model_dir = dirname(info->config_path);

	if (strcmp(info->model_paths.data[0], "null")) {
		size_t size = sprintf(model_config, "%s/%s", model_dir, info->model_paths.data[0]);
		model_config[size] = 0;
		InfModel *model = InfModelFactory::Create(model_config);
		if (model) {
			face_det = dynamic_cast<InfFaceDetect*>(model);
		}
	}

	if (strcmp(info->model_paths.data[1], "null")) {
		size_t size = sprintf(model_config, "%s/%s", model_dir, info->model_paths.data[1]);
		model_config[size] = 0;
		InfModel *model = InfModelFactory::Create(model_config);
		if (model) {
			face_enc = dynamic_cast<InfFaceEncode*>(model);
		}
	}

	if (face_det || face_enc) {
		face_reco = new InfFaceReco(info);
		face_reco->LoadModels(face_det, face_enc);
		face_reco->m_config = info;
	}
	return face_reco;
}

/**
 * @endcond
 */


/**
 * @brief Initialize model resource
 * @details
 * @param[in/out] ctx          empty model context to be initialized
 * @retval 0                   success
 * @retval -EFAULT             input pointers are null
 * @see Inf_InitModel()
 */
int Inf_InitModel(InfModelCtx *ctx, const char* model_config)
{
	CHECKPTR(!ctx || !model_config, "input pointer cannot be null!\n", -EFAULT);
	int ret = 0;
	InfModel *model = InfModelFactory::Create(model_config);
	if (model) {
		ctx->info = model->m_config;
		ctx->model = (void*) model;
	}
	return ret;
}

/**
 * @brief release model resource
 * @details
 * @param[in] ctx              model context
 * @retval 0                   success
 * @retval -EFAULT             input pointers are null
 * @see Inf_InitModel()
 */
int Inf_ReleaseModel(InfModelCtx *ctx)
{
	CHECKPTR(!ctx || !ctx->model, "input pointer cannot be null!\n", -EFAULT);

	delete (InfModel*)ctx->model;
	ctx->model = nullptr;
	ctx->info = nullptr;
	return 0;
}

/**
 * @brief Setup model verbse, debug level, num thread
 * @details
 * @param[in] ctx                  model context
 * @param[in] verbose              verbose level
 * @param[in] debug                debug level
 * @param[in] num_thread           number of thread(experimental)
 * @retval 0                       success
 * @retval -EFAULT                 input pointers are null
 * @see Inf_ReleaseModel()
 */
int Inf_Setup(InfModelCtx *ctx, int verbose, int debug, int num_thread)
{
	CHECKPTR(!ctx || !ctx->model, "input pointer cannot be null!\n", -EFAULT);

	const InfModelInfo *config = ctx->info;
	InfModel* model = static_cast<InfModel*>(ctx->model);
	model->m_verbose = verbose;
	model->m_debug = debug;
	model->m_num_thread = num_thread;

	switch (config->inference_type) {
		case InfRunDetect: {
			if (!strcmp(config->model_name, INF_FACEDET_MTCNN_NAME)) {
				LiteMtcnn *lmtcnn = dynamic_cast<LiteMtcnn*>(model);
				if (!lmtcnn) {
					inf_err("Cannot cast mtcnn from face detection context please check input args!\n");
					return 0;
				} 
				lmtcnn->SetVerbose(verbose);
				lmtcnn->SetDebug(debug);
				lmtcnn->SetModelThreads(num_thread);
			}
			break;
		} 
		case InfRunClassify:
		case InfRunFaceEncode: {
			SetupDebugTool(model->m_snapshot_prefix);
			break;
		}
		case InfRunFaceReco: {
			SetupDebugTool(model->m_snapshot_prefix);
			if (debug)
				model->SetDebug(debug);
		 	break;
		} default: {
			inf_err("Cannot Setup \"%s\" model!\n",
				GetInfTypeStr(config->inference_type));
			break;
		}
	}
	return 0;
}

/**
 * @brief Release detections result.
 * @details
 * @param[in/out] result       Detections result
 * @retval 0                   success
 * @see Inf_ReleaseResult()
 */
int Inf_ReleaseDetResult(InfDetList *result)
{
	CHECKPTR(!result, "input pointer cannot be null!\n", -EFAULT);

	if (!result->size)
		return 0;

	for (int i = 0; i < result->size; i++)
	{
		auto& obj = result->data[i];
		free(obj.cls);
		free(obj.prob);
	}
	free(result->data);
	result->data = nullptr;
	result->size = 0;
	return 0;
}

/**
 * @brief Release classification result.
 * @details
 * @param[in/out] result       classification result
 * @retval 0                   success
 * @see Inf_ReleaseDetResult()
 */
int Inf_ReleaseResult(InfResultList *result)
{
	if (!result->size)
		return 0;

	for (int i = 0; i < result->size; i++)
	{
		auto& obj = result->data[i];
		free(obj.cls);
		free(obj.prob);
	}
	free(result->data);
	result->data = nullptr;
	result->size = 0;
	return 0;
}
