mod := $(notdir $(subdir))

app-$(CONFIG_APP_AV_MAIN) += av_main
PHONY += av_main av_main-clean av_main-distclean
PHONY += av_main-install av_main-uninstall
av_main: libado libavftr libeaif libinf libcm libprio libtz
	$(Q)$(MAKE) -C $(AVMAIN_PATH) all

av_main-clean: libtz-clean libprio-clean libcm-clean \
	libavftr-clean libado-clean libeaif-clean libinf-clean
	$(Q)$(MAKE) -C $(AVMAIN_PATH) clean

av_main-distclean: libtz-distclean libprio-distclean libcm-distclean \
	libavftr-distclean libado-distclean libeaif-distclean libinf-distclean
	$(Q)$(MAKE) -C $(AVMAIN_PATH) distclean

av_main-install: libado-install libavftr-install libcm-install \
	libprio-install libtz-install libeaif-install libinf-install
	$(Q)$(MAKE) -C $(AVMAIN_PATH) install

av_main-uninstall: libtz-uninstall libprio-uninstall \
	libcm-uninstall libavftr-uninstall libado-uninstall libeaif-uninstall \
	libinf-uninstall
	$(Q)$(MAKE) -C $(AVMAIN_PATH) uninstall

app-$(CONFIG_APP_AV_MAIN2) += av_main2
PHONY += av_main2 av_main2-clean av_main2-distclean
PHONY += av_main2-install av_main2-uninstall
av_main2: libado libavftr libeaif libinf libcm libprio libtz
	$(Q)$(MAKE) -C $(AVMAIN2_PATH) all

av_main2-clean: libtz-clean libprio-clean libcm-clean \
	libavftr-clean libado-clean libeaif-clean libinf-clean
	$(Q)$(MAKE) -C $(AVMAIN2_PATH) clean

av_main2-distclean: libtz-distclean libprio-distclean libcm-distclean \
	libavftr-distclean libado-distclean libeaif-distclean libinf-distclean
	$(Q)$(MAKE) -C $(AVMAIN2_PATH) distclean

av_main2-install: libado-install libavftr-install libcm-install \
	libprio-install libtz-install libeaif-install libinf-install
	$(Q)$(MAKE) -C $(AVMAIN2_PATH) install

av_main2-uninstall: libtz-uninstall libprio-uninstall \
	libcm-uninstall libavftr-uninstall libado-uninstall \
	libeaif-uninstall libinf-uninstall
	$(Q)$(MAKE) -C $(AVMAIN2_PATH) uninstall

PHONY += $(mod) $(mod)-clean $(mod)-distclean
PHONY += $(mod)-install $(mod)-uninstall
$(mod): $(app-y)
$(mod)-clean: $(addsuffix -clean,$(app-y))
$(mod)-distclean: $(addsuffix -distclean,$(app-y))
$(mod)-install: $(addsuffix -install,$(app-y))
$(mod)-uninstall: $(addsuffix -uninstall,$(app-y))

APP_BUILD_DEPS += $(mod)
APP_CLEAN_DEPS += $(mod)-clean
APP_DISTCLEAN_DEPS += $(mod)-distclean
APP_INTALL_DEPS += $(mod)-install
APP_UNINTALL_DEPS += $(mod)-uninstall
