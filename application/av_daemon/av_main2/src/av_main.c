#include "mtk_common.h"
#include "av_main.h"

#include <pthread.h>
#include <sys/file.h>

#include "cfg_api.h"
#include "vftr_update.h"
#include "rtsp_api.h"
#include "mpi_types.h"
#include "video_td.h"
#include "video_md.h"
#include "video_dip.h"
#include "video_api.h"
#include "avftr_conn.h"
#include "osd.h"
#include "agtx_prio.h"

#include "agtx_iva.h"
#include "agtx_video.h"
#include "agtx_audio.h"
#include "agtx_cmd.h"
#include "agtx_osd.h"
#include "agtx_color_conf.h"

#include "json.h"
#include "cm_sys_info.h"
#include "cm_sys_feature_option.h"
#include "cm_sys_db_info.h"
#include "cm_adv_img_pref.h"
#include "cm_awb_pref.h"
#include "cm_color_conf.h"
#include "cm_dip_ae_conf.h"
#include "cm_dip_awb_conf.h"
#include "cm_dip_cal_conf.h"
#include "cm_dip_csm_conf.h"
#include "cm_dip_ctrl_conf.h"
#include "cm_dip_dbc_conf.h"
#include "cm_dip_dcc_conf.h"
#include "cm_dip_gamma_conf.h"
#include "cm_dip_iso_conf.h"
#include "cm_dip_lsc_conf.h"
#include "cm_dip_nr_conf.h"
#include "cm_dip_pta_conf.h"
#include "cm_dip_roi_conf.h"
#include "cm_dip_shp_conf.h"
#include "cm_dip_te_conf.h"
#include "cm_event_conf.h"
#include "cm_event_param.h"
#include "cm_gpio_conf.h"
#include "cm_local_record_conf.h"
#include "cm_img_pref.h"
#include "cm_iva_od_conf.h"
#include "cm_iva_md_conf.h"
#include "cm_iva_td_conf.h"
#include "cm_iva_aroi_conf.h"
#include "cm_iva_pd_conf.h"
#include "cm_iva_ld_conf.h"
#include "cm_iva_rms_conf.h"
#include "cm_iva_ef_conf.h"
#include "cm_vdbg_conf.h"
#include "cm_video_ptz_conf.h"
#include "cm_iva_shd_conf.h"
#include "cm_iva_eaif_conf.h"
#include "cm_iva_pfm_conf.h"
#include "cm_iva_bm_conf.h"
#include "cm_iaa_lsd_conf.h"
#include "cm_osd_conf.h"
#include "cm_osd_pm_conf.h"
#include "cm_audio_conf.h"
#include "cm_voice_conf.h"
#include "cm_siren_conf.h"
#include "cm_product_option.h"
#include "cm_product_option_list.h"
#include "cm_res_option.h"
#include "cm_venc_option.h"
#include "cm_stitch_conf.h"
#include "cm_video_dev_conf.h"
#include "cm_video_strm_conf.h"
#include "cm_video_layout_conf.h"
#include "cm_video_ldc_conf.h"
#include "cm_panorama_conf.h"
#include "cm_panning_conf.h"
#include "cm_surround_conf.h"
#include "cm_pwm_conf.h"
#include "cm_pir_conf.h"
#include "cm_floodlight_conf.h"
#include "cm_dip_shp_win_conf.h"
#include "cm_dip_nr_win_conf.h"
#include "cm_anti_flicker_conf.h"

#include "sqllib.h"
#include "config.h"
///////////////////////////////////// config

#include <execinfo.h>
#include <assert.h>

#define LOCK_FILE_PATH "/tmp/av_main.lock"
#define AVMAIN_RDY_FILE "/tmp/avmain_ready"
//#define ENABLE_RTSPSERVER

int g_lock_fd = -1;
pthread_mutex_t g_video_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t g_video_cond = PTHREAD_COND_INITIALIZER;
av_main_ctrl g_av_main_ctrl = { 0 };

typedef struct {
	MPI_WIN idx;
	MPI_SIZE_S res;
} AROI_CB_INFO;

static const char *g_iva_data_path = "/usrdata/active_setting/iva/";
static INT32 g_video_init = 0;
static INT32 g_cfg_init = 0;
static CFG_CAMERA_S g_config = { 0 };
pthread_mutex_t g_cfg_mutex = PTHREAD_MUTEX_INITIALIZER;
extern int g_mpi_sys_initialized;

AROI_CB_INFO g_aroi_cb_info = { { { 0 } } };
static CmdHandler cmd_handler[66] = { { 0 } };

int validate_json_string_db(struct json_object **json_obj, char *str, int strlen)
{
	int ret = 0;
	struct json_object *obj = NULL;
	struct json_tokener *tok = json_tokener_new();
	enum json_tokener_error jerr;

	/* Parse the buf */
	if (strlen > 0) {
		obj = json_tokener_parse_ex(tok, str, strlen);
	} else {
		*json_obj = NULL;
		ret = -1;
		goto end;
	}

	jerr = json_tokener_get_error(tok);

	if (jerr == json_tokener_success) {
		*json_obj = obj;
	} else {
		fprintf(stderr, "JSON Parsing errorer %s \n", json_tokener_error_desc(jerr));

		*json_obj = NULL;
		ret = -1;
	}
end:
	json_tokener_free(tok);

	return ret;
}

struct json_object *get_db_record_obj(const char *db_path, const int id)
{
	int ret = 0;
	char *rec = NULL;
	struct json_object *obj = NULL;

	rec = get_sqldata_by_id_str_path(db_path, "json_tbl", "jstr", id);
	if (!rec) {
		fprintf(stderr, "Record (id 0x%08X) is not in DB %s\n", id, db_path);
		goto end;
	}

	ret = validate_json_string_db(&obj, rec, strlen(rec));
	if (ret < 0) {
		fprintf(stderr, "Record (id 0x%08X) is not a valid JSON string\n", id);
		/* obj will be NULL pointer */
	}

	free(rec);
end:
	return obj;
}

static INT32 cfg_apply_vin(CFG_VIN_S *cfg_vin)
{
	INT32 ret = 0;

	/*apply config to mpi dev*/
	do {
		ret = VIDEO_DEV_startVideoDev(cfg_vin);

		if (ret == MPI_FAILURE) {
			VIDEO_DEV_stopVideoDev(cfg_vin);
		}
	} while (ret == MPI_FAILURE);

	return ret;
}

static INT32 cfg_apply_vchn(MPI_DEV idx, CFG_VCHN_S *cfg_vchn, INT32 init)
{
	MPI_CHN chn = MPI_VIDEO_CHN(idx.dev, 0);
	INT32 ret = 0;

	/*apply config to mpi dev*/
	if (init) {
		ret = VIDEO_DEV_startVideoChn(idx, cfg_vchn);
	} else {
		chn.chn = cfg_vchn->chn_idx;
		ret = VIDEO_DEV_setVideoChn(chn, cfg_vchn);
		if (ret < 0) {
			return ret;
		}
		//ret = AVFTR_notifyVideo(MPI_VIDEO_WIN(chn.dev, chn.chn, 0), -1);
	}

	return ret;
}

static INT32 cfg_apply_venc(CFG_VENC_S *cfg_venc, INT32 init)
{
	INT32 ret = 0;
	/*apply config to mpi venc*/
	if (init) {
		ret = VIDEO_ENC_startEncChn(cfg_venc);
	} else {
		ret = VIDEO_ENC_setEncChn(cfg_venc);
	}

	return ret;
}

INT32 cfg_apply_img(AGTX_IMG_PREF_S *img, UINT32 dev_idx)
{
	INT32 ret = 0;
	MPI_DEV dev = MPI_VIDEO_DEV(dev_idx);

	ret = VIDEO_DIP_setBrightness(dev, &img->brightness);
	if (ret < 0) {
		return ret;
	}

	ret = VIDEO_DIP_setContrast(dev, &img->contrast);
	if (ret < 0) {
		return ret;
	}

	ret = VIDEO_DIP_setSharpness(dev, &img->sharpness);
	if (ret < 0) {
		return ret;
	}

	ret = VIDEO_DIP_setSaturation(dev, &img->saturation);
	if (ret < 0) {
		return ret;
	}

	ret = VIDEO_DIP_setHue(dev, &img->hue);
	if (ret < 0) {
		return ret;
	}

	ret = VIDEO_DIP_setAntiFlicker(dev, &img->anti_flicker);
	if (ret < 0) {
		return ret;
	}

	return ret;
}

static INT32 cfg_apply_osd_pm(UINT32 dev_idx, UINT32 chn_idx, UINT16 width, UINT16 height, AGTX_OSD_PM_PARAM_S *osd_pm,
                              AGTX_OSD_PM_PARAM_S *osd_pm_old)
{
	INT32 ret = 0;
	INT32 i;
	AGTX_OSD_PM_S *new_osd_param = NULL;
	VIDEO_OSD_PM_PARAM_S osd_param = { 0 };
	MPI_ECHN idx = MPI_ENC_CHN(chn_idx);
	/* Disable Privacy Mask first */
	for (i = VIDEO_OSD_PM_HANDLE_OFFSET; i < VIDEO_OSD_MAX_HANDLE_CHANNEL; i++) {
		/* Use fake unbind here */
		ret = VIDEO_OSD_setBindState(idx, i, 0);
	}

	/* Enable Privacy Mask */
	for (i = 0; i < VIDEO_OSD_MAX_PM_HANDLE; i++) {
		new_osd_param = &osd_pm->param[i];

		if (osd_pm_old->param[i].enabled || new_osd_param->enabled) {
			osd_param.enabled = new_osd_param->enabled;
			if (new_osd_param->enabled) {
				osd_param.alpha = new_osd_param->alpha;
				osd_param.color = VIDEO_OSD_getAyuvColor(new_osd_param->color);
				osd_param.start.x = new_osd_param->start_x;
				osd_param.start.y = new_osd_param->start_y;
				osd_param.end.x = new_osd_param->end_x;
				osd_param.end.y = new_osd_param->end_y;
			} else { /* reset disabled param to conf */
				new_osd_param->enabled = 0;
				new_osd_param->color = 0;
				new_osd_param->alpha = 7;
				new_osd_param->start_x = 0;
				new_osd_param->start_y = 0;
				new_osd_param->end_x = 1;
				new_osd_param->end_y = 1;
				osd_param.enabled = 0;
				osd_param.alpha = 7;
				osd_param.color = VIDEO_OSD_getAyuvColor(new_osd_param->color);
				osd_param.start.x = 0;
				osd_param.start.y = 0;
				osd_param.end.x = 16;
				osd_param.end.y = 16;
			}
			ret = VIDEO_OSD_setPmParam(idx, i, &osd_param);

			if (ret) {
				return -1;
			}
		}
	}
	return 0;
}

static INT32 cfgInitSys()
{
	INT32 i = 0;
	INT32 ret = 0;
	CFG_CAMERA_S *cfg = &g_config;
	/*apply config to mpi sys*/
	ret = VIDEO_SYS_initSystem(cfg);
	if (ret < 0) {
		return ret;
	}
	for (i = 0; i < MAX_VIDEO_INPUT; i++) {
		// TODO: check if camera enable
		if (cfg[i].cfg_vin.enable) {
			ret = VIDEO_DEV_createVideoDev(&cfg[i].cfg_vin);
			if (ret < 0) {
				return ret;
			}
		} else {
			continue;
		}
	}

	return ret;
}

static INT32 cfgStartVideo()
{
	INT32 i = 0;
	INT32 j = 0;
	INT32 ret = 0;
	MPI_DEV dev_idx = MPI_VIDEO_DEV(0);
	CFG_CAMERA_S *cfg = &g_config;

	for (i = 0; i < MAX_VIDEO_INPUT; i++) {
		// TODO: check if camera enable
		if (cfg[i].cfg_vin.enable) {
			ret = cfg_apply_vin(&cfg[i].cfg_vin);
			if (ret < 0) {
				return ret;
			}
		} else {
			continue;
		}

		dev_idx.dev = cfg[i].cfg_vin.cfg_dev.video_dev_idx;

		ret = cfg_apply_vchn(dev_idx, cfg[i].cfg_vin.cfg_vchn, 1);
		if (ret < 0) {
			return ret;
		}

		for (j = 0; j < MAX_VENC_STREAM; j++) {
			if (cfg[i].cfg_venc[j].enable) {
				ret = cfg_apply_venc(&cfg[i].cfg_venc[j], 1);
				if (ret < 0) {
					return ret;
				}
			}
		}

		//set the default and GUI of parameters to DIP
		ret = VIDEO_DIP_setDipAllAttr(dev_idx);
		if (ret < 0) {
			return ret;
		}
		//		cfg_apply_enc_timestamp(cfg->cfg_camera[i].cfg_venc, &cfg->cfg_camera[i].cfg_timestamp);
	}

	return 0;
}

static INT32 cfgExitSys()
{
	INT32 i = 0;
	INT32 ret = 0;
	CFG_CAMERA_S *cfg = &g_config;
	for (i = 0; i < MAX_VIDEO_INPUT; i++) {
		// TODO: check if camera enable
		if (cfg[i].cfg_vin.enable) {
			ret = VIDEO_DEV_destroyVideoDev(&cfg[i].cfg_vin);
			if (ret < 0) {
				return ret;
			}
		} else {
			continue;
		}
	}
	ret = VIDEO_SYS_exitSystem();
	return ret;
}

static INT32 cfgStopVideo()
{
	INT32 i = 0;
	INT32 j = 0;
	INT32 ret = 0;
	MPI_DEV dev_idx = MPI_VIDEO_DEV(0);
	CFG_CAMERA_S *cfg = &g_config;

	for (i = 0; i < MAX_VIDEO_INPUT; i++) {
		// TODO: check if camera enable

		if (cfg[i].cfg_vin.enable) {
			for (j = 0; j < MAX_VENC_STREAM; j++) {
				if (cfg[i].cfg_venc[j].enable) {
					ret = VIDEO_ENC_stopEncChn(&cfg[i].cfg_venc[j]);
					if (ret < 0) {
						return ret;
					}
				}
			}

			dev_idx.dev = cfg[i].cfg_vin.cfg_dev.video_dev_idx;

			ret = VIDEO_DEV_stopVideoChn(dev_idx, cfg[i].cfg_vin.cfg_vchn);
			if (ret < 0) {
				return ret;
			}
			ret = VIDEO_DEV_stopVideoDev(&cfg[i].cfg_vin);
			if (ret < 0) {
				return ret;
			}
		} else {
			continue;
		}
	}

	return 0;
}

static INT32 cfg_set_vin(CFG_ATTR_S *attr, void *data, INT32 size)
{
	CFG_CAMERA_S *cfg = &g_config;
	CFG_VIN_S *cfg_vin = NULL;

	if (size != sizeof(CFG_VIN_S)) {
		SYS_TRACE("CFG_SET_CONFIG_VIN size %d / %d dismatch\n", size, sizeof(CFG_VIN_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	/*cfg doesn't initial,only save value*/
	if (!g_video_init) {
		cfg_vin = &cfg[attr->group].cfg_vin;
		memcpy(cfg_vin, data, sizeof(CFG_VIN_S));
		return 0;
	}

	return 0;
}

static INT32 cfg_set_vchn(CFG_ATTR_S *attr, void *data, INT32 size)
{
	INT32 ret = 0;
	MPI_DEV dev_idx = MPI_VIDEO_DEV(attr->group);
	CFG_CAMERA_S *cfg = &g_config;
	CFG_VCHN_S *cfg_chn = NULL;
	CFG_VCHN_S *cfg_chn_old = NULL;

	if (size != sizeof(CFG_VCHN_S)) {
		SYS_TRACE("CFG_SET_CONFIG_VCHN size %d / %d dismatch\n", size, sizeof(CFG_VCHN_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	/*cfg doesn't initial,only save value*/
	cfg_chn = &cfg[attr->group].cfg_vin.cfg_vchn[attr->id];
	if (!g_video_init || !cfg_chn->enable) {
		memcpy(cfg_chn, data, sizeof(CFG_VCHN_S));
		return 0;
	}

	cfg_chn = (CFG_VCHN_S *)data;
	cfg_chn_old = &cfg[attr->group].cfg_vin.cfg_vchn[attr->id];

	ret = cfg_apply_vchn(dev_idx, cfg_chn, 0);
	if (ret < 0) {
		return ret;
	}
	memcpy(cfg_chn_old, cfg_chn, sizeof(CFG_VCHN_S));

	return ret;
}

static INT32 cfg_set_venc(CFG_ATTR_S *attr, void *data, INT32 size)
{
	INT32 ret = 0;
	MPI_DEV dev_idx = MPI_VIDEO_DEV(attr->group);
	CFG_CAMERA_S *cfg = &g_config;
	CFG_VCHN_S *cfg_vchn = NULL;
	CFG_VENC_S *cfg_venc = NULL;
	CFG_VENC_S *cfg_venc_old = NULL;

	if (size != sizeof(CFG_VENC_S)) {
		SYS_TRACE("CFG_SET_CONFIG_VENC size %d / %d dismatch\n", size, sizeof(CFG_VENC_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	/*cfg doesn't initial,only save value*/
	cfg_venc = &cfg[attr->group].cfg_venc[attr->id];
	if (!g_video_init || !cfg_venc->enable) {
		memcpy(cfg_venc, data, sizeof(CFG_VENC_S));
		return 0;
	}

	cfg_venc = (CFG_VENC_S *)data;
	cfg_venc_old = &cfg[attr->group].cfg_venc[attr->id];
	if ((cfg_venc->strm.width != cfg_venc_old->strm.width) ||
	    (cfg_venc->strm.height != cfg_venc_old->strm.height)) {
		memcpy(&cfg[attr->group].cfg_venc[attr->id], cfg_venc, sizeof(CFG_VENC_S));
		cfg_vchn = &cfg[attr->group].cfg_vin.cfg_vchn[attr->id];
		cfg_vchn->max_width = cfg_venc->max_width;
		cfg_vchn->max_height = cfg_venc->max_height;
		cfg_vchn->strm.width = cfg_venc->strm.width;
		cfg_vchn->strm.height = cfg_venc->strm.height;
		cfg_vchn->strm.output_fps = cfg_venc->strm.output_fps;
		cfgStopVideo();
		cfgStartVideo();
	} else {
		if (cfg_venc_old->strm.output_fps != cfg_venc->strm.output_fps) {
			cfg[attr->group].cfg_vin.cfg_vchn[attr->id].strm.output_fps = cfg_venc->strm.output_fps;
			ret = cfg_apply_vchn(dev_idx, &cfg[attr->group].cfg_vin.cfg_vchn[attr->id], 0);
			if (ret < 0) {
				return ret;
			}
#if 0
			if (cfg[attr->group].cfg_vin.cfg_vchn[attr->id].od.enabled) {
				ret = cfg_apply_od(attr->group, attr->id,
				                   &cfg[attr->group].cfg_vin.cfg_vchn[attr->id].od, 0,
				                   cfg[attr->group].cfg_vin.cfg_vchn[attr->id].strm.output_fps);
				if (ret < 0) {
					return ret;
				}
			}
#endif
		}

		ret = cfg_apply_venc(cfg_venc, 0);
		memcpy(&cfg[attr->group].cfg_venc[attr->id], cfg_venc, sizeof(CFG_VENC_S));
	}

	return ret;
}

int init_osd = 0;

static INT32 cfg_set_osd(CFG_ATTR_S *attr, void *data)
{
	CFG_CAMERA_S *cfg = &g_config;
	CFG_VCHN_S *conf = NULL;
	AGTX_OSD_CONF_OUTER_S *cur = NULL;
	AGTX_OSD_CONF_OUTER_S *new = NULL;
	AGTX_OSD_CONF_S *cfg_osd = NULL;
	INT32 ret = 0;
	MPI_ECHN chn_idx = { { 0 } };

	if (data == NULL) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	cfg_osd = (AGTX_OSD_CONF_S *)data;
	if (init_osd) {
		for (int i = 0; i < MAX_AGTX_OSD_CONF_OUTER_S_REGION_SIZE; i++) {
			conf = &cfg[0].cfg_vin.cfg_vchn[i];
			if (!conf->enable)
				continue;
			chn_idx = MPI_ENC_CHN(conf->chn_idx);
			cur = (AGTX_OSD_CONF_OUTER_S *)&conf->osd;
			new = (AGTX_OSD_CONF_OUTER_S *)&cfg_osd->strm[i];
			ret |= VIDEO_OSD_checkParam(chn_idx, cur, new);
		}
		if (ret == VIDEO_OSD_ERR_PARAM)
			return -1;
		else if (ret == VIDEO_OSD_RESTREAM_REQUEST) {
		} else {
			VIDEO_OSD_setShowWeekDay(cfg_osd->showWeekDay);
			/* Directly set the region */
			for (int i = 0; i < MAX_AGTX_OSD_CONF_OUTER_S_REGION_SIZE; i++) {
				conf = &cfg[0].cfg_vin.cfg_vchn[i];
				if (!conf->enable)
					continue;
				cur = (AGTX_OSD_CONF_OUTER_S *)&conf->osd;
				new = (AGTX_OSD_CONF_OUTER_S *)&cfg_osd->strm[i];
				chn_idx = MPI_ENC_CHN(conf->chn_idx);

				/* Setup binding and param */
				for (int j = 0; j < MPI_OSD_MAX_BIND_CHANNEL; j++) {
					VIDEO_OSD_setBindState(chn_idx, j, 0);
				}
				for (int j = 0; j < MPI_OSD_MAX_BIND_CHANNEL; j++) {
					if (cur->region[j].enabled != -1) {
						VIDEO_OSD_setBindState(chn_idx, j, new->region[j].enabled);

						if (cur->region[j].enabled && new->region[j].enabled) {
							ret = VIDEO_OSD_setBindLoc(chn_idx, j, conf->strm.width,
							                           conf->strm.height, &new->region[j]);
							if (ret) {
								VIDEO_OSD_setBindState(chn_idx, j, 0);
								SYS_TRACE(
								        "Fail to reset OSD region in chn:%d osd region:%d\n",
								        conf->chn_idx, j);
								return -1;
							}
							cur->region[j].start_x = new->region[j].start_x;
							cur->region[j].start_y = new->region[j].start_y;
						}
						cur->region[j].enabled = new->region[j].enabled;

						/* TODO Modify Here to Support Fake bind editing*/
						if (strcmp((char *)cur->region[j].type_spec,
						           (const char *)new->region[j].type_spec)) {
							if (j == 0) {
								/* channel name is fixed to region 0 */
								strcpy((char *)cur->region[j].type_spec,
								       (const char *)new->region[j].type_spec);
								deleteText(chn_idx, j);
								createText(cur->region[j].start_x,
								           cur->region[j].start_y,
								           (char *)cur->region[j].type_spec, chn_idx,
								           conf->strm.width, conf->strm.height,
								           cur->region[j].enabled, j);
							}
							if (i == 0 && j == 1) {
								/* follow stream 0 region 1 for date time format */
								strcpy((char *)cur->region[j].type_spec,
								       (const char *)new->region[j].type_spec);
								setOsdDateTimeFormat((char *)cur->region[j].type_spec);
							}
						}
						//SYS_TRACE("setbind state chn:%d id:%d %d after\n", chn_idx.chn, j, new->region[j].enabled);
					}
				}
			}
		}
	} else {
		init_osd = 1;
		VIDEO_OSD_setShowWeekDay(cfg_osd->showWeekDay);
	}
	for (int i = 0; i < 4; i++) {
		AGTX_OSD_CONF_OUTER_S *cur = (AGTX_OSD_CONF_OUTER_S *)&cfg[0].cfg_vin.cfg_vchn[i].osd;
		AGTX_OSD_CONF_OUTER_S *new = (AGTX_OSD_CONF_OUTER_S *)&cfg_osd->strm[i];
		memcpy(cur, new, sizeof(AGTX_OSD_CONF_OUTER_S));
	}

	return ret;
}

static INT32 cfg_set_osd_pm(CFG_ATTR_S *attr, void *data, INT32 size)
{
	INT32 ret = 0;

	CFG_CAMERA_S *cfg = &g_config;
	CFG_VCHN_S *cfg_vchn = NULL;
	AGTX_OSD_PM_CONF_S *cfg_osd_pm = NULL;

	AGTX_OSD_PM_PARAM_S *old = NULL;
	AGTX_OSD_PM_PARAM_S *new = NULL;
	UINT32 dev_idx = attr->group;
	MPI_ECHN chn_idx = { { 0 } };

	if (size != sizeof(AGTX_OSD_PM_CONF_S)) {
		SYS_TRACE("CFG_OSD_PM_S size %d / %d dismatch\n", size, sizeof(AGTX_OSD_PM_CONF_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	cfg_osd_pm = (AGTX_OSD_PM_CONF_S *)data;

	/*cfg doesn't initial,only save value*/
	if (!g_video_init) {
		for (int i = 0; i < 4; i++) {
			memcpy(&cfg[attr->group].cfg_vin.cfg_vchn[i].osd_pm, &cfg_osd_pm->conf[i],
			       sizeof(AGTX_OSD_PM_PARAM_S));
		}
		return 0;
	}

	for (int i = 0; i < 4; i++) {
		cfg_vchn = &cfg[dev_idx].cfg_vin.cfg_vchn[i];
		if (cfg_vchn->enable == 0) {
			continue;
		}
		chn_idx = MPI_ENC_CHN(cfg_vchn->chn_idx);
		cfg_osd_pm = (AGTX_OSD_PM_CONF_S *)data;
		old = &cfg_vchn->osd_pm;
		new = &cfg_osd_pm->conf[i];
		ret |= VIDEO_OSD_checkPmParam(chn_idx, old, new, cfg_vchn->strm.width, cfg_vchn->strm.height);
	}
	if (ret < 0)
		return VIDEO_OSD_FAILURE;
	else if (ret == VIDEO_OSD_RESTREAM_REQUEST) {
	} else {
		for (int i = 0; i < 4; i++) {
			cfg_vchn = &cfg[dev_idx].cfg_vin.cfg_vchn[i];
			if (cfg_vchn->enable == 0) {
				continue;
			}
			cfg_osd_pm = (AGTX_OSD_PM_CONF_S *)data;
			old = &cfg_vchn->osd_pm;
			new = &cfg_osd_pm->conf[i];
			ret = cfg_apply_osd_pm(dev_idx, cfg_vchn->chn_idx, cfg_vchn->strm.width, cfg_vchn->strm.height,
			                       new, old);
			if (ret < 0) {
				return ret;
			}
		}
	}

	for (int i = 0; i < 4; i++) {
		cfg_vchn = &cfg[dev_idx].cfg_vin.cfg_vchn[i];
		cfg_osd_pm = (AGTX_OSD_PM_CONF_S *)data;
		old = &cfg_vchn->osd_pm;
		new = &cfg_osd_pm->conf[i];
		memcpy(old, new, sizeof(AGTX_OSD_PM_PARAM_S));
	}
	//cfg_vchn = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id];
	//for (int j = 0; j < 4; j++ ) {
	//	old = &cfg_vchn->osd_pm.conf[0];
	//	SYS_TRACE("[%s] chn:%d, i:%d, conf st:(%d,%d), end(%d,%d), cr:(%d), alpha:(%d), enabled(%d)\n",
	//    __func__, attr->id,j, old->param[j].start.x, old->param[j].start.y,
	//   old->param[j].end.x, old->param[j].end.y,
	//    old->param[j].color, old->param[j].alpha,
	//    old->param[j].enabled);
	//}
	return ret;
}

/**
 * @brief start & apply config to modules.
 * @return The execution result.
 */
INT32 CFG_startAllModule()
{
	INT32 ret = 0;

	/*if config not ready,do nothing*/
	if (!g_video_init) {
		pthread_mutex_lock(&g_cfg_mutex);
		ret = cfgStartVideo();
		g_video_init = 1;
		pthread_mutex_unlock(&g_cfg_mutex);
	}

	return ret;
}

/**
 * @brief close all modules.
 * @return The execution result.
 */
INT32 CFG_closeAllModule()
{
	/*if video not start,do nothing*/
	if (g_video_init) {
		pthread_mutex_lock(&g_cfg_mutex);
		cfgStopVideo();
		g_video_init = 0;
		pthread_mutex_unlock(&g_cfg_mutex);
	}

	return 0;
}

/**
 * @brief config initial.
 * @return The execution result.
 */
INT32 CFG_init()
{
	INT32 ret = 0;
	g_cfg_init = 1;
	pthread_mutex_lock(&g_cfg_mutex);
	ret = cfgInitSys();
	pthread_mutex_unlock(&g_cfg_mutex);
	return ret;
}

/**
 * @brief config initial.
 * @return The execution result.
 */
INT32 CFG_exit()
{
	INT32 ret = 0;
	g_cfg_init = 0;
	pthread_mutex_lock(&g_cfg_mutex);
	cfgExitSys();
	pthread_mutex_unlock(&g_cfg_mutex);
	return ret;
}

static int avmain_checkSingleInstance(void)
{
	int ret = 0;
	int lock_fd;
	const char *lock_file_path = LOCK_FILE_PATH;

	lock_fd = open(lock_file_path, O_CREAT | O_RDWR, 0600);
	if (lock_fd < 0) {
		SYS_TRACE("Open lock file %s failed\n", lock_file_path);
		return -1;
	}

	ret = flock(lock_fd, LOCK_EX | LOCK_NB);
	if (ret) {
		if (EWOULDBLOCK == errno) {
			SYS_TRACE("av_main is already running ...!\n");
			SYS_TRACE("To restart it, you have to kill the program and delete the lock file %s\n",
			          lock_file_path);
			close(lock_fd);
			return -1;
		}
	} else {
		SYS_TRACE("Starting av_main ...\n");
	}

	g_lock_fd = lock_fd;

	return 0;
}

static void avmain_removeSingleInstance(void)
{
	int lock_fd = g_lock_fd;

	if (lock_fd >= 0) {
		flock(lock_fd, LOCK_UN);
		close(lock_fd);
	}

	g_lock_fd = -1;
}

static void avmain_handle_sig_crash(int signo)
{
	FILE *out;
	char **symbollist;
	void *addrlist[64] = { 0 }; // storage array for stack trace address data

	out = fopen("/dev/console", "w+");

	if (out <= 0) {
		exit(0);
	}

	fprintf(out, "program stop\n");

	// retrieve current stack addresses
	int addrlen = backtrace(addrlist, sizeof(addrlist) / sizeof(void *));

	if (addrlen == 0) {
		exit(0);
	}

	// create readable strings to each frame.
	symbollist = backtrace_symbols(addrlist, addrlen);

	// print the stack trace.
	for (int i = 0; i < addrlen; i++) {
		fprintf(out, "%s\n", symbollist[i]);
	}

	free(symbollist);

	avmain_removeSingleInstance();
	exit(0);
}

static void avmain_handle_sig_int(int signo)
{
	if (signo == SIGINT) {
		printf("Caught SIGINT!\n");
	} else if (signo == SIGTERM) {
		printf("Caught SIGTERM!\n");
	} else {
		perror("Unexpected signal!\n");
		exit(1);
	}

	signal(SIGINT, SIG_DFL);
	pthread_mutex_destroy(&g_video_mutex);
	pthread_cond_destroy(&g_video_cond);

	/*close rtsp*/
	//VFTR_UPDATE_exit();
#ifdef ENABLE_RTSPSERVER
	RTSP_exit();
#endif /* !ENABLE_RTSPSERVER */

	/*close video system*/
	CFG_closeAllModule(); //TODO need move to other source file not in config
	CFG_exit();

	//if (AVFTR_exitServer() < 0) {
	//perror("Exit AVFTR server failed\n");
	//}

	avmain_removeSingleInstance();
	exit(0);
}

void avmain_send_onvif_event(AGTX_SW_EVENT_TRIG_TYPE_E type)
{
	INT32 ret = 0;
	INT32 sockfd = -1;
	INT32 servlen = 0;
	struct sockaddr_un serv_addr;
	int alarm = 0;
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;

	if (av_ctrl->onvif_event_fd <= 0) {
		/*Connect to onvif*/
		bzero((char *)&serv_addr, sizeof(serv_addr));
		serv_addr.sun_family = AF_UNIX;
		strcpy(serv_addr.sun_path, ONVIF_EVENT_PATH);
		servlen = strlen(serv_addr.sun_path) + sizeof(serv_addr.sun_family);

		if ((sockfd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
			SYS_TRACE("Create ONVIF sockfd failed %d(%m)\n", errno);
			sockfd = -1;
		}

		if (sockfd > 0) {
			fcntl(sockfd, F_SETFL, O_NONBLOCK);

			if (connect(sockfd, (struct sockaddr *)&serv_addr, servlen) < 0) {
				SYS_TRACE("Connecting to ONVIF server failed %d(%m)\n", errno);
				close(sockfd);
				sockfd = -1;
				return;
			} else {
				av_ctrl->onvif_event_fd = sockfd;
			}
		}

		alarm = type;
		ret = write(av_ctrl->onvif_event_fd, &alarm, sizeof(alarm));
		if (ret < 0) {
			SYS_TRACE("write socket error %d(%m)\n", errno);
			return;
		}
		//		close(sockfd);
	} else {
		alarm = type;
		ret = write(av_ctrl->onvif_event_fd, &alarm, sizeof(alarm));
		if (ret < 0) {
			SYS_TRACE("write socket error %d(%m)\n", errno);
			close(av_ctrl->onvif_event_fd);
			av_ctrl->onvif_event_fd = -1;
			return;
		}
	}
}

static INT32 avmain_setInputFps(int stream_num, int width, int height)
{
	CFG_CAMERA_S *cfg = &g_config;
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VIN_S cfg_vin = { 0 };

	/*Set Input fps*/
	cfg_attr.group = 0;
	cfg_attr.id = 0;
	memcpy(&cfg_vin, &cfg[cfg_attr.group].cfg_vin, sizeof(CFG_VIN_S));

	INT32 ret = VIDEO_getInputFps(MPI_VIDEO_DEV(0), &cfg_vin.cfg_dev.input_fps);
	if (ret) {
		return ret;
	}

	cfg_set_vin(&cfg_attr, &cfg_vin, sizeof(CFG_VIN_S));
	return 0;
}

/**
 * @brief set device configuration.
 * @return The execution result.
 */
INT32 avmain_set_dev_config(void *data, char *errstr)
{
	INT32 i = 0;
	INT32 ret = 0;
	AGTX_DEV_CONF_S dev = { 0 };
	CFG_CAMERA_S *cfg = &g_config;
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VIN_S cfg_vin = { 0 };
	AGTX_PATH_CONF_S *cfg_path = NULL;
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_VIDEO_DEV_CONF);
	parse_video_dev_conf(&dev, ret_obj);

	if (av_ctrl->video_init) {
		/*do nothing before config initial*/
		CFG_closeAllModule();
	}

	cfg_attr.group = 0;
	cfg_attr.id = 0;
	cfg_vin = cfg[cfg_attr.group].cfg_vin;

	cfg_vin.enable = 1;
	cfg_vin.cfg_dev.video_dev_idx = dev.video_dev_idx;
	cfg_vin.cfg_dev.hdr_mode = dev.hdr_mode;
	cfg_vin.cfg_dev.stitch_en = dev.stitch_en;
	cfg_vin.cfg_dev.eis_en = dev.eis_en;
	cfg_vin.cfg_dev.bayer = dev.bayer;
	cfg_vin.cfg_dev.input_fps = dev.input_fps; /*change at strm config setting*/

	for (i = 0; i < AGTX_MAX_INPUT_PATH_NUM; i++) {
		cfg_path = &cfg_vin.cfg_dev.input_path[i];
		cfg_path->path_en = dev.input_path[i].path_en;
		cfg_path->path_idx = dev.input_path[i].path_idx;
		cfg_path->sensor_idx = dev.input_path[i].sensor_idx;
		cfg_path->width = dev.input_path[i].width;
		cfg_path->height = dev.input_path[i].height;
	}

	ret = cfg_set_vin(&cfg_attr, &cfg_vin, sizeof(CFG_VIN_S));
	if (ret < 0) {
		return -1;
	}

	if (av_ctrl->video_init) {
		/*do nothing before config initial*/
		ret = CFG_startAllModule();
	}

	return ret;
}

/**
 * @brief set layout configuration.
 * @return The execution result.
 */
INT32 avmain_set_layout_config(void *data, char *errstr)
{
	INT32 i = 0;
	INT32 ret = 0;
	AGTX_LAYOUT_CONF_S layout = { 0 };
	CFG_CAMERA_S *cfg = &g_config;
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VCHN_S cfg_vchn = { 0 };
	static INT32 restart_rtsp = 0;
	static INT32 restart_video = 0;
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_VIDEO_LAYOUT_CONF);
	parse_layout_conf(&layout, ret_obj);

	if (restart_rtsp && av_ctrl->video_init) {
		system("killall -9 testOnDemandRTSPServer >> /dev/null");
#ifdef ENABLE_RTSPSERVER
		RTSP_exit();
#endif /* !ENABLE_RTSPSERVER */
	}

	/*do nothing before config initial*/
	if (restart_video && av_ctrl->video_init) {
		CFG_closeAllModule();
	}

	for (i = 0; i < layout.layout_num; i++) {
		cfg_attr.group = layout.video_dev_idx;
		cfg_attr.id = layout.video_layout[i].video_strm_idx;

		if (cfg_attr.group >= MAX_VIDEO_INPUT) {
			continue;
		}
		cfg_vchn = cfg[cfg_attr.group].cfg_vin.cfg_vchn[cfg_attr.id];

		cfg_vchn.layout_en = layout.layout_en;
		memcpy(&cfg_vchn.layout, &layout.video_layout[i], sizeof(AGTX_LAYOUT_PARAM_S));

		ret = cfg_set_vchn(&cfg_attr, &cfg_vchn, sizeof(CFG_VCHN_S));
		if (ret < 0) {
			continue;
		}
	}

	/*do nothing before config initial*/
	if (restart_video && av_ctrl->video_init) {
		ret = CFG_startAllModule();
	}

	if (restart_rtsp && av_ctrl->video_init) {
#ifdef ENABLE_RTSPSERVER
		RTSP_init();
#else
		system("testOnDemandRTSPServer 0&");
#endif /* !ENABLE_RTSPSERVER */
		system("testOnDemandRTSPServer 1&");
		if (av_ctrl->strm_cnt >= 3) {
			system("testOnDemandRTSPServer 2&");
		}
	}

	/* For on-the-fly change layout config */
	restart_video = 1;
	restart_rtsp = 1;

	return ret;
}

/**
 * @brief set stream configuration.
 * @return The execution result.
 */
INT32 avmain_set_strm_config(void *data, char *errstr)
{
	INT32 i = 0;
	INT32 ret = 0;
	INT32 stream_num = 0;
	INT32 restart_rtsp = 0;
	INT32 restart_video = 0;
	AGTX_STRM_CONF_S strm = { 0 };
	CFG_CAMERA_S *cfg = &g_config;
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VCHN_S cfg_vchn = { 0 };
	CFG_VENC_S cfg_venc = { 0 };
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_VIDEO_STRM_CONF);
	parse_video_strm_conf(&strm, ret_obj);

	strm.video_strm_cnt = (strm.video_strm_cnt >= AGTX_MAX_VIDEO_STRM_NUM) ? AGTX_MAX_VIDEO_STRM_NUM :
	                                                                         strm.video_strm_cnt;

	av_ctrl->strm_cnt = strm.video_strm_cnt;

	for (i = 0; i < strm.video_strm_cnt; i++) {
		cfg_attr.group = 0;
		cfg_attr.id = i;
		cfg_vchn = cfg[cfg_attr.group].cfg_vin.cfg_vchn[cfg_attr.id];
		cfg_venc = cfg[cfg_attr.group].cfg_venc[cfg_attr.id];

		if (strm.video_strm[i].strm_en) {
			stream_num++;
		}

		if (cfg_venc.enable != strm.video_strm[i].strm_en || cfg_venc.strm.width != strm.video_strm[i].width ||
		    cfg_venc.strm.height != strm.video_strm[i].height ||
		    cfg_venc.strm.venc_type != strm.video_strm[i].venc_type ||
		    cfg_venc.strm.rc_mode != strm.video_strm[i].rc_mode ||
		    cfg_venc.strm.venc_profile != strm.video_strm[i].venc_profile) {
			restart_video = 1;
			restart_rtsp = 1; //Disable this to restart 2nd,3rd instance of testOnDemandRTSPServer
		}

		/* disable restart 2nd,3rd instances of testOnDemandRTSPServer
		if (cfg_venc.venc_type != avmain_getVencType((strm.video_strm[i].venc_type)) ||
			(cfg_venc.width  != strm.video_strm[i].width) ||
			(cfg_venc.height != strm.video_strm[i].height)) {
			restart_rtsp = 1;
		} */
	}

	if (restart_rtsp && av_ctrl->video_init) {
		system("killall -9 testOnDemandRTSPServer >> /dev/null");
#ifdef ENABLE_RTSPSERVER
		RTSP_exit();
#endif /* !ENABLE_RTSPSERVER */
	}

	/*do nothing before config initial*/
	if (restart_video && av_ctrl->video_init) {
		CFG_closeAllModule();
	}

	avmain_setInputFps(stream_num, strm.video_strm[0].width, strm.video_strm[0].height);

	for (i = 0; i < strm.video_strm_cnt; i++) {
		cfg_attr.group = 0;
		cfg_attr.id = i;
		cfg_vchn = cfg[cfg_attr.group].cfg_vin.cfg_vchn[cfg_attr.id];
		cfg_venc = cfg[cfg_attr.group].cfg_venc[cfg_attr.id];

		/*set vchn*/
		cfg_vchn.enable = (UINT32)strm.video_strm[i].strm_en;
		cfg_vchn.chn_idx = (UINT32)strm.video_strm[i].video_strm_idx;
		cfg_vchn.strm.width = (UINT32)strm.video_strm[i].width;
		cfg_vchn.strm.height = (UINT32)strm.video_strm[i].height;
		cfg_vchn.max_width = (UINT32)strm.video_strm[i].width;
		cfg_vchn.max_width = (UINT32)strm.video_strm[i].height;
		cfg_vchn.strm.fps = (UINT32)strm.video_strm[i].output_fps;
		cfg_vchn.strm.rotate = strm.video_strm[i].rotate;
		cfg_vchn.strm.mirror = (UINT32)strm.video_strm[i].mirr_en;
		cfg_vchn.strm.flip = (UINT32)strm.video_strm[i].flip_en;

		/* FIXME for testing window-based streaming flow*/
		//cfg_vchn.layout_en = 0;

		/*set venc*/
		cfg_venc.enable = (UINT32)strm.video_strm[i].strm_en;
		cfg_venc.chn_idx = (UINT32)strm.video_strm[i].video_strm_idx;
		cfg_venc.strm.width = (UINT32)strm.video_strm[i].width;
		cfg_venc.strm.height = (UINT32)strm.video_strm[i].height;
		cfg_venc.max_width = (UINT32)strm.video_strm[i].width;
		cfg_venc.max_height = (UINT32)strm.video_strm[i].height;
		cfg_venc.bind_dev_idx = (UINT32)strm.video_dev_idx;
		cfg_venc.bind_chn_idx = (UINT32)strm.video_strm[i].video_strm_idx;
		cfg_venc.strm.output_fps = (UINT32)strm.video_strm[i].output_fps;
		cfg_venc.strm.venc_type = strm.video_strm[i].venc_type;
		cfg_venc.strm.venc_profile = strm.video_strm[i].venc_profile;
		cfg_venc.strm.rc_mode = strm.video_strm[i].rc_mode;
		cfg_venc.strm.gop_size = (UINT32)strm.video_strm[i].gop_size;
		cfg_venc.strm.bit_rate = (UINT32)strm.video_strm[i].bit_rate;
		cfg_venc.strm.min_qp = (UINT32)strm.video_strm[i].min_qp;
		cfg_venc.strm.max_qp = (UINT32)strm.video_strm[i].max_qp;
		cfg_venc.strm.min_q_factor = (UINT32)strm.video_strm[i].min_q_factor;
		cfg_venc.strm.max_q_factor = (UINT32)strm.video_strm[i].max_q_factor;
		cfg_venc.strm.fluc_level = (UINT32)strm.video_strm[i].fluc_level;
		cfg_venc.strm.regression_speed = (UINT32)strm.video_strm[i].regression_speed;
		cfg_venc.strm.scene_smooth = (UINT32)strm.video_strm[i].scene_smooth;
		cfg_venc.strm.i_continue_weight = (UINT32)strm.video_strm[i].i_continue_weight;
		cfg_venc.strm.i_qp_offset = strm.video_strm[i].i_qp_offset;
		cfg_venc.strm.vbr_max_bit_rate = (UINT32)strm.video_strm[i].vbr_max_bit_rate;
		cfg_venc.strm.vbr_quality_level_index = (UINT32)strm.video_strm[i].vbr_quality_level_index;
		cfg_venc.strm.sbr_adjust_br_thres_pc = (UINT32)strm.video_strm[i].sbr_adjust_br_thres_pc;
		cfg_venc.strm.sbr_adjust_step_times = (UINT32)strm.video_strm[i].sbr_adjust_step_times;
		cfg_venc.strm.sbr_converge_frame = (UINT32)strm.video_strm[i].sbr_converge_frame;
		cfg_venc.strm.cqp_i_frame_qp = (UINT32)strm.video_strm[i].cqp_i_frame_qp;
		cfg_venc.strm.cqp_p_frame_qp = (UINT32)strm.video_strm[i].cqp_p_frame_qp;
		cfg_venc.strm.cqp_q_factor = (UINT32)strm.video_strm[i].cqp_q_factor;
		cfg_venc.strm.obs = (UINT32)strm.video_strm[i].obs;
		cfg_venc.strm.obs_off_period = (UINT32)strm.video_strm[i].obs_off_period;

		ret = cfg_set_vchn(&cfg_attr, &cfg_vchn, sizeof(CFG_VCHN_S));
		if (ret < 0) {
			continue;
		}

		ret = cfg_set_venc(&cfg_attr, &cfg_venc, sizeof(CFG_VENC_S));
		if (ret < 0) {
			continue;
		}
	}

	/*do nothing before config initial*/
	if (restart_video && av_ctrl->video_init) {
		ret = CFG_startAllModule();
	}

	if (restart_rtsp && av_ctrl->video_init) {
#ifdef ENABLE_RTSPSERVER
		RTSP_init();
#else
		system("testOnDemandRTSPServer 0&");
#endif /* !ENABLE_RTSPSERVER  */
		system("testOnDemandRTSPServer 1&");
		if (av_ctrl->strm_cnt >= 3) {
			system("testOnDemandRTSPServer 2&");
		}
	}

	return ret;
}

/**
 * @brief set stitch configuration.
 * @return The execution result.
 */
INT32 avmain_set_stitch_config(void *data, char *errstr)
{
	INT32 ret = 0;
	AGTX_STITCH_CONF_S stitch = { 0 };
	CFG_CAMERA_S *cfg = &g_config;
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_STITCH_CONF);
	parse_stitch_conf(&stitch, ret_obj);

	/*Check stitch has been enable*/
	/* FIXME */
	MPI_DEV dev_idx = MPI_VIDEO_DEV(stitch.video_dev_idx);
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VCHN_S cfg_vchn = { 0 };

	cfg_attr.group = 0;
	cfg_attr.id = 0;

	cfg_vchn = cfg[cfg_attr.group].cfg_vin.cfg_vchn[cfg_attr.id];
	cfg_vchn.stitch = stitch;

	ret = cfg_set_vchn(&cfg_attr, &cfg_vchn, sizeof(CFG_VCHN_S));
	if (ret < 0) {
		return -1;
	}

	if (av_ctrl->video_init) {
		if (VIDEO_DEV_setStitchAttr(dev_idx, &cfg_vchn.stitch)) {
			SYS_TRACE("av_main set stitch attribute fail!\n");
			return -1;
		}
	}

	return 0;
}

/**
 * @brief set image configuration.
 * @return The execution result.
 */
INT32 avmain_set_img_config(void *data, char *errstr)
{
	INT32 ret = 0;
	CFG_ATTR_S cfg_attr = { 0 };
	AGTX_IMG_PREF_S cfg_img = { 0 };
	CFG_CAMERA_S *cfg = &g_config;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_IMG_PREF);
	parse_img_pref(&cfg_img, ret_obj);

	cfg_attr.group = 0;
	cfg_attr.id = 0;

	ret = cfg_apply_img(&cfg_img, cfg_attr.group);
	if (ret) {
		return -1;
	}

	memcpy(&cfg[cfg_attr.group].cfg_img, &cfg_img, sizeof(AGTX_IMG_PREF_S));
	return 0;
}

/**
 * @brief set advanced image performance configuration.
 * @return The execution result.
 */
INT32 avmain_set_adv_img_pref_config(void *data, char *errstr)
{
	INT32 ret = 0;
	CFG_ATTR_S cfg_attr = { 0 };
	AGTX_ADV_IMG_PREF_S cfg_adv_img = { 0 };
	CFG_CAMERA_S *cfg = &g_config;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_ADV_IMG_PREF);
	parse_adv_img_pref(&cfg_adv_img, ret_obj);

	cfg_attr.group = 0;
	cfg_attr.id = 0;

	ret = VIDEO_setAdvImgPref(MPI_VIDEO_DEV(cfg_attr.group), &cfg_adv_img);
	if (ret) {
		return -1;
	}
	memcpy(&cfg[cfg_attr.group].cfg_adv_img, &cfg_adv_img, sizeof(AGTX_ADV_IMG_PREF_S));

	return 0;
}

/**
 * @brief set awb performance configuration.
 * @return The execution result.
 */
INT32 avmain_set_awb_pref_config(void *data, char *errstr)
{
	INT32 ret = 0;
	CFG_ATTR_S cfg_attr = { 0 };
	AGTX_AWB_PREF_S cfg_awb = { 0 };
	CFG_CAMERA_S *cfg = &g_config;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_AWB_PREF);
	parse_awb_pref(&cfg_awb, ret_obj);

	cfg_attr.group = 0;
	cfg_attr.id = 0;

	ret = VIDEO_setAwbPref(MPI_VIDEO_DEV(cfg_attr.group), &cfg_awb);
	if (ret) {
		return -1;
	}
	memcpy(&cfg[cfg_attr.group].cfg_awb, &cfg_awb, sizeof(AGTX_AWB_PREF_S));

	return 0;
}

/**
 * @brief set color mode configuration.
 * @return The execution result.
 */
INT32 avmain_set_color_config(void *data, char *errstr)
{
	INT32 ret = 0;
	CFG_ATTR_S cfg_attr = { 0 };
	AGTX_COLOR_CONF_S cfg_color = { 0 };
	CFG_CAMERA_S *cfg = &g_config;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_COLOR_CONF);
	parse_color_conf(&cfg_color, ret_obj);

	cfg_attr.group = 0;
	cfg_attr.id = 0;

	ret = VIDEO_setColorMode(MPI_VIDEO_DEV(cfg_attr.group), &cfg_color);
	if (ret) {
		return -1;
	}

	memcpy(&cfg[cfg_attr.group].cfg_color, &cfg_color, sizeof(AGTX_COLOR_CONF_S));
	return 0;
}

/**
 * @brief set anti-flicker mode configuration.
 * @return The execution result.
 */
INT32 avmain_set_anti_flicker_config(void *data, char *errstr)
{
	INT32 ret = 0;
	AGTX_ANTI_FLICKER_CONF_S cfg_anti_flicker = { 0 };
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_CAMERA_S *cfg = &g_config;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_ANTI_FLICKER_CONF);
	parse_anti_flicker_conf(&cfg_anti_flicker, ret_obj);

	cfg_attr.group = 0;
	cfg_attr.id = 0;

	ret = VIDEO_DIP_setDefaultAntiFlicker(MPI_VIDEO_DEV(cfg_attr.group), &cfg_anti_flicker);
	if (ret) {
		return -1;
	}

	memcpy(&cfg[cfg_attr.group].cfg_anti_flicker, &cfg_anti_flicker, sizeof(AGTX_ANTI_FLICKER_CONF_S));

	return 0;
}

/**
 * @brief set cal configuration.
 * @return The execution result.
 */
INT32 avmain_set_cal_config(void *data, char *errstr)
{
	//INT32 ret = 0;
	AGTX_DIP_CAL_CONF_S cal;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_DIP_CAL);
	parse_dip_cal_conf(&cal, ret_obj);

	if (VIDEO_DIP_setCal(MPI_VIDEO_DEV(cal.video_dev_idx), &cal)) {
		SYS_TRACE("av_main set CAL attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set dbc configuration.
 * @return The execution result.
 */
INT32 avmain_set_dbc_config(void *data, char *errstr)
{
	//INT32 ret = 0;
	AGTX_DIP_DBC_CONF_S dbc;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_DIP_DBC);
	parse_dip_dbc_conf(&dbc, ret_obj);

	if (VIDEO_DIP_setDbc(MPI_VIDEO_DEV(dbc.video_dev_idx), &dbc)) {
		SYS_TRACE("av_main set DBC attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set dcc configuration.
 * @return The execution result.
 */
INT32 avmain_set_dcc_config(void *data, char *errstr)
{
	//INT32 ret = 0;
	AGTX_DIP_DCC_CONF_S dcc;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_DIP_DCC);
	parse_dip_dcc_conf(&dcc, ret_obj);

	if (VIDEO_DIP_setDcc(MPI_VIDEO_DEV(dcc.video_dev_idx), &dcc)) {
		SYS_TRACE("av_main set DCC attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set lsc configuration.
 * @return The execution result.
 */
INT32 avmain_set_lsc_config(void *data, char *errstr)
{
	//INT32 ret = 0;
	AGTX_DIP_LSC_CONF_S lsc;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_DIP_LSC);
	parse_dip_lsc_conf(&lsc, ret_obj);

	if (VIDEO_DIP_setLsc(MPI_VIDEO_DEV(lsc.video_dev_idx), &lsc)) {
		SYS_TRACE("av_main set LSC attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set roi configuration.
 * @return The execution result.
 */
INT32 avmain_set_roi_config(void *data, char *errstr)
{
	//INT32 ret = 0;
	AGTX_DIP_ROI_CONF_S roi;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_DIP_ROI);
	parse_dip_roi_conf(&roi, ret_obj);

	if (VIDEO_DIP_setRoi(MPI_VIDEO_DEV(roi.video_dev_idx), &roi)) {
		SYS_TRACE("av_main set ROI attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set ctrl configuration.
 * @return The execution result.
 */
INT32 avmain_set_ctrl_config(void *data, char *errstr)
{
	//INT32 ret = 0;
	AGTX_DIP_CTRL_CONF_S ctrl;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_DIP_CTRL);
	parse_dip_ctrl_conf(&ctrl, ret_obj);

	if (VIDEO_DIP_setCtrl(MPI_VIDEO_DEV(ctrl.video_dev_idx), &ctrl)) {
		SYS_TRACE("av_main set CTRL attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set ae configuration.
 * @return The execution result.
 */
INT32 avmain_set_ae_config(void *data, char *errstr)
{
	//INT32 ret = 0;
	AGTX_DIP_AE_CONF_S ae;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_DIP_AE);
	parse_dip_ae_conf(&ae, ret_obj);

	if (VIDEO_DIP_setAe(MPI_VIDEO_DEV(ae.video_dev_idx), &ae)) {
		SYS_TRACE("av_main set AE attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set dip_iso configuration.
 * @return The execution result.
 */
INT32 avmain_set_iso_config(void *data, char *errstr)
{
	//INT32 ret = 0;
	AGTX_DIP_ISO_CONF_S iso;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_DIP_ISO);
	parse_dip_iso_conf(&iso, ret_obj);

	if (VIDEO_DIP_setIso(MPI_VIDEO_DEV(iso.video_dev_idx), &iso)) {
		SYS_TRACE("av_main set DIP_ISO attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set awb configuration.
 * @return The execution result.
 */
INT32 avmain_set_awb_config(void *data, char *errstr)
{
	//INT32 ret = 0;
	AGTX_DIP_AWB_CONF_S awb;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_DIP_AWB);
	parse_dip_awb_conf(&awb, ret_obj);

	if (VIDEO_DIP_setAwb(MPI_VIDEO_DEV(awb.video_dev_idx), &awb)) {
		SYS_TRACE("av_main set AWB attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set csm configuration.
 * @return The execution result.
 */
INT32 avmain_set_csm_config(void *data, char *errstr)
{
	//INT32 ret = 0;
	AGTX_DIP_CSM_CONF_S csm;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_DIP_CSM);
	parse_dip_csm_conf(&csm, ret_obj);

	if (VIDEO_DIP_setCsm(MPI_VIDEO_DEV(csm.video_dev_idx), &csm)) {
		SYS_TRACE("av_main set CSM attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set pta configuration.
 * @return The execution result.
 */
INT32 avmain_set_pta_config(void *data, char *errstr)
{
	//INT32 ret = 0;
	AGTX_DIP_PTA_CONF_S pta;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_DIP_PTA);
	parse_dip_pta_conf(&pta, ret_obj);

	if (VIDEO_DIP_setPta(MPI_VIDEO_DEV(pta.video_dev_idx), &pta)) {
		SYS_TRACE("av_main set PTA attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set shp configuration.
 * @return The execution result.
 */
INT32 avmain_set_shp_config(void *data, char *errstr)
{
	//INT32 ret = 0;
	AGTX_DIP_SHP_CONF_S shp;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_DIP_SHP);
	parse_dip_shp_conf(&shp, ret_obj);

	if (VIDEO_DIP_setShp(MPI_VIDEO_DEV(shp.video_dev_idx), &shp)) {
		SYS_TRACE("av_main set SHP attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set nr configuration.
 * @return The execution result.
 */
INT32 avmain_set_nr_config(void *data, char *errstr)
{
	//INT32 ret = 0;
	AGTX_DIP_NR_CONF_S nr;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_DIP_NR);
	parse_dip_nr_conf(&nr, ret_obj);

	if (VIDEO_DIP_setNr(MPI_VIDEO_DEV(nr.video_dev_idx), &nr)) {
		SYS_TRACE("av_main set NR attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set window based shp configuration.
 * @return The execution result.
 */
INT32 avmain_set_win_shp_config(void *data, char *errstr)
{
	//INT32 ret = 0;
	INT32 i, j;
	UINT8 dev_id, chn_id, win_id;
	AGTX_DIP_SHP_WIN_CONF_S shp;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_DIP_SHP_WIN);
	parse_dip_shp_win_conf(&shp, ret_obj);

	if (shp.win_shp_en == 1) {
		dev_id = shp.video_dev_idx;

		for (i = 0; i < shp.strm_num; i++) {
			chn_id = shp.video_strm[i].video_strm_idx;

			for (j = 0; j < shp.video_strm[i].window_num; j++) {
				win_id = shp.video_strm[i].window_array[j].window_idx;

				if (VIDEO_DIP_setWinShp(MPI_VIDEO_WIN(dev_id, chn_id, win_id),
				                        &shp.video_strm[i].window_array[j])) {
					SYS_TRACE("av_main set window based SHP attribute failed!\n");
					return -1;
				}
			}
		}
	}

	return 0;
}

/**
 * @brief set window based nr configuration.
 * @return The execution result.
 */
INT32 avmain_set_win_nr_config(void *data, char *errstr)
{
	//INT32 ret = 0;
	INT32 i, j;
	UINT8 dev_id, chn_id, win_id;
	AGTX_DIP_NR_WIN_CONF_S nr;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_DIP_NR_WIN);
	parse_dip_nr_win_conf(&nr, ret_obj);

	if (nr.win_nr_en == 1) {
		dev_id = nr.video_dev_idx;

		for (i = 0; i < nr.strm_num; i++) {
			chn_id = nr.video_strm[i].video_strm_idx;

			for (j = 0; j < nr.video_strm[i].window_num; j++) {
				win_id = nr.video_strm[i].window_array[j].window_idx;

				if (VIDEO_DIP_setWinNr(MPI_VIDEO_WIN(dev_id, chn_id, win_id),
				                       &nr.video_strm[i].window_array[j])) {
					SYS_TRACE("av_main set window based NR attribute failed!\n");
					return -1;
				}
			}
		}
	}

	return 0;
}

/**
* @brief set te configuration.
 * @return The execution result.
 */
INT32 avmain_set_te_config(void *data, char *errstr)
{
	//INT32 ret = 0;
	AGTX_DIP_TE_CONF_S te;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_DIP_TE);
	parse_dip_te_conf(&te, ret_obj);

	if (VIDEO_DIP_setTe(MPI_VIDEO_DEV(te.video_dev_idx), &te)) {
		SYS_TRACE("av_main set TE attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set gamma configuration.
 * @return The execution result.
 */
INT32 avmain_set_gamma_config(void *data, char *errstr)
{
	//INT32 ret = 0;
	AGTX_DIP_GAMMA_CONF_S gamma;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_DIP_GAMMA);
	parse_dip_gamma_conf(&gamma, ret_obj);

	if (VIDEO_DIP_setGamma(MPI_VIDEO_DEV(gamma.video_dev_idx), &gamma)) {
		SYS_TRACE("av_main set GAMMA attribugamma failed!\n");
		return -1;
	}

	return 0;
}

INT32 avmain_set_osd_config(void *data, char *errstr)
{
	INT32 ret = 0;
	AGTX_OSD_CONF_S cfg_osd;
	CFG_ATTR_S cfg_attr = { 0 };
	INT32 restart_rtsp = 0;
	INT32 restart_video = 0;
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_OSD_CONF);
	parse_osd_conf(&cfg_osd, ret_obj);

	cfg_attr.group = 0;
	cfg_attr.id = 0;

	ret = cfg_set_osd(&cfg_attr, &cfg_osd);
	if (ret < 0) {
		return 1;
	} else if (ret == VIDEO_OSD_RESTREAM_REQUEST) {
		restart_rtsp = 1;
		restart_video = 1;
	}

	if (restart_rtsp && av_ctrl->video_init) {
		system("killall -9 testOnDemandRTSPServer >> /dev/null");
#ifdef ENABLE_RTSPSERVER
		RTSP_exit();
#endif /* !ENABLE_RTSPSERVER */
	}

	/*do nothing before config initial*/
	if (restart_video && av_ctrl->video_init) {
		CFG_closeAllModule();
	}

	/*do nothing before config initial*/
	if (restart_video && av_ctrl->video_init) {
		ret = CFG_startAllModule();
	}

	if (restart_rtsp && av_ctrl->video_init) {
		/* Check streaming status */
#ifdef ENABLE_RTSPSERVER
		RTSP_init();
#else
		system("testOnDemandRTSPServer 0&");
#endif /* !ENABLE_RTSPSERVER  */
		system("testOnDemandRTSPServer 1&");
		if (av_ctrl->strm_cnt >= 3) {
			system("testOnDemandRTSPServer 2&");
		}
	}

	return ret;
}
/**
 * @brief set osd privacy mask configuration.
 * @return The execution result.
 */
INT32 avmain_set_osd_pm_config(void *data, char *errstr)
{
	INT32 ret = 0;
	INT32 restart_rtsp = 0;
	INT32 restart_video = 0;
	CFG_ATTR_S cfg_attr = { 0 };
	AGTX_OSD_PM_CONF_S cfg_osd_pm;
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;
	struct json_object *ret_obj = NULL;

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_OSD_PM_CONF);
	parse_osd_pm_conf(&cfg_osd_pm, ret_obj);

	/* TODO DONT NEED CFG_ATTR here */
	cfg_attr.group = 0;
	cfg_attr.id = 0;
	ret = cfg_set_osd_pm(&cfg_attr, (AGTX_OSD_PM_CONF_S *)&cfg_osd_pm, sizeof(AGTX_OSD_PM_CONF_S));
	if (ret < 0) {
		return 1;
	} else if (ret == VIDEO_OSD_RESTREAM_REQUEST) {
		restart_rtsp = 1;
		restart_video = 1;
	}

	if (restart_rtsp && av_ctrl->video_init) {
		system("killall -9 testOnDemandRTSPServer >> /dev/null");
#ifdef ENABLE_RTSPSERVER
		RTSP_exit();
#endif /* !ENABLE_RTSPSERVER */
	}

	/*do nothing before config initial*/
	if (restart_video && av_ctrl->video_init) {
		CFG_closeAllModule();
	}

	/*do nothing before config initial*/
	if (restart_video && av_ctrl->video_init) {
		ret = CFG_startAllModule();
	}

	if (restart_rtsp && av_ctrl->video_init) {
		/* Check streaming status */
#ifdef ENABLE_RTSPSERVER
		RTSP_init();
#else
		system("testOnDemandRTSPServer 0&");
#endif /* !ENABLE_RTSPSERVER */
		system("testOnDemandRTSPServer 1&");
		if (av_ctrl->strm_cnt >= 3) {
			system("testOnDemandRTSPServer 2&");
		}
	}

	return ret;
}

/**
 * @brief set ldc configuration.
 * @return The execution result.
 */
INT32 avmain_set_ldc_config(void *data, char *errstr)
{
	INT32 ret = 0;
	AGTX_LDC_CONF_S ldc;
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;
	struct json_object *ret_obj = NULL;
	CFG_CAMERA_S *cfg = &g_config;
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VCHN_S cfg_vchn = { 0 };

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_LDC_CONF);
	parse_ldc_conf(&ldc, ret_obj);

	MPI_DEV dev_idx = MPI_VIDEO_DEV(ldc.video_dev_idx);

	cfg_attr.group = 0;
	cfg_attr.id = 0;
	cfg_vchn = cfg[cfg_attr.group].cfg_vin.cfg_vchn[cfg_attr.id];
	cfg_vchn.ldc = ldc;

	ret = cfg_set_vchn(&cfg_attr, &cfg_vchn, sizeof(CFG_VCHN_S));
	if (ret < 0) {
		return -1;
	}
	if (av_ctrl->video_init) {
		if (VIDEO_DEV_setLdcConf(dev_idx, &ldc)) {
			SYS_TRACE("av_main set LDC attribute fail!\n");
			return -1;
		}
	}

	return 0;
}

/**
 * @brief set panorama configuration.
 * @return The execution result.
 */
INT32 avmain_set_panorama_config(void *data, char *errstr)
{
	INT32 ret = 0;
	AGTX_PANORAMA_CONF_S pano;
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;
	struct json_object *ret_obj = NULL;
	CFG_CAMERA_S *cfg = &g_config;
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VCHN_S cfg_vchn = { 0 };

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_PANORAMA_CONF);
	parse_panorama_conf(&pano, ret_obj);

	MPI_DEV dev_idx = MPI_VIDEO_DEV(pano.video_dev_idx);

	cfg_attr.group = 0;
	cfg_attr.id = 0;
	cfg_vchn = cfg[cfg_attr.group].cfg_vin.cfg_vchn[cfg_attr.id];
	cfg_vchn.panorama = pano;

	ret = cfg_set_vchn(&cfg_attr, &cfg_vchn, sizeof(CFG_VCHN_S));
	if (ret < 0) {
		return -1;
	}

	if (av_ctrl->video_init) {
		if (VIDEO_DEV_setPanoramaConf(dev_idx, &pano)) {
			SYS_TRACE("av_main set Panorama attribute fail!\n");
			return -1;
		}
	}

	return 0;
}

/**
 * @brief set panning configuration.
 * @return The execution result.
 */
INT32 avmain_set_panning_config(void *data, char *errstr)
{
	INT32 ret = 0;
	AGTX_PANNING_CONF_S pann;
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;
	struct json_object *ret_obj = NULL;
	CFG_CAMERA_S *cfg = &g_config;
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VCHN_S cfg_vchn = { 0 };

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_PANNING_CONF);
	parse_panning_conf(&pann, ret_obj);

	MPI_DEV dev_idx = MPI_VIDEO_DEV(pann.video_dev_idx);

	cfg_attr.group = 0;
	cfg_attr.id = 0;
	cfg_vchn = cfg[cfg_attr.group].cfg_vin.cfg_vchn[cfg_attr.id];
	cfg_vchn.panning = pann;

	ret = cfg_set_vchn(&cfg_attr, &cfg_vchn, sizeof(CFG_VCHN_S));
	if (ret < 0) {
		return -1;
	}

	if (av_ctrl->video_init) {
		if (VIDEO_DEV_setPanningConf(dev_idx, &pann)) {
			SYS_TRACE("av_main set Panning attribute fail!\n");
			return -1;
		}
	}

	return 0;
}

/**
 * @brief set surround configuration.
 * @return The execution result.
 */
INT32 avmain_set_surround_config(void *data, char *errstr)
{
	INT32 ret = 0;
	AGTX_SURROUND_CONF_S surr;
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;
	struct json_object *ret_obj = NULL;
	CFG_CAMERA_S *cfg = &g_config;
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VCHN_S cfg_vchn = { 0 };

	ret_obj = get_db_record_obj(TMP_ACTIVE_DB, AGTX_CMD_SURROUND_CONF);
	parse_surround_conf(&surr, ret_obj);

	MPI_DEV dev_idx = MPI_VIDEO_DEV(surr.video_dev_idx);

	cfg_attr.group = 0;
	cfg_attr.id = 0;
	cfg_vchn = cfg[cfg_attr.group].cfg_vin.cfg_vchn[cfg_attr.id];
	cfg_vchn.surround = surr;

	ret = cfg_set_vchn(&cfg_attr, &cfg_vchn, sizeof(CFG_VCHN_S));
	if (ret < 0) {
		return -1;
	}

	if (av_ctrl->video_init) {
		if (VIDEO_DEV_setSurroundConf(dev_idx, &surr)) {
			SYS_TRACE("av_main set Surround attribute fail!\n");
			return -1;
		}
	}

	return 0;
}

/**
 * @brief Notify other module when configuration changed.
 * @return The execution result.
 */
INT32 avmain_set_notify()
{
	INT32 ret = 0;
	CFG_ATTR_S cfg_attr = { 0 };
	MPI_WIN idx = { .dev = cfg_attr.group, .chn = cfg_attr.id, .win = 0 };
	MPI_ECHN echn_idx = { .chn = 0 };
	MPI_ENC_EVENT_S event = { .user_setting_changed = 1 };

	if (g_mpi_sys_initialized) {
		MPI_ENC_notifyEvent(echn_idx, &event);
	}

	return 0;
}

INT32 register_cmd_handler()
{
	CmdHandler *cmd = cmd_handler;
	/* video cmd */
	cmd[0].set = avmain_set_dev_config;
	cmd[1].set = avmain_set_strm_config;
	cmd[2].set = avmain_set_stitch_config;
	cmd[3].set = avmain_set_osd_config;
	cmd[4].set = avmain_set_osd_pm_config;
	cmd[5].set = avmain_set_ldc_config;
	cmd[6].set = avmain_set_layout_config;
	cmd[7].set = avmain_set_panorama_config;
	cmd[8].set = avmain_set_panning_config;
	cmd[9].set = avmain_set_surround_config;

	/*dip low cmd*/
	cmd[10].set = avmain_set_cal_config;
	cmd[11].set = avmain_set_dbc_config;
	cmd[12].set = avmain_set_dcc_config;
	cmd[13].set = avmain_set_lsc_config;
	cmd[14].set = avmain_set_ctrl_config;
	cmd[15].set = avmain_set_ae_config;
	cmd[16].set = avmain_set_awb_config;
	cmd[17].set = avmain_set_pta_config;
	cmd[18].set = avmain_set_csm_config;
	cmd[19].set = avmain_set_shp_config;
	cmd[20].set = avmain_set_nr_config;
	cmd[21].set = avmain_set_roi_config;
	cmd[22].set = avmain_set_te_config;
	cmd[23].set = avmain_set_gamma_config;
	cmd[24].set = avmain_set_iso_config;
	cmd[25].set = avmain_set_win_shp_config;
	cmd[26].set = avmain_set_win_nr_config;

	/*dip high cmd*/
	cmd[27].set = avmain_set_anti_flicker_config;
	cmd[28].set = avmain_set_img_config;
	cmd[29].set = avmain_set_adv_img_pref_config;
	cmd[30].set = avmain_set_awb_pref_config;
	cmd[31].set = avmain_set_color_config;

	return 0;
}

INT32 main()
{
	int ret = 0;
	FILE *frdy = NULL;
	pthread_t tid = 0;
	//char name[16] = "av_main_cc";
	//char tmpname[16] = {0};
	int rc = 0;
	CmdHandler *cmd = cmd_handler;
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;

	ret = avmain_checkSingleInstance();
	if (ret) {
		exit(-1);
	}

	/*capture signal*/
	if (signal(SIGPIPE, SIG_IGN) == SIG_ERR) {
		perror("Cannot handle SIGPIPE!\n");
		return -1;
	}

	if (signal(SIGINT, avmain_handle_sig_int) == SIG_ERR) {
		perror("Cannot handle SIGINT!\n");
		return -1;
	}

	if (signal(SIGTERM, avmain_handle_sig_int) == SIG_ERR) {
		perror("Cannot handle SIGINT!\n");
		return -1;
	}

	if (signal(SIGSEGV, avmain_handle_sig_crash) == SIG_ERR) {
		perror("Cannot handle SIGINT!\n");
		return -1;
	}

	if (register_cmd_handler() < 0) {
		SYS_TRACE("register_cmd_handler failed\n");
		return -1;
	}

	for (int i = 0; i < 10; i++) {
		if (cmd[i].set(NULL, NULL) < 0) {
			SYS_TRACE("Init video cmd failed\n");
			return -1;
		}
	}
	CFG_init();

	ret = VIDEO_getDevAttr(MPI_VIDEO_DEV(0));
	assert(ret == 0 && "VIDEO_getDevAttr fail\n");

	for (int i = 10; i < 27; i++) {
		if (cmd[i].set(NULL, NULL) < 0) {
			SYS_TRACE("Init dip low cmd failed\n");
			return -1;
		}
	}

	ret = VIDEO_DIP_initDipHighAttr(MPI_VIDEO_DEV(0));
	assert(ret == 0 && "VIDEO_DIP_initDipHighAttr fail\n");

	for (int i = 27; i < 32; i++) {
		if (cmd[i].set(NULL, NULL) < 0) {
			SYS_TRACE("Init dip high cmd failed\n");
			return -1;
		}
	}

	ret = CFG_startAllModule();
	if (ret < 0) {
		system("touch /usrdata/dbrst");
		system("reboot");
	}

	av_ctrl->video_init = 1;

	/*Waiting for video start.*/
	/*
	pthread_mutex_lock(&g_video_mutex);
	pthread_cond_wait(&g_video_cond, &g_video_mutex);
	pthread_mutex_unlock(&g_video_mutex); 
	*/
	//avmain_connect_to_event_demon();

	//VFTR_UPDATE_init();
#ifdef ENABLE_RTSPSERVER
	RTSP_init();
#endif /* !ENABLE_RTSPSERVER */

	frdy = fopen(AVMAIN_RDY_FILE, "w");
	fclose(frdy);
	frdy = NULL;

	while (1) {
		sleep(1);
	}
	rc = pthread_join(tid, NULL);
	if (rc != 0) {
		printf("Error Occured at %s:%d \n", __func__, __LINE__);
	}

	return 0;
}
