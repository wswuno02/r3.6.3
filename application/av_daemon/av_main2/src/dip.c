#ifdef __cplusplus
extern "C" {
#endif /**< __cplusplus */

//#define DIP_DBG

#include "mpi_types.h"
#include "mpi_common.h"
#include "mpi_dip_common.h"
#include "mpi_dip_sns.h"
#include "mpi_dip_alg.h"
#include "mpi_dev.h"

//#include "config_api.h"
#include "cfg_api.h"
#include "video_dip.h"
#include "video_api.h"
#include "agtx_video.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <pthread.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <assert.h>

#define WAIT_DCC_APPLY_USEC (130000)
#define CLAMP_8B(x) ((x) > 255 ? 255 : ((x) < 0 ? 0 : x))
#define PREF_TH (50)
#define DIP_MODULE_NUM (15)
#define MAX_DARK_ENHANCE_LEVEL (128)
#define IR_STRENTH_PRC (8)
#define IR_STRENTH_STEP (652) //2.55 * 256

typedef enum {
	DIP_STAT_NULL,
	DIP_STAT_SINGLE,
	DIP_STAT_DUAL,
	DIP_STAT_STITCH,
	DIP_STAT_NUM,
} DIP_STAT_E;

/* Define the config struct type */
typedef struct {
	MPI_CAL_ATTR_S CAL[MAX_INPUT_PATH];
	MPI_DBC_ATTR_S DBC[MAX_INPUT_PATH];
	MPI_DCC_ATTR_S DCC[MAX_INPUT_PATH];
	MPI_LSC_ATTR_S LSC[MAX_INPUT_PATH];
	MPI_ROI_ATTR_S ROI[MAX_INPUT_PATH];
	MPI_AE_ATTR_S AE;
	MPI_ISO_ATTR_S DIP_ISO;
	MPI_AWB_ATTR_S AWB;
	MPI_PTA_ATTR_S PTA;
	MPI_CSM_ATTR_S CSM;
	MPI_SHP_ATTR_S SHP; // dev-based shp attr
	MPI_NR_ATTR_S NR; // dev-based nr attr
	MPI_SHP_ATTR_S WIN_SHP[MAX_VIDEO_CHNNEL * MAX_VIDEO_WINDOW]; // win-based shp attr
	MPI_NR_ATTR_S WIN_NR[MAX_VIDEO_CHNNEL * MAX_VIDEO_WINDOW]; // win-based nr attr
	MPI_GAMMA_ATTR_S GAMMA;
	MPI_TE_ATTR_S TE;
	MPI_DIP_ATTR_S DIP;
} VIDEO_DIP_CONF_S;

/* Global variables declaration */
int g_dip_update_flag = 0;
static INT32 g_dip_run = 0;
static INT32 g_color_mode = -1;
static DIP_STAT_E g_dip_stat = DIP_STAT_NULL;
static AGTX_ANTI_FLICKER_CONF_S g_anti_flicker = { 0 };
static INT32 g_default_anti_flicker_update = 0;
static INT32 g_ir_light_suppression = 0;
static INT32 g_ae_strategy_mode = 0;

VIDEO_DIP_CONF_S g_default_config;
VIDEO_DIP_CONF_S g_high_config;

/* Function declaration */
static INT32 handleBlc(const MPI_DEV dev_idx, const AGTX_ADV_IMG_PREF_S *cfg, const VIDEO_DIP_CONF_S *dft,
                       VIDEO_DIP_CONF_S *new);
static INT32 handleWdr(const MPI_DEV dev_idx, const AGTX_ADV_IMG_PREF_S *cfg, const VIDEO_DIP_CONF_S *dft,
                       VIDEO_DIP_CONF_S *new);
static INT32 getDarkEnhance(const INT32 dft, const INT32 value);
static INT32 SwitchDay(MPI_DEV dev_idx, INT32 *color_mode);
static INT32 SwitchNight(MPI_DEV dev_idx, INT32 *color_mode);

INT32 VIDEO_getDevAttr(MPI_DEV idx)
{
	MPI_DEV_ATTR_S dev_attr;
	UINT8 path_cnt;
	INT32 ret;

	ret = MPI_DEV_getDevAttr(idx, &dev_attr);

	path_cnt = (dev_attr.path.bmp == 0x3) ? 2 : 1;

	/* Determine parameters for get/set DIP attribute */
	if (dev_attr.stitch_en == 0 && path_cnt == 1) {
		g_dip_stat = DIP_STAT_SINGLE; // Only vaild on (d, p) = (0, 0)

	} else if (dev_attr.stitch_en == 0 && path_cnt == 2) {
		g_dip_stat = DIP_STAT_DUAL; // all vaild (d, p)

	} else if (dev_attr.stitch_en == 1 && path_cnt == 2) {
		g_dip_stat = DIP_STAT_STITCH; // DBC, DCC, LSC, CAL, ROI could accept (d, p) = (0~1, 0~1)

	} else {
		g_dip_stat = DIP_STAT_NULL;
		printf("%s(): Invaild device attribute, stitch_en = %d, path_cnt = %d, impossible case!\n", __func__,
		       dev_attr.stitch_en, path_cnt);
	}

	printf("###--- %s(): g_dip_stat = %d\n", __func__, g_dip_stat);

	return ret;
}

INT32 VIDEO_DIP_setDipAllAttr(MPI_DEV dev_idx)
{
	INT32 ret = 0;
	MPI_PATH path_idx = MPI_INVALID_INPUT_PATH;

	g_dip_run = 1;
	if (g_dip_update_flag == 0) {
		printf("av_main didn't get parameter from database yet.\n");
		return MPI_SUCCESS;
	}

	switch (g_dip_stat) {
	case DIP_STAT_SINGLE:

		path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

		ret += MPI_setAeAttr(path_idx, &g_high_config.AE);
		ret += MPI_setIsoAttr(path_idx, &g_high_config.DIP_ISO);
		ret += MPI_setAwbAttr(path_idx, &g_high_config.AWB);
		ret += MPI_setPtaAttr(path_idx, &g_high_config.PTA);
		ret += MPI_setShpAttr(path_idx, &g_high_config.SHP);
		ret += MPI_setNrAttr(path_idx, &g_high_config.NR);
		ret += MPI_setTeAttr(path_idx, &g_high_config.TE);
		ret += MPI_setGammaAttr(path_idx, &g_high_config.GAMMA);
		ret += MPI_setDbcAttr(path_idx, &g_high_config.DBC[path_idx.path]);
		ret += MPI_setDccAttr(path_idx, &g_high_config.DCC[path_idx.path]);
		ret += MPI_setLscAttr(path_idx, &g_high_config.LSC[path_idx.path]);
		ret += MPI_setRoiAttr(path_idx, &g_high_config.ROI[path_idx.path]);

		//adv_img_pref_day_night
		if (g_color_mode != -1) {
			if (g_color_mode == 0) {
				// day mode
				ret += MPI_setCalAttr(path_idx, &g_high_config.CAL[path_idx.path]);
				usleep(WAIT_DCC_APPLY_USEC); // waitting dcc apply
				ret += MPI_setDipAttr(path_idx, &g_high_config.DIP);
				ret += MPI_setCsmAttr(path_idx, &g_high_config.CSM);
			} else {
				// night mode
				ret += MPI_setCalAttr(path_idx, &g_high_config.CAL[path_idx.path]);
				ret += MPI_setCsmAttr(path_idx, &g_high_config.CSM);
				ret += MPI_setDipAttr(path_idx, &g_high_config.DIP);
			}
		} else {
			ret += MPI_setCalAttr(path_idx, &g_high_config.CAL[path_idx.path]);
			ret += MPI_setCsmAttr(path_idx, &g_high_config.CSM);
			ret += MPI_setDipAttr(path_idx, &g_high_config.DIP);
		}
		break;
	case DIP_STAT_DUAL:
		assert(0);
		break;
	case DIP_STAT_STITCH:
		path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

		ret += MPI_setAeAttr(path_idx, &g_high_config.AE);
		ret += MPI_setIsoAttr(path_idx, &g_high_config.DIP_ISO);
		ret += MPI_setAwbAttr(path_idx, &g_high_config.AWB);
		ret += MPI_setPtaAttr(path_idx, &g_high_config.PTA);
		ret += MPI_setShpAttr(path_idx, &g_high_config.SHP);
		ret += MPI_setNrAttr(path_idx, &g_high_config.NR);
		ret += MPI_setTeAttr(path_idx, &g_high_config.TE);
		ret += MPI_setGammaAttr(path_idx, &g_high_config.GAMMA);

		/* Set attribute */
		for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
			path_idx.path = sns_path;
			ret += MPI_setDbcAttr(path_idx, &g_high_config.DBC[sns_path]);
			ret += MPI_setDccAttr(path_idx, &g_high_config.DCC[sns_path]);
			ret += MPI_setLscAttr(path_idx, &g_high_config.LSC[sns_path]);
			ret += MPI_setRoiAttr(path_idx, &g_high_config.ROI[sns_path]);
		}

		//adv_img_pref_day_night
		if (g_color_mode != -1) {
			if (g_color_mode == 0) {
				// day mode
				for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
					path_idx.path = sns_path;
					ret += MPI_setCalAttr(path_idx, &g_high_config.CAL[sns_path]);
				}

				/* TODO: Fix this temp solution */
				path_idx.path = 0;

				// waitting dcc apply
				usleep(WAIT_DCC_APPLY_USEC);
				ret += MPI_setDipAttr(path_idx, &g_high_config.DIP);
				ret += MPI_setCsmAttr(path_idx, &g_high_config.CSM);
			} else {
				// night mode
				for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
					path_idx.path = sns_path;
					ret += MPI_setCalAttr(path_idx, &g_high_config.CAL[sns_path]);
				}

				/* TODO: Fix this temp solution */
				path_idx.path = 0;

				ret += MPI_setCsmAttr(path_idx, &g_high_config.CSM);
				ret += MPI_setDipAttr(path_idx, &g_high_config.DIP);
			}
		} else {
			for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
				path_idx.path = sns_path;
				ret += MPI_setCalAttr(path_idx, &g_high_config.CAL[sns_path]);
			}

			/* TODO: Fix this temp solution */
			path_idx.path = 0;

			ret += MPI_setCsmAttr(path_idx, &g_high_config.CSM);
			ret += MPI_setDipAttr(path_idx, &g_high_config.DIP);
		}
		break;
	default:
		printf("%s(): Invaild DIP_STAT(%d)\n", __func__, g_dip_stat);
		break;
	}

	if (ret) {
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 VIDEO_DIP_initDipHighAttr(MPI_DEV dev_idx)
{
	g_dip_update_flag = 1;
	memcpy(&g_high_config, &g_default_config, sizeof(VIDEO_DIP_CONF_S));

	return MPI_SUCCESS;
}

static INT32 SwitchDay(MPI_DEV dev_idx, INT32 *color_mode)
{
	INT32 ret = 0;
	MPI_PATH path_idx;

	switch (g_dip_stat) {
	case DIP_STAT_SINGLE:
		path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

		g_high_config.CAL[path_idx.path].dcc_en = 1;
		g_high_config.DIP.is_awb_en = 1;
		g_high_config.CSM.bw_en = 0;
		g_high_config.AE.strategy.mode = g_ae_strategy_mode;
		g_high_config.AE.strategy.strength = g_default_config.AE.strategy.strength;
		g_high_config.NR.ma_c_strength = g_default_config.NR.ma_c_strength;
		*color_mode = 0;

		if (g_dip_run == 0) {
			return ret;
		}

		ret += MPI_setCalAttr(path_idx, &g_high_config.CAL[path_idx.path]);
		/* waitting dcc apply */
		usleep(WAIT_DCC_APPLY_USEC);
		ret += MPI_setDipAttr(path_idx, &g_high_config.DIP);
		ret += MPI_setCsmAttr(path_idx, &g_high_config.CSM);
		ret += MPI_setAeAttr(path_idx, &g_high_config.AE);
		ret += MPI_setNrAttr(path_idx, &g_high_config.NR);

		break;
	case DIP_STAT_DUAL:
		assert(0);
		break;
	case DIP_STAT_STITCH:
		path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

		for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
			g_high_config.CAL[sns_path].dcc_en = 1;
		}

		g_high_config.DIP.is_awb_en = 1;
		g_high_config.CSM.bw_en = 0;
		g_high_config.AE.strategy.mode = g_default_config.AE.strategy.mode;
		g_high_config.AE.strategy.strength = g_default_config.AE.strategy.strength;
		*color_mode = 0;

		if (g_dip_run == 0) {
			return ret;
		}

		for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
			path_idx.path = sns_path;
			ret += MPI_setCalAttr(path_idx, &g_high_config.CAL[sns_path]);
		}

		/* TODO: Fix this temp solution */
		path_idx.path = 0;

		/* waitting dcc apply */
		usleep(WAIT_DCC_APPLY_USEC);
		ret += MPI_setDipAttr(path_idx, &g_high_config.DIP);
		ret += MPI_setCsmAttr(path_idx, &g_high_config.CSM);
		ret += MPI_setAeAttr(path_idx, &g_high_config.AE);
		break;
	default:
		printf("%s(): Invaild DIP_STAT(%d)\n", __func__, g_dip_stat);
		break;
	}

	return ret;
}

static INT32 SwitchNight(MPI_DEV dev_idx, INT32 *color_mode)
{
	INT32 ret = 0;
	MPI_PATH path_idx;
	INT32 strategy_strength = ((IR_STRENTH_STEP * g_ir_light_suppression) + (1 << (IR_STRENTH_PRC - 1))) >>
	                          IR_STRENTH_PRC;

	switch (g_dip_stat) {
	case DIP_STAT_SINGLE:
		path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
		g_high_config.CSM.bw_en = 1;
		g_high_config.DIP.is_awb_en = 0;
		g_high_config.CAL[path_idx.path].dcc_en = 0;
		g_high_config.AE.strategy.mode = AE_EXP_HIGHLIGHT_PRIOR;
		g_high_config.AE.strategy.strength = strategy_strength;
		g_high_config.NR.ma_c_strength = 0;
		*color_mode = 1;

		if (g_dip_run == 0) {
			return ret;
		}

		ret += MPI_setCalAttr(path_idx, &g_high_config.CAL[path_idx.path]);
		ret += MPI_setCsmAttr(path_idx, &g_high_config.CSM);
		ret += MPI_setDipAttr(path_idx, &g_high_config.DIP);
		ret += MPI_setAeAttr(path_idx, &g_high_config.AE);
		ret += MPI_setNrAttr(path_idx, &g_high_config.NR);

		break;
	case DIP_STAT_DUAL:
		assert(0);
		break;
	case DIP_STAT_STITCH:
		path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
		g_high_config.CSM.bw_en = 1;
		g_high_config.DIP.is_awb_en = 0;
		g_high_config.AE.strategy.mode = AE_EXP_HIGHLIGHT_PRIOR;
		g_high_config.AE.strategy.strength = strategy_strength;
		for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
			g_high_config.CAL[sns_path].dcc_en = 0;
		}
		*color_mode = 1;

		if (g_dip_run == 0) {
			return ret;
		}

		for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
			path_idx.path = sns_path;

			ret += MPI_setCalAttr(path_idx, &g_high_config.CAL[sns_path]);
		}

		/* TODO: Fix this temp solution */
		path_idx.path = 0;

		ret += MPI_setCsmAttr(path_idx, &g_high_config.CSM);
		ret += MPI_setDipAttr(path_idx, &g_high_config.DIP);
		ret += MPI_setAeAttr(path_idx, &g_high_config.AE);

		break;
	default:
		printf("%s(): Invaild DIP_STAT(%d)\n", __func__, g_dip_stat);
		break;
	}

	return ret;
}

static INT32 handleBlc(const MPI_DEV dev_idx, const AGTX_ADV_IMG_PREF_S *cfg, const VIDEO_DIP_CONF_S *dft,
                       VIDEO_DIP_CONF_S *new)
{
	switch (cfg->backlight_compensation) {
	case 0:
		new->AE.roi.luma_weight = dft->AE.roi.luma_weight;
		new->AE.roi.awb_weight = dft->AE.roi.awb_weight;
		break;
	case 1:
		new->AE.roi.luma_weight = 9;
		new->AE.roi.awb_weight = 2;
		break;
	default:
		assert("AGTX_ADV_IMG_PREF_S backlight_compensation is out of range\n");
		break;
	}

#ifdef DIP_DBG
	printf("-----[VIDEO_setAdvImgPref]-----\n");
	printf("default luma_weight = %d\n", dft->AE.roi.luma_weight);
	printf("default awb_weight = %d\n", dft->AE.roi.awb_weight);
	printf("luma_weight = %d\n", new->AE.roi.luma_weight);
	printf("awb_weight = %d\n", new->AE.roi.awb_weight);
#endif

	INT32 ret = 0;

	if (g_dip_run == 0) {
		return ret;
	}

	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_setAeAttr(path_idx, &new->AE);
	if (ret != MPI_SUCCESS) {
		printf("set AdvImgPref(Backlight Compensation) failed\n");
		return MPI_FAILURE;
	}

	return ret;
}

static INT32 getDarkEnhance(const INT32 dft, const INT32 value)
{
	assert(dft >= 0 && dft <= MAX_DARK_ENHANCE_LEVEL && "Invaild default range");
	assert(value >= 0 && value <= 100 && "Invaild value range");

	INT32 result = ((value * MAX_DARK_ENHANCE_LEVEL + (100 - value) * dft) + 50) / 100;

	assert(result >= 0 && result <= 255 && "Impossible result");

	return result;
}

static INT32 handleWdr(const MPI_DEV dev_idx, const AGTX_ADV_IMG_PREF_S *cfg, const VIDEO_DIP_CONF_S *dft,
                       VIDEO_DIP_CONF_S *new)
{
	switch (cfg->wdr_en) {
	case 0:
		/* DIP */
		new->DIP.is_ae_en = dft->DIP.is_ae_en;
		new->DIP.is_te_en = dft->DIP.is_te_en;
		/* AE */
		new->AE.strategy.mode = dft->AE.strategy.mode;
		new->AE.isp_gain_range.min = dft->AE.isp_gain_range.min;
		new->AE.isp_gain_range.max = dft->AE.isp_gain_range.max;
		g_ae_strategy_mode = new->AE.strategy.mode;
		/* TE */
		new->TE.mode = dft->TE.mode;
		new->TE.te_wdr.dark_enhance = dft->TE.te_wdr.dark_enhance;
		break;
	case 1:
		/* DIP */
		new->DIP.is_ae_en = 1;
		new->DIP.is_te_en = 1;
		/* AE */
		new->AE.strategy.mode = AE_EXP_HIGHLIGHT_PRIOR;
		new->AE.isp_gain_range.min = 32;
		new->AE.isp_gain_range.max = 32;
		g_ae_strategy_mode = new->AE.strategy.mode;
		/* TE */
		new->TE.mode = TE_WDR;
		new->TE.te_wdr.dark_enhance = getDarkEnhance(dft->TE.te_wdr.dark_enhance, cfg->wdr_strength);
		break;
	default:
		assert("AGTX_ADV_IMG_PREF_S wdr_en is out of range");
		break;
	}

#ifdef DIP_DBG
	printf("#---> %s:\n", __func__);
	printf("adv_img_pref_cfg->wdr_en = %d\n", cfg->wdr_en);
	printf("adv_img_pref_cfg->wdr_strength = %d\n", cfg->wdr_strength);
	/* DIP */
	printf("DIP.is_ae_en (dft, new) = (%d, %d)\n", dft->DIP.is_ae_en, new->DIP.is_ae_en);
	printf("DIP.is_te_en (dft, new) = (%d, %d)\n", dft->DIP.is_te_en, new->DIP.is_te_en);
	/* AE */
	printf("AE.strategy.mode (dft, new) = (%d, %d)\n", dft->AE.strategy.mode, new->AE.strategy.mode);
	printf("AE.isp_gain_range.min (dft, new) = (%d, %d)\n", dft->AE.isp_gain_range.min, new->AE.isp_gain_range.min);
	printf("AE.isp_gain_range.max (dft, new) = (%d, %d)\n", dft->AE.isp_gain_range.max, new->AE.isp_gain_range.max);
	/* TE */
	printf("TE.mode (dft, new) = (%d, %d)\n", dft->TE.mode, new->TE.mode);
	printf("TE.te_wdr.dark_enhance (dft, new) = (%d, %d)\n", dft->TE.te_wdr.dark_enhance,
	       new->TE.te_wdr.dark_enhance);
#endif

	INT32 ret = 0;

	if (g_dip_run == 0) {
		return ret;
	}

	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret += MPI_setDipAttr(path_idx, &new->DIP);
	ret += MPI_setAeAttr(path_idx, &new->AE);
	ret += MPI_setTeAttr(path_idx, &new->TE);
	if (ret != MPI_SUCCESS) {
		printf("set AdvImgPref(WDR) failed\n");
		return MPI_FAILURE;
	}

	return ret;
}

INT32 VIDEO_getInputFps(MPI_DEV dev_idx, AGTX_INT8 *fps)
{
	INT32 ret = 0;

	if (g_dip_update_flag == 0) {
		printf("Get input FPS fail.\n");
		return MPI_FAILURE;
	}

	*fps = g_default_config.AE.frame_rate;

#ifdef DIP_DBG
	printf("VIDEO_DIP_getInputFps\n");
	printf("default command fps = %f\n", g_default_config.AE.frame_rate);
	printf("high command fps = %f\n", g_high_config.AE.frame_rate);
#endif

	return ret;
}

INT32 VIDEO_DIP_setBrightness(MPI_DEV dev_idx, AGTX_INT16 *brightness)
{
	INT32 ret = 0;
	INT32 tmp = 0;

	INT32 ratio = ((INT32)*brightness << 8) / PREF_TH;
	tmp = CLAMP_8B((((INT32)g_default_config.PTA.brightness * ratio) + (1 << 7)) >> 8);
	g_high_config.PTA.brightness = (UINT8)tmp;
#ifdef DIP_DBG
	printf("-----[VIDEO_DIP_setBrightness]-----\n");
	printf("g_high_config.PTA.brightness = %d\n", g_high_config.PTA.brightness);
#endif

	if (g_dip_run == 0) {
		return ret;
	}

	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_setPtaAttr(path_idx, &g_high_config.PTA);
	if (ret != MPI_SUCCESS) {
		printf("set ImgPref(Brightness) failed\n");
		return ret;
	}

	return ret;
}

INT32 VIDEO_DIP_setContrast(MPI_DEV dev_idx, AGTX_INT16 *contrast)
{
	INT32 ret = 0;
	INT32 tmp = 0;

	INT32 ratio = ((INT32)*contrast << 8) / PREF_TH;
	tmp = CLAMP_8B((((INT32)g_default_config.PTA.contrast * ratio) + (1 << 7)) >> 8);
	g_high_config.PTA.contrast = (UINT8)tmp;
#ifdef DIP_DBG
	printf("-----[VIDEO_DIP_setContrast]-----\n");
	printf("g_high_config.PTA.contrast = %d\n", g_high_config.PTA.contrast);
#endif

	if (g_dip_run == 0) {
		return ret;
	}

	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_setPtaAttr(path_idx, &g_high_config.PTA);
	if (ret != MPI_SUCCESS) {
		printf("set ImgPref(Contrast) failed\n");
		return ret;
	}

	return ret;
}

INT32 VIDEO_DIP_setSharpness(MPI_DEV dev_idx, AGTX_INT16 *sharpness)
{
	INT32 ret = 0;
	INT32 tmp = 0;

	INT32 ratio = ((INT32)*sharpness << 8) / PREF_TH;

#ifdef DIP_DBG
	printf("-----[VIDEO_DIP_setSharpness]-----\n");
#endif

	MPI_CHN_ATTR_S chn_attr;
	UINT8 chn_en[MAX_VIDEO_CHNNEL];

	for (INT32 c = 0; c < MAX_VIDEO_CHNNEL; c++) {
		chn_en[c] = 0;
		if (MPI_DEV_getChnAttr(MPI_VIDEO_CHN(dev_idx.dev, c), &chn_attr) == MPI_SUCCESS) {
			chn_en[c] = 1;
		}
	}

	for (INT32 w = 0; w < MAX_VIDEO_CHNNEL * MAX_VIDEO_WINDOW; w++) {
		for (INT32 i = 0; i < MPI_ISO_LUT_ENTRY_NUM; i++) {
			tmp = CLAMP_8B(
			        (((INT32)g_default_config.WIN_SHP[w].shp_auto.sharpness[i] * ratio) + (1 << 7)) >> 8);
			g_high_config.WIN_SHP[w].shp_auto.sharpness[i] = (UINT8)tmp;
		}

#ifdef DIP_DBG
		for (INT32 i = 0; i < MPI_ISO_LUT_ENTRY_NUM; i++) {
			printf("[%d], sharpness[%d] = %d\n", w, i, g_high_config.WIN_SHP[w].shp_auto.sharpness[i]);
		}
#endif

		if (g_dip_run == 0) {
			continue;
		}

		if (chn_en[(w / MAX_VIDEO_WINDOW)] == 1) {
			MPI_WIN win_idx = MPI_VIDEO_WIN(dev_idx.dev, (w / MAX_VIDEO_WINDOW), (w % MAX_VIDEO_WINDOW));

			ret = MPI_setWinShpAttr(win_idx, &g_high_config.WIN_SHP[w]);
			if (ret != MPI_SUCCESS) {
				printf("set ImgPref(Sharpness) failed on (d, c, w) = (%d, %d, %d)\n", dev_idx.dev,
				       (w / MAX_VIDEO_WINDOW), (w % MAX_VIDEO_WINDOW));
				return ret;
			}
		}
	}

	return ret;
}

INT32 VIDEO_DIP_setSaturation(MPI_DEV dev_idx, AGTX_INT16 *saturation)
{
	INT32 ret = 0;
	INT32 tmp = 0;

	INT32 ratio = ((INT32)*saturation << 8) / PREF_TH;
	for (INT32 i = 0; i < MPI_ISO_LUT_ENTRY_NUM; i++) {
		tmp = CLAMP_8B((((INT32)g_default_config.CSM.csm_auto.saturation[i] * ratio) + (1 << 7)) >> 8);
		g_high_config.CSM.csm_auto.saturation[i] = (UINT8)tmp;
	}
#ifdef DIP_DBG
	printf("-----[VIDEO_DIP_setSaturation]-----\n");
	for (INT32 i = 0; i < MPI_ISO_LUT_ENTRY_NUM; i++) {
		printf("saturation[%d] = %d\n", i, g_high_config.CSM.csm_auto.saturation[i]);
	}
#endif

	if (g_dip_run == 0) {
		return ret;
	}

	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_setCsmAttr(path_idx, &g_high_config.CSM);
	if (ret != MPI_SUCCESS) {
		printf("set ImgPref(Saturation) failed\n");
		return ret;
	}

	return ret;
}

INT32 VIDEO_DIP_setHue(MPI_DEV dev_idx, INT16 *hue)
{
	INT32 ret = 0;
	INT32 tmp = 0;
	INT32 each_step_degree = 20 * 4; // degree_accuracy = 4

	/* hue is 0~100, hue_angle -x ~ x,*/
	INT32 rotate_angle = ((INT32)*hue - 50) * each_step_degree / 50;
	tmp = (INT32)g_default_config.CSM.hue_angle + rotate_angle;
	g_high_config.CSM.hue_angle = tmp;
#ifdef DIP_DBG
	printf("-----[VIDEO_DIP_setHue]-----\n");
	printf("g_high_config.CSM.hue_angle = %d\n", g_high_config.CSM.hue_angle);
#endif

	if (g_dip_run == 0) {
		return ret;
	}

	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_setCsmAttr(path_idx, &g_high_config.CSM);
	if (ret != MPI_SUCCESS) {
		printf("set ImgPref(Hue) failed\n");
		return ret;
	}

	return ret;
}

INT32 VIDEO_DIP_setDefaultAntiFlicker(MPI_DEV dev_idx, AGTX_ANTI_FLICKER_CONF_S *anti_flicker_cfg)
{
	memcpy(&g_anti_flicker, anti_flicker_cfg, sizeof(AGTX_ANTI_FLICKER_CONF_S));

	g_default_anti_flicker_update = 1;

#ifdef DIP_DBG
	printf("-----[VIDEO_DIP_setDefaultAntiFlicker]-----\n");
	printf("enable        = %d\n", g_anti_flicker.enable);
	printf("frequency_idx = %d\n", g_anti_flicker.frequency_idx);
	for (INT32 i = 0; i < MAX_ANTI_FLICKER_FREQUENCY_SIZE; i++) {
		printf("frequency_list[%d].frequency = %d\n", i, g_anti_flicker.frequency_list[i].frequency);
		printf("frequency_list[%d].fps = %f\n", i, g_anti_flicker.frequency_list[i].fps);
	}
#endif

	return MPI_SUCCESS;
}

INT32 VIDEO_DIP_setAntiFlicker(MPI_DEV dev_idx, AGTX_ANTI_FLICKER_E *anti_flicker)
{
	INT32 ret = 0;

	if (g_default_anti_flicker_update == 0) {
		printf("[Warning] default anti flicker not ready\n");
		return MPI_SUCCESS;
	}

	if (*anti_flicker == AGTX_ANTI_FLICKER_OFF) {
		g_high_config.AE.anti_flicker.enable = 0;
		g_high_config.AE.frame_rate = g_default_config.AE.frame_rate;

	} else if (*anti_flicker == AGTX_ANTI_FLICKER_AUTO) {
		INT32 idx = g_anti_flicker.frequency_idx;
		g_high_config.AE.anti_flicker.enable = 1;
		g_high_config.AE.anti_flicker.frequency = g_anti_flicker.frequency_list[idx].frequency;
		g_high_config.AE.frame_rate = g_anti_flicker.frequency_list[idx].fps;

	} else if (*anti_flicker == AGTX_ANTI_FLICKER_50HZ) {
		INT32 idx = 0;
		g_high_config.AE.anti_flicker.enable = 1;
		g_high_config.AE.anti_flicker.frequency = g_anti_flicker.frequency_list[idx].frequency;
		g_high_config.AE.frame_rate = g_anti_flicker.frequency_list[idx].fps;
	} else if (*anti_flicker == AGTX_ANTI_FLICKER_60HZ) {
		INT32 idx = 1;
		g_high_config.AE.anti_flicker.enable = 1;
		g_high_config.AE.anti_flicker.frequency = g_anti_flicker.frequency_list[idx].frequency;
		g_high_config.AE.frame_rate = g_anti_flicker.frequency_list[idx].fps;

	} else {
		printf("AGTX_IMG_PREF_S anti_flicker is out of range\n");
		return MPI_FAILURE;
	}

#ifdef DIP_DBG
	printf("-----[VIDEO_DIP_setAntiFlicker]-----\n");
	printf("anti_flicker.enable = %d\n", g_high_config.AE.anti_flicker.enable);
	printf("anti_flicker.frequency = %d\n", g_high_config.AE.anti_flicker.frequency);
	printf("anti_flicker.fps = %f\n", g_high_config.AE.frame_rate);
#endif

	if (g_dip_run == 0) {
		return ret;
	}

	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_setAeAttr(path_idx, &g_high_config.AE);
	if (ret != MPI_SUCCESS) {
		printf("set ImgPref(Anti_flicker) failed\n");
		return ret;
	}

	return ret;
}

INT32 VIDEO_setAwbPref(MPI_DEV dev_idx, AGTX_AWB_PREF_S *awb_pref_cfg)
{
	INT32 ret = 0;
	INT32 tmp = 0;

	if (awb_pref_cfg->mode == CFG_AWB_AUTO) {
		g_high_config.AWB.high_k = g_default_config.AWB.high_k;
		g_high_config.AWB.low_k = g_default_config.AWB.low_k;
	} else if (awb_pref_cfg->mode == CFG_AWB_MANUAL) {
		g_high_config.AWB.high_k = awb_pref_cfg->color_temp;
		g_high_config.AWB.low_k = awb_pref_cfg->color_temp;
	} else {
		printf("AGTX_AWB_PREF_S mode is out of range\n");
		return MPI_FAILURE;
	}

	INT32 ratio;
	ratio = ((INT32)awb_pref_cfg->r_gain << 8) / PREF_TH;
	tmp = CLAMP_8B((((INT32)g_default_config.AWB.r_extra_gain * ratio) + (1 << 7)) >> 8);
	g_high_config.AWB.r_extra_gain = (UINT8)tmp;
	ratio = ((INT32)awb_pref_cfg->b_gain << 8) / PREF_TH;
	tmp = CLAMP_8B((((INT32)g_default_config.AWB.b_extra_gain * ratio) + (1 << 7)) >> 8);
	g_high_config.AWB.b_extra_gain = (UINT8)tmp;

#ifdef DIP_DBG
	printf("-----[VIDEO_setAwbPref]-----\n");
	printf("default high_k = %d\n", g_default_config.AWB.high_k);
	printf("default low_k = %d\n", g_default_config.AWB.low_k);
	printf("awb high_k = %d\n", g_high_config.AWB.high_k);
	printf("awb low_k = %d\n", g_high_config.AWB.low_k);
	printf("awb b_extra_gain = %d\n", g_high_config.AWB.b_extra_gain);
	printf("awb r_extra_gain = %d\n", g_high_config.AWB.r_extra_gain);
#endif

	if (g_dip_run == 0) {
		return ret;
	}

	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_setAwbAttr(path_idx, &g_high_config.AWB);
	if (ret != MPI_SUCCESS) {
		printf("set AWBPref failed\n");
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 VIDEO_setColorMode(MPI_DEV dev_idx, AGTX_COLOR_CONF_S *color_cfg)
{
	INT32 ret = 0;
	INT32 color_mode = -1;

	if (color_cfg->color_mode == AGTX_COLOR_MODE_DAY) {
		ret = SwitchDay(dev_idx, &color_mode);

	} else if (color_cfg->color_mode == AGTX_COLOR_MODE_NIGHT) {
		ret = SwitchNight(dev_idx, &color_mode);

	} else {
		printf("AGTX_COLOR_CONF_S color_mode is out of range\n");
		return MPI_FAILURE;
	}

	if (g_dip_run == 0) {
		g_color_mode = color_mode;
		return ret;
	}

	if (ret != MPI_SUCCESS) {
		// if set fail, keep the last frame setting
		if (g_color_mode == 1) {
			// night mode
			ret = SwitchNight(dev_idx, &color_mode);
		} else {
			// day mode or first frame
			ret = SwitchDay(dev_idx, &color_mode);
		}
		printf("set AdvImgPref(Image Mode) failed\n");
		return MPI_FAILURE;
	}

#ifdef DIP_DBG
	printf("-----[VIDEO_setColorMode]-----\n");
	printf("last color mode = %d. 0 : day, 1 : night\n", g_color_mode);
	printf("color mode = %d\n", color_cfg->color_mode);
	for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
		printf("cal[%d].dcc_en = %d\n", sns_path, g_high_config.CAL[sns_path].dcc_en);
	}
	printf("dip.is_awb_en = %d\n", g_high_config.DIP.is_awb_en);
	printf("csm.bw_en = %d\n", g_high_config.CSM.bw_en);
#endif
	g_color_mode = color_mode;
	return MPI_SUCCESS;
}

INT32 VIDEO_setAdvImgPref(MPI_DEV dev_idx, AGTX_ADV_IMG_PREF_S *adv_img_pref_cfg)
{
	INT32 ret = 0;

	ret = handleBlc(dev_idx, adv_img_pref_cfg, &g_default_config, &g_high_config);

	assert(ret == MPI_SUCCESS && "Failed to handleBlc\n");

	ret = handleWdr(dev_idx, adv_img_pref_cfg, &g_default_config, &g_high_config);

	assert(ret == MPI_SUCCESS && "Failed to handleWdr\n");

	if ((adv_img_pref_cfg->ir_light_suppression > 100) || (adv_img_pref_cfg->ir_light_suppression < 0)) {
		printf("adv_img_pref ir_light_suppression out of range \n");
		return ret;
	}

	g_ir_light_suppression = adv_img_pref_cfg->ir_light_suppression;

	return ret;
}

INT32 VIDEO_DIP_setCal(MPI_DEV dev_idx, AGTX_DIP_CAL_CONF_S *cal_cfg)
{
	INT32 ret = 0;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
	MPI_CAL_ATTR_S cal_attr[MAX_INPUT_PATH];

	switch (g_dip_stat) {
	case DIP_STAT_SINGLE:
		cal_attr[path_idx.path].cal_en = (UINT8)cal_cfg->cal[path_idx.path].cal_en;
		cal_attr[path_idx.path].dbc_en = (UINT8)cal_cfg->cal[path_idx.path].dbc_en;
		cal_attr[path_idx.path].dcc_en = (UINT8)cal_cfg->cal[path_idx.path].dcc_en;
		cal_attr[path_idx.path].lsc_en = (UINT8)cal_cfg->cal[path_idx.path].lsc_en;

#ifdef DIP_DBG
		printf("-----[VIDEO_DIP_setCal]-----\n");
		printf("cal_en[%d] = %d\n", path_idx.path, cal_attr[path_idx.path].cal_en);
		printf("dbc_en[%d] = %d\n", path_idx.path, cal_attr[path_idx.path].dbc_en);
		printf("dcc_en[%d] = %d\n", path_idx.path, cal_attr[path_idx.path].dcc_en);
		printf("lsc_en[%d] = %d\n", path_idx.path, cal_attr[path_idx.path].lsc_en);
#endif

		if (g_dip_run == 0) {
			memcpy(&g_default_config.CAL[path_idx.path], &cal_attr[path_idx.path], sizeof(MPI_CAL_ATTR_S));
		} else {
			ret += MPI_setCalAttr(path_idx, &cal_attr[path_idx.path]);
		}

		if (g_dip_run == 0) {
			return ret;
		}

		if (ret != MPI_SUCCESS) {
			return MPI_FAILURE;
		}

		memcpy(&g_default_config.CAL[path_idx.path], &cal_attr[path_idx.path], sizeof(MPI_CAL_ATTR_S));
		memcpy(&g_high_config.CAL[path_idx.path], &cal_attr[path_idx.path], sizeof(MPI_CAL_ATTR_S));

		break;
	case DIP_STAT_DUAL:
		assert(0);
		break;
	case DIP_STAT_STITCH:
		for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
			cal_attr[sns_path].cal_en = (UINT8)cal_cfg->cal[sns_path].cal_en;
			cal_attr[sns_path].dbc_en = (UINT8)cal_cfg->cal[sns_path].dbc_en;
			cal_attr[sns_path].dcc_en = (UINT8)cal_cfg->cal[sns_path].dcc_en;
			cal_attr[sns_path].lsc_en = (UINT8)cal_cfg->cal[sns_path].lsc_en;

#ifdef DIP_DBG
			printf("-----[VIDEO_DIP_setCal]-----\n");
			printf("cal_en[%d] = %d\n", sns_path, cal_attr[sns_path].cal_en);
			printf("dbc_en[%d] = %d\n", sns_path, cal_attr[sns_path].dbc_en);
			printf("dcc_en[%d] = %d\n", sns_path, cal_attr[sns_path].dcc_en);
			printf("lsc_en[%d] = %d\n", sns_path, cal_attr[sns_path].lsc_en);
#endif

			if (g_dip_run == 0) {
				memcpy(&g_default_config.CAL[sns_path], &cal_attr[sns_path], sizeof(MPI_CAL_ATTR_S));
			} else {
				path_idx.path = sns_path;
				ret += MPI_setCalAttr(path_idx, &cal_attr[sns_path]);
			}
		}

		if (g_dip_run == 0) {
			return ret;
		}

		if (ret != MPI_SUCCESS) {
			return MPI_FAILURE;
		}

		for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
			memcpy(&g_default_config.CAL[sns_path], &cal_attr[sns_path], sizeof(MPI_CAL_ATTR_S));
			memcpy(&g_high_config.CAL[sns_path], &cal_attr[sns_path], sizeof(MPI_CAL_ATTR_S));
		}
		break;
	default:
		printf("%s(): Invaild DIP_STAT(%d)\n", __func__, g_dip_stat);
		break;
	}

	return MPI_SUCCESS;
}

INT32 VIDEO_DIP_setDbc(MPI_DEV dev_idx, AGTX_DIP_DBC_CONF_S *dbc_cfg)
{
	INT32 ret = 0;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
	MPI_DBC_ATTR_S dbc_attr[MAX_INPUT_PATH];

	switch (g_dip_stat) {
	case DIP_STAT_SINGLE:
		if (dbc_cfg->dbc[path_idx.path].mode == 0) {
			dbc_attr[path_idx.path].mode = ALG_OPT_AUTO;
		} else if (dbc_cfg->dbc[path_idx.path].mode == 2) {
			dbc_attr[path_idx.path].mode = ALG_OPT_MANUAL;
		} else {
			printf("AGTX_DIP_DBC_CONF_S mode is out of range\n");
			return MPI_FAILURE;
		}

		dbc_attr[path_idx.path].dbc_level = (UINT16)dbc_cfg->dbc[path_idx.path].dbc_level;

#ifdef DIP_DBG
		printf("-----[VIDEO_DIP_setDbc]-----\n");
		printf("mode[%d] = %d\n", path_idx.path, dbc_attr[path_idx.path].mode);
		printf("dbc_level[%d] = %d\n", path_idx.path, dbc_attr[path_idx.path].dbc_level);
#endif
		if (g_dip_run == 0) {
			memcpy(&g_default_config.DBC[path_idx.path], &dbc_attr[path_idx.path], sizeof(MPI_DBC_ATTR_S));
		} else {
			ret += MPI_setDbcAttr(path_idx, &dbc_attr[path_idx.path]);
		}

		if (g_dip_run == 0) {
			return ret;
		}

		if (ret != MPI_SUCCESS) {
			return MPI_FAILURE;
		}

		memcpy(&g_default_config.DBC[path_idx.path], &dbc_attr[path_idx.path], sizeof(MPI_DBC_ATTR_S));
		memcpy(&g_high_config.DBC[path_idx.path], &dbc_attr[path_idx.path], sizeof(MPI_DBC_ATTR_S));

		break;
	case DIP_STAT_DUAL:
		assert(0);
		break;
	case DIP_STAT_STITCH:
		for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
			if (dbc_cfg->dbc[sns_path].mode == 0) {
				dbc_attr[sns_path].mode = ALG_OPT_AUTO;
			} else if (dbc_cfg->dbc[sns_path].mode == 2) {
				dbc_attr[sns_path].mode = ALG_OPT_MANUAL;
			} else {
				printf("AGTX_DIP_DBC_CONF_S mode is out of range\n");
				return MPI_FAILURE;
			}
			dbc_attr[sns_path].dbc_level = (UINT16)dbc_cfg->dbc[sns_path].dbc_level;

#ifdef DIP_DBG
			printf("-----[VIDEO_DIP_setDbc]-----\n");
			printf("mode[%d] = %d\n", sns_path, dbc_attr[sns_path].mode);
			printf("dbc_level[%d] = %d\n", sns_path, dbc_attr[sns_path].dbc_level);
#endif
			if (g_dip_run == 0) {
				memcpy(&g_default_config.DBC[sns_path], &dbc_attr[sns_path], sizeof(MPI_DBC_ATTR_S));
			} else {
				path_idx.path = sns_path;
				ret += MPI_setDbcAttr(path_idx, &dbc_attr[sns_path]);
			}
		}

		if (g_dip_run == 0) {
			return ret;
		}

		if (ret != MPI_SUCCESS) {
			return MPI_FAILURE;
		}

		for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
			memcpy(&g_default_config.DBC[sns_path], &dbc_attr[sns_path], sizeof(MPI_DBC_ATTR_S));
			memcpy(&g_high_config.DBC[sns_path], &dbc_attr[sns_path], sizeof(MPI_DBC_ATTR_S));
		}
		break;
	default:
		printf("%s(): Invaild DIP_STAT(%d)\n", __func__, g_dip_stat);
		break;
	}

	return MPI_SUCCESS;
}

INT32 VIDEO_DIP_setDcc(MPI_DEV dev_idx, AGTX_DIP_DCC_CONF_S *dcc_cfg)
{
	INT32 ret = 0;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
	MPI_DCC_ATTR_S dcc_attr[MAX_INPUT_PATH];

	switch (g_dip_stat) {
	case DIP_STAT_SINGLE:
		for (INT32 num = 0; num < MPI_DCC_CHN_NUM; num++) {
			dcc_attr[path_idx.path].gain[num] = (UINT16)dcc_cfg->dcc[path_idx.path].gain[num];
			dcc_attr[path_idx.path].offset_2s[num] = (UINT16)dcc_cfg->dcc[path_idx.path].offset[num];
		}

#ifdef DIP_DBG
		printf("-----[VIDEO_DIP_setDcc]-----\n");
		for (INT32 num = 0; num < MPI_DCC_CHN_NUM; num++) {
			printf("dcc[%d].gain[%d] = %d\n", path_idx.path, num, dcc_attr[path_idx.path].gain[num]);
			printf("dcc[%d].offset_2s[%d] = %d\n", path_idx.path, num,
			       dcc_attr[path_idx.path].offset_2s[num]);
		}
#endif

		if (g_dip_run == 0) {
			memcpy(&g_default_config.DCC[path_idx.path], &dcc_attr[path_idx.path], sizeof(MPI_DCC_ATTR_S));
		} else {
			ret += MPI_setDccAttr(path_idx, &dcc_attr[path_idx.path]);
		}

		if (g_dip_run == 0) {
			return ret;
		}

		if (ret != MPI_SUCCESS) {
			return MPI_FAILURE;
		}

		memcpy(&g_default_config.DCC[path_idx.path], &dcc_attr[path_idx.path], sizeof(MPI_DCC_ATTR_S));
		memcpy(&g_high_config.DCC[path_idx.path], &dcc_attr[path_idx.path], sizeof(MPI_DCC_ATTR_S));

		break;
	case DIP_STAT_DUAL:
		assert(0);
		break;
	case DIP_STAT_STITCH:
		for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
			for (INT32 num = 0; num < MPI_DCC_CHN_NUM; num++) {
				dcc_attr[sns_path].gain[num] = (UINT16)dcc_cfg->dcc[sns_path].gain[num];
				dcc_attr[sns_path].offset_2s[num] = (UINT16)dcc_cfg->dcc[sns_path].offset[num];
			}

#ifdef DIP_DBG
			printf("-----[VIDEO_DIP_setDcc]-----\n");
			for (INT32 num = 0; num < MPI_DCC_CHN_NUM; num++) {
				printf("dcc[%d].gain[%d] = %d\n", sns_path, num, dcc_attr[sns_path].gain[num]);
				printf("dcc[%d].offset_2s[%d] = %d\n", sns_path, num,
				       dcc_attr[sns_path].offset_2s[num]);
			}
#endif

			if (g_dip_run == 0) {
				memcpy(&g_default_config.DCC[sns_path], &dcc_attr[sns_path], sizeof(MPI_DCC_ATTR_S));
			} else {
				path_idx.path = sns_path;
				ret += MPI_setDccAttr(path_idx, &dcc_attr[sns_path]);
			}
		}

		if (g_dip_run == 0) {
			return ret;
		}

		if (ret != MPI_SUCCESS) {
			return MPI_FAILURE;
		}

		for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
			memcpy(&g_default_config.DCC[sns_path], &dcc_attr[sns_path], sizeof(MPI_DCC_ATTR_S));
			memcpy(&g_high_config.DCC[sns_path], &dcc_attr[sns_path], sizeof(MPI_DCC_ATTR_S));
		}
		break;
	default:
		printf("%s(): Invaild DIP_STAT(%d)\n", __func__, g_dip_stat);
		break;
	}

	return MPI_SUCCESS;
}

INT32 VIDEO_DIP_setLsc(MPI_DEV dev_idx, AGTX_DIP_LSC_CONF_S *lsc_cfg)
{
	INT32 ret = 0;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
	MPI_LSC_ATTR_S lsc_attr[MAX_INPUT_PATH];

	switch (g_dip_stat) {
	case DIP_STAT_SINGLE:

		lsc_attr[path_idx.path].origin = (INT32)lsc_cfg->lsc[path_idx.path].origin;
		lsc_attr[path_idx.path].x_trend_2s = (UINT32)lsc_cfg->lsc[path_idx.path].x_trend;
		lsc_attr[path_idx.path].y_trend_2s = (UINT32)lsc_cfg->lsc[path_idx.path].y_trend;
		lsc_attr[path_idx.path].x_curvature = (UINT32)lsc_cfg->lsc[path_idx.path].x_curvature;
		lsc_attr[path_idx.path].y_curvature = (UINT32)lsc_cfg->lsc[path_idx.path].y_curvature;
		lsc_attr[path_idx.path].tilt_2s = (INT32)lsc_cfg->lsc[path_idx.path].tilt;

#ifdef DIP_DBG
		printf("-----[VIDEO_DIP_setLsc]-----\n");
		printf("origin     [%d] = %d\n", path_idx.path, lsc_attr[path_idx.path].origin);
		printf("x_trend_2s [%d] = %d\n", path_idx.path, lsc_attr[path_idx.path].x_trend_2s);
		printf("y_trend_2s [%d] = %d\n", path_idx.path, lsc_attr[path_idx.path].y_trend_2s);
		printf("x_curvature[%d] = %d\n", path_idx.path, lsc_attr[path_idx.path].x_curvature);
		printf("y_curvature[%d] = %d\n", path_idx.path, lsc_attr[path_idx.path].y_curvature);
		printf("tilt_2s    [%d] = %d\n", path_idx.path, lsc_attr[path_idx.path].tilt_2s);
#endif

		if (g_dip_run == 0) {
			memcpy(&g_default_config.LSC[path_idx.path], &lsc_attr[path_idx.path], sizeof(MPI_LSC_ATTR_S));
		} else {
			ret += MPI_setLscAttr(path_idx, &lsc_attr[path_idx.path]);
		}

		if (g_dip_run == 0) {
			return ret;
		}

		if (ret != MPI_SUCCESS) {
			return MPI_FAILURE;
		}

		memcpy(&g_default_config.LSC[path_idx.path], &lsc_attr[path_idx.path], sizeof(MPI_LSC_ATTR_S));
		memcpy(&g_high_config.LSC[path_idx.path], &lsc_attr[path_idx.path], sizeof(MPI_LSC_ATTR_S));

		break;
	case DIP_STAT_DUAL:
		assert(0);
		break;
	case DIP_STAT_STITCH:
		for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
			lsc_attr[sns_path].origin = (INT32)lsc_cfg->lsc[sns_path].origin;
			lsc_attr[sns_path].x_trend_2s = (UINT32)lsc_cfg->lsc[sns_path].x_trend;
			lsc_attr[sns_path].y_trend_2s = (UINT32)lsc_cfg->lsc[sns_path].y_trend;
			lsc_attr[sns_path].x_curvature = (UINT32)lsc_cfg->lsc[sns_path].x_curvature;
			lsc_attr[sns_path].y_curvature = (UINT32)lsc_cfg->lsc[sns_path].y_curvature;
			lsc_attr[sns_path].tilt_2s = (INT32)lsc_cfg->lsc[sns_path].tilt;

#ifdef DIP_DBG
			printf("-----[VIDEO_DIP_setLsc]-----\n");
			printf("origin     [%d] = %d\n", sns_path, lsc_attr[sns_path].origin);
			printf("x_trend_2s [%d] = %d\n", sns_path, lsc_attr[sns_path].x_trend_2s);
			printf("y_trend_2s [%d] = %d\n", sns_path, lsc_attr[sns_path].y_trend_2s);
			printf("x_curvature[%d] = %d\n", sns_path, lsc_attr[sns_path].x_curvature);
			printf("y_curvature[%d] = %d\n", sns_path, lsc_attr[sns_path].y_curvature);
			printf("tilt_2s    [%d] = %d\n", sns_path, lsc_attr[sns_path].tilt_2s);
#endif

			if (g_dip_run == 0) {
				memcpy(&g_default_config.LSC[sns_path], &lsc_attr[sns_path], sizeof(MPI_LSC_ATTR_S));
			} else {
				path_idx.path = sns_path;
				ret += MPI_setLscAttr(path_idx, &lsc_attr[sns_path]);
			}
		}

		if (g_dip_run == 0) {
			return ret;
		}

		if (ret != MPI_SUCCESS) {
			return MPI_FAILURE;
		}

		for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
			memcpy(&g_default_config.LSC[sns_path], &lsc_attr[sns_path], sizeof(MPI_LSC_ATTR_S));
			memcpy(&g_high_config.LSC[sns_path], &lsc_attr[sns_path], sizeof(MPI_LSC_ATTR_S));
		}
		break;
	default:
		printf("%s(): Invaild DIP_STAT(%d)\n", __func__, g_dip_stat);
		break;
	}

	return MPI_SUCCESS;
}

INT32 VIDEO_DIP_setRoi(MPI_DEV dev_idx, AGTX_DIP_ROI_CONF_S *roi_cfg)
{
	INT32 ret = 0;
	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);
	MPI_ROI_ATTR_S roi_attr[MAX_INPUT_PATH];

	switch (g_dip_stat) {
	case DIP_STAT_SINGLE:
		roi_attr[path_idx.path].luma_roi.sx = (INT16)roi_cfg->roi[path_idx.path].luma_roi_sx;
		roi_attr[path_idx.path].luma_roi.sy = (INT16)roi_cfg->roi[path_idx.path].luma_roi_sy;
		roi_attr[path_idx.path].luma_roi.ex = (INT16)roi_cfg->roi[path_idx.path].luma_roi_ex;
		roi_attr[path_idx.path].luma_roi.ey = (INT16)roi_cfg->roi[path_idx.path].luma_roi_ey;
		roi_attr[path_idx.path].awb_roi.sx = (INT16)roi_cfg->roi[path_idx.path].awb_roi_sx;
		roi_attr[path_idx.path].awb_roi.sy = (INT16)roi_cfg->roi[path_idx.path].awb_roi_sy;
		roi_attr[path_idx.path].awb_roi.ex = (INT16)roi_cfg->roi[path_idx.path].awb_roi_ex;
		roi_attr[path_idx.path].awb_roi.ey = (INT16)roi_cfg->roi[path_idx.path].awb_roi_ey;

#ifdef DIP_DBG
		printf("-----[VIDEO_DIP_setRoi]-----\n");
		printf("luma_roi.sx[%d] = %d\n", path_idx.path, roi_attr[path_idx.path].luma_roi.sx);
		printf("luma_roi.sy[%d] = %d\n", path_idx.path, roi_attr[path_idx.path].luma_roi.sy);
		printf("luma_roi.ex[%d] = %d\n", path_idx.path, roi_attr[path_idx.path].luma_roi.ex);
		printf("luma_roi.ey[%d] = %d\n", path_idx.path, roi_attr[path_idx.path].luma_roi.ey);
		printf("awb_roi.sx [%d] = %d\n", path_idx.path, roi_attr[path_idx.path].awb_roi.sx);
		printf("awb_roi.sy [%d] = %d\n", path_idx.path, roi_attr[path_idx.path].awb_roi.sy);
		printf("awb_roi.ex [%d] = %d\n", path_idx.path, roi_attr[path_idx.path].awb_roi.ex);
		printf("awb_roi.ey [%d] = %d\n", path_idx.path, roi_attr[path_idx.path].awb_roi.ey);
#endif

		if (g_dip_run == 0) {
			memcpy(&g_default_config.ROI[path_idx.path], &roi_attr[path_idx.path], sizeof(MPI_ROI_ATTR_S));
		} else {
			ret += MPI_setRoiAttr(path_idx, &roi_attr[path_idx.path]);
		}

		if (g_dip_run == 0) {
			return ret;
		}
		if (ret != MPI_SUCCESS) {
			return MPI_FAILURE;
		}

		memcpy(&g_default_config.ROI[path_idx.path], &roi_attr[path_idx.path], sizeof(MPI_ROI_ATTR_S));
		memcpy(&g_high_config.ROI[path_idx.path], &roi_attr[path_idx.path], sizeof(MPI_ROI_ATTR_S));

		break;
	case DIP_STAT_DUAL:
		assert(0);
		break;
	case DIP_STAT_STITCH:
		for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
			roi_attr[sns_path].luma_roi.sx = (INT16)roi_cfg->roi[sns_path].luma_roi_sx;
			roi_attr[sns_path].luma_roi.sy = (INT16)roi_cfg->roi[sns_path].luma_roi_sy;
			roi_attr[sns_path].luma_roi.ex = (INT16)roi_cfg->roi[sns_path].luma_roi_ex;
			roi_attr[sns_path].luma_roi.ey = (INT16)roi_cfg->roi[sns_path].luma_roi_ey;
			roi_attr[sns_path].awb_roi.sx = (INT16)roi_cfg->roi[sns_path].awb_roi_sx;
			roi_attr[sns_path].awb_roi.sy = (INT16)roi_cfg->roi[sns_path].awb_roi_sy;
			roi_attr[sns_path].awb_roi.ex = (INT16)roi_cfg->roi[sns_path].awb_roi_ex;
			roi_attr[sns_path].awb_roi.ey = (INT16)roi_cfg->roi[sns_path].awb_roi_ey;

#ifdef DIP_DBG
			printf("-----[VIDEO_DIP_setRoi]-----\n");
			printf("luma_roi.sx[%d] = %d\n", sns_path, roi_attr[sns_path].luma_roi.sx);
			printf("luma_roi.sy[%d] = %d\n", sns_path, roi_attr[sns_path].luma_roi.sy);
			printf("luma_roi.ex[%d] = %d\n", sns_path, roi_attr[sns_path].luma_roi.ex);
			printf("luma_roi.ey[%d] = %d\n", sns_path, roi_attr[sns_path].luma_roi.ey);
			printf("awb_roi.sx [%d] = %d\n", sns_path, roi_attr[sns_path].awb_roi.sx);
			printf("awb_roi.sy [%d] = %d\n", sns_path, roi_attr[sns_path].awb_roi.sy);
			printf("awb_roi.ex [%d] = %d\n", sns_path, roi_attr[sns_path].awb_roi.ex);
			printf("awb_roi.ey [%d] = %d\n", sns_path, roi_attr[sns_path].awb_roi.ey);
#endif

			if (g_dip_run == 0) {
				memcpy(&g_default_config.ROI[sns_path], &roi_attr[sns_path], sizeof(MPI_ROI_ATTR_S));
			} else {
				path_idx.path = sns_path;
				ret += MPI_setRoiAttr(path_idx, &roi_attr[sns_path]);
			}
		}
		if (g_dip_run == 0) {
			return ret;
		}
		if (ret != MPI_SUCCESS) {
			return MPI_FAILURE;
		}
		for (INT32 sns_path = 0; sns_path < MAX_INPUT_PATH; sns_path++) {
			memcpy(&g_default_config.ROI[sns_path], &roi_attr[sns_path], sizeof(MPI_ROI_ATTR_S));
			memcpy(&g_high_config.ROI[sns_path], &roi_attr[sns_path], sizeof(MPI_ROI_ATTR_S));
		}
		break;
	default:
		assert(0);
		printf("%s(): Invaild DIP_STAT(%d)\n", __func__, g_dip_stat);
		break;
	}

	return MPI_SUCCESS;
}

INT32 VIDEO_DIP_setCtrl(MPI_DEV dev_idx, AGTX_DIP_CTRL_CONF_S *ctrl_cfg)
{
	INT32 ret = 0;
	MPI_DIP_ATTR_S ctrl_attr;

	ctrl_attr.is_dip_en = (UINT8)ctrl_cfg->is_dip_en;
	ctrl_attr.is_ae_en = (UINT8)ctrl_cfg->is_ae_en;
	ctrl_attr.is_iso_en = (UINT8)ctrl_cfg->is_iso_en;
	ctrl_attr.is_awb_en = (UINT8)ctrl_cfg->is_awb_en;
	ctrl_attr.is_nr_en = (UINT8)ctrl_cfg->is_nr_en;
	ctrl_attr.is_te_en = (UINT8)ctrl_cfg->is_te_en;
	ctrl_attr.is_pta_en = (UINT8)ctrl_cfg->is_pta_en;
	ctrl_attr.is_csm_en = (UINT8)ctrl_cfg->is_csm_en;
	ctrl_attr.is_shp_en = (UINT8)ctrl_cfg->is_shp_en;
	ctrl_attr.is_gamma_en = (UINT8)ctrl_cfg->is_gamma_en;
	ctrl_attr.is_dpc_en = (UINT8)ctrl_cfg->is_dpc_en;
	ctrl_attr.is_dms_en = (UINT8)ctrl_cfg->is_dms_en;
	ctrl_attr.is_me_en = (UINT8)ctrl_cfg->is_me_en;

#ifdef DIP_DBG
	printf("-----[VIDEO_DIP_setCtrl]-----\n");
	printf("is_dip_en   = %d\n", ctrl_attr.is_dip_en);
	printf("is_ae_en    = %d\n", ctrl_attr.is_ae_en);
	printf("is_awb_en   = %d\n", ctrl_attr.is_awb_en);
	printf("is_nr_en    = %d\n", ctrl_attr.is_nr_en);
	printf("is_te_en    = %d\n", ctrl_attr.is_te_en);
	printf("is_pta_en   = %d\n", ctrl_attr.is_pta_en);
	printf("is_csm_en   = %d\n", ctrl_attr.is_csm_en);
	printf("is_shp_en   = %d\n", ctrl_attr.is_shp_en);
	printf("is_gamma_en = %d\n", ctrl_attr.is_gamma_en);
	printf("is_dpc_en   = %d\n", ctrl_attr.is_dpc_en);
	printf("is_dms_en   = %d\n", ctrl_attr.is_dms_en);
	printf("is_me_en    = %d\n", ctrl_attr.is_me_en);
#endif

	if (g_dip_run == 0) {
		memcpy(&g_default_config.DIP, &ctrl_attr, sizeof(MPI_DIP_ATTR_S));
		return ret;
	}

	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_setDipAttr(path_idx, &ctrl_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	memcpy(&g_default_config.DIP, &ctrl_attr, sizeof(MPI_DIP_ATTR_S));
	memcpy(&g_high_config.DIP, &ctrl_attr, sizeof(MPI_DIP_ATTR_S));

	return MPI_SUCCESS;
}

INT32 VIDEO_DIP_setAe(MPI_DEV dev_idx, AGTX_DIP_AE_CONF_S *ae_cfg)
{
	INT32 ret = 0;
	MPI_AE_ATTR_S ae_attr;

	/* exp strategy*/
	if (ae_cfg->exp_strategy == AGTX_EXP_STRATE_NORMAL) {
		ae_attr.strategy.mode = AE_EXP_NORMAL;
	} else if (ae_cfg->exp_strategy == AGTX_EXP_STRATE_HI_LIGHT_SUPPRES) {
		ae_attr.strategy.mode = AE_EXP_HIGHLIGHT_PRIOR;
	} else {
		printf("AGTX_DIP_AE_CONF_S exp_strategy is out of range\n");
		return MPI_FAILURE;
	}

	/* fps mode*/
	if (ae_cfg->fps_mode == AGTX_FPS_MO_FIXED) {
		ae_attr.fps_mode = AE_FPS_FIXED;
	} else if (ae_cfg->fps_mode == AGTX_FPS_MO_VARIABLE) {
		ae_attr.fps_mode = AE_FPS_DROP;
	} else {
		printf("AGTX_DIP_AE_CONF_S fps_mode is out of range\n");
		return MPI_FAILURE;
	}

	ae_attr.sys_gain_range.max = (UINT32)ae_cfg->max_sys_gain;
	ae_attr.sys_gain_range.min = (UINT32)ae_cfg->min_sys_gain;
	ae_attr.sensor_gain_range.max = (UINT32)ae_cfg->max_sensor_gain;
	ae_attr.sensor_gain_range.min = (UINT32)ae_cfg->min_sensor_gain;
	ae_attr.isp_gain_range.max = (UINT32)ae_cfg->max_isp_gain;
	ae_attr.isp_gain_range.min = (UINT32)ae_cfg->min_isp_gain;
	ae_attr.frame_rate = (FLOAT)ae_cfg->frame_rate;
	ae_attr.slow_frame_rate = (FLOAT)ae_cfg->slow_frame_rate;
	ae_attr.speed = (UINT8)ae_cfg->speed;
	ae_attr.black_speed_bias = (UINT8)ae_cfg->black_speed_bias;
	ae_attr.interval = (UINT8)ae_cfg->interval;
	ae_attr.brightness = (UINT16)ae_cfg->brightness;
	ae_attr.tolerance = (UINT16)ae_cfg->tolerance;
	ae_attr.gain_thr_up = (UINT16)ae_cfg->gain_thr_up;
	ae_attr.gain_thr_down = (UINT16)ae_cfg->gain_thr_down;

	ae_attr.strategy.strength = (INT32)ae_cfg->exp_strength;

	ae_attr.roi.awb_weight = (UINT8)ae_cfg->roi_awb_weight;
	ae_attr.roi.luma_weight = (UINT8)ae_cfg->roi_luma_weight;

	ae_attr.delay.black_delay_frame = (UINT16)ae_cfg->black_delay_frame;
	ae_attr.delay.white_delay_frame = (UINT16)ae_cfg->white_delay_frame;

	ae_attr.anti_flicker.enable = (BOOL)ae_cfg->anti_flicker.enable;
	ae_attr.anti_flicker.frequency = (UINT8)ae_cfg->anti_flicker.frequency;
	ae_attr.anti_flicker.luma_delta = (UINT16)ae_cfg->anti_flicker.luma_delta;

	ae_attr.manual.is_valid = (BOOL)ae_cfg->manual.enabled;
	ae_attr.manual.enable.val = (UINT32)ae_cfg->manual.flag;
	ae_attr.manual.exp_value = (UINT32)ae_cfg->manual.exp_value;
	ae_attr.manual.inttime = (UINT32)ae_cfg->manual.inttime;
	ae_attr.manual.sensor_gain = (UINT32)ae_cfg->manual.sensor_gain;
	ae_attr.manual.isp_gain = (UINT32)ae_cfg->manual.isp_gain;
	ae_attr.manual.sys_gain = (UINT32)ae_cfg->manual.sys_gain;

#ifdef DIP_DBG
	printf("-----[VIDEO_DIP_setAe]-----\n");
	printf("sys_gain_range.max = %d\n", ae_attr.sys_gain_range.max);
	printf("sys_gain_range.min = %d\n", ae_attr.sys_gain_range.min);
	printf("sensor_gain_range.max = %d\n", ae_attr.sensor_gain_range.max);
	printf("sensor_gain_range.min = %d\n", ae_attr.sensor_gain_range.min);
	printf("isp_gain_range.max = %d\n", ae_attr.isp_gain_range.max);
	printf("isp_gain_range.min = %d\n", ae_attr.isp_gain_range.min);
	printf("frame_rate = %f\n", ae_attr.frame_rate);
	printf("slow_frame_rate = %f\n", ae_attr.slow_frame_rate);
	printf("speed = %d\n", ae_attr.speed);
	printf("black_speed_bias = %d\n", ae_attr.black_speed_bias);
	printf("interval = %d\n", ae_attr.interval);
	printf("brightness = %d\n", ae_attr.brightness);
	printf("tolerance = %d\n", ae_attr.tolerance);
	printf("gain_thr_up = %d\n", ae_attr.gain_thr_up);
	printf("gain_thr_down  = %d\n", ae_attr.gain_thr_down);
	printf("strategy.mode = %d\n", ae_attr.strategy.mode);
	printf("strategy.strength  = %d\n", ae_attr.strategy.strength);
	printf("roi.awb_weight = %d\n", ae_attr.roi.awb_weight);
	printf("roi.luma_weight = %d\n", ae_attr.roi.luma_weight);
	printf("delay.black_delay_frame = %d\n", ae_attr.delay.black_delay_frame);
	printf("delay.white_delay_frame = %d\n", ae_attr.delay.white_delay_frame);
	printf("anti_flicker.enable = %d\n", ae_attr.anti_flicker.enable);
	printf("anti_flicker.frequency = %d\n", ae_attr.anti_flicker.frequency);
	printf("anti_flicker.luma_delta = %d\n", ae_attr.anti_flicker.luma_delta);
	printf("fps_mode = %d\n", ae_attr.fps_mode);
	printf("manual.is_valid = %d\n", ae_attr.manual.is_valid);
	printf("manual.enable.val = %d\n", ae_attr.manual.enable.val);
	printf("manual.exp_value = %d\n", ae_attr.manual.exp_value);
	printf("manual.inttime = %d\n", ae_attr.manual.inttime);
	printf("manual.sensor_gain = %d\n", ae_attr.manual.sensor_gain);
	printf("manual.isp_gain = %d\n", ae_attr.manual.isp_gain);
	printf("manual.sys_gain = %d\n", ae_attr.manual.sys_gain);
#endif

	if (g_dip_run == 0) {
		memcpy(&g_default_config.AE, &ae_attr, sizeof(MPI_AE_ATTR_S));
		return ret;
	}

	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_setAeAttr(path_idx, &ae_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}
	memcpy(&g_default_config.AE, &ae_attr, sizeof(MPI_AE_ATTR_S));
	memcpy(&g_high_config.AE, &ae_attr, sizeof(MPI_AE_ATTR_S));

	return MPI_SUCCESS;
}

INT32 VIDEO_DIP_setIso(MPI_DEV dev_idx, AGTX_DIP_ISO_CONF_S *iso_cfg)
{
	INT32 ret = 0;
	MPI_ISO_ATTR_S iso_attr;

	if (iso_cfg->mode == 0) {
		iso_attr.mode = ALG_OPT_AUTO;
	} else if (iso_cfg->mode == 2) {
		iso_attr.mode = ALG_OPT_MANUAL;
	} else {
		printf("AGTX_DIP_ISO_CONF_S mode is out of range\n");
		return MPI_FAILURE;
	}

	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		iso_attr.iso_auto.effective_iso[num] = (INT32)iso_cfg->auto_iso_table[num];
	}
	iso_attr.iso_manual.effective_iso = (INT32)iso_cfg->manual_iso;

#ifdef DIP_DBG
	printf("-----[VIDEO_DIP_setIso]-----\n");
	printf("DIP_ISO mode: %d\n", iso_attr.mode);
	for (int i = 0; i < MPI_ISO_LUT_ENTRY_NUM; i++) {
		printf("Auto effective_iso[%d]: %d\n", i, iso_attr.iso_auto.effective_iso[i]);
	}
	printf("manual effective_iso: %d\n", iso_attr.iso_manual.effective_iso);
#endif

	if (g_dip_run == 0) {
		memcpy(&g_default_config.DIP_ISO, &iso_attr, sizeof(MPI_ISO_ATTR_S));
		return ret;
	}

	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_setIsoAttr(path_idx, &iso_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}
	memcpy(&g_default_config.DIP_ISO, &iso_attr, sizeof(MPI_ISO_ATTR_S));
	memcpy(&g_high_config.DIP_ISO, &iso_attr, sizeof(MPI_ISO_ATTR_S));

	return MPI_SUCCESS;
}

INT32 VIDEO_DIP_setAwb(MPI_DEV dev_idx, AGTX_DIP_AWB_CONF_S *awb_cfg)
{
	INT32 ret = 0;
	MPI_AWB_ATTR_S awb_attr;

	awb_attr.speed = (UINT8)awb_cfg->speed;
	awb_attr.wht_density = (UINT8)awb_cfg->wht_density;
	awb_attr.r_extra_gain = (UINT8)awb_cfg->r_extra_gain;
	awb_attr.b_extra_gain = (UINT8)awb_cfg->b_extra_gain;
	awb_attr.g_extra_gain = (UINT8)awb_cfg->g_extra_gain;
	awb_attr.wht_weight = (UINT8)awb_cfg->wht_weight;
	awb_attr.gwd_weight = (UINT8)awb_cfg->gwd_weight;
	awb_attr.color_tolerance = (UINT8)awb_cfg->color_tolerance;
	awb_attr.max_lum_gain = (UINT8)awb_cfg->max_lum_gain;
	awb_attr.low_k = (UINT16)awb_cfg->low_k;
	awb_attr.high_k = (UINT16)awb_cfg->high_k;
	awb_attr.over_exp_th = (UINT16)awb_cfg->over_exp_th;
	awb_attr.k_table_valid_size = (UINT8)awb_cfg->k_table_valid_size;

	for (INT32 k_num = 0; k_num < MPI_K_TABLE_ENTRY_NUM; k_num++) {
		awb_attr.k_table[k_num].k = (UINT16)awb_cfg->k_table_list[k_num].k;

		for (INT32 num = 0; num < MPI_AWB_CHN_NUM; num++) {
			awb_attr.k_table[k_num].gain[num] = (UINT16)awb_cfg->k_table_list[k_num].gain[num];
			awb_attr.delta_table[k_num].gain[num] = (INT16)awb_cfg->delta_table_list[k_num].gain[num];
		}

		for (INT32 m_num = 0; m_num < (MPI_COLOR_CHN_NUM * MPI_COLOR_CHN_NUM); m_num++) {
			awb_attr.k_table[k_num].matrix[m_num] = (UINT16)awb_cfg->k_table_list[k_num].maxtrix[m_num];
		}

		awb_attr.bias_table[k_num].k = (UINT16)awb_cfg->k_table_bias_list[k_num].k;
		awb_attr.bias_table[k_num].color_tolerance_bias =
		        (UINT16)awb_cfg->k_table_bias_list[k_num].color_tolerance_bias;
		awb_attr.bias_table[k_num].wht_weight_bias = (UINT16)awb_cfg->k_table_bias_list[k_num].wht_weight_bias;
		awb_attr.bias_table[k_num].gwd_weight_bias = (UINT16)awb_cfg->k_table_bias_list[k_num].gwd_weight_bias;
		awb_attr.bias_table[k_num].r_extra_gain_bias =
		        (UINT16)awb_cfg->k_table_bias_list[k_num].r_extra_gain_bias;
		awb_attr.bias_table[k_num].g_extra_gain_bias =
		        (UINT16)awb_cfg->k_table_bias_list[k_num].g_extra_gain_bias;
		awb_attr.bias_table[k_num].b_extra_gain_bias =
		        (UINT16)awb_cfg->k_table_bias_list[k_num].b_extra_gain_bias;
	}

#ifdef DIP_DBG
	printf("-----[VIDEO_DIP_setAwb]-----\n");
	printf("speed = %d \n", awb_attr.speed);
	printf("low_k = %d \n", awb_attr.low_k);
	printf("high_k = %d \n", awb_attr.high_k);
	printf("r_extra_gain = %d \n", awb_attr.r_extra_gain);
	printf("b_extra_gain = %d \n", awb_attr.b_extra_gain);
	printf("g_extra_gain = %d \n", awb_attr.g_extra_gain);
	printf("wht_weight = %d \n", awb_attr.wht_weight);
	printf("gwd_weight = %d \n", awb_attr.gwd_weight);
	printf("color_tolerance = %d \n", awb_attr.color_tolerance);
	printf("max_lum_gain = %d \n", awb_attr.max_lum_gain);
	printf("wht_density = %d \n", awb_attr.wht_density);
	printf("over_exp_th = %d \n", awb_attr.over_exp_th);
	printf("k_table_valid_size = %d \n", awb_attr.k_table_valid_size);

	for (INT32 j = 0; j < MPI_K_TABLE_ENTRY_NUM; j++) {
		printf("k_table[%d].k = %d \n", j, awb_attr.k_table[j].k);

		for (INT32 i = 0; i < MPI_AWB_CHN_NUM; i++) {
			printf("k_table[%d].gain[%d] = %d \n", j, i, awb_attr.k_table[j].gain[i]);
		}
		for (INT32 i = 0; i < 9; i++) {
			printf("k_table[%d].matrix[%d] = %d \n", j, i, awb_attr.k_table[j].matrix[i]);
		}
		for (INT32 i = 0; i < MPI_AWB_CHN_NUM; i++) {
			printf("delta_table[%d].gain[%d] = %d \n", j, i, awb_attr.delta_table[j].gain[i]);
		}
	}

	for (INT32 j = 0; j < MPI_K_TABLE_ENTRY_NUM; j++) {
		printf("k_table_bias[%d].k = %d \n", j, awb_attr.bias_table[j].k);
		printf("k_table_bias[%d].color_tolerance_bias = %d \n", j, awb_attr.bias_table[j].color_tolerance_bias);
		printf("k_table_bias[%d].wht_weight_bias = %d \n", j, awb_attr.bias_table[j].wht_weight_bias);
		printf("k_table_bias[%d].gwd_weight_bias = %d \n", j, awb_attr.bias_table[j].gwd_weight_bias);
		printf("k_table_bias[%d].r_extra_gain_bias = %d \n", j, awb_attr.bias_table[j].r_extra_gain_bias);
		printf("k_table_bias[%d].g_extra_gain_bias = %d \n", j, awb_attr.bias_table[j].g_extra_gain_bias);
		printf("k_table_bias[%d].b_extra_gain_bias = %d \n", j, awb_attr.bias_table[j].b_extra_gain_bias);
	}
#endif

	if (g_dip_run == 0) {
		memcpy(&g_default_config.AWB, &awb_attr, sizeof(MPI_AWB_ATTR_S));
		return ret;
	}

	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_setAwbAttr(path_idx, &awb_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}
	memcpy(&g_default_config.AWB, &awb_attr, sizeof(MPI_AWB_ATTR_S));
	memcpy(&g_high_config.AWB, &awb_attr, sizeof(MPI_AWB_ATTR_S));

	return MPI_SUCCESS;
}

INT32 VIDEO_DIP_setCsm(MPI_DEV dev_idx, AGTX_DIP_CSM_CONF_S *csm_cfg)
{
	INT32 ret = 0;
	MPI_CSM_ATTR_S csm_attr;

	if (csm_cfg->mode == 0) {
		csm_attr.mode = ALG_OPT_AUTO;
	} else if (csm_cfg->mode == 2) {
		csm_attr.mode = ALG_OPT_MANUAL;
	} else {
		printf("AGTX_DIP_CSM_CONF_S mode is out of range\n");
		return MPI_FAILURE;
	}

	csm_attr.bw_en = (UINT8)csm_cfg->bw_en;
	csm_attr.hue_angle = (INT16)csm_cfg->hue;
	csm_attr.csm_manual.saturation = (UINT8)csm_cfg->manual_sat;

	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		csm_attr.csm_auto.saturation[num] = (UINT8)csm_cfg->auto_sat_table[num];
	}

#ifdef DIP_DBG
	printf("-----[VIDEO_DIP_setCsm]-----\n");
	printf("Saturation hue_angle: %d\n", csm_attr.hue_angle);
	printf("Saturation mode: %d\n", csm_attr.mode);
	for (int i = 0; i < MPI_ISO_LUT_ENTRY_NUM; i++) {
		printf("Auto saturation strength[%d]: %d\n", i, csm_attr.csm_auto.saturation[i]);
	}
	printf(" Manual saturation strength: %3d\n", csm_attr.csm_manual.saturation);
#endif

	if (g_dip_run == 0) {
		memcpy(&g_default_config.CSM, &csm_attr, sizeof(MPI_CSM_ATTR_S));
		return ret;
	}

	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_setCsmAttr(path_idx, &csm_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}
	memcpy(&g_default_config.CSM, &csm_attr, sizeof(MPI_CSM_ATTR_S));
	memcpy(&g_high_config.CSM, &csm_attr, sizeof(MPI_CSM_ATTR_S));

	return MPI_SUCCESS;
}

INT32 VIDEO_DIP_setPta(MPI_DEV dev_idx, AGTX_DIP_PTA_CONF_S *pta_cfg)
{
	INT32 ret = 0;
	MPI_PTA_ATTR_S pta_attr;

	if (pta_cfg->mode == 0) {
		pta_attr.mode = PTA_NORMAL;
	} else if (pta_cfg->mode == 1) {
		pta_attr.mode = PTA_MANUAL;
	} else {
		printf("AGTX_DIP_PTA_CONF_S mode is out of range\n");
		return MPI_FAILURE;
	}

	pta_attr.brightness = (UINT8)pta_cfg->brightness;
	pta_attr.contrast = (UINT8)pta_cfg->contrast;
	pta_attr.break_point = (UINT8)pta_cfg->break_point;

	for (INT32 num = 0; num < MPI_PTA_CURVE_ENTRY_NUM; num++) {
		pta_attr.pta_manual.curve[num] = (UINT32)pta_cfg->curve[num];
	}

	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		pta_attr.pta_auto.tone[num] = (UINT32)pta_cfg->auto_tone_table[num];
	}

#ifdef DIP_DBG
	printf("-----[VIDEO_DIP_setPta]-----\n");
	char str[256];
	int offset = 0;
	printf("Mode: %d (0: Noraml, 1:Manual)\n", pta_attr.mode);
	printf("Brightness strength: %3d\n", pta_attr.brightness);
	printf("Contrast strength: %3d\n", pta_attr.contrast);
	printf("Breakpoint of contrast: %3d\n", pta_attr.break_point);
	printf("scene_ratio[%d] = \n", MPI_ISO_LUT_ENTRY_NUM);

	for (int i = 0; i < MPI_ISO_LUT_ENTRY_NUM; i++) {
		offset += sprintf(str + offset, "%4d, ", pta_attr.pta_auto.tone[i]);
		if ((i % 6) == 5) {
			printf("%s\n", str);
			offset = 0;
		}
	}
	printf("%s\n", str);

	char str1[256];
	int offset1 = 0;
	printf("Curve[%d] = \n", MPI_PTA_CURVE_ENTRY_NUM);
	for (int i = 0; i < MPI_PTA_CURVE_ENTRY_NUM; i++) {
		offset1 += sprintf(str1 + offset1, "%4d, ", pta_attr.pta_manual.curve[i]);
		if ((i % 6) == 5) {
			printf("%s\n", str1);
			offset1 = 0;
		}
	}
	printf("%s\n", str1);
#endif

	if (g_dip_run == 0) {
		memcpy(&g_default_config.PTA, &pta_attr, sizeof(MPI_PTA_ATTR_S));
		return ret;
	}

	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_setPtaAttr(path_idx, &pta_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}
	memcpy(&g_default_config.PTA, &pta_attr, sizeof(MPI_PTA_ATTR_S));
	memcpy(&g_high_config.PTA, &pta_attr, sizeof(MPI_PTA_ATTR_S));

	return MPI_SUCCESS;
}

INT32 VIDEO_DIP_setShp(MPI_DEV dev_idx, AGTX_DIP_SHP_CONF_S *shp_cfg)
{
	INT32 ret = 0;
	MPI_SHP_ATTR_S shp_attr;

	if (shp_cfg->mode == 0) {
		shp_attr.mode = ALG_OPT_AUTO;
	} else if (shp_cfg->mode == 2) {
		shp_attr.mode = ALG_OPT_MANUAL;
	} else {
		printf("AGTX_DIP_SHP_CONF_S mode is out of range\n");
		return MPI_FAILURE;
	}

	shp_attr.shp_manual.sharpness = (UINT8)shp_cfg->manual_shp;

	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		shp_attr.shp_auto.sharpness[num] = (UINT8)shp_cfg->auto_shp_table[num];
	}

#ifdef DIP_DBG
	printf("-----[VIDEO_DIP_setShp]-----\n");
	printf("Sharpness mode: %d\n", shp_attr.mode);
	for (int i = 0; i < MPI_ISO_LUT_ENTRY_NUM; i++) {
		printf("Auto sharpness strength[%d]: %d\n", i, shp_attr.shp_auto.sharpness[i]);
	}
	printf("Manual sharpness strength: %3d\n", shp_attr.shp_manual.sharpness);
#endif

	if (g_dip_run == 0) {
		memcpy(&g_default_config.SHP, &shp_attr, sizeof(MPI_SHP_ATTR_S));

		for (INT32 i = 0; i < MAX_VIDEO_CHNNEL * MAX_VIDEO_WINDOW; i++) {
			memcpy(&g_default_config.WIN_SHP[i], &shp_attr, sizeof(MPI_SHP_ATTR_S));
		}
		return ret;
	}

	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_setShpAttr(path_idx, &shp_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}
	memcpy(&g_default_config.SHP, &shp_attr, sizeof(MPI_SHP_ATTR_S));
	memcpy(&g_high_config.SHP, &shp_attr, sizeof(MPI_SHP_ATTR_S));

	for (INT32 i = 0; i < MAX_VIDEO_CHNNEL * MAX_VIDEO_WINDOW; i++) {
		memcpy(&g_default_config.WIN_SHP[i], &shp_attr, sizeof(MPI_SHP_ATTR_S));
		memcpy(&g_high_config.WIN_SHP[i], &shp_attr, sizeof(MPI_SHP_ATTR_S));
	}

	return MPI_SUCCESS;
}

INT32 VIDEO_DIP_setNr(MPI_DEV dev_idx, AGTX_DIP_NR_CONF_S *nr_cfg)
{
	INT32 ret = 0;
	MPI_NR_ATTR_S nr_attr;

	if (nr_cfg->mode == 0) {
		nr_attr.mode = ALG_OPT_AUTO;
	} else if (nr_cfg->mode == 2) {
		nr_attr.mode = ALG_OPT_MANUAL;
	} else {
		printf("AGTX_DIP_NR_CONF_S mode is out of range\n");
		return MPI_FAILURE;
	}

	nr_attr.motion_comp = (UINT8)nr_cfg->motion_comp;
	nr_attr.trail_suppress = (UINT8)nr_cfg->trail_suppress;
	nr_attr.ghost_remove = (UINT8)nr_cfg->ghost_remove;
	nr_attr.ma_y_strength = (UINT8)nr_cfg->ma_y_strength;
	nr_attr.mc_y_strength = (UINT8)nr_cfg->mc_y_strength;
	nr_attr.ma_c_strength = (UINT8)nr_cfg->ma_c_strength;
	nr_attr.ratio_3d = (UINT8)nr_cfg->ratio_3d;
	nr_attr.mc_y_level_offset = (INT16)nr_cfg->mc_y_level_offset;
	nr_attr.me_frame_fallback_en = (UINT8)nr_cfg->me_frame_fallback_en;

	nr_attr.nr_manual.y_level_3d = (UINT8)nr_cfg->manual_y_level_3d;
	nr_attr.nr_manual.c_level_3d = (UINT8)nr_cfg->manual_c_level_3d;
	nr_attr.nr_manual.y_level_2d = (UINT8)nr_cfg->manual_y_level_2d;
	nr_attr.nr_manual.c_level_2d = (UINT8)nr_cfg->manual_c_level_2d;

	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		nr_attr.nr_auto.y_level_3d[num] = (UINT8)nr_cfg->auto_y_level_3d_list[num];
		nr_attr.nr_auto.c_level_3d[num] = (UINT8)nr_cfg->auto_c_level_3d_list[num];
		nr_attr.nr_auto.y_level_2d[num] = (UINT8)nr_cfg->auto_y_level_2d_list[num];
		nr_attr.nr_auto.c_level_2d[num] = (UINT8)nr_cfg->auto_c_level_2d_list[num];
	}

#ifdef DIP_DBG
	printf("-----[VIDEO_DIP_setNr]-----\n");
	printf(" mode = %d\n", nr_attr.mode);

	for (int i = 0; i < MPI_ISO_LUT_ENTRY_NUM; i++) {
		printf("auto_y_level_3d[%d] = %d\n", i, nr_attr.nr_auto.y_level_3d[i]);
		printf("auto_c_level_3d[%d] = %d\n", i, nr_attr.nr_auto.c_level_3d[i]);
		printf("auto_y_level_2d[%d] = %d\n", i, nr_attr.nr_auto.y_level_2d[i]);
		printf("auto_c_level_2d[%d] = %d\n", i, nr_attr.nr_auto.c_level_2d[i]);
	}

	printf("Manual_y_level_3d = %d\n", nr_attr.nr_manual.y_level_3d);
	printf("Manual_c_level_3d = %d\n", nr_attr.nr_manual.c_level_3d);
	printf("Manual_y_level_2d = %d\n", nr_attr.nr_manual.y_level_2d);
	printf("Manual_c_level_2d = %d\n", nr_attr.nr_manual.c_level_2d);

	printf("motion_comp = %d\n", nr_attr.motion_comp);
	printf("trail_suppress = %d\n", nr_attr.trail_suppress);
	printf("ghost_remove = %d\n", nr_attr.ghost_remove);
	printf("ma_y_strength = %d\n", nr_attr.ma_y_strength);
	printf("mc_y_strength = %d\n", nr_attr.mc_y_strength);
	printf("ma_c_strength = %d\n", nr_attr.ma_c_strength);
	printf("ratio_3d = %d\n", nr_attr.ratio_3d);
	printf("mc_y_level_offset = %d\n", nr_attr.mc_y_level_offset);
	printf("me_frame_fallback_en = %d\n", nr_attr.me_frame_fallback_en);
#endif

	if (g_dip_run == 0) {
		memcpy(&g_default_config.NR, &nr_attr, sizeof(MPI_NR_ATTR_S));

		for (INT32 i = 0; i < MAX_VIDEO_CHNNEL * MAX_VIDEO_WINDOW; i++) {
			memcpy(&g_default_config.WIN_NR[i], &nr_attr, sizeof(MPI_NR_ATTR_S));
		}
		return ret;
	}

	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_setNrAttr(path_idx, &nr_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}
	memcpy(&g_default_config.NR, &nr_attr, sizeof(MPI_NR_ATTR_S));
	memcpy(&g_high_config.NR, &nr_attr, sizeof(MPI_NR_ATTR_S));

	for (INT32 i = 0; i < MAX_VIDEO_CHNNEL * MAX_VIDEO_WINDOW; i++) {
		memcpy(&g_default_config.WIN_NR[i], &nr_attr, sizeof(MPI_NR_ATTR_S));
		memcpy(&g_high_config.WIN_NR[i], &nr_attr, sizeof(MPI_NR_ATTR_S));
	}

	return MPI_SUCCESS;
}

INT32 VIDEO_DIP_setWinShp(MPI_WIN win_idx, AGTX_SHP_WINDOW_PARAM_S *shp_cfg)
{
	INT32 ret = 0;
	MPI_SHP_ATTR_S shp_attr;

	if (shp_cfg->mode == 0) {
		shp_attr.mode = ALG_OPT_AUTO;
	} else if (shp_cfg->mode == 2) {
		shp_attr.mode = ALG_OPT_MANUAL;
	} else {
		printf("AGTX_SHP_WINDOW_PARAM_S mode is out of range\n");
		return MPI_FAILURE;
	}

	shp_attr.shp_manual.sharpness = (UINT8)shp_cfg->manual_shp;

	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		shp_attr.shp_auto.sharpness[num] = (UINT8)shp_cfg->auto_shp_table[num];
	}

#ifdef DIP_DBG
	printf("-----[VIDEO_DIP_setWinShp]-----\n");
	printf("Sharpness mode: %d\n", shp_attr.mode);
	for (int i = 0; i < MPI_ISO_LUT_ENTRY_NUM; i++) {
		printf("Auto sharpness strength[%d]: %d\n", i, shp_attr.shp_auto.sharpness[i]);
	}
	printf("Manual sharpness strength: %3d\n", shp_attr.shp_manual.sharpness);
#endif

	if (g_dip_run == 0) {
		memcpy(&g_default_config.WIN_SHP[(win_idx.chn * MAX_VIDEO_WINDOW) + win_idx.win], &shp_attr,
		       sizeof(MPI_SHP_ATTR_S));
		return ret;
	}

	ret = MPI_setWinShpAttr(win_idx, &shp_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}
	memcpy(&g_default_config.WIN_SHP[(win_idx.chn * MAX_VIDEO_WINDOW) + win_idx.win], &shp_attr,
	       sizeof(MPI_SHP_ATTR_S));
	memcpy(&g_high_config.WIN_SHP[(win_idx.chn * MAX_VIDEO_WINDOW) + win_idx.win], &shp_attr,
	       sizeof(MPI_SHP_ATTR_S));

	return MPI_SUCCESS;
}

INT32 VIDEO_DIP_setWinNr(MPI_WIN win_idx, AGTX_NR_WINDOW_PARAM_S *nr_cfg)
{
	INT32 ret = 0;
	MPI_NR_ATTR_S nr_attr;

	if (nr_cfg->mode == 0) {
		nr_attr.mode = ALG_OPT_AUTO;
	} else if (nr_cfg->mode == 2) {
		nr_attr.mode = ALG_OPT_MANUAL;
	} else {
		printf("AGTX_NR_WINDOW_PARAM_S mode is out of range\n");
		return MPI_FAILURE;
	}

	nr_attr.motion_comp = (UINT8)nr_cfg->motion_comp;
	nr_attr.trail_suppress = (UINT8)nr_cfg->trail_suppress;
	nr_attr.ghost_remove = (UINT8)nr_cfg->ghost_remove;
	nr_attr.ma_y_strength = (UINT8)nr_cfg->ma_y_strength;
	nr_attr.mc_y_strength = (UINT8)nr_cfg->mc_y_strength;
	nr_attr.ma_c_strength = (UINT8)nr_cfg->ma_c_strength;
	nr_attr.ratio_3d = (UINT8)nr_cfg->ratio_3d;
	nr_attr.mc_y_level_offset = (INT16)nr_cfg->mc_y_level_offset;
	nr_attr.me_frame_fallback_en = (UINT8)nr_cfg->me_frame_fallback_en;

	nr_attr.nr_manual.y_level_3d = (UINT8)nr_cfg->manual_y_level_3d;
	nr_attr.nr_manual.c_level_3d = (UINT8)nr_cfg->manual_c_level_3d;
	nr_attr.nr_manual.y_level_2d = (UINT8)nr_cfg->manual_y_level_2d;
	nr_attr.nr_manual.c_level_2d = (UINT8)nr_cfg->manual_c_level_2d;

	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		nr_attr.nr_auto.y_level_3d[num] = (UINT8)nr_cfg->auto_y_level_3d_list[num];
		nr_attr.nr_auto.c_level_3d[num] = (UINT8)nr_cfg->auto_c_level_3d_list[num];
		nr_attr.nr_auto.y_level_2d[num] = (UINT8)nr_cfg->auto_y_level_2d_list[num];
		nr_attr.nr_auto.c_level_2d[num] = (UINT8)nr_cfg->auto_c_level_2d_list[num];
	}

#ifdef DIP_DBG
	printf("-----[VIDEO_DIP_setWinNr]-----\n");
	printf(" mode = %d\n", nr_attr.mode);

	for (int i = 0; i < MPI_ISO_LUT_ENTRY_NUM; i++) {
		printf("auto_y_level_3d[%d] = %d\n", i, nr_attr.nr_auto.y_level_3d[i]);
		printf("auto_c_level_3d[%d] = %d\n", i, nr_attr.nr_auto.c_level_3d[i]);
		printf("auto_y_level_2d[%d] = %d\n", i, nr_attr.nr_auto.y_level_2d[i]);
		printf("auto_c_level_2d[%d] = %d\n", i, nr_attr.nr_auto.c_level_2d[i]);
	}

	printf("Manual_y_level_3d = %d\n", nr_attr.nr_manual.y_level_3d);
	printf("Manual_c_level_3d = %d\n", nr_attr.nr_manual.c_level_3d);
	printf("Manual_y_level_2d = %d\n", nr_attr.nr_manual.y_level_2d);
	printf("Manual_c_level_2d = %d\n", nr_attr.nr_manual.c_level_2d);

	printf("motion_comp = %d\n", nr_attr.motion_comp);
	printf("trail_suppress = %d\n", nr_attr.trail_suppress);
	printf("ghost_remove = %d\n", nr_attr.ghost_remove);
	printf("ma_y_strength = %d\n", nr_attr.ma_y_strength);
	printf("mc_y_strength = %d\n", nr_attr.mc_y_strength);
	printf("ma_c_strength = %d\n", nr_attr.ma_c_strength);
	printf("ratio_3d = %d\n", nr_attr.ratio_3d);
	printf("mc_y_level_offset = %d\n", nr_attr.mc_y_level_offset);
	printf("me_frame_fallback_en = %d\n", nr_attr.me_frame_fallback_en);
#endif

	if (g_dip_run == 0) {
		memcpy(&g_default_config.WIN_NR[(win_idx.chn * MAX_VIDEO_WINDOW) + win_idx.win], &nr_attr,
		       sizeof(MPI_NR_ATTR_S));
		return ret;
	}

	ret = MPI_setWinNrAttr(win_idx, &nr_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}
	memcpy(&g_default_config.WIN_NR[(win_idx.chn * MAX_VIDEO_WINDOW) + win_idx.win], &nr_attr,
	       sizeof(MPI_NR_ATTR_S));
	memcpy(&g_high_config.WIN_NR[(win_idx.chn * MAX_VIDEO_WINDOW) + win_idx.win], &nr_attr, sizeof(MPI_NR_ATTR_S));

	return MPI_SUCCESS;
}

INT32 VIDEO_DIP_setTe(MPI_DEV dev_idx, AGTX_DIP_TE_CONF_S *te_cfg)
{
	INT32 ret = 0;
	MPI_TE_ATTR_S te_attr;

	if (te_cfg->mode == 0) {
		te_attr.mode = TE_NORMAL;
	} else if (te_cfg->mode == 1) {
		te_attr.mode = TE_WDR;
	} else {
		printf("AGTX_DIP_TE_CONF_S mode is out of range\n");
		return MPI_FAILURE;
	}

	for (INT32 num = 0; num < MPI_TE_CURVE_ENTRY_NUM; num++) {
		te_attr.te_normal.curve[num] = (UINT32)te_cfg->normal_ctl[num];
	}

	te_attr.te_wdr.brightness = (UINT16)te_cfg->wdr_ctl.brightness;
	te_attr.te_wdr.strength = (UINT16)te_cfg->wdr_ctl.strength;
	te_attr.te_wdr.saliency = (UINT16)te_cfg->wdr_ctl.saliency;
	te_attr.te_wdr.iso_weight = (UINT8)te_cfg->wdr_ctl.iso_weight;
	te_attr.te_wdr.dark_enhance = (UINT8)te_cfg->wdr_ctl.dark_enhance;
	te_attr.te_wdr.iso_max = (UINT32)te_cfg->wdr_ctl.iso_max;
	te_attr.te_wdr.interval = (UINT8)te_cfg->wdr_ctl.interval;
	te_attr.te_wdr.precision = (UINT8)te_cfg->wdr_ctl.precision;

	for (INT32 num = 0; num < MPI_ISO_LUT_ENTRY_NUM; num++) {
		te_attr.te_wdr.noise_cstr[num] = (UINT16)te_cfg->wdr_ctl.noise_cstr[num];
	}

#ifdef DIP_DBG
	char str[512];
	int offset = 0;
	printf("-----[VIDEO_DIP_setTe]-----\n");
	printf("TE mode: %d (0: Normal, 1: WDR)\n", te_attr.mode);
	printf("Curve[%d] = \n", MPI_TE_CURVE_ENTRY_NUM);

	for (int i = 0; i < MPI_TE_CURVE_ENTRY_NUM; i++) {
		offset += sprintf(str + offset, "%6d, ", te_attr.te_normal.curve[i]);
		if ((i % 6) == 5) {
			printf("%s\n", str);
			offset = 0;
		}
	}

	printf("brightness = %d\n", te_attr.te_wdr.brightness);
	printf("strength = %d\n", te_attr.te_wdr.strength);
	printf("saliency = %d\n", te_attr.te_wdr.saliency);
	//printf("noise_cstr = %d\n", te_attr.te_wdr.noise_cstr);
	for (int i = 0; i < MPI_ISO_LUT_ENTRY_NUM; i++) {
		printf("noise_cstr[%d]: %d\n", i, te_attr.te_wdr.noise_cstr[i]);
	}
	printf("iso_weight = %d\n", te_attr.te_wdr.iso_weight);
	printf("dark_enhance = %d\n", te_attr.te_wdr.dark_enhance);
	printf("iso_max = %d\n", te_attr.te_wdr.iso_max);
	printf("interval = %d\n", te_attr.te_wdr.interval);
	printf("precision = %d\n", te_attr.te_wdr.precision);
#endif

	if (g_dip_run == 0) {
		memcpy(&g_default_config.TE, &te_attr, sizeof(MPI_TE_ATTR_S));
		return ret;
	}

	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_setTeAttr(path_idx, &te_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}
	memcpy(&g_default_config.TE, &te_attr, sizeof(MPI_TE_ATTR_S));
	memcpy(&g_high_config.TE, &te_attr, sizeof(MPI_TE_ATTR_S));

	return MPI_SUCCESS;
}

INT32 VIDEO_DIP_setGamma(MPI_DEV dev_idx, AGTX_DIP_GAMMA_CONF_S *gamma_cfg)
{
	INT32 ret = 0;
	MPI_GAMMA_ATTR_S gamma_attr;

	if (gamma_cfg->gamma == 0) {
		gamma_attr.mode = GAMMA_BT709;
	} else if (gamma_cfg->gamma == 1) {
		gamma_attr.mode = GAMMA_CRT;
	} else {
		printf("AGTX_DIP_GAMMA_CONF_S mode is out of range\n");
		return MPI_FAILURE;
	}

#ifdef DIP_DBG
	printf("-----[VIDEO_DIP_setGamma]-----\n");
	printf("Gamma mode: %d (0:bt709, 1:gamma 2.2)\n", gamma_attr.mode);
#endif

	if (g_dip_run == 0) {
		memcpy(&g_default_config.GAMMA, &gamma_attr, sizeof(MPI_GAMMA_ATTR_S));
		return ret;
	}

	MPI_PATH path_idx = MPI_INPUT_PATH(dev_idx.dev, 0);

	ret = MPI_setGammaAttr(path_idx, &gamma_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}
	memcpy(&g_default_config.GAMMA, &gamma_attr, sizeof(MPI_GAMMA_ATTR_S));
	memcpy(&g_high_config.GAMMA, &gamma_attr, sizeof(MPI_GAMMA_ATTR_S));

	return MPI_SUCCESS;
}
#ifdef __cplusplus
}
#endif /* __cplusplus */
