#ifdef __cplusplus
extern "C" {
#endif /**< __cplusplus */

#include "mpi_types.h"
#include "mpi_common.h"
#include "mpi_sys.h"

#include "cfg_api.h"
#include "mtk_common.h"
//#include "config_api.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int g_mpi_sys_initialized = 0;

INT32 VIDEO_SYS_initSystem(CFG_CAMERA_S *cfg)
{
	INT32 ret = 0;
	INT32 i;
	INT32 pool_cnt = 0;
	INT32 width = 0;
	INT32 height = 0;
	INT32 size = 0;
	MPI_VB_CONF_S vb_conf;
	CFG_VIN_S *cfg_vin;
	char pool_name[25];
	memset(&vb_conf, 0, sizeof(MPI_VB_CONF_S));

	vb_conf.max_pool_cnt = 32;

	for (i = 0; i < MAX_VIDEO_INPUT; i++) {
		// TODO: check if camera enable
		cfg_vin = &cfg[i].cfg_vin;
		/* Config TMV buffer */
		width = cfg_vin->cfg_dev.input_path[0].width;
		height = cfg_vin->cfg_dev.input_path[0].height;
		if (cfg_vin->cfg_dev.input_path[1].path_en) {
			if (width != cfg_vin->cfg_dev.input_path[1].width ||
			    height != cfg_vin->cfg_dev.input_path[1].height) {
				SYS_TRACE("Resolution for two sensors are not the same.\n");
				return -1;
			}
			width += width;
		}

		size = ((width + 15) / 16) * ((height + 15) / 16) * 16 + 16392;

		vb_conf.pub_pool[pool_cnt].blk_cnt = 3;
		vb_conf.pub_pool[pool_cnt].blk_size = size;
		sprintf(pool_name, "isp_TMV_%d", pool_cnt);
		strcpy((char *)vb_conf.pub_pool[pool_cnt].name, pool_name);

		pool_cnt++;
	}

	ret = MPI_SYS_init();
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Initialize system failed.\n");
		return MPI_FAILURE;
	}

	g_mpi_sys_initialized = 1;

	ret = MPI_VB_setConf(&vb_conf);
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Configure video buffer failed.\n");
		return MPI_FAILURE;
	}

	ret = MPI_VB_init();
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Initialize video buffer failed.\n");
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 VIDEO_SYS_exitSystem(VOID)
{
	INT32 ret = 0;

	ret = MPI_VB_exit();
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Exit video buffer failed.\n");
		return MPI_FAILURE;
	}
	ret = MPI_SYS_exit();
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Exit system failed.\n");
		return MPI_FAILURE;
	}

	g_mpi_sys_initialized = 0;

	return MPI_SUCCESS;
}

#ifdef __cplusplus
}
#endif /**< __cplusplus */
