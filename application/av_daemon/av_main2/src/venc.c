#ifdef __cplusplus
extern "C" {
#endif /**< __cplusplus */

#include "mpi_types.h"
#include "mpi_common.h"
#include "mpi_enc.h"

#include "cfg_api.h"
#include "mtk_common.h"
//#include "config_api.h"

#include <stdlib.h>
#include <stdio.h>

static INT32 getVencType(UINT32 venc_type)
{
	INT32 type = 0;

	switch (venc_type) {
	case AGTX_VENC_TYPE_H264:
		type = MPI_VENC_TYPE_H264;
		break;
	case AGTX_VENC_TYPE_H265:
		type = MPI_VENC_TYPE_H265;
		break;
	case AGTX_VENC_TYPE_MJPEG:
		type = MPI_VENC_TYPE_MJPEG;
		break;
	default:
		type = MPI_VENC_TYPE_H264;
		break;
	}

	return type;
}

static INT32 getVencRcMode(UINT32 rc_mode)
{
	INT32 mode = 0;

	switch (rc_mode) {
	case AGTX_RC_MODE_VBR:
		mode = MPI_RC_MODE_VBR;
		break;
	case AGTX_RC_MODE_CBR:
		mode = MPI_RC_MODE_CBR;
		break;
	case AGTX_RC_MODE_SBR:
		mode = MPI_RC_MODE_SBR;
		break;
	case AGTX_RC_MODE_CQP:
		mode = MPI_RC_MODE_CQP;
		break;
	default:
		mode = MPI_RC_MODE_CBR;
		break;
	}

	return mode;
}

static INT32 getVencProfile(UINT32 venc_profile)
{
	INT32 profile = 0;

	switch (venc_profile) {
	case AGTX_PRFL_BASELINE:
		profile = MPI_PRFL_BASELINE;
		break;
	case AGTX_PRFL_MAIN:
		profile = MPI_PRFL_MAIN;
		break;
	case AGTX_PRFL_HIGH:
		profile = MPI_PRFL_HIGH;
		break;
	default:
		profile = MPI_PRFL_BASELINE;
		break;
	}

	return profile;
}

static VOID setVencAttr(MPI_VENC_ATTR_S *attr, CFG_VENC_S *conf)
{
	attr->type = getVencType(conf->strm.venc_type);

	switch (attr->type) {
	case AGTX_VENC_TYPE_H264:
		attr->h264.profile = getVencProfile(conf->strm.venc_profile);
		attr->h264.rc.mode = getVencRcMode(conf->strm.rc_mode);
		attr->h264.rc.gop = conf->strm.gop_size;
		attr->h264.rc.frm_rate_o = conf->strm.output_fps;

		if (attr->h264.rc.mode == MPI_RC_MODE_VBR) {
			attr->h264.rc.vbr.max_bit_rate = conf->strm.vbr_max_bit_rate;
			attr->h264.rc.vbr.quality_level_index = conf->strm.vbr_quality_level_index;
			attr->h264.rc.vbr.regression_speed = conf->strm.regression_speed;
			attr->h264.rc.vbr.scene_smooth = conf->strm.scene_smooth;
			attr->h264.rc.vbr.fluc_level = conf->strm.fluc_level;
			attr->h264.rc.vbr.i_continue_weight = conf->strm.i_continue_weight;
			attr->h264.rc.vbr.max_qp = conf->strm.max_qp;
			attr->h264.rc.vbr.i_qp_offset = conf->strm.i_qp_offset;
		} else if (attr->h264.rc.mode == MPI_RC_MODE_CBR) {
			attr->h264.rc.cbr.bit_rate = conf->strm.bit_rate;
			attr->h264.rc.cbr.min_qp = conf->strm.min_qp;
			attr->h264.rc.cbr.max_qp = conf->strm.max_qp;
			attr->h264.rc.cbr.regression_speed = conf->strm.regression_speed;
			attr->h264.rc.cbr.scene_smooth = conf->strm.scene_smooth;
			attr->h264.rc.cbr.fluc_level = conf->strm.fluc_level;
			attr->h264.rc.cbr.i_continue_weight = conf->strm.i_continue_weight;
			attr->h264.rc.cbr.i_qp_offset = conf->strm.i_qp_offset;
		} else if (attr->h264.rc.mode == MPI_RC_MODE_SBR) {
			attr->h264.rc.sbr.bit_rate = conf->strm.bit_rate;
			attr->h264.rc.sbr.min_qp = conf->strm.min_qp;
			attr->h264.rc.sbr.max_qp = conf->strm.max_qp;
			attr->h264.rc.sbr.regression_speed = conf->strm.regression_speed;
			attr->h264.rc.sbr.scene_smooth = conf->strm.scene_smooth;
			attr->h264.rc.sbr.fluc_level = conf->strm.fluc_level;
			attr->h264.rc.sbr.i_continue_weight = conf->strm.i_continue_weight;
			attr->h264.rc.sbr.i_qp_offset = conf->strm.i_qp_offset;
			attr->h264.rc.sbr.adjust_br_thres_pc = conf->strm.sbr_adjust_br_thres_pc;
			attr->h264.rc.sbr.adjust_step_times = conf->strm.sbr_adjust_step_times;
			attr->h264.rc.sbr.converge_frame = conf->strm.sbr_converge_frame;
		} else if (attr->h264.rc.mode == MPI_RC_MODE_CQP) {
			attr->h264.rc.cqp.i_frame_qp = conf->strm.cqp_i_frame_qp;
			attr->h264.rc.cqp.p_frame_qp = conf->strm.cqp_p_frame_qp;
		} else {
			SYS_TRACE("Invalid rate control mode.\n");
		}

		break;

	case AGTX_VENC_TYPE_H265:
		attr->h265.profile = getVencProfile(conf->strm.venc_profile);
		attr->h265.rc.mode = getVencRcMode(conf->strm.rc_mode);
		attr->h265.rc.gop = conf->strm.gop_size;
		attr->h265.rc.frm_rate_o = conf->strm.output_fps;

		if (attr->h265.rc.mode == MPI_RC_MODE_VBR) {
			attr->h265.rc.vbr.max_bit_rate = conf->strm.vbr_max_bit_rate;
			attr->h265.rc.vbr.quality_level_index = conf->strm.vbr_quality_level_index;
			attr->h265.rc.vbr.regression_speed = conf->strm.regression_speed;
			attr->h265.rc.vbr.scene_smooth = conf->strm.scene_smooth;
			attr->h265.rc.vbr.fluc_level = conf->strm.fluc_level;
			attr->h265.rc.vbr.i_continue_weight = conf->strm.i_continue_weight;
			attr->h265.rc.vbr.max_qp = conf->strm.max_qp;
			attr->h265.rc.vbr.i_qp_offset = conf->strm.i_qp_offset;
		} else if (attr->h265.rc.mode == MPI_RC_MODE_CBR) {
			attr->h265.rc.cbr.bit_rate = conf->strm.bit_rate;
			attr->h265.rc.cbr.min_qp = conf->strm.min_qp;
			attr->h265.rc.cbr.max_qp = conf->strm.max_qp;
			attr->h265.rc.cbr.regression_speed = conf->strm.regression_speed;
			attr->h265.rc.cbr.scene_smooth = conf->strm.scene_smooth;
			attr->h265.rc.cbr.fluc_level = conf->strm.fluc_level;
			attr->h265.rc.cbr.i_continue_weight = conf->strm.i_continue_weight;
			attr->h265.rc.cbr.i_qp_offset = conf->strm.i_qp_offset;
		} else if (attr->h265.rc.mode == MPI_RC_MODE_SBR) {
			attr->h265.rc.sbr.bit_rate = conf->strm.bit_rate;
			attr->h265.rc.sbr.min_qp = conf->strm.min_qp;
			attr->h265.rc.sbr.max_qp = conf->strm.max_qp;
			attr->h265.rc.sbr.regression_speed = conf->strm.regression_speed;
			attr->h265.rc.sbr.scene_smooth = conf->strm.scene_smooth;
			attr->h265.rc.sbr.fluc_level = conf->strm.fluc_level;
			attr->h265.rc.sbr.i_continue_weight = conf->strm.i_continue_weight;
			attr->h265.rc.sbr.i_qp_offset = conf->strm.i_qp_offset;
			attr->h265.rc.sbr.adjust_br_thres_pc = conf->strm.sbr_adjust_br_thres_pc;
			attr->h265.rc.sbr.adjust_step_times = conf->strm.sbr_adjust_step_times;
			attr->h265.rc.sbr.converge_frame = conf->strm.sbr_converge_frame;
		} else if (attr->h265.rc.mode == MPI_RC_MODE_CQP) {
			attr->h265.rc.cqp.i_frame_qp = conf->strm.cqp_i_frame_qp;
			attr->h265.rc.cqp.p_frame_qp = conf->strm.cqp_p_frame_qp;
		} else {
			SYS_TRACE("Invalid rate control mode.\n");
		}

		break;

	case AGTX_VENC_TYPE_MJPEG:
		attr->mjpeg.rc.mode = getVencRcMode(conf->strm.rc_mode);
		attr->mjpeg.rc.frm_rate_o = conf->strm.output_fps;
		attr->mjpeg.rc.max_bit_rate = conf->strm.vbr_max_bit_rate;
		attr->mjpeg.rc.quality_level_index = conf->strm.vbr_quality_level_index;
		attr->mjpeg.rc.fluc_level = conf->strm.fluc_level;
		attr->mjpeg.rc.bit_rate = conf->strm.bit_rate;
		attr->mjpeg.rc.max_q_factor = conf->max_q_factor;
		attr->mjpeg.rc.min_q_factor = conf->min_q_factor;
		attr->mjpeg.rc.adjust_br_thres_pc = conf->strm.sbr_adjust_br_thres_pc;
		attr->mjpeg.rc.adjust_step_times = conf->strm.sbr_adjust_step_times;
		attr->mjpeg.rc.converge_frame = conf->strm.sbr_converge_frame;
		attr->mjpeg.rc.q_factor = conf->cqp_q_factor;
		break;

	default:
		SYS_TRACE("Invalid code type.\n");
		break;
	}
}

INT32 VIDEO_ENC_setEncChn(CFG_VENC_S *conf)
{
	INT32 ret = MPI_FAILURE;
	MPI_ECHN chn_idx = MPI_ENC_CHN(conf->chn_idx);
	MPI_VENC_ATTR_S venc_attr = { 0 };
	MPI_VENC_ATTR_EX_S venc_attr_ex = { 0 };

	ret = MPI_ENC_getVencAttr(chn_idx, &venc_attr);
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Get encoder attribute channel %d failed.\n", chn_idx.chn);
		return MPI_FAILURE;
	}

	setVencAttr(&venc_attr, conf);

	ret = MPI_ENC_setVencAttr(chn_idx, &venc_attr);
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Set encoder attribute channel %d failed.\n", chn_idx.chn);
		return MPI_FAILURE;
	}

	ret = MPI_ENC_getVencAttrEx(chn_idx, &venc_attr_ex);
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Get VENC attr EX for encoder channel %d failed.\n", chn_idx.chn);
		return MPI_FAILURE;
	}

	venc_attr_ex.obs = conf->strm.obs;

	ret = MPI_ENC_setVencAttrEx(chn_idx, &venc_attr_ex);
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Set VENC attr EX for encoder channel %d failed.\n", chn_idx.chn);
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 VIDEO_ENC_startEncChn(CFG_VENC_S *conf)
{
	INT32 ret = MPI_FAILURE;
	MPI_ECHN chn_idx = MPI_ENC_CHN(conf->chn_idx);

	MPI_ENC_CHN_ATTR_S chn_attr;
	MPI_ENC_BIND_INFO_S bind_info;
	MPI_VENC_ATTR_S venc_attr = { 0 };
	MPI_VENC_ATTR_EX_S venc_attr_ex = { 0 };

	chn_attr.res.width = conf->strm.width;
	chn_attr.res.height = conf->strm.height;
	chn_attr.max_res.width = conf->max_width;
	chn_attr.max_res.height = conf->max_height;

	ret = MPI_ENC_createChn(chn_idx, &chn_attr);
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Create encoder channel %d failed.\n", chn_idx.chn);
		return MPI_FAILURE;
	}

	bind_info.idx = MPI_VIDEO_CHN(conf->bind_dev_idx, conf->bind_chn_idx);

	ret = MPI_ENC_bindToVideoChn(chn_idx, &bind_info);
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Bind encoder channel %d failed.\n", chn_idx.chn);
		return MPI_FAILURE;
	}

	setVencAttr(&venc_attr, conf);

	ret = MPI_ENC_setVencAttr(chn_idx, &venc_attr);
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Set VENC attr for encoder channel %d failed.\n", chn_idx.chn);
		return MPI_FAILURE;
	}

	venc_attr_ex.obs = conf->strm.obs;
	venc_attr_ex.obs_off_period = conf->strm.obs_off_period;

	ret = MPI_ENC_setVencAttrEx(chn_idx, &venc_attr_ex);
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Set VENC attr EX for encoder channel %d failed.\n", chn_idx.chn);
		return MPI_FAILURE;
	}

	ret = MPI_ENC_startChn(chn_idx);
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Start encoder channel %d failed.\n", chn_idx.chn);
		return MPI_FAILURE;
	}

	SYS_TRACE("Start encoder channel %d succeeded!\n", chn_idx.chn);

	return MPI_SUCCESS;
}

INT32 VIDEO_ENC_stopEncChn(CFG_VENC_S *conf)
{
	INT32 ret = MPI_FAILURE;
	MPI_ECHN chn_idx = MPI_ENC_CHN(conf->chn_idx);

	ret = MPI_ENC_stopChn(chn_idx);
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Stop encoder channel %d failed.\n", chn_idx.chn);
		return MPI_FAILURE;
	}

	ret = MPI_ENC_unbindFromVideoChn(chn_idx);
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Unbind encoder channel %d failed.\n", chn_idx.chn);
		return MPI_FAILURE;
	}

	ret = MPI_ENC_destroyChn(chn_idx);
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Destroy encoder channel %d failed.\n", chn_idx.chn);
		return MPI_FAILURE;
	}

	SYS_TRACE("Stop encoder channel %d succeeded!\n", chn_idx.chn);

	return MPI_SUCCESS;
}

#ifdef __cplusplus
}
#endif /**< __cplusplus */
