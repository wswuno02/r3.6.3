/*
 * AUGENTIX INC. - PROPRIETARY
 *
 * osd.c - Reference code for displaying OSD
 * Copyright (C) 2018-2019 Augentix Inc. - All Rights Reserved
 *
 * NOTICE: The information contained herein is the property of Augentix Inc.
 * Copying and distributing of this file, via any medium,
 * must be licensed by Augentix Inc.
 *
 * * Brief: Reference code for displaying OSD,
 * *        including Titie, Date and Time, Logo and Notifications
 * *
 * * Author: Allen Chin <allen.chin@augentix.com>
 */

#include "mtk_common.h"
#include "osd.h"
#include "mpi_common.h"
#include "mpi_dip_common.h"
#include "mpi_dip_sns.h"
#include "mpi_sys.h"
#include "mpi_osd.h"
#include "sample_sys.h"
#include "sample_dip.h"
#include "sensor.h"
#include "alarm_icon.h"
#include "agtx_prio.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <assert.h>
#include <poll.h>
#include <tz_upd.h>

#define OSD_TH_NAME "datetime"
#define HANDLE_IDX_TOUCH 4

#define HANDLE_IDX_TIME 0
#define HANDLE_IDX_LOGO 1
#define HANDLE_IDX_TEXT 2
#define HANDLE_IDX_ALARM 3

#define MAX(x, y) ((x) > (y)) ? (x) : (y)
#define MIN(x, y) ((x) < (y)) ? (x) : (y)

//#define OSD_DIS_INFO
#ifdef OSD_DIS_INFO
#include <sys/stat.h>
#define OSD_DIS_PATH "./osd.txt"
#endif /* OSD_DIS_INFO */

//#define OSD_DEBUG
#ifdef OSD_DEBUG
#define osd_fmt(fmt) "[OSD][DEBUG] [%s:%d]" fmt
#define OSD_DEB(fmt, args...) printf(osd_fmt(fmt), __func__, __LINE__, ##args)
#else
#define OSD_DEB(fmt, args...)
#endif /* OSD_DEBUG */

typedef struct {
	INT32 showWeekDay;
	INT32 privMaskAreaMax[SAMPLE_MAX_VIDEO_CHN_NUM]; /**< Maximum Boundary for different Chns **/
	UINT8 bind_state[SAMPLE_MAX_VIDEO_CHN_NUM][VIDEO_OSD_MAX_HANDLE_CHANNEL]; /**< External Control interface **/
	UINT8 bind_state_internal[SAMPLE_MAX_VIDEO_CHN_NUM]
	                         [VIDEO_OSD_MAX_HANDLE_CHANNEL]; /**< Internal Control for create & destory **/
	OSD_HANDLE handle[SAMPLE_MAX_VIDEO_CHN_NUM][VIDEO_OSD_MAX_HANDLE_CHANNEL]; /**< Handle for OSD Region */
	MPI_OSD_BIND_ATTR_S osd_bind[SAMPLE_MAX_VIDEO_CHN_NUM]
	                            [VIDEO_OSD_MAX_HANDLE_CHANNEL]; /**< OSD Bind attr for OSD Region */
	MPI_RECT_POINT_S osd_rgn[SAMPLE_MAX_VIDEO_CHN_NUM][VIDEO_OSD_MAX_HANDLE_CHANNEL]; /**< OSD region location */
} VIDEO_OSD_HANDLE;

static VIDEO_OSD_HANDLE g_osd_handle = { 0 };

static INT32 g_osd_touchChn[SAMPLE_MAX_VIDEO_CHN_NUM] = { 0 };

char *g_osd_font_data;
ASCII_INDEX *g_osd_font_index;

static int g_prev_showWeekDay = -1;
static int g_prev_weekDay = -1;

DateFormat_E g_date_format = OSD_DATE_FORMAT_YYYY_MM_DD;
TimeFormat_E g_time_format = OSD_TIME_FORMAT_HH_MM_SS;

char *pic_image_pointer = NULL;
pthread_t threadUpdateDateTime;
UINT32 image_width;
UINT32 image_height;
UINT32 channel_numbers;
UINT32 pic_image_width;
UINT32 pic_image_height;
UINT32 pic_image_size;
UINT32 g_info_index[SAMPLE_MAX_VIDEO_CHN_NUM] = { 0 };
UINT32 g_active_font[SAMPLE_MAX_VIDEO_CHN_NUM] = { 0 };
UINT32 g_active_alarm[SAMPLE_MAX_VIDEO_CHN_NUM] = { 0 };

OSD_FONT_INFO_S g_fontInfo[MAX_FONT_FORMAT] = {
	{ "/system/mpp/font/0_359_font.ayuv", NULL, NULL, 0, 0, 359, 304, 32 },
	{ "/system/mpp/font/360_959_font.ayuv", NULL, NULL, 0, 360, 959, 448, 48 },
	{ "/system/mpp/font/960_1943_font.ayuv", NULL, NULL, 0, 960, 1943, 864, 80 },
	{ "/system/mpp/font/1944_unlimted_font.ayuv", NULL, NULL, 0, 1944, 0xFFFFFFFF, 1600, 144 }
};

OSD_ALARM_INFO_S g_alarmInfo[MAX_ALARM_FORMAT] = { { 0, 1079, 384, 0 }, { 1080, 0xffff, 683, 0 } };

MPI_OSD_CANVAS_ATTR_S p_canvas_attr_time[SAMPLE_MAX_VIDEO_CHN_NUM] = { { 0 } };
MPI_OSD_CANVAS_ATTR_S p_canvas_attr_logo_ch0 = { 0 };
MPI_OSD_CANVAS_ATTR_S p_canvas_attr_logo_ch1 = { 0 };
MPI_OSD_CANVAS_ATTR_S p_canvas_attr_logo_ch2 = { 0 };
MPI_OSD_CANVAS_ATTR_S p_canvas_attr_number_ch0 = { 0 };
MPI_OSD_CANVAS_ATTR_S p_canvas_attr_number_ch1 = { 0 };
MPI_OSD_CANVAS_ATTR_S p_canvas_attr_number_ch2 = { 0 };
MPI_OSD_CANVAS_ATTR_S p_canvas_attr_alarm_ch0 = { 0 };
MPI_OSD_CANVAS_ATTR_S p_canvas_attr_alarm_ch1 = { 0 };
MPI_OSD_CANVAS_ATTR_S p_canvas_attr_alarm_ch2 = { 0 };

UINT16 AYUV_TABLE[CR_MAX] = {
	0b1110000010001000, // AYUV_BLACK
	0b1110100101011111, // AYUV_RED
	0b1111001000110001, // AYUV_GREEN
	0b1110001111110110, // AYUV_BLUE
	0b1111101100001001, // AYUV_YELLOW
	0b1110101010111011, // AYUV_PURPLE
	0b1111011000001011, // AYUV_ORANGE
	0b1111111110001000 // AYUV_WHITE
};

/**
 * @brief Get AYUV color by color index VIDEO_OSD_CR_E
 * @param[in] color_index      color in VIDEO_OSD_CR_E.
 * @retval 16bit AYUV color
 * @retval 0 invalid color index.
 */
UINT16 VIDEO_OSD_getAyuvColor(INT32 color_index)
{
	if (color_index < CR_MAX) {
		return AYUV_TABLE[color_index];
	}
	return 0;
}

static INT32 osdCheckChnBindState(INT32 c_idx)
{
	INT32 i;
	INT32 binded_handle = 0;
	for (i = 0; i < VIDEO_OSD_MAX_HANDLE_CHANNEL; i++) {
		binded_handle += (g_osd_handle.bind_state[c_idx][i] > 0) ? 1 : 0;
	}
	OSD_DEB("bindstate sum on chn:%d is %d vs MAX handle per channel %d\n", c_idx, binded_handle,
	        MPI_OSD_MAX_BIND_CHANNEL);
	if (binded_handle >= MPI_OSD_MAX_BIND_CHANNEL) {
		OSD_DEB("Number of regions binded in chn:%d reach Max bind handles per channel (%d) !\n", c_idx,
		        MPI_OSD_MAX_BIND_CHANNEL);
		return -1;
	} else if (binded_handle == 0) {
		return 1;
	} else {
		return 0;
	}
}

static inline void setBindState(INT32 c_idx, INT32 j, INT32 val)
{
	g_osd_handle.bind_state[c_idx][j] = val;
}

static inline void setBindStateInternal(INT32 c_idx, INT32 j, INT32 val)
{
	g_osd_handle.bind_state_internal[c_idx][j] = val;
}

static inline void setHandle(INT32 c_idx, INT32 j, INT32 val)
{
	g_osd_handle.handle[c_idx][j] = val;
}

static inline UINT8 getBindState(INT32 c_idx, INT32 j)
{
	return g_osd_handle.bind_state[c_idx][j];
}

static inline UINT8 getBindStateInternal(INT32 c_idx, INT32 j)
{
	return g_osd_handle.bind_state_internal[c_idx][j];
}

static inline OSD_HANDLE getHandle(INT32 c_idx, INT32 j)
{
	return g_osd_handle.handle[c_idx][j];
}

static inline void setRgn(INT32 c_idx, INT32 j, const MPI_RECT_POINT_S *rect)
{
	g_osd_handle.osd_rgn[c_idx][j] = *rect;
}

static inline const MPI_RECT_POINT_S *getRgn(INT32 c_idx, INT32 j)
{
	return &g_osd_handle.osd_rgn[c_idx][j];
}

/**
 * @brief Creating start buff with no-region-binded channel
 * @param[in] chn_idx      video channel index.
 * @retval MPI_SUCCESS                 success.
 * @retval MPI_FAILURE                 unexpected fail.
 */
INT32 VIDEO_OSD_touchChn(MPI_ECHN chn_idx)
{
	INT32 c_idx = MPI_GET_ENC_CHN(chn_idx);
	INT32 bindState = osdCheckChnBindState(c_idx);
	g_osd_touchChn[c_idx] = 0;
	OSD_DEB("[%s:%d] Touching chn:%d, val:%d\n", __func__, __LINE__, c_idx, g_osd_touchChn[c_idx]);
	if (bindState == 1) {
		INT32 ret = 0;

		MPI_OSD_RGN_ATTR_S osd_attr = { 0 };
		setBindState(c_idx, HANDLE_IDX_TOUCH, 0);
		osd_attr.show = 0;
		osd_attr.qp_enable = FALSE;
		osd_attr.color_format = OSD_PM_CR_FORMAT;
		osd_attr.osd_type = MPI_OSD_OVERLAY_POLYGON;
		osd_attr.polygon.point_nums = 4;
		osd_attr.polygon.fill = 1;
		osd_attr.polygon.line_width = MPI_OSD_THICKNESS_MIN;
		MPI_OSD_BIND_ATTR_S osd_bind = { { 0 } };
		osd_bind.idx = chn_idx;

		osd_attr.polygon.point[0].x = 0;
		osd_attr.polygon.point[0].y = 0;
		osd_attr.polygon.point[1].x = 16;
		osd_attr.polygon.point[1].y = 0;
		osd_attr.polygon.point[2].x = 16;
		osd_attr.polygon.point[2].y = 16;
		osd_attr.polygon.point[3].x = 0;
		osd_attr.polygon.point[3].y = 16;
		osd_attr.polygon.color = 0;
		osd_attr.size.width = 16;
		osd_attr.size.height = 16;

		ret = MPI_createOsdRgn(&g_osd_handle.handle[c_idx][HANDLE_IDX_TOUCH], &osd_attr);
		if (ret != MPI_SUCCESS) {
			printf("Cannot create OSD touch in Channel:%d.\n", c_idx);
			return MPI_FAILURE;
		}

		ret = MPI_bindOsdToChn(g_osd_handle.handle[c_idx][HANDLE_IDX_TOUCH], &osd_bind);
		setBindStateInternal(c_idx, HANDLE_IDX_TOUCH, 1);
		if (ret != MPI_SUCCESS) {
			printf("Cannot bind OSD touch in Channel:%d.\n", c_idx);
			return MPI_FAILURE;
		}
		//setBindState(chn_idx, HANDLE_IDX_TOUCH, 1);
		g_osd_touchChn[c_idx] = 1;
	}
	OSD_DEB("[%s:%d] Exit Touching chn:%d, val:%d\n", __func__, __LINE__, c_idx, g_osd_touchChn[c_idx]);
	return MPI_SUCCESS;
}

/**
 * @brief Remove start buff with no-region-binded channel
 * @param[in] chn_idx      video channel index.
 * @retval MPI_SUCCESS                 success.
 * @retval MPI_FAILURE                 unexpected fail.
 */
INT32 VIDEO_OSD_untouchChn(MPI_ECHN chn_idx)
{
	INT32 c_idx = MPI_GET_ENC_CHN(chn_idx);
	OSD_DEB("[%s:%d] unTouching chn:%d, val:%d\n", __func__, __LINE__, c_idx, g_osd_touchChn[c_idx]);
	if (g_osd_touchChn[c_idx]) {
		INT32 ret = 0;
		MPI_OSD_BIND_ATTR_S osd_bind = { { 0 } };
		OSD_HANDLE handle = g_osd_handle.handle[c_idx][HANDLE_IDX_TOUCH];
		if (handle != -1) {
			osd_bind.idx = chn_idx;

			if (getBindStateInternal(c_idx, HANDLE_IDX_TOUCH)) {
				ret = MPI_unbindOsdFromChn(handle, &osd_bind);
				if (ret != MPI_SUCCESS) {
					printf("Cannot unbind OSD touch in Channel: %d.\n", c_idx);
					return MPI_FAILURE;
				}
				setBindStateInternal(c_idx, HANDLE_IDX_TOUCH, 0);
			}
			ret = MPI_destroyOsdRgn(handle);
			if (ret != MPI_SUCCESS) {
				printf("Cannot destory OSD touch in Channel: %d.\n", c_idx);
				return MPI_FAILURE;
			}
			g_osd_handle.handle[c_idx][HANDLE_IDX_TOUCH] = -1;

			g_osd_touchChn[c_idx] = 0;
		}
	}
	assert(g_osd_touchChn[c_idx] == 0);
	OSD_DEB("[%s:%d] unTouching chn:%d, val:%d\n", __func__, __LINE__, c_idx, g_osd_touchChn[c_idx]);
	return MPI_SUCCESS;
}

/**
 * @brief Check bindstate on seleted channel
 * @param[in] Chn_idx video channel index.
 * @retval 0 Channel binded region number is smaller than max bind per channel.
 * @retval 1 Channel binded region number is zero.
 * @retval -1 Channel binded region number meets max bind boundary.
 */
INT32 VIDEO_OSD_checkBindState(MPI_ECHN chn_idx)
{
	INT32 c_idx = MPI_GET_ENC_CHN(chn_idx);
	return osdCheckChnBindState(c_idx);
}

/**
 * @brief Set bind state
 * @param[in] Chn_idx video channel index.
 * @param[in] Handle_idx handle index per channel.
 * @param[in] Set value.
 * @retval 0 Success operation.
 */
INT32 VIDEO_OSD_setBindState(MPI_ECHN chn_idx, INT32 handle_idx, INT32 val)
{
	INT32 c_idx = MPI_GET_ENC_CHN(chn_idx);
	setBindState(c_idx, handle_idx, val);
	return 0;
}

/**
 * @brief Set handle value
 * @param[in] Chn_idx video channel index.
 * @param[in] Handle_idx handle index per channel.
 * @param[in] Set value.
 * @retval 0 Success operation.
 */
INT32 VIDEO_OSD_setHandle(MPI_ECHN chn_idx, INT32 handle_idx, INT32 val)
{
	INT32 c_idx = MPI_GET_ENC_CHN(chn_idx);
	setHandle(c_idx, handle_idx, val);
	return 0;
}

/**
 * @brief Get bind state by handle index on selected video channel
 * @param[in] Chn_idx video channel index.
 * @param[in] Handle_idx handle index per channel.
 * @retval 0 The region is not binded to the channel
 * @retval 1 The region is binded to the channel
 */
INT32 VIDEO_OSD_getBindState(MPI_ECHN chn_idx, INT32 handle_idx)
{
	INT32 c_idx = MPI_GET_ENC_CHN(chn_idx);
	return getBindState(c_idx, handle_idx);
}

/**
 * @brief Get handle by handle index on selected video channel
 * @param[in] Chn_idx video channel index.
 * @param[in] Handle_idx handle index per channel.
 * @retval OSD_HANDLE OSD handle.
 */
OSD_HANDLE VIDEO_OSD_getHandle(MPI_ECHN chn_idx, INT32 handle_idx)
{
	INT32 c_idx = MPI_GET_ENC_CHN(chn_idx);
	return getHandle(c_idx, handle_idx);
}

#ifdef OSD_DIS_INFO
void showDebug(void)
{
	int i, j;
	for (i = 0; i < SAMPLE_MAX_VIDEO_CHN_NUM; i++) {
		for (j = 0; j < VIDEO_OSD_MAX_HANDLE_CHANNEL; j++) {
			printf("[chn:%d,id:%d,bind_state:%hhu,handle:%d]\n", i, j, getBindState(i, j), getHandle(i, j));
		}
	}
}
#endif /* !OSD_DIS_INFO */

void initOsdHandle(void)
{
	memset(g_osd_handle.handle, -1, sizeof(g_osd_handle.handle));
}

void loadOsdFont(void)
{
	int i = 0;
	int time_osd_width = 0;
	FILE *fp;
	AyuvInfo_S info;
	OSD_FONT_INFO_S *fontInfo = NULL;
	ASCII_INDEX *idx = NULL;
	for (i = 0; i < MAX_FONT_FORMAT; i++) {
		fontInfo = &g_fontInfo[i];
		fp = fopen(fontInfo->path, "rb");
		assert(fp != NULL);

		fread(&info, sizeof(info), 1, fp);

		fontInfo->font_index = malloc(info.index_size);
		assert(fontInfo->font_index != NULL);
		fseek(fp, info.index_offset, SEEK_SET);
		fread(fontInfo->font_index, info.index_size, 1, fp);

		fontInfo->font_data = malloc(info.data_size);
		assert(fontInfo->font_data != NULL);
		fseek(fp, info.data_offset, SEEK_SET);
		fread(fontInfo->font_data, info.data_size, 1, fp);
		fclose(fp);

		fontInfo->max_font_width = info.ascii_width;

		idx = (ASCII_INDEX *)fontInfo->font_index;
		fontInfo->time_osd_height = ((idx[0].image_height + 15) / 16) * 16;
		/* Max time format " %4u-%02u-%02u  %02u:%02u:%02u tt %s%s%s" */
		//time_osd_width = 14 * idx['1'-32].image_width + 4 * idx[' '-32].image_width + 3 * fontInfo->max_font_width +
		//                 2 * idx['-'-32].image_width + 2 * idx[':'-32].image_width;
		time_osd_width = 14 * idx['1' - 32].image_width + 5 * idx[' ' - 32].image_width +
		                 3 * fontInfo->max_font_width + 2 * idx['-' - 32].image_width +
		                 2 * idx[':' - 32].image_width + idx['A' - 32].image_width + idx['M' - 32].image_width;
		fontInfo->time_osd_width = ((time_osd_width + 15) / 16) * 16;
	}
}

void unloadOsdFont(void)
{
	int i = 0;
	OSD_FONT_INFO_S *fontInfo = NULL;

	for (i = 0; i < MAX_FONT_FORMAT; i++) {
		fontInfo = &g_fontInfo[i];
		if (fontInfo->font_index) {
			free(fontInfo->font_index);
			fontInfo->font_index = NULL;
		}

		if (fontInfo->font_data) {
			free(fontInfo->font_data);
			fontInfo->font_data = NULL;
		}
	}
}

INT32 showHideOsdRegion(INT32 show, INT32 i, INT32 j)
{
	INT32 ret = 0;
	MPI_OSD_RGN_ATTR_S osd_attr = { 0 };

	if (g_osd_handle.handle[i][j] < 0) {
		return 0;
	}

	MPI_getOsdRgnAttr(g_osd_handle.handle[i][j], &osd_attr);
	osd_attr.show = show;
	MPI_setOsdRgnAttr(g_osd_handle.handle[i][j], &osd_attr);
	return ret;
}

INT32 bindOsdRegion(INT32 i, INT32 j, const MPI_OSD_BIND_ATTR_S *osd_bind)
{
	INT32 ret = 0;
	OSD_HANDLE handle = getHandle(i, j);
	if (osd_bind == NULL)
		osd_bind = &g_osd_handle.osd_bind[i][j];

	if (handle < 0) {
		return -1;
	}
	if (getBindStateInternal(i, j) == 0) {
		ret = osdCheckChnBindState(i);
		if (ret == -1) {
			OSD_DEB("Channel OSD binding region is full!\n");
			return -1;
		}
		ret = MPI_bindOsdToChn(handle, osd_bind);
		if (ret != MPI_SUCCESS) {
			OSD_DEB("Bind OSD %d from channel %d failed.\n", handle, i);
			return MPI_FAILURE;
		}
		setBindStateInternal(i, j, 1);
		setBindState(i, j, 1);
	} else {
		OSD_DEB("Handle:%d,%d is already binded\n", i, j);
	}
	OSD_DEB("[%s] id:(%d,%d) handle:%d %d\n", __func__, i, j, handle, i);
	return ret;
}

INT32 unbindOsdRegion(INT32 i, INT32 j)
{
	INT32 ret = 0;
	OSD_HANDLE handle = getHandle(i, j);
	MPI_OSD_BIND_ATTR_S *osd_bind = &g_osd_handle.osd_bind[i][j];
	//getBindAttr(i, j, osd_bind);

	if (handle < 0) {
		return -1;
	}
	//if (getBindState(i, j)) {
	ret = MPI_unbindOsdFromChn(handle, osd_bind);
	if (ret != MPI_SUCCESS) {
		OSD_DEB("Unbind OSD %d from channel %d failed.\n", handle, i);
		//setBindState(i, j, 0);
		return MPI_FAILURE;
	}
	setBindState(i, j, 0);
	setBindStateInternal(i, j, 0);

	//}
	OSD_DEB("[%s] id:(%d,%d) handle:%d %d\n", __func__, i, j, handle, getBindState(i, j));
	return ret;
}

void setOsdDateTimeFormat(char *spec)
{
	char input[32] = { 0 };
	strcpy(input, spec);
	char *token = strtok(input, " ");
	char dateFormat[16] = { 0 };
	char timeFormat[16] = { 0 };

	if (token != NULL)
		strcpy(dateFormat, token);
	else {
		assert(0);
	}
	token = strtok(NULL, " ");
	if (token != NULL)
		strcpy(timeFormat, token);
	if (token != NULL) {
		token = strtok(NULL, " ");
		if (token != NULL)
			sprintf(timeFormat, "%s %s", timeFormat, token);
	}
	if (strcmp(dateFormat, "YYYY-MM-DD") == 0) {
		g_date_format = OSD_DATE_FORMAT_YYYY_MM_DD;
	} else if (strcmp(dateFormat, "MM-DD-YYYY") == 0) {
		g_date_format = OSD_DATE_FORMAT_MM_DD_YYYY;
	} else if (strcmp(dateFormat, "DD-MM-YYYY") == 0) {
		g_date_format = OSD_DATE_FORMAT_DD_MM_YYYY;
	} else {
		assert(0);
	}

	if (strcmp(timeFormat, "h:mm:ss tt") == 0) {
		g_time_format = OSD_TIME_FORMAT_H_MM_SS_TT;
	} else if (strcmp(timeFormat, "hh:mm:ss tt") == 0) {
		g_time_format = OSD_TIME_FORMAT_HH_MM_SS_TT;
	} else if (strcmp(timeFormat, "H:mm:ss") == 0) {
		g_time_format = OSD_TIME_FORMAT_H_MM_SS;
	} else if (strcmp(timeFormat, "HH:mm:ss") == 0) {
		g_time_format = OSD_TIME_FORMAT_HH_MM_SS;
	} else {
		SYS_TRACE("[OSD] Does not specified time format! Set to default time format HH:mm:ss\n");
		g_time_format = OSD_TIME_FORMAT_HH_MM_SS;
	}
}

void hideIvaAlarm()
{
	INT32 i = 0;
	MPI_OSD_RGN_ATTR_S osd_attr = { 0 };

	for (i = 0; i < MPI_MAX_ENC_CHN_NUM; i++) {
		if (g_osd_handle.handle[i][HANDLE_IDX_ALARM] > 0) {
			MPI_getOsdRgnAttr(g_osd_handle.handle[i][HANDLE_IDX_ALARM], &osd_attr);
			osd_attr.show = 0;
			MPI_setOsdRgnAttr(g_osd_handle.handle[i][HANDLE_IDX_ALARM], &osd_attr);
		}
	}
	return;
}

INT32 showIvaAlarm()
{
	INT32 ret = 0;
	INT32 i = 0;
	MPI_OSD_RGN_ATTR_S osd_attr = { 0 };

	for (i = 0; i < MPI_MAX_ENC_CHN_NUM; i++) {
		if (g_osd_handle.handle[i][HANDLE_IDX_ALARM] > 0) {
			MPI_getOsdRgnAttr(g_osd_handle.handle[i][HANDLE_IDX_ALARM], &osd_attr);
			osd_attr.show = 1;
			MPI_setOsdRgnAttr(g_osd_handle.handle[i][HANDLE_IDX_ALARM], &osd_attr);
		}
	}
	signal(SIGALRM, hideIvaAlarm);
	alarm(3);
	return ret;
}

void createIvaAlarm(INT32 start_x, INT32 start_y, MPI_ECHN chn_idx, UINT16 width, UINT16 height, INT32 show)
{
	OSD_DEB(" Enter %s\n", __func__);
	INT32 c_idx = MPI_GET_ENC_CHN(chn_idx);
	INT32 ret = 0;
	INT32 j;
	INT32 x_input = 0;
	INT32 y_input = 0;
	INT32 image_width = 64;
	INT32 image_height = 48;
	MPI_RECT_POINT_S rect;
	UINT16 *canvas_addr = NULL;
	OSD_ALARM_INFO_S *alarmInfo;
	MPI_OSD_RGN_ATTR_S osd_attr = { 0 };
	MPI_OSD_BIND_ATTR_S *osd_bind = &g_osd_handle.osd_bind[c_idx][HANDLE_IDX_ALARM];
	MPI_OSD_CANVAS_ATTR_S *canvas = NULL;

	//getBindAttr(chn_idx, HANDLE_IDX_ALARM, osd_bind);

	osd_attr.show = 0;
	osd_attr.priority = 2;
	osd_attr.qp_enable = FALSE;
	osd_attr.color_format = MPI_OSD_COLOR_FORMAT_AYUV_3544;
	osd_attr.osd_type = MPI_OSD_OVERLAY_BITMAP;
	osd_attr.size.width = ((image_width + 15) / 16) * 16;
	osd_attr.size.height = ((image_height + 15) / 16) * 16;

	ret = MPI_createOsdRgn(&g_osd_handle.handle[c_idx][HANDLE_IDX_ALARM], &osd_attr);
	if (ret != MPI_SUCCESS) {
		printf("MPI_OSD_CreateRgn failed.\n");
		return;
	}

	canvas = (c_idx == 0) ? &p_canvas_attr_alarm_ch0 :
	                        (c_idx == 1) ? &p_canvas_attr_alarm_ch1 : &p_canvas_attr_alarm_ch2;
	ret = MPI_getOsdCanvas(g_osd_handle.handle[c_idx][HANDLE_IDX_ALARM], canvas);
	if (ret != MPI_SUCCESS) {
		printf("MPI_getOsdCanvas failed.\n");
		return;
	}

	canvas_addr = (UINT16 *)canvas->canvas_addr;
	for (j = 0; j < image_width; j++) {
		memcpy((canvas_addr + (j * (osd_attr.size.width))), (bell_ayuv + (j * image_width)), image_width * 2);
	}

	for (j = 0; j < MAX_ALARM_FORMAT; j++) {
		alarmInfo = &g_alarmInfo[j];
		if ((alarmInfo->max_width >= width) && (alarmInfo->min_width <= width)) {
			g_active_alarm[c_idx] = j;
			break;
		}
	}
	assert(j < MAX_ALARM_FORMAT);

	/* Config to OSD value mapping */
	x_input = start_x;
	y_input = start_y;
	if (x_input > 100)
		x_input = 100;
	if (y_input > 100)
		y_input = 100;
	if (x_input < 0)
		x_input = 0;
	if (y_input < 0)
		y_input = 0;

	x_input = ((((x_input * width) / 100) + 15) / 16) * 16;
	y_input = ((((y_input * height) / 100) + 15) / 16) * 16;
	rect.sx = x_input;
	rect.sy = y_input;
	rect.ex = rect.sx + osd_attr.size.width - 1;
	rect.ey = rect.sy + osd_attr.size.height - 1;
	width = ((width + 15) / 16) * 16;
	height = ((height + 15) / 16) * 16;
	if (rect.ex > width - 1) {
		x_input = (((rect.ex - width) / 16) + 1) * 16;
		rect.ex -= x_input;
		rect.sx -= x_input;
	}
	if (rect.ey > height - 1) {
		y_input = (((rect.ey - height) / 16) + 1) * 16;
		rect.ey -= y_input;
		rect.sy -= y_input;
	}

	osd_bind->point.x = rect.sx;
	osd_bind->point.y = rect.sy;
	osd_bind->idx = chn_idx;
	osd_bind->module = 0;

	setRgn(c_idx, HANDLE_IDX_ALARM, &rect);

	if (show) {
		ret = MPI_bindOsdToChn(g_osd_handle.handle[c_idx][HANDLE_IDX_ALARM], osd_bind);
		if (ret != MPI_SUCCESS) {
			printf("Bind OSD %d to channel %d failed.\n", g_osd_handle.handle[c_idx][HANDLE_IDX_ALARM],
			       MPI_GET_ENC_CHN(chn_idx));
			return;
		}
		setBindState(c_idx, HANDLE_IDX_ALARM, 1);
		setBindStateInternal(c_idx, HANDLE_IDX_ALARM, 1);
	}

	OSD_DEB(" Enter %s\n", __func__);
}

INT32 createInfo(INT32 start_x, INT32 start_y, char *spec, MPI_ECHN chn_idx, UINT16 width, UINT16 height, INT32 show,
                 INT32 k)
{
	OSD_DEB(" Enter %s\n", __func__);
	/* FIXME Info default bind channel to avoid NULL pointer error*/
	INT32 c_idx = MPI_GET_ENC_CHN(chn_idx);
	INT32 i = 0;
	INT32 ret = 0;
	MPI_OSD_RGN_ATTR_S osd_attr = { 0 };
	MPI_OSD_BIND_ATTR_S *osd_bind = &g_osd_handle.osd_bind[c_idx][k];
	MPI_RECT_POINT_S rect;
	int x_input;
	int y_input;
	int time_osd_width = 0;
	int time_osd_height = 0;
	OSD_FONT_INFO_S *fontInfo = NULL;
	assert(c_idx < SAMPLE_MAX_VIDEO_CHN_NUM);

	for (i = 0; i < MAX_FONT_FORMAT; i++) {
		fontInfo = &g_fontInfo[i];
		if ((fontInfo->max_height >= height) && (fontInfo->min_height <= height)) {
			g_active_font[c_idx] = i;
			break;
		}
	}

	x_input = start_x;
	y_input = start_y;

	fontInfo = &g_fontInfo[g_active_font[c_idx]];
	time_osd_width = fontInfo->time_osd_width;
	time_osd_height = fontInfo->time_osd_height;
	g_info_index[c_idx] = k;

	osd_attr.show = show;
	osd_attr.priority = 1;
	osd_attr.qp_enable = FALSE;
	osd_attr.color_format = MPI_OSD_COLOR_FORMAT_AYUV_3544;
	osd_attr.osd_type = MPI_OSD_OVERLAY_BITMAP;
	osd_attr.size.width = time_osd_width;
	osd_attr.size.height = time_osd_height;

	ret = MPI_createOsdRgn(&g_osd_handle.handle[c_idx][k], &osd_attr);
	if (ret != MPI_SUCCESS) {
		printf("MPI_OSD_CreateRgn failed.\n");
		return MPI_FAILURE;
	}

	ret = MPI_getOsdCanvas(g_osd_handle.handle[c_idx][k], &p_canvas_attr_time[c_idx]);
	if (ret != MPI_SUCCESS) {
		printf("MPI_getOsdCanvas failed.\n");
		return MPI_FAILURE;
	}

	if (x_input > 100)
		x_input = 100;
	if (y_input > 100)
		y_input = 100;
	if (x_input < 0)
		x_input = 0;
	if (y_input < 0)
		y_input = 0;

	x_input = ((((x_input * width) / 100) + 15) / 16) * 16;
	y_input = ((((y_input * height) / 100) + 15) / 16) * 16;
	rect.sx = x_input;
	rect.sy = y_input;
	rect.ex = rect.sx + osd_attr.size.width - 1;
	rect.ey = rect.sy + osd_attr.size.height - 1;
	width = ((width + 15) / 16) * 16;
	height = ((height + 15) / 16) * 16;
	if (rect.ex > width - 1) {
		x_input = (((rect.ex - width) / 16) + 1) * 16;
		rect.ex -= x_input;
		rect.sx -= x_input;
	}
	if (rect.ey > height - 1) {
		y_input = (((rect.ey - height) / 16) + 1) * 16;
		rect.ey -= y_input;
		rect.sy -= y_input;
	}

	osd_bind->point.x = rect.sx;
	osd_bind->point.y = rect.sy;
	osd_bind->idx = chn_idx;
	osd_bind->module = 0;

	/*setting time format,following main stream setting*/
	if (c_idx == 0) {
		setOsdDateTimeFormat(spec);
	}
	setBindState(c_idx, k, 0);
	if (show) {
		ret = MPI_bindOsdToChn(g_osd_handle.handle[c_idx][k], osd_bind);
		if (ret != MPI_SUCCESS) {
			printf("Bind OSD %d to channel %d failed.\n", g_osd_handle.handle[c_idx][k],
			       MPI_GET_ENC_CHN(chn_idx));
			return MPI_FAILURE;
		}
		setBindState(c_idx, k, 1);
		setBindStateInternal(c_idx, k, 1);
	}
	setRgn(c_idx, k, &rect);
	OSD_DEB(" Exit %s\n", __func__);
	return MPI_SUCCESS;
}

INT32 createText(INT32 start_x, INT32 start_y, char *spec, MPI_ECHN chn_idx, UINT16 width, UINT16 height, INT32 show,
                 INT32 k)
{
	OSD_DEB(" Enter %s\n", __func__);
	INT32 c_idx = chn_idx.chn;
	INT32 ret = 0;
	int i = 0;
	int j = 0;
	int g = 0;
	int width_acc = 0;
	UINT32 real_addr = 0;
	UINT16 *osd_addr = NULL;
	unsigned char input_ascii = 0;
	char *input_string = spec;
	char *font_data = NULL;
	int string_lens = 0;
	int string_width = 0;
	int string_height = 0;
	MPI_OSD_RGN_ATTR_S osd_attr = { 0 };
	MPI_OSD_BIND_ATTR_S *osd_bind = &g_osd_handle.osd_bind[c_idx][k];
	MPI_OSD_CANVAS_ATTR_S tempmap = { 0 };
	OSD_FONT_INFO_S *fontInfo = NULL;
	ASCII_INDEX *font_index = NULL;
	int x_input;
	int y_input;
	MPI_RECT_POINT_S rect;

	//	int step = (width > 960) ? 1 : 2;
	int step = 1;
	assert(c_idx < SAMPLE_MAX_VIDEO_CHN_NUM);

	for (i = 0; i < MAX_FONT_FORMAT; i++) {
		fontInfo = &g_fontInfo[i];
		if ((fontInfo->max_height >= height) && (fontInfo->min_height <= height)) {
			g_active_font[c_idx] = i;
			break;
		}
	}
	assert(i < MAX_FONT_FORMAT);

	font_index = fontInfo->font_index;
	font_data = fontInfo->font_data;

	x_input = start_x;
	y_input = start_y;

	string_lens = ((string_lens = strlen(input_string)) >= MAX_TEXT_LEN) ? MAX_TEXT_LEN : string_lens;

	string_height = font_index[0].image_height;

	for (i = 0; i < string_lens; i++) {
		input_ascii = input_string[i];
		input_ascii = input_ascii - 32;
		width_acc = width_acc + font_index[input_ascii].image_width;
	}

	string_width = width_acc / step;

	osd_attr.show = show;
	osd_attr.qp_enable = FALSE;
	osd_attr.color_format = MPI_OSD_COLOR_FORMAT_AYUV_3544;
	osd_attr.osd_type = MPI_OSD_OVERLAY_BITMAP;
	osd_attr.size.width = ((string_width + 15) / 16) * 16; /*alignment 16*/
	osd_attr.size.height = ((string_height + 15) / 16) * 16; /*alignment 16*/
	ret = MPI_createOsdRgn(&g_osd_handle.handle[c_idx][k], &osd_attr);
	if (ret != MPI_SUCCESS) {
		printf("MPI_OSD_CreateRgn failed.\n");
		return MPI_FAILURE;
	}

	ret = MPI_getOsdCanvas(g_osd_handle.handle[c_idx][k], &tempmap);
	if (ret != MPI_SUCCESS) {
		printf("MPI_getOsdCanvas failed.\n");
		return MPI_FAILURE;
	}
	width_acc = 0;

	for (i = 0; i < string_lens; i++) {
		input_ascii = input_string[i];
		input_ascii = input_ascii - 32;
		real_addr = (UINT32)font_data + font_index[input_ascii].image_offset;

		for (j = 0; j < (font_index[input_ascii].image_height); j += step) {
			osd_addr = (UINT16 *)((tempmap.canvas_addr) + (j / step * (osd_attr.size.width) * 2) +
			                      (width_acc / step * 2));
			for (g = 0; g < (font_index[input_ascii].image_width); g += step) {
				osd_addr[g / step] = *((UINT16 *)real_addr + g);
			}

			real_addr = (real_addr + (font_index[input_ascii].image_width * 2 * step));
		}
		width_acc = width_acc + font_index[input_ascii].image_width;
	}
	ret = MPI_updateOsdCanvas(getHandle(c_idx, k));
	if (ret != MPI_SUCCESS) {
		printf("OSD %d to update canvas failed.\n", g_osd_handle.handle[c_idx][k]);
		return MPI_FAILURE;
	}

	/* Config to OSD value mapping */
	if (x_input > 100)
		x_input = 100;
	if (y_input > 100)
		y_input = 100;
	if (x_input < 0)
		x_input = 0;
	if (y_input < 0)
		y_input = 0;

	x_input = ((((x_input * width) / 100) + 15) / 16) * 16;
	y_input = ((((y_input * height) / 100) + 15) / 16) * 16;
	rect.sx = x_input;
	rect.sy = y_input;
	rect.ex = rect.sx + osd_attr.size.width - 1;
	rect.ey = rect.sy + osd_attr.size.height - 1;
	width = ((width + 15) / 16) * 16;
	height = ((height + 15) / 16) * 16;
	if (rect.ex > width - 1) {
		x_input = (((rect.ex - width) / 16) + 1) * 16;
		rect.ex -= x_input;
		rect.sx -= x_input;
	}
	if (rect.ey > height - 1) {
		y_input = (((rect.ey - height) / 16) + 1) * 16;
		rect.ey -= y_input;
		rect.sy -= y_input;
	}

	osd_bind->point.x = rect.sx;
	osd_bind->point.y = rect.sy;
	osd_bind->idx = chn_idx;
	osd_bind->module = 0;
	setBindState(c_idx, k, 0);
	if (show) {
		ret = MPI_bindOsdToChn(g_osd_handle.handle[c_idx][k], osd_bind);
		if (ret != MPI_SUCCESS) {
			printf("Bind OSD %d to channel %d failed.\n", g_osd_handle.handle[c_idx][k],
			       MPI_GET_ENC_CHN(chn_idx));
			return MPI_FAILURE;
		}
		setBindState(c_idx, k, 1);
		setBindStateInternal(c_idx, k, 1);
	}
	setRgn(c_idx, k, &rect);

	OSD_DEB(" Exit %s\n", __func__);
	return MPI_SUCCESS;
}

INT32 deleteText(MPI_ECHN chn_idx, INT32 k)
{
	MPI_OSD_BIND_ATTR_S osd_bind = { { 0 } };
	INT32 c_idx = MPI_GET_ENC_CHN(chn_idx);
	OSD_HANDLE handle = g_osd_handle.handle[c_idx][k];
	INT32 ret = MPI_SUCCESS;
	INT32 ret_unbind = MPI_SUCCESS;
	if (handle >= 0) {
		osd_bind.idx = chn_idx;

		if (getBindStateInternal(c_idx, k)) {
			ret_unbind = MPI_unbindOsdFromChn(handle, &osd_bind);
			if (ret_unbind != MPI_SUCCESS) {
				ret_unbind = MPI_FAILURE;
			}
			setBindState(c_idx, k, 0);
			setBindStateInternal(c_idx, k, 0);
		}
		OSD_DEB("Handle:%d,%d deleteText bindState is set to 0\n", c_idx, k);

		ret = MPI_destroyOsdRgn(handle);
		if (ret != MPI_SUCCESS) {
			if (ret_unbind != MPI_SUCCESS) {
				printf("Unbind OSD %d from channel %d failed.\n", handle, MPI_GET_ENC_CHN(chn_idx));
			}
			printf("Destroy OSD failed.\n");
			ret = MPI_FAILURE;
		}
		setHandle(c_idx, k, -1);
	}

	return ret;
}

INT32 createMap(INT32 start_x, INT32 start_y, char *spec, MPI_ECHN chn_idx, UINT16 width, UINT16 height, INT32 show,
                INT32 k)
{
	FILE *fp_pic;
	int j = 0;
	INT32 ret = 0;
	INT32 c_idx = MPI_GET_ENC_CHN(chn_idx);
	MPI_OSD_RGN_ATTR_S osd_attr = { 0 };
	MPI_OSD_BIND_ATTR_S *osd_bind = &g_osd_handle.osd_bind[c_idx][k];
	MPI_OSD_CANVAS_ATTR_S tempmap = { 0 };
	MPI_RECT_POINT_S rect;

	int x_input;
	int y_input;

	x_input = start_x;
	y_input = start_y;

	fp_pic = fopen(spec, "rb");
	assert(fp_pic);

	fread(&pic_image_width, sizeof(UINT32), 1, fp_pic);

	fseek(fp_pic, 4, SEEK_SET);
	fread(&pic_image_height, sizeof(UINT32), 1, fp_pic);

	fseek(fp_pic, 8, SEEK_SET);
	fread(&pic_image_size, sizeof(UINT32), 1, fp_pic);

	pic_image_pointer = malloc(pic_image_size);
	assert(pic_image_pointer != NULL);

	fseek(fp_pic, 12, SEEK_SET);
	fread(pic_image_pointer, pic_image_size, 1, fp_pic);

	fclose(fp_pic);

	osd_attr.show = show;
	osd_attr.qp_enable = FALSE;
	osd_attr.color_format = MPI_OSD_COLOR_FORMAT_AYUV_3544;
	osd_attr.osd_type = MPI_OSD_OVERLAY_BITMAP;
	osd_attr.size.width = ((pic_image_width + 15) / 16) * 16; //((width/2 + 15)/16) * 16 ;//
	osd_attr.size.height = ((pic_image_height + 15) / 16) * 16; //((height + 15)/16) * 16

	ret = MPI_createOsdRgn(&g_osd_handle.handle[c_idx][k], &osd_attr);
	if (ret != MPI_SUCCESS) {
		printf("MPI_OSD_CreateRgn failed.\n");
		return MPI_FAILURE;
	}

	ret = MPI_getOsdCanvas(g_osd_handle.handle[c_idx][k], &tempmap);
	if (ret != MPI_SUCCESS) {
		printf("MPI_getOsdCanvas failed.\n");
		return MPI_FAILURE;
	}

	for (j = 0; j < pic_image_height; j++) {
		memcpy(((char *)tempmap.canvas_addr + (j * (osd_attr.size.width) * 2)),
		       ((char *)pic_image_pointer + (j * pic_image_width * 2)), pic_image_width * 2);
	}

	ret = MPI_updateOsdCanvas(g_osd_handle.handle[c_idx][k]);
	if (ret != MPI_SUCCESS) {
		printf("OSD %d to update canvas failed.\n", g_osd_handle.handle[c_idx][k]);
		return MPI_FAILURE;
	}

	/* Config to OSD value mapping */
	if (x_input > 100)
		x_input = 100;
	if (y_input > 100)
		y_input = 100;
	if (x_input < 0)
		x_input = 0;
	if (y_input < 0)
		y_input = 0;

	x_input = ((((x_input * width) / 100) + 15) / 16) * 16;
	y_input = ((((y_input * height) / 100) + 15) / 16) * 16;
	rect.sx = x_input;
	rect.sy = y_input;
	rect.ex = rect.sx + osd_attr.size.width - 1;
	rect.ey = rect.sy + osd_attr.size.height - 1;
	width = ((width + 15) / 16) * 16;
	height = ((height + 15) / 16) * 16;
	if (rect.ex > width - 1) {
		x_input = (((rect.ex - width) / 16) + 1) * 16;
		rect.ex -= x_input;
		rect.sx -= x_input;
	}
	if (rect.ey > height - 1) {
		y_input = (((rect.ey - height) / 16) + 1) * 16;
		rect.ey -= y_input;
		rect.sy -= y_input;
	}

	osd_bind->point.x = rect.sx;
	osd_bind->point.y = rect.sy;
	osd_bind->idx = chn_idx;
	osd_bind->module = 0;

	free(pic_image_pointer);
	setBindState(c_idx, k, 0);
	setBindStateInternal(c_idx, k, 0);
	//SYS_TRACE("[DEBUG] MPICHN: %d chn_idx:%d\n", chn_idx, c_idx);
	if (show) {
		ret = MPI_bindOsdToChn(g_osd_handle.handle[c_idx][k], osd_bind);
		if (ret != MPI_SUCCESS) {
			printf("Bind OSD %d to channel %d failed.\n", g_osd_handle.handle[c_idx][k],
			       MPI_GET_ENC_CHN(chn_idx));
			return MPI_FAILURE;
		}
		setBindState(c_idx, k, 1);
		setBindStateInternal(c_idx, k, 1);
	}
	setRgn(c_idx, k, &rect);

	return MPI_SUCCESS;
}

void createOsdRegions(MPI_ECHN chn_idx, UINT16 width, UINT16 height, const AGTX_OSD_CONF_OUTER_S *osd_conf,
                      const AGTX_OSD_PM_PARAM_S *osd_pm)
{
	OSD_DEB("Entering %s\n", __func__);
	INT32 ret = 0;

	//showDebug();

	for (int i = 0; i < 4; i++) {
		const AGTX_OSD_CONF_INNER_S *region = &osd_conf->region[i];
		if (region->enabled != -1) {
			switch (region->type) {
			case AGTX_OSD_TYPE_IMAGE:
				if (strcmp((const char *)region->type_spec, "icon:bell") == 0) {
					createIvaAlarm(region->start_x, region->start_y, chn_idx, width, height,
					               region->enabled);
				} else {
					createMap(region->start_x, region->start_y, (char *)region->type_spec, chn_idx,
					          width, height, region->enabled, i);
				}
				break;
			case AGTX_OSD_TYPE_TEXT:
				createText(region->start_x, region->start_y, (char *)region->type_spec, chn_idx, width,
				           height, region->enabled, i);
				break;
			case AGTX_OSD_TYPE_INFO:
				createInfo(region->start_x, region->start_y, (char *)region->type_spec, chn_idx, width,
				           height, region->enabled, i);
				break;
			default:
				assert(0);
				break;
			}
		}
	}
	VIDEO_OSD_initPrivacyMask(chn_idx, width, height, osd_pm);

	ret = VIDEO_OSD_touchChn(chn_idx);
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("OSD touch channel %d failed.\n", MPI_GET_ENC_CHN(chn_idx));
		return;
	}

	OSD_DEB("Exiting %s\n", __func__);
	//showDebug();
}

INT32 destroyOsdRegions(MPI_ECHN chn_idx)
{
	OSD_DEB("Entering %s\n", __func__);
	MPI_OSD_BIND_ATTR_S osd_bind = { { 0 } }; // = NULL;
	OSD_HANDLE handle;
	INT32 ret = MPI_SUCCESS;
	INT32 ret_unbind = MPI_SUCCESS;
	INT32 c_idx = MPI_GET_ENC_CHN(chn_idx);
	ret = VIDEO_OSD_untouchChn(chn_idx);
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("OSD untouch channel %d failed.\n", MPI_GET_ENC_CHN(chn_idx));
		return MPI_FAILURE;
	}

	ret = VIDEO_OSD_deletePrivacyMask(chn_idx);
	if (ret != MPI_SUCCESS) {
		printf("Destroy Privacy Mask from channel %d failed.\n", MPI_GET_ENC_CHN(chn_idx));
		ret = MPI_FAILURE;
	}

	for (int i = 0; i < MPI_OSD_MAX_BIND_CHANNEL; i++) {
		handle = g_osd_handle.handle[c_idx][i];
		if (handle >= 0) {
			osd_bind.idx = chn_idx;
			if (getBindStateInternal(c_idx, i)) {
				ret_unbind = MPI_unbindOsdFromChn(handle, &osd_bind);
				if (ret_unbind != MPI_SUCCESS) {
					ret_unbind = MPI_FAILURE;
				}
				setBindState(c_idx, i, 0);
				setBindStateInternal(c_idx, i, 0);
			}
			ret = MPI_destroyOsdRgn(handle);
			if (ret != MPI_SUCCESS) {
				if (ret_unbind != MPI_SUCCESS) {
					printf("Unbind OSD %d from channel %d failed.\n", handle,
					       MPI_GET_ENC_CHN(chn_idx));
				}
				printf("Destroy OSD failed.\n");
				ret = MPI_FAILURE;
			}
			setHandle(c_idx, i, -1);
		}
	}

	OSD_DEB("Exiting %s\n", __func__);
	return ret;
}

static int updateDateTime(void *data)
{
	char *font_data = NULL;
	ASCII_INDEX *font_index = NULL;
	OSD_FONT_INFO_S *fontInfo = NULL;
	MPI_OSD_CANVAS_ATTR_S *canvas_attr = NULL;
	OSD_HANDLE handle = 0;
	int i = 0;
	int j = 0;
	int k = 0;
	int hr = 0;
	int timehalfday = 0;
	int string_lens = 0;
	int time_osd_width = 0;
	unsigned char input_ascii = 0;
	UINT32 real_addr = 0;
	INT32 ret = 0;
	int showWeekDay = 1;
	int weekDayCharLim = 3;
	char timeFormatString[32] = { 0 };
	char timestring[128] = { 0 };
	int width_acc = 0;
#ifdef OSD_DATETIME_CHINESE
	int week[7][3] = { { 95, 96, 103 }, { 95, 96, 97 },  { 95, 96, 98 }, { 95, 96, 99 },
		           { 95, 96, 100 }, { 95, 96, 101 }, { 95, 96, 102 } };
#else /* OSD_DATETIME_ENGLISH */
	int week[7][3] = { { 51, 53, 46 }, { 45, 47, 46 }, { 52, 53, 37 }, { 55, 37, 36 },
		           { 52, 40, 53 }, { 38, 50, 41 }, { 51, 33, 52 } };
#endif /* !OSD_DATETIME_CHINESE */
	long utc = time(NULL);
	struct tm gmt_tm = { 0 };
	int fill_width = 0;

	if (setThreadSchedAttr(OSD_TH_NAME)) {
		printf("Failed to set thread %s!\n", OSD_TH_NAME);
	}
	while (1) {
		showWeekDay = g_osd_handle.showWeekDay;
		if (g_prev_showWeekDay == -1) {
			g_prev_showWeekDay = showWeekDay;
		}
		weekDayCharLim = (showWeekDay) ? 3 : 0;
#ifdef OSD_DIS_INFO
		static INT32 old_time = 0, sta = 0;
		struct stat buf;
		if (stat(OSD_DIS_PATH, &buf) != 0) {
			if (sta == 0) {
				printf("Err fail to check osd dis path\n");
				sta = 1;
			}
		} else {
			if (old_time == 0) {
				old_time = buf.st_mtime;
			}
			if (buf.st_mtime > old_time) {
				for (i = 0; i < SAMPLE_MAX_VIDEO_CHN_NUM; i++) {
					for (j = 0; j < VIDEO_OSD_MAX_HANDLE_CHANNEL; j++) {
						if (getBindState(i, j)) {
							printf("Region:(%d,%d) %d %d %d %d\n", i, j,
							       g_osd_handle.osd_rgn[i][j].sx,
							       g_osd_handle.osd_rgn[i][j].sy,
							       g_osd_handle.osd_rgn[i][j].ex,
							       g_osd_handle.osd_rgn[i][j].ey);
						}
					}
				}
				showDebug();
			}
			old_time = buf.st_mtime;
		}

#endif /* OSD_DIS_INFO */
		utc = time(NULL);
		localtime_r(&utc, &gmt_tm);

		if (g_prev_weekDay == -1) {
			g_prev_weekDay = gmt_tm.tm_wday;
		}

		memset(timeFormatString, 0, 24);

		if (gmt_tm.tm_hour >= 12) {
			hr = (gmt_tm.tm_hour > 12) ? gmt_tm.tm_hour - 12 : gmt_tm.tm_hour;
			timehalfday = 1;
		} else {
			hr = gmt_tm.tm_hour;
			timehalfday = 0;
		}

		switch (g_time_format) {
		case OSD_TIME_FORMAT_H_MM_SS_TT:
			sprintf(timeFormatString, " %2u:%02u:%02u %s ", hr, gmt_tm.tm_min, gmt_tm.tm_sec,
			        (timehalfday) ? "PM" : "AM");
			break;
		case OSD_TIME_FORMAT_HH_MM_SS_TT:
			sprintf(timeFormatString, " %02u:%02u:%02u %s ", hr, gmt_tm.tm_min, gmt_tm.tm_sec,
			        (timehalfday) ? "PM" : "AM");
			break;
		case OSD_TIME_FORMAT_H_MM_SS:
			sprintf(timeFormatString, " %2u:%02u:%02u %s ", gmt_tm.tm_hour, gmt_tm.tm_min, gmt_tm.tm_sec,
			        "  ");
			break;
		case OSD_TIME_FORMAT_HH_MM_SS:
			sprintf(timeFormatString, " %02u:%02u:%02u %s ", gmt_tm.tm_hour, gmt_tm.tm_min, gmt_tm.tm_sec,
			        "  ");
			break;
		default:
			assert(0);
			break;
		}

		switch (g_date_format) {
		case OSD_DATE_FORMAT_YYYY_MM_DD:
			sprintf(timestring, " %4u-%02u-%02u %s", gmt_tm.tm_year + 1900, gmt_tm.tm_mon + 1,
			        gmt_tm.tm_mday, timeFormatString);
			break;
		case OSD_DATE_FORMAT_MM_DD_YYYY:
			sprintf(timestring, " %02u-%02u-%4u %s", gmt_tm.tm_mon + 1, gmt_tm.tm_mday,
			        gmt_tm.tm_year + 1900, timeFormatString);
			break;
		case OSD_DATE_FORMAT_DD_MM_YYYY:
			sprintf(timestring, " %02u-%02u-%4u %s", gmt_tm.tm_mday, gmt_tm.tm_mon + 1,
			        gmt_tm.tm_year + 1900, timeFormatString);
			break;
		default:
			assert(0);
			break;
		}

		for (i = 0; i < SAMPLE_MAX_VIDEO_CHN_NUM; i++) {
			handle = g_osd_handle.handle[i][g_info_index[i]];
			if ((handle != -1) && (getBindState(i, g_info_index[i]))) {
				fontInfo = &g_fontInfo[g_active_font[i]];
				font_index = fontInfo->font_index;
				font_data = fontInfo->font_data;
				canvas_attr = &p_canvas_attr_time[i];
				time_osd_width = fontInfo->time_osd_width;

				string_lens = strlen(timestring);

				for (j = 0; j < 12; j++) {
					input_ascii = timestring[j];
					input_ascii = input_ascii - 32;
					real_addr = (UINT32)font_data + font_index[input_ascii].image_offset;

					for (k = 0; k < (font_index[input_ascii].image_height); k++) {
						memcpy((char *)((canvas_attr->canvas_addr) + (k * time_osd_width * 2) +
						                (width_acc * 2)),
						       (char *)real_addr, (font_index[input_ascii].image_width * 2));
						real_addr = (real_addr + (font_index[input_ascii].image_width * 2));
					}
					width_acc = width_acc + font_index[input_ascii].image_width;
				}
				for (j = 0; j < weekDayCharLim; j++) {
					input_ascii = week[gmt_tm.tm_wday][j];
					real_addr = (UINT32)font_data + font_index[input_ascii].image_offset;

					for (k = 0; k < (font_index[input_ascii].image_height); k++) {
						memcpy((char *)((canvas_attr->canvas_addr) + (k * time_osd_width * 2) +
						                (width_acc * 2)),
						       (char *)real_addr, (font_index[input_ascii].image_width * 2));
						real_addr = (real_addr + (font_index[input_ascii].image_width * 2));
					}
					width_acc = width_acc + font_index[input_ascii].image_width;
				}
				for (j = 12; j < string_lens; j++) {
					input_ascii = timestring[j];
					input_ascii = input_ascii - 32;
					real_addr = (UINT32)font_data + font_index[input_ascii].image_offset;

					for (k = 0; k < (font_index[input_ascii].image_height); k++) {
						memcpy((char *)((canvas_attr->canvas_addr) + (k * time_osd_width * 2) +
						                (width_acc * 2)),
						       (char *)real_addr, (font_index[input_ascii].image_width * 2));
						real_addr = (real_addr + (font_index[input_ascii].image_width * 2));
					}
					width_acc = width_acc + font_index[input_ascii].image_width;
				}
				/* Refill the blank area */
				/* 0. fill for change of showWeekDay flag */
				if (g_prev_showWeekDay != showWeekDay && showWeekDay == 0) {
					for (j = 0; j < 3; j++) {
						input_ascii = week[gmt_tm.tm_wday][j];
						real_addr = (UINT32)font_data + font_index[input_ascii].image_offset;

						for (k = 0; k < (font_index[input_ascii].image_height); k++) {
							memset((char *)((canvas_attr->canvas_addr) +
							                (k * time_osd_width * 2) + (width_acc * 2)),
							       0, (font_index[input_ascii].image_width * 2));
						}
						width_acc = width_acc + font_index[input_ascii].image_width;
					}
				} else if (g_prev_weekDay != gmt_tm.tm_wday) { /* 0. fill for change of weekday */
					input_ascii = week[0][0];
					fill_width = time_osd_width - width_acc;
					for (k = 0; k < (font_index[input_ascii].image_height); k++) {
						memset((char *)((canvas_attr->canvas_addr) + (k * time_osd_width * 2) +
						                (width_acc * 2)),
						       0, (fill_width * 2));
					}
				}
				width_acc = 0;
				ret = MPI_updateOsdCanvas(handle);
				if (ret != MPI_SUCCESS) {
					printf("OSD %d to update canvas failed.\n", handle);
					return MPI_FAILURE;
				}
			}
		}

		g_prev_weekDay = gmt_tm.tm_wday;
		g_prev_showWeekDay = showWeekDay;
		sleep(1);
	}
	return 0;
}

void createDateTimeThread(INT32 output_num)
{
	channel_numbers = output_num;
	int ret = 0;

	if (pthread_create(&threadUpdateDateTime, NULL, (void *)updateDateTime, NULL)) {
		printf("Create thread to DateTime failed.\n");
		return;
	}

	ret = pthread_setname_np(threadUpdateDateTime, OSD_TH_NAME);
	if (ret != 0) {
		printf("Set thread to DateTime failed. ret %d\n", ret);
		return;
	}

	if (enableTzUpdate()) {
		printf("enableTzUpdate failed.\n");
		return;
	}
}

void deleteDateTimeThread()
{
	INT32 ret = 0;

	ret = pthread_cancel(threadUpdateDateTime);
	if (ret < 0) {
		printf("cancel osd thread failed\n");
	}

	ret = pthread_join(threadUpdateDateTime, NULL);
	if (ret != 0) {
		printf("stop osd thread failed\n");
	}

	if (disableTzUpdate()) {
		printf("disableTzUpdate failed.\n");
	}

	g_prev_showWeekDay = -1;
	g_prev_weekDay = -1;
}

INT32 VIDEO_OSD_setShowWeekDay(INT32 showWeekDay)
{
	g_osd_handle.showWeekDay = showWeekDay;
	return 0;
}

#if 0
static INT32 osdPmCheckParam(INT32 c_idx, MPI_CHN_ATTR_S *chn_attr, const VIDEO_OSD_PM_PARAM_S *param)
{
	INT32 area;
	INT32 width, height;
	if ((param->end.x > chn_attr->res.width) || (param->end.y > chn_attr->res.height)) {
		SYS_TRACE("Privacy Mask region exceeds channel resolution (%d,%d), expected:(%d,%d) \n",
		       chn_attr->res.width, chn_attr->res.height, param->end.x, param->end.y);
		return MPI_FAILURE;
	}
	if ((param->start.x >= param->end.x) || (param->start.y >= param->end.y)) {
		SYS_TRACE("Privacy Mask start point (%d,%d) should not smaller or equal than end point (%d,%d) \n", param->start.x,
		       param->start.y, param->end.x, param->end.y);
		return MPI_FAILURE;
	}

	width = param->end.x - param->start.x;
	height = param->end.y - param->start.y;
	area = width * height;

	if (area > g_osd_handle.privMaskAreaMax[c_idx]) {
		printf("Privacy Mask area: %d should not greater than maximum threshold %d of CHN:%d!\n", area,
		       g_osd_handle.privMaskAreaMax[c_idx], c_idx);
		return MPI_FAILURE;
	}
	/* TODO check if overlap of bind_attr and osd_handle*/

	return MPI_SUCCESS;
}
#endif

static INT32 checkOsdPmParam(INT32 c_idx, MPI_CHN_ATTR_S *chn_attr, const AGTX_OSD_PM_S *param)
{
	INT32 area;
	INT32 width, height;
	if ((param->end_x > chn_attr->res.width) || (param->end_y > chn_attr->res.height)) {
		SYS_TRACE("Privacy Mask region exceeds channel resolution (%d,%d), expected:(%d,%d) \n",
		          chn_attr->res.width, chn_attr->res.height, param->end_x, param->end_y);
		return MPI_FAILURE;
	}
	if ((param->start_x >= param->end_x) || (param->start_y >= param->end_y)) {
		SYS_TRACE("Privacy Mask start point (%d,%d) should not smaller or equal than end point (%d,%d) \n",
		          param->start_x, param->start_y, param->end_x, param->end_y);
		return MPI_FAILURE;
	}

	if (param->color > CR_MAX - 1) {
		SYS_TRACE("Privacy Mask color exceed maximum number(%d) vs expected(%d)\n", CR_MAX - 1, param->color);
		return MPI_FAILURE;
	}

	width = param->end_x - param->start_x;
	height = param->end_y - param->start_y;
	area = width * height;

	if (area > g_osd_handle.privMaskAreaMax[c_idx]) {
		printf("Privacy Mask area: %d should not greater than maximum threshold %d of CHN:%d!\n", area,
		       g_osd_handle.privMaskAreaMax[c_idx], c_idx);
		return MPI_FAILURE;
	}
	/* TODO check if overlap of bind_attr and osd_handle*/

	return MPI_SUCCESS;
}

void osdPmSetParam(MPI_OSD_RGN_ATTR_S *osd_attr, MPI_OSD_BIND_ATTR_S *osd_bind, const VIDEO_OSD_PM_PARAM_S *param,
                   const MPI_SIZE_S *chn_res)
{
	/* FIXME Current only support Rect filling */
	osd_attr->polygon.point_nums = 4;
	osd_attr->polygon.color = (param->color & 0x1fff) + (((int)param->alpha) << OSD_AYUV_ALPHA_BS);

	OSD_DEB("[%s] input: st:(%d,%d), end(%d,%d), cr:(%d), orig bind(%d,%d)\n", __func__, param->start.x,
	        param->start.y, param->end.x, param->end.y, osd_attr->polygon.color, osd_bind->point.x,
	        osd_bind->point.y);

	/* FIXME Current only support Rect filling */
	MPI_POINT_S st;
	UINT16 w, h;
	int width = ((chn_res->width + 15) / 16) * 16;
	int height = ((chn_res->height + 15) / 16) * 16;

	st.x = ((param->start.x * width / 100) / 16) * 16;
	st.y = ((param->start.y * height / 100) / 16) * 16;
	w = (((((param->end.x - param->start.x) * width) / 100) + 15) / 16) * 16;
	h = (((((param->end.y - param->start.y) * height) / 100) + 15) / 16) * 16;
	osd_attr->polygon.point[0].x = 0;
	osd_attr->polygon.point[0].y = 0;
	osd_attr->polygon.point[1].x = w;
	osd_attr->polygon.point[1].y = 0;
	osd_attr->polygon.point[2].x = w;
	osd_attr->polygon.point[2].y = h;
	osd_attr->polygon.point[3].x = 0;
	osd_attr->polygon.point[3].y = h;

	osd_attr->size.width = w;
	osd_attr->size.height = h;

	osd_bind->point.x = st.x;
	osd_bind->point.y = st.y;
	OSD_DEB("[%s] output: st:(%d,%d), end(%d,%d), cr:(%d), sz:(%d,%d), bind(%d,%d)\n", __func__,
	        osd_attr->polygon.point[0].x, osd_attr->polygon.point[0].y, osd_attr->polygon.point[2].x,
	        osd_attr->polygon.point[2].y, osd_attr->polygon.color, osd_attr->size.width, osd_attr->size.height,
	        osd_bind->point.x, osd_bind->point.y);
}

void osdPmGetParam(const MPI_OSD_RGN_ATTR_S *osd_attr, const MPI_OSD_BIND_ATTR_S *osd_bind, VIDEO_OSD_PM_PARAM_S *param)
{
	param->color = osd_attr->polygon.color;
	param->alpha = (osd_attr->polygon.color >> OSD_AYUV_ALPHA_BS);

	param->start.x = osd_attr->polygon.point[0].x + osd_bind->point.x;
	param->start.y = osd_attr->polygon.point[0].y + osd_bind->point.y;
	param->end.x = osd_attr->polygon.point[1].x + osd_bind->point.x;
	param->end.y = osd_attr->polygon.point[2].y + osd_bind->point.y;
}

INT32 osdPmCompareAttrSize(const MPI_OSD_RGN_ATTR_S *osd_attr, const VIDEO_OSD_PM_PARAM_S *new, const MPI_SIZE_S *res)
{
	MPI_SIZE_S old_sz, sz;
	INT32 w, h;
	INT32 ret;
	old_sz.width = osd_attr->size.width;
	old_sz.height = osd_attr->size.height;
	w = (((((new->end.x - new->start.x) * res->width) / 100) + 15) / 16) * 16;
	h = (((((new->end.y - new->start.y) * res->height) / 100) + 15) / 16) * 16;
	sz.width = w;
	sz.height = h;
	ret = memcmp(&old_sz, &sz, sizeof(MPI_SIZE_S)) ? 1 : 0;
	OSD_DEB("old sz:(%d,%d), new:(%d,%d), newcorrd(st:(%d,%d),ed(%d,%d)) ret:%d\n", old_sz.width, old_sz.height,
	        sz.width, sz.height, new->start.x, new->start.y, new->end.x, new->end.y, ret);
	return ret;
}

INT32 osdPmIsSameSize(const MPI_OSD_RGN_ATTR_S *osd_attr, MPI_RECT_POINT_S *new, const MPI_SIZE_S *res)
{
	MPI_SIZE_S old_sz, sz;
	INT32 w, h;
	INT32 ret;
	old_sz.width = osd_attr->size.width;
	old_sz.height = osd_attr->size.height;
	w = (((((new->ex - new->sx) * res->width) / 100) + 15) / 16) * 16;
	h = (((((new->ey - new->sy) * res->height) / 100) + 15) / 16) * 16;
	sz.width = w;
	sz.height = h;
	ret = memcmp(&old_sz, &sz, sizeof(MPI_SIZE_S)) ? 1 : 0;
	OSD_DEB("old sz:(%d,%d), new:(%d,%d), newcorrd(st:(%d,%d),ed(%d,%d)) ret:%d\n", old_sz.width, old_sz.height,
	        sz.width, sz.height, new->sx, new->sy, new->ex, new->ey, ret);
	return ret;
}

INT32 osdPmCompareBindAttr(const MPI_OSD_BIND_ATTR_S *osd_bind, const VIDEO_OSD_PM_PARAM_S *param)
{
	INT32 ret = memcmp(&osd_bind->point, &param->start, sizeof(MPI_POINT_S)) ? 1 : 0;
	OSD_DEB("[%s] output: orig_bind:(%d,%d), st(%d,%d) sz_orig:%d vs sz_parm:%d revat:%d\n", __func__,
	        osd_bind->point.x, osd_bind->point.y, param->start.x, param->start.y, sizeof(osd_bind->point),
	        sizeof(param->start), ret);
	/* return 0 if equal, return 1 if not equal */
	return ret;
}

INT32 osdCreatePrivacyMask(MPI_ECHN chn_idx, INT32 j, UINT16 width, UINT16 height, const AGTX_OSD_PM_PARAM_S *osd_pm)
{
	INT32 ret = 0;
	MPI_OSD_RGN_ATTR_S osd_attr = { 0 };
	MPI_POINT_S st = { 0 };
	INT32 c_idx = chn_idx.chn;
	MPI_RECT_POINT_S rect;

	const AGTX_OSD_PM_S *param = &osd_pm->param[j - VIDEO_OSD_PM_HANDLE_OFFSET];
	osd_attr.show = 1;
	osd_attr.qp_enable = FALSE;
	osd_attr.color_format = OSD_PM_CR_FORMAT;
	osd_attr.osd_type = MPI_OSD_OVERLAY_POLYGON;
	osd_attr.polygon.point_nums = 4;
	osd_attr.polygon.fill = 1;
	osd_attr.polygon.line_width = MPI_OSD_THICKNESS_MIN;
	MPI_OSD_BIND_ATTR_S *osd_bind = &g_osd_handle.osd_bind[c_idx][j];
	setBindStateInternal(c_idx, j, 0);
	OSD_DEB("[%s] chn:%d,j:%d, st(%d,%d) end(%d,%d) cr:%d enabled:%d\n", __func__, c_idx, j, param->start_x,
	        param->start_y, param->end_x, param->end_y, param->color, param->enabled);

	width = ((width + 15) / 16) * 16;
	height = ((height + 15) / 16) * 16;

	if (param->enabled) {
		UINT16 w, h;
		st.x = (((param->start_x * width) / 100) / 16) * 16;
		st.y = (((param->start_y * height) / 100) / 16) * 16;
		w = (((((param->end_x - param->start_x) * width) / 100) + 15) / 16) * 16;
		h = (((((param->end_y - param->start_y) * height) / 100) + 15) / 16) * 16;
		osd_attr.polygon.point[0].x = 0;
		osd_attr.polygon.point[0].y = 0;
		osd_attr.polygon.point[1].x = w;
		osd_attr.polygon.point[1].y = 0;
		osd_attr.polygon.point[2].x = w;
		osd_attr.polygon.point[2].y = h;
		osd_attr.polygon.point[3].x = 0;
		osd_attr.polygon.point[3].y = h;
		osd_attr.polygon.color =
		        (param->alpha << OSD_AYUV_ALPHA_BS) + (VIDEO_OSD_getAyuvColor(param->color) & 0x1fff);
		osd_attr.size.width = w;
		osd_attr.size.height = h;
		osd_bind->point.x = st.x;
		osd_bind->point.y = st.y;
		osd_bind->idx = chn_idx;
		osd_bind->module = 0;

		rect = (MPI_RECT_POINT_S){ .sx = osd_bind->point.x,
			                   .sy = osd_bind->point.y,
			                   .ex = osd_bind->point.x + osd_attr.size.width - 1,
			                   .ey = osd_bind->point.y + osd_attr.size.height - 1 };
		setRgn(c_idx, j, &rect);

		ret = MPI_createOsdRgn(&g_osd_handle.handle[c_idx][j], &osd_attr);

		if (ret != MPI_SUCCESS) {
			printf("Video OSD Privacy mask create region failed.\n");
			return -1;
		}

		ret = MPI_bindOsdToChn(g_osd_handle.handle[c_idx][j], osd_bind);
		if (ret != MPI_SUCCESS) {
			printf("Cannot bind Privacy Mask in Channel: %d handle idx: %d\n", c_idx, j);
			return MPI_FAILURE;
		}
		setBindState(c_idx, j, 1);
		setBindStateInternal(c_idx, j, 1);
	}
	return 0;
}

INT32 VIDEO_OSD_setBindLoc(MPI_ECHN chn_idx, INT32 handle_idx, UINT32 width, UINT32 height,
                           const AGTX_OSD_CONF_INNER_S *rgn)
{
	/* Handle should be created */
	INT32 c_idx = chn_idx.chn;
	UINT32 rect_w, rect_h;
	INT32 ret = 0;
	MPI_RECT_POINT_S *old_rect = &g_osd_handle.osd_rgn[c_idx][handle_idx];
	MPI_RECT_POINT_S rect = { 0 };
	INT32 x = rgn->start_x;
	INT32 y = rgn->start_y;
	/* Early exit */
	if (getBindState(c_idx, handle_idx) != 1)
		return 0;

	rect_h = old_rect->ey - old_rect->sy + 1;
	rect_w = old_rect->ex - old_rect->sx + 1;

	if (x > 100)
		x = 100;
	if (y > 100)
		y = 100;
	if (x < 0)
		x = 0;
	if (y < 0)
		y = 0;

	rect.sx = (((x * width / 100) + 15) / 16) * 16;
	rect.sy = (((y * height / 100) + 15) / 16) * 16;
	rect.ex = rect.sx + rect_w - 1;
	rect.ey = rect.sy + rect_h - 1;
#if (0) /* Should not enter here */
	if (rect.sx < 0) {
		rect.ex -= rect.sx;
		rect.sx -= rect.sx;
	}
	if (rect.sy < 0) {
		rect.ey -= rect.sy;
		rect.sy -= rect.sy;
	}
#endif
	width = ((width + 15) / 16) * 16;
	height = ((height + 15) / 16) * 16;
	/* Make sure sx, sy align 16 */
	if (rect.ex > width - 1) {
		x = (((rect.ex - width) / 16) + 1) * 16;
		rect.ex -= x;
		rect.sx -= x;
	}
	if (rect.ey > height - 1) {
		y = (((rect.ey - height) / 16) + 1) * 16;
		rect.ey -= y;
		rect.sy -= y;
	}

	/* rebind region */
	MPI_OSD_BIND_ATTR_S *osd_bind = &g_osd_handle.osd_bind[c_idx][handle_idx];
	osd_bind->point.x = rect.sx;
	osd_bind->point.y = rect.sy;
	ret = unbindOsdRegion(c_idx, handle_idx);
	if (ret) {
		return -1;
	}
	ret = bindOsdRegion(c_idx, handle_idx, osd_bind);
	if (ret) {
		return -1;
	}
	memcpy(old_rect, &rect, sizeof(MPI_RECT_POINT_S));
	return 0;
}

/* Unused OSD driver support overlapping */
INT32 VIDEO_OSD_checkParam(MPI_ECHN chn_idx, const AGTX_OSD_CONF_OUTER_S *cur, const AGTX_OSD_CONF_OUTER_S *new)
{
	int restream = 0;
	int total_bind_number = 0;
	int c_idx = chn_idx.chn;

	for (int i = VIDEO_OSD_PM_HANDLE_OFFSET; i < VIDEO_OSD_PM_HANDLE_OFFSET + MPI_OSD_MAX_BIND_CHANNEL; i++) {
		total_bind_number += g_osd_handle.bind_state_internal[c_idx][i]; // privacy mask
	}

	for (int i = 0; i < MPI_OSD_MAX_BIND_CHANNEL; i++) {
		total_bind_number += (new->region[i].enabled == 1);
		restream |= cur->region[i].enabled != new->region[i].enabled;
		if (strcmp((const char *)cur->region[i].type_spec, (const char *)new->region[i].type_spec) != 0) {
			if (i == 0) {
				if (strlen((const char *)new->region[i].type_spec) > MAX_TEXT_LEN) {
					SYS_TRACE("[OSD] Text length exceed maximum threshold(%d) vs expected(%d)\n",
					          MAX_TEXT_LEN, strlen((const char *)new->region[i].type_spec));
					return VIDEO_OSD_ERR_PARAM;
				} else {
					restream |= 1;
				}
			} else if (i == 2) {
				if (access((const char *)new->region[i].type_spec, F_OK) == -1) {
					SYS_TRACE("[OSD] %s does not exist\n", new->region[i].type_spec);
					return VIDEO_OSD_ERR_PARAM;
				} else {
					restream |= 1;
				}
			}
		}
	}

	if (total_bind_number > MPI_OSD_MAX_BIND_CHANNEL) {
		SYS_TRACE(
		        "[OSD] Total OSD region number exceed maximum bind channel(%d) vs expected(%d) on channel 0x%08x\n",
		        MPI_OSD_MAX_BIND_CHANNEL, total_bind_number, chn_idx.value);
		return VIDEO_OSD_ERR_PARAM;
	}

	if (restream)
		return VIDEO_OSD_RESTREAM_REQUEST;

	return VIDEO_OSD_SUCCESS;
}

/* Unused OSD driver support overlapping */
INT32 VIDEO_OSD_checkPmParam(MPI_ECHN chn_idx, const AGTX_OSD_PM_PARAM_S *cur, const AGTX_OSD_PM_PARAM_S *new,
                             UINT32 width, UINT32 height)
{
	int restream = 0;
	int total_bind_number = 0;
	int c_idx = chn_idx.chn;
	MPI_CHN vchn_idx = MPI_VIDEO_CHN(0, chn_idx.chn);

	MPI_CHN_ATTR_S chn_attr = { { 0 } };
	MPI_OSD_RGN_ATTR_S osd_attr;
	MPI_RECT_POINT_S rect;
	OSD_HANDLE handle;
	INT32 ret = 0;

	ret = MPI_DEV_getChnAttr(vchn_idx, &chn_attr);
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Get video channel %d attribute failed.\n", c_idx);
		return VIDEO_OSD_FAILURE;
	}

	for (int i = 0; i < MPI_OSD_MAX_BIND_CHANNEL; i++) {
		total_bind_number += g_osd_handle.bind_state_internal[c_idx][i]; // osd
	}

	for (int i = 0, j = VIDEO_OSD_PM_HANDLE_OFFSET; i < MPI_OSD_MAX_BIND_CHANNEL; i++, j++) {
		total_bind_number += (new->param[i].enabled == 1);
		restream |= cur->param[i].enabled != new->param[i].enabled;
		handle = getHandle(c_idx, j);

		if (new->param[i].enabled) {
			if (checkOsdPmParam(c_idx, &chn_attr, &new->param[i])) {
				return VIDEO_OSD_ERR_PARAM;
			}
			if (cur->param[i].enabled == new->param[i].enabled) {
				ret = MPI_getOsdRgnAttr(handle, &osd_attr);
				if (ret != MPI_SUCCESS) {
					SYS_TRACE(
					        "Cannot get OSD region(handle number:%d) attribute for Privacy Mask\n",
					        handle);
					return VIDEO_OSD_FAILURE;
				}
				rect = (MPI_RECT_POINT_S){ new->param[i].start_x, new->param[i].start_y,
					                   new->param[i].end_x, new->param[i].end_y };
				restream |= osdPmIsSameSize(&osd_attr, &rect, &chn_attr.res);
			}
		}
	}
	if (total_bind_number > MPI_OSD_MAX_BIND_CHANNEL) {
		SYS_TRACE(
		        "[OSD] Total OSD region number exceed maximum bind channel(%d) vs expected(%d) on channel 0x%08x\n",
		        MPI_OSD_MAX_BIND_CHANNEL, total_bind_number, chn_idx.value);
		return VIDEO_OSD_ERR_PARAM;
	}

	if (restream)
		return VIDEO_OSD_RESTREAM_REQUEST;

	return VIDEO_OSD_SUCCESS;
}

/**
 * @brief Initialize Privacy Mask by config settings
 * @param[in] chn_idx      video channel index.
 * @param[in] width        channel resolution width.
 * @param[in] height       channel resolution height.
 * @param[in] *osd_pm      OSD privacy mask config params.
 * @retval MPI_SUCCESS                 success.
 * @retval MPI_FAILURE                 unexpected fail.
 */
INT32 VIDEO_OSD_initPrivacyMask(MPI_ECHN chn_idx, UINT32 width, UINT32 height, const AGTX_OSD_PM_PARAM_S *osd_pm)
{
	/* Create privacy mask region buffer */
	OSD_DEB("[%s:%d] Entering\n", __func__, __LINE__);
	INT32 j;
	INT32 c_idx = chn_idx.chn;
	g_osd_handle.privMaskAreaMax[c_idx] = (width * height) / VIDEO_OSD_PM_AREA_DIV;
#ifdef OSD_DEBUG
	for (j = 0; j < 4; j++) {
		printf("[%s] conf st:(%d,%d), end(%d,%d), cr:(%d), alpha:(%d), enabled(%d)\n", __func__,
		       osd_pm->conf[c_idx].param[j].start.x, osd_pm->conf[c_idx].param[j].start.y,
		       osd_pm->conf[c_idx].param[j].end.x, osd_pm->conf[c_idx].param[j].end.y,
		       osd_pm->conf[c_idx].param[j].color, osd_pm->conf[c_idx].param[j].alpha,
		       osd_pm->conf[c_idx].param[j].enabled);
	}
#endif /* !OSD_DEBUG */
	for (j = VIDEO_OSD_PM_HANDLE_OFFSET; j < VIDEO_OSD_MAX_HANDLE_CHANNEL; j++) {
		osdCreatePrivacyMask(chn_idx, j, width, height, osd_pm);
	}
	OSD_DEB("[%s:%d] Exiting \n", __func__, __LINE__);
	//showDebug();

	return 0;
}

/**
 * @brief Delete Privacy Mask by channel
 * @param[in] chn_idx      video channel index.
 * @retval MPI_SUCCESS                 success.
 * @retval MPI_FAILURE                 unexpected fail.
 */
INT32 VIDEO_OSD_deletePrivacyMask(MPI_ECHN chn_idx)
{
	/* Unbind and delete Privacy Mask */
	OSD_DEB("Entering %s\n", __func__);
	INT32 j;
	INT32 ret = MPI_SUCCESS;
	INT32 ret_unbind = MPI_SUCCESS;
	OSD_HANDLE handle;
	MPI_OSD_BIND_ATTR_S osd_bind = { { 0 } };
	INT32 c_idx = chn_idx.chn;

	for (j = VIDEO_OSD_PM_HANDLE_OFFSET; j < VIDEO_OSD_MAX_HANDLE_CHANNEL; j++) {
		handle = getHandle(c_idx, j);
		if (handle != -1) {
			/* force unbind */
			osd_bind.idx = chn_idx;
			if (getBindStateInternal(c_idx, j)) {
				ret_unbind = MPI_unbindOsdFromChn(handle, &osd_bind);
				if (ret_unbind != MPI_SUCCESS) {
					ret_unbind = MPI_FAILURE;
				}
				setBindState(c_idx, j, 0);
				setBindStateInternal(c_idx, j, 0);
			}
			ret = MPI_destroyOsdRgn(handle);
			if (ret != MPI_SUCCESS) {
				if (ret_unbind != MPI_SUCCESS) {
					printf("Unbind OSD %d from channel %d failed.\n", handle,
					       MPI_GET_ENC_CHN(chn_idx));
				}
				printf("Destroy OSD Privacy Mask region %d from channel %d failed.\n", handle,
				       MPI_GET_ENC_CHN(chn_idx));
				ret = MPI_FAILURE;
			}
			setHandle(c_idx, j, -1);
		}
	}
	OSD_DEB("Exiting %s\n", __func__);
	return ret;
}

/**
 * @brief Setup Privacy Mask param
 * @param[in] chn_idx        video channel index.
 * @param[in] chn_handle_idx video channel handle index.
 * @param[in] *param         OSD Pivacy Param structure.
 * @retval MPI_SUCCESS       success.
 * @retval MPI_FAILURE       unexpected fail.
 */
INT32 VIDEO_OSD_setPmParam(MPI_ECHN chn_idx, INT32 chn_handle_idx, const VIDEO_OSD_PM_PARAM_S *param)
{
	MPI_OSD_RGN_ATTR_S osd_attr;
	MPI_OSD_RGN_ATTR_S osd_attr_prev;
	MPI_OSD_BIND_ATTR_S *osd_bind = NULL;
	MPI_OSD_BIND_ATTR_S osd_bind_tmp = { { 0 } };
	MPI_CHN_ATTR_S chn_attr = { { 0 } };
	MPI_RECT_POINT_S rect;
	OSD_HANDLE handle;
	INT32 j = VIDEO_OSD_PM_HANDLE_OFFSET + chn_handle_idx;
	INT32 ret = 0;
	INT32 state = 0;
	INT32 c_idx = chn_idx.chn;
	MPI_CHN vchn_idx = MPI_VIDEO_CHN(0, chn_idx.chn);

	ret = MPI_DEV_getChnAttr(vchn_idx, &chn_attr);
	if (ret != MPI_SUCCESS) {
		printf("Get video channel %d attribute failed.\n", c_idx);
	}

	handle = getHandle(c_idx, j);
	osd_bind = &g_osd_handle.osd_bind[c_idx][j];
	osd_bind_tmp.idx = osd_bind->idx;
	if (handle == -1) {
		OSD_DEB("Set Privacy Mask but OSD handle is not created\n");
		return VIDEO_OSD_ERR_PARAM;
	} else {
		ret = MPI_getOsdRgnAttr(handle, &osd_attr_prev);
		if (ret != MPI_SUCCESS) {
			printf("Cannot get OSD region attribute for Privacy Mask\n");
			return MPI_FAILURE;
		}
		memcpy(&osd_attr, &osd_attr_prev, sizeof(MPI_OSD_RGN_ATTR_S));
	}

	osdPmSetParam(&osd_attr, &osd_bind_tmp, param, &chn_attr.res);
	rect = (MPI_RECT_POINT_S){ .sx = osd_bind_tmp.point.x,
		                   .sy = osd_bind_tmp.point.y,
		                   .ex = osd_bind_tmp.point.x + osd_attr.size.width - 1,
		                   .ey = osd_bind_tmp.point.y + osd_attr.size.height - 1 };

	state = memcmp(&osd_attr_prev, &osd_attr, sizeof(MPI_OSD_RGN_ATTR_S)) != 0;
	if (state) {
		ret = MPI_setOsdRgnAttr(handle, &osd_attr);
		if (ret != MPI_SUCCESS) {
			printf("Cannot set Privacy Mask Param in Channel: %d handle idx: %d\n", c_idx, j);
			return MPI_FAILURE;
		}
	}

	state = memcmp(getRgn(c_idx, j), &rect, sizeof(MPI_RECT_POINT_S)) != 0;
	if (state) {
		memcpy(osd_bind, &osd_bind_tmp, sizeof(MPI_OSD_BIND_ATTR_S));

		ret = unbindOsdRegion(c_idx, j);
		if (ret != MPI_SUCCESS) {
			printf("Cannot unbind Privacy Mask in Channel: %d handle idx: %d\n", c_idx, j);
			return MPI_FAILURE;
		}

		ret = bindOsdRegion(chn_idx.chn, j, osd_bind);
		if (ret != MPI_SUCCESS) {
			printf("Cannot bind Privacy Mask in Channel: %d handle idx: %d\n", c_idx, j);
			return MPI_FAILURE;
		}
	}

	return MPI_SUCCESS;
}

/**
 * @brief Get Privacy Mask param.
 * @param[in] chn_idx        video channel index.
 * @param[in] chn_handle_idx video channel handle index.
 * @param[in] *param         OSD Pivacy Param structure.
 * @retval MPI_SUCCESS       success.
 * @retval MPI_FAILURE       unexpected fail.
 */
INT32 VIDEO_OSD_getPmParam(MPI_ECHN chn_idx, INT32 chn_handle_idx, VIDEO_OSD_PM_PARAM_S *param)
{
	MPI_OSD_RGN_ATTR_S osd_attr;
	INT32 c_idx = chn_idx.chn;
	OSD_HANDLE handle = getHandle(c_idx, VIDEO_OSD_PM_HANDLE_OFFSET + chn_handle_idx);
	INT32 j = VIDEO_OSD_PM_HANDLE_OFFSET + chn_handle_idx;
	MPI_OSD_BIND_ATTR_S *osd_bind = &g_osd_handle.osd_bind[c_idx][j];
	INT32 ret = 0;

	if (handle == -1) {
		printf("OSD region handle for Privacy Mask is not created\n");
		return -1;
	}

	ret = MPI_getOsdRgnAttr(handle, &osd_attr);
	if (ret != MPI_SUCCESS) {
		printf("Cannot get OSD region attribute for Privacy Mask\n");
		return ret;
	}
	osdPmGetParam(&osd_attr, osd_bind, param);
	return 0;
}

/**
 * @brief Bind Privacy Mask region to channel.
 * @param[in] chn_idx        video channel index.
 * @param[in] chn_handle_idx video channel handle index.
 * @retval MPI_SUCCESS       success.
 * @retval MPI_FAILURE       unexpected fail.
 */
INT32 VIDEO_OSD_bindPmToChn(MPI_ECHN chn_idx, INT32 chn_handle_idx)
{
	return bindOsdRegion(chn_idx.chn, chn_handle_idx + VIDEO_OSD_PM_HANDLE_OFFSET, NULL);
}

/**
 * @brief Unbind Privacy Mask region to channel.
 * @param[in] chn_idx        video channel index.
 * @param[in] chn_handle_idx video channel handle index.
 * @retval MPI_SUCCESS       success.
 * @retval MPI_FAILURE       unexpected fail.
 */
INT32 VIDEO_OSD_unbindPmFromChn(MPI_ECHN chn_idx, INT32 chn_handle_idx)
{
	return unbindOsdRegion(chn_idx.chn, chn_handle_idx + VIDEO_OSD_PM_HANDLE_OFFSET);
}
