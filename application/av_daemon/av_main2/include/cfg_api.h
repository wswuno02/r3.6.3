/******************************************************************************
*
* opyright (c) AUGENTIX Inc. - All Rights Reserved
*
* Unauthorized copying of this file, via any medium is strictly prohibited.
*
* Proprietary and confidential.
*
******************************************************************************/

#ifndef AUGENTIX_CONFIG_API_H_
#define AUGENTIX_CONFIG_API_H_

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

#include "mpi_types.h"
#include "mpi_dip_alg.h"
#include "mpi_osd.h"
#include "mpi_sys.h"
#include "agtx_osd.h"
#include "agtx_iva.h"
#include "agtx_iaa.h"
#include "agtx_video_ldc_conf.h"
#include "agtx_video_layout_conf.h"
#include "agtx_video_ptz_conf.h"
#include "agtx_panorama_conf.h"
#include "agtx_panning_conf.h"
#include "agtx_surround_conf.h"
#include "agtx_anti_flicker_conf.h"

#include "agtx_common.h"
#include "agtx_video_dev_conf.h"
#include "agtx_video.h"
#include "agtx_color_conf.h"

#define MAX_PUB_POOL MPI_MAX_PUB_POOL
#define MAX_POOL_NAME_LEN MPI_MAX_PUB_POOL
#define MAX_VIDEO_INPUT 1
#define MAX_INPUT_PATH 2
#define MAX_VIDEO_CHNNEL 4
#define MAX_VIDEO_WINDOW 9
#define MAX_VENC_STREAM 4
#define MAX_OSD_STR_LEN 32
#define MAX_MD_RGN_NUM 64
#define MAX_EF_LINE_NUM 16
#define STITCH_SENSOR_NUM 2
#define STITCH_TABLE_NUM 3
#define MAX_ANTI_FLICKER_FREQUENCY_SIZE 2
#define TMP_ACTIVE_DB "/tmp/ini.db"

typedef enum {
	CFG_AWB_MANUAL,
	CFG_AWB_AUTO,
} CFG_AWB_MODE_E;

typedef struct {
	unsigned char group;
	unsigned char id;
} CFG_ATTR_S;

typedef struct {
	UINT32 enable;
	UINT32 chn_idx;
	UINT32 max_width;
	UINT32 max_height;
	AGTX_STRM_PARAM_S strm;
	AGTX_STITCH_CONF_S stitch;
	AGTX_IVA_TD_CONF_S td;
	AGTX_IVA_MD_CONF_S md;
	AGTX_IVA_AROI_CONF_S aroi;
	AGTX_IVA_PD_CONF_S pd;
	AGTX_IVA_OD_CONF_S od;
	AGTX_IVA_RMS_CONF_S rms;
	AGTX_IVA_LD_CONF_S ld;
	AGTX_IVA_EF_CONF_S ef;
	AGTX_VDBG_CONF_S vdbg;
	AGTX_IVA_SHD_CONF_S shd;
	AGTX_IVA_EAIF_CONF_S eaif;
	AGTX_IVA_PFM_CONF_S pfm;
	AGTX_IVA_BM_CONF_S bm;
	AGTX_OSD_CONF_OUTER_S osd;
	AGTX_OSD_PM_PARAM_S osd_pm;
	AGTX_LDC_CONF_S ldc;
	AGTX_PANORAMA_CONF_S panorama;
	AGTX_PANNING_CONF_S panning;
	AGTX_SURROUND_CONF_S surround;
	UINT32 layout_en;
	AGTX_LAYOUT_PARAM_S layout;
	AGTX_LAYOUT_PARAM_S lyt_cfg; /* Save transformed window attr */
} CFG_VCHN_S;

typedef struct {
	UINT32 enable;
	UINT32 chn_idx;
	UINT32 max_width;
	UINT32 max_height;
	UINT32 bind_dev_idx;
	UINT32 bind_chn_idx;
	AGTX_STRM_PARAM_S strm;
} CFG_VENC_S;

typedef struct {
	UINT32 enable;
	AGTX_DEV_CONF_S cfg_dev;
	CFG_VCHN_S cfg_vchn[MAX_VIDEO_CHNNEL];
	UINT32 alarm_on;
	AGTX_VIDEO_PTZ_CONF_S ptz;
	AGTX_IAA_LSD_CONF_S lsd;
} CFG_VIN_S;

typedef struct {
	UINT32 enable;
	CFG_VIN_S cfg_vin;
	CFG_VENC_S cfg_venc[MAX_VENC_STREAM];
	AGTX_IMG_PREF_S cfg_img;
	AGTX_ADV_IMG_PREF_S cfg_adv_img;
	AGTX_AWB_PREF_S cfg_awb;
	AGTX_COLOR_CONF_S cfg_color;
	AGTX_ANTI_FLICKER_CONF_S cfg_anti_flicker;
} CFG_CAMERA_S;

typedef struct {
	int (*set)(void *data, char *errstr);
	int (*start)(void);
	int (*stop)(void);
	int (*check_valid)(int sockfd, void *data, char *errstr);
	int (*check_resart)(void *data, char *errstr);
} CmdHandler;

/* Interface function prototype */
INT32 CFG_init();
INT32 CFG_exit();
INT32 CFG_startAllModule();
INT32 CFG_closeAllModule();

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* AUGENTIX_CONFIG_API_H_ */
