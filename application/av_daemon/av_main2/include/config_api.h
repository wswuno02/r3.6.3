/******************************************************************************
*
* opyright (c) AUGENTIX Inc. - All Rights Reserved
*
* Unauthorized copying of this file, via any medium is strictly prohibited.
*
* Proprietary and confidential.
*
******************************************************************************/

#ifndef AUGENTIX_CONFIG_API_H_
#define AUGENTIX_CONFIG_API_H_

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

#include "mpi_types.h"
#include "mpi_dip_alg.h"
#include "mpi_osd.h"
#include "mpi_sys.h"
#include "agtx_osd.h"
#include "agtx_iva.h"
#include "agtx_iaa.h"
#include "agtx_video_ldc_conf.h"
#include "agtx_video_layout_conf.h"
#include "agtx_video_ptz_conf.h"
#include "agtx_panorama_conf.h"
#include "agtx_panning_conf.h"
#include "agtx_surround_conf.h"
#include "agtx_anti_flicker_conf.h"

#define MAX_PUB_POOL MPI_MAX_PUB_POOL
#define MAX_POOL_NAME_LEN MPI_MAX_PUB_POOL
#define MAX_VIDEO_INPUT 1
#define MAX_INPUT_PATH 2
#define MAX_VIDEO_CHNNEL 4
#define MAX_VIDEO_WINDOW 9
#define MAX_VENC_STREAM 4
#define MAX_OSD_STR_LEN 32
#define MAX_MD_RGN_NUM 64
#define MAX_EF_LINE_NUM 16
#define STITCH_SENSOR_NUM 2
#define STITCH_TABLE_NUM 3
#define MAX_ANTI_FLICKER_FREQUENCY_SIZE 2

typedef enum {
	CFG_GET_CONFIG_SYS = 0,
	CFG_GET_CONFIG_VIN,
	CFG_GET_CONFIG_VCHN,
	CFG_GET_CONFIG_VENC,
	CFG_GET_CONFIG_IMG,
	CFG_GET_CONFIG_ADV_IMG,
	CFG_GET_CONFIG_AWB,
	CFG_GET_CONFIG_COLOR,
	CFG_GET_CONFIG_TD,
	CFG_GET_CONFIG_MD,
	CFG_GET_CONFIG_AROI,
	CFG_GET_CONFIG_PD,
	CFG_GET_CONFIG_OD,
	CFG_GET_CONFIG_RMS,
	CFG_GET_CONFIG_LD,
	CFG_GET_CONFIG_EF,
	CFG_GET_CONFIG_VDBG,
	CFG_GET_CONFIG_PTZ,
	CFG_GET_CONFIG_SHD,
	CFG_GET_CONFIG_EAIF,
	CFG_GET_CONFIG_PFM,
	CFG_GET_CONFIG_BM,
	CFG_GET_CONFIG_LSD,
	CFG_GET_CONFIG_OSD,
	CFG_GET_CONFIG_OSD_PM,
	CFG_GET_CONFIG_ANTI_FLICKER,
	CFG_GET_CONFIG_MAX,
} CFG_GET_CONFIG_E;

typedef enum {
	CFG_SET_CONFIG_SYS = 0,
	CFG_SET_CONFIG_VIN,
	CFG_SET_CONFIG_VCHN,
	CFG_SET_CONFIG_VENC,
	CFG_SET_CONFIG_IMG,
	CFG_SET_CONFIG_ADV_IMG,
	CFG_SET_CONFIG_AWB,
	CFG_SET_CONFIG_COLOR,
	CFG_SET_CONFIG_TD,
	CFG_SET_CONFIG_MD,
	CFG_SET_CONFIG_AROI,
	CFG_SET_CONFIG_PD,
	CFG_SET_CONFIG_OD,
	CFG_SET_CONFIG_RMS,
	CFG_SET_CONFIG_LD,
	CFG_SET_CONFIG_EF,
	CFG_SET_CONFIG_VDBG,
	CFG_SET_CONFIG_PTZ,
	CFG_SET_CONFIG_SHD,
	CFG_SET_CONFIG_EAIF,
	CFG_SET_CONFIG_PFM,
	CFG_SET_CONFIG_BM,
	CFG_SET_CONFIG_LSD,
	CFG_SET_CONFIG_OSD,
	CFG_SET_CONFIG_OSD_PM,
	CFG_SET_CONFIG_NOTIFY,
	CFG_SET_CONFIG_ANTI_FLICKER,
	CFG_SET_CONFIG_MAX,
} CFG_SET_CONFIG_E;

typedef enum {
	CFG_BAYER_TYPE_G0 = 0, /**< Bayer phase G0.*/
	CFG_BAYER_TYPE_R, /**< Bayer phase R.*/
	CFG_BAYER_TYPE_B, /**< Bayer phase B.*/
	CFG_BAYER_TYPE_G1, /**< Bayer phase G1.*/
	CFG_BAYER_TYPE_MAX, /**< The number of Bayer phase. */
} CFG_BAYER_TYPE_E;

typedef enum {
	CFG_VENC_TYPE_H264 = 0,
	CFG_VENC_TYPE_H265,
	CFG_VENC_TYPE_MJPEG,
	CFG_VENC_TYPE_JPEG,
	CFG_VENC_TYPE_MAX,
} CFG_VENC_TYPE_E;

typedef enum {
	CFG_VENC_RC_MODE_VBR = 0,
	CFG_VENC_RC_MODE_CBR,
	CFG_VENC_RC_MODE_CQP,
	CFG_VENC_RC_MODE_MAX,
} CFG_VENC_RC_MODE_E;

typedef enum {
	CFG_H264_5_PROFILE_BASELINE = 0,
	CFG_H264_5_PROFILE_MAIN,
	CFG_H264_5_PROFILE_HIGH,
	CFG_H264_5_PROFILE_MAX,
} CFG_H264_5_PROFILE_E;

typedef enum { CFG_IVA_MD_MODE_AREA, CFG_IVA_MD_MODE_ENERGY } CFG_IVA_MD_MODE_E;

typedef enum {
	CFG_IVA_LD_TRIGGER_COND_LIGHT_ON,
	CFG_IVA_LD_TRIGGER_COND_LIGHT_OFF,
	CFG_IVA_LD_TRIGGER_COND_BOTH,
} CFG_IVA_LD_TRIGGER_COND_E;

typedef enum {
	CFG_IVA_EF_MODE_DIR_NONE,
	CFG_IVA_EF_MODE_DIR_POS,
	CFG_IVA_EF_MODE_DIR_NEG,
	CFG_IVA_EF_MODE_DIR_BOTH
} CFG_IVA_EF_MODE_E;

typedef enum {
	CFG_ANTI_FLICKER_50HZ,
	CFG_ANTI_FLICKER_60HZ,
	CFG_ANTI_FLICKER_AUTO,
	CFG_ANTI_FLICKER_OFF,
} CFG_ANTI_FLICKER_E;

typedef enum {
	CFG_AWB_MANUAL,
	CFG_AWB_AUTO,
} CFG_AWB_MODE_E;

typedef enum {
	CFG_ROTATE_0,
	CFG_ROTATE_90,
	CFG_ROTATE_180,
	CFG_ROTATE_270,
	CFG_ROTATE_NUM,
} CFG_ROTATE_E;

typedef enum { CFG_NIGHT_MODE_OFF, CFG_NIGHT_MODE_ON, CFG_NIGHT_MODE_AUTO } CFG_NIGHT_MODE_E;

typedef enum { CFG_IMAGE_MODE_COLOR, CFG_IMAGE_MODE_GRAYSCALE, CFG_IMAGE_MODE_AUTO } CFG_IMAGE_MODE_E;

typedef enum { CFG_ICR_MODE_OFF, CFG_ICR_MODE_ON, CFG_ICR_MODE_AUTO } CFG_ICR_MODE_E;

typedef enum { CFG_COLOR_MODE_DAY, CFG_COLOR_MODE_NIGHT } CFG_COLOR_MODE_E;

typedef struct {
	unsigned char group;
	unsigned char id;
} CFG_ATTR_S;

typedef struct {
	UINT32 enable;
	UINT32 path_idx;
	UINT32 sensor_idx;
	UINT32 width;
	UINT32 height;
} CFG_PATH_S;

typedef struct {
	UINT8 enable;
	struct {
		UINT16 x;
		UINT16 y;
	} center[STITCH_SENSOR_NUM];
	INT32 dft_dist;
	INT32 table_num;
	struct {
		UINT16 dist;
		UINT16 ver_disp;
		UINT16 straighten;
		UINT16 src_zoom;
		INT16 theta[STITCH_SENSOR_NUM];
		UINT16 radius[STITCH_SENSOR_NUM];
		UINT16 curvature[STITCH_SENSOR_NUM];
		UINT16 fov_ratio[STITCH_SENSOR_NUM];
		UINT16 ver_scale[STITCH_SENSOR_NUM];
		INT16 ver_shift[STITCH_SENSOR_NUM];
	} table[STITCH_TABLE_NUM];
} CFG_STITCH_S;

typedef struct {
	CFG_IVA_MD_MODE_E mode;
	AGTX_IVA_MD_DET_E det_method;
	INT32 ey; /* End coordinates of detection region. (inclusive) */
	INT32 ex; /* End coordinates of detection region. (inclusive) */
	INT32 id;
	INT32 sx; /* Start coordinates of detection region. (inclusive) */
	INT32 sy; /* Start coordinates of detection region. (inclusive) */
	INT32 sens;
	INT32 max_spd;
	INT32 min_spd;
	INT32 obj_life_th;
} CFG_IVA_MD_REGION_S;

typedef struct {
	INT32 end_x; /* End coordinates of detection region. (inclusive) */
	INT32 end_y; /* End coordinates of detection region. (inclusive) */
	INT32 start_x; /* Start coordinates of detection region. (inclusive) */
	INT32 start_y; /* Start coordinates of detection region. (inclusive) */
} CFG_IVA_LD_REGION_S;

typedef struct {
	INT32 end_x; /* End coordinates of detection region. (inclusive) */
	INT32 end_y; /* End coordinates of detection region. (inclusive) */
	INT32 id;
	CFG_IVA_EF_MODE_E mode;
	INT32 obj_min_w;
	INT32 obj_min_h;
	INT32 obj_max_w;
	INT32 obj_max_h;
	INT32 obj_area;
	INT32 obj_v_th;
	INT32 start_x; /* Start coordinates of detection region. (inclusive) */
	INT32 start_y; /* Start coordinates of detection region. (inclusive) */
	INT32 obj_life_th;
} CFG_IVA_EF_LINE_S;

typedef struct {
	INT32 enabled;
	INT32 en_rgn;
	INT32 en_skip_shake;
	INT32 en_skip_pd;
	INT32 rgn_cnt;
	CFG_IVA_MD_MODE_E mode;
	AGTX_IVA_MD_DET_E det_method;
	INT32 sens;
	INT32 alarm_buffer;
	INT32 max_spd;
	INT32 min_spd;
	INT32 obj_life_th;
	CFG_IVA_MD_REGION_S rgn_list[MAX_MD_RGN_NUM];
} CFG_IVA_MD_S;

typedef struct {
	INT32 enabled;
	INT32 sensitivity;
	INT32 endurance;
	INT32 register_scene;
} CFG_IVA_TD_S;

typedef AGTX_IVA_AROI_CONF_S CFG_IVA_AROI_S;

typedef struct {
	INT32 enabled;
	INT32 max_aspect_ratio_w;
	INT32 max_aspect_ratio_h;
	INT32 min_aspect_ratio_w;
	INT32 min_aspect_ratio_h;
	INT32 max_size;
	INT32 min_size;
	INT32 obj_life_th;
} CFG_IVA_PD_S;

typedef struct {
	INT32 enabled;
	INT32 sensitivity;
	CFG_IVA_LD_REGION_S det_region;
	CFG_IVA_LD_TRIGGER_COND_E trig_cond;
} CFG_IVA_LD_S;

typedef struct {
	INT32 enabled;
	INT32 en_shake_det;
	INT32 en_crop_outside_obj;
	INT32 od_qual;
	INT32 od_track_refine;
	INT32 od_size_th;
	INT32 od_sen;
	INT32 en_stop_det;
} CFG_IVA_OD_S;

typedef struct {
	INT32 enabled;
	INT32 sensitivity;
	INT32 split_x;
	INT32 split_y;
} CFG_IVA_RMS_S;

typedef struct {
	INT32 enabled;
	INT32 line_cnt;
	CFG_IVA_EF_LINE_S line_list[MAX_EF_LINE_NUM];
} CFG_IVA_EF_S;

typedef struct {
	INT32 enabled;
	INT32 ctx;
} CFG_VDBG_S;

typedef AGTX_VIDEO_PTZ_CONF_S CFG_PTZ_S;

typedef AGTX_IVA_SHD_CONF_S CFG_IVA_SHD_S;

typedef AGTX_IVA_EAIF_CONF_S CFG_IVA_EAIF_S;

typedef AGTX_IVA_PFM_CONF_S CFG_IVA_PFM_S;

typedef AGTX_IVA_BM_CONF_S CFG_IVA_BM_S;

typedef AGTX_IAA_LSD_CONF_S CFG_IAA_LSD_S;

typedef AGTX_OSD_PM_CONF_S CFG_OSD_PM_S;

typedef struct {
	UINT32 enable;
	UINT32 chn_idx;
	UINT32 width;
	UINT32 height;
	UINT32 max_width;
	UINT32 max_height;
	UINT32 fps;
	CFG_ROTATE_E rotate;
	UINT32 mirror;
	UINT32 flip;
	CFG_STITCH_S stitch;
	CFG_IVA_TD_S td;
	CFG_IVA_MD_S md;
	CFG_IVA_AROI_S aroi;
	CFG_IVA_PD_S pd;
	CFG_IVA_OD_S od;
	CFG_IVA_RMS_S rms;
	CFG_IVA_LD_S ld;
	CFG_IVA_EF_S ef;
	CFG_VDBG_S vdbg;
	CFG_IVA_SHD_S shd;
	CFG_IVA_EAIF_S eaif;
	CFG_IVA_PFM_S pfm;
	CFG_IVA_BM_S bm;
	AGTX_OSD_CONF_OUTER_S osd;
	AGTX_OSD_PM_PARAM_S osd_pm;
	AGTX_LDC_CONF_S ldc;
	AGTX_PANORAMA_CONF_S panorama;
	AGTX_PANNING_CONF_S panning;
	AGTX_SURROUND_CONF_S surround;
	UINT32 layout_en;
	AGTX_LAYOUT_PARAM_S layout;
	AGTX_LAYOUT_PARAM_S lyt_cfg; /* Save transformed window attr */
} CFG_VCHN_S;

typedef struct {
	UINT32 enable;
	UINT32 dev_idx;
	UINT32 hdr_mode;
	UINT32 stitch_en;
	UINT32 eis_en;
	UINT32 bayer;
	FLOAT fps;
	CFG_PATH_S cfg_path[MAX_INPUT_PATH];
	CFG_VCHN_S cfg_vchn[MAX_VIDEO_CHNNEL];
	UINT32 alarm_on;
	CFG_PTZ_S ptz;
	CFG_IAA_LSD_S lsd;
} CFG_VIN_S;

typedef struct {
	UINT32 enable;
	UINT32 chn_idx;
	UINT32 width;
	UINT32 height;
	UINT32 max_width;
	UINT32 max_height;
	UINT32 fps;
	CFG_VENC_TYPE_E venc_type;
	CFG_H264_5_PROFILE_E venc_profile;
	UINT32 bind_dev_idx;
	UINT32 bind_chn_idx;
	/*RC ATTR*/
	CFG_VENC_RC_MODE_E rc_mode;
	UINT32 gop;
	/*CBR*/
	UINT32 bitrate;
	UINT32 min_qp;
	UINT32 max_qp;
	UINT32 min_q_factor;
	UINT32 max_q_factor;
	UINT32 fluc_level;
	UINT32 regression_speed;
	UINT32 scene_smooth;
	UINT32 i_continue_weight;
	INT32 i_qp_offset;
	/*VBR*/
	UINT32 vbr_max_bit_rate;
	UINT32 vbr_quality_level_index;
	/*SBR*/
	UINT32 sbr_adjust_br_thres_pc;
	UINT32 sbr_adjust_step_times;
	UINT32 sbr_converge_frame;
	/*CQP*/
	UINT32 cqp_i_frame_qp;
	UINT32 cqp_p_frame_qp;
	/*Optical-flow Bit-rate Saving*/
	UINT32 obs;
	UINT32 obs_off_period;
} CFG_VENC_S;

typedef struct {
	CFG_ANTI_FLICKER_E anti_flicker; /**< anti-flicker mode. */
	UINT8 brightness; /**< Brightness strength. */
	UINT8 contrast; /**< Contrast strength. */
	UINT8 sharpness; /**< sharpness strength. */
	UINT8 saturation; /**< saturation strength. */
	INT16 hue; /**< hue strength. */
} CFG_IMG_S;

typedef struct {
	CFG_AWB_MODE_E mode; /**< manual mode */
	UINT16 color_temp; /**< color temperature */
	UINT8 r_gain; /**< Extra red channel gain */
	UINT8 b_gain; /**< Extra blue channel gain */
} CFG_AWB_S;

typedef struct {
	INT32 backlight_compensation;
	CFG_ICR_MODE_E icr_mode;
	CFG_IMAGE_MODE_E image_mode;
	CFG_NIGHT_MODE_E night_mode;
	INT32 ir_light_suppression;
	INT32 wdr_en;
	INT32 wdr_strength;
} CFG_ADV_IMG_S;

typedef struct {
	CFG_COLOR_MODE_E color_mode;
} CFG_COLOR_S;

typedef struct {
	UINT32 enable;
	CFG_VIN_S cfg_vin;
	CFG_VENC_S cfg_venc[MAX_VENC_STREAM];
	CFG_IMG_S cfg_img;
	CFG_ADV_IMG_S cfg_adv_img;
	CFG_AWB_S cfg_awb;
	CFG_COLOR_S cfg_color;
	AGTX_ANTI_FLICKER_CONF_S cfg_anti_flicker;
} CFG_CAMERA_S;

typedef struct {
	CFG_CAMERA_S cfg_camera[MAX_VIDEO_INPUT];
} CFG_ALL_S;

/* Interface function prototype */
INT32 CFG_init();
INT32 CFG_exit();
INT32 CFG_startAllModule();
INT32 CFG_closeAllModule();
INT32 CFG_getConfig(CFG_GET_CONFIG_E cmd, CFG_ATTR_S *attr, void *data, int size);
INT32 CFG_setConfig(CFG_SET_CONFIG_E cmd, CFG_ATTR_S *attr, void *data, int size);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* AUGENTIX_CONFIG_API_H_ */
