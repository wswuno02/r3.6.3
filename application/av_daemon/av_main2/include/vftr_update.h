/******************************************************************************
*
* Copyright (c) AUGENTIX Inc. - All Rights Reserved
*
* Unauthorized copying of this file, via any medium is strictly prohibited.
*
* Proprietary and confidential.
*
******************************************************************************/
#ifndef VFTR_UPDATE_H_
#define VFTR_UPDATE_H_

#include "mpi_types.h"

INT32 VFTR_UPDATE_init();
INT32 VFTR_UPDATE_exit();

#endif /* VFTR_UPDATE_H_ */
