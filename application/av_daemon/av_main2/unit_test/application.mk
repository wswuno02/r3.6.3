SDKSRC_DIR ?= $(realpath $(CURDIR)/../../../..)
include $(SDKSRC_DIR)/application/internal.mk

BSP:=$(SDKSRC_DIR)
MPP:=$(BSP)/mpp
APP:=$(BSP)/application
FEATURE:=$(BSP)/feature

SRC_KO_PATH:=$(MPP)/ko
TARGET_BIN_PATH:=$(SYSTEM_BIN)
TARGET_KO_PATH:=$(SYSTEM_LIB)
AV_MAIN_PATH:=$(SDKSRC_DIR)/application/av_main

SENS:=$(MPP)/custom/sensor
SAMPLE:=$(MPI_STREAM_PATH)/src/libsample
RTSP:=$(AV_MAIN_PATH)/rtsp
VFTR:=$(AV_MAIN_PATH)/vftr
MPP_INC:=$(MPP)/include
MPP_LIB:=$(MPP)/lib
CONFIG_INC:=$(AV_MAIN_PATH)/av_main/config/include
COMMON_INC:=$(AV_MAIN_PATH)/av_main/include
VIDEO_INC:=$(AV_MAIN_PATH)/av_main/video/include

LD_PATH:=$(FEATURE)/libld
LD_INC:=$(LD_PATH)/inc
TD_PATH:=$(FEATURE)/libtd
TD_INC:=$(TD_PATH)/inc
MD_PATH:=$(FEATURE)/libmd
MD_INC:=$(MD_PATH)/inc
EF_PATH:=$(FEATURE)/libef
EF_INC:=$(EF_PATH)/inc

# Select sensor type
SENSOR ?= PS5510_MT800


SNS=$(shell echo $(SENSOR) | tr '[A-Z]' '[a-z]')

SENSOR_LIBS := $(SENS)/libsensor_$(SNS).a

export SENSOR:=$(SENSOR)
export SENS:=$(SENS)
export MPP_INC:=$(MPP_INC)
export COMMON_INC:=$(COMMON_INC)
export SAMPLE:=$(SAMPLE)
export INIPARSER:=$(INIPARSER)
