/*
 * AUGENTIX INC. - PROPRIETARY
 *
 * FILENAME - FILE_DESCRIPTION
 * Copyright (C) 2018 Augentix Inc. - All Rights Reserved
 *
 * NOTICE: The information contained herein is the property of Augentix Inc.
 * Copying and distributing of this file, via any medium,
 * must be licensed by Augentix Inc.
 *
 * * Brief: Dummy Unix Socket synchronous IO server Supports Multi client connections
 *
 * * Author: Rowan <rowan.lin@@augentix.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>

#include "agtx_cmd.h"
#include "fake_cc.h"

#define CC_MAX_CLIENT_NUM 5
#define CC_JSON_STR_BUF_SIZE 512

char *cc_skt_path = "/tmp/ccUnxSkt";
int ccUnxSktFD;

int main(int argc, char **argv)
{
	int addrlen, valread;
	int new_socket;
	int opt;
	struct sockaddr_un addr;
	char buf[CC_JSON_STR_BUF_SIZE];
	char ret_buf[CC_JSON_STR_BUF_SIZE];

	if ((ccUnxSktFD = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
		fprintf(stderr, "socket error \n");
		exit(-1);
	}

	/* set master socket to allow multiple connections. */
	if (setsockopt(ccUnxSktFD, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0) {
		fprintf(stderr, "setsockopt error \n");
		exit(-1);
	}

	//Type of Socket (Unix Socket)
	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	strcpy(addr.sun_path, cc_skt_path);

	//bind the socket to socketfd file
	if (bind(ccUnxSktFD, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
		fprintf(stderr, "bind error \n");
		exit(-1);
	}

	//Listen on the socketfile
	if (listen(ccUnxSktFD, CC_MAX_CLIENT_NUM) == -1) {
		fprintf(stderr, "listen error \n");
		exit(-1);
	}

	/* accept incoming connections */
	addrlen = sizeof(addr);

	while (1) {
		if ((new_socket = accept(ccUnxSktFD, (struct sockaddr *)&addr, (socklen_t *)&addrlen)) < 0) {
			fprintf(stderr, "Accept error ..\n");
			break;
		}

		//debuf print socket number - used in send and receive commands
		fprintf(stderr, "New connection , socket fd is %d\n", new_socket);

		valread = read(new_socket, buf, CC_JSON_STR_BUF_SIZE);

		if (valread == 0) {
			fprintf(stderr, "Client disconnected\n");
			close(new_socket);
			new_socket = 0;
			continue;
		} else if (valread == -1) {
			fprintf(stderr, "Failed to read! %s\n", strerror(errno));
			close(new_socket);
			new_socket = 0;
			break;
		}

		sprintf(ret_buf, "{ \"master_id\": 0, \"cmd_id\": %d, \"cmd_type\": \"reply\", \"rval\": 0 }",
		        AGTX_CMD_REG_CLIENT);
		if (send(new_socket, ret_buf, strlen(ret_buf), 0) == -1) {
			fprintf(stderr, "Unable to reply client connection ! %s\n", strerror(errno));
		}

		/* TODO */

		FCC_sendVBufConf(new_socket);
		FCC_sendStitchConf(new_socket);

		FCC_sendImgPref(new_socket);
		FCC_sendAwbPref(new_socket);
		FCC_sendAdvImgPref(new_socket);

		FCC_sendDipCalConf(new_socket);
		FCC_sendDipDbcConf(new_socket);
		FCC_sendDipDccConf(new_socket);
		FCC_sendDipLscConf(new_socket);
		FCC_sendDipRoiConf(new_socket);

		FCC_sendDipCtlConf(new_socket);
		FCC_sendDipAeConf(new_socket);
		FCC_sendDipAwbConf(new_socket);
		FCC_sendDipCsmConf(new_socket);
		FCC_sendDipPtaConf(new_socket);
		FCC_sendDipShpConf(new_socket);
		FCC_sendDipNrConf(new_socket);
		FCC_sendDipTeConf(new_socket);
		FCC_sendDipGammaConf(new_socket);
	}

	close(ccUnxSktFD);

	return 0;
}
