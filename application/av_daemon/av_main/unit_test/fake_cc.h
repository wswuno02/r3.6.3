#ifndef AV_MAIN_FAKE_CC_H_
#define AV_MAIN_FAKE_CC_H_

int FCC_sendVBufConf(int sockfd);
int FCC_sendStitchConf(int sockfd);

int FCC_sendImgPref(int sockfd);
int FCC_sendAwbPref(int sockfd);
int FCC_sendAdvImgPref(int sockfd);

int FCC_sendDipCalConf(int sockfd);
int FCC_sendDipDbcConf(int sockfd);
int FCC_sendDipDccConf(int sockfd);
int FCC_sendDipLscConf(int sockfd);
int FCC_sendDipRoiConf(int sockfd);
int FCC_sendDipCtlConf(int sockfd);
int FCC_sendDipAeConf(int sockfd);
int FCC_sendDipAwbConf(int sockfd);
int FCC_sendDipCsmConf(int sockfd);
int FCC_sendDipPtaConf(int sockfd);
int FCC_sendDipShpConf(int sockfd);
int FCC_sendDipNrConf(int sockfd);
int FCC_sendDipTeConf(int sockfd);
int FCC_sendDipGammaConf(int sockfd);

#endif
