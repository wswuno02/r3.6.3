#include "fake_cc.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include "agtx_cmd.h"
#include "agtx_video.h"
#include "agtx_dip_all_conf.h"

static int __sendConf(int sockfd, AGTX_MSG_HEADER_S *cmd, const void *conf)
{
	char buf[512];

	if (write(sockfd, cmd, sizeof(*cmd)) < 0) {
		fprintf(stderr, "write socket error %d(%m)\n", errno);
		return -1;
	}

	if (write(sockfd, conf, cmd->len) < 0) {
		fprintf(stderr, "write socket error %d(%m)\n", errno);
		return -1;
	}

	if (read(sockfd, cmd, sizeof(*cmd) < 0)) {
		fprintf(stderr, "wait reply cmd error %d(%m)\n", errno);
		return -1;
	}

	if (read(sockfd, buf, sizeof(buf) < 0)) {
		fprintf(stderr, "wait reply value error %d(%m)\n", errno);
		return -1;
	}

	return 0;
}

/*
int FCC_sendDipAeConf(int sockfd)
{
	const AGTX_DIP_AE_CONF_S conf = { 0 };
	AGTX_MSG_HEADER_S cmd_header = {0};

	cmd_header.cid = AGTX_CMD_DIP_AE;
	cmd_header.len = sizeof(conf);

	return __sendConf(sockfd, &cmd_header, &conf);
}
*/

int FCC_sendVBufConf(int sockfd)
{
	const AGTX_VB_CONF_S conf = { 0 };
	AGTX_MSG_HEADER_S cmd_header = { 0 };

	cmd_header.cid = AGTX_CMD_VIDEO_BUF_CONF;
	cmd_header.len = sizeof(conf);

	return __sendConf(sockfd, &cmd_header, &conf);
}

int FCC_sendStitchConf(int sockfd)
{
	const AGTX_STITCH_CONF_S conf = { 0 };
	AGTX_MSG_HEADER_S cmd_header = { 0 };

	cmd_header.cid = AGTX_CMD_STITCH_CONF;
	cmd_header.len = sizeof(conf);

	return __sendConf(sockfd, &cmd_header, &conf);
}

int FCC_sendImgPref(int sockfd)
{
	AGTX_IMG_PREF_S conf;
	AGTX_MSG_HEADER_S cmd_header = { 0 };

	cmd_header.cid = AGTX_CMD_IMG_PREF;
	cmd_header.len = sizeof(conf);

	conf.anti_flicker = AGTX_ANTI_FLICKER_AUTO;
	conf.brightness = 90;
	conf.contrast = 150;
	conf.saturation = 255;
	conf.hue = 50;
	conf.sharpness = 0;

	return __sendConf(sockfd, &cmd_header, &conf);
}

int FCC_sendAwbPref(int sockfd)
{
	AGTX_AWB_PREF_S conf;
	AGTX_MSG_HEADER_S cmd_header = { 0 };

	cmd_header.cid = AGTX_CMD_AWB_PREF;
	cmd_header.len = sizeof(conf);

	conf.mode = 0;
	conf.color_temp = 2505;
	conf.b_gain = 0;
	conf.r_gain = 255;

	return __sendConf(sockfd, &cmd_header, &conf);
}

int FCC_sendAdvImgPref(int sockfd)
{
	AGTX_ADV_IMG_PREF_S conf;
	AGTX_MSG_HEADER_S cmd_header = { 0 };

	cmd_header.cid = AGTX_CMD_ADV_IMG_PREF;
	cmd_header.len = sizeof(conf);

	conf.backlight_compensation = 1;
	conf.icr_mode = AGTX_ICR_MODE_OFF;
	conf.image_mode = AGTX_IMAGE_MODE_AUTO;
	conf.night_mode = AGTX_NIGHT_MODE_OFF;

	return __sendConf(sockfd, &cmd_header, &conf);
}

int FCC_sendDipCalConf(int sockfd)
{
	AGTX_DIP_CAL_CONF_S conf;
	AGTX_MSG_HEADER_S cmd_header = { 0 };

	cmd_header.cid = AGTX_CMD_DIP_CAL;
	cmd_header.len = sizeof(conf);

	conf.cal[0].cal_en = 1;
	conf.cal[0].dbc_en = 0;
	conf.cal[0].dcc_en = 1;
	conf.cal[0].lsc_en = 0;
	conf.cal[1].cal_en = 1;
	conf.cal[1].dbc_en = 0;
	conf.cal[1].dcc_en = 1;
	conf.cal[1].lsc_en = 0;
	conf.video_dev_idx = 0;

	return __sendConf(sockfd, &cmd_header, &conf);
}

int FCC_sendDipDbcConf(int sockfd)
{
	AGTX_DIP_DBC_CONF_S conf;
	AGTX_MSG_HEADER_S cmd_header = { 0 };

	cmd_header.cid = AGTX_CMD_DIP_DBC;
	cmd_header.len = sizeof(conf);

	conf.dbc[0].mode = 0;
	conf.dbc[0].dbc_level = 123;
	conf.dbc[1].mode = 2;
	conf.dbc[1].dbc_level = 555;
	conf.video_dev_idx = 0;

	return __sendConf(sockfd, &cmd_header, &conf);
}

int FCC_sendDipDccConf(int sockfd)
{
	AGTX_DIP_DCC_CONF_S conf;
	AGTX_MSG_HEADER_S cmd_header = { 0 };

	cmd_header.cid = AGTX_CMD_DIP_DCC;
	cmd_header.len = sizeof(conf);

	conf.dcc[0].gain[0] = 0;
	conf.dcc[0].gain[1] = 1;
	conf.dcc[0].gain[2] = 2;
	conf.dcc[0].gain[3] = 3;
	conf.dcc[0].offset[0] = 4;
	conf.dcc[0].offset[1] = 5;
	conf.dcc[0].offset[2] = 6;
	conf.dcc[0].offset[3] = 7;

	conf.dcc[1].gain[0] = 8;
	conf.dcc[1].gain[1] = 9;
	conf.dcc[1].gain[2] = 10;
	conf.dcc[1].gain[3] = 11;
	conf.dcc[1].offset[0] = 12;
	conf.dcc[1].offset[1] = 13;
	conf.dcc[1].offset[2] = 14;
	conf.dcc[1].offset[3] = 15;

	conf.video_dev_idx = 0;

	return __sendConf(sockfd, &cmd_header, &conf);
}

int FCC_sendDipLscConf(int sockfd)
{
	AGTX_DIP_LSC_CONF_S conf;
	AGTX_MSG_HEADER_S cmd_header = { 0 };

	cmd_header.cid = AGTX_CMD_DIP_LSC;
	cmd_header.len = sizeof(conf);

	conf.lsc[0].origin = 1;
	conf.lsc[0].x_trend = 2;
	conf.lsc[0].y_trend = 3;
	conf.lsc[0].x_curvature = 4;
	conf.lsc[0].y_curvature = 5;
	conf.lsc[0].tilt = 6;

	conf.lsc[1].origin = 7;
	conf.lsc[1].x_trend = 8;
	conf.lsc[1].y_trend = 9;
	conf.lsc[1].x_curvature = 10;
	conf.lsc[1].y_curvature = 11;
	conf.lsc[1].tilt = 12;

	conf.video_dev_idx = 0;

	return __sendConf(sockfd, &cmd_header, &conf);
}

int FCC_sendDipRoiConf(int sockfd)
{
	AGTX_DIP_ROI_CONF_S conf;
	AGTX_MSG_HEADER_S cmd_header = { 0 };

	cmd_header.cid = AGTX_CMD_DIP_ROI;
	cmd_header.len = sizeof(conf);

	conf.roi[0].luma_roi_sx = 1;
	conf.roi[0].luma_roi_sy = 2;
	conf.roi[0].luma_roi_ex = 3;
	conf.roi[0].luma_roi_ey = 4;
	conf.roi[0].awb_roi_sx = 5;
	conf.roi[0].awb_roi_sy = 6;
	conf.roi[0].awb_roi_ex = 7;
	conf.roi[0].awb_roi_ey = 8;

	conf.roi[1].luma_roi_sx = 9;
	conf.roi[1].luma_roi_sy = 10;
	conf.roi[1].luma_roi_ex = 11;
	conf.roi[1].luma_roi_ey = 12;
	conf.roi[1].awb_roi_sx = 13;
	conf.roi[1].awb_roi_sy = 14;
	conf.roi[1].awb_roi_ex = 15;
	conf.roi[1].awb_roi_ey = 16;

	conf.video_dev_idx = 0;

	return __sendConf(sockfd, &cmd_header, &conf);
}

int FCC_sendDipCtlConf(int sockfd)
{
	AGTX_DIP_CTRL_CONF_S conf;
	AGTX_MSG_HEADER_S cmd_header = { 0 };

	cmd_header.cid = AGTX_CMD_DIP_CTRL;
	cmd_header.len = sizeof(conf);

	conf.is_dip_en = 2;
	conf.is_ae_en = 3;
	conf.is_awb_en = 4;
	conf.is_csm_en = 5;
	conf.is_pta_en = 6;
	conf.is_shp_en = 7;
	conf.is_nr_en = 8;
	conf.is_te_en = 9;
	conf.is_gamma_en = 10;
	conf.is_dms_en = 11;
	conf.is_me_en = 12;
	conf.is_dpc_en = 13;
	conf.video_dev_idx = 14;

	return __sendConf(sockfd, &cmd_header, &conf);
}

int FCC_sendDipAeConf(int sockfd)
{
	AGTX_DIP_AE_CONF_S conf;
	AGTX_MSG_HEADER_S cmd_header = { 0 };

	cmd_header.cid = AGTX_CMD_DIP_AE;
	cmd_header.len = sizeof(conf);

	conf.anti_flicker.enable = 1;
	conf.anti_flicker.frequency = 60;
	conf.anti_flicker.luma_delta = 4000;
	conf.exp_strategy = AGTX_EXP_STRATE_NORMAL;
	conf.fps_mode = AGTX_FPS_MO_FIXED;

	conf.manual.enabled = 1;
	conf.manual.flag = 18;
	conf.manual.exp_value = 3200;
	conf.manual.inttime = 100;
	conf.manual.isp_gain = 32;
	conf.manual.sensor_gain = 32;
	conf.manual.sys_gain = 32;

	conf.white_delay_frame = 0;
	conf.black_delay_frame = 7;
	conf.black_speed_bias = 128;
	conf.brightness = 7000;
	conf.exp_strength = 128;
	conf.frame_rate = 30;
	conf.gain_thr_down = 289;
	conf.gain_thr_up = 255;
	conf.interval = 0;
	conf.max_isp_gain = 16383;
	conf.max_sensor_gain = 1024;
	conf.max_sys_gain = 2048;
	conf.min_isp_gain = 48;
	conf.min_sensor_gain = 32;
	conf.min_sys_gain = 48;
	conf.roi_awb_weight = 1;
	conf.roi_luma_weight = 1;
	conf.slow_frame_rate = 15;
	conf.speed = 126;
	conf.tolerance = 2000;
	conf.video_dev_idx = 0;

	return __sendConf(sockfd, &cmd_header, &conf);
}

int FCC_sendDipAwbConf(int sockfd)
{
	AGTX_DIP_AWB_CONF_S conf;
	AGTX_MSG_HEADER_S cmd_header = { 0 };

	cmd_header.cid = AGTX_CMD_DIP_AWB;
	cmd_header.len = sizeof(conf);

	conf.delta_table_list[0].gain[0] = 0;
	conf.delta_table_list[0].gain[1] = 1;
	conf.delta_table_list[0].gain[2] = 2;
	conf.delta_table_list[0].gain[3] = 3;
	conf.delta_table_list[1].gain[0] = 10;
	conf.delta_table_list[1].gain[1] = 11;
	conf.delta_table_list[1].gain[2] = 12;
	conf.delta_table_list[1].gain[3] = 13;
	conf.delta_table_list[2].gain[0] = 20;
	conf.delta_table_list[2].gain[1] = 21;
	conf.delta_table_list[2].gain[2] = 22;
	conf.delta_table_list[2].gain[3] = 23;
	conf.delta_table_list[3].gain[0] = 30;
	conf.delta_table_list[3].gain[1] = 31;
	conf.delta_table_list[3].gain[2] = 32;
	conf.delta_table_list[3].gain[3] = 33;

	int default_gain[MAX_AGTX_DIP_AWB_COLOR_TEMP_S_GAIN_SIZE];
	for (int i = 0; i < MAX_AGTX_DIP_AWB_COLOR_TEMP_S_GAIN_SIZE; i++) {
		default_gain[i] = i + 10;
	}
	int default_maxtrix[MAX_AGTX_DIP_AWB_COLOR_TEMP_S_MAXTRIX_SIZE];
	for (int i = 0; i < MAX_AGTX_DIP_AWB_COLOR_TEMP_S_MAXTRIX_SIZE; i++) {
		default_maxtrix[i] = i + 20;
	}

	conf.k_table_list[0].k = 0;
	conf.k_table_list[1].k = 1;
	conf.k_table_list[2].k = 2;
	conf.k_table_list[3].k = 3;
	for (int i = 0; i < MAX_AGTX_DIP_AWB_COLOR_TEMP_S_GAIN_SIZE; i++) {
		memcpy(&conf.k_table_list[i].gain, &default_gain, sizeof(default_gain));
		memcpy(&conf.k_table_list[i].maxtrix, &default_maxtrix, sizeof(default_maxtrix));
	}

	conf.r_extra_gain = 255;
	conf.g_extra_gain = 255;
	conf.b_extra_gain = 0;
	conf.color_tolerance = 128;
	conf.high_k = 6500;
	conf.low_k = 2700;
	conf.max_lum_gain = 32;
	conf.over_exp_th = 1100;
	conf.speed = 128;
	conf.wht_density = 123;
	conf.gwd_weight = 1;
	conf.wht_weight = 1;
	conf.video_dev_idx = 0;

	return __sendConf(sockfd, &cmd_header, &conf);
}

int FCC_sendDipCsmConf(int sockfd)
{
	AGTX_DIP_CSM_CONF_S conf;
	AGTX_MSG_HEADER_S cmd_header = { 0 };

	cmd_header.cid = AGTX_CMD_DIP_CSM;
	cmd_header.len = sizeof(conf);

	for (int i = 0; i < MAX_AGTX_DIP_CSM_CONF_S_AUTO_SAT_TABLE_SIZE; i++) {
		conf.auto_sat_table[i] = i;
	}
	conf.bw_en = 1;
	conf.hue = 0;
	conf.mode = 2;
	conf.manual_sat = 128;

	conf.video_dev_idx = 0;

	return __sendConf(sockfd, &cmd_header, &conf);
}

int FCC_sendDipPtaConf(int sockfd)
{
	AGTX_DIP_PTA_CONF_S conf;
	AGTX_MSG_HEADER_S cmd_header = { 0 };

	cmd_header.cid = AGTX_CMD_DIP_PTA;
	cmd_header.len = sizeof(conf);

	conf.mode = 1;
	conf.brightness = 0;
	conf.contrast = 255;
	conf.break_point = 30;
	for (int i = 0; i < MAX_AGTX_DIP_PTA_CONF_S_CURVE_SIZE; i++) {
		conf.curve[i] = i;
	}

	conf.video_dev_idx = 0;

	return __sendConf(sockfd, &cmd_header, &conf);
}

int FCC_sendDipShpConf(int sockfd)
{
	AGTX_DIP_SHP_CONF_S conf;
	AGTX_MSG_HEADER_S cmd_header = { 0 };

	cmd_header.cid = AGTX_CMD_DIP_SHP;
	cmd_header.len = sizeof(conf);

	conf.mode = 2;
	conf.manual_shp = 255;
	for (int i = 0; i < MAX_AGTX_DIP_SHP_CONF_S_AUTO_SHP_TABLE_SIZE; i++) {
		conf.auto_shp_table[i] = i;
	}

	conf.video_dev_idx = 0;

	return __sendConf(sockfd, &cmd_header, &conf);
}

int FCC_sendDipNrConf(int sockfd)
{
	AGTX_DIP_NR_CONF_S conf;
	AGTX_MSG_HEADER_S cmd_header = { 0 };

	cmd_header.cid = AGTX_CMD_DIP_NR;
	cmd_header.len = sizeof(conf);

	conf.mode = 2;
	for (int i = 0; i < MAX_AGTX_DIP_NR_CONF_S_AUTO_C_LEVEL_3D_LIST_SIZE; i++) {
		conf.auto_y_level_3d_list[i] = i + 10;
		conf.auto_c_level_3d_list[i] = i + 20;
		conf.auto_y_level_2d_list[i] = i + 30;
		conf.auto_c_level_2d_list[i] = i + 40;
	}

	conf.manual_y_level_3d = 1;
	conf.manual_c_level_3d = 2;
	conf.manual_y_level_2d = 3;
	conf.manual_c_level_2d = 4;
	conf.motion_comp = 5;
	conf.trail_suppress = 6;
	conf.ghost_remove = 7;
	conf.ma_y_strength = 8;
	conf.mc_y_strength = 9;
	conf.ma_c_strength = 10;
	conf.ratio_3d = 11;
	conf.mc_y_level_offset = 12;
	conf.me_frame_fallback_en = 13;

	conf.video_dev_idx = 0;

	return __sendConf(sockfd, &cmd_header, &conf);
}

int FCC_sendDipTeConf(int sockfd)
{
	AGTX_DIP_TE_CONF_S conf;
	AGTX_MSG_HEADER_S cmd_header = { 0 };

	cmd_header.cid = AGTX_CMD_DIP_TE;
	cmd_header.len = sizeof(conf);

	conf.mode = 1;
	for (int i = 0; i < MAX_AGTX_DIP_TE_CONF_S_NORMAL_CTL_SIZE; i++) {
		conf.normal_ctl[i] = i;
	}

	conf.wdr_ctl.brightness = 3000;
	conf.wdr_ctl.strength = 2000;
	conf.wdr_ctl.saliency = 1000;
	conf.wdr_ctl.iso_weight = 1;
	conf.wdr_ctl.dark_enhance = 255;
	conf.wdr_ctl.iso_max = 3200;

	for (int i = 0; i < MAX_AGTX_DIP_TE_WDR_S_NOISE_CSTR_SIZE; i++) {
		conf.wdr_ctl.noise_cstr[i] = i + 10;
	}

	conf.video_dev_idx = 0;

	return __sendConf(sockfd, &cmd_header, &conf);
}

int FCC_sendDipGammaConf(int sockfd)
{
	AGTX_DIP_GAMMA_CONF_S conf;
	AGTX_MSG_HEADER_S cmd_header = { 0 };

	cmd_header.cid = AGTX_CMD_DIP_GAMMA;
	cmd_header.len = sizeof(conf);

	conf.gamma = 1;

	conf.video_dev_idx = 0;

	return __sendConf(sockfd, &cmd_header, &conf);
}
