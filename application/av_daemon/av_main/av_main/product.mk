ifndef CONFIG_PROD
PRODUCT_DEFINE = MT801_1
endif

ifeq ($(CONFIG_PROD), MT800_1)
        # do some setting for MT800_1
        PRODUCT_DEFINE = -D$(CONFIG_PROD)
else ifeq ($(CONFIG_PROD), GT804_1)
        # do some setting for GT804_1
        PRODUCT_DEFINE = -D$(CONFIG_PROD)
else ifeq ($(CONFIG_PROD), GT804_2)
        # do some setting for GT804_2
        PRODUCT_DEFINE = -D$(CONFIG_PROD)
else ifeq ($(CONFIG_PROD), MT801_1)
        # do some setting for MT801_1
        PRODUCT_DEFINE = -D$(CONFIG_PROD)
else ifeq ($(CONFIG_PROD), MS701_1)
        # do some setting for MT701_1
        PRODUCT_DEFINE = -DMT800_1
else
        # do some setting for others
endif
