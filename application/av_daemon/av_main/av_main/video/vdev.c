#ifdef __cplusplus
extern "C" {
#endif /**< __cplusplus */

#include "mpi_dev.h"
#include "mpi_dip_alg.h"
#include "sensor.h"
#include "osd.h"

#include "mtk_common.h"
#include "config_api.h"

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

extern CUSTOM_SNS_CTRL_S custom_sns(SNS0_ID);
#if defined(SNS1)
extern CUSTOM_SNS_CTRL_S custom_sns(SNS1_ID);
#endif

static CUSTOM_SNS_CTRL_S *p_custom_sns[] = {
	&custom_sns(SNS0_ID),
#if defined(SNS1)
	&custom_sns(SNS1_ID),
#endif
};

static void toMpiStitchAttr(MPI_STITCH_ATTR_S *stitch_attr, const CFG_STITCH_S *attr)
{
	INT32 i = 0;
	INT32 j = 0;

	stitch_attr->enable = attr->enable;
	stitch_attr->dft_dist = attr->dft_dist;
	stitch_attr->table_num = attr->table_num;

	for (i = 0; i < STITCH_SENSOR_NUM; i++) {
		stitch_attr->center[i].x = attr->center[i].x;
		stitch_attr->center[i].y = attr->center[i].y;
	}

	for (i = 0; i < STITCH_TABLE_NUM; i++) {
		stitch_attr->table[i].dist = attr->table[i].dist;
		stitch_attr->table[i].ver_disp = attr->table[i].ver_disp;
		stitch_attr->table[i].straighten = attr->table[i].straighten;
		stitch_attr->table[i].src_zoom = attr->table[i].src_zoom;
		for (j = 0; j < STITCH_SENSOR_NUM; j++) {
			stitch_attr->table[i].theta[j] = attr->table[i].theta[j];
			stitch_attr->table[i].radius[j] = attr->table[i].radius[j];
			stitch_attr->table[i].curvature[j] = attr->table[i].curvature[j];
			stitch_attr->table[i].fov_ratio[j] = attr->table[i].fov_ratio[j];
			stitch_attr->table[i].ver_scale[j] = attr->table[i].ver_scale[j];
			stitch_attr->table[i].ver_shift[j] = attr->table[i].ver_shift[j];
		}
	}
}

static void toMpiLdcAttr(MPI_LDC_ATTR_S *ldc_attr, const AGTX_LDC_CONF_S *ldc_cfg)
{
	ldc_attr->enable = ldc_cfg->enable;
	ldc_attr->view_type = ldc_cfg->view_type;
	ldc_attr->center_offset.x = ldc_cfg->center_x_offset;
	ldc_attr->center_offset.y = ldc_cfg->center_y_offset;
	ldc_attr->ratio = ldc_cfg->ratio;
}

static void toMpiPanoramaAttr(MPI_PANORAMA_ATTR_S *pano_attr, const AGTX_PANORAMA_CONF_S *pano_cfg)
{
	pano_attr->enable = pano_cfg->enable;
	pano_attr->center_offset.x = pano_cfg->center_offset_x;
	pano_attr->center_offset.y = pano_cfg->center_offset_y;
	pano_attr->ldc_ratio = pano_cfg->ldc_ratio;
	pano_attr->radius = pano_cfg->radius;
	pano_attr->curvature = pano_cfg->curvature;
	pano_attr->straighten = pano_cfg->straighten;
}

static void toMpiPanningAttr(MPI_PANNING_ATTR_S *pann_attr, const AGTX_PANNING_CONF_S *pann_cfg)
{
	pann_attr->enable = pann_cfg->enable;
	pann_attr->center_offset.x = pann_cfg->center_offset_x;
	pann_attr->center_offset.y = pann_cfg->center_offset_y;
	pann_attr->ldc_ratio = pann_cfg->ldc_ratio;
	pann_attr->radius = pann_cfg->radius;
	pann_attr->hor_strength = pann_cfg->hor_strength;
	pann_attr->ver_strength = pann_cfg->ver_strength;
}

static void toMpiSurroundAttr(MPI_SURROUND_ATTR_S *surr_attr, const AGTX_SURROUND_CONF_S *surr_cfg)
{
	surr_attr->enable = surr_cfg->enable;
	surr_attr->center_offset.x = surr_cfg->center_offset_x;
	surr_attr->center_offset.y = surr_cfg->center_offset_y;
	surr_attr->ldc_ratio = surr_cfg->ldc_ratio;
	surr_attr->min_radius = surr_cfg->min_radius;
	surr_attr->max_radius = surr_cfg->max_radius;
	surr_attr->rotate = surr_cfg->rotate;
}

static inline void toMpiLayoutWindow(const MPI_RECT_S *pos, MPI_SIZE_S *chn_res, MPI_RECT_S *lyt_res)
{
#define MIN(a, b) ((a) < (b) ? (a) : (b))
	lyt_res->x = (((pos->x * (chn_res->width - 1) + 512) >> 10) + 8) & 0xFFFFFFF0;
	lyt_res->y = (((pos->y * (chn_res->height - 1) + 512) >> 10) + 16) & 0xFFFFFFE0;
	lyt_res->width = MIN((((pos->width * (chn_res->width - 1) + 512) >> 10) + 9) & 0xFFFFFFF0, chn_res->width);

	/* Handle boundary condition */
	if (pos->y + pos->height == 1024) {
		lyt_res->height = chn_res->height - lyt_res->y;
	} else {
		lyt_res->height = (((pos->height * (chn_res->height - 1) + 512) >> 10) + 16) & 0xFFFFFFE0;
	}
}

INT32 VIDEO_DEV_createVideoDev(CFG_VIN_S *conf)
{
	INT32 ret = MPI_FAILURE;
	UINT32 i = 0;
	MPI_DEV dev_idx = MPI_VIDEO_DEV(conf->dev_idx);
	MPI_PATH path_idx = MPI_INPUT_PATH(conf->dev_idx, 0);

	MPI_DEV_ATTR_S dev_attr;
	MPI_PATH_ATTR_S path_attr;

	CFG_PATH_S *path;

	/* Create video device */
	dev_attr.hdr_mode = conf->hdr_mode;
	dev_attr.stitch_en = conf->stitch_en;
	dev_attr.eis_en = conf->eis_en;
	dev_attr.bayer = (conf->bayer == CFG_BAYER_TYPE_G0) ?
	                         MPI_BAYER_PHASE_G0 :
	                         (conf->bayer == CFG_BAYER_TYPE_R) ?
	                         MPI_BAYER_PHASE_R :
	                         (conf->bayer == CFG_BAYER_TYPE_B) ?
	                         MPI_BAYER_PHASE_B :
	                         (conf->bayer == CFG_BAYER_TYPE_G1) ? MPI_BAYER_PHASE_G1 : MPI_BAYER_PHASE_R;
	dev_attr.fps = conf->fps;
	dev_attr.path.bmp = 0x0;

	for (i = 0; i < MPI_MAX_INPUT_PATH_NUM; ++i) {
		path = &conf->cfg_path[i];

		if (path->enable) {
			dev_attr.path.bmp |= (0x1 << i);
		}
	}

	ret = MPI_DEV_createDev(dev_idx, &dev_attr);
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Create video device %d failed.\n", dev_idx.dev);
		return MPI_FAILURE;
	}

	/* Configure input path */
	for (i = 0; i < MPI_MAX_INPUT_PATH_NUM; ++i) {
		path = &conf->cfg_path[i];

		if (path->enable) {
			path_idx.path = path->path_idx;

			path_attr.sensor_idx = path->sensor_idx;
			path_attr.res.width = path->width;
			path_attr.res.height = path->height;

			ret = MPI_DEV_addPath(path_idx, &path_attr);
			if (ret != MPI_SUCCESS) {
				SYS_TRACE("Set input path %d failed.\n", path_idx.path);
				return MPI_FAILURE;
			}

			/* Register sensor callback function */
			p_custom_sns[path->sensor_idx]->reg_callback(path_idx);

			/* Register AE, AWB lib */
			MPI_regAeDftLib(path_idx);
			MPI_regAwbDftLib(path_idx);

			/* Get parameter from sensor driver */
			MPI_updateSnsParam(path_idx);
		}
	}

	return MPI_SUCCESS;
}

INT32 VIDEO_DEV_startVideoDev(CFG_VIN_S *conf)
{
	INT32 ret = MPI_FAILURE;
	MPI_DEV dev_idx = MPI_VIDEO_DEV(conf->dev_idx);

	/* Start video device */
	ret = MPI_DEV_startDev(dev_idx);
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Start video device %d failed.\n", dev_idx.dev);
		//	SAMPLE_handleDevStartFail(dev_idx);

		return MPI_FAILURE;
	}

	SYS_TRACE("Start video device %d succeeded!\n", dev_idx.dev);

	return MPI_SUCCESS;
}

INT32 VIDEO_DEV_setVideoChn(MPI_CHN idx, CFG_VCHN_S *conf)
{
	INT32 ret = MPI_FAILURE;
	MPI_CHN_ATTR_S chn_attr = {{ 0 }};
	MPI_WIN_ATTR_S window_attr = {{ 0 }};
	MPI_WIN win_idx = MPI_VIDEO_WIN(idx.dev, idx.chn, 0);

	ret = MPI_DEV_getChnAttr(idx, &chn_attr);
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Get video channel %d attribute failed.\n", idx.chn);
		return MPI_FAILURE;
	}

	chn_attr.fps = conf->fps;

	ret = MPI_DEV_setChnAttr(idx, &chn_attr);
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Set video channel %d attribute failed.\n", idx.chn);
		return MPI_FAILURE;
	}

	if (conf->layout_en) {
		for (int k = 0; k < conf->layout.window_num; k++) {
			win_idx.win = conf->layout.window_array[k].window_idx; //map window index
			ret = MPI_DEV_getWindowAttr(win_idx, &window_attr);
			if (ret != MPI_SUCCESS) {
				SYS_TRACE("Get video window %d attribute failed.\n", win_idx.win);
				return MPI_FAILURE;
			}

			window_attr.fps = conf->layout.window_array[k].update_fps;
			window_attr.mirr_en = conf->mirror;
			window_attr.flip_en = conf->flip;
			window_attr.rotate = conf->rotate;

			ret = MPI_DEV_setWindowAttr(win_idx, &window_attr);
			if (ret != MPI_SUCCESS) {
				SYS_TRACE("Set video window %d attribute failed.\n", win_idx.win);
				return MPI_FAILURE;
			}
		}
	} else {
		ret = MPI_DEV_getWindowAttr(win_idx, &window_attr);
		if (ret != MPI_SUCCESS) {
			SYS_TRACE("Get video window %d attribute failed.\n", win_idx.win);
			return MPI_FAILURE;
		}

		window_attr.fps = conf->fps;
		window_attr.mirr_en = conf->mirror;
		window_attr.flip_en = conf->flip;
		window_attr.rotate = conf->rotate;

		ret = MPI_DEV_setWindowAttr(win_idx, &window_attr);
		if (ret != MPI_SUCCESS) {
			SYS_TRACE("Set video window %d attribute failed.\n", win_idx.win);
			return MPI_FAILURE;
		}
	}

	return MPI_SUCCESS;
}

INT32 VIDEO_DEV_startVideoChn(MPI_DEV dev_idx, CFG_VCHN_S *all_conf)
{
	INT32 ret = MPI_FAILURE;
	INT32 output_num = 0;
	CFG_VCHN_S *conf;
	MPI_STITCH_ATTR_S stitch_attr = { 0 };
	MPI_LDC_ATTR_S ldc_attr = { 0 };
	MPI_PANORAMA_ATTR_S pano_attr = { 0 };
	MPI_PANNING_ATTR_S pann_attr = { 0 };
	MPI_SURROUND_ATTR_S surr_attr = { 0 };
	int idx;
	MPI_CHN chn_idx = MPI_VIDEO_CHN(dev_idx.dev, 0);
	MPI_WIN win_idx = MPI_VIDEO_WIN(dev_idx.dev, 0, 0);
	MPI_ECHN e_chn_idx = MPI_ENC_CHN(0);

	for (int j = 0; j < MAX_VIDEO_CHNNEL; j++) {
		if (all_conf[j].enable) {
			output_num++;
		}
	}

	initOsdHandle();
	loadOsdFont();

	for (int j = 0; j < MAX_VIDEO_CHNNEL; j++) {
		conf = &all_conf[j];
		if (conf->enable) {

			MPI_CHN_ATTR_S chn_attr = { { 0 } };
			MPI_CHN_LAYOUT_S chn_layout = { 0 };
			MPI_WIN_ATTR_S window_attr[MAX_VIDEO_WINDOW];
			MPI_WIN_VIEW_TYPE_E view_type = MPI_WIN_VIEW_TYPE_NUM;
			AGTX_LAYOUT_PARAM_S *lyt_cfg = &conf->lyt_cfg;

			chn_idx.chn = conf->chn_idx;
			e_chn_idx.chn = conf->chn_idx;
			win_idx.chn = conf->chn_idx;
			chn_attr.res.width = conf->width;
			chn_attr.res.height = conf->height;
			chn_attr.fps = conf->fps;

			ret = MPI_DEV_addChn(chn_idx, &chn_attr);
			if (ret != MPI_SUCCESS) {
				SYS_TRACE("Add video channel %d failed.\n", chn_idx.chn);
				return MPI_FAILURE;
			}

			/* Transfer layout and window data from config to mpi */
			if (conf->layout_en == 1) {
				 if (j == conf->layout.video_strm_idx) {
					chn_layout.window_num = conf->layout.window_num;
					lyt_cfg->window_num = chn_layout.window_num;

					for (int k = 0; k < chn_layout.window_num; k++) {
						/* Change pos_* from [0 ~ 1024] to layout_window [0 ~ res_i] */
						idx = conf->layout.window_array[k].window_idx; // map window index
						win_idx.win = idx;
						chn_layout.win_id[k] = win_idx;

						MPI_RECT_S pos = { 0 };
						pos.x = conf->layout.window_array[k].pos_x;
						pos.y = conf->layout.window_array[k].pos_y;
						pos.width = conf->layout.window_array[k].pos_width;
						pos.height = conf->layout.window_array[k].pos_height;

						toMpiLayoutWindow(&pos, &chn_attr.res, &chn_layout.window[k]);
						lyt_cfg->window_array[idx].pos_x = chn_layout.window[k].x;
						lyt_cfg->window_array[idx].pos_y = chn_layout.window[k].y;
						lyt_cfg->window_array[idx].pos_width = chn_layout.window[k].width;
						lyt_cfg->window_array[idx].pos_height = chn_layout.window[k].height;
						lyt_cfg->window_array[idx].update_fps = conf->layout.window_array[k].update_fps;
#if 1
						fprintf(stderr, "######### %s: chn_layout.window[%d]\n", __func__, k);
						fprintf(stderr, "### idx = %d\n", idx);
						fprintf(stderr, "### x = %d\n", chn_layout.window[k].x);
						fprintf(stderr, "### y = %d\n", chn_layout.window[k].y);
						fprintf(stderr, "### width = %d\n", chn_layout.window[k].width);
						fprintf(stderr, "### height = %d\n", chn_layout.window[k].height);
						fprintf(stderr, "\n");
#endif
						window_attr[k].path.bmp = conf->layout.window_array[k].path_bmp;
						window_attr[k].fps = conf->layout.window_array[k].update_fps;
						window_attr[k].prio = conf->layout.window_array[k].priority;
						window_attr[k].src_id.value = conf->layout.window_array[k].parent;
						window_attr[k].const_qual = conf->layout.window_array[k].const_qual;
						window_attr[k].dyn_adj = conf->layout.window_array[k].dyn_adj;
						window_attr[k].rotate = conf->rotate;
						window_attr[k].mirr_en = conf->mirror;
						window_attr[k].flip_en = conf->flip;

						switch (conf->layout.window_array[k].view_type) {
						case AGTX_WINDOW_VIEW_TYPE_NORMAL:
							view_type = MPI_WIN_VIEW_TYPE_NORMAL;
							break;
						case AGTX_WINDOW_VIEW_TYPE_LDC:
							view_type = MPI_WIN_VIEW_TYPE_LDC;
							break;
						case AGTX_WINDOW_VIEW_TYPE_STITCH:
							view_type = MPI_WIN_VIEW_TYPE_STITCH;
							break;
						case AGTX_WINDOW_VIEW_TYPE_PANORAMA:
							view_type = MPI_WIN_VIEW_TYPE_PANORAMA;
							break;
						case AGTX_WINDOW_VIEW_TYPE_PANNING:
							view_type = MPI_WIN_VIEW_TYPE_PANNING;
							break;
						case AGTX_WINDOW_VIEW_TYPE_SURROUND:
							view_type = MPI_WIN_VIEW_TYPE_SURROUND;
							break;
						default:
							assert(0 && "Invaild view_type");
							break;
						}
						window_attr[k].view_type = view_type;
						window_attr[k].roi.x = conf->layout.window_array[k].roi_x;
						window_attr[k].roi.y = conf->layout.window_array[k].roi_y;
						window_attr[k].roi.width = conf->layout.window_array[k].roi_width;
						window_attr[k].roi.height = conf->layout.window_array[k].roi_height;
					}
				} else {
					assert(0 && "j != conf->layout.video_strm_idx");
				}
			} else if (conf->layout_en == 0) {
				chn_layout.window_num = 1;
				win_idx.win = 0;
				chn_layout.win_id[0] = win_idx;
				chn_layout.window[0].x = 0;
				chn_layout.window[0].y = 0;
				chn_layout.window[0].width = conf->width;
				chn_layout.window[0].height = conf->height;
				lyt_cfg->window_num = 1;
				lyt_cfg->window_array[0].pos_x = chn_layout.window[0].x;
				lyt_cfg->window_array[0].pos_y = chn_layout.window[0].y;
				lyt_cfg->window_array[0].pos_width = conf->width;
				lyt_cfg->window_array[0].pos_height = conf->height;
				window_attr[0].path.bmp = 1;
				window_attr[0].fps = conf->fps;
				window_attr[0].prio = 0;
				window_attr[0].src_id = MPI_INVALID_VIDEO_WIN;
				window_attr[0].const_qual = 1;
				window_attr[0].dyn_adj = 0;
				window_attr[0].rotate = conf->rotate;
				window_attr[0].mirr_en = conf->mirror;
				window_attr[0].flip_en = conf->flip;
				window_attr[0].view_type = MPI_WIN_VIEW_TYPE_NORMAL;
				window_attr[0].roi.x = 0;
				window_attr[0].roi.y = 0;
				window_attr[0].roi.width = 1024;
				window_attr[0].roi.height = 1024;
			} else {
				assert(0 && "Invaild layout_en");
			}

			ret = MPI_DEV_setChnLayout(chn_idx, &chn_layout);
			if (ret != MPI_SUCCESS) {
				printf("Set video channel layout %d failed.\n", chn_idx.chn);
				return MPI_FAILURE;
			}

			for (int w_idx = 0; w_idx < chn_layout.window_num; ++w_idx) {
				ret = MPI_DEV_setWindowAttr(chn_layout.win_id[w_idx], &window_attr[w_idx]);
				if (ret != MPI_SUCCESS) {
					printf("Set video window %d failed.\n", chn_layout.win_id[w_idx].win);
					return MPI_FAILURE;
				}
			}

			createOsdRegions(e_chn_idx, conf->width, conf->height, &conf->osd, &conf->osd_pm);
			//if (ret != MPI_SUCCESS) {
			//	printf("Start OSD %d failed.\n", chn_idx.chn);
			//	return MPI_FAILURE;
			//}
		}
	}

	createDateTimeThread(output_num);

	/* set gfx attributes */
	win_idx.chn = 0;
	win_idx.win = 0;
	conf = &all_conf[win_idx.chn];

	toMpiStitchAttr(&stitch_attr, &conf->stitch);
	ret = MPI_DEV_setStitchAttr(win_idx, &stitch_attr);
	if (ret != MPI_SUCCESS) {
		printf("Set STITCH attr for channel %d failed.\n", win_idx.chn);
		return MPI_FAILURE;
	}

	toMpiLdcAttr(&ldc_attr, &conf->ldc);
	ret = MPI_DEV_setLdcAttr(win_idx, &ldc_attr);
	if (ret != MPI_SUCCESS) {
		printf("Set LDC attr for channel %d failed.\n", win_idx.chn);
	}

	toMpiPanoramaAttr(&pano_attr, &conf->panorama);
	ret = MPI_DEV_setPanoramaAttr(win_idx, &pano_attr);
	if (ret != MPI_SUCCESS) {
		printf("Set Panorama attr for channel %d failed.\n", win_idx.chn);
	}

	toMpiPanningAttr(&pann_attr, &conf->panning);
	ret = MPI_DEV_setPanningAttr(win_idx, &pann_attr);
	if (ret != MPI_SUCCESS) {
		printf("Set Panning attr for channel %d failed.\n", win_idx.chn);
	}

	toMpiSurroundAttr(&surr_attr, &conf->surround);
	ret = MPI_DEV_setSurroundAttr(win_idx, &surr_attr);
	if (ret != MPI_SUCCESS) {
		printf("Set Surround attr for channel %d failed.\n", win_idx.chn);
	}

	/* start streaming */
	if (output_num == 1) {
		printf("#############################\n");
		printf("####### output_num = 1#######\n");
		printf("#############################\n");
		chn_idx.chn = 0;
		ret = MPI_DEV_startChn(chn_idx);
		if (ret != MPI_SUCCESS) {
			SYS_TRACE("Start video channel %d failed.\n", chn_idx.chn);
			return MPI_FAILURE;
		}
	} else {
		printf("#############################\n");
		printf("####### output_num > 1#######\n");
		printf("#############################\n");
		ret = MPI_DEV_startAllChn(dev_idx);
		if (ret != MPI_SUCCESS) {
			printf("Start all video channels on video device %d failed.\n", dev_idx.dev);
			return MPI_FAILURE;
		}
	}

	SYS_TRACE("Start video channel %d succeeded!\n", chn_idx.chn);

	return MPI_SUCCESS;
}

INT32 VIDEO_DEV_stopVideoDev(CFG_VIN_S *conf)
{
	INT32 ret = MPI_FAILURE;
	MPI_DEV dev_idx = MPI_VIDEO_DEV(conf->dev_idx);

	ret = MPI_DEV_stopDev(dev_idx);
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Stop video device %d failed.\n", dev_idx.dev);
		return MPI_FAILURE;
	}

	SYS_TRACE("Stop video device %d succeeded!\n", dev_idx.dev);
	return MPI_SUCCESS;
}

INT32 VIDEO_DEV_destroyVideoDev(CFG_VIN_S *conf)
{
	INT32 ret = MPI_FAILURE;
	UINT32 i = 0;
	MPI_DEV dev_idx = MPI_VIDEO_DEV(conf->dev_idx);
	MPI_PATH path_idx = MPI_INPUT_PATH(conf->dev_idx, 0);
	CFG_PATH_S *path;

	for (i = 0; i < MPI_MAX_INPUT_PATH_NUM; ++i) {
		path = &conf->cfg_path[i];

		if (path->enable) {
			path_idx.path = path->path_idx;

			/* Deregister AE, AWB lib */
			MPI_deregAeDftLib(path_idx);
			MPI_deregAwbDftLib(path_idx);

			/* Deregister sensor callback function */
			p_custom_sns[path->sensor_idx]->dereg_callback(path_idx);

			MPI_DEV_deletePath(path_idx);
		}
	}

	ret = MPI_DEV_destroyDev(dev_idx);
	if (ret != MPI_SUCCESS) {
		SYS_TRACE("Destroy video device %d failed.\n", dev_idx.dev);
		return MPI_FAILURE;
	}

	SYS_TRACE("Destroy video device %d succeeded!\n", dev_idx.dev);

	return MPI_SUCCESS;
}

INT32 VIDEO_DEV_stopVideoChn(MPI_DEV dev_idx, CFG_VCHN_S *all_conf)
{
	INT32 ret = MPI_FAILURE;
	INT32 output_num = 0;
	INT32 j = 0;
	MPI_CHN chn_idx = MPI_VIDEO_CHN(dev_idx.dev, 0);
	MPI_ECHN e_chn_idx = MPI_ENC_CHN(0);
	CFG_VCHN_S *conf = NULL;

	for (j = 0; j < MAX_VIDEO_CHNNEL; j++) {
		if (all_conf[j].enable) {
			output_num++;
		}
	}

	if (output_num > 1) {
		ret = MPI_DEV_stopAllChn(dev_idx);
		if (ret != MPI_SUCCESS) {
			printf("Stop all video channels on video device %d failed.\n", dev_idx.dev);
			return MPI_FAILURE;
		}
	}

	deleteDateTimeThread();

	for (j = 0; j < MAX_VIDEO_CHNNEL; j++) {
		conf = &all_conf[j];
		if (conf->enable) {
			chn_idx.chn = conf->chn_idx;
			e_chn_idx.chn = conf->chn_idx;
			if (output_num == 1) {
				ret = MPI_DEV_stopChn(chn_idx);
				if (ret != MPI_SUCCESS) {
					printf("Stop video channel %d failed.\n", chn_idx.chn);
					return MPI_FAILURE;
				}
			}

			ret = destroyOsdRegions(e_chn_idx);
			if (ret != MPI_SUCCESS) {
				printf("Stop OSD %d failed.\n", chn_idx.chn);
				return MPI_FAILURE;
			}

			ret = MPI_DEV_deleteChn(chn_idx);
			if (ret != MPI_SUCCESS) {
				SYS_TRACE("Delete video channel %d failed.\n", chn_idx.chn);
				return MPI_FAILURE;
			}
		}
	}

	unloadOsdFont();

	SYS_TRACE("Stop video channel %d succeeded!\n", chn_idx.chn);

	return MPI_SUCCESS;
}

INT32 VIDEO_DEV_setStitchAttr(MPI_DEV dev_idx, CFG_STITCH_S *conf)
{
	INT32 ret = MPI_FAILURE;
	MPI_STITCH_ATTR_S attr = { 0 };
	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx.dev, 0, 0);

	toMpiStitchAttr(&attr, conf);
	ret = MPI_DEV_setStitchAttr(idx, &attr);

	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "Set STITCH attr for device %d channel %d window %d failed.\n", idx.dev, idx.chn, idx.win);
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 VIDEO_DEV_setLdcConf(MPI_DEV dev_idx, AGTX_LDC_CONF_S *ldc_cfg)
{
	INT32 ret = MPI_FAILURE;
	MPI_LDC_ATTR_S ldc_attr = { 0 };
	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx.dev, 0, 0);

	toMpiLdcAttr(&ldc_attr, ldc_cfg);

#ifdef DIP_DBG
	printf("-----[VIDEO_setLdcConf]-----\n");
	printf("enable = %d\n", ldc_attr.enable);
	printf("view_type = %d\n", ldc_attr.view_type);
	printf("center_x_offset = %d\n", ldc_attr.center_offset.x);
	printf("center_y_offset = %d\n", ldc_attr.center_offset.y);
	printf("ratio = %d\n", ldc_attr.ratio);
#endif

	ret = MPI_DEV_setLdcAttr(idx, &ldc_attr);
	if (ret != MPI_SUCCESS) {
		fprintf(stderr, "Set LDC attr for device %d channel %d window %d failed.\n", idx.dev, idx.chn, idx.win);
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 VIDEO_DEV_setPanoramaConf(MPI_DEV dev_idx, AGTX_PANORAMA_CONF_S *pano_cfg)
{
	INT32 ret = 0;
	MPI_PANORAMA_ATTR_S pano_attr = { 0 };
	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx.dev, 0, 0);

	toMpiPanoramaAttr(&pano_attr, pano_cfg);

#ifdef DIP_DBG
	printf("-----[VIDEO_setLdcConf]-----\n");
	printf("enable = %d\n", pano_attr.enable);
	printf("view_type = %d\n", pano_attr.pano_info.view_type);
	printf("center_x_offset = %d\n", pano_attr.center_x_offset);
	printf("center_y_offset = %d\n", pano_attr.center_y_offset);
	printf("radius = %d\n", pano_attr.radius);
	printf("curvature = %d\n", pano_attr.curvature);
	printf("ldc_ratio = %d\n", pano_attr.ldc_ratio);
	printf("straighten = %d\n", pano_attr.straighten);
#endif

	ret = MPI_DEV_setPanoramaAttr(idx, &pano_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 VIDEO_DEV_setPanningConf(MPI_DEV dev_idx, AGTX_PANNING_CONF_S *pann_cfg)
{
	INT32 ret = 0;
	MPI_PANNING_ATTR_S pann_attr = { 0 };
	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx.dev, 0, 0);

	toMpiPanningAttr(&pann_attr, pann_cfg);

#ifdef DIP_DBG
	printf("-----[VIDEO_setLdcConf]-----\n");
	printf("enable = %d\n", pann_attr.enable);
	printf("center_x_offset = %d\n", pann_attr.center_x_offset);
	printf("center_y_offset = %d\n", pann_attr.center_y_offset);
	printf("radius = %d\n", pann_attr.radius);
	printf("hor_strength = %d\n", pann_attr.hor_strength);
	printf("ver_strength = %d\n", pann_attr.ver_strength);
	printf("ldc_ratio = %d\n", pann_attr.ldc_ratio);
#endif

	ret = MPI_DEV_setPanningAttr(idx, &pann_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}

INT32 VIDEO_DEV_setSurroundConf(MPI_DEV dev_idx, AGTX_SURROUND_CONF_S *surr_cfg)
{
	INT32 ret = 0;
	MPI_SURROUND_ATTR_S surr_attr = { 0 };
	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx.dev, 0, 0);

	toMpiSurroundAttr(&surr_attr, surr_cfg);

#ifdef DIP_DBG
	printf("-----[VIDEO_setLdcConf]-----\n");
	printf("enable = %d\n", surr_attr.enable);
	printf("view_type = %d\n", surr_attr.pano_info.view_type);
	printf("center_x_offset = %d\n", surr_attr.center_x_offset);
	printf("center_y_offset = %d\n", surr_attr.center_y_offset);
	printf("radius = %d\n", surr_attr.radius);
	printf("curvature = %d\n", surr_attr.curvature);
	printf("ldc_ratio = %d\n", surr_attr.ldc_ratio);
	printf("straighten = %d\n", surr_attr.straighten);
#endif

	ret = MPI_DEV_setSurroundAttr(idx, &surr_attr);
	if (ret != MPI_SUCCESS) {
		return MPI_FAILURE;
	}

	return MPI_SUCCESS;
}


#ifdef __cplusplus
}
#endif /**< __cplusplus */
