#ifndef OSD_H_
#define OSD_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <pthread.h>

#include "mpi_sys.h"
#include "mpi_dev.h"
#include "mpi_enc.h"
#include "mpi_osd.h"
#include "mpi_iva.h"
#include "config_api.h"

//#define OSD_DATETIME_CHINESE

#define MAX_FONT_FORMAT 4 /**< Max font format. */
#define MAX_ALARM_FORMAT 2 /**< Max font format. */
#define MAX_TEXT_LEN 32 /**< Max charater of title. */
#define LOGO_OSD_SIZE_WIDTH 544 /**< Default width of logo OSD region. */
#define LOGO_OSD_SIZE_HEIGHT 128 /**< Default height of logo OSD region. */
#define TIME_OSD_SIZE_WIDTH 416 /**< Default width of time OSD region. */
#define TIME_OSD_SIZE_HEIGHT 48 /**< Default height of time OSD region. */
#define NUMBER_OSD_SIZE_WIDTH 160 /**< Default width of number OSD region. */
#define NUMBER_OSD_SIZE_HEIGHT 48 /**< Default height of number OSD region. */
//#define OSD_FONT_PATH "/system/mpp/font/compact_chinese_noalpha.ayuv"

#define VIDEO_OSD_MAX_PM_HANDLE MPI_OSD_MAX_BIND_CHANNEL /**< Max Privacy Mask handle per channel. */
#define VIDEO_OSD_PM_HANDLE_OFFSET MPI_OSD_MAX_BIND_CHANNEL /**< Privacy Mask handle offset per channel. */
#define VIDEO_OSD_MAX_HANDLE_CHANNEL                                                                                   \
	(MPI_OSD_MAX_BIND_CHANNEL + VIDEO_OSD_MAX_PM_HANDLE) /**< Default Max handle per channel. */

#define OSD_AYUV_ALPHA_BS 13 /**< OSD AYUV Color alpha value bit shift. */
#define OSD_PM_CR_FORMAT MPI_OSD_COLOR_FORMAT_AYUV_3544 /**< Default Pivacy Mask Color format */
#define VIDEO_OSD_PM_AREA_DIV 1 /**< Maximum area for each OSD Privacy Mask */
#define VIDEO_OSD_PM_WIDTH_DIV 2
#define VIDEO_OSD_PM_HEIGHT_DIV 4

typedef enum {
	CR_BLACK = 0,
	CR_RED,
	CR_GREEN,
	CR_BLUE,
	CR_YELLOW,
	CR_PURPLE,
	CR_ORANGE,
	CR_WHITE,
	CR_MAX,
} VIDEO_OSD_CR_E;

typedef struct {
	UINT32 image_offset;
	UINT32 image_size;
	UINT32 image_width;
	UINT32 image_height;
} ASCII_INDEX;

typedef struct {
	UINT32 index_offset;
	UINT32 index_size;
	UINT32 ascii_index;
	UINT32 ascii_width;
	UINT32 cht_index;
	UINT32 cht_width;
	UINT32 other_index;
	UINT32 other_width;
	UINT32 data_offset;
	UINT32 data_size;
} AyuvInfo_S;

typedef enum {
	VIDEO_OSD_ERR_PARAM = -2,
	VIDEO_OSD_FAILURE = -1,
	VIDEO_OSD_SUCCESS = 0,
	VIDEO_OSD_RESTREAM_REQUEST,
} VIDEO_OSD_CHECKPARAM_RET_E;

typedef enum { OSD_DATE_FORMAT_YYYY_MM_DD, OSD_DATE_FORMAT_MM_DD_YYYY, OSD_DATE_FORMAT_DD_MM_YYYY } DateFormat_E;
typedef enum { OSD_TIME_FORMAT_H_MM_SS_TT, OSD_TIME_FORMAT_HH_MM_SS_TT, OSD_TIME_FORMAT_H_MM_SS, OSD_TIME_FORMAT_HH_MM_SS } TimeFormat_E;

typedef struct {
	UINT8 enabled;
	UINT8 alpha;
	UINT16 color;
	MPI_POINT_S start;
	MPI_POINT_S end;
} VIDEO_OSD_PM_PARAM_S;

typedef struct {
	char *path;
	char *font_data;
	ASCII_INDEX *font_index;
	UINT32 max_font_width;
	UINT32 min_height;
	UINT32 max_height;
	UINT32 time_osd_width;
	UINT32 time_osd_height;
} OSD_FONT_INFO_S;

typedef struct {
	UINT16 min_width;
	UINT16 max_width;
	UINT16 x;
	UINT16 y;
} OSD_ALARM_INFO_S;

void initOsdHandle(void);
void loadOsdFont(void);
void unloadOsdFont(void);
void createOsdRegions(MPI_ECHN chn_idx, UINT16 width, UINT16 height, const AGTX_OSD_CONF_OUTER_S *osd, const AGTX_OSD_PM_PARAM_S *osd_pm);
INT32 destroyOsdRegions(MPI_ECHN chn_idx);
void createDateTimeThread(INT32 output_num);
void deleteDateTimeThread();

INT32 createMap(INT32 start_x, INT32 start_y, char *spec, MPI_ECHN chn_idx, UINT16 width, UINT16 height, INT32 show,
                INT32 k);
INT32 createText(INT32 start_x, INT32 start_y, char *spec, MPI_ECHN chn_idx, UINT16 width, UINT16 height, INT32 show,
                 INT32 k);
INT32 deleteText(MPI_ECHN chn_idx, INT32 k);
INT32 createInfo(INT32 start_x, INT32 start_y, char *spec, MPI_ECHN chn_idx, UINT16 width, UINT16 height, INT32 show,
                 INT32 k);

INT32 showHideOsdRegion(INT32 show, INT32 i, INT32 j);
INT32 bindOsdRegion(INT32 i, INT32 j, const MPI_OSD_BIND_ATTR_S *osd_bind);
INT32 unbindOsdRegion(INT32 i, INT32 j);
void setOsdDateTimeFormat(char *spec);
INT32 showIvaAlarm();

INT32 VIDEO_OSD_setShowWeekDay(INT32 showWeekDay);

INT32 VIDEO_OSD_touchChn(MPI_ECHN chn_idx);
INT32 VIDEO_OSD_untouchChn(MPI_ECHN chn_idx);

INT32 VIDEO_OSD_checkParam(MPI_ECHN chn_idx, const AGTX_OSD_CONF_OUTER_S *cur, const AGTX_OSD_CONF_OUTER_S *new);
INT32 VIDEO_OSD_checkPmParam(MPI_ECHN chn_idx, const AGTX_OSD_PM_PARAM_S *cur, const AGTX_OSD_PM_PARAM_S *new, UINT32 width, UINT32 height);

INT32 VIDEO_OSD_setBindLoc(MPI_ECHN chn_idx, INT32 handle_idx, UINT32 width, UINT32 height, const AGTX_OSD_CONF_INNER_S *rgn);

UINT16 VIDEO_OSD_getAyuvColor(INT32 color_index);
INT32 VIDEO_OSD_initPrivacyMask(MPI_ECHN chn_idx, UINT32 width, UINT32 height, const AGTX_OSD_PM_PARAM_S *osd_pm);
INT32 VIDEO_OSD_deletePrivacyMask(MPI_ECHN chn_idx);
INT32 VIDEO_OSD_getPmParam(MPI_ECHN chn_idx, INT32 chn_handle_idx, VIDEO_OSD_PM_PARAM_S *param);
INT32 VIDEO_OSD_setPmParam(MPI_ECHN chn_idx, INT32 chn_handle_idx, const VIDEO_OSD_PM_PARAM_S *param);
INT32 VIDEO_OSD_bindPmToChn(MPI_ECHN chn_idx, INT32 chn_handle_idx);
INT32 VIDEO_OSD_unbindPmFromChn(MPI_ECHN chn_idx, INT32 chn_handle_idx);

//INT32 createOsdRegion(MPI_ECHN chn_idx, INT32 output_num, UINT16 width,
//UINT16 height);

#ifdef __cplusplus
}
#endif

#endif /* OSD_H_ */
