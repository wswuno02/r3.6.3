#ifndef VIDEO_API_H_
#define VIDEO_API_H_

#ifdef __cplusplus
extern "C" {
#endif /**< __cplusplus */

#include "mpi_index.h"
#include "config_api.h"
#include "agtx_video.h"

/* Interface function prototype */
INT32 VIDEO_SYS_initSystem(CFG_ALL_S *vin);
INT32 VIDEO_SYS_exitSystem(VOID);

INT32 VIDEO_DEV_createVideoDev(CFG_VIN_S *conf);
INT32 VIDEO_DEV_startVideoDev(CFG_VIN_S *conf);
INT32 VIDEO_DEV_startVideoChn(MPI_DEV dev_idx, CFG_VCHN_S *all_conf);
INT32 VIDEO_DEV_stopVideoDev(CFG_VIN_S *conf);
INT32 VIDEO_DEV_destroyVideoDev(CFG_VIN_S *conf);
INT32 VIDEO_DEV_stopVideoChn(MPI_DEV dev_idx, CFG_VCHN_S *all_conf);
INT32 VIDEO_DEV_setVideoChn(MPI_CHN chn_idx, CFG_VCHN_S *conf);

INT32 VIDEO_DEV_setStitchAttr(MPI_DEV dev_idx, CFG_STITCH_S *conf);
INT32 VIDEO_DEV_setLdcConf(MPI_DEV dev_idx, AGTX_LDC_CONF_S *ldc_cfg);
INT32 VIDEO_DEV_setPanoramaConf(MPI_DEV dev_idx, AGTX_PANORAMA_CONF_S *pano_cfg);
INT32 VIDEO_DEV_setPanningConf(MPI_DEV dev_idx, AGTX_PANNING_CONF_S *pann_cfg);
INT32 VIDEO_DEV_setSurroundConf(MPI_DEV dev_idx, AGTX_SURROUND_CONF_S *surr_cfg);

INT32 VIDEO_ENC_startEncChn(CFG_VENC_S *conf);
INT32 VIDEO_ENC_stopEncChn(CFG_VENC_S *conf);
INT32 VIDEO_ENC_setEncChn(CFG_VENC_S *conf);

//INT32 VIDEO_OSD_startTimestamp(CFG_VENC_S *cfg_venc,CFG_OSD_S *cfg_ts);

/* TODO: to be removed */
VOID VIDEO_DIP_createIniUpdateThread(MPI_DEV dev_idx);
VOID VIDEO_DIP_destroyIniUpdateThread(MPI_DEV dev_idx);

//INT32 VIDEO_DIP_getBrightness(UINT32 dev_idx, UINT8 *brightness);
INT32 VIDEO_DIP_setBrightness(MPI_DEV dev_idx, UINT8 *brightness);
//INT32 VIDEO_DIP_getContrast(UINT32 dev_idx, UINT8 *contrast);
INT32 VIDEO_DIP_setContrast(MPI_DEV dev_idx, UINT8 *contrast);
//INT32 VIDEO_DIP_getSharpness(UINT32 dev_idx, UINT8 *sharpness);
INT32 VIDEO_DIP_setSharpness(MPI_DEV dev_idx, UINT8 *sharpness);
//INT32 VIDEO_DIP_getSaturation(UINT32 dev_idx, UINT8 *saturation);
INT32 VIDEO_DIP_setSaturation(MPI_DEV dev_idx, UINT8 *saturation);
//INT32 VIDEO_DIP_getHue(UINT32 dev_idx, INT16 *hue);
INT32 VIDEO_DIP_setHue(MPI_DEV dev_idx, INT16 *hue);
INT32 VIDEO_DIP_setAntiFlicker(MPI_DEV dev_idx, CFG_ANTI_FLICKER_E *anti_flicker);
INT32 VIDEO_setAdvImgPref(MPI_DEV dev_idx, CFG_ADV_IMG_S *adv_img_pref_cfg);
INT32 VIDEO_setAwbPref(MPI_DEV dev_idx, CFG_AWB_S *awb_pref_cfg);
INT32 VIDEO_setColorMode(MPI_DEV dev_idx, CFG_COLOR_S *color_cfg);
INT32 VIDEO_DIP_setDefaultAntiFlicker(MPI_DEV dev_idx, AGTX_ANTI_FLICKER_CONF_S *anti_flicker_cfg);
INT32 VIDEO_DIP_setDipAllAttr(MPI_DEV dev_idx);
INT32 VIDEO_DIP_initDipHighAttr(MPI_DEV dev_idx);
INT32 VIDEO_getInputFps(MPI_DEV dev_idx, FLOAT *fps);
INT32 VIDEO_getDevAttr(MPI_DEV idx);

#ifdef __cplusplus
}
#endif /**< __cplusplus */

#endif /**< !VIDEO_API_H_ */
