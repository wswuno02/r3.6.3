#ifndef DIP_H_
#define DIP_H_

#ifdef __cplusplus
extern "C" {
#endif /**< __cplusplus */

#include "agtx_dip_all_conf.h"

INT32 VIDEO_DIP_setCal(MPI_DEV dev_idx, AGTX_DIP_CAL_CONF_S *cal_cfg);
INT32 VIDEO_DIP_setDbc(MPI_DEV dev_idx, AGTX_DIP_DBC_CONF_S *dbc_cfg);
INT32 VIDEO_DIP_setDcc(MPI_DEV dev_idx, AGTX_DIP_DCC_CONF_S *dcc_cfg);
INT32 VIDEO_DIP_setLsc(MPI_DEV dev_idx, AGTX_DIP_LSC_CONF_S *lsc_cfg);
INT32 VIDEO_DIP_setRoi(MPI_DEV dev_idx, AGTX_DIP_ROI_CONF_S *roi_cfg);
INT32 VIDEO_DIP_setCtrl(MPI_DEV dev_idx, AGTX_DIP_CTRL_CONF_S *ctrl_cfg);
INT32 VIDEO_DIP_setAe(MPI_DEV dev_idx, AGTX_DIP_AE_CONF_S *ae_cfg);
INT32 VIDEO_DIP_setIso(MPI_DEV dev_idx, AGTX_DIP_ISO_CONF_S *iso_cfg);
INT32 VIDEO_DIP_setAwb(MPI_DEV dev_idx, AGTX_DIP_AWB_CONF_S *awb_cfg);
INT32 VIDEO_DIP_setCsm(MPI_DEV dev_idx, AGTX_DIP_CSM_CONF_S *csm_cfg);
INT32 VIDEO_DIP_setPta(MPI_DEV dev_idx, AGTX_DIP_PTA_CONF_S *pta_cfg);
INT32 VIDEO_DIP_setShp(MPI_DEV dev_idx, AGTX_DIP_SHP_CONF_S *shp_cfg);
INT32 VIDEO_DIP_setNr(MPI_DEV dev_idx, AGTX_DIP_NR_CONF_S *nr_cfg);
INT32 VIDEO_DIP_setWinShp(MPI_WIN win_idx, AGTX_SHP_WINDOW_PARAM_S *shp_cfg);
INT32 VIDEO_DIP_setWinNr(MPI_WIN win_idx, AGTX_NR_WINDOW_PARAM_S *nr_cfg);
INT32 VIDEO_DIP_setTe(MPI_DEV dev_idx, AGTX_DIP_TE_CONF_S *te_cfg);
INT32 VIDEO_DIP_setGamma(MPI_DEV dev_idx, AGTX_DIP_GAMMA_CONF_S *gamma_cfg);
INT32 VIDEO_DIP_setEnh(MPI_DEV dev_idx, AGTX_DIP_ENH_CONF_S *enh_cfg);
INT32 VIDEO_DIP_setCoring(MPI_DEV dev_idx, AGTX_DIP_CORING_CONF_S *coring_cfg);
INT32 VIDEO_DIP_setFcs(MPI_DEV dev_idx, AGTX_DIP_FCS_CONF_S *fcs_cfg);
INT32 VIDEO_DIP_setStat(MPI_DEV dev_idx, AGTX_DIP_STAT_CONF_S *stat_cfg);
INT32 VIDEO_DIP_setHdrSynth(MPI_DEV dev_idx, AGTX_DIP_HDR_SYNTH_CONF_S *hdr_synth_cfg);

#ifdef __cplusplus
}
#endif /**< __cplusplus */

#endif /**< !DIP_H_ */
