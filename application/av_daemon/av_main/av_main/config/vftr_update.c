#include "vftr_update.h"
#include <sys/stat.h>
#include "mtk_common.h"

#include "video_api.h"
#include "avftr_ef.h"
#include "avftr_td.h"
#include "avftr_ld.h"
#include "avftr_md.h"
#include "video_od.h"
#include "video_rms.h"
#include "avftr_aroi.h"
#include "avftr_pd.h"
#include "agtx_prio.h"

#define VFTR_UPDATE_TH_NAME "vftr_update"
#define UPDATE_CONFIG_PATH "./vftr_update"
#define MAX_EF_VL_NUM 3

static INT32 g_old_time = 0; /*FIXME temp solution for dynamic change VFTR setting*/

static void tdUpdate(UINT32 dev_idx, UINT32 chn_idx, UINT32 win_idx, FILE *fp)
{
	INT32 td_enable = 0;
	INT32 td_reset = 0;
	AVFTR_TD_PARAM_S td;
	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, win_idx);
	fscanf(fp, "td: enable=%d\n\tsensitivity=%d, endurance=%d, reset=%d\n", &td_enable, &td.td_param.sensitivity,
	       &td.td_param.endurance, &td_reset);
	printf("td: enable=%d\n\tsensitivity=%d, endurance=%d, reset=%d\n", td_enable, td.td_param.sensitivity,
	       td.td_param.endurance, td_reset);
	if (td_enable) {
		AVFTR_TD_enable(idx);
		AVFTR_TD_setParam(idx, &td);
		if (td_reset) {
			AVFTR_TD_reset(idx);
		}
	} else {
		AVFTR_TD_disable(idx);
	}
}

static void ldUpdate(UINT32 dev_idx, UINT32 chn_idx, UINT32 win_idx, FILE *fp)
{
	INT32 ld_enable = 0;
	VFTR_LD_PARAM_S ld;
	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, win_idx);

	fscanf(fp,
	       "ld: enable=%d\n\tsen_th=%hu, alarm_supr=%hhu, alarm_latency=%hhu, alarm_latency_cycle=%hhu, det_period=%hhu, trig_cond=%d, ",
	       &ld_enable, &ld.sen_th, &ld.alarm_supr, &ld.alarm_latency, &ld.alarm_latency_cycle, &ld.det_period,
	       (INT32 *)&ld.trig_cond);
	fscanf(fp, "sx=%hd, sy=%hd, ex=%hd, ey=%hd\n", &ld.roi.sx, &ld.roi.sy, &ld.roi.ex, &ld.roi.ey);
	printf("ld: enable=%d\n\tsen_th=%d, alarm_supr=%d, alarm_latency=%d, alarm_latency_cycle=%d, det_period=%d, trig_cond=%d, ",
	       ld_enable, ld.sen_th, ld.alarm_supr, ld.alarm_latency, ld.alarm_latency_cycle, ld.det_period,
	       ld.trig_cond);
	printf("sx=%hd, sy=%hd, ex=%hd, ey=%hd\n", ld.roi.sx, ld.roi.sy, ld.roi.ex, ld.roi.ey);
	if (ld_enable) {
		AVFTR_LD_enable(idx);
		AVFTR_LD_setParam(idx, &ld);
	} else {
		AVFTR_LD_disable(idx);
	}
}

static void mdUpdate(UINT32 dev_idx, UINT32 chn_idx, UINT32 win_idx, FILE *fp)
{
	INT32 md_enable = 0;
	AVFTR_MD_PARAM_S md = { 0 };
	int i = 0;
	uint8_t roi_idx = 0;
	static int last_reg_num = 0;
	int reg_num = 0;
	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, win_idx);

	fscanf(fp, "md: enable=%d, reg_num=%d\n", &md_enable, &reg_num);
	printf("md: enable=%d, reg_num=%d\n", md_enable, reg_num);
	for (i = 0; i < reg_num; i++) {
		fscanf(fp, "\tthr_v_obj_min=%hu, thr_v_obj_max=%hu, thr_v_reg=%llu, md_mode=%d\n",
		    &md.md_param.attr[i].thr_v_obj_min, &md.md_param.attr[i].thr_v_obj_max,
		    &md.md_param.attr[i].thr_v_reg, (INT32 *)&md.md_param.attr[i].md_mode);
		printf("\tthr_v_obj_min=%d, thr_v_obj_max=%d, thr_v_reg=%llu, md_mode=%d\n",
		    md.md_param.attr[i].thr_v_obj_min, md.md_param.attr[i].thr_v_obj_max,
		    md.md_param.attr[i].thr_v_reg, md.md_param.attr[i].md_mode);
		fscanf(fp, "\tsx=%hd, sy=%hd, ex=%hd, ey=%hd\n",
		    &md.md_param.attr[i].pts.sx, &md.md_param.attr[i].pts.sy,
		    &md.md_param.attr[i].pts.ex, &md.md_param.attr[i].pts.ey);
		printf("\tsx=%hd, sy=%hd, ex=%hd, ey=%hd\n",
		    md.md_param.attr[i].pts.sx, md.md_param.attr[i].pts.sy,
		    md.md_param.attr[i].pts.ex, md.md_param.attr[i].pts.ey);
	}
	if (md_enable) {
		//VIDEO_FTR_setMdParam(idx, &md);
		
		for (i = 0; i < last_reg_num; i++) {
			AVFTR_MD_rmRoi(idx, i + 1);
		}
		AVFTR_MD_enable(idx);
		AVFTR_MD_setParam(idx, &md);
		for (i = 0; i < reg_num; i++) {
			AVFTR_MD_addRoi(idx, &md.md_param.attr[i], &roi_idx);
		}
		last_reg_num = reg_num;
	} else {
		AVFTR_MD_disable(idx);
	}
}

static void efUpdate(UINT32 dev_idx, UINT32 chn_idx, UINT32 win_idx, FILE *fp)
{
	INT32 ef_enable = 0;
	VFTR_EF_VL_ATTR_S *ef;
	static int last_vl_num = 0;
	int vl_num = 0;
	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, win_idx);

	fscanf(fp, "ef: enable=%d, vl_num=%d\n", &ef_enable, &vl_num);
	printf("ef: enable=%d, vl_num=%d\n", ef_enable, vl_num);
	ef = malloc(sizeof(VFTR_EF_VL_ATTR_S) * vl_num);
	for (int fence_i = 0; fence_i < vl_num; fence_i++) {
		fscanf(fp, "\tsx=%hd, sy=%hd, ex=%hd, ey=%hd, obj_min_size={%hu, %hu}, obj_max_size={%hu, %hu}, obj_v_th=%hhu, mode=%d\n",
		       &ef[fence_i].line.sx, &ef[fence_i].line.sy, &ef[fence_i].line.ex, &ef[fence_i].line.ey,
		       &ef[fence_i].obj_size_min.width, &ef[fence_i].obj_size_min.height, &ef[fence_i].obj_size_max.width, &ef[fence_i].obj_size_max.height,
		       &ef[fence_i].obj_v_th, (INT32 *)&ef[fence_i].mode);
		printf("\tsx=%hd, sy=%hd, ex=%hd, ey=%hd, obj_min_size={%hu, %hu}, obj_max_size={%hu, %hu}, obj_v_th=%hhu, mode=%d\n",
		       ef[fence_i].line.sx, ef[fence_i].line.sy, ef[fence_i].line.ex, ef[fence_i].line.ey,
		       ef[fence_i].obj_size_min.width, ef[fence_i].obj_size_min.height, ef[fence_i].obj_size_max.width, ef[fence_i].obj_size_max.height,
		       ef[fence_i].obj_v_th, ef[fence_i].mode);
	}
	if (ef_enable) {
		for (int fence_i = 0; fence_i < last_vl_num; fence_i++) {
			AVFTR_EF_rmVl(idx, fence_i + 1);
		}
		AVFTR_EF_enable(idx);
		for (int fence_i = 0; fence_i < vl_num; fence_i++) {
			AVFTR_EF_addVl(idx, &ef[fence_i]);
		}
		last_vl_num = vl_num;
	} else {
		AVFTR_EF_disable(idx);
	}
	free(ef);
}

static void odUpdate(UINT32 dev_idx, UINT32 chn_idx, UINT32 win_idx, FILE *fp)
{
	INT32 od_enable = 0;
	VIDEO_FTR_OD_PARAM_S od = {0};
	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, win_idx);

	fscanf(fp, "od: enable=%d\n\tquality=%hhu, size=%hhu, sensitivity=%hhu\n",
	    &od_enable, &od.od_param.od_qual, &od.od_param.od_size_th, &od.od_param.od_sen);
	printf("od: enable=%d\n\tquality=%hhu, size=%hhu, sensitivity=%hhu\n",
	    od_enable, od.od_param.od_qual, od.od_param.od_size_th, od.od_param.od_sen);
	if (od_enable) {
		VIDEO_FTR_enableOd(idx);
		VIDEO_FTR_setOdParam(idx, &od);
	} else {
		VIDEO_FTR_disableOd(idx);
	}
}

static void rmsUpdate(UINT32 dev_idx, UINT32 chn_idx, UINT32 win_idx, FILE *fp)
{
	INT32 rms_enable = 0;
	MPI_IVA_RMS_PARAM_S rms;
	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, win_idx);

	fscanf(fp, "rms: enable=%d\n\tsensitivity=%hhu, split_x=%hhu, split_y=%hhu\n", &rms_enable, &rms.sen,
	       &rms.split_x, &rms.split_y);
	printf("rms: enable=%d\n\tsensitivity=%hhu, split_x=%hhu, split_y=%hhu\n", rms_enable, rms.sen, rms.split_x,
	       rms.split_y);
	if (rms_enable) {
		VIDEO_FTR_enableRms(idx);
		VIDEO_FTR_setRmsParam(idx, &rms);
	} else {
		VIDEO_FTR_disableRms(idx);
	}
}

static void aroiUpdate(UINT32 dev_idx, UINT32 chn_idx, UINT32 win_idx, FILE *fp)
{
	INT32 aroi_enable = 0;
	AVFTR_AROI_PARAM_S aroi = { 0 };
	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, win_idx);

	fscanf(fp, "aroi: enable=%d\n\taspect ratio=%hu, min roi={%hu, %hu}, max roi={%hu, %hu}, ", &aroi_enable,
	       &aroi.aroi_param.aspect_ratio,
	       &aroi.aroi_param.min_roi.width, &aroi.aroi_param.min_roi.height,
	       &aroi.aroi_param.max_roi.width, &aroi.aroi_param.max_roi.height);
	fscanf(fp, "max track delta={%hhu, %hhu}, update ratio=%hhu\n", &aroi.aroi_param.max_track_delta_x,
	       &aroi.aroi_param.max_track_delta_y, &aroi.aroi_param.update_ratio);
	fscanf(fp, "\tmax return delta={%hhu, %hhu}, wait time=%hu\n", &aroi.aroi_param.max_return_delta_x,
	       &aroi.aroi_param.max_return_delta_y, &aroi.aroi_param.wait_time);
	printf("aroi: enable=%d\n\taspect ratio=%hu, min roi={%hu, %hu}, max roi={%hu, %hu}, ", aroi_enable,
	       aroi.aroi_param.aspect_ratio,
	       aroi.aroi_param.min_roi.width, aroi.aroi_param.min_roi.height,
	       aroi.aroi_param.max_roi.width, aroi.aroi_param.max_roi.height);
	printf("max track delta={%hhu, %hhu}, update ratio=%hhu\n",
	       aroi.aroi_param.max_track_delta_x, aroi.aroi_param.max_track_delta_y,
	       aroi.aroi_param.update_ratio);
	printf("\tmax return delta={%hhu, %hhu}, wait time=%hu\n",
	       aroi.aroi_param.max_return_delta_x, aroi.aroi_param.max_return_delta_y,
	       aroi.aroi_param.wait_time);
	if (aroi_enable) {
		AVFTR_AROI_enable(idx);
		AVFTR_AROI_setParam(idx, &aroi);
	} else {
		AVFTR_AROI_disable(idx);
	}
}

static void pdUpdate(UINT32 dev_idx, UINT32 chn_idx, UINT32 win_idx, FILE *fp)
{
	INT32 pd_enable = 0;
	VFTR_OSC_PARAM_S pd;
	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, win_idx);

	fscanf(fp, "pd: enable=%d\n\tmin_aspect_ratio=%u, max_aspect_ratio=%u, ", &pd_enable, &pd.min_aspect_ratio,
	       &pd.max_aspect_ratio);
	fscanf(fp, "min pd={%hu, %hu}, max pd={%hu, %hu}\n", &pd.min_sz.width, &pd.min_sz.height, &pd.max_sz.width,
	       &pd.max_sz.height);
	printf("pd: enable=%d\n\tmin_aspect_ratio=%u, max_aspect_ratio=%u, ", pd_enable, pd.min_aspect_ratio,
	       pd.max_aspect_ratio);
	printf("min pd={%hu, %hu}, max pd={%hu, %hu}\n", pd.min_sz.width, pd.min_sz.height, pd.max_sz.width,
	       pd.max_sz.height);
	if (pd_enable) {
		AVFTR_PD_enable(idx);
		AVFTR_PD_setParam(idx, &pd);
	} else {
		AVFTR_PD_disable(idx);
	}
}

/*FIXME temp solution for runtime change VFTR*/
static void *vftrUpdate(void *data)
{
	struct stat buf;
	FILE *fp = NULL;
	UINT32 dev_idx = 0;
	UINT32 chn_idx = 0;
	UINT32 win_idx = 0;

	SYS_TRACE("config update thread start\n");

	if (setThreadSchedAttr(VFTR_UPDATE_TH_NAME)) {
		printf("Failed to set thread %s!\n", VFTR_UPDATE_TH_NAME);
	}

	if (stat(UPDATE_CONFIG_PATH, &buf) != 0) {
		SYS_TRACE("Leave VFTR config update\n");
		return NULL;
	}

	while (1) {
		if (stat(UPDATE_CONFIG_PATH, &buf) != 0) {
			SYS_TRACE("Leave VFTR config update\n");
		} else { /* print the date/time last modified */
			if (g_old_time == 0) {
				g_old_time = buf.st_mtime;
				continue;
			}
			//			printf("time %d / %d\n", buf.st_mtime, g_old_time);
			if (buf.st_mtime > g_old_time) {
				fp = fopen(UPDATE_CONFIG_PATH, "r+");
				if (fp <= 0) {
					SYS_TRACE("filename %s open fail %d(%m)\n", UPDATE_CONFIG_PATH, errno);
					return NULL;
				}

				fscanf(fp, "dev=%d, chn=%d, win=%d\n", &dev_idx, &chn_idx, &win_idx);
				printf("dev=%d, chn=%d, win=%d\n", dev_idx, chn_idx, win_idx);

				tdUpdate(dev_idx, chn_idx, win_idx, fp);
				ldUpdate(dev_idx, chn_idx, win_idx,  fp);
				mdUpdate(dev_idx, chn_idx, win_idx,  fp);
				efUpdate(dev_idx, chn_idx, win_idx,  fp);
				odUpdate(dev_idx, chn_idx, win_idx,  fp);
				rmsUpdate(dev_idx, chn_idx, win_idx,  fp);
				aroiUpdate(dev_idx, chn_idx, win_idx, fp);
				pdUpdate(dev_idx, chn_idx, win_idx,  fp);

				fclose(fp);
				g_old_time = buf.st_mtime;
			}
		}

		sleep(1);
	}

	return NULL;
}

INT32 VFTR_UPDATE_init()
{
	pthread_t tid = 0;
	int ret = 0;

	/*FIXME temp solution for runtime change VFTR*/
	if (pthread_create(&tid, NULL, vftrUpdate, NULL) != 0) {
		SYS_TRACE("Cancel thread of vftrUpdate.\n");
		return -1;
	}

	ret = pthread_setname_np(tid, VFTR_UPDATE_TH_NAME);
	if (ret != 0) {
		printf("Set thread to vftrUpdate failed. ret %d\n", ret);
		return -1;
	}

	return 0;
}

INT32 VFTR_UPDATE_exit()
{
	FILE *fp = NULL;
	UINT32 dev_idx = 0;
	UINT32 chn_idx = 0;
	UINT32 win_idx = 0;
	MPI_WIN idx;

	fp = fopen(UPDATE_CONFIG_PATH, "r+");
	if (fp <= 0) {
		return 0;
	}
	fscanf(fp, "dev=%d, chn=%d, win=%d\n", &dev_idx, &chn_idx, &win_idx);
	idx = MPI_VIDEO_WIN(dev_idx, chn_idx, win_idx);
	AVFTR_TD_disable(idx);
	AVFTR_LD_disable(idx);
	AVFTR_MD_disable(idx);
	AVFTR_EF_disable(idx);
	VIDEO_FTR_disableOd(idx);
	VIDEO_FTR_disableRms(idx);
	AVFTR_AROI_disable(idx);
	AVFTR_PD_disable(idx);
	fclose(fp);
	return 0;
}
