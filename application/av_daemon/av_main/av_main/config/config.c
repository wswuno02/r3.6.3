#include "mtk_common.h"
#include "config.h"
#include "config_api.h"
#include "pthread.h"
#include <assert.h>

#include "video_api.h"
#include "avftr.h"
#include "avftr_conn.h"
#include "avftr_ef.h"
#include "avftr_td.h"
#include "avftr_ld.h"
#include "avftr_md.h"
#include "avftr_aroi.h"
#include "avftr_pd.h"
#include "video_od.h"
#include "video_rms.h"
#include "video_vdbg.h"
#include "video_ptz.h"
#include "avftr_shd.h"
#include "avftr_eaif.h"
#include "video_pfm.h"
#include "video_bm.h"
#include "avftr_dk.h"
#include "avftr_fld.h"
#include "avftr_sd.h"
#include "osd.h"


typedef struct {
	MPI_WIN idx;
	MPI_SIZE_S res;
} AROI_CB_INFO;

#define EAIF_FACE_DIR_NAME "/usrdata/eaif/facereco"
#define EAIF_FACE_BIN_NAME "face.bin"

static const char *g_iva_data_path = "/usrdata/active_setting/iva/";
static INT32 g_video_init = 0;
static INT32 g_cfg_init = 0;
static CFG_ALL_S g_config = { { { 0 } } };
pthread_mutex_t g_cfg_mutex = PTHREAD_MUTEX_INITIALIZER;
extern int g_mpi_sys_initialized;

extern AVFTR_MD_ALARM_CB g_md_cb;
extern AVFTR_TD_ALARM_CB g_td_cb;

AROI_CB_INFO g_aroi_cb_info = {{{0}}};

static INT32 cfg_apply_vin(CFG_VIN_S *cfg_vin)
{
	INT32 ret = 0;

	/*apply config to mpi dev*/
	do {
		ret = VIDEO_DEV_startVideoDev(cfg_vin);

		if (ret == MPI_FAILURE) {
			VIDEO_DEV_stopVideoDev(cfg_vin);
		}
	} while (ret == MPI_FAILURE);

	return ret;
}

static INT32 cfg_apply_vchn(MPI_DEV idx, CFG_VCHN_S *cfg_vchn, INT32 init)
{
	MPI_CHN chn = MPI_VIDEO_CHN(idx.dev, 0);
	INT32 ret = 0;

	/*apply config to mpi dev*/
	if (init) {
		ret = VIDEO_DEV_startVideoChn(idx, cfg_vchn);
	} else {
		chn.chn = cfg_vchn->chn_idx;
		ret = VIDEO_DEV_setVideoChn(chn, cfg_vchn);
		if (ret < 0) {
			return ret;
		}
		ret = AVFTR_notifyVideo(MPI_VIDEO_WIN(chn.dev, chn.chn, 0), -1);
	}

	return ret;
}

static INT32 cfg_apply_venc(CFG_VENC_S *cfg_venc, INT32 init)
{
	INT32 ret = 0;
	/*apply config to mpi venc*/
	if (init) {
		ret = VIDEO_ENC_startEncChn(cfg_venc);
	} else {
		ret = VIDEO_ENC_setEncChn(cfg_venc);
	}

	return ret;
}

INT32 cfg_apply_img(CFG_IMG_S *img, UINT32 dev_idx)
{
	INT32 ret = 0;
	MPI_DEV dev = MPI_VIDEO_DEV(dev_idx);

	ret = VIDEO_DIP_setBrightness(dev, &img->brightness);
	if (ret < 0) {
		return ret;
	}

	ret = VIDEO_DIP_setContrast(dev, &img->contrast);
	if (ret < 0) {
		return ret;
	}

	ret = VIDEO_DIP_setSharpness(dev, &img->sharpness);
	if (ret < 0) {
		return ret;
	}

	ret = VIDEO_DIP_setSaturation(dev, &img->saturation);
	if (ret < 0) {
		return ret;
	}

	ret = VIDEO_DIP_setHue(dev, &img->hue);
	if (ret < 0) {
		return ret;
	}

	ret = VIDEO_DIP_setAntiFlicker(dev, &img->anti_flicker);
	if (ret < 0) {
		return ret;
	}

	return ret;
}

static INT32 cfg_apply_md(UINT32 dev_idx, UINT32 chn_idx, UINT32 width, UINT32 height, CFG_IVA_MD_S *md,
                          CFG_IVA_MD_S *md_old, INT32 enable_change)
{
	INT32 ret = 0;
	INT32 i = 0;
	INT32 j = 0;
	AVFTR_MD_PARAM_S vftr_md;
	AVFTR_MD_PARAM_S vftr_md_old = { 0 };
	vftr_md.duration = md->alarm_buffer; // AVFTR_VIDEO_JIF_HZ (unit: 10ms);
	vftr_md.en_skip_shake = md->en_skip_shake;
	vftr_md.en_skip_pd = md->en_skip_pd;

	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, 0);

	/*if enable changed,enable or disable MD*/
	if (enable_change && md->enabled) {
		if (g_md_cb != NULL) {
			ret = AVFTR_MD_regCallback(idx, g_md_cb);
			if (ret != 0) {
				return ret;
			}
		}
		ret = AVFTR_MD_enable(idx);
		if (ret != 0) {
			return ret;
		}
	} else if (enable_change && !md->enabled) {
		ret = AVFTR_MD_disable(idx);
		if (ret != 0) {
			return ret;
		}
	}

	ret = AVFTR_MD_getParam(idx, &vftr_md_old);
	if (ret != 0) {
		return ret;
	}

	ret = AVFTR_MD_setParam(idx, &vftr_md);
	if (ret != 0) {
		return ret;
	}

#define MD_ENERGY_TH_V_REF_RATIO (15) /* FIXME: Tuning the featurelib or formula */
#define MD_ENERGY_TH_V_REF_RATIO_MAX (255)

	vftr_md.md_param.region_num = md->rgn_cnt;
	for (i = 0; i < md->rgn_cnt; i++) {
		//Convert from percent to actually point
		vftr_md.md_param.attr[i].pts.sx = (width * md->rgn_list[i].sx / 100);
		vftr_md.md_param.attr[i].pts.sy = (height * md->rgn_list[i].sy / 100);
		vftr_md.md_param.attr[i].pts.ex = (width * md->rgn_list[i].ex / 100);
		vftr_md.md_param.attr[i].pts.ey = (height * md->rgn_list[i].ey / 100);
		vftr_md.md_param.attr[i].thr_v_obj_max = md->rgn_list[i].max_spd;
		vftr_md.md_param.attr[i].thr_v_obj_min = md->rgn_list[i].min_spd;
		vftr_md.md_param.attr[i].obj_life_th = md->rgn_list[i].obj_life_th;
		vftr_md.md_param.attr[i].det_method = (md->rgn_list[i].det_method == AGTX_IVA_MD_DET_NORMAL) ?
		                                              VFTR_MD_DET_NORMAL :
		                                              VFTR_MD_DET_SUBTRACT;
		vftr_md.md_param.attr[i].md_mode = (md->rgn_list[i].mode == CFG_IVA_MD_MODE_AREA) ?
		                                           VFTR_MD_MOVING_AREA :
		                                           (md->rgn_list[i].mode == CFG_IVA_MD_MODE_ENERGY) ?
		                                           VFTR_MD_MOVING_ENERGY :
		                                           VFTR_MD_MOVING_AREA;
		if (md->rgn_list[i].mode == CFG_IVA_MD_MODE_AREA) {
			vftr_md.md_param.attr[i].thr_v_reg =
			        ((width * height / 100) * (100 - md->rgn_list[i].sens)) / 1000;
		} else { //CFG_IVA_MD_MODE_MOVING_ENERGY
			vftr_md.md_param.attr[i].thr_v_reg =
			        (((((width * height) / 100) * md->rgn_list[i].max_spd) / MD_ENERGY_TH_V_REF_RATIO ) * (100 - md->rgn_list[i].sens)) / 1000;
		}
	}

	for (i = 0; i < md_old->rgn_cnt; i++) {
		ret = AVFTR_MD_rmRoi(idx, md_old->rgn_list[i].id);
		if (ret != 0) {
			return ret;
		}
	}

	/*Apply new ROI */
	INT32 final_ret = 0;
	for (i = 0; i < md->rgn_cnt; i++) {
		ret = AVFTR_MD_addRoi(idx, &vftr_md.md_param.attr[i], (UINT8 *)&md->rgn_list[i].id);
		if (ret != 0) {
			final_ret = 1;
			break;
		}
	}

	/* Error handling */
	if (final_ret) {
		/* Remove newly added ROI */
		for (j = 0; j < i; j++) {
			ret = AVFTR_MD_rmRoi(idx, md->rgn_list[j].id);
			if (ret != 0) {
				return ret;
			}
		}
		/* Add old ROI Here should not return fail as it is from old setting */
		for (i = 0; i < vftr_md_old.md_param.region_num; i++) {
			ret = AVFTR_MD_addRoi(idx, &vftr_md_old.md_param.attr[i],
			                      (UINT8 *)&vftr_md_old.md_param.attr[i].id);
			if (ret != 0) {
				return ret;
			}
		}
		/* Apply old alarm buff setup */
		ret = AVFTR_MD_setParam(idx, &vftr_md_old);
		if (ret != 0) {
			return ret;
		}
		return -1;
	}

	return 0;
}

static INT32 cfg_close_md(MPI_WIN idx, CFG_IVA_MD_S *md)
{
	INT32 ret = 0;
	INT32 i = 0;
	/*remove old ROI*/
	for (i = 0; i < md->rgn_cnt; i++) {
		ret = AVFTR_MD_rmRoi(idx, md->rgn_list[i].id);
		if (ret != 0) {
			return ret;
		}
	}

	ret = AVFTR_MD_disable(idx);
	if (ret != 0) {
		return ret;
	}

	return 0;
}

static INT32 cfg_apply_td(UINT32 dev_idx, UINT32 chn_idx, CFG_IVA_TD_S *td, INT32 enable_change)
{
	INT32 ret = 0;
	INT32 temp = 0;
	INT32 sen_min_map = 0;
	INT32 sen_max_map = 0;
	AVFTR_TD_PARAM_S vftr_td = { { 0 } };
	VFTR_TD_PARAM_S *td_param = &vftr_td.td_param;

	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, 0);
	/*if enable changed,enable or disable TD*/
	if (enable_change && td->enabled) {
		if (g_td_cb != NULL) {
			ret = AVFTR_TD_regCallback(idx, g_td_cb);
			if (ret != 0) {
				return ret;
			}
		}
		ret = AVFTR_TD_enable(idx);
		if (ret != 0) {
			return ret;
		}

		ret = AVFTR_TD_regMpiInfo(idx);
		if (ret != 0) {
			return ret;
		}

	} else if (enable_change && !td->enabled) {
		ret = AVFTR_TD_disable(idx);
		if (ret != 0) {
			return ret;
		}

		ret = AVFTR_TD_releaseMpiInfo(idx);
		if (ret != 0) {
			return ret;
		}
	}

	ret = AVFTR_TD_getParam(idx, &vftr_td);
	if (ret != 0) {
		return ret;
	}

	td_param->endurance = td->endurance;
	/*Mapping to useful sensitivity range*/
	sen_min_map = 16;
	sen_max_map = 216;
	temp = ((td->sensitivity * (sen_max_map - sen_min_map) / 100) + sen_min_map);
	td_param->sensitivity = (temp > VFTR_TD_SENSITIVITY_MAX) ? VFTR_TD_SENSITIVITY_MAX : temp;

	ret = AVFTR_TD_setParam(idx, &vftr_td);
	if (ret != 0) {
		return -1;
	}

	/*Always reset TD on TD resume */

	return 0;
}

VOID cfg_aroi_video_empty_cb(MPI_WIN idx, const VFTR_AROI_STATUS_S *status, const VIDEO_FTR_OBJ_LIST_S *obj_list)
{
	return;
}

VOID cfg_aroi_video_cb(MPI_WIN idx, const VFTR_AROI_STATUS_S *status, const VIDEO_FTR_OBJ_LIST_S *obj_list)
{
	const MPI_RECT_POINT_S *p = &status->roi;
	MPI_RECT_POINT_S roi_tmp = { 0 };
	MPI_SIZE_S *res = &g_aroi_cb_info.res;

#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
	roi_tmp.sx = MAX(p->sx * 1024 / MAX(res->width, 1), 0);
	roi_tmp.sy = MAX(p->sy * 1024 / MAX(res->height, 1), 0);
	roi_tmp.ex = MIN(MAX(p->ex * 1024 / MAX(res->width, 1), 0), 1023);
	roi_tmp.ey = MIN(MAX(p->ey * 1024 / MAX(res->height, 1), 0), 1023);
	VIDEO_FTR_updateAutoPtz(&roi_tmp);

	return;
}

#if 0 /* Deprecated Reset function is embedded in ptz video_ftr */
static INT32 resetSubWinROI(UINT32 dev_idx, CFG_PTZ_S *ptz_old)
{
	INT32 ret = 0;
	INT32 i;
	MPI_RECT_S roi = {256 , 256, 512, 512};
	for (i = 0; i < ptz_old->subwindow_disp.win_num; i++) {
		MPI_WIN id = MPI_VIDEO_WIN(dev_idx, ptz_old->subwindow_disp.win[i].chn_idx, ptz_old->subwindow_disp.win[i].win_idx);
		ret = MPI_DEV_setWindowRoi(id, &roi);
		if (ret != MPI_SUCCESS) {
			return -1;
		}
	}
	return 0;
}
#endif

int init_ptz = 1;

static INT32 cfg_apply_ptz(UINT32 dev_idx, CFG_PTZ_S *ptz, CFG_PTZ_S *ptz_old, CFG_VIN_S *vin, UINT32 enable_change)
{
	int i, ret = 0;
	VIDEO_FTR_PTZ_PARAM_S vftr_ptz = {0};

	ret = VIDEO_FTR_getPtzParam(&vftr_ptz);
	if (ret != 0) {
		return -1;
	}

	for (i = 0; i < ptz->subwindow_disp.win_num; i++) {
		int ptz_win = ptz->subwindow_disp.win[i].win_idx;
		int ptz_chn = ptz->subwindow_disp.win[i].chn_idx;
		if ((!vin->cfg_vchn[ptz_chn].enable || vin->cfg_vchn[ptz_chn].layout.window_num <= ptz_win) &&
		    ptz->enabled) {
			SYS_TRACE("Cannot enable ptz with WIN:%x , CHN:%x not enabled\n", ptz_win, ptz_chn);
			return 0;
		}
	}

	if ((enable_change || !VIDEO_FTR_getPtzStat()) && ptz->enabled) {
		ret = VIDEO_FTR_enablePtz();
		if (ret != 0) {
			return -1;
		}
		if (init_ptz) {
			vftr_ptz.roi_bd.width = ptz->roi_width;
			vftr_ptz.roi_bd.height = ptz->roi_height;
			init_ptz = 0;
		}

	} else if (enable_change && !ptz->enabled) {
		ret = VIDEO_FTR_disablePtz();
		if (ret != 0) {
			return -1;
		}
	}

	if (ptz->enabled) {
		vftr_ptz.win_num = ptz->subwindow_disp.win_num;
		for (i = 0; i < ptz->subwindow_disp.win_num; i++) {
			vftr_ptz.win_id[i] = MPI_VIDEO_WIN(dev_idx, ptz->subwindow_disp.win[i].chn_idx, ptz->subwindow_disp.win[i].win_idx);
		}
	}

	vftr_ptz.win_size_limit.min = ptz->win_size_limit_min;
	vftr_ptz.win_size_limit.max = ptz->win_size_limit_max;
	vftr_ptz.mv.x = ptz->win_speed_x;
	vftr_ptz.mv.y = ptz->win_speed_y;
	vftr_ptz.zoom_v.x = ptz->zoom_speed_width;
	vftr_ptz.zoom_v.y = ptz->zoom_speed_height;
	vftr_ptz.mv_pos.x = ptz->win_pos_x;
	vftr_ptz.mv_pos.y = ptz->win_pos_y;
	vftr_ptz.speed.x = ptz->speed_x;
	vftr_ptz.speed.y = ptz->speed_y;
	vftr_ptz.zoom_lvl = ptz->zoom_level;
	vftr_ptz.zoom_change = ptz->zoom_change;
	int same = (ptz_old->mode == ptz->mode);
	switch (ptz->mode) {
	case AGTX_VIDEO_PTZ_MODE_AUTO:
		if (!same || enable_change) {
			ret = AVFTR_AROI_regCallback(g_aroi_cb_info.idx, cfg_aroi_video_cb);
			if (ret != 0) {
				return -1;
			}
		}
		vftr_ptz.mode = VIDEO_FTR_PTZ_MODE_AUTO;
		break;
	case AGTX_VIDEO_PTZ_MODE_MANUAL:
		vftr_ptz.mode = VIDEO_FTR_PTZ_MODE_MANUAL;
		if (!same || enable_change) {
			ret = AVFTR_AROI_regCallback(g_aroi_cb_info.idx, cfg_aroi_video_empty_cb);
			if (ret != 0) {
				return -1;
			}
		}
		break;
	case AGTX_VIDEO_PTZ_MODE_SCAN:
		vftr_ptz.mode = VIDEO_FTR_PTZ_MODE_SCAN;
		vftr_ptz.roi_bd.width = ptz->roi_width;
		vftr_ptz.roi_bd.height = ptz->roi_height;
		if (!same) {
			ret = AVFTR_AROI_regCallback(g_aroi_cb_info.idx, cfg_aroi_video_empty_cb);
			if (ret != 0) {
				return -1;
			}
		}
		break;
	default:
		vftr_ptz.mode = VIDEO_FTR_PTZ_MODE_MANUAL;

		ret = AVFTR_AROI_regCallback(g_aroi_cb_info.idx, cfg_aroi_video_empty_cb);
		if (ret != 0) {
			return -1;
		}

		break;
	}

	// protect restreaming status
	if (ptz->enabled) {
		ret = VIDEO_FTR_setPtzParam(&vftr_ptz);
		if (ret != 0) {
			return ret;
		}
	}

	return 0;
}

static INT32 cfg_apply_aroi(UINT32 dev_idx, UINT32 chn_idx,
                            UINT32 width, UINT32 height,
                            CFG_IVA_AROI_S *aroi, CFG_IVA_AROI_S *aroi_old,
                            INT32 enable_change)
{
	INT32 ret = 0;
	INT32 aspect_ratio = 0;
	UINT16 ar_limit = ~0;
	UINT8 i;
	/* TODO support window mode */
	UINT32 win_idx = 0;

	AVFTR_AROI_PARAM_S vftr_aroi = { 0 };

	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, win_idx);
	/*if enable changed,enable or disable AROI*/
	if (enable_change && aroi->enabled) {
		ret = AVFTR_AROI_enable(idx);
		if (ret != 0) {
			return ret;
		}
	} else if (enable_change && !aroi->enabled) {
		ret = AVFTR_AROI_disable(idx);
		if (ret != 0) {
			return ret;
		}
	}

	ret = AVFTR_AROI_getParam(idx, &vftr_aroi);
	if (ret != 0) {
		return -1;
	}

	if (aroi->aspect_ratio_height == 0) {
		aspect_ratio = 0;
	} else {
		aspect_ratio = (aroi->aspect_ratio_width << VFTR_AROI_AR_FRACTIONAL_BIT) / aroi->aspect_ratio_height;
		aspect_ratio = (aspect_ratio > ar_limit) ? ar_limit : aspect_ratio;
	}
	/* TODO get window attr from avmain config instead from MPI */
	MPI_CHN chn = MPI_VIDEO_CHN(dev_idx, chn_idx);
	MPI_CHN_LAYOUT_S layout_attr;
	MPI_DEV_getChnLayout(chn, &layout_attr);

	for (i = 0; i < layout_attr.window_num; i++) {
		if (layout_attr.win_id[i].value == idx.value) {
			break;
		}
	}
	if (i == layout_attr.window_num) {
		return -1;
	}

	vftr_aroi.en_skip_shake = aroi->en_skip_shake;

	width = layout_attr.window[i].width;
	height = layout_attr.window[i].height;
	vftr_aroi.aroi_param.obj_life_th = aroi->obj_life_th;
	vftr_aroi.aroi_param.aspect_ratio = (UINT16)aspect_ratio;
	vftr_aroi.aroi_param.min_roi.width = (aroi->min_roi_width * width + 50) / 100;
	vftr_aroi.aroi_param.min_roi.height = (aroi->min_roi_height * height + 50) / 100;
	vftr_aroi.aroi_param.max_roi.width = (aroi->max_roi_width * width + 50) / 100;
	vftr_aroi.aroi_param.max_roi.height = (aroi->max_roi_height * height + 50) / 100;

	/* Track detla  min=0, mid=50, max=200  */
	/* Return Delta min=4, mid=8,  max=50  */
	/* Wait time    min=2, mid=29, max=100 */
	/* update rate  min=16, mid=16, max=64 */

	if (aroi->track_speed < 50) {
		vftr_aroi.aroi_param.max_track_delta_x = aroi->track_speed;
		vftr_aroi.aroi_param.max_track_delta_y = aroi->track_speed;
		vftr_aroi.aroi_param.update_ratio = 16;
	} else {
		vftr_aroi.aroi_param.max_track_delta_x = aroi->track_speed * 3 - 100;
		vftr_aroi.aroi_param.max_track_delta_y = vftr_aroi.aroi_param.max_track_delta_x;
		vftr_aroi.aroi_param.update_ratio = (aroi->track_speed * 48) / 50 - 32;
	}

	if (aroi->return_speed > 50) {
		vftr_aroi.aroi_param.max_return_delta_x = aroi->return_speed / 2;
		vftr_aroi.aroi_param.wait_time = 2 + ((((100 - aroi->return_speed) * 54) + 50) / 100);
	} else {
		vftr_aroi.aroi_param.max_return_delta_x = 4 + ((aroi->return_speed - 4 + 13) / 25);
		vftr_aroi.aroi_param.wait_time = 29 + ((((100 - (aroi->return_speed * 2)) * 71) + 50) / 100);
	}
	vftr_aroi.aroi_param.max_return_delta_y = vftr_aroi.aroi_param.max_return_delta_x;

	ret = AVFTR_AROI_setParam(idx, &vftr_aroi);
	if (ret != 0) {
		return -1;
	}

	g_aroi_cb_info.idx.value = idx.value;
	g_aroi_cb_info.res = (MPI_SIZE_S){.width=width, .height=height};

	return 0;
}

static INT32 cfg_apply_pd(UINT32 dev_idx, UINT32 chn_idx, UINT32 width, UINT32 height, CFG_IVA_PD_S *pd,
                          INT32 enable_change)
{
	INT32 ret = 0;
	VFTR_OSC_PARAM_S vftr_pd = { { 0 } };
	INT32 length = 0;
	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, 0);
	/*if enable changed,enable or disable PD*/
	if (enable_change && pd->enabled) {
		ret = AVFTR_PD_enable(idx);
		if (ret != 0) {
			return -1;
		}
	} else if (enable_change && !pd->enabled) {
		ret = AVFTR_PD_disable(idx);
		if (ret != 0) {
			return -1;
		}
	}

	ret = AVFTR_PD_getParam(idx, &vftr_pd);
	if (ret != 0) {
		return -1;
	}

	length = (width > height) ? height : width;

	vftr_pd.max_aspect_ratio = ((pd->max_aspect_ratio_w << VFTR_OSC_AR_FRACTIONAL_BIT) + (pd->max_aspect_ratio_h >> 1)) /
	                           pd->max_aspect_ratio_h;
	vftr_pd.min_aspect_ratio = ((pd->min_aspect_ratio_w << VFTR_OSC_AR_FRACTIONAL_BIT) + (pd->min_aspect_ratio_h >> 1)) /
	                           pd->min_aspect_ratio_h;
	vftr_pd.min_sz.width = pd->min_size * length / 100;
	vftr_pd.min_sz.height = pd->min_size * length / 100;
	vftr_pd.max_sz.width = pd->max_size * length / 100;
	vftr_pd.max_sz.height = pd->max_size * length / 100;
	vftr_pd.obj_life_th = pd->obj_life_th;

	ret = AVFTR_PD_setParam(idx, &vftr_pd);
	if (ret != 0) {
		return -1;
	}

	return 0;
}

static INT32 cfg_apply_ld(UINT32 dev_idx, UINT32 chn_idx, UINT32 width, UINT32 height, CFG_IVA_LD_S *ld,
                          INT32 enable_change)
{
	INT32 ret = 0;
	VFTR_LD_PARAM_S vftr_ld = { 0 };
	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, 0);
	/*if enable changed,enable or disable LD*/
	if (enable_change && ld->enabled) {
		ret = AVFTR_LD_enable(idx);
		if (ret != 0) {
			return -1;
		}

		MPI_RECT_S rect;
		ret = getRoi(idx, &rect);
		if (ret != 0) {
			SYS_TRACE("Get Roi failed !\n");
			return ret;
		}
		MPI_RECT_POINT_S roi = {
			.sx = rect.x, .sy = rect.y, .ex = (rect.x + rect.width - 1), .ey = (rect.y + rect.height - 1)
		};

		ret = AVFTR_LD_regMpiInfo(idx, &roi);
		if (ret != 0) {
			return ret;
		}

	} else if (enable_change && !ld->enabled) {
		ret = AVFTR_LD_disable(idx);
		if (ret != 0) {
			return -1;
		}
		ret = AVFTR_LD_releaseMpiInfo(idx);
		if (ret != 0) {
			return ret;
		}
	}
	ret = AVFTR_LD_getParam(idx, &vftr_ld);
	if (ret != 0) {
		return -1;
	}

	/* sen_th             : min=12 mid=62 max=112 */
	/* alarm_latency      : fixed=5               */
	/* alarm_latency_cycle: min=1  mid=3  max=5   */
	/* det_period         : min=2  mid=6  max=10  */

	vftr_ld.sen_th = 112 - ld->sensitivity;

	vftr_ld.alarm_supr = 7 - ((ld->sensitivity + 10) / 20);
	vftr_ld.alarm_latency = 5;
	vftr_ld.alarm_latency_cycle = ((ld->sensitivity + 12) / 25) + 1;
	vftr_ld.det_period = (10 - (ld->sensitivity * 8 / 100));
	vftr_ld.trig_cond = ld->trig_cond;

	vftr_ld.roi.sx = ld->det_region.start_x * (width - 1) / 100;
	vftr_ld.roi.sy = ld->det_region.start_y * (height - 1) / 100;
	vftr_ld.roi.ex = ld->det_region.end_x * (width - 1) / 100;
	vftr_ld.roi.ey = ld->det_region.end_y * (height - 1) / 100;

	ret = AVFTR_LD_setParam(idx, &vftr_ld);
	if (ret != 0) {
		return -1;
	}

	ret = AVFTR_LD_regMpiInfo(idx, &vftr_ld.roi);
	if (ret != 0) {
		return ret;
	}

	return 0;
}

static INT32 cfg_apply_od(UINT32 dev_idx, UINT32 chn_idx, CFG_IVA_OD_S *od, INT32 enable_change, UINT8 fps)
{
	INT32 ret = 0;
	VIDEO_FTR_OD_PARAM_S vftr_od = { 0 };
	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, 0);
	//printf("%s %d enc:%d en:%d id :0x%x libid:0x%x\n", __func__, __LINE__, enable_change, od->enabled, idx.value, param.idx.value);

	/*if enable changed,enable or disable OD*/
	if (enable_change && od->enabled) {
		ret = VIDEO_FTR_enableOd(idx);
		if (ret != 0) {
			return -1;
		}
	} else if (enable_change && !od->enabled) {
		ret = VIDEO_FTR_disableOd(idx);
		if (ret != 0) {
			return -1;
		}
	}

	ret = VIDEO_FTR_getOdParam(idx, &vftr_od);
	if (ret != 0) {
		return -1;
	}

#define VFTR_OD_MAX_DETECTION_SEC 3
#define VFTR_OD_MIN_DETECTION_SEC 0
#define VFTR_OD_DETECTION_SECOND 0.9 //Recommend Default
#define VFTR_OD_DETECTION_PERCENT 70 //Recommend Default

#define VFTR_OD_MAX_TRACK_REFINE_SEC 2
#define VFTR_OD_MIN_TRACK_REFINE_SEC 0
#define VFTR_OD_TRACK_REFINE_SECOND 0.267 //Recommend Default
#define VFTR_OD_TRACK_REFINE_PERCENT 86 //Recommend Default
//1-VFTR_OD_TRACK_REFINE_SECOND/(VFTR_OD_MAX_TRACK_REFINE_SEC-VFTR_OD_MIN_TRACK_REFINE_SEC)

	int od_qual_t = MPI_IVA_OD_MAX_QUA + 1 - (fps*(100-od->od_qual)
	              *(VFTR_OD_MAX_DETECTION_SEC-VFTR_OD_MIN_DETECTION_SEC))/100;

	if (od_qual_t < MPI_IVA_OD_MIN_QUA) {
		od_qual_t = MPI_IVA_OD_MIN_QUA;
	} else if (od_qual_t > MPI_IVA_OD_MAX_QUA) {
		od_qual_t = MPI_IVA_OD_MAX_QUA;
	}

	int od_track_refine_t = MPI_IVA_OD_MAX_TRACK_REFINE + 1 - (fps*(100-od->od_track_refine)
	                      *(VFTR_OD_MAX_TRACK_REFINE_SEC-VFTR_OD_MIN_TRACK_REFINE_SEC))/100;

	if (od_track_refine_t < MPI_IVA_OD_MIN_TRACK_REFINE) {
		od_track_refine_t = MPI_IVA_OD_MIN_TRACK_REFINE;
	} else if (od_track_refine_t > MPI_IVA_OD_MAX_TRACK_REFINE) {
		od_track_refine_t = MPI_IVA_OD_MAX_TRACK_REFINE;
	}

	vftr_od.od_param.od_qual = od_qual_t;
	vftr_od.od_param.od_track_refine = od_track_refine_t;
	vftr_od.od_param.od_size_th = od->od_size_th * MPI_IVA_OD_MAX_OBJ_SIZE / 100;
	vftr_od.od_param.od_sen = od->od_sen * MPI_IVA_OD_MAX_SEN / 100;
	vftr_od.od_param.en_stop_det = od->en_stop_det;
	vftr_od.en_crop_outside_obj = od->en_crop_outside_obj;

	ret = VIDEO_FTR_setOdParam(idx, &vftr_od);

	return 0;
}

static INT32 cfg_apply_rms(UINT32 dev_idx, UINT32 chn_idx, CFG_IVA_RMS_S *rms, INT32 enable_change)
{
	INT32 ret = 0;
	VIDEO_FTR_RMS_PARAM_S vftr_rms = { 0 };
	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, 0);
	/*if enable changed,enable or disable OD*/
	if (enable_change && rms->enabled) {
		ret = VIDEO_FTR_enableRms(idx);
		if (ret != 0) {
			return -1;
		}
	} else if (enable_change && !rms->enabled) {
		ret = VIDEO_FTR_disableRms(idx);
		if (ret != 0) {
			return -1;
		}
	}

	ret = VIDEO_FTR_getRmsParam(idx, &vftr_rms);
	if (ret != 0) {
		return -1;
	}

	vftr_rms.sen = rms->sensitivity * MPI_IVA_RMS_MAX_SEN / 100;
	vftr_rms.split_x = rms->split_x;
	vftr_rms.split_y = rms->split_y;

	ret = VIDEO_FTR_setRmsParam(idx, &vftr_rms);
	if (ret != 0) {
		return -1;
	}

	return 0;
}

static INT32 cfg_apply_ef(UINT32 dev_idx, UINT32 chn_idx, UINT32 width, UINT32 height, CFG_IVA_EF_S *ef,
                          CFG_IVA_EF_S *ef_old, INT32 enable_change)
{
	INT32 ret = 0;
	INT32 i = 0;
	AVFTR_EF_VL_ATTR_S ef_line = { 0 };
	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, 0);
	/*if enable changed,enable or disable EF*/
	if (enable_change && ef->enabled) {
		ret = AVFTR_EF_enable(idx);
		if (ret != 0) {
			return ret;
		}
	} else if (enable_change && !ef->enabled) {
		ret = AVFTR_EF_disable(idx);
		if (ret != 0) {
			return ret;
		}
	}

	/*remove old ROI*/
	for (i = 0; i < ef_old->line_cnt; i++) {
		ret = AVFTR_EF_rmVl(idx, ef_old->line_list[i].id);
		if (ret != 0) {
			return ret;
		}
	}

	/*Apply new ROI*/
	for (i = 0; i < ef->line_cnt; i++) {
		/*Convert from percent to actually point*/
		ef_line.line.sx = MIN(MAX((width - 1) * ef->line_list[i].start_x / 100, 0), width - 1);
		ef_line.line.sy = MIN(MAX((height - 1) * ef->line_list[i].start_y / 100, 0), height - 1);
		ef_line.line.ex = MIN(MAX((width - 1) * ef->line_list[i].end_x / 100, 0), width - 1);
		ef_line.line.ey = MIN(MAX((height - 1) * ef->line_list[i].end_y / 100, 0), height - 1);
		ef_line.obj_size_min.width = width * ef->line_list[i].obj_min_w / 100;
		ef_line.obj_size_min.height = height * ef->line_list[i].obj_min_h / 100;
		ef_line.obj_size_max.width = width * ef->line_list[i].obj_max_w / 100;
		ef_line.obj_size_max.height = height * ef->line_list[i].obj_max_h / 100;
		ef_line.obj_area = ef->line_list[i].obj_area * width * height / 100;
		ef_line.obj_v_th = ef->line_list[i].obj_v_th * VFTR_EF_MAX_THR_V_OBJ / 100;
		ef_line.obj_life_th = ef->line_list[i].obj_life_th;
		switch (ef->line_list[i].mode) {
		case CFG_IVA_EF_MODE_DIR_NONE:
			ef_line.mode = VFTR_EF_DIR_NONE;
			break;
		case CFG_IVA_EF_MODE_DIR_POS:
			ef_line.mode = VFTR_EF_DIR_POS;
			break;
		case CFG_IVA_EF_MODE_DIR_NEG:
			ef_line.mode = VFTR_EF_DIR_NEG;
			break;
		case CFG_IVA_EF_MODE_DIR_BOTH:
			ef_line.mode = VFTR_EF_DIR_BOTH;
			break;
		default:
			ef_line.mode = VFTR_EF_DIR_NONE;
			printf("Unkown IVA EF mode\n");
			break;
		}
		ret = AVFTR_EF_addVl(idx, &ef_line);
		if (ret != 0) {
			return ret;
		}
		ef->line_list[i].id = ef_line.id;
	}

	ret = AVFTR_EF_checkParam(idx);
	if (ret != 0) {
		return ret;
	}

	return 0;
}

static INT32 cfg_apply_vdbg(UINT32 dev_idx, UINT32 chn_idx, CFG_VDBG_S *vdbg, INT32 enable_change)
{
	INT32 ret = 0;
	static INT32 vdbg_init = 0;

	if (vdbg_init == 0) {
		AVFTR_VDBG_debugInit();
		vdbg_init = 1;
	}

	/*if enable changed,enable or disable OD*/
	if (enable_change && vdbg->enabled) {
		ret = VIDEO_FTR_enableVdbg();
		if (ret != 0) {
			return -1;
		}
	} else if (enable_change && !vdbg->enabled) {
		ret = VIDEO_FTR_disableVdbg();
		if (ret != 0) {
			return -1;
		}
	}

	ret = VIDEO_FTR_setVdbg(vdbg->ctx);
	if (ret != 0) {
		return -1;
	}

	return 0;
}

static INT32 cfg_apply_shd(UINT32 dev_idx, UINT32 chn_idx, UINT32 width, UINT32 height, CFG_IVA_SHD_S *shd,
                          const CFG_IVA_SHD_S *shd_old, INT32 enable_change)
{
	INT32 ret = 0, i;
	VFTR_SHD_PARAM_S vftr_shd = { 0 };
	VFTR_SHD_LONGTERM_LIST_S lt_list = { 0 };
	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, 0);

	if (enable_change || shd->sensitivity != shd_old->sensitivity ||
	shd->quality != shd_old->quality ||
	shd->obj_life_th != shd_old->obj_life_th ||
	shd->longterm_life_th != shd_old->longterm_life_th ||
	shd->instance_duration != shd_old->instance_duration ||
	shd->shaking_update_duration != shd_old->shaking_update_duration ||
	shd->longterm_dec_period != shd_old->longterm_dec_period) {
		vftr_shd.en = shd->enabled;
		vftr_shd.sensitivity = shd->sensitivity;
		vftr_shd.quality = shd->quality;
		vftr_shd.obj_life_th = shd->obj_life_th;
		vftr_shd.longterm_life_th = shd->longterm_life_th;
		vftr_shd.instance_duration = shd->instance_duration;
		vftr_shd.shaking_update_duration = shd->shaking_update_duration;
		vftr_shd.longterm_dec_period = shd->longterm_dec_period;
		ret = AVFTR_SHD_setParam(idx, &vftr_shd);
		if (ret != 0) {
			return -1;
		}
	}
	int set_lt_list = (shd->longterm_num != shd_old->longterm_num) ||
	                  memcmp(shd->longterm_list, shd_old->longterm_list, sizeof(AGTX_IVA_SHD_LT_LIST_S)*shd->longterm_num);

	if (set_lt_list != 0) {
		lt_list.num = shd->longterm_num;
		for (i = 0; i < lt_list.num; i++) {
			lt_list.item[i].rgn.sx = shd->longterm_list[i].start_x * width / 100;
			lt_list.item[i].rgn.sy = shd->longterm_list[i].start_y * height / 100;
			lt_list.item[i].rgn.ex = shd->longterm_list[i].end_x * width / 100;
			lt_list.item[i].rgn.ey = shd->longterm_list[i].end_y * height / 100;
		}
		ret = AVFTR_SHD_setUsrList(idx, &lt_list);
		if (ret != 0) {
			return -1;
		}
	}

	return 0;
}

static inline INT32 _set_eaif(MPI_WIN idx, CFG_IVA_EAIF_S *eaif, AVFTR_EAIF_PARAM_S *vftr_eaif)
{
	int ret = 0;
	const char *avmain_eaif_face_file_dir = EAIF_FACE_DIR_NAME;
	const char *avmain_eaif_face_face_db = EAIF_FACE_BIN_NAME;

	int tar_dev = (eaif->target_idx) & 0xff;
	int tar_chn = (eaif->target_idx >> 8) & 0xff;
	int tar_win = (eaif->target_idx >> 16) & 0xff;
	vftr_eaif->target_idx = MPI_VIDEO_WIN(tar_dev, tar_chn, tar_win);
	vftr_eaif->api = eaif->api;
	vftr_eaif->data_fmt = eaif->data_fmt;
	vftr_eaif->obj_life_th = eaif->obj_life_th;
	vftr_eaif->detection_period = eaif->detection_period;
	vftr_eaif->inf_with_obj_list = eaif->inf_with_obj_list;
	vftr_eaif->pos_stop_count_th = eaif->pos_stop_count_th;
	vftr_eaif->pos_classify_period = eaif->pos_classify_period;
	vftr_eaif->neg_classify_period = eaif->neg_classify_period;
	vftr_eaif->obj_exist_classify_period = eaif->obj_exist_classify_period;
	vftr_eaif->snapshot_width = MAX(eaif->snapshot_width, 0);
	vftr_eaif->snapshot_height = MAX(eaif->snapshot_height, 0);
	vftr_eaif->inf_utils.cmd = eaif->inf_cmd;
	vftr_eaif->inf_utils.roi.sx = eaif->facereco_roi_sx;
	vftr_eaif->inf_utils.roi.sy = eaif->facereco_roi_sy;
	vftr_eaif->inf_utils.roi.ex = eaif->facereco_roi_ex;
	vftr_eaif->inf_utils.roi.ey = eaif->facereco_roi_ey;
	strncpy(vftr_eaif->inf_utils.dir, (const char *)avmain_eaif_face_file_dir, EAIF_URL_CHAR_LEN);
	strncpy(vftr_eaif->inf_utils.face_db, (const char *)avmain_eaif_face_face_db, EAIF_CHAR_LEN);
	strncpy(vftr_eaif->inf_utils.face_name, (const char *)eaif->face_name, EAIF_CHAR_LEN);
	strncpy(vftr_eaif->url, (const char *)eaif->url, EAIF_URL_CHAR_LEN);
	strncpy(vftr_eaif->classify_model, (const char *)eaif->classify_model, EAIF_MODEL_LEN);
	strncpy(vftr_eaif->classify_cv_model, (const char *)eaif->classify_cv_model, EAIF_MODEL_LEN);
	strncpy(vftr_eaif->detect_model, (const char *)eaif->detect_model, EAIF_MODEL_LEN);
	strncpy(vftr_eaif->face_reco_model, (const char *)eaif->face_reco_model, EAIF_MODEL_LEN);
	strncpy(vftr_eaif->face_detect_model, (const char *)eaif->face_detect_model, EAIF_MODEL_LEN);
	strncpy(vftr_eaif->human_classify_model, (const char *)eaif->human_classify_model, EAIF_MODEL_LEN);

	ret = AVFTR_EAIF_setParam(idx, vftr_eaif);
	return ret;
}

static INT32 cfg_apply_eaif(UINT32 dev_idx, UINT32 chn_idx, CFG_IVA_EAIF_S *eaif,
                          INT32 enable_change)
{
	INT32 ret = 0;
	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, 0);
	AVFTR_EAIF_PARAM_S vftr_eaif = { 0 };

	ret = AVFTR_EAIF_getParam(idx, &vftr_eaif);
	if (ret != 0) {
		return ret;
	}

	ret = _set_eaif(idx, eaif, &vftr_eaif);
	if (ret != 0) {
		return ret;
	}

	/*if enable changed,enable or disable PD*/
	if (enable_change && eaif->enabled) {
		ret = AVFTR_EAIF_enable(idx);
		if (ret != 0) {
			return ret;
		}
	} else if (enable_change && !eaif->enabled) {
		ret = AVFTR_EAIF_disable(idx);
		if (ret != 0) {
			return ret;
		}
	}

	return 0;
}

static INT32 cfg_apply_pfm(UINT32 dev_idx, UINT32 chn_idx, UINT32 width, UINT32 height,
                           CFG_IVA_PFM_S *pfm, INT32 enable_change)
{
	INT32 ret = 0;
	INT32 temp = 0;
	INT32 min_map = 0;
	INT32 max_map = 0;
	INT32 start_x, start_y, end_x, end_y;
	VIDEO_FTR_PFM_PARAM_S vftr_pfm = { { 0 } };
	PFM_PARAM_S *pfm_param = &vftr_pfm.pfm_param;
	VIDEO_PFM_SCHEDULE_S *schedule = &vftr_pfm.schedule;
	INT32 i;

	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, 0);
	/*if enable changed,enable or disable TD*/
	if (enable_change && pfm->enabled) {
		ret = VIDEO_FTR_enablePfm(idx);
		if (ret != 0) {
			return -1;
		}
	} else if (enable_change && !pfm->enabled) {
		ret = VIDEO_FTR_disablePfm(idx);
		if (ret != 0) {
			return -1;
		}
	}

	ret = VIDEO_FTR_getPfmParam(idx, &vftr_pfm);
	if (ret != 0) {
		return -1;
	}

	/*Mapping to useful sensitivity range*/
	min_map = 16;
	max_map = 216;
	temp = ((pfm->sensitivity * (max_map - min_map) / 100) + min_map);
	pfm_param->sensitivity = (temp > PFM_SENSITIVITY_MAX) ? PFM_SENSITIVITY_MAX : temp;
	//temp = ((pfm->endurance * (max_map - min_map) / 100) + min_map);
	pfm_param->endurance = pfm->endurance;
	start_x = MIN(pfm->roi.start_x, 99);
	start_y = MIN(pfm->roi.start_y, 99);
	end_x = MAX(pfm->roi.end_x, 1);
	end_y = MAX(pfm->roi.end_y, 1);
	pfm_param->roi.sx = (((width * start_x / 100) / 16) * 16);
	pfm_param->roi.sy = (((height * start_y / 100) / 16) * 16);
	pfm_param->roi.ex = (((width * end_x / 100) / 16) * 16) - 1;
	pfm_param->roi.ey = (((height * end_y / 100) / 16) * 16) - 1;
	schedule->time_num = pfm->time_number;
	schedule->regisBg_feed_interval = pfm->regis_to_feeding_interval;
	for (i = 0; i<schedule->time_num; i++) {
		schedule->times[i] = pfm->schedule[i];
	}

	ret = VIDEO_FTR_setPfmParam(idx, &vftr_pfm);
	if (ret != 0) {
		return -1;
	}

	if (pfm->register_scene) {
		VIDEO_FTR_resetPfmData(idx, pfm->register_scene);
	}

	return 0;
}

static INT32 cfg_apply_bm(UINT32 dev_idx, UINT32 chn_idx, const AGTX_WINDOW_PARAM_S *win_attr,
                           CFG_IVA_BM_S *bm, INT32 enable_change)
{
	INT32 ret = 0;
	INT32 min_map = 0;
	INT32 max_map = 0;
	INT32 width = win_attr->pos_width;
	INT32 height = win_attr->pos_height;
	INT32 update_fps = win_attr->update_fps;
	INT32 start_x, start_y, end_x, end_y;
	INT32 sensitivity, quality;
	const char *iva_data_path = g_iva_data_path;
	VIDEO_FTR_BM_PARAM_S old_vftr_bm, vftr_bm = { { 0 } };
	FD_PARAM_S *fd_param = &vftr_bm.fd_param;

	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, 0);
	/*if enable changed,enable or disable TD*/
	if (enable_change && bm->enabled) {
		ret = VIDEO_FTR_enableBm(idx);
		if (ret != 0) {
			return -1;
		}
	} else if (enable_change && !bm->enabled) {
		ret = VIDEO_FTR_disableBm(idx);
		if (ret != 0) {
			return -1;
		}
	}

	ret = VIDEO_FTR_getBmParam(idx, &vftr_bm);
	if (ret != 0) {
		return -1;
	}
	old_vftr_bm = vftr_bm;

	/*Mapping to useful sensitivity range*/
	min_map = 0;
	max_map = 255;
	sensitivity = ((bm->sensitivity * (max_map - min_map) / 100) + min_map);
	quality = bm->quality * 255 / 100;

	/* mapping */
	/* sensitivity 0 - 255  */
	/* quality     0 - 255  */
	/* time_buffer >=16 convert from seconds to number of frames  */
	/* sensitivity 0 - 255  */
	/* roi     [resolution] */

	fd_param->sensitivity = (sensitivity > 255) ? 255 : sensitivity;
	fd_param->quality = (quality > 255) ? 255 : quality;
	fd_param->time_buffer = MAX(bm->time_buffer * update_fps, FD_TIME_BUFFER_SIZE_MIN);
	fd_param->suppression = bm->suppression * update_fps;
	fd_param->boundary_thickness = (bm->boundary_thickness > 255) ? 255 : (bm->boundary_thickness < 1) ? 1 : bm->boundary_thickness;
	fd_param->event_type = FD_OBJECT_MONITOR; // assign event type
	start_x = MIN(bm->roi.start_x, 99);
	start_y = MIN(bm->roi.start_y, 99);
	end_x = MAX(bm->roi.end_x, 1);
	end_y = MAX(bm->roi.end_y, 1);
	fd_param->roi.x = (width * start_x / 100);
	fd_param->roi.y = (height * start_y / 100);
	int ex = (width * end_x / 100) - 1;
	int ey = (height * end_y / 100) - 1;
	fd_param->roi.width = ex - fd_param->roi.x + 1;
	fd_param->roi.height = ey - fd_param->roi.y + 1;

	if (memcmp(&old_vftr_bm, &vftr_bm, sizeof(VIDEO_FTR_BM_PARAM_S))) {
		ret = VIDEO_FTR_setBmParam(idx, &vftr_bm);
		if (ret != 0) {
			return -1;
		}
	}

	if (bm->reset) {
		ret = VIDEO_FTR_resetBmData(idx);
		if (ret) {
			return -1;
		}
	}

	if ((VIDEO_BM_DATA_CTRL_E)bm->data_ctrl != FD_DATA_NONE) {
		ret = VIDEO_FTR_ctrlBmData(idx, iva_data_path, bm->data_ctrl);
		if (ret) {
			return -1;
		}
	}

	return 0;
}

static INT32 cfg_apply_dk(UINT32 dev_idx, UINT32 chn_idx, const AGTX_WINDOW_PARAM_S *win_attr, CFG_IVA_DK_S *dk,
                          INT32 enable_change)
{
	INT32 ret = 0;
	INT32 width = win_attr->pos_width;
	INT32 height = win_attr->pos_height;
	INT32 start_x, start_y, end_x, end_y;
	AVFTR_DK_PARAM_S vftr_dk = { { 0 } };
	VFTR_DK_PARAM_S *dk_param = &vftr_dk.dk_param;

	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, 0);

	/*if enable changed,enable or disable DK*/
	if (enable_change && dk->enabled) {
		ret = AVFTR_DK_enable(idx);
		if (ret != 0) {
			return ret;
		}
	} else if (enable_change && !dk->enabled) {
		ret = AVFTR_DK_disable(idx);
		if (ret != 0) {
			return ret;
		}
	}

	ret = AVFTR_DK_getParam(idx, &vftr_dk);
	if (ret != 0) {
		return ret;
	}

	/*Avoid invalid value*/
	dk_param->obj_life_th = MAX(dk->obj_life_th, 0);
	dk_param->loiter_period_th = MAX(dk->loiter_period_th, 10);

	dk_param->overlap_ratio_th = (MAX(dk->overlap_ratio_th, 0) << VFTR_DK_OVERLAP_FRACTION) / 100;

	start_x = MIN(dk->roi.start_x, 99);
	start_y = MIN(dk->roi.start_y, 99);
	end_x = MAX(dk->roi.end_x, 1);
	end_y = MAX(dk->roi.end_y, 1);
	dk_param->roi_pts.sx = (width * start_x / 100);
	dk_param->roi_pts.sy = (height * start_y / 100);
	dk_param->roi_pts.ex = (width * end_x / 100);
	dk_param->roi_pts.ey = (height * end_y / 100);

	ret = AVFTR_DK_setParam(idx, &vftr_dk);
	if (ret != 0) {
		return ret;
	}

	return 0;
}

static INT32 cfg_apply_fld(UINT32 dev_idx, UINT32 chn_idx, CFG_IVA_FLD_S *fld, INT32 enable_change)
{
	INT32 ret = 0;
	AVFTR_FLD_PARAM_S vftr_fld = { { 0 } };
	VFTR_FLD_PARAM_S *fld_param = &vftr_fld.fld_param;

	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, 0);
	/*if enable changed,enable or disable FLD*/
	if (enable_change && fld->enabled) {
		ret = AVFTR_FLD_enable(idx);
		if (ret != 0) {
			return ret;
		}

	} else if (enable_change && !fld->enabled) {
		ret = AVFTR_FLD_disable(idx);
		if (ret != 0) {
			return ret;
		}
	}

	ret = AVFTR_FLD_getParam(idx, &vftr_fld);
	if (ret != 0) {
		return ret;
	}

	/*Avoid invalid value*/
	fld_param->obj_life_th = MAX(fld->obj_life_th, 0);
	fld_param->obj_falling_mv_th = MAX(fld->obj_falling_mv_th, 0);
	fld_param->obj_stop_mv_th = MAX(fld->obj_stop_mv_th, 0);
	fld_param->falling_period_th = MAX(fld->falling_period_th, 10);
	fld_param->down_period_th = MAX(fld->down_period_th, 10);
	fld_param->fallen_period_th = MAX(fld->fallen_period_th, 10);
	fld_param->demo_level = MAX(fld->demo_level, 0);

	fld_param->obj_high_ratio_th = (MAX(fld->obj_high_ratio_th, 0) << VFTR_FLD_FRACTION) / 100;

	ret = AVFTR_FLD_setParam(idx, &vftr_fld);
	if (ret != 0) {
		return ret;
	}

	return 0;
}

static INT32 cfg_apply_lsd(UINT32 dev_idx, CFG_IAA_LSD_S *lsd,
                          INT32 enable_change)
{
	INT32 ret = 0;
	AFTR_SD_PARAM_S aftr_lsd = { 0 };
	MPI_DEV idx = MPI_VIDEO_DEV(dev_idx);
	/*if enable changed,enable or disable LSD*/
	if (enable_change && lsd->enabled) {
		ret = AVFTR_SD_enable(idx);
		if (ret != 0) {
			return -1;
		}
		printf("[CONFIG] success to enable SD\n");
	} else if (enable_change && !lsd->enabled) {
		ret = AVFTR_SD_disable(idx);
		if (ret != 0) {
			return -1;
		}
		printf("[CONFIG] success to disable SD\n");
	}
	ret = AVFTR_SD_getParam(idx, &aftr_lsd);
	if (ret != 0) {
		return -1;
	}
	printf("[CONFIG] success to getParam SD\n");

	aftr_lsd.volume = lsd->volume;
	aftr_lsd.duration = lsd->duration;
	aftr_lsd.suppression = lsd->suppression;

	ret = AVFTR_SD_setParam(idx, &aftr_lsd);
	if (ret != 0) {
		return -1;
	}
	printf("[CONFIG] success to setParam SD\n");

	return 0;
}

static INT32 cfg_apply_osd_pm(UINT32 dev_idx, UINT32 chn_idx, UINT16 width, UINT16 height, AGTX_OSD_PM_PARAM_S *osd_pm,
                              AGTX_OSD_PM_PARAM_S *osd_pm_old)
{
	INT32 ret = 0;
	INT32 i;
	AGTX_OSD_PM_S *new_osd_param = NULL;
	VIDEO_OSD_PM_PARAM_S osd_param = { 0 };
	MPI_ECHN idx = MPI_ENC_CHN(chn_idx);

	/* Enable Privacy Mask */
	for (i = 0; i < VIDEO_OSD_MAX_PM_HANDLE; i++) {
		new_osd_param = &osd_pm->param[i];

		if (osd_pm_old->param[i].enabled || new_osd_param->enabled) {
			osd_param.enabled = new_osd_param->enabled;
			if (new_osd_param->enabled) {
				osd_param.alpha = new_osd_param->alpha;
				osd_param.color = VIDEO_OSD_getAyuvColor(new_osd_param->color);
				osd_param.start.x = new_osd_param->start_x;
				osd_param.start.y = new_osd_param->start_y;
				osd_param.end.x = new_osd_param->end_x;
				osd_param.end.y = new_osd_param->end_y;
			} else { /* reset disabled param to conf */
				new_osd_param->enabled = 0;
				new_osd_param->color = 0;
				new_osd_param->alpha = 7;
				new_osd_param->start_x = 0;
				new_osd_param->start_y = 0;
				new_osd_param->end_x = 1;
				new_osd_param->end_y = 1;
				osd_param.enabled = 0;
				osd_param.alpha = 7;
				osd_param.color = VIDEO_OSD_getAyuvColor(new_osd_param->color);
				osd_param.start.x = 0;
				osd_param.start.y = 0;
				osd_param.end.x = 16;
				osd_param.end.y = 16;
			}
			ret = VIDEO_OSD_setPmParam(idx, i, &osd_param);

			if (ret) {
				return -1;
			}
		}
	}
	return 0;
}

static INT32 cfg_close_ef(MPI_WIN idx, CFG_IVA_EF_S *ef)
{
	INT32 ret = 0;
	INT32 i = 0;

	/*remove old ROI*/
	for (i = 0; i < ef->line_cnt; i++) {
		ret = AVFTR_EF_rmVl(idx, ef->line_list[i].id);
		if (ret != 0) {
			return ret;
		}
	}

	ret = AVFTR_EF_disable(idx);
	if (ret != 0) {
		return ret;
	}

	return 0;
}

static INT32 cfg_register_all_iva(MPI_WIN idx)
{
	INT32 ret = 0;

	ret = AVFTR_DK_addInstance(idx);
	if (ret != 0) {
		return ret;
	}

	ret = AVFTR_MD_addInstance(idx);
	if (ret != 0) {
		return ret;
	}

	ret = AVFTR_TD_addInstance(idx);
	if (ret != 0) {
		return ret;
	}

	ret = AVFTR_LD_addInstance(idx);
	if (ret != 0) {
		return ret;
	}

	ret = AVFTR_FLD_addInstance(idx);
	if (ret != 0) {
		return ret;
	}

	ret = AVFTR_EF_addInstance(idx);
	if (ret != 0) {
		return ret;
	}

	ret = AVFTR_EAIF_addInstance(idx);
	if (ret != 0) {
		return ret;
	}

	ret = AVFTR_AROI_addInstance(idx);
	if (ret != 0) {
		return ret;
	}

	ret = AVFTR_PD_addInstance(idx);
	if (ret != 0) {
		return ret;
	}

	ret = AVFTR_SHD_addInstance(idx);
	if (ret != 0) {
		return ret;
	}

	return 0;
}

static INT32 cfg_start_all_iva(UINT32 dev_idx, CFG_VIN_S *vin)
{
	UINT32 chn_idx = 0; //iva support one channel only
	UINT32 win_idx = 0; //iva support one window only
	CFG_VCHN_S *vchn = &vin->cfg_vchn[chn_idx];
	INT32 ret = 0;
	CFG_IVA_MD_S md = { 0 };
	CFG_IVA_EF_S ef = { 0 };
	CFG_IVA_AROI_S aroi = { 0 };
	CFG_IVA_SHD_S shd = { 0 };
	CFG_PTZ_S ptz = {0};
	AGTX_WINDOW_PARAM_S *win_attr = &vchn->lyt_cfg.window_array[win_idx];
	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, win_idx);

	//register all iva before apply
	ret = cfg_register_all_iva(idx);
	if (ret != 0) {
		return ret;
	}

	ret = cfg_apply_td(dev_idx, chn_idx, &vchn->td, 1);
	if (ret < 0) {
		return ret;
	}

	/*put empty md config that don't remove any ROI*/
	ret = cfg_apply_md(dev_idx, chn_idx, win_attr->pos_width, win_attr->pos_height, &vchn->md, &md, 1);
	if (ret < 0) {
		return ret;
	}

	ret = cfg_apply_aroi(dev_idx, chn_idx, win_attr->pos_width, win_attr->pos_height, &vchn->aroi, &aroi, 1);
	if (ret < 0) {
		return ret;
	}

	ret = cfg_apply_pd(dev_idx, chn_idx, win_attr->pos_width, win_attr->pos_height, &vchn->pd, 1);
	if (ret < 0) {
		return ret;
	}

	ret = cfg_apply_eaif(dev_idx, chn_idx, &vchn->eaif, 1);
	if (ret < 0) {
		return ret;
	}

	ret = cfg_apply_od(dev_idx, chn_idx, &vchn->od, 1, vchn->fps);
	if (ret < 0) {
		return ret;
	}

	ret = cfg_apply_rms(dev_idx, chn_idx, &vchn->rms, 1);
	if (ret < 0) {
		return ret;
	}

	ret = cfg_apply_ld(dev_idx, chn_idx, win_attr->pos_width, win_attr->pos_height, &vchn->ld, 1);
	if (ret < 0) {
		return ret;
	}

	ret = cfg_apply_ef(dev_idx, chn_idx, win_attr->pos_width, win_attr->pos_height, &vchn->ef, &ef, 1);
	if (ret < 0) {
		return ret;
	}

	ret = cfg_apply_shd(dev_idx, chn_idx, win_attr->pos_width, win_attr->pos_height, &vchn->shd, &shd, 1);
	if (ret < 0) {
		return ret;
	}

	ret = cfg_apply_pfm(dev_idx, chn_idx, win_attr->pos_width, win_attr->pos_height, &vchn->pfm, 1);
	if (ret < 0) {
		return ret;
	}

	ret = cfg_apply_bm(dev_idx, chn_idx, win_attr, &vchn->bm, 1);
	if (ret < 0) {
		return ret;
	}

	ret = cfg_apply_dk(dev_idx, chn_idx, win_attr, &vchn->dk, 1);
	if (ret < 0) {
		return ret;
	}

	ret = cfg_apply_fld(dev_idx, chn_idx, &vchn->fld, 1);
	if (ret < 0) {
		return ret;
	}

	ret = cfg_apply_vdbg(dev_idx, chn_idx, &vchn->vdbg, 1);
	if (ret < 0) {
		return ret;
	}

	ret = cfg_apply_ptz(dev_idx, &vin->ptz, &ptz, vin, 1);
	if (ret < 0) {
		return ret;
	}

	ret = AVFTR_runIva(idx);
	if (ret < 0) {
		SYS_TRACE("Cannot create VFTR_runIva thread for WIN:%x\n", idx.value);
		return ret;
	}

	return 0;
}

static INT32 cfg_remove_all_iva(MPI_WIN idx)
{
	INT32 ret = 0;

	ret = AVFTR_DK_deleteInstance(idx);
	if (ret != 0) {
		return ret;
	}

	ret = AVFTR_MD_deleteInstance(idx);
	if (ret != 0) {
		return ret;
	}

	ret = AVFTR_TD_deleteInstance(idx);
	if (ret != 0) {
		return ret;
	}

	ret = AVFTR_LD_deleteInstance(idx);
	if (ret != 0) {
		return ret;
	}

	ret = AVFTR_FLD_deleteInstance(idx);
	if (ret != 0) {
		return ret;
	}

	ret = AVFTR_EF_deleteInstance(idx);
	if (ret != 0) {
		return ret;
	}

	ret = AVFTR_EAIF_deleteInstance(idx);
	if (ret != 0) {
		return ret;
	}

	ret = AVFTR_AROI_deleteInstance(idx);
	if (ret != 0) {
		return ret;
	}

	ret = AVFTR_SHD_deleteInstance(idx);
	if (ret != 0) {
		return ret;
	}

	ret = AVFTR_PD_deleteInstance(idx);
	if (ret != 0) {
		return ret;
	}

	return 0;
}

static INT32 cfg_close_all_iva(UINT32 dev_idx, CFG_VIN_S *vin)
{
	UINT32 chn_idx = 0; //iva support one channel only
	CFG_VCHN_S *vchn = &vin->cfg_vchn[chn_idx];
	INT32 ret = 0;
	MPI_WIN idx = MPI_VIDEO_WIN(dev_idx, chn_idx, 0);

	ret = AVFTR_TD_disable(idx);
	if (ret < 0) {
		return ret;
	}
	ret = AVFTR_TD_releaseMpiInfo(idx);
	if (ret < 0) {
		return ret;
	}

	ret = AVFTR_LD_disable(idx);
	if (ret < 0) {
		return ret;
	}
	ret = AVFTR_LD_releaseMpiInfo(idx);
	if (ret < 0) {
		return ret;
	}

	ret = cfg_close_md(idx, &vchn->md);
	if (ret < 0) {
		return ret;
	}

	ret = AVFTR_AROI_disable(idx);
	if (ret < 0) {
		return ret;
	}

	ret = AVFTR_PD_disable(idx);
	if (ret < 0) {
		return ret;
	}

	ret = AVFTR_EAIF_disable(idx);
	if (ret < 0) {
		return ret;
	}

	ret = VIDEO_FTR_disableOd(idx);
	if (ret < 0) {
		return ret;
	}

	ret = VIDEO_FTR_disableRms(idx);
	if (ret < 0) {
		return ret;
	}

	ret = VIDEO_FTR_disableBm(idx);
	if (ret < 0) {
		return ret;
	}

	ret = AVFTR_DK_disable(idx);
	if (ret < 0) {
		return ret;
	}

	ret = AVFTR_FLD_disable(idx);
	if (ret < 0) {
		return ret;
	}

	ret = cfg_close_ef(idx, &vchn->ef);
	if (ret < 0) {
		return ret;
	}

	ret = VIDEO_FTR_disablePfm(idx);
	if (ret < 0) {
		return ret;
	}

	ret = VIDEO_FTR_disableVdbg();
	if (ret < 0) {
		return ret;
	}

	ret = VIDEO_FTR_disablePtz();
	if (ret < 0) {
		return ret;
	}

	ret = cfg_remove_all_iva(idx);
	if (ret < 0) {
		return ret;
	}

	ret = AVFTR_exitIva(idx);
	if (ret < 0) {
		SYS_TRACE("Cannot exit VFTR_runIva thread for WIN:%x\n", idx.value);
	}

	return 0;
}

static INT32 cfg_register_all_iaa(MPI_DEV idx)
{
	INT32 ret = 0;

	ret = AVFTR_SD_addInstance(idx);
	if (ret != 0) {
		return ret;
	}

	return 0;
}

static INT32 cfg_start_all_iaa(UINT32 dev_idx, CFG_VIN_S *vin)
{
	INT32 ret = 0;
	MPI_DEV idx = MPI_VIDEO_DEV(dev_idx);

	//register all iva before apply
	ret = cfg_register_all_iaa(idx);
	if (ret != 0) {
		return ret;
	}

	ret = cfg_apply_lsd(dev_idx, &vin->lsd, 1);
	if (ret < 0) {
		return ret;
	}

	ret = AVFTR_runIaa(idx);
	if (ret < 0) {
		SYS_TRACE("Cannot create AFTR_runIaa thread for DEV:%x\n", idx.value);
		return ret;
	}

	return 0;
}

static INT32 cfg_remove_all_iaa(MPI_DEV idx)
{
	INT32 ret = 0;

	ret = AVFTR_SD_deleteInstance(idx);
	if (ret != 0) {
		return ret;
	}

	return 0;
}

static INT32 cfg_close_all_iaa(UINT32 dev_idx, CFG_VIN_S *vin)
{
	INT32 ret = 0;
	MPI_DEV idx = MPI_VIDEO_DEV(dev_idx);

	ret = AVFTR_SD_disable(idx);
	if (ret < 0) {
		return ret;
	}

	ret = cfg_remove_all_iaa(idx);
	if (ret < 0) {
		return ret;
	}

	ret = AVFTR_exitIaa(idx);
	if (ret < 0) {
		SYS_TRACE("Cannot exit AFTR_runIaa thread for DEV:%x\n", idx.value);
	}

	return 0;
}

static INT32 cfgInitSys()
{
	INT32 i = 0;
	INT32 ret = 0;
	CFG_ALL_S *cfg = &g_config;
	/*apply config to mpi sys*/
	ret = VIDEO_SYS_initSystem(cfg);
	if (ret < 0) {
		return ret;
	}
	for (i = 0; i < MAX_VIDEO_INPUT; i++) {
		// TODO: check if camera enable
		if (cfg->cfg_camera[i].cfg_vin.enable) {
			ret = VIDEO_DEV_createVideoDev(&cfg->cfg_camera[i].cfg_vin);
			if (ret < 0) {
				return ret;
			}
		} else {
			continue;
		}
	}

	return ret;
}

static INT32 cfgStartVideo()
{
	INT32 i = 0;
	INT32 j = 0;
	INT32 ret = 0;
	MPI_DEV dev_idx = MPI_VIDEO_DEV(0);
	CFG_ALL_S *cfg = &g_config;

	for (i = 0; i < MAX_VIDEO_INPUT; i++) {
		// TODO: check if camera enable
		if (cfg->cfg_camera[i].cfg_vin.enable) {
			ret = cfg_apply_vin(&cfg->cfg_camera[i].cfg_vin);
			if (ret < 0) {
				return ret;
			}
		} else {
			continue;
		}

		dev_idx.dev = cfg->cfg_camera[i].cfg_vin.dev_idx;

		ret = cfg_apply_vchn(dev_idx, cfg->cfg_camera[i].cfg_vin.cfg_vchn, 1);
		if (ret < 0) {
			return ret;
		}

		for (j = 0; j < MAX_VENC_STREAM; j++) {
			if (cfg->cfg_camera[i].cfg_venc[j].enable) {
				ret = cfg_apply_venc(&cfg->cfg_camera[i].cfg_venc[j], 1);
				if (ret < 0) {
					return ret;
				}
			}
		}

		if (cfg->cfg_camera[i].cfg_vin.enable) {
			ret = cfg_start_all_iaa(i, &cfg->cfg_camera[i].cfg_vin);
			if (ret < 0) {
				SYS_TRACE("Cannot Start all iaa.\n");
				return ret;
			}
		}

		ret = cfg_start_all_iva(i, &cfg->cfg_camera[i].cfg_vin);
		if (ret < 0) {
			SYS_TRACE("Cannot Start all iva.\n");
			return ret;
		}

		//set the default and GUI of parameters to DIP
		ret = VIDEO_DIP_setDipAllAttr(dev_idx);
		if (ret < 0) {
			return ret;
		}
		//		cfg_apply_enc_timestamp(cfg->cfg_camera[i].cfg_venc, &cfg->cfg_camera[i].cfg_timestamp);
	}

	return 0;
}

static INT32 cfgExitSys()
{
	INT32 i = 0;
	INT32 ret = 0;
	CFG_ALL_S *cfg = &g_config;
	for (i = 0; i < MAX_VIDEO_INPUT; i++) {
		// TODO: check if camera enable
		if (cfg->cfg_camera[i].cfg_vin.enable) {
			ret = VIDEO_DEV_destroyVideoDev(&cfg->cfg_camera[i].cfg_vin);
			if (ret < 0) {
				return ret;
			}
		} else {
			continue;
		}
	}
	ret = VIDEO_SYS_exitSystem();
	return ret;
}

static INT32 cfgStopVideo()
{
	INT32 i = 0;
	INT32 j = 0;
	INT32 ret = 0;
	MPI_DEV dev_idx = MPI_VIDEO_DEV(0);
	CFG_ALL_S *cfg = &g_config;

	for (i = 0; i < MAX_VIDEO_INPUT; i++) {
		// TODO: check if camera enable

		if (cfg->cfg_camera[i].cfg_vin.enable) {
			ret = cfg_close_all_iva(i, &cfg->cfg_camera[i].cfg_vin);
			if (ret < 0) {
				return ret;
			}

			ret = cfg_close_all_iaa(i, &cfg->cfg_camera[i].cfg_vin);
			if (ret < 0) {
				return ret;
			}

			for (j = 0; j < MAX_VENC_STREAM; j++) {
				if (cfg->cfg_camera[i].cfg_venc[j].enable) {
					ret = VIDEO_ENC_stopEncChn(&cfg->cfg_camera[i].cfg_venc[j]);
					if (ret < 0) {
						return ret;
					}
				}
			}

			dev_idx.dev = cfg->cfg_camera[i].cfg_vin.dev_idx;

			ret = VIDEO_DEV_stopVideoChn(dev_idx, cfg->cfg_camera[i].cfg_vin.cfg_vchn);
			if (ret < 0) {
				return ret;
			}
			ret = VIDEO_DEV_stopVideoDev(&cfg->cfg_camera[i].cfg_vin);
			if (ret < 0) {
				return ret;
			}
		} else {
			continue;
		}
	}

	return 0;
}

static INT32 cfg_set_img(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	UINT32 dev_idx;
	CFG_ALL_S *cfg = &g_config;
	CFG_IMG_S *cfg_img = NULL;

	if (size != sizeof(CFG_IMG_S)) {
		SYS_TRACE("CFG_SET_CONFIG_IMG size %d / %d dismatch\n", size, sizeof(CFG_IMG_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	dev_idx = attr->group;
	cfg_img = (CFG_IMG_S *)data;
	ret = cfg_apply_img(cfg_img, dev_idx);
	if (ret) {
		return -1;
	}
	cfg_img = &cfg->cfg_camera[attr->group].cfg_img;
	memcpy(cfg_img, data, sizeof(CFG_IMG_S));

	return 0;
}

static INT32 cfg_set_adv_img(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	MPI_DEV dev_idx = MPI_VIDEO_DEV(attr->group);
	CFG_ALL_S *cfg = &g_config;
	CFG_ADV_IMG_S *cfg_adv_img = NULL;

	if (size != sizeof(CFG_ADV_IMG_S)) {
		SYS_TRACE("CFG_SET_CONFIG_ADV_IMG size %d / %d dismatch\n", size, sizeof(CFG_ADV_IMG_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	cfg_adv_img = (CFG_ADV_IMG_S *)data;
	ret = VIDEO_setAdvImgPref(dev_idx, cfg_adv_img);
	if (ret) {
		return -1;
	}
	cfg_adv_img = &cfg->cfg_camera[attr->group].cfg_adv_img;
	memcpy(cfg_adv_img, data, sizeof(CFG_ADV_IMG_S));

	return 0;
}

static INT32 cfg_set_awb(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	MPI_DEV dev_idx = MPI_VIDEO_DEV(attr->group);
	CFG_ALL_S *cfg = &g_config;
	CFG_AWB_S *cfg_awb = NULL;

	if (size != sizeof(CFG_AWB_S)) {
		SYS_TRACE("CFG_SET_CONFIG_AWB size %d / %d dismatch\n", size, sizeof(CFG_AWB_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	cfg_awb = (CFG_AWB_S *)data;
	ret = VIDEO_setAwbPref(dev_idx, cfg_awb);
	if (ret) {
		return -1;
	}
	cfg_awb = &cfg->cfg_camera[attr->group].cfg_awb;
	memcpy(cfg_awb, data, sizeof(CFG_AWB_S));

	return 0;
}

static INT32 cfg_set_color(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	MPI_DEV dev_idx = MPI_VIDEO_DEV(attr->group);
	CFG_ALL_S *cfg = &g_config;
	CFG_COLOR_S *cfg_color = NULL;

	if (size != sizeof(CFG_COLOR_S)) {
		SYS_TRACE("CFG_SET_CONFIG_COLOR size %d / %d dismatch\n", size, sizeof(CFG_COLOR_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	cfg_color = (CFG_COLOR_S *)data;
	ret = VIDEO_setColorMode(dev_idx, cfg_color);
	if (ret) {
		return -1;
	}
	cfg_color = &cfg->cfg_camera[attr->group].cfg_color;
	memcpy(cfg_color, data, sizeof(CFG_COLOR_S));

	return 0;
}

static INT32 cfg_set_anti_flicker(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	MPI_DEV dev_idx = MPI_VIDEO_DEV(attr->group);
	CFG_ALL_S *cfg = &g_config;
	AGTX_ANTI_FLICKER_CONF_S *cfg_anti_flicker = NULL;

	if (size != sizeof(AGTX_ANTI_FLICKER_CONF_S)) {
		SYS_TRACE("AGTX_ANTI_FLICKER_CONF_S size %d / %d dismatch\n", size, sizeof(AGTX_ANTI_FLICKER_CONF_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	cfg_anti_flicker = (AGTX_ANTI_FLICKER_CONF_S *)data;
	ret = VIDEO_DIP_setDefaultAntiFlicker(dev_idx, cfg_anti_flicker);
	if (ret) {
		return -1;
	}
	cfg_anti_flicker = &cfg->cfg_camera[attr->group].cfg_anti_flicker;
	memcpy(cfg_anti_flicker, data, sizeof(AGTX_ANTI_FLICKER_CONF_S));

	return 0;
}

static INT32 cfg_set_vin(CFG_ATTR_S *attr, char *data, INT32 size)
{
	CFG_ALL_S *cfg = &g_config;
	CFG_VIN_S *cfg_vin = NULL;

	if (size != sizeof(CFG_VIN_S)) {
		SYS_TRACE("CFG_SET_CONFIG_VIN size %d / %d dismatch\n", size, sizeof(CFG_VIN_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	/*cfg doesn't initial,only save value*/
	if (!g_video_init) {
		cfg_vin = &cfg->cfg_camera[attr->group].cfg_vin;
		memcpy(cfg_vin, data, sizeof(CFG_VIN_S));
		return 0;
	}

	return 0;
}

static INT32 cfg_set_vchn(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	MPI_DEV dev_idx = MPI_VIDEO_DEV(attr->group);
	CFG_ALL_S *cfg = &g_config;
	CFG_VCHN_S *cfg_chn = NULL;
	CFG_VCHN_S *cfg_chn_old = NULL;

	if (size != sizeof(CFG_VCHN_S)) {
		SYS_TRACE("CFG_SET_CONFIG_VCHN size %d / %d dismatch\n", size, sizeof(CFG_VCHN_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	/*cfg doesn't initial,only save value*/
	cfg_chn = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id];
	if (!g_video_init || !cfg_chn->enable) {
		memcpy(cfg_chn, data, sizeof(CFG_VCHN_S));
		return 0;
	}

	cfg_chn = (CFG_VCHN_S *)data;
	cfg_chn_old = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id];

	ret = cfg_apply_vchn(dev_idx, cfg_chn, 0);
	if (ret < 0) {
		return ret;
	}
	memcpy(cfg_chn_old, cfg_chn, sizeof(CFG_VCHN_S));

	return ret;
}

static INT32 cfg_set_venc(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	MPI_DEV dev_idx = MPI_VIDEO_DEV(attr->group);
	CFG_ALL_S *cfg = &g_config;
	CFG_VCHN_S *cfg_vchn = NULL;
	CFG_VENC_S *cfg_venc = NULL;
	CFG_VENC_S *cfg_venc_old = NULL;

	if (size != sizeof(CFG_VENC_S)) {
		SYS_TRACE("CFG_SET_CONFIG_VENC size %d / %d dismatch\n", size, sizeof(CFG_VENC_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	/*cfg doesn't initial,only save value*/
	cfg_venc = &cfg->cfg_camera[attr->group].cfg_venc[attr->id];
	if (!g_video_init || !cfg_venc->enable) {
		memcpy(cfg_venc, data, sizeof(CFG_VENC_S));
		return 0;
	}

	cfg_venc = (CFG_VENC_S *)data;
	cfg_venc_old = &cfg->cfg_camera[attr->group].cfg_venc[attr->id];
	if ((cfg_venc->width != cfg_venc_old->width) || (cfg_venc->height != cfg_venc_old->height)) {
		memcpy(&cfg->cfg_camera[attr->group].cfg_venc[attr->id], cfg_venc, sizeof(CFG_VENC_S));
		cfg_vchn = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id];
		cfg_vchn->max_width = cfg_venc->max_width;
		cfg_vchn->max_height = cfg_venc->max_height;
		cfg_vchn->width = cfg_venc->width;
		cfg_vchn->height = cfg_venc->height;
		cfg_vchn->fps = cfg_venc->fps;
		cfgStopVideo();
		cfgStartVideo();
	} else {
		if (cfg_venc_old->fps != cfg_venc->fps) {
			cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].fps = cfg_venc->fps;
			ret = cfg_apply_vchn(dev_idx, &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id], 0);
			if (ret < 0) {
				return ret;
			}

			if (cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].od.enabled) {
				ret = cfg_apply_od(attr->group, attr->id,
				                   &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].od, 0,
				                   cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].fps);
				if (ret < 0) {
					return ret;
				}
			}
		}

		ret = cfg_apply_venc(cfg_venc, 0);
		memcpy(&cfg->cfg_camera[attr->group].cfg_venc[attr->id], cfg_venc, sizeof(CFG_VENC_S));
	}

	return ret;
}

static INT32 cfg_set_td(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	INT32 enable_change = 0;
	UINT32 dev_idx = 0;
	UINT32 chn_idx = 0;
	CFG_ALL_S *cfg = &g_config;
	CFG_IVA_TD_S *cfg_td = NULL;
	CFG_IVA_TD_S *cfg_td_old = NULL;

	if (size != sizeof(CFG_IVA_TD_S)) {
		SYS_TRACE("CFG_SET_CONFIG_TD size %d / %d dismatch\n", size, sizeof(CFG_IVA_TD_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	/*cfg doesn't initial,only save value*/
	if (!g_video_init) {
		cfg_td = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].td;
		memcpy(cfg_td, data, sizeof(CFG_IVA_TD_S));
		return 0;
	}

	dev_idx = attr->group;
	chn_idx = attr->id;
	cfg_td = (CFG_IVA_TD_S *)data;
	cfg_td_old = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].td;
	enable_change = (cfg_td->enabled != cfg_td_old->enabled) ? 1 : 0;
	ret = cfg_apply_td(dev_idx, chn_idx, cfg_td, enable_change);

	if (ret < 0) {
		return ret;
	}

	memcpy(cfg_td_old, data, sizeof(CFG_IVA_TD_S));

	return ret;
}

static INT32 cfg_set_md(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	INT32 enable_change = 0;
	UINT32 dev_idx = 0;
	UINT32 chn_idx = 0;
	UINT32 win_idx = 0;
	CFG_ALL_S *cfg = &g_config;
	CFG_VCHN_S *cfg_vchn = NULL;
	CFG_IVA_MD_S *cfg_md = NULL;
	CFG_IVA_MD_S *cfg_md_old = NULL;
	AGTX_WINDOW_PARAM_S *win_attr = NULL;

	if (size != sizeof(CFG_IVA_MD_S)) {
		SYS_TRACE("CFG_SET_CONFIG_MD size %d / %d dismatch\n", size, sizeof(CFG_IVA_MD_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	/*cfg doesn't initial,only save value*/
	if (!g_video_init) {
		cfg_md = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].md;
		memcpy(cfg_md, data, sizeof(CFG_IVA_MD_S));
		return 0;
	}

	dev_idx = attr->group;
	chn_idx = attr->id;
	cfg_md = (CFG_IVA_MD_S *)data;
	cfg_md_old = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].md;
	enable_change = (cfg_md->enabled != cfg_md_old->enabled) ? 1 : 0;
	cfg_vchn = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id];
	/* WINDOW resolution calc from vdev */
	win_attr = &cfg_vchn->lyt_cfg.window_array[win_idx];
	ret = cfg_apply_md(dev_idx, chn_idx, win_attr->pos_width, win_attr->pos_height, cfg_md, cfg_md_old, enable_change);

	if (ret < 0) {
		return ret;
	}

	memcpy(cfg_md_old, data, sizeof(CFG_IVA_MD_S));

	return ret;
}

static INT32 cfg_set_aroi(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	INT32 enable_change = 0;
	UINT32 dev_idx = 0;
	UINT32 chn_idx = 0;
	UINT32 win_idx = 0;
	CFG_ALL_S *cfg = &g_config;
	CFG_VCHN_S *cfg_vchn = NULL;
	CFG_IVA_AROI_S *cfg_aroi = NULL;
	CFG_IVA_AROI_S *cfg_aroi_old = NULL;
	AGTX_WINDOW_PARAM_S *win_attr = NULL;

	if (size != sizeof(CFG_IVA_AROI_S)) {
		SYS_TRACE("CFG_SET_CONFIG_AROI size %d / %d dismatch\n", size, sizeof(CFG_IVA_AROI_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	/*cfg doesn't initial,only save value*/
	if (!g_video_init) {
		cfg_aroi = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].aroi;
		memcpy(cfg_aroi, data, sizeof(CFG_IVA_AROI_S));
		return 0;
	}

	dev_idx = attr->group;
	chn_idx = attr->id;
	cfg_aroi = (CFG_IVA_AROI_S *)data;
	cfg_aroi_old = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].aroi;
	enable_change = (cfg_aroi->enabled != cfg_aroi_old->enabled) ? 1 : 0;
	/* TODO get layout res instead of channel res */
	cfg_vchn = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id];
	/* WINDOW resolution calc from vdev */
	win_attr = &cfg_vchn->lyt_cfg.window_array[win_idx];
	ret = cfg_apply_aroi(dev_idx, chn_idx, win_attr->pos_width, win_attr->pos_height, cfg_aroi, cfg_aroi_old, enable_change);
	if (ret < 0) {
		return ret;
	}

	memcpy(cfg_aroi_old, data, sizeof(CFG_IVA_AROI_S));

	return ret;
}

static INT32 cfg_set_pd(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	INT32 enable_change = 0;
	UINT32 dev_idx = 0;
	UINT32 chn_idx = 0;
	UINT32 win_idx = 0;
	CFG_ALL_S *cfg = &g_config;
	CFG_VCHN_S *cfg_vchn = NULL;
	CFG_IVA_PD_S *cfg_pd = NULL;
	CFG_IVA_PD_S *cfg_pd_old = NULL;
	AGTX_WINDOW_PARAM_S *win_attr = NULL;

	if (size != sizeof(CFG_IVA_PD_S)) {
		SYS_TRACE("CFG_SET_CONFIG_PD size %d / %d dismatch\n", size, sizeof(CFG_IVA_PD_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	/*cfg doesn't initial,only save value*/
	if (!g_video_init) {
		cfg_pd = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].pd;
		memcpy(cfg_pd, data, sizeof(CFG_IVA_PD_S));
		return 0;
	}

	dev_idx = attr->group;
	chn_idx = attr->id;
	cfg_pd = (CFG_IVA_PD_S *)data;
	cfg_pd_old = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].pd;
	enable_change = (cfg_pd->enabled != cfg_pd_old->enabled) ? 1 : 0;
	cfg_vchn = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id];
	/* avmain iva only work on win_idx 0 */
	win_attr = &cfg_vchn->lyt_cfg.window_array[win_idx];
	ret = cfg_apply_pd(dev_idx, chn_idx, win_attr->pos_width, win_attr->pos_height, cfg_pd, enable_change);

	if (ret < 0) {
		return ret;
	}

	memcpy(cfg_pd_old, data, sizeof(CFG_IVA_PD_S));

	return ret;
}

static INT32 cfg_set_od(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	INT32 enable_change = 0;
	UINT32 dev_idx = 0;
	UINT32 chn_idx = 0;
	CFG_ALL_S *cfg = &g_config;
	CFG_IVA_OD_S *cfg_od = NULL;
	CFG_IVA_OD_S *cfg_od_old = NULL;

	if (size != sizeof(CFG_IVA_OD_S)) {
		SYS_TRACE("CFG_SET_CONFIG_OD size %d / %d dismatch\n", size, sizeof(CFG_IVA_OD_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	/*cfg doesn't initial,only save value*/
	if (!g_video_init) {
		cfg_od = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].od;
		memcpy(cfg_od, data, sizeof(CFG_IVA_OD_S));
		return 0;
	}

	dev_idx = attr->group;
	chn_idx = attr->id;
	cfg_od = (CFG_IVA_OD_S *)data;
	cfg_od_old = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].od;
	enable_change = (cfg_od->enabled != cfg_od_old->enabled) ? 1 : 0;

	ret = cfg_apply_od(dev_idx, chn_idx, cfg_od, enable_change,
	                   cfg->cfg_camera[dev_idx].cfg_vin.cfg_vchn[chn_idx].fps);

	if (ret < 0) {
		return ret;
	}

	memcpy(cfg_od_old, data, sizeof(CFG_IVA_OD_S));

	return ret;
}

static INT32 cfg_set_rms(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	INT32 enable_change = 0;
	UINT32 dev_idx = 0;
	UINT32 chn_idx = 0;
	CFG_ALL_S *cfg = &g_config;
	CFG_IVA_RMS_S *cfg_rms = NULL;
	CFG_IVA_RMS_S *cfg_rms_old = NULL;

	if (size != sizeof(CFG_IVA_RMS_S)) {
		SYS_TRACE("CFG_SET_CONFIG_RMS size %d / %d dismatch\n", size, sizeof(CFG_IVA_RMS_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	/*cfg doesn't initial,only save value*/
	if (!g_video_init) {
		cfg_rms = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].rms;
		memcpy(cfg_rms, data, sizeof(CFG_IVA_RMS_S));
		return 0;
	}

	dev_idx = attr->group;
	chn_idx = attr->id;
	cfg_rms = (CFG_IVA_RMS_S *)data;
	cfg_rms_old = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].rms;
	enable_change = (cfg_rms->enabled != cfg_rms_old->enabled) ? 1 : 0;

	ret = cfg_apply_rms(dev_idx, chn_idx, cfg_rms, enable_change);

	if (ret < 0) {
		return ret;
	}

	memcpy(cfg_rms_old, data, sizeof(CFG_IVA_RMS_S));

	return ret;
}

static INT32 cfg_set_ld(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	INT32 enable_change = 0;
	UINT32 dev_idx = 0;
	UINT32 chn_idx = 0;
	UINT32 win_idx = 0;
	CFG_ALL_S *cfg = &g_config;
	CFG_VCHN_S *cfg_vchn = NULL;
	CFG_IVA_LD_S *cfg_ld = NULL;
	CFG_IVA_LD_S *cfg_ld_old = NULL;
	AGTX_WINDOW_PARAM_S *win_attr = NULL;

	if (size != sizeof(CFG_IVA_LD_S)) {
		SYS_TRACE("CFG_SET_CONFIG_LD size %d / %d dismatch\n", size, sizeof(CFG_IVA_RMS_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	/*cfg doesn't initial,only save value*/
	if (!g_video_init) {
		cfg_ld = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].ld;
		memcpy(cfg_ld, data, sizeof(CFG_IVA_LD_S));
		return 0;
	}

	dev_idx = attr->group;
	chn_idx = attr->id;
	cfg_ld = (CFG_IVA_LD_S *)data;
	cfg_ld_old = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].ld;
	enable_change = (cfg_ld->enabled != cfg_ld_old->enabled) ? 1 : 0;
	cfg_vchn = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id];
	win_attr = &cfg_vchn->lyt_cfg.window_array[win_idx];

	ret = cfg_apply_ld(dev_idx, chn_idx, win_attr->pos_width, win_attr->pos_height, cfg_ld, enable_change);
	if (ret < 0) {
		return ret;
	}

	memcpy(cfg_ld_old, data, sizeof(CFG_IVA_LD_S));

	return ret;
}

static INT32 cfg_set_ef(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	INT32 enable_change = 0;
	UINT32 dev_idx = 0;
	UINT32 chn_idx = 0;
	UINT32 win_idx = 0;
	CFG_ALL_S *cfg = &g_config;
	CFG_VCHN_S *cfg_vchn = NULL;
	CFG_IVA_EF_S *cfg_ef = NULL;
	CFG_IVA_EF_S *cfg_ef_old = NULL;
	AGTX_WINDOW_PARAM_S *win_attr = NULL;

	if (size != sizeof(CFG_IVA_EF_S)) {
		SYS_TRACE("CFG_SET_CONFIG_EF size %d / %d dismatch\n", size, sizeof(CFG_IVA_EF_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	/*cfg doesn't initial,only save value*/
	if (!g_video_init) {
		cfg_ef = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].ef;
		memcpy(cfg_ef, data, sizeof(CFG_IVA_EF_S));
		return 0;
	}

	dev_idx = attr->group;
	chn_idx = attr->id;
	cfg_ef = (CFG_IVA_EF_S *)data;
	cfg_ef_old = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].ef;
	enable_change = (cfg_ef->enabled != cfg_ef_old->enabled) ? 1 : 0;
	cfg_vchn = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id];
	win_attr = &cfg_vchn->lyt_cfg.window_array[win_idx];
	ret = cfg_apply_ef(dev_idx, chn_idx, win_attr->pos_width, win_attr->pos_height, cfg_ef, cfg_ef_old, enable_change);

	if (ret < 0) {
		return ret;
	}

	memcpy(cfg_ef_old, data, sizeof(CFG_IVA_EF_S));

	return ret;
}

static INT32 cfg_set_vdbg(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	INT32 enable_change = 0;
	UINT32 dev_idx = 0;
	UINT32 chn_idx = 0;
	CFG_ALL_S *cfg = &g_config;
	CFG_VDBG_S *cfg_vdbg = NULL;
	CFG_VDBG_S *cfg_vdbg_old = NULL;

	if (size != sizeof(CFG_VDBG_S)) {
		SYS_TRACE("CFG_SET_CONFIG_VDBG size %d / %d dismatch\n", size, sizeof(CFG_VDBG_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	/*cfg doesn't initial,only save value*/
	if (!g_video_init) {
		cfg_vdbg = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].vdbg;
		memcpy(cfg_vdbg, data, sizeof(CFG_VDBG_S));
		return 0;
	}

	dev_idx = attr->group;
	chn_idx = attr->id;
	cfg_vdbg = (CFG_VDBG_S *)data;
	cfg_vdbg_old = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].vdbg;
	enable_change = (cfg_vdbg->enabled != cfg_vdbg_old->enabled) ? 1 : 0;

	ret = cfg_apply_vdbg(dev_idx, chn_idx, cfg_vdbg, enable_change);

	if (ret < 0) {
		return ret;
	}

	memcpy(cfg_vdbg_old, data, sizeof(CFG_VDBG_S));

	return ret;
}

static INT32 cfg_set_ptz(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	INT32 enable_change = 0;
	UINT32 dev_idx = 0;
	CFG_ALL_S *cfg = &g_config;
	CFG_PTZ_S *cfg_ptz = NULL;
	CFG_PTZ_S *cfg_ptz_old = NULL;
	CFG_VIN_S *cfg_vin = NULL;

	if (size != sizeof(CFG_PTZ_S)) {
		SYS_TRACE("CFG_SET_CONFIG_PTZ size %d / %d dismatch\n", size, sizeof(CFG_PTZ_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	/*cfg doesn't initial,only save value*/
	if (!g_video_init) {
		cfg_ptz = &cfg->cfg_camera[attr->group].cfg_vin.ptz;
		memcpy(cfg_ptz, data, sizeof(CFG_PTZ_S));
		return 0;
	}

	dev_idx = attr->group;
	cfg_ptz = (CFG_PTZ_S *)data;
	cfg_ptz_old = &cfg->cfg_camera[attr->group].cfg_vin.ptz;
	cfg_vin = &cfg->cfg_camera[attr->group].cfg_vin;
	enable_change = (cfg_ptz->enabled != cfg_ptz_old->enabled) ? 1 : 0;

	ret = cfg_apply_ptz(dev_idx, cfg_ptz, cfg_ptz_old, cfg_vin, enable_change);

	if (ret < 0) {
		return ret;
	}

	memcpy(cfg_ptz_old, data, sizeof(CFG_PTZ_S));

	return ret;
}

static INT32 cfg_set_shd(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	INT32 enable_change = 0;
	UINT32 dev_idx = 0;
	UINT32 chn_idx = 0;
	UINT32 win_idx = 0;
	CFG_ALL_S *cfg = &g_config;
	CFG_VCHN_S *cfg_vchn = NULL;
	CFG_IVA_SHD_S *cfg_shd = NULL;
	CFG_IVA_SHD_S *cfg_shd_old = NULL;
	AGTX_WINDOW_PARAM_S *win_attr = NULL;

	if (size != sizeof(CFG_IVA_SHD_S)) {
		SYS_TRACE("CFG_SET_CONFIG_SHD size %d / %d dismatch\n", size, sizeof(CFG_IVA_SHD_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	/*cfg doesn't initial,only save value*/
	if (!g_video_init) {
		cfg_shd = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].shd;
		memcpy(cfg_shd, data, sizeof(CFG_IVA_SHD_S));
		return 0;
	}

	dev_idx = attr->group;
	chn_idx = attr->id;
	cfg_shd = (CFG_IVA_SHD_S *)data;
	cfg_shd_old = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].shd;
	enable_change = (cfg_shd->enabled != cfg_shd_old->enabled) ? 1 : 0;
	cfg_vchn = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id];
	win_attr = &cfg_vchn->lyt_cfg.window_array[win_idx];
	/* avmain iva only work on win_idx 0 */
	ret = cfg_apply_shd(dev_idx, chn_idx, win_attr->pos_width, win_attr->pos_height, cfg_shd, cfg_shd_old, enable_change);

	if (ret < 0) {
		return ret;
	}

	memcpy(cfg_shd_old, data, sizeof(CFG_IVA_SHD_S));

	return ret;
}

static INT32 cfg_set_eaif(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	INT32 enable_change = 0;
	UINT32 dev_idx = 0;
	UINT32 chn_idx = 0;
	//UINT32 win_idx = 0;
	CFG_ALL_S *cfg = &g_config;
	CFG_IVA_EAIF_S *cfg_eaif = NULL;
	CFG_IVA_EAIF_S *cfg_eaif_old = NULL;

	if (size != sizeof(CFG_IVA_EAIF_S)) {
		SYS_TRACE("CFG_SET_CONFIG_EAIF size %d / %d dismatch\n", size, sizeof(CFG_IVA_EAIF_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	/*cfg doesn't initial,only save value*/
	if (!g_video_init) {
		cfg_eaif = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].eaif;
		memcpy(cfg_eaif, data, sizeof(CFG_IVA_EAIF_S));
		return 0;
	}

	dev_idx = attr->group;
	chn_idx = attr->id;
	cfg_eaif = (CFG_IVA_EAIF_S *)data;
	cfg_eaif_old = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].eaif;
	enable_change = (cfg_eaif->enabled != cfg_eaif_old->enabled) ? 1 : 0;
	/* avmain iva only work on win_idx 0 */
	ret = cfg_apply_eaif(dev_idx, chn_idx, cfg_eaif, enable_change);

	if (ret < 0) {
		return ret;
	}

	memcpy(cfg_eaif_old, data, sizeof(CFG_IVA_EAIF_S));

	return ret;
}

static INT32 cfg_set_pfm(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	INT32 enable_change = 0;
	UINT32 dev_idx = 0;
	UINT32 chn_idx = 0;
	UINT32 win_idx = 0;
	CFG_ALL_S *cfg = &g_config;
	CFG_VCHN_S *cfg_vchn = NULL;
	CFG_IVA_PFM_S *cfg_pfm = NULL;
	CFG_IVA_PFM_S *cfg_pfm_old = NULL;
	AGTX_WINDOW_PARAM_S *win_attr = NULL;

	if (size != sizeof(CFG_IVA_PFM_S)) {
		SYS_TRACE("CFG_SET_CONFIG_PFM size %d / %d dismatch\n", size, sizeof(CFG_IVA_PFM_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	/*cfg doesn't initial,only save value*/
	if (!g_video_init) {
		cfg_pfm = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].pfm;
		memcpy(cfg_pfm, data, sizeof(CFG_IVA_PFM_S));
		return 0;
	}

	dev_idx = attr->group;
	chn_idx = attr->id;
	cfg_pfm = (CFG_IVA_PFM_S *)data;
	cfg_pfm_old = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].pfm;
	enable_change = (cfg_pfm->enabled != cfg_pfm_old->enabled) ? 1 : 0;
	cfg_vchn = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id];
	/* WINDOW resolution calc from vdev */
	win_attr = &cfg_vchn->lyt_cfg.window_array[win_idx];

	/* avmain iva only work on win_idx 0 */
	ret = cfg_apply_pfm(dev_idx, chn_idx, win_attr->pos_width, win_attr->pos_height, cfg_pfm, enable_change);

	if (ret < 0) {
		return ret;
	}

	memcpy(cfg_pfm_old, data, sizeof(CFG_IVA_PFM_S));

	return ret;
}

static INT32 cfg_set_bm(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	INT32 enable_change = 0;
	UINT32 dev_idx = 0;
	UINT32 chn_idx = 0;
	UINT32 win_idx = 0;
	CFG_ALL_S *cfg = &g_config;
	CFG_VCHN_S *cfg_vchn = NULL;
	CFG_IVA_BM_S *cfg_bm = NULL;
	CFG_IVA_BM_S *cfg_bm_old = NULL;
	AGTX_WINDOW_PARAM_S *win_attr = NULL;

	if (size != sizeof(CFG_IVA_BM_S)) {
		SYS_TRACE("CFG_SET_CONFIG_BM size %d / %d dismatch\n", size, sizeof(CFG_IVA_BM_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	/*cfg doesn't initial,only save value*/
	if (!g_video_init) {
		cfg_bm = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].bm;
		memcpy(cfg_bm, data, sizeof(CFG_IVA_BM_S));
		return 0;
	}

	dev_idx = attr->group;
	chn_idx = attr->id;
	cfg_bm = (CFG_IVA_BM_S *)data;
	cfg_bm_old = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].bm;
	enable_change = (cfg_bm->enabled != cfg_bm_old->enabled) ? 1 : 0;
	cfg_vchn = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id];
	/* WINDOW resolution calc from vdev */
	win_attr = &cfg_vchn->lyt_cfg.window_array[win_idx];

	/* avmain iva only work on win_idx 0 */
	ret = cfg_apply_bm(dev_idx, chn_idx, win_attr, cfg_bm, enable_change);

	if (ret < 0) {
		return ret;
	}

	memcpy(cfg_bm_old, data, sizeof(CFG_IVA_BM_S));

	return ret;
}

static INT32 cfg_set_dk(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	INT32 enable_change = 0;
	UINT32 dev_idx = 0;
	UINT32 chn_idx = 0;
	UINT32 win_idx = 0;
	CFG_ALL_S *cfg = &g_config;
	CFG_VCHN_S *cfg_vchn = NULL;
	CFG_IVA_DK_S *cfg_dk = NULL;
	CFG_IVA_DK_S *cfg_dk_old = NULL;
	AGTX_WINDOW_PARAM_S *win_attr = NULL;

	if (size != sizeof(CFG_IVA_DK_S)) {
		SYS_TRACE("CFG_SET_CONFIG_DK size %d / %d dismatch\n", size, sizeof(CFG_IVA_DK_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	/*cfg doesn't initial,only save value*/
	if (!g_video_init) {
		cfg_dk = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].dk;
		memcpy(cfg_dk, data, sizeof(CFG_IVA_DK_S));
		return 0;
	}

	dev_idx = attr->group;
	chn_idx = attr->id;
	cfg_dk = (CFG_IVA_DK_S *)data;
	cfg_dk_old = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].dk;
	enable_change = (cfg_dk->enabled != cfg_dk_old->enabled) ? 1 : 0;
	cfg_vchn = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id];
	/* WINDOW resolution calc from vdev */
	win_attr = &cfg_vchn->lyt_cfg.window_array[win_idx];

	/* avmain iva only work on win_idx 0 */
	ret = cfg_apply_dk(dev_idx, chn_idx, win_attr, cfg_dk, enable_change);

	if (ret < 0) {
		return ret;
	}

	memcpy(cfg_dk_old, data, sizeof(CFG_IVA_DK_S));

	return ret;
}

static INT32 cfg_set_fld(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	INT32 enable_change = 0;
	UINT32 dev_idx = 0;
	UINT32 chn_idx = 0;
	CFG_ALL_S *cfg = &g_config;
	CFG_IVA_FLD_S *cfg_fld = NULL;
	CFG_IVA_FLD_S *cfg_fld_old = NULL;

	if (size != sizeof(CFG_IVA_FLD_S)) {
		SYS_TRACE("CFG_SET_CONFIG_FLD size %d / %d dismatch\n", size, sizeof(CFG_IVA_FLD_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	/*cfg doesn't initial,only save value*/
	if (!g_video_init) {
		cfg_fld = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].fld;
		memcpy(cfg_fld, data, sizeof(CFG_IVA_FLD_S));
		return 0;
	}

	dev_idx = attr->group;
	chn_idx = attr->id;
	cfg_fld = (CFG_IVA_FLD_S *)data;
	cfg_fld_old = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].fld;
	enable_change = (cfg_fld->enabled != cfg_fld_old->enabled) ? 1 : 0;
	/* avmain iva only work on win_idx 0 */
	ret = cfg_apply_fld(dev_idx, chn_idx, cfg_fld, enable_change);

	if (ret < 0) {
		return ret;
	}

	memcpy(cfg_fld_old, data, sizeof(CFG_IVA_FLD_S));

	return ret;
}

static INT32 cfg_set_lsd(CFG_ATTR_S *attr, char *data, INT32 size)
{
	INT32 ret = 0;
	INT32 enable_change = 0;
	UINT32 dev_idx = 0;
	CFG_ALL_S *cfg = &g_config;
	CFG_IAA_LSD_S *cfg_lsd = NULL;
	CFG_IAA_LSD_S *cfg_lsd_old = NULL;

	if (size != sizeof(CFG_IAA_LSD_S)) {
		SYS_TRACE("CFG_SET_CONFIG_LSD size %d / %d dismatch\n", size, sizeof(CFG_IAA_LSD_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	/*cfg doesn't initial,only save value*/
	if (!g_video_init) {
		cfg_lsd = &cfg->cfg_camera[attr->group].cfg_vin.lsd;
		memcpy(cfg_lsd, data, sizeof(CFG_IAA_LSD_S));
		return 0;
	}

	dev_idx = attr->group;
	cfg_lsd = (CFG_IAA_LSD_S *)data;
	cfg_lsd_old = &cfg->cfg_camera[attr->group].cfg_vin.lsd;
	enable_change = (cfg_lsd->enabled != cfg_lsd_old->enabled) ? 1 : 0;

	ret = cfg_apply_lsd(dev_idx, cfg_lsd, enable_change);

	if (ret < 0) {
		return ret;
	}

	memcpy(cfg_lsd_old, data, sizeof(CFG_IAA_LSD_S));

	return ret;
}

int init_osd = 0;

static INT32 cfg_set_osd(CFG_ATTR_S *attr, void *data)
{
	CFG_VCHN_S *conf = NULL;
	AGTX_OSD_CONF_OUTER_S *cur = NULL;
	AGTX_OSD_CONF_OUTER_S *new = NULL;
	AGTX_OSD_CONF_S *cfg_osd = NULL;
	INT32 ret = 0;
	MPI_ECHN chn_idx = { { 0 } };

	if (data == NULL) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	cfg_osd = (AGTX_OSD_CONF_S *)data;
	if (init_osd) {

		for (int i = 0; i < MAX_AGTX_OSD_CONF_OUTER_S_REGION_SIZE; i++) {
			conf = &g_config.cfg_camera[0].cfg_vin.cfg_vchn[i];
			if (!conf->enable)
				continue;
			chn_idx = MPI_ENC_CHN(conf->chn_idx);
			cur = (AGTX_OSD_CONF_OUTER_S *)&conf->osd;
			new = (AGTX_OSD_CONF_OUTER_S *)&cfg_osd->strm[i];
			ret |= VIDEO_OSD_checkParam(chn_idx, cur, new);
		}
		if (ret == VIDEO_OSD_ERR_PARAM)
			return -1;
		else if (ret == VIDEO_OSD_RESTREAM_REQUEST) { }
		else {
			VIDEO_OSD_setShowWeekDay(cfg_osd->showWeekDay);
			/* Directly set the region */
			for (int i = 0; i < MAX_AGTX_OSD_CONF_OUTER_S_REGION_SIZE; i++) {
				conf = &g_config.cfg_camera[0].cfg_vin.cfg_vchn[i];
				if (!conf->enable)
					continue;
				cur = (AGTX_OSD_CONF_OUTER_S *)&conf->osd;
				new = (AGTX_OSD_CONF_OUTER_S *)&cfg_osd->strm[i];
				chn_idx = MPI_ENC_CHN(conf->chn_idx);

				for (int j = 0; j < MPI_OSD_MAX_BIND_CHANNEL; j++) {
					if (cur->region[j].enabled != -1) {

						if (cur->region[j].enabled && new->region[j].enabled) {
							ret = VIDEO_OSD_setBindLoc(chn_idx, j, conf->width, conf->height, &new->region[j]);
							if (ret) {
								SYS_TRACE("Fail to reset OSD region in chn:%d osd region:%d\n", conf->chn_idx, j);
									return -1;
							}
							cur->region[j].start_x = new->region[j].start_x;
							cur->region[j].start_y = new->region[j].start_y;
						}
						cur->region[j].enabled = new->region[j].enabled;

						/* TODO Modify Here to Support Fake bind editing*/
						if (strcmp((char *)cur->region[j].type_spec,
								(const char *)new->region[j].type_spec)) {
							if (j == 0) {
								/* channel name is fixed to region 0 */
								strcpy((char *)cur->region[j].type_spec,
									(const char *)new->region[j].type_spec);
								deleteText(chn_idx, j);
								createText(cur->region[j].start_x, cur->region[j].start_y,
										(char *)cur->region[j].type_spec, chn_idx, conf->width,
										conf->height, cur->region[j].enabled, j);
							}
							if (i == 0 && j == 1) {
								/* follow stream 0 region 1 for date time format */
								strcpy((char *)cur->region[j].type_spec,
									(const char *)new->region[j].type_spec);
								setOsdDateTimeFormat((char *)cur->region[j].type_spec);
							}
						}
						//SYS_TRACE("setbind state chn:%d id:%d %d after\n", chn_idx.chn, j, new->region[j].enabled);
					}
				}
			}
		}
	} else {
		init_osd = 1;
		VIDEO_OSD_setShowWeekDay(cfg_osd->showWeekDay);
	}
	for (int i = 0; i < 4; i++) {
		AGTX_OSD_CONF_OUTER_S *cur =
		        (AGTX_OSD_CONF_OUTER_S *)&g_config.cfg_camera[0].cfg_vin.cfg_vchn[i].osd;
		AGTX_OSD_CONF_OUTER_S *new = (AGTX_OSD_CONF_OUTER_S *)&cfg_osd->strm[i];
		memcpy(cur, new, sizeof(AGTX_OSD_CONF_OUTER_S));
	}

	return ret;
}

static INT32 cfg_set_osd_pm(CFG_ATTR_S *attr, void *data, INT32 size)
{
	INT32 ret = 0;

	CFG_ALL_S *cfg = &g_config;
	CFG_VCHN_S *cfg_vchn = NULL;
	AGTX_OSD_PM_CONF_S *cfg_osd_pm = NULL;

	AGTX_OSD_PM_PARAM_S *old = NULL;
	AGTX_OSD_PM_PARAM_S *new = NULL;
	UINT32 dev_idx = attr->group;
	MPI_ECHN chn_idx = { { 0 } };

	if (size != sizeof(AGTX_OSD_PM_CONF_S)) {
		SYS_TRACE("CFG_OSD_PM_S size %d / %d dismatch\n", size, sizeof(AGTX_OSD_PM_CONF_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}

	cfg_osd_pm = (AGTX_OSD_PM_CONF_S *)data;

	/*cfg doesn't initial,only save value*/
	if (!g_video_init) {
		for (int i = 0; i < 4; i++) {
			memcpy(&cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[i].osd_pm, &cfg_osd_pm->conf[i],
			       sizeof(AGTX_OSD_PM_PARAM_S));
		}
		return 0;
	}

	for (int i = 0; i < 4; i++) {
		cfg_vchn = &cfg->cfg_camera[dev_idx].cfg_vin.cfg_vchn[i];
		if (cfg_vchn->enable == 0) {
			continue;
		}
		chn_idx = MPI_ENC_CHN(cfg_vchn->chn_idx);
		cfg_osd_pm = (CFG_OSD_PM_S *)data;
		old = &cfg_vchn->osd_pm;
		new = &cfg_osd_pm->conf[i];
		ret |= VIDEO_OSD_checkPmParam(chn_idx, old, new, cfg_vchn->width, cfg_vchn->height);
	}
	if (ret < 0)
		return VIDEO_OSD_FAILURE;
	else if (ret == VIDEO_OSD_RESTREAM_REQUEST) { }
	else {
		for (int i = 0; i < 4; i++) {
			cfg_vchn = &cfg->cfg_camera[dev_idx].cfg_vin.cfg_vchn[i];
			if (cfg_vchn->enable == 0) {
				continue;
			}
			cfg_osd_pm = (CFG_OSD_PM_S *)data;
			old = &cfg_vchn->osd_pm;
			new = &cfg_osd_pm->conf[i];
			ret = cfg_apply_osd_pm(dev_idx, cfg_vchn->chn_idx, cfg_vchn->width, cfg_vchn->height, new, old);
			if (ret < 0) {
				return ret;
			}
		}
	}

	for (int i = 0; i < 4; i++) {
		cfg_vchn = &cfg->cfg_camera[dev_idx].cfg_vin.cfg_vchn[i];
		cfg_osd_pm = (CFG_OSD_PM_S *)data;
		old = &cfg_vchn->osd_pm;
		new = &cfg_osd_pm->conf[i];
		memcpy(old, new, sizeof(AGTX_OSD_PM_PARAM_S));
	}
	//cfg_vchn = &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id];
	//for (int j = 0; j < 4; j++ ) {
	//	old = &cfg_vchn->osd_pm.conf[0];
	//	SYS_TRACE("[%s] chn:%d, i:%d, conf st:(%d,%d), end(%d,%d), cr:(%d), alpha:(%d), enabled(%d)\n",
	//    __func__, attr->id,j, old->param[j].start.x, old->param[j].start.y,
	//   old->param[j].end.x, old->param[j].end.y,
	//    old->param[j].color, old->param[j].alpha,
	//    old->param[j].enabled);
	//}
	return ret;
}

static INT32 cfg_set_notify(CFG_ATTR_S *attr, void *data, INT32 size)
{
	INT32 ret = 0;
	/*Currently,notify for all setting we don't specify channel*/
	MPI_WIN idx = {.dev=attr->group, .chn=attr->id, .win=0};
	MPI_ECHN echn_idx = {.chn=0};
	MPI_ENC_EVENT_S event = {.user_setting_changed = 1};

	/*
	if (size != sizeof(CFG_OSD_PM_S)) {
		SYS_TRACE("CFG_NOTIFY_S size %d / %d dismatch\n", size, sizeof(CFG_NOTIFY_S));
		return -1;
	}

	if (!data) {
		SYS_TRACE("Set config failed NULL point\n");
		return -1;
	}
	*/

	if (g_mpi_sys_initialized) {
		MPI_ENC_notifyEvent(echn_idx, &event);
	}

	ret = AVFTR_notifyVideo(idx, -1);

	return ret;
}

INT32 CFG_setConfig(CFG_SET_CONFIG_E cmd, CFG_ATTR_S *attr, void *data, int size)
{
	INT32 ret = 0;

	pthread_mutex_lock(&g_cfg_mutex);
	switch (cmd) {
	case CFG_SET_CONFIG_VIN:
		ret = cfg_set_vin(attr, data, size);
		break;
	case CFG_SET_CONFIG_VCHN:
		ret = cfg_set_vchn(attr, data, size);
		break;
	case CFG_SET_CONFIG_VENC:
		ret = cfg_set_venc(attr, data, size);
		break;
	case CFG_SET_CONFIG_IMG:
		ret = cfg_set_img(attr, data, size);
		break;
	case CFG_SET_CONFIG_ADV_IMG:
		ret = cfg_set_adv_img(attr, data, size);
		break;
	case CFG_SET_CONFIG_AWB:
		ret = cfg_set_awb(attr, data, size);
		break;
	case CFG_SET_CONFIG_COLOR:
		ret = cfg_set_color(attr, data, size);
		break;
	case CFG_SET_CONFIG_ANTI_FLICKER:
		ret = cfg_set_anti_flicker(attr, data, size);
		break;
	case CFG_SET_CONFIG_TD:
		ret = cfg_set_td(attr, data, size);
		break;
	case CFG_SET_CONFIG_MD:
		ret = cfg_set_md(attr, data, size);
		break;
	case CFG_SET_CONFIG_AROI:
		ret = cfg_set_aroi(attr, data, size);
		break;
	case CFG_SET_CONFIG_PD:
		ret = cfg_set_pd(attr, data, size);
		break;
	case CFG_SET_CONFIG_OD:
		ret = cfg_set_od(attr, data, size);
		break;
	case CFG_SET_CONFIG_RMS:
		ret = cfg_set_rms(attr, data, size);
		break;
	case CFG_SET_CONFIG_LD:
		ret = cfg_set_ld(attr, data, size);
		break;
	case CFG_SET_CONFIG_EF:
		ret = cfg_set_ef(attr, data, size);
		break;
	case CFG_SET_CONFIG_VDBG:
		ret = cfg_set_vdbg(attr, data, size);
		break;
	case CFG_SET_CONFIG_PTZ:
		ret = cfg_set_ptz(attr, data, size);
		break;
	case CFG_SET_CONFIG_SHD:
		ret = cfg_set_shd(attr, data, size);
		break;
	case CFG_SET_CONFIG_EAIF:
		ret = cfg_set_eaif(attr, data, size);
		break;
	case CFG_SET_CONFIG_PFM:
		ret = cfg_set_pfm(attr, data, size);
		break;
	case CFG_SET_CONFIG_BM:
		ret = cfg_set_bm(attr, data, size);
		break;
	case CFG_SET_CONFIG_DK:
		ret = cfg_set_dk(attr, data, size);
		break;
	case CFG_SET_CONFIG_FLD:
		ret = cfg_set_fld(attr, data, size);
		break;
	case CFG_SET_CONFIG_LSD:
		ret = cfg_set_lsd(attr, data, size);
		break;
	case CFG_SET_CONFIG_OSD:
		ret = cfg_set_osd(attr, data);
		break;
	case CFG_SET_CONFIG_OSD_PM:
		ret = cfg_set_osd_pm(attr, data, size);
		break;
	case CFG_SET_CONFIG_NOTIFY:
		ret = cfg_set_notify(attr, data, size);
		break;
	default:
		assert(0);
		break;
	}
	pthread_mutex_unlock(&g_cfg_mutex);

	return ret;
}

INT32 CFG_getConfig(CFG_GET_CONFIG_E cmd, CFG_ATTR_S *attr, void *data, int size)
{
	INT32 ret = 0;
	CFG_ALL_S *cfg = &g_config;

	pthread_mutex_lock(&g_cfg_mutex);
	switch (cmd) {
	case CFG_GET_CONFIG_VIN:
		if (size != sizeof(CFG_VIN_S)) {
			SYS_TRACE("CFG_GET_CONFIG_VIN size %d / %d dismatch\n", size, sizeof(CFG_VIN_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_VIN group %d out of range\n", attr->group);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_vin, size);
		break;
	case CFG_GET_CONFIG_VCHN:
		if (size != sizeof(CFG_VCHN_S)) {
			SYS_TRACE("CFG_GET_CONFIG_VCHN size %d / %d dismatch\n", size, sizeof(CFG_VCHN_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_VCHN group %d out of range\n", attr->group);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id], size);
		break;
	case CFG_GET_CONFIG_VENC:
		if (size != sizeof(CFG_VENC_S)) {
			SYS_TRACE("CFG_GET_CONFIG_VENC size %d / %d dismatch\n", size, sizeof(CFG_VENC_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_VENC group %d out of range\n", attr->group);
			ret = -1;
			break;
		}
		if (attr->id >= MAX_VENC_STREAM) {
			SYS_TRACE("CFG_GET_CONFIG_VENC id %d out of range\n", attr->id);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_venc[attr->id], size);
		break;
	case CFG_GET_CONFIG_IMG:
		if (size != sizeof(CFG_IMG_S)) {
			SYS_TRACE("CFG_GET_CONFIG_IMG size %d / %d dismatch\n", size, sizeof(CFG_IMG_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_IMG id %d out of range\n", attr->id);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_img, size);
		break;
	case CFG_GET_CONFIG_ADV_IMG:
		if (size != sizeof(CFG_ADV_IMG_S)) {
			SYS_TRACE("CFG_GET_CONFIG_ADV_IMG size %d / %d dismatch\n", size, sizeof(CFG_ADV_IMG_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_ADV_IMG id %d out of range\n", attr->id);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_adv_img, size);
		break;
	case CFG_GET_CONFIG_AWB:
		if (size != sizeof(CFG_AWB_S)) {
			SYS_TRACE("CFG_GET_CONFIG_AWB size %d / %d dismatch\n", size, sizeof(CFG_AWB_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_AWB id %d out of range\n", attr->id);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_awb, size);
		break;
	case CFG_GET_CONFIG_COLOR:
		if (size != sizeof(CFG_COLOR_S)) {
			SYS_TRACE("CFG_GET_CONFIG_COLOR size %d / %d dismatch\n", size, sizeof(CFG_COLOR_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_COLOR id %d out of range\n", attr->id);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_color, size);
		break;
	case CFG_GET_CONFIG_ANTI_FLICKER:
		if (size != sizeof(AGTX_ANTI_FLICKER_CONF_S)) {
			SYS_TRACE("CFG_GET_CONFIG_ANTI_FLICKER size %d / %d dismatch\n", size, sizeof(AGTX_ANTI_FLICKER_CONF_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_ANTI_FLICKER id %d out of range\n", attr->id);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_anti_flicker, size);
		break;
	case CFG_GET_CONFIG_TD:
		if (size != sizeof(CFG_IVA_TD_S)) {
			SYS_TRACE("CFG_GET_CONFIG_TD size %d / %d dismatch\n", size, sizeof(CFG_IVA_TD_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_TD id %d out of range\n", attr->id);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].td, size);
		break;
	case CFG_GET_CONFIG_MD:
		if (size != sizeof(CFG_IVA_MD_S)) {
			SYS_TRACE("CFG_GET_CONFIG_MD size %d / %d dismatch\n", size, sizeof(CFG_IVA_MD_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_MD id %d out of range\n", attr->id);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].md, size);
		break;
	case CFG_GET_CONFIG_AROI:
		if (size != sizeof(CFG_IVA_AROI_S)) {
			SYS_TRACE("CFG_GET_CONFIG_AROI size %d / %d dismatch\n", size, sizeof(CFG_IVA_AROI_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_AROI id %d out of range\n", attr->id);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].aroi, size);
		break;
	case CFG_GET_CONFIG_PD:
		if (size != sizeof(CFG_IVA_PD_S)) {
			SYS_TRACE("CFG_GET_CONFIG_PD size %d / %d dismatch\n", size, sizeof(CFG_IVA_PD_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_PD id %d out of range\n", attr->id);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].pd, size);
		break;
	case CFG_GET_CONFIG_OD:
		if (size != sizeof(CFG_IVA_OD_S)) {
			SYS_TRACE("CFG_GET_CONFIG_OD size %d / %d dismatch\n", size, sizeof(CFG_IVA_OD_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_OD id %d out of range\n", attr->id);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].od, size);
		break;
	case CFG_GET_CONFIG_RMS:
		if (size != sizeof(CFG_IVA_RMS_S)) {
			SYS_TRACE("CFG_GET_CONFIG_RMS size %d / %d dismatch\n", size, sizeof(CFG_IVA_RMS_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_RMS id %d out of range\n", attr->id);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].rms, size);
		break;
	case CFG_GET_CONFIG_EF:
		if (size != sizeof(CFG_IVA_EF_S)) {
			SYS_TRACE("CFG_GET_CONFIG_EF size %d / %d dismatch\n", size, sizeof(CFG_IVA_EF_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_EF id %d out of range\n", attr->id);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].ef, size);
		break;
	case CFG_GET_CONFIG_VDBG:
		if (size != sizeof(CFG_VDBG_S)) {
			SYS_TRACE("CFG_GET_CONFIG_VDBG size %d / %d dismatch\n", size, sizeof(CFG_VDBG_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_VDBG id %d out of range\n", attr->id);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].vdbg, size);
		break;
	case CFG_GET_CONFIG_PTZ:
		if (size != sizeof(CFG_PTZ_S)) {
			SYS_TRACE("CFG_GET_CONFIG_PTZ size %d / %d dismatch\n", size, sizeof(CFG_PTZ_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_PTZ id %d out of range\n", attr->id);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_vin.ptz, size);
		break;
	case CFG_GET_CONFIG_SHD:
		if (size != sizeof(CFG_IVA_SHD_S)) {
			SYS_TRACE("CFG_GET_CONFIG_SHD size %d / %d dismatch\n", size, sizeof(CFG_IVA_SHD_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_SHD id %d out of range\n", attr->id);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].shd, size);
		break;
	case CFG_GET_CONFIG_EAIF:
		if (size != sizeof(CFG_IVA_EAIF_S)) {
			SYS_TRACE("CFG_GET_CONFIG_EAIF size %d / %d dismatch\n", size, sizeof(CFG_IVA_EAIF_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_EAIF id %d out of range\n", attr->id);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].eaif, size);
		break;
	case CFG_GET_CONFIG_PFM:
		if (size != sizeof(CFG_IVA_PFM_S)) {
			SYS_TRACE("CFG_GET_CONFIG_PFM size %d / %d dismatch\n", size, sizeof(CFG_IVA_PFM_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_PFM id %d out of range\n", attr->id);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].pfm, size);
		break;
	case CFG_GET_CONFIG_BM:
		if (size != sizeof(CFG_IVA_BM_S)) {
			SYS_TRACE("CFG_GET_CONFIG_BM size %d / %d dismatch\n", size, sizeof(CFG_IVA_BM_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_BM id %d out of range\n", attr->id);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].bm, size);
		break;
	case CFG_GET_CONFIG_DK:
		if (size != sizeof(CFG_IVA_DK_S)) {
			SYS_TRACE("CFG_GET_CONFIG_DK size %d / %d dismatch\n", size, sizeof(CFG_IVA_DK_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_DK id %d out of range\n", attr->id);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].dk, size);
		break;
	case CFG_GET_CONFIG_FLD:
		if (size != sizeof(CFG_IVA_FLD_S)) {
			SYS_TRACE("CFG_GET_CONFIG_FLD size %d / %d dismatch\n", size, sizeof(CFG_IVA_FLD_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_FLD id %d out of range\n", attr->id);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_vin.cfg_vchn[attr->id].fld, size);
		break;
	case CFG_GET_CONFIG_LSD:
		if (size != sizeof(CFG_IAA_LSD_S)) {
			SYS_TRACE("CFG_IAA_LSD_S size %d / %d dismatch\n", size, sizeof(CFG_IAA_LSD_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_IAA_LSD_S id %d out of range\n", attr->group);
			ret = -1;
			break;
		}
		memcpy(data, &cfg->cfg_camera[attr->group].cfg_vin.lsd, size);
		break;
	case CFG_GET_CONFIG_OSD:
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_OSD id %d out of range\n", attr->id);
			ret = -1;
			break;
		}
		AGTX_OSD_CONF_S *cfg_osd = (AGTX_OSD_CONF_S *)data;
		for (int i = 0; i < 4; i++) {
			memcpy(&cfg_osd->strm[i], &g_config.cfg_camera[attr->group].cfg_vin.cfg_vchn[i].osd,
			       sizeof(AGTX_OSD_CONF_OUTER_S));
		}
		break;
	case CFG_GET_CONFIG_OSD_PM:
		if (size != sizeof(CFG_OSD_PM_S)) {
			SYS_TRACE("CFG_OSD_PM_S size %d / %d mismatch\n", size, sizeof(CFG_OSD_PM_S));
			ret = -1;
			break;
		}
		if (attr->group >= MAX_VIDEO_INPUT) {
			SYS_TRACE("CFG_GET_CONFIG_OSD_PM id %d out of range\n", attr->id);
			ret = -1;
			break;
		}
		AGTX_OSD_PM_CONF_S *cfg_osd_pm = (AGTX_OSD_PM_CONF_S *)data;
		for (int i = 0; i < 4; i++) {
			memcpy(&cfg_osd_pm->conf[i], &g_config.cfg_camera[attr->group].cfg_vin.cfg_vchn[i].osd_pm,
			       sizeof(AGTX_OSD_PM_PARAM_S));
		}
		break;
	default:
		break;
	}
	pthread_mutex_unlock(&g_cfg_mutex);

	return ret;
}
/**
 * @brief start & apply config to modules.
 * @return The execution result.
 */
INT32 CFG_startAllModule()
{
	INT32 ret = 0;

	/*if config not ready,do nothing*/
	if (!g_video_init) {
		pthread_mutex_lock(&g_cfg_mutex);
		ret = cfgStartVideo();
		g_video_init = 1;
		pthread_mutex_unlock(&g_cfg_mutex);
	}

	return ret;
}

/**
 * @brief close all modules.
 * @return The execution result.
 */
INT32 CFG_closeAllModule()
{
	/*if video not start,do nothing*/
	if (g_video_init) {
		pthread_mutex_lock(&g_cfg_mutex);
		cfgStopVideo();
		g_video_init = 0;
		pthread_mutex_unlock(&g_cfg_mutex);
	}

	return 0;
}

/**
 * @brief config initial.
 * @return The execution result.
 */
INT32 CFG_init()
{
	INT32 ret = 0;
	g_cfg_init = 1;
	pthread_mutex_lock(&g_cfg_mutex);
	ret = cfgInitSys();
	pthread_mutex_unlock(&g_cfg_mutex);
	return ret;
}

/**
 * @brief config initial.
 * @return The execution result.
 */
INT32 CFG_exit()
{
	INT32 ret = 0;
	g_cfg_init = 0;
	pthread_mutex_lock(&g_cfg_mutex);
	cfgExitSys();
	pthread_mutex_unlock(&g_cfg_mutex);
	return ret;
}
