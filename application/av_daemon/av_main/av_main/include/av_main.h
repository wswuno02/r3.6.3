/******************************************************************************
*
* copyright (c) AUGENTIX Inc. - All Rights Reserved
*
* Unauthorized copying of this file, via any medium is strictly prohibited.
*
* Proprietary and confidential.
*
******************************************************************************/
#ifndef AUGENTIX_AV_MAIN_H_
#define AUGENTIX_AV_MAIN_H_

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

#define CC_SOCKET_PATH "/tmp/ccUnxSkt"
#define TD_SOCKET_PATH "/tmp/iva_td_skt"
#define MD_SOCKET_PATH "/tmp/iva_md_skt"
#define ONVIF_EVENT_PATH "/tmp/event_onvif"
#define AV_MAIN_RECV_DONE_MASK (((0xFFFFFFFF) << (32 - AV_MAIN_RECV_DONE_NUM)) >> (32 - AV_MAIN_RECV_DONE_NUM))
#define AV_MAIN_RECV_DIP_LOW_LV_DONE_MASK                                                                              \
	(((0xFFFFFFFF) << (32 - AV_MAIN_RECV_DIP_LOW_LV_DONE_NUM)) >> (32 - AV_MAIN_RECV_DIP_LOW_LV_DONE_NUM))
#define AV_MAIN_RECV_DIP_HIGH_LV_DONE_MASK                                                                             \
	(((0xFFFFFFFF) << (32 - AV_MAIN_RECV_DIP_HIGH_LV_DONE_NUM)) >> (32 - AV_MAIN_RECV_DIP_HIGH_LV_DONE_NUM))

/**
 * @brief Enumeration of recv config flag.
 */
typedef enum {
	AV_MAIN_RECV_VIN_DONE = 0,
	AV_MAIN_RECV_LAYOUT_DONE,
	AV_MAIN_RECV_VCHN_DONE,
	AV_MAIN_RECV_ENC_DONE,
	AV_MAIN_RECV_TD_DONE,
	AV_MAIN_RECV_MD_DONE,
	AV_MAIN_RECV_AROI_DONE,
	AV_MAIN_RECV_PD_DONE,
	AV_MAIN_RECV_OD_DONE,
	AV_MAIN_RECV_RMS_DONE,
	AV_MAIN_RECV_LD_DONE,
	AV_MAIN_RECV_EF_DONE,
	AV_MAIN_RECV_VDBG_DONE,
	AV_MAIN_RECV_PTZ_DONE,
	AV_MAIN_RECV_SHD_DONE,
	AV_MAIN_RECV_EAIF_DONE,
	AV_MAIN_RECV_PFM_DONE,
	AV_MAIN_RECV_BM_DONE,
	AV_MAIN_RECV_DK_DONE,
	AV_MAIN_RECV_FLD_DONE,
	AV_MAIN_RECV_LSD_DONE,
	AV_MAIN_RECV_OSD_DONE,
	AV_MAIN_RECV_OSD_PM_DONE,
	AV_MAIN_RECV_AUDIO_DONE,
	AV_MAIN_RECV_LDC_DONE,
	AV_MAIN_RECV_PANORAMA_DONE,
	AV_MAIN_RECV_PANNING_DONE,
	AV_MAIN_RECV_SURROUND_DONE,
	AV_MAIN_RECV_DONE_NUM,
} AV_MAIN_RECV_DONE_E;

/**
 * @brief Enumeration of recv config flag.
 */
typedef enum {
	AV_MAIN_RECV_CAL_DONE,
	AV_MAIN_RECV_DBC_DONE,
	AV_MAIN_RECV_DCC_DONE,
	AV_MAIN_RECV_LSC_DONE,
	AV_MAIN_RECV_CTRL_DONE,
	AV_MAIN_RECV_AE_DONE,
	AV_MAIN_RECV_ISO_DONE,
	AV_MAIN_RECV_AWB_DONE,
	AV_MAIN_RECV_PTA_DONE,
	AV_MAIN_RECV_CSM_DONE,
	AV_MAIN_RECV_SHP_DONE,
	AV_MAIN_RECV_NR_DONE,
	AV_MAIN_RECV_ROI_DONE,
	AV_MAIN_RECV_TE_DONE,
	AV_MAIN_RECV_GAMMA_DONE,
	AV_MAIN_RECV_SHP_WIN_DONE,
	AV_MAIN_RECV_NR_WIN_DONE,
	AV_MAIN_RECV_ENH_DONE,
	AV_MAIN_RECV_CORING_DONE,
	AV_MAIN_RECV_FCS_DONE,
	AV_MAIN_RECV_STAT_DONE,
	AV_MAIN_RECV_HDR_SYNTH_DONE,
	AV_MAIN_RECV_DIP_LOW_LV_DONE_NUM,
	AV_MAIN_RECV_IMG_PREF_DONE,
	AV_MAIN_RECV_ADV_IMG_PREF_DONE,
	AV_MAIN_RECV_AWB_PREF_DONE,
	AV_MAIN_RECV_COLOR_CONF_DONE,
	AV_MAIN_RECV_ANTI_FLICKKER_CONF_DONE,
	AV_MAIN_RECV_DIP_HIGH_LV_DONE_NUM,
} AV_MAIN_RECV_DIP_DONE_E;

typedef struct av_main_control_ {
	int video_init;
	int td_fd;
	int md_fd;
	int onvif_event_fd;
	int strm_cnt;
} av_main_ctrl;

/* Interface function prototype */

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* AUGENTIX_AV_MAIN_ */
