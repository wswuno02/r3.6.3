#include "mtk_common.h"
#include "av_main.h"
#include "config_api.h"
#include "vftr_update.h"
#include "rtsp_api.h"
#include "mpi_base_types.h"
#include "agtx_event_conf.h"
#include "avftr_td.h"
#include "avftr_md.h"
#include "video_dip.h"
#include "video_api.h"
#include "avftr_conn.h"
#include "osd.h"
#include <pthread.h>
#include <sys/file.h>
#include "agtx_prio.h"

#include "agtx_iva.h"
#include "agtx_video.h"
#include "agtx_audio.h"
#include "agtx_cmd.h"
#include "agtx_osd.h"
#include "agtx_color_conf.h"

#include <execinfo.h>
#include <assert.h>

#define LOCK_FILE_PATH "/tmp/av_main.lock"
#define AVMAIN_RDY_FILE "/tmp/avmain_ready"

//#define ENABLE_RTSPSERVER

int g_lock_fd = -1;
pthread_mutex_t g_video_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t g_video_cond = PTHREAD_COND_INITIALIZER;
av_main_ctrl g_av_main_ctrl = { 0 };

AVFTR_MD_ALARM_CB g_md_cb = NULL;
AVFTR_TD_ALARM_CB g_td_cb = NULL;

static int avmain_checkSingleInstance(void)
{
	int ret = 0;
	int lock_fd;
	const char *lock_file_path = LOCK_FILE_PATH;

	lock_fd = open(lock_file_path, O_CREAT | O_RDWR, 0600);
	if (lock_fd < 0) {
		SYS_TRACE("Open lock file %s failed\n", lock_file_path);
		return -1;
	}

	ret = flock(lock_fd, LOCK_EX | LOCK_NB);
	if (ret) {
		if (EWOULDBLOCK == errno) {
			SYS_TRACE("av_main is already running ...!\n");
			SYS_TRACE("To restart it, you have to kill the program and delete the lock file %s\n",
			          lock_file_path);
			close(lock_fd);
			return -1;
		}
	} else {
		SYS_TRACE("Starting av_main ...\n");
	}

	g_lock_fd = lock_fd;

	return 0;
}

static void avmain_removeSingleInstance(void)
{
	int lock_fd = g_lock_fd;

	if (lock_fd >= 0) {
		flock(lock_fd, LOCK_UN);
		close(lock_fd);
	}

	g_lock_fd = -1;
}

static void avmain_handle_sig_crash(int signo)
{
	FILE *out;
	char **symbollist;
	void *addrlist[64] = { 0 }; // storage array for stack trace address data

	out = fopen("/dev/console", "w+");

	if (out <= 0) {
		exit(0);
	}

	fprintf(out, "program stop\n");

	// retrieve current stack addresses
	int addrlen = backtrace(addrlist, sizeof(addrlist) / sizeof(void *));

	if (addrlen == 0) {
		exit(0);
	}

	// create readable strings to each frame.
	symbollist = backtrace_symbols(addrlist, addrlen);

	// print the stack trace.
	for (int i = 0; i < addrlen; i++) {
		fprintf(out, "%s\n", symbollist[i]);
	}

	free(symbollist);

	avmain_removeSingleInstance();
	exit(0);
}

static void avmain_handle_sig_int(int signo)
{
	if (signo == SIGINT) {
		printf("Caught SIGINT!\n");
	} else if (signo == SIGTERM) {
		printf("Caught SIGTERM!\n");
	} else {
		perror("Unexpected signal!\n");
		exit(1);
	}

	signal(SIGINT, SIG_DFL);
	pthread_mutex_destroy(&g_video_mutex);
	pthread_cond_destroy(&g_video_cond);

	/*close rtsp*/
	VFTR_UPDATE_exit();
#ifdef ENABLE_RTSPSERVER
	RTSP_exit();
#endif /* !ENABLE_RTSPSERVER */

	/*close video system*/
	CFG_closeAllModule(); //TODO need move to other source file not in config
	CFG_exit();

	if (AVFTR_exitServer() < 0) {
		perror("Exit AVFTR server failed\n");
	}

	avmain_removeSingleInstance();
	exit(0);
}

void avmain_send_onvif_event(AGTX_SW_EVENT_TRIG_TYPE_E type)
{
	INT32 ret = 0;
	INT32 sockfd = -1;
	INT32 servlen = 0;
	struct sockaddr_un serv_addr;
	int alarm = 0;
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;

	if (av_ctrl->onvif_event_fd <= 0) {
		/*Connect to onvif*/
		bzero((char *)&serv_addr, sizeof(serv_addr));
		serv_addr.sun_family = AF_UNIX;
		strcpy(serv_addr.sun_path, ONVIF_EVENT_PATH);
		servlen = strlen(serv_addr.sun_path) + sizeof(serv_addr.sun_family);

		if ((sockfd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
			SYS_TRACE("Create ONVIF sockfd failed %d(%m)\n", errno);
			sockfd = -1;
		}

		if (sockfd > 0) {
			fcntl(sockfd, F_SETFL, O_NONBLOCK);

			if (connect(sockfd, (struct sockaddr *)&serv_addr, servlen) < 0) {
			//	SYS_TRACE("Connecting to ONVIF server failed %d(%m)\n", errno);
				close(sockfd);
				sockfd = -1;
				return;
			} else {
				av_ctrl->onvif_event_fd = sockfd;
			}
		}

		alarm = type;
		ret = write(av_ctrl->onvif_event_fd, &alarm, sizeof(alarm));
		if (ret < 0) {
			SYS_TRACE("write socket error %d(%m)\n", errno);
			return;
		}
		//		close(sockfd);
	} else {
		alarm = type;
		ret = write(av_ctrl->onvif_event_fd, &alarm, sizeof(alarm));
		if (ret < 0) {
			SYS_TRACE("write socket error %d(%m)\n", errno);
			close(av_ctrl->onvif_event_fd);
			av_ctrl->onvif_event_fd = -1;
			return;
		}
	}
}

void avmain_td_cb()
{
	INT32 ret = 0;
	static time_t duration;
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;
	AGTX_SW_EVENT_RULE_S rule = { 0 };

	time_t t = time(0);

	if (time(0) < duration + 2) {
		return;
	}

	rule.trigger_type = AGTX_SW_EVENT_TRIG_TYPE_IVA_TD_POSITIVE;
	if (av_ctrl->td_fd > 0) {
		ret = write(av_ctrl->td_fd, &rule, sizeof(rule));
		if (ret < 0) {
			/* This condition happened if eventd SW thread disabled */
			if (errno == EAGAIN) {
				return;
			}

			SYS_TRACE("write socket error %d(%m)\n", errno);
			return;
		}
	}

	duration = t;

	/*FIXME temp solution for show alarm icons*/
	showIvaAlarm();

	return;
}

void avmain_md_cb(uint8_t alarm)
{
	INT32 ret = 0;
	static time_t duration;
	static uint32_t prev_alarm = 0;
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;
	AGTX_SW_EVENT_RULE_S rule = { 0 };

	time_t t = time(0);

	if (prev_alarm ^ alarm) { //edge trigger
		//ONVIF
		avmain_send_onvif_event(alarm ? AGTX_SW_EVENT_TRIG_TYPE_IVA_MD_POSITIVE :
		                                AGTX_SW_EVENT_TRIG_TYPE_IVA_MD_NEGATIVE);
		prev_alarm = alarm;
	}

	if (time(0) < duration + 2) {
		return;
	}

	duration = t;

	if (alarm) { // level trigger
		rule.trigger_type = AGTX_SW_EVENT_TRIG_TYPE_IVA_MD_POSITIVE;
		if (av_ctrl->md_fd > 0) {
			ret = write(av_ctrl->md_fd, &rule, sizeof(rule));
			if (ret < 0) {
				/* This condition happened if eventd SW thread disabled */
				if (errno == EAGAIN) {
					return;
				}

				SYS_TRACE("write socket error %d(%m)\n", errno);
				return;
			}
		}
		/*FIXME temp solution for show alarm icons*/
		showIvaAlarm();
	}

	return;
}

/**
 * @brief connect to event demon.
 * @return The execution result.
 */
INT32 avmain_connect_to_event_demon()
{
	INT32 sockfd = -1;
	INT32 servlen = 0;
	INT32 dev_idx = 0;
	INT32 chn_idx = 0;
	struct sockaddr_un serv_addr;
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;

	av_ctrl->td_fd = -1;
	av_ctrl->md_fd = -1;

	/*Connect to event demon for TD*/
	bzero((char *)&serv_addr, sizeof(serv_addr));
	serv_addr.sun_family = AF_UNIX;
	strcpy(serv_addr.sun_path, TD_SOCKET_PATH);
	servlen = strlen(serv_addr.sun_path) + sizeof(serv_addr.sun_family);

	if ((sockfd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
		SYS_TRACE("Create TD sockfd failed %d(%m)\n", errno);
		sockfd = -1;
	}

	if (sockfd > 0) {
		fcntl(sockfd, F_SETFL, O_NONBLOCK);

		if (connect(sockfd, (struct sockaddr *)&serv_addr, servlen) < 0) {
			SYS_TRACE("Connecting to TD server failed %d(%m)\n", errno);
			close(sockfd);
			sockfd = -1;
		} else {
			av_ctrl->td_fd = sockfd;
		}
	}

	if (AVFTR_TD_regCallback(MPI_VIDEO_WIN(dev_idx, chn_idx, 0), &avmain_td_cb) < 0) {
		SYS_TRACE("VIDEO_FTR_regTdCallback failed. \n");
		close(av_ctrl->td_fd);
		av_ctrl->td_fd = -1;
	}
	g_td_cb = &avmain_td_cb;

	/*Connect to event demon for MD*/
	bzero((char *)&serv_addr, sizeof(serv_addr));
	serv_addr.sun_family = AF_UNIX;
	strcpy(serv_addr.sun_path, MD_SOCKET_PATH);
	servlen = strlen(serv_addr.sun_path) + sizeof(serv_addr.sun_family);

	if ((sockfd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
		SYS_TRACE("Create MD sockfd failed %d(%m)\n", errno);
		sockfd = -1;
	}

	if (sockfd > 0) {
		fcntl(sockfd, F_SETFL, O_NONBLOCK);

		if (connect(sockfd, (struct sockaddr *)&serv_addr, servlen) < 0) {
			SYS_TRACE("Connecting to MD server failed %d(%m)\n", errno);
			close(sockfd);
			sockfd = -1;
		} else {
			av_ctrl->md_fd = sockfd;
		}
	}

	if (AVFTR_MD_regCallback(MPI_VIDEO_WIN(dev_idx, chn_idx, 0), &avmain_md_cb) < 0) {
		SYS_TRACE("AVFTR_MD_regCallback failed. \n");
		close(av_ctrl->md_fd);
		av_ctrl->md_fd = -1;
	}
	g_md_cb = &avmain_md_cb;

	return 0;
}

static INT32 avmain_getVencType(INT32 venc_type)
{
	INT32 type = 0;

	switch (venc_type) {
	case AGTX_VENC_TYPE_H264:
		type = CFG_VENC_TYPE_H264;
		break;
	case AGTX_VENC_TYPE_H265:
		type = CFG_VENC_TYPE_H265;
		break;
	case AGTX_VENC_TYPE_MJPEG:
		type = CFG_VENC_TYPE_MJPEG;
		break;
	default:
		type = CFG_VENC_TYPE_H264;
		break;
	}

	return type;
}

static INT32 avmain_getVencProfile(INT32 venc_profile)
{
	INT32 profile = 0;

	switch (venc_profile) {
	case AGTX_PRFL_BASELINE:
		profile = CFG_H264_5_PROFILE_BASELINE;
		break;
	case AGTX_PRFL_MAIN:
		profile = CFG_H264_5_PROFILE_MAIN;
		break;
	case AGTX_PRFL_HIGH:
		profile = CFG_H264_5_PROFILE_HIGH;
		break;
	default:
		profile = CFG_H264_5_PROFILE_BASELINE;
		break;
	}

	return profile;
}

static INT32 avmain_getVencRcMode(INT32 rc_mode)
{
	INT32 mode = 0;

	switch (rc_mode) {
	case AGTX_RC_MODE_VBR:
		mode = CFG_VENC_RC_MODE_VBR;
		break;
	case AGTX_RC_MODE_CBR:
		mode = CFG_VENC_RC_MODE_CBR;
		break;
	case AGTX_RC_MODE_SBR:
		mode = CFG_VENC_RC_MODE_SBR;
		break;
	case AGTX_RC_MODE_CQP:
		mode = CFG_VENC_RC_MODE_CQP;
		break;
	default:
		mode = CFG_VENC_RC_MODE_CBR;
		break;
	}

	return mode;
}

static INT32 avmain_setInputFps(int stream_num, int width, int height)
{
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VIN_S cfg_vin = { 0 };

	/*Set Input fps*/
	cfg_attr.group = 0;
	cfg_attr.id = 0;
	if (CFG_getConfig(CFG_GET_CONFIG_VIN, &cfg_attr, (char *)&cfg_vin, sizeof(CFG_VIN_S)) < 0) {
		return -1;
	}

	INT32 ret = VIDEO_getInputFps(MPI_VIDEO_DEV(0), &cfg_vin.fps);
	if (ret) {
		return ret;
	}

	if (CFG_setConfig(CFG_SET_CONFIG_VIN, &cfg_attr, (char *)&cfg_vin, sizeof(CFG_VIN_S)) < 0) {
		return -1;
	}

	return 0;
}

/**
 * @brief convert stitch parameter to configuration parameter.
 * @return The execution result.
 */
static void avmain_agtx_stitch_to_cfg(CFG_STITCH_S *attr, const AGTX_STITCH_CONF_S *stitch_attr)
{
	INT32 i = 0;

	attr->enable = stitch_attr->enable;
	attr->dft_dist = stitch_attr->dft_dist;
	attr->table_num = AGTX_MAX_STITCH_TABLE_NUM;

	attr->center[0].x = stitch_attr->center_0_x;
	attr->center[0].y = stitch_attr->center_0_y;

	attr->center[1].x = stitch_attr->center_1_x;
	attr->center[1].y = stitch_attr->center_1_y;

	for (i = 0; i < STITCH_TABLE_NUM; i++) {
		attr->table[i].dist = stitch_attr->dist_tbl[i].dist;
		attr->table[i].ver_disp = stitch_attr->dist_tbl[i].ver_disp;
		attr->table[i].straighten = stitch_attr->dist_tbl[i].straighten;
		attr->table[i].src_zoom = stitch_attr->dist_tbl[i].src_zoom;

		attr->table[i].theta[0] = stitch_attr->dist_tbl[i].theta_0;
		attr->table[i].radius[0] = stitch_attr->dist_tbl[i].radius_0;
		attr->table[i].curvature[0] = stitch_attr->dist_tbl[i].curvature_0;
		attr->table[i].fov_ratio[0] = stitch_attr->dist_tbl[i].fov_ratio_0;
		attr->table[i].ver_scale[0] = stitch_attr->dist_tbl[i].ver_scale_0;
		attr->table[i].ver_shift[0] = stitch_attr->dist_tbl[i].ver_shift_0;

		attr->table[i].theta[1] = stitch_attr->dist_tbl[i].theta_1;
		attr->table[i].radius[1] = stitch_attr->dist_tbl[i].radius_1;
		attr->table[i].curvature[1] = stitch_attr->dist_tbl[i].curvature_1;
		attr->table[i].fov_ratio[1] = stitch_attr->dist_tbl[i].fov_ratio_1;
		attr->table[i].ver_scale[1] = stitch_attr->dist_tbl[i].ver_scale_1;
		attr->table[i].ver_shift[1] = stitch_attr->dist_tbl[i].ver_shift_1;
	}
}

/**
 * @brief set device configuration.
 * @return The execution result.
 */
INT32 avmain_set_dev_config(INT32 sockfd, INT32 len)
{
	INT32 i = 0;
	INT32 ret = 0;
	AGTX_DEV_CONF_S dev = { 0 };
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VIN_S cfg_vin = { 0 };
	CFG_PATH_S *cfg_path = NULL;
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;

	if (len < 0) {
		SYS_TRACE("AGTX_CMD_VIDEO_DEV_CONF request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_DEV_CONF_S)) {
		SYS_TRACE("AGTX_CMD_VIDEO_DEV_CONF size doesn't match %d / %d\n", len, sizeof(AGTX_DEV_CONF_S));
		return -1;
	}

	ret = read(sockfd, &dev, sizeof(AGTX_DEV_CONF_S));
	if (ret != sizeof(AGTX_DEV_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	if (av_ctrl->video_init) {
		/*do nothing before config initial*/
		CFG_closeAllModule();
	}

	cfg_attr.group = 0;
	cfg_attr.id = 0;
	if (CFG_getConfig(CFG_GET_CONFIG_VIN, &cfg_attr, (char *)&cfg_vin, sizeof(CFG_VIN_S)) < 0) {
		return -1;
	}

	cfg_vin.enable = 1;
	cfg_vin.dev_idx = dev.video_dev_idx;
	cfg_vin.hdr_mode = dev.hdr_mode;
	cfg_vin.stitch_en = dev.stitch_en;
	cfg_vin.eis_en = dev.eis_en;
	cfg_vin.bayer = (dev.bayer == AGTX_BAYER_G0) ?
	                        CFG_BAYER_TYPE_G0 :
	                        (dev.bayer == AGTX_BAYER_R) ?
	                        CFG_BAYER_TYPE_R :
	                        (dev.bayer == AGTX_BAYER_B) ?
	                        CFG_BAYER_TYPE_B :
	                        (dev.bayer == AGTX_BAYER_G1) ? CFG_BAYER_TYPE_G1 : CFG_BAYER_TYPE_R;
	cfg_vin.fps = dev.input_fps; /*change at strm config setting*/

	for (i = 0; i < AGTX_MAX_INPUT_PATH_NUM; i++) {
		cfg_path = &cfg_vin.cfg_path[i];
		cfg_path->enable = dev.input_path[i].path_en;
		cfg_path->path_idx = dev.input_path[i].path_idx;
		cfg_path->sensor_idx = dev.input_path[i].sensor_idx;
		cfg_path->width = dev.input_path[i].width;
		cfg_path->height = dev.input_path[i].height;
	}

	if (CFG_setConfig(CFG_SET_CONFIG_VIN, &cfg_attr, (char *)&cfg_vin, sizeof(CFG_VIN_S)) < 0) {
		return -1;
	}

	if (av_ctrl->video_init) {
		/*do nothing before config initial*/
		ret = CFG_startAllModule();
	}

	return ret;
}

/**
 * @brief set layout configuration.
 * @return The execution result.
 */
INT32 avmain_set_layout_config(INT32 sockfd, INT32 len)
{
	INT32 i = 0;
	INT32 ret = 0;
	AGTX_LAYOUT_CONF_S layout = { 0 };
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VCHN_S cfg_vchn = { 0 };
	static INT32 restart_video = 0;
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;

	if (len < 0) {
		SYS_TRACE("AGTX_LAYOUT_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_LAYOUT_CONF_S)) {
		SYS_TRACE("AGTX_CMD_VIDEO_STRM_CONF size doesn't match %d / %d\n", len, sizeof(AGTX_LAYOUT_CONF_S));
		return -1;
	}

	ret = read(sockfd, &layout, sizeof(AGTX_LAYOUT_CONF_S));
	if (ret != sizeof(AGTX_LAYOUT_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}


	/*do nothing before config initial*/
	if (restart_video && av_ctrl->video_init) {
		CFG_closeAllModule();
	}

	for (i = 0; i < layout.layout_num; i++) {
		cfg_attr.group = layout.video_dev_idx;
		cfg_attr.id = layout.video_layout[i].video_strm_idx;

		if (CFG_getConfig(CFG_GET_CONFIG_VCHN, &cfg_attr, (char *)&cfg_vchn, sizeof(CFG_VCHN_S)) < 0) {
			continue;
		}

		cfg_vchn.layout_en = layout.layout_en;
		memcpy(&cfg_vchn.layout, &layout.video_layout[i], sizeof(AGTX_LAYOUT_PARAM_S));

		if (CFG_setConfig(CFG_GET_CONFIG_VCHN, &cfg_attr, (char *)&cfg_vchn, sizeof(CFG_VCHN_S)) < 0) {
			continue;
		}
	}

	/*do nothing before config initial*/
	if (restart_video && av_ctrl->video_init) {
		ret = CFG_startAllModule();
	}


	/* For on-the-fly change layout config */
	restart_video = 1;

	return ret;
}

/**
 * @brief set stream configuration.
 * @return The execution result.
 */
INT32 avmain_set_strm_config(INT32 sockfd, INT32 len)
{
	INT32 i = 0;
	INT32 ret = 0;
	INT32 stream_num = 0;
	INT32 restart_video = 0;
	AGTX_STRM_CONF_S strm = { 0 };
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VCHN_S cfg_vchn = { 0 };
	CFG_VENC_S cfg_venc = { 0 };
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;

	if (len < 0) {
		SYS_TRACE("AGTX_STRM_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_STRM_CONF_S)) {
		SYS_TRACE("AGTX_CMD_VIDEO_STRM_CONF size doesn't match %d / %d\n", len, sizeof(AGTX_STRM_CONF_S));
		return -1;
	}

	ret = read(sockfd, &strm, sizeof(AGTX_STRM_CONF_S));
	if (ret != sizeof(AGTX_STRM_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	strm.video_strm_cnt =
	        (strm.video_strm_cnt >= AGTX_MAX_VIDEO_STRM_NUM) ? AGTX_MAX_VIDEO_STRM_NUM : strm.video_strm_cnt;

	av_ctrl->strm_cnt = strm.video_strm_cnt;

	for (i = 0; i < strm.video_strm_cnt; i++) {
		cfg_attr.group = 0;
		cfg_attr.id = i;

		if (CFG_getConfig(CFG_GET_CONFIG_VCHN, &cfg_attr, (char *)&cfg_vchn, sizeof(CFG_VCHN_S)) < 0) {
			continue;
		}

		if (CFG_getConfig(CFG_GET_CONFIG_VENC, &cfg_attr, (char *)&cfg_venc, sizeof(CFG_VENC_S)) < 0) {
			continue;
		}

		if (strm.video_strm[i].strm_en) {
			stream_num++;
		}

		if (cfg_venc.enable != strm.video_strm[i].strm_en || cfg_venc.width != strm.video_strm[i].width ||
		    cfg_venc.height != strm.video_strm[i].height ||
		    cfg_venc.venc_type != avmain_getVencType(strm.video_strm[i].venc_type) ||
		    cfg_venc.rc_mode != avmain_getVencRcMode(strm.video_strm[i].rc_mode) ||
		    cfg_venc.venc_profile != avmain_getVencProfile(strm.video_strm[i].venc_profile)) {
			restart_video = 1;
		}
	}

	/*do nothing before config initial*/
	if (restart_video && av_ctrl->video_init) {
		CFG_closeAllModule();
	}

	avmain_setInputFps(stream_num, strm.video_strm[0].width, strm.video_strm[0].height);

	for (i = 0; i < strm.video_strm_cnt; i++) {
		cfg_attr.group = 0;
		cfg_attr.id = i;
		if (CFG_getConfig(CFG_GET_CONFIG_VCHN, &cfg_attr, (char *)&cfg_vchn, sizeof(CFG_VCHN_S)) < 0) {
			continue;
		}

		if (CFG_getConfig(CFG_GET_CONFIG_VENC, &cfg_attr, (char *)&cfg_venc, sizeof(CFG_VENC_S)) < 0) {
			continue;
		}

		/*set vchn*/
		cfg_vchn.enable = (UINT32)strm.video_strm[i].strm_en;
		cfg_vchn.chn_idx = (UINT32)strm.video_strm[i].video_strm_idx;
		cfg_vchn.width = (UINT32)strm.video_strm[i].width;
		cfg_vchn.height = (UINT32)strm.video_strm[i].height;
		cfg_vchn.max_width = (UINT32)strm.video_strm[i].width;
		cfg_vchn.max_width = (UINT32)strm.video_strm[i].height;
		cfg_vchn.fps = (UINT32)strm.video_strm[i].output_fps;
		cfg_vchn.rotate = strm.video_strm[i].rotate;
		cfg_vchn.mirror = (UINT32)strm.video_strm[i].mirr_en;
		cfg_vchn.flip = (UINT32)strm.video_strm[i].flip_en;

		/* FIXME for testing window-based streaming flow*/
		//cfg_vchn.layout_en = 0;

		/*set venc*/
		cfg_venc.enable = (UINT32)strm.video_strm[i].strm_en;
		cfg_venc.chn_idx = (UINT32)strm.video_strm[i].video_strm_idx;
		cfg_venc.width = (UINT32)strm.video_strm[i].width;
		cfg_venc.height = (UINT32)strm.video_strm[i].height;
		cfg_venc.max_width = (UINT32)strm.video_strm[i].width;
		cfg_venc.max_height = (UINT32)strm.video_strm[i].height;
		cfg_venc.bind_dev_idx = (UINT32)strm.video_dev_idx;
		cfg_venc.bind_chn_idx = (UINT32)strm.video_strm[i].video_strm_idx;
		cfg_venc.fps = (UINT32)strm.video_strm[i].output_fps;
		cfg_venc.venc_type = avmain_getVencType(strm.video_strm[i].venc_type);
		cfg_venc.venc_profile = avmain_getVencProfile(strm.video_strm[i].venc_profile);
		cfg_venc.rc_mode = avmain_getVencRcMode(strm.video_strm[i].rc_mode);
		cfg_venc.gop = (UINT32)strm.video_strm[i].gop_size;
		cfg_venc.bitrate = (UINT32)strm.video_strm[i].bit_rate;
		cfg_venc.min_qp = (UINT32)strm.video_strm[i].min_qp;
		cfg_venc.max_qp = (UINT32)strm.video_strm[i].max_qp;
		cfg_venc.min_q_factor = (UINT32)strm.video_strm[i].min_q_factor;
		cfg_venc.max_q_factor = (UINT32)strm.video_strm[i].max_q_factor;
		cfg_venc.fluc_level = (UINT32)strm.video_strm[i].fluc_level;
		cfg_venc.regression_speed = (UINT32)strm.video_strm[i].regression_speed;
		cfg_venc.scene_smooth = (UINT32)strm.video_strm[i].scene_smooth;
		cfg_venc.i_continue_weight = (UINT32)strm.video_strm[i].i_continue_weight;
		cfg_venc.i_qp_offset = strm.video_strm[i].i_qp_offset;
		cfg_venc.vbr_max_bit_rate = (UINT32)strm.video_strm[i].vbr_max_bit_rate;
		cfg_venc.vbr_quality_level_index = (UINT32)strm.video_strm[i].vbr_quality_level_index;
		cfg_venc.sbr_adjust_br_thres_pc = (UINT32)strm.video_strm[i].sbr_adjust_br_thres_pc;
		cfg_venc.sbr_adjust_step_times = (UINT32)strm.video_strm[i].sbr_adjust_step_times;
		cfg_venc.sbr_converge_frame = (UINT32)strm.video_strm[i].sbr_converge_frame;
		cfg_venc.cqp_i_frame_qp = (UINT32)strm.video_strm[i].cqp_i_frame_qp;
		cfg_venc.cqp_p_frame_qp = (UINT32)strm.video_strm[i].cqp_p_frame_qp;
		cfg_venc.cqp_q_factor = (UINT32)strm.video_strm[i].cqp_q_factor;
		cfg_venc.obs = (UINT32)strm.video_strm[i].obs;
		cfg_venc.obs_off_period = (UINT32)strm.video_strm[i].obs_off_period;

		ret = CFG_setConfig(CFG_SET_CONFIG_VCHN, &cfg_attr, (char *)&cfg_vchn, sizeof(CFG_VCHN_S));
		if (ret < 0) {
			continue;
		}

		ret = CFG_setConfig(CFG_SET_CONFIG_VENC, &cfg_attr, (char *)&cfg_venc, sizeof(CFG_VENC_S));
		if (ret < 0) {
			continue;
		}
	}

	/*do nothing before config initial*/
	if (restart_video && av_ctrl->video_init) {
		ret = CFG_startAllModule();
	}

	return ret;
}

/**
 * @brief set stitch configuration.
 * @return The execution result.
 */
INT32 avmain_set_stitch_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_STITCH_CONF_S stitch = { 0 };
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;

	if (len < 0) {
		SYS_TRACE("AGTX_STITCH_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_STITCH_CONF_S)) {
		SYS_TRACE("AGTX_CMD_STITCH_CONF size doesn't match %d / %d\n", len, sizeof(AGTX_STITCH_CONF_S));
		return -1;
	}

	ret = read(sockfd, &stitch, sizeof(AGTX_STITCH_CONF_S));
	if (ret != sizeof(AGTX_STITCH_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	/*Check stitch has been enable*/
	/* FIXME */
	MPI_DEV dev_idx = MPI_VIDEO_DEV(stitch.video_dev_idx);
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VCHN_S cfg_vchn = { 0 };

	cfg_attr.group = 0;
	cfg_attr.id = 0;

	/* If config does not exist, continue */
	if (CFG_getConfig(CFG_GET_CONFIG_VCHN, &cfg_attr, &cfg_vchn, sizeof(CFG_VCHN_S)) < 0) {
		return -1;
	}

	avmain_agtx_stitch_to_cfg(&cfg_vchn.stitch, &stitch);

	if (CFG_setConfig(CFG_SET_CONFIG_VCHN, &cfg_attr, &cfg_vchn, sizeof(CFG_VCHN_S)) < 0) {
		return -1;
	}

	if (av_ctrl->video_init) {
		if (VIDEO_DEV_setStitchAttr(dev_idx, &cfg_vchn.stitch)) {
			SYS_TRACE("av_main set stitch attribute fail!\n");
			return -1;
		}
	}

	return 0;
}

/**
 * @brief set image configuration.
 * @return The execution result.
 */
INT32 avmain_set_img_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_IMG_PREF_S img = { 0 };
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_IMG_S cfg_img = { 0 };

	if (len < 0) {
		SYS_TRACE("AGTX_IMG_PREF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_IMG_PREF_S)) {
		SYS_TRACE("AGTX_CMD_IMG_PREF size doesn't match %d / %d\n", len, sizeof(AGTX_IMG_PREF_S));
		return -1;
	}

	ret = read(sockfd, &img, sizeof(AGTX_IMG_PREF_S));
	if (ret != sizeof(AGTX_IMG_PREF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	cfg_attr.group = 0;
	cfg_attr.id = 0;

	cfg_img.brightness = img.brightness;
	cfg_img.contrast = img.contrast;
	cfg_img.saturation = img.saturation;
	cfg_img.hue = img.hue;
	cfg_img.sharpness = img.sharpness;
	cfg_img.anti_flicker = img.anti_flicker;

	if (CFG_setConfig(CFG_SET_CONFIG_IMG, &cfg_attr, (char *)&cfg_img, sizeof(CFG_IMG_S)) < 0) {
		return -1;
	}

	return 0;
}

/**
 * @brief set advanced image performance configuration.
 * @return The execution result.
 */
INT32 avmain_set_adv_img_pref_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_ADV_IMG_PREF_S adv_img = { 0 };
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_ADV_IMG_S cfg_adv_img = { 0 };

	if (len < 0) {
		SYS_TRACE("AGTX_ADV_IMG_PREF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_ADV_IMG_PREF_S)) {
		SYS_TRACE("AGTX_CMD_ADV_IMG_PREF size doesn't match %d / %d\n", len, sizeof(AGTX_ADV_IMG_PREF_S));
		return -1;
	}

	ret = read(sockfd, &adv_img, sizeof(AGTX_ADV_IMG_PREF_S));
	if (ret != sizeof(AGTX_ADV_IMG_PREF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	cfg_attr.group = 0;
	cfg_attr.id = 0;

	cfg_adv_img.backlight_compensation = adv_img.backlight_compensation;
	cfg_adv_img.icr_mode = adv_img.icr_mode;
	cfg_adv_img.image_mode = adv_img.image_mode;
	cfg_adv_img.night_mode = adv_img.night_mode;
	cfg_adv_img.ir_light_suppression = adv_img.ir_light_suppression;
	cfg_adv_img.wdr_en = adv_img.wdr_en;
	cfg_adv_img.wdr_strength = adv_img.wdr_strength;

	if (CFG_setConfig(CFG_SET_CONFIG_ADV_IMG, &cfg_attr, (char *)&cfg_adv_img, sizeof(CFG_ADV_IMG_S)) < 0) {
		return -1;
	}

	return 0;
}

/**
 * @brief set awb performance configuration.
 * @return The execution result.
 */
INT32 avmain_set_awb_pref_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_AWB_PREF_S awb = { 0 };
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_AWB_S cfg_awb = { 0 };

	if (len < 0) {
		SYS_TRACE("AGTX_AWB_PREF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_AWB_PREF_S)) {
		SYS_TRACE("AGTX_CMD_AWB_PREF size doesn't match %d / %d\n", len, sizeof(AGTX_AWB_PREF_S));
		return -1;
	}

	ret = read(sockfd, &awb, sizeof(AGTX_AWB_PREF_S));
	if (ret != sizeof(AGTX_AWB_PREF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	cfg_attr.group = 0;
	cfg_attr.id = 0;

	cfg_awb.mode = awb.mode;
	cfg_awb.color_temp = awb.color_temp;
	cfg_awb.r_gain = awb.r_gain;
	cfg_awb.b_gain = awb.b_gain;

	if (CFG_setConfig(CFG_SET_CONFIG_AWB, &cfg_attr, (char *)&cfg_awb, sizeof(CFG_AWB_S)) < 0) {
		return -1;
	}

	return 0;
}

/**
 * @brief set color mode configuration.
 * @return The execution result.
 */
INT32 avmain_set_color_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_COLOR_CONF_S color = { 0 };
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_COLOR_S cfg_color = { 0 };

	if (len < 0) {
		SYS_TRACE("AGTX_COLOR_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_COLOR_CONF_S)) {
		SYS_TRACE("AGTX_COLOR_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_COLOR_CONF_S));
		return -1;
	}

	ret = read(sockfd, &color, sizeof(AGTX_COLOR_CONF_S));
	if (ret != sizeof(AGTX_COLOR_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	cfg_attr.group = 0;
	cfg_attr.id = 0;

	cfg_color.color_mode = color.color_mode;

	if (CFG_setConfig(CFG_SET_CONFIG_COLOR, &cfg_attr, (char *)&cfg_color, sizeof(CFG_COLOR_S)) < 0) {
		return -1;
	}

	return 0;
}

/**
 * @brief set anti-flicker mode configuration.
 * @return The execution result.
 */
INT32 avmain_set_anti_flicker_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_ANTI_FLICKER_CONF_S anti_flicker = { 0 };
	CFG_ATTR_S cfg_attr = { 0 };

	if (len < 0) {
		SYS_TRACE("AGTX_ANTI_FLICKER_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_ANTI_FLICKER_CONF_S)) {
		SYS_TRACE("AGTX_ANTI_FLICKER_CONF_S size doesn't match %d / %d\n", len,
		           sizeof(AGTX_ANTI_FLICKER_CONF_S));
		return -1;
	}

	ret = read(sockfd, &anti_flicker, sizeof(AGTX_ANTI_FLICKER_CONF_S));
	if (ret != sizeof(AGTX_ANTI_FLICKER_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	cfg_attr.group = 0;
	cfg_attr.id = 0;

	if (CFG_setConfig(CFG_SET_CONFIG_ANTI_FLICKER, &cfg_attr, (char *)&anti_flicker,
	                  sizeof(AGTX_ANTI_FLICKER_CONF_S)) < 0) {
		return -1;
	}

	return 0;
}

/**
 * @brief set cal configuration.
 * @return The execution result.
 */
INT32 avmain_set_cal_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_DIP_CAL_CONF_S cal;

	if (len < 0) {
		SYS_TRACE("AGTX_DIP_CAL_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_DIP_CAL_CONF_S)) {
		SYS_TRACE("AGTX_DIP_CAL_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_CAL_CONF_S));
		return -1;
	}

	ret = read(sockfd, &cal, sizeof(AGTX_DIP_CAL_CONF_S));
	if (ret != sizeof(AGTX_DIP_CAL_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	if (VIDEO_DIP_setCal(MPI_VIDEO_DEV(cal.video_dev_idx), &cal)) {
		SYS_TRACE("av_main set CAL attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set dbc configuration.
 * @return The execution result.
 */
INT32 avmain_set_dbc_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_DIP_DBC_CONF_S dbc;

	if (len < 0) {
		SYS_TRACE("AGTX_DIP_DBC_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_DIP_DBC_CONF_S)) {
		SYS_TRACE("AGTX_DIP_DBC_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_DBC_CONF_S));
		return -1;
	}

	ret = read(sockfd, &dbc, sizeof(AGTX_DIP_DBC_CONF_S));
	if (ret != sizeof(AGTX_DIP_DBC_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	if (VIDEO_DIP_setDbc(MPI_VIDEO_DEV(dbc.video_dev_idx), &dbc)) {
		SYS_TRACE("av_main set DBC attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set dcc configuration.
 * @return The execution result.
 */
INT32 avmain_set_dcc_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_DIP_DCC_CONF_S dcc;

	if (len < 0) {
		SYS_TRACE("AGTX_DIP_DCC_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_DIP_DCC_CONF_S)) {
		SYS_TRACE("AGTX_DIP_DCC_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_DCC_CONF_S));
		return -1;
	}

	ret = read(sockfd, &dcc, sizeof(AGTX_DIP_DCC_CONF_S));
	if (ret != sizeof(AGTX_DIP_DCC_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	if (VIDEO_DIP_setDcc(MPI_VIDEO_DEV(dcc.video_dev_idx), &dcc)) {
		SYS_TRACE("av_main set DCC attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set lsc configuration.
 * @return The execution result.
 */
INT32 avmain_set_lsc_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_DIP_LSC_CONF_S lsc;

	if (len < 0) {
		SYS_TRACE("AGTX_DIP_LSC_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_DIP_LSC_CONF_S)) {
		SYS_TRACE("AGTX_DIP_LSC_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_LSC_CONF_S));
		return -1;
	}

	ret = read(sockfd, &lsc, sizeof(AGTX_DIP_LSC_CONF_S));
	if (ret != sizeof(AGTX_DIP_LSC_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	if (VIDEO_DIP_setLsc(MPI_VIDEO_DEV(lsc.video_dev_idx), &lsc)) {
		SYS_TRACE("av_main set LSC attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set roi configuration.
 * @return The execution result.
 */
INT32 avmain_set_roi_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_DIP_ROI_CONF_S roi;

	if (len < 0) {
		SYS_TRACE("AGTX_DIP_ROI_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_DIP_ROI_CONF_S)) {
		SYS_TRACE("AGTX_DIP_ROI_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_ROI_CONF_S));
		return -1;
	}

	ret = read(sockfd, &roi, sizeof(AGTX_DIP_ROI_CONF_S));
	if (ret != sizeof(AGTX_DIP_ROI_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	if (VIDEO_DIP_setRoi(MPI_VIDEO_DEV(roi.video_dev_idx), &roi)) {
		SYS_TRACE("av_main set ROI attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set ctrl configuration.
 * @return The execution result.
 */
INT32 avmain_set_ctrl_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_DIP_CTRL_CONF_S ctrl;

	if (len < 0) {
		SYS_TRACE("AGTX_DIP_CTRL_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_DIP_CTRL_CONF_S)) {
		SYS_TRACE("AGTX_DIP_CTRL_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_CTRL_CONF_S));
		return -1;
	}

	ret = read(sockfd, &ctrl, sizeof(AGTX_DIP_CTRL_CONF_S));
	if (ret != sizeof(AGTX_DIP_CTRL_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	if (VIDEO_DIP_setCtrl(MPI_VIDEO_DEV(ctrl.video_dev_idx), &ctrl)) {
		SYS_TRACE("av_main set CTRL attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set ae configuration.
 * @return The execution result.
 */
INT32 avmain_set_ae_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_DIP_AE_CONF_S ae;

	if (len < 0) {
		SYS_TRACE("AGTX_DIP_AE_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_DIP_AE_CONF_S)) {
		SYS_TRACE("AGTX_DIP_AE_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_AE_CONF_S));
		return -1;
	}

	ret = read(sockfd, &ae, sizeof(AGTX_DIP_AE_CONF_S));
	if (ret != sizeof(AGTX_DIP_AE_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	if (VIDEO_DIP_setAe(MPI_VIDEO_DEV(ae.video_dev_idx), &ae)) {
		SYS_TRACE("av_main set AE attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set dip_iso configuration.
 * @return The execution result.
 */
INT32 avmain_set_iso_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_DIP_ISO_CONF_S iso;

	if (len < 0) {
		SYS_TRACE("AGTX_DIP_ISO_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_DIP_ISO_CONF_S)) {
		SYS_TRACE("AGTX_DIP_ISO_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_ISO_CONF_S));
		return -1;
	}

	ret = read(sockfd, &iso, sizeof(AGTX_DIP_ISO_CONF_S));
	if (ret != sizeof(AGTX_DIP_ISO_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	if (VIDEO_DIP_setIso(MPI_VIDEO_DEV(iso.video_dev_idx), &iso)) {
		SYS_TRACE("av_main set DIP_ISO attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set awb configuration.
 * @return The execution result.
 */
INT32 avmain_set_awb_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_DIP_AWB_CONF_S awb;

	if (len < 0) {
		SYS_TRACE("AGTX_DIP_AWB_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_DIP_AWB_CONF_S)) {
		SYS_TRACE("AGTX_DIP_AWB_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_AWB_CONF_S));
		return -1;
	}

	ret = read(sockfd, &awb, sizeof(AGTX_DIP_AWB_CONF_S));
	if (ret != sizeof(AGTX_DIP_AWB_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	if (VIDEO_DIP_setAwb(MPI_VIDEO_DEV(awb.video_dev_idx), &awb)) {
		SYS_TRACE("av_main set AWB attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set csm configuration.
 * @return The execution result.
 */
INT32 avmain_set_csm_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_DIP_CSM_CONF_S csm;

	if (len < 0) {
		SYS_TRACE("AGTX_DIP_CSM_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_DIP_CSM_CONF_S)) {
		SYS_TRACE("AGTX_DIP_CSM_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_CSM_CONF_S));
		return -1;
	}

	ret = read(sockfd, &csm, sizeof(AGTX_DIP_CSM_CONF_S));
	if (ret != sizeof(AGTX_DIP_CSM_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	if (VIDEO_DIP_setCsm(MPI_VIDEO_DEV(csm.video_dev_idx), &csm)) {
		SYS_TRACE("av_main set CSM attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set pta configuration.
 * @return The execution result.
 */
INT32 avmain_set_pta_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_DIP_PTA_CONF_S pta;

	if (len < 0) {
		SYS_TRACE("AGTX_DIP_PTA_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_DIP_PTA_CONF_S)) {
		SYS_TRACE("AGTX_DIP_PTA_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_PTA_CONF_S));
		return -1;
	}

	ret = read(sockfd, &pta, sizeof(AGTX_DIP_PTA_CONF_S));
	if (ret != sizeof(AGTX_DIP_PTA_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	if (VIDEO_DIP_setPta(MPI_VIDEO_DEV(pta.video_dev_idx), &pta)) {
		SYS_TRACE("av_main set PTA attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set shp configuration.
 * @return The execution result.
 */
INT32 avmain_set_shp_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_DIP_SHP_CONF_S shp;

	if (len < 0) {
		SYS_TRACE("AGTX_DIP_SHP_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_DIP_SHP_CONF_S)) {
		SYS_TRACE("AGTX_DIP_SHP_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_SHP_CONF_S));
		return -1;
	}

	ret = read(sockfd, &shp, sizeof(AGTX_DIP_SHP_CONF_S));
	if (ret != sizeof(AGTX_DIP_SHP_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	if (VIDEO_DIP_setShp(MPI_VIDEO_DEV(shp.video_dev_idx), &shp)) {
		SYS_TRACE("av_main set SHP attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set nr configuration.
 * @return The execution result.
 */
INT32 avmain_set_nr_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_DIP_NR_CONF_S nr;

	if (len < 0) {
		SYS_TRACE("AGTX_DIP_NR_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_DIP_NR_CONF_S)) {
		SYS_TRACE("AGTX_DIP_NR_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_NR_CONF_S));
		return -1;
	}

	ret = read(sockfd, &nr, sizeof(AGTX_DIP_NR_CONF_S));
	if (ret != sizeof(AGTX_DIP_NR_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	if (VIDEO_DIP_setNr(MPI_VIDEO_DEV(nr.video_dev_idx), &nr)) {
		SYS_TRACE("av_main set NR attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set window based shp configuration.
 * @return The execution result.
 */
INT32 avmain_set_win_shp_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	INT32 i, j;
	UINT8 dev_id, chn_id, win_id;
	AGTX_DIP_SHP_WIN_CONF_S shp;

	if (len < 0) {
		SYS_TRACE("AGTX_DIP_SHP_WIN_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_DIP_SHP_WIN_CONF_S)) {
		SYS_TRACE("AGTX_DIP_SHP_WIN_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_SHP_WIN_CONF_S));
		return -1;
	}

	ret = read(sockfd, &shp, sizeof(AGTX_DIP_SHP_WIN_CONF_S));
	if (ret != sizeof(AGTX_DIP_SHP_WIN_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	if (shp.win_shp_en == 1) {
		dev_id = shp.video_dev_idx;

		for (i = 0; i < shp.strm_num; i++) {
			chn_id = shp.video_strm[i].video_strm_idx;

			for (j = 0; j < shp.video_strm[i].window_num; j++) {
				win_id = shp.video_strm[i].window_array[j].window_idx;

				if (VIDEO_DIP_setWinShp(MPI_VIDEO_WIN(dev_id, chn_id, win_id), &shp.video_strm[i].window_array[j])) {
					SYS_TRACE("av_main set window based SHP attribute failed!\n");
					return -1;
				}
			}
		}
	}

	return 0;
}

/**
 * @brief set window based nr configuration.
 * @return The execution result.
 */
INT32 avmain_set_win_nr_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	INT32 i, j;
	UINT8 dev_id, chn_id, win_id;
	AGTX_DIP_NR_WIN_CONF_S nr;

	if (len < 0) {
		SYS_TRACE("AGTX_DIP_NR_WIN_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_DIP_NR_WIN_CONF_S)) {
		SYS_TRACE("AGTX_DIP_NR_WIN_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_NR_WIN_CONF_S));
		return -1;
	}

	ret = read(sockfd, &nr, sizeof(AGTX_DIP_NR_WIN_CONF_S));
	if (ret != sizeof(AGTX_DIP_NR_WIN_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	if (nr.win_nr_en == 1) {
		dev_id = nr.video_dev_idx;

		for (i = 0; i < nr.strm_num; i++) {
			chn_id = nr.video_strm[i].video_strm_idx;

			for (j = 0; j < nr.video_strm[i].window_num; j++) {
				win_id = nr.video_strm[i].window_array[j].window_idx;

				if (VIDEO_DIP_setWinNr(MPI_VIDEO_WIN(dev_id, chn_id, win_id), &nr.video_strm[i].window_array[j])) {
					SYS_TRACE("av_main set window based NR attribute failed!\n");
					return -1;
				}
			}
		}
	}

	return 0;
}

/**
* @brief set te configuration.
 * @return The execution result.
 */
INT32 avmain_set_te_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_DIP_TE_CONF_S te;

	if (len < 0) {
		SYS_TRACE("AGTX_DIP_TE_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_DIP_TE_CONF_S)) {
		SYS_TRACE("AGTX_DIP_TE_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_TE_CONF_S));
		return -1;
	}

	if (len != sizeof(AGTX_DIP_TE_CONF_S)) {
		SYS_TRACE("AGTX_DIP_TE_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_TE_CONF_S));
		return -1;
	}

	ret = read(sockfd, &te, sizeof(AGTX_DIP_TE_CONF_S));
	if (ret != sizeof(AGTX_DIP_TE_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	if (VIDEO_DIP_setTe(MPI_VIDEO_DEV(te.video_dev_idx), &te)) {
		SYS_TRACE("av_main set TE attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set gamma configuration.
 * @return The execution result.
 */
INT32 avmain_set_gamma_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_DIP_GAMMA_CONF_S gamma;

	if (len < 0) {
		SYS_TRACE("AGTX_DIP_GAMMA_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_DIP_GAMMA_CONF_S)) {
		SYS_TRACE("AGTX_DIP_GAMMA_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_GAMMA_CONF_S));
		return -1;
	}

	ret = read(sockfd, &gamma, sizeof(AGTX_DIP_GAMMA_CONF_S));
	if (ret != sizeof(AGTX_DIP_GAMMA_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}
	if (VIDEO_DIP_setGamma(MPI_VIDEO_DEV(gamma.video_dev_idx), &gamma)) {
		SYS_TRACE("av_main set GAMMA attribugamma failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set enh configuration.
 * @return The execution result.
 */
INT32 avmain_set_enh_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_DIP_ENH_CONF_S enh;

	if (len < 0) {
		SYS_TRACE("AGTX_DIP_ENH_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_DIP_ENH_CONF_S)) {
		SYS_TRACE("AGTX_DIP_ENH_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_ENH_CONF_S));
		return -1;
	}

	ret = read(sockfd, &enh, sizeof(AGTX_DIP_ENH_CONF_S));
	if (ret != sizeof(AGTX_DIP_ENH_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}
	if (VIDEO_DIP_setEnh(MPI_VIDEO_DEV(enh.video_dev_idx), &enh)) {
		SYS_TRACE("av_main set ENH attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set coring configuration.
 * @return The execution result.
 */
INT32 avmain_set_coring_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_DIP_CORING_CONF_S coring;

	if (len < 0) {
		SYS_TRACE("AGTX_DIP_CORING_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_DIP_CORING_CONF_S)) {
		SYS_TRACE("AGTX_DIP_CORING_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_CORING_CONF_S));
		return -1;
	}

	ret = read(sockfd, &coring, sizeof(AGTX_DIP_CORING_CONF_S));
	if (ret != sizeof(AGTX_DIP_CORING_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}
	if (VIDEO_DIP_setCoring(MPI_VIDEO_DEV(coring.video_dev_idx), &coring)) {
		SYS_TRACE("av_main set CORING attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set fcs configuration.
 * @return The execution result.
 */
INT32 avmain_set_fcs_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_DIP_FCS_CONF_S fcs;

	if (len < 0) {
		SYS_TRACE("AGTX_DIP_FCS_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_DIP_FCS_CONF_S)) {
		SYS_TRACE("AGTX_DIP_FCS_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_FCS_CONF_S));
		return -1;
	}

	ret = read(sockfd, &fcs, sizeof(AGTX_DIP_FCS_CONF_S));
	if (ret != sizeof(AGTX_DIP_FCS_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}
	if (VIDEO_DIP_setFcs(MPI_VIDEO_DEV(fcs.video_dev_idx), &fcs)) {
		SYS_TRACE("av_main set FCS attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set stat configuration.
 * @return The execution result.
 */
INT32 avmain_set_stat_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_DIP_STAT_CONF_S stat;

	if (len < 0) {
		SYS_TRACE("AGTX_DIP_STAT_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_DIP_STAT_CONF_S)) {
		SYS_TRACE("AGTX_DIP_STAT_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_STAT_CONF_S));
		return -1;
	}

	ret = read(sockfd, &stat, sizeof(AGTX_DIP_STAT_CONF_S));
	if (ret != sizeof(AGTX_DIP_STAT_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}
	if (VIDEO_DIP_setStat(MPI_VIDEO_DEV(stat.video_dev_idx), &stat)) {
		SYS_TRACE("av_main set STAT attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set hdr_synth configuration.
 * @return The execution result.
 */
INT32 avmain_set_hdr_synth_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_DIP_HDR_SYNTH_CONF_S hdr_synth;

	if (len < 0) {
		SYS_TRACE("AGTX_DIP_HDR_SYNTH_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_DIP_HDR_SYNTH_CONF_S)) {
		SYS_TRACE("AGTX_DIP_HDR_SYNTH_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_HDR_SYNTH_CONF_S));
		return -1;
	}

	ret = read(sockfd, &hdr_synth, sizeof(AGTX_DIP_HDR_SYNTH_CONF_S));
	if (ret != sizeof(AGTX_DIP_HDR_SYNTH_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}
	if (VIDEO_DIP_setHdrSynth(MPI_VIDEO_DEV(hdr_synth.video_dev_idx), &hdr_synth)) {
		SYS_TRACE("av_main set HDR_SYNTH attribute failed!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief set tamper detection configuration.
 * @return The execution result.
 */
INT32 avmain_set_td_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_IVA_TD_CONF_S td = { 0 };
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_IVA_TD_S cfg_td = { 0 };

	if (len < 0) {
		SYS_TRACE("AGTX_CMD_IVA_TD request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_IVA_TD_CONF_S)) {
		SYS_TRACE("AGTX_CMD_IVA_TD size doesn't match %d / %d\n", len, sizeof(AGTX_IVA_TD_CONF_S));
		return -1;
	}

	ret = read(sockfd, &td, sizeof(AGTX_IVA_TD_CONF_S));
	if (ret != sizeof(AGTX_IVA_TD_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	/*FIXME Need to specify channel?*/
	cfg_attr.group = 0;
	cfg_attr.id = td.video_chn_idx;
	if (CFG_getConfig(CFG_SET_CONFIG_TD, &cfg_attr, (char *)&cfg_td, sizeof(CFG_IVA_TD_S)) < 0) {
		return -1;
	}

	cfg_td.enabled = td.enabled;
	cfg_td.endurance = td.endurance;
	cfg_td.sensitivity = td.sensitivity;

	ret = CFG_setConfig(CFG_SET_CONFIG_TD, &cfg_attr, (char *)&cfg_td, sizeof(CFG_IVA_TD_S));

	if (ret < 0) {
		return -1;
	}

	return (ret != 0);
}

/**
 * @brief set motion detection configuration.
 * @return The execution result.
 */
INT32 avmain_set_md_config(INT32 sockfd, INT32 len)
{
	INT32 i = 0;
	INT32 ret = 0;
	AGTX_IVA_MD_CONF_S md = {0};
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VENC_S cfg_venc = { 0 };
	CFG_IVA_MD_S cfg_md = { 0 };

	if (len < 0) {
		SYS_TRACE("AGTX_CMD_IVA_MD request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_IVA_MD_CONF_S)) {
		SYS_TRACE("AGTX_CMD_IVA_MD size doesn't match %d / %d\n", len, sizeof(AGTX_IVA_MD_CONF_S));
		return -1;
	}

	ret = read(sockfd, &md, sizeof(AGTX_IVA_MD_CONF_S));
	if (ret != sizeof(AGTX_IVA_MD_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	/*FIXME Need to specify channel?*/
	cfg_attr.group = 0;
	cfg_attr.id = md.video_chn_idx;
	if (CFG_getConfig(CFG_SET_CONFIG_MD, &cfg_attr, (char *)&cfg_md, sizeof(CFG_IVA_MD_S)) < 0) {
		return -1;
	}

	ret = CFG_getConfig(CFG_SET_CONFIG_VENC, &cfg_attr, (char *)&cfg_venc, sizeof(CFG_VENC_S));
	if (ret < 0) {
		return -1;
	}

	cfg_md.enabled = md.enabled;
	cfg_md.en_rgn = md.en_rgn;
	cfg_md.det_method = md.det;
	cfg_md.en_skip_pd = md.en_skip_pd;
	cfg_md.en_skip_shake = md.en_skip_shake;
	cfg_md.mode = (md.mode == AGTX_IVA_MD_MODE_AREA) ?
	                      CFG_IVA_MD_MODE_AREA :
	                      (md.mode == AGTX_IVA_MD_MODE_ENERGY) ? CFG_IVA_MD_MODE_ENERGY : CFG_IVA_MD_MODE_AREA;
	cfg_md.min_spd = md.min_spd;
	cfg_md.max_spd = md.max_spd;
	cfg_md.sens = md.sens;
	cfg_md.obj_life_th = md.obj_life_th;
	cfg_md.rgn_cnt = md.rgn_cnt;
	cfg_md.alarm_buffer = md.alarm_buffer;
	for (i = 0; i < cfg_md.rgn_cnt; i++) {
		cfg_md.rgn_list[i].sx = md.rgn_list[i].sx;
		cfg_md.rgn_list[i].sy = md.rgn_list[i].sy;
		cfg_md.rgn_list[i].ex = md.rgn_list[i].ex;
		cfg_md.rgn_list[i].ey = md.rgn_list[i].ey;
		cfg_md.rgn_list[i].min_spd = md.rgn_list[i].min_spd;
		cfg_md.rgn_list[i].max_spd = md.rgn_list[i].max_spd;
		cfg_md.rgn_list[i].sens = md.rgn_list[i].sens;
		cfg_md.rgn_list[i].obj_life_th = md.rgn_list[i].obj_life_th;
		cfg_md.rgn_list[i].det_method = md.rgn_list[i].det;
		cfg_md.rgn_list[i].mode = (md.rgn_list[i].mode == AGTX_IVA_MD_MODE_AREA) ?
		                                  CFG_IVA_MD_MODE_AREA :
		                                  (md.rgn_list[i].mode == AGTX_IVA_MD_MODE_ENERGY) ?
		                                  CFG_IVA_MD_MODE_ENERGY :
		                                  CFG_IVA_MD_MODE_AREA;
	}

	ret = CFG_setConfig(CFG_SET_CONFIG_MD, &cfg_attr, (char *)&cfg_md, sizeof(CFG_IVA_MD_S));

	return (ret!=0);
}

/**
 * @brief set automatic region of interest configuration.
 * @return The execution result.
 */
INT32 avmain_set_aroi_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_IVA_AROI_CONF_S aroi = { 0 };
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VENC_S cfg_venc = { 0 };
	CFG_IVA_AROI_S cfg_aroi = { 0 };

	if (len < 0) {
		SYS_TRACE("AGTX_CMD_IVA_AROI request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_IVA_AROI_CONF_S)) {
		SYS_TRACE("AGTX_CMD_IVA_AROI size doesn't match %d / %d\n", len, sizeof(AGTX_IVA_AROI_CONF_S));
		return -1;
	}

	ret = read(sockfd, &aroi, sizeof(AGTX_IVA_AROI_CONF_S));
	if (ret != sizeof(AGTX_IVA_AROI_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	/*FIXME Need to specify channel?*/
	cfg_attr.group = 0;
	cfg_attr.id = aroi.video_chn_idx;
	if (CFG_getConfig(CFG_SET_CONFIG_AROI, &cfg_attr, (char *)&cfg_aroi, sizeof(CFG_IVA_AROI_S)) < 0) {
		return -1;
	}

	ret = CFG_getConfig(CFG_SET_CONFIG_VENC, &cfg_attr, (char *)&cfg_venc, sizeof(CFG_VENC_S));
	if (ret < 0) {
		return -1;
	}

	memcpy(&cfg_aroi, &aroi, sizeof(AGTX_IVA_AROI_CONF_S));

	ret = CFG_setConfig(CFG_SET_CONFIG_AROI, &cfg_attr, (char *)&cfg_aroi, sizeof(CFG_IVA_AROI_S));

	return (ret!=0);
}

/**
 * @brief set pedestrian detection configuration.
 * @return The execution result.
 */
INT32 avmain_set_pd_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_IVA_PD_CONF_S pd = { 0 };
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VENC_S cfg_venc = { 0 };
	CFG_IVA_PD_S cfg_pd = { 0 };

	if (len < 0) {
		SYS_TRACE("AGTX_CMD_IVA_PD request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_IVA_PD_CONF_S)) {
		SYS_TRACE("AGTX_CMD_IVA_PD size doesn't match %d / %d\n", len, sizeof(AGTX_IVA_PD_CONF_S));
		return -1;
	}

	ret = read(sockfd, &pd, sizeof(AGTX_IVA_PD_CONF_S));
	if (ret != sizeof(AGTX_IVA_PD_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	/*FIXME Need to specify channel?*/
	cfg_attr.group = 0;
	cfg_attr.id = pd.video_chn_idx;
	if (CFG_getConfig(CFG_SET_CONFIG_PD, &cfg_attr, (char *)&cfg_pd, sizeof(CFG_IVA_PD_S)) < 0) {
		return -1;
	}

	ret = CFG_getConfig(CFG_SET_CONFIG_VENC, &cfg_attr, (char *)&cfg_venc, sizeof(CFG_VENC_S));
	if (ret < 0) {
		return -1;
	}

	cfg_pd.enabled = pd.enabled;
	cfg_pd.max_aspect_ratio_w = pd.max_aspect_ratio_w;
	cfg_pd.max_aspect_ratio_h = pd.max_aspect_ratio_h;
	cfg_pd.min_aspect_ratio_w = pd.min_aspect_ratio_w;
	cfg_pd.min_aspect_ratio_h = pd.min_aspect_ratio_h;
	cfg_pd.max_size = pd.max_size;
	cfg_pd.min_size = pd.min_size;
	cfg_pd.obj_life_th = pd.obj_life_th;

	ret = CFG_setConfig(CFG_SET_CONFIG_PD, &cfg_attr, (char *)&cfg_pd, sizeof(CFG_IVA_PD_S));

	return (ret!=0);
}

/**
 * @brief set object detection configuration.
 * @return The execution result.
 */
INT32 avmain_set_od_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_IVA_OD_CONF_S od = { 0 };
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VENC_S cfg_venc = { 0 };
	CFG_IVA_OD_S cfg_od = { 0 };

	if (len < 0) {
		SYS_TRACE("AGTX_CMD_IVA_OD request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_IVA_OD_CONF_S)) {
		SYS_TRACE("AGTX_CMD_IVA_OD size doesn't match %d / %d\n", len, sizeof(AGTX_IVA_OD_CONF_S));
		return -1;
	}

	ret = read(sockfd, &od, sizeof(AGTX_IVA_OD_CONF_S));
	if (ret != sizeof(AGTX_IVA_OD_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	/*FIXME Need to specify channel?*/
	cfg_attr.group = 0;
	cfg_attr.id = od.video_chn_idx;
	if (CFG_getConfig(CFG_SET_CONFIG_OD, &cfg_attr, (char *)&cfg_od, sizeof(CFG_IVA_OD_S)) < 0) {
		return -1;
	}

	ret = CFG_getConfig(CFG_SET_CONFIG_VENC, &cfg_attr, (char *)&cfg_venc, sizeof(CFG_VENC_S));
	if (ret < 0) {
		return -1;
	}

	cfg_od.enabled = od.enabled;
	cfg_od.en_shake_det = od.en_shake_det;
	cfg_od.en_crop_outside_obj = od.en_crop_outside_obj;
	cfg_od.od_qual = od.od_qual;
	cfg_od.od_track_refine = od.od_track_refine;
	cfg_od.od_size_th = od.od_size_th;
	cfg_od.od_sen = od.od_sen;
	cfg_od.en_stop_det = od.en_stop_det;

	ret = CFG_setConfig(CFG_SET_CONFIG_OD, &cfg_attr, (char *)&cfg_od, sizeof(CFG_IVA_OD_S));

	return (ret!=0);
}

/**
 * @brief set regional motion sensor configuration.
 * @return The execution result.
 */
INT32 avmain_set_rms_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_IVA_RMS_CONF_S rms = { 0 };
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_IVA_RMS_S cfg_rms = { 0 };

	if (len < 0) {
		SYS_TRACE("AGTX_CMD_IVA_RMS request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_IVA_RMS_CONF_S)) {
		SYS_TRACE("AGTX_CMD_IVA_RMS size doesn't match %d / %d\n", len, sizeof(AGTX_IVA_RMS_CONF_S));
		return -1;
	}

	ret = read(sockfd, &rms, sizeof(AGTX_IVA_RMS_CONF_S));
	if (ret != sizeof(AGTX_IVA_RMS_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	/*FIXME Need to specify channel?*/
	cfg_attr.group = 0;
	cfg_attr.id = rms.video_chn_idx;
	if (CFG_getConfig(CFG_SET_CONFIG_RMS, &cfg_attr, (char *)&cfg_rms, sizeof(CFG_IVA_RMS_S)) < 0) {
		return -1;
	}

	cfg_rms.enabled = rms.enabled;
	cfg_rms.sensitivity = rms.sensitivity;
	cfg_rms.split_x = rms.split_x;
	cfg_rms.split_y = rms.split_y;

	ret = CFG_setConfig(CFG_SET_CONFIG_RMS, &cfg_attr, (char *)&cfg_rms, sizeof(CFG_IVA_RMS_S));

	return (ret!=0);
}

/**
 * @brief set light on off detection configuration.
 * @return The execution result.
 */
INT32 avmain_set_ld_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_IVA_LD_CONF_S ld = { { 0 } };
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VENC_S cfg_venc = { 0 };
	CFG_IVA_LD_S cfg_ld = { 0 };

	if (len < 0) {
		SYS_TRACE("AGTX_CMD_IVA_LD request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_IVA_LD_CONF_S)) {
		SYS_TRACE("AGTX_CMD_IVA_LD size doesn't match %d / %d\n", len, sizeof(AGTX_IVA_LD_CONF_S));
		return -1;
	}

	ret = read(sockfd, &ld, sizeof(AGTX_IVA_LD_CONF_S));
	if (ret != sizeof(AGTX_IVA_LD_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	/*FIXME Need to specify channel?*/
	cfg_attr.group = 0;
	cfg_attr.id = ld.video_chn_idx;
	if (CFG_getConfig(CFG_SET_CONFIG_LD, &cfg_attr, (char *)&cfg_ld, sizeof(CFG_IVA_LD_S)) < 0) {
		return -1;
	}

	ret = CFG_getConfig(CFG_SET_CONFIG_VENC, &cfg_attr, (char *)&cfg_venc, sizeof(CFG_VENC_S));
	if (ret < 0) {
		return -1;
	}

	cfg_ld.enabled = ld.enabled;
	cfg_ld.sensitivity = ld.sensitivity;
	cfg_ld.trig_cond = ld.trigger_cond;
	//cfg_ld.det_region = (CFG_IVA_LD_REGION_S) ld.det_region;
	cfg_ld.det_region.start_x = ld.det_region.start_x;
	cfg_ld.det_region.start_y = ld.det_region.start_y;
	cfg_ld.det_region.end_x = ld.det_region.end_x;
	cfg_ld.det_region.end_y = ld.det_region.end_y;

	ret = CFG_setConfig(CFG_SET_CONFIG_LD, &cfg_attr, (char *)&cfg_ld, sizeof(CFG_IVA_LD_S));
	return (ret!=0);
}

/**
 * @brief set electric fence configuration.
 * @return The execution result.
 */
INT32 avmain_set_ef_config(INT32 sockfd, INT32 len)
{
	INT32 i = 0;
	INT32 ret = 0;
	AGTX_IVA_EF_CONF_S ef = {{0}};
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VENC_S cfg_venc = { 0 };
	CFG_IVA_EF_S cfg_ef = { 0 };

	if (len < 0) {
		SYS_TRACE("AGTX_CMD_IVA_EF request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_IVA_EF_CONF_S)) {
		SYS_TRACE("AGTX_CMD_IVA_EF size doesn't match %d / %d\n", len, sizeof(AGTX_IVA_EF_CONF_S));
		return -1;
	}

	ret = read(sockfd, &ef, sizeof(AGTX_IVA_EF_CONF_S));
	if (ret != sizeof(AGTX_IVA_EF_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	/*FIXME Need to specify channel?*/
	cfg_attr.group = 0;
	cfg_attr.id = ef.video_chn_idx;
	if (CFG_getConfig(CFG_SET_CONFIG_EF, &cfg_attr, (char *)&cfg_ef, sizeof(CFG_IVA_EF_S)) < 0) {
		return -1;
	}

	ret = CFG_getConfig(CFG_SET_CONFIG_VENC, &cfg_attr, (char *)&cfg_venc, sizeof(CFG_VENC_S));
	if (ret < 0) {
		return -1;
	}

	cfg_ef.enabled = ef.enabled;
	cfg_ef.line_cnt = ef.line_cnt;
	for (i = 0; i < cfg_ef.line_cnt; i++) {
		cfg_ef.line_list[i].start_x = ef.line_list[i].start_x;
		cfg_ef.line_list[i].start_y = ef.line_list[i].start_y;
		cfg_ef.line_list[i].end_x = ef.line_list[i].end_x;
		cfg_ef.line_list[i].end_y = ef.line_list[i].end_y;
		cfg_ef.line_list[i].obj_min_w = ef.line_list[i].obj_min_w;
		cfg_ef.line_list[i].obj_min_h = ef.line_list[i].obj_min_h;
		cfg_ef.line_list[i].obj_max_w = ef.line_list[i].obj_max_w;
		cfg_ef.line_list[i].obj_max_h = ef.line_list[i].obj_max_h;
		cfg_ef.line_list[i].obj_area = ef.line_list[i].obj_area;
		cfg_ef.line_list[i].obj_v_th = ef.line_list[i].obj_v_th;
		cfg_ef.line_list[i].obj_life_th = ef.line_list[i].obj_life_th;
		switch (ef.line_list[i].mode) {
		case AGTX_IVA_EF_MODE_DIR_NONE:
			cfg_ef.line_list[i].mode = CFG_IVA_EF_MODE_DIR_NONE;
			break;
		case AGTX_IVA_EF_MODE_DIR_POS:
			cfg_ef.line_list[i].mode = CFG_IVA_EF_MODE_DIR_POS;
			break;
		case AGTX_IVA_EF_MODE_DIR_NEG:
			cfg_ef.line_list[i].mode = CFG_IVA_EF_MODE_DIR_NEG;
			break;
		case AGTX_IVA_EF_MODE_DIR_BOTH:
			cfg_ef.line_list[i].mode = CFG_IVA_EF_MODE_DIR_BOTH;
			break;
		default:
			cfg_ef.line_list[i].mode = CFG_IVA_EF_MODE_DIR_NONE;
			printf("Unkown IVA EF mode\n");
			break;
		}
	}

	ret = CFG_setConfig(CFG_SET_CONFIG_EF, &cfg_attr, (char *)&cfg_ef, sizeof(CFG_IVA_EF_S));

	return (ret!=0);
}

/**
 * @brief set vdbg configuration.
 * @return The execution result.
 */
INT32 avmain_set_vdbg_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_VDBG_CONF_S vdbg = { 0 };
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VDBG_S cfg_vdbg = { 0 };

	if (len < 0) {
		SYS_TRACE("AGTX_CMD_VDBG request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_VDBG_CONF_S)) {
		SYS_TRACE("AGTX_CMD_IVA_VDBG size doesn't match %d / %d\n", len, sizeof(AGTX_VDBG_CONF_S));
		return -1;
	}

	ret = read(sockfd, &vdbg, sizeof(AGTX_VDBG_CONF_S));
	if (ret != sizeof(AGTX_VDBG_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	/*FIXME Need to specify channel?*/
	cfg_attr.group = 0;

	cfg_vdbg.enabled = vdbg.enabled;
	cfg_vdbg.ctx = vdbg.ctx;

	ret = CFG_setConfig(CFG_SET_CONFIG_VDBG, &cfg_attr, (char *)&cfg_vdbg, sizeof(CFG_VDBG_S));
	return (ret!=0);
}

/**
 * @brief set ptz configuration.
 * @return The execution result.
 */
INT32 avmain_set_video_ptz_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_VIDEO_PTZ_CONF_S ptz = { 0 };
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_PTZ_S cfg_ptz = { 0 };

	if (len < 0) {
		SYS_TRACE("AGTX_CMD_PTZ request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_VIDEO_PTZ_CONF_S)) {
		SYS_TRACE("AGTX_VIDEO_PTZ_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_VIDEO_PTZ_CONF_S));
		return -1;
	}

	ret = read(sockfd, &ptz, sizeof(AGTX_VIDEO_PTZ_CONF_S));
	if (ret != sizeof(AGTX_VIDEO_PTZ_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	/*FIXME Need to specify channel?*/
	cfg_attr.group = 0;

	memcpy(&cfg_ptz, &ptz, sizeof(AGTX_VIDEO_PTZ_CONF_S));
	//cfg_ptz.enabled = ptz.enabled;
	//cfg_ptz.ctx = ptz.ctx;

	ret = CFG_setConfig(CFG_SET_CONFIG_PTZ, &cfg_attr, (char *)&cfg_ptz, sizeof(CFG_PTZ_S));
	return ret;
}

/**
 * @brief set pedestrian detection configuration.
 * @return The execution result.
 */
INT32 avmain_set_shd_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_IVA_SHD_CONF_S shd = { 0 };
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VENC_S cfg_venc = { 0 };
	CFG_IVA_SHD_S cfg_shd = { 0 };

	if (len < 0) {
		SYS_TRACE("AGTX_IVA_SHD_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_IVA_SHD_CONF_S)) {
		SYS_TRACE("AGTX_IVA_SHD_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_IVA_SHD_CONF_S));
		return -1;
	}

	ret = read(sockfd, &shd, sizeof(AGTX_IVA_SHD_CONF_S));
	if (ret != sizeof(AGTX_IVA_SHD_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	/*FIXME Need to specify channel?*/
	cfg_attr.group = 0;
	cfg_attr.id = shd.video_chn_idx;
	if (CFG_getConfig(CFG_GET_CONFIG_SHD, &cfg_attr, (char *)&cfg_shd, sizeof(CFG_IVA_SHD_S)) < 0) {
		return -1;
	}

	ret = CFG_getConfig(CFG_GET_CONFIG_VENC, &cfg_attr, (char *)&cfg_venc, sizeof(CFG_VENC_S));
	if (ret < 0) {
		return -1;
	}

	memcpy(&cfg_shd, &shd, sizeof(CFG_IVA_SHD_S));

	ret = CFG_setConfig(CFG_SET_CONFIG_SHD, &cfg_attr, (char *)&cfg_shd, sizeof(CFG_IVA_SHD_S));

	return (ret!=0);
}

/**
 * @brief set edge ai assisted feature configuration.
 * @return The execution result.
 */
INT32 avmain_set_eaif_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_IVA_EAIF_CONF_S eaif = { 0 };
	CFG_ATTR_S cfg_attr = { 0 };
	//CFG_VENC_S cfg_venc = { 0 };
	CFG_IVA_EAIF_S cfg_eaif = { 0 };

	if (len < 0) {
		SYS_TRACE("AGTX_IVA_EAIF_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_IVA_EAIF_CONF_S)) {
		SYS_TRACE("AGTX_IVA_EAIF_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_IVA_EAIF_CONF_S));
		return -1;
	}

	ret = read(sockfd, &eaif, sizeof(AGTX_IVA_EAIF_CONF_S));
	if (ret != sizeof(AGTX_IVA_EAIF_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	/*FIXME Need to specify channel?*/
	cfg_attr.group = 0;
	cfg_attr.id = eaif.video_chn_idx;
	if (CFG_getConfig(CFG_GET_CONFIG_EAIF, &cfg_attr, (char *)&cfg_eaif, sizeof(CFG_IVA_EAIF_S)) < 0) {
		return -1;
	}

	//ret = CFG_getConfig(CFG_GET_CONFIG_VENC, &cfg_attr, (char *)&cfg_venc, sizeof(CFG_VENC_S));
	//if (ret < 0) {
	//	return -1;
	//}

	memcpy(&cfg_eaif, &eaif, sizeof(CFG_IVA_EAIF_S));

	ret = CFG_setConfig(CFG_SET_CONFIG_EAIF, &cfg_attr, (char *)&cfg_eaif, sizeof(CFG_IVA_EAIF_S));

	return (ret!=0);
}

/**
 * @brief set pet diet monitor configuration.
 * @return The execution result.
 */
INT32 avmain_set_pfm_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_IVA_PFM_CONF_S pfm = { 0 };
	CFG_ATTR_S cfg_attr = { 0 };
	//CFG_VENC_S cfg_venc = { 0 };
	CFG_IVA_PFM_S cfg_pfm = { 0 };

	if (len < 0) {
		SYS_TRACE("AGTX_IVA_PFM_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_IVA_PFM_CONF_S)) {
		SYS_TRACE("AGTX_IVA_PFM_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_IVA_PFM_CONF_S));
		return -1;
	}

	ret = read(sockfd, &pfm, sizeof(AGTX_IVA_PFM_CONF_S));
	if (ret != sizeof(AGTX_IVA_PFM_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	/*FIXME Need to specify channel?*/
	cfg_attr.group = 0;
	cfg_attr.id = pfm.video_chn_idx;
	if (CFG_getConfig(CFG_GET_CONFIG_PFM, &cfg_attr, (char *)&cfg_pfm, sizeof(CFG_IVA_PFM_S)) < 0) {
		return -1;
	}

	//ret = CFG_getConfig(CFG_GET_CONFIG_VENC, &cfg_attr, (char *)&cfg_venc, sizeof(CFG_VENC_S));
	//if (ret < 0) {
	//	return -1;
	//}

	memcpy(&cfg_pfm, &pfm, sizeof(CFG_IVA_PFM_S));

	ret = CFG_setConfig(CFG_SET_CONFIG_PFM, &cfg_attr, (char *)&cfg_pfm, sizeof(CFG_IVA_PFM_S));

	return (ret!=0);
}

/**
 * @brief set baby monitor configuration.
 * @return The execution result.
 */
INT32 avmain_set_bm_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_IVA_BM_CONF_S bm = { 0 };
	CFG_ATTR_S cfg_attr = { 0 };
	//CFG_VENC_S cfg_venc = { 0 };
	CFG_IVA_BM_S cfg_bm = { 0 };

	if (len < 0) {
		SYS_TRACE("AGTX_IVA_BM_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_IVA_BM_CONF_S)) {
		SYS_TRACE("AGTX_IVA_BM_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_IVA_BM_CONF_S));
		return -1;
	}

	ret = read(sockfd, &bm, sizeof(AGTX_IVA_BM_CONF_S));
	if (ret != sizeof(AGTX_IVA_BM_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	/*FIXME Need to specify channel?*/
	cfg_attr.group = 0;
	cfg_attr.id = bm.video_chn_idx;
	if (CFG_getConfig(CFG_GET_CONFIG_BM, &cfg_attr, (char *)&cfg_bm, sizeof(CFG_IVA_BM_S)) < 0) {
		return -1;
	}

	//ret = CFG_getConfig(CFG_GET_CONFIG_VENC, &cfg_attr, (char *)&cfg_venc, sizeof(CFG_VENC_S));
	//if (ret < 0) {
	//	return -1;
	//}

	memcpy(&cfg_bm, &bm, sizeof(CFG_IVA_BM_S));

	ret = CFG_setConfig(CFG_SET_CONFIG_BM, &cfg_attr, (char *)&cfg_bm, sizeof(CFG_IVA_BM_S));

	return (ret!=0);
}

/**
 * @brief set door keeper configuration.
 * @return The execution result.
 */
INT32 avmain_set_dk_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_IVA_DK_CONF_S dk = { 0 };
	CFG_ATTR_S cfg_attr = { 0 };
	//CFG_VENC_S cfg_venc = { 0 };
	CFG_IVA_DK_S cfg_dk = { 0 };

	if (len < 0) {
		SYS_TRACE("AGTX_IVA_DK_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_IVA_DK_CONF_S)) {
		SYS_TRACE("AGTX_IVA_DK_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_IVA_DK_CONF_S));
		return -1;
	}

	ret = read(sockfd, &dk, sizeof(AGTX_IVA_DK_CONF_S));
	if (ret != sizeof(AGTX_IVA_DK_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	/*FIXME Need to specify channel?*/
	cfg_attr.group = 0;
	cfg_attr.id = dk.video_chn_idx;
	if (CFG_getConfig(CFG_GET_CONFIG_DK, &cfg_attr, (char *)&cfg_dk, sizeof(CFG_IVA_DK_S)) < 0) {
		return -1;
	}

	//ret = CFG_getConfig(CFG_GET_CONFIG_VENC, &cfg_attr, (char *)&cfg_venc, sizeof(CFG_VENC_S));
	//if (ret < 0) {
	//	return -1;
	//}

	memcpy(&cfg_dk, &dk, sizeof(CFG_IVA_DK_S));

	ret = CFG_setConfig(CFG_SET_CONFIG_DK, &cfg_attr, (char *)&cfg_dk, sizeof(CFG_IVA_DK_S));

	return (ret != 0);
}

/**
 * @brief set fall detection configuration.
 * @return The execution result.
 */
INT32 avmain_set_fld_config(INT32 sockfld, INT32 len)
{
	INT32 ret = 0;
	AGTX_IVA_FLD_CONF_S fld = { 0 };
	CFG_ATTR_S cfg_attr = { 0 };
	//CFG_VENC_S cfg_venc = { 0 };
	CFG_IVA_FLD_S cfg_fld = { 0 };

	if (len < 0) {
		SYS_TRACE("AGTX_IVA_FLD_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_IVA_FLD_CONF_S)) {
		SYS_TRACE("AGTX_IVA_FLD_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_IVA_FLD_CONF_S));
		return -1;
	}

	ret = read(sockfld, &fld, sizeof(AGTX_IVA_FLD_CONF_S));
	if (ret != sizeof(AGTX_IVA_FLD_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	/*FIXME Need to specify channel?*/
	cfg_attr.group = 0;
	cfg_attr.id = fld.video_chn_idx;
	if (CFG_getConfig(CFG_SET_CONFIG_FLD, &cfg_attr, (char *)&cfg_fld, sizeof(CFG_IVA_FLD_S)) < 0) {
		return -1;
	}

	//ret = CFG_getConfig(CFG_GET_CONFIG_VENC, &cfg_attr, (char *)&cfg_venc, sizeof(CFG_VENC_S));
	//if (ret < 0) {
	//    return -1;
	//}

	memcpy(&cfg_fld, &fld, sizeof(CFG_IVA_FLD_S));

	ret = CFG_setConfig(CFG_SET_CONFIG_FLD, &cfg_attr, (char *)&cfg_fld, sizeof(CFG_IVA_FLD_S));
	return (ret != 0);
}

/**
 * @brief set loud sound detection configuration.
 * @return The execution result.
 */
INT32 avmain_set_lsd_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_IAA_LSD_CONF_S lsd = { 0 };
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_IAA_LSD_S cfg_lsd = { 0 };

	if (len < 0) {
		SYS_TRACE("AGTX_IAA_LSD_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_IAA_LSD_CONF_S)) {
		SYS_TRACE("AGTX_IAA_LSD_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_IAA_LSD_CONF_S));
		return -1;
	}

	ret = read(sockfd, &lsd, sizeof(AGTX_IAA_LSD_CONF_S));
	if (ret != sizeof(AGTX_IAA_LSD_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	/*FIXME Need to specify channel?*/
	cfg_attr.group = lsd.audio_dev_idx;
	if (CFG_getConfig(CFG_GET_CONFIG_LSD, &cfg_attr, (char *)&cfg_lsd, sizeof(CFG_IAA_LSD_S)) < 0) {
		return -1;
	}

	memcpy(&cfg_lsd, &lsd, sizeof(CFG_IAA_LSD_S));

	ret = CFG_setConfig(CFG_SET_CONFIG_LSD, &cfg_attr, (char *)&cfg_lsd, sizeof(CFG_IAA_LSD_S));

	return (ret!=0);
}

/**
 * @brief set osd configuration.
 * @return The execution result.
 */
INT32 avmain_set_osd_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_OSD_CONF_S cfg_osd;
	CFG_ATTR_S cfg_attr = { 0 };
	AGTX_OSD_CONF_S cfg_osd_old;
	INT32 restart_video = 0;
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;

	if (len < 0) {
		SYS_TRACE("AGTX_OSD_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_OSD_CONF_S)) {
		SYS_TRACE("AGTX_OSD_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_OSD_CONF_S));
		return -1;
	}

	ret = read(sockfd, &cfg_osd, sizeof(AGTX_OSD_CONF_S));
	if (ret != sizeof(AGTX_OSD_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	cfg_attr.group = 0;
	cfg_attr.id = 0;

	//if (CFG_setConfig(CFG_SET_CONFIG_OSD,&cfg_attr,&cfg_osd,0) < 0) {
	//	return -1;
	//}
	if (CFG_getConfig(CFG_GET_CONFIG_OSD, &cfg_attr, (char *)&cfg_osd_old, sizeof(AGTX_OSD_CONF_S)) < 0) {
		return -1;
	}

	ret = CFG_setConfig(CFG_SET_CONFIG_OSD, &cfg_attr, &cfg_osd, 0);
	if (ret < 0) {
		return 1;
	} else if (ret == VIDEO_OSD_RESTREAM_REQUEST) {
		restart_video = 1;
	}

	/*do nothing before config initial*/
	if (restart_video && av_ctrl->video_init) {
		CFG_closeAllModule();
	}

	/*do nothing before config initial*/
	if (restart_video && av_ctrl->video_init) {
		ret = CFG_startAllModule();
	}

	return ret;
}
/**
 * @brief set osd privacy mask configuration.
 * @return The execution result.
 */
INT32 avmain_set_osd_pm_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	INT32 restart_video = 0;
	AGTX_OSD_PM_CONF_S osd_pm;
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_OSD_PM_S *cfg_osd_pm;
	CFG_OSD_PM_S cfg_osd_pm_old;
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;

	if (len < 0) {
		SYS_TRACE("AGTX_CMD_OSD_PM request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_OSD_PM_CONF_S)) {
		SYS_TRACE("AGTX_CMD_OSD_PM size doesn't match %d / %d\n", len, sizeof(AGTX_OSD_PM_CONF_S));
		return -1;
	}

	ret = read(sockfd, &osd_pm, sizeof(AGTX_OSD_PM_CONF_S));
	if (ret != sizeof(AGTX_OSD_PM_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	cfg_osd_pm = (CFG_OSD_PM_S *)(&osd_pm);

	ret = CFG_getConfig(CFG_GET_CONFIG_OSD_PM, &cfg_attr, (char *)&cfg_osd_pm_old, sizeof(AGTX_OSD_PM_CONF_S));

	/* TODO DONT NEED CFG_ATTR here */
	cfg_attr.group = 0;
	cfg_attr.id = 0;
	ret = CFG_setConfig(CFG_SET_CONFIG_OSD_PM, &cfg_attr, (char *)cfg_osd_pm, sizeof(CFG_OSD_PM_S));
	if (ret < 0) {
		return 1;
	} else if (ret == VIDEO_OSD_RESTREAM_REQUEST) {
		restart_video = 1;
	}

	/*do nothing before config initial*/
	if (restart_video && av_ctrl->video_init) {
		CFG_closeAllModule();
	}

	/*do nothing before config initial*/
	if (restart_video && av_ctrl->video_init) {
		ret = CFG_startAllModule();
	}

	return ret;
}

/**
 * @brief set ldc configuration.
 * @return The execution result.
 */
INT32 avmain_set_ldc_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_LDC_CONF_S ldc;
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;

	if (len < 0) {
		SYS_TRACE("AGTX_LDC_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_LDC_CONF_S)) {
		SYS_TRACE("AGTX_LDC_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_DIP_NR_CONF_S));
		return -1;
	}

	ret = read(sockfd, &ldc, sizeof(AGTX_LDC_CONF_S));
	if (ret != sizeof(AGTX_LDC_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	MPI_DEV dev_idx = MPI_VIDEO_DEV(ldc.video_dev_idx);
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VCHN_S cfg_vchn = { 0 };

	cfg_attr.group = 0;
	cfg_attr.id = 0;

	/* If config does not exist, continue */
	if (CFG_getConfig(CFG_GET_CONFIG_VCHN, &cfg_attr, &cfg_vchn, sizeof(CFG_VCHN_S)) < 0) {
		return -1;
	}

	cfg_vchn.ldc = ldc;

	if (CFG_setConfig(CFG_SET_CONFIG_VCHN, &cfg_attr, &cfg_vchn, sizeof(CFG_VCHN_S)) < 0) {
		return -1;
	}

	if (av_ctrl->video_init) {
		if (VIDEO_DEV_setLdcConf(dev_idx, &ldc)) {
			SYS_TRACE("av_main set LDC attribute fail!\n");
			return -1;
		}
	}

	return 0;
}

/**
 * @brief set panorama configuration.
 * @return The execution result.
 */
INT32 avmain_set_panorama_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_PANORAMA_CONF_S pano;
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;

	if (len < 0) {
		SYS_TRACE("AGTX_PANORAMA_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_PANORAMA_CONF_S)) {
		SYS_TRACE("AGTX_PANORAMA_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_PANORAMA_CONF_S));
		return -1;
	}

	ret = read(sockfd, &pano, sizeof(AGTX_PANORAMA_CONF_S));
	if (ret != sizeof(AGTX_PANORAMA_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	MPI_DEV dev_idx = MPI_VIDEO_DEV(pano.video_dev_idx);
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VCHN_S cfg_vchn = { 0 };

	cfg_attr.group = 0;
	cfg_attr.id = 0;

	/* If config does not exist, continue */
	if (CFG_getConfig(CFG_GET_CONFIG_VCHN, &cfg_attr, &cfg_vchn, sizeof(CFG_VCHN_S)) < 0) {
		return -1;
	}

	cfg_vchn.panorama = pano;

	if (CFG_setConfig(CFG_SET_CONFIG_VCHN, &cfg_attr, &cfg_vchn, sizeof(CFG_VCHN_S)) < 0) {
		return -1;
	}

	if (av_ctrl->video_init) {
		if (VIDEO_DEV_setPanoramaConf(dev_idx, &pano)) {
			SYS_TRACE("av_main set Panorama attribute fail!\n");
			return -1;
		}
	}

	return 0;
}

/**
 * @brief set panning configuration.
 * @return The execution result.
 */
INT32 avmain_set_panning_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_PANNING_CONF_S pann;
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;

	if (len < 0) {
		SYS_TRACE("AGTX_PANNING_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_PANNING_CONF_S)) {
		SYS_TRACE("AGTX_PANNING_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_PANNING_CONF_S));
		return -1;
	}

	ret = read(sockfd, &pann, sizeof(AGTX_PANNING_CONF_S));
	if (ret != sizeof(AGTX_PANNING_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	MPI_DEV dev_idx = MPI_VIDEO_DEV(pann.video_dev_idx);
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VCHN_S cfg_vchn = { 0 };

	cfg_attr.group = 0;
	cfg_attr.id = 0;

	/* If config does not exist, continue */
	if (CFG_getConfig(CFG_GET_CONFIG_VCHN, &cfg_attr, &cfg_vchn, sizeof(CFG_VCHN_S)) < 0) {
		return -1;
	}

	cfg_vchn.panning = pann;

	if (CFG_setConfig(CFG_SET_CONFIG_VCHN, &cfg_attr, &cfg_vchn, sizeof(CFG_VCHN_S)) < 0) {
		return -1;
	}

	if (av_ctrl->video_init) {
		if (VIDEO_DEV_setPanningConf(dev_idx, &pann)) {
			SYS_TRACE("av_main set Panning attribute fail!\n");
			return -1;
		}
	}

	return 0;
}

/**
 * @brief set surround configuration.
 * @return The execution result.
 */
INT32 avmain_set_surround_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_SURROUND_CONF_S surr;
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;

	if (len < 0) {
		SYS_TRACE("AGTX_SURROUND_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_SURROUND_CONF_S)) {
		SYS_TRACE("AGTX_SURROUND_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_SURROUND_CONF_S));
		return -1;
	}

	ret = read(sockfd, &surr, sizeof(AGTX_SURROUND_CONF_S));
	if (ret != sizeof(AGTX_SURROUND_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	MPI_DEV dev_idx = MPI_VIDEO_DEV(surr.video_dev_idx);
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VCHN_S cfg_vchn = { 0 };

	cfg_attr.group = 0;
	cfg_attr.id = 0;

	/* If config does not exist, continue */
	if (CFG_getConfig(CFG_GET_CONFIG_VCHN, &cfg_attr, &cfg_vchn, sizeof(CFG_VCHN_S)) < 0) {
		return -1;
	}

	cfg_vchn.surround = surr;

	if (CFG_setConfig(CFG_SET_CONFIG_VCHN, &cfg_attr, &cfg_vchn, sizeof(CFG_VCHN_S)) < 0) {
		return -1;
	}

	if (av_ctrl->video_init) {
		if (VIDEO_DEV_setSurroundConf(dev_idx, &surr)) {
			SYS_TRACE("av_main set Surround attribute fail!\n");
			return -1;
		}
	}

	return 0;
}


/**
 * @brief set audio configuration.
 * @return The execution result.
 */
INT32 avmain_set_audio_config(INT32 sockfd, INT32 len)
{
	INT32 ret = 0;
	AGTX_AUDIO_CONF_S audio;

	if (len < 0) {
		SYS_TRACE("AGTX_AUDIO_CONF_S request failed err %d\n", len);
		return -1;
	}

	if (len != sizeof(AGTX_AUDIO_CONF_S)) {
		SYS_TRACE("AGTX_AUDIO_CONF_S size doesn't match %d / %d\n", len, sizeof(AGTX_AUDIO_CONF_S));
		return -1;
	}

	ret = read(sockfd, &audio, sizeof(AGTX_AUDIO_CONF_S));
	if (ret != sizeof(AGTX_AUDIO_CONF_S)) {
		SYS_TRACE("Read too short %d(%m)\n", errno);
		return -1;
	}

	return 0;
}

/**
 * @brief Notify other module when configuration changed.
 * @return The execution result.
 */
INT32 avmain_set_notify()
{
	CFG_ATTR_S cfg_attr = { 0 };
	CFG_VCHN_S cfg_vchn = { 0 };

	if (CFG_setConfig(CFG_SET_CONFIG_NOTIFY, &cfg_attr, &cfg_vchn, sizeof(CFG_VCHN_S)) < 0) {
		return -1;
	}

	return 0;
}

/**
 * @brief set receive flag from central control.
 * @return The execution result.
 */
INT32 avmain_set_cfg_recv_flag(INT32 cmd, INT32 *recv_flag)
{
	switch (cmd) {
	case AGTX_CMD_VIDEO_DEV_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_VIN_DONE;
		break;
	case AGTX_CMD_VIDEO_LAYOUT_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_LAYOUT_DONE;
		break;
	case AGTX_CMD_STITCH_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_VCHN_DONE;
		break;
	case AGTX_CMD_VIDEO_STRM_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_ENC_DONE;
		break;
	case AGTX_CMD_TD_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_TD_DONE;
		break;
	case AGTX_CMD_MD_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_MD_DONE;
		break;
	case AGTX_CMD_AROI_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_AROI_DONE;
		break;
	case AGTX_CMD_PD_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_PD_DONE;
		break;
	case AGTX_CMD_OD_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_OD_DONE;
		break;
	case AGTX_CMD_RMS_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_RMS_DONE;
		break;
	case AGTX_CMD_LD_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_LD_DONE;
		break;
	case AGTX_CMD_EF_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_EF_DONE;
		break;
	case AGTX_CMD_VDBG_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_VDBG_DONE;
		break;
	case AGTX_CMD_VIDEO_PTZ_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_PTZ_DONE;
		break;
	case AGTX_CMD_SHD_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_SHD_DONE;
		break;
	case AGTX_CMD_EAIF_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_EAIF_DONE;
		break;
	case AGTX_CMD_PFM_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_PFM_DONE;
		break;
	case AGTX_CMD_BM_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_BM_DONE;
		break;
	case AGTX_CMD_DK_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_DK_DONE;
		break;
	case AGTX_CMD_FLD_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_FLD_DONE;
		break;
	case AGTX_CMD_LSD_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_LSD_DONE;
		break;
	case AGTX_CMD_OSD_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_OSD_DONE;
		break;
	case AGTX_CMD_OSD_PM_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_OSD_PM_DONE;
		break;
	case AGTX_CMD_AUDIO_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_AUDIO_DONE;
		break;
	case AGTX_CMD_LDC_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_LDC_DONE;
		break;
	case AGTX_CMD_PANORAMA_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_PANORAMA_DONE;
		break;
	case AGTX_CMD_PANNING_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_PANNING_DONE;
		break;
	case AGTX_CMD_SURROUND_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_SURROUND_DONE;
		break;
	default:
		break;
	}
	return 0;
}

/**
 * @brief set receive digital image processing flag from central control.
 * @return The execution result.
 */
INT32 avmain_set_dip_cfg_recv_flag(INT32 cmd, INT32 *recv_flag)
{
	switch (cmd) {
	case AGTX_CMD_DIP_CAL:
		*recv_flag |= 1 << AV_MAIN_RECV_CAL_DONE;
		break;
	case AGTX_CMD_DIP_DBC:
		*recv_flag |= 1 << AV_MAIN_RECV_DBC_DONE;
		break;
	case AGTX_CMD_DIP_DCC:
		*recv_flag |= 1 << AV_MAIN_RECV_DCC_DONE;
		break;
	case AGTX_CMD_DIP_LSC:
		*recv_flag |= 1 << AV_MAIN_RECV_LSC_DONE;
		break;
	case AGTX_CMD_DIP_CTRL:
		*recv_flag |= 1 << AV_MAIN_RECV_CTRL_DONE;
		break;
	case AGTX_CMD_DIP_AE:
		*recv_flag |= 1 << AV_MAIN_RECV_AE_DONE;
		break;
	case AGTX_CMD_DIP_ISO:
		*recv_flag |= 1 << AV_MAIN_RECV_ISO_DONE;
		break;
	case AGTX_CMD_DIP_AWB:
		*recv_flag |= 1 << AV_MAIN_RECV_AWB_DONE;
		break;
	case AGTX_CMD_DIP_PTA:
		*recv_flag |= 1 << AV_MAIN_RECV_PTA_DONE;
		break;
	case AGTX_CMD_DIP_CSM:
		*recv_flag |= 1 << AV_MAIN_RECV_CSM_DONE;
		break;
	case AGTX_CMD_DIP_SHP:
		*recv_flag |= 1 << AV_MAIN_RECV_SHP_DONE;
		break;
	case AGTX_CMD_DIP_NR:
		*recv_flag |= 1 << AV_MAIN_RECV_NR_DONE;
		break;
	case AGTX_CMD_DIP_ROI:
		*recv_flag |= 1 << AV_MAIN_RECV_ROI_DONE;
		break;
	case AGTX_CMD_DIP_TE:
		*recv_flag |= 1 << AV_MAIN_RECV_TE_DONE;
		break;
	case AGTX_CMD_DIP_GAMMA:
		*recv_flag |= 1 << AV_MAIN_RECV_GAMMA_DONE;
		break;
	case AGTX_CMD_DIP_ENH:
		*recv_flag |= 1 << AV_MAIN_RECV_ENH_DONE;
		break;
	case AGTX_CMD_DIP_CORING:
		*recv_flag |= 1 << AV_MAIN_RECV_CORING_DONE;
		break;
	case AGTX_CMD_DIP_FCS:
		*recv_flag |= 1 << AV_MAIN_RECV_FCS_DONE;
		break;
	case AGTX_CMD_DIP_STAT:
		*recv_flag |= 1 << AV_MAIN_RECV_STAT_DONE;
		break;
	case AGTX_CMD_DIP_HDR_SYNTH:
		*recv_flag |= 1 << AV_MAIN_RECV_HDR_SYNTH_DONE;
		break;
	case AGTX_CMD_IMG_PREF:
		*recv_flag |= 1 << AV_MAIN_RECV_IMG_PREF_DONE;
		break;
	case AGTX_CMD_ADV_IMG_PREF:
		*recv_flag |= 1 << AV_MAIN_RECV_ADV_IMG_PREF_DONE;
		break;
	case AGTX_CMD_AWB_PREF:
		*recv_flag |= 1 << AV_MAIN_RECV_AWB_PREF_DONE;
		break;
	case AGTX_CMD_COLOR_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_COLOR_CONF_DONE;
		break;
	case AGTX_CMD_ANTI_FLICKER_CONF:
		*recv_flag |= 1 << AV_MAIN_RECV_ANTI_FLICKKER_CONF_DONE;
		break;
	case AGTX_CMD_DIP_SHP_WIN:
		*recv_flag |= 1 << AV_MAIN_RECV_SHP_WIN_DONE;
		break;
	case AGTX_CMD_DIP_NR_WIN:
		*recv_flag |= 1 << AV_MAIN_RECV_NR_WIN_DONE;
		break;
	default:
		break;
	}
	return 0;
}

/**
 * @brief send reply to central control.
 * @return The execution result.
 */

INT32 avmain_cc_send_reply(INT32 sockfd, AGTX_MSG_HEADER_S *cmd, INT32 ret)
{
	if (write(sockfd, cmd, sizeof(*cmd)) < 0) {
		SYS_TRACE("write socket error %d(%m)\n", errno);
		return -1;
	}
	if (write(sockfd, &ret, sizeof(INT32)) < 0) {
		SYS_TRACE("write socket error %d(%m)\n", errno);
		return -1;
	}
	return 0;
}

/**
 * @brief Processing cmd from central control.
 * @return The execution result.
 */
INT32 avmain_cc_cmd(INT32 sockfd, AGTX_MSG_HEADER_S *cmd)
{
	INT32 ret = 0;
	INT32 skip_notify = 0;
	switch (cmd->cid) {
	case AGTX_CMD_VIDEO_DEV_CONF:
		ret = avmain_set_dev_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_STITCH_CONF:
		ret = avmain_set_stitch_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_VIDEO_LAYOUT_CONF:
		ret = avmain_set_layout_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_VIDEO_STRM_CONF:
		ret = avmain_set_strm_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_IMG_PREF:
		ret = avmain_set_img_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_TD_CONF:
		ret = avmain_set_td_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_MD_CONF:
		ret = avmain_set_md_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_AROI_CONF:
		ret = avmain_set_aroi_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_PD_CONF:
		ret = avmain_set_pd_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_OD_CONF:
		ret = avmain_set_od_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_RMS_CONF:
		ret = avmain_set_rms_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_LD_CONF:
		ret = avmain_set_ld_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_EF_CONF:
		ret = avmain_set_ef_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_VDBG_CONF:
		ret = avmain_set_vdbg_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_VIDEO_PTZ_CONF:
		ret = avmain_set_video_ptz_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_SHD_CONF:
		ret = avmain_set_shd_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_EAIF_CONF:
		ret = avmain_set_eaif_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_PFM_CONF:
		ret = avmain_set_pfm_config(sockfd, cmd->len);
		skip_notify =1;
		break;
	case AGTX_CMD_BM_CONF:
		ret = avmain_set_bm_config(sockfd, cmd->len);
		skip_notify =1;
		break;
	case AGTX_CMD_DK_CONF:
		ret = avmain_set_dk_config(sockfd, cmd->len);
		skip_notify = 1;
		break;
	case AGTX_CMD_FLD_CONF:
		ret = avmain_set_fld_config(sockfd, cmd->len);
		skip_notify = 1;
		break;
	case AGTX_CMD_LSD_CONF:
		ret = avmain_set_lsd_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_OSD_CONF:
		ret = avmain_set_osd_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_OSD_PM_CONF:
		ret = avmain_set_osd_pm_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_AWB_PREF:
		ret = avmain_set_awb_pref_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_ADV_IMG_PREF:
		ret = avmain_set_adv_img_pref_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_DIP_CAL:
		ret = avmain_set_cal_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_DIP_DBC:
		ret = avmain_set_dbc_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_DIP_DCC:
		ret = avmain_set_dcc_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_DIP_LSC:
		ret = avmain_set_lsc_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_DIP_ROI:
		ret = avmain_set_roi_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_DIP_CTRL:
		ret = avmain_set_ctrl_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_DIP_AE:
		ret = avmain_set_ae_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_DIP_ISO:
		ret = avmain_set_iso_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_DIP_AWB:
		ret = avmain_set_awb_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_DIP_PTA:
		ret = avmain_set_pta_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_DIP_CSM:
		ret = avmain_set_csm_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_DIP_SHP:
		ret = avmain_set_shp_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_DIP_NR:
		ret = avmain_set_nr_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_DIP_SHP_WIN:
		ret = avmain_set_win_shp_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_DIP_NR_WIN:
		ret = avmain_set_win_nr_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_DIP_TE:
		ret = avmain_set_te_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_DIP_GAMMA:
		ret = avmain_set_gamma_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_DIP_ENH:
		ret = avmain_set_enh_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_DIP_CORING:
		ret = avmain_set_coring_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_DIP_FCS:
		ret = avmain_set_fcs_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_DIP_STAT:
		ret = avmain_set_stat_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_DIP_HDR_SYNTH:
		ret = avmain_set_hdr_synth_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_COLOR_CONF:
		ret = avmain_set_color_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_ANTI_FLICKER_CONF:
		ret = avmain_set_anti_flicker_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_LDC_CONF:
		ret = avmain_set_ldc_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_PANORAMA_CONF:
		ret = avmain_set_panorama_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_PANNING_CONF:
		ret = avmain_set_panning_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_SURROUND_CONF:
		ret = avmain_set_surround_config(sockfd, cmd->len);
		break;
	case AGTX_CMD_AUDIO_CONF:
		ret = avmain_set_audio_config(sockfd, cmd->len);
		break;
	default:
		SYS_TRACE("unknown command\n");
		ret = 0;
		break;
	}

	/*send notify to relate module when configuration changed*/
	if (!skip_notify)
		avmain_set_notify();
	//	cmd_reply.cid = cmd->cid;
	//	cmd_reply.sid = 0;
	//	cmd_reply.len = sizeof(INT32);
	//
	//	avmain_cc_send_reply(sockfd, &cmd_reply, ret);
#if 0
	if ((ret < 0)) {
		system("touch /usrdata/dbrst");
		system("reboot");
	}
#endif

	return ret;
}

INT32 avmain_request_all_config(INT32 sockfd)
{
	AGTX_MSG_HEADER_S cmd_header = { 0 };
	INT32 i = 0;
	INT32 cmd_num = 0;

	INT32 cmd[128] = {
		AGTX_CMD_VIDEO_DEV_CONF, AGTX_CMD_VIDEO_LAYOUT_CONF, AGTX_CMD_VIDEO_STRM_CONF, AGTX_CMD_STITCH_CONF,
		AGTX_CMD_TD_CONF,        AGTX_CMD_MD_CONF,           AGTX_CMD_AROI_CONF,       AGTX_CMD_PD_CONF,
		AGTX_CMD_OD_CONF,        AGTX_CMD_RMS_CONF,          AGTX_CMD_LD_CONF,         AGTX_CMD_EF_CONF,
		AGTX_CMD_VDBG_CONF,      AGTX_CMD_SHD_CONF,          AGTX_CMD_EAIF_CONF,       AGTX_CMD_PFM_CONF,
		AGTX_CMD_BM_CONF,        AGTX_CMD_DK_CONF,			 AGTX_CMD_FLD_CONF,        AGTX_CMD_LSD_CONF,        
		AGTX_CMD_OSD_CONF,       AGTX_CMD_OSD_PM_CONF,       AGTX_CMD_AUDIO_CONF,      AGTX_CMD_LDC_CONF,
		AGTX_CMD_PANORAMA_CONF,  AGTX_CMD_PANNING_CONF,      AGTX_CMD_SURROUND_CONF,   AGTX_CMD_VIDEO_PTZ_CONF,
		0
	};


	for (i = 0; i < 128; i++) {
		if (cmd[i] == 0) {
			break;
		}
		cmd_num++;
	}

	for (i = 0; i < cmd_num; i++) {
		cmd_header.cid = cmd[i];
		if (write(sockfd, &cmd_header, sizeof(cmd_header)) < 0) {
			SYS_TRACE("num %d cmd %d write socket error %d(%m)\n", i, cmd_header.cid, errno);
			return -1;
		}
	}

	return 0;
}

INT32 avmain_request_dip_low_level_config(INT32 sockfd)
{
	AGTX_MSG_HEADER_S cmd_header = { 0 };
	INT32 i = 0;
	INT32 cmd_num = 0;
	/* clang-format off */
	INT32 cmd[128] = { AGTX_CMD_DIP_CAL,     AGTX_CMD_DIP_DBC,    AGTX_CMD_DIP_DCC,
	                   AGTX_CMD_DIP_LSC,     AGTX_CMD_DIP_CTRL,   AGTX_CMD_DIP_AE,
	                   AGTX_CMD_DIP_ISO,     AGTX_CMD_DIP_AWB,    AGTX_CMD_DIP_PTA,
	                   AGTX_CMD_DIP_CSM,     AGTX_CMD_DIP_SHP,    AGTX_CMD_DIP_NR,
	                   AGTX_CMD_DIP_ROI,     AGTX_CMD_DIP_TE,     AGTX_CMD_DIP_GAMMA,
	                   AGTX_CMD_DIP_ENH,     AGTX_CMD_DIP_CORING, AGTX_CMD_DIP_FCS,
					   AGTX_CMD_DIP_STAT,    AGTX_CMD_DIP_HDR_SYNTH,   AGTX_CMD_DIP_SHP_WIN, AGTX_CMD_DIP_NR_WIN,  0 };
	/* clang-format on */

	for (i = 0; i < 128; i++) {
		if (cmd[i] == 0) {
			break;
		}
		cmd_num++;
	}

	for (i = 0; i < cmd_num; i++) {
		cmd_header.cid = cmd[i];
		if (write(sockfd, &cmd_header, sizeof(cmd_header)) < 0) {
			SYS_TRACE("num %d cmd %d write socket error %d(%m)\n", i, cmd_header.cid, errno);
			return -1;
		}
	}

	return 0;
}

INT32 avmain_request_dip_high_level_config(INT32 sockfd)
{
	AGTX_MSG_HEADER_S cmd_header = { 0 };
	INT32 i = 0;
	INT32 cmd_num = 0;
	// AGTX_CMD_COLOR_CONF need to after AGTX_CMD_ADV_IMG_PREF
	INT32 cmd[128] = { AGTX_CMD_ANTI_FLICKER_CONF, AGTX_CMD_IMG_PREF, AGTX_CMD_ADV_IMG_PREF, AGTX_CMD_AWB_PREF,
	                   AGTX_CMD_COLOR_CONF,
	0 };

	for (i = 0; i < 128; i++) {
		if (cmd[i] == 0) {
			break;
		}
		cmd_num++;
	}

	for (i = 0; i < cmd_num; i++) {
		cmd_header.cid = cmd[i];
		if (write(sockfd, &cmd_header, sizeof(cmd_header)) < 0) {
			SYS_TRACE("num %d cmd %d write socket error %d(%m)\n", i, cmd_header.cid, errno);
			return -1;
		}
	}

	return 0;
}

/**
 * @brief collect all video configuration from central control.
 * @return The execution result.
 */
INT32 avmain_collect_config(INT32 sockfd)
{
	AGTX_MSG_HEADER_S cmd_header = { 0 };
	INT32 ret = 0;
	INT32 err_cnt = 0;
	INT32 recv_flag = 0;
	fd_set read_fds;
	struct timeval tv = { 0 };

	if (avmain_request_all_config(sockfd) < 0) {
		return -1;
	}

	while (1) {
		tv.tv_sec = 0;
		tv.tv_usec = 100000;
		FD_ZERO(&read_fds);
		FD_SET(sockfd, &read_fds);
		ret = select(sockfd + 1, &read_fds, NULL, NULL, &tv);
		if (ret < 0) {
			printf("select error\n");
			continue;
		} else if (ret == 0) {
			//			printf( "select timeout\n" );
			continue;
		} else {
			if (err_cnt > 5) {
				SYS_TRACE("Too many error close socket and leave.\n");
				break;
			}

			ret = read(sockfd, &cmd_header, sizeof(cmd_header));
			if (ret != sizeof(cmd_header)) {
				SYS_TRACE("Read too short size %d %d(%m)\n", ret, errno);
				err_cnt++;
				continue;
			}

			if (avmain_cc_cmd(sockfd, &cmd_header) < 0) {
				return -1;
			}
			err_cnt = 0;
			/*set receive flag to check all configuration received*/
			avmain_set_cfg_recv_flag(cmd_header.cid, &recv_flag);
			/*FIXME remove*/
			SYS_TRACE("cmd %d flag %x\n", cmd_header.cid, recv_flag);
			/*check all configuration received*/
			if (AV_MAIN_RECV_DONE_MASK == recv_flag) {
				SYS_TRACE("Recv done cmd complete!\n");
				break;
			}
		}
	}
	return 0;
}

/**
 * @brief collect digital image processing configurations from central control.
 * @return The execution result.
 */
INT32 avmain_set_dip_default(INT32 sockfd)
{
	AGTX_MSG_HEADER_S cmd_header = { 0 };
	INT32 ret = 0;
	INT32 err_cnt = 0;
	INT32 recv_flag = 0;
	fd_set read_fds;
	struct timeval tv = { 0 };

	ret = VIDEO_getDevAttr(MPI_VIDEO_DEV(0));
	assert(ret == 0 && "VIDEO_getDevAttr fail\n");

	if (avmain_request_dip_low_level_config(sockfd) < 0) {
		return -1;
	}

	while (1) {
		tv.tv_sec = 0;
		tv.tv_usec = 100000;
		FD_ZERO(&read_fds);
		FD_SET(sockfd, &read_fds);
		ret = select(sockfd + 1, &read_fds, NULL, NULL, &tv);
		if (ret < 0) {
			printf("select error\n");
			continue;
		} else if (ret == 0) {
			//printf( "select timeout\n" );
			continue;
		} else {
			if (err_cnt > 5) {
				SYS_TRACE("Too many error close socket and leave.\n");
				break;
			}
			ret = read(sockfd, &cmd_header, sizeof(cmd_header));
			if (ret != sizeof(cmd_header)) {
				SYS_TRACE("Read too short size %d %d(%m)\n", ret, errno);
				err_cnt++;
				continue;
			}

			if (avmain_cc_cmd(sockfd, &cmd_header) < 0) {
				return -1;
			}

			err_cnt = 0;
			/*set receive flag to check all configuration received*/
			avmain_set_dip_cfg_recv_flag(cmd_header.cid, &recv_flag);
			/*FIXME remove*/
			SYS_TRACE("cmd %d flag %x\n", cmd_header.cid, recv_flag);
			/*check all dip low level configuration received*/
			if (AV_MAIN_RECV_DIP_LOW_LV_DONE_MASK == recv_flag) {
				/*update low level receive flag*/
				recv_flag |= 1 << AV_MAIN_RECV_DIP_LOW_LV_DONE_NUM;

				/*init dip low level global attribute*/
				ret = VIDEO_DIP_initDipHighAttr(MPI_VIDEO_DEV(0));
				assert(ret == 0 && "VIDEO_DIP_initDipHighAttr fail\n");

				/* start request CC to send dip high level config */
				if (avmain_request_dip_high_level_config(sockfd) < 0) {
					return -1;
				}
			}

			/*check all dip high level configuration received*/
			if (AV_MAIN_RECV_DIP_HIGH_LV_DONE_MASK == recv_flag) {
				SYS_TRACE("Database of DIP attribute is completed\n");
				break;
			}
		}
	}

	return 0;
}

/**
 * @brief register to central control.
 * @return The execution result.
 */

INT32 avmain_register_to_cc(INT32 sockfd)
{
	int ret;
	char buf[128] = { 0 };
	char reg_buf[128] = { 0 };
	char ret_cmd[128] = { 0 };

	sprintf(reg_buf, "{ \"master_id\":0, \"cmd_id\":%d, \"cmd_type\":\"ctrl\", \"name\":\"AV_MAIN\"}",
	        AGTX_CMD_REG_CLIENT);
	sprintf(ret_cmd, "{ \"master_id\": 0, \"cmd_id\": %d, \"cmd_type\": \"reply\", \"rval\": 0 }",
	        AGTX_CMD_REG_CLIENT);

	/*Send register information*/
	if (write(sockfd, &reg_buf, strlen(reg_buf)) < 0) {
		SYS_TRACE("write socket error %d(%m)\n", errno);
		return -1;
	}

	while (1) {
		ret = read(sockfd, buf, strlen(ret_cmd));
		if (ret != strlen(ret_cmd)) {
			SYS_TRACE("read socket error %d(%m)\n", errno);
			continue;
		}

		if (strncmp(buf, ret_cmd, strlen(ret_cmd))) {
			usleep(100000);
			SYS_TRACE("Wating CC replay register cmd\n");
			continue;
		} else {
			SYS_TRACE("Registered to CC from %s\n", "AV_MAIN");
			break;
		}
	}

	return 0;
}

#if 0 // Was used in avmain_case_stitch_to_cfg()
/**
 * @brief convert stitch parameter to configuration parameter.
 * @return The execution result.
 */
static void avmain_case_stitch_to_cfg(CFG_STITCH_S *attr, const MPI_STITCH_ATTR_S *stitch_attr)
{
	INT32 i = 0;
	INT32 j = 0;

	attr->dft_dist = stitch_attr->dft_dist;
	attr->table_num = stitch_attr->table_num;

	for ( i = 0;i < STITCH_SENSOR_NUM;i++) {
		attr->center[i].x = stitch_attr->center[i].x;
		attr->center[i].y = stitch_attr->center[i].y;
	}

	for ( i = 0;i < STITCH_TABLE_NUM;i++) {
		attr->table[i].dist = stitch_attr->table[i].dist;
		attr->table[i].ver_disp = stitch_attr->table[i].ver_disp;
		attr->table[i].straighten = stitch_attr->table[i].straighten;
		attr->table[i].src_zoom = stitch_attr->table[i].src_zoom;
		for ( j = 0;j < STITCH_SENSOR_NUM;j++) {
			attr->table[i].theta[j] = stitch_attr->table[i].theta[j];
			attr->table[i].radius[j] = stitch_attr->table[i].radius[j];
			attr->table[i].curvature[j] = stitch_attr->table[i].curvature[j];
			attr->table[i].fov_ratio[j] = stitch_attr->table[i].fov_ratio[j];
			attr->table[i].ver_scale[j] = stitch_attr->table[i].ver_scale[j];
			attr->table[i].ver_shift[j] = stitch_attr->table[i].ver_shift[j];
		}
	}
}
#endif

#if 0 // Was used in avmain_connect_to_cc()
/**
 * @brief config read from case config.
 * @return The execution result.
 */
static int avmain_read_from_case_config()
{
	INT32 i = 0;
	INT32 idx = 0;
	char *buf[] = {"mpi","-d/system/mpp/case_config/case_config_2001"};
	CFG_ALL_S  config = {{{0}}};
	CFG_ALL_S  *cfg = &config;
	CFG_SYS_S  *cfg_sys  = NULL;
	CFG_VIN_S  *cfg_vin  = NULL;
	CFG_PATH_S *cfg_path = NULL;
	CFG_VCHN_S *cfg_vchn = NULL;
	CFG_VENC_S *cfg_venc = NULL;
	CFG_ATTR_S cfg_attr = {0};
	MPI_MCVC_VBR_PARAM_S  *vbr = NULL;
	MPI_MCVC_CBR_PARAM_S  *cbr = NULL;

	SAMPLE_CONF_S conf = {{0}};

	init_conf(&conf);

	if (!parse(2, buf, &conf)) {
		SYS_TRACE("parse config fail\n");
		return -1;
	}

	cfg_sys = &cfg->cfg_camera[idx].cfg_sys;

	cfg_sys->max_pool_cnt = conf.sys.vb_conf.max_pool_cnt;
	for (i = 0; i < MAX_PUB_POOL; i++) {
		if (!conf.sys.vb_conf.pub_pool[i].blk_size) {
			break;
		}
		cfg_sys->pool[i].blk_cnt = conf.sys.vb_conf.pub_pool[i].blk_cnt;
		cfg_sys->pool[i].blk_size = conf.sys.vb_conf.pub_pool[i].blk_size;
		strcpy((char*)cfg_sys->pool[i].name,(char*)conf.sys.vb_conf.pub_pool[i].name);
	}

	cfg_attr.group = 0;
	cfg_attr.id = 0;
	if (CFG_setConfig(CFG_SET_CONFIG_SYS,&cfg_attr,(char*)cfg_sys,sizeof(CFG_SYS_S)) < 0) {
		return -1;
	}

	cfg_vin = &cfg->cfg_camera[idx].cfg_vin;

	cfg_vin->enable    = 1;
	cfg_vin->dev_idx   = conf.dev[idx].dev_idx;
	cfg_vin->bayer     = conf.dev[idx].gen.bayer;
	cfg_vin->hdr_mode  = conf.dev[idx].gen.hdr_mode;
	cfg_vin->stitch_en = conf.dev[idx].gen.stitch_en;
	cfg_vin->eis_en    = conf.dev[idx].gen.eis_en;
	cfg_vin->fps       = conf.dev[idx].gen.fps;

	/*read video input*/
	for (i = 0; i < conf.dev[idx].path_cnt; i++) {
		cfg_path = &cfg_vin->cfg_path[i];

		cfg_path->enable     = 1;
		cfg_path->path_idx   = conf.dev[idx].path[i].path_idx;
		cfg_path->sensor_idx = conf.dev[idx].path[i].gen.sensor_idx;
		cfg_path->width      = conf.dev[idx].path[i].gen.res.width;
		cfg_path->height     = conf.dev[idx].path[i].gen.res.height;
	}

	cfg_attr.group = 0;
	cfg_attr.id = 0;
	if (CFG_setConfig(CFG_SET_CONFIG_VIN,&cfg_attr,(char*)cfg_vin,sizeof(CFG_VIN_S)) < 0) {
		return -1;
	}

	for (i = 0; i < conf.dev[idx].chn_cnt; i++) {
		cfg_vchn = &cfg_vin->cfg_vchn[i];

		cfg_vchn->enable            = 1;
		cfg_vchn->chn_idx           = conf.dev[idx].chn[i].chn_idx;
		cfg_vchn->width             = conf.dev[idx].chn[i].gen.res.width;
		cfg_vchn->height            = conf.dev[idx].chn[i].gen.res.height;
		cfg_vchn->max_width         = conf.dev[idx].chn[i].gen.max_res.width;
		cfg_vchn->max_height        = conf.dev[idx].chn[i].gen.max_res.height;
		cfg_vchn->fps               = conf.dev[idx].chn[i].gen.fps;
		cfg_vchn->mirror            = conf.dev[idx].chn[i].gen.mirr_en;
		cfg_vchn->rotate            = conf.dev[idx].chn[i].gen.rotate;
		cfg_vchn->flip              = conf.dev[idx].chn[i].gen.flip_en;
		cfg_vchn->stitch_enable     = conf.dev[idx].chn[i].gen.stitch_en;
		avmain_case_stitch_to_cfg(&cfg_vchn->stitch,&conf.dev[idx].chn[i].cas);
		/*FIXME*/
//		cfg_vchn->vftr.od_en 		= conf.dev[idx].chn[i].vftr.od_en;
//		cfg_vchn->vftr.rms_en 		= conf.dev[idx].chn[i].vftr.rms_en;
//		cfg_vchn->vftr.ld_en 		= conf.dev[idx].chn[i].vftr.ld_en;
//		cfg_vchn->vftr.ef_en         = conf.dev[idx].chn[i].vftr.ef_en;

		cfg_vchn->td.enabled = conf.dev[idx].chn[i].vftr.td_en;
		cfg_vchn->td.endurance = 48;
		cfg_vchn->td.sensitivity = 128;
		cfg_vchn->md.enabled = conf.dev[idx].chn[i].vftr.md_en;
		cfg_vchn->md.min_speed = 5;
		cfg_vchn->md.max_speed = 0;
		cfg_vchn->md.sensitivity = 100;
		cfg_vchn->md.mode = CFG_IVA_MD_MODE_MOVING_AREA;
		cfg_vchn->md.det_region_cnt = 1;
		cfg_vchn->md.det_region_list[0].start_x = 0;
		cfg_vchn->md.det_region_list[0].start_y = 0;
		cfg_vchn->md.det_region_list[0].end_x = cfg_vchn->width;
		cfg_vchn->md.det_region_list[0].end_y = cfg_vchn->height;

		cfg_attr.group = 0;
		cfg_attr.id = i;
		if (CFG_setConfig(CFG_SET_CONFIG_VCHN,&cfg_attr,(char*)cfg_vchn,sizeof(CFG_VCHN_S)) < 0) {
			continue;
		}

		SYS_TRACE("vchn output width %d height %d\n",cfg_vchn->width,cfg_vchn->height);
	}

	/*read video encode*/
	for (i = 0;i < conf.enc_chn_cnt;i++) {
		cfg_venc = &cfg->cfg_camera[idx].cfg_venc[i];
		cfg_venc->enable = 1;
		cfg_venc->chn_idx = conf.enc_chn[i].chn_idx;
		cfg_venc->venc_type =
		     (conf.enc_chn[i].venc.type == MPI_VENC_TYPE_H264)?CFG_VENC_TYPE_H264:
		     (conf.enc_chn[i].venc.type == MPI_VENC_TYPE_H265)?CFG_VENC_TYPE_H265:
		     (conf.enc_chn[i].venc.type == MPI_VENC_TYPE_MJPEG)?CFG_VENC_TYPE_MJPEG:
		     CFG_VENC_TYPE_H264;
		cfg_venc->width = conf.enc_chn[i].chn.res.width;
		cfg_venc->height = conf.enc_chn[i].chn.res.height;
		cfg_venc->max_width = conf.enc_chn[i].chn.max_res.width;
		cfg_venc->max_height = conf.enc_chn[i].chn.max_res.height;
		cfg_venc->vbr_quality_level_index = 4; //TODO
		cfg_venc->bind_chn_idx = conf.enc_chn[i].bind_info.bind_chn_idx;
		cfg_venc->bind_dev_idx = conf.enc_chn[i].bind_info.bind_dev_idx;
		if (cfg_venc->venc_type == CFG_VENC_TYPE_H264) {
			cfg_venc->venc_profile =
			       (conf.enc_chn[i].venc.h264.profile == MPI_PRFL_BASELINE)?CFG_H264_5_PROFILE_BASELINE:
			       (conf.enc_chn[i].venc.h264.profile == MPI_PRFL_MAIN)?CFG_H264_5_PROFILE_MAIN:
			       (conf.enc_chn[i].venc.h264.profile == MPI_PRFL_HIGH)?CFG_H264_5_PROFILE_HIGH:
			       CFG_H264_5_PROFILE_BASELINE;
			cfg_venc->fps = conf.enc_chn[i].venc.h264.rc.frm_rate_o;
			cfg_venc->rc_mode =
			        (conf.enc_chn[i].venc.h264.rc.mode == MPI_RC_MODE_VBR)? CFG_VENC_RC_MODE_VBR:
	                (conf.enc_chn[i].venc.h264.rc.mode == MPI_RC_MODE_CBR)? CFG_VENC_RC_MODE_CBR:
	                (conf.enc_chn[i].venc.h264.rc.mode == MPI_RC_MODE_CQP)? CFG_VENC_RC_MODE_CQP:
	                CFG_VENC_RC_MODE_CBR;
			cfg_venc->gop = conf.enc_chn[i].venc.h264.rc.gop;
			if (cfg_venc->rc_mode == CFG_VENC_RC_MODE_CBR) {
				cbr = &conf.enc_chn[i].venc.h264.rc.cbr;
				cfg_venc->cbr_bitrate = cbr->bit_rate;
				cfg_venc->cbr_regression_speed = cbr->regression_speed;
				cfg_venc->cbr_scene_smooth = cbr->scene_smooth;
				cfg_venc->cbr_fluc_level = cbr->fluc_level;
				cfg_venc->cbr_min_qp = cbr->min_qp;
				cfg_venc->cbr_max_qp = cbr->max_qp;
			} else if (cfg_venc->rc_mode == CFG_VENC_RC_MODE_VBR) {
				vbr = &conf.enc_chn[i].venc.h264.rc.vbr;
				cfg_venc->vbr_max_bit_rate = vbr->max_bit_rate;
				cfg_venc->vbr_quality_level_index = vbr->quality_level_index;
			} else {
				SYS_TRACE("unknown rc mode %d\n",cfg_venc->rc_mode);
				cfg_venc->cbr_bitrate = 4000; //TODO set default value
			}
		} else if (cfg_venc->venc_type == CFG_VENC_TYPE_H265) {
			cfg_venc->venc_profile =
				       (conf.enc_chn[i].venc.h265.profile == MPI_PRFL_BASELINE)?CFG_H264_5_PROFILE_BASELINE:
				       (conf.enc_chn[i].venc.h265.profile == MPI_PRFL_MAIN)?CFG_H264_5_PROFILE_MAIN:
				       (conf.enc_chn[i].venc.h265.profile == MPI_PRFL_HIGH)?CFG_H264_5_PROFILE_HIGH:
				       CFG_H264_5_PROFILE_BASELINE;
			cfg_venc->fps = conf.enc_chn[i].venc.h265.rc.frm_rate_o;
			cfg_venc->rc_mode =
			           (conf.enc_chn[i].venc.h265.rc.mode == MPI_RC_MODE_VBR)? CFG_VENC_RC_MODE_VBR:
			           (conf.enc_chn[i].venc.h265.rc.mode == MPI_RC_MODE_CBR)? CFG_VENC_RC_MODE_CBR:
			           (conf.enc_chn[i].venc.h265.rc.mode == MPI_RC_MODE_CQP)? CFG_VENC_RC_MODE_CQP:
			           CFG_VENC_RC_MODE_CBR;
			cfg_venc->gop = conf.enc_chn[i].venc.h265.rc.gop;
			if (cfg_venc->rc_mode == CFG_VENC_RC_MODE_CBR) {
				cbr = &conf.enc_chn[i].venc.h265.rc.cbr;
				cfg_venc->cbr_bitrate = conf.enc_chn[i].venc.h265.rc.cbr.bit_rate;
				cfg_venc->cbr_regression_speed = cbr->regression_speed;
				cfg_venc->cbr_scene_smooth = cbr->scene_smooth;
				cfg_venc->cbr_fluc_level = cbr->fluc_level;
				cfg_venc->cbr_min_qp = cbr->min_qp;
				cfg_venc->cbr_max_qp = cbr->max_qp;
			} else if (cfg_venc->rc_mode == CFG_VENC_RC_MODE_VBR) {
				vbr = &conf.enc_chn[i].venc.h265.rc.vbr;
				cfg_venc->vbr_max_bit_rate = conf.enc_chn[i].venc.h265.rc.vbr.max_bit_rate;
				cfg_venc->vbr_quality_level_index = vbr->quality_level_index;
			} else {
				SYS_TRACE("unknown rc mode %d\n",cfg_venc->rc_mode);
				cfg_venc->cbr_bitrate = 4000; //TODO set default value
			}
		} else if (cfg_venc->venc_type == CFG_VENC_TYPE_MJPEG) {
			SYS_TRACE("Unsupport MPI_VENC_TYPE_MJPEG\n");
			cfg_venc->rc_mode = conf.enc_chn[i].venc.mjpeg.rc.mode;
			cfg_venc->fps = conf.enc_chn[i].venc.mjpeg.rc.frm_rate_o;
			if (cfg_venc->rc_mode == CFG_VENC_RC_MODE_CBR) {
				cfg_venc->cbr_bitrate = conf.enc_chn[i].venc.mjpeg.rc.max_bit_rate;
				cfg_venc->cbr_min_qp = conf.enc_chn[i].venc.mjpeg.rc.max_q_factor;
				cfg_venc->cbr_max_qp = conf.enc_chn[i].venc.mjpeg.rc.min_q_factor;
			} else if (cfg_venc->rc_mode == CFG_VENC_RC_MODE_VBR) {
				cfg_venc->vbr_max_bit_rate = conf.enc_chn[i].venc.mjpeg.rc.max_bit_rate;
				cfg_venc->vbr_quality_level_index = conf.enc_chn[i].venc.mjpeg.rc.quality_level_index;
			} else if (cfg_venc->rc_mode == CFG_VENC_RC_MODE_CQP) {
				cfg_venc->cbr_max_qp = conf.enc_chn[i].venc.mjpeg.rc.q_factor;
			} else {
				SYS_TRACE("unknown rc mode %d\n",cfg_venc->rc_mode);
				cfg_venc->cbr_bitrate = 4000; //TODO set default value
			}
		} else if (cfg_venc->venc_type == CFG_VENC_TYPE_JPEG) {
			SYS_TRACE("Unsupport MPI_VENC_TYPE_JPEG\n");
		} else {
			SYS_TRACE("unknown venc type %d,set to H264\n",cfg_venc->venc_type);
			cfg_venc->venc_type = CFG_VENC_TYPE_H264;
			cfg_venc->venc_profile = CFG_H264_5_PROFILE_BASELINE;
			cfg_venc->rc_mode = CFG_VENC_RC_MODE_CBR;
			cfg_venc->gop = conf.enc_chn[i].venc.h264.rc.gop;
			if (cfg_venc->rc_mode == CFG_VENC_RC_MODE_CBR) {
				cfg_venc->cbr_bitrate = conf.enc_chn[i].venc.h264.rc.cbr.bit_rate;
			} else if (cfg_venc->rc_mode == CFG_VENC_RC_MODE_VBR) {
				cfg_venc->vbr_max_bit_rate = conf.enc_chn[i].venc.h264.rc.vbr.max_bit_rate;
			} else {
				SYS_TRACE("unknown rc mode %d\n",cfg_venc->rc_mode);
				cfg_venc->cbr_bitrate = 4096; //TODO set default value
			}
		}

		cfg_attr.group = 0;
		cfg_attr.id = i;
		if (CFG_setConfig(CFG_SET_CONFIG_VENC,&cfg_attr,(char*)cfg_venc,sizeof(CFG_VENC_S)) < 0) {
			continue;
		}
	}

	return 0;
}
#endif

/**
 * @brief connect to central control.
 * @return The execution result.
 */
void *avmain_connect_to_cc(void *data)
{
	INT32 sockfd = -1;
	INT32 servlen = 0;
	INT32 ret = 0;
	INT32 err_cnt = 0;
	fd_set read_fds;
	struct timeval tv = { 0 };
	struct sockaddr_un serv_addr;
	AGTX_MSG_HEADER_S cmd_header = { 0 };
	AGTX_MSG_HEADER_S cmd_reply = { 0 };
	av_main_ctrl *av_ctrl = &g_av_main_ctrl;
	const char *th_name = "av_main_cc";
	printf("\tUpdate thread scheduling policy...\n");
	if (!setThreadSchedAttr(th_name))
		printThreadSchedAttr();
	else
		printf("Failed to set thread %s!\n", th_name);

	bzero((char *)&serv_addr, sizeof(serv_addr));
	serv_addr.sun_family = AF_UNIX;
	strcpy(serv_addr.sun_path, CC_SOCKET_PATH);
	servlen = strlen(serv_addr.sun_path) + sizeof(serv_addr.sun_family);

	if ((sockfd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
		SYS_TRACE("Create sockfd failed %d(%m)\n", errno);
		return NULL;
	}

	if (connect(sockfd, (struct sockaddr *)&serv_addr, servlen) < 0) {
		SYS_TRACE("Connecting to server failed %d(%m)\n", errno);
		close(sockfd);
		return NULL;
	}

	/*Register module to CC*/
	avmain_register_to_cc(sockfd);
#if 1

	if (avmain_collect_config(sockfd) < 0) {
		return NULL;
	}

	CFG_init();

	if (avmain_set_dip_default(sockfd) < 0) {
		return NULL;
	}

	ret = CFG_startAllModule();
	if (ret < 0) {
		system("touch /usrdata/dbrst");
		system("reboot");
	}

	av_ctrl->video_init = 1;

#else
	/*Initial from case config*/
	avmain_read_from_case_config();
	CFG_init();
	CFG_startAllModule();
#endif

	pthread_cond_signal(&g_video_cond);

	while (1) {
		tv.tv_sec = 0;
		tv.tv_usec = 100000;
		FD_ZERO(&read_fds);
		FD_SET(sockfd, &read_fds);
		ret = select(sockfd + 1, &read_fds, NULL, NULL, &tv);
		if (ret < 0) {
			printf("select error\n");
			continue;
		} else if (ret == 0) {
			//			printf( "select timeout\n" );
			continue;
		} else {
			if (err_cnt > 5) {
				SYS_TRACE("Too many error close socket and leave.\n");
				break;
			}

			ret = read(sockfd, &cmd_header, sizeof(cmd_header));
			if (ret < 0) {
				SYS_TRACE("Read failed %d(%m),leave thread.\n", errno);
				break;
			} else if (ret != sizeof(cmd_header)) {
				SYS_TRACE("Read too short %d(%m)\n", errno);
				err_cnt++;
				continue;
			}

			err_cnt = 0;

			ret = avmain_cc_cmd(sockfd, &cmd_header);

			cmd_reply.cid = cmd_header.cid;
			cmd_reply.sid = 0;
			cmd_reply.len = sizeof(INT32);

			avmain_cc_send_reply(sockfd, &cmd_reply, ret);
		}
	}

	close(sockfd);

	return NULL;
}

INT32 main()
{
	int ret = 0;
	FILE *frdy = NULL;
	pthread_t tid = 0;
    char name[16] = "av_main_cc";
    char tmpname[16] = {0};
    int rc = 0;


	ret = avmain_checkSingleInstance();
	if (ret) {
		exit(-1);
	}

	/*capture signal*/
    if (signal(SIGPIPE, SIG_IGN) == SIG_ERR) {
		perror("Cannot handle SIGPIPE!\n");
		return -1;
	}

	if (signal(SIGINT, avmain_handle_sig_int) == SIG_ERR) {
		perror("Cannot handle SIGINT!\n");
		return -1;
	}

	if (signal(SIGTERM, avmain_handle_sig_int) == SIG_ERR) {
		perror("Cannot handle SIGINT!\n");
		return -1;
	}

	if (signal(SIGSEGV, avmain_handle_sig_crash) == SIG_ERR) {
		perror("Cannot handle SIGINT!\n");
		return -1;
	}

	if (AVFTR_initServer() < 0) {
		SYS_TRACE("Init AVFTR server failed\n");
		return -1;
	}

	if (pthread_create(&tid, NULL, avmain_connect_to_cc, NULL) != 0) {
		SYS_TRACE("Create thread to avmain_cc failed.\n");
		return -1;
	}
    rc = pthread_setname_np (tid, name);
    sleep(1);
    if ( rc == 0 ) {
        if ( pthread_getname_np(tid, tmpname, sizeof(tmpname)) == 0) {
            printf("\n%s: Get thread name [Done]\n", tmpname);
        } else {
            printf("\n%s: Get thread name [Fail]\n", name);
        }
    }

	/*Waiting for video start.*/
	pthread_mutex_lock(&g_video_mutex);
	pthread_cond_wait(&g_video_cond, &g_video_mutex);
	pthread_mutex_unlock(&g_video_mutex);
	avmain_connect_to_event_demon();

	VFTR_UPDATE_init();
#ifdef ENABLE_RTSPSERVER
	RTSP_init();
#endif /* !ENABLE_RTSPSERVER */

	frdy = fopen(AVMAIN_RDY_FILE, "w");
	fclose(frdy);
	frdy = NULL;

	while (1) {
		sleep(1);
	}
    rc = pthread_join(tid,NULL);
    if (rc != 0) {
        printf("Error Occured at %s:%d \n",__func__,__LINE__);
    }

	return 0;
}
