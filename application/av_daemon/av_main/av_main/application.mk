SDKSRC_DIR ?= $(realpath $(CURDIR)/../../../..)
include $(SDKSRC_DIR)/application/internal.mk

BSP:=$(SDKSRC_DIR)
MPP:=$(BSP)/mpp
APP:=$(BSP)/application
FEATURE:=$(BSP)/feature

SRC_KO_PATH:=$(MPP)/ko
TARGET_BIN_PATH:=$(SYSTEM_BIN)
TARGET_KO_PATH:=$(SYSTEM_LIB)

SENS:=$(MPP)/custom/sensor
SAMPLE:=$(MPI_STREAM_PATH)/src/libsample
CONFIG_INC:=$(AVMAIN_PATH)/av_main/config/include
COMMON_INC:=$(AVMAIN_PATH)/av_main/include
VIDEO_INC:=$(AVMAIN_PATH)/av_main/video/include

export SENSOR:=$(SENSOR)
export SENS:=$(SENS)
export MPP_INC:=$(MPP_INC)
export COMMON_INC:=$(COMMON_INC)
export SAMPLE:=$(SAMPLE)
export INIPARSER:=$(INIPARSER)
