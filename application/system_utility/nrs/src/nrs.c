#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

#include "nrs-api.h"
#include "nrs-ioctl.h"

#define NRS_DEV_NODE "/dev/nrs"

/* Invalid Nrs key */
#define NRS_INVALID 0xffffffffffffffffUL

#define BITS(x, lsb, width) ((x >> lsb) & ((1 << width) - 1))

typedef uint64_t Nrs;

#ifdef NRS_DEBUG
#include <stdlib.h>

#define STDIN_MAX_STLEN 32
static Nrs g_key = 0;
static Nrs __GetNrs(void)
{
	return g_key;
}

#define CALL_CHECK_FN(f) do { printf(#f ": %d\n", f()); } while(0)
void do_all_func(void)
{
	CALL_CHECK_FN(AgtxGetCpuSpeed);
	CALL_CHECK_FN(AgtxSupportH264hp);
	CALL_CHECK_FN(AgtxSupportH265);
	CALL_CHECK_FN(AgtxSupportVencMpHp);
	CALL_CHECK_FN(AgtxGetMaxRes);
	CALL_CHECK_FN(AgtxGetMaxBitDepth);
	CALL_CHECK_FN(AgtxGetMaxEncPxlRate);
	CALL_CHECK_FN(AgtxGetMaxFePxlRate);
	CALL_CHECK_FN(AgtxIsTurnkey);
	CALL_CHECK_FN(AgtxSupportI2cs);
	CALL_CHECK_FN(AgtxSupportMipi);
	CALL_CHECK_FN(AgtxSupportLvds);
	CALL_CHECK_FN(AgtxSupportDualMipi);
	CALL_CHECK_FN(AgtxSupportDualLvds);
	CALL_CHECK_FN(AgtxSupportRstDeglitch);
	CALL_CHECK_FN(AgtxSupportGfxPlrXfm);
	CALL_CHECK_FN(AgtxSupportGfxSqrXfm);
	CALL_CHECK_FN(AgtxSupportGfxSphrXfm);
	CALL_CHECK_FN(AgtxSupportGfxLdc);
	CALL_CHECK_FN(AgtxSupportDmaAes);
	CALL_CHECK_FN(AgtxSupportBswAes);
	CALL_CHECK_FN(AgtxSupportHdr);
	CALL_CHECK_FN(AgtxSupportTnbHdr);
	CALL_CHECK_FN(AgtxSupportSbsHdr);
	CALL_CHECK_FN(AgtxSupportLineClctHdr);
	CALL_CHECK_FN(AgtxSupportLineIntlHdr);
	CALL_CHECK_FN(AgtxSupportPxlClctHdr);
	CALL_CHECK_FN(AgtxSupportPxlIntlHdr);
	CALL_CHECK_FN(AgtxGetWaferCode);
	CALL_CHECK_FN(AgtxGetPackageCode);
	CALL_CHECK_FN(AgtxGetCustomerId);
}

int main (void)
{
	printf("AUGENTIX NRS READER.\n");
	char str[STDIN_MAX_STLEN];
	while(1) {
		printf("Type a hex value (starts with 0x):\n");
		fgets(str, sizeof(str), stdin);
		g_key = strtoull(str, NULL, 0);
		printf("Hex: 0x%016llx\n", g_key);
		do_all_func();
	}
	return 0;
}
#else
static Nrs __GetNrs(void)
{
	int fd = -1;
	Nrs val = NRS_INVALID;

	fd = open(NRS_DEV_NODE, O_RDONLY);
	if (fd == -1) {
		perror("I'm sorry Dave, I'm afraid I can't do that.");
		return NRS_INVALID;
	}

	if (ioctl(fd, NRS_IOCTLR_READ, &val) < 0) {
		perror("Stop Dave. Stop Dave. I am afraid. I am afraid Dave.");
		val = NRS_INVALID;
	}

	close(fd);
	return val;
}
#endif /* NRS_DEBUG */

int AgtxGetCpuSpeed(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 5, 2);
}

int AgtxSupportH264hp(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 9, 1) == 0 ? 1 : 0;
}

int AgtxSupportH265(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 31, 1) == 0 ? 1 : 0;
}

int AgtxSupportVencMpHp(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 25, 1) == 0 ? 1 : 0;
}

int AgtxGetMaxRes(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 18, 2);
}

int AgtxGetMaxBitDepth(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 20, 2);
}

int AgtxGetMaxEncPxlRate(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 42, 2);
}

int AgtxGetMaxFePxlRate(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 29, 2);
}

int AgtxIsTurnkey(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 10, 1);
}

int AgtxSupportI2cs(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 27, 1) == 0 ? 1 : 0;
}

int AgtxSupportMipi(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 16, 1) == 0 ? 1 : 0;
}

int AgtxSupportLvds(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 17, 1) == 0 ? 1 : 0;
}

int AgtxSupportDualMipi(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 33, 1) == 0 ? 1 : 0;
}

int AgtxSupportDualLvds(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 34, 1) == 0 ? 1 : 0;
}

int AgtxSupportRstDeglitch(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 28, 1) == 0 ? 1 : 0;
}

int AgtxSupportGfxPlrXfm(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 22, 1) == 0 ? 1 : 0;
}

int AgtxSupportGfxSqrXfm(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 23, 1) == 0 ? 1 : 0;
}

int AgtxSupportGfxSphrXfm(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 32, 1) == 0 ? 1 : 0;
}

int AgtxSupportGfxLdc(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 24, 1) == 0 ? 1 : 0;
}

int AgtxSupportDmaAes(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 26, 1) == 0 ? 1 : 0;
}

int AgtxSupportBswAes(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 44, 1) == 0 ? 1 : 0;
}

int AgtxSupportHdr(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 35, 1) == 0 ? 1 : 0;
}

int AgtxSupportTnbHdr(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 36, 1) == 0 ? 1 : 0;
}

int AgtxSupportSbsHdr(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 37, 1) == 0 ? 1 : 0;
}

int AgtxSupportLineClctHdr(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 38, 1) == 0 ? 1 : 0;
}

int AgtxSupportLineIntlHdr(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 39, 1) == 0 ? 1 : 0;
}

int AgtxSupportPxlClctHdr(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 40, 1) == 0 ? 1 : 0;
}

int AgtxSupportPxlIntlHdr(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_FAIL;
	return BITS(val, 41, 1) == 0 ? 1 : 0;
}

uint32_t AgtxGetWaferCode(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_CODE_INVALID;
	return BITS(val, 48, 4);
}

uint32_t AgtxGetPackageCode(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_CODE_INVALID;
	return BITS(val, 52, 4);
}

uint32_t AgtxGetCustomerId(void)
{
	Nrs val = __GetNrs();
	if (val == NRS_INVALID)
		return NRS_CODE_INVALID;
	return BITS(val, 56, 8);
}
