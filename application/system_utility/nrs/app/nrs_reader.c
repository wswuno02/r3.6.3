#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "nrs-api.h"

#define CALL_CHECK_FN(f) do { printf(#f ": %d\n", f()); } while(0)

int main (void)
{
	printf("AUGENTIX NRS READER.\n");
	CALL_CHECK_FN(AgtxGetCpuSpeed);
	CALL_CHECK_FN(AgtxSupportH264hp);
	CALL_CHECK_FN(AgtxSupportH265);
	CALL_CHECK_FN(AgtxSupportVencMpHp);
	CALL_CHECK_FN(AgtxGetMaxRes);
	CALL_CHECK_FN(AgtxGetMaxBitDepth);
	CALL_CHECK_FN(AgtxGetMaxEncPxlRate);
	CALL_CHECK_FN(AgtxGetMaxFePxlRate);
	CALL_CHECK_FN(AgtxIsTurnkey);
	CALL_CHECK_FN(AgtxSupportI2cs);
	CALL_CHECK_FN(AgtxSupportMipi);
	CALL_CHECK_FN(AgtxSupportLvds);
	CALL_CHECK_FN(AgtxSupportDualMipi);
	CALL_CHECK_FN(AgtxSupportDualLvds);
	CALL_CHECK_FN(AgtxSupportRstDeglitch);
	CALL_CHECK_FN(AgtxSupportGfxPlrXfm);
	CALL_CHECK_FN(AgtxSupportGfxSqrXfm);
	CALL_CHECK_FN(AgtxSupportGfxSphrXfm);
	CALL_CHECK_FN(AgtxSupportGfxLdc);
	CALL_CHECK_FN(AgtxSupportDmaAes);
	CALL_CHECK_FN(AgtxSupportBswAes);
	CALL_CHECK_FN(AgtxSupportHdr);
	CALL_CHECK_FN(AgtxSupportTnbHdr);
	CALL_CHECK_FN(AgtxSupportSbsHdr);
	CALL_CHECK_FN(AgtxSupportLineClctHdr);
	CALL_CHECK_FN(AgtxSupportLineIntlHdr);
	CALL_CHECK_FN(AgtxSupportPxlClctHdr);
	CALL_CHECK_FN(AgtxSupportPxlIntlHdr);
	CALL_CHECK_FN(AgtxGetWaferCode);
	CALL_CHECK_FN(AgtxGetPackageCode);
	CALL_CHECK_FN(AgtxGetCustomerId);
	return 0;
}
