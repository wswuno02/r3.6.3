mod := $(notdir $(subdir))

# always enable access_mode
app-$(CONFIG_APP_ACCESS_MODE) += access_mode
PHONY += access_mode access_mode-clean access_mode-distclean
PHONY += access_mode-install access_mode-uninstall
access_mode:
	$(Q)$(MAKE) -C $(ACCESS_MODE_PATH) all

access_mode-clean:
	$(Q)$(MAKE) -C $(ACCESS_MODE_PATH) clean

access_mode-distclean:
	$(Q)$(MAKE) -C $(ACCESS_MODE_PATH) distclean

access_mode-install:
	$(Q)$(MAKE) -C $(ACCESS_MODE_PATH) install

access_mode-uninstall:
	$(Q)$(MAKE) -C $(ACCESS_MODE_PATH) uninstall

app-$(CONFIG_APP_ALARM_OUT) += alarm_out
PHONY += alarm_out alarm_out-clean alarm_out-distclean
PHONY += alarm_out-install alarm_out-uninstall
alarm_out: libprio
	$(Q)$(MAKE) -C $(ALARMOUT_PATH) all

alarm_out-clean: libprio-clean
	$(Q)$(MAKE) -C $(ALARMOUT_PATH) clean

alarm_out-distclean: libprio-distclean
	$(Q)$(MAKE) -C $(ALARMOUT_PATH) distclean

alarm_out-install: libprio-install
	$(Q)$(MAKE) -C $(ALARMOUT_PATH) install

alarm_out-uninstall: libprio-uninstall
	$(Q)$(MAKE) -C $(ALARMOUT_PATH) uninstall

app-$(CONFIG_APP_DAY_NIGHT_MODE) += day_night_mode
PHONY += day_night_mode day_night_mode-clean day_night_mode-distclean
PHONY += day_night_mode-install day_night_mode-uninstall
day_night_mode: libprio
	$(Q)$(MAKE) -C $(DAY_NIGHT_MODE_PATH) all

day_night_mode-clean: libprio-clean
	$(Q)$(MAKE) -C $(DAY_NIGHT_MODE_PATH) clean

day_night_mode-distclean: libprio-distclean
	$(Q)$(MAKE) -C $(DAY_NIGHT_MODE_PATH) distclean

day_night_mode-install: libprio-install
	$(Q)$(MAKE) -C $(DAY_NIGHT_MODE_PATH) install

day_night_mode-uninstall: libprio-uninstall
	$(Q)$(MAKE) -C $(DAY_NIGHT_MODE_PATH) uninstall

app-$(CONFIG_APP_EVENT_DAEMON) += event_daemon
PHONY += event_daemon event_daemon-clean event_daemon-distclean
PHONY += event_daemon-install event_daemon-uninstall
event_daemon: libtz
	$(Q)$(MAKE) -C $(EVENTD_PATH) all

event_daemon-clean: libtz-clean
	$(Q)$(MAKE) -C $(EVENTD_PATH) clean

event_daemon-distclean: libtz-distclean
	$(Q)$(MAKE) -C $(EVENTD_PATH) distclean

event_daemon-install: libtz-install
	$(Q)$(MAKE) -C $(EVENTD_PATH) install

event_daemon-uninstall: libtz-uninstall
	$(Q)$(MAKE) -C $(EVENTD_PATH) uninstall

app-$(CONFIG_APP_GPIO_DAEMONS) += gpio_daemons
PHONY += gpio_daemons gpio_daemons-clean gpio_daemons-distclean
PHONY += gpio_daemons-install gpio_daemons-uninstall
gpio_daemons:
	$(Q)$(MAKE) -C $(GPIO_DAEMONS_PATH) all

gpio_daemons-clean:
	$(Q)$(MAKE) -C $(GPIO_DAEMONS_PATH) clean

gpio_daemons-distclean:
	$(Q)$(MAKE) -C $(GPIO_DAEMONS_PATH) distclean

gpio_daemons-install:
	$(Q)$(MAKE) -C $(GPIO_DAEMONS_PATH) install

gpio_daemons-uninstall:
	$(Q)$(MAKE) -C $(GPIO_DAEMONS_PATH) uninstall

app-$(CONFIG_APP_MP_AUTOTEST) += mp_autotest
PHONY += mp_autotest mp_autotest-clean mp_autotest-distclean
PHONY += mp_autotest-install mp_autotest-uninstall
mp_autotest: libado libprio
	$(Q)$(MAKE) -C $(MP_AUTOTEST_PATH) all

mp_autotest-clean: libprio-clean libado-clean
	$(Q)$(MAKE) -C $(MP_AUTOTEST_PATH) clean

mp_autotest-distclean: libprio-distclean libado-distclean
	$(Q)$(MAKE) -C $(MP_AUTOTEST_PATH) distclean

mp_autotest-install: libado-install libprio-install
	$(Q)$(MAKE) -C $(MP_AUTOTEST_PATH) install

mp_autotest-uninstall: libprio-uninstall libado-uninstall
	$(Q)$(MAKE) -C $(MP_AUTOTEST_PATH) uninstall

app-$(CONFIG_APP_NRS) += nrs
PHONY += nrs nrs-clean nrs-distclean
PHONY += nrs-install nrs-uninstall
nrs:
	$(Q)$(MAKE) -C $(NRS_BUILD) all

nrs-clean:
	$(Q)$(MAKE) -C $(NRS_BUILD) clean

nrs-distclean:
	$(Q)$(MAKE) -C $(NRS_BUILD) distclean

nrs-install:
	$(Q)$(MAKE) -C $(NRS_BUILD) install

nrs-uninstall:
	$(Q)$(MAKE) -C $(NRS_BUILD) uninstall

app-$(CONFIG_APP_SNTP) += sntp
PHONY += sntp sntp-clean sntp-distclean
PHONY += sntp-install sntp-uninstall
sntp:
	$(Q)$(MAKE) -C $(SNTP_PATH) all

sntp-clean:
	$(Q)$(MAKE) -C $(SNTP_PATH) clean

sntp-distclean:
	$(Q)$(MAKE) -C $(SNTP_PATH) distclean

sntp-install:
	$(Q)$(MAKE) -C $(SNTP_PATH) install

sntp-uninstall:
	$(Q)$(MAKE) -C $(SNTP_PATH) uninstall

app-$(CONFIG_APP_SYSUPD_TOOLS) += sysupd_tools
PHONY += sysupd_tools sysupd_tools-clean sysupd_tools-distclean
PHONY += sysupd_tools-install sysupd_tools-uninstall
sysupd_tools:
	$(Q)$(MAKE) -C $(SYSUPD_TOOLS_PATH) all

sysupd_tools-clean:
	$(Q)$(MAKE) -C $(SYSUPD_TOOLS_PATH) clean

sysupd_tools-distclean:
	$(Q)$(MAKE) -C $(SYSUPD_TOOLS_PATH) distclean

sysupd_tools-install:
	$(Q)$(MAKE) -C $(SYSUPD_TOOLS_PATH) install

sysupd_tools-uninstall:
	$(Q)$(MAKE) -C $(SYSUPD_TOOLS_PATH) uninstall

app-$(CONFIG_APP_THERMAL_PROTECTION) += thermal_protection
PHONY += thermal_protection thermal_protection-clean
PHONY += thermal_protection-distclean
PHONY += thermal_protection-install thermal_protection-uninstall
thermal_protection:
	$(Q)$(MAKE) -C $(THERMAL_PROTECTION_PATH) all

thermal_protection-clean:
	$(Q)$(MAKE) -C $(THERMAL_PROTECTION_PATH) clean

thermal_protection-distclean:
	$(Q)$(MAKE) -C $(THERMAL_PROTECTION_PATH) distclean

thermal_protection-install:
	$(Q)$(MAKE) -C $(THERMAL_PROTECTION_PATH) install

thermal_protection-uninstall:
	$(Q)$(MAKE) -C $(THERMAL_PROTECTION_PATH) uninstall

app-$(CONFIG_APP_UVC) += uvc
PHONY += uvc uvc-clean uvc-distclean
PHONY += uvc-install uvc-uninstall
uvc: libcm libprio
	$(Q)$(MAKE) -C $(UVC_PATH) all

uvc-clean: libprio-clean libcm-clean
	$(Q)$(MAKE) -C $(UVC_PATH) clean

uvc-distclean: libprio-distclean libcm-distclean
	$(Q)$(MAKE) -C $(UVC_PATH) distclean

uvc-install: libcm-install libprio-install
	$(Q)$(MAKE) -C $(UVC_PATH) install

uvc-uninstall: libprio-uninstall libcm-uninstall
	$(Q)$(MAKE) -C $(UVC_PATH) uninstall

app-$(CONFIG_APP_WATCHDOG) += watchdog
PHONY += watchdog watchdog-clean watchdog-distclean
PHONY += watchdog-install watchdog-uninstall
watchdog:
	$(Q)$(MAKE) -C $(WATCHDOG_PATH) all

watchdog-clean:
	$(Q)$(MAKE) -C $(WATCHDOG_PATH) clean

watchdog-distclean:
	$(Q)$(MAKE) -C $(WATCHDOG_PATH) distclean

watchdog-install:
	$(Q)$(MAKE) -C $(WATCHDOG_PATH) install

watchdog-uninstall:
	$(Q)$(MAKE) -C $(WATCHDOG_PATH) uninstall

PHONY += $(mod) $(mod)-clean $(mod)-distclean
PHONY += $(mod)-install $(mod)-uninstall
$(mod): $(app-y)
$(mod)-clean: $(addsuffix -clean,$(app-y))
$(mod)-distclean: $(addsuffix -distclean,$(app-y))
$(mod)-install: $(addsuffix -install,$(app-y))
$(mod)-uninstall: $(addsuffix -uninstall,$(app-y))

APP_BUILD_DEPS += $(mod)
APP_CLEAN_DEPS += $(mod)-clean
APP_DISTCLEAN_DEPS += $(mod)-distclean
APP_INTALL_DEPS += $(mod)-install
APP_UNINTALL_DEPS += $(mod)-uninstall
