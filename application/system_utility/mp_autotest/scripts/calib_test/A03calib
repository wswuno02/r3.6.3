#!/bin/sh

PATH=/bin:/sbin:/usr/bin:/usr/sbin:/root/bin:/system/bin

test_name="calibtest"

sdcard_autotest="/mnt/sdcard/autotest"
sdcard_autotest_script="/mnt/sdcard/autotest/script"

# Define path for save result on SD Card
sdcard_autotest_res="/mnt/sdcard/autotest/$test_name"

# Define path for save result on DUT
dut_autotest_res="/calib/autotest/$test_name"

# Define path for autotest utils on SD Card
### Config
C_LED="$sdcard_autotest_script/led.conf"
C_IP_ADDRESS="$sdcard_autotest_script/ip_address.conf"
C_WPA_SUP="$sdcard_autotest_script/wpa_supplicant.conf"
C_LIGHT_TEST="$sdcard_autotest_script/light_test.conf" # Tool utils

### Database
D_MP_DB="$sdcard_autotest_script/ini_mp.db"
D_DUT_TMP_DB="/tmp/ini.db"

### Script
S_CCSERVER="$sdcard_autotest_script/C01ccserver"
S_AVMAIN="$sdcard_autotest_script/C02av_main"
S_UNICORN="$sdcard_autotest_script/C03unicorn"
S_GPIO_UTILS="$sdcard_autotest_script/gpio_utils" # Tool utils
S_IR_CUT="$sdcard_autotest_script/ir_cut.sh" # Tool utils
S_LED="$sdcard_autotest_script/led.sh" # Tool utils
S_USB_ETH="$sdcard_autotest_script/usb_eth.sh"

### Binary
B_LED="$sdcard_autotest_script/ledctrl"
B_LIGHT_TEST="$sdcard_autotest_script/light_test" # Tool utils
B_AUDIO="$sdcard_autotest_script/audioctrl" # Tool utils

##########################################################

ERROR()
{
	echo "[ERROR] ${1}"
}

WARN()
{
	echo "[WARN] ${1}"
}

NOTICE()
{
	echo "[NOTICE] ${1}"
}

INFO()
{
	echo "[INFO] ${1}"
}

DEBUG()
{
	echo "[DEBUG] ${1}"
}

##########################################################

# $1: file
checkExist() {
	#retval: 1: FAIL,0: SUCCESS

	if [ -e "$1" ]; then
		INFO "$1 found."
		retval=0
	else
		ERROR "$1 not found."
		retval=1
	fi

	return $retval
}

# $1: type
checkNetworkConnection() {

	DEBUG "checkNetworkConnection(): intput $1"

	retval=0 # 1: FAIL,0: SUCCESS

	if [ "$1" == "eth0" ]; then
		eth_carrier=$(cat /sys/class/net/eth0/carrier)
	elif [ "$1" == "eth1" ]; then
		$S_USB_ETH start eth1 #usb2eth
		eth_carrier=$(cat /sys/class/net/eth1/carrier)
	elif [ "$1" == "wlan0" ]; then

		ssid=$(cat $C_WPA_SUP | awk '/ssid/{print substr($1, 6)}')

		if [ ! -e "$C_WPA_SUP" ]; then
			ERROR "$C_WPA_SUP not found."
			retval=1
			return $retval
		fi

		# Set wifi connection #
		wpa_supplicant -B -i wlan0 -c $C_WPA_SUP -Dwext

		INFO "WIFI connecting ..."
		sleep 3

		if [ ! -e "$C_IP_ADDRESS" ]; then
			ERROR "$C_IP_ADDRESS not found."
			retval=1
			return $retval
		else
			ip_addr=`cat $C_IP_ADDRESS`
			ifconfig wlan0 $ip_addr netmask 255.255.255.0
		fi

		wifi_essid=$(iwconfig wlan0 | awk '/IEEE/{print substr($4, 7)}')
		INFO "DUT wifi_essid = $wifi_essid"
	else
		ERROR "Invaild network type $type, only support 'eth0/eth1' or 'wlan0'."
		retval=1
		return $retval
	fi

	query_connect_cnt=0

	case "$1" in
		eth0)
			DEBUG "ETH0"

			while [ "$eth_carrier" != "1" ]
			do
				query_connect_cnt=$((query_connect_cnt+1))
				sleep 1

				#if [ "$query_connect_cnt" -gt 10 ]; then
				#	ERROR "@@@ Wait too long for eth0 connect ... $query_connect_cnt @@@"
				#	retval=1
				#	break
				#fi

				NOTICE "@@@ Query eth0 carrier $eth_carrier ... ($query_connect_cnt) @@@"
				eth_carrier=$(cat /sys/class/net/eth0/carrier)
			done

		;;
		eth1)
			DEBUG "ETH1"

			while [ "$eth_carrier" != "1" ]
			do
				query_connect_cnt=$((query_connect_cnt+1))
				sleep 1

				#if [ "$query_connect_cnt" -gt 10 ]; then
				#	ERROR "@@@ Wait too long for eth1 connect ... $query_connect_cnt @@@"
				#	retval=1
				#	break
				#fi

				NOTICE "@@@ Query eth1 carrier $eth_carrier ... ($query_connect_cnt) @@@"
				eth_carrier=$(cat /sys/class/net/eth1/carrier)
			done

		;;
		wlan0)
			DEBUG "WLAN0"

			while [ "$wifi_essid" != "$ssid" ]
			do
				query_connect_cnt=$((query_connect_cnt+1))
				sleep 1

				#if [ "$query_connect_cnt" -gt 10 ]; then
				#	ERROR "@@@ Wait too long for connect ... $query_connect_cnt @@@"
				#	retval=1
				#	break
				#fi

				NOTICE "@@@ Query wifi essid $essid ... ($query_connect_cnt) @@@"
				wifi_essid=$(iwconfig wlan0 | awk '/IEEE/{print substr($4, 7)}')
			done

		;;
		*)
			retval=1
		;;
	esac

	return "$retval"
}

checkAvmainStatus() {

	retval=0 # 1: FAIL, 0: SUCCESS
	local all_chn_state=$(cat /dev/enc | awk '/State/{print substr($2, 1)}')
	local chn_state_0=$(echo ${all_chn_state} | cut -d " " -f 1)
	local query_chn_state_cnt=0

	while [ "$chn_state_0" != "RUNNING" ]
	do
		query_chn_state_cnt=$((query_chn_state_cnt+1))
		sleep 1

		if [ "$query_chn_state_cnt" -gt 10 ]; then
			ERROR "@@@ Wait too long for av_main finish ... $query_chn_state_cnt @@@"
			retval=1
			break
		fi

		NOTICE "@@@ Query channel state0 $chn_state_0 ... @@@"
		all_chn_state=$(cat /dev/enc | awk '/State/{print substr($2, 1)}')
		chn_state_0=$(echo ${all_chn_state} | cut -d " " -f 1)
	done

	return "$retval"
}

#####################
##### main flow #####
#####################

# Let user decide if DUT should erase the previous test result of the station by click button #
C_BTN="$sdcard_autotest_script/btn.conf"
B_BTN="$sdcard_autotest_script/btndetect"
dut_tmp_btn_detect_res="/tmp/btnDetectRes"
wait_time=2000000
click_time=1000000
if [ -d $dut_autotest_res ]; then
	touch $dut_tmp_btn_detect_res

	$B_LED -r -f 250 $C_LED &
	pid=$!

	# Click button #
	NOTICE "Please click button greater than $wait_time(ms) to test AGAIN..."
	NOTICE "Please click button less than $wait_time(ms) to SKIP test..."

	$B_BTN -c $C_BTN
	sleep 1
	click_time=$(cat $dut_tmp_btn_detect_res)

	echo "$(($click_time))"

	INFO "click_time = $click_time(us)."

	if [ $click_time -lt $wait_time ]; then
		# SKIP test, let led flicker #
		INFO "@@@ SKIP test @@@"
		kill -SIGTERM $pid; wait $pid
		$B_LED -rb -f 500 $C_LED &
		pid=$!
		sleep 3
		kill -SIGTERM $pid
		exit
	else
		# Test AGAIN, let led flicker #
		INFO "@@@ Test AGAIN @@@"
		kill -SIGTERM $pid; wait $pid
	fi
fi

mkdir -p $dut_autotest_res; rm -rf $dut_autotest_res/*
mkdir -p $sdcard_autotest_res; rm -rf $sdcard_autotest_res/*

# Let led flicker, and get pid #
$B_LED -rb -f 500 $C_LED &
pid=$!

# Check mode #
MODE=/system/bin/mode
mode=$($MODE)

if [ $mode != "factory" ]; then
	ERROR "Invaild mode($mode), it should be factory mode."

	kill -SIGTERM $pid; wait $pid
	$B_LED -ry $C_LED # Light-up red led #
	exit
fi

# Write mac_address
mac_address=$(cat /sys/class/net/wlan0/address)
echo $mac_address > /tmp/mac_address

# Check network connection #
type=eth1 # type: eth0, eth1(usb2eth), wlan0

checkNetworkConnection $type
retval=$?

if [ "$type" == "eth0" ]; then
	ip=$(ifconfig eth0 | awk '/inet addr/{print substr($2,6)}')
elif [ "$type" == "eth1" ]; then
	ip=$(ifconfig eth1 | awk '/inet addr/{print substr($2,6)}')
elif [ "$type" == "wlan0" ]; then
	ip=$(ifconfig wlan0 | awk '/inet addr/{print substr($2,6)}')
else
	ERROR "Invaild network type $type, only support 'eth0/eth1' or 'wlan0'."

	kill -SIGTERM $pid; wait $pid
	$B_LED -ry $C_LED # Light-up red led #
	exit
fi

if [ "$retval" != 0 ]; then
	ERROR "Connect failure!!!"
	ERROR "If network type is "eth0/eth1", please check the network cable connection is correct."
	ERROR "If network type is "wlan0", please check the wpa_supplicant.conf is correct."

	kill -SIGTERM $pid; wait $pid
	$B_LED -ry $C_LED # Light-up red led #
	exit
else
	INFO "Connect success: ip-address of DUT is <$ip>"

	kill -SIGTERM $pid; wait $pid
	$B_LED -by $C_LED # Light-up blue led #
fi

# Copy database to /tmp/ini.db #
cp -f $D_MP_DB $D_DUT_TMP_DB; sync

# Start central control #
$S_CCSERVER start

# Start audio/video sample program #
$S_AVMAIN start

checkAvmainStatus
retval=$?
if [ "$retval" != 0 ]; then
	ERROR "Check state failure: av_main is not ready"
	ERROR "Please check if the av_main is correct."

	$B_LED -ry $C_LED # Light-up red led #
	exit
else
	INFO "Check state success: av_main is ready"
	$B_LED -by $C_LED # Light-up blue led #
fi

# Start dut/tool communication program #
$S_UNICORN start

# Light-up blue&red led #
$B_LED -rby $C_LED