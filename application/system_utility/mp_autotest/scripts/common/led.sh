#!/bin/sh

if [ "${1}" == "-h" ]; then
  echo "#======================#"
  echo "#== Turn on/off LED! ==#"
  echo "=======================#"
  echo "# NOTE: This script is used to turn on or turn off LED."
  echo ""
  echo "USAGE:"
  echo "  sh led.sh [LED] [VALUE]"
  echo ""
  echo "OPTION:"
  echo "  [LED]: led gpio pin number"
  echo "  [VALUE]: 0 or 1"
  echo ""
  echo "EXAMPLE:"
  echo "  sh led.sh 60 0"
  echo "  sh led.sh 60 1"
fi

LED=$1
VALUE=$2

Path1="/sys/class/gpio/gpio${LED}/"

#GPIO is already exported
if [ "${VALUE}" == '1' ] && [ -d "$Path1" ]; then

  # Turn on LED
  echo 1     > /sys/class/gpio/gpio${LED}/value

#GPIO is not exported
elif [ "${VALUE}" == '1' ] && [ ! -d "$Path1" ]; then

  # Export LED GPIO
  echo ${LED}    > /sys/class/gpio/export
  echo "out"  > /sys/class/gpio/gpio${LED}/direction

  # Turn on LED
  echo 1     > /sys/class/gpio/gpio${LED}/value

#GPIO is already exported
elif [ "${VALUE}" == '0' ] && [ -d "$Path1" ]; then

  # Turn off LED
  echo 0     > /sys/class/gpio/gpio${LED}/value


#GPIO is not exported
elif [ "${VALUE}" == '0' ] && [ ! -d "$Path1" ]; then

  # Export LED GPIO
  echo ${LED}    > /sys/class/gpio/export
  echo "out"  > /sys/class/gpio/gpio${LED}/direction

  # Turn off LED
  echo 0     > /sys/class/gpio/gpio${LED}/value
fi