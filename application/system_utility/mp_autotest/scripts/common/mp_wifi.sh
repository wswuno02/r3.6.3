#!/bin/sh

interface=wlan0
wpa_exist=0



# check if the command line is correct
if [ -z "$1" ]; then
   echo "[error] no input parameter"
   echo "usage: wifi_on.sh on wpa_supplicant.conf ip_address.conf" 
   echo "usage: wifi_on.sh off" 
   exit
fi

ctrl=$1
if [ "$ctrl" == "off" ]; then
	#ps | grep udhcpc | grep -v grep | awk '{ print $1}' | xargs kill -9 $1
	wifi_pid=$(ps | grep udhcpc | grep -v grep | awk '{ print $1}')
	echo $wifi_pid
	kill -9  $wifi_pid; wait $wifi_pid
	wpa_cli -i $interface terminate
	ifconfig $interface down
	exit
elif [ "$ctrl" == "on" ]; then
	# check if the wpa_supplicant.conf is exist
	if [ -f "$2" ]; then
	   conf=$2
	else
	   echo "[error] no such file:" $2
	   exit
	fi

	# check if the ip_address.conf is exist
	if [ -f "$3" ]; then
	   ip_conf=$3
	else
	   echo "[error] no such file:" $3
	   exit
	fi
	
	# check if the wpa_supplicant is running
	wpa_exist=$(ps | grep "wpa_supplicant -B" | grep -v grep | awk '{ print $1 }')
	if [ ! -z $wpa_exist ]; then
		wifi_pid=$(ps | grep udhcpc | grep -v grep | awk '{ print $1}')
		echo $wifi_pid
		kill -9 $wifi_pid; wait $wifi_pid
		wpa_cli -i $interface terminate
		ifconfig $interface down
	fi
else
   echo "[error] no such ctrl:" $1
   exit
fi

# start wpa_suppliant to connect to wifi ap
wpa_supplicant -B -i $interface -c $conf -Dnl80211,wext


# clear the old ip-address and request ip-address from wifi-ap
# Do NOT modify the file path of PID file, otherwise connmngr will fail
udhcpc -n -i $interface -R -t 20 -T 2 -p /var/run/udhcpc.pid -S

ret=$?
if [ $ret != 0 ]; then
   echo "Start udhcpc again"
   udhcpc -b -i $interface -R -t 20 -p /var/run/udhcpc.pid -S
fi

ip_addr=`cat $ip_conf`
ifconfig $interface $ip_addr netmask 255.255.255.0

# check if connection is established.
wlan_ip=$(ifconfig $interface | awk '/inet addr/{print substr($2,6)}')
if [ -z "$wlan_ip" ]; then
   echo "[error] connect failure: don't get ip-address."
   echo "please check if the settings of wpa_supplicant.conf is correct."
else
   echo "[finished] connect success: ip-address<$wlan_ip>"
fi
