#!/bin/sh
#
# Start/stop USB Ethernet adapter
#

usb_eth_adap=asix # USB Ethernet adapter driver
ip=`cat /mnt/sdcard/autotest/script/ip_address.conf`
mask=255.255.255.0
usb_switch_pin=0
switch_sel_usb_conn=1 # USB switch selection for USB connector
switch_sel_usb_wifi=0 # USB switch selection for USB WiFi

eth_intf=$2


# export_gpio(): export GPIO pin
export_gpio() {
	echo $1 > /sys/class/gpio/export
}

# unexport_gpio(): unexport GPIO pin
unexport_gpio() {
	echo $1 > /sys/class/gpio/unexport
}

# set_gpio(): GPIO setting function
set_gpio() {
	GPIO_ID=$1
	DIRECTION=$2
	VALUE=$3

	export_gpio $GPIO_ID

	if [ $DIRECTION = "out" ]; then
	        echo "out" > /sys/class/gpio/gpio$GPIO_ID/direction
		if [ ! $VALUE ]; then # Output level not specified
			echo "GPIO output level not specified!"
			exit 1;
		elif [ $VALUE = "1" ]; then # HIGH
			echo 1 > /sys/class/gpio/gpio$GPIO_ID/value
		elif [ $VALUE = "0" ]; then # LOW
			echo 0 > /sys/class/gpio/gpio$GPIO_ID/value
		else #INVALID
			echo "Invalid GPIO value!"
			exit 1;
		fi
	elif [ $DIRECTION = "in" ]; then
		echo "in" > /sys/class/gpio/gpio$GPIO_ID/direction
	fi

	unexport_gpio $GPIO_ID
}


case "$1" in
  start)
        printf "Starting USB Ethernet: "
        ifconfig eth0 down # Disable eth0 if it exist to prevent unexpected error
        set_gpio $usb_switch_pin "out" $switch_sel_usb_conn # Set USB switch to USB connector
        sleep 3 # Wait USB Ethernet adapter driver done
        # modprobe $usb_eth_adap # Probe USB Ethernet adapter driver, it will load usbnet automatically
        ifconfig $eth_intf up # Enable USB Ethernet interface
        ifconfig $eth_intf $ip netmask $mask # Set IP and NETMASK for USB Ethernet interface
        ;;
  stop)
        printf "Stopping USB Ethernet: "
        ifconfig $eth_intf down
        [ $? = 0 ] && echo "OK" || echo "FAIL"
        # rmmod $usb_eth_adap # Remove USB Ethernet adapter driver
        # rmmod usbnet # Remove USB Ethernet net driver
        set_gpio $usb_switch_pin "out" $switch_sel_usb_wifi # Set USB switch to USB WiFi
        ;;
  restart|reload)
        "$0" stop
        "$0" start
        ;;
  *)
        echo "Usage: $0 {start|stop|restart}"
        exit 1
esac

exit $?
