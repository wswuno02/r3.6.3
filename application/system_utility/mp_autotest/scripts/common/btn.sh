#!/bin/sh

PATH=/bin:/sbin:/usr/bin:/usr/sbin:/root/bin:/system/bin

##########################################################

ERROR()
{
	echo "[ERROR] ${1}"
}

WARN()
{
	echo "[WARN] ${1}"
}

NOTICE()
{
	echo "[NOTICE] ${1}"
}

INFO()
{
	echo "[INFO] ${1}"
}

DEBUG()
{
	echo "[DEBUG] ${1}"
}

##########################################################

if [ "${1}" == "-h" ]; then
  echo ""
  echo "USAGE:"
  echo "  sh btn.sh [BIN] [CONF]"
  echo ""
  echo "EXAMPLE:"
  echo "sh btn.sh btndetect btn.conf"
fi

INFO()
{
	echo "[INFO] ${1}"
}

C_BTN=$2
B_BTN=$1

dut_tmp_btn_detect_res="/tmp/btnDetectRes"
wait_time=2000
click_time=-1
rm $dut_tmp_btn_detect_res
touch $dut_tmp_btn_detect_res 
echo $click_time > $dut_tmp_btn_detect_res

# Click button #
NOTICE "Please click button greater than $wait_time(ms)."

$B_BTN -c $C_BTN &
pid=$!
echo $pid

sleep 5
click_time=$(cat $dut_tmp_btn_detect_res)

echo "$(($click_time))"

#INFO "click_time = $click_time(us)."

if [ $click_time -gt $wait_time ]; then
	INFO "@@@ PRESS BUTTON @@@"
	echo INFO
	exit
else
	# Test AGAIN, let led flicker #
	INFO "@@@ NO PRESS BUTTON @@@"
	kill -SIGTERM $pid; wait $pid
	exit
fi
