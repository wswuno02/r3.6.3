#ifndef MB_TEST_LED_H_
#define MB_TEST_LED_H_

#include "led_conf.h"

typedef enum led_type {
	LED_TYPE_RED = 0,
	LED_TYPE_BLUE,
	LED_TYPE_NUM
} LedType;

typedef enum led_flicker_speed {
	LED_FLICKER_FAST = 0,
	LED_FLICKER_MED,
	LED_FLICKER_SLOW,
	LED_FLICKER_NUM
} LedFlickerSpeed;


int LED_init(LedConf *);
int LED_setFlicker(LedType, int);
int LED_setStatus(LedType, int);
void LED_exit(void);

#endif
