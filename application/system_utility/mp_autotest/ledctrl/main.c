#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <json.h>

#include "led.h"
#include "parse.h"

LedCtrlConf g_conf;
int flicker_speed;
int flicker_enable;
int level;
int blue_en = 0;
int red_en = 0;
char *log_name = "ledctrl";
char *json_str = NULL;

char *getFileContent(const char *);
void printConf(LedCtrlConf *);
void initLedCtrl(LedCtrlConf *);
void exitLedCtrl();
void help();
void parse(int, char **);

static void handleSigInt(int signo)
{
	//	UINT32 idx = 0;

	if (signo == SIGINT) {
		printf("Caught SIGINT!\n");
	} else if (signo == SIGTERM) {
		printf("Caught SIGTERM!\n");
	} else {
		perror("Unexpected signal!\n");
	}

	if (json_str) {
		free(json_str);
		json_str = NULL;
	}

	exitLedCtrl();

	exit(0);
}

int main(int argc, char **argv)
{

	parse(argc, argv);
	printConf(&g_conf);
	initLedCtrl(&g_conf);

	/* TODO: add more test here */
	if (flicker_enable) {
		if (red_en) {
			LED_setFlicker(LED_TYPE_RED, flicker_speed);
		}
		if (blue_en) {
			LED_setFlicker(LED_TYPE_BLUE, flicker_speed);
		}

		while(1) {
			sleep(1);
		}
	} else {
		if (red_en) {
			LED_setStatus(LED_TYPE_RED, level);
		}

		if (blue_en) {
			LED_setStatus(LED_TYPE_BLUE, level);
		}
	}

	exitLedCtrl();

	return 0;
}

void parse(int argc, char **argv)
{
	json_object *jobj = NULL;
	int opt;

	while ((opt = getopt(argc, argv, "rbynf:h")) != -1) {
		switch (opt) {
		case 'r':
			red_en = 1;
			break;
		case 'b':
			blue_en = 1;
			break;
		case 'y':
			flicker_enable = 0;
			level = 1;
			break;
		case 'n':
			flicker_enable = 0;
			level = 0;
			break;
		case 'f':
			flicker_enable = 1;
			flicker_speed = atoi(optarg);
			break;
		case 'h':
			help();
			break;
		default:
			exit(EXIT_FAILURE);
			break;
		}
	}

	if (optind >= argc) {
		fprintf(stderr, "Expect file name after options.\n");
		exit(EXIT_FAILURE);
	}

	json_str = getFileContent(argv[optind]);
	if (!json_str) {
		printf("Failed to read %s.", argv[optind]);
		exit(EXIT_FAILURE);
	}

	jobj = json_tokener_parse(json_str);
	if (jobj == NULL) {
		printf("Invalid JSON string.");
		goto free_json_str;
	}

	if (signal(SIGINT, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGINT!\n");
		goto free_json_str;
	}

	if (signal(SIGTERM, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGTERM!\n");
		goto free_json_str;
	}

	parse_led_control(&g_conf, jobj);

	return;

free_json_str:

	free(json_str);
	json_str = NULL;
	exit(EXIT_FAILURE);
}

char *getFileContent(const char *path)
{
	char *ptr = NULL;
	FILE *fp;
	long fsize = 0;

	if ((fp = fopen(path, "r")) != NULL) {
		fseek(fp, 0L, SEEK_END);
		fsize = ftell(fp);
		fseek(fp, 0L, SEEK_SET);

		ptr = (char *)malloc(fsize + 1);

		if (!ptr) {
			goto end;
		}

		memset(ptr, 0, fsize);

		fread(ptr, fsize, 1, fp);

		ptr[fsize] = 0;

		if (ferror(fp)) {
			free(ptr);
		}
	}

end:
	fclose(fp);
	return ptr;
}

void initLedCtrl(LedCtrlConf *conf)
{
	if (LED_init(&conf->led_conf)) {
		goto end;
	}

	free(json_str);
	json_str = NULL;

	return;
end:
	free(json_str);
	json_str = NULL;
	exit(1);
}

void exitLedCtrl()
{
	LED_exit();
}

void printConf(LedCtrlConf *conf)
{
	printf("red_led.gpio = %d\n", conf->led_conf.led[0].gpio);
	printf("red_led.level_off = %d\n", conf->led_conf.led[0].level_off);
	printf("red_led.level_on = %d\n", conf->led_conf.led[0].level_on);
	printf("blue_led.gpio = %d\n", conf->led_conf.led[1].gpio);
	printf("blue_led.level_off = %d\n", conf->led_conf.led[1].level_off);
	printf("blue_led.level_on = %d\n", conf->led_conf.led[1].level_on);
	printf("led.period = %d\n", conf->led_conf.period);
}

void help()
{
	printf("Usuage:\n");
	printf("\t$s [OPTION] <GPIO>\n");
	printf("\n");
	printf("\t-r\t\tspecify red led\n");
	printf("\t-b\t\tspecify blue led\n");
	printf("\t-f PERIOD\tenable flicker mode with PERIOD\n");
	printf("\t-y \t\tenable spcified leds\n");
	printf("\t-n \t\tdisable spcified leds\n");
	exit(0);
}

