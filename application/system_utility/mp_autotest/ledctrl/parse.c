#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#include <stdio.h>
#include <string.h>
#include <json.h>
#include "parse.h"


void parse_led(Led *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	if (json_object_object_get_ex(cmd_obj, "gpio", &tmp_obj)) {
		data->gpio = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "level_off", &tmp_obj)) {
		data->level_off = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "level_on", &tmp_obj)) {
		data->level_on = json_object_get_int(tmp_obj);
	}
}

void parse_led_conf(LedConf *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	int i;
	if (json_object_object_get_ex(cmd_obj, "led", &tmp_obj)) {
		for (i = 0; i < MAX_LEDCONF_LED_SIZE; i++) {
			parse_led(&data->led[i], json_object_array_get_idx(tmp_obj, i));
		}
	}
	if (json_object_object_get_ex(cmd_obj, "period", &tmp_obj)) {
		data->period = json_object_get_int(tmp_obj);
	}
}

void parse_led_control(LedCtrlConf *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	if (json_object_object_get_ex(cmd_obj, "led_conf", &tmp_obj)) {
		parse_led_conf(&data->led_conf, tmp_obj);
	}
}


#ifdef __cplusplus
}
#endif /* __cplusplus */
