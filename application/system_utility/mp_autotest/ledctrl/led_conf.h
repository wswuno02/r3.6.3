#ifndef MBTESTCO_H_
#define MBTESTCO_H_

#include "agtx_types.h"
struct json_object;

#define MAX_LEDCONF_LED_SIZE 2

typedef struct {
	AGTX_INT32 gpio;
	AGTX_INT32 level_off;
	AGTX_INT32 level_on;
} Led;

typedef struct {
	Led led[MAX_LEDCONF_LED_SIZE];
	AGTX_INT32 period;
} LedConf;

typedef struct {
	LedConf led_conf;
} LedCtrlConf;


#endif /* MBTESTCO_H_ */
