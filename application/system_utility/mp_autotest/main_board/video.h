#ifndef MB_TEST_VIDEO_H_
#define MB_TEST_VIDEO_H_

#define LOAD_MPP_CMD "sh /system/mpp/script/load_mpp.sh -i"
#define CTRL_SNAPSHOT_CMD "/system/bin/mpi_snapshot jpeg 0"
#define CTRL_STREAM_CMD "/system/bin/mpi_stream -d /mnt/sdcard/autotest/script/snap_shot.conf >/dev/null &"

int VIDEO_init(VideoConf *);
int VIDEO_autoTest();
void VIDEO_exit(void);
void VIDEO_showConf(const VideoConf *);

#endif