#ifndef MBTESTCO_H_
#define MBTESTCO_H_

#include "agtx_types.h"
struct json_object;

#define MAX_LEDCONF_LED_SIZE (2)
#define MAX_IRCUT_GPIO_NUM (2)
#define MAX_STRING_LEN (128)

typedef struct {
	AGTX_INT32 gpio;
	AGTX_INT32 level_off;
	AGTX_INT32 level_on;
} GpioLevel;

typedef GpioLevel Btn;
typedef GpioLevel Led;
typedef GpioLevel IrCut;
typedef GpioLevel IrLed;

typedef struct {
	AGTX_UINT8 test_res_path[MAX_STRING_LEN];
	AGTX_UINT8 station_id[MAX_STRING_LEN];
	AGTX_UINT8 station_name[MAX_STRING_LEN];
	AGTX_UINT8 test_item_name[MAX_STRING_LEN];
} TestResultFormat;

typedef struct {
	AGTX_INT32 chn_id;
	AGTX_INT32 max;
	AGTX_INT32 min;
	AGTX_INT32 day_th;
	AGTX_INT32 night_th;
} Adc;

typedef struct {
	AGTX_INT32 enabled;
	AGTX_INT32 ircut_enabled;
	AGTX_INT32 irled_enabled;
	AGTX_INT32 adc_enabled;
	TestResultFormat format;
	Adc   adc;
	IrCut ircut[MAX_IRCUT_GPIO_NUM];
	IrLed irled;
	AGTX_INT32 period;
} LightConf;

typedef struct {
	AGTX_INT32 enabled;
	AGTX_INT32 mic_enabled;
	AGTX_INT32 spk_enabled;
	TestResultFormat format;
	AGTX_UINT8 mic_args[MAX_STRING_LEN];
	AGTX_UINT8 speaker_args[MAX_STRING_LEN];
} AudioConf;

typedef struct {
	AGTX_INT32 enabled;
	TestResultFormat format;
	AGTX_UINT8 snapshot_args[MAX_STRING_LEN];
} VideoConf;

typedef struct {
	AGTX_INT32 enabled;
	TestResultFormat format;
} LvdsConf;

typedef struct {
	AGTX_INT32 enabled;
	TestResultFormat format;
	AGTX_UINT8 net_type[MAX_STRING_LEN];
} MacConf;

typedef struct {
	AGTX_INT32 enabled;
	TestResultFormat format;
	AGTX_UINT8 test_conf_args[MAX_STRING_LEN];
} SdConf;

typedef struct {
	Led led[MAX_LEDCONF_LED_SIZE];
	AGTX_INT32 period;
} LedConf;

typedef struct {
	Btn btn;
	AGTX_INT32 period;
} BtnConf;

typedef struct {
	BtnConf btn_conf;
	LedConf led_conf;
	SdConf  sd_conf;
	MacConf mac_conf;
	LvdsConf lvds_conf;
	VideoConf video_conf;
	AudioConf audio_conf;
	LightConf light_conf;
} MbTestConf;

#endif /* MBTESTCO_H_ */
