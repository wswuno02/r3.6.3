#ifndef MB_TEST_AUDIO_H_
#define MB_TEST__AUDIO_H_

int AUDIO_init(AudioConf *);
int AUDIO_autoTest();
void AUDIO_exit(void);
void AUDIO_showConf(const AudioConf *);

#endif
