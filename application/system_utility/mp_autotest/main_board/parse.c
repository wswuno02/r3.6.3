#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#include <stdio.h>
#include <string.h>
#include <json.h>
#include "parse.h"

#define max(X,Y) (((X) > (Y)) ? (X) : (Y))

const char * adcrev_map[] = {
	"POSITIVE",
	"NEGATIVE"
};

void parse_format(TestResultFormat *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	int i;
	if (json_object_object_get_ex(cmd_obj, "test_res_path", &tmp_obj)) {
		i = max(MAX_STRING_LEN + 1, json_object_get_string_len(tmp_obj));
		strncpy((char *)data->test_res_path, json_object_get_string(tmp_obj), i);
		data->test_res_path[i+1] = '\0';
	}
	if (json_object_object_get_ex(cmd_obj, "station_id", &tmp_obj)) {
		i = max(MAX_STRING_LEN + 1, json_object_get_string_len(tmp_obj));
		strncpy((char *)data->station_id, json_object_get_string(tmp_obj), i);
		data->station_id[i+1] = '\0';
	}
	if (json_object_object_get_ex(cmd_obj, "station_name", &tmp_obj)) {
		i = max(MAX_STRING_LEN + 1, json_object_get_string_len(tmp_obj));
		strncpy((char *)data->station_name, json_object_get_string(tmp_obj), i);
		data->station_name[i+1] = '\0';
	}
	if (json_object_object_get_ex(cmd_obj, "test_item_name", &tmp_obj)) {
		i = max(MAX_STRING_LEN + 1, json_object_get_string_len(tmp_obj));
		strncpy((char *)data->test_item_name, json_object_get_string(tmp_obj), i);
		data->test_item_name[i+1] = '\0';
	}
}

void parse_gpio_level(GpioLevel *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	if (json_object_object_get_ex(cmd_obj, "gpio", &tmp_obj)) {
		data->gpio = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "level_off", &tmp_obj)) {
		data->level_off = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "level_on", &tmp_obj)) {
		data->level_on = json_object_get_int(tmp_obj);
	}
}

void parse_adc(Adc *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	if (json_object_object_get_ex(cmd_obj, "day_th", &tmp_obj)) {
		data->day_th = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "chn_id", &tmp_obj)) {
		data->chn_id = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "max", &tmp_obj)) {
		data->max = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "min", &tmp_obj)) {
		data->min = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "night_th", &tmp_obj)) {
		data->night_th = json_object_get_int(tmp_obj);
	}
#if 0
	if (json_object_object_get_ex(cmd_obj, "reverse", &tmp_obj)) {
		int i;
		const char *str;
		str = json_object_get_string(tmp_obj);
		for (i = 0; i < sizeof(adcrev_map)/sizeof(char *); i++) {
			if (strcmp(adcrev_map[i], str) == 0) {
				data->reverse = (ADCREV)i;
			}
		}
	}
#endif
}

void parse_light_conf(LightConf *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	int i;
	if (json_object_object_get_ex(cmd_obj, "enabled", &tmp_obj)) {
		data->enabled = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "ircut_enabled", &tmp_obj)) {
		data->ircut_enabled = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "irled_enabled", &tmp_obj)) {
		data->irled_enabled = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "adc_enabled", &tmp_obj)) {
		data->adc_enabled = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "format", &tmp_obj)) {
		parse_format(&data->format, tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "adc", &tmp_obj)) {
		parse_adc(&data->adc, tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "ircut", &tmp_obj)) {
		for (i = 0; i < MAX_LEDCONF_LED_SIZE; i++) {
			parse_gpio_level(&data->ircut[i], json_object_array_get_idx(tmp_obj, i));
		}
	}
	if (json_object_object_get_ex(cmd_obj, "irled", &tmp_obj)) {
		parse_gpio_level(&data->irled, tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "period", &tmp_obj)) {
		data->period = json_object_get_int(tmp_obj);
	}
}

void parse_audio_conf(AudioConf *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	int i;
	if (json_object_object_get_ex(cmd_obj, "enabled", &tmp_obj)) {
		data->enabled = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "mic_enabled", &tmp_obj)) {
		data->mic_enabled = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "spk_enabled", &tmp_obj)) {
		data->spk_enabled = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "format", &tmp_obj)) {
		parse_format(&data->format, tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "mic_args", &tmp_obj)) {
		i = max(MAX_STRING_LEN + 1, json_object_get_string_len(tmp_obj));
		strncpy((char *)data->mic_args, json_object_get_string(tmp_obj), i);
		data->mic_args[i+1] = '\0';
	}
	if (json_object_object_get_ex(cmd_obj, "speaker_args", &tmp_obj)) {
		i = max(MAX_STRING_LEN + 1, json_object_get_string_len(tmp_obj));
		strncpy((char *)data->speaker_args, json_object_get_string(tmp_obj), i);
		data->speaker_args[i+1] = '\0';
	}
}

void parse_video_conf(VideoConf *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	int i;
	if (json_object_object_get_ex(cmd_obj, "enabled", &tmp_obj)) {
		data->enabled = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "format", &tmp_obj)) {
		parse_format(&data->format, tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "snapshot_args", &tmp_obj)) {
		i = max(MAX_STRING_LEN + 1, json_object_get_string_len(tmp_obj));
		strncpy((char *)data->snapshot_args, json_object_get_string(tmp_obj), i);
		data->snapshot_args[i+1] = '\0';
	}
}

void parse_lvds_conf(LvdsConf *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	if (json_object_object_get_ex(cmd_obj, "enabled", &tmp_obj)) {
		data->enabled = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "format", &tmp_obj)) {
		parse_format(&data->format, tmp_obj);
	}
}

void parse_mac_conf(MacConf *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	int i;
	if (json_object_object_get_ex(cmd_obj, "enabled", &tmp_obj)) {
		data->enabled = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "format", &tmp_obj)) {
		parse_format(&data->format, tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "net_type", &tmp_obj)) {
		i = max(MAX_STRING_LEN + 1, json_object_get_string_len(tmp_obj));
		strncpy((char *)data->net_type, json_object_get_string(tmp_obj), i);
		data->net_type[i+1] = '\0';
	}
}

void parse_sd_conf(SdConf *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	int i;
	if (json_object_object_get_ex(cmd_obj, "enabled", &tmp_obj)) {
		data->enabled = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "format", &tmp_obj)) {
		parse_format(&data->format, tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "test_conf_args", &tmp_obj)) {
		i = max(MAX_STRING_LEN + 1, json_object_get_string_len(tmp_obj));
		strncpy((char *)data->test_conf_args, json_object_get_string(tmp_obj), i);
		data->test_conf_args[i+1] = '\0';
	}
}

void parse_led_conf(LedConf *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	int i;
	if (json_object_object_get_ex(cmd_obj, "led", &tmp_obj)) {
		for (i = 0; i < MAX_LEDCONF_LED_SIZE; i++) {
			parse_gpio_level(&data->led[i], json_object_array_get_idx(tmp_obj, i));
		}
	}
	if (json_object_object_get_ex(cmd_obj, "period", &tmp_obj)) {
		data->period = json_object_get_int(tmp_obj);
	}
}

void parse_btn_conf(BtnConf *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	if (json_object_object_get_ex(cmd_obj, "period", &tmp_obj)) {
		data->period = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "btn", &tmp_obj)) {
		parse_gpio_level(&data->btn, tmp_obj);
	}
}

void parse_mb_test(MbTestConf *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	if (json_object_object_get_ex(cmd_obj, "btn_conf", &tmp_obj)) {
		parse_btn_conf(&data->btn_conf, tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "led_conf", &tmp_obj)) {
		parse_led_conf(&data->led_conf, tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "sd_conf", &tmp_obj)) {
		parse_sd_conf(&data->sd_conf, tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "mac_conf", &tmp_obj)) {
		parse_mac_conf(&data->mac_conf, tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "lvds_conf", &tmp_obj)) {
		parse_lvds_conf(&data->lvds_conf, tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "video_conf", &tmp_obj)) {
		parse_video_conf(&data->video_conf, tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "audio_conf", &tmp_obj)) {
		parse_audio_conf(&data->audio_conf, tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "light_conf", &tmp_obj)) {
		parse_light_conf(&data->light_conf, tmp_obj);
	}
}


#ifdef __cplusplus
}
#endif /* __cplusplus */
