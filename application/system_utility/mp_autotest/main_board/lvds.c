#include "event.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include "lvds.h"
#include "debug.h"

#define BUFFER_SIZE (32)
#define MAX_SENSOR_NUM 2
#define MAX_LANE_NUM 4

int LVDS_execCmd(char *action_args)
{
	int status;
	char buf[128];

	sprintf(buf, "%s", action_args);
	status = system(buf);

	if (status == -1) {
		MBAT_ERR("System error!\n");
	} else {
		if (WIFEXITED(status)) {
			if (WEXITSTATUS(status) == 0) {
				MBAT_NOTICE("(%s) be excuted successfully.\n", buf);
			} else {
				MBAT_ERR("Run cmd fail and exit code is %d (%s)!\n", WEXITSTATUS(status), buf);
			}
		} else {
			MBAT_ERR("exit status is %d (%s)!\n", WEXITSTATUS(status), buf);
		}
	}

	return status;
}

int LVDS_getFileContent(const char *path, char *buffer)
{
	int file;
	int read_size;

	file = open(path, O_RDONLY);

	if (file == -1) {
		MBAT_ERR("%s: file not found\n", path);
		return (-1);
	}

	while ((read_size = read(file, buffer, BUFFER_SIZE)) > 0) {
		write(1, &buffer, read_size);
	}

	close(file);

	return 0;
}

int LVDS_writeTestResult(const TestResultFormat *format, const char *res, const char *json, const int json_size)
{
	int status;
	char buf[128];

	sprintf(buf, "%s/%s_%s_%s_%s.txt",
	        (char *) format->test_res_path,
	        (char *) format->station_id,
	        (char *) format->station_name,
	        (char *) format->test_item_name,
	        res);

	FILE* file_ptr = fopen(buf, "w");
	if (file_ptr == NULL) {
		MBAT_ERR("Failed to write file.\n");
		status = -1;
	} else {
		MBAT_NOTICE("-------------> (%s).\n", json);
		fwrite(json, sizeof(char), json_size, file_ptr);
		MBAT_NOTICE("Success to write (%s).\n", buf);
		status = 0;
	}

	fclose(file_ptr);

	return status;
}

int LVDS_init(LvdsConf *conf)
{
	return 0;
}

int LVDS_autoTest(const LvdsConf *conf)
{
	MBAT_DBG("#--- Start %s()...\n", __func__);
	MBAT_NOTICE("======================\n");
	MBAT_NOTICE("=== LVDS test start ===\n");
	MBAT_NOTICE("======================\n");

	int err = 0;
	char json_buf[1024];
	int json_size = 0;
	int sensor_idx, lane_idx;

	err += LVDS_execCmd(LOAD_MPP_CMD);
	err += LVDS_execCmd(CALIB_LVDS_DELAY_CMD);

	if (access(LVDS_DELAY_RES, R_OK) != -1) {
		signed char data_delay[MAX_SENSOR_NUM][MAX_LANE_NUM];
		signed char clock_delay[MAX_SENSOR_NUM][MAX_LANE_NUM];
		FILE *fptr;
		/* file exists */
		/* TO-DO: parse data_delay from LVDS_DELAY_RES */
		MBAT_NOTICE("%s_%s_%s ... [OK]\n",
					conf->format.station_id,
					conf->format.station_name,
					conf->format.test_item_name);
		MBAT_INFO("LED stat --- <Blue Led, ON>, <Red Led, OFF>.\n");
		BLUE_LED(LED_ON);
		RED_LED(LED_OFF);

		/* Read data from calibration file */
		fptr = fopen(LVDS_DELAY_RES, "rb");
		if (fptr == NULL) {
			MBAT_INFO("--------> LVDS calibration file open failed\n");
			for (sensor_idx = 0; sensor_idx < MAX_SENSOR_NUM; sensor_idx++) {
				for (lane_idx = 0; lane_idx < MAX_LANE_NUM; lane_idx++) {
					data_delay[sensor_idx][lane_idx] = -1;
					clock_delay[sensor_idx][lane_idx] = -1;
				}
			}
		} else {
			for (sensor_idx = 0; sensor_idx < MAX_SENSOR_NUM; sensor_idx++) {
				for (lane_idx = 0; lane_idx < MAX_LANE_NUM; lane_idx++) {
					if (fread(&clock_delay[sensor_idx][lane_idx], sizeof(signed char), 1, fptr) != sizeof(signed char)) {
						clock_delay[sensor_idx][lane_idx] = -1;
					}
					if (fread(&data_delay[sensor_idx][lane_idx], sizeof(signed char), 1, fptr) != sizeof(signed char)) {
						data_delay[sensor_idx][lane_idx] = -1;
					}
				}
			}
		}
		fclose(fptr);

		/* Update report file */
		json_size = sprintf(json_buf, "{\n");
		for (sensor_idx = 0; sensor_idx < MAX_SENSOR_NUM; sensor_idx++) {
			for (lane_idx = 0; lane_idx < MAX_LANE_NUM; lane_idx++) {
				json_size += sprintf(json_buf + json_size, "\t\"sensor%d_lane%d_data_delay\": %d,\n", \
						sensor_idx, lane_idx, data_delay[sensor_idx][lane_idx]);
				json_size += sprintf(json_buf + json_size, "\t\"sensor%d_lane%d_clock_delay\": %d%c\n", \
						sensor_idx, lane_idx, clock_delay[sensor_idx][lane_idx], \
						sensor_idx == (MAX_SENSOR_NUM - 1) ? (lane_idx == (MAX_LANE_NUM - 1) ? ' ' : ',') : ',');
			}
		}
		json_size += sprintf(json_buf + json_size, "}");
		LVDS_writeTestResult(&conf->format, "OK", json_buf, json_size);
	} else {
		/* file doesn't exists */
		MBAT_NOTICE("%s_%s_%s ... [NG]\n",
					conf->format.station_id,
					conf->format.station_name,
					conf->format.test_item_name);
		MBAT_INFO("LED stat --- <Blue Led, OFF>, <Red Led, ON>.\n");
		BLUE_LED(LED_OFF);
		RED_LED(LED_ON);
		MBAT_INFO("--------> LVDS calibration failed\n");
		/* Update report file */
		json_size = sprintf(json_buf, "{\n");
		for (sensor_idx = 0; sensor_idx < MAX_SENSOR_NUM; sensor_idx++) {
			for (lane_idx = 0; lane_idx < MAX_LANE_NUM; lane_idx++) {
				json_size += sprintf(json_buf + json_size, "\t\"sensor%d_lane%d_data_delay\": %d,\n", sensor_idx, lane_idx, -1);
				json_size += sprintf(json_buf + json_size, "\t\"sensor%d_lane%d_clock_delay\": %d%c\n", sensor_idx, lane_idx, -1, \
						sensor_idx == (MAX_SENSOR_NUM - 1) ? (lane_idx == (MAX_LANE_NUM - 1) ? ' ' : ',') : ',');
			}
		}
		json_size += sprintf(json_buf + json_size, "}");
		LVDS_writeTestResult(&conf->format, "NG", json_buf, json_size);
	}


	MBAT_DBG("#--- End %s()...\n", __func__);

	sleep(5);

	return 0;
}

void LVDS_exit(void)
{
	return;
}

void LVDS_showConf(const LvdsConf *conf)
{
	MBAT_INFO("#--- [%s]\n", __func__);
	MBAT_INFO("enabled = %d\n", conf->enabled);
	MBAT_INFO("TEST_CASE_FORMT: %s/%s_%s_%s\n", conf->format.test_res_path, conf->format.station_id, conf->format.station_name, conf->format.test_item_name);
	return;
}
