#include "event.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "sdcard.h"
#include "debug.h"

typedef struct {
	AGTX_UINT8 test_result_args[MAX_STRING_LEN];
	AGTX_UINT8 test_snapshot_args[MAX_STRING_LEN];
	AGTX_UINT8 test_mic_args[MAX_STRING_LEN];
} FileList;

const FileList k_file_list = {
	.test_result_args = "/calib/autotest/mbtest/*.txt",
	.test_snapshot_args = "/calib/autotest/mbtest/snap_shot.jpg",
	.test_mic_args = "/calib/autotest/mbtest/test_mic.wav",
};

int SD_writeTestResult(const TestResultFormat *format, const char *res)
{
	int status;
	char buf[128];

	sprintf(buf, "%s/%s_%s_%s_%s.txt",
	        (char *) format->test_res_path,
	        (char *) format->station_id,
	        (char *) format->station_name,
	        (char *) format->test_item_name,
	        res);

	FILE* file_ptr = fopen(buf, "w");
	if (file_ptr == NULL) {
		MBAT_ERR("Failed to write file.\n");
		status = -1;
	} else {
		MBAT_NOTICE("Success to write (%s).\n", buf);
		status = 0;
	}

	fclose(file_ptr);

	return status;
}

int SD_rmFileList(const FileList *list)
{
	int err;

	if (access((char *) list->test_result_args, F_OK) != -1) {
		/* file exists, and remove it */
		if (remove((char *) list->test_result_args) != 0) {
			MBAT_ERR("Failed to remove file (%s).\n", (char *) list->test_result_args);
			err = -1;
		}
	} else {
		/* file doesn't exists, and do nothing*/
		err = 0;
	}

	if (access((char *) list->test_snapshot_args, F_OK) != -1) {
		/* file exists, and remove it */
		if (remove((char *) list->test_snapshot_args) != 0) {
			MBAT_ERR("Failed to remove file (%s).\n", (char *) list->test_snapshot_args);
			err = -1;
		}
	} else {
		/* file doesn't exists, and do nothing*/
		err = 0;
	}

	if (access((char *) list->test_mic_args, F_OK) != -1) {
		/* file exists, and remove it */
		if (remove((char *) list->test_mic_args) != 0) {
			MBAT_ERR("Failed to remove file (%s).\n", (char *) list->test_mic_args);
			err = -1;
		}
	} else {
		/* file doesn't exists, and do nothing*/
		err = 0;
	}

	return err;
}

int SD_init(SdConf *conf)
{
	return 0;
}

int SD_autoTest(const SdConf *conf)
{
	MBAT_DBG("#--- Start %s()...\n", __func__);

	int err = 0;

	MBAT_NOTICE("==========================\n");
	MBAT_NOTICE("=== SD CARD test start ===\n");
	MBAT_NOTICE("==========================\n");

	if (access((char *) conf->test_conf_args, F_OK) != -1) {
		/* file exists */
		err += 0;
	} else {
		/* file doesn't exists */
		MBAT_ERR("File (%s) is not exist.\n", (char *) conf->test_conf_args);
		err += -1;
	}

	//err += SD_rmFileList(&k_file_list);

	if (err != 0) {

		MBAT_NOTICE("%s_%s_%s ... [NG]\n",
		            conf->format.station_id,
		            conf->format.station_name,
		            conf->format.test_item_name);
		MBAT_INFO("LED stat --- <Blue Led, OFF>, <Red Led, ON>.\n");
		BLUE_LED(LED_OFF);
		RED_LED(LED_ON);
		SD_writeTestResult(&conf->format, "NG");
	} else {

		MBAT_NOTICE("%s_%s_%s ... [OK]\n",
		            conf->format.station_id,
		            conf->format.station_name,
		            conf->format.test_item_name);
		MBAT_INFO("LED stat --- <Blue Led, ON>, <Red Led, OFF>.\n");
		BLUE_LED(LED_ON);
		RED_LED(LED_OFF);
		SD_writeTestResult(&conf->format, "OK");
	}

	MBAT_DBG("#--- End %s()...\n", __func__);

	sleep(5);

	return 0;
}

void SD_exit(void)
{
	return;
}

void SD_showConf(const SdConf *conf)
{
	MBAT_INFO("#--- [%s]\n", __func__);
	MBAT_INFO("enabled = %d\n", conf->enabled);
	MBAT_INFO("TEST_CASE_FORMT: %s/%s_%s_%s\n", conf->format.test_res_path, conf->format.station_id, conf->format.station_name, conf->format.test_item_name);
	MBAT_INFO("test_conf_args = %s\n", conf->test_conf_args);
}
