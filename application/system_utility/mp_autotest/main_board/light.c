#include "event.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include "light.h"
#include "debug.h"

#define WAIT_SEC (2000000)

typedef enum {
	ADC_CHN_0 = 0,
	ADC_CHN_1,
	ADC_CHN_2,
	ADC_CHN_NUM,
} AdcChn;

typedef struct {
	int     id;
	char    *name;
} ALIAS_TABLE_S;

static ALIAS_TABLE_S k_adc_path[ADC_CHN_NUM] = {
	{ ADC_CHN_0,   "/sys/bus/iio/devices/iio:device0/in_voltage0_raw"},
	{ ADC_CHN_1,   "/sys/bus/iio/devices/iio:device0/in_voltage1_raw"},
	{ ADC_CHN_2,   "/sys/bus/iio/devices/iio:device0/in_voltage2_raw"},
};

int LIGHT_execCmd(char *action_args)
{
	int status;
	char buf[128];

	sprintf(buf, "%s", action_args);
	status = system(buf);

	if (status == -1) {
		MBAT_ERR("System error!\n");
	} else {
		if (WIFEXITED(status)) {
			if (WEXITSTATUS(status) == 0) {
				MBAT_NOTICE("(%s) be excuted successfully.\n", buf);
			} else {
				MBAT_ERR("Run cmd fail and exit code is %d (%s)!\n", WEXITSTATUS(status), buf);
			}
		} else {
			MBAT_ERR("exit status is %d (%s)!\n", WEXITSTATUS(status), buf);
		}
	}

	return status;
}

int LIGHT_writeTestResult(const TestResultFormat *format, const char *res, const char *json, const int json_size)
{
	int status;
	char buf[128];

	sprintf(buf, "%s/%s_%s_%s_%s.txt",
	        (char *) format->test_res_path,
	        (char *) format->station_id,
	        (char *) format->station_name,
	        (char *) format->test_item_name,
	        res);

	FILE* file_ptr = fopen(buf, "w");
	if (file_ptr == NULL) {
		MBAT_ERR("Failed to write file.\n");
		status = -1;
	} else {
		MBAT_NOTICE("-------------> (%s).\n", json);
		fwrite(json, sizeof(char), json_size, file_ptr);
		MBAT_NOTICE("Success to write (%s).\n", buf);
		status = 0;
	}

	fclose(file_ptr);

	return status;
}

int LIGHT_getAdcValue(const char *adc_file_path, int *adc_value)
{
	int fd = 0;
	char ch[4] = {0};

	fd = open(adc_file_path, O_RDONLY, 0x644);
	if (fd < 0) {
		MBAT_ERR("Cannot open ADC file path %s!\n", adc_file_path);
		return fd;
	}
	read(fd, ch, 4);
	close(fd);

	*adc_value = atoi(ch);

	return 0;
}

int LIGHT_init(LightConf *conf)
{
	return 0;
}

int LIGHT_autoTest(const LightConf *conf)
{
	MBAT_DBG("#--- Start %s()...\n", __func__);

	int err = 0;
	char ir_cut_active[128];
	char ir_cut_remove[128];
	char ir_led_off[128];
	char ir_led_on[128];
	long usec;
	int night_adc_value = -1;

	sprintf(ir_cut_active, "%s %d %d %s", CTRL_IR_CUT_CMD, conf->ircut[0].gpio, conf->ircut[1].gpio, "active");
	sprintf(ir_cut_remove, "%s %d %d %s", CTRL_IR_CUT_CMD, conf->ircut[0].gpio, conf->ircut[1].gpio, "remove");

	sprintf(ir_led_off, "%s %d %d", CTRL_IR_LED_CMD, conf->irled.gpio, conf->irled.level_off);
	sprintf(ir_led_on, "%s %d %d", CTRL_IR_LED_CMD, conf->irled.gpio, conf->irled.level_on);

	if (conf->ircut_enabled == 1) {
		// Test IR CUT
		MBAT_NOTICE("=========================\n");
		MBAT_NOTICE("=== IR CUT test start ===\n");
		MBAT_NOTICE("=========================\n");
		MBAT_INFO("LED stat --- <Blue Led, MED_FLICKER>, <Red Led, MED_FLICKER>.\n");
		RED_LED_FLICKER(MED_SPEED);
		BLUE_LED_FLICKER(MED_SPEED);
		sleep(3);

		err += LIGHT_execCmd(ir_cut_remove);
		sleep(3);
		err += LIGHT_execCmd(ir_cut_active);

		MBAT_NOTICE("WAIT for judgment... OK / NG\n");
		MBAT_INFO("LED stat --- <Blue Led, OFF>, <Red Led, OFF>.\n");
		BLUE_LED(LED_OFF);
		RED_LED(LED_OFF);

		usec = BUTTON_CLICK();
		MBAT_INFO("BTN stat --- Detect %ld us click.\n", usec);

		if (usec < WAIT_SEC) {

			MBAT_NOTICE("%s_%s_%s ... [OK]\n",
						conf->format.station_id,
						conf->format.station_name,
						conf->format.test_item_name);
			MBAT_INFO("LED stat --- <Blue Led, ON>, <Red Led, OFF>.\n");
			BLUE_LED(LED_ON);
			RED_LED(LED_OFF);
			err += 0;
		} else {

			MBAT_NOTICE("%s_%s_%s ... [NG]\n",
						conf->format.station_id,
						conf->format.station_name,
						conf->format.test_item_name);
			MBAT_INFO("LED stat --- <Blue Led, OFF>, <Red Led, ON>.\n");
			BLUE_LED(LED_OFF);
			RED_LED(LED_ON);
			err += -1;
		}

		sleep(3);
	}

	if (conf->irled_enabled == 1) {
		// Test IR LED
		MBAT_NOTICE("=========================\n");
		MBAT_NOTICE("=== IR LED test start ===\n");
		MBAT_NOTICE("=========================\n");
		MBAT_INFO("LED stat --- <Blue Led, MED_FLICKER>, <Red Led, MED_FLICKER>.\n");
		RED_LED_FLICKER(MED_SPEED);
		BLUE_LED_FLICKER(MED_SPEED);
		sleep(3);

		err += LIGHT_execCmd(ir_led_on);
		sleep(3);
		err += LIGHT_execCmd(ir_led_off);

		MBAT_NOTICE("WAIT for judgment... OK / NG\n");
		MBAT_INFO("LED stat --- <Blue Led, OFF>, <Red Led, OFF>.\n");
		BLUE_LED(LED_OFF);
		RED_LED(LED_OFF);

		usec = BUTTON_CLICK();
		MBAT_INFO("BTN stat --- Detect %ld us click.\n", usec);

		if (usec < WAIT_SEC) {

			MBAT_NOTICE("%s_%s_%s ... [OK]\n",
						conf->format.station_id,
						conf->format.station_name,
						conf->format.test_item_name);
			MBAT_INFO("LED stat --- <Blue Led, ON>, <Red Led, OFF>.\n");
			BLUE_LED(LED_ON);
			RED_LED(LED_OFF);
			err += 0;
		} else {

			MBAT_NOTICE("%s_%s_%s ... [NG]\n",
						conf->format.station_id,
						conf->format.station_name,
						conf->format.test_item_name);
			MBAT_INFO("LED stat --- <Blue Led, OFF>, <Red Led, ON>.\n");
			BLUE_LED(LED_OFF);
			RED_LED(LED_ON);
			err += -1;
		}

		sleep(3);
	}

	if (conf->adc_enabled == 1) {
		// Test light sensor ADC
		MBAT_NOTICE("====================================\n");
		MBAT_NOTICE("=== LIGHT SENSOR(ADC) test start ===\n");
		MBAT_NOTICE("====================================\n");
		MBAT_INFO("LED stat --- <Blue Led, MED_FLICKER>, <Red Led, MED_FLICKER>.\n");
		RED_LED_FLICKER(MED_SPEED);
		BLUE_LED_FLICKER(MED_SPEED);
		sleep(3);
		MBAT_NOTICE("COVER light sensor by hand [Keep cover]... (%s)\n", k_adc_path[conf->adc.chn_id].name);

		int is_detected_night = 0;
		int is_detected_day = 0;
		int adc_value;

		while (is_detected_night != 1) {
			LIGHT_getAdcValue(k_adc_path[conf->adc.chn_id].name, &adc_value);
			MBAT_INFO("night_th = %4d, day_th = %4d, adc_value = %4d.\n", conf->adc.night_th, conf->adc.day_th, adc_value);
			if (adc_value <= conf->adc.night_th) {
				night_adc_value = adc_value;
				MBAT_NOTICE("DETECT night...\n");
				//err += LIGHT_execCmd(ir_cut_remove);
				err += LIGHT_execCmd(ir_led_on);
				is_detected_night = 1;
			} else {

			}

			usleep(conf->period * 1000);
		}

		MBAT_NOTICE("OPEN your hand... (%s)\n", k_adc_path[conf->adc.chn_id].name);

		while (is_detected_day != 1) {
			LIGHT_getAdcValue(k_adc_path[conf->adc.chn_id].name, &adc_value);
			MBAT_INFO("night_th = %4d, day_th = %4d, adc_value = %4d.\n", conf->adc.night_th, conf->adc.day_th, adc_value);
			if (adc_value > conf->adc.day_th) {

				MBAT_NOTICE("DETECT day...\n");
				//err += LIGHT_execCmd(ir_cut_active);
				err += LIGHT_execCmd(ir_led_off);
				is_detected_day = 1;
			} else {

			}

			usleep(conf->period * 1000);
		}
	}

	MBAT_NOTICE("WAIT for judgment... OK / NG\n");
	MBAT_INFO("LED stat --- <Blue Led, OFF>, <Red Led, OFF>.\n");
	BLUE_LED(LED_OFF);
	RED_LED(LED_OFF);

	usec = BUTTON_CLICK();
	MBAT_INFO("BTN stat --- Detect %ld us click.\n", usec);

	if (usec < WAIT_SEC) {

		MBAT_NOTICE("%s_%s_%s ... [OK]\n",
		            conf->format.station_id,
		            conf->format.station_name,
		            conf->format.test_item_name);
		MBAT_INFO("LED stat --- <Blue Led, ON>, <Red Led, OFF>.\n");
		BLUE_LED(LED_ON);
		RED_LED(LED_OFF);
		err += 0;
	} else {

		MBAT_NOTICE("%s_%s_%s ... [NG]\n",
		            conf->format.station_id,
		            conf->format.station_name,
		            conf->format.test_item_name);
		MBAT_INFO("LED stat --- <Blue Led, OFF>, <Red Led, ON>.\n");
		BLUE_LED(LED_OFF);
		RED_LED(LED_ON);
		err += -1;
	}

	sleep(3);

	char json_buf[128];
	int json_size = 0;

	if (err != 0) {
		night_adc_value = -1;
		MBAT_INFO("--------> night_adc_value = %d\n", night_adc_value);
		json_size = sprintf(json_buf, "{\n    \"adc_value\": %d\n}",night_adc_value);
		LIGHT_writeTestResult(&conf->format, "NG", json_buf, json_size);
	} else {
		MBAT_INFO("--------> night_adc_value = %d\n", night_adc_value);
		json_size = sprintf(json_buf, "{\n    \"adc_value\": %d\n}",night_adc_value);
		LIGHT_writeTestResult(&conf->format, "OK", json_buf, json_size);
	}

	MBAT_DBG("#--- End %s()...\n", __func__);

	return 0;
}

void LIGHT_exit(void)
{
	return;
}

void LIGHT_showConf(const LightConf *conf)
{
	MBAT_INFO("#--- [%s]\n", __func__);
	MBAT_INFO("enabled = %d\n", conf->enabled);
	MBAT_INFO("ircut_enabled = %d\n", conf->ircut_enabled);
	MBAT_INFO("irled_enabled = %d\n", conf->irled_enabled);
	MBAT_INFO("adc_enabled = %d\n", conf->adc_enabled);
	MBAT_INFO("TEST_CASE_FORMT: %s/%s_%s_%s\n", conf->format.test_res_path, conf->format.station_id, conf->format.station_name, conf->format.test_item_name);
	MBAT_INFO("adc.chn_id = %d\n", conf->adc.chn_id);
	MBAT_INFO("adc.min = %d\n", conf->adc.min);
	MBAT_INFO("adc.max = %d\n", conf->adc.max);
	MBAT_INFO("adc.day_th = %d\n", conf->adc.day_th);
	MBAT_INFO("adc.night_th = %d\n", conf->adc.night_th);
	MBAT_INFO("ircut0.gpio = %d\n", conf->ircut[0].gpio);
	MBAT_INFO("ircut0.level_off = %d\n", conf->ircut[0].level_off);
	MBAT_INFO("ircut0.level_on = %d\n", conf->ircut[0].level_on);
	MBAT_INFO("ircut1.gpio = %d\n", conf->ircut[1].gpio);
	MBAT_INFO("ircut1.level_off = %d\n", conf->ircut[1].level_off);
	MBAT_INFO("ircut1.level_on = %d\n", conf->ircut[1].level_on);
	MBAT_INFO("irled.gpio = %d\n", conf->irled.gpio);
	MBAT_INFO("irled.level_off = %d\n", conf->irled.level_off);
	MBAT_INFO("irled.level_on = %d\n", conf->irled.level_on);
	MBAT_INFO("period = %d\n", conf->period);
}