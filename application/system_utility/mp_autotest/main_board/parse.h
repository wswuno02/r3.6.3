/******************************************************************************
*
* copyright (c) Augentix Inc. - All Rights Reserved
*
* Unauthorized copying of this file, via any medium is strictly prohibited.
*
* Proprietary and confidential.
*
******************************************************************************/

#ifndef CM_tCo_H_
#define CM_tCo_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#include "agtx_types.h"
#include "agtx_common.h"
#include "mb_conf.h"


struct json_object;


void parse_mb_test(MbTestConf *data, struct json_object *cmd_obj);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* !CM_tCo_H_ */
