#ifndef MB_TEST_SDCARD_H_
#define MB_TEST_SDCARD_H_

int SD_init(SdConf *);
int SD_autoTest();
void SD_exit(void);
void SD_showConf(const SdConf *);

#endif
