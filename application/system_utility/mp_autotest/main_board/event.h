#ifndef MB_TEST_EVENT_H_
#define MB_TEST_EVENT_H_

#include "led.h"
#include "button.h"
#include "light.h"


/* Empty now */

#define LED_ON  (1)
#define LED_OFF (0)
#define MED_SPEED  (1000)
#define HIGH_SPEED (250)
#define LOW_SPEED  (4000)
#define SHORT_TIME (1000)
#define LONG_TIME  (2000)


#define BLUE_LED_FLICKER(x) \
	do { \
		LED_setFlicker(LED_TYPE_BLUE, (x)); \
	} while(0);


#define RED_LED_FLICKER(x) \
	do { \
		LED_setFlicker(LED_TYPE_RED, (x)); \
	} while(0);


#define RED_LED(x) \
	do { \
		LED_setStatus(LED_TYPE_RED, (x)); \
	} while (0);

#define BLUE_LED(x) \
	do { \
		LED_setStatus(LED_TYPE_BLUE, (x)); \
	} while (0);

#define BUTTON_PRESS(x) \
	do { \
		BTN_detectEvent(BTN_STATUS_PUSH, (x)); \
	} while (0);


#define BUTTON_FREE(x) \
	do { \
		BTN_detectEvent(BTN_STATUS_REL, (x)); \
	} while (0);

#define BUTTON_PUSH() \
	do { \
		BTN_detectEvent(BTN_ACTION_PUSH, 0); \
	} while (0);

#define BUTTON_REL() \
	do { \
		BTN_detectEvent(BTN_ACTION_REL, 0); \
	} while (0);

#define BUTTON_CLICK() \
	({ \
		float __ret; \
		do { \
		__ret = BTN_detectEvent(BTN_ACTION_PUSH_REL, 0); \
		} while (0); \
		__ret; \
	});


#endif
