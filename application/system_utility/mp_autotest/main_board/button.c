#include "button.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>
#include "debug.h"

#include "gpio.h"

#define ASSERT(x)

typedef struct {
	char *name;
	int fd;
	int gpio;
	int level_on;
	int level_off;
	int period;
	int level;
} BtnCtx;

char *btn_name = "BUTTON";


BtnCtx g_btn_ctx;

static int BTN_readValue()
{
	BtnCtx *ctx = &g_btn_ctx;
	char file[256];
	char c[4];

	sprintf(file, "/sys/class/gpio/gpio%d/value", ctx->gpio);

	ctx->fd = open(file, O_RDONLY);

	if (ctx->fd < 0) {
		MBAT_ERR("Failed to open %s.\n", file);
		goto error;
	}

	if (read(ctx->fd, c, 4) < 0) {
		MBAT_ERR("Failed to read from GPIO device\n");
		close(ctx->fd);
		goto error;
	}

	int val = (c[0] == '0') ? 0 : 1;

	close(ctx->fd);

	return val;
error:
	return -1;

}

static int BTN_detectStatusEvent(BtnEvent evt, int cnt)
{
	BtnCtx *ctx = &g_btn_ctx;
	int level = (evt == BTN_STATUS_REL) ? ctx->level_off : ctx->level_on;
	const int max_cnt = cnt;
	int val;

	while (cnt > 0) {
		val = BTN_readValue();

		if (val < 0) {
			return val;
		}

		MBAT_DBG("button = %d.\n", val);

		cnt -= ctx->period;

		if (val != level) {
			cnt = max_cnt;
		}

		usleep(ctx->period * 1000);
	}

	MBAT_DBG("Detect press over %d ms.\n", max_cnt);

	return 0;
}

static inline int BTN_detectEdge(int level_bf, int level_aft)
{
	BtnCtx *ctx = &g_btn_ctx;
	int val;
	int level[2] = { level_bf, level_aft };

	for (int i = 0; i < 2; ++i) {
		while (1) {
			val = BTN_readValue();

			if (val < 0) {
				return val;
			}

			MBAT_DBG("button = %d.\n", val);


			if (val == level[i])
				break;

			usleep(ctx->period * 1000);
		}
	}

	MBAT_DBG("Detect edge (%d->%d) button.\n", level_bf, level_aft);

	return 0;
}

static long BTN_detectActionEvent(BtnEvent evt)
{
	BtnCtx *ctx = &g_btn_ctx;
	int level_bf, level_aft, level_post;
	struct timeval start, end;

	switch (evt) {
		case BTN_ACTION_PUSH:
			level_bf = ctx->level_off;
			level_aft = ctx->level_on;
			break;
		case BTN_ACTION_REL:
			level_aft = ctx->level_off;
			level_bf = ctx->level_on;
			break;
		case BTN_ACTION_PUSH_REL:
			level_bf = ctx->level_off;
			level_aft = ctx->level_on;
			level_post = ctx->level_off;
			break;
		default:
			ASSERT("Unkwon action.");
	}

	ASSERT(ctx->level_bf != ctx->level_aft);
	if (BTN_detectEdge(level_bf, level_aft)) {
		return -1;
	}

	if (evt != BTN_ACTION_PUSH_REL)
		return 0;

	gettimeofday(&start, NULL);

	if (BTN_detectEdge(level_aft, level_post)) {
		return -1;
	}

	gettimeofday(&end, NULL);
	long diff = 1000000 * (end.tv_sec - start.tv_sec) + end.tv_usec - start.tv_usec;

	return diff;
}


long BTN_detectEvent(BtnEvent evt, int cnt)
{
	if (evt >= BTN_ACTION_PUSH) {
		return BTN_detectActionEvent(evt);
	}

	return BTN_detectStatusEvent(evt, cnt);
}

int BTN_init(BtnConf *conf)
{
	int err = -1;

	BtnCtx *ctx = &g_btn_ctx;

	ctx->gpio = conf->btn.gpio;
	ctx->level_on = conf->btn.level_on;
	ctx->level_off = conf->btn.level_off;
	ctx->period = conf->period;
	ctx->name = btn_name;

	err = GPIO_exportGpio(ctx->gpio);
	if (err < 0) {
		MBAT_ERR("Failed to export GPIO %d.\n", ctx->gpio);
		return -1;
	}

	if (err == 0) {
		if (GPIO_setGpioDir(ctx->gpio, "in")) {
			MBAT_ERR("Failed to set GPIO %d \"IN\"\n", ctx->gpio);
			GPIO_unexportGpio(ctx->gpio);
			return -1;
		}
	}

	MBAT_NOTICE("Initialized button with GPIO %d.\n", conf->btn.gpio);
	return 0;
}

void BTN_exit()
{
	BtnCtx *ctx = &g_btn_ctx;
	MBAT_NOTICE("Stop button with GPIO %d.\n", ctx->gpio);

	if (GPIO_unexportGpio(ctx->gpio)) {
		MBAT_ERR("Failed to unexport GPIO %d.\n", ctx->gpio);
	}
}

void BTN_showConf(const BtnConf *conf)
{
	MBAT_INFO("#--- [%s]\n", __func__);
	MBAT_INFO("btn.gpio = %d\n", conf->btn.gpio);
	MBAT_INFO("btn.level_off = %d\n", conf->btn.level_off);
	MBAT_INFO("btn.level_on = %d\n", conf->btn.level_on);
	MBAT_INFO("period = %d\n", conf->period);
}
