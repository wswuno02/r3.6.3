#include "gpio.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>

#include "debug.h"

int GPIO_exportGpio(int gpio)
{
	char path[128];
	char buf[4];
	int fd, ret = 0;
	sprintf(path, "/sys/class/gpio/gpio%d", gpio);
	sprintf(buf, "%d", gpio);

	if (access(path, F_OK) != -1) {
		/* file exists */
		ret = 1;
		return ret;
	} else {
		/* file doesn't exists */
		//printf("Export GPIO %s\n", buf);
		fd = open("/sys/class/gpio/export", O_WRONLY);

		if (fd < 0) {
			MBAT_ERR("Failed to open GPIO device node!\n");
			return fd;
		}

		if (write(fd, buf, strlen(buf)) < 0) {
			MBAT_ERR("Failed to export GPIO %d device node!\n", gpio);
			ret = -1;
		}

		close(fd);
	}

	return ret;
}

int GPIO_unexportGpio(int gpio)
{
	char path[128];
	char buf[4];
	int fd, ret = 0;
	sprintf(path, "/sys/class/gpio/gpio%d", gpio);
	sprintf(buf, "%d", gpio);

	if (access(path, F_OK) != -1) {
		//printf("Export GPIO %s\n", buf);
		fd = open("/sys/class/gpio/unexport", O_WRONLY);

		if (fd < 0) {
			MBAT_ERR("Failed to open GPIO device node!\n");
			return fd;
		}

		if (write(fd, buf, strlen(buf)) < 0) {
			MBAT_ERR("Failed to unexport GPIO %d device node!\n", gpio);
			ret = -1;
		}

		close(fd);

	} else {
		/* file not exists */
		return ret;
	}

	return ret;
}

int GPIO_setGpioDir(int gpio, const char *direction)
{
	char file[256];
	int fd;
	int ret = 0;

	sprintf(file, "/sys/class/gpio/gpio%d/direction", gpio);

	/*
	* printf("%s: Set GPIO %d direction to \"%s\"\n",
	*   filepath, gpio->id, direction);
	*/
	fd = open(file, O_WRONLY);

	if (fd < 0) {
		MBAT_ERR("Failed to open GPIO %d node!\n", gpio);
		return fd;
	}

	if (write(fd, direction, strlen(direction)) < 0) {
		MBAT_ERR("Failed to set direction to GPIO %d node!\n", gpio);
		ret = -1;
	}

	close(fd);

	return ret;
}

int GPIO_setGpioVal(int gpio, const int val)
{
	char file[256];
	int fd;
	int ret = 0;
	char c;
	c = (val == 0) ? '0' : '1';

	sprintf(file, "/sys/class/gpio/gpio%d/value", gpio);

	fd = open(file, O_WRONLY);

	if (fd < 0) {
		MBAT_ERR("Failed to open GPIO %d node!\n", gpio);
		return fd;
	}

	if (write(fd, &c, 1) < 0) {
		MBAT_ERR("Failed to set %d to GPIO %d node!\n", val, gpio);
		ret = -1;
	}

	close(fd);

	return ret;
}


