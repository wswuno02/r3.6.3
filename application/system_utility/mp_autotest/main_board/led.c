#include "led.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include "debug.h"

#include "gpio.h"

#define ASSERT(x)


typedef enum {
	LED_STATUS_OFF = 0,
	LED_STATUS_ON,
	LED_STATUS_FLICKER,
	LED_STATUS_NUM
} LedStatus;

typedef enum {
	LED_RUN_STATUS_NONE = 0,
	LED_RUN_STATUS_RUN,
	LED_RUN_STATUS_STOP
} LedRunStatus;

typedef struct {
	pthread_t tid;
	LedStatus status;
	LedRunStatus run;
	char *name;
	int gpio;
	int level_on;
	int level_off;
	int period;
	int cnt;
	int max_cnt;
	int level;
} LedThreadCtx;


char *led_name[MAX_LEDCONF_LED_SIZE] = { "LED_RED", "LED_BULE" };
LedThreadCtx g_led_ctx[MAX_LEDCONF_LED_SIZE];

static void LED_setGpioVal(LedType led, LedStatus status)
{
	char file[256];
	LedThreadCtx *ctx = &g_led_ctx[(int)led];
	int fd;
	char c;

	sprintf(file, "/sys/class/gpio/gpio%d/value", ctx->gpio);
	fd = open(file, O_WRONLY);

	if (fd < 0) {
		MBAT_ERR("Failed to open %s.\n", file);
		return;
	}

	switch (ctx->status) {
		case LED_STATUS_OFF:
			c = (ctx->level_off == 0) ? '0' : '1';
			if (write(fd, &c, 1) < 0) {
				MBAT_ERR("Failed to set %d to GPIO %d.\n", ctx->level_off, ctx->gpio);
				goto end;
			}
			ctx->level = ctx->level_off;
			break;
		case LED_STATUS_ON:
			c = (ctx->level_on == 0) ? '0' : '1';
			if (write(fd, &c, 1) < 0) {
				MBAT_ERR("Failed to set %d to GPIO %d.\n", ctx->level_off, ctx->gpio);
				goto end;
			}
			ctx->level = ctx->level_on;
			break;
		default:
			ASSERT("Invalid action.\n");
			break;
	}

end:
	close(fd);
}


static void *LED_thread(void *data)
{
	char file[256];
	LedThreadCtx *ctx = &g_led_ctx[(int)data];
	int fd;
	char c;

	sprintf(file, "/sys/class/gpio/gpio%d/value", ctx->gpio);
	fd = open(file, O_WRONLY);

	if (fd < 0) {
		MBAT_ERR("Failed to open %s.\n", file);
		return NULL;
	}

	while (ctx->run == LED_RUN_STATUS_RUN) {
		switch (ctx->status) {
			case LED_STATUS_FLICKER:
				if (ctx->cnt > 0) {
					ctx->cnt -= ctx->period;
				} else {
					ctx->level = ~ctx->level & 0x1;
					ctx->cnt += ctx->max_cnt;
					c = (ctx->level == 0) ? '0' : '1';
					if (write(fd, &c, 1) < 0) {
						MBAT_ERR("Failed to set %d to GPIO %d.\n", ctx->level_off, ctx->gpio);
						goto end;
					}
				}

				break;
			default:
				/* Ignore */
				break;
		}
		usleep(ctx->period * 1000);
	}

	/* Turn off LED before status change to none */
	if (ctx->status == LED_STATUS_FLICKER) {
		c = (ctx->level_off == 0) ? '0' : '1';
		if (write(fd, &c, 1) < 0) {
			MBAT_ERR("Failed to set %d to GPIO %d.\n", ctx->level_off, ctx->gpio);
		}
	}

end:
	ctx->run = LED_RUN_STATUS_NONE;
	close(fd);

	return NULL;
}

int LED_init(LedConf *conf)
{
	int ret = 0;
	int err = -1;

	for (int i = 0; i < MAX_LEDCONF_LED_SIZE; ++i) {
		LedThreadCtx *ctx = &g_led_ctx[i];

		ctx->gpio = conf->led[i].gpio;
		ctx->level_on = conf->led[i].level_on;
		ctx->level_off = conf->led[i].level_off;
		ctx->period = conf->period;
		ctx->cnt = 0;
		ctx->status = LED_STATUS_OFF;
		ctx->name = led_name[i];
		ctx->max_cnt = 0;
		ctx->run = LED_RUN_STATUS_NONE;

		err = GPIO_exportGpio(ctx->gpio);
		if (err < 0) {
			MBAT_ERR("Failed to export GPIO %d.\n", ctx->gpio);
			return -1;
		}

		if (err == 0) {
			if (GPIO_setGpioDir(ctx->gpio, "out") < 0) {
				MBAT_ERR("Failed to set GPIO %d \"OUT\"\n", ctx->gpio);
				GPIO_unexportGpio(ctx->gpio);
				return -1;
			}
		}

		err = GPIO_setGpioVal(ctx->gpio, ctx->level_off);
		if (err < 0) {
			MBAT_ERR("Failed to set value %d to GPIO %d.\n", ctx->level_off, ctx->gpio);
			return -1;
		}

		ret = pthread_create(&ctx->tid, NULL, LED_thread, (void *)i);

		if (ret != 0) {
			MBAT_ERR("Failed to create LED thread(%s)\n", ctx->name);
		}

		ctx->run = LED_RUN_STATUS_RUN;

		MBAT_NOTICE("Initialized LED %d with GPIO %d.\n", i, conf->led[i].gpio);
	}

	return 0;
}

int LED_setFlicker(LedType type, int count)
{
	const int idx = type;
	LedThreadCtx *ctx = &g_led_ctx[idx];

	if (count < 0) {
		ASSERT("Invalid input value.");
	}

	ctx->max_cnt = (count + 1) >> 2;
	ctx->cnt = (ctx->cnt > ctx->max_cnt) ? ctx->max_cnt : ctx->cnt;
	ctx->cnt = (ctx->cnt <= 0) ? 0 : ctx->cnt;
	ctx->status = LED_STATUS_FLICKER;

	return 0;
}


int LED_setStatus(LedType type, int val)
{
	const int idx = type;
	LedThreadCtx *ctx = &g_led_ctx[idx];

	if ((val >= LED_STATUS_FLICKER) || (val < 0)) {
		ASSERT("Invalue input value.");
	}

	ctx->status = val;
	LED_setGpioVal(type, (LedStatus)val);

	return 0;
}

void LED_exit(void)
{
	for (int i = 0; i < MAX_LEDCONF_LED_SIZE; ++i) {
		LedThreadCtx *ctx = &g_led_ctx[i];
		MBAT_NOTICE("Stop LED %d with GPIO %d.\n", i, ctx->gpio);
		ctx->run = LED_RUN_STATUS_STOP;
	}

	for (int i = 0; i < MAX_LEDCONF_LED_SIZE; ++i) {
		LedThreadCtx *ctx = &g_led_ctx[i];
		pthread_join(ctx->tid, NULL);

		if (GPIO_unexportGpio(ctx->gpio)) {
			MBAT_ERR("Failed to unexport GPIO %d.\n", ctx->gpio);
		}
	}

	return;
}

void LED_showConf(const LedConf *conf)
{
	MBAT_INFO("#--- [%s]\n", __func__);
	MBAT_INFO("red_led.gpio = %d\n", conf->led[0].gpio);
	MBAT_INFO("red_led.level_off = %d\n", conf->led[0].level_off);
	MBAT_INFO("red_led.level_on = %d\n", conf->led[0].level_on);
	MBAT_INFO("blue_led.gpio = %d\n", conf->led[1].gpio);
	MBAT_INFO("blue_led.level_off = %d\n", conf->led[1].level_off);
	MBAT_INFO("blue_led.level_on = %d\n", conf->led[1].level_on);
	MBAT_INFO("period = %d\n", conf->period);
}
