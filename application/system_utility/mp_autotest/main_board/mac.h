#ifndef MB_TEST_MAC_H_
#define MB_TEST_MAC_H_

int MAC_init(MacConf *);
int MAC_autoTest(const MacConf *);
void MAC_exit(void);
void MAC_showConf(const MacConf *);

#endif
