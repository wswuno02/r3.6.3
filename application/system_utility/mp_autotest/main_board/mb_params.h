#ifndef MB_TEST_PARAMS_H_
#define MB_TEST_PARAMS_H_

#include "mb_conf.h"

const MbTestConf k_conf = {
	.btn_conf = {
		.btn = {
			.gpio = 58,
			.level_off = 1,
			.level_on = 0,
		},
		.period = 125,
	},
	.led_conf = {
		.led[0] = {
			.gpio = 57,
			.level_off = 0,
			.level_on = 1,
		},
		.led[1] = {
			.gpio = 60,
			.level_off = 0,
			.level_on = 1,
		},
		.period = 125,
	},
	.sd_conf = {
		.enabled = 1,
		.format = {
			.station_id = "ID-0",
			.station_name = "STATION-4",
			.test_item_name = "SD-CARD",
		},
		.test_conf_args = "/mnt/sdcard/autotest/script/A04mb_test",
	},
	.video_conf = {
		.enabled = 1,
		.format = {
			.station_id = "ID-0",
			.station_name = "STATION-4",
			.test_item_name = "IMAGE-SENSOR",
		},
		.snapshot_args = "/mnt/sdcard/autotest/script/agt902_snapshot_config",
	},
	.audio_conf = {
		.enabled = 1,
		.format = {
			.station_id = "ID-0",
			.station_name = "STATION-4",
			.test_item_name = "AUDIO",
		},
		.mic_args = "/calib/autotest/mbtest/test_mic.wav",
		.speaker_args = "/mnt/sdcard/autotest/script/test_speaker.wav",
	},
	.light_conf = {
		.enabled = 1,
		.format = {
			.station_id = "ID-0",
			.station_name = "STATION-4",
			.test_item_name = "LIGHT-SENSOR",
		},
		.adc = {
			.chn_id = 1,
			.max = 4095,
			.min = 0,
			.day_th = 900,
			.night_th = 500,
		},
		.ircut[0] = {
			.gpio = 53,
			.level_off = 1,
			.level_on = 0,
		},
		.ircut[1] = {
			.gpio = 54,
			.level_off = 0,
			.level_on = 1,
		},
		.irled = {
			.gpio = 61,
			.level_off = 0,
			.level_on = 1,
		},
		.period = 125,
	},
};

#endif