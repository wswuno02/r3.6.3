#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <json.h>

#include "debug.h"
#include "led.h"
#include "audio.h"
#include "video.h"
#include "event.h"
#include "parse.h"
#include "sdcard.h"
#include "mac.h"
#include "lvds.h"
#include "mb_params.h"

#define ENABLE_JSON (1)

MbTestConf g_conf;
char *log_name = "mb_test";
char *json_str;

char *getFileContent(const char *);
void printConf(MbTestConf *);
void help();
void initMbTest(MbTestConf *);
void exitMbTest();

int execCmd(char *action_args)
{
	int status;
	char buf[128];

	sprintf(buf, "%s", action_args);
	status = system(buf);

	if (status == -1) {
		MBAT_ERR("System error!\n");
	} else {
		if (WIFEXITED(status)) {
			if (WEXITSTATUS(status) == 0) {
				MBAT_NOTICE("(%s) be excuted successfully.\n", buf);
			} else {
				MBAT_ERR("Run cmd fail and exit code is %d (%s)!\n", WEXITSTATUS(status), buf);
			}
		} else {
			MBAT_ERR("exit status is %d (%s)!\n", WEXITSTATUS(status), buf);
		}
	}

	return status;
}

static void handleSigInt(int signo)
{
	//	UINT32 idx = 0;

	if (signo == SIGINT) {
		printf("Caught SIGINT!\n");
	} else if (signo == SIGTERM) {
		printf("Caught SIGTERM!\n");
	} else {
		perror("Unexpected signal!\n");
		exit(1);
	}

	free(json_str);
	exitMbTest();

	exit(0);
}

int main(int argc, char **argv)
{
	if (signal(SIGINT, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGINT!\n");
		exit(1);
	}

#if ENABLE_JSON
	json_object *jobj = NULL;

	if (argc < 2) {
		help();
		return 0;
	}

	json_str = getFileContent(argv[1]);
	if (!json_str) {
		MBAT_ERR("Failed to read %s.\n", argv[1]);
		return 0;
	}

	jobj = json_tokener_parse(json_str);
	if (jobj == NULL) {
		MBAT_ERR("Invalid JSON string.\n");
		goto free_json_str;
	}

	parse_mb_test(&g_conf, jobj);
#else
	memcpy(&g_conf, &k_conf, sizeof(MbTestConf));
#endif

	printConf(&g_conf);
	initMbTest(&g_conf);

	/* TODO: add more test here */
	if (g_conf.sd_conf.enabled == 1) {
		SD_autoTest(&g_conf.sd_conf);
	}

	if (g_conf.mac_conf.enabled == 1) {
		MAC_autoTest(&g_conf.mac_conf);
	}

	if (g_conf.lvds_conf.enabled == 1) {
		LVDS_autoTest(&g_conf.lvds_conf);
	}

	if (g_conf.video_conf.enabled == 1) {
		VIDEO_autoTest(&g_conf.video_conf);
	}

	if (g_conf.audio_conf.enabled == 1) {
		AUDIO_autoTest(&g_conf.audio_conf);
	}

	if (g_conf.light_conf.enabled == 1) {
		LIGHT_autoTest(&g_conf.light_conf);
	}

	exitMbTest();
#if ENABLE_JSON
free_json_str:
	free(json_str);
#endif

	return 0;
}

char *getFileContent(const char *path)
{
	char *ptr = NULL;
	FILE *fp;
	long fsize = 0;

	if ((fp = fopen(path, "r")) != NULL) {
		fseek(fp, 0L, SEEK_END);
		fsize = ftell(fp);
		fseek(fp, 0L, SEEK_SET);

		ptr = (char *)malloc(fsize + 1);

		if (!ptr) {
			goto end;
		}

		memset(ptr, 0, fsize);

		fread(ptr, fsize, 1, fp);

		ptr[fsize] = 0;

		if (ferror(fp)) {
			free(ptr);
		}
	}

end:
	fclose(fp);
	return ptr;
}

void initMbTest(MbTestConf *conf)
{
	if (LED_init(&conf->led_conf)) {
		goto end;
	}
	if (BTN_init(&conf->btn_conf)) {
		goto led_exit;
	}
	if (SD_init(&conf->sd_conf)) {
		goto btn_exit;
	}
	if (MAC_init(&conf->mac_conf)) {
		goto sd_exit;
	}
	if (LVDS_init(&conf->lvds_conf)) {
		goto mac_exit;
	}
	if (VIDEO_init(&conf->video_conf)) {
		goto lvds_exit;
	}
	if (AUDIO_init(&conf->audio_conf)) {
		goto video_exit;
	}
	if (LIGHT_init(&conf->light_conf)) {
		goto audio_exit;
	}

	return;
audio_exit:
	AUDIO_exit();
video_exit:
	VIDEO_exit();
lvds_exit:
	LVDS_exit();
mac_exit:
	MAC_exit();
sd_exit:
	SD_exit();
btn_exit:
	BTN_exit();
led_exit:
	LED_exit();
end:
	free(json_str);
	exit(1);
}

void exitMbTest()
{
	LED_exit();
	BTN_exit();
	SD_exit();
	MAC_exit();
	LVDS_exit();
	VIDEO_exit();
	AUDIO_exit();
	LIGHT_exit();
}

void printConf(MbTestConf *conf)
{
	MBAT_INFO("============== [Show AUTOTEST config] =================\n");
	LED_showConf(&conf->led_conf);
	BTN_showConf(&conf->btn_conf);
	SD_showConf(&conf->sd_conf);
	MAC_showConf(&conf->mac_conf);
	LVDS_showConf(&conf->lvds_conf);
	VIDEO_showConf(&conf->video_conf);
	AUDIO_showConf(&conf->audio_conf);
	LIGHT_showConf(&conf->light_conf);
	MBAT_INFO("=======================================================\n");
}

void help()
{
	/* TODO */
}

