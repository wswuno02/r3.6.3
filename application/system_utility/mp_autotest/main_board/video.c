#include "event.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "video.h"
#include "debug.h"

int VIDEO_execCmd(char *action_args)
{
	int status;
	char buf[128];

	sprintf(buf, "%s", action_args);
	status = system(buf);

	if (status == -1) {
		MBAT_ERR("System error!\n");
	} else {
		if (WIFEXITED(status)) {
			if (WEXITSTATUS(status) == 0) {
				MBAT_NOTICE("(%s) be excuted successfully.\n", buf);
			} else {
				MBAT_ERR("Run cmd fail and exit code is %d (%s)!\n", WEXITSTATUS(status), buf);
			}
		} else {
			MBAT_ERR("exit status is %d (%s)!\n", WEXITSTATUS(status), buf);
		}
	}

	return status;
}

int VIDEO_writeTestResult(const TestResultFormat *format, const char *res)
{
	int status;
	char buf[128];

	sprintf(buf, "%s/%s_%s_%s_%s.txt",
	        (char *) format->test_res_path,
	        (char *) format->station_id,
	        (char *) format->station_name,
	        (char *) format->test_item_name,
	        res);

	FILE* file_ptr = fopen(buf, "w");
	if (file_ptr == NULL) {
		MBAT_ERR("Failed to write file.\n");
		status = -1;
	} else {
		MBAT_NOTICE("Success to write (%s).\n", buf);
		status = 0;
	}

	fclose(file_ptr);

	return status;
}

int VIDEO_init(VideoConf *conf)
{
	return 0;
}

int VIDEO_autoTest(const VideoConf *conf)
{
	MBAT_DBG("#--- Start %s()...\n", __func__);
	MBAT_NOTICE("========================\n");
	MBAT_NOTICE("=== VIDEO test start ===\n");
	MBAT_NOTICE("========================\n");

	int err = 0;
	char buf[128];

	sprintf(buf, "%s %s", CTRL_SNAPSHOT_CMD, (char *) conf->snapshot_args);

	err += VIDEO_execCmd(LOAD_MPP_CMD);
	err += VIDEO_execCmd(CTRL_STREAM_CMD);
	sleep(3);
	err += VIDEO_execCmd(buf);

	if (err != 0) {

		MBAT_NOTICE("%s_%s_%s ... [NG]\n",
		            conf->format.station_id,
		            conf->format.station_name,
		            conf->format.test_item_name);
		MBAT_INFO("LED stat --- <Blue Led, OFF>, <Red Led, ON>.\n");
		BLUE_LED(LED_OFF);
		RED_LED(LED_ON);
		VIDEO_writeTestResult(&conf->format, "NG");
	} else {

		MBAT_NOTICE("%s_%s_%s ... [OK]\n",
		            conf->format.station_id,
		            conf->format.station_name,
		            conf->format.test_item_name);
		MBAT_INFO("LED stat --- <Blue Led, ON>, <Red Led, OFF>.\n");
		BLUE_LED(LED_ON);
		RED_LED(LED_OFF);
		VIDEO_writeTestResult(&conf->format, "OK");
	}

	MBAT_DBG("#--- End %s()...\n", __func__);

	sleep(5);

	return 0;
}

void VIDEO_exit(void)
{
	return;
}

void VIDEO_showConf(const VideoConf *conf)
{
	MBAT_INFO("#--- [%s]\n", __func__);
	MBAT_INFO("enabled = %d\n", conf->enabled);
	MBAT_INFO("TEST_CASE_FORMT: %s/%s_%s_%s\n", conf->format.test_res_path, conf->format.station_id, conf->format.station_name, conf->format.test_item_name);
	MBAT_INFO("snapshot_args = %s\n", conf->snapshot_args);
}