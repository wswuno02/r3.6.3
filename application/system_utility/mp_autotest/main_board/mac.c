#include "event.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include "mac.h"
#include "debug.h"

#define BUFFER_SIZE (32)

int MAC_execCmd(char *action_args)
{
	int status;
	char buf[128];

	sprintf(buf, "%s", action_args);
	status = system(buf);

	if (status == -1) {
		MBAT_ERR("System error!\n");
	} else {
		if (WIFEXITED(status)) {
			if (WEXITSTATUS(status) == 0) {
				MBAT_NOTICE("(%s) be excuted successfully.\n", buf);
			} else {
				MBAT_ERR("Run cmd fail and exit code is %d (%s)!\n", WEXITSTATUS(status), buf);
			}
		} else {
			MBAT_ERR("exit status is %d (%s)!\n", WEXITSTATUS(status), buf);
		}
	}

	return status;
}

int MAC_getFileContent(const char *path, char *buffer)
{
	int file;
	int read_size;

	file = open(path, O_RDONLY);

	if (file == -1) {
		MBAT_ERR("%s: file not found\n", path);
		return (-1);
	}

	while ((read_size = read(file, buffer, BUFFER_SIZE)) > 0) {
		//write(1, &buffer, read_size);
		if (read_size == -1) {
			MBAT_ERR("%s: fail to read file\n", path);
			return (-1);
		}
	}

	MBAT_INFO("###--- %s(): buffer = %s\n", __func__, buffer);

	close(file);

	return 0;
}

int MAC_writeTestResult(const TestResultFormat *format, const char *res, const char *json, const int json_size)
{
	int status;
	char buf[128];

	sprintf(buf, "%s/%s_%s_%s_%s.txt",
	        (char *) format->test_res_path,
	        (char *) format->station_id,
	        (char *) format->station_name,
	        (char *) format->test_item_name,
	        res);

	FILE* file_ptr = fopen(buf, "w");
	if (file_ptr == NULL) {
		MBAT_ERR("Failed to write file.\n");
		status = -1;
	} else {
		MBAT_NOTICE("-------------> (%s).\n", json);
		fwrite(json, sizeof(char), json_size, file_ptr);
		MBAT_NOTICE("Success to write (%s).\n", buf);
		status = 0;
	}

	fclose(file_ptr);

	return status;
}

int MAC_init(MacConf *conf)
{
	return 0;
}

int MAC_autoTest(const MacConf *conf)
{
	MBAT_DBG("#--- Start %s()...\n", __func__);
	MBAT_NOTICE("======================\n");
	MBAT_NOTICE("=== MAC test start ===\n");
	MBAT_NOTICE("======================\n");

	int err = 0;
	char cmd[128] = {0};
	char tmp_is_vaild[1] = {0};
	char tmp_mac_address[32] = { 0 };
	char *is_vaild = NULL;
	char *mac_address = NULL;

	sprintf(cmd, "/mnt/sdcard/autotest/script/mac_utils %s", (char *) conf->net_type);

	err += MAC_execCmd(cmd);
	err += MAC_getFileContent("/tmp/MacAddress", tmp_mac_address);
	mac_address = strtok(tmp_mac_address, "\n");
	err += MAC_getFileContent("/tmp/isMacAddressValid", tmp_is_vaild);
	is_vaild = strtok(tmp_is_vaild, "\n");

	MBAT_INFO("###--- mac_address = %s\n", mac_address);
	MBAT_INFO("###--- is_vaild = %s\n", is_vaild);

	if (strstr(is_vaild, "0") != NULL) {
		// Success
	} else {
		// Fail
		err += -1;
	}

	char json_buf[128] = {0};
	int json_size = 0;

	if (err != 0) {

		MBAT_NOTICE("%s_%s_%s ... [NG]\n",
		            conf->format.station_id,
		            conf->format.station_name,
		            conf->format.test_item_name);
		MBAT_INFO("LED stat --- <Blue Led, OFF>, <Red Led, ON>.\n");
		BLUE_LED(LED_OFF);
		RED_LED(LED_ON);

		MBAT_INFO("--------> mac_address = %s\n", mac_address);
		json_size = sprintf(json_buf, "{\n    \"mac_address\": \"%s\"\n}", mac_address);
		MAC_writeTestResult(&conf->format, "NG", json_buf, json_size);
	} else {

		MBAT_NOTICE("%s_%s_%s ... [OK]\n",
		            conf->format.station_id,
		            conf->format.station_name,
		            conf->format.test_item_name);
		MBAT_INFO("LED stat --- <Blue Led, ON>, <Red Led, OFF>.\n");
		BLUE_LED(LED_ON);
		RED_LED(LED_OFF);

		MBAT_INFO("--------> mac_address = %s\n", mac_address);
		json_size = sprintf(json_buf, "{\n    \"mac_address\": \"%s\"\n}", mac_address);
		MAC_writeTestResult(&conf->format, "OK", json_buf, json_size);
	}

	MBAT_DBG("#--- End %s()...\n", __func__);

	sleep(5);

	return 0;
}

void MAC_exit(void)
{
	return;
}

void MAC_showConf(const MacConf *conf)
{
	MBAT_INFO("#--- [%s]\n", __func__);
	MBAT_INFO("enabled = %d\n", conf->enabled);
	MBAT_INFO("TEST_CASE_FORMT: %s/%s_%s_%s\n", conf->format.test_res_path, conf->format.station_id, conf->format.station_name, conf->format.test_item_name);
	MBAT_INFO("net_type = %s\n", conf->net_type);
	return;
}