#ifndef MB_TEST_LVDS_H_
#define MB_TEST_LVDS_H_

#include "mb_conf.h"

#define LOAD_MPP_CMD "sh /system/mpp/script/load_mpp.sh -i"
#define CALIB_LVDS_DELAY_CMD "/system/bin/lvds_test -d /mnt/sdcard/autotest/script/snap_shot.conf"
#define LVDS_DELAY_RES "/calib/factory_default/lvds_delay"

int LVDS_init(LvdsConf *);
int LVDS_autoTest(const LvdsConf *);
void LVDS_exit(void);
void LVDS_showConf(const LvdsConf *);

#endif
