#ifndef MB_TEST_DEBUG_H_
#define MB_TEST_DEBUG_H_

#define MBAT_TRACE (1)

#define MBAT_ERR(x, ...) printf("[MBAT][ERROR] %s(): " x, __func__, ##__VA_ARGS__)
#define MBAT_WARN(x, ...) printf("[MBAT][WARNING] %s(): " x, __func__, ##__VA_ARGS__)
#define MBAT_NOTICE(x, ...) printf("[MBAT][NOTICE] " x, ##__VA_ARGS__)
#define MBAT_INFO(x, ...) printf("[MBAT][INFO] " x, ##__VA_ARGS__)

#if MBAT_TRACE
#define MBAT_DBG(x, ...) printf("[MBAT][DEBUG] " x, ##__VA_ARGS__)
#else
#define MBAT_DBG(x, ...)
#endif

#endif