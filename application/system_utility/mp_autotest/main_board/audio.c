#include "event.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ado_ctrl.h>
#include "audio.h"
#include "debug.h"

#define WAIT_SEC (2000000)

int AUDIO_writeTestResult(const TestResultFormat *format, const char *res)
{
	int status;
	char buf[128];

	sprintf(buf, "%s/%s_%s_%s_%s.txt",
	        (char *) format->test_res_path,
	        (char *) format->station_id,
	        (char *) format->station_name,
	        (char *) format->test_item_name,
	        res);

	FILE* file_ptr = fopen(buf, "w");
	if (file_ptr == NULL) {
		MBAT_ERR("Failed to write file.\n");
		status = -1;
	} else {
		MBAT_NOTICE("Success to write (%s).\n", buf);
		status = 0;
	}

	fclose(file_ptr);

	return status;
}

int AUDIO_init(AudioConf *conf)
{
	return 0;
}
int AUDIO_autoTest(const AudioConf *conf)
{
	MBAT_DBG("#--- Start %s()...\n", __func__);
	MBAT_NOTICE("========================\n");
	MBAT_NOTICE("=== AUDIO test start ===\n");
	MBAT_NOTICE("========================\n");
	MBAT_INFO("LED stat --- <Blue Led, MED_FLICKER>, <Red Led, MED_FLICKER>.\n");
	RED_LED_FLICKER(MED_SPEED);
	BLUE_LED_FLICKER(MED_SPEED);

	int err = 0;
	int ret = -1;

	if (conf->mic_enabled == 1) {
		MBAT_INFO("BTN stat --- Press to START audio record (3s)...\n");
		BUTTON_PUSH();
		MBAT_INFO("LED stat --- <Blue Led, HIGH_FLICKER>, <Red Led, HIGH_FLICKER>.\n");
		RED_LED_FLICKER(HIGH_SPEED);
		BLUE_LED_FLICKER(HIGH_SPEED);

		ret = ADO_recordFileStart((char *)conf->mic_args);

		if (ret < 0) {
			MBAT_ERR("ADO_recordFileStart failed (ret = %d)\n", ret);
			err += ret;
		}

		MBAT_INFO("BTN stat --- Release for PLAY audio record...\n");
		BUTTON_REL();

		ret = ADO_recordFileStop();

		if (ret < 0) {
			MBAT_ERR("ADO_recordFileStop failed (ret = %d)\n", ret);
			err += ret;
		}

		sleep(1);
		MBAT_NOTICE("PLAY audio... (%s)\n", conf->mic_args);
		ret = ADO_playFile((char *)conf->mic_args);

		if (ret < 0) {
			MBAT_ERR("---> FAIL\n");
			err += ret;
		} else {
			MBAT_NOTICE("---> SUCCESS\n");
		}
	}

	if (conf->spk_enabled == 1) {
		MBAT_NOTICE("PLAY audio... (%s)\n", conf->speaker_args);
		ret = ADO_playFile((char *)conf->speaker_args);

		if (ret < 0) {
			MBAT_ERR("---> FAIL\n");
			err += ret;
		} else {
			MBAT_NOTICE("---> SUCCESS\n");
		}
	}

	MBAT_NOTICE("WAIT for judgment... OK / NG\n");
	MBAT_INFO("LED stat --- <Blue Led, OFF>, <Red Led, OFF>.\n");
	BLUE_LED(LED_OFF);
	RED_LED(LED_OFF);

	long usec = BUTTON_CLICK();
	MBAT_INFO("BTN stat --- Detect %ld us click.\n", usec);

	if (usec < WAIT_SEC) {

		MBAT_NOTICE("%s_%s_%s ... [OK]\n",
		            conf->format.station_id,
		            conf->format.station_name,
		            conf->format.test_item_name);
		MBAT_INFO("LED stat --- <Blue Led, ON>, <Red Led, OFF>.\n");
		BLUE_LED(LED_ON);
		RED_LED(LED_OFF);
		err += 0;
	} else {

		MBAT_NOTICE("%s_%s_%s ... [NG]\n",
		            conf->format.station_id,
		            conf->format.station_name,
		            conf->format.test_item_name);
		MBAT_INFO("LED stat --- <Blue Led, OFF>, <Red Led, ON>.\n");
		BLUE_LED(LED_OFF);
		RED_LED(LED_ON);
		err += -1;
	}

	if (err != 0) {
		AUDIO_writeTestResult(&conf->format, "NG");
	} else {
		AUDIO_writeTestResult(&conf->format, "OK");
	}

	MBAT_DBG("#--- End %s()...\n", __func__);

	sleep(5);

	return 0;
}
void AUDIO_exit(void)
{
	return;
}
void AUDIO_showConf(const AudioConf *conf)
{
	MBAT_INFO("#--- [%s]\n", __func__);
	MBAT_INFO("enabled = %d\n", conf->enabled);
	MBAT_INFO("mic_enabled = %d\n", conf->mic_enabled);
	MBAT_INFO("spk_enabled = %d\n", conf->spk_enabled);
	MBAT_INFO("TEST_CASE_FORMT: %s/%s_%s_%s\n", conf->format.test_res_path, conf->format.station_id, conf->format.station_name, conf->format.test_item_name);
	MBAT_INFO("mic_args = %s\n", conf->mic_args);
	MBAT_INFO("speaker_args = %s\n", conf->speaker_args);
}