#ifndef MB_TEST_LIGHT_H_
#define MB_TEST_LIGHT_H_

#include "mb_conf.h"

#define CTRL_IR_CUT_CMD "sh /mnt/sdcard/autotest/script/ir_cut.sh"
#define CTRL_IR_LED_CMD "sh /mnt/sdcard/autotest/script/led.sh"

int LIGHT_init(LightConf *);
int LIGHT_autoTest(const LightConf *);
void LIGHT_exit(void);
void LIGHT_showConf(const LightConf *);

#endif
