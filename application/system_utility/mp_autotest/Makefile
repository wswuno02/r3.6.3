SDKSRC_DIR ?= $(realpath $(CURDIR)/../../..)
include $(SDKSRC_DIR)/application/internal.mk

TARGETS := ledctrl btndetect main_board audioctrl light_test daynightctrl
TARGETS_CLEAN :=  $(addsuffix -clean, $(TARGETS))

SEMIMB_SRC_SCRIPT_DIR = $(MP_AUTOTEST_PATH)/scripts/semimb_test
CALIB_SRC_SCRIPT_DIR = $(MP_AUTOTEST_PATH)/scripts/calib_test
MB_SRC_SCRIPT_DIR = $(MP_AUTOTEST_PATH)/scripts/mb_test
IMG_SRC_SCRIPT_DIR = $(MP_AUTOTEST_PATH)/scripts/img_test
WIFI_SRC_SCRIPT_DIR = $(MP_AUTOTEST_PATH)/scripts/wifi_test
SHIPPING_SRC_SCRIPT_DIR = $(MP_AUTOTEST_PATH)/scripts/shipping_test
COMMON_DIR = $(MP_AUTOTEST_PATH)/scripts/common
CHANGE_MODE_SRC_SCRIPT_DIR = $(MP_AUTOTEST_PATH)/scripts/change_mode
DEV_INFO_SRC_SCRIPT_DIR = $(MP_AUTOTEST_PATH)/scripts/dev_info
STRESS_SRC_SCRIPT_DIR = $(MP_AUTOTEST_PATH)/scripts/stress_test

# SD card interface path #
SD_CARD_INTF = $(MP_AUTOTEST_PATH)/SdCardAccessCommandInterface


DEST_SCRIPT_DIR = $(SYSTEMFS)/calibtest
DEST_MP_DIR = $(SYSROOT)/usrdata/mass_production

.PHONY: all
all: binall

.PHONY: binall
binall: $(TARGETS)

.PHONY: $(TARGETS)
$(TARGETS):
	$(Q)$(MAKE) -C $@ all

.PHONY: $(TARGETS_CLEAN)
$(TARGETS_CLEAN):
	$(Q)$(MAKE) -C $(patsubst %-clean,%,$@) clean

.PHONY: clean
clean: $(TARGETS_CLEAN)

.PHONY: distclean
distclean: uninstall clean

.PHONY: install
install:
	@rm -rf $(DEST_SCRIPT_DIR)

	# install MB_SRC_SCRIPT_DIR
	@cp -f $(MP_AUTOTEST_PATH)/ledctrl/ledctrl $(SEMIMB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/led.conf $(SEMIMB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/ir_cut.sh $(SEMIMB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/led.sh $(SEMIMB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/test_speaker.wav $(SEMIMB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/mac_utils $(SEMIMB_SRC_SCRIPT_DIR)
	@cp -f $(MP_AUTOTEST_PATH)/main_board/mb $(SEMIMB_SRC_SCRIPT_DIR)
	@cp -f $(MP_AUTOTEST_PATH)/btndetect/btndetect $(SEMIMB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/btn.conf $(SEMIMB_SRC_SCRIPT_DIR)

	# install CALIB_SRC_SCRIPT_DIR
	@cp -f $(MP_AUTOTEST_PATH)/ledctrl/ledctrl $(CALIB_SRC_SCRIPT_DIR)
	@cp -f $(MP_AUTOTEST_PATH)/audioctrl/audioctrl $(CALIB_SRC_SCRIPT_DIR)
	@cp -f $(MP_AUTOTEST_PATH)/light_test/light_test $(CALIB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/led.conf $(CALIB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/ir_cut.sh $(CALIB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/led.sh $(CALIB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/test_speaker.wav $(CALIB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/ini_mp.db $(CALIB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/C0* $(CALIB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/ip_address.conf $(CALIB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/wpa_supplicant.conf $(CALIB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/gpio_utils $(CALIB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/ini_mp_bp.db $(CALIB_SRC_SCRIPT_DIR)
	@cp -f $(MP_AUTOTEST_PATH)/btndetect/btndetect $(CALIB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/btn.conf $(CALIB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/usb_eth.sh $(CALIB_SRC_SCRIPT_DIR)

	# install STRESS_SRC_SCRIPT_DIR
	@cp -f $(MP_AUTOTEST_PATH)/ledctrl/ledctrl $(STRESS_SRC_SCRIPT_DIR)
	@cp -f $(MP_AUTOTEST_PATH)/audioctrl/audioctrl $(STRESS_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/led.conf $(STRESS_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/siren.ul $(STRESS_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/C0* $(STRESS_SRC_SCRIPT_DIR)

	# install MB_SRC_SCRIPT_DIR
	@cp -f $(MP_AUTOTEST_PATH)/ledctrl/ledctrl $(MB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/led.conf $(MB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/ir_cut.sh $(MB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/led.sh $(MB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/test_speaker.wav $(MB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/mac_utils $(MB_SRC_SCRIPT_DIR)
	@cp -f $(MP_AUTOTEST_PATH)/main_board/mb $(MB_SRC_SCRIPT_DIR)
	@cp -f $(MP_AUTOTEST_PATH)/btndetect/btndetect $(MB_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/btn.conf $(MB_SRC_SCRIPT_DIR)

	# install IMG_SRC_SCRIPT_DIR
	@cp -f $(MP_AUTOTEST_PATH)/ledctrl/ledctrl $(IMG_SRC_SCRIPT_DIR)
	@cp -f $(MP_AUTOTEST_PATH)/light_test/light_test $(IMG_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/led.conf $(IMG_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/ini_mp.db $(IMG_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/C0* $(IMG_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/ip_address.conf $(IMG_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/wpa_supplicant.conf $(IMG_SRC_SCRIPT_DIR)
	@cp -f $(MP_AUTOTEST_PATH)/btndetect/btndetect $(IMG_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/btn.conf $(IMG_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/usb_eth.sh $(IMG_SRC_SCRIPT_DIR)

	# install WIFI_SRC_SCRIPT_DIR
	@cp -f $(MP_AUTOTEST_PATH)/ledctrl/ledctrl $(WIFI_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/led.conf $(WIFI_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/ir_cut.sh $(WIFI_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/ini_mp.db $(WIFI_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/C0* $(WIFI_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/ip_address.conf $(WIFI_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/wpa_supplicant.conf $(WIFI_SRC_SCRIPT_DIR)
	@cp -f $(MP_AUTOTEST_PATH)/btndetect/btndetect $(WIFI_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/btn.conf $(WIFI_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/usb_eth.sh $(WIFI_SRC_SCRIPT_DIR)

	# install SHIPPING_SRC_SCRIPT_DIR
	@cp -f $(MP_AUTOTEST_PATH)/ledctrl/ledctrl $(SHIPPING_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/led.conf $(SHIPPING_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/ir_cut.sh $(SHIPPING_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/ini_mp.db $(SHIPPING_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/C0* $(SHIPPING_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/ip_address.conf $(SHIPPING_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/wpa_supplicant.conf $(SHIPPING_SRC_SCRIPT_DIR)
	@cp -f $(MP_AUTOTEST_PATH)/btndetect/btndetect $(SHIPPING_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/btn.conf $(SHIPPING_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/usb_eth.sh $(SHIPPING_SRC_SCRIPT_DIR)

	# install CHANGE_MODE_SRC_SCRIPT_DIR
	@cp -f $(MP_AUTOTEST_PATH)/ledctrl/ledctrl $(CHANGE_MODE_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/led.conf $(CHANGE_MODE_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/C03unicorn $(CHANGE_MODE_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/ip_address.conf $(CHANGE_MODE_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/wpa_supplicant.conf $(CHANGE_MODE_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/usb_eth.sh $(CHANGE_MODE_SRC_SCRIPT_DIR)

	# install DEV_INFO_SRC_SCRIPT_DIR
	@cp -f $(MP_AUTOTEST_PATH)/ledctrl/ledctrl $(DEV_INFO_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/led.conf $(DEV_INFO_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/C03unicorn $(DEV_INFO_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/ip_address.conf $(DEV_INFO_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/wpa_supplicant.conf $(DEV_INFO_SRC_SCRIPT_DIR)
	@cp -f $(COMMON_DIR)/usb_eth.sh $(DEV_INFO_SRC_SCRIPT_DIR)


	@rm -rf $(DEST_MP_DIR)
	mkdir -p $(DEST_MP_DIR)
	# install DEST_MP_DIR
	@cp -f $(MP_AUTOTEST_PATH)/audioctrl/audioctrl $(DEST_MP_DIR)	
	@cp -f $(MP_AUTOTEST_PATH)/btndetect/btndetect $(DEST_MP_DIR)
	@cp -f $(MP_AUTOTEST_PATH)/daynightctrl/daynightctrl $(DEST_MP_DIR)
	@cp -f $(MP_AUTOTEST_PATH)/daynightctrl/day_night.json $(DEST_MP_DIR)
	@cp -f $(MP_AUTOTEST_PATH)/ledctrl/ledctrl $(DEST_MP_DIR)
	@cp -f $(MP_AUTOTEST_PATH)/light_test/light_test $(DEST_MP_DIR)
	@cp -f $(COMMON_DIR)/led.sh $(DEST_MP_DIR)
	@cp -f $(COMMON_DIR)/usb_eth.sh $(DEST_MP_DIR)
	@cp -f $(COMMON_DIR)/btn.sh $(DEST_MP_DIR)
	@cp -f $(COMMON_DIR)/ini_mp.db $(DEST_MP_DIR)
	@cp -f $(COMMON_DIR)/test_speaker.wav $(DEST_MP_DIR)
	@cp -f $(COMMON_DIR)/btn.conf $(DEST_MP_DIR)
	@cp -f $(COMMON_DIR)/ip_address.conf $(DEST_MP_DIR)
	@cp -f $(COMMON_DIR)/led.conf $(DEST_MP_DIR)
	@cp -f $(COMMON_DIR)/light_test.conf $(DEST_MP_DIR)
	@cp -f $(COMMON_DIR)/wpa_supplicant.conf $(DEST_MP_DIR)
	@cp -f $(COMMON_DIR)/mp_wifi.sh $(DEST_MP_DIR)
	@cp -f $(COMMON_DIR)/C01ccserver $(DEST_MP_DIR)
	@cp -f $(COMMON_DIR)/C02av_main $(DEST_MP_DIR)
	@cp -f $(COMMON_DIR)/C03unicorn $(DEST_MP_DIR)
	
.PHONY: uninstall
uninstall:
	@rm -rf $(DEST_SCRIPT_DIR)

	@rm -f $(SEMIMB_SRC_SCRIPT_DIR)/ledctrl
	@rm -f $(SEMIMB_SRC_SCRIPT_DIR)/led.conf
	@rm -f $(SEMIMB_SRC_SCRIPT_DIR)/ir_cut.sh
	@rm -f $(SEMIMB_SRC_SCRIPT_DIR)/led.sh
	@rm -f $(SEMIMB_SRC_SCRIPT_DIR)/test_speaker.wav
	@rm -f $(SEMIMB_SRC_SCRIPT_DIR)/mac_utils
	@rm -f $(SEMIMB_SRC_SCRIPT_DIR)/mb
	@rm -f $(SEMIMB_SRC_SCRIPT_DIR)/btndetect

	@rm -f $(CALIB_SRC_SCRIPT_DIR)/ledctrl
	@rm -f $(CALIB_SRC_SCRIPT_DIR)/audioctrl
	@rm -f $(CALIB_SRC_SCRIPT_DIR)/light_test
	@rm -f $(CALIB_SRC_SCRIPT_DIR)/led.conf
	@rm -f $(CALIB_SRC_SCRIPT_DIR)/ir_cut.sh
	@rm -f $(CALIB_SRC_SCRIPT_DIR)/led.sh
	@rm -f $(CALIB_SRC_SCRIPT_DIR)/test_speaker.wav
	@rm -f $(CALIB_SRC_SCRIPT_DIR)/ini_mp.db
	@rm -f $(CALIB_SRC_SCRIPT_DIR)/C0*
	@rm -f $(CALIB_SRC_SCRIPT_DIR)/ip_address.conf
	@rm -f $(CALIB_SRC_SCRIPT_DIR)/wpa_supplicant.conf
	@rm -f $(CALIB_SRC_SCRIPT_DIR)/gpio_utils
	@rm -f $(CALIB_SRC_SCRIPT_DIR)/ini_mp_bp.db
	@rm -f $(CALIB_SRC_SCRIPT_DIR)/btndetect
	@rm -f $(CALIB_SRC_SCRIPT_DIR)/btn.conf
	@rm -f $(CALIB_SRC_SCRIPT_DIR)/btn.conf
	@rm -f $(CALIB_SRC_SCRIPT_DIR)/usb_eth.sh

	@rm -f $(STRESS_SRC_SCRIPT_DIR)/ledctrl
	@rm -f $(STRESS_SRC_SCRIPT_DIR)/audioctrl
	@rm -f $(STRESS_SRC_SCRIPT_DIR)/led.conf
	@rm -f $(STRESS_SRC_SCRIPT_DIR)/siren.ul
	@rm -f $(STRESS_SRC_SCRIPT_DIR)/C0*

	@rm -f $(MB_SRC_SCRIPT_DIR)/ledctrl
	@rm -f $(MB_SRC_SCRIPT_DIR)/led.conf
	@rm -f $(MB_SRC_SCRIPT_DIR)/ir_cut.sh
	@rm -f $(MB_SRC_SCRIPT_DIR)/led.sh
	@rm -f $(MB_SRC_SCRIPT_DIR)/test_speaker.wav
	@rm -f $(MB_SRC_SCRIPT_DIR)/mac_utils
	@rm -f $(MB_SRC_SCRIPT_DIR)/mb
	@rm -f $(MB_SRC_SCRIPT_DIR)/btndetect
	@rm -f $(MB_SRC_SCRIPT_DIR)/btn.conf

	@rm -f $(IMG_SRC_SCRIPT_DIR)/ledctrl
	@rm -f $(IMG_SRC_SCRIPT_DIR)/light_test
	@rm -f $(IMG_SRC_SCRIPT_DIR)/led.conf
	@rm -f $(IMG_SRC_SCRIPT_DIR)/ini_mp.db
	@rm -f $(IMG_SRC_SCRIPT_DIR)/C0*
	@rm -f $(IMG_SRC_SCRIPT_DIR)/ip_address.conf
	@rm -f $(IMG_SRC_SCRIPT_DIR)/wpa_supplicant.conf
	@rm -f $(IMG_SRC_SCRIPT_DIR)/btndetect
	@rm -f $(IMG_SRC_SCRIPT_DIR)/btn.conf
	@rm -f $(IMG_SRC_SCRIPT_DIR)/usb_eth.sh

	@rm -f $(WIFI_SRC_SCRIPT_DIR)/ledctrl
	@rm -f $(WIFI_SRC_SCRIPT_DIR)/led.conf
	@rm -f $(WIFI_SRC_SCRIPT_DIR)/ir_cut.sh
	@rm -f $(WIFI_SRC_SCRIPT_DIR)/ini_mp.db
	@rm -f $(WIFI_SRC_SCRIPT_DIR)/C0*
	@rm -f $(WIFI_SRC_SCRIPT_DIR)/ip_address.conf
	@rm -f $(WIFI_SRC_SCRIPT_DIR)/wpa_supplicant.conf
	@rm -f $(WIFI_SRC_SCRIPT_DIR)/btndetect
	@rm -f $(WIFI_SRC_SCRIPT_DIR)/btn.conf
	@rm -f $(WIFI_SRC_SCRIPT_DIR)/usb_eth.sh

	@rm -f $(SHIPPING_SRC_SCRIPT_DIR)/ledctrl
	@rm -f $(SHIPPING_SRC_SCRIPT_DIR)/led.conf
	@rm -f $(SHIPPING_SRC_SCRIPT_DIR)/ir_cut.sh
	@rm -f $(SHIPPING_SRC_SCRIPT_DIR)/ini_mp.db
	@rm -f $(SHIPPING_SRC_SCRIPT_DIR)/C0*
	@rm -f $(SHIPPING_SRC_SCRIPT_DIR)/ip_address.conf
	@rm -f $(SHIPPING_SRC_SCRIPT_DIR)/wpa_supplicant.conf
	@rm -f $(SHIPPING_SRC_SCRIPT_DIR)/btndetect
	@rm -f $(SHIPPING_SRC_SCRIPT_DIR)/btn.conf
	@rm -f $(SHIPPING_SRC_SCRIPT_DIR)/usb_eth.sh

	@rm -f $(CHANGE_MODE_SRC_SCRIPT_DIR)/ledctrl
	@rm -f $(CHANGE_MODE_SRC_SCRIPT_DIR)/led.conf
	@rm -f $(CHANGE_MODE_SRC_SCRIPT_DIR)/C03unicorn
	@rm -f $(CHANGE_MODE_SRC_SCRIPT_DIR)/ip_address.conf
	@rm -f $(CHANGE_MODE_SRC_SCRIPT_DIR)/wpa_supplicant.conf
	@rm -f $(CHANGE_MODE_SRC_SCRIPT_DIR)/usb_eth.sh

	@rm -f $(DEV_INFO_SRC_SCRIPT_DIR)/ledctrl
	@rm -f $(DEV_INFO_SRC_SCRIPT_DIR)/led.conf
	@rm -f $(DEV_INFO_SRC_SCRIPT_DIR)/C03unicorn
	@rm -f $(DEV_INFO_SRC_SCRIPT_DIR)/ip_address.conf
	@rm -f $(DEV_INFO_SRC_SCRIPT_DIR)/wpa_supplicant.conf
	@rm -f $(DEV_INFO_SRC_SCRIPT_DIR)/usb_eth.sh

	@rm -rf $(DEST_MP_DIR)

	# install MB_BIN_DIR
	@rm -f $(DEST_MP_DIR)/audioctrl
	@rm -f $(DEST_MP_DIR)/btndetect
	@rm -f $(DEST_MP_DIR)/daynightctrl/daynightctrl
	@rm -f $(DEST_MP_DIR)/daynightctrl/day_night.json
	@rm -f $(DEST_MP_DIR)/ledctrl
	@rm -f $(DEST_MP_DIR)/light_test
	@rm -f $(DEST_MP_DIR)/led.sh
	@rm -f $(DEST_MP_DIR)/usb_eth.sh
	@rm -f $(DEST_MP_DIR)/ini_mp.db
	@rm -f $(DEST_MP_DIR)/test_speaker.wav
	@rm -f $(DEST_MP_DIR)/btn.conf
	@rm -f $(DEST_MP_DIR)/ip_address.conf
	@rm -f $(DEST_MP_DIR)/led.conf
	@rm -f $(DEST_MP_DIR)/light_test.conf
	@rm -f $(DEST_MP_DIR)/wpa_supplicant.conf
	@rm -f $(DEST_MP_DIR)/mp_wifi.sh
	@rm -f $(DEST_MP_DIR)/C01ccserver
	@rm -f $(DEST_MP_DIR)/C02av_main
	@rm -f $(DEST_MP_DIR)/C03unicorn
	
.PHONY: sdcard-intf
sdcard-intf:
	mkdir -p $(SD_CARD_INTF)
	rm -rf $(SD_CARD_INTF)/*

	# create autotest script folder #
	mkdir -p $(SD_CARD_INTF)/semimbtest/autotest/script
	mkdir -p $(SD_CARD_INTF)/calibtest/autotest/script
	mkdir -p $(SD_CARD_INTF)/mbtest/autotest/script
	mkdir -p $(SD_CARD_INTF)/imgtest/autotest/script
	mkdir -p $(SD_CARD_INTF)/wifitest/autotest/script
	mkdir -p $(SD_CARD_INTF)/shippingtest/autotest/script
	mkdir -p $(SD_CARD_INTF)/changemode/autotest/script
	mkdir -p $(SD_CARD_INTF)/devinfo/autotest/script
	mkdir -p $(SD_CARD_INTF)/stresstest/autotest/script

	# put flag for each autotest #
	touch $(SD_CARD_INTF)/semimbtest/RunAutoTest
	touch $(SD_CARD_INTF)/calibtest/RunAutoTest
	touch $(SD_CARD_INTF)/mbtest/RunAutoTest
	touch $(SD_CARD_INTF)/imgtest/RunAutoTest
	touch $(SD_CARD_INTF)/wifitest/RunAutoTest
	touch $(SD_CARD_INTF)/shippingtest/RunAutoTest
	touch $(SD_CARD_INTF)/shippingtest/RunProductionTool
	touch $(SD_CARD_INTF)/changemode/RunAutoTest
	touch $(SD_CARD_INTF)/changemode/RunProductionTool
	touch $(SD_CARD_INTF)/devinfo/RunAutoTest
	touch $(SD_CARD_INTF)/devinfo/RunProductionTool
	touch $(SD_CARD_INTF)/stresstest/RunAutoTest

	# copy file #
	rsync -avh $(SEMIMB_SRC_SCRIPT_DIR)/* $(SD_CARD_INTF)/semimbtest/autotest/script/ --exclude $(SEMIMB_SRC_SCRIPT_DIR)/.gitignore
	rsync -avh $(CALIB_SRC_SCRIPT_DIR)/* $(SD_CARD_INTF)/calibtest/autotest/script/ --exclude $(CALIB_SRC_SCRIPT_DIR)/.gitignore
	rsync -avh $(MB_SRC_SCRIPT_DIR)/* $(SD_CARD_INTF)/mbtest/autotest/script/ --exclude $(MB_SRC_SCRIPT_DIR)/.gitignore
	rsync -avh $(IMG_SRC_SCRIPT_DIR)/* $(SD_CARD_INTF)/imgtest/autotest/script/ --exclude $(IMG_SRC_SCRIPT_DIR)/.gitignore
	rsync -avh $(WIFI_SRC_SCRIPT_DIR)/* $(SD_CARD_INTF)/wifitest/autotest/script/ --exclude $(WIFI_SRC_SCRIPT_DIR)/.gitignore
	rsync -avh $(SHIPPING_SRC_SCRIPT_DIR)/* $(SD_CARD_INTF)/shippingtest/autotest/script/ --exclude $(SHIPPING_SRC_SCRIPT_DIR)/.gitignore
	rsync -avh $(CHANGE_MODE_SRC_SCRIPT_DIR)/* $(SD_CARD_INTF)/changemode/autotest/script/ --exclude $(CHANGE_MODE_SRC_SCRIPT_DIR)/.gitignore
	rsync -avh $(DEV_INFO_SRC_SCRIPT_DIR)/* $(SD_CARD_INTF)/devinfo/autotest/script/ --exclude $(DEV_INFO_SRC_SCRIPT_DIR)/.gitignore
	rsync -avh $(STRESS_SRC_SCRIPT_DIR)/* $(SD_CARD_INTF)/stresstest/autotest/script/ --exclude $(STRESS_SRC_SCRIPT_DIR)/.gitignore

.PHONY: sdcard-intf-clean
sdcard-intf-clean:
	rm -rf $(SD_CARD_INTF)
