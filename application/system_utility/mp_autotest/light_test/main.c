#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <json.h>

#include "parse.h"

typedef enum {
	ADC_CHN_0 = 0,
	ADC_CHN_1,
	ADC_CHN_2,
	ADC_CHN_NUM,
} AdcChn;

typedef struct {
	int     id;
	char    *name;
} ALIAS_TABLE_S;

static ALIAS_TABLE_S k_adc_path[ADC_CHN_NUM] = {
	{ ADC_CHN_0,   "/sys/bus/iio/devices/iio:device0/in_voltage0_raw"},
	{ ADC_CHN_1,   "/sys/bus/iio/devices/iio:device0/in_voltage1_raw"},
	{ ADC_CHN_2,   "/sys/bus/iio/devices/iio:device0/in_voltage2_raw"},
};

LightTestConf g_conf;
char *json_str = NULL;

char *getFileContent(const char *);
void printConf(LightTestConf *);
void help(char *str);
void parse(int, char **);
int getAdcValue(const char *adc_file_path, int *adc_value);
int execCmd(char *action_args);
int runLightTest(LightTest *conf);

static void handleSigInt(int signo)
{
	//	UINT32 idx = 0;

	if (signo == SIGINT) {
		printf("Caught SIGINT!\n");
	} else if (signo == SIGTERM) {
		printf("Caught SIGTERM!\n");
	} else {
		perror("Unexpected signal!\n");
	}

	if (json_str) {
		free(json_str);
		json_str = NULL;
	}

	exit(0);
}

int main(int argc, char **argv)
{
	parse(argc, argv);
	printConf(&g_conf);

	if (g_conf.light_test.enabled == 1) {
		runLightTest(&g_conf.light_test);
	}

	return 0;
}

void parse(int argc, char **argv)
{
	json_object *jobj = NULL;
	int opt;

	while ((opt = getopt(argc, argv, "h")) != -1) {
		switch (opt) {
		case 'h':
			help(argv[0]);
			break;
		default:
			exit(EXIT_FAILURE);
			break;
		}
	}

	if (optind >= argc) {
		fprintf(stderr, "Expect file name after options.\n");
		exit(EXIT_FAILURE);
	}

	json_str = getFileContent(argv[optind]);
	if (!json_str) {
		printf("Failed to read %s.", argv[optind]);
		exit(EXIT_FAILURE);
	}

	jobj = json_tokener_parse(json_str);
	if (jobj == NULL) {
		printf("Invalid JSON string.");
		goto free_json_str;
	}

	if (signal(SIGINT, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGINT!\n");
		goto free_json_str;
	}

	if (signal(SIGTERM, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGTERM!\n");
		goto free_json_str;
	}

	parse_light_test_conf(&g_conf, jobj);

	return;

free_json_str:

	free(json_str);
	json_str = NULL;
	exit(EXIT_FAILURE);
}

char *getFileContent(const char *path)
{
	char *ptr = NULL;
	FILE *fp;
	long fsize = 0;

	if ((fp = fopen(path, "r")) != NULL) {
		fseek(fp, 0L, SEEK_END);
		fsize = ftell(fp);
		fseek(fp, 0L, SEEK_SET);

		ptr = (char *)malloc(fsize + 1);

		if (!ptr) {
			goto end;
		}

		memset(ptr, 0, fsize);

		fread(ptr, fsize, 1, fp);

		ptr[fsize] = 0;

		if (ferror(fp)) {
			free(ptr);
		}
	}

end:
	fclose(fp);
	return ptr;
}

void printConf(LightTestConf *conf)
{
	printf("enabled = %d\n", conf->light_test.enabled);
	printf("adc.period = %d\n", conf->light_test.adc.period);
	printf("adc.chn = %d\n", conf->light_test.adc.chn);
	printf("adc.day_th = %d\n", conf->light_test.adc.day_th);
	printf("adc.night_th = %d\n", conf->light_test.adc.night_th);
	printf("adc.day_action = %s\n", conf->light_test.adc.day_action);
	printf("adc.night_action = %s\n", conf->light_test.adc.night_action);
}

void help(char *str)
{
	printf("USAGE:\n");
	printf("\t%s [FILE]\n", str);
	printf("\n");
	printf("OPTION:\n");
	printf("\t'-h' help\n");
	printf("\n");
}

int getAdcValue(const char *adc_file_path, int *adc_value)
{
	int fd = 0;
	char ch[4] = {0};

	fd = open(adc_file_path, O_RDONLY, 0x644);
	if (fd < 0) {
		printf("Cannot open ADC file path %s!\n", adc_file_path);
		return fd;
	}
	read(fd, ch, 4);
	close(fd);

	*adc_value = atoi(ch);

	return 0;
}

int execCmd(char *action_args)
{
	int status;
	char buf[128];

	sprintf(buf, "%s", action_args);
	status = system(buf);

	if (status == -1) {
		printf("System error!\n");
	} else {
		if (WIFEXITED(status)) {
			if (WEXITSTATUS(status) == 0) {
				printf("(%s) be excuted successfully.\n", buf);
			} else {
				printf("Run cmd fail and exit code is %d (%s)!\n", WEXITSTATUS(status), buf);
			}
		} else {
			printf("exit status is %d (%s)!\n", WEXITSTATUS(status), buf);
		}
	}

	return status;
}

int runLightTest(LightTest *conf)
{
	int err = 0;
	int prev_state = 0;
	int curr_state = 0;
	int prev_adc_value;
	int curr_adc_value;

	getAdcValue(k_adc_path[conf->adc.chn].name, &curr_adc_value);

	if (curr_adc_value <= conf->adc.night_th) {
		err += execCmd((char *)conf->adc.night_action);
		curr_state = 0;
	} else if (curr_adc_value > conf->adc.day_th) {
		err += execCmd((char *)conf->adc.day_action);
		curr_state = 1;
	} else {
		// keep curr_state
	}

	prev_state = curr_state;
	prev_adc_value = curr_adc_value;

	printf("night_th = %4d, day_th = %4d, prev_adc_value = %4d, curr_adc_value = %4d.\n", conf->adc.night_th, conf->adc.day_th, prev_adc_value, curr_adc_value);

	while (1) {
		getAdcValue(k_adc_path[conf->adc.chn].name, &curr_adc_value);

		if (curr_adc_value <= conf->adc.night_th) {
			curr_state = 0;
		} else if (curr_adc_value > conf->adc.day_th) {
			curr_state = 1;
		} else {
			// keep curr_state
		}

		if (curr_state - prev_state == -1) {

			printf("DETECT night...\n");
			printf("night_th = %4d, day_th = %4d, prev_adc_value = %4d, curr_adc_value = %4d.\n", conf->adc.night_th, conf->adc.day_th, prev_adc_value, curr_adc_value);
			err += execCmd((char *)conf->adc.night_action);

		} else if (curr_state - prev_state == 1) {

			printf("DETECT day...\n");
			printf("night_th = %4d, day_th = %4d, prev_adc_value = %4d, curr_adc_value = %4d.\n", conf->adc.night_th, conf->adc.day_th, prev_adc_value, curr_adc_value);
			err += execCmd((char *)conf->adc.day_action);
		} else {

		}

		prev_state = curr_state;
		prev_adc_value = curr_adc_value;
		usleep(conf->adc.period * 1000);
	}

	return err;
}