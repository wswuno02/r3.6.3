#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#include <stdio.h>
#include <string.h>
#include <json.h>
#include "parse.h"

#define max(X,Y) (((X) > (Y)) ? (X) : (Y))

void parse_adc(Adc *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	int i;
	if (json_object_object_get_ex(cmd_obj, "period", &tmp_obj)) {
		data->period = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "chn", &tmp_obj)) {
		data->chn = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "day_th", &tmp_obj)) {
		data->day_th = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "night_th", &tmp_obj)) {
		data->night_th = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "day_action", &tmp_obj)) {
		i = max(MAX_STRING_LEN + 1, json_object_get_string_len(tmp_obj));
		strncpy((char *)data->day_action, json_object_get_string(tmp_obj), i);
		data->day_action[i+1] = '\0';
	}
	if (json_object_object_get_ex(cmd_obj, "night_action", &tmp_obj)) {
		i = max(MAX_STRING_LEN + 1, json_object_get_string_len(tmp_obj));
		strncpy((char *)data->night_action, json_object_get_string(tmp_obj), i);
		data->night_action[i+1] = '\0';
	}
}

void parse_light_test(LightTest *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	if (json_object_object_get_ex(cmd_obj, "enabled", &tmp_obj)) {
		data->enabled = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "adc", &tmp_obj)) {
		parse_adc(&data->adc, tmp_obj);
	}
}

void parse_light_test_conf(LightTestConf *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	if (json_object_object_get_ex(cmd_obj, "light_test_conf", &tmp_obj)) {
		parse_light_test(&data->light_test, tmp_obj);
	}
}


#ifdef __cplusplus
}
#endif /* __cplusplus */
