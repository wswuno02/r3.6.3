#ifndef MBTESTCO_H_
#define MBTESTCO_H_

#include "agtx_types.h"
struct json_object;
#define MAX_STRING_LEN (256)

typedef struct {
	AGTX_INT32 period;
	AGTX_INT32 chn;
	AGTX_INT32 day_th;
	AGTX_INT32 night_th;
	AGTX_UINT8 day_action[MAX_STRING_LEN];
	AGTX_UINT8 night_action[MAX_STRING_LEN];
} Adc;

typedef struct {
	AGTX_INT32 enabled;
	Adc adc;
} LightTest;

typedef struct {
	LightTest light_test;
} LightTestConf;

#endif /* MBTESTCO_H_ */