#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <json.h>
#include <getopt.h>
#include <ctype.h>

#include "parse.h"

typedef enum {
	ACTION_NULL,
	ACTION_DAY,
	ACTION_NIGHT,
	ACTION_ADC,
} ACTION_E;

typedef enum {
	ADC_CHN_0 = 0,
	ADC_CHN_1,
	ADC_CHN_2,
	ADC_CHN_NUM,
} ADC_CHN_E;

typedef struct {
	int     id;
	char    *name;
} ALIAS_TABLE_S;

static ALIAS_TABLE_S k_adc_path[ADC_CHN_NUM] = {
	{ ADC_CHN_0,   "/sys/bus/iio/devices/iio:device0/in_voltage0_raw"},
	{ ADC_CHN_1,   "/sys/bus/iio/devices/iio:device0/in_voltage1_raw"},
	{ ADC_CHN_2,   "/sys/bus/iio/devices/iio:device0/in_voltage2_raw"},
};

typedef struct {
	int choice;
	DayNightConf conf;
} APP_CFG_S;

static APP_CFG_S g_app_cfg;
char *json_str = NULL;

char *getFileContent(const char *);
void printCfg(APP_CFG_S *);
void help(char *str);
void parse(int, char **, APP_CFG_S *);

int getAdcValue(const char *adc_file_path, int *adc_value);
int execCmd(char *action_args);
int runAdcTest(APP_CFG_S *cfg);


static const char *g_short_opts = "dnah";
static const struct option g_long_opts[] = {
	{ "day", no_argument, NULL, 'd' },
	{ "night", no_argument, NULL, 'n' },
	{ "adc", no_argument, NULL, 'a' },
	{ 0, 0, 0, 0 },
};

static void handleSig(int signo)
{
	if (json_str) {
		free(json_str);
		json_str = NULL;
	}

	exit(0);
}

int main(int argc, char **argv)
{
	signal(SIGTERM, handleSig);
	signal(SIGQUIT, handleSig);
	signal(SIGKILL, handleSig);
	signal(SIGHUP, handleSig);
	signal(SIGINT, handleSig);

	parse(argc, argv, &g_app_cfg);
	printCfg(&g_app_cfg);
#if 1
	/* select mode */
	switch (g_app_cfg.choice) {
	case ACTION_DAY:

		execCmd((char *)&g_app_cfg.conf.day_action);

		break;
	case ACTION_NIGHT:

		execCmd((char *)&g_app_cfg.conf.night_action);

		break;
	case ACTION_ADC:

		if (g_app_cfg.conf.adc.enabled == 1) {
			runAdcTest(&g_app_cfg);
		}

		break;
	default:
		abort();
	}
#endif
	return 0;
}

void parse(int argc, char **argv, APP_CFG_S *cfg)
{
	int c;
	int num;

	memset(cfg, 0, sizeof(APP_CFG_S));

	json_object *jobj = NULL;

	while ((c = getopt_long(argc, argv, g_short_opts, g_long_opts, NULL)) != -1) {
		num = argc - optind;

		switch (c) {
		case 'd':
			if (num == 1) {
				printf("--- day\n");
				cfg->choice = ACTION_DAY;
			} else {
				printf("%d arguments expected for control day.\n", 1);
				help(argv[0]);
				exit(1);
			}
			break;
		case 'n':
			if (num == 1) {
				cfg->choice = ACTION_NIGHT;
			} else {
				printf("%d arguments expected for control night.\n", 1);
				help(argv[0]);
				exit(1);
			}
			break;
		case 'a':
			if (num == 1) {
				cfg->choice = ACTION_ADC;
			} else {
				printf("%d arguments expected for control adc.\n", 1);
				help(argv[0]);
				exit(1);
			}
			break;
		case 'h':
			help(argv[0]);
			exit(1);
			break;
		case '?':
			if ((optopt == 'g') || (optopt == 's') || (optopt == 'f')) {
				fprintf(stderr, "Option -%c requires an argument.\n", optopt);
			} else if (isprint(optopt)) {
				fprintf(stderr, "Unknown option '-%c'.\n", optopt);
			} else {
				fprintf(stderr, "Unknow option character '\\x%x'.\n", optopt);
			}
			return;
		default:
			abort();
		}
	}

	printf("#### %s\n", argv[optind]);
	json_str = getFileContent(argv[optind]);
	if (!json_str) {
		printf("Failed to read %s.", argv[optind]);
		exit(EXIT_FAILURE);
	}

	jobj = json_tokener_parse(json_str);
	if (jobj == NULL) {
		printf("Invalid JSON string.");
		goto free_json_str;
	}

	parse_day_night_conf(&cfg->conf, jobj);

	return;

free_json_str:

	free(json_str);
	json_str = NULL;
	exit(EXIT_FAILURE);
}

char *getFileContent(const char *path)
{
	char *ptr = NULL;
	FILE *fp;
	long fsize = 0;

	if ((fp = fopen(path, "r")) != NULL) {
		fseek(fp, 0L, SEEK_END);
		fsize = ftell(fp);
		fseek(fp, 0L, SEEK_SET);

		ptr = (char *)malloc(fsize + 1);

		if (!ptr) {
			goto end;
		}

		memset(ptr, 0, fsize);

		fread(ptr, fsize, 1, fp);

		ptr[fsize] = 0;

		if (ferror(fp)) {
			free(ptr);
		}
	}

end:
	fclose(fp);
	return ptr;
}

void printCfg(APP_CFG_S *cfg)
{
	printf("cfg->choice = %d\n", cfg->choice);

	printf("cfg->conf.day_action = %s\n", cfg->conf.day_action);
	printf("cfg->conf.night_action = %s\n", cfg->conf.night_action);

	printf("adc.enabled = %d\n", cfg->conf.adc.enabled);
	printf("adc.period = %d\n", cfg->conf.adc.period);
	printf("adc.chn = %d\n", cfg->conf.adc.chn);
	printf("adc.day_th = %d\n", cfg->conf.adc.day_th);
	printf("adc.night_th = %d\n", cfg->conf.adc.night_th);

}

void help(char *str)
{
	printf("USAGE:\n");
	printf("\t%s [OPTION] [FILE]\n", str);
	printf("\n");
	printf("OPTION:\n");
	printf("\t'-d json_file' ctrl day\n");
	printf("\t'-n json_file' ctrl night\n");
	printf("\t'-a json_file' switch by adc\n");
	printf("\t'-h' help\n");
	printf("\n");
}

int getAdcValue(const char *adc_file_path, int *adc_value)
{
	int fd = 0;
	char ch[4] = {0};

	fd = open(adc_file_path, O_RDONLY, 0x644);
	if (fd < 0) {
		printf("Cannot open ADC file path %s!\n", adc_file_path);
		return fd;
	}
	read(fd, ch, 4);
	close(fd);

	*adc_value = atoi(ch);

	return 0;
}

int execCmd(char *action_args)
{
	int status;
	char buf[128];

	sprintf(buf, "%s", action_args);
	status = system(buf);

	if (status == -1) {
		printf("System error!\n");
	} else {
		if (WIFEXITED(status)) {
			if (WEXITSTATUS(status) == 0) {
				printf("(%s) be excuted successfully.\n", buf);
			} else {
				printf("Run cmd fail and exit code is %d (%s)!\n", WEXITSTATUS(status), buf);
			}
		} else {
			printf("exit status is %d (%s)!\n", WEXITSTATUS(status), buf);
		}
	}

	return status;
}

int runAdcTest(APP_CFG_S *cfg)
{
	int err = 0;
	int prev_state = 0;
	int curr_state = 0;
	int prev_adc_value;
	int curr_adc_value;

	getAdcValue(k_adc_path[cfg->conf.adc.chn].name, &curr_adc_value);

	if (curr_adc_value <= cfg->conf.adc.night_th) {
		err += execCmd((char *)cfg->conf.night_action);
		curr_state = 0;
	} else if (curr_adc_value > cfg->conf.adc.day_th) {
		err += execCmd((char *)cfg->conf.day_action);
		curr_state = 1;
	} else {
		// keep curr_state
	}

	prev_state = curr_state;
	prev_adc_value = curr_adc_value;

	printf("night_th = %4d, day_th = %4d, prev_adc_value = %4d, curr_adc_value = %4d.\n", cfg->conf.adc.night_th, cfg->conf.adc.day_th, prev_adc_value, curr_adc_value);

	while (1) {
		getAdcValue(k_adc_path[cfg->conf.adc.chn].name, &curr_adc_value);

		if (curr_adc_value <= cfg->conf.adc.night_th) {
			curr_state = 0;
		} else if (curr_adc_value > cfg->conf.adc.day_th) {
			curr_state = 1;
		} else {
			// keep curr_state
		}

		if (curr_state - prev_state == -1) {

			printf("DETECT night...\n");
			printf("night_th = %4d, day_th = %4d, prev_adc_value = %4d, curr_adc_value = %4d.\n", cfg->conf.adc.night_th, cfg->conf.adc.day_th, prev_adc_value, curr_adc_value);
			err += execCmd((char *)cfg->conf.night_action);

		} else if (curr_state - prev_state == 1) {

			printf("DETECT day...\n");
			printf("night_th = %4d, day_th = %4d, prev_adc_value = %4d, curr_adc_value = %4d.\n", cfg->conf.adc.night_th, cfg->conf.adc.day_th, prev_adc_value, curr_adc_value);
			err += execCmd((char *)cfg->conf.day_action);
		} else {

		}

		prev_state = curr_state;
		prev_adc_value = curr_adc_value;
		usleep(cfg->conf.adc.period * 1000);
	}

	return err;
}