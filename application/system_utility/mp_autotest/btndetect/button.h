#ifndef MB_TEST_BUTTON_H_
#define MB_TEST_BUTTON_H_

#include "btn_conf.h"

typedef enum {
	BTN_STATUS_PUSH = 0,
	BTN_STATUS_REL,
	BTN_ACTION_PUSH,
	BTN_ACTION_REL,
	BTN_ACTION_PUSH_REL,
	BTN_ACTION_NUM
} BtnEvent;

int BTN_init(BtnConf *);
long BTN_detectEvent(BtnEvent, int);
void BTN_exit();
void BTN_showConf(const BtnConf *);


#endif
