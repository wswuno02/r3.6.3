#ifndef MBTESTCO_H_
#define MBTESTCO_H_

#include "agtx_types.h"
struct json_object;

typedef struct {
	AGTX_INT32 gpio;
	AGTX_INT32 level_off;
	AGTX_INT32 level_on;
} Btn;

typedef struct {
	Btn btn;
	AGTX_INT32 period;
} BtnConf;

typedef struct {
	BtnConf btn_conf;
} BtnDetectConf;

#endif /* MBTESTCO_H_ */
