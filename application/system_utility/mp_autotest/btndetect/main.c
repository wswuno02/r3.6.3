#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <json.h>

#include "button.h"
#include "parse.h"

#define BTN_DETECT_RES "/tmp/btnDetectRes"

BtnDetectConf g_conf;

int click_en = 0;
char *log_name = "btndetect";
char *json_str = NULL;

char *getFileContent(const char *);
void printConf(BtnDetectConf *);
void initBtnDetect(BtnDetectConf *);
void exitBtnDetect();
int writeBtnResult(const char *, long);
void help();
void parse(int, char **);

static void handleSigInt(int signo)
{
	//	UINT32 idx = 0;

	if (signo == SIGINT) {
		printf("Caught SIGINT!\n");
	} else if (signo == SIGTERM) {
		printf("Caught SIGTERM!\n");
	} else {
		perror("Unexpected signal!\n");
	}

	if (json_str) {
		free(json_str);
		json_str = NULL;
	}

	exitBtnDetect();

	exit(0);
}

int main(int argc, char **argv)
{
	long usec;
	int err = -1;

	parse(argc, argv);
	printConf(&g_conf);
	initBtnDetect(&g_conf);

	/* TODO: add more test here */
	if (click_en) {
		usec = BTN_detectEvent(BTN_ACTION_PUSH_REL, 0);

		if (usec < 0) {
			printf("Failed to detect button release (%ld)!\n", usec);
		}

		err = writeBtnResult(BTN_DETECT_RES, usec);
		if (err < 0) {
			printf("Write result to path (%s) failed!\n", BTN_DETECT_RES);
		}
	}

	exitBtnDetect();

	return 0;
}

void parse(int argc, char **argv)
{
	json_object *jobj = NULL;
	int opt;

	while ((opt = getopt(argc, argv, "ch")) != -1) {
		switch (opt) {
		case 'c':
			click_en = 1;
			break;
		case 'h':
			help();
			break;
		default:
			exit(EXIT_FAILURE);
			break;
		}
	}

	if (optind >= argc) {
		fprintf(stderr, "Expect file name after options.\n");
		exit(EXIT_FAILURE);
	}

	json_str = getFileContent(argv[optind]);
	if (!json_str) {
		printf("Failed to read %s.", argv[optind]);
		exit(EXIT_FAILURE);
	}

	jobj = json_tokener_parse(json_str);
	if (jobj == NULL) {
		printf("Invalid JSON string.");
		goto free_json_str;
	}

	if (signal(SIGINT, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGINT!\n");
		goto free_json_str;
	}

	if (signal(SIGTERM, handleSigInt) == SIG_ERR) {
		perror("Cannot handle SIGTERM!\n");
		goto free_json_str;
	}

	parse_btn_detect(&g_conf, jobj);

	return;

free_json_str:

	free(json_str);
	json_str = NULL;
	exit(EXIT_FAILURE);
}

char *getFileContent(const char *path)
{
	char *ptr = NULL;
	FILE *fp;
	long fsize = 0;

	if ((fp = fopen(path, "r")) != NULL) {
		fseek(fp, 0L, SEEK_END);
		fsize = ftell(fp);
		fseek(fp, 0L, SEEK_SET);

		ptr = (char *)malloc(fsize + 1);

		if (!ptr) {
			goto end;
		}

		memset(ptr, 0, fsize);

		fread(ptr, fsize, 1, fp);

		ptr[fsize] = 0;

		if (ferror(fp)) {
			free(ptr);
		}
	}

end:
	fclose(fp);
	return ptr;
}

void initBtnDetect(BtnDetectConf *conf)
{
	if (BTN_init(&conf->btn_conf)) {
		goto end;
	}

	free(json_str);
	json_str = NULL;

	return;
end:
	free(json_str);
	json_str = NULL;
	exit(1);
}

void exitBtnDetect()
{
	BTN_exit();
}

int writeBtnResult(const char *path, long usec)
{
	int ret = 0;
	int fd = 0;
	char buf[128];

	sprintf(buf, "%ld", usec);

	fd = open(path, O_WRONLY);

	if (fd < 0) {
		printf("Failed to open path (%s)!\n", path);
		return fd;
	}

	if (write(fd, buf, strlen(buf)) < 0) {
		printf("Failed to writre (%ld) to path (%s)!\n", usec, path);
		ret = -1;
	}

	close(fd);

	return ret;
}

void printConf(BtnDetectConf *conf)
{
	printf("btn.gpio = %d\n", conf->btn_conf.btn.gpio);
	printf("btn.level_off = %d\n", conf->btn_conf.btn.level_off);
	printf("btn.level_on = %d\n", conf->btn_conf.btn.level_on);
	printf("period = %d\n", conf->btn_conf.period);
}

void help()
{
	printf("Usuage:\n");
	printf("\t$s [OPTION] <GPIO>\n");
	printf("\n");
	printf("\t-c\t\tdetect click event\n");
	exit(0);
}

