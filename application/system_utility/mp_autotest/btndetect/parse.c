#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#include <stdio.h>
#include <string.h>
#include <json.h>
#include "parse.h"


void parse_btn(Btn *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	if (json_object_object_get_ex(cmd_obj, "gpio", &tmp_obj)) {
		data->gpio = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "level_off", &tmp_obj)) {
		data->level_off = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "level_on", &tmp_obj)) {
		data->level_on = json_object_get_int(tmp_obj);
	}
}

void parse_btn_conf(BtnConf *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	if (json_object_object_get_ex(cmd_obj, "period", &tmp_obj)) {
		data->period = json_object_get_int(tmp_obj);
	}
	if (json_object_object_get_ex(cmd_obj, "btn", &tmp_obj)) {
		parse_btn(&data->btn, tmp_obj);
	}
}

void parse_btn_detect(BtnDetectConf *data, struct json_object *cmd_obj)
{
	struct json_object *tmp_obj;
	if (json_object_object_get_ex(cmd_obj, "btn_conf", &tmp_obj)) {
		parse_btn_conf(&data->btn_conf, tmp_obj);
	}
}


#ifdef __cplusplus
}
#endif /* __cplusplus */
