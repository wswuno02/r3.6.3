#ifndef MB_TEST_GPIO_H_
#define MB_TEST_GPIO_H_

int GPIO_setGpioVal(int, const int);
int GPIO_exportGpio(int);
int GPIO_unexportGpio(int);
int GPIO_setGpioDir(int, const char *);

#endif
