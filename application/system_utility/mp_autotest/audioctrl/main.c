#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <ado_ctrl.h>
#include <pthread.h>

typedef struct {
	int start;
	int stop;
	int play;
	int volume;
	char *filename;
} CFG_S;

static CFG_S g_cfgs;

void help(char *str)
{
	printf("USAGE:\n");
	printf("\t%s [OPTION]\n", str);
	printf("\n");
	printf("OPTION:\n");
	printf("\t'-s [FILE]' start record file\n");
	printf("\t'-e' stop record\n");
	printf("\t'-p [FILE]' play file\n");
	printf("\t'-v [VOL]' set volume when play file (0~100)\n");
	printf("\t'-h' help\n");
	printf("\n");
}

void init_cfg(CFG_S *config)
{
	config->start = 0;
	config->stop = 0;
	config->play = 0;
	config->volume = 100;
	config->filename = NULL;
}

void parse(int argc, char **argv, CFG_S *config)
{
	int c;

	init_cfg(config);

	while ((c = getopt(argc, argv, "s:ep:v:h")) != -1) {
		switch (c) {
		case 's':
			config->start = 1;
			config->filename = optarg;
			break;
		case 'e':
			config->stop = 1;
			break;
		case 'p':
			config->play = 1;
			config->filename = optarg;
			break;
		case 'v':
			config->volume = atoi(optarg);
			break;
		case 'h':
			help(argv[0]);
			exit(1);
			break;
		case '?':
			if ((optopt == 's') || (optopt == 'e') || (optopt == 'p') || (optopt == 'v') || (optopt == 'h')) {
				fprintf(stderr, "Option -%c requires an argument.\n", optopt);
			} else if (isprint(optopt)) {
				fprintf(stderr, "Unknown option '-%c'.\n", optopt);
			} else {
				fprintf(stderr, "Unknow option character '\\x%x'.\n", optopt);
			}
			return;
		default:
			abort();
		}
	}

	return;
}

int AUDIO_playFile(char *file, int volume)
{
	int fd, dsize = 0;
	int ret = -1;
	char *buffer;
	snd_pcm_uframes_t frames;
	frames = FRAME_SIZE;

	ret = ADOO_initSystem();
	if (ret < 0) {
		printf("%s: init system failed\n", __func__);
		return ret;
	}

	ret = ADOO_startSystem();
	if (ret < 0) {
		printf("%s: start system failed\n", __func__);
		return ret;
	}

	/* Set audio output volume */
	ret = ADOO_setVolume(volume);
	if (ret < 0) {
		printf("%s: set volume failed\n", __func__);
		return ret;
	}

	fd = open(file, O_CREAT | O_RDWR);
#define SAMPLE_SIZE_IN_BYTE 2
	buffer = (char *)malloc(frames * 2);
	while ((dsize = read(fd, buffer, frames * 2)) > 0) {
		ret = ADOO_setBitStream(buffer, dsize, 0);
		if (ret < 0) {
			break;
		}
	}

	close(fd);
	ADOO_closeSystem();
	free(buffer);

	return ret;
}

int main(int argc, char **argv)
{
	int ret = -1;

	/* parse config */
	parse(argc, argv, &g_cfgs);

	if (g_cfgs.start) {
		ret = ADO_recordFileStart(g_cfgs.filename);
		if (ret < 0) {
			printf("ADO_recordFileStart failed, %s.\n", g_cfgs.filename);
		} else {
			printf("ADO_recordFileStart success.\n");
		}
		pthread_exit(NULL);
	}

	if (g_cfgs.stop) {
		ret = ADO_recordFileStop();
		if (ret < 0) {
			printf("ADO_recordFileStop failed.\n");
		} else {
			printf("ADO_recordFileStop success.\n");
		}
	}

	if (g_cfgs.play) {
		ret = AUDIO_playFile(g_cfgs.filename, g_cfgs.volume);
		if (ret < 0) {
			printf("AUDIO_playFile failed, %s.\n", g_cfgs.filename);
		} else {
			printf("AUDIO_playFile success with volume %d.\n", g_cfgs.volume);
		}
	}

	return ret;
}
